package com.navdy.service.library.device.connection;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Process;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.TransactionTooLargeException;
import com.amazonaws.services.s3.internal.Constants;
import com.navdy.hud.app.IEventListener;
import com.navdy.hud.app.IEventSource;
import com.navdy.hud.app.IEventSource.Stub;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.device.RemoteDevice.LinkStatus;
import com.navdy.service.library.device.RemoteDeviceRegistry;
import com.navdy.service.library.device.connection.Connection.ConnectionFailureCause;
import com.navdy.service.library.device.connection.Connection.DisconnectCause;
import com.navdy.service.library.device.connection.ConnectionListener.Listener;
import com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.DeviceInfo.Builder;
import com.navdy.service.library.events.Ext_NavdyEvent;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.NavdyEvent.MessageType;
import com.navdy.service.library.events.NavdyEventUtil;
import com.navdy.service.library.events.Ping;
import com.navdy.service.library.events.WireUtil;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState;
import com.navdy.service.library.events.connection.ConnectionStatus;
import com.navdy.service.library.events.connection.ConnectionStatus.Status;
import com.navdy.service.library.events.connection.DisconnectRequest;
import com.navdy.service.library.events.connection.LinkPropertiesChanged;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.SystemUtils;
import com.squareup.wire.Message;
import com.squareup.wire.Wire;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public abstract class ConnectionService extends Service implements Listener, RemoteDevice.Listener {
    public static final UUID ACCESSORY_IAP2 = UUID.fromString("00000000-deca-fade-deca-deafdecacaff");
    public static final String ACTION_LINK_BANDWIDTH_LEVEL_CHANGED = "LINK_BANDWIDTH_LEVEL_CHANGED";
    public static final String CATEGORY_NAVDY_LINK = "NAVDY_LINK";
    private static final int CONNECT_TIMEOUT = 10000;
    private static final int DEAD_CONNECTION_TIME = 60000;
    private static final ConnectionStatus DEVICES_CHANGED_EVENT = new ConnectionStatus(Status.CONNECTION_PAIRED_DEVICES_CHANGED, null);
    public static final UUID DEVICE_IAP2 = UUID.fromString("00000000-deca-fade-deca-deafdecacafe");
    private static final int EVENT_DISCONNECT = 3;
    protected static final int EVENT_HEARTBEAT = 2;
    private static final int EVENT_RESTART_LISTENERS = 4;
    private static final int EVENT_STATE_CHANGE = 1;
    public static final String EXTRA_BANDWIDTH_LEVEL = "EXTRA_BANDWIDTH_MODE";
    private static final int HEARTBEAT_INTERVAL = 4000;
    private static final int IDLE_TIMEOUT = 1000;
    public static final String ILLEGAL_ARGUMENT = "ILLEGAL_ARGUMENT";
    public static final String NAVDY_IAP_NAME = "Navdy iAP";
    public static final String NAVDY_PROTO_SERVICE_NAME = "Navdy";
    public static final UUID NAVDY_PROTO_SERVICE_UUID = UUID.fromString("1992B7D7-C9B9-4F4F-AA7F-9FE33D95851E");
    public static final UUID NAVDY_PROXY_TUNNEL_UUID = UUID.fromString("D72BC85F-F015-4F24-A72F-35924E10888F");
    private static final int PAIRING_TIMEOUT = 30000;
    public static final String REASON_DEAD_CONNECTION = "DEAD_CONNECTION";
    private static final int RECONNECT_DELAY = 2000;
    private static final int RECONNECT_TIMEOUT = 10000;
    protected static final int RETRY_CONNECTING_AFTER_FAILURE_TIMEOUT = 30000;
    private static final int SLEEP_TIMEOUT = 60000;
    private BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (!intent.getAction().equals("android.bluetooth.adapter.action.STATE_CHANGED") || intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE) != 12) {
                return;
            }
            if (ConnectionService.this.state == State.START) {
                ConnectionService.this.logger.i("bluetooth turned on - exiting START state");
                ConnectionService.this.setState(State.IDLE);
                return;
            }
            ConnectionService.this.logger.i("bluetooth turned on - restarting listeners");
            ConnectionService.this.serviceHandler.sendEmptyMessage(4);
        }
    };
    private boolean broadcasting = false;
    protected final Object connectionLock = new Object();
    protected RemoteDeviceRegistry deviceRegistry;
    private boolean forceReconnect = false;
    protected boolean inProcess = true;
    private long lastMessageReceivedTime;
    protected final Object listenerLock = new Object();
    private IEventListener[] listenersArray = new IEventListener[0];
    private boolean listening = false;
    protected final Logger logger = new Logger(getClass());
    protected ConnectionListener[] mConnectionListeners;
    private Stub mEventSource = new Stub() {
        public void addEventListener(IEventListener listener) throws RemoteException {
            synchronized (ConnectionService.this.listenerLock) {
                ConnectionService.this.mListeners.add(listener);
                ConnectionService.this.createListenerList();
            }
            ConnectionService.this.logger.v("listener added: pid:" + SystemUtils.getProcessName(ConnectionService.this.getApplicationContext(), AnonymousClass1.getCallingPid()));
            if (ConnectionService.this.mRemoteDevice == null || !ConnectionService.this.mRemoteDevice.isConnected()) {
                ConnectionService.this.forwardEventLocally(new ConnectionStateChange("", ConnectionState.CONNECTION_DISCONNECTED));
                ConnectionService.this.forwardEventLocally(new ConnectionStateChange("", ConnectionState.CONNECTION_LINK_LOST));
            } else {
                ConnectionService.this.forwardEventLocally(new ConnectionStateChange(ConnectionService.this.mRemoteDevice.getDeviceId().toString(), ConnectionState.CONNECTION_LINK_ESTABLISHED));
                ConnectionService.this.forwardEventLocally(new ConnectionStateChange(ConnectionService.this.mRemoteDevice.getDeviceId().toString(), ConnectionState.CONNECTION_CONNECTED));
                Message deviceInfo = ConnectionService.this.mRemoteDevice.getDeviceInfo();
                if (deviceInfo != null) {
                    ConnectionService.this.logger.v("send device info");
                    ConnectionService.this.forwardEventLocally(deviceInfo);
                }
            }
            ConnectionService.this.sendEventsOnLocalConnect();
        }

        public void removeEventListener(IEventListener listener) throws RemoteException {
            synchronized (ConnectionService.this.listenerLock) {
                ConnectionService.this.mListeners.remove(listener);
                ConnectionService.this.createListenerList();
            }
            ConnectionService.this.logger.v("listener removed: pid:" + SystemUtils.getProcessName(ConnectionService.this.getApplicationContext(), AnonymousClass1.getCallingPid()));
        }

        public void postEvent(byte[] bytes) throws RemoteException {
            boolean sentEvent = false;
            boolean createList = false;
            synchronized (ConnectionService.this.listenerLock) {
                for (int i = 0; i < ConnectionService.this.listenersArray.length; i++) {
                    try {
                        ConnectionService.this.listenersArray[i].onEvent(bytes);
                        sentEvent = true;
                    } catch (DeadObjectException exception) {
                        ConnectionService.this.logger.w("Reaping dead listener", exception);
                        ConnectionService.this.mListeners.remove(ConnectionService.this.listenersArray[i]);
                        createList = true;
                    } catch (TransactionTooLargeException tooLargeException) {
                        ConnectionService.this.logger.w("Communication Pipe is full:", tooLargeException);
                    } catch (Throwable throwable) {
                        ConnectionService.this.logger.w("Exception throws by remote:", throwable);
                    }
                }
                if (ConnectionService.this.logger.isLoggable(2) && !sentEvent) {
                    ConnectionService.this.logger.d("No one listening for event - byte length " + bytes.length);
                }
                if (createList) {
                    ConnectionService.this.createListenerList();
                }
            }
        }

        public void postRemoteEvent(String remoteDeviceId, byte[] bytes) throws RemoteException {
            if (remoteDeviceId == null || bytes == null) {
                ConnectionService.this.logger.e("illegal argument");
                throw new RemoteException(ConnectionService.ILLEGAL_ARGUMENT);
            }
            NavdyDeviceId deviceId = new NavdyDeviceId(remoteDeviceId);
            if (deviceId.equals(NavdyDeviceId.getThisDevice(ConnectionService.this))) {
                MessageType messageType = WireUtil.getEventType(bytes);
                if (messageType == null || !ConnectionService.this.processLocalEvent(bytes, messageType)) {
                    ConnectionService.this.logger.w("Connection service ignored message:" + messageType);
                }
            } else if (ConnectionService.this.mRemoteDevice == null || !ConnectionService.this.mRemoteDevice.isConnected()) {
                if (ConnectionService.this.logger.isLoggable(2)) {
                    ConnectionService.this.logger.e("app not connected");
                }
            } else if (ConnectionService.this.mRemoteDevice.getDeviceId().equals(deviceId)) {
                ConnectionService.this.mRemoteDevice.postEvent(bytes);
            } else {
                ConnectionService.this.logger.i("Device id mismatch: deviceId=" + deviceId + " remote:" + ConnectionService.this.mRemoteDevice.getDeviceId());
            }
        }
    };
    private final List<IEventListener> mListeners = new ArrayList();
    protected RemoteDevice mRemoteDevice;
    protected RemoteDeviceBroadcaster[] mRemoteDeviceBroadcasters;
    protected Wire mWire;
    private PendingConnectHandler pendingConnectHandler;
    private ProxyService proxyService;
    private volatile boolean quitting;
    protected Runnable reconnectRunnable = new Runnable() {
        public void run() {
            if (ConnectionService.this.state == State.RECONNECTING && ConnectionService.this.mRemoteDevice != null && !ConnectionService.this.mRemoteDevice.isConnecting() && !ConnectionService.this.mRemoteDevice.isConnected()) {
                ConnectionService.this.logger.i("Retrying to connect to " + ConnectionService.this.mRemoteDevice.getDeviceId());
                ConnectionService.this.mRemoteDevice.connect();
            }
        }
    };
    protected boolean serverMode = false;
    protected volatile ServiceHandler serviceHandler;
    private volatile Looper serviceLooper;
    protected State state = State.START;
    private long stateAge = 0;

    private class PendingConnectHandler implements Runnable {
        private Connection connection;
        private RemoteDevice device;

        public PendingConnectHandler(RemoteDevice device, Connection connection) {
            this.device = device;
            this.connection = connection;
        }

        public void run() {
            boolean newDevice = ConnectionService.this.mRemoteDevice != this.device;
            ConnectionService.this.logger.d("Trying to " + (newDevice ? "connect" : "reconnect") + " to " + this.device.getDeviceId().getDisplayName());
            ConnectionService.this.setRemoteDevice(this.device);
            if (this.connection != null) {
                ConnectionService.this.mRemoteDevice.setActiveConnection(this.connection);
            } else if (ConnectionService.this.mRemoteDevice.isConnected()) {
                ConnectionService.this.onDeviceConnected(ConnectionService.this.mRemoteDevice);
            } else {
                ConnectionService.this.setState(newDevice ? State.CONNECTING : State.RECONNECTING);
                ConnectionService.this.mRemoteDevice.connect();
            }
            ConnectionService.this.pendingConnectHandler = null;
        }
    }

    protected final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case 1:
                    State newState = msg.obj;
                    if (newState != ConnectionService.this.state) {
                        ConnectionService.this.exitState(ConnectionService.this.state);
                        ConnectionService.this.state = newState;
                        ConnectionService.this.enterState(ConnectionService.this.state);
                        return;
                    }
                    return;
                case 2:
                    ConnectionService.this.heartBeat();
                    return;
                case 3:
                    RemoteDevice device = msg.obj;
                    ConnectionService.this.handleDeviceDisconnect(device, DisconnectCause.values()[msg.arg1]);
                    return;
                case 4:
                    if (ConnectionService.this.listening) {
                        ConnectionService.this.logger.i("stopping/starting listeners");
                        ConnectionService.this.stopListeners();
                        ConnectionService.this.startListeners();
                    }
                    if (ConnectionService.this.broadcasting) {
                        ConnectionService.this.logger.i("restarting broadcasters");
                        ConnectionService.this.stopBroadcasters();
                        ConnectionService.this.startBroadcasters();
                    }
                    ConnectionService.this.stopProxyService();
                    ConnectionService.this.startProxyService();
                    return;
                default:
                    ConnectionService.this.logger.e("Unknown message: " + msg);
                    return;
            }
        }
    }

    public enum State {
        START,
        IDLE,
        SEARCHING,
        CONNECTING,
        RECONNECTING,
        CONNECTED,
        DISCONNECTING,
        DISCONNECTED,
        PAIRING,
        LISTENING,
        DESTROYED
    }

    protected abstract ProxyService createProxyService() throws IOException;

    protected abstract ConnectionListener[] getConnectionListeners(Context context);

    protected abstract RemoteDeviceBroadcaster[] getRemoteDeviceBroadcasters();

    private void sendPingIfNeeded() {
        try {
            if (this.mRemoteDevice != null && this.mRemoteDevice.isConnected() && SystemClock.elapsedRealtime() - this.mRemoteDevice.lastMessageSentTime > 4000) {
                this.mRemoteDevice.postEvent(new Ping());
                if (this.serverMode && this.logger.isLoggable(2)) {
                    this.logger.v("NAVDY-PACKET [H2P-Outgoing-Event] Ping");
                }
            }
        } catch (Throwable t) {
            this.logger.e(t);
        }
    }

    private void checkConnection() {
        if (this.mRemoteDevice == null || !this.mRemoteDevice.isConnected()) {
            this.logger.v("dead connection remotedevice=" + (this.mRemoteDevice == null ? Constants.NULL_VERSION_ID : "not null") + " isConnected:" + (this.mRemoteDevice == null ? "false" : Boolean.valueOf(this.mRemoteDevice.isConnected())));
            setState(State.DISCONNECTING);
        } else if (this.mRemoteDevice.getLinkStatus() == LinkStatus.CONNECTED) {
            long timeElapsed = SystemClock.elapsedRealtime() - this.lastMessageReceivedTime;
            if (timeElapsed > 60000) {
                this.logger.v("dead connection timed out:" + timeElapsed);
                if (reconnectAfterDeadConnection()) {
                    this.logger.d("Reconncting after dead connection timed out disconnect");
                    reconnect(REASON_DEAD_CONNECTION);
                    return;
                }
                setState(State.DISCONNECTING);
            }
        }
    }

    public boolean reconnectAfterDeadConnection() {
        return false;
    }

    public IBinder onBind(Intent intent) {
        if (IEventSource.class.getName().equals(intent.getAction())) {
            this.logger.d("returning IEventSource API endpoint");
            return this.mEventSource;
        }
        this.logger.w("invalid action:" + intent.getAction());
        return null;
    }

    public void onCreate() {
        this.logger.e("ConnectionService created: mainthread:" + Looper.getMainLooper().getThread().getId());
        this.mWire = new Wire(Ext_NavdyEvent.class);
        HandlerThread thread = new HandlerThread("ConnectionService");
        thread.start();
        this.serviceLooper = thread.getLooper();
        this.serviceHandler = new ServiceHandler(this.serviceLooper);
        registerReceiver(this.bluetoothReceiver, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
        this.deviceRegistry = RemoteDeviceRegistry.getInstance(this);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            if (this.state == State.CONNECTED) {
                this.logger.i("BT enabled, disconnecting since app likely crashed");
                setState(State.DISCONNECTING);
            } else {
                this.logger.i("BT enabled, switching to IDLE state");
                setState(State.IDLE);
            }
        }
        return 1;
    }

    public void onDestroy() {
        this.logger.v("ConnectionService destroyed.  mwhahahaha.");
        try {
            setState(State.DESTROYED);
            unregisterReceiver(this.bluetoothReceiver);
            this.serviceLooper.quitSafely();
            stopProxyService();
        } catch (Throwable t) {
            this.logger.e(t);
        }
    }

    protected void forwardEventLocally(Message message) {
        forwardEventLocally(NavdyEventUtil.eventFromMessage(message).toByteArray());
    }

    protected void forwardEventLocally(byte[] eventData) {
        try {
            this.mEventSource.postEvent(eventData);
        } catch (Throwable t) {
            this.logger.e(t);
        }
    }

    protected void startBroadcasters() {
        synchronized (this.connectionLock) {
            this.broadcasting = true;
            if (this.mRemoteDeviceBroadcasters == null) {
                this.mRemoteDeviceBroadcasters = getRemoteDeviceBroadcasters();
            }
            for (int i = 0; i < this.mRemoteDeviceBroadcasters.length; i++) {
                this.logger.v("starting connection broadcaster:" + this.mRemoteDeviceBroadcasters[i].getClass().getName());
                try {
                    this.mRemoteDeviceBroadcasters[i].start();
                } catch (Throwable t) {
                    this.logger.e(t);
                }
            }
        }
    }

    protected void stopBroadcasters() {
        this.logger.v("stopping all broadcasters");
        synchronized (this.connectionLock) {
            this.broadcasting = false;
            if (this.mRemoteDeviceBroadcasters != null) {
                for (int i = 0; i < this.mRemoteDeviceBroadcasters.length; i++) {
                    this.logger.v("stopping connection broadcaster:" + this.mRemoteDeviceBroadcasters[i].getClass().getName());
                    try {
                        this.mRemoteDeviceBroadcasters[i].stop();
                    } catch (Throwable t) {
                        this.logger.e(t);
                    }
                }
            }
        }
    }

    protected void startListeners() {
        synchronized (this.connectionLock) {
            int i;
            this.listening = true;
            if (this.mConnectionListeners == null) {
                this.logger.v("initializing connector listener");
                this.mConnectionListeners = getConnectionListeners(getApplicationContext());
                for (ConnectionListener addListener : this.mConnectionListeners) {
                    addListener.addListener(this);
                }
            }
            for (i = 0; i < this.mConnectionListeners.length; i++) {
                this.logger.v("starting connection listener:" + this.mConnectionListeners[i]);
                try {
                    this.mConnectionListeners[i].start();
                } catch (Throwable t) {
                    this.logger.e(t);
                }
            }
        }
    }

    protected void stopListeners() {
        synchronized (this.connectionLock) {
            this.listening = false;
            if (this.mConnectionListeners != null) {
                for (int i = 0; i < this.mConnectionListeners.length; i++) {
                    this.logger.v("stopping:" + this.mConnectionListeners[i]);
                    try {
                        this.mConnectionListeners[i].stop();
                    } catch (Throwable t) {
                        this.logger.e(t);
                    }
                }
            }
        }
    }

    public void onStarted(ConnectionListener connectionListener) {
        this.logger.v("started listening");
    }

    public void onStartFailure(ConnectionListener connectionListener) {
        this.logger.e("failed to start listening:" + connectionListener);
    }

    public void onStopped(ConnectionListener connectionListener) {
        this.logger.v("stopped listening:" + connectionListener);
    }

    public void onConnected(ConnectionListener connectionListener, Connection connection) {
        this.logger.v("listener connected");
        setActiveDevice(new RemoteDevice(getApplicationContext(), connection.getConnectionInfo(), this.inProcess), connection);
    }

    public void onConnectionFailed(ConnectionListener connectionListener) {
        this.logger.e("onConnectionFailed:restart listeners");
        setState(State.IDLE);
    }

    protected synchronized void setRemoteDevice(RemoteDevice remoteDevice) {
        if (remoteDevice != this.mRemoteDevice) {
            if (this.mRemoteDevice != null) {
                this.mRemoteDevice.removeListener(this);
            }
            this.mRemoteDevice = remoteDevice;
            if (this.mRemoteDevice != null) {
                this.mRemoteDevice.addListener(this);
            }
        }
    }

    public void connect(ConnectionInfo connectionInfo) {
        setActiveDevice(new RemoteDevice((Context) this, connectionInfo, this.inProcess));
    }

    public boolean setActiveDevice(RemoteDevice remoteDevice) {
        return setActiveDevice(remoteDevice, null);
    }

    public synchronized void reconnect(String reconnectReason) {
        if (this.mRemoteDevice != null) {
            this.forceReconnect = true;
            setState(State.DISCONNECTING);
        }
    }

    public synchronized boolean setActiveDevice(RemoteDevice remoteDevice, Connection connection) {
        if (this.mRemoteDevice != null) {
            if (this.mRemoteDevice.isConnected() || this.mRemoteDevice.isConnecting()) {
                setState(State.DISCONNECTING);
            } else {
                setRemoteDevice(null);
            }
        }
        if (remoteDevice != null) {
            PendingConnectHandler connectHandler = new PendingConnectHandler(remoteDevice, connection);
            if (this.mRemoteDevice == null) {
                connectHandler.run();
            } else {
                this.pendingConnectHandler = connectHandler;
            }
        }
        return true;
    }

    private boolean hasPaired() {
        return this.deviceRegistry.hasPaired();
    }

    public void setState(State newState) {
        if (!this.quitting && this.state != newState) {
            if (newState == State.DESTROYED) {
                this.quitting = true;
            }
            this.serviceHandler.sendMessage(this.serviceHandler.obtainMessage(1, newState));
        }
    }

    protected void enterState(State state) {
        this.logger.d("Entering state:" + state);
        if (state == State.DESTROYED) {
            if (this.listening) {
                stopListeners();
            }
            if (this.broadcasting) {
                stopBroadcasters();
            }
            this.serviceHandler.removeCallbacksAndMessages(null);
            return;
        }
        this.stateAge = 0;
        int timeout = 1000;
        switch (state) {
            case IDLE:
                timeout = 1000;
                break;
            case PAIRING:
                timeout = 30000;
                startBroadcasters();
                startListeners();
                forwardEventLocally(new ConnectionStatus(Status.CONNECTION_PAIRING, ""));
                break;
            case LISTENING:
                if (isPromiscuous()) {
                    startBroadcasters();
                }
                startListeners();
                if (this.serverMode) {
                    timeout = 30000;
                    break;
                }
                break;
            case CONNECTING:
                timeout = 10000;
                break;
            case RECONNECTING:
                timeout = 10000;
                break;
            case DISCONNECTING:
                if (this.mRemoteDevice != null) {
                    this.mRemoteDevice.disconnect();
                    break;
                }
                break;
            case CONNECTED:
                stopListeners();
                stopBroadcasters();
                handleDeviceConnect(this.mRemoteDevice);
                break;
        }
        this.serviceHandler.removeMessages(2);
        this.serviceHandler.sendEmptyMessageDelayed(2, (long) timeout);
    }

    protected void exitState(State state) {
        this.logger.d("Exiting state:" + state);
        switch (state) {
            case PAIRING:
                if (!isPromiscuous()) {
                    stopBroadcasters();
                    return;
                }
                return;
            case RECONNECTING:
                this.serviceHandler.removeCallbacks(this.reconnectRunnable);
                return;
            case START:
                startProxyService();
                return;
            default:
                return;
        }
    }

    protected void heartBeat() {
        if (this.stateAge % 10 == 0) {
            this.logger.d("Heartbeat: in state:" + this.state);
        }
        this.stateAge++;
        int timeout = 1000;
        switch (this.state) {
            case IDLE:
                if (this.pendingConnectHandler == null) {
                    if (hasPaired() || !this.serverMode) {
                        if (!hasPaired()) {
                            timeout = 60000;
                            break;
                        } else {
                            setState(State.LISTENING);
                            break;
                        }
                    }
                    setState(State.PAIRING);
                    break;
                }
                this.pendingConnectHandler.run();
                break;
                break;
            case PAIRING:
                timeout = 30000;
                break;
            case LISTENING:
                if (this.serverMode) {
                    if (this.mRemoteDevice == null) {
                        if (!needAutoSearch()) {
                            timeout = 60000;
                            break;
                        } else {
                            setState(State.SEARCHING);
                            break;
                        }
                    }
                    setState(State.RECONNECTING);
                    this.serviceHandler.post(this.reconnectRunnable);
                    break;
                }
                timeout = 60000;
                break;
            case CONNECTING:
            case RECONNECTING:
                setState(State.IDLE);
                break;
            case CONNECTED:
                timeout = HEARTBEAT_INTERVAL;
                sendPingIfNeeded();
                checkConnection();
                break;
        }
        this.serviceHandler.removeMessages(2);
        this.serviceHandler.sendEmptyMessageDelayed(2, (long) timeout);
    }

    public boolean needAutoSearch() {
        return true;
    }

    public boolean isPromiscuous() {
        return false;
    }

    public void onDeviceConnecting(RemoteDevice device) {
    }

    public void onDeviceConnected(RemoteDevice device) {
        setState(State.CONNECTED);
    }

    public void onDeviceConnectFailure(RemoteDevice device, ConnectionFailureCause cause) {
        if (this.state == State.RECONNECTING) {
            this.logger.i("onDeviceConnectFailure - retrying");
            this.serviceHandler.postDelayed(this.reconnectRunnable, 2000);
            return;
        }
        this.logger.i("onDeviceConnectFailure - switching to idle state");
        if (device != this.mRemoteDevice || this.serverMode) {
            this.logger.d("Not clearing the remote device, to attempt reconnect");
        } else {
            setRemoteDevice(null);
        }
        setState(State.IDLE);
    }

    public void onDeviceDisconnected(RemoteDevice device, DisconnectCause cause) {
        this.serviceHandler.sendMessage(this.serviceHandler.obtainMessage(3, cause.ordinal(), -1, device));
    }

    protected void rememberPairedDevice(RemoteDevice device) {
        this.deviceRegistry.addPairedConnection(device.getActiveConnectionInfo());
        forwardEventLocally(DEVICES_CHANGED_EVENT);
    }

    protected void forgetPairedDevice(RemoteDevice device) {
        this.deviceRegistry.removePairedConnection(device.getActiveConnectionInfo());
        forwardEventLocally(DEVICES_CHANGED_EVENT);
    }

    protected void forgetPairedDevice(BluetoothDevice device) {
        this.deviceRegistry.removePairedConnection(device);
        forwardEventLocally(DEVICES_CHANGED_EVENT);
    }

    private void handleDeviceConnect(RemoteDevice device) {
        this.logger.v("onDeviceConnected:remembering device");
        rememberPairedDevice(device);
        this.logger.v("onDeviceConnected:heartBeat started");
        this.lastMessageReceivedTime = SystemClock.elapsedRealtime();
        this.logger.d("Connecting with app context: " + getApplicationContext());
        if (!this.mRemoteDevice.startLink()) {
            setState(State.DISCONNECTING);
        }
    }

    protected void handleDeviceDisconnect(RemoteDevice device, DisconnectCause cause) {
        this.logger.v("device disconnect:" + device + " cause:" + cause);
        if (device != null) {
            device.stopLink();
        }
        if (this.mRemoteDevice != device) {
            return;
        }
        if ((cause == DisconnectCause.ABORTED || this.forceReconnect) && this.serverMode) {
            this.forceReconnect = false;
            new PendingConnectHandler(this.mRemoteDevice, null).run();
            return;
        }
        setRemoteDevice(null);
        setState(State.IDLE);
    }

    public void onNavdyEventReceived(RemoteDevice device, byte[] eventData) {
        this.lastMessageReceivedTime = SystemClock.elapsedRealtime();
        MessageType messageType = WireUtil.getEventType(eventData);
        if (this.logger.isLoggable(2)) {
            this.logger.v((this.serverMode ? "NAVDY-PACKET [P2H" : "NAVDY-PACKET [H2P") + "-Event] " + (messageType != null ? messageType.name() : "UNKNOWN") + " size:" + eventData.length);
        }
        if (messageType != null && messageType != MessageType.Ping) {
            if (messageType == MessageType.DisconnectRequest) {
                try {
                    handleRemoteDisconnect((DisconnectRequest) ((NavdyEvent) this.mWire.parseFrom(eventData, NavdyEvent.class)).getExtension(Ext_NavdyEvent.disconnectRequest));
                } catch (Throwable t) {
                    this.logger.e("Failed to parse event", t);
                }
            } else if (!processEvent(eventData, messageType)) {
                if (messageType == MessageType.DeviceInfo) {
                    try {
                        if (this.mRemoteDevice != null && this.mRemoteDevice.isConnected()) {
                            DeviceInfo patchedDeviceInfo = new Builder((DeviceInfo) NavdyEventUtil.messageFromEvent((NavdyEvent) this.mWire.parseFrom(eventData, NavdyEvent.class))).deviceId(this.mRemoteDevice.getDeviceId().toString()).build();
                            this.mRemoteDevice.setDeviceInfo(patchedDeviceInfo);
                            this.logger.v("set remote device info");
                            forwardEventLocally(NavdyEventUtil.eventFromMessage(patchedDeviceInfo).toByteArray());
                            return;
                        }
                        return;
                    } catch (Throwable t2) {
                        this.logger.e("Failed to parse deviceinfo", t2);
                        return;
                    }
                }
                forwardEventLocally(eventData);
            }
        }
    }

    public void onNavdyEventReceived(RemoteDevice device, NavdyEvent event) {
        this.lastMessageReceivedTime = SystemClock.elapsedRealtime();
        if (event.type == MessageType.DisconnectRequest) {
            handleRemoteDisconnect((DisconnectRequest) event.getExtension(Ext_NavdyEvent.disconnectRequest));
        }
    }

    protected boolean processEvent(byte[] eventData, MessageType messageType) {
        return false;
    }

    protected boolean processLocalEvent(byte[] eventData, MessageType messageType) {
        return false;
    }

    private void createListenerList() {
        this.listenersArray = new IEventListener[this.mListeners.size()];
        this.mListeners.toArray(this.listenersArray);
    }

    private void handleRemoteDisconnect(DisconnectRequest disconnectRequest) {
        if (this.mRemoteDevice != null) {
            if (disconnectRequest.forget != null && disconnectRequest.forget.booleanValue()) {
                forgetPairedDevice(this.mRemoteDevice);
            }
            setState(State.DISCONNECTING);
        }
    }

    protected void startProxyService() {
        if (this.proxyService == null || !this.proxyService.isAlive()) {
            this.logger.v(SystemUtils.getProcessName(getApplicationContext(), Process.myPid()) + ": start service for proxy");
            try {
                this.proxyService = createProxyService();
                this.proxyService.start();
            } catch (Exception e) {
                this.logger.e("Failed to start proxy service", e);
            }
        }
    }

    protected void stopProxyService() {
        if (this.proxyService != null) {
            this.proxyService.cancel();
            this.proxyService = null;
        }
    }

    public void sendMessage(Message event) {
        if (isConnected()) {
            this.mRemoteDevice.postEvent(event);
        }
    }

    public boolean isConnected() {
        return this.mRemoteDevice != null && this.mRemoteDevice.isConnected();
    }

    protected void sendEventsOnLocalConnect() {
    }

    protected void setBandwidthLevel(int level) {
        if (this.state == State.CONNECTED && this.mRemoteDevice != null) {
            if (level == 0 || level == 1) {
                this.mRemoteDevice.setLinkBandwidthLevel(level);
                LinkPropertiesChanged.Builder builder = new LinkPropertiesChanged.Builder();
                builder.bandwidthLevel(Integer.valueOf(level));
                forwardEventLocally(builder.build());
            }
        }
    }
}
