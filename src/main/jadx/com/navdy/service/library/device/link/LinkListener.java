package com.navdy.service.library.device.link;

import com.navdy.service.library.device.connection.Connection.DisconnectCause;
import com.navdy.service.library.device.connection.ConnectionType;

public interface LinkListener {
    void linkEstablished(ConnectionType connectionType);

    void linkLost(ConnectionType connectionType, DisconnectCause disconnectCause);

    void onNavdyEventReceived(byte[] bArr);

    void onNetworkLinkReady();
}
