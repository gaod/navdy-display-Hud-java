package com.navdy.service.library.device.connection;

import android.net.nsd.NsdServiceInfo;
import com.google.gson.annotations.SerializedName;
import com.navdy.service.library.device.NavdyDeviceId;

public class TCPConnectionInfo extends ConnectionInfo {
    public static final String FORMAT_SERVICE_NAME_WITH_DEVICE_ID = "Navdy %s";
    public static final int SERVICE_PORT = 21301;
    public static final String SERVICE_TYPE = "_navdybus._tcp.";
    @SerializedName("address")
    String mHostName;

    public static NsdServiceInfo nsdServiceWithDeviceId(NavdyDeviceId deviceId) {
        NsdServiceInfo serviceInfo = new NsdServiceInfo();
        serviceInfo.setServiceName(String.format(FORMAT_SERVICE_NAME_WITH_DEVICE_ID, new Object[]{deviceId.toString()}));
        serviceInfo.setServiceType(SERVICE_TYPE);
        serviceInfo.setPort(SERVICE_PORT);
        return serviceInfo;
    }

    public TCPConnectionInfo(NavdyDeviceId deviceId, String hostName) {
        super(deviceId, ConnectionType.TCP_PROTOBUF);
        if (hostName == null) {
            throw new IllegalArgumentException("hostname required");
        }
        this.mHostName = hostName;
    }

    public TCPConnectionInfo(NsdServiceInfo serviceInfo) {
        super(serviceInfo);
        String hostName = serviceInfo.getHost().getHostAddress();
        if (hostName == null) {
            throw new IllegalArgumentException("hostname not found in service record");
        }
        this.mHostName = hostName;
    }

    public ServiceAddress getAddress() {
        return new ServiceAddress(this.mHostName, String.valueOf(SERVICE_PORT));
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass() || !super.equals(o)) {
            return false;
        }
        return this.mHostName.equals(((TCPConnectionInfo) o).mHostName);
    }

    public int hashCode() {
        return (super.hashCode() * 31) + this.mHostName.hashCode();
    }
}
