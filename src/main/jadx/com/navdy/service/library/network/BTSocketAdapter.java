package com.navdy.service.library.network;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Build.VERSION;
import android.os.ParcelFileDescriptor;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;

public class BTSocketAdapter implements SocketAdapter {
    private static final boolean needsPatch;
    private static Logger sLogger = new Logger(BTSocketAdapter.class);
    private boolean closed = false;
    private int mFd = -1;
    private ParcelFileDescriptor mPfd;
    final BluetoothSocket socket;

    static {
        boolean z = VERSION.SDK_INT >= 17 && VERSION.SDK_INT <= 20;
        needsPatch = z;
    }

    public BTSocketAdapter(BluetoothSocket socket) {
        if (socket == null) {
            throw new IllegalArgumentException("Socket must not be null");
        }
        this.socket = socket;
        if (needsPatch) {
            storeFd();
        }
    }

    public void connect() throws IOException {
        try {
            this.socket.connect();
            if (needsPatch) {
                storeFd();
            }
        } catch (IOException e) {
            if (needsPatch) {
                storeFd();
            }
            throw e;
        }
    }

    public void close() throws IOException {
        synchronized (this.socket) {
            if (!this.closed) {
                if (needsPatch) {
                    storeFd();
                }
                this.socket.close();
                if (needsPatch) {
                    cleanupFd();
                }
                this.closed = true;
            }
        }
    }

    public InputStream getInputStream() throws IOException {
        return this.socket.getInputStream();
    }

    public OutputStream getOutputStream() throws IOException {
        return this.socket.getOutputStream();
    }

    public boolean isConnected() {
        return this.socket.isConnected();
    }

    public NavdyDeviceId getRemoteDevice() {
        BluetoothDevice device = this.socket.getRemoteDevice();
        if (device != null) {
            return new NavdyDeviceId(device);
        }
        return null;
    }

    private synchronized void storeFd() {
        if (this.mPfd == null || this.mFd == -1) {
            try {
                Field field = this.socket.getClass().getDeclaredField("mPfd");
                field.setAccessible(true);
                ParcelFileDescriptor pfd = (ParcelFileDescriptor) field.get(this.socket);
                if (pfd != null) {
                    this.mPfd = pfd;
                }
                int fd = IOUtils.getSocketFD(this.socket);
                if (fd != -1) {
                    this.mFd = fd;
                }
                sLogger.d("Stored " + this.mPfd + " fd:" + this.mFd);
            } catch (Exception e) {
                sLogger.w("Exception storing socket internals", e);
            }
        }
        return;
    }

    private synchronized void cleanupFd() throws IOException {
        if (this.mPfd != null) {
            sLogger.d("Closing mPfd");
            this.mPfd.close();
            this.mPfd = null;
        }
        if (this.mFd != -1) {
            sLogger.d("Closing mFd:" + this.mFd);
            IOUtils.closeFD(this.mFd);
            this.mFd = -1;
        }
    }
}
