package com.navdy.service.library.network;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.Build.VERSION;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.connection.BTConnectionInfo;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.device.connection.ConnectionType;
import com.navdy.service.library.log.Logger;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.UUID;

public class BTSocketAcceptor implements SocketAcceptor {
    private static final boolean needsPatch;
    private static Logger sLogger = new Logger(BTSocketAcceptor.class);
    BluetoothServerSocket btServerSocket;
    private BTSocketAdapter internalSocket;
    private final String sdpName;
    private final boolean secure;
    private final UUID serviceUUID;

    static {
        boolean z = VERSION.SDK_INT >= 17 && VERSION.SDK_INT <= 20;
        needsPatch = z;
    }

    public BTSocketAcceptor(String sdpName, UUID serviceUUID, boolean secure) {
        this.sdpName = sdpName;
        this.serviceUUID = serviceUUID;
        this.secure = secure;
    }

    public BTSocketAcceptor(String sdpName, UUID serviceUUID) {
        this(sdpName, serviceUUID, true);
    }

    public SocketAdapter accept() throws IOException {
        if (this.btServerSocket == null) {
            BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
            if (adapter == null) {
                throw new IOException("Bluetooth unavailable");
            }
            if (this.secure) {
                this.btServerSocket = adapter.listenUsingRfcommWithServiceRecord(this.sdpName, this.serviceUUID);
            } else {
                this.btServerSocket = adapter.listenUsingInsecureRfcommWithServiceRecord(this.sdpName, this.serviceUUID);
            }
            if (needsPatch) {
                storeSocket();
            }
        }
        return new BTSocketAdapter(this.btServerSocket.accept());
    }

    public void close() throws IOException {
        if (this.btServerSocket != null) {
            this.btServerSocket.close();
            if (this.internalSocket != null) {
                this.internalSocket.close();
                this.internalSocket = null;
            }
            this.btServerSocket = null;
        }
    }

    public ConnectionInfo getRemoteConnectionInfo(SocketAdapter socket, ConnectionType connectionType) {
        NavdyDeviceId deviceId = socket.getRemoteDevice();
        if (deviceId != null) {
            return new BTConnectionInfo(deviceId, this.serviceUUID, connectionType);
        }
        return null;
    }

    private void storeSocket() {
        try {
            Field mSocketField = this.btServerSocket.getClass().getDeclaredField("mSocket");
            mSocketField.setAccessible(true);
            this.internalSocket = new BTSocketAdapter((BluetoothSocket) mSocketField.get(this.btServerSocket));
        } catch (Exception e) {
            sLogger.w("Failed to close server socket", e);
        }
    }
}
