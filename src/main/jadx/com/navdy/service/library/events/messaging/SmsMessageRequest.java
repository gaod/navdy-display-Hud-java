package com.navdy.service.library.events.messaging;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class SmsMessageRequest extends Message {
    public static final String DEFAULT_ID = "";
    public static final String DEFAULT_MESSAGE = "";
    public static final String DEFAULT_NAME = "";
    public static final String DEFAULT_NUMBER = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String id;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String message;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String name;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String number;

    public static final class Builder extends com.squareup.wire.Message.Builder<SmsMessageRequest> {
        public String id;
        public String message;
        public String name;
        public String number;

        public Builder(SmsMessageRequest message) {
            super(message);
            if (message != null) {
                this.number = message.number;
                this.message = message.message;
                this.name = message.name;
                this.id = message.id;
            }
        }

        public Builder number(String number) {
            this.number = number;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public SmsMessageRequest build() {
            checkRequiredFields();
            return new SmsMessageRequest();
        }
    }

    public SmsMessageRequest(String number, String message, String name, String id) {
        this.number = number;
        this.message = message;
        this.name = name;
        this.id = id;
    }

    private SmsMessageRequest(Builder builder) {
        this(builder.number, builder.message, builder.name, builder.id);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof SmsMessageRequest)) {
            return false;
        }
        SmsMessageRequest o = (SmsMessageRequest) other;
        if (equals( this.number,  o.number) && equals( this.message,  o.message) && equals( this.name,  o.name) && equals( this.id,  o.id)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.number != null ? this.number.hashCode() : 0) * 37;
        if (this.message != null) {
            hashCode = this.message.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.name != null) {
            hashCode = this.name.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.id != null) {
            i = this.id.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
