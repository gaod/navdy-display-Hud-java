package com.navdy.service.library.events.places;

import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.destination.Destination;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class PlaceTypeSearchResponse extends Message {
    public static final List<Destination> DEFAULT_DESTINATIONS = Collections.emptyList();
    public static final String DEFAULT_REQUEST_ID = "";
    public static final RequestStatus DEFAULT_REQUEST_STATUS = RequestStatus.REQUEST_SUCCESS;
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REPEATED, messageType = Destination.class, tag = 3)
    public final List<Destination> destinations;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String request_id;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final RequestStatus request_status;

    public static final class Builder extends com.squareup.wire.Message.Builder<PlaceTypeSearchResponse> {
        public List<Destination> destinations;
        public String request_id;
        public RequestStatus request_status;

        public Builder(PlaceTypeSearchResponse message) {
            super(message);
            if (message != null) {
                this.request_id = message.request_id;
                this.request_status = message.request_status;
                this.destinations = Message.copyOf(message.destinations);
            }
        }

        public Builder request_id(String request_id) {
            this.request_id = request_id;
            return this;
        }

        public Builder request_status(RequestStatus request_status) {
            this.request_status = request_status;
            return this;
        }

        public Builder destinations(List<Destination> destinations) {
            this.destinations = com.squareup.wire.Message.Builder.checkForNulls(destinations);
            return this;
        }

        public PlaceTypeSearchResponse build() {
            return new PlaceTypeSearchResponse();
        }
    }

    public PlaceTypeSearchResponse(String request_id, RequestStatus request_status, List<Destination> destinations) {
        this.request_id = request_id;
        this.request_status = request_status;
        this.destinations = Message.immutableCopyOf(destinations);
    }

    private PlaceTypeSearchResponse(Builder builder) {
        this(builder.request_id, builder.request_status, builder.destinations);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof PlaceTypeSearchResponse)) {
            return false;
        }
        PlaceTypeSearchResponse o = (PlaceTypeSearchResponse) other;
        if (equals( this.request_id,  o.request_id) && equals( this.request_status,  o.request_status) && equals(this.destinations, o.destinations)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.request_id != null ? this.request_id.hashCode() : 0) * 37;
        if (this.request_status != null) {
            i = this.request_status.hashCode();
        }
        result = ((hashCode + i) * 37) + (this.destinations != null ? this.destinations.hashCode() : 1);
        this.hashCode = result;
        return result;
    }
}
