package com.navdy.service.library.events.input;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class MediaRemoteKeyEvent extends Message {
    public static final KeyEvent DEFAULT_ACTION = KeyEvent.KEY_DOWN;
    public static final MediaRemoteKey DEFAULT_KEY = MediaRemoteKey.MEDIA_REMOTE_KEY_SIRI;
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.ENUM)
    public final KeyEvent action;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final MediaRemoteKey key;

    public static final class Builder extends com.squareup.wire.Message.Builder<MediaRemoteKeyEvent> {
        public KeyEvent action;
        public MediaRemoteKey key;

        public Builder(MediaRemoteKeyEvent message) {
            super(message);
            if (message != null) {
                this.key = message.key;
                this.action = message.action;
            }
        }

        public Builder key(MediaRemoteKey key) {
            this.key = key;
            return this;
        }

        public Builder action(KeyEvent action) {
            this.action = action;
            return this;
        }

        public MediaRemoteKeyEvent build() {
            checkRequiredFields();
            return new MediaRemoteKeyEvent();
        }
    }

    public MediaRemoteKeyEvent(MediaRemoteKey key, KeyEvent action) {
        this.key = key;
        this.action = action;
    }

    private MediaRemoteKeyEvent(Builder builder) {
        this(builder.key, builder.action);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof MediaRemoteKeyEvent)) {
            return false;
        }
        MediaRemoteKeyEvent o = (MediaRemoteKeyEvent) other;
        if (equals( this.key,  o.key) && equals( this.action,  o.action)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        if (this.key != null) {
            result = this.key.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.action != null) {
            i = this.action.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
