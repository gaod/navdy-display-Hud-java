package com.navdy.service.library.events;

import android.support.v4.media.TransportMediator;
import com.glympse.android.lib.StaticConfig;
import com.here.posclient.analytics.TrackerEvent;
import com.navdy.hud.app.bluetooth.obex.ObexHelper;
import com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient;
import com.squareup.wire.ExtendableMessage;
import com.squareup.wire.ExtendableMessage.ExtendableBuilder;
import com.squareup.wire.Extension;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;

public final class NavdyEvent extends ExtendableMessage<NavdyEvent> {
    public static final MessageType DEFAULT_TYPE = MessageType.Coordinate;
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.ENUM)
    public final MessageType type;

    public static final class Builder extends ExtendableBuilder<NavdyEvent> {
        public MessageType type;

        public Builder(NavdyEvent message) {
            super(message);
            if (message != null) {
                this.type = message.type;
            }
        }

        public Builder type(MessageType type) {
            this.type = type;
            return this;
        }

        public <E> Builder setExtension(Extension<NavdyEvent, E> extension, E value) {
            super.setExtension(extension, value);
            return this;
        }

        public NavdyEvent build() {
            checkRequiredFields();
            return new NavdyEvent();
        }
    }

    public enum MessageType implements ProtoEnum {
        Coordinate(1),
        Notification(2),
        CallEvent(3),
        FileListRequest(4),
        FileListResponse(5),
        PreviewFileRequest(6),
        PreviewFileResponse(7),
        NavigationManeuverEvent(8),
        NavigationRouteRequest(9),
        NavigationRouteResponse(10),
        NavigationSessionRequest(11),
        NavigationSessionResponse(12),
        NavigationSessionStatusEvent(13),
        DismissScreen(14),
        ShowScreen(15),
        GestureEvent(16),
        PlacesSearchRequest(17),
        PlacesSearchResponse(18),
        ReadSettingsRequest(19),
        ReadSettingsResponse(20),
        UpdateSettings(21),
        ConnectionStateChange(22),
        SpeechRequest(23),
        TelephonyRequest(24),
        TelephonyResponse(25),
        NotificationEvent(26),
        DeviceInfo(27),
        PhoneEvent(28),
        VoiceAssistRequest(29),
        PhotoRequest(30),
        PhotoResponse(31),
        NotificationListRequest(32),
        NotificationListResponse(33),
        ShowCustomNotification(34),
        DateTimeConfiguration(35),
        FileTransferResponse(36),
        FileTransferRequest(37),
        FileTransferData(38),
        FileTransferStatus(39),
        Ping(40),
        NotificationsStateChange(41),
        NotificationsStateRequest(42),
        MusicTrackInfoRequest(43),
        MusicTrackInfo(44),
        MusicEvent(45),
        DialStatusRequest(46),
        DialStatusResponse(47),
        DialBondRequest(48),
        DialBondResponse(49),
        DriverProfilePreferencesRequest(50),
        DriverProfilePreferencesUpdate(51),
        TransmitLocation(52),
        DisconnectRequest(53),
        VoiceAssistResponse(54),
        GetNavigationSessionState(55),
        RouteManeuverRequest(56),
        RouteManeuverResponse(57),
        NavigationPreferencesRequest(58),
        NavigationPreferencesUpdate(59),
        DialSimulationEvent(60),
        MediaRemoteKeyEvent(61),
        ContactRequest(62),
        ContactResponse(63),
        FavoriteContactsRequest(64),
        FavoriteContactsResponse(65),
        FavoriteDestinationsRequest(66),
        FavoriteDestinationsUpdate(67),
        RecommendedDestinationsRequest(68),
        RecommendedDestinationsUpdate(69),
        ConnectionRequest(70),
        ConnectionStatus(71),
        PhoneStatusRequest(72),
        PhoneStatusResponse(73),
        LaunchAppEvent(74),
        InputPreferencesRequest(75),
        InputPreferencesUpdate(76),
        PhoneBatteryStatus(77),
        SmsMessageRequest(78),
        SmsMessageResponse(79),
        ObdStatusRequest(80),
        ObdStatusResponse(81),
        TripUpdate(82),
        TripUpdateAck(83),
        PhotoUpdatesRequest(84),
        PhotoUpdate(85),
        DisplaySpeakerPreferencesRequest(86),
        DisplaySpeakerPreferencesUpdate(87),
        NavigationRouteStatus(88),
        NavigationRouteCancelRequest(89),
        NotificationPreferencesRequest(90),
        NotificationPreferencesUpdate(91),
        StartDriveRecordingEvent(92),
        StopDriveRecordingEvent(93),
        DriveRecordingsRequest(94),
        DriveRecordingsResponse(95),
        StartDrivePlaybackEvent(96),
        StopDrivePlaybackEvent(97),
        StartDriveRecordingResponse(98),
        StartDrivePlaybackResponse(99),
        NavigationSessionRouteChange(100),
        NetworkStateChange(BluetoothPbapClient.EVENT_SET_PHONE_BOOK_ERROR),
        StopDriveRecordingResponse(102),
        NavigationSessionDeferRequest(BluetoothPbapClient.EVENT_PULL_VCARD_LISTING_ERROR),
        CallStateUpdateRequest(BluetoothPbapClient.EVENT_PULL_VCARD_ENTRY_ERROR),
        AutoCompleteRequest(105),
        AutoCompleteResponse(BluetoothPbapClient.EVENT_PULL_VCARD_LISTING_SIZE_ERROR),
        GlanceEvent(107),
        NowPlayingUpdateRequest(108),
        CancelSpeechRequest(109),
        NetworkLinkReady(110),
        SuggestedDestination(TrackerEvent.PositioningOnlineOutdoor),
        CalendarEvent(112),
        CalendarEventUpdates(113),
        SpeechRequestStatus(114),
        AudioStatus(115),
        LinkPropertiesChanged(116),
        VoiceSearchRequest(117),
        VoiceSearchResponse(118),
        MusicCollectionRequest(119),
        MusicCollectionResponse(StaticConfig.MEMORY_IMAGE_CACHE_SIZE_THRESHOLD),
        MusicCapabilitiesRequest(TrackerEvent.PositioningHybridOutdoor),
        MusicCapabilitiesResponse(122),
        MusicArtworkRequest(123),
        MusicArtworkResponse(124),
        DashboardPreferences(125),
        ClearGlances(TransportMediator.KEYCODE_MEDIA_PLAY),
        PlaceTypeSearchRequest(TransportMediator.KEYCODE_MEDIA_PAUSE),
        PlaceTypeSearchResponse(128),
        AccelerateShutdown(ObexHelper.OBEX_OPCODE_DISCONNECT),
        DestinationSelectedRequest(130),
        DestinationSelectedResponse(131),
        PhotoUpdateQuery(132),
        PhotoUpdateQueryResponse(133),
        ResumeMusicRequest(134),
        CannedMessagesRequest(135),
        CannedMessagesUpdate(136),
        MusicCollectionSourceUpdate(137);
        
        private final int value;

        private MessageType(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public NavdyEvent(MessageType type) {
        this.type = type;
    }

    private NavdyEvent(Builder builder) {
        this(builder.type);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NavdyEvent)) {
            return false;
        }
        NavdyEvent o = (NavdyEvent) other;
        if (extensionsEqual(o)) {
            return equals( this.type,  o.type);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        result = (extensionsHashCode() * 37) + (this.type != null ? this.type.hashCode() : 0);
        this.hashCode = result;
        return result;
    }
}
