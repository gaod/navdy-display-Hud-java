package com.navdy.service.library.events.places;

import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class PlacesSearchResponse extends Message {
    public static final String DEFAULT_REQUESTID = "";
    public static final List<PlacesSearchResult> DEFAULT_RESULTS = Collections.emptyList();
    public static final String DEFAULT_SEARCHQUERY = "";
    public static final RequestStatus DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String requestId;
    @ProtoField(label = Label.REPEATED, messageType = PlacesSearchResult.class, tag = 4)
    public final List<PlacesSearchResult> results;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String searchQuery;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<PlacesSearchResponse> {
        public String requestId;
        public List<PlacesSearchResult> results;
        public String searchQuery;
        public RequestStatus status;
        public String statusDetail;

        public Builder(PlacesSearchResponse message) {
            super(message);
            if (message != null) {
                this.searchQuery = message.searchQuery;
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.results = Message.copyOf(message.results);
                this.requestId = message.requestId;
            }
        }

        public Builder searchQuery(String searchQuery) {
            this.searchQuery = searchQuery;
            return this;
        }

        public Builder status(RequestStatus status) {
            this.status = status;
            return this;
        }

        public Builder statusDetail(String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }

        public Builder results(List<PlacesSearchResult> results) {
            this.results = com.squareup.wire.Message.Builder.checkForNulls(results);
            return this;
        }

        public Builder requestId(String requestId) {
            this.requestId = requestId;
            return this;
        }

        public PlacesSearchResponse build() {
            checkRequiredFields();
            return new PlacesSearchResponse();
        }
    }

    public PlacesSearchResponse(String searchQuery, RequestStatus status, String statusDetail, List<PlacesSearchResult> results, String requestId) {
        this.searchQuery = searchQuery;
        this.status = status;
        this.statusDetail = statusDetail;
        this.results = Message.immutableCopyOf(results);
        this.requestId = requestId;
    }

    private PlacesSearchResponse(Builder builder) {
        this(builder.searchQuery, builder.status, builder.statusDetail, builder.results, builder.requestId);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof PlacesSearchResponse)) {
            return false;
        }
        PlacesSearchResponse o = (PlacesSearchResponse) other;
        if (equals( this.searchQuery,  o.searchQuery) && equals( this.status,  o.status) && equals( this.statusDetail,  o.statusDetail) && equals(this.results, o.results) && equals( this.requestId,  o.requestId)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.searchQuery != null ? this.searchQuery.hashCode() : 0) * 37;
        if (this.status != null) {
            hashCode = this.status.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.statusDetail != null) {
            hashCode = this.statusDetail.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (((hashCode2 + hashCode) * 37) + (this.results != null ? this.results.hashCode() : 1)) * 37;
        if (this.requestId != null) {
            i = this.requestId.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
