package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class VoiceSearchRequest extends Message {
    public static final Boolean DEFAULT_END = Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.BOOL)
    public final Boolean end;

    public static final class Builder extends com.squareup.wire.Message.Builder<VoiceSearchRequest> {
        public Boolean end;

        public Builder(VoiceSearchRequest message) {
            super(message);
            if (message != null) {
                this.end = message.end;
            }
        }

        public Builder end(Boolean end) {
            this.end = end;
            return this;
        }

        public VoiceSearchRequest build() {
            return new VoiceSearchRequest();
        }
    }

    public VoiceSearchRequest(Boolean end) {
        this.end = end;
    }

    private VoiceSearchRequest(Builder builder) {
        this(builder.end);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof VoiceSearchRequest) {
            return equals( this.end,  ((VoiceSearchRequest) other).end);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.end != null ? this.end.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
