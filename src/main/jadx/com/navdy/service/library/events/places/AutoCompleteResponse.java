package com.navdy.service.library.events.places;

import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class AutoCompleteResponse extends Message {
    public static final String DEFAULT_PARTIALSEARCH = "";
    public static final List<String> DEFAULT_RESULTS = Collections.emptyList();
    public static final RequestStatus DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String partialSearch;
    @ProtoField(label = Label.REPEATED, tag = 4, type = Datatype.STRING)
    public final List<String> results;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<AutoCompleteResponse> {
        public String partialSearch;
        public List<String> results;
        public RequestStatus status;
        public String statusDetail;

        public Builder(AutoCompleteResponse message) {
            super(message);
            if (message != null) {
                this.partialSearch = message.partialSearch;
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.results = Message.copyOf(message.results);
            }
        }

        public Builder partialSearch(String partialSearch) {
            this.partialSearch = partialSearch;
            return this;
        }

        public Builder status(RequestStatus status) {
            this.status = status;
            return this;
        }

        public Builder statusDetail(String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }

        public Builder results(List<String> results) {
            this.results = com.squareup.wire.Message.Builder.checkForNulls(results);
            return this;
        }

        public AutoCompleteResponse build() {
            checkRequiredFields();
            return new AutoCompleteResponse();
        }
    }

    public AutoCompleteResponse(String partialSearch, RequestStatus status, String statusDetail, List<String> results) {
        this.partialSearch = partialSearch;
        this.status = status;
        this.statusDetail = statusDetail;
        this.results = Message.immutableCopyOf(results);
    }

    private AutoCompleteResponse(Builder builder) {
        this(builder.partialSearch, builder.status, builder.statusDetail, builder.results);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof AutoCompleteResponse)) {
            return false;
        }
        AutoCompleteResponse o = (AutoCompleteResponse) other;
        if (equals( this.partialSearch,  o.partialSearch) && equals( this.status,  o.status) && equals( this.statusDetail,  o.statusDetail) && equals(this.results, o.results)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.partialSearch != null ? this.partialSearch.hashCode() : 0) * 37;
        if (this.status != null) {
            hashCode = this.status.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.statusDetail != null) {
            i = this.statusDetail.hashCode();
        }
        result = ((hashCode + i) * 37) + (this.results != null ? this.results.hashCode() : 1);
        this.hashCode = result;
        return result;
    }
}
