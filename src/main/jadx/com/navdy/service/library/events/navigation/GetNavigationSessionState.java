package com.navdy.service.library.events.navigation;

import com.squareup.wire.Message;

public final class GetNavigationSessionState extends Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<GetNavigationSessionState> {
        public Builder(GetNavigationSessionState message) {
            super(message);
        }

        public GetNavigationSessionState build() {
            return new GetNavigationSessionState();
        }
    }

    private GetNavigationSessionState(Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        return other instanceof GetNavigationSessionState;
    }

    public int hashCode() {
        return 0;
    }
}
