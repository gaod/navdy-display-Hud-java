package com.navdy.service.library.events.callcontrol;

import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class TelephonyResponse extends Message {
    public static final CallAction DEFAULT_ACTION = CallAction.CALL_ACCEPT;
    public static final RequestStatus DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final CallAction action;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<TelephonyResponse> {
        public CallAction action;
        public RequestStatus status;
        public String statusDetail;

        public Builder(TelephonyResponse message) {
            super(message);
            if (message != null) {
                this.action = message.action;
                this.status = message.status;
                this.statusDetail = message.statusDetail;
            }
        }

        public Builder action(CallAction action) {
            this.action = action;
            return this;
        }

        public Builder status(RequestStatus status) {
            this.status = status;
            return this;
        }

        public Builder statusDetail(String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }

        public TelephonyResponse build() {
            checkRequiredFields();
            return new TelephonyResponse();
        }
    }

    public TelephonyResponse(CallAction action, RequestStatus status, String statusDetail) {
        this.action = action;
        this.status = status;
        this.statusDetail = statusDetail;
    }

    private TelephonyResponse(Builder builder) {
        this(builder.action, builder.status, builder.statusDetail);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof TelephonyResponse)) {
            return false;
        }
        TelephonyResponse o = (TelephonyResponse) other;
        if (equals( this.action,  o.action) && equals( this.status,  o.status) && equals( this.statusDetail,  o.statusDetail)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.action != null ? this.action.hashCode() : 0) * 37;
        if (this.status != null) {
            hashCode = this.status.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.statusDetail != null) {
            i = this.statusDetail.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
