package com.navdy.service.library.events.preferences;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class DisplaySpeakerPreferences extends Message {
    public static final Boolean DEFAULT_FEEDBACK_SOUND = Boolean.valueOf(true);
    public static final Boolean DEFAULT_MASTER_SOUND = Boolean.valueOf(true);
    public static final Boolean DEFAULT_NOTIFICATION_SOUND = Boolean.valueOf(true);
    public static final Long DEFAULT_SERIAL_NUMBER = Long.valueOf(0);
    public static final Integer DEFAULT_VOLUME_LEVEL = Integer.valueOf(50);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean feedback_sound;
    @ProtoField(tag = 2, type = Datatype.BOOL)
    public final Boolean master_sound;
    @ProtoField(tag = 5, type = Datatype.BOOL)
    public final Boolean notification_sound;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT64)
    public final Long serial_number;
    @ProtoField(tag = 3, type = Datatype.INT32)
    public final Integer volume_level;

    public static final class Builder extends com.squareup.wire.Message.Builder<DisplaySpeakerPreferences> {
        public Boolean feedback_sound;
        public Boolean master_sound;
        public Boolean notification_sound;
        public Long serial_number;
        public Integer volume_level;

        public Builder(DisplaySpeakerPreferences message) {
            super(message);
            if (message != null) {
                this.serial_number = message.serial_number;
                this.master_sound = message.master_sound;
                this.volume_level = message.volume_level;
                this.feedback_sound = message.feedback_sound;
                this.notification_sound = message.notification_sound;
            }
        }

        public Builder serial_number(Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }

        public Builder master_sound(Boolean master_sound) {
            this.master_sound = master_sound;
            return this;
        }

        public Builder volume_level(Integer volume_level) {
            this.volume_level = volume_level;
            return this;
        }

        public Builder feedback_sound(Boolean feedback_sound) {
            this.feedback_sound = feedback_sound;
            return this;
        }

        public Builder notification_sound(Boolean notification_sound) {
            this.notification_sound = notification_sound;
            return this;
        }

        public DisplaySpeakerPreferences build() {
            checkRequiredFields();
            return new DisplaySpeakerPreferences();
        }
    }

    public DisplaySpeakerPreferences(Long serial_number, Boolean master_sound, Integer volume_level, Boolean feedback_sound, Boolean notification_sound) {
        this.serial_number = serial_number;
        this.master_sound = master_sound;
        this.volume_level = volume_level;
        this.feedback_sound = feedback_sound;
        this.notification_sound = notification_sound;
    }

    private DisplaySpeakerPreferences(Builder builder) {
        this(builder.serial_number, builder.master_sound, builder.volume_level, builder.feedback_sound, builder.notification_sound);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof DisplaySpeakerPreferences)) {
            return false;
        }
        DisplaySpeakerPreferences o = (DisplaySpeakerPreferences) other;
        if (equals( this.serial_number,  o.serial_number) && equals( this.master_sound,  o.master_sound) && equals( this.volume_level,  o.volume_level) && equals( this.feedback_sound,  o.feedback_sound) && equals( this.notification_sound,  o.notification_sound)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.serial_number != null ? this.serial_number.hashCode() : 0) * 37;
        if (this.master_sound != null) {
            hashCode = this.master_sound.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.volume_level != null) {
            hashCode = this.volume_level.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.feedback_sound != null) {
            hashCode = this.feedback_sound.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.notification_sound != null) {
            i = this.notification_sound.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
