package com.navdy.service.library.events.settings;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class Setting extends Message {
    public static final String DEFAULT_KEY = "";
    public static final String DEFAULT_VALUE = "";
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String key;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.STRING)
    public final String value;

    public static final class Builder extends com.squareup.wire.Message.Builder<Setting> {
        public String key;
        public String value;

        public Builder(Setting message) {
            super(message);
            if (message != null) {
                this.key = message.key;
                this.value = message.value;
            }
        }

        public Builder key(String key) {
            this.key = key;
            return this;
        }

        public Builder value(String value) {
            this.value = value;
            return this;
        }

        public Setting build() {
            checkRequiredFields();
            return new Setting();
        }
    }

    public Setting(String key, String value) {
        this.key = key;
        this.value = value;
    }

    private Setting(Builder builder) {
        this(builder.key, builder.value);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof Setting)) {
            return false;
        }
        Setting o = (Setting) other;
        if (equals( this.key,  o.key) && equals( this.value,  o.value)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        if (this.key != null) {
            result = this.key.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.value != null) {
            i = this.value.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
