package com.navdy.service.library.events.notification;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class ShowCustomNotification extends Message {
    public static final String DEFAULT_ID = "";
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String id;

    public static final class Builder extends com.squareup.wire.Message.Builder<ShowCustomNotification> {
        public String id;

        public Builder(ShowCustomNotification message) {
            super(message);
            if (message != null) {
                this.id = message.id;
            }
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public ShowCustomNotification build() {
            checkRequiredFields();
            return new ShowCustomNotification();
        }
    }

    public ShowCustomNotification(String id) {
        this.id = id;
    }

    private ShowCustomNotification(Builder builder) {
        this(builder.id);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof ShowCustomNotification) {
            return equals( this.id,  ((ShowCustomNotification) other).id);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.id != null ? this.id.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
