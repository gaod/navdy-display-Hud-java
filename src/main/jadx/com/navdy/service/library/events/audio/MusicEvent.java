package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;

public final class MusicEvent extends Message {
    public static final Action DEFAULT_ACTION = Action.MUSIC_ACTION_PLAY;
    public static final String DEFAULT_COLLECTIONID = "";
    public static final MusicCollectionSource DEFAULT_COLLECTIONSOURCE = MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
    public static final MusicCollectionType DEFAULT_COLLECTIONTYPE = MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
    public static final MusicDataSource DEFAULT_DATASOURCE = MusicDataSource.MUSIC_SOURCE_NONE;
    public static final Integer DEFAULT_INDEX = Integer.valueOf(0);
    public static final MusicRepeatMode DEFAULT_REPEATMODE = MusicRepeatMode.MUSIC_REPEAT_MODE_UNKNOWN;
    public static final MusicShuffleMode DEFAULT_SHUFFLEMODE = MusicShuffleMode.MUSIC_SHUFFLE_MODE_UNKNOWN;
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final Action action;
    @ProtoField(tag = 13, type = Datatype.STRING)
    public final String collectionId;
    @ProtoField(tag = 14, type = Datatype.ENUM)
    public final MusicCollectionSource collectionSource;
    @ProtoField(tag = 15, type = Datatype.ENUM)
    public final MusicCollectionType collectionType;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final MusicDataSource dataSource;
    @ProtoField(tag = 16, type = Datatype.INT32)
    public final Integer index;
    @ProtoField(tag = 18, type = Datatype.ENUM)
    public final MusicRepeatMode repeatMode;
    @ProtoField(tag = 17, type = Datatype.ENUM)
    public final MusicShuffleMode shuffleMode;

    public enum Action implements ProtoEnum {
        MUSIC_ACTION_PLAY(1),
        MUSIC_ACTION_PAUSE(2),
        MUSIC_ACTION_NEXT(3),
        MUSIC_ACTION_PREVIOUS(4),
        MUSIC_ACTION_REWIND_START(5),
        MUSIC_ACTION_FAST_FORWARD_START(6),
        MUSIC_ACTION_REWIND_STOP(7),
        MUSIC_ACTION_FAST_FORWARD_STOP(8),
        MUSIC_ACTION_MODE_CHANGE(9);
        
        private final int value;

        private Action(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public static final class Builder extends com.squareup.wire.Message.Builder<MusicEvent> {
        public Action action;
        public String collectionId;
        public MusicCollectionSource collectionSource;
        public MusicCollectionType collectionType;
        public MusicDataSource dataSource;
        public Integer index;
        public MusicRepeatMode repeatMode;
        public MusicShuffleMode shuffleMode;

        public Builder(MusicEvent message) {
            super(message);
            if (message != null) {
                this.action = message.action;
                this.dataSource = message.dataSource;
                this.collectionSource = message.collectionSource;
                this.collectionType = message.collectionType;
                this.collectionId = message.collectionId;
                this.index = message.index;
                this.shuffleMode = message.shuffleMode;
                this.repeatMode = message.repeatMode;
            }
        }

        public Builder action(Action action) {
            this.action = action;
            return this;
        }

        public Builder dataSource(MusicDataSource dataSource) {
            this.dataSource = dataSource;
            return this;
        }

        public Builder collectionSource(MusicCollectionSource collectionSource) {
            this.collectionSource = collectionSource;
            return this;
        }

        public Builder collectionType(MusicCollectionType collectionType) {
            this.collectionType = collectionType;
            return this;
        }

        public Builder collectionId(String collectionId) {
            this.collectionId = collectionId;
            return this;
        }

        public Builder index(Integer index) {
            this.index = index;
            return this;
        }

        public Builder shuffleMode(MusicShuffleMode shuffleMode) {
            this.shuffleMode = shuffleMode;
            return this;
        }

        public Builder repeatMode(MusicRepeatMode repeatMode) {
            this.repeatMode = repeatMode;
            return this;
        }

        public MusicEvent build() {
            checkRequiredFields();
            return new MusicEvent();
        }
    }

    public MusicEvent(Action action, MusicDataSource dataSource, MusicCollectionSource collectionSource, MusicCollectionType collectionType, String collectionId, Integer index, MusicShuffleMode shuffleMode, MusicRepeatMode repeatMode) {
        this.action = action;
        this.dataSource = dataSource;
        this.collectionSource = collectionSource;
        this.collectionType = collectionType;
        this.collectionId = collectionId;
        this.index = index;
        this.shuffleMode = shuffleMode;
        this.repeatMode = repeatMode;
    }

    private MusicEvent(Builder builder) {
        this(builder.action, builder.dataSource, builder.collectionSource, builder.collectionType, builder.collectionId, builder.index, builder.shuffleMode, builder.repeatMode);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof MusicEvent)) {
            return false;
        }
        MusicEvent o = (MusicEvent) other;
        if (equals( this.action,  o.action) && equals( this.dataSource,  o.dataSource) && equals( this.collectionSource,  o.collectionSource) && equals( this.collectionType,  o.collectionType) && equals( this.collectionId,  o.collectionId) && equals( this.index,  o.index) && equals( this.shuffleMode,  o.shuffleMode) && equals( this.repeatMode,  o.repeatMode)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.action != null ? this.action.hashCode() : 0) * 37;
        if (this.dataSource != null) {
            hashCode = this.dataSource.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.collectionSource != null) {
            hashCode = this.collectionSource.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.collectionType != null) {
            hashCode = this.collectionType.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.collectionId != null) {
            hashCode = this.collectionId.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.index != null) {
            hashCode = this.index.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.shuffleMode != null) {
            hashCode = this.shuffleMode.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.repeatMode != null) {
            i = this.repeatMode.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
