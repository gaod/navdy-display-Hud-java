package com.navdy.service.library.events.input;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class LaunchAppEvent extends Message {
    public static final String DEFAULT_APPBUNDLEID = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String appBundleID;

    public static final class Builder extends com.squareup.wire.Message.Builder<LaunchAppEvent> {
        public String appBundleID;

        public Builder(LaunchAppEvent message) {
            super(message);
            if (message != null) {
                this.appBundleID = message.appBundleID;
            }
        }

        public Builder appBundleID(String appBundleID) {
            this.appBundleID = appBundleID;
            return this;
        }

        public LaunchAppEvent build() {
            return new LaunchAppEvent();
        }
    }

    public LaunchAppEvent(String appBundleID) {
        this.appBundleID = appBundleID;
    }

    private LaunchAppEvent(Builder builder) {
        this(builder.appBundleID);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof LaunchAppEvent) {
            return equals( this.appBundleID,  ((LaunchAppEvent) other).appBundleID);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.appBundleID != null ? this.appBundleID.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
