package com.navdy.service.library.events.callcontrol;

import com.squareup.wire.ProtoEnum;

public enum CallAction implements ProtoEnum {
    CALL_ACCEPT(1),
    CALL_END(2),
    CALL_DIAL(3),
    CALL_MUTE(4),
    CALL_UNMUTE(5),
    CALL_REJECT(6);
    
    private final int value;

    private CallAction(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
