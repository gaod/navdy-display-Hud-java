package com.navdy.service.library.events.places;

import com.navdy.service.library.events.location.Coordinate;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class PlacesSearchResult extends Message {
    public static final String DEFAULT_ADDRESS = "";
    public static final String DEFAULT_CATEGORY = "";
    public static final Double DEFAULT_DISTANCE = Double.valueOf(0.0d);
    public static final String DEFAULT_LABEL = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String address;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String category;
    @ProtoField(tag = 3)
    public final Coordinate destinationLocation;
    @ProtoField(tag = 6, type = Datatype.DOUBLE)
    public final Double distance;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String label;
    @ProtoField(tag = 2)
    public final Coordinate navigationPosition;

    public static final class Builder extends com.squareup.wire.Message.Builder<PlacesSearchResult> {
        public String address;
        public String category;
        public Coordinate destinationLocation;
        public Double distance;
        public String label;
        public Coordinate navigationPosition;

        public Builder(PlacesSearchResult message) {
            super(message);
            if (message != null) {
                this.label = message.label;
                this.navigationPosition = message.navigationPosition;
                this.destinationLocation = message.destinationLocation;
                this.address = message.address;
                this.category = message.category;
                this.distance = message.distance;
            }
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public Builder navigationPosition(Coordinate navigationPosition) {
            this.navigationPosition = navigationPosition;
            return this;
        }

        public Builder destinationLocation(Coordinate destinationLocation) {
            this.destinationLocation = destinationLocation;
            return this;
        }

        public Builder address(String address) {
            this.address = address;
            return this;
        }

        public Builder category(String category) {
            this.category = category;
            return this;
        }

        public Builder distance(Double distance) {
            this.distance = distance;
            return this;
        }

        public PlacesSearchResult build() {
            return new PlacesSearchResult();
        }
    }

    public PlacesSearchResult(String label, Coordinate navigationPosition, Coordinate destinationLocation, String address, String category, Double distance) {
        this.label = label;
        this.navigationPosition = navigationPosition;
        this.destinationLocation = destinationLocation;
        this.address = address;
        this.category = category;
        this.distance = distance;
    }

    private PlacesSearchResult(Builder builder) {
        this(builder.label, builder.navigationPosition, builder.destinationLocation, builder.address, builder.category, builder.distance);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof PlacesSearchResult)) {
            return false;
        }
        PlacesSearchResult o = (PlacesSearchResult) other;
        if (equals( this.label,  o.label) && equals( this.navigationPosition,  o.navigationPosition) && equals( this.destinationLocation,  o.destinationLocation) && equals( this.address,  o.address) && equals( this.category,  o.category) && equals( this.distance,  o.distance)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.label != null ? this.label.hashCode() : 0) * 37;
        if (this.navigationPosition != null) {
            hashCode = this.navigationPosition.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.destinationLocation != null) {
            hashCode = this.destinationLocation.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.address != null) {
            hashCode = this.address.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.category != null) {
            hashCode = this.category.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.distance != null) {
            i = this.distance.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
