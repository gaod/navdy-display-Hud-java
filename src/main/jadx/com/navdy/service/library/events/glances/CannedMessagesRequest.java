package com.navdy.service.library.events.glances;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class CannedMessagesRequest extends Message {
    public static final Long DEFAULT_SERIAL_NUMBER = Long.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.INT64)
    public final Long serial_number;

    public static final class Builder extends com.squareup.wire.Message.Builder<CannedMessagesRequest> {
        public Long serial_number;

        public Builder(CannedMessagesRequest message) {
            super(message);
            if (message != null) {
                this.serial_number = message.serial_number;
            }
        }

        public Builder serial_number(Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }

        public CannedMessagesRequest build() {
            return new CannedMessagesRequest();
        }
    }

    public CannedMessagesRequest(Long serial_number) {
        this.serial_number = serial_number;
    }

    private CannedMessagesRequest(Builder builder) {
        this(builder.serial_number);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof CannedMessagesRequest) {
            return equals( this.serial_number,  ((CannedMessagesRequest) other).serial_number);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.serial_number != null ? this.serial_number.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
