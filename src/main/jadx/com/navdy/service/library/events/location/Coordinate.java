package com.navdy.service.library.events.location;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class Coordinate extends Message {
    public static final Float DEFAULT_ACCURACY = Float.valueOf(0.0f);
    public static final Double DEFAULT_ALTITUDE = Double.valueOf(0.0d);
    public static final Float DEFAULT_BEARING = Float.valueOf(0.0f);
    public static final Double DEFAULT_LATITUDE = Double.valueOf(0.0d);
    public static final Double DEFAULT_LONGITUDE = Double.valueOf(0.0d);
    public static final String DEFAULT_PROVIDER = "";
    public static final Float DEFAULT_SPEED = Float.valueOf(0.0f);
    public static final Long DEFAULT_TIMESTAMP = Long.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 3, type = Datatype.FLOAT)
    public final Float accuracy;
    @ProtoField(tag = 4, type = Datatype.DOUBLE)
    public final Double altitude;
    @ProtoField(tag = 5, type = Datatype.FLOAT)
    public final Float bearing;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.DOUBLE)
    public final Double latitude;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.DOUBLE)
    public final Double longitude;
    @ProtoField(tag = 8, type = Datatype.STRING)
    public final String provider;
    @ProtoField(tag = 6, type = Datatype.FLOAT)
    public final Float speed;
    @ProtoField(tag = 7, type = Datatype.INT64)
    public final Long timestamp;

    public static final class Builder extends com.squareup.wire.Message.Builder<Coordinate> {
        public Float accuracy;
        public Double altitude;
        public Float bearing;
        public Double latitude;
        public Double longitude;
        public String provider;
        public Float speed;
        public Long timestamp;

        public Builder(Coordinate message) {
            super(message);
            if (message != null) {
                this.latitude = message.latitude;
                this.longitude = message.longitude;
                this.accuracy = message.accuracy;
                this.altitude = message.altitude;
                this.bearing = message.bearing;
                this.speed = message.speed;
                this.timestamp = message.timestamp;
                this.provider = message.provider;
            }
        }

        public Builder latitude(Double latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder longitude(Double longitude) {
            this.longitude = longitude;
            return this;
        }

        public Builder accuracy(Float accuracy) {
            this.accuracy = accuracy;
            return this;
        }

        public Builder altitude(Double altitude) {
            this.altitude = altitude;
            return this;
        }

        public Builder bearing(Float bearing) {
            this.bearing = bearing;
            return this;
        }

        public Builder speed(Float speed) {
            this.speed = speed;
            return this;
        }

        public Builder timestamp(Long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Builder provider(String provider) {
            this.provider = provider;
            return this;
        }

        public Coordinate build() {
            checkRequiredFields();
            return new Coordinate();
        }
    }

    public Coordinate(Double latitude, Double longitude, Float accuracy, Double altitude, Float bearing, Float speed, Long timestamp, String provider) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.accuracy = accuracy;
        this.altitude = altitude;
        this.bearing = bearing;
        this.speed = speed;
        this.timestamp = timestamp;
        this.provider = provider;
    }

    private Coordinate(Builder builder) {
        this(builder.latitude, builder.longitude, builder.accuracy, builder.altitude, builder.bearing, builder.speed, builder.timestamp, builder.provider);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof Coordinate)) {
            return false;
        }
        Coordinate o = (Coordinate) other;
        if (equals( this.latitude,  o.latitude) && equals( this.longitude,  o.longitude) && equals( this.accuracy,  o.accuracy) && equals( this.altitude,  o.altitude) && equals( this.bearing,  o.bearing) && equals( this.speed,  o.speed) && equals( this.timestamp,  o.timestamp) && equals( this.provider,  o.provider)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.latitude != null ? this.latitude.hashCode() : 0) * 37;
        if (this.longitude != null) {
            hashCode = this.longitude.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.accuracy != null) {
            hashCode = this.accuracy.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.altitude != null) {
            hashCode = this.altitude.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.bearing != null) {
            hashCode = this.bearing.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.speed != null) {
            hashCode = this.speed.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.timestamp != null) {
            hashCode = this.timestamp.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.provider != null) {
            i = this.provider.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
