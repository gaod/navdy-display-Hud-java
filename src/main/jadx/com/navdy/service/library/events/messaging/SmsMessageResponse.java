package com.navdy.service.library.events.messaging;

import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class SmsMessageResponse extends Message {
    public static final String DEFAULT_ID = "";
    public static final String DEFAULT_NAME = "";
    public static final String DEFAULT_NUMBER = "";
    public static final RequestStatus DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String id;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String name;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String number;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;

    public static final class Builder extends com.squareup.wire.Message.Builder<SmsMessageResponse> {
        public String id;
        public String name;
        public String number;
        public RequestStatus status;

        public Builder(SmsMessageResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.number = message.number;
                this.name = message.name;
                this.id = message.id;
            }
        }

        public Builder status(RequestStatus status) {
            this.status = status;
            return this;
        }

        public Builder number(String number) {
            this.number = number;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public SmsMessageResponse build() {
            checkRequiredFields();
            return new SmsMessageResponse();
        }
    }

    public SmsMessageResponse(RequestStatus status, String number, String name, String id) {
        this.status = status;
        this.number = number;
        this.name = name;
        this.id = id;
    }

    private SmsMessageResponse(Builder builder) {
        this(builder.status, builder.number, builder.name, builder.id);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof SmsMessageResponse)) {
            return false;
        }
        SmsMessageResponse o = (SmsMessageResponse) other;
        if (equals( this.status,  o.status) && equals( this.number,  o.number) && equals( this.name,  o.name) && equals( this.id,  o.id)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.number != null) {
            hashCode = this.number.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.name != null) {
            hashCode = this.name.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.id != null) {
            i = this.id.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
