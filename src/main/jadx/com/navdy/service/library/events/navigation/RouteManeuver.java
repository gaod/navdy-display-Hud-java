package com.navdy.service.library.events.navigation;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class RouteManeuver extends Message {
    public static final String DEFAULT_CURRENTROAD = "";
    public static final Float DEFAULT_DISTANCETOPENDINGROAD = Float.valueOf(0.0f);
    public static final DistanceUnit DEFAULT_DISTANCETOPENDINGROADUNIT = DistanceUnit.DISTANCE_MILES;
    public static final Integer DEFAULT_MANEUVERTIME = Integer.valueOf(0);
    public static final String DEFAULT_PENDINGROAD = "";
    public static final NavigationTurn DEFAULT_PENDINGTURN = NavigationTurn.NAV_TURN_START;
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String currentRoad;
    @ProtoField(tag = 4, type = Datatype.FLOAT)
    public final Float distanceToPendingRoad;
    @ProtoField(tag = 5, type = Datatype.ENUM)
    public final DistanceUnit distanceToPendingRoadUnit;
    @ProtoField(tag = 6, type = Datatype.UINT32)
    public final Integer maneuverTime;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String pendingRoad;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final NavigationTurn pendingTurn;

    public static final class Builder extends com.squareup.wire.Message.Builder<RouteManeuver> {
        public String currentRoad;
        public Float distanceToPendingRoad;
        public DistanceUnit distanceToPendingRoadUnit;
        public Integer maneuverTime;
        public String pendingRoad;
        public NavigationTurn pendingTurn;

        public Builder(RouteManeuver message) {
            super(message);
            if (message != null) {
                this.currentRoad = message.currentRoad;
                this.pendingTurn = message.pendingTurn;
                this.pendingRoad = message.pendingRoad;
                this.distanceToPendingRoad = message.distanceToPendingRoad;
                this.distanceToPendingRoadUnit = message.distanceToPendingRoadUnit;
                this.maneuverTime = message.maneuverTime;
            }
        }

        public Builder currentRoad(String currentRoad) {
            this.currentRoad = currentRoad;
            return this;
        }

        public Builder pendingTurn(NavigationTurn pendingTurn) {
            this.pendingTurn = pendingTurn;
            return this;
        }

        public Builder pendingRoad(String pendingRoad) {
            this.pendingRoad = pendingRoad;
            return this;
        }

        public Builder distanceToPendingRoad(Float distanceToPendingRoad) {
            this.distanceToPendingRoad = distanceToPendingRoad;
            return this;
        }

        public Builder distanceToPendingRoadUnit(DistanceUnit distanceToPendingRoadUnit) {
            this.distanceToPendingRoadUnit = distanceToPendingRoadUnit;
            return this;
        }

        public Builder maneuverTime(Integer maneuverTime) {
            this.maneuverTime = maneuverTime;
            return this;
        }

        public RouteManeuver build() {
            return new RouteManeuver();
        }
    }

    public RouteManeuver(String currentRoad, NavigationTurn pendingTurn, String pendingRoad, Float distanceToPendingRoad, DistanceUnit distanceToPendingRoadUnit, Integer maneuverTime) {
        this.currentRoad = currentRoad;
        this.pendingTurn = pendingTurn;
        this.pendingRoad = pendingRoad;
        this.distanceToPendingRoad = distanceToPendingRoad;
        this.distanceToPendingRoadUnit = distanceToPendingRoadUnit;
        this.maneuverTime = maneuverTime;
    }

    private RouteManeuver(Builder builder) {
        this(builder.currentRoad, builder.pendingTurn, builder.pendingRoad, builder.distanceToPendingRoad, builder.distanceToPendingRoadUnit, builder.maneuverTime);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof RouteManeuver)) {
            return false;
        }
        RouteManeuver o = (RouteManeuver) other;
        if (equals( this.currentRoad,  o.currentRoad) && equals( this.pendingTurn,  o.pendingTurn) && equals( this.pendingRoad,  o.pendingRoad) && equals( this.distanceToPendingRoad,  o.distanceToPendingRoad) && equals( this.distanceToPendingRoadUnit,  o.distanceToPendingRoadUnit) && equals( this.maneuverTime,  o.maneuverTime)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.currentRoad != null ? this.currentRoad.hashCode() : 0) * 37;
        if (this.pendingTurn != null) {
            hashCode = this.pendingTurn.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.pendingRoad != null) {
            hashCode = this.pendingRoad.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.distanceToPendingRoad != null) {
            hashCode = this.distanceToPendingRoad.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.distanceToPendingRoadUnit != null) {
            hashCode = this.distanceToPendingRoadUnit.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.maneuverTime != null) {
            i = this.maneuverTime.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
