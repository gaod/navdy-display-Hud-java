package com.navdy.service.library.file;

import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.File;
import java.io.RandomAccessFile;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public abstract class TransferDataSource {
    public static final String TEST_DATA_NAME = "<TESTDATA>";
    private static Logger sLogger = new Logger(TransferDataSource.class);
    protected long mCurOffset;

    private static class FileTransferDataSource extends TransferDataSource {
        File mFile;

        FileTransferDataSource(File file) {
            this.mFile = file;
        }

        public long length() {
            return this.mFile.length();
        }

        public int read(byte[] b) throws Exception {
            RandomAccessFile f = null;
            int res = -1;
            try {
                RandomAccessFile f2 = new RandomAccessFile(this.mFile, "r");
                try {
                    f2.seek(this.mCurOffset);
                    res = f2.read(b);
                    IOUtils.closeObject(f2);
                    f = f2;
                    return res;
                } catch (Throwable th) {
                    f = f2;
                    IOUtils.closeObject(f);
                    return res;
                }
            } catch (Throwable th2) {
                IOUtils.closeObject(f);
                return res;
            }
        }

        public String checkSum() {
            return IOUtils.hashForFile(this.mFile);
        }

        public String checkSum(long length) {
            return IOUtils.hashForFile(this.mFile, length);
        }

        public String getName() {
            return this.mFile.getName();
        }

        public File getFile() {
            return this.mFile;
        }
    }

    private static class TestDataSource extends TransferDataSource {
        static final int DATA_BUFFER_LEN = 4096;
        byte[] mDataBuffer = new byte[4096];
        long mLength;

        TestDataSource(long len) {
            this.mLength = len;
            new Random().nextBytes(this.mDataBuffer);
        }

        public long length() {
            return this.mLength;
        }

        public int read(byte[] b) {
            int di = (int) (this.mLength % 4096);
            long remaining = this.mLength - this.mCurOffset;
            int len = remaining >= ((long) b.length) ? b.length : (int) remaining;
            for (int i = 0; i < len; i++) {
                b[i] = this.mDataBuffer[di];
                di = (di + 1) % 4096;
            }
            return len;
        }

        public String checkSum() {
            return checkSum(this.mLength);
        }

        public String checkSum(long length) {
            long offset = 0;
            if (length > this.mLength) {
                length = this.mLength;
            }
            try {
                MessageDigest msgDigest = MessageDigest.getInstance("MD5");
                while (offset < length) {
                    long remaining = length - offset;
                    int l = remaining < 4096 ? (int) remaining : 4096;
                    msgDigest.update(this.mDataBuffer, 0, l);
                    offset += (long) l;
                }
                return IOUtils.bytesToHexString(msgDigest.digest());
            } catch (NoSuchAlgorithmException e) {
                TransferDataSource.sLogger.e("unable to get MD5 algorithm");
                return null;
            }
        }

        public String getName() {
            return TransferDataSource.TEST_DATA_NAME;
        }

        public File getFile() {
            return null;
        }
    }

    public abstract String checkSum();

    public abstract String checkSum(long j);

    public abstract File getFile();

    public abstract String getName();

    public abstract long length();

    public abstract int read(byte[] bArr) throws Exception;

    public void seek(long offset) {
        this.mCurOffset = offset;
    }

    public static TransferDataSource fileSource(File f) {
        return new FileTransferDataSource(f);
    }

    public static TransferDataSource testSource(long length) {
        return new TestDataSource(length);
    }
}
