package com.navdy.service.library.file;

import com.navdy.service.library.events.file.FileTransferData;
import com.navdy.service.library.events.file.FileTransferRequest;
import com.navdy.service.library.events.file.FileTransferResponse;
import com.navdy.service.library.events.file.FileTransferStatus;
import com.navdy.service.library.events.file.FileType;

public interface IFileTransferManager {
    FileType getFileType(int i);

    FileTransferData getNextChunk(int i) throws Throwable;

    FileTransferStatus handleFileTransferData(FileTransferData fileTransferData);

    FileTransferResponse handleFileTransferRequest(FileTransferRequest fileTransferRequest);

    boolean handleFileTransferStatus(FileTransferStatus fileTransferStatus);

    void stop();
}
