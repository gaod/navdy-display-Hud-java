package com.navdy.service.library.file;

import android.content.Context;
import com.navdy.service.library.events.file.FileTransferData;
import com.navdy.service.library.events.file.FileTransferError;
import com.navdy.service.library.events.file.FileTransferResponse;
import com.navdy.service.library.events.file.FileTransferResponse.Builder;
import com.navdy.service.library.events.file.FileTransferStatus;
import com.navdy.service.library.events.file.FileType;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import okio.ByteString;

public class FileTransferSession {
    public static final int CHUNK_SIZE = 131072;
    public static final int PULL_CHUNK_SIZE = 16384;
    private static final Logger sLogger = new Logger(FileTransferSession.class);
    private long mAlreadyTransferred;
    MessageDigest mDataCheckSum;
    String mDestinationFolder;
    int mExpectedChunk = 0;
    File mFile;
    String mFileName;
    private FileInputStream mFileReader;
    long mFileSize;
    private FileType mFileType;
    private boolean mIsTestTransfer = false;
    private long mLastActivity = 0;
    private boolean mPullRequest = false;
    private byte[] mReadBuffer;
    long mTotalBytesTransferred;
    int mTransferId = -1;
    private boolean mWaitForAcknowledgements;
    long negotiatedOffset = -1;

    public FileTransferSession(int mTransferId) {
        this.mTransferId = mTransferId;
    }

    public synchronized long getLastActivity() {
        return this.mLastActivity;
    }

    public synchronized FileTransferResponse initPull(Context context, FileType type, String destinationFolder, String sourceFileName, boolean supportsFlowControl) {
        FileTransferResponse build;
        Builder builder = new Builder();
        this.mWaitForAcknowledgements = supportsFlowControl;
        this.mFileType = type;
        builder.supportsAcks(Boolean.valueOf(supportsFlowControl));
        builder.destinationFileName(sourceFileName).fileType(type);
        try {
            synchronized (this) {
                this.mLastActivity = System.currentTimeMillis();
                this.mDestinationFolder = destinationFolder;
                this.mFileName = sourceFileName;
                this.mFile = new File(destinationFolder, this.mFileName);
                this.mPullRequest = true;
                if (this.mFile.exists() && this.mFile.canRead() && this.mFile.length() > 0) {
                    this.mFileSize = this.mFile.length();
                    this.mFileReader = new FileInputStream(this.mFile);
                    build = builder.success(Boolean.valueOf(true)).maxChunkSize(Integer.valueOf(16384)).transferId(Integer.valueOf(this.mTransferId)).offset(Long.valueOf(this.mFileSize)).build();
                } else {
                    build = builder.success(Boolean.valueOf(false)).error(FileTransferError.FILE_TRANSFER_IO_ERROR).build();
                }
            }
        } catch (Throwable th) {
            build = builder.success(Boolean.valueOf(false)).error(FileTransferError.FILE_TRANSFER_IO_ERROR).build();
        }
        return build;
    }

    public synchronized FileTransferResponse initFileTransfer(Context context, FileType fileType, String destinationFolder, String destinationFileName, long size, long offsetRequested, String checkSum, boolean override) {
        Throwable t;
        FileTransferResponse build;
        Throwable th;
        synchronized (this) {
            this.mLastActivity = System.currentTimeMillis();
        }
        this.mDestinationFolder = destinationFolder;
        this.mFileName = destinationFileName;
        this.mFileSize = size;
        this.mFileType = fileType;
        synchronized (this) {
            this.mExpectedChunk = 0;
        }
        File destinationDirectory = new File(destinationFolder);
        String responseCheckSum = null;
        Builder builder = new Builder();
        builder.fileType(fileType).destinationFileName(destinationFileName);
        builder.transferId(Integer.valueOf(this.mTransferId));
        try {
            long finalOffset;
            File file = new File(destinationDirectory, destinationFileName);
            if (file.exists()) {
                if (file.canWrite()) {
                    long localOffset = file.length();
                    sLogger.d("Requested offset :" + offsetRequested + " , Already existing file size:" + localOffset + ", override :" + override);
                    if (offsetRequested <= localOffset) {
                        if (override) {
                            finalOffset = offsetRequested;
                        } else {
                            finalOffset = localOffset;
                        }
                        if (finalOffset > 0 && !IOUtils.hashForFile(file, offsetRequested).equals(checkSum)) {
                            sLogger.e("Checksum mismatch, local file does not match remote file");
                            finalOffset = 0;
                        }
                    } else {
                        finalOffset = localOffset;
                    }
                    sLogger.d("New proposed offset :" + finalOffset);
                    if (localOffset - finalOffset > 0) {
                        FileOutputStream fos = null;
                        FileChannel channel = null;
                        try {
                            FileOutputStream fos2 = new FileOutputStream(file, true);
                            try {
                                channel = fos2.getChannel();
                                channel.truncate(finalOffset);
                                IOUtils.closeStream(fos2);
                                IOUtils.closeStream(channel);
                                sLogger.e("Truncated the file , previous size :" + localOffset + ", new size :" + file.length());
                            } catch (Throwable th2) {
                                th = th2;
                                fos = fos2;
                                IOUtils.closeStream(fos);
                                IOUtils.closeStream(channel);
                                throw th;
                            }
                        } catch (Throwable th3) {
                            t = th3;
                            sLogger.e("Exception truncating the file to offset " + finalOffset, t);
                            build = builder.error(FileTransferError.FILE_TRANSFER_IO_ERROR).success(Boolean.valueOf(false)).build();
                            IOUtils.closeStream(fos);
                            IOUtils.closeStream(channel);
                            return build;
                        }
                    }
                }
                build = builder.error(FileTransferError.FILE_TRANSFER_PERMISSION_DENIED).success(Boolean.valueOf(false)).build();
                return build;
            }
            sLogger.d("File does not exists, creating new file");
            finalOffset = 0;
            if (!file.createNewFile()) {
                sLogger.d("Permission denied");
                build = builder.error(FileTransferError.FILE_TRANSFER_PERMISSION_DENIED).success(Boolean.valueOf(false)).build();
                return build;
            }
            this.negotiatedOffset = finalOffset;
            sLogger.d("Final offset proposed " + this.negotiatedOffset);
            long totalSize = size - this.negotiatedOffset;
            if (IOUtils.getFreeSpace(destinationFolder) <= totalSize) {
                sLogger.d("Not enough space to write the file, space required :" + totalSize);
                build = builder.error(FileTransferError.FILE_TRANSFER_IO_ERROR).success(Boolean.valueOf(false)).build();
            } else {
                if (this.negotiatedOffset == offsetRequested) {
                    responseCheckSum = checkSum;
                } else if (this.negotiatedOffset > 0) {
                    responseCheckSum = IOUtils.hashForFile(file, this.negotiatedOffset);
                }
                builder.maxChunkSize(Integer.valueOf(131072)).offset(Long.valueOf(this.negotiatedOffset));
                sLogger.d("FileTransferRequest successful");
                builder.success(Boolean.valueOf(true));
                if (responseCheckSum != null) {
                    builder.checksum(responseCheckSum);
                }
                build = builder.build();
            }
        } catch (Exception e) {
            sLogger.e("Error processing FileTransferRequest ", e);
            build = builder.error(FileTransferError.FILE_TRANSFER_IO_ERROR).success(Boolean.valueOf(false)).build();
        }
        return build;
    }

    public synchronized FileTransferResponse initTestData(Context context, FileType fileType, String fileName, long size) {
        FileTransferResponse build;
        this.mLastActivity = System.currentTimeMillis();
        this.mDestinationFolder = "";
        this.mFileName = fileName;
        this.mFileSize = size;
        this.mFileType = fileType;
        this.mExpectedChunk = 0;
        this.mTotalBytesTransferred = 0;
        this.negotiatedOffset = 0;
        this.mIsTestTransfer = true;
        Builder builder = new Builder();
        builder.fileType(fileType).destinationFileName(fileName);
        builder.maxChunkSize(Integer.valueOf(131072)).offset(Long.valueOf(this.negotiatedOffset));
        builder.transferId(Integer.valueOf(this.mTransferId));
        try {
            this.mDataCheckSum = MessageDigest.getInstance("MD5");
            sLogger.d("FileTransferRequest successful");
            builder.success(Boolean.valueOf(true));
            build = builder.build();
        } catch (NoSuchAlgorithmException e) {
            sLogger.e("unable to create MD5 message digest");
            build = builder.error(FileTransferError.FILE_TRANSFER_CHECKSUM_ERROR).success(Boolean.valueOf(false)).build();
        }
        return build;
    }

    private synchronized FileTransferStatus verifyTestDataChunk(Context context, FileTransferData data, FileTransferStatus.Builder responseBuilder) {
        FileTransferStatus build;
        if (data.chunkIndex.intValue() != this.mExpectedChunk) {
            sLogger.e("Illegal chunk,  Expected :" + this.mExpectedChunk + ", received:" + data.chunkIndex);
            build = responseBuilder.error(FileTransferError.FILE_TRANSFER_ILLEGAL_CHUNK).build();
        } else {
            if (data.dataBytes != null && data.dataBytes.size() > 0) {
                this.mTotalBytesTransferred += (long) data.dataBytes.size();
                this.mDataCheckSum.update(data.dataBytes.toByteArray());
            }
            responseBuilder.totalBytesTransferred(Long.valueOf(this.mTotalBytesTransferred));
            this.mExpectedChunk++;
            if (data.lastChunk.booleanValue()) {
                if (this.mTotalBytesTransferred != this.mFileSize) {
                    sLogger.e("Not the last chunk, data missing. Did not receive full file");
                    build = responseBuilder.error(FileTransferError.FILE_TRANSFER_ILLEGAL_CHUNK).build();
                } else {
                    if (IOUtils.bytesToHexString(this.mDataCheckSum.digest()).equals(data.fileCheckSum)) {
                        sLogger.d("Received last chunk, Transfer complete, final data size " + this.mFileSize);
                        responseBuilder.transferComplete(Boolean.valueOf(true));
                    } else {
                        sLogger.e(String.format("Received last chunk but the checksum did not match. expected = \"%s\", actual = \"%s\"", new Object[]{data.fileCheckSum, finalChecksum}));
                        build = responseBuilder.error(FileTransferError.FILE_TRANSFER_CHECKSUM_ERROR).build();
                    }
                }
            }
            build = responseBuilder.success(Boolean.valueOf(true)).build();
        }
        return build;
    }

    public synchronized boolean handleFileTransferStatus(FileTransferStatus fileTransferStatus) {
        boolean z;
        if (fileTransferStatus != null) {
            if (fileTransferStatus.success.booleanValue() && fileTransferStatus.chunkIndex.intValue() == this.mExpectedChunk - 1 && fileTransferStatus.totalBytesTransferred.longValue() == this.mAlreadyTransferred) {
                z = true;
            }
        }
        z = false;
        return z;
    }

    public synchronized FileTransferStatus appendChunk(Context context, FileTransferData data) {
        FileTransferStatus verifyTestDataChunk;
        IOException e;
        Throwable th;
        this.mLastActivity = System.currentTimeMillis();
        FileOutputStream outputStream = null;
        FileTransferStatus.Builder responseBuilder = new FileTransferStatus.Builder().transferId(Integer.valueOf(this.mTransferId)).totalBytesTransferred(Long.valueOf(0)).transferComplete(Boolean.valueOf(false)).success(Boolean.valueOf(false)).chunkIndex(data.chunkIndex);
        if (this.mIsTestTransfer) {
            verifyTestDataChunk = verifyTestDataChunk(context, data, responseBuilder);
        } else {
            File file = new File(this.mDestinationFolder, this.mFileName);
            if (!file.exists()) {
                sLogger.e("Session not initiated ");
                verifyTestDataChunk = responseBuilder.error(FileTransferError.FILE_TRANSFER_NOT_INITIATED).build();
            } else if (data.chunkIndex.intValue() != this.mExpectedChunk) {
                sLogger.e("Illegal chunk,  Expected :" + this.mExpectedChunk + ", received:" + data.chunkIndex);
                verifyTestDataChunk = responseBuilder.error(FileTransferError.FILE_TRANSFER_ILLEGAL_CHUNK).build();
            } else {
                try {
                    sLogger.v(String.format("Starting to write chunk %d into file %s from folder %s", new Object[]{data.chunkIndex, file.getName(), file.getAbsolutePath()}));
                    if (data.dataBytes != null && data.dataBytes.size() > 0) {
                        FileOutputStream outputStream2 = new FileOutputStream(file, true);
                        try {
                            outputStream2.write(data.dataBytes.toByteArray());
                            outputStream = outputStream2;
                        } catch (IOException e2) {
                            e = e2;
                            outputStream = outputStream2;
                            try {
                                sLogger.e("Error writing the chunk to the file", e);
                                verifyTestDataChunk = responseBuilder.error(FileTransferError.FILE_TRANSFER_IO_ERROR).build();
                                IOUtils.fileSync(outputStream);
                                IOUtils.closeStream(outputStream);
                                return verifyTestDataChunk;
                            } catch (Throwable th2) {
                                th = th2;
                            }
                        } catch (Throwable th3) {
                            th = th3;
                            outputStream = outputStream2;
                            IOUtils.fileSync(outputStream);
                            IOUtils.closeStream(outputStream);
                            throw th;
                        }
                    }
                    responseBuilder.totalBytesTransferred(Long.valueOf(file.length()));
                    this.mExpectedChunk++;
                    if (data.lastChunk.booleanValue()) {
                        if (file.length() != this.mFileSize) {
                            sLogger.e("Not the last chunk, data missing. Did not receive full file");
                            verifyTestDataChunk = responseBuilder.error(FileTransferError.FILE_TRANSFER_ILLEGAL_CHUNK).build();
                            IOUtils.fileSync(outputStream);
                            IOUtils.closeStream(outputStream);
                        } else {
                            if (IOUtils.hashForFile(file).equals(data.fileCheckSum)) {
                                sLogger.d("Written last chunk to the file , Transfer complete, final file size " + file.length());
                                responseBuilder.transferComplete(Boolean.valueOf(true));
                            } else {
                                sLogger.e(String.format("Received last chunk but the checksum did not match. expected = \"%s\", actual = \"%s\"", new Object[]{data.fileCheckSum, finalChecksumForTheFile}));
                                verifyTestDataChunk = responseBuilder.error(FileTransferError.FILE_TRANSFER_CHECKSUM_ERROR).build();
                                IOUtils.fileSync(outputStream);
                                IOUtils.closeStream(outputStream);
                            }
                        }
                    }
                    IOUtils.fileSync(outputStream);
                    IOUtils.closeStream(outputStream);
                    verifyTestDataChunk = responseBuilder.success(Boolean.valueOf(true)).build();
                } catch (IOException e3) {
                    e = e3;
                }
            }
        }
        return verifyTestDataChunk;
    }

    public synchronized FileTransferData getNextChunk() throws Throwable {
        FileTransferData build;
        try {
            int chunkId = this.mExpectedChunk;
            this.mExpectedChunk = chunkId + 1;
            long remainingBytes = this.mFileSize - this.mAlreadyTransferred;
            if (remainingBytes < 0) {
                throw new EOFException("End of file, no more chunk");
            }
            int nextChunkSize = 16384;
            boolean lastChunk = false;
            if (remainingBytes < ((long) 16384)) {
                nextChunkSize = (int) remainingBytes;
                lastChunk = true;
            }
            if (this.mReadBuffer == null || this.mReadBuffer.length != nextChunkSize) {
                this.mReadBuffer = new byte[nextChunkSize];
            }
            this.mAlreadyTransferred += (long) this.mFileReader.read(this.mReadBuffer);
            build = new FileTransferData.Builder().transferId(Integer.valueOf(this.mTransferId)).chunkIndex(Integer.valueOf(chunkId)).dataBytes(ByteString.of(this.mReadBuffer)).lastChunk(Boolean.valueOf(lastChunk)).build();
        } catch (Throwable t) {
            sLogger.e("Exception while reading the next chunk ", t);
            IOUtils.closeStream(this.mFileReader);
            build = null;
        }
        return build;
    }

    public synchronized void endSession(Context context, boolean deleteFile) {
        IOUtils.closeStream(this.mFileReader);
        if (this.mPullRequest && deleteFile) {
            IOUtils.deleteFile(context, this.mFile.getAbsolutePath());
        }
        this.mReadBuffer = null;
    }

    public FileType getFileType() {
        return this.mFileType;
    }

    public boolean isFlowControlEnabled() {
        return this.mWaitForAcknowledgements;
    }
}
