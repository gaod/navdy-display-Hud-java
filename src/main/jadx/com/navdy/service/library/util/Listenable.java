package com.navdy.service.library.util;

import com.navdy.service.library.log.Logger;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;

public abstract class Listenable<T extends Listener> {
    private final Object listenerLock = new Object();
    private final Logger logger = new Logger(getClass());
    protected HashSet<WeakReference<T>> mListeners = new HashSet();

    protected interface EventDispatcher<U extends Listenable, T extends Listener> {
        void dispatchEvent(U u, T t);
    }

    public interface Listener {
    }

    public boolean addListener(T newListener) {
        if (newListener == null) {
            this.logger.e("attempted to add null listener");
            return false;
        }
        synchronized (this.listenerLock) {
            Iterator iterator = this.mListeners.iterator();
            while (iterator.hasNext()) {
                Listener listener = (Listener) ((WeakReference) iterator.next()).get();
                if (listener == null) {
                    iterator.remove();
                } else if (listener.equals(newListener)) {
                    return false;
                }
            }
            this.mListeners.add(new WeakReference(newListener));
            return true;
        }
    }

    public boolean removeListener(T listenerToRemove) {
        boolean z = false;
        if (listenerToRemove == null) {
            this.logger.e("attempted to remove null listener");
        } else {
            synchronized (this.listenerLock) {
                Iterator iterator = this.mListeners.iterator();
                while (iterator.hasNext()) {
                    Listener listener = (Listener) ((WeakReference) iterator.next()).get();
                    if (listener == null) {
                        iterator.remove();
                    } else if (listener.equals(listenerToRemove)) {
                        iterator.remove();
                        z = true;
                        break;
                    }
                }
            }
        }
        return z;
    }

    protected void dispatchToListeners(EventDispatcher dispatcher) {
        HashSet<WeakReference<T>> clonedListeners;
        synchronized (this.listenerLock) {
            clonedListeners = (HashSet) this.mListeners.clone();
        }
        Iterator iterator = clonedListeners.iterator();
        while (iterator.hasNext()) {
            Listener listener = (Listener) ((WeakReference) iterator.next()).get();
            if (listener == null) {
                iterator.remove();
            } else {
                dispatcher.dispatchEvent(this, listener);
            }
        }
    }
}
