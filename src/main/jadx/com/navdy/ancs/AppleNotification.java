package com.navdy.ancs;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AppleNotification implements Parcelable {
    public static final int ACTION_ID_NEGATIVE = 1;
    public static final int ACTION_ID_POSITIVE = 0;
    public static final int ATTRIBUTE_APP_IDENTIFIER = 0;
    public static final int ATTRIBUTE_DATE = 5;
    public static final int ATTRIBUTE_MESSAGE = 3;
    public static final int ATTRIBUTE_MESSAGE_SIZE = 4;
    public static final int ATTRIBUTE_NEGATIVE_ACTION_LABEL = 7;
    public static final int ATTRIBUTE_POSITIVE_ACTION_LABEL = 6;
    public static final int ATTRIBUTE_SUBTITLE = 2;
    public static final int ATTRIBUTE_TITLE = 1;
    public static final int CATEGORY_BUSINESS_AND_FINANCE = 9;
    public static final int CATEGORY_EMAIL = 6;
    public static final int CATEGORY_ENTERTAINMENT = 11;
    public static final int CATEGORY_HEALTH_AND_FITNESS = 8;
    public static final int CATEGORY_INCOMING_CALL = 1;
    public static final int CATEGORY_LOCATION = 10;
    public static final int CATEGORY_MISSED_CALL = 2;
    public static final int CATEGORY_NEWS = 7;
    public static final int CATEGORY_OTHER = 0;
    public static final int CATEGORY_SCHEDULE = 5;
    public static final int CATEGORY_SOCIAL = 4;
    public static final int CATEGORY_VOICE_MAIL = 3;
    public static final Creator<AppleNotification> CREATOR = new Creator<AppleNotification>() {
        public AppleNotification createFromParcel(Parcel source) {
            return new AppleNotification(source);
        }

        public AppleNotification[] newArray(int size) {
            return new AppleNotification[size];
        }
    };
    public static final int EVENT_ADDED = 0;
    static final int EVENT_FLAG_IMPORTANT = 2;
    static final int EVENT_FLAG_NEGATIVE_ACTION = 16;
    static final int EVENT_FLAG_POSITIVE_ACTION = 8;
    static final int EVENT_FLAG_PRE_EXISTING = 4;
    static final int EVENT_FLAG_SILENT = 1;
    public static final int EVENT_MODIFIED = 1;
    public static final int EVENT_REMOVED = 2;
    private static final String TAG = AppleNotification.class.getSimpleName();
    private String appId;
    private String appName;
    private int categoryCount;
    private int categoryId;
    private Date date;
    private int eventFlags;
    private int eventId;
    private String message;
    private String negativeActionLabel;
    private int notificationUid;
    private String positiveActionLabel;
    private String subTitle;
    private String title;

    public static AppleNotification parse(byte[] bytes) {
        return new AppleNotification(bytes[0] & 255, bytes[1] & 255, bytes[2] & 255, bytes[3] & 255, bytesToInt(bytes, 4));
    }

    public AppleNotification(int eventId, int eventFlags, int categoryId, int categoryCount, int notificationUid) {
        this.eventId = eventId;
        this.categoryId = categoryId;
        this.categoryCount = categoryCount;
        this.eventFlags = eventFlags;
        this.notificationUid = notificationUid;
    }

    public boolean isSilent() {
        return (this.eventFlags & 1) != 0;
    }

    public boolean isImportant() {
        return (this.eventFlags & 2) != 0;
    }

    public boolean isPreExisting() {
        return (this.eventFlags & 4) != 0;
    }

    public boolean hasPositiveAction() {
        return (this.eventFlags & 8) != 0;
    }

    public boolean hasNegativeAction() {
        return (this.eventFlags & 16) != 0;
    }

    public int getEventId() {
        return this.eventId;
    }

    public int getCategoryId() {
        return this.categoryId;
    }

    public int getCategoryCount() {
        return this.categoryCount;
    }

    public int getEventFlags() {
        return this.eventFlags;
    }

    public int getNotificationUid() {
        return this.notificationUid;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return this.subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAppId() {
        return this.appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getPositiveActionLabel() {
        return this.positiveActionLabel;
    }

    public void setPositiveActionLabel(String positiveActionLabel) {
        this.positiveActionLabel = positiveActionLabel;
    }

    public String getNegativeActionLabel() {
        return this.negativeActionLabel;
    }

    public void setNegativeActionLabel(String negativeActionLabel) {
        this.negativeActionLabel = negativeActionLabel;
    }

    public String getAppName() {
        return this.appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public void setAttribute(int attributeId, String attributeValue) {
        switch (attributeId) {
            case 0:
                setAppId(attributeValue);
                return;
            case 1:
                setTitle(attributeValue);
                return;
            case 2:
                setSubTitle(attributeValue);
                return;
            case 3:
                setMessage(attributeValue);
                return;
            case 5:
                try {
                    setDate(new SimpleDateFormat("yyyyMMdd'T'HHmmSS", Locale.ENGLISH).parse(attributeValue));
                    return;
                } catch (ParseException e) {
                    Log.e(TAG, "Unable to parse date - " + attributeValue);
                    return;
                }
            case 6:
                setPositiveActionLabel(attributeValue);
                return;
            case 7:
                setNegativeActionLabel(attributeValue);
                return;
            default:
                Log.d(TAG, "Unhandled attribute - id:" + attributeId + " val:" + attributeValue);
                return;
        }
    }

    public String toString() {
        StringBuilder builder = new StringBuilder("Notification{eventId=" + this.eventId + ", categoryId=" + this.categoryId + ", categoryCount=" + this.categoryCount + ", eventFlags=" + this.eventFlags + ", notificationUid=" + this.notificationUid);
        if (this.appId != null) {
            builder.append(", appId='");
            builder.append(this.appId);
            builder.append('\'');
        }
        if (this.appName != null) {
            builder.append(", appName='");
            builder.append(this.appName);
            builder.append('\'');
        }
        if (this.title != null) {
            builder.append(", title='");
            builder.append(this.title);
            builder.append('\'');
        }
        if (this.subTitle != null) {
            builder.append(", subTitle='");
            builder.append(this.subTitle);
            builder.append('\'');
        }
        if (this.message != null) {
            builder.append(", message='");
            builder.append(this.message);
            builder.append('\'');
        }
        if (this.positiveActionLabel != null) {
            builder.append(", positiveAction='");
            builder.append(this.positiveActionLabel);
            builder.append('\'');
        }
        if (this.negativeActionLabel != null) {
            builder.append(", negativeAction='");
            builder.append(this.negativeActionLabel);
            builder.append('\'');
        }
        if (this.date != null) {
            builder.append(", date='");
            builder.append(this.date);
            builder.append('\'');
        }
        builder.append("}");
        return builder.toString();
    }

    public static int byteToInt(byte b) {
        return b & 255;
    }

    public static int bytesToShort(byte b0, byte b1) {
        return byteToInt(b0) + (byteToInt(b1) << 8);
    }

    public static int bytesToShort(byte[] bytes, int offset) {
        return bytesToShort(bytes[offset], bytes[offset + 1]);
    }

    public static int bytesToInt(byte b0, byte b1, byte b2, byte b3) {
        return ((byteToInt(b0) + (byteToInt(b1) << 8)) + (byteToInt(b2) << 16)) + (byteToInt(b3) << 24);
    }

    public static int bytesToInt(byte[] bytes, int offset) {
        return bytesToInt(bytes[offset], bytes[offset + 1], bytes[offset + 2], bytes[offset + 3]);
    }

    public int describeContents() {
        return 0;
    }

    public AppleNotification(Parcel in) {
        this(in.readInt(), in.readInt(), in.readInt(), in.readInt(), in.readInt());
        setTitle(in.readString());
        setSubTitle(in.readString());
        setMessage(in.readString());
        long dateMs = in.readLong();
        if (dateMs != 0) {
            setDate(new Date(dateMs));
        }
        setAppId(in.readString());
        setPositiveActionLabel(in.readString());
        setNegativeActionLabel(in.readString());
        setAppName(in.readString());
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.eventId);
        dest.writeInt(this.eventFlags);
        dest.writeInt(this.categoryId);
        dest.writeInt(this.categoryCount);
        dest.writeInt(this.notificationUid);
        dest.writeString(this.title);
        dest.writeString(this.subTitle);
        dest.writeString(this.message);
        dest.writeLong(this.date != null ? this.date.getTime() : 0);
        dest.writeString(this.appId);
        dest.writeString(this.positiveActionLabel);
        dest.writeString(this.negativeActionLabel);
        dest.writeString(this.appName);
    }
}
