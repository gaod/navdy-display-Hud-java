package com.navdy.hud.mfi;

import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.SparseArray;
import com.navdy.hud.app.bluetooth.obex.ObexHelper;
import com.navdy.hud.app.bluetooth.obex.ResponseCodes;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.profile.HudLocale;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;

public class iAPProcessor implements SessionPacketReceiver, EASessionPacketReceiver {
    public static final String CHARSET_NAME = "UTF-8";
    public static final int DEVICE_AUTHENTICATION_CERTIFICATE_WAIT_TIME = 3000;
    public static final int DEVICE_AUTHENTICATION_RETRY_INTERVAL = 5000;
    private static final int EASESSION_SESSION = 7;
    public static final int HEADER_BUFFER = 100;
    public static final String HID_COMPONENT_NAME = "navdy-media-remote";
    public static final int MAX_DEVICE_AUTHENTICATION_RETRIES = 5;
    private static final int MESSAGE_LINK_LOST = 2;
    private static final int MESSAGE_RETRY_DEVICE_AUTHENTICATION = 3;
    private static final int MESSAGE_SESSION_MESSAGE = 1;
    private static final int MESSAGE_SKIP_DEVICE_AUTHENTICATION = 4;
    public static final String NAVDY_CLIENT_BUNDLE_ID = "com.navdy.NavdyClient";
    public static final int PRODUCT_IDENTIFIER = 1;
    public static final String PROTOCOL_V1 = "com.navdy.hud.api.v1";
    public static final String PROXY_V1 = "com.navdy.hud.proxy.v1";
    public static final int STARTING_OFFSET = 6;
    private static final String TAG = iAPProcessor.class.getSimpleName();
    public static final int VENDOR_IDENTIFIER = 39046;
    static final SparseArray<iAPMessage> iAPMsgById = new SparseArray();
    static final boolean isCommunicationUpdateSupported = false;
    static final boolean isCommunicationsSupported = true;
    static final boolean isHIDKeyboardSupported = false;
    static final boolean isHIDMediaRemoteSupported = true;
    private static final boolean isHIDSupported = true;
    static final boolean isMusicSupported = true;
    public static final MediaRemoteComponent mediaRemoteComponent = MediaRemoteComponent.HIDMediaRemote;
    private static final byte[] msgStart = new byte[]{(byte) 64, (byte) 64};
    private static String[] protocols = new String[]{PROTOCOL_V1, PROXY_V1};
    iAPMessage __someiAPMessageToForceEnumLoading__;
    String accessoryName;
    private AuthCoprocessor authCoprocessor;
    private HashMap<iAPMessage, IAPControlMessageProcessor> controlProcessors;
    private int controlSession;
    private int deviceAuthenticationRetries;
    private SparseArray<EASession> eaSessionById;
    int[] hidDescriptorKeyboard;
    int[] hidDescriptorMediaRemote;
    private IAPListener iapListener;
    private boolean isDeviceAuthenticationSupported;
    private boolean linkEstablished;
    private LinkLayer linkLayer;
    private IIAPFileTransferManager mFileTransferManager;
    private final Handler myHandler;
    private SparseArray<SessionMessage> reassambleMsgs;
    private StateListener stateListener;

    enum AccessoryHIDReport {
        HIDComponentIdentifier,
        HIDReport
    }

    enum AppLaunchMethod {
        LaunchWithUserAlert,
        LaunchWithoutAlert
    }

    enum BluetoothTransportComponent {
        TransportComponentIdentifier,
        TransportComponentName,
        TransportSupportsiAP2Connection,
        BluetoothTransportMediaAccessControlAddress
    }

    enum ExternalAccessoryProtocol {
        ExternalAccessoryProtocolIdentifier,
        ExternalAccessoryProtocolName,
        ExternalAccessoryProtocolMatchAction,
        NativeTransportComponentIdentifier
    }

    enum HIDComponentFunction {
        Keyboard,
        MediaPlaybackRemote,
        AssistiveTouchPointer,
        StandardGamepadFormFitting,
        ExtendedGamepadFormFitting,
        ExtendedGamepadNonFormFitting,
        AssistiveSwitchControl,
        Headphone
    }

    static class IAP2Param {
        int id;
        int length;
        int offset;

        IAP2Param() {
        }
    }

    static class IAP2Params {
        public static final int DATA_OFFSET = 4;
        public static final int ID_OFFSET = 2;
        byte[] data;
        SparseArray<IAP2Param> params = new SparseArray();

        IAP2Params(byte[] data, int offset) {
            this.data = data;
            int i = offset;
            while (i < data.length) {
                IAP2Param param = new IAP2Param();
                param.offset = i;
                param.length = Utils.unpackUInt16(data, i);
                param.id = Utils.unpackUInt16(data, i + 2);
                this.params.put(param.id, param);
                i += param.length;
            }
        }

        boolean hasParam(int id) {
            return this.params.get(id) != null;
        }

        boolean hasParam(Enum e) {
            return hasParam(e.ordinal());
        }

        int getUInt8(Enum param) {
            return getUInt8(param.ordinal());
        }

        int getUInt8(int paramId) {
            return Utils.unpackUInt8(this.data, ((IAP2Param) this.params.get(paramId)).offset + 4);
        }

        int getUInt16(int paramId) {
            return Utils.unpackUInt16(this.data, ((IAP2Param) this.params.get(paramId)).offset + 4);
        }

        int getUInt16(Enum param) {
            return getUInt16(param.ordinal());
        }

        long getUInt32(Enum param) {
            return getUInt32(param.ordinal());
        }

        long getUInt32(int paramId) {
            return Utils.unpackUInt32(this.data, ((IAP2Param) this.params.get(paramId)).offset + 4);
        }

        BigInteger getUInt64(int paramId) {
            return Utils.unpackUInt64(this.data, ((IAP2Param) this.params.get(paramId)).offset + 4);
        }

        byte[] getBlob(int paramId) {
            IAP2Param p = (IAP2Param) this.params.get(paramId);
            return Arrays.copyOfRange(this.data, p.offset + 4, p.offset + p.length);
        }

        byte[] getBlob(Enum param) {
            return getBlob(param.ordinal());
        }

        String getUTF8(int paramId) {
            byte[] blob = getBlob(paramId);
            try {
                return new String(blob, 0, blob.length - 1, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                return null;
            }
        }

        String getUTF8(Enum param) {
            return getUTF8(param.ordinal());
        }

        int getEnum(int paramId) {
            return getUInt8(paramId);
        }

        <E> E getEnum(Class<E> type, Enum param) {
            return getEnum((Class) type, param.ordinal());
        }

        <E> E getEnum(Class<E> type, int param) {
            int value = getEnum(param);
            E[] values = type.getEnumConstants();
            if (value < 0 || value >= values.length) {
                return null;
            }
            return values[value];
        }

        int getEnum(Enum param) {
            return getEnum(param.ordinal());
        }

        boolean getBoolean(int paramId) {
            return getUInt8(paramId) != 0;
        }

        boolean getBoolean(Enum param) {
            return getBoolean(param.ordinal());
        }
    }

    static class IAP2ParamsCreator {
        private static final int META_DATA_LENGTH = 4;
        public static final int SIZE_OF_LENGTH = 2;
        public static final int SIZE_OF_PARAM_ID = 2;
        DataOutputStream dos = new DataOutputStream(this.params);
        ByteArrayOutputStream params = new ByteArrayOutputStream();

        byte[] toBytes() {
            return this.params.toByteArray();
        }

        IAP2ParamsCreator addBoolean(int paramId, boolean val) {
            return addUInt8(paramId, val ? 1 : 0);
        }

        IAP2ParamsCreator addBoolean(Enum param, boolean val) {
            return addUInt8(param.ordinal(), val ? 1 : 0);
        }

        IAP2ParamsCreator addUInt8(int paramId, int b) {
            try {
                this.dos.writeShort(5);
                this.dos.writeShort(paramId);
                this.dos.writeByte(b);
            } catch (IOException e) {
                Log.e(iAPProcessor.TAG, "Exception while creating message ", e);
            }
            return this;
        }

        IAP2ParamsCreator addUInt16(int paramId, int i) {
            try {
                this.dos.writeShort(6);
                this.dos.writeShort(paramId);
                this.dos.writeShort(i);
            } catch (IOException e) {
                Log.e(iAPProcessor.TAG, "Exception while creating message ", e);
            }
            return this;
        }

        IAP2ParamsCreator addEnum(int paramId, int b) {
            return addUInt8(paramId, b);
        }

        IAP2ParamsCreator addBlob(int paramId, byte[] blob) {
            try {
                this.dos.writeShort(blob.length + 4);
                this.dos.writeShort(paramId);
                this.dos.write(blob);
            } catch (IOException e) {
                Log.e(iAPProcessor.TAG, "Exception while creating message ", e);
            }
            return this;
        }

        IAP2ParamsCreator addString(int paramId, String s) {
            try {
                byte[] data = s.getBytes("UTF-8");
                this.dos.writeShort((data.length + 4) + 1);
                this.dos.writeShort(paramId);
                this.dos.write(data);
                this.dos.write(new byte[]{(byte) 0});
            } catch (IOException e) {
                Log.e(iAPProcessor.TAG, "Exception while creating message ", e);
            }
            return this;
        }

        IAP2ParamsCreator addNone(int paramId) {
            try {
                this.dos.writeShort(4);
                this.dos.writeShort(paramId);
            } catch (IOException e) {
                Log.e(iAPProcessor.TAG, "Exception while creating message ", e);
            }
            return this;
        }

        IAP2ParamsCreator addUInt8(Enum paramId, int b) {
            return addUInt8(paramId.ordinal(), b);
        }

        IAP2ParamsCreator addUInt16(Enum paramId, int i) {
            return addUInt16(paramId.ordinal(), i);
        }

        IAP2ParamsCreator addEnum(Enum paramId, int b) {
            return addEnum(paramId.ordinal(), b);
        }

        IAP2ParamsCreator addEnum(Enum paramId, Enum b) {
            return addEnum(paramId.ordinal(), b.ordinal());
        }

        IAP2ParamsCreator addBlob(Enum paramId, byte[] blob) {
            return addBlob(paramId.ordinal(), blob);
        }

        IAP2ParamsCreator addString(Enum paramId, String s) {
            return addString(paramId.ordinal(), s);
        }

        IAP2ParamsCreator addNone(Enum paramId) {
            return addNone(paramId.ordinal());
        }
    }

    static class IAP2SessionMessage extends IAP2ParamsCreator {
        int msgId;

        IAP2SessionMessage(int msgId) {
            this.msgId = msgId;
            try {
                this.dos.write(iAPProcessor.msgStart);
                this.dos.writeShort(0);
                this.dos.writeShort(msgId);
            } catch (IOException ie) {
                Log.e(iAPProcessor.TAG, "Exception while creating message ", ie);
            }
        }

        IAP2SessionMessage(iAPMessage msgId) {
            this(msgId.id);
        }

        byte[] toBytes() {
            byte[] bytes = super.toBytes();
            iAPProcessor.copy(bytes, 2, Utils.packUInt16(bytes.length));
            return bytes;
        }
    }

    enum IdentificationInformation {
        Name,
        ModelIdentifier,
        Manufacturer,
        SerialNumber,
        FirmwareVersion,
        HardwareVersion,
        MessagesSentByAccessory,
        MessagesReceivedFromDevice,
        PowerProvidingCapability,
        MaximumCurrentDrawnFromDevice,
        SupportedExternalAccessoryProtocol,
        AppMatchTeamID,
        CurrentLanguage,
        SupportedLanguage,
        SerialTransportComponent,
        USBDeviceTransportComponent,
        USBHostTransportComponent,
        BluetoothTransportComponent,
        iAP2HIDComponent,
        VehicleInformationComponent,
        VehicleStatusComponent,
        LocationInformationComponent,
        USBHostHIDComponent
    }

    public enum MediaRemoteComponent {
        HIDKeyboard,
        HIDMediaRemote
    }

    enum RequestAppLaunch {
        AppBundleID,
        LaunchAlert
    }

    class SessionMessage {
        ByteArrayBuilder builder;
        int msgId;
        int msgLen;

        SessionMessage(int msgLen, int msgId) {
            this.msgLen = msgLen;
            this.msgId = msgId;
            this.builder = new ByteArrayBuilder(msgLen);
        }

        void append(byte[] bytes) {
            try {
                this.builder.addBlob(bytes);
            } catch (IOException e) {
                Log.e(iAPProcessor.TAG, "Error appending messages ");
            }
        }
    }

    enum StartHID {
        HIDComponentIdentifier,
        VendorIdentifier,
        ProductIdentifier,
        LocalizedKeyboardCountryCode,
        HIDReportDescriptor
    }

    public interface StateListener {
        void onError();

        void onReady();

        void onSessionStart(EASession eASession);

        void onSessionStop(EASession eASession);
    }

    enum StopHID {
        HIDComponentIdentifier
    }

    enum iAP2HIDComponent {
        HIDComponentIdentifier,
        HIDComponentName,
        HIDComponentFunction
    }

    enum iAPMessage {
        RequestAuthenticationRequest(43520),
        AuthenticationCertificate(43521),
        RequestAuthenticationChallengeResponse(43522),
        AuthenticationResponse(43523),
        AuthenticationFailed(43524),
        AuthenticationSucceeded(43525),
        StartIdentification(7424),
        IdentificationInformation(7425),
        IdentificationAccepted(7426),
        IdentificationRejected(7427),
        CancelIdentification(7428),
        IdentificationInformationUpdate(7429),
        StartExternalAccessoryProtocolSession(59904),
        StopExternalAccessoryProtocolSession(59905),
        StatusExternalAccessoryProtocolSession(59906),
        RequestDeviceAuthenticationCertificate(43536),
        DeviceAuthenticationCertificate(43537),
        RequestDeviceAuthenticationChallengeResponse(43538),
        DeviceAuthenticationResponse(43539),
        DeviceAuthenticationFailed(43540),
        DeviceAuthenticationSucceeded(43541),
        StartHID(26624),
        DeviceHIDReport(26625),
        AccessoryHIDReport(26626),
        StopHID(26627),
        StartCallStateUpdates(16724),
        CallStateUpdate(16725),
        StopCallStateUpdates(16726),
        InitiateCall(16730),
        AcceptCall(16731),
        EndCall(16732),
        HoldStatusUpdate(16735),
        MuteStatusUpdate(16736),
        StartCommunicationUpdates(16727),
        CommunicationUpdate(16728),
        StopCommunicationUpdates(16729),
        StartListUpdates(16752),
        ListUpdate(16753),
        StopListUpdate(16754),
        StartNowPlayingUpdates(20480),
        NowPlayingUpdate(20481),
        StopNowPlayingUpdates(20482),
        RequestAppLaunch(59906);
        
        int id;

        private iAPMessage(int id) {
            this.id = id;
            iAPProcessor.iAPMsgById.put(id, this);
        }
    }

    public void setAccessoryName(String accessoryName) {
        this.accessoryName = accessoryName;
    }

    public void connect(IIAPFileTransferManager fileTransferManager) {
        this.mFileTransferManager = fileTransferManager;
    }

    public void connect(StateListener eaSessionListener) {
        this.stateListener = eaSessionListener;
    }

    public void connect(LinkLayer linkLayer) {
        this.linkLayer = linkLayer;
    }

    public void registerControlMessageProcessor(iAPMessage message, IAPControlMessageProcessor processor) {
        if (processor != null) {
            this.controlProcessors.put(message, processor);
        }
    }

    public iAPProcessor(IAPListener listener, Context context) {
        this.eaSessionById = new SparseArray();
        this.deviceAuthenticationRetries = 0;
        this.controlSession = -1;
        this.isDeviceAuthenticationSupported = true;
        this.hidDescriptorMediaRemote = new int[]{5, 12, 9, 1, ResponseCodes.OBEX_HTTP_CREATED, 1, 21, 0, 37, 1, 9, ResponseCodes.OBEX_HTTP_UNSUPPORTED_TYPE, 9, ResponseCodes.OBEX_HTTP_ENTITY_TOO_LARGE, 9, ResponseCodes.OBEX_HTTP_USE_PROXY, 9, 182, 117, 1, 149, 4, ObexHelper.OBEX_OPCODE_DISCONNECT, 2, 117, 4, 149, 1, ObexHelper.OBEX_OPCODE_DISCONNECT, 3, 192};
        this.hidDescriptorKeyboard = new int[]{5, 12, 9, 1, ResponseCodes.OBEX_HTTP_CREATED, 1, 5, 7, 21, 0, 37, 1, 117, 8, 149, 1, ObexHelper.OBEX_OPCODE_DISCONNECT, 3, 5, 12, 21, 0, 37, 1, 9, 64, 9, ResponseCodes.OBEX_HTTP_ENTITY_TOO_LARGE, 9, ResponseCodes.OBEX_HTTP_USE_PROXY, 9, 182, 117, 1, 149, 4, ObexHelper.OBEX_OPCODE_DISCONNECT, 2, 117, 4, 149, 1, ObexHelper.OBEX_OPCODE_DISCONNECT, 3, 192};
        this.accessoryName = "Navdy HUD";
        this.myHandler = new Handler(Looper.getMainLooper()) {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        iAPProcessor.this.handleSessionPacket(msg.obj);
                        return;
                    case 2:
                        iAPProcessor.this.handleLinkLoss();
                        return;
                    case 3:
                        iAPProcessor.this.retryDeviceAuthentication();
                        return;
                    case 4:
                        if (iAPProcessor.this.stateListener != null) {
                            Log.d(Utils.MFI_TAG, "Skipping Device Authentication as we did not get the certificate");
                            iAPProcessor.this.stateListener.onReady();
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        };
        this.__someiAPMessageToForceEnumLoading__ = iAPMessage.RequestAuthenticationRequest;
        this.reassambleMsgs = new SparseArray();
        this.iapListener = listener;
        this.authCoprocessor = new AuthCoprocessor(listener, context);
        this.controlProcessors = new HashMap();
    }

    public iAPProcessor(IAPListener listener) {
        this(listener, null);
    }

    private boolean startsWith(byte[] d, byte[] prefix) {
        for (int i = 0; i < d.length; i++) {
            if (i >= prefix.length) {
                return true;
            }
            if (d[i] != prefix[i]) {
                return false;
            }
        }
        return false;
    }

    static void copy(byte[] dst, int dstPos, byte[] src) {
        System.arraycopy(src, 0, dst, dstPos, src.length);
    }

    private void retryDeviceAuthentication() {
        if (this.linkEstablished) {
            this.deviceAuthenticationRetries++;
            sendToDevice(this.controlSession, new IAP2SessionMessage(iAPMessage.RequestDeviceAuthenticationCertificate));
        }
    }

    private void handleLinkLoss() {
        this.myHandler.removeMessages(3);
        this.myHandler.removeMessages(4);
        this.linkEstablished = false;
        this.deviceAuthenticationRetries = 0;
        if (this.mFileTransferManager != null) {
            this.mFileTransferManager.clear();
        }
        for (int index = 0; index < this.eaSessionById.size(); index++) {
            stopEASession((EASession) this.eaSessionById.valueAt(index));
        }
        this.eaSessionById.clear();
    }

    private void handleSessionPacket(SessionPacket smsg) {
        try {
            this.linkEstablished = true;
            if (smsg.session != 2) {
                SessionMessage rcvMsg = (SessionMessage) this.reassambleMsgs.get(smsg.session);
                if (rcvMsg == null) {
                    if (startsWith(smsg.data, msgStart)) {
                        rcvMsg = new SessionMessage(Utils.unpackUInt16(smsg.data, 2), Utils.unpackUInt16(smsg.data, 4));
                        this.reassambleMsgs.put(smsg.session, rcvMsg);
                    } else {
                        int eaSessionId = Utils.unpackUInt16(smsg.data, 0);
                        Log.d(Utils.MFI_TAG, "Apple Device(E):" + eaSessionId + " Size:" + smsg.data.length);
                        if (this.eaSessionById.get(eaSessionId) != null) {
                            ((EASession) this.eaSessionById.get(eaSessionId)).queue(Arrays.copyOfRange(smsg.data, 2, smsg.data.length));
                        }
                    }
                }
                if (rcvMsg != null) {
                    rcvMsg.append(smsg.data);
                    if (rcvMsg.builder.size() < rcvMsg.msgLen) {
                        return;
                    }
                    if (rcvMsg.builder.size() != rcvMsg.msgLen) {
                        this.reassambleMsgs.remove(smsg.session);
                        throw new RuntimeException("bad message size");
                    }
                    handleSessionMessage(smsg.session, rcvMsg.msgId, rcvMsg.builder.build());
                    this.reassambleMsgs.remove(smsg.session);
                }
            } else if (this.mFileTransferManager != null) {
                this.mFileTransferManager.queue(smsg.data);
            }
        } catch (Exception e) {
            Log.e(TAG, "", e);
            this.reassambleMsgs.remove(smsg.session);
            if (this.stateListener != null) {
                this.stateListener.onError();
            }
        }
    }

    private void handleSessionMessage(int session, int msgid, byte[] data) throws IOException {
        iAPMessage msgid_ = (iAPMessage) iAPMsgById.get(msgid);
        if (msgid_ == null) {
            Log.e(TAG, String.format("received unknown message (%d bytes)", new Object[]{Integer.valueOf(data.length)}));
            return;
        }
        Log.d(Utils.MFI_TAG, "received message " + msgid_.toString() + " (" + data.length + " bytes) ");
        switch (msgid_) {
            case RequestAuthenticationRequest:
                this.controlSession = session;
                this.deviceAuthenticationRetries = 0;
                this.authCoprocessor.open();
                try {
                    sendToDevice(session, new IAP2SessionMessage(iAPMessage.AuthenticationCertificate).addBlob(0, this.authCoprocessor.readAccessoryCertificate()));
                    return;
                } finally {
                    this.authCoprocessor.close();
                }
            case RequestAuthenticationChallengeResponse:
                byte[] chg = parse(data).getBlob(0);
                this.authCoprocessor.open();
                try {
                    sendToDevice(session, new IAP2SessionMessage(iAPMessage.AuthenticationResponse).addBlob(0, this.authCoprocessor.createAccessorySignature(chg)));
                    return;
                } finally {
                    this.authCoprocessor.close();
                }
            case AuthenticationSucceeded:
                return;
            case StartIdentification:
                this.isDeviceAuthenticationSupported = true;
                sendIdentification(session);
                return;
            case IdentificationAccepted:
                this.deviceAuthenticationRetries = 0;
                if (this.isDeviceAuthenticationSupported) {
                    Log.d(Utils.MFI_TAG, "DeviceAuthentication supported, attempting");
                    sendToDevice(session, new IAP2SessionMessage(iAPMessage.RequestDeviceAuthenticationCertificate));
                    this.myHandler.sendEmptyMessageDelayed(4, 3000);
                    return;
                } else if (this.stateListener != null) {
                    this.stateListener.onReady();
                    return;
                } else {
                    return;
                }
            case IdentificationRejected:
                Log.d(TAG, String.format("msg data: %s", new Object[]{Utils.bytesToHex(data, true)}));
                if (this.isDeviceAuthenticationSupported) {
                    Log.d(Utils.MFI_TAG, "Identification rejected, trying without DeviceAuthentication");
                    this.isDeviceAuthenticationSupported = false;
                    sendIdentification(session);
                    return;
                }
                Log.d(Utils.MFI_TAG, "Identification rejected, even with DeviceAuthentication turned off");
                if (this.stateListener != null) {
                    this.stateListener.onError();
                    return;
                }
                return;
            case DeviceAuthenticationCertificate:
                this.myHandler.removeMessages(4);
                byte[] cert = parse(data).getBlob(0);
                this.authCoprocessor.open();
                try {
                    sendToDevice(session, new IAP2SessionMessage(iAPMessage.RequestDeviceAuthenticationChallengeResponse).addBlob(0, this.authCoprocessor.validateDeviceCertificateAndGenerateDeviceChallenge(cert)));
                    return;
                } finally {
                    this.authCoprocessor.close();
                }
            case DeviceAuthenticationResponse:
                byte[] respChg = parse(data).getBlob(0);
                this.authCoprocessor.open();
                try {
                    boolean ok;
                    if (respChg.length > 128) {
                        Log.w(Utils.MFI_TAG, "DeviceAuthenticationResponse response too big - working around iOS 10 bug");
                        ok = true;
                    } else {
                        ok = this.authCoprocessor.validateDeviceSignature(respChg);
                    }
                    if (!ok) {
                        Log.e(Utils.MFI_TAG, "DeviceAuthenticationResponse validation failed");
                    }
                    if (this.deviceAuthenticationRetries >= 5) {
                        Log.d(Utils.MFI_TAG, "DeviceAuthenticationRetries reached the limit so sending success");
                        ok = true;
                    }
                    sendToDevice(session, new IAP2SessionMessage(ok ? iAPMessage.DeviceAuthenticationSucceeded : iAPMessage.DeviceAuthenticationFailed));
                    if (!ok) {
                        this.myHandler.sendEmptyMessageDelayed(3, 5000);
                    } else if (this.iapListener != null) {
                        this.iapListener.onDeviceAuthenticationSuccess(this.deviceAuthenticationRetries);
                    }
                    if (this.stateListener != null) {
                        if (ok) {
                            this.stateListener.onReady();
                        } else {
                            this.stateListener.onError();
                        }
                    }
                    this.authCoprocessor.close();
                    return;
                } catch (Throwable th) {
                    this.authCoprocessor.close();
                }
            case StartExternalAccessoryProtocolSession:
                IAP2Params params = parse(data);
                int protoId = params.getUInt8(0);
                int eaSessionId = params.getUInt16(1);
                Log.d(Utils.MFI_TAG, "StartExternalAccessoryProtocolSession: " + eaSessionId + " (protoId: " + protoId + HereManeuverDisplayBuilder.CLOSE_BRACKET);
                EASession eaSession = new EASession(this, this.linkLayer.getRemoteAddress(), this.linkLayer.getRemoteName(), eaSessionId, protocols[protoId]);
                this.eaSessionById.append(eaSessionId, eaSession);
                startEASession(eaSession);
                return;
            case StopExternalAccessoryProtocolSession:
                int sessionId = parse(data).getUInt16(0);
                stopEASession((EASession) this.eaSessionById.get(sessionId));
                this.eaSessionById.remove(sessionId);
                return;
            default:
                IAPControlMessageProcessor processor = (IAPControlMessageProcessor) this.controlProcessors.get(msgid_);
                if (processor != null) {
                    processor.processControlMessage(msgid_, session, data);
                    return;
                }
                return;
        }
    }

    private void sendIdentification(int session) throws IOException {
        IAP2ParamsCreator message = new IAP2SessionMessage(iAPMessage.IdentificationInformation);
        message.addString(IdentificationInformation.Name, this.accessoryName).addString(IdentificationInformation.ModelIdentifier, Build.MODEL).addString(IdentificationInformation.Manufacturer, Build.MANUFACTURER).addString(IdentificationInformation.SerialNumber, Build.SERIAL).addString(IdentificationInformation.FirmwareVersion, VERSION.RELEASE).addString(IdentificationInformation.HardwareVersion, Build.HARDWARE);
        ByteArrayBuilder messageSentByAccessory = new ByteArrayBuilder();
        if (this.isDeviceAuthenticationSupported) {
            messageSentByAccessory.addInt16(iAPMessage.RequestDeviceAuthenticationCertificate.id).addInt16(iAPMessage.RequestDeviceAuthenticationChallengeResponse.id).addInt16(iAPMessage.DeviceAuthenticationSucceeded.id).addInt16(iAPMessage.DeviceAuthenticationFailed.id).build();
        }
        messageSentByAccessory.addInt16(iAPMessage.StartHID.id).addInt16(iAPMessage.StopHID.id).addInt16(iAPMessage.AccessoryHIDReport.id);
        messageSentByAccessory.addInt16(iAPMessage.StartCallStateUpdates.id).addInt16(iAPMessage.StopCallStateUpdates.id).addInt16(iAPMessage.InitiateCall.id).addInt16(iAPMessage.AcceptCall.id).addInt16(iAPMessage.EndCall.id).addInt16(iAPMessage.MuteStatusUpdate.id);
        messageSentByAccessory.addInt16(iAPMessage.StartNowPlayingUpdates.id).addInt16(iAPMessage.StopNowPlayingUpdates.id);
        messageSentByAccessory.addInt16(iAPMessage.RequestAppLaunch.id);
        ByteArrayBuilder messageReceivedByAccessory = new ByteArrayBuilder();
        messageReceivedByAccessory.addInt16(iAPMessage.StartExternalAccessoryProtocolSession.id).addInt16(iAPMessage.StopExternalAccessoryProtocolSession.id);
        messageReceivedByAccessory.addInt16(iAPMessage.DeviceHIDReport.id);
        messageReceivedByAccessory.addInt16(iAPMessage.CallStateUpdate.id);
        messageReceivedByAccessory.addInt16(iAPMessage.NowPlayingUpdate.id);
        if (this.isDeviceAuthenticationSupported) {
            messageReceivedByAccessory.addInt16(iAPMessage.DeviceAuthenticationCertificate.id).addInt16(iAPMessage.DeviceAuthenticationResponse.id);
        }
        message.addBlob(IdentificationInformation.MessagesSentByAccessory, messageSentByAccessory.build()).addBlob(IdentificationInformation.MessagesReceivedFromDevice, messageReceivedByAccessory.build()).addEnum(IdentificationInformation.PowerProvidingCapability, 0).addUInt16(IdentificationInformation.MaximumCurrentDrawnFromDevice, 0).addBlob(IdentificationInformation.SupportedExternalAccessoryProtocol, new IAP2ParamsCreator().addUInt8(ExternalAccessoryProtocol.ExternalAccessoryProtocolIdentifier, 0).addString(ExternalAccessoryProtocol.ExternalAccessoryProtocolName, protocols[0]).addEnum(ExternalAccessoryProtocol.ExternalAccessoryProtocolMatchAction, 1).toBytes()).addBlob(IdentificationInformation.SupportedExternalAccessoryProtocol, new IAP2ParamsCreator().addUInt8(ExternalAccessoryProtocol.ExternalAccessoryProtocolIdentifier, 1).addString(ExternalAccessoryProtocol.ExternalAccessoryProtocolName, protocols[1]).addEnum(ExternalAccessoryProtocol.ExternalAccessoryProtocolMatchAction, 1).toBytes()).addString(IdentificationInformation.CurrentLanguage, HudLocale.DEFAULT_LANGUAGE).addString(IdentificationInformation.SupportedLanguage, HudLocale.DEFAULT_LANGUAGE).addBlob(IdentificationInformation.BluetoothTransportComponent, new IAP2ParamsCreator().addUInt16(BluetoothTransportComponent.TransportComponentIdentifier, 1234).addString(BluetoothTransportComponent.TransportComponentName, "navdy-transport").addNone(BluetoothTransportComponent.TransportSupportsiAP2Connection).addBlob(BluetoothTransportComponent.BluetoothTransportMediaAccessControlAddress, this.linkLayer.getLocalAddress()).toBytes());
        message.addBlob(IdentificationInformation.iAP2HIDComponent, new IAP2ParamsCreator().addUInt16(iAP2HIDComponent.HIDComponentIdentifier, MediaRemoteComponent.HIDMediaRemote.ordinal()).addString(iAP2HIDComponent.HIDComponentName, HID_COMPONENT_NAME).addEnum(iAP2HIDComponent.HIDComponentFunction, HIDComponentFunction.MediaPlaybackRemote).toBytes());
        sendToDevice(session, message);
    }

    public void sendControlMessage(IAP2ParamsCreator msg) {
        sendToDevice(this.controlSession, msg);
    }

    public void sendToDevice(int sessionId, IAP2ParamsCreator msg) {
        byte[] data = msg.toBytes();
        if (msg instanceof IAP2SessionMessage) {
            Log.d(Utils.MFI_TAG, "sending message " + ((iAPMessage) iAPMsgById.get(((IAP2SessionMessage) msg).msgId)).toString());
            Utils.logTransfer(TAG, "sending message %s (%d bytes)", msgId_.toString(), Integer.valueOf(data.length));
        }
        sendToDevice(sessionId, data);
    }

    public void sendToDevice(int sessionId, byte[] bytes) {
        this.linkLayer.queue(new SessionPacket(sessionId, bytes));
    }

    private void startEASession(EASession eaSession) {
        Log.d(TAG, "startEASession: session: " + eaSession.getSessionIdentifier());
        if (this.stateListener != null) {
            this.stateListener.onSessionStart(eaSession);
        }
    }

    private void stopEASession(EASession eaSession) {
        Log.d(TAG, "stopEASession: session: " + eaSession.getSessionIdentifier());
        if (this.stateListener != null) {
            this.stateListener.onSessionStop(eaSession);
        }
    }

    public void linkLost() {
        this.myHandler.sendEmptyMessage(2);
    }

    public void queue(SessionPacket pkt) {
        this.myHandler.sendMessage(this.myHandler.obtainMessage(1, pkt));
    }

    public void queue(EASessionPacket pkt) {
        int eaSessionIdentifier = ((EASession) this.eaSessionById.get(pkt.session)).getSessionIdentifier();
        int maximumLinkPayloadSize = this.linkLayer.native_get_maximum_payload_size() - 100;
        if (maximumLinkPayloadSize > 0) {
            Utils.logTransfer(TAG, "EA->iAP[" + eaSessionIdentifier + HereManeuverDisplayBuilder.SLASH + 7 + "]", new Object[0]);
            int remainingDataLength = pkt.data.length;
            int offset = 0;
            do {
                int packetLength = Math.min(remainingDataLength, maximumLinkPayloadSize);
                remainingDataLength -= packetLength;
                int bufferSize = packetLength + 2;
                try {
                    Log.d(Utils.MFI_TAG, "Accessory(E)   :" + eaSessionIdentifier + " Size:" + packetLength);
                    sendToDevice(7, new ByteArrayBuilder(bufferSize).addInt16(eaSessionIdentifier).addBlob(pkt.data, offset, packetLength).build());
                    offset += packetLength;
                    continue;
                } catch (IOException e) {
                    Log.e(TAG, "Error while sending to the device" + e);
                    continue;
                }
            } while (remainingDataLength > 0);
        }
    }

    public void startHIDSession() {
        int[] descriptor = null;
        int component = mediaRemoteComponent.ordinal();
        switch (mediaRemoteComponent) {
            case HIDKeyboard:
                descriptor = this.hidDescriptorKeyboard;
                break;
            case HIDMediaRemote:
                descriptor = this.hidDescriptorMediaRemote;
                break;
        }
        sendToDevice(this.controlSession, new IAP2SessionMessage(iAPMessage.StartHID).addUInt16(StartHID.HIDComponentIdentifier, component).addUInt16(StartHID.VendorIdentifier, (int) VENDOR_IDENTIFIER).addUInt16(StartHID.ProductIdentifier, 1).addBlob(StartHID.HIDReportDescriptor, Utils.intsToBytes(descriptor)));
    }

    public void stopHIDSession() {
        sendToDevice(this.controlSession, new IAP2SessionMessage(iAPMessage.StopHID).addUInt16(AccessoryHIDReport.HIDComponentIdentifier, mediaRemoteComponent.ordinal()));
    }

    public void onKeyDown(Enum key) {
        onKeyDown(key.ordinal());
    }

    public void onKeyDown(int key) {
        sendHIDReport(1 << key);
    }

    public void onKeyUp(Enum key) {
        sendHIDReport(0);
    }

    public void onKeyUp(int key) {
        sendHIDReport(0);
    }

    private void sendHIDReport(int keyMask) {
        int component = mediaRemoteComponent.ordinal();
        int[] report = null;
        switch (mediaRemoteComponent) {
            case HIDKeyboard:
                report = new int[]{0, keyMask};
                break;
            case HIDMediaRemote:
                report = new int[]{keyMask};
                break;
        }
        sendToDevice(this.controlSession, new IAP2SessionMessage(iAPMessage.AccessoryHIDReport).addUInt16(AccessoryHIDReport.HIDComponentIdentifier, component).addBlob(AccessoryHIDReport.HIDReport, Utils.intsToBytes(report)));
    }

    public int getControlSessionId() {
        return this.controlSession;
    }

    public void launchApp(String appBundleId, boolean launchAlert) {
        IAP2SessionMessage requestAppLaunchMessage = new IAP2SessionMessage(iAPMessage.RequestAppLaunch.id);
        requestAppLaunchMessage.addString(RequestAppLaunch.AppBundleID, appBundleId);
        requestAppLaunchMessage.addEnum(RequestAppLaunch.LaunchAlert, launchAlert ? AppLaunchMethod.LaunchWithUserAlert : AppLaunchMethod.LaunchWithoutAlert);
        sendControlMessage(requestAppLaunchMessage);
    }

    public void launchApp(boolean launchAlert) {
        launchApp(NAVDY_CLIENT_BUNDLE_ID, launchAlert);
    }

    public static IAP2Params parse(byte[] data) {
        if (data != null) {
            return new IAP2Params(data, 6);
        }
        throw new IllegalArgumentException();
    }
}
