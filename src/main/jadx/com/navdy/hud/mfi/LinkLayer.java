package com.navdy.hud.mfi;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import java.util.concurrent.ConcurrentLinkedQueue;

public class LinkLayer implements SessionPacketReceiver, LinkPacketReceiver {
    public static final int CONTROL_BYTE_ACK = 64;
    public static final int CONTROL_BYTE_EAK = 32;
    public static final int CONTROL_BYTE_RST = 16;
    public static final int CONTROL_BYTE_SLP = 8;
    public static final int CONTROL_BYTE_SYN = 128;
    public static final int DELAY_MILLIS = 20;
    public static final int MESSAGE_CONNECTION_ENDED = 4;
    public static final int MESSAGE_CONNECTION_STARTED = 3;
    public static final int MESSAGE_LINK_PACKET = 1;
    public static final int MESSAGE_PERIODIC = 0;
    public static final int MESSAGE_SESSION_PACKET = 2;
    public static final String TAG = "LinkLayer";
    private static final boolean sLogLinkPacket = true;
    private ConcurrentLinkedQueue<SessionPacket> controlMessages;
    private iAPProcessor iAPProcessor;
    private Handler myHandler;
    private ConcurrentLinkedQueue<SessionPacket> nonControlMessages;
    private PhysicalLayer physicalLayer;
    private String remoteAddress;
    private String remoteName;

    public interface PhysicalLayer extends LinkPacketReceiver {
        byte[] getLocalAddress();
    }

    private native int native_get_message_session();

    private native byte[] native_pop_message_data(int i);

    private native int native_runloop(int i, byte[] bArr, int i2);

    public native int native_get_maximum_payload_size();

    public void connect(PhysicalLayer physicalLayer) {
        this.physicalLayer = physicalLayer;
    }

    public void connect(iAPProcessor iAPProcessor) {
        this.iAPProcessor = iAPProcessor;
    }

    static {
        System.loadLibrary("iap2");
    }

    public LinkLayer() {
        start();
    }

    public void start() {
        this.controlMessages = new ConcurrentLinkedQueue();
        this.nonControlMessages = new ConcurrentLinkedQueue();
        this.myHandler = new Handler(Looper.getMainLooper()) {
            public void handleMessage(Message msg) {
                byte[] data = null;
                int session = 0;
                int messageType = msg.what;
                switch (msg.what) {
                    case 0:
                    case 2:
                        SessionPacket sessionPacket = null;
                        if (LinkLayer.this.controlMessages.peek() != null) {
                            sessionPacket = (SessionPacket) LinkLayer.this.controlMessages.poll();
                        } else if (LinkLayer.this.nonControlMessages.peek() != null) {
                            sessionPacket = (SessionPacket) LinkLayer.this.nonControlMessages.poll();
                        }
                        if (sessionPacket != null) {
                            messageType = 2;
                            data = sessionPacket.data;
                            session = sessionPacket.session;
                            break;
                        }
                        break;
                    case 1:
                        data = ((Packet) msg.obj).data;
                        break;
                    case 4:
                        LinkLayer.this.iAPProcessor.linkLost();
                        break;
                }
                if (msg.what == 0) {
                    sendEmptyMessageDelayed(0, 20);
                }
                doNativeRunLoop(messageType, data, session);
            }

            private void doNativeRunLoop(int messageType, byte[] data, int session) {
                if (LinkLayer.this.native_runloop(messageType, data, session) < 0) {
                    Log.i(LinkLayer.TAG, "runloop detected a disconnect");
                    LinkLayer.this.connectionEnded();
                    return;
                }
                transferPackets(1);
                transferPackets(2);
            }

            private void transferPackets(int messageType) {
                while (true) {
                    int session = LinkLayer.this.native_get_message_session();
                    byte[] data = LinkLayer.this.native_pop_message_data(messageType);
                    if (data != null) {
                        switch (messageType) {
                            case 1:
                                LinkLayer.this.logOutGoingLinkPacket(data);
                                LinkLayer.this.physicalLayer.queue(new LinkPacket(data));
                                break;
                            case 2:
                                SessionPacket pkt = new SessionPacket(session, data);
                                LinkLayer.this.log("LL->iAP[" + pkt.session + "]", pkt);
                                LinkLayer.this.iAPProcessor.queue(pkt);
                                break;
                            default:
                                Log.e(LinkLayer.TAG, "transferPackets: unsupported message type (" + messageType + HereManeuverDisplayBuilder.CLOSE_BRACKET);
                                break;
                        }
                    }
                    return;
                }
            }
        };
        this.myHandler.sendEmptyMessageDelayed(0, 20);
    }

    private void logOutGoingLinkPacket(byte[] data) {
        if (data != null && data.length > 7) {
            int packetSequenceNumber = data[5] & 255;
            int ackNumber = data[6] & 255;
            int controlByte = data[4] & 255;
            StringBuilder controlBytes = new StringBuilder();
            if ((controlByte & 64) != 0) {
                controlBytes.append(" ACK ");
            }
            if ((controlByte & 128) != 0) {
                controlBytes.append(" SYN ");
            }
            if ((controlByte & 32) != 0) {
                controlBytes.append(" EAK ");
            }
            if ((controlByte & 16) != 0) {
                controlBytes.append(" RST ");
            }
            if ((controlByte & 8) != 0) {
                controlBytes.append(" SLP ");
            }
            Log.d(Utils.MFI_TAG, "Accessory(L)   :" + packetSequenceNumber + " " + ackNumber + " Size:" + data.length + controlBytes.toString());
        }
    }

    public void stop() {
    }

    private void queuePacket(int messageType, Object pkt) {
        this.myHandler.sendMessage(this.myHandler.obtainMessage(messageType, pkt));
    }

    private void log(String desc, Packet pkt) {
        Utils.logTransfer(TAG, "%s: %s", desc, pkt.data);
    }

    public void queue(LinkPacket pkt) {
        queuePacket(1, pkt);
    }

    public void queue(SessionPacket pkt) {
        log("iAP->LL[" + pkt.session + "]", pkt);
        if (pkt.session == this.iAPProcessor.getControlSessionId()) {
            this.controlMessages.add(pkt);
        } else {
            this.nonControlMessages.add(pkt);
        }
        this.myHandler.sendEmptyMessage(2);
    }

    public byte[] getLocalAddress() {
        return this.physicalLayer.getLocalAddress();
    }

    public String getRemoteAddress() {
        return this.remoteAddress;
    }

    public String getRemoteName() {
        return this.remoteName;
    }

    public void connectionStarted(String remoteAddress, String remoteName) {
        this.remoteAddress = remoteAddress;
        this.remoteName = remoteName;
        queuePacket(3, null);
    }

    public void connectionEnded() {
        if (this.myHandler != null) {
            this.myHandler.removeMessages(0);
            this.myHandler.removeMessages(1);
            this.myHandler.removeMessages(2);
        }
        this.controlMessages.clear();
        this.nonControlMessages.clear();
        queuePacket(4, null);
    }
}
