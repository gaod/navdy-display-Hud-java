package com.navdy.hud.mfi;

public interface IAPListener {
    void onCoprocessorStatusCheckFailed(String str, String str2, String str3);

    void onDeviceAuthenticationSuccess(int i);
}
