package com.navdy.hud.mfi;

import android.util.Log;
import android.util.SparseArray;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Locale;

public class Utils {
    public static final String MFI_TAG = "MFi";
    protected static final String TAG = "navdy-mfi-Utils";
    protected static final char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        return bytesToHex(bytes, false);
    }

    public static String bytesToHex(byte[] bytes, boolean spaces) {
        int charsPerByte = spaces ? 3 : 2;
        char[] hexChars = new char[(bytes.length * charsPerByte)];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 255;
            hexChars[j * charsPerByte] = hexArray[v >>> 4];
            hexChars[(j * charsPerByte) + 1] = hexArray[v & 15];
            if (spaces) {
                hexChars[(j * charsPerByte) + 2] = HereManeuverDisplayBuilder.SPACE_CHAR;
            }
        }
        return new String(hexChars);
    }

    public static String bytesToMacAddress(byte[] address) {
        return String.format(Locale.US, "%02X:%02X:%02X:%02X:%02X:%02X", new Object[]{Byte.valueOf(address[0]), Byte.valueOf(address[1]), Byte.valueOf(address[2]), Byte.valueOf(address[3]), Byte.valueOf(address[4]), Byte.valueOf(address[5])});
    }

    public static int nibbleToByte(byte n) {
        int b = n & 255;
        if (b <= 57) {
            return b - 48;
        }
        return (b - 65) + 10;
    }

    public static byte[] parseMACAddress(String addr) {
        byte[] d = addr.toUpperCase().getBytes();
        byte[] mac = new byte[6];
        for (int i = 0; i < 6; i++) {
            mac[i] = (byte) ((nibbleToByte(d[i * 3]) << 4) | nibbleToByte(d[(i * 3) + 1]));
        }
        return mac;
    }

    public static SparseArray<String> getConstantsMap(Class c) {
        return getConstantsMap(c, null);
    }

    public static SparseArray<String> getConstantsMap(Class c, String prefix) {
        SparseArray<String> constants = new SparseArray();
        for (Field field : c.getDeclaredFields()) {
            try {
                if (field.getType().equals(Integer.TYPE) && Modifier.isStatic(field.getModifiers()) && (prefix == null || field.getName().startsWith(prefix))) {
                    constants.put(field.getInt(null), field.getName());
                }
            } catch (IllegalAccessException e) {
                Log.e(TAG, "", e);
            }
        }
        return constants;
    }

    public static void logTransfer(String tag, String fmt, Object... args) {
        if (Log.isLoggable(tag, 3)) {
            boolean logPacketData = Log.isLoggable(tag, 2);
            for (int i = 0; i < args.length; i++) {
                if (args[i] instanceof byte[]) {
                    String bytesToHex;
                    byte[] data = (byte[]) args[i];
                    if (logPacketData) {
                        bytesToHex = bytesToHex(data, true);
                    } else {
                        bytesToHex = String.format("%d bytes", new Object[]{Integer.valueOf(data.length)});
                    }
                    args[i] = bytesToHex;
                }
            }
            Log.d(tag, String.format(fmt, args));
        }
    }

    public static byte[] intsToBytes(int[] ints) {
        byte[] bytes = new byte[ints.length];
        for (int i = 0; i < ints.length; i++) {
            bytes[i] = (byte) ints[i];
        }
        return bytes;
    }

    public static String toASCII(byte[] bytes) {
        String s = "";
        for (byte b : bytes) {
            int v = b & 255;
            StringBuilder append = new StringBuilder().append(s);
            char c = (v < 32 || v >= 128) ? '.' : (char) v;
            s = append.append(c).toString();
        }
        return s;
    }

    public static long unpackInt64(byte[] d, int offset) {
        return ByteBuffer.wrap(d, offset, d.length - offset).getLong();
    }

    public static long unpackUInt32(byte[] d, int offset) {
        return (((((long) (d[offset] & 255)) << 24) | (((long) (d[offset + 1] & 255)) << 16)) | (((long) (d[offset + 2] & 255)) << 8)) | ((long) (d[offset + 3] & 255));
    }

    public static int unpackUInt16(byte[] d, int offset) {
        return ((d[offset] & 255) << 8) | (d[offset + 1] & 255);
    }

    public static int unpackUInt8(byte[] d, int offset) {
        return d[offset] & 255;
    }

    public static byte[] packUInt16(int i) {
        return new byte[]{(byte) ((i >> 8) & 255), (byte) (i & 255)};
    }

    public static byte[] packUInt8(int b) {
        return new byte[]{(byte) (b & 255)};
    }

    public static BigInteger unpackUInt64(byte[] d, int offset) {
        return new BigInteger(new byte[]{(byte) (d[offset] & 255), (byte) (d[offset + 1] & 255), (byte) (d[offset + 2] & 255), (byte) (d[offset + 3] & 255), (byte) (d[offset + 4] & 255), (byte) (d[offset + 5] & 255), (byte) (d[offset + 6] & 255), (byte) (d[offset + 7] & 255)});
    }
}
