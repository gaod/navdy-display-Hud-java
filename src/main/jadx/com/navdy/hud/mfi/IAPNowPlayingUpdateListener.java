package com.navdy.hud.mfi;

public interface IAPNowPlayingUpdateListener {
    void onNowPlayingUpdate(NowPlayingUpdate nowPlayingUpdate);
}
