package com.navdy.hud.app.maps.notification;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.destinations.DestinationsManager;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.maps.here.HereRouteCache;
import com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo;
import com.navdy.hud.app.maps.here.HereRouteManager;
import com.navdy.hud.app.maps.util.RouteUtils;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout2.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Selection;
import com.navdy.hud.app.ui.component.HaloView;
import com.navdy.hud.app.ui.component.destination.DestinationParcelable;
import com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType;
import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView.Style;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.audio.CancelSpeechRequest;
import com.navdy.service.library.events.audio.SpeechRequest.Category;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.destination.Destination;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.navigation.NavigationRouteResponse.Builder;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.events.places.DestinationSelectedRequest;
import com.navdy.service.library.events.places.DestinationSelectedResponse;
import com.navdy.service.library.events.places.PlaceType;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class RouteCalculationNotification implements INotification {
    public static final RemoteEvent CANCEL_CALC_TTS = new RemoteEvent(new CancelSpeechRequest(ROUTE_CALC_TTS_ID));
    public static final RemoteEvent CANCEL_TBT_TTS = new RemoteEvent(new CancelSpeechRequest(ROUTE_TBT_TTS_ID));
    private static final float ICON_SCALE_FACTOR = 1.38f;
    private static final int NAV_LOOK_UP_TIMEOUT = ((int) TimeUnit.SECONDS.toMillis(10));
    public static final String ROUTE_CALC_TTS_ID = ("route_calc_" + UUID.randomUUID().toString());
    public static final String ROUTE_PICKER = "ROUTE_PICKER";
    public static final String ROUTE_TBT_TTS_ID = ("route_tbt_" + UUID.randomUUID().toString());
    private static final int TAG_CANCEL = 1;
    private static final int TAG_DISMISS = 3;
    private static final int TAG_RETRY = 4;
    private static final int TAG_ROUTES = 2;
    private static final int TIMEOUT = ((int) TimeUnit.SECONDS.toMillis(30));
    private static List<Choice> cancelRouteCalc = new ArrayList(1);
    public static final String fastestRoute = resources.getString(R.string.fastest_route);
    public static final int notifBkColor = resources.getColor(R.color.route_sel);
    private static final Resources resources = HudApplication.getAppContext().getResources();
    private static List<Choice> routeError = new ArrayList(2);
    private static final String routeFailed = resources.getString(R.string.route_failed);
    private static final Logger sLogger = new Logger(RouteCalculationNotification.class);
    public static final String shortestRoute = resources.getString(R.string.shortest_route);
    private static final int showRouteBkColor = resources.getColor(R.color.route_unsel);
    private static List<Choice> showRoutes = new ArrayList(2);
    private static final int startTripImageContainerSize = resources.getDimensionPixelSize(R.dimen.start_trip_image_container_size);
    private static final int startTripImageSize = resources.getDimensionPixelSize(R.dimen.start_trip_image_size);
    private static final int startTripLeftMargin = resources.getDimensionPixelSize(R.dimen.start_trip_left_margin);
    private static final int startTripTextLeftMargin = resources.getDimensionPixelSize(R.dimen.start_trip_text_left_margin);
    private Bus bus;
    private ChoiceLayout2 choiceLayoutView;
    private List<Choice> choices;
    private ViewGroup containerView;
    private INotificationController controller;
    private String destinationAddress;
    private String destinationDistance;
    private String destinationLabel;
    private boolean dismissedbyUser;
    private RouteCalculationEvent event;
    private Handler handler = new Handler(Looper.getMainLooper());
    private int icon;
    private int iconBkColor = 0;
    private IconColorImageView iconColorImageView;
    private InitialsImageView iconInitialsImageView;
    private int iconSide;
    private ImageView iconSpinnerView;
    private String initials;
    private IListener listener = new IListener() {
        public void executeItem(Selection selection) {
            if (RouteCalculationNotification.this.controller != null) {
                switch (selection.id) {
                    case 1:
                        RouteCalculationNotification.sLogger.v("cancel");
                        RouteCalculationNotification.this.dismissedbyUser = true;
                        AnalyticsSupport.recordRouteSelectionCancelled();
                        if (RouteCalculationNotification.this.lookupRequest == null) {
                            RouteCalculationNotification.sLogger.v("cancel route");
                            if (HereRouteManager.cancelActiveRouteRequest()) {
                                RouteCalculationNotification.sLogger.v("route calc was cancelled");
                            } else {
                                RouteCalculationNotification.sLogger.v("route calc was not cancelled, may be it is not running");
                                if (TextUtils.isEmpty(RouteCalculationNotification.this.routeId)) {
                                    RouteCalculationNotification.sLogger.v("route calc was not cancelled,no route id");
                                } else {
                                    RouteCalculationNotification.this.bus.post(new RemoteEvent(new Builder().requestId(RouteCalculationNotification.this.routeId).status(RequestStatus.REQUEST_CANCELLED).destination(new Coordinate.Builder().latitude(Double.valueOf(0.0d)).longitude(Double.valueOf(0.0d)).build()).build()));
                                    RouteCalculationNotification.sLogger.v("route calc event sent:" + RouteCalculationNotification.this.routeId);
                                }
                            }
                        }
                        RouteCalculationNotification.this.dismissNotification();
                        return;
                    case 2:
                        RouteCalculationNotification.sLogger.v("show route picker: choice");
                        RouteCalculationNotification.this.dismissedbyUser = true;
                        RouteCalculationNotification.this.showRoutePicker();
                        return;
                    case 3:
                        RouteCalculationNotification.sLogger.v("dismiss route");
                        RouteCalculationNotification.this.dismissedbyUser = true;
                        RouteCalculationNotification.this.dismissNotification();
                        return;
                    case 4:
                        RouteCalculationNotification.sLogger.v("retry");
                        RouteCalculationEvent event = RouteCalculationNotification.this.routeCalculationEventHandler.getCurrentRouteCalcEvent();
                        if (event == null || event.request == null) {
                            RouteCalculationNotification.sLogger.v("retry: request not found");
                            RouteCalculationNotification.this.dismissNotification();
                            return;
                        }
                        RouteCalculationNotification.this.bus.post(event.request);
                        RouteCalculationNotification.this.bus.post(new RemoteEvent(event.request));
                        return;
                    default:
                        return;
                }
            }
        }

        public void itemSelected(Selection selection) {
        }
    };
    private ObjectAnimator loadingAnimator;
    private Destination lookupDestination;
    private DestinationSelectedRequest lookupRequest;
    private int notifColor = 0;
    private boolean registered;
    private RouteCalculationEventHandler routeCalculationEventHandler;
    private String routeId;
    private TextView routeTtaView;
    private long startTime;
    private String subTitle;
    private TextView subTitleView;
    private Runnable timeoutRunnable = new Runnable() {
        public void run() {
            RouteCalculationNotification.sLogger.v("timeoutRunnable");
            RouteCalculationNotification.this.dismissNotification();
        }
    };
    private String title;
    private TextView titleView;
    private SpannableStringBuilder tripDuration;
    private String tts;
    private boolean ttsPlayed;
    private UIStateManager uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
    private Runnable waitForNavLookupRunnable = new Runnable() {
        public void run() {
            if (RouteCalculationNotification.this.controller != null && RouteCalculationNotification.this.lookupRequest != null) {
                RouteCalculationNotification.sLogger.v("nav lookup expired");
                RouteCalculationNotification.this.lookupRequest = null;
                RouteCalculationNotification.this.launchOriginalDestination();
            }
        }
    };

    static {
        int dismissColor = resources.getColor(R.color.glance_dismiss);
        int retryColor = resources.getColor(R.color.glance_ok_blue);
        int moreColor = resources.getColor(R.color.glance_ok_blue);
        int goColor = resources.getColor(R.color.glance_ok_go);
        String moreRoutes = resources.getString(R.string.more_routes);
        cancelRouteCalc.add(new Choice(1, R.drawable.icon_glances_dismiss, dismissColor, R.drawable.icon_glances_dismiss, -16777216, resources.getString(R.string.cancel), dismissColor));
        showRoutes.add(new Choice(2, R.drawable.icon_glances_read, moreColor, R.drawable.icon_glances_read, -16777216, moreRoutes, moreColor));
        showRoutes.add(new Choice(3, R.drawable.icon_glances_ok_strong, goColor, R.drawable.icon_glances_ok_strong, -16777216, resources.getString(R.string.route_go), goColor));
        routeError.add(new Choice(4, R.drawable.icon_glances_retry, retryColor, R.drawable.icon_glances_retry, -16777216, resources.getString(R.string.retry), retryColor));
        routeError.add(new Choice(3, R.drawable.icon_glances_dismiss, dismissColor, R.drawable.icon_glances_dismiss, -16777216, resources.getString(R.string.dismiss), dismissColor));
    }

    public RouteCalculationNotification(RouteCalculationEventHandler routeCalculationEventHandler, Bus bus) {
        this.routeCalculationEventHandler = routeCalculationEventHandler;
        this.bus = bus;
    }

    public NotificationType getType() {
        return NotificationType.ROUTE_CALC;
    }

    public String getId() {
        return NotificationId.ROUTE_CALC_NOTIFICATION_ID;
    }

    public View getView(Context context) {
        if (this.containerView == null) {
            this.containerView = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.notification_route_calc, null);
            this.titleView = (TextView) this.containerView.findViewById(R.id.title);
            this.subTitleView = (TextView) this.containerView.findViewById(R.id.subTitle);
            this.iconColorImageView = (IconColorImageView) this.containerView.findViewById(R.id.iconColorView);
            this.iconInitialsImageView = (InitialsImageView) this.containerView.findViewById(R.id.iconInitialsView);
            this.iconSpinnerView = (ImageView) this.containerView.findViewById(R.id.iconSpinner);
            this.choiceLayoutView = (ChoiceLayout2) this.containerView.findViewById(R.id.choiceLayout);
            this.routeTtaView = (TextView) this.containerView.findViewById(R.id.routeTtaView);
        }
        return this.containerView;
    }

    public View getExpandedView(Context context, Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(INotificationController controller) {
        sLogger.v("onStart");
        this.controller = controller;
        this.handler.removeCallbacks(this.timeoutRunnable);
        if (!this.registered) {
            this.bus.register(this);
            this.registered = true;
        }
        setUI();
    }

    public void onUpdate() {
    }

    public void onStop() {
        sLogger.v("onStop");
        hideStartTrip();
        stopLoadingAnimation();
        this.handler.removeCallbacks(this.waitForNavLookupRunnable);
        this.controller = null;
        this.ttsPlayed = false;
        if (this.choices == cancelRouteCalc) {
            sLogger.v("cancel tts");
            this.bus.post(CANCEL_CALC_TTS);
        }
        if ((this.choices == showRoutes || this.choices == routeError) && isAlive()) {
            this.handler.removeCallbacks(this.timeoutRunnable);
            this.handler.postDelayed(this.timeoutRunnable, (long) TIMEOUT);
            sLogger.v("expire notif in " + TIMEOUT);
        }
        if (this.registered) {
            this.registered = false;
            this.bus.unregister(this);
        }
    }

    public int getTimeout() {
        if (this.choices == cancelRouteCalc) {
            return 0;
        }
        if (this.choices == routeError) {
            return TIMEOUT;
        }
        if (this.choices == showRoutes) {
            return TIMEOUT;
        }
        return 0;
    }

    public boolean isAlive() {
        boolean alive = false;
        String reason = null;
        if (this.choices == cancelRouteCalc) {
            alive = this.dismissedbyUser ? false : this.routeCalculationEventHandler.getCurrentRouteCalcEvent() != null;
            reason = "cancelRouteCalc";
        } else if (this.choices == routeError) {
            alive = false;
            reason = "routeError";
        } else if (this.choices == showRoutes) {
            if (this.dismissedbyUser) {
                alive = false;
            } else {
                boolean timedOut;
                long diff = SystemClock.elapsedRealtime() - this.startTime;
                if (diff >= ((long) TIMEOUT)) {
                    timedOut = true;
                } else {
                    timedOut = false;
                }
                if (this.routeCalculationEventHandler.getCurrentRouteCalcEvent() == null || timedOut) {
                    alive = false;
                } else {
                    alive = true;
                }
                sLogger.v("timedOut=" + diff + " = " + timedOut);
            }
            reason = "showRoutes";
        }
        sLogger.v("alive=" + alive + " type=" + reason);
        return alive;
    }

    public boolean isPurgeable() {
        return !isAlive();
    }

    public boolean canAddToStackIfCurrentExists() {
        return true;
    }

    public int getColor() {
        return notifBkColor;
    }

    public void onNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    public boolean supportScroll() {
        return false;
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(GestureEvent event) {
        if (this.controller == null || this.choices != showRoutes) {
            return false;
        }
        switch (event.gesture) {
            case GESTURE_SWIPE_LEFT:
                sLogger.v("show route picker:gesture");
                this.dismissedbyUser = true;
                showRoutePicker();
                return true;
            default:
                return false;
        }
    }

    public boolean onKey(CustomKeyEvent event) {
        if (this.controller == null) {
            return false;
        }
        switch (event) {
            case LEFT:
                this.controller.resetTimeout();
                this.choiceLayoutView.moveSelectionLeft();
                return true;
            case RIGHT:
                this.controller.resetTimeout();
                this.choiceLayoutView.moveSelectionRight();
                return true;
            case SELECT:
                this.choiceLayoutView.executeSelectedItem();
                return true;
            default:
                return false;
        }
    }

    public IInputHandler nextHandler() {
        return null;
    }

    private void dismissNotification() {
        this.routeCalculationEventHandler.clearCurrentRouteCalcEvent();
        NotificationManager.getInstance().removeNotification(NotificationId.ROUTE_CALC_NOTIFICATION_ID);
    }

    private void setUI() {
        this.startTime = SystemClock.elapsedRealtime();
        this.titleView.setText(this.title);
        this.subTitleView.setText(this.subTitle);
        this.iconSpinnerView.setImageResource(this.iconSide);
        int selection = 0;
        if (this.choices == cancelRouteCalc) {
            this.routeTtaView.setVisibility(8);
            setIcon();
            startLoadingAnimation();
            if (!(this.tts == null || this.ttsPlayed)) {
                playTts(this.tts);
                this.tts = null;
                this.ttsPlayed = true;
            }
            if (this.lookupDestination != null) {
                this.lookupRequest = new DestinationSelectedRequest.Builder().request_id(UUID.randomUUID().toString()).destination(this.lookupDestination).build();
                this.handler.removeCallbacks(this.waitForNavLookupRunnable);
                this.handler.postDelayed(this.waitForNavLookupRunnable, (long) NAV_LOOK_UP_TIMEOUT);
                this.bus.post(new RemoteEvent(this.lookupRequest));
                sLogger.v("launched nav lookup request:" + this.lookupRequest.request_id + " dest:" + this.lookupDestination + " placeid:" + this.lookupDestination.place_id);
            }
        } else if (this.choices == showRoutes) {
            selection = 1;
            setIcon();
            stopLoadingAnimation();
            this.routeTtaView.setText(this.tripDuration);
            this.routeTtaView.setVisibility(0);
        } else if (this.choices == routeError) {
            this.routeTtaView.setVisibility(8);
            setIcon();
            stopLoadingAnimation();
        }
        this.choiceLayoutView.setChoices(this.choices, selection, this.listener);
        this.controller.startTimeout(getTimeout());
    }

    private void setIcon() {
        if (this.iconBkColor != 0) {
            this.iconColorImageView.setIcon(this.icon, this.iconBkColor, null, ICON_SCALE_FACTOR);
            this.iconColorImageView.setVisibility(0);
            this.iconInitialsImageView.setVisibility(8);
            return;
        }
        this.iconInitialsImageView.setImage(this.icon, this.initials, Style.LARGE);
        this.iconInitialsImageView.setVisibility(0);
        this.iconColorImageView.setVisibility(8);
    }

    private void startLoadingAnimation() {
        if (this.loadingAnimator == null) {
            this.loadingAnimator = ObjectAnimator.ofFloat(this.iconSpinnerView, View.ROTATION, new float[]{360.0f});
            this.loadingAnimator.setDuration(500);
            this.loadingAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
            this.loadingAnimator.addListener(new DefaultAnimationListener() {
                public void onAnimationEnd(Animator animation) {
                    if (RouteCalculationNotification.this.loadingAnimator != null) {
                        RouteCalculationNotification.this.loadingAnimator.setStartDelay(33);
                        RouteCalculationNotification.this.loadingAnimator.start();
                        return;
                    }
                    RouteCalculationNotification.sLogger.v("abandon loading animation");
                }
            });
        }
        if (!this.loadingAnimator.isRunning()) {
            sLogger.v("started loading animation");
            this.loadingAnimator.start();
        }
    }

    private void stopLoadingAnimation() {
        if (this.loadingAnimator != null) {
            sLogger.v("cancelled loading animation");
            this.loadingAnimator.removeAllListeners();
            this.loadingAnimator.cancel();
            this.iconSpinnerView.setRotation(0.0f);
            this.loadingAnimator = null;
        }
    }

    public void showRouteSearch(String title, String subTitle, int icon, int iconBkColor, int notifColor, String initials, String tts, Destination lookupDestination, String routeId, String destinationLabel, String destinationAddress, String destinationDistance) {
        sLogger.v("showRouteSearch: navlookup:" + (lookupDestination != null));
        boolean navLookupTransition = false;
        if (this.lookupDestination != null && TextUtils.equals(this.title, title)) {
            navLookupTransition = true;
            sLogger.v("showRouteSearch: nav lookup transition");
        }
        this.title = title;
        this.subTitle = subTitle;
        this.routeId = routeId;
        if (!navLookupTransition) {
            this.icon = icon;
            this.iconBkColor = iconBkColor;
            this.notifColor = notifColor;
            this.initials = initials;
        }
        this.choices = cancelRouteCalc;
        this.iconSide = R.drawable.loader_circle;
        this.event = null;
        this.tts = tts;
        this.lookupDestination = lookupDestination;
        this.destinationLabel = destinationLabel;
        this.destinationAddress = destinationAddress;
        this.destinationDistance = destinationDistance;
        if (this.controller != null) {
            setUI();
        }
    }

    public void showError() {
        this.title = routeFailed;
        this.choices = routeError;
        this.iconSide = R.drawable.icon_badge_alert;
        this.event = null;
        this.tts = null;
        if (this.controller != null) {
            setUI();
        }
    }

    public void showRoutes(RouteCalculationEvent event) {
        switch (event.routeOptions.getRouteType()) {
            case FASTEST:
                this.title = fastestRoute;
                break;
            case SHORTEST:
                this.title = shortestRoute;
                break;
        }
        this.subTitle = resources.getString(R.string.via_desc, new Object[]{((NavigationRouteResult) event.response.results.get(0)).via});
        this.choices = showRoutes;
        this.icon = 0;
        this.iconBkColor = showRouteBkColor;
        this.iconSide = R.drawable.icon_route;
        this.event = event;
        this.tripDuration = null;
        this.tts = null;
        NavigationRouteResult result = (NavigationRouteResult) event.response.results.get(0);
        RouteInfo routeInfo = HereRouteCache.getInstance().getRoute(result.routeId);
        if (routeInfo != null) {
            this.tripDuration = HereMapUtil.getEtaString(routeInfo.route, result.routeId, false);
        } else {
            this.tripDuration = new SpannableStringBuilder();
        }
        if (this.controller != null) {
            setUI();
        }
    }

    private void showRoutePicker() {
        if (this.event == null || this.event.response == null || this.event.request == null) {
            sLogger.v("showRoutePicker invalid state");
            dismissNotification();
            return;
        }
        Bundle args = new Bundle();
        args.putString(DestinationPickerScreen.PICKER_LEFT_TITLE, resources.getString(R.string.routes));
        args.putInt(DestinationPickerScreen.PICKER_LEFT_ICON, R.drawable.icon_route);
        args.putInt(DestinationPickerScreen.PICKER_LEFT_ICON_BKCOLOR, 0);
        args.putString(DestinationPickerScreen.PICKER_TITLE, resources.getString(R.string.pick_route));
        args.putBoolean(DestinationPickerScreen.PICKER_HIDE_IF_NAV_STOPS, true);
        args.putBoolean(ROUTE_PICKER, true);
        args.putBoolean(DestinationPickerScreen.PICKER_SHOW_ROUTE_MAP, true);
        GeoCoordinate geoCoordinate = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
        args.putDouble(DestinationPickerScreen.PICKER_MAP_START_LAT, geoCoordinate.getLatitude());
        args.putDouble(DestinationPickerScreen.PICKER_MAP_START_LNG, geoCoordinate.getLongitude());
        args.putDouble(DestinationPickerScreen.PICKER_MAP_END_LAT, this.event.request.destination.latitude.doubleValue());
        args.putDouble(DestinationPickerScreen.PICKER_MAP_END_LNG, this.event.request.destination.longitude.doubleValue());
        int len = this.event.response.results.size();
        DestinationParcelable[] destinationParcelables = new DestinationParcelable[len];
        double displayLat = 0.0d;
        double displayLng = 0.0d;
        double navLat = 0.0d;
        double navLng = 0.0d;
        if (this.event.request.destination != null) {
            navLat = this.event.request.destination.latitude.doubleValue();
            navLng = this.event.request.destination.longitude.doubleValue();
        }
        if (this.event.request.destinationDisplay != null) {
            displayLat = this.event.request.destinationDisplay.latitude.doubleValue();
            displayLng = this.event.request.destinationDisplay.longitude.doubleValue();
        }
        StringBuilder builder = new StringBuilder();
        HereRouteCache hereRouteCache = HereRouteCache.getInstance();
        TimeHelper timeHelper = RemoteDeviceManager.getInstance().getTimeHelper();
        ManeuverDisplay maneuverDisplay = new ManeuverDisplay();
        Destination requestDestination = this.event.request.requestDestination;
        for (int i = 0; i < len; i++) {
            PlaceType placeType;
            String time = "";
            String etaStr = "";
            NavigationRouteResult result = (NavigationRouteResult) this.event.response.results.get(i);
            HereManeuverDisplayBuilder.setNavigationDistance((long) result.length.intValue(), maneuverDisplay, false, true);
            RouteInfo routeInfo = hereRouteCache.getRoute(result.routeId);
            if (routeInfo != null) {
                Date ttaDate = HereMapUtil.getRouteTtaDate(routeInfo.route);
                if (ttaDate != null) {
                    etaStr = timeHelper.formatTime12Hour(ttaDate, builder, false) + " " + builder.toString();
                    long duration = ttaDate.getTime() - System.currentTimeMillis();
                    if (duration < 0) {
                        duration = 0;
                    } else {
                        duration = TimeUnit.MILLISECONDS.toMinutes(duration);
                    }
                    time = RouteUtils.formatEtaMinutes(resources, (int) duration);
                }
            }
            String title = resources.getString(R.string.route_title, new Object[]{time, result.via});
            String subTitle = maneuverDisplay.distanceToPendingRoadText + " " + etaStr;
            String str = result.address;
            int i2 = notifBkColor;
            DestinationType destinationType = DestinationType.DESTINATION;
            if (requestDestination != null) {
                placeType = requestDestination.place_type;
            } else {
                placeType = null;
            }
            destinationParcelables[i] = new DestinationParcelable(0, title, subTitle, false, null, false, str, navLat, navLng, displayLat, displayLng, R.drawable.icon_route, R.drawable.icon_route_sm, i2, 0, destinationType, placeType);
            destinationParcelables[i].setRouteId(((NavigationRouteResult) this.event.response.results.get(i)).routeId);
            if (requestDestination != null) {
                destinationParcelables[i].setIdentifier(requestDestination.identifier);
                destinationParcelables[i].setPlaceId(requestDestination.place_id);
            }
            builder.setLength(0);
        }
        args.putParcelableArray(DestinationPickerScreen.PICKER_DESTINATIONS, destinationParcelables);
        NotificationManager.getInstance().removeNotification(NotificationId.ROUTE_CALC_NOTIFICATION_ID, true, Screen.SCREEN_DESTINATION_PICKER, args, new RouteCalcPickerHandler(this.event));
        this.event = null;
    }

    private void playTts(String tts) {
        sLogger.v("tts[" + tts + "]");
        if (HereNavigationManager.getInstance().getNavigationSessionPreference().spokenTurnByTurn) {
            this.bus.post(CANCEL_CALC_TTS);
            TTSUtils.sendSpeechRequest(tts, Category.SPEECH_TURN_BY_TURN, ROUTE_CALC_TTS_ID);
        }
    }

    @Subscribe
    public void onDestinationLookup(DestinationSelectedResponse response) {
        if (this.controller != null) {
            if (this.lookupRequest == null) {
                sLogger.v("receive destination lookup:" + response.request_id + " , not in lookup mode, ignore");
            } else if (TextUtils.equals(response.request_id, this.lookupRequest.request_id)) {
                if (response.request_status == null || response.request_status != RequestStatus.REQUEST_SUCCESS) {
                    sLogger.v("nav lookup failed:" + response.request_status);
                    launchOriginalDestination();
                } else {
                    sLogger.v("nav lookup suceeded:" + response.destination);
                    DestinationsManager.getInstance().requestNavigation(DestinationsManager.getInstance().transformToInternalDestination(response.destination));
                }
                this.lookupRequest = null;
            } else {
                sLogger.v("receive destination lookup, mismatch id recv[" + response.request_id + "] lookup[" + this.lookupRequest.request_id + "]");
            }
        }
    }

    @Subscribe
    public void onConnectionStateChange(ConnectionStateChange event) {
        if (this.controller != null) {
            switch (event.state) {
                case CONNECTION_DISCONNECTED:
                    if (this.lookupRequest != null) {
                        sLogger.v("nav lookup device disconnected");
                        this.lookupRequest = null;
                        launchOriginalDestination();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    private void launchOriginalDestination() {
        if (this.lookupDestination != null) {
            com.navdy.hud.app.framework.destinations.Destination d = DestinationsManager.getInstance().transformToInternalDestination(this.lookupDestination);
            DestinationsManager.getInstance().requestNavigation(d);
            this.lookupDestination = null;
            sLogger.v("original destination launched:" + d);
            return;
        }
        sLogger.v("original destination not launched");
    }

    public void showStartTrip() {
        if (this.controller == null) {
            sLogger.v("showStartTrip:null");
            return;
        }
        Main main = RemoteDeviceManager.getInstance().getUiStateManager().getRootScreen();
        if (main != null) {
            Context context = this.controller.getUIContext();
            View lyt = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.vlist_item, null);
            HaloView haloView = (HaloView) lyt.findViewById(R.id.halo);
            haloView.setVisibility(8);
            MarginLayoutParams lytParams = (MarginLayoutParams) ((ViewGroup) lyt.findViewById(R.id.imageContainer)).getLayoutParams();
            lytParams.width = startTripImageContainerSize;
            lytParams.height = startTripImageContainerSize;
            lytParams.leftMargin = startTripLeftMargin;
            ViewGroup iconContainer = (ViewGroup) lyt.findViewById(R.id.iconContainer);
            lytParams = (MarginLayoutParams) iconContainer.getLayoutParams();
            lytParams.width = startTripImageSize;
            lytParams.height = startTripImageSize;
            ((MarginLayoutParams) ((ViewGroup) lyt.findViewById(R.id.textContainer)).getLayoutParams()).leftMargin = startTripTextLeftMargin;
            TextView startTripTitle = (TextView) lyt.findViewById(R.id.title);
            TextView startTripSubTitle = (TextView) lyt.findViewById(R.id.subTitle);
            TextView startTripSubTitle2 = (TextView) lyt.findViewById(R.id.subTitle2);
            Model model = IconBkColorViewHolder.buildModel(0, 0, 0, 0, 0, this.destinationLabel, this.destinationAddress, this.destinationDistance, null);
            VerticalList.setFontSize(model, false);
            ((MarginLayoutParams) startTripTitle.getLayoutParams()).topMargin = (int) model.fontInfo.titleFontTopMargin;
            ((MarginLayoutParams) startTripSubTitle.getLayoutParams()).topMargin = (int) model.fontInfo.subTitleFontTopMargin;
            ((MarginLayoutParams) startTripSubTitle2.getLayoutParams()).topMargin = (int) model.fontInfo.subTitle2FontTopMargin;
            startTripTitle.setTextSize(model.fontInfo.titleFontSize);
            startTripTitle.setSingleLine(true);
            startTripTitle.setText(this.destinationLabel);
            if (TextUtils.isEmpty(this.destinationAddress)) {
                startTripSubTitle.setVisibility(8);
            } else {
                startTripSubTitle.setTextSize(model.fontInfo.subTitleFontSize);
                startTripSubTitle.setText(this.destinationAddress);
            }
            if (TextUtils.isEmpty(this.destinationDistance)) {
                startTripSubTitle2.setVisibility(8);
            } else {
                startTripSubTitle2.setTextSize(model.fontInfo.subTitle2FontSize);
                startTripSubTitle2.setText(this.destinationDistance);
                startTripSubTitle2.setVisibility(0);
            }
            if (this.iconBkColor != 0) {
                IconColorImageView imageView = new IconColorImageView(context);
                iconContainer.addView(imageView);
                haloView.setStrokeColor(Color.argb(IconBaseViewHolder.FLUCTUATOR_OPACITY_ALPHA, Color.red(this.iconBkColor), Color.green(this.iconBkColor), Color.blue(this.iconBkColor)));
                imageView.setIcon(this.icon, this.iconBkColor, null, 0.83f);
            } else {
                InitialsImageView imageView2 = new InitialsImageView(context);
                iconContainer.addView(imageView2);
                haloView.setStrokeColor(Color.argb(IconBaseViewHolder.FLUCTUATOR_OPACITY_ALPHA, Color.red(this.notifColor), Color.green(this.notifColor), Color.blue(this.notifColor)));
                imageView2.setImage(this.icon, this.initials, Style.MEDIUM);
            }
            main.injectMainLowerView(lyt);
        }
    }

    public void hideStartTrip() {
        Main main = this.uiStateManager.getRootScreen();
        if (main != null) {
            main.ejectMainLowerView();
        }
    }

    public boolean isStarting() {
        return this.choices == cancelRouteCalc;
    }
}
