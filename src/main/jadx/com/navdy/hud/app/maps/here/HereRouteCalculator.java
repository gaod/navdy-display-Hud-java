package com.navdy.hud.app.maps.here;

import android.os.SystemClock;
import android.text.TextUtils;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.routing.CoreRouter;
import com.here.android.mpa.routing.CoreRouter.Connectivity;
import com.here.android.mpa.routing.CoreRouter.Listener;
import com.here.android.mpa.routing.DynamicPenalty;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.Route.TrafficPenaltyMode;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;
import com.here.android.mpa.routing.RouteResult.ViolatedOption;
import com.here.android.mpa.routing.RouteWaypoint;
import com.here.android.mpa.routing.RoutingError;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.maps.MapSettings;
import com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo;
import com.navdy.hud.app.maps.util.RouteUtils;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class HereRouteCalculator {
    private static AtomicLong ID = new AtomicLong(1);
    private static final int PROGRESS_INTERVAL_MS = 200;
    private CoreRouter coreRouter;
    private Logger logger;
    private String tag = ("[" + ID.getAndIncrement() + "]");
    private boolean verbose;

    public interface RouteCalculatorListener {
        void error(RoutingError routingError, Throwable th);

        void postSuccess(ArrayList<NavigationRouteResult> arrayList);

        void preSuccess();

        void progress(int i);
    }

    public HereRouteCalculator(Logger logger, boolean verbose) {
        this.logger = logger;
        this.verbose = verbose;
    }

    public void calculateRoute(NavigationRouteRequest request, GeoCoordinate startPoint, List<GeoCoordinate> waypoints, GeoCoordinate endPoint, boolean factoringInTraffic, RouteCalculatorListener listener, int maxRoutes, RouteOptions routeOptions, boolean allowCancellation) {
        calculateRoute(request, startPoint, waypoints, endPoint, factoringInTraffic, listener, maxRoutes, routeOptions, true, true, allowCancellation);
    }

    public void calculateRoute(NavigationRouteRequest request, GeoCoordinate startPoint, List<GeoCoordinate> waypoints, GeoCoordinate endPoint, boolean factoringInTraffic, RouteCalculatorListener listener, int maxRoutes, RouteOptions routeOptions, boolean cache, boolean calculatePolyline, boolean allowCancellation) {
        this.coreRouter = new CoreRouter();
        RoutePlan routePlan = new RoutePlan();
        routeOptions.setRouteCount(maxRoutes);
        routePlan.setRouteOptions(routeOptions);
        routePlan.addWaypoint(new RouteWaypoint(startPoint));
        if (waypoints != null) {
            for (GeoCoordinate waypoint : waypoints) {
                routePlan.addWaypoint(new RouteWaypoint(waypoint));
            }
        }
        routePlan.addWaypoint(new RouteWaypoint(endPoint));
        DynamicPenalty dynamicPenalty = new DynamicPenalty();
        if (factoringInTraffic) {
            this.logger.v(this.tag + "Factoring traffic in route calc[ONLINE]");
            dynamicPenalty.setTrafficPenaltyMode(TrafficPenaltyMode.OPTIMAL);
            this.coreRouter.setConnectivity(Connectivity.ONLINE);
        } else {
            this.logger.v(this.tag + "Not factoring traffic in route calc[OFFLINE]");
            dynamicPenalty.setTrafficPenaltyMode(TrafficPenaltyMode.DISABLED);
            this.coreRouter.setConnectivity(Connectivity.OFFLINE);
        }
        this.coreRouter.setDynamicPenalty(dynamicPenalty);
        this.logger.i(this.tag + "Generating route with " + routePlan.getWaypointCount() + " waypoints");
        final long startTime = SystemClock.elapsedRealtime();
        final RouteCalculatorListener routeCalculatorListener = listener;
        final boolean z = allowCancellation;
        final NavigationRouteRequest navigationRouteRequest = request;
        final boolean z2 = calculatePolyline;
        final boolean z3 = cache;
        final boolean z4 = factoringInTraffic;
        final GeoCoordinate geoCoordinate = startPoint;
        this.coreRouter.calculateRoute(routePlan, new Listener() {
            private int lastPercentage = -1;
            private long lastProgress = 0;

            public void onCalculateRouteFinished(final List<RouteResult> results, final RoutingError routingError) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        try {
                            long l2 = SystemClock.elapsedRealtime();
                            if ((routingError == RoutingError.NONE || routingError == RoutingError.VIOLATES_OPTIONS) && results.size() > 0) {
                                routeCalculatorListener.preSuccess();
                                ArrayList<NavigationRouteResult> arrayList = new ArrayList(results.size());
                                String requestId = null;
                                if (z && navigationRouteRequest != null) {
                                    requestId = navigationRouteRequest.requestId;
                                }
                                String[] viaList = HereRouteViaGenerator.generateVia(results, requestId);
                                int index = 0;
                                Iterator it = results.iterator();
                                while (true) {
                                    int index2 = index;
                                    if (it.hasNext()) {
                                        RouteResult routeResult = (RouteResult) it.next();
                                        if (!z || !HereRouteCalculator.this.isRouteCancelled(navigationRouteRequest)) {
                                            NavigationRouteResult outgoingRouteResult;
                                            Route route = routeResult.getRoute();
                                            String id = UUID.randomUUID().toString();
                                            index = index2 + 1;
                                            String via = viaList[index2];
                                            if (TextUtils.isEmpty(via)) {
                                                via = HudApplication.getAppContext().getResources().getString(R.string.route_num, new Object[]{Integer.valueOf(index)});
                                            }
                                            EnumSet<ViolatedOption> violatedOptions = routeResult.getViolatedOptions();
                                            if (violatedOptions != null && violatedOptions.size() > 0) {
                                                Iterator it2 = violatedOptions.iterator();
                                                while (it2.hasNext()) {
                                                    HereRouteCalculator.this.logger.v(HereRouteCalculator.this.tag + " violated options [" + ((ViolatedOption) it2.next()).name() + "]");
                                                }
                                            }
                                            if (navigationRouteRequest != null) {
                                                outgoingRouteResult = HereRouteCalculator.this.getRouteResultFromRoute(id, route, via, navigationRouteRequest.label, navigationRouteRequest.streetAddress, z2);
                                            } else {
                                                outgoingRouteResult = HereRouteCalculator.this.getRouteResultFromRoute(id, route, via, null, null, z2);
                                            }
                                            arrayList.add(outgoingRouteResult);
                                            if (z3) {
                                                HereRouteCache.getInstance().addRoute(id, new RouteInfo(route, navigationRouteRequest, outgoingRouteResult, z4, geoCoordinate));
                                            }
                                            if (HereRouteCalculator.this.logger.isLoggable(2)) {
                                                HereMapUtil.printRouteDetails(route, navigationRouteRequest == null ? "" : navigationRouteRequest.streetAddress, navigationRouteRequest, null);
                                            }
                                            if (MapSettings.isGenerateRouteIcons()) {
                                                HereMapUtil.generateRouteIcons(id, navigationRouteRequest.label, route);
                                            }
                                        } else {
                                            return;
                                        }
                                    } else if (!z || !HereRouteCalculator.this.isRouteCancelled(navigationRouteRequest)) {
                                        routeCalculatorListener.postSuccess(arrayList);
                                    } else {
                                        return;
                                    }
                                }
                            }
                            int resultsSize = -1;
                            if (results != null) {
                                resultsSize = results.size();
                            }
                            HereRouteCalculator.this.logger.e(HereRouteCalculator.this.tag + "got error:" + routingError + " results=" + resultsSize);
                            routeCalculatorListener.error(routingError, null);
                            HereRouteCalculator.this.logger.v(HereRouteCalculator.this.tag + "handleSearchRequest- time-2=" + (SystemClock.elapsedRealtime() - l2));
                        } catch (Throwable t) {
                            HereRouteCalculator.this.logger.e(HereRouteCalculator.this.tag, t);
                            routeCalculatorListener.error(null, t);
                        }
                    }
                }, 2);
            }

            public void onProgress(int percentage) {
                long now = SystemClock.elapsedRealtime();
                if (percentage == this.lastPercentage) {
                    return;
                }
                if (now > this.lastProgress + 200 || percentage == 100) {
                    this.lastPercentage = percentage;
                    this.lastProgress = now;
                    HereRouteCalculator.this.logger.i(HereRouteCalculator.this.tag + "route calculation: [" + percentage + "%] done timeElapsed:" + (now - startTime));
                    routeCalculatorListener.progress(percentage);
                }
            }
        });
    }

    public NavigationRouteResult getRouteResultFromRoute(String routeId, Route route, String via, String label, String address, boolean calculatePolyline) {
        List<Float> simplified = null;
        List<Coordinate> oldList = new ArrayList();
        if (calculatePolyline) {
            long t1 = SystemClock.elapsedRealtime();
            List<GeoCoordinate> routeGeometry = route.getRouteGeometry();
            this.logger.v(this.tag + "getting geometry took " + (SystemClock.elapsedRealtime() - t1) + " ms");
            this.logger.v(this.tag + "points before simplification: " + routeGeometry.size());
            List<Float> simplified2 = RouteUtils.simplify(routeGeometry);
            this.logger.v(this.tag + "points after simplification: " + (simplified2.size() / 2));
            Coordinate first = new Coordinate(Double.valueOf(((GeoCoordinate) routeGeometry.get(0)).getLatitude()), Double.valueOf(((GeoCoordinate) routeGeometry.get(0)).getLongitude()), null, null, null, null, null, null);
            oldList = new ArrayList();
            oldList.add(first);
            simplified = simplified2;
        } else {
            this.logger.v(this.tag + " poly line calc off");
        }
        int trafficDuration = route.getTta(TrafficPenaltyMode.OPTIMAL, 268435455).getDuration();
        int length = route.getLength();
        int freeFlowDuration = route.getTta(TrafficPenaltyMode.DISABLED, 268435455).getDuration();
        NavigationRouteResult outgoingRouteResult = new NavigationRouteResult(routeId, label, oldList, Integer.valueOf(length), Integer.valueOf(freeFlowDuration), Integer.valueOf(trafficDuration), simplified, via, address);
        this.logger.v(this.tag + "route id[" + routeId + "] via[" + via + "] length[" + length + "] duration[" + trafficDuration + "] freeFlowDuration[" + freeFlowDuration + "]");
        return outgoingRouteResult;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void cancel() {
        try {
            if (this.coreRouter != null) {
                this.coreRouter.cancel();
            }
            this.coreRouter = null;
        } catch (Throwable th) {
            this.coreRouter = null;
        }
    }

    private boolean isRouteCancelled(NavigationRouteRequest request) {
        if (request == null) {
            return false;
        }
        String activeRouteCalcId = HereRouteManager.getActiveRouteCalcId();
        if (TextUtils.equals(request.requestId, activeRouteCalcId)) {
            return false;
        }
        this.logger.v("route request [" + request.requestId + "] is not active anymore, current [" + activeRouteCalcId + "]");
        return true;
    }
}
