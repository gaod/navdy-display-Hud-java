package com.navdy.hud.app.maps.util;

import android.content.res.Resources;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.List;

public class RouteUtils {
    private static final double SCALE_FACTOR = 1048576.0d;
    private static final Logger sLogger = new Logger(RouteUtils.class);

    public static final class Bounds {
        public double height;
        public double width;

        Bounds(double width, double height) {
            this.width = width;
            this.height = height;
        }
    }

    public static List<Float> simplify(List<GeoCoordinate> points) {
        Bounds bounds = getLatLngBounds(points);
        return simplify(points, 2000.0d * Math.max(bounds.width, bounds.height));
    }

    public static List<Float> simplify(List<GeoCoordinate> points, double tolerance) {
        return simplifyRadialDistance(points, tolerance * tolerance);
    }

    static List<Float> simplifyRadialDistance(List<GeoCoordinate> points, double sqTolerance) {
        if (points == null || points.size() == 0) {
            return null;
        }
        float prevLatitude = (float) ((GeoCoordinate) points.get(0)).getLatitude();
        float prevLongitude = (float) ((GeoCoordinate) points.get(0)).getLongitude();
        List<Float> newLatLongs = new ArrayList();
        newLatLongs.add(Float.valueOf(prevLatitude));
        newLatLongs.add(Float.valueOf(prevLongitude));
        int len = points.size();
        GeoCoordinate geo = (GeoCoordinate) points.get(0);
        float pointLongitude = (float) geo.getLongitude();
        newLatLongs.add(Float.valueOf((float) geo.getLatitude()));
        newLatLongs.add(Float.valueOf(pointLongitude));
        for (int i = 1; i < len - 1; i++) {
            geo = (GeoCoordinate) points.get(i);
            float pointLatitude = (float) geo.getLatitude();
            pointLongitude = (float) geo.getLongitude();
            double dx = ((double) (pointLatitude - prevLatitude)) * SCALE_FACTOR;
            double dy = ((double) (pointLongitude - prevLongitude)) * SCALE_FACTOR;
            if ((dx * dx) + (dy * dy) > sqTolerance) {
                newLatLongs.add(Float.valueOf(pointLatitude));
                newLatLongs.add(Float.valueOf(pointLongitude));
                prevLatitude = pointLatitude;
                prevLongitude = pointLongitude;
            }
        }
        geo = (GeoCoordinate) points.get(len - 1);
        pointLongitude = (float) geo.getLongitude();
        newLatLongs.add(Float.valueOf((float) geo.getLatitude()));
        newLatLongs.add(Float.valueOf(pointLongitude));
        return newLatLongs;
    }

    static Bounds getLatLngBounds(List<GeoCoordinate> points) {
        double lowestLat = ((GeoCoordinate) points.get(0)).getLatitude();
        double highestLat = lowestLat;
        double lowestLong = ((GeoCoordinate) points.get(0)).getLongitude();
        double highestLong = lowestLong;
        for (GeoCoordinate geo : points) {
            double latitude = geo.getLatitude();
            double longitude = geo.getLongitude();
            lowestLat = Math.min(latitude, lowestLat);
            highestLat = Math.max(latitude, highestLat);
            lowestLong = Math.min(longitude, lowestLong);
            highestLong = Math.max(longitude, highestLong);
        }
        return new Bounds(highestLong - lowestLong, highestLat - lowestLat);
    }

    public static String formatEtaMinutes(Resources resources, int minutes) {
        if (minutes < 60) {
            return resources.getString(R.string.eta_time_min, new Object[]{Integer.valueOf(minutes)});
        } else if (minutes < HomeScreenConstants.MINUTES_DAY) {
            if (minutes - ((minutes / 60) * 60) > 0) {
                return resources.getString(R.string.eta_time_hour, new Object[]{Integer.valueOf(minutes / 60), Integer.valueOf(minutes - ((minutes / 60) * 60))});
            }
            return resources.getString(R.string.eta_time_hour_no_min, new Object[]{Integer.valueOf(minutes / 60)});
        } else {
            if ((minutes - ((minutes / HomeScreenConstants.MINUTES_DAY) * HomeScreenConstants.MINUTES_DAY)) / 60 > 0) {
                return resources.getString(R.string.eta_time_day, new Object[]{Integer.valueOf(minutes / HomeScreenConstants.MINUTES_DAY), Integer.valueOf((minutes - ((minutes / HomeScreenConstants.MINUTES_DAY) * HomeScreenConstants.MINUTES_DAY)) / 60)});
            }
            return resources.getString(R.string.eta_time_day_no_hour, new Object[]{Integer.valueOf(minutes / HomeScreenConstants.MINUTES_DAY)});
        }
    }
}
