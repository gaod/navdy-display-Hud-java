package com.navdy.hud.app.maps.here;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.Route.TrafficPenaltyMode;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RouteOptions.Type;
import com.here.android.mpa.routing.RouteTta;
import com.here.android.mpa.routing.RoutingError;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.framework.network.NetworkBandwidthController;
import com.navdy.hud.app.framework.network.NetworkBandwidthController.UserBandwidthSettingChanged;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo;
import com.navdy.hud.app.maps.here.HereRouteCalculator.RouteCalculatorListener;
import com.navdy.hud.app.util.CrashReporter;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import net.hockeyapp.android.tasks.LoginTask;

public class NavdyTrafficRerouteManager {
    private static final int KILL_ROUTE_CALC = 45000;
    private static final int STALE_PERIOD = 300000;
    private static final Logger sLogger = new Logger("NavdyTrafficRerout");
    private final Bus bus;
    private Runnable fasterRouteCheck = new Runnable() {
        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            try {
                NavdyTrafficRerouteManager.this.handler.removeCallbacks(NavdyTrafficRerouteManager.this.killRouteCalc);
                if (!NetworkStateManager.isConnectedToNetwork(HudApplication.getAppContext())) {
                    NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                    NavdyTrafficRerouteManager.sLogger.v("skip traffic check, no n/w");
                    if (NavdyTrafficRerouteManager.this.running) {
                        NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                    }
                } else if (!NavdyTrafficRerouteManager.this.hereNavigationManager.isNavigationModeOn()) {
                    NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
                    NavdyTrafficRerouteManager.sLogger.v("skip traffic check, not navigating");
                    if (NavdyTrafficRerouteManager.this.running) {
                        NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                    }
                } else if (NavdyTrafficRerouteManager.this.hereNavigationManager.isRerouting()) {
                    NavdyTrafficRerouteManager.sLogger.v("skip traffic check, rerouting");
                    NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
                    if (NavdyTrafficRerouteManager.this.running) {
                        NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                    }
                } else if (NavdyTrafficRerouteManager.this.hereNavigationManager.hasArrived()) {
                    NavdyTrafficRerouteManager.sLogger.v("skip traffic check, arrival mode on");
                    NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
                    if (NavdyTrafficRerouteManager.this.running) {
                        NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                    }
                } else if (NavdyTrafficRerouteManager.this.hereNavigationManager.isOnGasRoute()) {
                    NavdyTrafficRerouteManager.sLogger.v("skip traffic check, on gas route");
                    NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
                    if (NavdyTrafficRerouteManager.this.running) {
                        NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                    }
                } else if (NetworkBandwidthController.getInstance().isLimitBandwidthModeOn() && NavdyTrafficRerouteManager.this.hereNavigationManager.hasTrafficRerouteOnce()) {
                    NavdyTrafficRerouteManager.sLogger.v("user has already selected faster route, no-op in limited b/w condition");
                    if (NavdyTrafficRerouteManager.this.running) {
                        NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                    }
                } else {
                    Route currentRoute = NavdyTrafficRerouteManager.this.hereNavigationManager.getCurrentRoute();
                    if (currentRoute == null) {
                        NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
                        NavdyTrafficRerouteManager.sLogger.v("skip traffic check, no current route");
                        if (NavdyTrafficRerouteManager.this.running) {
                            NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                            return;
                        }
                        return;
                    }
                    HereMapsManager hereMapsManager = HereMapsManager.getInstance();
                    GeoCoordinate startPoint = hereMapsManager.getLocationFixManager().getLastGeoCoordinate();
                    if (startPoint == null) {
                        NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                        NavdyTrafficRerouteManager.sLogger.e("skip traffic check, no start point");
                        if (NavdyTrafficRerouteManager.this.running) {
                            NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                            return;
                        }
                        return;
                    }
                    GeoCoordinate endPoint = currentRoute.getDestination();
                    if (endPoint == null) {
                        NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                        NavdyTrafficRerouteManager.sLogger.e("skip traffic check, no end point");
                        if (NavdyTrafficRerouteManager.this.running) {
                            NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                        }
                    } else if (NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.shouldCalculateFasterRoute()) {
                        RouteOptions routeOptions = hereMapsManager.getRouteOptions();
                        routeOptions.setRouteType(Type.FASTEST);
                        NavdyTrafficRerouteManager.this.routeCalculator.cancel();
                        NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.killRouteCalc, 45000);
                        NavdyTrafficRerouteManager.this.routeCalculator.calculateRoute(null, startPoint, null, endPoint, true, new RouteCalculatorListener() {
                            public void preSuccess() {
                                NavdyTrafficRerouteManager.this.handler.removeCallbacks(NavdyTrafficRerouteManager.this.killRouteCalc);
                            }

                            public void postSuccess(final ArrayList<NavigationRouteResult> outgoingResults) {
                                TaskManager.getInstance().execute(new Runnable() {
                                    public void run() {
                                        try {
                                            if (!NavdyTrafficRerouteManager.this.running) {
                                                NavdyTrafficRerouteManager.sLogger.v("success,route manager stopped");
                                            } else if (outgoingResults == null || outgoingResults.size() == 0) {
                                                NavdyTrafficRerouteManager.sLogger.v("success, but no result");
                                                NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                            } else {
                                                NavdyTrafficRerouteManager.sLogger.v(LoginTask.BUNDLE_SUCCESS);
                                                NavigationRouteResult result = (NavigationRouteResult) outgoingResults.get(0);
                                                HereRouteCache hereRouteCache = HereRouteCache.getInstance();
                                                RouteInfo routeInfo = hereRouteCache.getRoute(result.routeId);
                                                if (routeInfo == null) {
                                                    NavdyTrafficRerouteManager.sLogger.v("did not get routeinfo");
                                                    NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                                    return;
                                                }
                                                hereRouteCache.removeRoute(result.routeId);
                                                if (result.duration_traffic.intValue() == 0) {
                                                    NavdyTrafficRerouteManager.sLogger.v("did not get traffic duration");
                                                    NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                                    return;
                                                }
                                                Route currentRoute = NavdyTrafficRerouteManager.this.hereNavigationManager.getCurrentRoute();
                                                if (currentRoute == null) {
                                                    NavdyTrafficRerouteManager.sLogger.v("no current route");
                                                    NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                                    return;
                                                }
                                                RouteTta trafficTta = routeInfo.route.getTta(TrafficPenaltyMode.OPTIMAL, 268435455);
                                                RouteTta noTrafficTta = routeInfo.route.getTta(TrafficPenaltyMode.DISABLED, 268435455);
                                                if (!(trafficTta == null || noTrafficTta == null)) {
                                                    NavdyTrafficRerouteManager.sLogger.v("route duration traffic[" + trafficTta.getDuration() + "] no-traffic[" + noTrafficTta.getDuration() + "]");
                                                }
                                                List<Maneuver> fasterManeuvers = routeInfo.route.getManeuvers();
                                                List<Maneuver> currentManeuvers = currentRoute.getManeuvers();
                                                Maneuver currentManeuver = NavdyTrafficRerouteManager.this.hereNavigationManager.getNavController().getNextManeuver();
                                                int firstOffSet = -1;
                                                int secondOffset = 0;
                                                if (currentManeuver == null) {
                                                    NavdyTrafficRerouteManager.sLogger.i("no current maneuver");
                                                    firstOffSet = 0;
                                                } else {
                                                    int i;
                                                    int len = currentManeuvers.size();
                                                    for (i = 0; i < len; i++) {
                                                        if (HereMapUtil.areManeuverEqual((Maneuver) currentManeuvers.get(i), currentManeuver)) {
                                                            firstOffSet = i;
                                                            break;
                                                        }
                                                    }
                                                    if (firstOffSet == -1) {
                                                        NavdyTrafficRerouteManager.sLogger.i("cannot find current maneuver in route");
                                                        NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                                        return;
                                                    }
                                                    len = fasterManeuvers.size();
                                                    for (i = 0; i < len; i++) {
                                                        if (HereMapUtil.areManeuverEqual((Maneuver) fasterManeuvers.get(i), currentManeuver)) {
                                                            secondOffset = i;
                                                            break;
                                                        }
                                                    }
                                                }
                                                NavdyTrafficRerouteManager.sLogger.i("firstOff = " + firstOffSet + " secondOff = " + secondOffset);
                                                boolean areRoutesEqual = HereMapUtil.areManeuversEqual(currentManeuvers, firstOffSet, fasterManeuvers, secondOffset, new ArrayList(2));
                                                String via = HereRouteViaGenerator.getViaString(routeInfo.route);
                                                String currentVia = null;
                                                if (HereRouteCache.getInstance().getRoute(NavdyTrafficRerouteManager.this.hereNavigationManager.getCurrentRouteId()) != null) {
                                                    currentVia = routeInfo.routeResult.via;
                                                }
                                                if (areRoutesEqual) {
                                                    NavdyTrafficRerouteManager.sLogger.v("fast route same as current route faster-via[" + via + "] current-via[" + currentVia + "]");
                                                    if (TTSUtils.isDebugTTSEnabled()) {
                                                        TTSUtils.debugShowFasterRouteToast("Faster route not found", " current route via[" + currentVia + "] is still fastest");
                                                    }
                                                    NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                                    return;
                                                }
                                                NavdyTrafficRerouteManager.sLogger.i("fast route is different than current route");
                                                String currentRoadNames = HereMapUtil.getAllRoadNames(currentManeuvers, 0);
                                                String newRoadNames = HereMapUtil.getAllRoadNames(fasterManeuvers, 0);
                                                NavdyTrafficRerouteManager.sLogger.v("[RoadNames-current] [offset=" + firstOffSet + "] maneuverCount=" + currentManeuvers.size());
                                                NavdyTrafficRerouteManager.sLogger.v(currentRoadNames);
                                                NavdyTrafficRerouteManager.sLogger.v("[RoadNames-faster] [offset=" + secondOffset + "] maneuverCount=" + fasterManeuvers.size());
                                                NavdyTrafficRerouteManager.sLogger.v(newRoadNames);
                                                Route currentProposedRoute = NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.getCurrentProposedRoute();
                                                if (currentProposedRoute != null) {
                                                    NavdyTrafficRerouteManager.sLogger.i("check if current proposed route is same as new one");
                                                    List<Maneuver> currentProposedRouteManeuvers = currentProposedRoute.getManeuvers();
                                                    if (HereMapUtil.areManeuversEqual(currentProposedRouteManeuvers, 0, fasterManeuvers, 0, null)) {
                                                        Date oldEta = NavdyTrafficRerouteManager.this.hereNavigationManager.getNavController().getEta(true, TrafficPenaltyMode.OPTIMAL);
                                                        if (HereMapUtil.isValidEtaDate(oldEta)) {
                                                            long diff = ((oldEta.getTime() - System.currentTimeMillis()) / 1000) - ((long) trafficTta.getDuration());
                                                            int minDuration = NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.getRerouteMinDuration();
                                                            NavdyTrafficRerouteManager.sLogger.i("current proposed route[" + currentVia + "] is same as new one[" + via + "] new-diff[" + diff + "] old-diff[" + NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.getFasterBy() + "] minDur[" + minDuration + "]");
                                                            if (diff < ((long) minDuration)) {
                                                                NavdyTrafficRerouteManager.sLogger.i("route has dropped below threshold:" + diff);
                                                                NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
                                                                return;
                                                            }
                                                            NavdyTrafficRerouteManager.sLogger.i("route still above threshold, update");
                                                        } else {
                                                            NavdyTrafficRerouteManager.sLogger.v("current eta is invalid:" + oldEta);
                                                            NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                                            return;
                                                        }
                                                    }
                                                    NavdyTrafficRerouteManager.sLogger.i("current proposed route[" + currentVia + "] is different from new one[" + via + "]");
                                                    currentRoadNames = HereMapUtil.getAllRoadNames(currentProposedRouteManeuvers, 0);
                                                    newRoadNames = HereMapUtil.getAllRoadNames(fasterManeuvers, 0);
                                                    NavdyTrafficRerouteManager.sLogger.v("[RoadNames-proposed-current] maneuverCount=" + currentProposedRouteManeuvers.size());
                                                    NavdyTrafficRerouteManager.sLogger.v(currentRoadNames);
                                                    NavdyTrafficRerouteManager.sLogger.v("[RoadNames-proposed-faster] maneuverCount=" + fasterManeuvers.size());
                                                    NavdyTrafficRerouteManager.sLogger.v(newRoadNames);
                                                }
                                                NavdyTrafficRerouteManager.sLogger.i("calling here traffic reroute listener");
                                                NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.handleFasterRoute(routeInfo.route, false);
                                            }
                                        } catch (Throwable t) {
                                            NavdyTrafficRerouteManager.sLogger.e(t);
                                        }
                                    }
                                }, 2);
                            }

                            public void error(RoutingError error, Throwable t) {
                                NavdyTrafficRerouteManager.this.handler.removeCallbacks(NavdyTrafficRerouteManager.this.killRouteCalc);
                                NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                if (TTSUtils.isDebugTTSEnabled()) {
                                    String title = "Faster route not found [Error]";
                                    String info = "";
                                    if (t != null) {
                                        info = t.toString();
                                    } else if (error != null) {
                                        info = error.name();
                                    }
                                    TTSUtils.debugShowFasterRouteToast(title, info);
                                }
                                NavdyTrafficRerouteManager.sLogger.e("error occured:" + error, t);
                            }

                            public void progress(int progress) {
                            }
                        }, 1, routeOptions, true, false, false);
                        if (NavdyTrafficRerouteManager.this.running) {
                            NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                        }
                    } else {
                        NavdyTrafficRerouteManager.sLogger.e("skip traffic check, time not right");
                        if (NavdyTrafficRerouteManager.this.running) {
                            NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                        }
                    }
                }
            } catch (Throwable th) {
                if (NavdyTrafficRerouteManager.this.running) {
                    NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long) (NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                }
            }
        }
    };
    private Runnable fasterRouteTrigger = new Runnable() {
        public void run() {
            TaskManager.getInstance().execute(NavdyTrafficRerouteManager.this.fasterRouteCheck, 2);
        }
    };
    private Handler handler = new Handler(Looper.getMainLooper());
    private final HereNavigationManager hereNavigationManager;
    private final HereTrafficRerouteListener hereTrafficRerouteListener;
    private Runnable killRouteCalc = new Runnable() {
        public void run() {
            try {
                NavdyTrafficRerouteManager.sLogger.v("stopping route calc");
                NavdyTrafficRerouteManager.this.routeCalculator.cancel();
            } catch (Throwable t) {
                NavdyTrafficRerouteManager.sLogger.e(t);
            }
        }
    };
    private HereRouteCalculator routeCalculator = new HereRouteCalculator(sLogger, false);
    private volatile boolean running;
    private Runnable staleRoute = new Runnable() {
        public void run() {
            try {
                NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
            } catch (Throwable t) {
                NavdyTrafficRerouteManager.sLogger.e(t);
            }
        }
    };

    NavdyTrafficRerouteManager(HereNavigationManager hereNavigationManager, HereTrafficRerouteListener hereTrafficRerouteListener, Bus bus) {
        sLogger.v("NavdyTrafficRerouteManager:ctor");
        this.hereNavigationManager = hereNavigationManager;
        this.hereTrafficRerouteListener = hereTrafficRerouteListener;
        this.bus = bus;
    }

    public void start() {
        if (!this.running) {
            this.running = true;
            this.handler.removeCallbacks(this.staleRoute);
            this.handler.removeCallbacks(this.fasterRouteTrigger);
            this.handler.postDelayed(this.fasterRouteTrigger, (long) HereTrafficRerouteListener.ROUTE_CHECK_INITIAL_INTERVAL);
            this.bus.register(this);
            sLogger.v("running");
        }
    }

    public void stop() {
        if (this.running) {
            this.running = false;
            this.handler.removeCallbacks(this.fasterRouteTrigger);
            this.handler.postDelayed(this.staleRoute, 300000);
            this.bus.unregister(this);
            sLogger.v("stopped");
        }
    }

    @Subscribe
    public void onBandwidthSettingChanged(UserBandwidthSettingChanged event) {
        this.handler.removeCallbacks(this.fasterRouteTrigger);
        if (this.running) {
            long interval = (long) (this.hereNavigationManager.getRerouteInterval() / 2);
            sLogger.v("faster route time changed to " + interval);
            this.handler.postDelayed(this.fasterRouteTrigger, interval);
        }
    }

    private void cleanupRouteIfStale() {
        if (this.hereTrafficRerouteListener.getCurrentProposedRoute() != null) {
            long time = SystemClock.elapsedRealtime() - this.hereTrafficRerouteListener.getCurrentProposedRouteTime();
            if (time > 300000) {
                sLogger.v("clear stale route:" + time);
                this.hereTrafficRerouteListener.dismissReroute();
                return;
            }
            sLogger.v("route not stale:" + time);
            return;
        }
        sLogger.v("no proposed route");
    }

    public void reset() {
        try {
            sLogger.v("reset::");
            this.routeCalculator.cancel();
            this.hereTrafficRerouteListener.dismissReroute();
            this.handler.removeCallbacks(this.fasterRouteTrigger);
            if (this.running) {
                sLogger.v("reset::timer reset");
                this.handler.postDelayed(this.fasterRouteTrigger, (long) HereTrafficRerouteListener.ROUTE_CHECK_INITIAL_INTERVAL);
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    void reCalculateCurrentRoute(Route currentRoute, Route fasterRoute, boolean notifFromHereTraffic, String via, long diff, String additionalVia, long etaUtc, long distanceDiff, String currentVia, RouteTta fasterRouteTta) {
        try {
            sLogger.v("reCalculateCurrentRoute:");
            this.handler.removeCallbacks(this.killRouteCalc);
            if (!NetworkStateManager.isConnectedToNetwork(HudApplication.getAppContext())) {
                cleanupRouteIfStale();
                this.hereTrafficRerouteListener.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
                sLogger.v("reCalculateCurrentRoute:skip no n/w");
            } else if (!this.hereNavigationManager.isNavigationModeOn()) {
                this.hereTrafficRerouteListener.dismissReroute();
                this.hereTrafficRerouteListener.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
                sLogger.v("reCalculateCurrentRoute: not navigating");
            } else if (this.hereNavigationManager.isRerouting()) {
                sLogger.v("reCalculateCurrentRoute: rerouting");
                this.hereTrafficRerouteListener.dismissReroute();
                this.hereTrafficRerouteListener.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
            } else if (this.hereNavigationManager.hasArrived()) {
                sLogger.v("reCalculateCurrentRoute: arrival mode on");
                this.hereTrafficRerouteListener.dismissReroute();
                this.hereTrafficRerouteListener.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
            } else if (this.hereNavigationManager.isOnGasRoute()) {
                sLogger.v("reCalculateCurrentRoute: on gas route");
                this.hereTrafficRerouteListener.dismissReroute();
                this.hereTrafficRerouteListener.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
            } else {
                HereMapsManager hereMapsManager = HereMapsManager.getInstance();
                GeoCoordinate startPoint = hereMapsManager.getLocationFixManager().getLastGeoCoordinate();
                if (startPoint == null) {
                    cleanupRouteIfStale();
                    this.hereTrafficRerouteListener.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
                    sLogger.e("reCalculateCurrentRoute: no start point");
                    return;
                }
                GeoCoordinate endPoint = currentRoute.getDestination();
                if (endPoint == null) {
                    cleanupRouteIfStale();
                    this.hereTrafficRerouteListener.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
                    sLogger.e("reCalculateCurrentRoute: no end point");
                    return;
                }
                RouteOptions routeOptions = hereMapsManager.getRouteOptions();
                routeOptions.setRouteType(Type.FASTEST);
                Maneuver currentManeuver = this.hereNavigationManager.getNavController().getNextManeuver();
                if (currentManeuver == null) {
                    cleanupRouteIfStale();
                    this.hereTrafficRerouteListener.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
                    sLogger.i("reCalculateCurrentRoute: no current maneuver");
                    return;
                }
                int i;
                int offset = -1;
                List<Maneuver> maneuvers = currentRoute.getManeuvers();
                int len = 0;
                if (maneuvers != null) {
                    len = maneuvers.size();
                    for (i = 0; i < len; i++) {
                        if (HereMapUtil.areManeuverEqual((Maneuver) maneuvers.get(i), currentManeuver)) {
                            offset = i;
                            break;
                        }
                    }
                }
                if (offset == -1) {
                    sLogger.i("reCalculateCurrentRoute: cannot find current maneuver in route");
                    cleanupRouteIfStale();
                    this.hereTrafficRerouteListener.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
                    return;
                }
                List<GeoCoordinate> waypoints = new ArrayList();
                for (i = offset; i < len; i++) {
                    GeoCoordinate geoCoordinate = ((Maneuver) maneuvers.get(i)).getCoordinate();
                    if (geoCoordinate != null) {
                        sLogger.i("reCalculateCurrentRoute: added waypoint:" + geoCoordinate);
                        waypoints.add(geoCoordinate);
                    }
                }
                sLogger.i("reCalculateCurrentRoute: waypoints:" + waypoints.size());
                this.routeCalculator.cancel();
                this.handler.postDelayed(this.killRouteCalc, 45000);
                final boolean z = notifFromHereTraffic;
                final long j = diff;
                final String str = via;
                final String str2 = currentVia;
                final RouteTta routeTta = fasterRouteTta;
                final long j2 = etaUtc;
                final Route route = fasterRoute;
                final String str3 = additionalVia;
                final long j3 = distanceDiff;
                this.routeCalculator.calculateRoute(null, startPoint, waypoints, endPoint, true, new RouteCalculatorListener() {
                    public void preSuccess() {
                        NavdyTrafficRerouteManager.this.handler.removeCallbacks(NavdyTrafficRerouteManager.this.killRouteCalc);
                    }

                    public void postSuccess(final ArrayList<NavigationRouteResult> outgoingResults) {
                        TaskManager.getInstance().execute(new Runnable() {
                            public void run() {
                                try {
                                    if (!NavdyTrafficRerouteManager.this.running) {
                                        NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: success,route manager stopped");
                                        NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                        NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(z, j, str, str2);
                                    } else if (outgoingResults == null || outgoingResults.size() == 0) {
                                        NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: success, but no result");
                                        NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                        NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(z, j, str, str2);
                                    } else {
                                        NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: success");
                                        NavigationRouteResult result = (NavigationRouteResult) outgoingResults.get(0);
                                        HereRouteCache hereRouteCache = HereRouteCache.getInstance();
                                        RouteInfo routeInfo = hereRouteCache.getRoute(result.routeId);
                                        if (routeInfo == null) {
                                            NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: did not get routeinfo");
                                            NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                            NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(z, j, str, str2);
                                            return;
                                        }
                                        hereRouteCache.removeRoute(result.routeId);
                                        if (result.duration_traffic.intValue() == 0) {
                                            NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: did not get traffic duration");
                                            NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                            NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(z, j, str, str2);
                                            return;
                                        }
                                        RouteTta currentTrafficTta = routeInfo.route.getTta(TrafficPenaltyMode.OPTIMAL, 268435455);
                                        if (currentTrafficTta == null) {
                                            NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: could not get traffic route tta");
                                            NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                            NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(z, j, str, str2);
                                            return;
                                        }
                                        int currentDuration = currentTrafficTta.getDuration();
                                        int fasterDuration = routeTta.getDuration();
                                        NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: currentDuration=" + currentDuration + " fasterDuration=" + fasterDuration);
                                        long newDiff = (long) (currentDuration - fasterDuration);
                                        int minDuration = NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.getRerouteMinDuration();
                                        if (newDiff >= ((long) minDuration)) {
                                            NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: set faster route, duration:" + newDiff + " >= threshold:" + minDuration);
                                            long now = System.currentTimeMillis();
                                            long newEtaUtc = now + ((long) (fasterDuration * 1000));
                                            Date date = new Date(newEtaUtc);
                                            date = new Date(now);
                                            NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: newEta:" + date + " now=" + date + " currentEta=" + new Date(j2));
                                            NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.setFasterRoute(route, z, str, 0, str3, 0, j3, newEtaUtc);
                                            return;
                                        }
                                        NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: duration:" + newDiff + " < threshold:" + minDuration);
                                        NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                        NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(z, newDiff, str, str2);
                                    }
                                } catch (Throwable t) {
                                    NavdyTrafficRerouteManager.sLogger.e(t);
                                }
                            }
                        }, 2);
                    }

                    public void error(RoutingError error, Throwable t) {
                        NavdyTrafficRerouteManager.this.handler.removeCallbacks(NavdyTrafficRerouteManager.this.killRouteCalc);
                        NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                        NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(z, j, str, str2);
                        NavdyTrafficRerouteManager.sLogger.e("error occured:" + error, t);
                    }

                    public void progress(int progress) {
                    }
                }, 1, routeOptions, true, false, false);
            }
        } catch (Throwable t) {
            sLogger.e(t);
            cleanupRouteIfStale();
            this.hereTrafficRerouteListener.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
            CrashReporter.getInstance().reportNonFatalException(t);
        }
    }
}
