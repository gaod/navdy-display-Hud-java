package com.navdy.hud.app.maps.here;

import android.os.Bundle;
import android.os.SystemClock;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.guidance.NavigationManager.Error;
import com.here.android.mpa.guidance.NavigationManager.TrafficRerouteListener;
import com.here.android.mpa.guidance.NavigationManager.TrafficRerouteListener.TrafficEnabledRoutingState;
import com.here.android.mpa.guidance.TrafficNotification;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.Route.TrafficPenaltyMode;
import com.here.android.mpa.routing.RouteTta;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.TrafficRerouteEvent;
import com.navdy.hud.app.maps.here.HereMapUtil.LongContainer;
import com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo;
import com.navdy.hud.app.maps.notification.RouteCalculationNotification;
import com.navdy.hud.app.maps.notification.TrafficNotificationManager;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.util.CrashReporter;
import com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

class HereTrafficRerouteListener extends TrafficRerouteListener {
    static final int MIN_INTERVAL_REROUTE_FIRST = 90;
    static final long NOTIF_FROM_HERE_THRESHOLD = TimeUnit.MINUTES.toMillis(1);
    static final long REROUTE_POINT_THRESHOLD = TimeUnit.MINUTES.toSeconds(20);
    static final int ROUTE_CHECK_INITIAL_INTERVAL = ((int) TimeUnit.MINUTES.toMillis(1));
    static final int ROUTE_CHECK_INTERVAL = ((int) TimeUnit.MINUTES.toMillis(5));
    static final int ROUTE_CHECK_INTERVAL_LIMIT_BANDWIDTH = ((int) TimeUnit.MINUTES.toMillis(30));
    static final int WAIT_INTERVAL_AFTER_FIRST_MANEUVER_SHOWN = ((int) TimeUnit.SECONDS.toMillis(60));
    private final Bus bus;
    private Route currentProposedRoute;
    private long fasterBy;
    private final HereNavigationManager hereNavigationManager;
    private long lastTrafficNotifFromHere;
    private final Logger logger = new Logger("HereTrafficReroute");
    private final HereNavController navController;
    private NavdyTrafficRerouteManager navdyTrafficRerouteManager;
    private boolean notifFromHere;
    private GeoCoordinate placeCalculated;
    private Runnable startRunnable = new Runnable() {
        public void run() {
            HereTrafficRerouteListener.this.navController.addTrafficRerouteListener(new WeakReference(HereTrafficRerouteListener.this));
            HereTrafficRerouteListener.this.logger.v("addTrafficRerouteListener");
        }
    };
    private Runnable stopRunnable = new Runnable() {
        public void run() {
            HereTrafficRerouteListener.this.navController.removeTrafficRerouteListener(HereTrafficRerouteListener.this);
            HereTrafficRerouteListener.this.logger.v("removeTrafficRerouteListener");
        }
    };
    private final String tag;
    private long timeCalculated;
    private boolean trafficRerouteListenerRunning;
    private final boolean verbose;
    private String viaString;

    HereTrafficRerouteListener(String tag, boolean verbose, HereNavController navController, HereNavigationManager hereNavigationManager, Bus bus) {
        this.logger.v("HereTrafficRerouteListener:ctor");
        this.tag = tag;
        this.verbose = verbose;
        this.navController = navController;
        this.hereNavigationManager = hereNavigationManager;
        this.bus = bus;
    }

    public void confirmReroute() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (HereTrafficRerouteListener.this.currentProposedRoute != null) {
                    HereTrafficRerouteListener.this.hereNavigationManager.clearNavManeuver();
                    Error error = HereTrafficRerouteListener.this.navController.setRoute(HereTrafficRerouteListener.this.currentProposedRoute);
                    if (error == Error.NONE) {
                        HereTrafficRerouteListener.this.hereNavigationManager.updateMapRoute(HereTrafficRerouteListener.this.currentProposedRoute, RerouteReason.NAV_SESSION_TRAFFIC_REROUTE, null, null, false, true);
                        HereTrafficRerouteListener.this.logger.e("confirmReroute: set");
                    } else {
                        HereTrafficRerouteListener.this.logger.e("confirmReroute: traffic route could not be set:" + error);
                    }
                    HereTrafficRerouteListener.this.clearProposedRoute();
                    return;
                }
                HereTrafficRerouteListener.this.logger.e("confirmReroute: no route available");
            }
        }, 20);
    }

    public void dismissReroute() {
        clearProposedRoute();
    }

    private void clearProposedRoute() {
        this.logger.v("clearProposedRoute");
        if (this.currentProposedRoute != null) {
            this.logger.v("clearProposedRoute:cleared");
            TrafficNotificationManager.getInstance().removeFasterRouteNotifiation();
            this.currentProposedRoute = null;
            this.timeCalculated = 0;
            this.placeCalculated = null;
            this.notifFromHere = false;
            this.viaString = null;
            this.fasterBy = 0;
        }
    }

    public void onTrafficRerouteBegin(TrafficNotification trafficNotification) {
        this.logger.v(this.tag + " reroute-begin:");
        if (this.verbose) {
            HereMapUtil.printTrafficNotification(trafficNotification, this.tag);
        }
    }

    public void onTrafficRerouted(Route fasterRoute) {
        long now = SystemClock.elapsedRealtime();
        long diff = now - this.lastTrafficNotifFromHere;
        if (diff < NOTIF_FROM_HERE_THRESHOLD) {
            this.logger.i(this.tag + "here traffic notif came in " + diff);
        } else if (isMoreRouteNotificationComplete()) {
            this.lastTrafficNotifFromHere = now;
            handleFasterRoute(fasterRoute, true);
        } else {
            this.logger.v("onTrafficRerouted: more route notif in progress");
        }
    }

    public void onTrafficRerouteFailed(TrafficNotification trafficNotification) {
        this.logger.e(this.tag + " reroute-failed");
        HereMapUtil.printTrafficNotification(trafficNotification, this.tag);
    }

    public void onTrafficRerouteState(TrafficEnabledRoutingState trafficEnabledRoutingState) {
    }

    public void start() {
        if (!this.trafficRerouteListenerRunning) {
            this.trafficRerouteListenerRunning = true;
            TaskManager.getInstance().execute(this.startRunnable, 15);
            this.logger.v("added traffic reroute listener");
        }
    }

    public void stop() {
        if (this.trafficRerouteListenerRunning) {
            this.trafficRerouteListenerRunning = false;
            TaskManager.getInstance().execute(this.stopRunnable, 15);
            this.logger.v("removed traffic reroute listener");
        }
    }

    public boolean shouldCalculateFasterRoute() {
        long elapsed = SystemClock.elapsedRealtime() - this.timeCalculated;
        this.logger.v("elapsed=" + elapsed);
        if (elapsed < ((long) (this.hereNavigationManager.getRerouteInterval() - 15000))) {
            return false;
        }
        if (isMoreRouteNotificationComplete()) {
            return true;
        }
        this.logger.v("shouldCalculateFasterRoute: more route notif in progress");
        return false;
    }

    public boolean isMoreRouteNotificationComplete() {
        if (!this.hereNavigationManager.isShownFirstManeuver()) {
            this.logger.v("isMoreRouteNotificationComplete:not shown first maneuver");
            return false;
        } else if (NotificationManager.getInstance().isCurrentNotificationId(NotificationId.ROUTE_CALC_NOTIFICATION_ID)) {
            this.logger.v("isMoreRouteNotificationComplete:route calc notif on");
            return false;
        } else {
            BaseScreen screen = RemoteDeviceManager.getInstance().getUiStateManager().getCurrentScreen();
            if (screen != null && screen.getScreen() == Screen.SCREEN_DESTINATION_PICKER) {
                Bundle bundle = screen.getArguments();
                if (bundle != null && bundle.getBoolean(RouteCalculationNotification.ROUTE_PICKER)) {
                    this.logger.v("isMoreRouteNotificationComplete:user looking at route picker");
                    return false;
                }
            }
            if (HereMapsManager.getInstance().getRouteCalcEventHandler().hasMoreRoutes()) {
                this.logger.v("isMoreRouteNotificationComplete:user has not seen more routes");
                return false;
            }
            long diff = SystemClock.elapsedRealtime() - this.hereNavigationManager.getFirstManeuverShowntime();
            if (diff >= ((long) WAIT_INTERVAL_AFTER_FIRST_MANEUVER_SHOWN)) {
                return true;
            }
            this.logger.v("isMoreRouteNotificationComplete:first maneuver just shown:" + diff);
            return false;
        }
    }

    public Route getCurrentProposedRoute() {
        return this.currentProposedRoute;
    }

    public long getCurrentProposedRouteTime() {
        return this.timeCalculated;
    }

    public String getViaString() {
        return this.viaString;
    }

    public long getFasterBy() {
        return this.fasterBy;
    }

    public void handleFasterRoute(final Route fasterRoute, final boolean notifFromHereTraffic) {
        if (fasterRoute == null) {
            this.logger.e("null faster route:" + notifFromHereTraffic);
        } else {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    try {
                        String fromTag = notifFromHereTraffic ? "[Here Traffic]" : "[Navdy Calc]";
                        HereTrafficRerouteListener.this.logger.v(HereTrafficRerouteListener.this.tag + fromTag + " onTrafficRerouted");
                        HereTrafficRerouteListener.this.dismissReroute();
                        if (!HereTrafficRerouteListener.this.hereNavigationManager.isNavigationModeOn()) {
                            HereTrafficRerouteListener.this.logger.v(HereTrafficRerouteListener.this.tag + " onTrafficRerouted, not currently navigating");
                        } else if (HereTrafficRerouteListener.this.hereNavigationManager.hasArrived()) {
                            HereTrafficRerouteListener.this.logger.v("onTrafficRerouted, arrival mode on");
                        } else if (HereTrafficRerouteListener.this.hereNavigationManager.isOnGasRoute()) {
                            HereTrafficRerouteListener.this.logger.v("onTrafficRerouted, on fas route");
                        } else {
                            Route currentRoute = HereTrafficRerouteListener.this.hereNavigationManager.getCurrentRoute();
                            if (currentRoute == null) {
                                HereTrafficRerouteListener.this.logger.v(HereTrafficRerouteListener.this.tag + " onTrafficRerouted, not current route");
                                return;
                            }
                            List<Maneuver> currentManeuvers = currentRoute.getManeuvers();
                            if (notifFromHereTraffic) {
                                String currentRoadNames = HereMapUtil.getAllRoadNames(fasterRoute.getManeuvers(), 0);
                                String newRoadNames = HereMapUtil.getAllRoadNames(currentManeuvers, 0);
                                HereTrafficRerouteListener.this.logger.v("[HereNotif-RoadNames-current]");
                                HereTrafficRerouteListener.this.logger.v(currentRoadNames);
                                HereTrafficRerouteListener.this.logger.v("[HereNotif-RoadNames-faster]");
                                HereTrafficRerouteListener.this.logger.v(newRoadNames);
                            }
                            RouteTta fasterRouteTta = fasterRoute.getTta(TrafficPenaltyMode.OPTIMAL, 268435455);
                            RouteTta fasterRouteTtaNoTraffic = fasterRoute.getTta(TrafficPenaltyMode.DISABLED, 268435455);
                            if (fasterRouteTta == null || fasterRouteTtaNoTraffic == null) {
                                HereTrafficRerouteListener.this.logger.v(HereTrafficRerouteListener.this.tag + "error retrieving navigation and TTA");
                                return;
                            }
                            if (!notifFromHereTraffic) {
                                long d1 = (long) fasterRouteTta.getDuration();
                                if (d1 == ((long) fasterRouteTtaNoTraffic.getDuration())) {
                                    HereTrafficRerouteListener.this.logger.v(HereTrafficRerouteListener.this.tag + "faster route durations is same=" + d1);
                                    return;
                                }
                            }
                            long oldTtaDuration = 0;
                            RouteTta currentRouteTta = currentRoute.getTta(TrafficPenaltyMode.OPTIMAL, 268435455);
                            if (currentRouteTta != null) {
                                oldTtaDuration = (long) currentRouteTta.getDuration();
                            }
                            Date etaDate = HereTrafficRerouteListener.this.navController.getEta(true, TrafficPenaltyMode.OPTIMAL);
                            if (HereMapUtil.isValidEtaDate(etaDate)) {
                                long etaUtc = etaDate.getTime();
                                long now = System.currentTimeMillis();
                                long oldTta = (etaUtc - now) / 1000;
                                long newTta = (long) fasterRouteTta.getDuration();
                                long diff = oldTta - newTta;
                                HereTrafficRerouteListener.this.logger.i("etaDate[" + etaDate + "] etaUtc[" + etaUtc + "] currentTta[" + oldTta + "] now[" + now + "] newTta[" + newTta + "]" + "] diff[" + diff + "]" + "] oldTtaDuration[" + oldTtaDuration + "]");
                                HashSet<String> seenVia = new HashSet();
                                List<Maneuver> arrayList = new ArrayList(1);
                                List<Maneuver> newRouteDifferentManeuver = HereMapUtil.getDifferentManeuver(fasterRoute, currentRoute, 2, arrayList);
                                if (newRouteDifferentManeuver.size() == 0) {
                                    HereTrafficRerouteListener.this.logger.i("could not find a difference in maneuvers");
                                    return;
                                }
                                String str;
                                String via = null;
                                StringBuilder viaBuilder = new StringBuilder();
                                for (Maneuver m : newRouteDifferentManeuver) {
                                    str = HereMapUtil.getNextRoadName(m);
                                    if (via == null) {
                                        via = str;
                                        viaBuilder.append(via);
                                        seenVia.add(via);
                                    } else if (!seenVia.contains(str)) {
                                        viaBuilder.append(HereManeuverDisplayBuilder.COMMA);
                                        viaBuilder.append(" ");
                                        viaBuilder.append(str);
                                        seenVia.add(str);
                                    }
                                }
                                str = HereRouteViaGenerator.getViaString(fasterRoute);
                                if (!seenVia.contains(str)) {
                                    viaBuilder.append(HereManeuverDisplayBuilder.COMMA);
                                    viaBuilder.append(" ");
                                    viaBuilder.append(str);
                                }
                                String currentVia = null;
                                RouteInfo routeInfo = HereRouteCache.getInstance().getRoute(HereTrafficRerouteListener.this.hereNavigationManager.getCurrentRouteId());
                                if (routeInfo != null) {
                                    currentVia = routeInfo.routeResult.via;
                                }
                                String additionalVia = viaBuilder.toString();
                                long distanceDiff = ((long) fasterRoute.getLength()) - HereTrafficRerouteListener.this.navController.getDestinationDistance();
                                int minDuration = HereTrafficRerouteListener.this.getRerouteMinDuration();
                                HereTrafficRerouteListener.this.logger.v(HereTrafficRerouteListener.this.tag + fromTag + " new route via[" + via + "] additionalVia[" + additionalVia + "] is faster by[" + diff + "] seconds than current route via[" + currentVia + "] distanceDiff [" + distanceDiff + "] minDur[" + minDuration + "]");
                                if (diff >= ((long) minDuration)) {
                                    HereTrafficRerouteListener.this.logger.v("faster route is above threshold:" + diff);
                                    if (HereTrafficRerouteListener.this.isFasterRouteDivergePointCloseBy(currentManeuvers, arrayList, newRouteDifferentManeuver)) {
                                        HereTrafficRerouteListener.this.setFasterRoute(fasterRoute, notifFromHereTraffic, via, diff, additionalVia, etaUtc, distanceDiff, null);
                                        return;
                                    } else {
                                        HereTrafficRerouteListener.this.logger.v("faster route not near by, threshold not met");
                                        return;
                                    }
                                }
                                HereTrafficRerouteListener.this.logger.v("faster route is below threshold:" + diff);
                                if (!HereMapsManager.getInstance().isRecalcCurrentRouteForTrafficEnabled()) {
                                    HereTrafficRerouteListener.this.fasterRouteNotFound(notifFromHereTraffic, diff, via, currentVia);
                                    return;
                                } else if (HereTrafficRerouteListener.this.isFasterRouteDivergePointCloseBy(currentManeuvers, arrayList, newRouteDifferentManeuver)) {
                                    HereTrafficRerouteListener.this.navdyTrafficRerouteManager.reCalculateCurrentRoute(currentRoute, fasterRoute, notifFromHereTraffic, via, diff, additionalVia, etaUtc, distanceDiff, currentVia, fasterRouteTta);
                                    return;
                                } else {
                                    HereTrafficRerouteListener.this.logger.v("faster route not near by, threshold not met");
                                    return;
                                }
                            }
                            HereTrafficRerouteListener.this.logger.v(HereTrafficRerouteListener.this.tag + "error retrieving eta for current");
                        }
                    } catch (Throwable t) {
                        HereTrafficRerouteListener.this.logger.e(t);
                        CrashReporter.getInstance().reportNonFatalException(t);
                    }
                }
            }, 2);
        }
    }

    private boolean isFasterRouteDivergePointCloseBy(List<Maneuver> currentManeuvers, List<Maneuver> oldRouteDifferentManeuver, List<Maneuver> newRouteDifferentManeuver) {
        Maneuver fasterChange;
        Maneuver nextSlowChange;
        Maneuver currentManeuver = this.navController.getNextManeuver();
        Maneuver slowChange = (Maneuver) oldRouteDifferentManeuver.get(0);
        if (newRouteDifferentManeuver.size() > 1) {
            fasterChange = (Maneuver) newRouteDifferentManeuver.get(1);
        } else {
            fasterChange = (Maneuver) newRouteDifferentManeuver.get(0);
        }
        if (oldRouteDifferentManeuver.size() > 1) {
            nextSlowChange = (Maneuver) oldRouteDifferentManeuver.get(1);
        } else {
            nextSlowChange = slowChange;
        }
        this.logger.v(this.tag + " diverge point road faster [" + HereMapUtil.getRoadName(fasterChange) + HereManeuverDisplayBuilder.COMMA + HereMapUtil.getNextRoadName(fasterChange) + "]  slower [" + HereMapUtil.getRoadName(nextSlowChange) + HereManeuverDisplayBuilder.COMMA + HereMapUtil.getNextRoadName(nextSlowChange) + "]");
        if (HereMapUtil.areManeuverEqual(currentManeuver, slowChange)) {
            this.logger.i(this.tag + "first change and current are same");
        } else {
            int len;
            int i;
            int offset = -1;
            if (currentManeuver == null) {
                this.logger.i(this.tag + "no current maneuver");
            } else {
                len = currentManeuvers.size();
                for (i = 0; i < len; i++) {
                    if (HereMapUtil.areManeuverEqual((Maneuver) currentManeuvers.get(i), currentManeuver)) {
                        offset = i;
                        break;
                    }
                }
            }
            if (offset != -1) {
                this.logger.v(this.tag + "reroute offset=" + offset);
                long timeToManeuver = 0;
                long distToManeuver = 0;
                LongContainer longContainer = new LongContainer();
                len = currentManeuvers.size();
                boolean slowManeuverFound = false;
                for (i = offset; i < len; i++) {
                    Maneuver m = (Maneuver) currentManeuvers.get(i);
                    if (HereMapUtil.areManeuverEqual(m, slowChange)) {
                        this.logger.v(this.tag + "reroute diverge:" + i + HereManeuverDisplayBuilder.COMMA + len);
                        slowManeuverFound = true;
                        break;
                    }
                    timeToManeuver += HereMapUtil.getManeuverDriveTime(m, longContainer);
                    distToManeuver += longContainer.val;
                }
                if (!slowManeuverFound) {
                    this.logger.i(this.tag + "reroute threshold slow maneuver not found");
                    return true;
                } else if (timeToManeuver >= REROUTE_POINT_THRESHOLD) {
                    this.logger.i(this.tag + "reroute threshold [" + timeToManeuver + "] is > " + REROUTE_POINT_THRESHOLD + " dist=" + distToManeuver);
                    dismissReroute();
                    return false;
                } else {
                    this.logger.v(this.tag + "reroute point threshold [" + timeToManeuver + "] distance[" + distToManeuver + "]");
                }
            } else {
                this.logger.i(this.tag + "offset not found");
            }
        }
        return true;
    }

    void setFasterRoute(Route fasterRoute, boolean notifFromHereTraffic, String via, long diff, String additionalVia, long etaUtc, long distanceDiff, long newEta) {
        this.hereNavigationManager.incrCurrentTrafficRerouteCount();
        this.hereNavigationManager.setLastTrafficRerouteTime(SystemClock.elapsedRealtime());
        this.currentProposedRoute = fasterRoute;
        this.timeCalculated = SystemClock.elapsedRealtime();
        this.notifFromHere = notifFromHereTraffic;
        this.viaString = via;
        this.fasterBy = diff;
        this.placeCalculated = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
        this.bus.post(new TrafficRerouteEvent(additionalVia, additionalVia, diff, etaUtc, distanceDiff, newEta));
        this.logger.v("faster reroute event sent");
    }

    void fasterRouteNotFound(boolean notifFromHereTraffic, long diff, String via, String currentVia) {
        String fromTag = notifFromHereTraffic ? "[Here Traffic]" : "[Navdy Calc]";
        this.logger.v(this.tag + fromTag + " new route threshold not met:" + getRerouteMinDuration());
        if (diff > 0 && TTSUtils.isDebugTTSEnabled()) {
            TTSUtils.debugShowFasterRouteToast(fromTag + " Faster route below threshold", "via[" + via + "] faster by[" + diff + "] seconds current via[" + currentVia + "]");
        }
    }

    public void setNavdyTrafficRerouteManager(NavdyTrafficRerouteManager navdyTrafficRerouteManager) {
        this.navdyTrafficRerouteManager = navdyTrafficRerouteManager;
    }

    public int getRerouteMinDuration() {
        if (this.hereNavigationManager.getCurrentTrafficRerouteCount() < 1) {
            this.logger.v("getRerouteMinDuration:90");
            return MIN_INTERVAL_REROUTE_FIRST;
        }
        int secondsTillLastDuration = ((int) (SystemClock.elapsedRealtime() - this.hereNavigationManager.getLastTrafficRerouteTime())) / 1000;
        int ret = (int) (90.0d + (1.5d * ((double) Math.max(360 - secondsTillLastDuration, 0))));
        this.logger.v("getRerouteMinDuration:" + ret + " secslast:" + secondsTillLastDuration);
        return ret;
    }
}
