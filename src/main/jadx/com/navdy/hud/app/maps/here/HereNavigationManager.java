package com.navdy.hud.app.maps.here;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import com.amazonaws.services.s3.internal.Constants;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.RoadElement;
import com.here.android.mpa.guidance.AudioPlayerDelegate;
import com.here.android.mpa.guidance.NavigationManager;
import com.here.android.mpa.guidance.NavigationManager.Error;
import com.here.android.mpa.guidance.NavigationManager.MapUpdateMode;
import com.here.android.mpa.guidance.NavigationManager.TrafficAvoidanceMode;
import com.here.android.mpa.guidance.NavigationManager.TtsOutputFormat;
import com.here.android.mpa.guidance.NavigationManager.UnitSystem;
import com.here.android.mpa.guidance.VoiceCatalog;
import com.here.android.mpa.guidance.VoiceSkin;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.mapping.MapView;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Maneuver.Action;
import com.here.android.mpa.routing.Maneuver.Icon;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.Route.TrafficPenaltyMode;
import com.here.android.mpa.routing.RouteOptions;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.common.TimeHelper.UpdateClock;
import com.navdy.hud.app.device.dial.DialConstants;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.LocalSpeechRequest;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.framework.network.NetworkBandwidthController;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnitChanged;
import com.navdy.hud.app.maps.MapEvents.ArrivalEvent;
import com.navdy.hud.app.maps.MapEvents.DestinationDirection;
import com.navdy.hud.app.maps.MapEvents.GPSSpeedEvent;
import com.navdy.hud.app.maps.MapEvents.GpsStatusChange;
import com.navdy.hud.app.maps.MapEvents.LiveTrafficDismissEvent;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.maps.MapEvents.ManeuverEvent;
import com.navdy.hud.app.maps.MapEvents.ManeuverEvent.Type;
import com.navdy.hud.app.maps.MapEvents.ManeuverSoonEvent;
import com.navdy.hud.app.maps.MapEvents.NavigationModeChange;
import com.navdy.hud.app.maps.MapEvents.NewRouteAdded;
import com.navdy.hud.app.maps.MapEvents.SpeedWarning;
import com.navdy.hud.app.maps.MapEvents.TrafficRerouteAction;
import com.navdy.hud.app.maps.MapEvents.TrafficRerouteDismissEvent;
import com.navdy.hud.app.maps.MapSettings;
import com.navdy.hud.app.maps.MapsEventHandler;
import com.navdy.hud.app.maps.NavSessionPreferences;
import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState;
import com.navdy.hud.app.maps.here.HereNavController.State;
import com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo;
import com.navdy.hud.app.maps.notification.RouteCalculationNotification;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.profile.DriverSessionPreferences;
import com.navdy.hud.app.profile.HudLocale;
import com.navdy.hud.app.ui.component.homescreen.NavigationView;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.audio.SpeechRequest.Builder;
import com.navdy.service.library.events.audio.SpeechRequest.Category;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.events.navigation.NavigationSessionRequest;
import com.navdy.service.library.events.navigation.NavigationSessionResponse;
import com.navdy.service.library.events.navigation.NavigationSessionRouteChange;
import com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason;
import com.navdy.service.library.events.navigation.NavigationSessionState;
import com.navdy.service.library.events.navigation.NavigationSessionStatusEvent;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.service.library.events.preferences.NavigationPreferences.RerouteForTraffic;
import com.navdy.service.library.events.preferences.NotificationPreferences;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

public class HereNavigationManager {
    private static final ArrivalEvent ARRIVAL_EVENT = new ArrivalEvent();
    private static final int ARRIVAL_GEO_FENCE_THRESHOLD = 804;
    static final String EMPTY_STR = "";
    private static final int ETA_CALCULATION_INTERVAL = 30000;
    private static final int ETA_UPDATE_THRESHOLD = 60000;
    private static Map<String, String> LANGUAGE_HIERARCHY = new HashMap();
    static final SpeedWarning SPEED_EXCEEDED = new SpeedWarning(true);
    static final SpeedWarning SPEED_NORMAL = new SpeedWarning(false);
    private static final String TAG_AUDIO_CALLBACK = "[CB-AUDIO]";
    private static final String TAG_GPS_CALLBACK = "[CB-GPS]";
    private static final String TAG_NAV_MANAGER_CALLBACK = "[CB-NAVM]";
    private static final String TAG_NEW_MANEUVER_CALLBACK = "[CB-NEWM]";
    private static final String TAG_POS_UPDATE_CALLBACK = "[CB-POS-UPDATE]";
    private static final String TAG_REROUTE_CALLBACK = "[CB-REROUTE]";
    private static final String TAG_SAFETY_CALLBACK = "[CB-SAFE]";
    private static final String TAG_SHOWLANE_INFO_CALLBACK = "[CB-LANEINFO]";
    private static final String TAG_SPEED_CALLBACK = "[CB-SPD]";
    private static final String TAG_TRAFFIC_ETA_TRACKER = "[CB-TRAFFIC-ETA]";
    private static final String TAG_TRAFFIC_REROUTE_CALLBACK = "[CB-TRAFFIC-REROUTE]";
    private static final String TAG_TRAFFIC_UPDATE = "[CB-TRAFFIC-UPDATE]";
    private static final String TAG_TRAFFIC_WARN = "[CB-TRAFFIC-WARN]";
    private static final String TAG_TTS = "[CB-TTS]";
    private static final String TBT_LANGUAGE_CODE = "en-US";
    private static final boolean VERBOSE = false;
    private static Context context = HudApplication.getAppContext();
    private static final HereMapsManager hereMapsManager = HereMapsManager.getInstance();
    private static final MapsEventHandler mapsEventHandler = MapsEventHandler.getInstance();
    static final Logger sLogger = new Logger(HereNavigationManager.class);
    private static final HereNavigationManager sSingleton = new HereNavigationManager();
    private static final SpeedManager speedManager = SpeedManager.getInstance();
    private static final Object trafficRerouteLock = new Object();
    final GpsStatusChange BUS_GPS_SIGNAL_LOST = new GpsStatusChange(false);
    final GpsStatusChange BUS_GPS_SIGNAL_RESTORED = new GpsStatusChange(true);
    private final String HERE_TBT_AUDIO_DESTINATION_REACHED_PATTERN;
    private String INVALID_ROUTE_ID;
    private String INVALID_STATE;
    private String NO_CURRENT_POSITION;
    String TTS_KMS;
    String TTS_METERS;
    String TTS_MILES;
    LocalSpeechRequest TTS_REROUTING;
    LocalSpeechRequest TTS_REROUTING_FAILED;
    private AudioPlayerDelegate audioPlayerDelegate;
    private NetworkBandwidthController bandwidthController = NetworkBandwidthController.getInstance();
    private Bus bus = MapsEventHandler.getInstance().getBus();
    private Maneuver currentManeuver;
    private HereNavigationInfo currentNavigationInfo = new HereNavigationInfo();
    private NavigationMode currentNavigationMode;
    private NavigationSessionState currentSessionState;
    private String debugManeuverDistance;
    private int debugManeuverIcon;
    private String debugManeuverInstruction;
    private ManeuverState debugManeuverState;
    private int debugNextManeuverIcon;
    private DriverSessionPreferences driverSessionPreferences;
    private Runnable etaCalcRunnable = new Runnable() {
        public void run() {
            if (HereNavigationManager.this.currentNavigationMode != NavigationMode.MAP) {
                long time = System.currentTimeMillis() - HereNavigationManager.this.currentNavigationInfo.lastManeuverPostTime;
                if (time < 0) {
                    HereNavigationManager.this.handler.postDelayed(this, 30000);
                } else if (time < 60000) {
                    if (HereNavigationManager.sLogger.isLoggable(2)) {
                        HereNavigationManager.sLogger.v("etaCalc threshold not met:" + time);
                    }
                    HereNavigationManager.this.handler.postDelayed(this, 30000);
                } else {
                    HereNavigationManager.this.refreshNavigationInfo();
                    if (HereNavigationManager.sLogger.isLoggable(2)) {
                        HereNavigationManager.sLogger.v("etaCalc updated maneuver:" + time);
                    }
                    HereNavigationManager.this.handler.postDelayed(this, 30000);
                }
            }
        }
    };
    private Boolean generatePhoneticTTS;
    private HereGpsSignalListener gpsSignalListener;
    private Handler handler = new Handler(Looper.getMainLooper());
    private HereNavController hereNavController;
    private HereRealisticViewListener hereRealisticViewListener;
    private HereLaneInfoListener laneInfoListener;
    private Maneuver maneuverAfterCurrent;
    private HereMapController mapController;
    private MapView mapView;
    private NavdyTrafficRerouteManager navdyTrafficRerouteManager;
    private NavigationManager navigationManager;
    private HereNavigationEventListener navigationManagerEventListener;
    private NavigationView navigationView;
    private HereNewManeuverListener newManeuverEventListener;
    private HerePositionUpdateListener positionUpdateListener;
    private Maneuver prevManeuver;
    private HereRerouteListener reRouteListener;
    private HereSafetySpotListener safetySpotListener;
    private SharedPreferences sharedPreferences = MapsEventHandler.getInstance().getSharedPreferences();
    private HereSpeedWarningManager speedWarningManager;
    private StringBuilder stringBuilder = new StringBuilder();
    private boolean switchingToNewRoute;
    private TimeHelper timeHelper;
    private HereTrafficETATracker trafficEtaTracker;
    private HereTrafficRerouteListener trafficRerouteListener;
    private HereTrafficUpdater2 trafficUpdater;
    private boolean trafficUpdaterStarted;

    static {
        LANGUAGE_HIERARCHY.put("en-AU", "en-GB");
        LANGUAGE_HIERARCHY.put("en-GB", HudLocale.DEFAULT_LANGUAGE);
        LANGUAGE_HIERARCHY.put(TBT_LANGUAGE_CODE, HudLocale.DEFAULT_LANGUAGE);
    }

    public static HereNavigationManager getInstance() {
        return sSingleton;
    }

    private HereNavigationManager() {
        String language = Locale.getDefault().getLanguage();
        Resources resources = HudApplication.getAppContext().getResources();
        this.TTS_REROUTING = new LocalSpeechRequest(new Builder().words(resources.getString(R.string.tts_recalculate_route)).category(Category.SPEECH_REROUTE).language(language).build());
        this.TTS_REROUTING_FAILED = new LocalSpeechRequest(new Builder().words(resources.getString(R.string.tts_recalculate_route_failed)).category(Category.SPEECH_REROUTE).language(language).build());
        this.TTS_MILES = resources.getString(R.string.tts_miles_per_hour);
        this.TTS_KMS = resources.getString(R.string.tts_kms_per_hour);
        this.TTS_METERS = resources.getString(R.string.tts_meters_per_second);
        this.INVALID_STATE = resources.getString(R.string.invalid_state);
        this.NO_CURRENT_POSITION = resources.getString(R.string.no_current_position);
        this.INVALID_ROUTE_ID = resources.getString(R.string.invalid_routeid);
        this.HERE_TBT_AUDIO_DESTINATION_REACHED_PATTERN = resources.getString(R.string.tbt_audio_destination_reached_pattern).toLowerCase();
        this.currentSessionState = NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
        this.timeHelper = RemoteDeviceManager.getInstance().getTimeHelper();
        initNavigationManager();
    }

    private void initNavigationManager() {
        this.navigationManager = NavigationManager.getInstance();
        this.hereNavController = new HereNavController(this.navigationManager, this.bus);
        this.mapController = hereMapsManager.getMapController();
        this.navigationManagerEventListener = new HereNavigationEventListener(sLogger, TAG_NAV_MANAGER_CALLBACK, this);
        this.navigationManager.addNavigationManagerEventListener(new WeakReference(this.navigationManagerEventListener));
        this.newManeuverEventListener = new HereNewManeuverListener(sLogger, TAG_NEW_MANEUVER_CALLBACK, false, this.hereNavController, this, this.bus);
        this.navigationManager.addNewInstructionEventListener(new WeakReference(this.newManeuverEventListener));
        this.gpsSignalListener = new HereGpsSignalListener(sLogger, TAG_GPS_CALLBACK, this.bus, mapsEventHandler, this);
        this.navigationManager.addGpsSignalListener(new WeakReference(this.gpsSignalListener));
        this.speedWarningManager = new HereSpeedWarningManager(TAG_SPEED_CALLBACK, this.bus, mapsEventHandler, this);
        this.safetySpotListener = new HereSafetySpotListener(this.bus, this.mapController);
        this.navigationManager.addSafetySpotListener(new WeakReference(this.safetySpotListener));
        this.reRouteListener = new HereRerouteListener(sLogger, TAG_REROUTE_CALLBACK, this, this.bus);
        this.navigationManager.addRerouteListener(new WeakReference(this.reRouteListener));
        if (!DeviceUtil.isUserBuild() && MapSettings.isLaneGuidanceEnabled()) {
            this.laneInfoListener = new HereLaneInfoListener(this.bus, this.hereNavController);
        }
        this.hereRealisticViewListener = new HereRealisticViewListener(this.bus, this.hereNavController);
        this.positionUpdateListener = new HerePositionUpdateListener(TAG_POS_UPDATE_CALLBACK, this.hereNavController, this, this.bus);
        this.navigationManager.addPositionListener(new WeakReference(this.positionUpdateListener));
        this.trafficRerouteListener = new HereTrafficRerouteListener(TAG_TRAFFIC_REROUTE_CALLBACK, true, this.hereNavController, this, this.bus);
        this.navdyTrafficRerouteManager = new NavdyTrafficRerouteManager(this, this.trafficRerouteListener, this.bus);
        this.trafficRerouteListener.setNavdyTrafficRerouteManager(this.navdyTrafficRerouteManager);
        this.trafficUpdater = new HereTrafficUpdater2(this.bus);
        this.trafficEtaTracker = new HereTrafficETATracker(this.hereNavController, this.bus);
        this.navigationManager.addNavigationManagerEventListener(new WeakReference(this.trafficEtaTracker));
        setMapUpdateMode();
        setupNavigationTTS();
        setNavigationManagerUnit();
        this.driverSessionPreferences = DriverProfileHelper.getInstance().getDriverProfileManager().getSessionPreferences();
        if (this.bandwidthController.isLimitBandwidthModeOn()) {
            sLogger.v("limitbandwidth: initial on");
            setBandwidthPreferences();
        } else {
            setTrafficRerouteMode(mapsEventHandler.getNavigationPreferences().rerouteForTraffic);
        }
        setGeneratePhoneticTTS(Boolean.TRUE.equals(mapsEventHandler.getNavigationPreferences().phoneticTurnByTurn));
        this.bus.register(this);
        sLogger.v("start speed warning manager");
        this.speedWarningManager.start();
        if (!setNavigationMode(NavigationMode.MAP, null, null)) {
            sLogger.e("navigation mode cannot be switched to map");
        }
    }

    public void setMapView(MapView mapView, NavigationView navigationView) {
        this.mapView = mapView;
        this.navigationView = navigationView;
    }

    @Subscribe
    public void onNavigationPreferences(NavigationPreferences preferences) {
        setGeneratePhoneticTTS(Boolean.TRUE.equals(preferences.phoneticTurnByTurn));
    }

    @Subscribe
    public void onNotificationPreference(NotificationPreferences preferences) {
        if (!preferences.enabled.booleanValue()) {
            sLogger.v("glances, disabled, turn traffic notif off");
            resetTrafficRerouteListener();
            stopTrafficRerouteListener();
        } else if (GlanceHelper.isTrafficNotificationEnabled()) {
            sLogger.v("glances, enabled, turn traffic notif on if not on");
            startTrafficRerouteListener();
        } else {
            sLogger.v("traffic glances disabled, turn traffic notif off");
            resetTrafficRerouteListener();
            stopTrafficRerouteListener();
        }
    }

    @Subscribe
    public void onTrafficRerouteConfirm(TrafficRerouteAction action) {
        if (action == TrafficRerouteAction.REROUTE) {
            this.trafficRerouteListener.confirmReroute();
        } else {
            this.trafficRerouteListener.dismissReroute();
        }
    }

    private void tearDownCurrentRoute() {
        removeCurrentRoute();
        removeDestinationMarker();
        this.currentNavigationInfo.mapDestinationMarker = null;
        this.trafficUpdater.setRoute(null);
        this.trafficRerouteListener.dismissReroute();
        this.bus.post(new TrafficRerouteDismissEvent());
        this.bus.post(new LiveTrafficDismissEvent());
    }

    public synchronized boolean removeCurrentRoute() {
        boolean z;
        try {
            MapRoute current = this.currentNavigationInfo.mapRoute;
            if (current != null) {
                sLogger.v("removed map route:" + current);
                this.currentNavigationInfo.mapRoute = null;
                this.mapController.removeMapObject(current);
                z = true;
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
        z = false;
        return z;
    }

    public synchronized void addCurrentRoute(MapRoute mapRoute) {
        try {
            if (this.currentNavigationInfo.mapRoute != null) {
                removeCurrentRoute();
            }
            if (mapRoute != null) {
                this.currentNavigationInfo.mapRoute = mapRoute;
                this.mapController.addMapObject(mapRoute);
                sLogger.v("addCurrentRoute:" + mapRoute);
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
        return;
    }

    private VoiceSkin findSkinSupporting(List<VoiceSkin> skins, String languageCode) {
        if (skins == null) {
            return null;
        }
        if (languageCode == null) {
            return null;
        }
        String desiredLanguage = languageCode.toLowerCase(Locale.US);
        for (VoiceSkin skin : skins) {
            if (skin.getLanguageCode().toLowerCase(Locale.US).startsWith(desiredLanguage)) {
                return skin;
            }
        }
        return null;
    }

    private void dumpVoiceSkinInfo(List<VoiceSkin> skins) {
        StringBuffer buffer = new StringBuffer("Supported voice skins: ");
        for (VoiceSkin skin : skins) {
            buffer.append("[language:");
            buffer.append(skin.getLanguage());
            buffer.append(",code:");
            buffer.append(skin.getLanguageCode());
            buffer.append("],");
        }
        sLogger.i(buffer.toString());
    }

    private void setupNavigationTTS() {
        Locale currentLocale = HudLocale.getCurrentLocale(HudApplication.getAppContext());
        String regionalLanguage = currentLocale.toLanguageTag();
        String baseLanguage = HudLocale.getBaseLanguage(regionalLanguage);
        List<VoiceSkin> localSkins = VoiceCatalog.getInstance().getLocalVoiceSkins();
        VoiceSkin selectedSkin = null;
        if (localSkins != null) {
            while (regionalLanguage != null && selectedSkin == null) {
                selectedSkin = findSkinSupporting(localSkins, regionalLanguage);
                regionalLanguage = (String) LANGUAGE_HIERARCHY.get(regionalLanguage);
            }
            if (selectedSkin == null) {
                selectedSkin = findSkinSupporting(localSkins, baseLanguage);
            }
            if (selectedSkin == null) {
                selectedSkin = findSkinSupporting(localSkins, TBT_LANGUAGE_CODE);
            }
        }
        if (selectedSkin == null) {
            sLogger.e("No voice skin found for default locale:" + HereMapUtil.TBT_ISO3_LANG_CODE);
            return;
        }
        sLogger.d("Found voice skin lang[" + selectedSkin.getLanguage() + " code[" + selectedSkin.getLanguageCode() + "] for locale:" + currentLocale);
        this.navigationManager.setVoiceSkin(selectedSkin);
        hereMapsManager.setVoiceSkinsLoaded();
        this.audioPlayerDelegate = new AudioPlayerDelegate() {
            public boolean playText(String s) {
                HereNavigationManager.sLogger.v("[CB-TTS] [" + s + "]");
                if (HereNavigationManager.this.currentNavigationMode == NavigationMode.MAP) {
                    HereNavigationManager.sLogger.i("[CB-TTS] not navigating");
                } else {
                    boolean hasArrivedPattern = false;
                    if (s != null && s.toLowerCase().contains(HereNavigationManager.this.HERE_TBT_AUDIO_DESTINATION_REACHED_PATTERN)) {
                        hasArrivedPattern = true;
                    }
                    if (hasArrivedPattern && !HereNavigationManager.this.currentNavigationInfo.hasArrived) {
                        HereNavigationManager.sLogger.v("last maneuver marked arrived tts:" + HereNavigationManager.this.currentNavigationInfo.lastManeuver);
                        if (!HereNavigationManager.this.currentNavigationInfo.lastManeuver) {
                            HereNavigationManager.this.setLastManeuver();
                        }
                    }
                    if (HereNavigationManager.this.getNavigationSessionPreference().spokenTurnByTurn && !HereNavigationManager.this.currentNavigationInfo.hasArrived) {
                        if (HereNavigationManager.this.currentNavigationInfo.lastManeuver && HereNavigationManager.this.currentNavigationInfo.destinationDirection != null && HereNavigationManager.this.currentNavigationInfo.destinationDirection != DestinationDirection.UNKNOWN && hasArrivedPattern) {
                            HereNavigationManager.sLogger.v("Found destination reached tts[" + s + "]");
                            Context access$400 = HereNavigationManager.context;
                            Object[] objArr = new Object[1];
                            objArr[0] = HereNavigationManager.this.currentNavigationInfo.destinationDirection == DestinationDirection.LEFT ? HereManeuverDisplayBuilder.DESTINATION_LEFT : HereManeuverDisplayBuilder.DESTINATION_RIGHT;
                            s = access$400.getString(R.string.tbt_audio_destination_reached, objArr);
                            HereNavigationManager.sLogger.v("Converted destination reached tts[" + s + "]");
                        }
                        HereNavigationManager.this.bus.post(RouteCalculationNotification.CANCEL_TBT_TTS);
                        TTSUtils.sendSpeechRequest(s, Category.SPEECH_TURN_BY_TURN, RouteCalculationNotification.ROUTE_TBT_TTS_ID);
                    }
                }
                return true;
            }

            public boolean playFiles(String[] strings) {
                return true;
            }
        };
        this.navigationManager.getAudioPlayer().setDelegate(this.audioPlayerDelegate);
    }

    public void updateMapRoute(Route newRoute, RerouteReason reason, String routeId, String via, boolean sendStartManeuver, boolean includedTraffic) {
        updateMapRoute(newRoute, reason, this.currentNavigationInfo.navigationRouteRequest, routeId, via, sendStartManeuver, includedTraffic);
    }

    public synchronized void updateMapRoute(Route newRoute, RerouteReason reason, NavigationRouteRequest navigationRouteRequest, String routeId, String viaStr, boolean sendStartManeuver, boolean includedTraffic) {
        boolean updateDisplay = false;
        if (!(this.currentNavigationInfo.mapRoute == null && this.currentNavigationInfo.mapDestinationMarker == null)) {
            tearDownCurrentRoute();
            this.currentNavigationInfo.destinationDirection = null;
            this.currentNavigationInfo.destinationDirectionStr = null;
            this.currentNavigationInfo.destinationIconId = -1;
            if (newRoute == null || reason == null) {
                updateDisplay = true;
                this.currentNavigationInfo.hasArrived = false;
                this.currentNavigationInfo.ignoreArrived = false;
                this.currentNavigationInfo.lastManeuver = false;
                this.currentNavigationInfo.arrivedManeuverDisplay = null;
            }
        }
        if (newRoute != null) {
            MapRoute mapRoute = new MapRoute(newRoute);
            mapRoute.setTrafficEnabled(true);
            addCurrentRoute(mapRoute);
            this.currentNavigationInfo.mapRoute.setTrafficEnabled(true);
            this.bus.post(new NewRouteAdded(reason));
            this.newManeuverEventListener.setNewRoute();
            if (sendStartManeuver) {
                sendStartManeuver(newRoute);
                sLogger.v("updateMapRoute:sending start maneuver");
            }
            sLogger.v("map route added:" + this.currentNavigationInfo.mapRoute);
            removeDestinationMarker();
            this.currentNavigationInfo.mapDestinationMarker = null;
            this.currentNavigationInfo.currentRerouteReason = reason;
            addDestinationMarker(newRoute.getDestination(), R.drawable.icon_pin_dot_destination);
            this.trafficUpdater.setRoute(this.currentNavigationInfo.mapRoute.getRoute());
            if (reason != null) {
                final String id = routeId != null ? routeId : UUID.randomUUID().toString();
                final String oldRouteId = this.currentNavigationInfo.routeId;
                final NavigationRouteRequest routeRequest = this.currentNavigationInfo.navigationRouteRequest;
                if (reason == RerouteReason.NAV_SESSION_FUEL_REROUTE) {
                    this.currentNavigationInfo.navigationRouteRequest = navigationRouteRequest;
                    this.currentNavigationInfo.routeOptions = hereMapsManager.getRouteOptions();
                    this.currentNavigationInfo.streetAddress = HereMapUtil.parseStreetAddress(navigationRouteRequest.streetAddress);
                    this.currentNavigationInfo.destinationLabel = null;
                } else if (reason == RerouteReason.NAV_SESSION_TRAFFIC_REROUTE) {
                    this.currentNavigationInfo.trafficRerouteOnce = true;
                }
                final String str = viaStr;
                final Route route = newRoute;
                final boolean z = includedTraffic;
                final RerouteReason rerouteReason = reason;
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        HereRouteCalculator routeCalculator = new HereRouteCalculator(HereNavigationManager.sLogger, false);
                        String via = str;
                        if (TextUtils.isEmpty(via)) {
                            via = HereRouteViaGenerator.getViaString(route);
                            if (TextUtils.isEmpty(via)) {
                                via = "";
                            }
                        }
                        String label = null;
                        String streetAddress = null;
                        List<RouteAttribute> routeAttributes = null;
                        NavigationRouteRequest navRequest = HereNavigationManager.this.currentNavigationInfo.navigationRouteRequest;
                        if (navRequest != null) {
                            label = navRequest.label;
                            streetAddress = navRequest.streetAddress;
                            routeAttributes = navRequest.routeAttributes;
                        }
                        NavigationRouteResult result = routeCalculator.getRouteResultFromRoute(id, route, via, label, streetAddress, true);
                        HereNavigationManager.this.currentNavigationInfo.routeId = id;
                        HereNavigationManager.this.currentNavigationInfo.route = route;
                        HereRouteCache.getInstance().addRoute(id, new RouteInfo(route, navRequest, result, z, HereNavigationManager.hereMapsManager.getLocationFixManager().getLastGeoCoordinate()));
                        HereNavigationManager.sLogger.v("new route added to cache");
                        HereNavigationManager.this.postNavigationSessionStatusEvent(NavigationSessionState.NAV_SESSION_REROUTED, false);
                        NavigationSessionRouteChange routeChange = new NavigationSessionRouteChange(oldRouteId, result, rerouteReason);
                        if (routeAttributes != null && routeAttributes.contains(RouteAttribute.ROUTE_ATTRIBUTE_GAS)) {
                            HereMapUtil.saveGasRouteInfo(navRequest, HereNavigationManager.this.currentNavigationInfo.deviceId, HereNavigationManager.sLogger);
                        }
                        HereNavigationManager.this.bus.post(routeChange);
                        HereNavigationManager.this.bus.post(new RemoteEvent(routeChange));
                        HereNavigationManager.sLogger.v("NavigationSessionRouteChange sent to client oldRoute[" + oldRouteId + "] newRoute[" + id + "] reason[" + rerouteReason + "] via[" + via + "]");
                        if (HereNavigationManager.sLogger.isLoggable(2)) {
                            HereMapUtil.printRouteDetails(route, routeRequest.streetAddress, routeRequest, null);
                        }
                    }
                }, 2);
            }
        } else {
            hereMapsManager.getLocationFixManager().addMarkers(this.mapController);
        }
        if (updateDisplay) {
            refreshNavigationInfo();
        }
    }

    public synchronized void setReroute(Route route, RerouteReason rerouteReason, String routeId, String via, boolean sendStartManeuver, boolean trafficConsidered) {
        sLogger.v("setReroute:" + sendStartManeuver);
        if (isNavigationModeOn()) {
            clearNavManeuver();
            Error error = this.hereNavController.setRoute(route);
            if (error == Error.NONE) {
                updateMapRoute(route, rerouteReason, routeId, via, sendStartManeuver, trafficConsidered);
                sLogger.i("setReroute: done");
            } else {
                sLogger.e("setReroute: route could not be set:" + error);
            }
        } else {
            sLogger.v("setReroute: nav mode ended");
        }
    }

    public void postNavigationSessionStatusEvent(boolean sendRouteResult) {
        postNavigationSessionStatusEvent(this.currentSessionState, sendRouteResult);
    }

    public void postNavigationSessionStatusEvent(final NavigationSessionState sessionState, final boolean sendRouteResult) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                String label = null;
                NavigationRouteResult result = null;
                String routeId = null;
                NavigationRouteRequest navRequest = HereNavigationManager.this.currentNavigationInfo.navigationRouteRequest;
                if (navRequest != null) {
                    label = navRequest.label;
                    routeId = HereNavigationManager.this.currentNavigationInfo.routeId;
                    if (sendRouteResult) {
                        RouteInfo routeInfo = HereRouteCache.getInstance().getRoute(routeId);
                        if (routeInfo != null) {
                            result = routeInfo.routeResult;
                            int freeFlowDuration = result.duration.intValue();
                            int trafficDuration = result.duration_traffic.intValue();
                            Date noTraffic = HereNavigationManager.this.hereNavController.getEta(true, TrafficPenaltyMode.DISABLED);
                            Date traffic = HereNavigationManager.this.hereNavController.getEta(true, TrafficPenaltyMode.OPTIMAL);
                            long now = System.currentTimeMillis();
                            if (HereMapUtil.isValidEtaDate(noTraffic)) {
                                freeFlowDuration = ((int) (noTraffic.getTime() - now)) / 1000;
                            }
                            if (HereMapUtil.isValidEtaDate(traffic)) {
                                trafficDuration = ((int) (traffic.getTime() - now)) / 1000;
                            }
                            int length = (int) HereNavigationManager.this.hereNavController.getDestinationDistance();
                            result = new NavigationRouteResult.Builder(result).duration(Integer.valueOf(freeFlowDuration)).duration_traffic(Integer.valueOf(trafficDuration)).length(Integer.valueOf(length)).build();
                            HereNavigationManager.sLogger.v("NavigationSessionStatusEvent new data duration[" + freeFlowDuration + "] traffic[" + trafficDuration + "] length[" + length + "]");
                        }
                    }
                }
                if (routeId == null && sessionState == NavigationSessionState.NAV_SESSION_STOPPED) {
                    NavigationRouteRequest lastRequest = HereNavigationManager.this.currentNavigationInfo.getLastNavigationRequest();
                    if (lastRequest != null) {
                        label = lastRequest.label;
                        routeId = HereNavigationManager.this.currentNavigationInfo.getLastRouteId();
                    }
                }
                NavigationSessionStatusEvent navigationSessionState = new NavigationSessionStatusEvent(sessionState, label, routeId, result, HereNavigationManager.this.currentNavigationInfo.destinationIdentifier);
                HereNavigationManager.mapsEventHandler.sendEventToClient(navigationSessionState);
                HereNavigationManager.sLogger.v("posted NavigationSessionStatusEvent state[" + navigationSessionState.sessionState + "] label[" + navigationSessionState.label + "] routeId[" + navigationSessionState.routeId + "]" + "] result[" + (result == null ? Constants.NULL_VERSION_ID : result.via) + "]");
            }
        }, 2);
    }

    protected void returnResponse(RequestStatus status, String errorText, NavigationSessionState pendingState, String routeId) {
        if (status != RequestStatus.REQUEST_SUCCESS) {
            sLogger.e(status + ": " + errorText);
        } else {
            sLogger.i(DialConstants.DIAL_PAIRING_SUCCESS);
        }
        mapsEventHandler.sendEventToClient(new NavigationSessionResponse(status, errorText, pendingState, routeId));
    }

    public void handleNavigationSessionRequest(final NavigationSessionRequest event) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                HereNavigationManager.this.handleNavigationSessionRequestInternal(event);
            }
        }, 20);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void handleNavigationSessionRequestInternal(NavigationSessionRequest event) {
        GenericUtil.checkNotOnMainThread();
        try {
            sLogger.v("NavigationSessionRequest id[" + event.routeId + "] newState[" + event.newState + "] label[" + event.label + "] sim-speed[" + event.simulationSpeed + "] currentState[" + this.currentSessionState + "]");
            if (HereMapUtil.isValidNavigationState(event.newState)) {
                if (hereMapsManager.getLocationFixManager().getLastGeoCoordinate() != null) {
                    RouteInfo routeInfo = null;
                    boolean routeIdProvided = false;
                    if (event.routeId != null) {
                        routeIdProvided = true;
                        routeInfo = HereRouteCache.getInstance().getRoute(event.routeId);
                    }
                    long l1;
                    if (!isNavigationModeOn()) {
                        sLogger.v("navigation is not active");
                        switch (event.newState) {
                            case NAV_SESSION_PAUSED:
                                sLogger.e("cannot pause navigation current state:" + this.currentSessionState);
                                returnResponse(RequestStatus.REQUEST_INVALID_STATE, context.getString(R.string.cannot_move_state, new Object[]{this.currentSessionState.name(), event.newState.name()}), null, event.routeId);
                                break;
                            case NAV_SESSION_STOPPED:
                                sLogger.v("already stopped");
                                returnResponse(RequestStatus.REQUEST_SUCCESS, "", NavigationSessionState.NAV_SESSION_STOPPED, null);
                                break;
                            case NAV_SESSION_STARTED:
                                if (routeInfo != null) {
                                    l1 = SystemClock.elapsedRealtime();
                                    startNavigation(event, routeInfo);
                                    sLogger.v("navigation took[" + (SystemClock.elapsedRealtime() - l1) + "]");
                                    break;
                                }
                                sLogger.e("valid route id required");
                                returnResponse(RequestStatus.REQUEST_INVALID_REQUEST, this.INVALID_ROUTE_ID, null, event.routeId);
                                break;
                        }
                    }
                    sLogger.v("navigation is active");
                    StringBuilder error;
                    switch (event.newState) {
                        case NAV_SESSION_PAUSED:
                            if (this.currentSessionState != NavigationSessionState.NAV_SESSION_STARTED) {
                                sLogger.v("already paused");
                                returnResponse(RequestStatus.REQUEST_SUCCESS, "", NavigationSessionState.NAV_SESSION_PAUSED, null);
                                break;
                            }
                            sLogger.v("pausing navigation");
                            this.hereNavController.pause();
                            this.currentSessionState = NavigationSessionState.NAV_SESSION_PAUSED;
                            returnResponse(RequestStatus.REQUEST_SUCCESS, "", NavigationSessionState.NAV_SESSION_PAUSED, event.routeId);
                            postNavigationSessionStatusEvent(false);
                            break;
                        case NAV_SESSION_STOPPED:
                            sLogger.v("stopping navigation");
                            error = new StringBuilder();
                            if (!setNavigationMode(NavigationMode.MAP, null, error)) {
                                returnResponse(RequestStatus.REQUEST_SERVICE_ERROR, error.toString(), null, event.routeId);
                                break;
                            }
                            this.navigationView.resetMapOverview();
                            returnResponse(RequestStatus.REQUEST_SUCCESS, "", this.currentSessionState, event.routeId);
                            break;
                        case NAV_SESSION_STARTED:
                            if (routeInfo != null || !routeIdProvided) {
                                boolean isRouteIdCurrent = event.routeId == null || TextUtils.equals(event.routeId, this.currentNavigationInfo.routeId);
                                if (!isRouteIdCurrent) {
                                    sLogger.v("different route: switching to new route");
                                    error = new StringBuilder();
                                    sLogger.v("stop navigation");
                                    l1 = SystemClock.elapsedRealtime();
                                    this.switchingToNewRoute = true;
                                    if (setNavigationMode(NavigationMode.MAP, null, error)) {
                                        sLogger.v("start navigation");
                                        startNavigation(event, routeInfo);
                                        sLogger.v("navigation took[" + (SystemClock.elapsedRealtime() - l1) + "]");
                                    } else {
                                        returnResponse(RequestStatus.REQUEST_SERVICE_ERROR, error.toString(), null, event.routeId);
                                    }
                                    this.switchingToNewRoute = false;
                                    break;
                                }
                                sLogger.v("same route");
                                if (this.currentSessionState != NavigationSessionState.NAV_SESSION_STARTED) {
                                    sLogger.v("resume navigation");
                                    Error error2 = this.hereNavController.resume();
                                    if (error2 != Error.NONE) {
                                        sLogger.e("cannot resume navigation:" + error2);
                                        returnResponse(RequestStatus.REQUEST_SERVICE_ERROR, context.getString(R.string.unable_to_resume, new Object[]{error2}), NavigationSessionState.NAV_SESSION_STARTED, event.routeId);
                                        if (!setNavigationMode(NavigationMode.MAP, null, null)) {
                                            sLogger.w("cannot even go to tracking mode");
                                            break;
                                        }
                                    }
                                    this.currentSessionState = NavigationSessionState.NAV_SESSION_STARTED;
                                    returnResponse(RequestStatus.REQUEST_SUCCESS, "", NavigationSessionState.NAV_SESSION_STARTED, event.routeId);
                                    postNavigationSessionStatusEvent(false);
                                    break;
                                }
                                returnResponse(RequestStatus.REQUEST_SUCCESS, "", NavigationSessionState.NAV_SESSION_STARTED, event.routeId);
                                break;
                            }
                            returnResponse(RequestStatus.REQUEST_INVALID_REQUEST, this.INVALID_ROUTE_ID, null, event.routeId);
                            break;
                            break;
                    }
                }
                returnResponse(RequestStatus.REQUEST_NO_LOCATION_SERVICE, this.NO_CURRENT_POSITION, null, event.routeId);
            } else {
                returnResponse(RequestStatus.REQUEST_INVALID_REQUEST, this.INVALID_STATE, null, event.routeId);
            }
        } catch (Throwable t) {
            sLogger.e("handleNavigationSessionRequest", t);
            returnResponse(RequestStatus.REQUEST_SERVICE_ERROR, t.toString(), null, event.routeId);
        }
        return;
    }

    private void startNavigation(NavigationSessionRequest event, RouteInfo routeInfo) {
        HereNavigationInfo navigationInfo = new HereNavigationInfo();
        navigationInfo.navigationRouteRequest = routeInfo.routeRequest;
        navigationInfo.startLocation = routeInfo.routeStartPoint;
        navigationInfo.routeOptions = hereMapsManager.getRouteOptions();
        navigationInfo.waypoints = new ArrayList();
        for (Coordinate waypoint : routeInfo.routeRequest.waypoints) {
            navigationInfo.waypoints.add(new GeoCoordinate(waypoint.latitude.doubleValue(), waypoint.longitude.doubleValue()));
        }
        navigationInfo.destination = new GeoCoordinate(routeInfo.routeRequest.destination.latitude.doubleValue(), routeInfo.routeRequest.destination.longitude.doubleValue());
        navigationInfo.route = routeInfo.route;
        navigationInfo.simulationSpeed = event.simulationSpeed.intValue();
        navigationInfo.routeId = event.routeId;
        navigationInfo.deviceId = RemoteDeviceManager.getInstance().getDeviceId();
        NavdyDeviceId lastKnownDeviceId = HereMapUtil.getLastKnownDeviceId();
        if (navigationInfo.deviceId == null) {
            navigationInfo.deviceId = lastKnownDeviceId;
            sLogger.v("lastknown device id = " + navigationInfo.deviceId);
        }
        if (navigationInfo.deviceId == null) {
            DeviceInfo deviceInfo = RemoteDeviceManager.getInstance().getConnectionHandler().getLastConnectedDeviceInfo();
            if (deviceInfo != null) {
                navigationInfo.deviceId = new NavdyDeviceId(deviceInfo.deviceId);
                sLogger.v("device id is null , adding last device:" + deviceInfo.deviceId);
            }
        }
        if (routeInfo.routeRequest.destination_identifier != null) {
            navigationInfo.destinationIdentifier = routeInfo.routeRequest.destination_identifier;
        }
        String streetAddress = null;
        NavigationRouteRequest navRequest = navigationInfo.navigationRouteRequest;
        if (navRequest != null) {
            streetAddress = navRequest.streetAddress;
            navigationInfo.destinationLabel = navRequest.label;
        }
        navigationInfo.streetAddress = HereMapUtil.parseStreetAddress(streetAddress);
        StringBuilder error = new StringBuilder();
        if (setNavigationMode(NavigationMode.MAP_ON_ROUTE, navigationInfo, error)) {
            returnResponse(RequestStatus.REQUEST_SUCCESS, null, this.currentSessionState, event.routeId);
            return;
        }
        returnResponse(RequestStatus.REQUEST_SERVICE_ERROR, error.toString(), this.currentSessionState, event.routeId);
    }

    public void handleConnectionState(boolean connected) {
        if (connected) {
            sLogger.i("client connected");
            if (isNavigationModeOn()) {
                final NavdyDeviceId deviceId = RemoteDeviceManager.getInstance().getDeviceId();
                if (deviceId == null || deviceId.equals(this.currentNavigationInfo.deviceId)) {
                    sLogger.i("navigation still on, device id has not changed");
                    return;
                } else {
                    TaskManager.getInstance().execute(new Runnable() {
                        public void run() {
                            HereNavigationManager.sLogger.i("stop navigation, device id has changed from[" + HereNavigationManager.this.currentNavigationInfo.deviceId + "] to [" + deviceId + "]");
                            HereNavigationManager.this.setNavigationMode(NavigationMode.MAP, null, null);
                        }
                    }, 20);
                    return;
                }
            }
            return;
        }
        sLogger.i("client not connected");
    }

    private static MapUpdateMode getHereMapUpdateMode() {
        return MapUpdateMode.NONE;
    }

    public NavigationMode getCurrentNavigationMode() {
        return this.currentNavigationMode;
    }

    public NavigationSessionState getCurrentNavigationState() {
        return this.currentSessionState;
    }

    public boolean isNavigationModeOn() {
        if (this.hereNavController.getState() == State.NAVIGATING) {
            return true;
        }
        return false;
    }

    synchronized boolean setNavigationMode(NavigationMode navigationMode, HereNavigationInfo navigationInfo, StringBuilder error) {
        boolean z;
        GenericUtil.checkNotOnMainThread();
        sLogger.i("setNavigationMode:" + navigationMode);
        if (this.currentNavigationMode == navigationMode) {
            sLogger.i("setNavigationMode: already in " + navigationMode);
            z = true;
        } else {
            if (navigationInfo == null) {
                if (navigationMode != NavigationMode.MAP) {
                    sLogger.e("setNavigationMode: navigation info cannot be null");
                    if (error != null) {
                        error.append(context.getString(R.string.invalid_param));
                    }
                    z = false;
                }
            }
            NavigationSessionState prevState = this.currentSessionState;
            boolean changeNavigation = true;
            boolean sendStartManeuver = false;
            if (this.currentNavigationMode != null) {
                if ((this.currentNavigationMode == NavigationMode.MAP_ON_ROUTE && navigationMode == NavigationMode.TBT_ON_ROUTE) || (this.currentNavigationMode == NavigationMode.TBT_ON_ROUTE && navigationMode == NavigationMode.MAP_ON_ROUTE)) {
                    sLogger.v("navigation switch not reqd from= " + this.currentNavigationMode + " to=" + navigationMode);
                    this.currentNavigationMode = navigationMode;
                    changeNavigation = false;
                } else {
                    boolean isNavOn = isNavigationModeOn();
                    sLogger.v("remove route before calling nav:stop navon=" + isNavOn);
                    if (isNavOn) {
                        sLogger.v("navigation stopped from:" + this.currentSessionState);
                        updateMapRoute(null, null, null, null, false, false);
                        HereMapUtil.removeRouteInfo(sLogger, false);
                    }
                    this.hereNavController.stopNavigation(true);
                    this.currentSessionState = NavigationSessionState.NAV_SESSION_STOPPED;
                    if (prevState != NavigationSessionState.NAV_SESSION_STOPPED) {
                        postNavigationSessionStatusEvent(false);
                    }
                    prevState = this.currentSessionState;
                }
            }
            this.handler.removeCallbacks(this.etaCalcRunnable);
            this.currentNavigationInfo.clear();
            this.debugManeuverState = null;
            if (changeNavigation) {
                switch (navigationMode) {
                    case MAP:
                        this.hereNavController.stopNavigation(false);
                        this.bus.post(new GPSSpeedEvent());
                        if (!DeviceUtil.isUserBuild() && MapSettings.isLaneGuidanceEnabled()) {
                            this.laneInfoListener.stop();
                        }
                        this.hereRealisticViewListener.stop();
                        this.currentNavigationInfo.copy(navigationInfo);
                        this.currentNavigationMode = NavigationMode.MAP;
                        this.currentSessionState = NavigationSessionState.NAV_SESSION_STOPPED;
                        this.mapController.setMapScheme(hereMapsManager.getTrackingMapScheme());
                        break;
                    case MAP_ON_ROUTE:
                    case TBT_ON_ROUTE:
                        updateMapRoute(navigationInfo.route, null, null, null, false, isTrafficConsidered(navigationInfo.routeId));
                        Error navError = this.hereNavController.startNavigation(navigationInfo.route, navigationInfo.simulationSpeed);
                        if (navError != Error.NONE) {
                            updateMapRoute(null, null, null, null, false, false);
                            this.currentSessionState = NavigationSessionState.NAV_SESSION_ERROR;
                            sLogger.e("cannot switch navigation mode from:" + this.currentNavigationMode + " to=" + navigationMode + " error=" + navError + " simulating=" + (navigationInfo.simulationSpeed > 0 ? "true" : "false"));
                            postNavigationSessionStatusEvent(false);
                            if (error != null) {
                                error.append(navError.name());
                            }
                            z = false;
                            break;
                        }
                        sLogger.v("switched navigation mode from:" + this.currentNavigationMode + " to=" + navigationMode + " simulating=" + (navigationInfo.simulationSpeed > 0 ? "true" : "false"));
                        this.currentNavigationInfo.copy(navigationInfo);
                        this.currentNavigationMode = navigationMode;
                        this.currentSessionState = NavigationSessionState.NAV_SESSION_STARTED;
                        if (!DeviceUtil.isUserBuild() && MapSettings.isLaneGuidanceEnabled()) {
                            this.laneInfoListener.start();
                        }
                        this.hereRealisticViewListener.start();
                        this.mapController.setMapScheme(hereMapsManager.getEnrouteMapScheme());
                        sendStartManeuver = true;
                        this.handler.postDelayed(this.etaCalcRunnable, 30000);
                        if (!this.currentNavigationInfo.navigationRouteRequest.routeAttributes.contains(RouteAttribute.ROUTE_ATTRIBUTE_GAS)) {
                            HereMapUtil.saveRouteInfo(navigationInfo.navigationRouteRequest, navigationInfo.deviceId, sLogger);
                            break;
                        }
                        HereMapUtil.saveGasRouteInfo(navigationInfo.navigationRouteRequest, navigationInfo.deviceId, sLogger);
                        break;
                }
            }
            setTrafficOverlay();
            this.bus.post(new NavigationModeChange(this.currentNavigationMode));
            if (sendStartManeuver) {
                sLogger.v("send start maneuver");
                sendStartManeuver(this.currentNavigationInfo.route);
            }
            if (prevState != this.currentSessionState) {
                postNavigationSessionStatusEvent(false);
            }
            z = true;
        }
        return z;
    }

    void refreshNavigationInfo() {
        updateNavigationInfo(null, null, null, false);
    }

    void updateNavigationInfo(Maneuver nextManeuver, Maneuver maneuverAfterNext, Maneuver previous, boolean clearPrevious) {
        final Maneuver maneuver = nextManeuver;
        final Maneuver maneuver2 = maneuverAfterNext;
        final Maneuver maneuver3 = previous;
        final boolean z = clearPrevious;
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    HereNavigationManager.this.updateNavigationInfoInternal(maneuver, maneuver2, maneuver3, z);
                } catch (Throwable t) {
                    HereNavigationManager.sLogger.e("updateNavigationInfoInternal", t);
                }
            }
        }, 4);
    }

    private synchronized void updateNavigationInfoInternal(Maneuver nextManeuver, Maneuver maneuverAfterNext, Maneuver previous, boolean clearPrevious) {
        boolean calculateEarlyManeuver = true;
        ManeuverDisplay maneuverDisplay;
        if (this.currentNavigationInfo.hasArrived) {
            if (this.currentNavigationInfo.ignoreArrived) {
                sLogger.v("ignore arrived");
            } else {
                try {
                    GeoPosition currentPos = hereMapsManager.getLastGeoPosition();
                    if (currentPos != null) {
                        int distance = (int) currentPos.getCoordinate().distanceTo(this.currentNavigationInfo.destination);
                        if (distance >= ARRIVAL_GEO_FENCE_THRESHOLD) {
                            sLogger.v("we have exceeded geo threshold:" + distance + " , stopping nav");
                            stopNavigation();
                        }
                    }
                } catch (Throwable t) {
                    sLogger.e(t);
                }
                postArrivedManeuver();
            }
        } else if (this.currentSessionState == NavigationSessionState.NAV_SESSION_STARTED || this.currentSessionState == NavigationSessionState.NAV_SESSION_PAUSED) {
            if (nextManeuver == null) {
                nextManeuver = this.currentManeuver;
                maneuverAfterNext = this.maneuverAfterCurrent;
                previous = this.prevManeuver;
                if (!(this.maneuverAfterCurrent == null || this.currentNavigationInfo.maneuverAfterCurrent != this.maneuverAfterCurrent || this.currentNavigationInfo.maneuverState == null || this.currentNavigationInfo.maneuverState == ManeuverState.STAY)) {
                    calculateEarlyManeuver = false;
                }
            } else {
                if (clearPrevious) {
                    sLogger.v("setNewRoute: cleared");
                    this.prevManeuver = null;
                } else {
                    this.prevManeuver = this.currentManeuver;
                }
                this.currentManeuver = nextManeuver;
                this.maneuverAfterCurrent = maneuverAfterNext;
                this.currentNavigationInfo.maneuverAfterCurrent = maneuverAfterNext;
                this.currentNavigationInfo.maneuverAfterCurrentIconid = -1;
                this.currentNavigationInfo.maneuverState = null;
            }
            if (nextManeuver != null) {
                NavigationRouteRequest navigationRouteRequest;
                boolean last = false;
                if (nextManeuver.getAction() == Action.END || nextManeuver.getIcon() == Icon.END) {
                    last = true;
                }
                boolean hasDirectionInfo = false;
                boolean calculateDirection = false;
                if (last) {
                    if (this.currentNavigationInfo.destinationDirection != null) {
                        hasDirectionInfo = true;
                    } else {
                        calculateDirection = true;
                    }
                }
                long distance2 = this.hereNavController.getNextManeuverDistance();
                Maneuver maneuver = this.currentManeuver;
                Maneuver maneuver2 = this.prevManeuver;
                if (calculateDirection) {
                    navigationRouteRequest = this.currentNavigationInfo.navigationRouteRequest;
                } else {
                    navigationRouteRequest = null;
                }
                maneuverDisplay = HereManeuverDisplayBuilder.getManeuverDisplay(maneuver, last, distance2, null, maneuver2, navigationRouteRequest, this.maneuverAfterCurrent, calculateEarlyManeuver, true, this.currentNavigationInfo.destinationDirection);
                if (!last) {
                    if (this.debugManeuverState != null) {
                        maneuverDisplay.maneuverState = this.debugManeuverState;
                    }
                    if (this.debugManeuverInstruction != null) {
                        maneuverDisplay.pendingRoad = this.debugManeuverInstruction;
                    }
                    if (this.debugManeuverIcon != 0) {
                        maneuverDisplay.turnIconId = this.debugManeuverIcon;
                    }
                    if (this.debugManeuverDistance != null) {
                        maneuverDisplay.distanceToPendingRoadText = this.debugManeuverDistance;
                    }
                    if (this.debugNextManeuverIcon != 0) {
                        maneuverDisplay.nextTurnIconId = this.debugNextManeuverIcon;
                    }
                }
                if (last && !this.currentNavigationInfo.lastManeuver) {
                    sLogger.v("last maneuver marked distance:" + distance2);
                    setLastManeuver();
                }
                if (last) {
                    if (!hasDirectionInfo) {
                        this.currentNavigationInfo.destinationDirection = maneuverDisplay.direction;
                        this.currentNavigationInfo.destinationDirectionStr = maneuverDisplay.pendingRoad;
                        this.currentNavigationInfo.destinationIconId = maneuverDisplay.destinationIconId;
                        if (this.currentNavigationInfo.destinationDirection != DestinationDirection.UNKNOWN) {
                            sLogger.v("we have destination marker info: " + this.currentNavigationInfo.destinationDirection);
                            removeDestinationMarker();
                            this.currentNavigationInfo.mapDestinationMarker = null;
                            if (this.currentNavigationInfo.route != null) {
                                addDestinationMarker(this.currentNavigationInfo.route.getDestination(), this.currentNavigationInfo.destinationIconId);
                            }
                        }
                    } else if (this.currentNavigationInfo.destinationDirection != DestinationDirection.UNKNOWN) {
                        maneuverDisplay.pendingRoad = this.currentNavigationInfo.destinationDirectionStr;
                        maneuverDisplay.destinationIconId = this.currentNavigationInfo.destinationIconId;
                    }
                }
                if (this.currentNavigationInfo.maneuverAfterCurrentIconid == -1) {
                    this.currentNavigationInfo.maneuverAfterCurrentIconid = maneuverDisplay.nextTurnIconId;
                    this.currentNavigationInfo.maneuverState = maneuverDisplay.maneuverState;
                } else {
                    maneuverDisplay.nextTurnIconId = this.currentNavigationInfo.maneuverAfterCurrentIconid;
                }
                Date etaDate = this.hereNavController.getEta(true, TrafficPenaltyMode.OPTIMAL);
                if (!HereMapUtil.isValidEtaDate(etaDate)) {
                    etaDate = this.hereNavController.getTtaDate(true, TrafficPenaltyMode.OPTIMAL);
                    if (HereMapUtil.isValidEtaDate(etaDate)) {
                        sLogger.v("intermediate maneuver eta-tta date = " + etaDate);
                    } else {
                        sLogger.v("intermediate maneuver eta-tta date invalid = " + etaDate);
                        etaDate = null;
                    }
                }
                if (etaDate != null) {
                    maneuverDisplay.eta = this.timeHelper.formatTime(etaDate, this.stringBuilder);
                    maneuverDisplay.etaAmPm = this.stringBuilder.toString();
                    maneuverDisplay.etaDate = etaDate;
                }
                boolean isHighway = false;
                RoadElement element = HereMapUtil.getCurrentRoadElement();
                if (element != null) {
                    isHighway = HereMapUtil.isCurrentRoadHighway(element);
                }
                if (HereManeuverDisplayBuilder.getManeuverState(this.hereNavController.getNextManeuverDistance(), isHighway) == ManeuverState.SOON) {
                    this.bus.post(new ManeuverSoonEvent(nextManeuver));
                }
                if (sLogger.isLoggable(2)) {
                    sLogger.d("ManeuverDisplay-nav:" + getHereNavigationState() + " :" + maneuverDisplay.toString());
                    sLogger.v("[ManeuverDisplay-nav-short] " + maneuverDisplay.pendingTurn + " " + maneuverDisplay.pendingRoad);
                }
                this.currentNavigationInfo.lastManeuverPostTime = System.currentTimeMillis();
                this.bus.post(maneuverDisplay);
            } else {
                sLogger.w("there is no next maneuver");
            }
        } else if (this.currentSessionState == NavigationSessionState.NAV_SESSION_STOPPED) {
            this.prevManeuver = null;
            this.currentManeuver = null;
            this.maneuverAfterCurrent = null;
            maneuverDisplay = new ManeuverDisplay();
            maneuverDisplay.currentRoad = HereMapUtil.getCurrentRoadName();
            maneuverDisplay.currentSpeedLimit = HereMapUtil.getCurrentSpeedLimit();
            if (sLogger.isLoggable(2)) {
                sLogger.d("ManeuverDisplay:" + getHereNavigationState() + " :" + maneuverDisplay.toString());
            }
            this.bus.post(maneuverDisplay);
        } else {
            this.prevManeuver = null;
            this.currentManeuver = null;
            this.maneuverAfterCurrent = null;
            sLogger.e("Invalid state:" + this.currentSessionState + " maneuver=" + nextManeuver);
        }
    }

    public synchronized void setMapUpdateMode() {
        GenericUtil.checkNotOnMainThread();
        MapUpdateMode mapUpdateMode = getHereMapUpdateMode();
        sLogger.v("setting here map update mode to[" + mapUpdateMode + "]");
        this.navigationManager.setMapUpdateMode(mapUpdateMode);
    }

    public String getDestinationStreetAddress() {
        return this.currentNavigationInfo.streetAddress;
    }

    public String getDestinationLabel() {
        return this.currentNavigationInfo.destinationLabel;
    }

    public Route getCurrentRoute() {
        return this.currentNavigationInfo.route;
    }

    public String getCurrentRouteId() {
        return this.currentNavigationInfo.routeId;
    }

    public MapRoute getCurrentMapRoute() {
        return this.currentNavigationInfo.mapRoute;
    }

    public NavigationRouteRequest getCurrentNavigationRouteRequest() {
        return this.currentNavigationInfo.navigationRouteRequest;
    }

    public MapMarker getDestinationMarker() {
        return this.currentNavigationInfo.mapDestinationMarker;
    }

    public NavdyDeviceId getCurrentNavigationDeviceId() {
        return this.currentNavigationInfo.deviceId;
    }

    public void sendStartManeuver(Route route) {
        GenericUtil.checkNotOnMainThread();
        if (route != null) {
            List<Maneuver> maneuvers = route.getManeuvers();
            if (maneuvers.size() >= 2) {
                ManeuverDisplay display = HereManeuverDisplayBuilder.getStartManeuverDisplay((Maneuver) maneuvers.get(0), (Maneuver) maneuvers.get(1));
                Date date = this.hereNavController.getEta(true, TrafficPenaltyMode.OPTIMAL);
                if (HereMapUtil.isValidEtaDate(date)) {
                    sLogger.v("start maneuver eta date = " + date);
                } else {
                    date = this.hereNavController.getTtaDate(true, TrafficPenaltyMode.OPTIMAL);
                    if (HereMapUtil.isValidEtaDate(date)) {
                        sLogger.v("start maneuver eta-tta date = " + date);
                    } else {
                        date = HereMapUtil.getRouteTtaDate(route);
                        sLogger.v("start maneuver tta date = " + date);
                    }
                }
                if (date != null) {
                    display.etaDate = date;
                    display.eta = this.timeHelper.formatTime(date, this.stringBuilder);
                    display.etaAmPm = this.stringBuilder.toString();
                }
                if (sLogger.isLoggable(2)) {
                    sLogger.d("ManeuverDisplay:START:" + getHereNavigationState() + " :" + display.toString());
                }
                this.currentNavigationInfo.lastManeuverPostTime = System.currentTimeMillis();
                this.bus.post(display);
                return;
            }
            sLogger.i("start maneuver not sent");
        }
    }

    private void setTrafficRerouteMode(RerouteForTraffic mode) {
        this.navigationManager.setTrafficAvoidanceMode(TrafficAvoidanceMode.MANUAL);
    }

    private void setTrafficOverlay() {
        sLogger.v("enableTraffic on map:" + true);
        if (1 != null) {
            hereMapsManager.setTrafficOverlay(this.currentNavigationMode);
        } else {
            hereMapsManager.clearTrafficOverlay();
        }
    }

    public NavSessionPreferences getNavigationSessionPreference() {
        return this.driverSessionPreferences.getNavigationSessionPreference();
    }

    public void stopAndRemoveAllRoutes() {
        sLogger.v("stopAndRemoveAllRoutes");
        GenericUtil.checkNotOnMainThread();
        HereMapUtil.removeRouteInfo(sLogger, true);
        stopNavigation();
        AnalyticsSupport.recordNavigationCancelled();
    }

    public void stopNavigation() {
        if (isNavigationModeOn()) {
            sLogger.v("stopNavigation: nav mode is on");
            String label = null;
            NavigationRouteRequest navRequest = this.currentNavigationInfo.navigationRouteRequest;
            if (navRequest != null) {
                label = navRequest.label;
            }
            handleNavigationSessionRequest(new NavigationSessionRequest(NavigationSessionState.NAV_SESSION_STOPPED, label, this.currentNavigationInfo.routeId, Integer.valueOf(0), Boolean.valueOf(true)));
            return;
        }
        sLogger.v("stopNavigation: nav mode not on");
    }

    public String getCurrentDestinationIdentifier() {
        return this.currentNavigationInfo.destinationIdentifier;
    }

    public Boolean getGeneratePhoneticTTS() {
        return this.generatePhoneticTTS;
    }

    private void setGeneratePhoneticTTS(boolean phoneticTTS) {
        this.generatePhoneticTTS = Boolean.valueOf(phoneticTTS);
        this.navigationManager.setTtsOutputFormat(phoneticTTS ? TtsOutputFormat.NUANCE : TtsOutputFormat.RAW);
    }

    @Subscribe
    public void onDriverProfileChanged(DriverProfileChanged profileChanged) {
        sLogger.v("driver profile changed");
    }

    @Subscribe
    public void onTimeSettingsChange(UpdateClock event) {
        sLogger.v("time setting changed");
        if (isNavigationModeOn()) {
            refreshNavigationInfo();
        }
    }

    @Subscribe
    public void onSpeedUnitChanged(SpeedUnitChanged event) {
        sLogger.i("speed unit setting has changed to " + speedManager.getSpeedUnit().name() + ", refreshing");
        refreshNavigationInfo();
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                HereNavigationManager.this.setNavigationManagerUnit();
            }
        }, 3);
    }

    private void setNavigationManagerUnit() {
        switch (speedManager.getSpeedUnit()) {
            case KILOMETERS_PER_HOUR:
                this.hereNavController.setDistanceUnit(UnitSystem.METRIC);
                return;
            default:
                this.hereNavController.setDistanceUnit(UnitSystem.IMPERIAL_US);
                return;
        }
    }

    private void addDestinationMarker(GeoCoordinate geoCoordinate, int iconId) {
        try {
            if (this.currentNavigationInfo.mapDestinationMarker != null) {
                removeDestinationMarker();
                this.currentNavigationInfo.mapDestinationMarker = null;
            }
            Image image = new Image();
            image.setImageResource(iconId);
            this.currentNavigationInfo.mapDestinationMarker = new MapMarker(geoCoordinate, image);
            this.mapController.addMapObject(this.currentNavigationInfo.mapDestinationMarker);
            hereMapsManager.getLocationFixManager().addMarkers(this.mapController);
            sLogger.v("added destination marker");
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    private void removeDestinationMarker() {
        if (this.currentNavigationInfo.mapDestinationMarker != null) {
            sLogger.v("removed destination marker");
            this.mapController.removeMapObject(this.currentNavigationInfo.mapDestinationMarker);
            hereMapsManager.getLocationFixManager().removeMarkers(this.mapController);
        }
    }

    public NavigationView getNavigationView() {
        return this.navigationView;
    }

    public void arrived() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                synchronized (this) {
                    if (HereNavigationManager.this.isNavigationModeOn()) {
                        HereNavigationManager.sLogger.v("arrived = true");
                        HereNavigationManager.this.hereNavController.arrivedAtDestination();
                        HereMapUtil.removeRouteInfo(HereNavigationManager.sLogger, false);
                        if (HereNavigationManager.this.currentNavigationInfo.navigationRouteRequest.routeAttributes.contains(RouteAttribute.ROUTE_ATTRIBUTE_GAS)) {
                            HereNavigationManager.sLogger.v("arrived : gas");
                            final NavigationRouteRequest nextRoute = HereMapUtil.getSavedRouteData(HereNavigationManager.sLogger).navigationRouteRequest;
                            HereNavigationManager.this.setNavigationMode(NavigationMode.MAP, null, null);
                            if (nextRoute != null) {
                                HereNavigationManager.sLogger.v("arrived : gas, has next route");
                                HereNavigationManager.this.handler.post(new Runnable() {
                                    public void run() {
                                        HereNavigationManager.this.bus.post(nextRoute);
                                        HereNavigationManager.this.bus.post(new RemoteEvent(nextRoute));
                                    }
                                });
                            } else {
                                HereNavigationManager.sLogger.v("arrived : gas, no next route");
                            }
                        } else {
                            HereNavigationManager.sLogger.v("arrived : regular");
                            if (!HereNavigationManager.this.currentNavigationInfo.hasArrived) {
                                HereNavigationManager.this.bus.post(new ManeuverEvent(Type.LAST, null));
                            }
                            HereNavigationManager.this.currentNavigationInfo.hasArrived = true;
                            HereNavigationManager.this.currentNavigationInfo.ignoreArrived = false;
                            HereNavigationManager.this.bus.post(HereNavigationManager.ARRIVAL_EVENT);
                            HereNavigationManager.this.postArrivedManeuver();
                            HereNavigationManager.this.removeCurrentRoute();
                            HereNavigationManager.this.currentSessionState = NavigationSessionState.NAV_SESSION_STOPPED;
                            HereNavigationManager.sLogger.v("arrived: session stopped");
                            HereNavigationManager.this.postNavigationSessionStatusEvent(NavigationSessionState.NAV_SESSION_ARRIVED, false);
                            HereNavigationManager.this.postNavigationSessionStatusEvent(false);
                            HereNavigationManager.sLogger.v("arrived: posted arrival maneuver");
                        }
                    }
                }
            }
        }, 20);
    }

    public boolean hasArrived() {
        if (isNavigationModeOn()) {
            return this.currentNavigationInfo.hasArrived;
        }
        return false;
    }

    public void setIgnoreArrived(boolean b) {
        if (isNavigationModeOn() && this.currentNavigationInfo.hasArrived) {
            sLogger.v("ignore arrived:" + b);
            this.currentNavigationInfo.ignoreArrived = b;
        }
    }

    public boolean isOnGasRoute() {
        if (!isNavigationModeOn()) {
            return false;
        }
        NavigationRouteRequest request = this.currentNavigationInfo.navigationRouteRequest;
        if (request == null || request.routeAttributes == null) {
            return false;
        }
        return request.routeAttributes.contains(RouteAttribute.ROUTE_ATTRIBUTE_GAS);
    }

    public boolean isRerouting() {
        return this.reRouteListener.isRecalculating();
    }

    public boolean isLastManeuver() {
        return this.currentNavigationInfo.lastManeuver;
    }

    public void setLastManeuver() {
        this.currentNavigationInfo.lastManeuver = true;
    }

    public Error addNewRoute(ArrayList<NavigationRouteResult> outgoingResults, RerouteReason reason, NavigationRouteRequest navigationRouteRequest) {
        GenericUtil.checkNotOnMainThread();
        return addNewRoute(outgoingResults, reason, navigationRouteRequest, -1);
    }

    public Error addNewRoute(ArrayList<NavigationRouteResult> outgoingResults, RerouteReason reason, NavigationRouteRequest navigationRouteRequest, long simulationSpeed) {
        GenericUtil.checkNotOnMainThread();
        if (outgoingResults == null || outgoingResults.size() <= 0) {
            sLogger.e("addNewRoute calc no result, end trip:");
            stopNavigation();
            return Error.NOT_FOUND;
        }
        sLogger.v("addNewRoute calculated=" + reason);
        String id = ((NavigationRouteResult) outgoingResults.get(0)).routeId;
        RouteInfo info = HereRouteCache.getInstance().getRoute(id);
        if (info != null) {
            updateMapRoute(info.route, reason, navigationRouteRequest, null, null, false, info.traffic);
            sLogger.i("setNavigationMode: set transition mode");
            Error navError = this.hereNavController.startNavigation(info.route, (int) simulationSpeed);
            if (navError == Error.NONE) {
                sLogger.v("addNewRoute navigation started");
                return navError;
            }
            sLogger.e("addNewRoute navigation started failed:" + navError);
            stopNavigation();
            return navError;
        }
        sLogger.e("addNewRoute calc could not found route:" + id);
        stopNavigation();
        return Error.NOT_FOUND;
    }

    public Error addNewArrivalRoute(ArrayList<NavigationRouteResult> outgoingResults) {
        GenericUtil.checkNotOnMainThread();
        return addNewRoute(outgoingResults, RerouteReason.NAV_SESSION_ARRIVAL_REROUTE, null);
    }

    private void postArrivedManeuver() {
        ManeuverDisplay maneuverDisplay = this.currentNavigationInfo.arrivedManeuverDisplay;
        if (maneuverDisplay == null) {
            maneuverDisplay = HereManeuverDisplayBuilder.getArrivedManeuverDisplay();
            this.currentNavigationInfo.arrivedManeuverDisplay = maneuverDisplay;
            sLogger.v("arrival maneuver created");
        }
        this.currentNavigationInfo.lastManeuverPostTime = System.currentTimeMillis();
        this.bus.post(maneuverDisplay);
    }

    public NavigationManager.NavigationMode getHereNavigationState() {
        GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getNavigationMode();
    }

    public State getNavigationState() {
        return this.hereNavController.getState();
    }

    public int getCurrentTrafficRerouteCount() {
        return this.currentNavigationInfo.getTrafficReRouteCount();
    }

    public void incrCurrentTrafficRerouteCount() {
        this.currentNavigationInfo.incrTrafficRerouteCount();
    }

    public long getLastTrafficRerouteTime() {
        return this.currentNavigationInfo.getLastTrafficRerouteTime();
    }

    public void setLastTrafficRerouteTime(long l) {
        this.currentNavigationInfo.setLastTrafficRerouteTime(l);
    }

    public void startTrafficRerouteListener() {
        synchronized (trafficRerouteLock) {
            this.navdyTrafficRerouteManager.start();
            if (NetworkBandwidthController.getInstance().isLimitBandwidthModeOn()) {
                sLogger.v("startTrafficRerouteListener: limit b/w not starting here traffic reroute");
            } else {
                this.trafficRerouteListener.start();
            }
        }
    }

    public void stopTrafficRerouteListener() {
        synchronized (trafficRerouteLock) {
            this.navdyTrafficRerouteManager.stop();
            this.trafficRerouteListener.stop();
        }
    }

    public void resetTrafficRerouteListener() {
        synchronized (trafficRerouteLock) {
            this.navdyTrafficRerouteManager.reset();
        }
    }

    public boolean isTrafficUpdaterRunning() {
        return this.trafficUpdater.isRunning();
    }

    public void postManeuverDisplay() {
        refreshNavigationInfo();
    }

    public Maneuver getNextManeuver() {
        if (isNavigationModeOn()) {
            return this.currentManeuver;
        }
        return null;
    }

    public Maneuver getPrevManeuver() {
        if (isNavigationModeOn()) {
            return this.prevManeuver;
        }
        return null;
    }

    public void setTrafficToMode() {
        GenericUtil.checkNotOnMainThread();
        try {
            HereMapController.State state = this.mapController.getState();
            switch (state) {
                case AR_MODE:
                    sLogger.v("setTrafficToMode: " + state);
                    if (DriverProfileHelper.getInstance().getDriverProfileManager().isTrafficEnabled()) {
                        this.mapController.setTrafficInfoVisible(true);
                        sLogger.v("setTrafficToMode: map traffic enabled");
                        MapRoute mapRoute = getCurrentMapRoute();
                        if (mapRoute != null) {
                            mapRoute.setTrafficEnabled(true);
                            sLogger.v("setTrafficToMode: route traffic enabled");
                            return;
                        }
                        return;
                    }
                    sLogger.v("setTrafficToMode: traffic is not enabled");
                    return;
                default:
                    sLogger.v("setTrafficToMode: " + state + "::  no-op");
                    return;
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
        sLogger.e(t);
    }

    public HereNavController getNavController() {
        return this.hereNavController;
    }

    void setBandwidthPreferences() {
        boolean limitBandwidthModeOn = this.bandwidthController.isLimitBandwidthModeOn();
        DriverProfileManager profileManager = DriverProfileHelper.getInstance().getDriverProfileManager();
        if (limitBandwidthModeOn) {
            sLogger.v("limitbandwidth: on");
            sLogger.v("limitbandwidth: turn map traffic off");
            hereMapsManager.clearMapTraffic();
            profileManager.disableTraffic();
            sLogger.v("limitbandwidth: turn HERE traffic listener off");
            synchronized (trafficRerouteLock) {
                this.trafficRerouteListener.stop();
            }
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    HereNavigationManager.sLogger.v("limitbandwidth: turn off HERE traffic avoidance mode");
                    HereNavigationManager.this.hereNavController.setTrafficAvoidanceMode(TrafficAvoidanceMode.DISABLE);
                }
            }, 3);
            return;
        }
        sLogger.v("limitbandwidth: off");
        sLogger.v("limitbandwidth: turn map traffic on");
        hereMapsManager.setTrafficOverlay(getCurrentNavigationMode());
        profileManager.enableTraffic();
        synchronized (trafficRerouteLock) {
            if (GlanceHelper.isTrafficNotificationEnabled()) {
                sLogger.v("limitbandwidth: turn HERE traffic listener on");
                this.trafficRerouteListener.start();
            } else {
                sLogger.v("limitbandwidth: HERE traffic listener off, not enabled");
            }
        }
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                HereNavigationManager.sLogger.v("limitbandwidth: turn on HERE traffic avoidance mode");
                HereNavigationManager.this.hereNavController.setTrafficAvoidanceMode(TrafficAvoidanceMode.MANUAL);
            }
        }, 3);
    }

    public int getRerouteInterval() {
        if (this.bandwidthController.isLimitBandwidthModeOn()) {
            return HereTrafficRerouteListener.ROUTE_CHECK_INTERVAL_LIMIT_BANDWIDTH;
        }
        return HereTrafficRerouteListener.ROUTE_CHECK_INTERVAL;
    }

    public RerouteReason getCurrentRerouteReason() {
        return this.currentNavigationInfo.currentRerouteReason;
    }

    public boolean hasTrafficRerouteOnce() {
        return this.currentNavigationInfo.trafficRerouteOnce;
    }

    public boolean isShownFirstManeuver() {
        return this.currentNavigationInfo.firstManeuverShown;
    }

    public long getFirstManeuverShowntime() {
        return this.currentNavigationInfo.firstManeuverShownTime;
    }

    public void setShownFirstManeuver(boolean b) {
        this.currentNavigationInfo.firstManeuverShown = b;
        this.currentNavigationInfo.firstManeuverShownTime = SystemClock.elapsedRealtime();
    }

    public RouteOptions getRouteOptions() {
        return this.currentNavigationInfo.routeOptions;
    }

    public boolean isSwitchingToNewRoute() {
        return this.switchingToNewRoute;
    }

    public void setDebugManeuverState(ManeuverState state) {
        sLogger.v("debugManeuverState:" + state);
        this.debugManeuverState = state;
    }

    public void setDebugManeuverInstruction(String instruction) {
        sLogger.v("debugManeuverInstruction:" + instruction);
        this.debugManeuverInstruction = instruction;
    }

    public void setDebugNextManeuverIcon(int icon) {
        sLogger.v("debugNextManeuverIcon:" + icon);
        if (icon == 0) {
            this.currentNavigationInfo.maneuverAfterCurrentIconid = -1;
        }
        this.debugNextManeuverIcon = icon;
    }

    public void setDebugManeuverIcon(int icon) {
        sLogger.v("debugManeuverIcon:" + icon);
        this.debugManeuverIcon = icon;
    }

    public void setDebugManeuverDistance(String distance) {
        sLogger.v("debugManeuverDistance:" + distance);
        this.debugManeuverDistance = distance;
    }

    void startTrafficUpdater() {
        if (this.trafficUpdater != null && !this.trafficUpdaterStarted) {
            this.trafficUpdaterStarted = true;
            this.trafficUpdater.start();
        }
    }

    public String getCurrentVia() {
        String id = getCurrentRouteId();
        if (id == null) {
            return null;
        }
        RouteInfo info = HereRouteCache.getInstance().getRoute(id);
        if (info == null || info.routeResult == null) {
            return null;
        }
        return info.routeResult.via;
    }

    public String getFullStreetAddress() {
        NavigationRouteRequest req = this.currentNavigationInfo.navigationRouteRequest;
        if (req != null) {
            return req.streetAddress;
        }
        return null;
    }

    public void clearNavManeuver() {
        this.newManeuverEventListener.clearNavManeuver();
    }

    public Maneuver getNavManeuver() {
        return this.newManeuverEventListener.getNavManeuver();
    }

    public boolean isTrafficConsidered(String routeId) {
        try {
            RouteInfo info = HereRouteCache.getInstance().getRoute(routeId);
            if (info != null) {
                return info.traffic;
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
        return false;
    }

    public GeoCoordinate getStartLocation() {
        return this.currentNavigationInfo.startLocation;
    }

    public HereSafetySpotListener getSafetySpotListener() {
        return this.safetySpotListener;
    }
}
