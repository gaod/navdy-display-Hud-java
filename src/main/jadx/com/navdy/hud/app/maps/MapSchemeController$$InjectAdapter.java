package com.navdy.hud.app.maps;

import android.content.SharedPreferences;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class MapSchemeController$$InjectAdapter extends Binding<MapSchemeController> implements Provider<MapSchemeController>, MembersInjector<MapSchemeController> {
    private Binding<SharedPreferences> sharedPreferences;

    public MapSchemeController$$InjectAdapter() {
        super("com.navdy.hud.app.maps.MapSchemeController", "members/com.navdy.hud.app.maps.MapSchemeController", false, MapSchemeController.class);
    }

    public void attach(Linker linker) {
        this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", MapSchemeController.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.sharedPreferences);
    }

    public MapSchemeController get() {
        MapSchemeController result = new MapSchemeController();
        injectMembers(result);
        return result;
    }

    public void injectMembers(MapSchemeController object) {
        object.sharedPreferences = (SharedPreferences) this.sharedPreferences.get();
    }
}
