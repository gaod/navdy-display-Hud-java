package com.navdy.hud.app.maps.notification;

import android.animation.AnimatorSet;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.maps.MapEvents.TrafficJamProgressEvent;
import com.navdy.hud.app.maps.util.MapUtils;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout2.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Selection;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;
import com.navdy.hud.app.view.Gauge;
import com.squareup.otto.Bus;
import java.util.ArrayList;
import java.util.List;

public class TrafficJamNotification extends BaseTrafficNotification {
    public static final float REGULAR_SIZE = 64.0f;
    public static final float SMALL_SIZE = 46.0f;
    private static final int TAG_DISMISS = 1;
    private static String delay;
    private static String dismiss;
    private static List<Choice> dismissChoices = new ArrayList(1);
    private static String hr;
    private static String min;
    private static String trafficJam;
    private Bus bus;
    private IListener choiceListener = new IListener() {
        public void executeItem(Selection selection) {
            TrafficJamNotification.this.dismissNotification();
            TrafficJamNotification.this.trafficJamEvent = null;
        }

        public void itemSelected(Selection selection) {
        }
    };
    private ViewGroup container;
    private Gauge gauge;
    private int initialRemainingTime;
    private int notifColor;
    private TextView title;
    private TextView title1;
    private TextView title2;
    private TextView title3;
    private TrafficJamProgressEvent trafficJamEvent;

    public TrafficJamNotification(Bus bus, TrafficJamProgressEvent initialEvent) {
        if (trafficJam == null) {
            Resources resources = HudApplication.getAppContext().getResources();
            trafficJam = resources.getString(R.string.traffic_jam_title);
            delay = resources.getString(R.string.traffic_notification_delay);
            min = resources.getString(R.string.min);
            hr = resources.getString(R.string.hr);
            dismiss = resources.getString(R.string.traffic_notification_dismiss);
            int dismissColor = resources.getColor(R.color.glance_dismiss);
            dismissChoices.add(new Choice(1, R.drawable.icon_glances_dismiss, dismissColor, R.drawable.icon_glances_dismiss, -16777216, dismiss, dismissColor));
            this.notifColor = resources.getColor(R.color.traffic_bad);
        }
        this.bus = bus;
        this.initialRemainingTime = initialEvent.remainingTime;
        this.logger.v("TrafficJamNotification: initialRemainingTime=" + initialEvent.remainingTime);
    }

    public NotificationType getType() {
        return NotificationType.TRAFFIC_JAM;
    }

    public String getId() {
        return NotificationId.TRAFFIC_JAM_NOTIFICATION_ID;
    }

    public View getView(Context context) {
        if (this.container == null) {
            this.container = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.notification_traffic_jam, null);
            this.title = (TextView) this.container.findViewById(R.id.title);
            this.title1 = (TextView) this.container.findViewById(R.id.title1);
            this.title2 = (TextView) this.container.findViewById(R.id.title2);
            this.title3 = (TextView) this.container.findViewById(R.id.title3);
            this.gauge = (Gauge) this.container.findViewById(R.id.traffic_jam_progress);
            this.gauge.setMaxValue(this.initialRemainingTime);
            this.choiceLayout = (ChoiceLayout2) this.container.findViewById(R.id.choiceLayout);
        }
        return this.container;
    }

    public View getExpandedView(Context context, Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(INotificationController controller) {
        super.onStart(controller);
        updateState();
    }

    public void onUpdate() {
        updateState();
    }

    public void onStop() {
        super.onStop();
    }

    public int getTimeout() {
        return 0;
    }

    public boolean isAlive() {
        return true;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return true;
    }

    public int getColor() {
        return this.notifColor;
    }

    public void onNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    private void updateState() {
        if (this.trafficJamEvent != null) {
            this.title.setText(trafficJam);
            this.title1.setText(delay);
            if (this.trafficJamEvent.remainingTime >= 3600) {
                this.title2.setTextSize(2, 46.0f);
                this.title3.setText(hr);
            } else {
                this.title2.setTextSize(2, 64.0f);
                this.title3.setText(min);
            }
            this.title2.setText(MapUtils.formatTime(this.trafficJamEvent.remainingTime));
            this.gauge.setValue(this.trafficJamEvent.remainingTime);
            this.logger.v("TrafficJamNotification: trafficJamEvent.remainingTime=" + this.trafficJamEvent.remainingTime);
            this.choiceLayout.setChoices(dismissChoices, 0, this.choiceListener, 0.5f);
        }
    }

    public void setTrafficEvent(TrafficJamProgressEvent event) {
        this.trafficJamEvent = event;
    }
}
