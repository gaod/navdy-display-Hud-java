package com.navdy.hud.app.maps.notification;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.glance.GlanceViewCache;
import com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.maps.MapEvents.TrafficDelayEvent;
import com.navdy.hud.app.maps.MapEvents.TrafficRerouteAction;
import com.navdy.hud.app.maps.MapEvents.TrafficRerouteEvent;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout2.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Selection;
import com.navdy.hud.app.ui.component.image.ColorImageView;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;
import com.navdy.service.library.events.audio.CancelSpeechRequest;
import com.navdy.service.library.events.audio.SpeechRequest.Category;
import com.squareup.otto.Bus;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class RouteTimeUpdateNotification extends BaseTrafficNotification {
    private static final int TAG_DISMISS = 102;
    private static final int TAG_GO = 101;
    private static final int TAG_READ = 103;
    private static StringBuilder ampmMarker = new StringBuilder();
    private static int badTrafficColor;
    private static String delay;
    private static List<Choice> delayChoices = new ArrayList(1);
    private static String done;
    private static String fasterRoute;
    private static String fasterRouteAvailable;
    private static String go;
    private static String hour;
    private static String hours;
    private static String hr;
    private static String min;
    private static String minute;
    private static String minutes;
    private static int normalTrafficColor;
    private static List<Choice> reRouteChoices = new ArrayList(2);
    private static Resources resources;
    private static String save;
    private static int trafficRerouteColor;
    private static String trafficUpdate;
    private static String via;
    private Bus bus;
    private CancelSpeechRequest cancelSpeechRequest;
    private IListener choiceListener = new IListener() {
        public void executeItem(Selection selection) {
            if (RouteTimeUpdateNotification.this.controller != null) {
                switch (selection.id) {
                    case 5:
                        if (RouteTimeUpdateNotification.this.controller != null && RouteTimeUpdateNotification.this.controller.isExpandedWithStack()) {
                            RouteTimeUpdateNotification.this.controller.collapseNotification(false, false);
                            return;
                        }
                        return;
                    case 101:
                        if (RouteTimeUpdateNotification.this.fasterRouteEvent != null) {
                            RouteTimeUpdateNotification.this.bus.post(TrafficRerouteAction.REROUTE);
                            RouteTimeUpdateNotification.this.fasterRouteEvent = null;
                        }
                        RouteTimeUpdateNotification.this.dismissNotification();
                        return;
                    case 102:
                        if (RouteTimeUpdateNotification.this.fasterRouteEvent != null) {
                            RouteTimeUpdateNotification.this.bus.post(TrafficRerouteAction.DISMISS);
                            RouteTimeUpdateNotification.this.fasterRouteEvent = null;
                        } else if (RouteTimeUpdateNotification.this.trafficDelayEvent != null) {
                            RouteTimeUpdateNotification.this.trafficDelayEvent = null;
                        }
                        RouteTimeUpdateNotification.this.dismissNotification();
                        return;
                    case 103:
                        if (RouteTimeUpdateNotification.this.fasterRouteEvent != null) {
                            RouteTimeUpdateNotification.this.switchToExpandedMode();
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        }

        public void itemSelected(Selection selection) {
        }
    };
    private ViewGroup container;
    private String distanceDiffStr = "";
    private String distanceStr = "";
    private String etaStr = "";
    private String etaUnitStr = "";
    private ViewGroup extendedContainer;
    private TrafficRerouteEvent fasterRouteEvent;
    private String id;
    private ColorImageView mainImage;
    private TextView mainTitle;
    private ImageView sideImage;
    private TextView subTitle;
    private TextView text1;
    private TextView text2;
    private TextView text3;
    private String timeStr = "";
    private String timeStrExpanded = "";
    private String timeUnitStr = "";
    private TrafficDelayEvent trafficDelayEvent;
    private String ttsMessage;
    private NotificationType type;

    public RouteTimeUpdateNotification(String id, NotificationType type, Bus bus) {
        this.id = id;
        this.type = type;
        this.cancelSpeechRequest = new CancelSpeechRequest(id);
        if (trafficUpdate == null) {
            resources = HudApplication.getAppContext().getResources();
            trafficUpdate = resources.getString(R.string.traffic_notification_delay_title);
            fasterRoute = resources.getString(R.string.traffic_reroute_faster_route);
            delay = resources.getString(R.string.traffic_notification_delay);
            done = resources.getString(R.string.done);
            min = resources.getString(R.string.min);
            hr = resources.getString(R.string.hr);
            fasterRouteAvailable = resources.getString(R.string.traffic_faster_route_available);
            hour = resources.getString(R.string.hour);
            hours = resources.getString(R.string.hours);
            minute = resources.getString(R.string.minute);
            minutes = resources.getString(R.string.minutes);
            save = resources.getString(R.string.traffic_notification_save);
            go = resources.getString(R.string.traffic_reroute_go);
            via = resources.getString(R.string.traffic_reroute_via);
            normalTrafficColor = resources.getColor(R.color.traffic_good);
            badTrafficColor = resources.getColor(R.color.traffic_bad);
            trafficRerouteColor = resources.getColor(R.color.grey_4a);
            int dismissColor = resources.getColor(R.color.glance_dismiss);
            int readColor = resources.getColor(R.color.glance_ok_blue);
            int goColor = resources.getColor(R.color.glance_ok_go);
            String dismiss = resources.getString(R.string.dismiss);
            delayChoices.add(new Choice(102, R.drawable.icon_glances_dismiss, dismissColor, R.drawable.icon_glances_dismiss, -16777216, dismiss, dismissColor));
            reRouteChoices.add(new Choice(103, R.drawable.icon_glances_read, readColor, R.drawable.icon_glances_read, -16777216, GlanceConstants.read, readColor));
            reRouteChoices.add(new Choice(101, R.drawable.icon_glances_ok_strong, goColor, R.drawable.icon_glances_ok_strong, -16777216, go, goColor));
            reRouteChoices.add(new Choice(102, R.drawable.icon_glances_dismiss, dismissColor, R.drawable.icon_glances_dismiss, -16777216, dismiss, dismissColor));
        }
        this.bus = bus;
        bus.register(this);
    }

    public NotificationType getType() {
        return this.type;
    }

    public String getId() {
        return this.id;
    }

    public View getView(Context context) {
        if (this.container == null) {
            this.container = (ViewGroup) GlanceViewCache.getView(ViewType.SMALL_SIGN, context);
            this.mainTitle = (TextView) this.container.findViewById(R.id.mainTitle);
            this.subTitle = (TextView) this.container.findViewById(R.id.subTitle);
            this.mainImage = (ColorImageView) this.container.findViewById(R.id.mainImage);
            this.sideImage = (ImageView) this.container.findViewById(R.id.sideImage);
            this.text1 = (TextView) this.container.findViewById(R.id.text1);
            this.text2 = (TextView) this.container.findViewById(R.id.text2);
            this.text3 = (TextView) this.container.findViewById(R.id.text3);
            this.choiceLayout = (ChoiceLayout2) this.container.findViewById(R.id.choiceLayout);
        }
        return this.container;
    }

    public View getExpandedView(Context context, Object data) {
        this.extendedContainer = (ViewGroup) GlanceViewCache.getView(ViewType.BIG_MULTI_TEXT, context);
        setExpandedContent(context);
        return this.extendedContainer;
    }

    void setExpandedContent(Context context) {
        TextView textView1 = (TextView) this.extendedContainer.findViewById(R.id.title1);
        textView1.setTextAppearance(context, R.style.glance_title_1);
        TextView textView2 = (TextView) this.extendedContainer.findViewById(R.id.title2);
        textView2.setTextAppearance(context, R.style.glance_title_2);
        if (this.fasterRouteEvent != null) {
            String str;
            textView1.setText(fasterRouteAvailable);
            if (this.timeStr == null) {
                buildData();
            }
            if (this.fasterRouteEvent.etaDifference != 0) {
                str = resources.getString(R.string.traffic_reroute_msg, new Object[]{this.timeStr, this.timeStrExpanded, this.fasterRouteEvent.additionalVia, this.etaStr, this.etaUnitStr, this.distanceStr, this.distanceDiffStr});
            } else {
                str = resources.getString(R.string.traffic_reroute_msg_eta_only, new Object[]{this.fasterRouteEvent.additionalVia, this.etaStr, this.etaUnitStr, this.distanceStr, this.distanceDiffStr});
            }
            textView2.setText(str);
        }
        textView1.setVisibility(0);
        textView2.setVisibility(0);
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(INotificationController controller) {
        super.onStart(controller);
        updateState();
        if (controller.isExpandedWithStack()) {
            this.mainTitle.setAlpha(0.0f);
            this.subTitle.setAlpha(0.0f);
            this.choiceLayout.setAlpha(0.0f);
            return;
        }
        this.container.setTranslationX(0.0f);
        this.container.setTranslationY(0.0f);
        this.container.setAlpha(1.0f);
        this.mainTitle.setAlpha(1.0f);
        this.subTitle.setAlpha(1.0f);
        this.choiceLayout.setAlpha(1.0f);
    }

    public void onUpdate() {
        updateState();
    }

    public void onStop() {
        cancelTts();
        super.onStop();
        if (this.container != null) {
            cleanupView(ViewType.SMALL_SIGN, this.container);
            this.container = null;
        }
        if (this.extendedContainer != null) {
            cleanupView(ViewType.BIG_MULTI_TEXT, this.extendedContainer);
            this.extendedContainer = null;
        }
    }

    public int getTimeout() {
        return 0;
    }

    public boolean isAlive() {
        return true;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return true;
    }

    public int getColor() {
        if (this.fasterRouteEvent != null) {
            return GlanceConstants.colorFasterRoute;
        }
        return GlanceConstants.colorTrafficDelay;
    }

    public void onNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationEvent(Mode mode) {
        if (mode != Mode.COLLAPSE) {
            sendtts();
        } else if (this.controller != null) {
            cancelTts();
            if (NotificationManager.getInstance().isNotificationMarkedForRemoval(this.id)) {
                dismissNotification();
            } else {
                updateState();
            }
        }
    }

    public void onExpandedNotificationSwitched() {
        if (this.controller != null && this.controller.isExpandedWithStack()) {
            sendtts();
        }
    }

    public AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        AnimatorSet set = new AnimatorSet();
        ObjectAnimator o1;
        ObjectAnimator o2;
        ObjectAnimator o3;
        if (viewIn) {
            o1 = ObjectAnimator.ofFloat(this.mainTitle, View.ALPHA, new float[]{0.0f, 1.0f});
            o2 = ObjectAnimator.ofFloat(this.subTitle, View.ALPHA, new float[]{0.0f, 1.0f});
            o3 = ObjectAnimator.ofFloat(this.choiceLayout, View.ALPHA, new float[]{0.0f, 1.0f});
            set.playTogether(new Animator[]{o1, o2, o3});
        } else {
            o1 = ObjectAnimator.ofFloat(this.mainTitle, View.ALPHA, new float[]{1.0f, 0.0f});
            o2 = ObjectAnimator.ofFloat(this.subTitle, View.ALPHA, new float[]{1.0f, 0.0f});
            o3 = ObjectAnimator.ofFloat(this.choiceLayout, View.ALPHA, new float[]{1.0f, 0.0f});
            set.playTogether(new Animator[]{o1, o2, o3});
        }
        return set;
    }

    public boolean expandNotification() {
        if (this.controller == null) {
            return false;
        }
        switchToExpandedMode();
        return true;
    }

    private void updateState() {
        if (this.controller != null) {
            this.logger.v("updateState");
            if (this.trafficDelayEvent == null && this.fasterRouteEvent != null) {
                this.mainTitle.setText(fasterRoute);
                if (this.fasterRouteEvent.via != null) {
                    this.subTitle.setText(via + " " + this.fasterRouteEvent.via);
                } else {
                    this.subTitle.setText("");
                }
                this.mainImage.setColor(trafficRerouteColor);
                this.sideImage.setImageResource(R.drawable.icon_badge_route);
                buildData();
                if (this.fasterRouteEvent.etaDifference != 0) {
                    this.text1.setText(save);
                    ((MarginLayoutParams) this.text2.getLayoutParams()).topMargin = GlanceConstants.calendarNormalMargin;
                    this.text2.setTextAppearance(HudApplication.getAppContext(), R.style.small_glance_sign_text_1);
                    this.text2.setText(this.timeStr);
                    this.text3.setText(this.timeUnitStr);
                }
                if (this.controller.isExpandedWithStack()) {
                    this.choiceLayout.setChoices(GlanceConstants.backChoice2, 0, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(GlanceConstants.backChoice);
                } else {
                    this.choiceLayout.setChoices(reRouteChoices, 1, this.choiceListener, 0.5f);
                }
                if (this.extendedContainer != null && this.controller.isExpandedWithStack()) {
                    setExpandedContent(HudApplication.getAppContext());
                }
            }
        }
    }

    public void setDelayEvent(TrafficDelayEvent event) {
        this.trafficDelayEvent = event;
    }

    public void setFasterRouteEvent(TrafficRerouteEvent event) {
        this.fasterRouteEvent = event;
    }

    private void buildData() {
        if (this.fasterRouteEvent != null) {
            if (this.fasterRouteEvent.etaDifference != 0) {
                long val = TimeUnit.SECONDS.toMinutes(this.fasterRouteEvent.etaDifference);
                if (val > 99) {
                    val = TimeUnit.SECONDS.toHours(this.fasterRouteEvent.etaDifference);
                    this.timeStr = String.valueOf(val);
                    this.timeUnitStr = hr;
                    if (val > 1) {
                        this.timeStrExpanded = hours;
                    } else {
                        this.timeStrExpanded = hour;
                    }
                } else {
                    this.timeStr = String.valueOf(val);
                    this.timeUnitStr = min;
                    if (val > 1) {
                        this.timeStrExpanded = minutes;
                    } else {
                        this.timeStrExpanded = minute;
                    }
                }
            }
            Date etaDate = null;
            if (this.fasterRouteEvent.newEta != 0) {
                etaDate = new Date(this.fasterRouteEvent.newEta);
            } else if (this.fasterRouteEvent.currentEta != 0) {
                etaDate = new Date(this.fasterRouteEvent.currentEta - (this.fasterRouteEvent.etaDifference * 1000));
            }
            if (etaDate != null) {
                ampmMarker.setLength(0);
                this.etaStr = RemoteDeviceManager.getInstance().getTimeHelper().formatTime12Hour(etaDate, ampmMarker, false);
                this.etaUnitStr = ampmMarker.toString();
            }
            long diff = this.fasterRouteEvent.distanceDifference;
            if (diff > 0) {
                this.distanceDiffStr = resources.getString(R.string.longer);
            } else {
                this.distanceDiffStr = resources.getString(R.string.shorter);
                diff = -diff;
            }
            ManeuverDisplay display = new ManeuverDisplay();
            HereManeuverDisplayBuilder.setNavigationDistance(diff, display, true, true);
            this.distanceStr = display.distanceToPendingRoadText;
            if (this.fasterRouteEvent.etaDifference != 0) {
                this.ttsMessage = resources.getString(R.string.traffic_faster_route_tts, new Object[]{this.timeStr, this.timeStrExpanded});
                return;
            }
            this.ttsMessage = resources.getString(R.string.traffic_faster_route_tts_eta_only);
        }
    }

    private void switchToExpandedMode() {
        this.choiceLayout.setChoices(GlanceConstants.backChoice2, 0, this.choiceListener, 0.5f);
        if (!this.controller.isExpandedWithStack()) {
            this.controller.expandNotification(true);
        }
    }

    private void sendtts() {
        if (this.controller != null && this.controller.isTtsOn() && !TextUtils.isEmpty(this.ttsMessage)) {
            TTSUtils.sendSpeechRequest(this.ttsMessage, Category.SPEECH_REROUTE, this.id);
        }
    }

    private void cancelTts() {
        if (this.controller != null && this.controller.isTtsOn()) {
            this.logger.v("tts-cancelled [" + this.id + "]");
            this.bus.post(new RemoteEvent(this.cancelSpeechRequest));
        }
    }

    private void cleanupView(ViewType viewType, ViewGroup viewGroup) {
        ViewGroup parent = (ViewGroup) viewGroup.getParent();
        if (parent != null) {
            parent.removeView(viewGroup);
        }
        switch (viewType) {
            case SMALL_SIGN:
                this.mainTitle.setAlpha(1.0f);
                this.mainTitle.setVisibility(0);
                this.subTitle.setAlpha(1.0f);
                this.subTitle.setVisibility(0);
                this.choiceLayout.setAlpha(1.0f);
                this.choiceLayout.setVisibility(0);
                this.choiceLayout.setTag(null);
                this.sideImage.setImageResource(0);
                viewGroup.setAlpha(1.0f);
                break;
            case BIG_MULTI_TEXT:
                viewGroup.setAlpha(1.0f);
                break;
        }
        GlanceViewCache.putView(viewType, viewGroup);
    }
}
