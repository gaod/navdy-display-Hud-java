package com.navdy.hud.app.maps.here;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Handler;
import android.os.Looper;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.OnScreenCaptureListener;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.Map.Animation;
import com.here.android.mpa.mapping.Map.Scheme;
import com.here.android.mpa.mapping.MapOffScreenRenderer;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import java.io.File;
import java.io.FileOutputStream;

public class HereMapImageGenerator {
    private static final Logger sLogger = new Logger(HereMapImageGenerator.class);
    private static final HereMapImageGenerator singleton = new HereMapImageGenerator();
    private Handler handler = new Handler(Looper.getMainLooper());
    private final Map map = new Map();
    private final MapOffScreenRenderer mapOffScreenRenderer = new MapOffScreenRenderer(HudApplication.getAppContext());
    private boolean renderRunning;
    private Object waitForRender = new Object();

    public interface ICallback {
        void result(Bitmap bitmap);
    }

    public static class MapGeneratorParams {
        public ICallback callback;
        public int height;
        public String id;
        public double latitude;
        public double longitude;
        public int width;
        public double zoomLevel;
    }

    public static HereMapImageGenerator getInstance() {
        return singleton;
    }

    private HereMapImageGenerator() {
        this.map.setMapScheme(Scheme.TERRAIN_DAY);
        this.map.setExtrudedBuildingsVisible(false);
        this.map.setFadingAnimations(false);
        this.map.getPositionIndicator().setVisible(false);
        this.mapOffScreenRenderer.setMap(this.map);
        this.mapOffScreenRenderer.setBlockingRendering(true);
    }

    public String getMapImageFile(String id) {
        return DriverProfileHelper.getInstance().getCurrentProfile().getPlacesImageDir().getAbsolutePath() + File.separator + id + ".jpg";
    }

    public void generateMapSnapshot(MapGeneratorParams mapGeneratorParams) {
        if (mapGeneratorParams == null) {
            throw new IllegalArgumentException();
        } else if (!HereMapsManager.getInstance().isInitialized()) {
            sLogger.e("map engine not initialized");
            if (mapGeneratorParams.callback != null) {
                mapGeneratorParams.callback.result(null);
            }
        }
    }

    private void generateMapSnapshotInternal(final MapGeneratorParams mapGeneratorParams) {
        try {
            sLogger.e("generateMapSnapshotInternal lat:" + mapGeneratorParams.latitude + " lon:" + mapGeneratorParams.longitude);
            this.map.setCenter(new GeoCoordinate(mapGeneratorParams.latitude, mapGeneratorParams.longitude), Animation.NONE);
            this.mapOffScreenRenderer.setSize(mapGeneratorParams.width, mapGeneratorParams.height);
            if (null == null) {
                this.mapOffScreenRenderer.start();
            }
            this.mapOffScreenRenderer.getScreenCapture(new OnScreenCaptureListener() {
                public void onScreenCaptured(Bitmap bitmap) {
                    try {
                        HereMapImageGenerator.this.stopOffscreenRenderer();
                        HereMapImageGenerator.sLogger.e("onScreenCaptured bitmap:" + bitmap);
                        HereMapImageGenerator.this.saveFileToDisk(mapGeneratorParams, bitmap);
                    } catch (Throwable t) {
                        HereMapImageGenerator.sLogger.e("onScreenCaptured", t);
                    }
                }
            });
        } catch (Throwable t) {
            sLogger.e("renderMapSnapshot", t);
            if (null != null) {
                stopOffscreenRenderer();
            }
        }
    }

    private void stopOffscreenRenderer() {
        try {
            this.mapOffScreenRenderer.stop();
        } catch (Throwable t) {
            sLogger.e(t);
        } finally {
            notifyWaitingRenders();
        }
    }

    private void notifyWaitingRenders() {
        synchronized (this.waitForRender) {
            this.renderRunning = false;
            this.waitForRender.notifyAll();
        }
    }

    private void saveFileToDisk(final MapGeneratorParams mapGeneratorParams, final Bitmap bitmap) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                Throwable t;
                Throwable th;
                FileOutputStream fout = null;
                try {
                    if (bitmap != null) {
                        String path = HereMapImageGenerator.this.getMapImageFile(mapGeneratorParams.id);
                        FileOutputStream fout2 = new FileOutputStream(path);
                        try {
                            bitmap.compress(CompressFormat.JPEG, 100, fout2);
                            IOUtils.fileSync(fout2);
                            IOUtils.closeStream(fout2);
                            fout = null;
                            HereMapImageGenerator.sLogger.v("bitmap saved:" + path);
                        } catch (Throwable th2) {
                            th = th2;
                            fout = fout2;
                            IOUtils.fileSync(fout);
                            IOUtils.closeStream(fout);
                            throw th;
                        }
                    }
                    HereMapImageGenerator.sLogger.e("bitmap not saved:");
                    if (mapGeneratorParams.callback != null) {
                        HereMapImageGenerator.this.handler.post(new Runnable() {
                            public void run() {
                                mapGeneratorParams.callback.result(bitmap);
                            }
                        });
                    }
                    IOUtils.fileSync(fout);
                    IOUtils.closeStream(fout);
                } catch (Throwable th3) {
                    t = th3;
                }
            }
        }, 1);
    }
}
