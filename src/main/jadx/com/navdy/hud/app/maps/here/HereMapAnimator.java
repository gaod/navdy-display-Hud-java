package com.navdy.hud.app.maps.here;

import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.mapping.Map.Animation;
import com.here.android.mpa.mapping.OnMapRenderListener;
import com.navdy.hud.app.maps.MapSettings;
import com.navdy.hud.app.maps.here.HereMapController.State;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;

public class HereMapAnimator {
    private static final int DRASTIC_HEADING_CHANGE = 45;
    private static final int INVALID_STEP = -1;
    private static final int INVALID_TILT = -1;
    private static final int INVALID_ZOOM = -1;
    private static final int MAX_DIFFERENCE_HEADING_FILTER = 120;
    private static final int MAX_HEADING = 360;
    private static final long MAX_INTERVAL_HEADING_UPDATE = 3000;
    private static final int MIN_HEADING = 0;
    private static final double MIN_SPEED_FILTER = 0.44704d;
    private static final double MPH_TO_MS = 0.44704d;
    private static final long REFRESH_TIME_THROTTLE = ((long) (1000 / MapSettings.getMapFps()));
    private static final Object geoPositionLock = new Object();
    private static final Logger logger = new Logger(HereMapAnimator.class);
    private static final Object tiltLock = new Object();
    private static final Object zoomLock = new Object();
    private GeoPosition currentGeoPosition;
    private float currentTilt;
    private double currentZoom;
    private long geoPositionUpdateInterval;
    private long lastGeoPositionUpdateTime;
    private volatile float lastHeading = -1.0f;
    private long lastPreDrawTime;
    private long lastTiltUpdateTime;
    private long lastZoomUpdateTime;
    private HereMapController mapController;
    private OnMapRenderListener mapRenderListener;
    private AnimationMode mode = AnimationMode.NONE;
    private HereNavController navController;
    private long preDrawFinishTime;
    private volatile boolean renderingEnabled = true;
    private long tiltUpdateInterval;
    private long zoomUpdateInterval;

    enum AnimationMode {
        NONE,
        ANIMATION,
        ZOOM_TILT_ANIMATION
    }

    HereMapAnimator(AnimationMode mode, HereMapController mapController, HereNavController navController) {
        this.mode = mode;
        if (mode != AnimationMode.NONE) {
            logger.v("animation throttle = " + REFRESH_TIME_THROTTLE);
            this.mapRenderListener = new OnMapRenderListener() {
                public void onPreDraw() {
                    HereMapAnimator.this.onPreDraw();
                }

                public void onPostDraw(boolean invalidated, long renderTime) {
                    HereMapAnimator.this.onPostDraw(invalidated, renderTime);
                }

                public void onSizeChanged(int width, int height) {
                }

                public void onGraphicsDetached() {
                }

                public void onRenderBufferCreated() {
                }
            };
        }
        this.mapController = mapController;
        this.navController = navController;
        this.currentGeoPosition = null;
        this.currentZoom = mapController.getZoomLevel();
        this.currentTilt = mapController.getTilt();
    }

    public OnMapRenderListener getMapRenderListener() {
        return this.mapRenderListener;
    }

    public AnimationMode getAnimationMode() {
        return this.mode;
    }

    public void setGeoPosition(@NonNull final GeoPosition geoPosition) {
        switch (this.mode) {
            case NONE:
                double heading = geoPosition.getHeading();
                boolean animate = false;
                if (this.lastHeading != -1.0f) {
                    double orientationDiff = getOrientationDiff(heading, (double) this.lastHeading);
                    double speed = geoPosition.getSpeed();
                    if (speed <= 0.44704d && Math.abs(orientationDiff) >= 120.0d) {
                        logger.v("filtering out spurious heading last: " + this.lastHeading + " new:" + heading + " speed:" + speed);
                        return;
                    } else if (Math.abs(orientationDiff) >= 45.0d) {
                        animate = true;
                    }
                }
                this.currentGeoPosition = geoPosition;
                this.lastHeading = (float) heading;
                if (this.renderingEnabled && this.mapController.getState() == State.AR_MODE) {
                    this.mapController.setCenter(geoPosition.getCoordinate(), animate ? Animation.BOW : Animation.NONE, -1.0d, this.lastHeading, -1.0f);
                    return;
                }
                return;
            default:
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        long interval = 0;
                        if (HereMapAnimator.this.lastGeoPositionUpdateTime > 0) {
                            interval = SystemClock.elapsedRealtime() - HereMapAnimator.this.lastGeoPositionUpdateTime;
                        }
                        if (HereMapAnimator.this.isValidGeoPosition(geoPosition)) {
                            GeoPosition oldPosition;
                            HereMapAnimator.this.lastGeoPositionUpdateTime = SystemClock.elapsedRealtime();
                            synchronized (HereMapAnimator.geoPositionLock) {
                                HereMapAnimator.this.geoPositionUpdateInterval = interval;
                                oldPosition = HereMapAnimator.this.currentGeoPosition;
                                HereMapAnimator.this.currentGeoPosition = geoPosition;
                            }
                            if (oldPosition == null && HereMapAnimator.this.renderingEnabled) {
                                HereMapAnimator.logger.v("initial setCenter");
                                HereMapAnimator.this.mapController.setCenter(HereMapAnimator.this.currentGeoPosition.getCoordinate(), Animation.NONE, -1.0d, (float) HereMapAnimator.this.currentGeoPosition.getHeading(), -1.0f);
                                return;
                            }
                            return;
                        }
                        HereMapAnimator.logger.v("filtering out spurious heading: " + geoPosition.getHeading() + " speed:" + geoPosition.getSpeed());
                    }
                }, 17);
                return;
        }
    }

    void setZoom(final double zoom) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                long interval = 0;
                if (HereMapAnimator.this.lastZoomUpdateTime > 0) {
                    interval = SystemClock.elapsedRealtime() - HereMapAnimator.this.lastZoomUpdateTime;
                }
                if (HereMapAnimator.this.isValidZoom(zoom)) {
                    HereMapAnimator.this.lastZoomUpdateTime = SystemClock.elapsedRealtime();
                    synchronized (HereMapAnimator.zoomLock) {
                        HereMapAnimator.this.zoomUpdateInterval = interval;
                        HereMapAnimator.this.currentZoom = zoom;
                    }
                    return;
                }
                HereMapAnimator.logger.v("filtering out spurious zoom: " + zoom);
            }
        }, 17);
    }

    void setTilt(final float tilt) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                long interval = 0;
                if (HereMapAnimator.this.lastTiltUpdateTime > 0) {
                    interval = SystemClock.elapsedRealtime() - HereMapAnimator.this.lastTiltUpdateTime;
                }
                if (HereMapAnimator.this.isValidTilt(tilt)) {
                    HereMapAnimator.this.lastTiltUpdateTime = SystemClock.elapsedRealtime();
                    synchronized (HereMapAnimator.tiltLock) {
                        HereMapAnimator.this.tiltUpdateInterval = interval;
                        HereMapAnimator.this.currentTilt = tilt;
                    }
                    return;
                }
                HereMapAnimator.logger.v("filtering out spurious tilt: " + tilt);
            }
        }, 17);
    }

    private void onPreDraw() {
        if (this.currentGeoPosition != null) {
            long preDrawStartTime = SystemClock.elapsedRealtime();
            if (this.lastPreDrawTime > 0) {
                long geoPositionUpdateIntervalCopy;
                GeoPosition currentGeoPositionCopy;
                long zoomUpdateIntervalCopy;
                double currentZoomCopy;
                long tiltUpdateIntervalCopy;
                float currentTiltCopy;
                long preDrawInterval = preDrawStartTime - this.lastPreDrawTime;
                this.lastPreDrawTime = preDrawStartTime;
                GeoPosition geoPosition = null;
                double zoom = -1.0d;
                float tilt = -1.0f;
                double geoPositionStep = -1.0d;
                double zoomStep = -1.0d;
                double tiltStep = -1.0d;
                synchronized (geoPositionLock) {
                    geoPositionUpdateIntervalCopy = this.geoPositionUpdateInterval;
                    currentGeoPositionCopy = this.currentGeoPosition;
                }
                synchronized (zoomLock) {
                    zoomUpdateIntervalCopy = this.zoomUpdateInterval;
                    currentZoomCopy = this.currentZoom;
                }
                synchronized (tiltLock) {
                    tiltUpdateIntervalCopy = this.tiltUpdateInterval;
                    currentTiltCopy = this.currentTilt;
                }
                boolean hasGeoPositionUpdate = currentGeoPositionCopy != null && geoPositionUpdateIntervalCopy > 0;
                boolean hasZoomUpdate = currentZoomCopy >= 0.0d && zoomUpdateIntervalCopy > 0;
                boolean hasTiltUpdate = currentTiltCopy >= 0.0f && tiltUpdateIntervalCopy > 0;
                if (hasGeoPositionUpdate) {
                    geoPosition = currentGeoPositionCopy;
                    geoPositionStep = ((double) preDrawInterval) / ((double) geoPositionUpdateIntervalCopy);
                }
                if (hasZoomUpdate) {
                    zoom = currentZoomCopy;
                    zoomStep = ((double) preDrawInterval) / ((double) zoomUpdateIntervalCopy);
                }
                if (hasTiltUpdate) {
                    tilt = currentTiltCopy;
                    tiltStep = ((double) preDrawInterval) / ((double) tiltUpdateIntervalCopy);
                }
                if (hasGeoPositionUpdate || hasZoomUpdate || hasTiltUpdate) {
                    double step = Math.max(geoPositionStep, Math.max(zoomStep, tiltStep));
                    if (step > 1.0d) {
                        step = 1.0d;
                    }
                    GeoCoordinate newCenter = getNewCenter(geoPosition, step);
                    float newHeading = getNewHeading(geoPosition, step);
                    double newZoomLevel = getNewZoomLevel(zoom, step);
                    float newTilt = getNewTilt(tilt, step);
                    if (this.renderingEnabled && this.mapController.getState() == State.AR_MODE) {
                        this.mapController.setCenter(newCenter, Animation.NONE, newZoomLevel, newHeading, newTilt);
                    }
                    this.preDrawFinishTime = SystemClock.elapsedRealtime();
                    if (logger.isLoggable(2)) {
                        logger.v("predraw logic took " + (this.preDrawFinishTime - preDrawStartTime) + " ms");
                        return;
                    }
                    return;
                }
                this.preDrawFinishTime = SystemClock.elapsedRealtime();
                return;
            }
            this.lastPreDrawTime = preDrawStartTime;
        }
    }

    private void onPostDraw(boolean invalidated, long renderTime) {
        long lastRenderTime = SystemClock.elapsedRealtime() - this.preDrawFinishTime;
        long sleepTime = REFRESH_TIME_THROTTLE - lastRenderTime;
        if (logger.isLoggable(2)) {
            logger.v("last render took " + lastRenderTime + " ms");
        }
        if (sleepTime > 0) {
            if (logger.isLoggable(2)) {
                logger.v("sleeping render thread for " + sleepTime + " ms");
            }
            try {
                Thread.sleep(sleepTime);
            } catch (Throwable e) {
                logger.e(e);
            }
        } else if (logger.isLoggable(2)) {
            logger.v("render thread could not sleep");
        }
    }

    private boolean isValidGeoPosition(@NonNull GeoPosition geoPosition) {
        GeoPosition currentGeoPositionCopy;
        synchronized (geoPositionLock) {
            currentGeoPositionCopy = this.currentGeoPosition;
            long geoPositionUpdateIntervalCopy = this.geoPositionUpdateInterval;
        }
        if (currentGeoPositionCopy == null) {
            return true;
        }
        double orientationDiff = getOrientationDiff(geoPosition.getHeading(), currentGeoPositionCopy.getHeading());
        if (geoPosition.getSpeed() >= 0.44704d || Math.abs(orientationDiff) <= 120.0d || geoPositionUpdateIntervalCopy >= 3000) {
            return true;
        }
        return false;
    }

    private boolean isValidZoom(double zoom) {
        return zoom >= 0.0d && zoom <= 20.0d;
    }

    private boolean isValidTilt(float tilt) {
        return tilt >= 0.0f && tilt < 90.0f;
    }

    private double getOrientationDiff(double firstOrientation, double secondOrientation) {
        double orientationDiff = firstOrientation - secondOrientation;
        if (orientationDiff > 180.0d) {
            return orientationDiff - 360.0d;
        }
        if (orientationDiff < -180.0d) {
            return orientationDiff + 360.0d;
        }
        return orientationDiff;
    }

    private GeoCoordinate getNewCenter(@Nullable GeoPosition geoPosition, double step) {
        GeoCoordinate currentCenter = this.mapController.getCenter();
        if (geoPosition == null || step == -1.0d) {
            return currentCenter;
        }
        GeoCoordinate newCoords = geoPosition.getCoordinate();
        return new GeoCoordinate(currentCenter.getLatitude() + ((newCoords.getLatitude() - currentCenter.getLatitude()) * step), currentCenter.getLongitude() + ((newCoords.getLongitude() - currentCenter.getLongitude()) * step));
    }

    private float getNewHeading(@Nullable GeoPosition geoPosition, double step) {
        float currentOrientation = this.mapController.getOrientation();
        if (geoPosition == null || step == -1.0d) {
            return currentOrientation;
        }
        double newHeading = ((double) currentOrientation) + (step * getOrientationDiff((double) ((float) geoPosition.getHeading()), (double) currentOrientation));
        if (newHeading > 360.0d) {
            newHeading -= 360.0d;
        } else if (newHeading < 0.0d) {
            newHeading += 360.0d;
        }
        return (float) newHeading;
    }

    private double getNewZoomLevel(double zoom, double step) {
        if (this.mode != AnimationMode.ZOOM_TILT_ANIMATION || zoom == -1.0d || step == -1.0d) {
            return -1.0d;
        }
        double currentZoomLevel = this.mapController.getZoomLevel();
        double animatorZoom = currentZoomLevel + ((zoom - currentZoomLevel) * step);
        logger.v("animatorZoom: " + animatorZoom);
        return animatorZoom;
    }

    private float getNewTilt(float tilt, double step) {
        if (this.mode != AnimationMode.ZOOM_TILT_ANIMATION || tilt == -1.0f || step == -1.0d) {
            return -1.0f;
        }
        float currentTilt = this.mapController.getTilt();
        float animatorTilt = (float) (((double) currentTilt) + (((double) (tilt - currentTilt)) * step));
        logger.v("animatorTilt: " + animatorTilt);
        return animatorTilt;
    }

    public void startMapRendering() {
        logger.v("startMapRendering: rendering enabled");
        this.renderingEnabled = true;
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                GeoPosition pos = HereMapAnimator.this.currentGeoPosition;
                if (pos == null) {
                    return;
                }
                if (HereMapAnimator.this.mode == AnimationMode.NONE) {
                    HereMapAnimator.this.setGeoPosition(pos);
                    HereMapAnimator.logger.v("rendering: set last pos");
                    return;
                }
                HereMapAnimator.logger.v("rendering: set center");
                HereMapAnimator.this.mapController.setCenter(pos.getCoordinate(), Animation.NONE, -1.0d, (float) pos.getHeading(), -1.0f);
            }
        }, 17);
    }

    public void stopMapRendering() {
        logger.v("stopMapRendering: rendering disabled");
        this.renderingEnabled = false;
    }

    public void clearState() {
        logger.v("clearState");
        this.lastHeading = -1.0f;
    }
}
