package com.navdy.hud.app.maps.here;

import com.here.android.mpa.guidance.NavigationManager.GpsSignalListener;
import com.navdy.hud.app.maps.MapsEventHandler;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;

public class HereGpsSignalListener extends GpsSignalListener {
    private Bus bus;
    private HereNavigationManager hereNavigationManager;
    private Logger logger;
    private MapsEventHandler mapsEventHandler;
    private String tag;

    HereGpsSignalListener(Logger logger, String tag, Bus bus, MapsEventHandler mapsEventHandler, HereNavigationManager hereNavigationManager) {
        this.logger = logger;
        this.tag = tag;
        this.bus = bus;
        this.mapsEventHandler = mapsEventHandler;
        this.hereNavigationManager = hereNavigationManager;
    }

    public void onGpsLost() {
        this.logger.w(this.tag + " Gps signal lost");
        this.bus.post(this.hereNavigationManager.BUS_GPS_SIGNAL_LOST);
    }

    public void onGpsRestored() {
        this.logger.i(this.tag + " Gps signal restored");
        this.bus.post(this.hereNavigationManager.BUS_GPS_SIGNAL_RESTORED);
    }
}
