package com.navdy.hud.app.maps.here;

import android.os.Handler;
import android.os.Looper;
import com.here.android.mpa.guidance.NavigationManager.NavigationManagerEventListener;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.Route.TrafficPenaltyMode;
import com.navdy.hud.app.maps.MapEvents.TrafficDelayDismissEvent;
import com.navdy.hud.app.maps.MapEvents.TrafficDelayEvent;
import com.navdy.hud.app.maps.here.HereNavController.State;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class HereTrafficETATracker extends NavigationManagerEventListener {
    private static final long ETA_THRESHOLD = TimeUnit.MINUTES.toMillis(10);
    private static final long INVALID = -1;
    private static final long REFRESH_ETA_INTERVAL_MILLIS = TimeUnit.SECONDS.toMillis(60);
    private long baseEta;
    private final Bus bus;
    private Runnable dismissEtaRunnable = new Runnable() {
        public void run() {
            HereTrafficETATracker.this.sendTrafficDelayDismissEvent();
        }
    };
    private final Handler handler;
    private final HereNavController navController;
    private Runnable navigationModeChangeBkRunnable = new Runnable() {
        public void run() {
            State state = HereTrafficETATracker.this.navController.getState();
            if (state == State.NAVIGATING) {
                Date etaDate = HereTrafficETATracker.this.navController.getEta(true, TrafficPenaltyMode.OPTIMAL);
                if (HereMapUtil.isValidEtaDate(etaDate)) {
                    HereTrafficETATracker.this.baseEta = etaDate.getTime();
                    HereTrafficETATracker.this.sLogger.v("got baseEta from eta:" + HereTrafficETATracker.this.baseEta);
                } else {
                    etaDate = HereTrafficETATracker.this.navController.getTtaDate(true, TrafficPenaltyMode.OPTIMAL);
                    if (HereMapUtil.isValidEtaDate(etaDate)) {
                        HereTrafficETATracker.this.baseEta = etaDate.getTime();
                        HereTrafficETATracker.this.sLogger.v("got baseEta from eta--tta:" + HereTrafficETATracker.this.baseEta);
                    } else {
                        HereTrafficETATracker.this.baseEta = -1;
                        HereTrafficETATracker.this.sendTrafficDelayDismissEvent();
                        HereTrafficETATracker.this.sLogger.i("no baseEta");
                    }
                }
                HereTrafficETATracker.this.refreshEta();
                return;
            }
            HereTrafficETATracker.this.sLogger.i("no baseEta, not navigation:" + state);
            HereTrafficETATracker.this.baseEta = -1;
            HereTrafficETATracker.this.handler.removeCallbacks(HereTrafficETATracker.this.refreshEtaRunnable);
            HereTrafficETATracker.this.handler.removeCallbacks(HereTrafficETATracker.this.dismissEtaRunnable);
            HereTrafficETATracker.this.sendTrafficDelayDismissEvent();
        }
    };
    private Runnable refreshEtaBkRunnable = new Runnable() {
        public void run() {
            HereTrafficETATracker.this.etaUpdate();
        }
    };
    private Runnable refreshEtaRunnable = new Runnable() {
        public void run() {
            TaskManager.getInstance().execute(HereTrafficETATracker.this.refreshEtaBkRunnable, 3);
        }
    };
    private final Logger sLogger = new Logger(HereTrafficETATracker.class);

    public HereTrafficETATracker(HereNavController navController, Bus bus) {
        this.navController = navController;
        this.bus = bus;
        this.handler = new Handler(Looper.getMainLooper());
        this.sLogger.v("ctor");
    }

    private void refreshEta() {
        this.sLogger.v("posting refresh ETA in 60 sec...");
        this.handler.removeCallbacks(this.refreshEtaRunnable);
        this.handler.postDelayed(this.refreshEtaRunnable, REFRESH_ETA_INTERVAL_MILLIS);
    }

    private void etaUpdate() {
        this.sLogger.v("etaUpdate");
        if (this.baseEta == -1) {
            this.sLogger.v("base ETA is invalid");
            return;
        }
        Date etaDate = this.navController.getEta(true, TrafficPenaltyMode.OPTIMAL);
        if (HereMapUtil.isValidEtaDate(etaDate)) {
            long newEta = etaDate.getTime();
            long etaDiff = newEta - this.baseEta;
            if (etaDiff > ETA_THRESHOLD) {
                this.sLogger.v("ETA threshold delay, triggering delay:" + etaDiff);
                this.baseEta = newEta;
                this.bus.post(new TrafficDelayEvent(etaDiff / 1000));
                this.handler.removeCallbacks(this.dismissEtaRunnable);
                this.handler.postDelayed(this.dismissEtaRunnable, ETA_THRESHOLD);
            } else {
                this.sLogger.v("ETA threshold ok:" + etaDiff);
            }
        }
        refreshEta();
    }

    public void onRouteUpdated(Route route) {
        sendTrafficDelayDismissEvent();
    }

    public void onNavigationModeChanged() {
        TaskManager.getInstance().execute(this.navigationModeChangeBkRunnable, 3);
    }

    private void sendTrafficDelayDismissEvent() {
        this.bus.post(new TrafficDelayDismissEvent());
    }
}
