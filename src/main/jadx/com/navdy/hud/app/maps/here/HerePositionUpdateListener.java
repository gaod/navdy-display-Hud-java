package com.navdy.hud.app.maps.here;

import android.os.SystemClock;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.MatchedGeoPosition;
import com.here.android.mpa.guidance.NavigationManager.PositionListener;
import com.here.android.mpa.routing.Route.TrafficPenaltyMode;
import com.here.android.mpa.routing.RouteTta;
import com.navdy.hud.app.framework.trips.TripManager.TrackingEvent;
import com.navdy.hud.app.util.CrashReporter;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;

public class HerePositionUpdateListener extends PositionListener {
    private final Bus bus;
    private HereNavigationManager hereNavigationManager;
    private GeoPosition lastGeoPosition;
    private long lastGeoPositionTime;
    private Logger logger = new Logger("HerePositionListener");
    private HereNavController navController;
    private String tag;

    public static class MapMatchEvent {
        public final MatchedGeoPosition mapMatch;

        public MapMatchEvent(MatchedGeoPosition mapMatch) {
            this.mapMatch = mapMatch;
        }
    }

    HerePositionUpdateListener(String tag, HereNavController navController, HereNavigationManager hereNavigationManager, Bus bus) {
        this.tag = tag;
        this.navController = navController;
        this.hereNavigationManager = hereNavigationManager;
        this.bus = bus;
        bus.register(this);
    }

    public void onPositionUpdated(final GeoPosition geoPosition) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                HerePositionUpdateListener.this.handlePositionUpdate(geoPosition);
            }
        }, 4);
    }

    private void handlePositionUpdate(GeoPosition geoPosition) {
        try {
            if (this.navController.isInitialized() && geoPosition != null) {
                if (this.lastGeoPosition != null) {
                    long t = SystemClock.elapsedRealtime() - this.lastGeoPositionTime;
                    if (t < 500 && HereMapUtil.isCoordinateEqual(this.lastGeoPosition.getCoordinate(), geoPosition.getCoordinate())) {
                        if (this.logger.isLoggable(2)) {
                            this.logger.v(this.tag + "GEO-Here-Nav same pos as last:" + t);
                            return;
                        }
                        return;
                    }
                }
                this.lastGeoPosition = geoPosition;
                this.lastGeoPositionTime = SystemClock.elapsedRealtime();
                if (this.logger.isLoggable(2)) {
                    this.logger.v(this.tag + " onPosUpdated: road name =" + HereMapUtil.getCurrentRoadName());
                }
                postTrackingEvent(geoPosition);
                this.hereNavigationManager.refreshNavigationInfo();
            }
        } catch (Throwable t2) {
            this.logger.e(t2);
            CrashReporter.getInstance().reportNonFatalException(t2);
        }
    }

    private void postTrackingEvent(GeoPosition geoPosition) {
        if (this.hereNavigationManager.isNavigationModeOn()) {
            RouteTta routeTta = this.navController.getTta(TrafficPenaltyMode.OPTIMAL, true);
            if (routeTta != null) {
                this.bus.post(new TrackingEvent(geoPosition, this.hereNavigationManager.getCurrentDestinationIdentifier(), routeTta.getDuration(), (int) this.navController.getDestinationDistance()));
                return;
            } else {
                this.bus.post(new TrackingEvent(geoPosition));
                return;
            }
        }
        this.bus.post(new TrackingEvent(geoPosition));
    }
}
