package com.navdy.hud.app.presenter;

import android.os.Bundle;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.view.NotificationView;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class NotificationPresenter extends BasePresenter<NotificationView> {
    private static final Logger sLogger = new Logger(NotificationPresenter.class);
    @Inject
    Bus bus;

    public void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);
        this.bus.register(this);
    }
}
