package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardException extends Exception {
    public VCardException(String message) {
        super(message);
    }
}
