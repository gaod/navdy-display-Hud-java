package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardInvalidCommentLineException extends VCardInvalidLineException {
    public VCardInvalidCommentLineException(String message) {
        super(message);
    }
}
