package com.navdy.hud.app.bluetooth.vcard;

public interface VCardEntryHandler {
    void onEnd();

    void onEntryCreated(VCardEntry vCardEntry);

    void onStart();
}
