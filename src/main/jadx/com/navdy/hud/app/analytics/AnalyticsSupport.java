package com.navdy.hud.app.analytics;

import android.app.Application;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.net.LocalSocketAddress.Namespace;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import com.amazonaws.services.s3.internal.Constants;
import com.amazonaws.services.s3.model.BucketLifecycleConfiguration;
import com.here.android.mpa.routing.Maneuver;
import com.localytics.android.Localytics;
import com.localytics.android.LocalyticsActivityLifecycleCallbacks;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.common.TimeHelper.DateTimeAvailableEvent;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.device.dial.DialConstants;
import com.navdy.hud.app.device.gps.GpsConstants;
import com.navdy.hud.app.event.DeviceInfoAvailable;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.event.Shutdown.Reason;
import com.navdy.hud.app.event.Shutdown.State;
import com.navdy.hud.app.event.Wakeup;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.connection.ConnectionNotification;
import com.navdy.hud.app.framework.glance.GlanceNotification;
import com.navdy.hud.app.framework.network.NetworkBandwidthController;
import com.navdy.hud.app.framework.network.NetworkBandwidthController.Component;
import com.navdy.hud.app.framework.network.NetworkBandwidthController.UserBandwidthSettingChanged;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.framework.toast.ToastManager.ShowToast;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.MapEvents.ArrivalEvent;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.maps.MapEvents.ManeuverEvent;
import com.navdy.hud.app.maps.MapEvents.ManeuverEvent.Type;
import com.navdy.hud.app.maps.MapEvents.ManeuverSoonEvent;
import com.navdy.hud.app.maps.MapEvents.MapEngineInitialize;
import com.navdy.hud.app.maps.MapEvents.RegionEvent;
import com.navdy.hud.app.maps.MapEvents.RerouteEvent;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.maps.here.HereOfflineMapsVersion;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.service.HudConnectionService;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode;
import com.navdy.hud.app.ui.framework.INotificationAnimationListener;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.hud.app.util.SerialNumber;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.hud.app.view.TemperatureWarningView;
import com.navdy.obd.ECU;
import com.navdy.obd.Pid;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.audio.VoiceSearchResponse;
import com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState;
import com.navdy.service.library.events.destination.Destination;
import com.navdy.service.library.events.destination.Destination.FavoriteType;
import com.navdy.service.library.events.destination.Destination.SuggestionType;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.places.PlaceType;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem;
import com.navdy.service.library.events.preferences.InputPreferences;
import com.navdy.service.library.events.preferences.LocalPreferences;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.service.library.events.preferences.NavigationPreferences.RoutingType;
import com.navdy.service.library.events.preferences.NotificationPreferences;
import com.navdy.service.library.events.ui.DismissScreen;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Subscribe;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import mortar.Mortar;

public class AnalyticsSupport {
    private static final String ANALYTICS_EVENT_ADAPTIVE_BRIGHTNESS_ADJUSTMENT_CHANGE = "Adaptive_Brightness_Adjustment_Change";
    private static final String ANALYTICS_EVENT_ATTEMPT_TO_CONNECT = "Attempt_To_Connect";
    private static final String ANALYTICS_EVENT_BAD_ROUTE_POSITION = "Route_Bad_Pos";
    private static final String ANALYTICS_EVENT_BRIGHTNESS_ADJUSTMENT_CHANGE = "Brightness_Adjustment_Change";
    private static final String ANALYTICS_EVENT_BRIGHTNESS_CHANGE = "Brightness_Change";
    private static final String ANALYTICS_EVENT_CPU_OVERHEAT = "Cpu_Overheat";
    private static final String ANALYTICS_EVENT_DASH_GAUGE_SETTING = "Dash_Gauge_Setting";
    private static final String ANALYTICS_EVENT_DESTINATION_SUGGESTION = "Destination_Suggestion_Show";
    private static final String ANALYTICS_EVENT_DIAL_UPDATE_PROMPT = "Dial_Update_Prompt";
    private static final String ANALYTICS_EVENT_DISPLAY_UPDATE_PROMPT = "Display_Update_Prompt";
    private static final String ANALYTICS_EVENT_DISTANCE_TRAVELLED = "Distance_Travelled";
    private static final String ANALYTICS_EVENT_DRIVER_PREFERENCES = "Driver_Preferences";
    private static final String ANALYTICS_EVENT_FUEL_LEVEL = "Fuel_Level";
    public static final String ANALYTICS_EVENT_GLANCE_ADVANCE = "Glance_Advance";
    public static final String ANALYTICS_EVENT_GLANCE_DELETE_ALL = "Glance_Delete_All";
    public static final String ANALYTICS_EVENT_GLANCE_DISMISS = "Glance_Dismiss";
    public static final String ANALYTICS_EVENT_GLANCE_OPEN_FULL = "Glance_Open_Full";
    public static final String ANALYTICS_EVENT_GLANCE_OPEN_MINI = "Glance_Open_Mini";
    private static final String ANALYTICS_EVENT_GLYMPSE_SENT = "Glympse_Sent";
    private static final String ANALYTICS_EVENT_GLYMPSE_VIEWED = "Glympse_Viewed";
    public static final String ANALYTICS_EVENT_GPS_ACCURACY_STATISTICS = "GPS_Accuracy";
    public static final String ANALYTICS_EVENT_GPS_ACQUIRE_LOCATION = "GPS_Acquired_Location";
    public static final String ANALYTICS_EVENT_GPS_ATTEMPT_ACQUIRE_LOCATION = "GPS_Attempt_Acquire_Location";
    public static final String ANALYTICS_EVENT_GPS_CALIBRATION_IMU_DONE = "GPS_Calibration_IMU_Done";
    public static final String ANALYTICS_EVENT_GPS_CALIBRATION_LOST = "GPS_Calibration_Lost";
    public static final String ANALYTICS_EVENT_GPS_CALIBRATION_SENSOR_DONE = "GPS_Calibration_Sensor_Done";
    public static final String ANALYTICS_EVENT_GPS_CALIBRATION_START = "GPS_Calibration_Start";
    public static final String ANALYTICS_EVENT_GPS_CALIBRATION_VIN_SWITCH = "GPS_Calibration_VinSwitch";
    public static final String ANALYTICS_EVENT_GPS_LOST_LOCATION = "GPS_Lost_Signal";
    public static final String ANALYTICS_EVENT_IAP_FAILURE = "Mobile_iAP_Failure";
    public static final String ANALYTICS_EVENT_IAP_RETRY_SUCCESS = "Mobile_iAP_Retry_Success";
    private static final String ANALYTICS_EVENT_KEY_FAILED = "Key_Failed";
    private static final String ANALYTICS_EVENT_MENU_SELECTION = "Menu_Selection";
    private static final String ANALYTICS_EVENT_MILEAGE = "Navdy_Mileage";
    private static final String ANALYTICS_EVENT_MOBILE_APP_CONNECT = "Mobile_App_Connect";
    private static final String ANALYTICS_EVENT_MOBILE_APP_DISCONNECT = "Mobile_App_Disconnect";
    private static final String ANALYTICS_EVENT_MUSIC_ACTION = "Music_Action";
    private static final String ANALYTICS_EVENT_NAVIGATION_ARRIVED = "Navigation_Arrived";
    private static final String ANALYTICS_EVENT_NAVIGATION_DESTINATION_SET = "Navigation_Destination_Set";
    private static final String ANALYTICS_EVENT_NAVIGATION_MANEUVER_MISSED = "Navigation_Maneuver_Missed";
    private static final String ANALYTICS_EVENT_NAVIGATION_MANEUVER_REQUIRED = "Navigation_Maneuver_Required";
    private static final String ANALYTICS_EVENT_NAVIGATION_MANEUVER_WARNING = "Navigation_Maneuver_Warning";
    private static final String ANALYTICS_EVENT_NAVIGATION_PREFERENCES = "Navigation_Preferences";
    private static final String ANALYTICS_EVENT_NAVIGATION_REGION_CHANGE = "Map_Region_Change";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_ARRIVED = "Nearby_Search_Arrived";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_CLOSED = "Nearby_Search_Closed";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_DISMISS = "Nearby_Search_Dismiss";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_ENDED = "Nearby_Search_Ended";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_NO_RESULTS = "Nearby_Search_No_Results";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_CLOSE = "Nearby_Search_Results_Close";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_DISMISS = "Nearby_Search_Results_Dismiss";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_VIEW = "Nearby_Search_Results_View";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_RETURN_MM = "Nearby_Search_Return_Main_Menu";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_SELECTION = "Nearby_Search_Selection";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_TRIP_CANCELLED = "Nearby_Search_Trip_Cancelled";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_TRIP_INITIATED = "Nearby_Search_Trip_Initiated";
    public static final String ANALYTICS_EVENT_NEW_DEVICE = "Analytics_New_Device";
    private static final String ANALYTICS_EVENT_OBD_CONNECTED = "OBD_Connected";
    private static final String ANALYTICS_EVENT_OBD_LISTENING_POSTED = "OBD_Listening_Posted";
    private static final String ANALYTICS_EVENT_OBD_LISTENING_STATUS = "OBD_Listening_Status";
    private static final String ANALYTICS_EVENT_OFFLINE_MAPS_UPDATED = "Offline_Maps_Updated";
    private static final String ANALYTICS_EVENT_OPTION_SELECTED = "Option_Selected";
    private static final String ANALYTICS_EVENT_OTADOWNLOAD = "OTA_Download";
    private static final String ANALYTICS_EVENT_OTAINSTALL = "OTA_Install";
    private static final String ANALYTICS_EVENT_OTA_INSTALL_RESULT = "OTA_Install_Result";
    private static final String ANALYTICS_EVENT_PHONE_DISCONNECTED = "Phone_Disconnected";
    private static final String ANALYTICS_EVENT_ROUTE_COMPLETED = "Route_Completed";
    private static final String ANALYTICS_EVENT_SHUTDOWN = "Shutdown";
    private static final String ANALYTICS_EVENT_SMS_SENT = "sms_sent";
    private static final String ANALYTICS_EVENT_SWIPED_PREFIX = "Swiped_";
    private static final String ANALYTICS_EVENT_TEMPERATURE_CHANGE = "Temperature_Change";
    private static final String ANALYTICS_EVENT_VEHICLE_TELEMETRY_DATA = "Vehicle_Telemetry_Data";
    private static final String ANALYTICS_EVENT_VOICE_SEARCH_ANDROID = "Voice_Search_Android";
    private static final String ANALYTICS_EVENT_VOICE_SEARCH_IOS = "Voice_Search_iOS";
    private static final String ANALYTICS_EVENT_WAKEUP = "Wakeup";
    private static final String ANALYTICS_EVENT_ZERO_FUEL_DATA = "OBD_Zero_Fuel_Level";
    public static final String ANALYTICS_INTENT = "com.navdy.hud.app.analytics.AnalyticsEvent";
    public static final String ANALYTICS_INTENT_EXTRA_ARGUMENTS = "arguments";
    public static final String ANALYTICS_INTENT_EXTRA_INCLUDE_REMOTE_DEVICE_INFO = "include_device_info";
    public static final String ANALYTICS_INTENT_EXTRA_TAG_NAME = "tag";
    private static final String ATTR_BRIGHTNESS_ADJ_NEW = "New";
    private static final String ATTR_BRIGHTNESS_ADJ_PREVIOUS = "Previous";
    private static final String ATTR_BRIGHTNESS_BRIGHTNESS = "Brightness";
    private static final String ATTR_BRIGHTNESS_LIGHT_SENSOR = "Light_Sensor";
    private static final String ATTR_BRIGHTNESS_NEW = "New";
    private static final String ATTR_BRIGHTNESS_PREVIOUS = "Previous";
    private static final String ATTR_BRIGHTNESS_RAW1_SENSOR = "Raw1_Sensor";
    private static final String ATTR_BRIGHTNESS_RAW_SENSOR = "Raw_Sensor";
    private static final String ATTR_CONN_ATTEMPT_NEW_DEVICE = "Was_New_Device";
    private static final String ATTR_CONN_ATTEMPT_TIME = "Time_To_Connect";
    private static final String ATTR_CONN_CAR_MAKE = "Car_Make";
    private static final String ATTR_CONN_CAR_MODEL = "Car_Model";
    private static final String ATTR_CONN_CAR_YEAR = "Car_Year";
    private static final String ATTR_CONN_DEVICE_ID = "Device_Id";
    private static final String ATTR_CONN_REMOTE_CLIENT_VERSION = "Remote_Client_Version";
    private static final String ATTR_CONN_REMOTE_DEVICE_ID = "Remote_Device_Id";
    private static final String ATTR_CONN_REMOTE_DEVICE_NAME = "Remote_Device_Name";
    private static final String ATTR_CONN_REMOTE_PLATFORM = "Remote_Platform";
    private static final String ATTR_CONN_VIN = "VIN";
    private static final String ATTR_CPU_OVERHEAT_STATE = "State";
    private static final String ATTR_CPU_OVERHEAT_UPTIME = "Uptime";
    private static final String ATTR_DESTINATION_SUGGESTION_ACCEPTED = "Accepted";
    private static final String ATTR_DESTINATION_SUGGESTION_TYPE = "Type";
    private static final String ATTR_DISC_CAR_MAKE = "Car_Make";
    private static final String ATTR_DISC_CAR_MODEL = "Car_Model";
    private static final String ATTR_DISC_CAR_YEAR = "Car_Year";
    private static final String ATTR_DISC_ELAPSED_TIME = "Elapsed_Time";
    private static final String ATTR_DISC_FORCE_RECONNECT = "Force_Reconnect";
    private static final String ATTR_DISC_FORCE_RECONNECT_REASON = "Force_Reconnect_Reason";
    private static final String ATTR_DISC_MOVING = "While_Moving";
    private static final String ATTR_DISC_PHONE_MAKE = "Phone_Make";
    private static final String ATTR_DISC_PHONE_MODEL = "Phone_Model";
    private static final String ATTR_DISC_PHONE_OS = "Phone_OS";
    private static final String ATTR_DISC_PHONE_TYPE = "Phone_Type";
    private static final String ATTR_DISC_REASON = "Reason";
    private static final String ATTR_DISC_STATUS = "Status";
    private static final String ATTR_DISC_VIN = "VIN";
    private static final String ATTR_DISTANCE = "Distance";
    private static final String ATTR_DRIVER_PREFERENCES_AUTO_ON = "Auto_On";
    private static final String ATTR_DRIVER_PREFERENCES_DISPLAY_FORMAT = "Display_Format";
    private static final String ATTR_DRIVER_PREFERENCES_FEATURE_MODE = "Feature_Mode";
    private static final String ATTR_DRIVER_PREFERENCES_GESTURES = "Gestures";
    private static final String ATTR_DRIVER_PREFERENCES_GLANCES = "Glances";
    private static final String ATTR_DRIVER_PREFERENCES_LIMIT_BANDWIDTH = "Limit_Bandwidth";
    private static final String ATTR_DRIVER_PREFERENCES_LOCALE = "Locale";
    private static final String ATTR_DRIVER_PREFERENCES_MANUAL_ZOOM = "Manual_Zoom";
    private static final String ATTR_DRIVER_PREFERENCES_OBD = "OBD";
    private static final String ATTR_DRIVER_PREFERENCES_UNITS = "Units";
    private static final String ATTR_DURATION = "Duration";
    private static final String ATTR_EVENT_AVERAGE_MPG = "Event_Average_Mpg";
    private static final String ATTR_EVENT_AVERAGE_RPM = "Event_Average_Rpm";
    private static final String ATTR_EVENT_ENGINE_TEMPERATURE = "Event_Engine_Temperature";
    private static final String ATTR_EVENT_FUEL_LEVEL = "Event_Fuel_Level";
    private static final String ATTR_EVENT_MAX_G = "Event_Max_G";
    private static final String ATTR_EVENT_MAX_G_ANGLE = "Event_Max_G_Angle";
    private static final String ATTR_FUEL_CONSUMPTION = "Fuel_Consumption";
    private static final String ATTR_FUEL_DISTANCE = "Distance";
    private static final String ATTR_FUEL_LEVEL = "Fuel_Level";
    private static final String ATTR_FUEL_VIN = "VIN";
    private static final String ATTR_GAUGE_ID = "Gauge_name";
    private static final String ATTR_GAUGE_SIDE = "Side";
    private static final String ATTR_GAUGE_SIDE_LEFT = "Left";
    private static final String ATTR_GAUGE_SIDE_RIGHT = "Right";
    private static final String ATTR_GLANCE_NAME = "Glance_Name";
    private static final String ATTR_GLANCE_NAV_METHOD = "Nav_Method";
    private static final String ATTR_GLYMPSE_ID = "Id";
    private static final String ATTR_GLYMPSE_MESSAGE = "Message";
    private static final String ATTR_GLYMPSE_SHARE = "Share";
    private static final String ATTR_GLYMPSE_SUCCESS = "Success";
    private static final String ATTR_KEY_FAIL_ERROR = "Error";
    private static final String ATTR_KEY_FAIL_KEY_TYPE = "Key_Type";
    private static final String ATTR_MENU_NAME = "Name";
    private static final String ATTR_MUSIC_ACTION_TYPE = "Type";
    private static final String ATTR_NAVDY_KILOMETERS = "Navdy_Kilometers";
    private static final String ATTR_NAVDY_USAGE_RATE = "Navdy_Usage_Rate";
    private static final String ATTR_NAVIGATING = "Navigating";
    private static final String ATTR_NAVIGATION_PREFERENCES_ALLOW_AUTO_TRAINS = "Allow_Auto_Trains";
    private static final String ATTR_NAVIGATION_PREFERENCES_ALLOW_FERRIES = "Allow_Ferries";
    private static final String ATTR_NAVIGATION_PREFERENCES_ALLOW_HIGHWAYS = "Allow_Highways";
    private static final String ATTR_NAVIGATION_PREFERENCES_ALLOW_HOV_LANES = "Allow_HOV_Lanes";
    private static final String ATTR_NAVIGATION_PREFERENCES_ALLOW_TOLL_ROADS = "Allow_Toll_Roads";
    private static final String ATTR_NAVIGATION_PREFERENCES_ALLOW_TUNNELS = "Allow_Tunnels";
    private static final String ATTR_NAVIGATION_PREFERENCES_ALLOW_UNPAVED_ROADS = "Allow_Unpaved_Roads";
    private static final String ATTR_NAVIGATION_PREFERENCES_REROUTE_FOR_TRAFFIC = "Reroute_For_Traffic";
    private static final String ATTR_NAVIGATION_PREFERENCES_ROUTING_TYPE = "Routing_Type";
    private static final String ATTR_NAVIGATION_PREFERENCES_SHOW_TRAFFIC_NAVIGATING = "Show_Traffic_Navigating";
    private static final String ATTR_NAVIGATION_PREFERENCES_SHOW_TRAFFIC_OPEN_MAP = "Show_Traffic_Open_Map";
    private static final String ATTR_NAVIGATION_PREFERENCES_SPOKEN_SPEED_WARNINGS = "Spoken_Speed_Warnings";
    private static final String ATTR_NAVIGATION_PREFERENCES_SPOKEN_TBT = "Spoken_Turn_By_Turn";
    private static final String ATTR_NEARBY_SEARCH_DEST_SCROLLED = "Scrolled";
    private static final String ATTR_NEARBY_SEARCH_DEST_SELECTED_POSITION = "Selected_Position";
    private static final String ATTR_NEARBY_SEARCH_NO_RESULTS_ACTION = "Action";
    private static final String ATTR_NEARBY_SEARCH_PLACE_TYPE = "Place_Type";
    private static final String ATTR_OBD_CONN_ECU = "ECU";
    private static final String ATTR_OBD_CONN_FIRMWARE_VERSION = "Obd_Chip_Firmware";
    private static final String ATTR_OBD_CONN_PROTOCOL = "Protocol";
    private static final String ATTR_OBD_CONN_VIN = "VIN";
    private static final String ATTR_OBD_DATA_CAR_MAKE = "Car_Make";
    private static final String ATTR_OBD_DATA_CAR_MODEL = "Car_Model";
    private static final String ATTR_OBD_DATA_CAR_VIN = "Vin";
    private static final String ATTR_OBD_DATA_CAR_YEAR = "Car_Year";
    private static final String ATTR_OBD_DATA_MONITORING_DATA_SIZE = "Data_Size";
    private static final String ATTR_OBD_DATA_MONITORING_SUCCESS = "Success";
    private static final String ATTR_OBD_DATA_VERSION = "Version";
    private static final String ATTR_OFFLINE_MAPS_UPDATED_NEW_DATE = "New_Date";
    private static final String ATTR_OFFLINE_MAPS_UPDATED_NEW_PACKAGES = "New_Packages";
    private static final String ATTR_OFFLINE_MAPS_UPDATED_NEW_VERSION = "New_Version";
    private static final String ATTR_OFFLINE_MAPS_UPDATED_OLD_DATE = "Old_Date";
    private static final String ATTR_OFFLINE_MAPS_UPDATED_OLD_PACKAGES = "Old_Packages";
    private static final String ATTR_OFFLINE_MAPS_UPDATED_OLD_VERSION = "Old_Version";
    private static final String ATTR_OFFLINE_MAPS_UPDATED_UPDATE_COUNT = "Update_Count";
    private static final String ATTR_OPTION_NAME = "Name";
    private static final String ATTR_OTA_CURRENT_VERSION = "Current_Version";
    private static final String ATTR_OTA_INSTALL_INCREMENTAL = "Incremental";
    private static final String ATTR_OTA_INSTALL_PRIOR_VERSION = "Prior_Version";
    private static final String ATTR_OTA_INSTALL_SUCCESS = "Success";
    private static final String ATTR_OTA_UPDATE_VERSION = "Update_Version";
    private static final String ATTR_PHONE_CONNECTION = "Connection";
    public static final String ATTR_PLATFORM_ANDROID = "Android";
    public static final String ATTR_PLATFORM_IOS = "iOS";
    private static final String ATTR_PROFILE_BUILD_TYPE = "Build_Type";
    private static final String ATTR_PROFILE_CAR_MAKE = "Car_Make";
    private static final String ATTR_PROFILE_CAR_MODEL = "Car_Model";
    private static final String ATTR_PROFILE_CAR_YEAR = "Car_Year";
    private static final String ATTR_PROFILE_HUD_VERSION = "Hud_Version_Number";
    private static final String ATTR_PROFILE_SERIAL_NUMBER = "Hud_Serial_Number";
    private static final String ATTR_REGION_COUNTRY = "Country";
    private static final String ATTR_REGION_STATE = "State";
    private static final String ATTR_REMOTE_DEVICE_HARDWARE = "Remote_Device_Hardware";
    private static final String ATTR_REMOTE_DEVICE_PLATFORM = "Remote_Device_Platform";
    private static final String ATTR_REMOTE_DEVICE_SW_VERSION = "Remote_Device_SW_Version";
    private static final String ATTR_ROUTE_COMPLETED_ACTUAL_DISTANCE = "Actual_Distance";
    private static final String ATTR_ROUTE_COMPLETED_ACTUAL_DURATION = "Actual_Duration";
    private static final String ATTR_ROUTE_COMPLETED_DISTANCE_VARIANCE = "Distance_PCT_Diff";
    private static final String ATTR_ROUTE_COMPLETED_DURATION_VARIANCE = "Duration_PCT_Diff";
    private static final String ATTR_ROUTE_COMPLETED_EXPECTED_DISTANCE = "Expected_Distance";
    private static final String ATTR_ROUTE_COMPLETED_EXPECTED_DURATION = "Expected_Duration";
    private static final String ATTR_ROUTE_COMPLETED_RECALCULATIONS = "Recalculations";
    private static final String ATTR_ROUTE_COMPLETED_TRAFFIC_LEVEL = "Traffic_Level";
    private static final String ATTR_ROUTE_DISPLAY_POS = "Display_Pos";
    private static final String ATTR_ROUTE_NAV_POS = "Nav_Pos";
    private static final String ATTR_ROUTE_STREET_ADDRESS = "Street_Addr";
    private static final String ATTR_SESSION_ACCELERATION_COUNT = "Session_Hard_Acceleration_Count";
    private static final String ATTR_SESSION_AVERAGE_MPG = "Session_Average_Mpg";
    private static final String ATTR_SESSION_AVERAGE_ROLLING_SPEED = "Session_Average_Rolling_Speed";
    private static final String ATTR_SESSION_AVERAGE_SPEED = "Session_Average_Speed";
    private static final String ATTR_SESSION_DISTANCE = "Session_Distance";
    private static final String ATTR_SESSION_DURATION = "Session_Duration";
    private static final String ATTR_SESSION_EXCESSIVE_SPEEDING_FREQUENCY = "Session_Excessive_Speeding_Frequency";
    private static final String ATTR_SESSION_HARD_BRAKING_COUNT = "Session_Hard_Braking_Count";
    private static final String ATTR_SESSION_HIGH_G_COUNT = "Session_High_G_Count";
    private static final String ATTR_SESSION_MAX_RPM = "Session_Max_RPM";
    private static final String ATTR_SESSION_MAX_SPEED = "Session_Max_Speed";
    private static final String ATTR_SESSION_ROLLING_DURATION = "Session_Rolling_Duration";
    private static final String ATTR_SESSION_SPEEDING_FREQUENCY = "Session_Speeding_Frequency";
    private static final String ATTR_SESSION_TROUBLE_CODE_COUNT = "Session_Trouble_Code_Count";
    private static final String ATTR_SESSION_TROUBLE_CODE_LAST = "Session_Trouble_Code_Last";
    private static final String ATTR_SHUTDOWN_BATTERY_LEVEL = "Battery_Level";
    private static final String ATTR_SHUTDOWN_REASON = "Reason";
    private static final String ATTR_SHUTDOWN_SLEEP_TIME = "Sleep_Time";
    private static final String ATTR_SHUTDOWN_TYPE = "Type";
    private static final String ATTR_SHUTDOWN_UPTIME = "Uptime";
    private static final String ATTR_SMS_MESSAGE_TYPE = "Message_Type";
    private static final String ATTR_SMS_MESSAGE_TYPE_CANNED = "Canned";
    private static final String ATTR_SMS_MESSAGE_TYPE_CUSTOM = "Custom";
    private static final String ATTR_SMS_PLATFORM = "Platform";
    private static final String ATTR_SMS_SUCCESS = "Success";
    private static final String ATTR_SPEEDING_AMOUNT = "Session_Speeding_Amount";
    private static final String ATTR_TEMP_CPU_AVERAGE = "CPU_Average";
    private static final String ATTR_TEMP_CPU_MAX = "CPU_Max";
    private static final String ATTR_TEMP_CPU_TEMP = "CPU_Temperature";
    private static final String ATTR_TEMP_DMD_AVERAGE = "DMD_Average";
    private static final String ATTR_TEMP_DMD_MAX = "DMD_Max";
    private static final String ATTR_TEMP_DMD_TEMP = "DMD_Temperature";
    private static final String ATTR_TEMP_FAN_SPEED = "Fan_Speed";
    private static final String ATTR_TEMP_INLET_AVERAGE = "Inlet_Average";
    private static final String ATTR_TEMP_INLET_MAX = "Inlet_Max";
    private static final String ATTR_TEMP_INLET_TEMP = "Inlet_Temperature";
    private static final String ATTR_TEMP_LED_AVERAGE = "LED_Average";
    private static final String ATTR_TEMP_LED_MAX = "LED_Max";
    private static final String ATTR_TEMP_LED_TEMP = "LED_Temperature";
    private static final String ATTR_TEMP_WARNING = "Warning";
    private static final String ATTR_UPDATE_ACCEPTED = "Accepted";
    private static final String ATTR_UPDATE_TO_VERSION = "Update_To_Version";
    private static final String ATTR_VEHICLE_KILO_METERS = "Vehicle_Kilo_Meters";
    private static final String ATTR_VIN = "Vin";
    private static final String ATTR_VOICE_SEARCH_ADDITIONAL_RESULTS_GLANCE = "Additional_Results_Glance";
    private static final String ATTR_VOICE_SEARCH_CANCEL_TYPE = "Cancel_Type";
    private static final String ATTR_VOICE_SEARCH_CLIENT_VERSION = "Client_Version";
    private static final String ATTR_VOICE_SEARCH_CONFIDENCE_LEVEL = "Confidence_Level";
    private static final String ATTR_VOICE_SEARCH_DESTINATION_TYPE = "Destination_Type";
    private static final String ATTR_VOICE_SEARCH_EXPLICIT_RETRY = "Explicit_Retry";
    private static final String ATTR_VOICE_SEARCH_LIST_SELECTION = "Additional_Results_List_Item_Selected";
    private static final String ATTR_VOICE_SEARCH_MICROPHONE_USED = "Microphone_Used";
    private static final String ATTR_VOICE_SEARCH_NAVIGATION_INITIATED = "Navigation_Initiated";
    private static final String ATTR_VOICE_SEARCH_NAV_CANCELLED_QUICKLY = "Navigation_Cancelled_Quickly";
    private static final String ATTR_VOICE_SEARCH_NUMBER_OF_WORDS = "Number_Of_Words";
    private static final String ATTR_VOICE_SEARCH_NUM_RESULTS = "Num_Total_Results";
    private static final String ATTR_VOICE_SEARCH_PREFIX = "Prefix";
    private static final String ATTR_VOICE_SEARCH_RESPONSE = "Response";
    private static final String ATTR_VOICE_SEARCH_RETRY_COUNT = "Retry_Count";
    private static final String ATTR_VOICE_SEARCH_SUGGESTION = "Suggestion";
    private static final String ATTR_WAKEUP_BATTERY_DRAIN = "Battery_Drain";
    private static final String ATTR_WAKEUP_BATTERY_LEVEL = "Battery_Level";
    private static final String ATTR_WAKEUP_MAX_BATTERY = "Max_Battery";
    private static final String ATTR_WAKEUP_REASON = "Reason";
    private static final String ATTR_WAKEUP_SLEEP_TIME = "Sleep_Time";
    private static final String ATTR_WAKEUP_VIN = "VIN";
    private static final int CUSTOM_DIMENSION_HW_VERSION = 0;
    private static final String DESTINATION_SUGGESTION_TYPE_CALENDAR = "Calendar";
    private static final String DESTINATION_SUGGESTION_TYPE_SMART_SUGGESTION = "SmartSuggestion";
    private static final String FALSE = "false";
    private static final long FUEL_REPORTING_INTERVAL = 180000;
    private static final String LIGHT_SENSOR_DEVICE = "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_illuminance0_input";
    private static final String LIGHT_SENSOR_RAW1_DEVICE = "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw1";
    private static final String LIGHT_SENSOR_RAW_DEVICE = "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw";
    private static final int MAX_CPU_TEMP = 93;
    private static final long MAX_DMD_HIGH_TIME_MS = TimeUnit.MINUTES.toMillis(10);
    private static final int MAX_DMD_TEMP = 65;
    private static final long MAX_INLET_HIGH_TIME_MS = TimeUnit.MINUTES.toMillis(2);
    private static final int MAX_INLET_TEMP = 72;
    private static final long NAVIGATION_CANCEL_TIMEOUT = TimeUnit.MINUTES.toMillis(1);
    private static final int NOTEMP = -1000;
    private static final String OFFLINE_MAPS_UPDATE_COUNT = "offlineMapsUpdateCount";
    private static final int PREF_CHANGE_DELAY_MS = 15000;
    private static final String PREVIOUS_OFFLINE_MAPS_DATE = "previousOfflineMapsDate";
    private static final String PREVIOUS_OFFLINE_MAPS_PACKAGES = "previousOfflineMapsPackages";
    private static final String PREVIOUS_OFFLINE_MAPS_VERSION = "previousOfflineMapsVersion";
    private static final long TEMP_WARNING_BOOT_DELAY_S = TimeUnit.MINUTES.toSeconds(3);
    private static final long TEMP_WARNING_INTERVAL_MS = TimeUnit.MINUTES.toMillis(10);
    private static final String TRUE = "true";
    private static final long UPLOAD_INTERVAL = TimeUnit.MINUTES.toMillis(5);
    private static final long UPLOAD_INTERVAL_BOOT = TimeUnit.MINUTES.toMillis(5);
    private static final long UPLOAD_INTERVAL_LIMITED_BANDWIDTH = TimeUnit.MINUTES.toMillis(20);
    private static final boolean VERBOSE = false;
    private static int currentCpuAvgTemperature = -1;
    private static ArrayList<DeferredLocalyticsEvent> deferredEvents = null;
    private static DeferredWakeup deferredWakeup = null;
    private static boolean destinationSet = false;
    private static boolean disconnectDueToForceReconnect = false;
    private static String glanceNavigationSource = null;
    private static Handler handler = new Handler(Looper.getMainLooper());
    private static int lastDiscReason = 0;
    private static String lastForceReconnectReason = "";
    private static volatile boolean localyticsInitialized = false;
    private static int nearbySearchDestScrollPosition = -1;
    private static PlaceType nearbySearchPlaceType = null;
    private static boolean newDevicePaired = false;
    private static boolean phonePaired = false;
    private static boolean quietMode = false;
    private static DeviceInfo remoteDevice = null;
    private static FuelMonitor sFuelMonitor;
    private static PreferenceWatcher sLastNavigationPreferences = null;
    private static DriverProfile sLastProfile = null;
    private static PreferenceWatcher sLastProfilePreferences = null;
    private static String sLastVIN = null;
    private static Logger sLogger = new Logger(AnalyticsSupport.class);
    private static NotificationReceiver sNotificationReceiver;
    private static ObdManager sObdManager;
    private static String sRemoteDeviceId = null;
    private static RemoteDeviceManager sRemoteDeviceManager = RemoteDeviceManager.getInstance();
    private static Runnable sUploader = new Runnable() {
        public void run() {
            if (NetworkBandwidthController.getInstance().isNetworkAccessAllowed(Component.LOCALYTICS)) {
                AnalyticsSupport.sLogger.v("called Localytics.upload");
                Localytics.upload();
            }
            AnalyticsSupport.handler.postDelayed(this, AnalyticsSupport.getUpdateInterval());
        }
    };
    private static long startTime;
    private static double startingBatteryVoltage = -1.0d;
    private static int startingDistance;
    private static VoiceResultsRunnable voiceResultsRunnable = null;
    private static VoiceSearchAdditionalResultsAction voiceSearchAdditionalResultsAction = null;
    private static Integer voiceSearchConfidence = null;
    private static int voiceSearchCount = 0;
    private static boolean voiceSearchExplicitRetry = false;
    private static Integer voiceSearchListSelection = null;
    private static boolean voiceSearchListeningOverBluetooth = false;
    private static String voiceSearchPrefix = null;
    private static VoiceSearchProgress voiceSearchProgress = null;
    private static List<Destination> voiceSearchResults = null;
    private static String voiceSearchWords = null;
    private static long welcomeScreenTime = -1;

    public static class AnalyticsIntentsReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            String tagName = intent.getStringExtra(AnalyticsSupport.ANALYTICS_INTENT_EXTRA_TAG_NAME);
            boolean includeDeviceInfo = intent.getBooleanExtra(AnalyticsSupport.ANALYTICS_INTENT_EXTRA_INCLUDE_REMOTE_DEVICE_INFO, false);
            if (tagName != null) {
                String[] args = intent.getStringArrayExtra(AnalyticsSupport.ANALYTICS_INTENT_EXTRA_ARGUMENTS);
                if (args == null) {
                    args = new String[0];
                }
                if (tagName.equals(AnalyticsSupport.ANALYTICS_EVENT_NEW_DEVICE)) {
                    AnalyticsSupport.recordNewDevicePaired();
                } else {
                    AnalyticsSupport.localyticsSendEvent(tagName, includeDeviceInfo, args);
                }
            }
        }
    }

    private static class DeferredLocalyticsEvent {
        public String[] args;
        public String tag;

        DeferredLocalyticsEvent(String tag, String[] args) {
            this.tag = tag;
            this.args = args;
        }
    }

    private static class DeferredWakeup {
        String batteryDrain;
        String batteryLevel;
        String maxBattery;
        public WakeupReason reason;
        String sleepTime;

        DeferredWakeup(WakeupReason r, String bLevel, String bDrain, String sTime, String maxBat) {
            this.reason = r;
            this.batteryLevel = bLevel;
            this.batteryDrain = bDrain;
            this.sleepTime = sTime;
            this.maxBattery = maxBat;
        }
    }

    private static class EventSender implements Runnable {
        private Event event;

        EventSender(Event event) {
            this.event = event;
        }

        public void run() {
            Localytics.tagEvent(this.event.tag, this.event.argMap);
        }
    }

    private static class FuelMonitor implements Runnable {
        private FuelMonitor() {
        }

        /* synthetic */ FuelMonitor(AnonymousClass1 x0) {
            this();
        }

        public void run() {
            try {
                int distance = AnalyticsSupport.sObdManager.getDistanceTravelled();
                int fuelLevel = AnalyticsSupport.sObdManager.getFuelLevel();
                double fuelConsumption = AnalyticsSupport.sObdManager.getInstantFuelConsumption();
                String vin = AnalyticsSupport.sObdManager.getVin();
                if (fuelLevel != -1) {
                    AnalyticsSupport.localyticsSendEvent("Fuel_Level", "VIN", vin, "Fuel_Level", Integer.toString(fuelLevel), AnalyticsSupport.ATTR_FUEL_CONSUMPTION, Double.toString(fuelConsumption), "Distance", Integer.toString(distance));
                }
                AnalyticsSupport.handler.postDelayed(this, AnalyticsSupport.FUEL_REPORTING_INTERVAL);
            } catch (Throwable th) {
                AnalyticsSupport.handler.postDelayed(this, AnalyticsSupport.FUEL_REPORTING_INTERVAL);
            }
        }
    }

    interface PreferenceAdapter {
        Event calculateState(DriverProfile driverProfile);
    }

    public static class NotificationReceiver implements INotificationAnimationListener {
        @Inject
        public SharedPreferences appPreferences;
        private Maneuver lastRequiredManeuver = null;
        private ManeuverDisplay lastWarningEvent = null;
        @Inject
        public PowerManager powerManager;

        NotificationReceiver() {
            RemoteDeviceManager.getInstance().getBus().register(this);
            RemoteDeviceManager.getInstance().getUiStateManager().addNotificationAnimationListener(this);
            Mortar.inject(HudApplication.getAppContext(), this);
        }

        public void onStart(String id, NotificationType type, Mode mode) {
            if (mode == Mode.EXPAND) {
                AnalyticsSupport.recordGlanceAction(AnalyticsSupport.ANALYTICS_EVENT_GLANCE_OPEN_MINI, NotificationManager.getInstance().getCurrentNotification(), AnalyticsSupport.glanceNavigationSource != null ? AnalyticsSupport.glanceNavigationSource : "auto");
            }
            AnalyticsSupport.glanceNavigationSource = null;
        }

        public void onStop(String id, NotificationType type, Mode mode) {
        }

        @Subscribe
        public void onDeviceInfoAvailable(DeviceInfoAvailable deviceInfoAvailable) {
            if (deviceInfoAvailable.deviceInfo != null) {
                AnalyticsSupport.remoteDevice = deviceInfoAvailable.deviceInfo;
                AnalyticsSupport.setupProfile();
            }
        }

        @Subscribe
        public void onMapEngineInitialized(MapEngineInitialize event) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    AnalyticsSupport.recordOfflineMapsChange(NotificationReceiver.this.appPreferences);
                }
            }, 1);
        }

        @Subscribe
        public void onDateTimeAvailable(DateTimeAvailableEvent event) {
            AnalyticsSupport.maybeOpenSession();
        }

        @Subscribe
        public void onShutdown(Shutdown event) {
            if (event.state == State.SHUTTING_DOWN) {
                AnalyticsSupport.closeSession();
            }
        }

        @Subscribe
        public void onDriverProfileChange(DriverProfileChanged event) {
            DriverProfile profile = DriverProfileHelper.getInstance().getCurrentProfile();
            if (!profile.isDefaultProfile()) {
                AnalyticsSupport.sLastProfile = profile;
                AnalyticsSupport.sLogger.d("Remembering cached preferences");
                AnalyticsSupport.sLastProfilePreferences = new PreferenceWatcher(profile, new PreferenceAdapter() {
                    public Event calculateState(DriverProfile profile) {
                        return AnalyticsSupport.recordPreference(profile);
                    }
                });
                AnalyticsSupport.sLastNavigationPreferences = new PreferenceWatcher(profile, new PreferenceAdapter() {
                    public Event calculateState(DriverProfile profile) {
                        return AnalyticsSupport.recordNavigationPreference(profile);
                    }
                });
            }
        }

        @Subscribe
        public void onConnectionStatusChange(ConnectionStateChange event) {
            if (event.state == ConnectionState.CONNECTION_DISCONNECTED) {
                AnalyticsSupport.localyticsSendEvent(AnalyticsSupport.ANALYTICS_EVENT_MOBILE_APP_DISCONNECT, AnalyticsSupport.ATTR_CONN_DEVICE_ID, Build.SERIAL, AnalyticsSupport.ATTR_CONN_REMOTE_DEVICE_ID, AnalyticsSupport.sRemoteDeviceId);
                AnalyticsSupport.clearVoiceSearch();
            }
        }

        @Subscribe
        public void onShowToast(ShowToast toast) {
            if (toast.name.equals(ConnectionNotification.DISCONNECT_ID)) {
                AnalyticsSupport.recordPhoneDisconnect(false, null);
            }
        }

        @Subscribe
        public void onRegionEvent(RegionEvent event) {
            AnalyticsSupport.localyticsSendEvent(AnalyticsSupport.ANALYTICS_EVENT_NAVIGATION_REGION_CHANGE, "State", event.state, AnalyticsSupport.ATTR_REGION_COUNTRY, event.country);
        }

        private boolean objEquals(Object s1, Object s2) {
            return s1 == s2 || (s1 != null && s1.equals(s2));
        }

        @Subscribe
        public void onManeuverWarning(ManeuverDisplay event) {
            if (this.lastWarningEvent == null || event.turnIconId != this.lastWarningEvent.turnIconId || !objEquals(event.pendingTurn, this.lastWarningEvent.pendingTurn) || !objEquals(event.pendingRoad, this.lastWarningEvent.pendingRoad) || !objEquals(event.currentRoad, this.lastWarningEvent.currentRoad) || !objEquals(event.navigationTurn, this.lastWarningEvent.navigationTurn)) {
                AnalyticsSupport.localyticsSendEvent(AnalyticsSupport.ANALYTICS_EVENT_NAVIGATION_MANEUVER_WARNING, new String[0]);
                this.lastWarningEvent = event;
            }
        }

        @Subscribe
        public void onManeuverRequired(ManeuverSoonEvent event) {
            if (event.maneuver != this.lastRequiredManeuver) {
                AnalyticsSupport.localyticsSendEvent(AnalyticsSupport.ANALYTICS_EVENT_NAVIGATION_MANEUVER_REQUIRED, new String[0]);
                this.lastRequiredManeuver = event.maneuver;
            }
        }

        @Subscribe
        public void onManeuverMissed(RerouteEvent event) {
            AnalyticsSupport.localyticsSendEvent(AnalyticsSupport.ANALYTICS_EVENT_NAVIGATION_MANEUVER_MISSED, new String[0]);
        }

        @Subscribe
        public void onArrived(ManeuverEvent event) {
            if (event.type == Type.LAST) {
                AnalyticsSupport.sLogger.v("NAVIGATION_ARRIVED");
                AnalyticsSupport.localyticsSendEvent(AnalyticsSupport.ANALYTICS_EVENT_NAVIGATION_ARRIVED, new String[0]);
            }
        }

        @Subscribe
        public void onDismissScreen(DismissScreen event) {
        }

        @Subscribe
        public void onWakeup(Wakeup event) {
            AnalyticsSupport.recordWakeup(event.reason);
            AnalyticsSupport.quietMode = false;
            AnalyticsSupport.maybeOpenSession();
        }

        @Subscribe
        public void onBandwidthSettingChanged(UserBandwidthSettingChanged event) {
            long interval = AnalyticsSupport.getUpdateInterval();
            AnalyticsSupport.sLogger.v("uploader changed to " + interval);
            AnalyticsSupport.handler.removeCallbacks(AnalyticsSupport.sUploader);
            AnalyticsSupport.handler.postDelayed(AnalyticsSupport.sUploader, interval);
        }

        @Subscribe
        public void ObdStateChangeEvent(ObdConnectionStatusEvent event) {
            if (event.connected) {
                AnalyticsSupport.reportObdState();
            }
        }

        @Subscribe
        public void onRouteSearchRequest(NavigationRouteRequest event) {
            AnalyticsSupport.processNavigationRouteRequest(event);
        }

        @Subscribe
        public void onArrived(ArrivalEvent event) {
            AnalyticsSupport.processNavigationArrivalEvent(event);
        }
    }

    private static class PreferenceWatcher {
        PreferenceAdapter adapter;
        Event oldState;
        Runnable queuedUpdate;

        PreferenceWatcher(DriverProfile profile, PreferenceAdapter adapter) {
            this.oldState = adapter.calculateState(profile);
            this.adapter = adapter;
        }

        void enqueue(final DriverProfile profile) {
            if (this.queuedUpdate != null) {
                AnalyticsSupport.handler.removeCallbacks(this.queuedUpdate);
            }
            this.queuedUpdate = new Runnable() {
                public void run() {
                    Event newState = PreferenceWatcher.this.adapter.calculateState(profile);
                    String changedFields = newState.getChangedFields(PreferenceWatcher.this.oldState);
                    if (TextUtils.isEmpty(changedFields)) {
                        AnalyticsSupport.sLogger.d("No change in " + newState.tag);
                        return;
                    }
                    newState.argMap.put("Preferences_Updated", changedFields);
                    PreferenceWatcher.this.oldState = newState;
                    AnalyticsSupport.sLogger.d("Recording preference change: " + newState);
                    AnalyticsSupport.localyticsSendEvent(newState, false);
                }
            };
            AnalyticsSupport.handler.postDelayed(this.queuedUpdate, 15000);
        }
    }

    public static class TemperatureReporter extends Thread {
        private static final String SOCKET_NAME = "tempstatus";
        @Inject
        PowerManager powerManager;

        public void run() {
            try {
                LocalSocket socket = new LocalSocket();
                socket.connect(new LocalSocketAddress(SOCKET_NAME, Namespace.RESERVED));
                InputStream socketInputStream = socket.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socketInputStream));
                int retries = 0;
                try {
                    Threshold inletThreshold = new Threshold(72, AnalyticsSupport.MAX_INLET_HIGH_TIME_MS);
                    Threshold dmdThreshold = new Threshold(AnalyticsSupport.MAX_DMD_TEMP, AnalyticsSupport.MAX_DMD_HIGH_TIME_MS);
                    while (true) {
                        String origData = bufferedReader.readLine();
                        if (origData != null) {
                            int nFields;
                            origData = origData.trim();
                            String data = origData;
                            String[] fields = new String[13];
                            int nFields2 = 0;
                            while (true) {
                                nFields = nFields2;
                                if (nFields >= fields.length - 1 || !data.contains(" ")) {
                                    nFields2 = nFields + 1;
                                } else {
                                    String[] split = data.split(" +", 2);
                                    nFields2 = nFields + 1;
                                    fields[nFields] = split[0];
                                    data = split[1];
                                }
                            }
                            nFields2 = nFields + 1;
                            fields[nFields] = data;
                            if (nFields2 != fields.length) {
                                AnalyticsSupport.sLogger.e(String.format(Locale.US, "missing fields in temperature reporting data, found = %d, expected = %d data: %s", new Object[]{Integer.valueOf(nFields2), Integer.valueOf(fields.length), origData}));
                            } else {
                                String str;
                                if (fields[nFields2 - 1].contains(" ")) {
                                    AnalyticsSupport.sLogger.w("too many fields in temperature reporting data: " + origData);
                                    fields[nFields2 - 1] = fields[nFields2 - 1].split(" ")[0];
                                }
                                int avgCpuTemp = AnalyticsSupport.parseTemp(fields[5]);
                                AnalyticsSupport.currentCpuAvgTemperature = avgCpuTemp;
                                int inletTemp = AnalyticsSupport.parseTemp(fields[2]);
                                int dmdTemp = AnalyticsSupport.parseTemp(fields[3]);
                                long now = SystemClock.elapsedRealtime();
                                dmdThreshold.update(dmdTemp);
                                boolean doShutdown = dmdThreshold.isHit();
                                boolean doWarning = false;
                                if (!this.powerManager.inQuietMode()) {
                                    inletThreshold.update(inletTemp);
                                    doWarning = avgCpuTemp >= AnalyticsSupport.MAX_CPU_TEMP || inletThreshold.isHit();
                                    long lastWarningTime = TemperatureWarningView.getLastDismissedTime();
                                    if (lastWarningTime == 0 || (lastWarningTime != -1 && now - lastWarningTime <= AnalyticsSupport.TEMP_WARNING_INTERVAL_MS)) {
                                        doWarning = false;
                                    }
                                    if (doWarning && AnalyticsSupport.uptime() < AnalyticsSupport.TEMP_WARNING_BOOT_DELAY_S) {
                                        doWarning = false;
                                    }
                                }
                                if (doWarning) {
                                    RemoteDeviceManager.getInstance().getBus().post(new Builder().screen(Screen.SCREEN_TEMPERATURE_WARNING).build());
                                }
                                if (this.powerManager.inQuietMode() && doShutdown) {
                                    AnalyticsSupport.sLogger.d("Shutting down due to high DMD temperature");
                                    RemoteDeviceManager.getInstance().getBus().post(new Shutdown(Reason.HIGH_TEMPERATURE));
                                }
                                String str2 = AnalyticsSupport.ANALYTICS_EVENT_TEMPERATURE_CHANGE;
                                String[] strArr = new String[28];
                                strArr[0] = AnalyticsSupport.ATTR_TEMP_FAN_SPEED;
                                strArr[1] = fields[0];
                                strArr[2] = AnalyticsSupport.ATTR_TEMP_CPU_TEMP;
                                strArr[3] = fields[1];
                                strArr[4] = AnalyticsSupport.ATTR_TEMP_INLET_TEMP;
                                strArr[5] = fields[2];
                                strArr[6] = AnalyticsSupport.ATTR_TEMP_DMD_TEMP;
                                strArr[7] = fields[3];
                                strArr[8] = AnalyticsSupport.ATTR_TEMP_LED_TEMP;
                                strArr[9] = fields[4];
                                strArr[10] = AnalyticsSupport.ATTR_TEMP_CPU_AVERAGE;
                                strArr[11] = fields[5];
                                strArr[12] = AnalyticsSupport.ATTR_TEMP_INLET_AVERAGE;
                                strArr[13] = fields[6];
                                strArr[14] = AnalyticsSupport.ATTR_TEMP_DMD_AVERAGE;
                                strArr[15] = fields[7];
                                strArr[16] = AnalyticsSupport.ATTR_TEMP_LED_AVERAGE;
                                strArr[17] = fields[8];
                                strArr[18] = AnalyticsSupport.ATTR_TEMP_CPU_MAX;
                                strArr[19] = fields[9];
                                strArr[20] = AnalyticsSupport.ATTR_TEMP_INLET_MAX;
                                strArr[21] = fields[10];
                                strArr[22] = AnalyticsSupport.ATTR_TEMP_DMD_MAX;
                                strArr[23] = fields[11];
                                strArr[24] = AnalyticsSupport.ATTR_TEMP_LED_MAX;
                                strArr[25] = fields[12];
                                strArr[26] = AnalyticsSupport.ATTR_TEMP_WARNING;
                                if (doWarning) {
                                    str = AnalyticsSupport.TRUE;
                                } else {
                                    str = "false";
                                }
                                strArr[27] = str;
                                AnalyticsSupport.localyticsSendEvent(str2, strArr);
                            }
                        }
                    }
                } catch (Exception e) {
                    AnalyticsSupport.sLogger.e("error reading from temperature reporting socket", e);
                    retries++;
                    if (retries > 3) {
                        AnalyticsSupport.sLogger.e("too many errors reading temperature reporting socket, giving up");
                        IOUtils.closeStream(socketInputStream);
                        IOUtils.closeStream(socket);
                    }
                } catch (Throwable th) {
                    IOUtils.closeStream(socketInputStream);
                    IOUtils.closeStream(socket);
                }
            } catch (Exception e2) {
                AnalyticsSupport.sLogger.e("error connecting to temperature reporting socket", e2);
            }
        }
    }

    private static class Threshold {
        private final int max;
        private final long minimumTime;
        private long triggerTime = -1;

        Threshold(int max, long minimumTime) {
            this.max = max;
            this.minimumTime = minimumTime;
        }

        public void update(int val) {
            if (val < this.max) {
                this.triggerTime = -1;
            } else if (this.triggerTime == -1) {
                this.triggerTime = SystemClock.elapsedRealtime();
            }
        }

        boolean isHit() {
            return this.triggerTime != -1 && SystemClock.elapsedRealtime() - this.triggerTime >= this.minimumTime;
        }
    }

    private static class VoiceResultsRunnable implements Runnable {
        VoiceSearchAdditionalResultsAction additionalResultsAction;
        private boolean done = false;
        boolean explicitRetry;
        Integer listItemSelected = null;
        String microphoneUsed;
        String prefix = null;
        private NavigationRouteRequest request;
        private int retryCount;
        private Integer searchConfidence;
        List<Destination> searchResults;
        private String searchWords;

        VoiceResultsRunnable(NavigationRouteRequest req) {
            this.request = req;
            this.searchWords = AnalyticsSupport.voiceSearchWords;
            this.searchConfidence = AnalyticsSupport.voiceSearchConfidence;
            this.retryCount = AnalyticsSupport.voiceSearchCount - 1;
            this.microphoneUsed = AnalyticsSupport.voiceSearchListeningOverBluetooth ? "bluetooth" : GpsConstants.USING_PHONE_LOCATION;
            this.searchResults = AnalyticsSupport.voiceSearchResults;
            this.additionalResultsAction = AnalyticsSupport.voiceSearchAdditionalResultsAction;
            this.explicitRetry = AnalyticsSupport.voiceSearchExplicitRetry;
            this.listItemSelected = AnalyticsSupport.voiceSearchListSelection;
            this.prefix = AnalyticsSupport.voiceSearchPrefix;
        }

        public synchronized void run() {
            if (!this.done) {
                AnalyticsSupport.sLogger.i("sending voice search event: " + this);
                AnalyticsSupport.sendVoiceSearchEvent(null, this.request, this.searchWords, this.searchConfidence, this.retryCount, this.microphoneUsed, this.searchResults, this.additionalResultsAction, this.explicitRetry, this.listItemSelected, this.prefix);
                AnalyticsSupport.voiceResultsRunnable = null;
                this.done = true;
            }
        }

        public synchronized void cancel(VoiceSearchCancelReason reason) {
            if (!this.done) {
                AnalyticsSupport.handler.removeCallbacks(this);
                AnalyticsSupport.voiceResultsRunnable = null;
                this.done = true;
                AnalyticsSupport.sLogger.i("cancelling voice search reason: " + reason + " data:" + this);
                AnalyticsSupport.sendVoiceSearchEvent(reason, this.request, this.searchWords, this.searchConfidence, this.retryCount, this.microphoneUsed, this.searchResults, this.additionalResultsAction, this.explicitRetry, this.listItemSelected, this.prefix);
            }
        }

        public String toString() {
            return "VoiceResultsRunnable{done=" + this.done + ", request=" + this.request + ", searchWords='" + this.searchWords + '\'' + ", searchConfidence=" + this.searchConfidence + ", retryCount=" + this.retryCount + ", microphoneUsed='" + this.microphoneUsed + '\'' + ", searchResults=" + this.searchResults + ", additionalResultsAction=" + this.additionalResultsAction + ", explicitRetry=" + this.explicitRetry + ", listItemSelected=" + this.listItemSelected + ", prefix='" + this.prefix + '\'' + '}';
        }
    }

    public enum VoiceSearchAdditionalResultsAction {
        ADDITIONAL_RESULTS_TIMEOUT("timeout"),
        ADDITIONAL_RESULTS_LIST("list"),
        ADDITIONAL_RESULTS_GO("go");
        
        public final String tag;

        private VoiceSearchAdditionalResultsAction(String tag) {
            this.tag = tag;
        }
    }

    private enum VoiceSearchCancelReason {
        end_trip,
        new_voice_search,
        new_non_voice_search,
        cancel_search,
        cancel_route_calculation,
        cancel_list
    }

    private enum VoiceSearchProgress {
        PROGRESS_SEARCHING,
        PROGRESS_DESTINATION_FOUND,
        PROGRESS_LIST_DISPLAYED,
        PROGRESS_ROUTE_SELECTED
    }

    public enum WakeupReason {
        POWERBUTTON("Power_Button"),
        VOLTAGE_SPIKE("Voltage_Spike"),
        DIAL("Dial"),
        PHONE("Phone");
        
        public final String attr;

        private WakeupReason(String attr) {
            this.attr = attr;
        }
    }

    public static void setGlanceNavigationSource(String source) {
        glanceNavigationSource = source;
    }

    public static void recordNavigation(boolean started) {
        if (started) {
            localyticsSendEvent(ANALYTICS_EVENT_NAVIGATION_DESTINATION_SET, new String[0]);
            destinationSet = true;
        }
    }

    private static void recordOfflineMapsChange(SharedPreferences preferences) {
        String previousVersion = preferences.getString(PREVIOUS_OFFLINE_MAPS_VERSION, null);
        String previousPackages = preferences.getString(PREVIOUS_OFFLINE_MAPS_PACKAGES, null);
        String previousBuildDate = preferences.getString(PREVIOUS_OFFLINE_MAPS_DATE, null);
        HereMapsManager mapsManager = HereMapsManager.getInstance();
        if (mapsManager.isInitialized()) {
            HereOfflineMapsVersion offlineMapsVersion = mapsManager.getOfflineMapsVersion();
            if (offlineMapsVersion != null) {
                List<String> packageList = offlineMapsVersion.getPackages();
                String packages = packageList != null ? TextUtils.join(HereManeuverDisplayBuilder.COMMA, packageList) : "";
                String version = offlineMapsVersion.getVersion();
                String buildDate = offlineMapsVersion.getRawDate();
                if (version == null) {
                    version = "";
                }
                if (buildDate == null) {
                    buildDate = "";
                }
                if (!packages.equals(previousPackages) || !buildDate.equals(previousBuildDate) || !version.equals(previousVersion)) {
                    Event event = new Event(ANALYTICS_EVENT_OFFLINE_MAPS_UPDATED, ATTR_OFFLINE_MAPS_UPDATED_OLD_DATE, previousBuildDate, ATTR_OFFLINE_MAPS_UPDATED_OLD_VERSION, previousVersion, ATTR_OFFLINE_MAPS_UPDATED_OLD_PACKAGES, previousPackages, ATTR_OFFLINE_MAPS_UPDATED_NEW_VERSION, version, ATTR_OFFLINE_MAPS_UPDATED_NEW_PACKAGES, packages, ATTR_OFFLINE_MAPS_UPDATED_NEW_DATE, buildDate, ATTR_OFFLINE_MAPS_UPDATED_UPDATE_COUNT, String.valueOf(preferences.getInt(OFFLINE_MAPS_UPDATE_COUNT, 0) + 1));
                    sLogger.i("Recording offline maps change:" + event);
                    localyticsSendEvent(event, false);
                    preferences.edit().putString(PREVIOUS_OFFLINE_MAPS_DATE, buildDate).putString(PREVIOUS_OFFLINE_MAPS_PACKAGES, packages).putString(PREVIOUS_OFFLINE_MAPS_VERSION, version).putInt(OFFLINE_MAPS_UPDATE_COUNT, count).apply();
                }
            }
        }
    }

    public static void recordCpuOverheat(String state) {
        sLogger.i("Recording CPU overheat event uptime:" + Long.toString(uptime()) + " state:" + state);
        localyticsSendEvent(ANALYTICS_EVENT_CPU_OVERHEAT, "Uptime", uptime, "State", state);
    }

    public static void recordOptionSelection(String option) {
        localyticsSendEvent(ANALYTICS_EVENT_OPTION_SELECTED, "Name", option);
    }

    public static void recordPhoneDisconnect(boolean reconnected, String elapsedTime) {
        boolean moving;
        if (SpeedManager.getInstance().getCurrentSpeed() > 0) {
            moving = true;
        } else {
            moving = false;
        }
        if (remoteDevice != null) {
            String make = null;
            String model = null;
            String year = null;
            if (sLastProfile != null) {
                make = sLastProfile.getCarMake();
                model = sLastProfile.getCarModel();
                year = sLastProfile.getCarYear();
            }
            String str = ANALYTICS_EVENT_PHONE_DISCONNECTED;
            String[] strArr = new String[28];
            strArr[0] = ATTR_DISC_STATUS;
            strArr[1] = reconnected ? "reconnected" : "failed";
            strArr[2] = "Reason";
            strArr[3] = Integer.toString(lastDiscReason);
            strArr[4] = ATTR_DISC_FORCE_RECONNECT;
            strArr[5] = Boolean.toString(disconnectDueToForceReconnect);
            strArr[6] = ATTR_DISC_FORCE_RECONNECT_REASON;
            strArr[7] = lastForceReconnectReason;
            strArr[8] = ATTR_DISC_MOVING;
            strArr[9] = Boolean.toString(moving);
            strArr[10] = ATTR_DISC_PHONE_TYPE;
            strArr[11] = remoteDevice.platform.toString();
            strArr[12] = ATTR_DISC_PHONE_OS;
            strArr[13] = remoteDevice.systemVersion;
            strArr[14] = ATTR_DISC_PHONE_MAKE;
            strArr[15] = remoteDevice.deviceMake;
            strArr[16] = ATTR_DISC_PHONE_MODEL;
            strArr[17] = remoteDevice.model;
            strArr[18] = ATTR_DISC_ELAPSED_TIME;
            strArr[19] = elapsedTime;
            strArr[20] = "Car_Make";
            strArr[21] = make;
            strArr[22] = "Car_Model";
            strArr[23] = model;
            strArr[24] = "Car_Year";
            strArr[25] = year;
            strArr[26] = "VIN";
            strArr[27] = sLastVIN;
            localyticsSendEvent(str, strArr);
            sLastVIN = null;
            sLastProfile = null;
            remoteDevice = null;
            lastDiscReason = 0;
            lastForceReconnectReason = "";
            disconnectDueToForceReconnect = false;
        }
    }

    private static String isEnabled(boolean val) {
        return val ? "Enabled" : BucketLifecycleConfiguration.DISABLED;
    }

    private static Event recordPreference(DriverProfile profile) {
        String glances;
        InputPreferences inputPreferences = profile.getInputPreferences();
        NotificationPreferences notificationPreferences = profile.getNotificationPreferences();
        LocalPreferences localPreferences = profile.getLocalPreferences();
        String units = profile.getUnitSystem() == UnitSystem.UNIT_SYSTEM_IMPERIAL ? "Imperial" : "Metric";
        String displayFormat = profile.getDisplayFormat() == DisplayFormat.DISPLAY_FORMAT_COMPACT ? "Compact" : "Normal";
        String featureMode = "Unknown";
        switch (profile.getFeatureMode()) {
            case FEATURE_MODE_BETA:
                featureMode = "Beta";
                break;
            case FEATURE_MODE_RELEASE:
                featureMode = "Release";
                break;
            case FEATURE_MODE_EXPERIMENTAL:
                featureMode = "Experimental";
                break;
        }
        if (!notificationPreferences.enabled.booleanValue()) {
            glances = BucketLifecycleConfiguration.DISABLED;
        } else if (notificationPreferences.readAloud.booleanValue() && !notificationPreferences.showContent.booleanValue()) {
            glances = "Read_Aloud";
        } else if (notificationPreferences.readAloud.booleanValue() || !notificationPreferences.showContent.booleanValue()) {
            glances = "Read_and_Show";
        } else {
            glances = "Show_Content";
        }
        ObdScanSetting scanSetting = profile.getObdScanSetting();
        String str = ANALYTICS_EVENT_DRIVER_PREFERENCES;
        String[] strArr = new String[20];
        strArr[0] = ATTR_DRIVER_PREFERENCES_LOCALE;
        strArr[1] = profile.getLocale().toString();
        strArr[2] = ATTR_DRIVER_PREFERENCES_UNITS;
        strArr[3] = units;
        strArr[4] = ATTR_DRIVER_PREFERENCES_AUTO_ON;
        strArr[5] = isEnabled(profile.isAutoOnEnabled());
        strArr[6] = ATTR_DRIVER_PREFERENCES_DISPLAY_FORMAT;
        strArr[7] = displayFormat;
        strArr[8] = ATTR_DRIVER_PREFERENCES_FEATURE_MODE;
        strArr[9] = featureMode;
        strArr[10] = ATTR_DRIVER_PREFERENCES_GESTURES;
        strArr[11] = isEnabled(inputPreferences.use_gestures.booleanValue());
        strArr[12] = ATTR_DRIVER_PREFERENCES_GLANCES;
        strArr[13] = glances;
        strArr[14] = ATTR_DRIVER_PREFERENCES_OBD;
        strArr[15] = scanSetting != null ? scanSetting.name() : Constants.NULL_VERSION_ID;
        strArr[16] = ATTR_DRIVER_PREFERENCES_LIMIT_BANDWIDTH;
        strArr[17] = isEnabled(profile.isLimitBandwidthModeOn());
        strArr[18] = ATTR_DRIVER_PREFERENCES_MANUAL_ZOOM;
        strArr[19] = isEnabled(localPreferences.manualZoom.booleanValue());
        return new Event(str, strArr);
    }

    public static void recordPreferenceChange(DriverProfile profile) {
        if (sLastProfilePreferences != null) {
            sLastProfilePreferences.enqueue(profile);
        }
    }

    public static void recordNavigationPreferenceChange(DriverProfile profile) {
        if (sLastNavigationPreferences != null) {
            sLastNavigationPreferences.enqueue(profile);
        }
    }

    private static Event recordNavigationPreference(DriverProfile profile) {
        NavigationPreferences prefs = profile.getNavigationPreferences();
        String routingType = prefs.routingType == RoutingType.ROUTING_FASTEST ? "Fastest" : "Shortest";
        String reroute = "Unknown";
        switch (prefs.rerouteForTraffic) {
            case REROUTE_AUTOMATIC:
                reroute = "Automatic";
                break;
            case REROUTE_CONFIRM:
                reroute = "Confirm";
                break;
            case REROUTE_NEVER:
                reroute = "Never";
                break;
        }
        return new Event(ANALYTICS_EVENT_NAVIGATION_PREFERENCES, ATTR_NAVIGATION_PREFERENCES_REROUTE_FOR_TRAFFIC, reroute, ATTR_NAVIGATION_PREFERENCES_ROUTING_TYPE, routingType, ATTR_NAVIGATION_PREFERENCES_SPOKEN_TBT, isEnabled(prefs.spokenTurnByTurn.booleanValue()), ATTR_NAVIGATION_PREFERENCES_SPOKEN_SPEED_WARNINGS, isEnabled(prefs.spokenSpeedLimitWarnings.booleanValue()), ATTR_NAVIGATION_PREFERENCES_SHOW_TRAFFIC_OPEN_MAP, isEnabled(prefs.showTrafficInOpenMap.booleanValue()), ATTR_NAVIGATION_PREFERENCES_SHOW_TRAFFIC_NAVIGATING, isEnabled(prefs.showTrafficWhileNavigating.booleanValue()), ATTR_NAVIGATION_PREFERENCES_ALLOW_HIGHWAYS, isEnabled(prefs.allowHighways.booleanValue()), ATTR_NAVIGATION_PREFERENCES_ALLOW_TOLL_ROADS, isEnabled(prefs.allowTollRoads.booleanValue()), ATTR_NAVIGATION_PREFERENCES_ALLOW_FERRIES, isEnabled(prefs.allowFerries.booleanValue()), ATTR_NAVIGATION_PREFERENCES_ALLOW_TUNNELS, isEnabled(prefs.allowTunnels.booleanValue()), ATTR_NAVIGATION_PREFERENCES_ALLOW_UNPAVED_ROADS, isEnabled(prefs.allowUnpavedRoads.booleanValue()), ATTR_NAVIGATION_PREFERENCES_ALLOW_AUTO_TRAINS, isEnabled(prefs.allowAutoTrains.booleanValue()), ATTR_NAVIGATION_PREFERENCES_ALLOW_HOV_LANES, isEnabled(prefs.allowHOVLanes.booleanValue()));
    }

    public static void recordIncorrectFuelData() {
        String make = null;
        String model = null;
        String year = null;
        DriverProfile currentProfile = DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        if (currentProfile != null) {
            make = currentProfile.getCarMake();
            model = currentProfile.getCarModel();
            year = currentProfile.getCarYear();
        }
        localyticsSendEvent(ANALYTICS_EVENT_ZERO_FUEL_DATA, "Car_Make", make, "Car_Model", model, "Car_Year", year);
    }

    private static int parseTemp(String tempStr) {
        int result = -1000;
        try {
            return Integer.parseInt(tempStr);
        } catch (NumberFormatException e) {
            sLogger.e("invalid temperature: " + tempStr);
            return result;
        }
    }

    public static int getCurrentTemperature() {
        return currentCpuAvgTemperature;
    }

    private static String getHwVersion() {
        return SerialNumber.instance.configurationCode + SerialNumber.instance.revisionCode;
    }

    private static void setupCustomDimensions() {
        Localytics.setCustomDimension(0, getHwVersion());
    }

    private static boolean isTimeValid() {
        if (Calendar.getInstance().get(1) >= 2016) {
            return true;
        }
        return false;
    }

    public static void analyticsApplicationInit(Application app) {
        app.registerActivityLifecycleCallbacks(new LocalyticsActivityLifecycleCallbacks(app));
        Localytics.setPushDisabled(true);
        setupCustomDimensions();
        sNotificationReceiver = new NotificationReceiver();
        startTime = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime());
        TemperatureReporter tempReporter = new TemperatureReporter();
        tempReporter.setName("TemperatureReporter");
        tempReporter.start();
        Mortar.inject(HudApplication.getAppContext(), tempReporter);
        sObdManager = ObdManager.getInstance();
        sFuelMonitor = new FuelMonitor();
        quietMode = sNotificationReceiver.powerManager.inQuietMode();
        maybeOpenSession();
        IntentFilter intentFilter = new IntentFilter("android.bluetooth.device.action.ACL_DISCONNECTED");
        intentFilter.addAction(HudConnectionService.ACTION_DEVICE_FORCE_RECONNECT);
        HudApplication.getAppContext().registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if ("android.bluetooth.device.action.ACL_DISCONNECTED".equals(action)) {
                    BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                    if (device == null || !(device.getType() == 1 || device.getType() == 3)) {
                        AnalyticsSupport.sLogger.d("ACTION_ACL_DISCONNECTED, Non classic device , Disconnect reason " + intent.getIntExtra("android.bluetooth.device.extra.STATUS", 0));
                        return;
                    }
                    AnalyticsSupport.lastDiscReason = intent.getIntExtra("android.bluetooth.device.extra.STATUS", 0);
                    AnalyticsSupport.sLogger.d("ACTION_ACL_DISCONNECTED, Disconnect reason " + AnalyticsSupport.lastDiscReason + " " + intent);
                } else if (HudConnectionService.ACTION_DEVICE_FORCE_RECONNECT.equals(action)) {
                    AnalyticsSupport.disconnectDueToForceReconnect = true;
                    AnalyticsSupport.lastForceReconnectReason = intent.getStringExtra(HudConnectionService.EXTRA_HUD_FORCE_RECONNECT_REASON);
                    AnalyticsSupport.sLogger.d("ACTION_DEVICE_FORCE_RECONNECT, Reason : " + AnalyticsSupport.lastForceReconnectReason + " " + intent);
                }
            }
        }, intentFilter);
    }

    private static void maybeOpenSession() {
        if (((!quietMode ? 1 : 0) & isTimeValid()) != 0) {
            openSession();
        }
    }

    private static synchronized void openSession() {
        synchronized (AnalyticsSupport.class) {
            if (!localyticsInitialized) {
                sLogger.i("openSession()");
                Localytics.openSession();
                localyticsInitialized = true;
                sendDeferredLocalyticsEvents();
                handler.postDelayed(sFuelMonitor, FUEL_REPORTING_INTERVAL);
                handler.postDelayed(sUploader, UPLOAD_INTERVAL_BOOT);
            }
        }
    }

    private static void setupProfile() {
        String remPlatform;
        String remDeviceName;
        String remClientVersion;
        DriverProfile driverProfile = DriverProfileHelper.getInstance().getCurrentProfile();
        String make = null;
        String model = null;
        String year = null;
        if (!driverProfile.isDefaultProfile()) {
            Localytics.setCustomerFullName(driverProfile.getDriverName());
            Localytics.setCustomerEmail(driverProfile.getDriverEmail());
            make = driverProfile.getCarMake();
            model = driverProfile.getCarModel();
            year = driverProfile.getCarYear();
            Localytics.setProfileAttribute(ATTR_PROFILE_SERIAL_NUMBER, Build.SERIAL);
            Localytics.setProfileAttribute("Car_Make", make);
            Localytics.setProfileAttribute("Car_Model", model);
            Localytics.setProfileAttribute("Car_Year", year);
            Localytics.setProfileAttribute(ATTR_PROFILE_HUD_VERSION, SystemProperties.get("ro.build.description", ""));
            Localytics.setProfileAttribute(ATTR_PROFILE_BUILD_TYPE, Build.TYPE);
        }
        DeviceInfo devInfo = sRemoteDeviceManager.getRemoteDeviceInfo();
        if (devInfo != null) {
            remPlatform = devInfo.platform.toString();
            remDeviceName = devInfo.deviceName;
            remClientVersion = devInfo.clientVersion;
            sRemoteDeviceId = devInfo.deviceId;
        } else {
            remClientVersion = "unknown";
            sRemoteDeviceId = remClientVersion;
            remDeviceName = remClientVersion;
            remPlatform = remClientVersion;
        }
        String vin = sObdManager.getVin();
        localyticsSendEvent(ANALYTICS_EVENT_MOBILE_APP_CONNECT, ATTR_CONN_DEVICE_ID, Build.SERIAL, ATTR_CONN_REMOTE_DEVICE_ID, sRemoteDeviceId, ATTR_CONN_REMOTE_DEVICE_NAME, remDeviceName, ATTR_CONN_REMOTE_PLATFORM, remPlatform, ATTR_CONN_REMOTE_CLIENT_VERSION, remClientVersion, "Car_Make", make, "Car_Model", model, "Car_Year", year, "VIN", vin);
        sLogger.i("setupProfile: DeviceId = " + Build.SERIAL + ", RemoteDeviceId = " + sRemoteDeviceId + ", remotePlatform = " + ", RemoteDeviceName = " + remDeviceName + ", RemoteClientVersion = " + remClientVersion + " vin = " + vin);
    }

    private static synchronized void closeSession() {
        synchronized (AnalyticsSupport.class) {
            if (localyticsInitialized) {
                sendDistance();
                sLogger.v("closeSession()");
                Localytics.closeSession();
                sRemoteDeviceId = "unknown";
            }
        }
    }

    public static void recordDownloadOTAUpdate(SharedPreferences sharedPreferences) {
        String currentVersion = VERSION.INCREMENTAL;
        String updateVersion = OTAUpdateService.getIncrementalUpdateVersion(sharedPreferences);
        localyticsSendEvent(ANALYTICS_EVENT_OTADOWNLOAD, ATTR_OTA_CURRENT_VERSION, currentVersion, ATTR_OTA_UPDATE_VERSION, updateVersion);
    }

    public static void recordInstallOTAUpdate(SharedPreferences sharedPreferences) {
        String currentVersion = VERSION.INCREMENTAL;
        String updateVersion = OTAUpdateService.getIncrementalUpdateVersion(sharedPreferences);
        localyticsSendEvent(ANALYTICS_EVENT_OTAINSTALL, ATTR_OTA_CURRENT_VERSION, currentVersion, ATTR_OTA_UPDATE_VERSION, updateVersion);
    }

    public static void recordOTAInstallResult(boolean incremental, String priorVersion, boolean success) {
        String str = ANALYTICS_EVENT_OTA_INSTALL_RESULT;
        String[] strArr = new String[6];
        strArr[0] = ATTR_OTA_INSTALL_INCREMENTAL;
        strArr[1] = incremental ? TRUE : "false";
        strArr[2] = ATTR_OTA_INSTALL_PRIOR_VERSION;
        strArr[3] = priorVersion;
        strArr[4] = DialConstants.DIAL_PAIRING_SUCCESS;
        strArr[5] = success ? TRUE : "false";
        localyticsSendEvent(str, strArr);
    }

    private static String readLightSensor(String devicePath) {
        String result = "";
        try {
            result = IOUtils.convertFileToString(devicePath);
        } catch (Exception e) {
            sLogger.e("exception reading light sensor device: " + devicePath, e);
        }
        return result.trim();
    }

    public static void recordBrightnessChanged(int prevBrightness, int newBrightness) {
        if (prevBrightness != newBrightness) {
            String lightSensorValue = readLightSensor(LIGHT_SENSOR_DEVICE);
            String lightSensorRawValue = readLightSensor(LIGHT_SENSOR_RAW_DEVICE);
            String lightSensorRaw1Value = readLightSensor(LIGHT_SENSOR_RAW1_DEVICE);
            localyticsSendEvent(ANALYTICS_EVENT_BRIGHTNESS_CHANGE, "Previous", Integer.toString(prevBrightness), "New", Integer.toString(newBrightness), ATTR_BRIGHTNESS_LIGHT_SENSOR, lightSensorValue, ATTR_BRIGHTNESS_RAW_SENSOR, lightSensorRawValue, ATTR_BRIGHTNESS_RAW1_SENSOR, lightSensorRaw1Value);
        }
    }

    public static void recordAutobrightnessAdjustmentChanged(int brightness, int prevAdjustment, int newAdjustment) {
        if (prevAdjustment != newAdjustment) {
            String lightSensorValue = readLightSensor(LIGHT_SENSOR_DEVICE);
            String lightSensorRawValue = readLightSensor(LIGHT_SENSOR_RAW_DEVICE);
            String lightSensorRaw1Value = readLightSensor(LIGHT_SENSOR_RAW1_DEVICE);
            localyticsSendEvent(ANALYTICS_EVENT_BRIGHTNESS_ADJUSTMENT_CHANGE, ATTR_BRIGHTNESS_BRIGHTNESS, Integer.toString(brightness), "Previous", Integer.toString(prevAdjustment), "New", Integer.toString(newAdjustment), ATTR_BRIGHTNESS_LIGHT_SENSOR, lightSensorValue, ATTR_BRIGHTNESS_RAW_SENSOR, lightSensorRawValue, ATTR_BRIGHTNESS_RAW1_SENSOR, lightSensorRaw1Value);
        }
    }

    public static void recordAdaptiveAutobrightnessAdjustmentChanged(int brightness) {
        String lightSensorValue = readLightSensor(LIGHT_SENSOR_DEVICE);
        String lightSensorRawValue = readLightSensor(LIGHT_SENSOR_RAW_DEVICE);
        String lightSensorRaw1Value = readLightSensor(LIGHT_SENSOR_RAW1_DEVICE);
        localyticsSendEvent(ANALYTICS_EVENT_ADAPTIVE_BRIGHTNESS_ADJUSTMENT_CHANGE, ATTR_BRIGHTNESS_BRIGHTNESS, Integer.toString(brightness), ATTR_BRIGHTNESS_LIGHT_SENSOR, lightSensorValue, ATTR_BRIGHTNESS_RAW_SENSOR, lightSensorRawValue, ATTR_BRIGHTNESS_RAW1_SENSOR, lightSensorRaw1Value);
    }

    public static void recordKeyFailure(String keyType, String error) {
        localyticsSendEvent(ANALYTICS_EVENT_KEY_FAILED, ATTR_KEY_FAIL_KEY_TYPE, keyType, ATTR_KEY_FAIL_ERROR, error);
    }

    private static String pidListToHexString(List<Pid> pList) {
        long[] bits = new long[5];
        for (Pid p : pList) {
            int v = p.getId();
            int i = v / 64;
            bits[i] = bits[i] | (1 << (v % 64));
        }
        String sep = "";
        String res = "";
        for (int i2 = bits.length - 1; i2 >= 0; i2--) {
            res = res + sep + String.format(Locale.US, "%08x", new Object[]{Long.valueOf(bits[i2])});
            sep = " ";
        }
        return res;
    }

    private static void reportObdState() {
        List<ECU> ecus = sObdManager.getEcus();
        int ecuCount = ecus == null ? 0 : ecus.size();
        if (ecuCount == 0) {
            sLogger.e("Obd connection with no ECUs");
        }
        sLastVIN = sObdManager.getVin();
        String obdChipFirmwareVersion = sObdManager.getObdChipFirmwareVersion();
        String[] attrs = new String[((ecuCount * 2) + 6)];
        int i = 0 + 1;
        attrs[0] = ATTR_OBD_CONN_FIRMWARE_VERSION;
        int i2 = i + 1;
        if (obdChipFirmwareVersion == null) {
            obdChipFirmwareVersion = "";
        }
        attrs[i] = obdChipFirmwareVersion;
        i = i2 + 1;
        attrs[i2] = "VIN";
        i2 = i + 1;
        attrs[i] = sLastVIN;
        i = i2 + 1;
        attrs[i2] = ATTR_OBD_CONN_PROTOCOL;
        i2 = i + 1;
        attrs[i] = sObdManager.getProtocol();
        i = i2;
        for (int i3 = 0; i3 < ecuCount; i3++) {
            List<Pid> pList = ((ECU) ecus.get(i3)).supportedPids.asList();
            i2 = i + 1;
            attrs[i] = String.format(Locale.US, "%s%d", new Object[]{ATTR_OBD_CONN_ECU, Integer.valueOf(i3 + 1)});
            i = i2 + 1;
            attrs[i2] = String.format(Locale.US, "Address: 0x%x, Pids: %s", new Object[]{Integer.valueOf(e.address), pidListToHexString(pList)});
        }
        localyticsSendEvent(ANALYTICS_EVENT_OBD_CONNECTED, attrs);
        if (deferredWakeup != null) {
            localyticsSendEvent(ANALYTICS_EVENT_WAKEUP, false, true, "Reason", deferredWakeup.reason.attr, DialConstants.DIAL_BATTERY_LEVEL, deferredWakeup.batteryLevel, ATTR_WAKEUP_BATTERY_DRAIN, deferredWakeup.batteryDrain, "Sleep_Time", deferredWakeup.sleepTime, ATTR_WAKEUP_MAX_BATTERY, deferredWakeup.maxBattery, "VIN", sLastVIN);
            deferredWakeup = null;
        }
    }

    public static void recordMenuSelection(String name) {
        localyticsSendEvent(ANALYTICS_EVENT_MENU_SELECTION, "Name", name);
    }

    public static void recordBadRoutePosition(String displayPos, String navPos, String streetAddress) {
        localyticsSendEvent(ANALYTICS_EVENT_BAD_ROUTE_POSITION, ATTR_ROUTE_DISPLAY_POS, displayPos, ATTR_ROUTE_NAV_POS, navPos, ATTR_ROUTE_STREET_ADDRESS, streetAddress);
    }

    public static void recordNearbySearchReturnMainMenu() {
        recordMenuSelection("back");
        localyticsSendEvent(ANALYTICS_EVENT_NEARBY_SEARCH_RETURN_MM, new String[0]);
    }

    public static void recordNearbySearchClosed() {
        localyticsSendEvent(ANALYTICS_EVENT_NEARBY_SEARCH_CLOSED, new String[0]);
    }

    public static void recordNearbySearchDismiss() {
        localyticsSendEvent(ANALYTICS_EVENT_NEARBY_SEARCH_DISMISS, new String[0]);
    }

    public static void recordNearbySearchNoResult(boolean retry) {
        String str = ANALYTICS_EVENT_NEARBY_SEARCH_NO_RESULTS;
        String[] strArr = new String[2];
        strArr[0] = "Action";
        strArr[1] = retry ? "Retry" : "Dismiss";
        localyticsSendEvent(str, strArr);
    }

    public static void recordNearbySearchResultsDismiss() {
        localyticsSendEvent(ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_DISMISS, new String[0]);
    }

    public static void recordNearbySearchResultsView() {
        localyticsSendEvent(ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_VIEW, new String[0]);
        nearbySearchDestScrollPosition = -1;
        nearbySearchPlaceType = null;
    }

    public static void recordNearbySearchSelection(PlaceType placeType, int pos) {
        localyticsSendEvent(ANALYTICS_EVENT_MENU_SELECTION, "Name", "nearby_places_results");
        String str = ANALYTICS_EVENT_NEARBY_SEARCH_SELECTION;
        String[] strArr = new String[6];
        strArr[0] = ATTR_NEARBY_SEARCH_PLACE_TYPE;
        strArr[1] = placeType.toString();
        strArr[2] = ATTR_NEARBY_SEARCH_DEST_SELECTED_POSITION;
        strArr[3] = Integer.toString(pos);
        strArr[4] = ATTR_NEARBY_SEARCH_DEST_SCROLLED;
        strArr[5] = nearbySearchDestScrollPosition > 0 ? TRUE : "false";
        localyticsSendEvent(str, strArr);
        nearbySearchPlaceType = placeType;
    }

    public static void recordNearbySearchResultsClose() {
        localyticsSendEvent(ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_CLOSE, new String[0]);
    }

    public static void recordNearbySearchDestinationScroll(int pos) {
        if (pos > nearbySearchDestScrollPosition) {
            nearbySearchDestScrollPosition = pos;
        }
    }

    private static String screenName(BaseScreen screen) {
        Screen scr = screen.getScreen();
        String name = scr.toString();
        if (scr != Screen.SCREEN_HOME) {
            return name;
        }
        DisplayMode dm = ((HomeScreen) screen).getDisplayMode();
        if (dm == null || !HereMapsManager.getInstance().isInitialized()) {
            return name;
        }
        return dm.toString() + (HereNavigationManager.getInstance().isNavigationModeOn() ? "_ROUTE" : "");
    }

    private static void recordNewDevicePaired() {
        newDevicePaired = true;
    }

    public static void recordScreen(BaseScreen screen) {
        if (screen.getScreen() == Screen.SCREEN_WELCOME) {
            welcomeScreenTime = SystemClock.elapsedRealtime();
        } else if (screen.getScreen() == Screen.SCREEN_HOME && !phonePaired && welcomeScreenTime != -1 && sRemoteDeviceManager.isRemoteDeviceConnected()) {
            long curTime = SystemClock.elapsedRealtime();
            String str = ANALYTICS_EVENT_ATTEMPT_TO_CONNECT;
            String[] strArr = new String[4];
            strArr[0] = ATTR_CONN_ATTEMPT_NEW_DEVICE;
            strArr[1] = newDevicePaired ? TRUE : "false";
            strArr[2] = ATTR_CONN_ATTEMPT_TIME;
            strArr[3] = Long.toString(curTime - welcomeScreenTime);
            localyticsSendEvent(str, strArr);
            welcomeScreenTime = -1;
            newDevicePaired = false;
            phonePaired = true;
        }
        if (localyticsInitialized) {
            Localytics.tagScreen(screenName(screen));
        }
    }

    public static void recordGlanceAction(String action, INotification notification, String navMethod) {
        if (navMethod == null) {
            navMethod = glanceNavigationSource != null ? glanceNavigationSource : "unknown";
        }
        glanceNavigationSource = null;
        if (notification != null) {
            String glanceName;
            NotificationType nt = notification.getType();
            if (notification instanceof GlanceNotification) {
                glanceName = ((GlanceNotification) notification).getGlanceApp().toString();
            } else {
                glanceName = nt.toString();
            }
            localyticsSendEvent(action, ATTR_GLANCE_NAME, glanceName, ATTR_GLANCE_NAV_METHOD, navMethod);
            return;
        }
        localyticsSendEvent(action, ATTR_GLANCE_NAV_METHOD, navMethod);
    }

    private static void clearVoiceSearch() {
        voiceSearchProgress = null;
        voiceSearchWords = null;
        voiceSearchConfidence = null;
        voiceSearchListeningOverBluetooth = false;
        voiceSearchResults = null;
        voiceSearchAdditionalResultsAction = null;
        voiceSearchExplicitRetry = false;
        voiceSearchListSelection = null;
        voiceSearchPrefix = null;
    }

    public static void recordVoiceSearchEntered() {
        if (voiceSearchProgress == VoiceSearchProgress.PROGRESS_LIST_DISPLAYED) {
            clearVoiceSearch();
            voiceSearchExplicitRetry = true;
        }
    }

    public static void recordVoiceSearchRetry() {
        voiceSearchExplicitRetry = true;
    }

    public static void recordVoiceSearchAdditionalResultsAction(VoiceSearchAdditionalResultsAction action) {
        voiceSearchAdditionalResultsAction = action;
        if (action == VoiceSearchAdditionalResultsAction.ADDITIONAL_RESULTS_LIST) {
            voiceSearchProgress = VoiceSearchProgress.PROGRESS_LIST_DISPLAYED;
        } else {
            voiceSearchProgress = VoiceSearchProgress.PROGRESS_ROUTE_SELECTED;
        }
    }

    public static void recordVoiceSearchListItemSelection(int selection) {
        if (selection == -1 || selection == -2) {
            voiceSearchExplicitRetry = true;
            return;
        }
        voiceSearchListSelection = Integer.valueOf(selection);
        voiceSearchProgress = VoiceSearchProgress.PROGRESS_ROUTE_SELECTED;
    }

    public static void recordVoiceSearchResult(VoiceSearchResponse resp) {
        switch (resp.state) {
            case VOICE_SEARCH_STARTING:
                voiceSearchCount++;
                if (voiceSearchProgress != null) {
                    sLogger.w("starting voice search with unexpected progress state " + voiceSearchProgress);
                    clearVoiceSearch();
                }
                sLogger.v("starting voice search");
                voiceSearchProgress = VoiceSearchProgress.PROGRESS_SEARCHING;
                return;
            case VOICE_SEARCH_LISTENING:
                if (resp.listeningOverBluetooth != null) {
                    voiceSearchListeningOverBluetooth = resp.listeningOverBluetooth.booleanValue();
                    return;
                }
                return;
            case VOICE_SEARCH_SUCCESS:
                sLogger.v("voice search successful");
                voiceSearchProgress = VoiceSearchProgress.PROGRESS_DESTINATION_FOUND;
                if (resp.listeningOverBluetooth != null) {
                    voiceSearchListeningOverBluetooth = resp.listeningOverBluetooth.booleanValue();
                }
                voiceSearchResults = resp.results;
                break;
            case VOICE_SEARCH_SEARCHING:
                break;
            case VOICE_SEARCH_ERROR:
                sLogger.v("voice search error: " + resp.error);
                clearVoiceSearch();
                sendVoiceSearchErrorEvent(resp.error, voiceSearchCount - 1);
                return;
            default:
                return;
        }
        if (resp.recognizedWords != null) {
            voiceSearchWords = resp.recognizedWords;
        }
        if (resp.confidenceLevel != null) {
            voiceSearchConfidence = resp.confidenceLevel;
        }
        if (resp.prefix != null) {
            voiceSearchPrefix = resp.prefix;
        }
    }

    private static int countWords(String s) {
        int count = 0;
        if (s != null) {
            s = s.trim();
            int i = 0;
            int slen = s.length();
            while (i < slen) {
                count++;
                while (i < slen && s.charAt(i) != HereManeuverDisplayBuilder.SPACE_CHAR) {
                    i++;
                }
                while (i < slen && s.charAt(i) == HereManeuverDisplayBuilder.SPACE_CHAR) {
                    i++;
                }
            }
        }
        return count;
    }

    private static String getVoiceSearchEvent() {
        String phone_type = remoteDevice == null ? null : remoteDevice.platform.toString();
        if (phone_type != null) {
            return phone_type.equals("PLATFORM_Android") ? ANALYTICS_EVENT_VOICE_SEARCH_ANDROID : ANALYTICS_EVENT_VOICE_SEARCH_IOS;
        } else {
            return null;
        }
    }

    private static String getClientVersion() {
        DeviceInfo devInfo = sRemoteDeviceManager.getRemoteDeviceInfo();
        String clientVersion = devInfo == null ? null : devInfo.clientVersion;
        if (clientVersion != null) {
            return clientVersion;
        }
        sLogger.e("failed to get client version");
        return "";
    }

    private static void sendVoiceSearchErrorEvent(VoiceSearchError error, int retryCount) {
        String event = getVoiceSearchEvent();
        if (event == null) {
            sLogger.e("cannot determine phone_type for voice search");
            return;
        }
        String str;
        String[] strArr = new String[6];
        strArr[0] = ATTR_VOICE_SEARCH_RESPONSE;
        if (error == null) {
            str = "";
        } else {
            str = error.toString();
        }
        strArr[1] = str;
        strArr[2] = ATTR_VOICE_SEARCH_RETRY_COUNT;
        strArr[3] = Integer.toString(retryCount);
        strArr[4] = ATTR_VOICE_SEARCH_CLIENT_VERSION;
        strArr[5] = getClientVersion();
        localyticsSendEvent(event, strArr);
    }

    private static void sendVoiceSearchEvent(VoiceSearchCancelReason reason, NavigationRouteRequest request, String words, Integer confidence, int retryCount, String microphoneUsed, List<Destination> searchResults, VoiceSearchAdditionalResultsAction additionalResultsAction, boolean explicitRetry, Integer listItemSelection, String prefix) {
        String event = getVoiceSearchEvent();
        if (event == null) {
            sLogger.e("cannot determine phone_type for voice search");
            return;
        }
        Map args = new HashMap();
        args.put(ATTR_VOICE_SEARCH_RESPONSE, DialConstants.DIAL_PAIRING_SUCCESS);
        args.put(ATTR_VOICE_SEARCH_CLIENT_VERSION, getClientVersion());
        if (confidence != null) {
            args.put(ATTR_VOICE_SEARCH_CONFIDENCE_LEVEL, confidence.toString());
        }
        if (words != null) {
            args.put(ATTR_VOICE_SEARCH_NUMBER_OF_WORDS, Integer.toString(countWords(words)));
        }
        boolean cancelledQuickly = reason == VoiceSearchCancelReason.end_trip || reason == VoiceSearchCancelReason.new_voice_search || reason == VoiceSearchCancelReason.new_non_voice_search;
        String str = ATTR_VOICE_SEARCH_NAVIGATION_INITIATED;
        Object obj = (reason == null || cancelledQuickly) ? TRUE : "false";
        args.put(str, obj);
        args.put(ATTR_VOICE_SEARCH_NAV_CANCELLED_QUICKLY, cancelledQuickly ? TRUE : "false");
        args.put(ATTR_VOICE_SEARCH_RETRY_COUNT, Integer.toString(retryCount));
        args.put(ATTR_VOICE_SEARCH_MICROPHONE_USED, microphoneUsed);
        args.put(ATTR_VOICE_SEARCH_NUM_RESULTS, Integer.toString(searchResults == null ? 0 : searchResults.size()));
        if (additionalResultsAction != null) {
            args.put(ATTR_VOICE_SEARCH_ADDITIONAL_RESULTS_GLANCE, additionalResultsAction.tag);
        }
        args.put(ATTR_VOICE_SEARCH_EXPLICIT_RETRY, explicitRetry ? TRUE : "false");
        if (reason != null) {
            args.put(ATTR_VOICE_SEARCH_CANCEL_TYPE, reason.toString());
        }
        if (listItemSelection != null) {
            args.put(ATTR_VOICE_SEARCH_LIST_SELECTION, listItemSelection.toString());
        }
        boolean isSuggestion = false;
        String destType = "ADDRESS";
        if (request != null) {
            Destination dest = request.requestDestination;
            isSuggestion = (dest == null || dest.suggestion_type == null || dest.suggestion_type == SuggestionType.SUGGESTION_NONE) ? false : true;
            if (request.destinationType != null && request.destinationType != FavoriteType.FAVORITE_NONE) {
                destType = request.destinationType.toString();
            } else if (request.label != null) {
                destType = "PLACE";
            }
            args.put(ATTR_VOICE_SEARCH_DESTINATION_TYPE, destType);
        }
        args.put(ATTR_VOICE_SEARCH_SUGGESTION, isSuggestion ? TRUE : "false");
        if (prefix != null) {
            args.put(ATTR_VOICE_SEARCH_PREFIX, prefix);
        }
        localyticsSendEvent(new Event(event, args), false);
    }

    private static void processNavigationRouteRequest(NavigationRouteRequest request) {
        if (nearbySearchPlaceType != null) {
            localyticsSendEvent(ANALYTICS_EVENT_NEARBY_SEARCH_TRIP_INITIATED, new String[0]);
            return;
        }
        boolean isVoiceSearchRoute;
        if (voiceSearchProgress == VoiceSearchProgress.PROGRESS_ROUTE_SELECTED || voiceSearchProgress == VoiceSearchProgress.PROGRESS_DESTINATION_FOUND) {
            isVoiceSearchRoute = true;
        } else {
            isVoiceSearchRoute = false;
        }
        if (voiceResultsRunnable != null) {
            voiceResultsRunnable.cancel(isVoiceSearchRoute ? VoiceSearchCancelReason.new_voice_search : VoiceSearchCancelReason.new_non_voice_search);
        }
        if (isVoiceSearchRoute) {
            voiceResultsRunnable = new VoiceResultsRunnable(request);
            handler.postDelayed(voiceResultsRunnable, NAVIGATION_CANCEL_TIMEOUT);
            clearVoiceSearch();
        } else if (voiceSearchProgress != null) {
            sLogger.w("Received NavigationRouteRequest with unexpected voice progress state: " + voiceSearchProgress);
        }
        voiceSearchCount = 0;
    }

    private static void processNavigationArrivalEvent(ArrivalEvent unused) {
        if (nearbySearchPlaceType != null) {
            localyticsSendEvent(ANALYTICS_EVENT_NEARBY_SEARCH_ARRIVED, ATTR_NEARBY_SEARCH_PLACE_TYPE, nearbySearchPlaceType.toString());
            nearbySearchPlaceType = null;
        }
    }

    public static void recordRouteSelectionCancelled() {
        if (nearbySearchPlaceType != null) {
            sLogger.v("nearby search route calculation cancelled");
            localyticsSendEvent(ANALYTICS_EVENT_NEARBY_SEARCH_TRIP_CANCELLED, new String[0]);
            nearbySearchPlaceType = null;
        }
        if (voiceSearchProgress != null) {
            VoiceSearchCancelReason reason = null;
            if (voiceSearchProgress == VoiceSearchProgress.PROGRESS_SEARCHING) {
                reason = VoiceSearchCancelReason.cancel_search;
            } else if (voiceSearchProgress == VoiceSearchProgress.PROGRESS_LIST_DISPLAYED) {
                reason = VoiceSearchCancelReason.cancel_list;
            }
            if (reason == null) {
                sLogger.e("unexpected progress " + voiceSearchProgress + " in recordRouteSelectionCancelled()");
                clearVoiceSearch();
                return;
            }
            sLogger.v("route selection cancelled, reason = " + reason);
            sendVoiceSearchEvent(reason, null, null, voiceSearchConfidence, voiceSearchCount - 1, voiceSearchListeningOverBluetooth ? "bluetooth" : GpsConstants.USING_PHONE_LOCATION, voiceSearchResults, voiceSearchAdditionalResultsAction, voiceSearchExplicitRetry, null, voiceSearchPrefix);
        } else if (voiceResultsRunnable != null) {
            sLogger.v("route calculation cancelled");
            voiceResultsRunnable.cancel(VoiceSearchCancelReason.cancel_route_calculation);
        }
    }

    public static void recordNavigationCancelled() {
        if (nearbySearchPlaceType != null) {
            localyticsSendEvent(ANALYTICS_EVENT_NEARBY_SEARCH_ENDED, new String[0]);
            sLogger.v("navigation ended for nearby search trip");
            nearbySearchPlaceType = null;
        } else if (voiceResultsRunnable != null) {
            sLogger.v("navigation ended, cancelling voice search callback");
            voiceResultsRunnable.cancel(VoiceSearchCancelReason.end_trip);
        }
    }

    private static String normalizeName(String name) {
        char[] ca = name.toCharArray();
        boolean wordStart = true;
        for (int i = 0; i < ca.length; i++) {
            char c = ca[i];
            if (wordStart) {
                c = Character.toUpperCase(c);
            }
            wordStart = c == '_';
            ca[i] = c;
        }
        return new String(ca);
    }

    public static void recordSwipedCalibration(String eventName, String[] args) {
        eventName = ANALYTICS_EVENT_SWIPED_PREFIX + normalizeName(eventName);
        for (int i = 0; i < args.length - 1; i += 2) {
            args[i] = normalizeName(args[i]);
        }
        localyticsSendEvent(eventName, args);
    }

    public static void recordDestinationSuggestion(boolean accepted, boolean calendar) {
        String str = ANALYTICS_EVENT_DESTINATION_SUGGESTION;
        String[] strArr = new String[4];
        strArr[0] = "Accepted";
        strArr[1] = accepted ? TRUE : "false";
        strArr[2] = DialConstants.DIAL_EVENT_TYPE;
        strArr[3] = calendar ? DESTINATION_SUGGESTION_TYPE_CALENDAR : DESTINATION_SUGGESTION_TYPE_SMART_SUGGESTION;
        localyticsSendEvent(str, strArr);
    }

    public static void recordUpdatePrompt(boolean accepted, boolean isDial, String toVersion) {
        String event = isDial ? ANALYTICS_EVENT_DIAL_UPDATE_PROMPT : ANALYTICS_EVENT_DISPLAY_UPDATE_PROMPT;
        String[] strArr = new String[4];
        strArr[0] = "Accepted";
        strArr[1] = accepted ? TRUE : "false";
        strArr[2] = ATTR_UPDATE_TO_VERSION;
        strArr[3] = toVersion;
        localyticsSendEvent(event, strArr);
    }

    public static void recordMusicAction(String action) {
        localyticsSendEvent(ANALYTICS_EVENT_MUSIC_ACTION, DialConstants.DIAL_EVENT_TYPE, action);
    }

    public static void recordMusicBrowsePlayAction(String collectionType) {
        localyticsSendEvent(collectionType + "_played", true, new String[0]);
    }

    private static long uptime() {
        return TimeUnit.NANOSECONDS.toSeconds(System.nanoTime()) - startTime;
    }

    private static void sendDistance() {
        int totalDistance = RemoteDeviceManager.getInstance().getTripManager().getTotalDistanceTravelled() - startingDistance;
        if (totalDistance > 10 && !quietMode) {
            long duration = uptime();
            String str = ANALYTICS_EVENT_DISTANCE_TRAVELLED;
            String[] strArr = new String[10];
            strArr[0] = "Distance";
            strArr[1] = Long.toString((long) totalDistance);
            strArr[2] = ATTR_DURATION;
            strArr[3] = Long.toString(duration);
            strArr[4] = ATTR_NAVIGATING;
            strArr[5] = destinationSet ? TRUE : "false";
            strArr[6] = "Connection";
            strArr[7] = phonePaired ? TRUE : "false";
            strArr[8] = "Vin";
            strArr[9] = sLastVIN;
            localyticsSendEvent(str, false, true, strArr);
        }
    }

    public static void recordStartingBatteryVoltage(double voltage) {
        if (startingBatteryVoltage == -1.0d) {
            startingBatteryVoltage = voltage;
            sLogger.i(String.format(Locale.US, "starting battery voltage = %6.1f", new Object[]{Double.valueOf(voltage)}));
        }
    }

    public static void recordShutdown(Reason reason, boolean isQuietMode) {
        String type = isQuietMode ? PowerManager.QUIET_MODE : "full";
        double batteryLevel = sObdManager.getBatteryVoltage();
        String timeAttr = quietMode ? "Sleep_Time" : "Uptime";
        String str = ANALYTICS_EVENT_SHUTDOWN;
        r5 = new String[8];
        r5[5] = String.format(Locale.US, "%6.1f", new Object[]{Double.valueOf(batteryLevel)});
        r5[6] = timeAttr;
        r5[7] = Long.toString(uptime());
        localyticsSendEvent(str, false, true, r5);
    }

    private static void recordWakeup(WakeupReason reason) {
        String sleepTimeStr = Long.toString(uptime());
        String batteryLevelStr = String.format(Locale.US, "%6.1f", new Object[]{Double.valueOf(sObdManager.getBatteryVoltage())});
        String maxBatteryStr = String.format(Locale.US, "%6.1f", new Object[]{Double.valueOf(sObdManager.getMaxBatteryVoltage())});
        String vin = sObdManager.getVin();
        double batteryDrain = 0.0d;
        if (startingBatteryVoltage != -1.0d) {
            batteryDrain = startingBatteryVoltage - sObdManager.getMinBatteryVoltage();
        }
        String batteryDrainStr = String.format(Locale.US, "%6.1f", new Object[]{Double.valueOf(batteryDrain)});
        if (vin != null) {
            localyticsSendEvent(ANALYTICS_EVENT_WAKEUP, false, true, "Reason", reason.attr, DialConstants.DIAL_BATTERY_LEVEL, batteryLevelStr, ATTR_WAKEUP_BATTERY_DRAIN, batteryDrainStr, "Sleep_Time", sleepTimeStr, ATTR_WAKEUP_MAX_BATTERY, maxBatteryStr, "VIN", vin);
        } else {
            deferredWakeup = new DeferredWakeup(reason, batteryLevelStr, batteryDrainStr, sleepTimeStr, maxBatteryStr);
        }
        startTime = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime());
        startingDistance = sRemoteDeviceManager.getTripManager().getTotalDistanceTravelled();
    }

    public static void recordDashPreference(String gauge, boolean leftSide) {
        String str = ANALYTICS_EVENT_DASH_GAUGE_SETTING;
        String[] strArr = new String[4];
        strArr[0] = ATTR_GAUGE_SIDE;
        strArr[1] = leftSide ? ATTR_GAUGE_SIDE_LEFT : ATTR_GAUGE_SIDE_RIGHT;
        strArr[2] = ATTR_GAUGE_ID;
        strArr[3] = gauge;
        localyticsSendEvent(str, strArr);
    }

    public static void recordNavdyMileageEvent(double vehicleKiloMeters, double navdyKiloMeters, double navdyUsageRate) {
        localyticsSendEvent(ANALYTICS_EVENT_MILEAGE, ATTR_VEHICLE_KILO_METERS, Double.toString(vehicleKiloMeters), ATTR_NAVDY_KILOMETERS, Double.toString(navdyKiloMeters), ATTR_NAVDY_USAGE_RATE, Double.toString(navdyUsageRate));
    }

    private static synchronized boolean deferLocalyticsEvent(String tag, String[] args) {
        boolean z;
        synchronized (AnalyticsSupport.class) {
            if (localyticsInitialized) {
                z = false;
            } else {
                if (deferredEvents == null) {
                    deferredEvents = new ArrayList(5);
                }
                deferredEvents.add(new DeferredLocalyticsEvent(tag, args));
                z = true;
            }
        }
        return z;
    }

    private static synchronized void sendDeferredLocalyticsEvents() {
        synchronized (AnalyticsSupport.class) {
            if (!localyticsInitialized) {
                sLogger.e("sendDeferredLocalyticsEvents() called before Localytics was initialized");
            } else if (deferredEvents != null) {
                sLogger.v("sending " + deferredEvents.size() + " deferred localytics events");
                Iterator it = deferredEvents.iterator();
                while (it.hasNext()) {
                    DeferredLocalyticsEvent de = (DeferredLocalyticsEvent) it.next();
                    localyticsSendEvent(de.tag, de.args);
                }
                deferredEvents = null;
            }
        }
    }

    public static void localyticsSendEvent(String tag, String... args) {
        localyticsSendEvent(tag, false, false, args);
    }

    private static void localyticsSendEvent(String tag, boolean includeDeviceInfo, String... args) {
        localyticsSendEvent(tag, includeDeviceInfo, false, args);
    }

    private static void localyticsSendEvent(String tag, boolean includeDeviceInfo, boolean immediate, String... args) {
        if (immediate || !deferLocalyticsEvent(tag, args)) {
            Event event = new Event(tag, args);
            Map<String, String> argMap = event.argMap;
            if (includeDeviceInfo) {
                DeviceInfo deviceInfo = RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
                if (deviceInfo != null) {
                    if (deviceInfo.platform != null) {
                        argMap.put(ATTR_REMOTE_DEVICE_PLATFORM, deviceInfo.platform.name());
                    }
                    argMap.put(ATTR_REMOTE_DEVICE_HARDWARE, deviceInfo.deviceMake + " " + deviceInfo.model);
                    argMap.put(ATTR_REMOTE_DEVICE_SW_VERSION, deviceInfo.deviceMake + " " + deviceInfo.systemVersion);
                }
            }
            localyticsSendEvent(event, immediate);
        }
    }

    private static void localyticsSendEvent(Event event, boolean immediate) {
        if (immediate) {
            Localytics.tagEvent(event.tag, event.argMap);
        } else {
            TaskManager.getInstance().execute(new EventSender(event), 1);
        }
    }

    private static long getUpdateInterval() {
        if (NetworkBandwidthController.getInstance().isLimitBandwidthModeOn()) {
            return UPLOAD_INTERVAL_LIMITED_BANDWIDTH;
        }
        return UPLOAD_INTERVAL;
    }

    static void submitNavigationQualityReport(Report report) {
        localyticsSendEvent(ANALYTICS_EVENT_ROUTE_COMPLETED, ATTR_ROUTE_COMPLETED_EXPECTED_DURATION, String.valueOf(report.expectedDuration), ATTR_ROUTE_COMPLETED_ACTUAL_DURATION, String.valueOf(report.actualDuration), ATTR_ROUTE_COMPLETED_EXPECTED_DISTANCE, String.valueOf(report.expectedDistance), ATTR_ROUTE_COMPLETED_ACTUAL_DISTANCE, String.valueOf(report.actualDistance), ATTR_ROUTE_COMPLETED_RECALCULATIONS, String.valueOf(report.nRecalculations), ATTR_ROUTE_COMPLETED_DURATION_VARIANCE, String.valueOf(report.getDurationVariance()), ATTR_ROUTE_COMPLETED_DISTANCE_VARIANCE, String.valueOf(report.getDistanceVariance()), ATTR_ROUTE_COMPLETED_TRAFFIC_LEVEL, report.trafficLevel.name());
    }

    public static void recordGlympseSent(boolean success, String share, String message, String id) {
        sLogger.v("glympse sent id=" + id + " sucess=" + success + " share=" + share + " msg=" + message);
        localyticsSendEvent(ANALYTICS_EVENT_GLYMPSE_SENT, DialConstants.DIAL_PAIRING_SUCCESS, String.valueOf(success), ATTR_GLYMPSE_SHARE, share, ATTR_GLYMPSE_MESSAGE, message, "Id", id);
    }

    public static void recordGlympseViewed(String id) {
        sLogger.v("glympse viewed id=" + id);
        localyticsSendEvent(ANALYTICS_EVENT_GLYMPSE_VIEWED, "Id", id);
    }

    public static void recordObdCanBusDataSent(String version) {
        if (sLastProfile != null) {
            String make = sLastProfile.getCarMake();
            String model = sLastProfile.getCarModel();
            String year = sLastProfile.getCarYear();
            localyticsSendEvent(ANALYTICS_EVENT_OBD_LISTENING_POSTED, "Car_Make", make, "Car_Model", model, "Car_Year", year, "Version", version);
            return;
        }
        localyticsSendEvent(ANALYTICS_EVENT_OBD_LISTENING_POSTED, "Version", version);
    }

    public static void recordObdCanBusMonitoringState(String vin, boolean success, int approxDataSize, int version) {
        String str;
        String[] strArr;
        if (sLastProfile != null) {
            String make = sLastProfile.getCarMake();
            String model = sLastProfile.getCarModel();
            String year = sLastProfile.getCarYear();
            str = ANALYTICS_EVENT_OBD_LISTENING_STATUS;
            strArr = new String[14];
            strArr[0] = "Vin";
            strArr[1] = vin;
            strArr[2] = "Car_Make";
            strArr[3] = make;
            strArr[4] = "Car_Model";
            strArr[5] = model;
            strArr[6] = "Car_Year";
            strArr[7] = year;
            strArr[8] = DialConstants.DIAL_PAIRING_SUCCESS;
            strArr[9] = success ? TRUE : "false";
            strArr[10] = "Version";
            strArr[11] = Integer.toString(version);
            strArr[12] = ATTR_OBD_DATA_MONITORING_DATA_SIZE;
            strArr[13] = Integer.toString(approxDataSize);
            localyticsSendEvent(str, strArr);
            return;
        }
        str = ANALYTICS_EVENT_OBD_LISTENING_STATUS;
        strArr = new String[8];
        strArr[0] = "Vin";
        strArr[1] = vin;
        strArr[2] = DialConstants.DIAL_PAIRING_SUCCESS;
        strArr[3] = success ? TRUE : "false";
        strArr[4] = "Version";
        strArr[5] = Integer.toString(version);
        strArr[6] = ATTR_OBD_DATA_MONITORING_DATA_SIZE;
        strArr[7] = Integer.toString(approxDataSize);
        localyticsSendEvent(str, strArr);
    }

    public static void recordSmsSent(boolean success, String platform, boolean cannedMessage) {
        sLogger.v("sms-sent success=" + success + " plaform=" + platform + " canned=" + cannedMessage);
        String str = ANALYTICS_EVENT_SMS_SENT;
        String[] strArr = new String[6];
        strArr[0] = DialConstants.DIAL_PAIRING_SUCCESS;
        strArr[1] = String.valueOf(success);
        strArr[2] = ATTR_SMS_PLATFORM;
        strArr[3] = platform;
        strArr[4] = ATTR_SMS_MESSAGE_TYPE;
        strArr[5] = cannedMessage ? ATTR_SMS_MESSAGE_TYPE_CANNED : ATTR_SMS_MESSAGE_TYPE_CUSTOM;
        localyticsSendEvent(str, strArr);
    }

    public static void recordVehicleTelemetryData(long sessionDistance, long sessionDuration, long sessionRollingDuration, float sessionAverageSpeed, float sessionAverageRollingSpeed, float sessionMaxSpeed, int sessionHardBrakingCount, int sessionHardAccelerationCount, float sessionSpeedingFrequency, float sessionExcessiveSpeedingFrequency, int sessionTroubleCodeCount, String sessionTroubleCodes, int averageMpg, int maxRpm, int eventAverageMpg, int eventAverageRpm, int eventFuelLevel, float eventEngineTemperature, float maxG, float maxGAngle, int sessionHighGCount) {
        DriverProfile currentProfile = DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        String make = "";
        String model = "";
        String year = "";
        if (currentProfile != null) {
            make = currentProfile.getCarMake();
            model = currentProfile.getCarModel();
            year = currentProfile.getCarYear();
        }
        localyticsSendEvent(ANALYTICS_EVENT_VEHICLE_TELEMETRY_DATA, ATTR_SESSION_DISTANCE, Long.toString(sessionDistance), ATTR_SESSION_DURATION, Long.toString(sessionDuration), ATTR_SESSION_ROLLING_DURATION, Long.toString(sessionRollingDuration), ATTR_SESSION_AVERAGE_SPEED, Float.toString(sessionAverageSpeed), ATTR_SESSION_AVERAGE_ROLLING_SPEED, Float.toString(sessionAverageRollingSpeed), ATTR_SESSION_MAX_SPEED, Float.toString(sessionMaxSpeed), ATTR_SESSION_HARD_BRAKING_COUNT, Integer.toString(sessionHardBrakingCount), ATTR_SESSION_ACCELERATION_COUNT, Integer.toString(sessionHardAccelerationCount), ATTR_SESSION_SPEEDING_FREQUENCY, Float.toString(sessionSpeedingFrequency), ATTR_SESSION_EXCESSIVE_SPEEDING_FREQUENCY, Float.toString(sessionExcessiveSpeedingFrequency), ATTR_SESSION_TROUBLE_CODE_COUNT, Integer.toString(sessionTroubleCodeCount), ATTR_SESSION_TROUBLE_CODE_LAST, sessionTroubleCodes, ATTR_SESSION_AVERAGE_MPG, Integer.toString(averageMpg), ATTR_SESSION_MAX_RPM, Integer.toString(maxRpm), ATTR_EVENT_AVERAGE_MPG, Integer.toString(eventAverageMpg), ATTR_EVENT_AVERAGE_RPM, Integer.toString(eventAverageRpm), ATTR_EVENT_FUEL_LEVEL, Integer.toString(eventFuelLevel), ATTR_EVENT_ENGINE_TEMPERATURE, Float.toString(eventEngineTemperature), ATTR_EVENT_MAX_G, Float.toString(maxG), ATTR_EVENT_MAX_G_ANGLE, Float.toString(maxGAngle), ATTR_SESSION_HIGH_G_COUNT, Integer.toString(sessionHighGCount), "Car_Make", make, "Car_Model", model, "Car_Year", year);
    }
}
