package com.navdy.hud.app.analytics;

import android.os.SystemClock;
import com.navdy.service.library.log.Logger;

public class NavigationQualityTracker {
    private static final int INVALID = -1;
    private static final NavigationQualityTracker instance = new NavigationQualityTracker();
    private static final Logger logger = new Logger(NavigationQualityTracker.class);
    private boolean isRoutingWithTraffic;
    private Report report;
    private long tripDurationIntervalInit = -1;
    private long tripStartUtc = -1;

    static class Report {
        private static final double HEAVY_TRAFFIC_RATIO = 1.25d;
        private static final double MODERATE_TRAFFIC_RATIO = 1.1d;
        long actualDistance;
        long actualDuration;
        final long expectedDistance;
        final long expectedDuration;
        int nRecalculations;
        final TrafficLevel trafficLevel;

        private Report(long durationWithTraffic, long duration, long expectedDistance, boolean isTrafficEnabled) {
            this.expectedDuration = durationWithTraffic;
            this.expectedDistance = expectedDistance;
            this.nRecalculations = 0;
            if (isTrafficEnabled) {
                double trafficRatio = ((double) durationWithTraffic) / ((double) duration);
                if (trafficRatio < MODERATE_TRAFFIC_RATIO) {
                    this.trafficLevel = TrafficLevel.LIGHT;
                    return;
                } else if (trafficRatio < MODERATE_TRAFFIC_RATIO || trafficRatio >= HEAVY_TRAFFIC_RATIO) {
                    this.trafficLevel = TrafficLevel.HEAVY;
                    return;
                } else {
                    this.trafficLevel = TrafficLevel.MODERATE;
                    return;
                }
            }
            this.trafficLevel = TrafficLevel.NO_DATA;
        }

        double getDurationVariance() {
            return ((double) (100 * (this.actualDuration - this.expectedDuration))) / ((double) this.expectedDuration);
        }

        double getDistanceVariance() {
            return ((double) (100 * (this.actualDistance - this.expectedDistance))) / ((double) this.expectedDuration);
        }

        public String toString() {
            return "Report{trafficLevel=" + this.trafficLevel + ", expectedDuration=" + this.expectedDuration + ", expectedDistance=" + this.expectedDistance + ", actualDuration=" + this.actualDuration + ", actualDistance=" + this.actualDistance + ", nRecalculations=" + this.nRecalculations + '}';
        }

        public boolean isValid() {
            return this.expectedDistance > 0 && this.expectedDuration > 0 && this.actualDistance > 0 && this.actualDuration > 0;
        }
    }

    enum TrafficLevel {
        NO_DATA,
        LIGHT,
        MODERATE,
        HEAVY
    }

    public static NavigationQualityTracker getInstance() {
        return instance;
    }

    private NavigationQualityTracker() {
    }

    public synchronized void trackTripStarted(long durationWithTraffic, long duration, long distance) {
        logger.i("trip started, init new navigation quality report");
        this.tripStartUtc = System.currentTimeMillis();
        this.tripDurationIntervalInit = SystemClock.elapsedRealtime();
        logger.v("tripStartUtc: " + this.tripStartUtc);
        this.report = new Report(durationWithTraffic, duration, distance, this.isRoutingWithTraffic);
    }

    public synchronized void trackTripRecalculated() {
        if (this.report == null) {
            logger.w("report is null, no-op");
        } else {
            Report report = this.report;
            report.nRecalculations++;
        }
    }

    public synchronized void cancelTrip() {
        this.tripStartUtc = -1;
        this.tripDurationIntervalInit = -1;
        this.isRoutingWithTraffic = false;
        this.report = null;
    }

    public synchronized void trackTripEnded(long distanceCovered) {
        if (this.report == null) {
            logger.w("report is null, no-op");
        } else {
            long tripEndUtc = System.currentTimeMillis();
            logger.v("tripEndUtc: " + tripEndUtc);
            logger.v("tripEndUtc - tripStartUtc, should be same as actualDuration: " + (tripEndUtc - this.tripStartUtc));
            this.report.actualDuration = (SystemClock.elapsedRealtime() - this.tripDurationIntervalInit) / 1000;
            this.report.actualDistance = distanceCovered;
            logger.i("route has ended, submitting navigation quality report: " + this.report);
            if (this.report.isValid()) {
                AnalyticsSupport.submitNavigationQualityReport(this.report);
            }
            this.report = null;
        }
    }

    public synchronized void trackCalculationWithTraffic(boolean factoringInTraffic) {
        this.isRoutingWithTraffic = factoringInTraffic;
    }

    public long getTripStartUtc() {
        return this.tripStartUtc;
    }

    public int getActualDurationSoFar() {
        return this.tripDurationIntervalInit != -1 ? (int) ((SystemClock.elapsedRealtime() - this.tripDurationIntervalInit) / 1000) : -1;
    }

    public int getExpectedDuration() {
        if (this.report == null) {
            return -1;
        }
        return (int) this.report.expectedDuration;
    }

    public int getExpectedDistance() {
        if (this.report == null) {
            return -1;
        }
        return (int) this.report.expectedDistance;
    }
}
