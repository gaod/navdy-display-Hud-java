package com.navdy.hud.app.analytics;

import com.glympse.android.hal.NotificationListener;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.service.library.log.Logger;
import java.util.LinkedList;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0019\n\u0002\u0018\u0002\n\u0002\b1\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b1\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0010\u0002\n\u0002\b\u000b\b\u00c6\u0002\u0018\u00002\u00020\u0001:\u0004\u00c6\u0001\u00c7\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u00bc\u0001\u001a\u00030\u00bd\u0001J#\u0010\u00be\u0001\u001a\u00030\u00bd\u00012\u0007\u0010\u00bf\u0001\u001a\u00020\u00042\u0007\u0010\u00c0\u0001\u001a\u00020\u00042\u0007\u0010\u00c1\u0001\u001a\u00020\u0004J\b\u0010\u00c2\u0001\u001a\u00030\u00bd\u0001J\b\u0010\u00c3\u0001\u001a\u00030\u00bd\u0001J\b\u0010\u00c4\u0001\u001a\u00030\u00bd\u0001J\n\u0010\u00c5\u0001\u001a\u00030\u00bd\u0001H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\n\u001a\u00020\u0007X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\tR\u000e\u0010\f\u001a\u00020\rX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\rX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u00020\u0011X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u000e\u0010\u0014\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u00020\u0011X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0013R \u0010\u0018\u001a\u00020\r2\u0006\u0010\u0017\u001a\u00020\r8B@BX\u0082\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR \u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u0017\u001a\u00020\u001b8B@BX\u0082\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u000e\u0010\u001f\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\"\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\u001a\"\u0004\b$\u0010%R\u001a\u0010&\u001a\u00020'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R$\u0010-\u001a\u00020\u00072\u0006\u0010,\u001a\u00020\u0007@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010\t\"\u0004\b/\u00100R$\u00101\u001a\u00020\u00112\u0006\u0010,\u001a\u00020\u0011@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b2\u0010\u0013\"\u0004\b3\u00104R$\u00105\u001a\u00020\u001b2\u0006\u0010,\u001a\u00020\u001b@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b6\u0010\u001e\"\u0004\b7\u00108R$\u00109\u001a\u00020\u00042\u0006\u0010,\u001a\u00020\u0004@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=R\u0011\u0010>\u001a\u00020\r8F\u00a2\u0006\u0006\u001a\u0004\b?\u0010\u001aR\u001c\u0010@\u001a\u0004\u0018\u00010AX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bB\u0010C\"\u0004\bD\u0010ER\u001a\u0010F\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bG\u0010\t\"\u0004\bH\u00100R\u001a\u0010I\u001a\u00020\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bJ\u0010\u0013\"\u0004\bK\u00104R\u001a\u0010L\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bM\u0010\u001a\"\u0004\bN\u0010%R\u001a\u0010O\u001a\u00020\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bP\u0010\u0013\"\u0004\bQ\u00104R\u001c\u0010R\u001a\u00020\r8FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bS\u0010\u001a\"\u0004\bT\u0010%R\u001a\u0010U\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bV\u0010\u001a\"\u0004\bW\u0010%R\u001a\u0010X\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bY\u0010\u001a\"\u0004\bZ\u0010%R\u001a\u0010[\u001a\u00020'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b[\u0010)\"\u0004\b\\\u0010+R$\u0010]\u001a\u00020'2\u0006\u0010,\u001a\u00020'8F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b]\u0010)\"\u0004\b^\u0010+R$\u0010_\u001a\u00020'2\u0006\u0010,\u001a\u00020'8F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b_\u0010)\"\u0004\b`\u0010+R$\u0010a\u001a\u00020'2\u0006\u0010,\u001a\u00020'8F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\ba\u0010)\"\u0004\bb\u0010+R\u001a\u0010c\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bd\u0010;\"\u0004\be\u0010=R\u001a\u0010f\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bg\u0010;\"\u0004\bh\u0010=R\u001a\u0010i\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bj\u0010\u001a\"\u0004\bk\u0010%R\u001a\u0010l\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bm\u0010\u001a\"\u0004\bn\u0010%R\u001a\u0010o\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bp\u0010\u001a\"\u0004\bq\u0010%R\u000e\u0010r\u001a\u00020sX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010t\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bu\u0010;\"\u0004\bv\u0010=R\u001a\u0010w\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bx\u0010;\"\u0004\by\u0010=R\u001a\u0010z\u001a\u00020\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b{\u0010\u0013\"\u0004\b|\u00104R\u000e\u0010}\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R&\u0010~\u001a\u0015\u0012\u0011\u0012\u000f\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00040\u0080\u00010\u007f\u00a2\u0006\n\n\u0000\u001a\u0006\b\u0081\u0001\u0010\u0082\u0001R\u001d\u0010\u0083\u0001\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0084\u0001\u0010\t\"\u0005\b\u0085\u0001\u00100R)\u0010\u0086\u0001\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u00048F@BX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0087\u0001\u0010;\"\u0005\b\u0088\u0001\u0010=R)\u0010\u0089\u0001\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u00048F@BX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u008a\u0001\u0010;\"\u0005\b\u008b\u0001\u0010=R\u001f\u0010\u008c\u0001\u001a\u00020\r8FX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u008d\u0001\u0010\u001a\"\u0005\b\u008e\u0001\u0010%R)\u0010\u008f\u0001\u001a\u00020\r2\u0006\u0010\u0017\u001a\u00020\r8F@BX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0090\u0001\u0010\u001a\"\u0005\b\u0091\u0001\u0010%R'\u0010\u0092\u0001\u001a\u00020\u00042\u0006\u0010,\u001a\u00020\u00048F@BX\u0086\u000e\u00a2\u0006\u000e\u001a\u0005\b\u0093\u0001\u0010;\"\u0005\b\u0094\u0001\u0010=R\u001d\u0010\u0095\u0001\u001a\u00020\u0011X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0096\u0001\u0010\u0013\"\u0005\b\u0097\u0001\u00104R\u001d\u0010\u0098\u0001\u001a\u00020\u0011X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0099\u0001\u0010\u0013\"\u0005\b\u009a\u0001\u00104R\u001d\u0010\u009b\u0001\u001a\u00020\u0011X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u009c\u0001\u0010\u0013\"\u0005\b\u009d\u0001\u00104R\u001d\u0010\u009e\u0001\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u009f\u0001\u0010;\"\u0005\b\u00a0\u0001\u0010=R\u001d\u0010\u00a1\u0001\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00a2\u0001\u0010\u001a\"\u0005\b\u00a3\u0001\u0010%R\u001f\u0010\u00a4\u0001\u001a\u00020\r8FX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00a5\u0001\u0010\u001a\"\u0005\b\u00a6\u0001\u0010%R'\u0010\u00a7\u0001\u001a\u00020\u00042\u0006\u0010,\u001a\u00020\u00048F@BX\u0086\u000e\u00a2\u0006\u000e\u001a\u0005\b\u00a8\u0001\u0010;\"\u0005\b\u00a9\u0001\u0010=R\u000f\u0010\u00aa\u0001\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u00ab\u0001\u001a\u00020'X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00ac\u0001\u0010)\"\u0005\b\u00ad\u0001\u0010+R\u001d\u0010\u00ae\u0001\u001a\u00020\u0011X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00af\u0001\u0010\u0013\"\u0005\b\u00b0\u0001\u00104R \u0010\u00b1\u0001\u001a\u00030\u00b2\u0001X\u0086\u000e\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00b3\u0001\u0010\u00b4\u0001\"\u0006\b\u00b5\u0001\u0010\u00b6\u0001R\u001f\u0010\u00b7\u0001\u001a\u00020\r8FX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00b8\u0001\u0010\u001a\"\u0005\b\u00b9\u0001\u0010%R\u000f\u0010\u00ba\u0001\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000f\u0010\u00bb\u0001\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u00c8\u0001"}, d2 = {"Lcom/navdy/hud/app/analytics/TelemetrySession;", "", "()V", "EXCESSIVE_SPEEDING_SPEED_OVER_SPEED_LIMIT_MS", "", "G_LOWER_THRESHOLD_THRESHOLD", "HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND", "", "getHARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND", "()D", "HARD_BRAKING_THRESHOLD_METERS_PER_SECOND", "getHARD_BRAKING_THRESHOLD_METERS_PER_SECOND", "HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS", "", "HIGH_G_THRESHOLD_TIME_MILLIS", "MAX_G_THRESHOLD", "MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION", "", "getMINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION", "()I", "SPEEDING_SPEED_OVER_SPEED_LIMIT_MS", "SPEED_DATA_ROLLING_WINDOW_SIZE", "getSPEED_DATA_ROLLING_WINDOW_SIZE", "<set-?>", "_currentDistance", "get_currentDistance", "()J", "Lcom/navdy/hud/app/analytics/RawSpeed;", "_currentSpeed", "get_currentSpeed", "()Lcom/navdy/hud/app/analytics/RawSpeed;", "_excessiveSpeedingDuration", "_rollingDuration", "_speedingDuration", "confirmedHighGManeuverEndTime", "getConfirmedHighGManeuverEndTime", "setConfirmedHighGManeuverEndTime", "(J)V", "confirmedHighGManueverNotified", "", "getConfirmedHighGManueverNotified", "()Z", "setConfirmedHighGManueverNotified", "(Z)V", "value", "currentMpg", "getCurrentMpg", "setCurrentMpg", "(D)V", "currentRpm", "getCurrentRpm", "setCurrentRpm", "(I)V", "currentSpeed", "getCurrentSpeed", "setCurrentSpeed", "(Lcom/navdy/hud/app/analytics/RawSpeed;)V", "currentSpeedLimit", "getCurrentSpeedLimit", "()F", "setCurrentSpeedLimit", "(F)V", "currentTime", "getCurrentTime", "dataSource", "Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;", "getDataSource", "()Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;", "setDataSource", "(Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;)V", "eventAverageMpg", "getEventAverageMpg", "setEventAverageMpg", "eventAverageRpm", "getEventAverageRpm", "setEventAverageRpm", "eventMpgSamples", "getEventMpgSamples", "setEventMpgSamples", "eventRpmSamples", "getEventRpmSamples", "setEventRpmSamples", "excessiveSpeedingDuration", "getExcessiveSpeedingDuration", "setExcessiveSpeedingDuration", "excessiveSpeedingStartTime", "getExcessiveSpeedingStartTime", "setExcessiveSpeedingStartTime", "highGManeuverStartTime", "getHighGManeuverStartTime", "setHighGManeuverStartTime", "isAboveWaterMark", "setAboveWaterMark", "isDoingHighGManeuver", "setDoingHighGManeuver", "isExcessiveSpeeding", "setExcessiveSpeeding", "isSpeeding", "setSpeeding", "lastG", "getLastG", "setLastG", "lastGAngle", "getLastGAngle", "setLastGAngle", "lastGTime", "getLastGTime", "setLastGTime", "lastHardAccelerationTime", "getLastHardAccelerationTime", "setLastHardAccelerationTime", "lastHardBrakingTime", "getLastHardBrakingTime", "setLastHardBrakingTime", "logger", "Lcom/navdy/service/library/log/Logger;", "maxG", "getMaxG", "setMaxG", "maxGAngle", "getMaxGAngle", "setMaxGAngle", "maxRpm", "getMaxRpm", "setMaxRpm", "movingStartTime", "rollingSpeedData", "Ljava/util/LinkedList;", "Lkotlin/Pair;", "getRollingSpeedData", "()Ljava/util/LinkedList;", "sessionAverageMpg", "getSessionAverageMpg", "setSessionAverageMpg", "sessionAverageRollingSpeed", "getSessionAverageRollingSpeed", "setSessionAverageRollingSpeed", "sessionAverageSpeed", "getSessionAverageSpeed", "setSessionAverageSpeed", "sessionDistance", "getSessionDistance", "setSessionDistance", "sessionDuration", "getSessionDuration", "setSessionDuration", "sessionExcessiveSpeedingPercentage", "getSessionExcessiveSpeedingPercentage", "setSessionExcessiveSpeedingPercentage", "sessionHardAccelerationCount", "getSessionHardAccelerationCount", "setSessionHardAccelerationCount", "sessionHardBrakingCount", "getSessionHardBrakingCount", "setSessionHardBrakingCount", "sessionHighGCount", "getSessionHighGCount", "setSessionHighGCount", "sessionMaxSpeed", "getSessionMaxSpeed", "setSessionMaxSpeed", "sessionMpgSamples", "getSessionMpgSamples", "setSessionMpgSamples", "sessionRollingDuration", "getSessionRollingDuration", "setSessionRollingDuration", "sessionSpeedingPercentage", "getSessionSpeedingPercentage", "setSessionSpeedingPercentage", "sessionStartTime", "sessionStarted", "getSessionStarted", "setSessionStarted", "sessionTroubleCodeCount", "getSessionTroubleCodeCount", "setSessionTroubleCodeCount", "sessionTroubleCodesReport", "", "getSessionTroubleCodesReport", "()Ljava/lang/String;", "setSessionTroubleCodesReport", "(Ljava/lang/String;)V", "speedingDuration", "getSpeedingDuration", "setSpeedingDuration", "speedingStartTime", "startDistance", "eventReported", "", "setGForce", "x", "y", "z", "speedSourceChanged", "startSession", "stopSession", "updateValuesBasedOnSpeed", "DataSource", "InterestingEvent", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: TelemetrySession.kt */
public final class TelemetrySession {
    private static final float EXCESSIVE_SPEEDING_SPEED_OVER_SPEED_LIMIT_MS = 0.0f;
    private static final float G_LOWER_THRESHOLD_THRESHOLD = 0.3f;
    private static final double HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND = 3.53d;
    private static final double HARD_BRAKING_THRESHOLD_METERS_PER_SECOND = 3.919d;
    private static final long HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS = 3000;
    private static final long HIGH_G_THRESHOLD_TIME_MILLIS = 500;
    public static final TelemetrySession INSTANCE = null;
    private static final float MAX_G_THRESHOLD = 0.45f;
    private static final int MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION = 2000;
    private static final float SPEEDING_SPEED_OVER_SPEED_LIMIT_MS = 0.0f;
    private static final int SPEED_DATA_ROLLING_WINDOW_SIZE = 500;
    private static long _currentDistance;
    private static RawSpeed _currentSpeed;
    private static long _excessiveSpeedingDuration;
    private static long _rollingDuration;
    private static long _speedingDuration;
    private static long confirmedHighGManeuverEndTime;
    private static boolean confirmedHighGManueverNotified;
    private static double currentMpg;
    private static int currentRpm;
    @NotNull
    private static RawSpeed currentSpeed;
    private static float currentSpeedLimit;
    @Nullable
    private static DataSource dataSource;
    private static double eventAverageMpg;
    private static int eventAverageRpm;
    private static long eventMpgSamples;
    private static int eventRpmSamples;
    private static long excessiveSpeedingDuration;
    private static long excessiveSpeedingStartTime;
    private static long highGManeuverStartTime;
    private static boolean isAboveWaterMark;
    private static float lastG;
    private static float lastGAngle;
    private static long lastGTime;
    private static long lastHardAccelerationTime;
    private static long lastHardBrakingTime;
    private static final Logger logger = null;
    private static float maxG;
    private static float maxGAngle;
    private static int maxRpm;
    private static long movingStartTime;
    @NotNull
    private static final LinkedList<Pair<Long, Float>> rollingSpeedData = null;
    private static double sessionAverageMpg;
    private static float sessionAverageRollingSpeed;
    private static float sessionAverageSpeed;
    private static long sessionDistance;
    private static long sessionDuration;
    private static int sessionHardAccelerationCount;
    private static int sessionHardBrakingCount;
    private static int sessionHighGCount;
    private static float sessionMaxSpeed;
    private static long sessionMpgSamples;
    private static long sessionRollingDuration;
    private static long sessionStartTime;
    private static boolean sessionStarted;
    private static int sessionTroubleCodeCount;
    @NotNull
    private static String sessionTroubleCodesReport;
    private static long speedingDuration;
    private static long speedingStartTime;
    private static long startDistance;

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH&J\b\u0010\n\u001a\u00020\u000bH&J\b\u0010\f\u001a\u00020\u000bH&J\b\u0010\r\u001a\u00020\u0003H&\u00a8\u0006\u000e"}, d2 = {"Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;", "", "absoluteCurrentTime", "", "currentSpeed", "Lcom/navdy/hud/app/analytics/RawSpeed;", "interestingEventDetected", "", "event", "Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;", "isHighAccuracySpeedAvailable", "", "isVerboseLoggingNeeded", "totalDistanceTravelledWithMeters", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TelemetrySession.kt */
    public interface DataSource {
        long absoluteCurrentTime();

        @NotNull
        RawSpeed currentSpeed();

        void interestingEventDetected(@NotNull InterestingEvent interestingEvent);

        boolean isHighAccuracySpeedAvailable();

        boolean isVerboseLoggingNeeded();

        long totalDistanceTravelledWithMeters();
    }

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u000b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000b\u00a8\u0006\f"}, d2 = {"Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;", "", "(Ljava/lang/String;I)V", "NONE", "HARD_BRAKING", "HARD_ACCELERATION", "HIGH_G_STARTED", "HIGH_G_ENDED", "SPEEDING_STARTED", "SPEEDING_STOPPED", "EXCESSIVE_SPEEDING_STARTED", "EXCESSIVE_SPEEDING_STOPPED", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TelemetrySession.kt */
    public enum InterestingEvent {
    }

    static {
        TelemetrySession telemetrySession = new TelemetrySession();
    }

    private TelemetrySession() {
        INSTANCE = this;
        logger = new Logger(INSTANCE.getClass().getSimpleName());
        SPEEDING_SPEED_OVER_SPEED_LIMIT_MS = TelemetrySessionKt.MPHToMetersPerSecond(8.0f);
        EXCESSIVE_SPEEDING_SPEED_OVER_SPEED_LIMIT_MS = TelemetrySessionKt.MPHToMetersPerSecond(16.0f);
        MAX_G_THRESHOLD = 0.45f;
        G_LOWER_THRESHOLD_THRESHOLD = G_LOWER_THRESHOLD_THRESHOLD;
        HIGH_G_THRESHOLD_TIME_MILLIS = 500;
        HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS = 3000;
        sessionMpgSamples = 1;
        sessionTroubleCodesReport = "";
        _currentSpeed = new RawSpeed((float) -1, 0);
        SPEED_DATA_ROLLING_WINDOW_SIZE = 500;
        HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND = HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND;
        HARD_BRAKING_THRESHOLD_METERS_PER_SECOND = HARD_BRAKING_THRESHOLD_METERS_PER_SECOND;
        MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION = 2000;
        currentSpeed = new RawSpeed((float) -1, 0);
        rollingSpeedData = new LinkedList();
        eventMpgSamples = 1;
        eventRpmSamples = 1;
    }

    @Nullable
    public final DataSource getDataSource() {
        return dataSource;
    }

    public final void setDataSource(@Nullable DataSource <set-?>) {
        dataSource = <set-?>;
    }

    public final double getSessionAverageMpg() {
        return sessionAverageMpg;
    }

    public final void setSessionAverageMpg(double <set-?>) {
        sessionAverageMpg = <set-?>;
    }

    public final long getSessionMpgSamples() {
        return sessionMpgSamples;
    }

    public final void setSessionMpgSamples(long <set-?>) {
        sessionMpgSamples = <set-?>;
    }

    public final double getCurrentMpg() {
        return currentMpg;
    }

    public final void setCurrentMpg(double value) {
        currentMpg = value;
        if (value > ((double) 0)) {
            sessionAverageMpg += (value - sessionAverageMpg) / ((double) sessionMpgSamples);
            sessionMpgSamples += (long) 1;
            eventAverageMpg += (value - eventAverageMpg) / ((double) eventMpgSamples);
            eventMpgSamples += (long) 1;
        }
    }

    public final long getCurrentTime() {
        DataSource dataSource = dataSource;
        return dataSource != null ? dataSource.absoluteCurrentTime() : System.currentTimeMillis();
    }

    public final int getMaxRpm() {
        return maxRpm;
    }

    public final void setMaxRpm(int <set-?>) {
        maxRpm = <set-?>;
    }

    public final int getCurrentRpm() {
        return currentRpm;
    }

    public final void setCurrentRpm(int value) {
        currentRpm = value;
        if (value > 0) {
            maxRpm = Math.max(value, maxRpm);
            eventAverageRpm += (value - eventAverageRpm) / eventRpmSamples;
            eventRpmSamples++;
        }
    }

    private final void setSessionAverageSpeed(float <set-?>) {
        sessionAverageSpeed = <set-?>;
    }

    public final float getSessionAverageSpeed() {
        return getSessionDuration() > ((long) 0) ? ((float) getSessionDistance()) / (((float) getSessionDuration()) / ((float) 1000)) : 0.0f;
    }

    private final void setSessionAverageRollingSpeed(float <set-?>) {
        sessionAverageRollingSpeed = <set-?>;
    }

    public final float getSessionAverageRollingSpeed() {
        return getSessionRollingDuration() > ((long) 0) ? ((float) getSessionDistance()) / (((float) getSessionRollingDuration()) / ((float) 1000)) : 0.0f;
    }

    public final float getSessionSpeedingPercentage() {
        return getSessionRollingDuration() == 0 ? 0.0f : ((float) getSpeedingDuration()) / ((float) getSessionRollingDuration());
    }

    private final void setSessionSpeedingPercentage(float value) {
    }

    public final float getSessionExcessiveSpeedingPercentage() {
        return getSessionRollingDuration() == 0 ? 0.0f : ((float) getExcessiveSpeedingDuration()) / ((float) getSessionRollingDuration());
    }

    private final void setSessionExcessiveSpeedingPercentage(float value) {
    }

    public final int getSessionTroubleCodeCount() {
        return sessionTroubleCodeCount;
    }

    public final void setSessionTroubleCodeCount(int <set-?>) {
        sessionTroubleCodeCount = <set-?>;
    }

    @NotNull
    public final String getSessionTroubleCodesReport() {
        return sessionTroubleCodesReport;
    }

    public final void setSessionTroubleCodesReport(@NotNull String <set-?>) {
        Intrinsics.checkParameterIsNotNull(<set-?>, "<set-?>");
        sessionTroubleCodesReport = <set-?>;
    }

    private final long get_currentDistance() {
        DataSource dataSource = dataSource;
        return dataSource != null ? dataSource.totalDistanceTravelledWithMeters() : 0;
    }

    private final RawSpeed get_currentSpeed() {
        DataSource dataSource = dataSource;
        if (dataSource != null) {
            RawSpeed currentSpeed = dataSource.currentSpeed();
            if (currentSpeed != null) {
                return currentSpeed;
            }
        }
        return new RawSpeed((float) -1, 0);
    }

    public final int getSPEED_DATA_ROLLING_WINDOW_SIZE() {
        return SPEED_DATA_ROLLING_WINDOW_SIZE;
    }

    public final double getHARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND() {
        return HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND;
    }

    public final double getHARD_BRAKING_THRESHOLD_METERS_PER_SECOND() {
        return HARD_BRAKING_THRESHOLD_METERS_PER_SECOND;
    }

    public final int getMINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION() {
        return MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION;
    }

    public final long getLastHardAccelerationTime() {
        return lastHardAccelerationTime;
    }

    public final void setLastHardAccelerationTime(long <set-?>) {
        lastHardAccelerationTime = <set-?>;
    }

    public final long getLastHardBrakingTime() {
        return lastHardBrakingTime;
    }

    public final void setLastHardBrakingTime(long <set-?>) {
        lastHardBrakingTime = <set-?>;
    }

    public final int getSessionHardAccelerationCount() {
        return sessionHardAccelerationCount;
    }

    public final void setSessionHardAccelerationCount(int <set-?>) {
        sessionHardAccelerationCount = <set-?>;
    }

    public final int getSessionHardBrakingCount() {
        return sessionHardBrakingCount;
    }

    public final void setSessionHardBrakingCount(int <set-?>) {
        sessionHardBrakingCount = <set-?>;
    }

    @NotNull
    public final RawSpeed getCurrentSpeed() {
        return currentSpeed;
    }

    public final void setCurrentSpeed(@NotNull RawSpeed value) {
        Intrinsics.checkParameterIsNotNull(value, NotificationListener.INTENT_EXTRA_VALUE);
        currentSpeed = value;
        updateValuesBasedOnSpeed();
        if (((int) value.getSpeed()) == -1) {
            rollingSpeedData.clear();
            return;
        }
        DataSource dataSource = dataSource;
        if (dataSource != null ? dataSource.isHighAccuracySpeedAvailable() : false) {
            long timeStamp = value.getTimeStamp();
            rollingSpeedData.add(new Pair(Long.valueOf(timeStamp), Float.valueOf(value.getSpeed())));
            if (rollingSpeedData.size() != 1) {
                boolean outDatedDataExists = true;
                Pair lastSamplePastWindow = null;
                do {
                    Pair oldestSample = (Pair) rollingSpeedData.peek();
                    if (oldestSample == null || timeStamp - ((Number) oldestSample.getFirst()).longValue() <= ((long) SPEED_DATA_ROLLING_WINDOW_SIZE)) {
                        outDatedDataExists = false;
                        if (lastSamplePastWindow != null) {
                            rollingSpeedData.addFirst(lastSamplePastWindow);
                            continue;
                        } else {
                            continue;
                        }
                    } else {
                        lastSamplePastWindow = (Pair) rollingSpeedData.remove();
                        continue;
                    }
                } while (outDatedDataExists);
                Pair firstSampleInWindow = (Pair) rollingSpeedData.peek();
                double timeDifference = ((double) (timeStamp - ((Number) firstSampleInWindow.getFirst()).longValue())) / ((double) 1000);
                if (timeDifference < ((double) 0.5f)) {
                    return;
                }
                if (value.getSpeed() > ((Number) firstSampleInWindow.getSecond()).floatValue()) {
                    double acceleration = ((double) (value.getSpeed() - ((Number) firstSampleInWindow.getSecond()).floatValue())) / timeDifference;
                    dataSource = dataSource;
                    if (dataSource != null ? dataSource.isVerboseLoggingNeeded() : false) {
                        logger.d("RawSpeed " + value + ", Change is speed : " + (value.getSpeed() - ((Number) firstSampleInWindow.getSecond()).floatValue()) + " Acceleration : " + acceleration + " " + GlanceConstants.NEWLINE + " Rolling data : " + rollingSpeedData);
                    }
                    if (acceleration <= HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND) {
                        return;
                    }
                    if (lastHardAccelerationTime == 0 || timeStamp - lastHardAccelerationTime > ((long) MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION)) {
                        logger.d("Hard Acceleration detected, Acceleration " + acceleration + ", In time :" + timeDifference);
                        logger.d("Current RawSpeed : " + value + ", Old RawSpeed : " + ((Number) firstSampleInWindow.getSecond()).floatValue());
                        logger.d("Rolling Window : " + rollingSpeedData);
                        logger.d("Last G , g : " + lastG + " , gAngle : " + lastGAngle + " , time : " + lastGTime);
                        lastHardAccelerationTime = timeStamp;
                        sessionHardAccelerationCount++;
                        dataSource = dataSource;
                        if (dataSource != null) {
                            dataSource.interestingEventDetected(InterestingEvent.HARD_ACCELERATION);
                            return;
                        }
                        return;
                    }
                    return;
                }
                double deceleration = ((double) (((Number) firstSampleInWindow.getSecond()).floatValue() - value.getSpeed())) / timeDifference;
                dataSource = dataSource;
                if (dataSource != null ? dataSource.isVerboseLoggingNeeded() : false) {
                    logger.d("RawSpeed " + value + ", Change is speed : " + (value.getSpeed() - ((Number) firstSampleInWindow.getSecond()).floatValue()) + " Braking : " + deceleration + " " + GlanceConstants.NEWLINE + " Rolling data : " + rollingSpeedData);
                }
                if (deceleration <= HARD_BRAKING_THRESHOLD_METERS_PER_SECOND) {
                    return;
                }
                if (lastHardBrakingTime == 0 || timeStamp - lastHardBrakingTime > ((long) MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION)) {
                    logger.d("Hard Deceleration detected, Acceleration " + deceleration + ", In time :" + timeDifference);
                    logger.d("Current RawSpeed : " + value + ", Old RawSpeed : " + ((Number) firstSampleInWindow.getSecond()).floatValue());
                    logger.d("Rolling Window : " + rollingSpeedData);
                    logger.d("Last G , g : " + lastG + " , gAngle : " + lastGAngle + " , time : " + lastGTime);
                    lastHardBrakingTime = timeStamp;
                    sessionHardBrakingCount++;
                    dataSource = dataSource;
                    if (dataSource != null) {
                        dataSource.interestingEventDetected(InterestingEvent.HARD_BRAKING);
                    }
                }
            }
        }
    }

    public final void speedSourceChanged() {
        if (sessionStarted) {
            rollingSpeedData.clear();
        }
    }

    private final void updateValuesBasedOnSpeed() {
        if (sessionStarted) {
            RawSpeed speedValue = currentSpeed;
            long now = getCurrentTime();
            if (speedValue.getSpeed() > ((float) 0)) {
                if (movingStartTime == 0) {
                    movingStartTime = now;
                }
            } else if (movingStartTime != 0) {
                _rollingDuration += now - movingStartTime;
                movingStartTime = 0;
            }
            sessionMaxSpeed = Math.max(speedValue.getSpeed(), sessionMaxSpeed);
            DataSource dataSource;
            if (currentSpeedLimit != 0.0f) {
                if (speedValue.getSpeed() >= currentSpeedLimit + SPEEDING_SPEED_OVER_SPEED_LIMIT_MS) {
                    if (speedingStartTime == 0) {
                        speedingStartTime = now;
                        dataSource = dataSource;
                        if (dataSource != null) {
                            dataSource.interestingEventDetected(InterestingEvent.SPEEDING_STARTED);
                        }
                    }
                } else if (speedingStartTime != 0) {
                    _speedingDuration += now - speedingStartTime;
                    speedingStartTime = 0;
                    dataSource = dataSource;
                    if (dataSource != null) {
                        dataSource.interestingEventDetected(InterestingEvent.SPEEDING_STOPPED);
                    }
                }
                if (speedValue.getSpeed() >= currentSpeedLimit + EXCESSIVE_SPEEDING_SPEED_OVER_SPEED_LIMIT_MS) {
                    if (excessiveSpeedingStartTime == 0) {
                        excessiveSpeedingStartTime = now;
                        dataSource = dataSource;
                        if (dataSource != null) {
                            dataSource.interestingEventDetected(InterestingEvent.EXCESSIVE_SPEEDING_STARTED);
                            return;
                        }
                        return;
                    }
                    return;
                } else if (excessiveSpeedingStartTime != 0) {
                    _excessiveSpeedingDuration += now - excessiveSpeedingStartTime;
                    excessiveSpeedingStartTime = 0;
                    dataSource = dataSource;
                    if (dataSource != null) {
                        dataSource.interestingEventDetected(InterestingEvent.EXCESSIVE_SPEEDING_STOPPED);
                        return;
                    }
                    return;
                } else {
                    return;
                }
            }
            if (speedingStartTime != 0) {
                _speedingDuration += now - speedingStartTime;
                speedingStartTime = 0;
                dataSource = dataSource;
                if (dataSource != null) {
                    dataSource.interestingEventDetected(InterestingEvent.SPEEDING_STOPPED);
                }
            }
            if (excessiveSpeedingStartTime != 0) {
                _excessiveSpeedingDuration += now - excessiveSpeedingStartTime;
                excessiveSpeedingStartTime = 0;
                dataSource = dataSource;
                if (dataSource != null) {
                    dataSource.interestingEventDetected(InterestingEvent.EXCESSIVE_SPEEDING_STOPPED);
                }
            }
        }
    }

    public final float getCurrentSpeedLimit() {
        return currentSpeedLimit;
    }

    public final void setCurrentSpeedLimit(float value) {
        if (value <= ((float) 0)) {
            value = 0.0f;
        }
        currentSpeedLimit = value;
        updateValuesBasedOnSpeed();
    }

    public final long getExcessiveSpeedingStartTime() {
        return excessiveSpeedingStartTime;
    }

    public final void setExcessiveSpeedingStartTime(long <set-?>) {
        excessiveSpeedingStartTime = <set-?>;
    }

    public final boolean isSpeeding() {
        return speedingStartTime > ((long) null);
    }

    public final void setSpeeding(boolean value) {
    }

    public final void setSpeedingDuration(long <set-?>) {
        speedingDuration = <set-?>;
    }

    public final long getSpeedingDuration() {
        long j = 0;
        long j2 = _speedingDuration;
        if (speedingStartTime != 0) {
            j = getCurrentTime() - speedingStartTime;
        }
        return j + j2;
    }

    public final boolean isExcessiveSpeeding() {
        return excessiveSpeedingStartTime > ((long) null);
    }

    public final void setExcessiveSpeeding(boolean value) {
    }

    public final void setExcessiveSpeedingDuration(long <set-?>) {
        excessiveSpeedingDuration = <set-?>;
    }

    public final long getExcessiveSpeedingDuration() {
        long j = 0;
        long j2 = _excessiveSpeedingDuration;
        if (excessiveSpeedingStartTime != 0) {
            j = getCurrentTime() - excessiveSpeedingStartTime;
        }
        return j + j2;
    }

    public final float getSessionMaxSpeed() {
        return sessionMaxSpeed;
    }

    public final void setSessionMaxSpeed(float <set-?>) {
        sessionMaxSpeed = <set-?>;
    }

    private final void setSessionDuration(long <set-?>) {
        sessionDuration = <set-?>;
    }

    public final long getSessionDuration() {
        return getCurrentTime() - sessionStartTime;
    }

    public final void setSessionRollingDuration(long <set-?>) {
        sessionRollingDuration = <set-?>;
    }

    public final long getSessionRollingDuration() {
        long j = 0;
        long j2 = _rollingDuration;
        if (movingStartTime != 0) {
            j = getCurrentTime() - movingStartTime;
        }
        return j + j2;
    }

    public final void setSessionDistance(long <set-?>) {
        sessionDistance = <set-?>;
    }

    public final long getSessionDistance() {
        return get_currentDistance() - startDistance;
    }

    @NotNull
    public final LinkedList<Pair<Long, Float>> getRollingSpeedData() {
        return rollingSpeedData;
    }

    public final boolean getSessionStarted() {
        return sessionStarted;
    }

    public final void setSessionStarted(boolean <set-?>) {
        sessionStarted = <set-?>;
    }

    public final void startSession() {
        sessionStartTime = getCurrentTime();
        sessionDuration = 0;
        startDistance = get_currentDistance();
        rollingSpeedData.clear();
        setCurrentSpeed(get_currentSpeed());
        sessionRollingDuration = 0;
        _speedingDuration = 0;
        _rollingDuration = 0;
        _excessiveSpeedingDuration = 0;
        excessiveSpeedingStartTime = 0;
        sessionMaxSpeed = 0.0f;
        movingStartTime = 0;
        speedingStartTime = 0;
        sessionStarted = true;
        sessionHighGCount = 0;
    }

    public final void stopSession() {
        sessionStarted = false;
    }

    public final double getEventAverageMpg() {
        return eventAverageMpg;
    }

    public final void setEventAverageMpg(double <set-?>) {
        eventAverageMpg = <set-?>;
    }

    public final long getEventMpgSamples() {
        return eventMpgSamples;
    }

    public final void setEventMpgSamples(long <set-?>) {
        eventMpgSamples = <set-?>;
    }

    public final int getEventAverageRpm() {
        return eventAverageRpm;
    }

    public final void setEventAverageRpm(int <set-?>) {
        eventAverageRpm = <set-?>;
    }

    public final int getEventRpmSamples() {
        return eventRpmSamples;
    }

    public final void setEventRpmSamples(int <set-?>) {
        eventRpmSamples = <set-?>;
    }

    public final void eventReported() {
        eventAverageMpg = 0.0d;
        eventMpgSamples = 1;
        eventAverageRpm = 0;
        eventRpmSamples = 1;
        maxG = 0.0f;
        maxGAngle = 0.0f;
    }

    public final float getMaxG() {
        return maxG;
    }

    public final void setMaxG(float <set-?>) {
        maxG = <set-?>;
    }

    public final float getMaxGAngle() {
        return maxGAngle;
    }

    public final void setMaxGAngle(float <set-?>) {
        maxGAngle = <set-?>;
    }

    public final int getSessionHighGCount() {
        return sessionHighGCount;
    }

    public final void setSessionHighGCount(int <set-?>) {
        sessionHighGCount = <set-?>;
    }

    public final float getLastG() {
        return lastG;
    }

    public final void setLastG(float <set-?>) {
        lastG = <set-?>;
    }

    public final float getLastGAngle() {
        return lastGAngle;
    }

    public final void setLastGAngle(float <set-?>) {
        lastGAngle = <set-?>;
    }

    public final long getLastGTime() {
        return lastGTime;
    }

    public final void setLastGTime(long <set-?>) {
        lastGTime = <set-?>;
    }

    public final boolean isAboveWaterMark() {
        return isAboveWaterMark;
    }

    public final void setAboveWaterMark(boolean <set-?>) {
        isAboveWaterMark = <set-?>;
    }

    public final long getHighGManeuverStartTime() {
        return highGManeuverStartTime;
    }

    public final void setHighGManeuverStartTime(long <set-?>) {
        highGManeuverStartTime = <set-?>;
    }

    public final long getConfirmedHighGManeuverEndTime() {
        return confirmedHighGManeuverEndTime;
    }

    public final void setConfirmedHighGManeuverEndTime(long <set-?>) {
        confirmedHighGManeuverEndTime = <set-?>;
    }

    public final boolean getConfirmedHighGManueverNotified() {
        return confirmedHighGManueverNotified;
    }

    public final void setConfirmedHighGManueverNotified(boolean <set-?>) {
        confirmedHighGManueverNotified = <set-?>;
    }

    public final void setGForce(float x, float y, float z) {
        float g = Math.max(Math.abs(x), Math.abs(y));
        float gAngle = (float) ((Math.atan((double) ((-y) / x)) * ((double) 180.0f)) / 3.141592653589793d);
        if (x < ((float) 0)) {
            gAngle += 180.0f;
        }
        gAngle = (((float) 360) + gAngle) % ((float) 360);
        lastG = g;
        lastGAngle = gAngle;
        long now = getCurrentTime();
        lastGTime = now;
        if (g > maxG) {
            maxG = g;
            maxGAngle = gAngle;
        }
        DataSource dataSource;
        if (g > MAX_G_THRESHOLD) {
            boolean highGAngle;
            logger.d("G " + g + " , Angle " + gAngle);
            Object obj = (125.0f > gAngle || gAngle > 235.0f) ? null : 1;
            if (obj == null) {
                if (305.0f > gAngle || gAngle > 360.0f) {
                    obj = null;
                } else {
                    obj = 1;
                }
                if (obj == null) {
                    if (0.0f > gAngle || gAngle > 55.0f) {
                        obj = null;
                    } else {
                        obj = 1;
                    }
                    if (obj == null) {
                        highGAngle = false;
                        if (highGAngle) {
                            if (!isAboveWaterMark && TelemetrySessionKt.timeSince(now, confirmedHighGManeuverEndTime) > HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS) {
                                logger.d("High G maneuver started ");
                                highGManeuverStartTime = now;
                            }
                            if (isAboveWaterMark && !confirmedHighGManueverNotified && highGManeuverStartTime != 0 && TelemetrySessionKt.timeSince(now, highGManeuverStartTime) > HIGH_G_THRESHOLD_TIME_MILLIS) {
                                sessionHighGCount++;
                                confirmedHighGManueverNotified = true;
                                dataSource = dataSource;
                                if (dataSource != null) {
                                    dataSource.interestingEventDetected(InterestingEvent.HIGH_G_STARTED);
                                }
                                logger.d("High G maneuver confirmed and notified");
                            }
                            isAboveWaterMark = true;
                        }
                    }
                }
            }
            highGAngle = true;
            if (highGAngle) {
                logger.d("High G maneuver started ");
                highGManeuverStartTime = now;
                sessionHighGCount++;
                confirmedHighGManueverNotified = true;
                dataSource = dataSource;
                if (dataSource != null) {
                    dataSource.interestingEventDetected(InterestingEvent.HIGH_G_STARTED);
                }
                logger.d("High G maneuver confirmed and notified");
                isAboveWaterMark = true;
            }
        } else if (g < G_LOWER_THRESHOLD_THRESHOLD) {
            if (highGManeuverStartTime != 0) {
                highGManeuverStartTime = 0;
                logger.d("High G maneuver ended");
                if (confirmedHighGManueverNotified) {
                    confirmedHighGManeuverEndTime = now;
                    confirmedHighGManueverNotified = false;
                    dataSource = dataSource;
                    if (dataSource != null) {
                        dataSource.interestingEventDetected(InterestingEvent.HIGH_G_ENDED);
                    }
                }
            }
            isAboveWaterMark = false;
        }
    }

    public final boolean isDoingHighGManeuver() {
        return confirmedHighGManueverNotified;
    }

    public final void setDoingHighGManeuver(boolean value) {
    }
}
