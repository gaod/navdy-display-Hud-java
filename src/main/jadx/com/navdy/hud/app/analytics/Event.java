package com.navdy.hud.app.analytics;

import com.navdy.service.library.log.Logger;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

class Event {
    public static final String TAG = "Localytics";
    private static final Logger sLogger = new Logger(TAG);
    public Map<String, String> argMap;
    public String tag;

    public Event(String tag, Map<String, String> argMap) {
        this.tag = tag;
        this.argMap = argMap;
    }

    public Event(String tag, String... fieldAndValues) {
        this.tag = tag;
        this.argMap = new HashMap();
        int i = 0;
        while (i < fieldAndValues.length - 1) {
            String a = fieldAndValues[i + 1];
            Map map = this.argMap;
            Object obj = fieldAndValues[i];
            if (a == null) {
                a = "";
            }
            map.put(obj, a);
            i += 2;
        }
        if (i < fieldAndValues.length) {
            sLogger.e("Odd number of event arguments for tag " + tag);
        }
    }

    String getChangedFields(Event oldEvent) {
        StringBuilder builder = new StringBuilder();
        for (Entry<String, String> entry : this.argMap.entrySet()) {
            String fieldName = (String) entry.getKey();
            String oldValue = (String) oldEvent.argMap.get(fieldName);
            String newValue = (String) entry.getValue();
            if (!(newValue == null || newValue.equals(oldValue)) || (newValue == null && oldValue != null)) {
                if (builder.length() == 0) {
                    builder.append("|");
                }
                builder.append(fieldName);
                builder.append("|");
            }
        }
        return builder.toString();
    }

    public String toString() {
        return "Event{tag='" + this.tag + '\'' + ", argMap=" + this.argMap + '}';
    }
}
