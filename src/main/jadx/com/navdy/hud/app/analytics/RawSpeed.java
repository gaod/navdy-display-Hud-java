package com.navdy.hud.app.analytics;

import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import kotlin.Metadata;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/navdy/hud/app/analytics/RawSpeed;", "", "speed", "", "timeStamp", "", "(FJ)V", "getSpeed", "()F", "getTimeStamp", "()J", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: RawSpeed.kt */
public final class RawSpeed {
    private final float speed;
    private final long timeStamp;

    @NotNull
    public static /* bridge */ /* synthetic */ RawSpeed copy$default(RawSpeed rawSpeed, float f, long j, int i, Object obj) {
        if ((i & 1) != 0) {
            f = rawSpeed.speed;
        }
        if ((i & 2) != 0) {
            j = rawSpeed.timeStamp;
        }
        return rawSpeed.copy(f, j);
    }

    public final float component1() {
        return this.speed;
    }

    public final long component2() {
        return this.timeStamp;
    }

    @NotNull
    public final RawSpeed copy(float speed, long timeStamp) {
        return new RawSpeed(speed, timeStamp);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof RawSpeed)) {
                return false;
            }
            RawSpeed rawSpeed = (RawSpeed) obj;
            if (Float.compare(this.speed, rawSpeed.speed) != 0) {
                return false;
            }
            if (!(this.timeStamp == rawSpeed.timeStamp)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int floatToIntBits = Float.floatToIntBits(this.speed) * 31;
        long j = this.timeStamp;
        return floatToIntBits + ((int) (j ^ (j >>> 32)));
    }

    public String toString() {
        return "RawSpeed(speed=" + this.speed + ", timeStamp=" + this.timeStamp + HereManeuverDisplayBuilder.CLOSE_BRACKET;
    }

    public RawSpeed(float speed, long timeStamp) {
        this.speed = speed;
        this.timeStamp = timeStamp;
    }

    public /* synthetic */ RawSpeed(float f, long j, int i, DefaultConstructorMarker defaultConstructorMarker) {
        if ((i & 2) != 0) {
            j = 0;
        }
        this(f, j);
    }

    public final float getSpeed() {
        return this.speed;
    }

    public final long getTimeStamp() {
        return this.timeStamp;
    }
}
