package com.navdy.hud.app.ui.component.homescreen;

public interface IHomeScreenLifecycle {
    void onPause();

    void onResume();
}
