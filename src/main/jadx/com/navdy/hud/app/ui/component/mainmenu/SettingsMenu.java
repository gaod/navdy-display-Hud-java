package com.navdy.hud.app.ui.component.mainmenu;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.event.Shutdown.Reason;
import com.navdy.hud.app.manager.UpdateReminderManager;
import com.navdy.hud.app.screen.WelcomeScreen;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder;
import com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import java.util.ArrayList;
import java.util.List;

public class SettingsMenu implements IMenu {
    private static final Model back;
    private static final Model brightness;
    private static final Model connectPhone;
    private static final Model dialFwUpdate;
    private static final Model displayFwUpdate;
    private static final Model factoryReset;
    private static final Model learningGestures;
    private static final Resources resources = HudApplication.getAppContext().getResources();
    private static final Logger sLogger = new Logger(SettingsMenu.class);
    private static final String setting = resources.getString(R.string.carousel_menu_settings_title);
    private static final int settingColor = resources.getColor(R.color.mm_settings);
    private static final Model shutdown;
    private static final Model systemInfo = IconsTwoViewHolder.buildModel(R.id.main_menu_system_info, R.drawable.icon_settings_navdy_data, R.drawable.icon_settings_navdy_data_sm, resources.getColor(R.color.mm_settings_sys_info), -1, resources.getString(R.string.carousel_menu_system_info_title), null);
    private int backSelection;
    private int backSelectionId;
    private Bus bus;
    private List<Model> cachedList;
    private IMenu parent;
    private Presenter presenter;
    private SystemInfoMenu systemInfoMenu;
    private VerticalMenuComponent vscrollComponent;

    static {
        String title = resources.getString(R.string.back);
        int fluctuatorColor = resources.getColor(R.color.mm_back);
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_settings_brightness);
        fluctuatorColor = resources.getColor(R.color.mm_settings_brightness);
        brightness = IconBkColorViewHolder.buildModel(R.id.settings_menu_brightness, R.drawable.icon_settings_brightness_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_settings_connect_phone_title);
        fluctuatorColor = resources.getColor(R.color.mm_connnect_phone);
        connectPhone = IconBkColorViewHolder.buildModel(R.id.settings_menu_connect_phone, R.drawable.icon_settings_connect_phone_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_settings_learning_gesture_title);
        fluctuatorColor = resources.getColor(R.color.mm_settings_learn_gestures);
        learningGestures = IconBkColorViewHolder.buildModel(R.id.settings_menu_learning_gestures, R.drawable.icon_settings_learn_gestures_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_settings_software_update_title);
        fluctuatorColor = resources.getColor(R.color.mm_settings_update_display);
        displayFwUpdate = IconBkColorViewHolder.buildModel(R.id.settings_menu_software_update, R.drawable.icon_software_update_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_settings_dial_update_title);
        fluctuatorColor = resources.getColor(R.color.mm_settings_update_dial);
        dialFwUpdate = IconBkColorViewHolder.buildModel(R.id.settings_menu_dial_update, R.drawable.icon_dial_update_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_settings_shutdown_title);
        fluctuatorColor = resources.getColor(R.color.mm_settings_shutdown);
        shutdown = IconBkColorViewHolder.buildModel(R.id.settings_menu_shutdown, R.drawable.icon_settings_shutdown_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_settings_factory_reset_title);
        fluctuatorColor = resources.getColor(R.color.mm_settings_factory_reset);
        factoryReset = IconBkColorViewHolder.buildModel(R.id.settings_menu_factory_reset, R.drawable.icon_settings_factory_reset_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
    }

    public SettingsMenu(Bus bus, VerticalMenuComponent vscrollComponent, Presenter presenter, IMenu parent) {
        this.bus = bus;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
    }

    public List<Model> getItems() {
        if (this.cachedList != null) {
            return this.cachedList;
        }
        List<Model> list = new ArrayList();
        list.add(back);
        list.add(brightness);
        list.add(connectPhone);
        list.add(learningGestures);
        if (OTAUpdateService.isUpdateAvailable()) {
            list.add(displayFwUpdate);
        }
        DialManager dialManager = DialManager.getInstance();
        if (dialManager.isDialConnected() && dialManager.getDialFirmwareUpdater().getVersions().isUpdateAvailable()) {
            list.add(dialFwUpdate);
        }
        list.add(shutdown);
        list.add(factoryReset);
        list.add(systemInfo);
        this.cachedList = list;
        return list;
    }

    public int getInitialSelection() {
        return 1;
    }

    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
        this.backSelectionId = id;
    }

    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_mm_settings_2, settingColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText(setting);
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    public Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(Model model, View view, int pos, ModelState state) {
    }

    public IMenu getChildMenu(IMenu parent, String args, String path) {
        return null;
    }

    public void onUnload(MenuLevel level) {
    }

    public void onItemSelected(ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }

    public boolean selectItem(ItemSelectionState selection) {
        sLogger.v("select id:" + selection.id + " pos:" + selection.pos);
        Bundle args;
        switch (selection.id) {
            case R.id.main_menu_system_info:
                sLogger.v("system info");
                AnalyticsSupport.recordMenuSelection("system_info");
                if (this.systemInfoMenu == null) {
                    this.systemInfoMenu = new SystemInfoMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                this.presenter.loadMenu(this.systemInfoMenu, MenuLevel.SUB_LEVEL, selection.pos, 1, true);
                break;
            case R.id.menu_back:
                sLogger.v("back");
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                this.backSelectionId = 0;
                break;
            case R.id.settings_menu_auto_brightness:
                sLogger.v("auto brightness");
                AnalyticsSupport.recordMenuSelection("auto_brightness");
                this.presenter.performSelectionAnimation(new ActionRunnable(this.bus, Screen.SCREEN_AUTO_BRIGHTNESS));
                break;
            case R.id.settings_menu_brightness:
                sLogger.v("brightness");
                AnalyticsSupport.recordMenuSelection("brightness");
                this.presenter.performSelectionAnimation(new ActionRunnable(this.bus, Screen.SCREEN_BRIGHTNESS));
                break;
            case R.id.settings_menu_connect_phone:
                sLogger.v("connect phone");
                AnalyticsSupport.recordMenuSelection("connect_phone");
                args = new Bundle();
                args.putString(WelcomeScreen.ARG_ACTION, WelcomeScreen.ACTION_SWITCH_PHONE);
                this.presenter.performSelectionAnimation(new ActionRunnable(this.bus, Screen.SCREEN_WELCOME, args, false));
                break;
            case R.id.settings_menu_dial_pairing:
                sLogger.v("Dial pairing");
                AnalyticsSupport.recordMenuSelection("dial_pairing");
                this.presenter.performSelectionAnimation(new ActionRunnable(this.bus, Screen.SCREEN_DIAL_PAIRING));
                break;
            case R.id.settings_menu_dial_update:
                sLogger.v("dial update");
                AnalyticsSupport.recordMenuSelection("dial_update");
                args = new Bundle();
                args.putBoolean(UpdateReminderManager.EXTRA_UPDATE_REMINDER, false);
                this.presenter.performSelectionAnimation(new ActionRunnable(this.bus, Screen.SCREEN_DIAL_UPDATE_CONFIRMATION, args, false));
                break;
            case R.id.settings_menu_factory_reset:
                sLogger.v("factory reset");
                AnalyticsSupport.recordMenuSelection("factory_reset");
                this.presenter.performSelectionAnimation(new ActionRunnable(this.bus, Screen.SCREEN_FACTORY_RESET));
                break;
            case R.id.settings_menu_learning_gestures:
                sLogger.v("Learning gestures");
                AnalyticsSupport.recordMenuSelection("learning_gestures");
                this.presenter.performSelectionAnimation(new ActionRunnable(this.bus, Screen.SCREEN_GESTURE_LEARNING));
                break;
            case R.id.settings_menu_shutdown:
                sLogger.v("shutdown");
                AnalyticsSupport.recordMenuSelection("shutdown");
                this.presenter.performSelectionAnimation(new ActionRunnable(this.bus, Screen.SCREEN_SHUTDOWN_CONFIRMATION, Reason.MENU.asBundle(), false));
                break;
            case R.id.settings_menu_software_update:
                sLogger.v("software update");
                AnalyticsSupport.recordMenuSelection("software_update");
                args = new Bundle();
                args.putBoolean(UpdateReminderManager.EXTRA_UPDATE_REMINDER, false);
                this.presenter.performSelectionAnimation(new ActionRunnable(this.bus, Screen.SCREEN_OTA_CONFIRMATION, args, false));
                break;
        }
        return true;
    }

    public Menu getType() {
        return Menu.SETTINGS;
    }
}
