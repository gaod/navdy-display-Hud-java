package com.navdy.hud.app.ui.component.homescreen;

import android.widget.ImageView;
import butterknife.ButterKnife.Finder;
import com.here.android.mpa.mapping.MapView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.FluctuatorAnimatorView;

public class NavigationView$$ViewInjector {
    public static void inject(Finder finder, NavigationView target, Object source) {
        target.mapView = (MapView) finder.findRequiredView(source, R.id.mapView, "field 'mapView'");
        target.startDestinationFluctuatorView = (FluctuatorAnimatorView) finder.findRequiredView(source, R.id.startDestinationFluctuator, "field 'startDestinationFluctuatorView'");
        target.mapMask = (ImageView) finder.findRequiredView(source, R.id.map_mask, "field 'mapMask'");
        target.mapIconIndicator = (ImageView) finder.findRequiredView(source, R.id.mapIconIndicator, "field 'mapIconIndicator'");
        target.noLocationView = (NoLocationView) finder.findRequiredView(source, R.id.noLocationContainer, "field 'noLocationView'");
    }

    public static void reset(NavigationView target) {
        target.mapView = null;
        target.startDestinationFluctuatorView = null;
        target.mapMask = null;
        target.mapIconIndicator = null;
        target.noLocationView = null;
    }
}
