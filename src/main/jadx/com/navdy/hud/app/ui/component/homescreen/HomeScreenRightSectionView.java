package com.navdy.hud.app.ui.component.homescreen;

import android.animation.AnimatorSet.Builder;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.FrameLayout;
import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.service.library.log.Logger;

public class HomeScreenRightSectionView extends FrameLayout {
    private static final Logger sLogger = new Logger(HomeScreenRightSectionView.class);

    public HomeScreenRightSectionView(Context context) {
        super(context);
    }

    public HomeScreenRightSectionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HomeScreenRightSectionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void updateLayoutForMode(NavigationMode navigationMode, HomeScreenView homeScreenView) {
        ((MarginLayoutParams) getLayoutParams()).height = homeScreenView.isNavigationActive() ? HomeScreenResourceValues.activeMapHeight : HomeScreenResourceValues.openMapHeight;
        requestLayout();
    }

    public void injectRightSection(View view) {
        sLogger.v("injectRightSection");
        removeAllViews();
        addView(view);
    }

    public void ejectRightSection() {
        sLogger.v("ejectRightSection");
        removeAllViews();
    }

    public boolean isViewVisible() {
        return getX() < ((float) HomeScreenResourceValues.mainScreenRightSectionX);
    }

    public boolean hasView() {
        return getChildCount() > 0;
    }

    public void setView(CustomAnimationMode mode) {
        sLogger.v("setview: " + mode);
        setX((float) HomeScreenResourceValues.mainScreenRightSectionX);
    }

    public void getCustomAnimator(CustomAnimationMode mode, Builder mainBuilder) {
        switch (mode) {
            case SHRINK_MODE:
                mainBuilder.with(HomeScreenUtils.getXPositionAnimator(this, (float) HomeScreenResourceValues.mainScreenRightSectionShrinkX));
                return;
            case EXPAND:
                mainBuilder.with(HomeScreenUtils.getXPositionAnimator(this, (float) HomeScreenResourceValues.mainScreenRightSectionX));
                return;
            default:
                return;
        }
    }
}
