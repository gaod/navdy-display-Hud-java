package com.navdy.hud.app.ui.framework;

import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.screen.DialManagerScreen;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.hud.app.ui.component.homescreen.NavigationView;
import com.navdy.hud.app.ui.component.homescreen.SmartDashView;
import com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants.Type;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.hud.app.view.ToastView;
import com.navdy.service.library.events.ui.Screen;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Iterator;

public class UIStateManager {
    private static final EnumSet<Screen> FULLSCREEN_MODES = EnumSet.of(Screen.SCREEN_FIRST_LAUNCH, Screen.SCREEN_HOME, Screen.SCREEN_WELCOME, Screen.SCREEN_DIAL_PAIRING);
    private static final EnumSet<Screen> MAIN_SCREENS_SET = EnumSet.of(Screen.SCREEN_HOME);
    private static final EnumSet<Screen> OVERLAY_MODES = EnumSet.of(Screen.SCREEN_MENU, new Screen[]{Screen.SCREEN_MAIN_MENU, Screen.SCREEN_RECENT_CALLS, Screen.SCREEN_RECOMMENDED_PLACES, Screen.SCREEN_FAVORITE_PLACES, Screen.SCREEN_OTA_CONFIRMATION, Screen.SCREEN_BRIGHTNESS, Screen.SCREEN_FAVORITE_CONTACTS, Screen.SCREEN_SHUTDOWN_CONFIRMATION, Screen.SCREEN_SETTINGS, Screen.SCREEN_AUTO_BRIGHTNESS, Screen.SCREEN_FACTORY_RESET, Screen.SCREEN_REPORT_ISSUE, Screen.SCREEN_TOAST, Screen.SCREEN_FORCE_UPDATE, Screen.SCREEN_GESTURE_LEARNING, Screen.SCREEN_DESTINATION_PICKER, Screen.SCREEN_MUSIC_DETAILS});
    private static final EnumSet<Screen> PAUSE_HOME_SCREEN_SET = EnumSet.of(Screen.SCREEN_MENU, new Screen[]{Screen.SCREEN_RECENT_CALLS, Screen.SCREEN_RECOMMENDED_PLACES, Screen.SCREEN_FAVORITE_PLACES, Screen.SCREEN_OTA_CONFIRMATION, Screen.SCREEN_FAVORITE_CONTACTS, Screen.SCREEN_SHUTDOWN_CONFIRMATION, Screen.SCREEN_SETTINGS, Screen.SCREEN_AUTO_BRIGHTNESS, Screen.SCREEN_FACTORY_RESET, Screen.SCREEN_REPORT_ISSUE, Screen.SCREEN_FORCE_UPDATE, Screen.SCREEN_GESTURE_LEARNING, Screen.SCREEN_MUSIC_DETAILS});
    private static final EnumSet<Screen> SIDE_PANEL_MODES = EnumSet.of(Screen.SCREEN_NOTIFICATION);
    private volatile Type currentDashboardType;
    private BaseScreen currentScreen;
    private volatile Screen currentViewMode;
    private Screen defaultMainActiveScreen = Screen.SCREEN_HOME;
    private HomeScreenView homeScreenView;
    private BaseScreen mainActiveScreen;
    private int mainPanelWidth;
    private HashSet<INotificationAnimationListener> notificationListeners = new HashSet();
    private Main rootScreen;
    private HashSet<IScreenAnimationListener> screenListeners = new HashSet();
    private int sidePanelWidth;
    private ToastView toastView;

    public enum Mode {
        EXPAND,
        COLLAPSE
    }

    public static boolean isSidePanelMode(Screen screen) {
        return SIDE_PANEL_MODES.contains(screen);
    }

    public static boolean isOverlayMode(Screen screen) {
        return OVERLAY_MODES.contains(screen);
    }

    public static boolean isFullscreenMode(Screen screen) {
        return FULLSCREEN_MODES.contains(screen);
    }

    public static boolean isPauseHomescreen(Screen screen) {
        return PAUSE_HOME_SCREEN_SET.contains(screen);
    }

    public NavigationView getNavigationView() {
        if (this.homeScreenView != null) {
            return this.homeScreenView.getNavigationView();
        }
        return null;
    }

    public SmartDashView getSmartDashView() {
        if (this.homeScreenView != null) {
            return (SmartDashView) this.homeScreenView.getSmartDashView();
        }
        return null;
    }

    public void setCurrentViewMode(Screen screen) {
        this.currentViewMode = screen;
    }

    public Screen getCurrentViewMode() {
        return this.currentViewMode;
    }

    public void setCurrentDashboardType(Type dashboardType) {
        this.currentDashboardType = dashboardType;
    }

    public Type getCurrentDashboardType() {
        return this.currentDashboardType;
    }

    public boolean isMainUIShrunk() {
        if (this.rootScreen != null) {
            return this.rootScreen.isMainUIShrunk();
        }
        return false;
    }

    public void setMainActiveScreen(BaseScreen screen) {
        if (isFullscreenMode(screen.getScreen())) {
            this.mainActiveScreen = screen;
            Screen s = screen.getScreen();
            if (!(s == Screen.SCREEN_WELCOME || s == Screen.SCREEN_DIAL_PAIRING || s == Screen.SCREEN_FIRST_LAUNCH)) {
                this.defaultMainActiveScreen = s;
            }
        } else {
            this.mainActiveScreen = null;
        }
        this.currentScreen = screen;
    }

    public void setHomescreenView(HomeScreenView view) {
        this.homeScreenView = view;
    }

    public HomeScreenView getHomescreenView() {
        return this.homeScreenView;
    }

    public ToastView getToastView() {
        return this.toastView;
    }

    public void setToastView(ToastView toastView) {
        this.toastView = toastView;
    }

    public BaseScreen getMainActiveScreen() {
        return this.mainActiveScreen;
    }

    public Screen getDefaultMainActiveScreen() {
        return this.defaultMainActiveScreen;
    }

    public void setRootScreen(Main rootScreen) {
        this.rootScreen = rootScreen;
    }

    public Main getRootScreen() {
        return this.rootScreen;
    }

    public BaseScreen getCurrentScreen() {
        return this.currentScreen;
    }

    public void addScreenAnimationListener(IScreenAnimationListener listener) {
        checkListener(listener);
        this.screenListeners.add(listener);
    }

    public void removeScreenAnimationListener(IScreenAnimationListener listener) {
        if (listener != null) {
            this.screenListeners.remove(listener);
        }
    }

    public void addNotificationAnimationListener(INotificationAnimationListener listener) {
        synchronized (this.notificationListeners) {
            checkListener(listener);
            this.notificationListeners.add(listener);
        }
    }

    public void removeNotificationAnimationListener(INotificationAnimationListener listener) {
        synchronized (this.notificationListeners) {
            if (listener != null) {
                this.notificationListeners.remove(listener);
            }
        }
    }

    private void checkListener(Object listener) {
        if (listener == null) {
            throw new IllegalArgumentException();
        }
    }

    public void postScreenAnimationEvent(boolean start, BaseScreen in, BaseScreen out) {
        Iterator it = this.screenListeners.iterator();
        while (it.hasNext()) {
            IScreenAnimationListener listener = (IScreenAnimationListener) it.next();
            if (start) {
                listener.onStart(in, out);
            } else {
                listener.onStop(in, out);
            }
        }
    }

    public void postNotificationAnimationEvent(boolean start, String id, NotificationType type, Mode mode) {
        synchronized (this.notificationListeners) {
            Iterator it = this.notificationListeners.iterator();
            while (it.hasNext()) {
                INotificationAnimationListener listener = (INotificationAnimationListener) it.next();
                if (start) {
                    listener.onStart(id, type, mode);
                } else {
                    listener.onStop(id, type, mode);
                }
            }
        }
    }

    public EnumSet<Screen> getMainScreensSet() {
        return MAIN_SCREENS_SET;
    }

    public CustomAnimationMode getCustomAnimateMode() {
        CustomAnimationMode mode = CustomAnimationMode.EXPAND;
        if (isMainUIShrunk() && (this.rootScreen.isNotificationViewShowing() || this.rootScreen.isNotificationExpanding())) {
            mode = CustomAnimationMode.SHRINK_LEFT;
        }
        if (mode == null || this.homeScreenView == null || mode != CustomAnimationMode.EXPAND || !this.homeScreenView.isModeVisible()) {
            return mode;
        }
        return CustomAnimationMode.SHRINK_MODE;
    }

    public boolean isWelcomeScreenOn() {
        BaseScreen screen = getCurrentScreen();
        if (screen == null || screen.getScreen() != Screen.SCREEN_WELCOME) {
            return false;
        }
        return true;
    }

    public boolean isDialPairingScreenOn() {
        BaseScreen screen = getCurrentScreen();
        if (screen == null || screen.getScreen() != Screen.SCREEN_DIAL_PAIRING) {
            return false;
        }
        return true;
    }

    public boolean isDialPairingScreenScanning() {
        BaseScreen screen = getCurrentScreen();
        if (screen == null || screen.getScreen() != Screen.SCREEN_DIAL_PAIRING) {
            return false;
        }
        return ((DialManagerScreen) screen).isScanningMode();
    }

    public int getMainPanelWidth() {
        return this.mainPanelWidth;
    }

    public void setMainPanelWidth(int n) {
        this.mainPanelWidth = n;
    }

    public int getSidePanelWidth() {
        return this.sidePanelWidth;
    }

    public void setSidePanelWidth(int n) {
        this.sidePanelWidth = n;
    }

    public void showSystemTray(int visibility) {
        if (this.rootScreen != null) {
            this.rootScreen.setSystemTrayVisibility(visibility);
        }
    }

    public void enableSystemTray(boolean enabled) {
        if (this.rootScreen != null) {
            this.rootScreen.enableSystemTray(enabled);
        }
    }

    public void setInputFocus() {
        if (this.rootScreen != null) {
            this.rootScreen.setInputFocus();
        }
    }

    public void enableNotificationColor(boolean enabled) {
        if (this.rootScreen != null) {
            this.rootScreen.enableNotificationColor(enabled);
        }
    }

    public boolean isNavigationActive() {
        HomeScreenView homeScreenView = getHomescreenView();
        if (homeScreenView != null) {
            return homeScreenView.isNavigationActive();
        }
        return false;
    }
}
