package com.navdy.hud.app.ui.component.destination;

import android.text.Html;
import android.text.TextUtils;
import android.widget.TextView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.destinations.Destination;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView.Style;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType;
import com.navdy.hud.app.util.ViewUtil;
import com.navdy.hud.app.view.MaxWidthLinearLayout;

public class DestinationConfirmationHelper {
    private static float[] FONT_SIZES = new float[]{30.0f, 26.0f, 26.0f, 22.0f};
    private static int[] MAX_LINES = new int[]{1, 1, 2, 2};

    public static void configure(ConfirmationLayout confirmationLayout, DestinationParcelable destination) {
        configure(confirmationLayout, null, destination.destinationTitle, destination.destinationSubTitle, destination.destinationSubTitleFormatted, destination.iconUnselected == 0, destination.icon, destination.iconSelectedColor);
    }

    public static void configure(ConfirmationLayout confirmationLayout, Model model, Destination destination) {
        configure(confirmationLayout, destination, destination.destinationTitle, destination.destinationSubtitle, false, model.type == ModelType.ICON_BKCOLOR, model.icon, model.iconSelectedColor);
    }

    private static void setInitials(Destination destination, InitialsImageView initialsImageView) {
        if (destination == null || TextUtils.isEmpty(destination.initials)) {
            initialsImageView.setInitials(null, Style.DEFAULT);
            return;
        }
        Style style;
        if (destination.isInitialNumber) {
            if (destination.initials.length() <= 3) {
                style = Style.LARGE;
            } else {
                style = Style.MEDIUM;
            }
        } else if (destination.initials.length() <= 2) {
            style = Style.LARGE;
        } else {
            style = Style.MEDIUM;
        }
        initialsImageView.setInitials(destination.initials, style);
    }

    private static void setImage(ConfirmationLayout confirmationLayout, Destination destination, boolean hasBkColor, int icon, int iconBkColor) {
        if (hasBkColor) {
            confirmationLayout.screenImageIconBkColor.setIcon(icon, iconBkColor, null, 1.25f);
            confirmationLayout.screenImage.setVisibility(8);
            confirmationLayout.screenImageIconBkColor.setVisibility(0);
            return;
        }
        setInitials(destination, confirmationLayout.screenImage);
        confirmationLayout.screenImage.setImageResource(icon);
        confirmationLayout.screenImage.setVisibility(0);
        confirmationLayout.screenImageIconBkColor.setVisibility(8);
    }

    private static void configure(ConfirmationLayout confirmationLayout, Destination destination, String title, CharSequence subTitle, boolean subTitleFormatted, boolean hasBkColor, int icon, int iconBkColor) {
        setImage(confirmationLayout, destination, hasBkColor, icon, iconBkColor);
        confirmationLayout.screenTitle.setText(R.string.destination_change_navigation_title);
        confirmationLayout.title1.setVisibility(8);
        TextView titleTextView = confirmationLayout.title2;
        TextView subTitleTextView = confirmationLayout.title3;
        MaxWidthLinearLayout infoContainer = (MaxWidthLinearLayout) confirmationLayout.findViewById(R.id.infoContainer);
        int padding = infoContainer.getPaddingStart() + infoContainer.getPaddingEnd();
        int maxWidth = infoContainer.getMaxWidth();
        titleTextView.setText(title);
        int[] holder = new int[2];
        ViewUtil.autosize(titleTextView, MAX_LINES, maxWidth - padding, FONT_SIZES, holder);
        titleTextView.setMaxLines(holder[1]);
        if (!TextUtils.isEmpty(subTitle) && subTitleFormatted) {
            subTitle = Html.fromHtml((String) subTitle);
        }
        ViewUtil.applyTextAndStyle(subTitleTextView, subTitle, 2, R.style.destination_subtitle_two_line);
        titleTextView.setVisibility(0);
        subTitleTextView.setVisibility(0);
    }
}
