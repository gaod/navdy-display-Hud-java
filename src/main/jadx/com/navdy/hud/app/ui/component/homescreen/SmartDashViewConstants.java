package com.navdy.hud.app.ui.component.homescreen;

public class SmartDashViewConstants {
    public static final int DEFAULT_SPEED_LIMIT_THRESHOLD_KMPH = 13;
    public static final int DEFAULT_SPEED_LIMIT_THRESHOLD_MPH = 8;
    public static final String PREFERENCE_SPEED_LIMIT_THRESHOLD = "PREFERENCE_SPEED_LIMIT_THRESHOLD";

    public enum Type {
        Smart,
        Eco
    }
}
