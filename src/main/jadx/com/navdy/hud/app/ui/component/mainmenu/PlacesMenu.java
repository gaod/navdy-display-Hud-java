package com.navdy.hud.app.ui.component.mainmenu;

import android.content.res.Resources;
import android.view.View;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.destinations.Destination;
import com.navdy.hud.app.framework.destinations.Destination.DestinationType;
import com.navdy.hud.app.framework.destinations.Destination.PlaceCategory;
import com.navdy.hud.app.framework.destinations.DestinationsManager;
import com.navdy.hud.app.framework.destinations.DestinationsManager.GasDestination;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapsNotification;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.ui.component.ChoiceLayout.IListener;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.hud.app.ui.component.destination.DestinationConfirmationHelper;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.VerticalModelCache;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.util.RemoteCapabilitiesUtil;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class PlacesMenu implements IMenu {
    public static final int CANCEL_POSITION = 1;
    private static List<String> CONFIRMATION_CHOICES = new ArrayList();
    public static final int NAVIGATE_POSITION = 0;
    private static final int SELECTION_ANIMATION_DELAY = 1000;
    private static final Model back;
    private static final int backColor = resources.getColor(R.color.mm_back);
    public static final int bkColorUnselected = resources.getColor(R.color.icon_bk_color_unselected);
    private static final int contactColor = resources.getColor(R.color.mm_place_contact);
    private static final int favColor = resources.getColor(R.color.mm_place_favorite);
    private static final int gasColor = resources.getColor(R.color.mm_place_gas);
    private static final int homeColor = resources.getColor(R.color.mm_place_home);
    public static final String places = resources.getString(R.string.carousel_menu_map_places);
    private static final int placesColor = resources.getColor(R.color.mm_places);
    private static final int recentColor = resources.getColor(R.color.mm_recent_places);
    private static final Model recentPlaces;
    private static final Resources resources = HudApplication.getAppContext().getResources();
    private static final Logger sLogger = new Logger(PlacesMenu.class);
    private static final Model search;
    public static final String suggested = resources.getString(R.string.suggested);
    public static final int suggestedColor = resources.getColor(R.color.mm_place_suggested);
    private static final int workColor = resources.getColor(R.color.mm_place_work);
    private int backSelection;
    private int backSelectionId;
    private Bus bus;
    private List<Model> cachedList;
    private HashSet<String> latlngSet = new HashSet();
    private NearbyPlacesMenu nearbyPlacesMenu;
    private IMenu parent;
    private Presenter presenter;
    private RecentPlacesMenu recentPlacesMenu;
    private List<Model> returnToCacheList;
    private VerticalMenuComponent vscrollComponent;

    static {
        String title = resources.getString(R.string.back);
        int fluctuatorColor = backColor;
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_menu_recent_place);
        fluctuatorColor = resources.getColor(R.color.mm_recent_places);
        recentPlaces = IconBkColorViewHolder.buildModel(R.id.places_menu_recent, R.drawable.icon_place_recent_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_menu_search_title);
        fluctuatorColor = resources.getColor(R.color.mm_search);
        search = IconBkColorViewHolder.buildModel(R.id.main_menu_search, R.drawable.icon_mm_search_2, fluctuatorColor, bkColorUnselected, fluctuatorColor, title, null);
        CONFIRMATION_CHOICES.add(resources.getString(R.string.destination_start_navigation_yes));
        CONFIRMATION_CHOICES.add(resources.getString(R.string.destination_start_navigation_cancel));
    }

    public PlacesMenu(Bus bus, VerticalMenuComponent vscrollComponent, Presenter presenter, IMenu parent) {
        this.bus = bus;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
    }

    public List<Model> getItems() {
        if (this.cachedList != null) {
            return this.cachedList;
        }
        int counter;
        Model model;
        List<Model> list = new ArrayList();
        this.returnToCacheList = new ArrayList();
        list.add(back);
        DeviceInfo deviceInfo = RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
        boolean isConnected = !DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        if (isConnected && deviceInfo == null) {
            isConnected = false;
        }
        sLogger.v("isConnected:" + isConnected);
        if (isConnected && RemoteCapabilitiesUtil.supportsPlaceSearch()) {
            list.add(search);
        }
        list.add(recentPlaces);
        int firstIndex = list.size();
        int counter2 = 0;
        DestinationsManager destinationsManager = DestinationsManager.getInstance();
        this.latlngSet.clear();
        List<Destination> suggestedDestinations = destinationsManager.getSuggestedDestinations();
        if (suggestedDestinations == null || suggestedDestinations.size() <= 0) {
            sLogger.v("no suggested destinations");
        } else {
            sLogger.v("suggested destinations:" + suggestedDestinations.size());
            for (Destination destination : suggestedDestinations) {
                counter = counter2 + 1;
                model = buildModel(destination, counter2);
                model.subTitle = suggested;
                model.state = destination;
                model.extras = new HashMap();
                model.extras.put(Model.SUBTITLE_COLOR, String.valueOf(suggestedColor));
                list.add(model);
                this.returnToCacheList.add(model);
                this.latlngSet.add(destination.displayPositionLatitude + HereManeuverDisplayBuilder.COMMA + destination.displayPositionLongitude + HereManeuverDisplayBuilder.COMMA + destination.navigationPositionLatitude + HereManeuverDisplayBuilder.COMMA + destination.navigationPositionLongitude);
                counter2 = counter;
            }
        }
        List<Destination> favoriteDestinations = destinationsManager.getFavoriteDestinations();
        if (favoriteDestinations == null || favoriteDestinations.size() <= 0) {
            sLogger.v("no favorite destinations");
        } else {
            sLogger.v("favorite destinations:" + favoriteDestinations.size());
            for (Destination destination2 : favoriteDestinations) {
                if (this.latlngSet.contains(destination2.displayPositionLatitude + HereManeuverDisplayBuilder.COMMA + destination2.displayPositionLongitude + HereManeuverDisplayBuilder.COMMA + destination2.navigationPositionLatitude + HereManeuverDisplayBuilder.COMMA + destination2.navigationPositionLongitude)) {
                    sLogger.v("latlng already seen in suggested:" + destination2.destinationTitle);
                } else {
                    counter = counter2 + 1;
                    model = buildModel(destination2, counter2);
                    model.state = destination2;
                    list.add(model);
                    this.returnToCacheList.add(model);
                    counter2 = counter;
                }
            }
        }
        GasDestination gasDestination = DestinationsManager.getInstance().getGasDestination();
        if (gasDestination != null) {
            counter = counter2 + 1;
            model = buildModel(gasDestination.destination, counter2);
            model.subTitle = suggested;
            model.state = gasDestination.destination;
            model.extras = new HashMap();
            model.extras.put(Model.SUBTITLE_COLOR, String.valueOf(suggestedColor));
            if (gasDestination.showFirst) {
                list.add(firstIndex, model);
            } else {
                list.add(model);
            }
            this.returnToCacheList.add(model);
            counter2 = counter;
        }
        this.cachedList = list;
        return list;
    }

    public int getInitialSelection() {
        if (this.cachedList == null) {
            return 0;
        }
        int index = this.cachedList.indexOf(recentPlaces);
        if (index != -1) {
            return index;
        }
        if (this.cachedList.size() >= 3) {
            return 2;
        }
        return 1;
    }

    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
        this.backSelectionId = id;
    }

    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_mm_places_2, placesColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText(places);
    }

    public Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(Model model, View view, int pos, ModelState state) {
    }

    public IMenu getChildMenu(IMenu parent, String args, String path) {
        return null;
    }

    public void onUnload(MenuLevel level) {
        switch (level) {
            case CLOSE:
                if (this.returnToCacheList != null) {
                    sLogger.v("pm:unload add to cache");
                    VerticalModelCache.addToCache(this.returnToCacheList);
                    this.returnToCacheList = null;
                }
                if (this.recentPlacesMenu != null) {
                    this.recentPlacesMenu.onUnload(MenuLevel.CLOSE);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onItemSelected(ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }

    public boolean selectItem(ItemSelectionState selection) {
        sLogger.v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case R.id.main_menu_search:
                sLogger.v("search");
                AnalyticsSupport.recordMenuSelection("search");
                if (this.nearbyPlacesMenu == null) {
                    this.nearbyPlacesMenu = new NearbyPlacesMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                this.presenter.loadMenu(this.nearbyPlacesMenu, MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            case R.id.menu_back:
                sLogger.v("back");
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                this.backSelectionId = 0;
                break;
            case R.id.places_menu_recent:
                sLogger.v("recent places");
                AnalyticsSupport.recordMenuSelection("recent_places");
                if (this.recentPlacesMenu == null) {
                    this.recentPlacesMenu = new RecentPlacesMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                this.presenter.loadMenu(this.recentPlacesMenu, MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            default:
                launchDestination((Model) this.cachedList.get(selection.pos));
                break;
        }
        return true;
    }

    public Menu getType() {
        return Menu.PLACES;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    public static Model buildModel(Destination destination, int id) {
        String subTitle;
        String title = destination.destinationTitle;
        if (destination.recentTimeLabel != null) {
            subTitle = destination.recentTimeLabel;
        } else {
            subTitle = destination.destinationSubtitle;
        }
        return getPlaceModel(destination, id, title, subTitle);
    }

    public static Model getPlaceModel(Destination destination, int id, String title, String subTitle) {
        if (destination == null) {
            return IconBkColorViewHolder.buildModel(id, R.drawable.icon_mm_places_2, placesColor, MainMenu.bkColorUnselected, placesColor, title, subTitle);
        }
        switch (destination.favoriteDestinationType) {
            case FAVORITE_HOME:
                return IconBkColorViewHolder.buildModel(id, R.drawable.icon_place_home_2, homeColor, MainMenu.bkColorUnselected, homeColor, title, subTitle);
            case FAVORITE_WORK:
                return IconBkColorViewHolder.buildModel(id, R.drawable.icon_place_work_2, workColor, MainMenu.bkColorUnselected, workColor, title, subTitle);
            case FAVORITE_CONTACT:
                Model model = IconsTwoViewHolder.buildModel(id, R.drawable.icon_user_bg_1, 0, contactColor, MainMenu.bkColorUnselected, title, subTitle);
                model.extras = new HashMap();
                model.extras.put(Model.INITIALS, destination.initials);
                return model;
            default:
                if (destination.placeCategory == PlaceCategory.SUGGESTED_RECENT) {
                    return IconBkColorViewHolder.buildModel(id, R.drawable.icon_place_recent_2, recentColor, MainMenu.bkColorUnselected, recentColor, title, subTitle);
                }
                switch (destination.favoriteDestinationType) {
                    case FAVORITE_CUSTOM:
                        return IconBkColorViewHolder.buildModel(id, R.drawable.icon_place_favorite_2, favColor, MainMenu.bkColorUnselected, favColor, title, subTitle);
                    case FAVORITE_CALENDAR:
                        return IconsTwoViewHolder.buildModel(id, R.drawable.icon_place_calendar, R.drawable.icon_place_calendar_sm, homeColor, -1, title, subTitle);
                    default:
                        if (destination.destinationType == DestinationType.FIND_GAS) {
                            return IconBkColorViewHolder.buildModel(id, R.drawable.icon_places_gas_2, gasColor, MainMenu.bkColorUnselected, gasColor, title, subTitle);
                        } else if (destination.recommendation || destination.placeCategory != PlaceCategory.RECENT) {
                            return IconBkColorViewHolder.buildModel(id, R.drawable.icon_place_favorite_2, favColor, MainMenu.bkColorUnselected, favColor, title, subTitle);
                        } else {
                            return IconBkColorViewHolder.buildModel(id, R.drawable.icon_place_recent_2, recentColor, MainMenu.bkColorUnselected, recentColor, title, subTitle);
                        }
                }
        }
    }

    void launchDestination(Model m) {
        if (HereMapsManager.getInstance().isInitialized()) {
            final Model model = new Model(m);
            HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
            if (!hereNavigationManager.isNavigationModeOn() || hereNavigationManager.hasArrived()) {
                sLogger.v("called requestNavigation");
                this.presenter.performSelectionAnimation(new Runnable() {
                    public void run() {
                        PlacesMenu.this.presenter.close(new Runnable() {
                            public void run() {
                                DestinationsManager.getInstance().requestNavigation((Destination) model.state);
                            }
                        });
                    }
                }, 1000);
                return;
            }
            final Destination destination = model.state;
            ConfirmationLayout confirmationLayout = this.presenter.getConfirmationLayout();
            if (confirmationLayout == null) {
                sLogger.v("confirmation layout not found");
                return;
            }
            DestinationConfirmationHelper.configure(confirmationLayout, model, destination);
            confirmationLayout.setChoices(CONFIRMATION_CHOICES, 0, new IListener() {
                public void executeItem(int pos, int id) {
                    ConfirmationLayout confirmationLayout = PlacesMenu.this.presenter.getConfirmationLayout();
                    if (confirmationLayout == null) {
                        PlacesMenu.sLogger.v("confirmation layout not found");
                        return;
                    }
                    switch (pos) {
                        case 0:
                            PlacesMenu.sLogger.v("called requestNavigation");
                            PlacesMenu.this.presenter.close(new Runnable() {
                                public void run() {
                                    DestinationsManager.getInstance().requestNavigation(destination);
                                }
                            });
                            return;
                        case 1:
                            confirmationLayout.setVisibility(8);
                            PlacesMenu.this.presenter.reset();
                            PlacesMenu.this.vscrollComponent.verticalList.unlock();
                            return;
                        default:
                            return;
                    }
                }

                public void itemSelected(int pos, int id) {
                }
            });
            confirmationLayout.setVisibility(0);
            return;
        }
        sLogger.w("Here maps engine not initialized, exit");
        this.presenter.performSelectionAnimation(new Runnable() {
            public void run() {
                PlacesMenu.this.presenter.close();
                MapsNotification.showMapsEngineNotInitializedToast();
            }
        });
    }
}
