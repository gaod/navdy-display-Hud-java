package com.navdy.hud.app.ui.component.vlist.viewholder;

import kotlin.Metadata;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 6})
/* compiled from: SwitchViewHolder.kt */
final class SwitchViewHolder$fluctuatorRunnable$1 implements Runnable {
    final /* synthetic */ SwitchViewHolder this$0;

    SwitchViewHolder$fluctuatorRunnable$1(SwitchViewHolder switchViewHolder) {
        this.this$0 = switchViewHolder;
    }

    public final void run() {
        this.this$0.startFluctuator();
    }
}
