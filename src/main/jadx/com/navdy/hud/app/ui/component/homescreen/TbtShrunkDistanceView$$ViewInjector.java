package com.navdy.hud.app.ui.component.homescreen;

import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class TbtShrunkDistanceView$$ViewInjector {
    public static void inject(Finder finder, TbtShrunkDistanceView target, Object source) {
        target.tbtShrunkDistance = (TextView) finder.findRequiredView(source, R.id.tbtShrunkDistance, "field 'tbtShrunkDistance'");
        target.nowShrunkIcon = finder.findRequiredView(source, R.id.now_shrunk_icon, "field 'nowShrunkIcon'");
        target.progressBarShrunk = (ProgressBar) finder.findRequiredView(source, R.id.progress_bar_shrunk, "field 'progressBarShrunk'");
    }

    public static void reset(TbtShrunkDistanceView target) {
        target.tbtShrunkDistance = null;
        target.nowShrunkIcon = null;
        target.progressBarShrunk = null;
    }
}
