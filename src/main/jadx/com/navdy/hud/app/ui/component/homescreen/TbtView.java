package com.navdy.hud.app.ui.component.homescreen;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.AnimatorSet.Builder;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.support.annotation.Nullable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

public class TbtView extends RelativeLayout implements IHomeScreenLifecycle {
    static final long MANEUVER_PROGRESS_ANIMATION_DURATION = 250;
    private static final long MANEUVER_SWAP_DURATION = 500;
    private static final TimeInterpolator maneuverSwapInterpolator = new AccelerateDecelerateInterpolator();
    @InjectView(R.id.activeMapDistance)
    TextView activeMapDistance;
    @InjectView(R.id.activeMapIcon)
    ImageView activeMapIcon;
    @InjectView(R.id.activeMapInstruction)
    TextView activeMapInstruction;
    private Bus bus;
    private HomeScreenView homeScreenView;
    private long initialProgressBarDistance;
    private String lastDistance;
    private ManeuverDisplay lastEvent;
    private ManeuverState lastManeuverState;
    private CustomAnimationMode lastMode;
    private String lastRoadText;
    private int lastTurnIconId;
    private String lastTurnText;
    private Logger logger;
    @InjectView(R.id.nextMapIcon)
    ImageView nextMapIcon;
    @InjectView(R.id.nextMapIconContainer)
    View nextMapIconContainer;
    @InjectView(R.id.nextMapInstruction)
    TextView nextMapInstruction;
    @InjectView(R.id.nextMapInstructionContainer)
    View nextMapInstructionContainer;
    @InjectView(R.id.now_icon)
    View nowIcon;
    private boolean paused;
    @InjectView(R.id.progress_bar)
    ProgressBar progressBar;
    private UIStateManager uiStateManager;

    public TbtView(Context context) {
        this(context, null);
    }

    public TbtView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TbtView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.lastTurnIconId = -1;
    }

    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        RemoteDeviceManager remoteDeviceManager = RemoteDeviceManager.getInstance();
        this.bus = remoteDeviceManager.getBus();
        this.uiStateManager = remoteDeviceManager.getUiStateManager();
        setLayoutTransition(new LayoutTransition());
    }

    public void init(HomeScreenView homeScreenView) {
        this.homeScreenView = homeScreenView;
        this.bus.register(this);
    }

    public void clearState() {
        this.lastTurnIconId = -1;
        this.lastDistance = null;
        this.lastTurnText = null;
        this.lastRoadText = null;
        cleanInstructions();
    }

    public void setView(CustomAnimationMode mode) {
        this.lastMode = mode;
        MarginLayoutParams margins = (MarginLayoutParams) this.activeMapInstruction.getLayoutParams();
        MarginLayoutParams nextMargins = (MarginLayoutParams) this.nextMapInstructionContainer.getLayoutParams();
        switch (mode) {
            case EXPAND:
                if (this.lastManeuverState == ManeuverState.NOW) {
                    this.nowIcon.setScaleX(1.0f);
                    this.nowIcon.setScaleY(1.0f);
                    this.nowIcon.setAlpha(1.0f);
                }
                this.activeMapDistance.setX((float) HomeScreenResourceValues.activeRoadInfoDistanceX);
                this.activeMapIcon.setX((float) HomeScreenResourceValues.activeRoadInfoIconX);
                this.nextMapIconContainer.setX((float) HomeScreenResourceValues.activeRoadInfoIconX);
                this.activeMapInstruction.setX((float) HomeScreenResourceValues.activeRoadInfoInstructionX);
                this.nextMapInstructionContainer.setX((float) HomeScreenResourceValues.activeRoadInfoInstructionX);
                margins.width = HomeScreenResourceValues.activeRoadInfoInstructionWidth;
                nextMargins.width = HomeScreenResourceValues.activeRoadInfoInstructionWidth;
                return;
            case SHRINK_LEFT:
                this.nowIcon.setScaleX(0.9f);
                this.nowIcon.setScaleY(0.9f);
                this.nowIcon.setAlpha(0.0f);
                this.activeMapDistance.setX((float) HomeScreenResourceValues.activeRoadInfoDistanceX);
                this.activeMapIcon.setX((float) HomeScreenResourceValues.activeRoadInfoIconShrinkLeftX);
                this.nextMapIconContainer.setX((float) HomeScreenResourceValues.activeRoadInfoIconShrinkLeftX);
                this.activeMapInstruction.setX((float) HomeScreenResourceValues.activeRoadInfoInstructionShrinkLeftX);
                this.nextMapInstructionContainer.setX((float) HomeScreenResourceValues.activeRoadInfoInstructionShrinkLeftX);
                nextMargins = (MarginLayoutParams) this.nextMapInstructionContainer.getLayoutParams();
                ((MarginLayoutParams) this.activeMapInstruction.getLayoutParams()).width = HomeScreenResourceValues.activeRoadInfoInstructionShrinkWidth;
                nextMargins.width = HomeScreenResourceValues.activeRoadInfoInstructionShrinkWidth;
                return;
            default:
                return;
        }
    }

    public void getCustomAnimator(CustomAnimationMode mode, Builder mainBuilder) {
        this.lastMode = mode;
        AnimatorSet animatorSet;
        Builder builder;
        ObjectAnimator activeInstructionXAnimator;
        ValueAnimator activeInstructionWidthAnimator;
        ObjectAnimator nextInstructionXAnimator;
        ValueAnimator nextInstructionWidthAnimator;
        switch (mode) {
            case EXPAND:
                animatorSet = new AnimatorSet();
                builder = animatorSet.play(HomeScreenUtils.getXPositionAnimator(this.activeMapIcon, (float) HomeScreenResourceValues.activeRoadInfoIconX));
                builder.with(HomeScreenUtils.getXPositionAnimator(this.nextMapIconContainer, (float) HomeScreenResourceValues.activeRoadInfoIconX));
                activeInstructionXAnimator = HomeScreenUtils.getXPositionAnimator(this.activeMapInstruction, (float) HomeScreenResourceValues.activeRoadInfoInstructionX);
                activeInstructionWidthAnimator = HomeScreenUtils.getWidthAnimator(this.activeMapInstruction, HomeScreenResourceValues.activeRoadInfoInstructionWidth);
                builder.with(activeInstructionXAnimator);
                builder.with(activeInstructionWidthAnimator);
                nextInstructionXAnimator = HomeScreenUtils.getXPositionAnimator(this.nextMapInstructionContainer, (float) HomeScreenResourceValues.activeRoadInfoInstructionX);
                nextInstructionWidthAnimator = HomeScreenUtils.getWidthAnimator(this.nextMapInstructionContainer, HomeScreenResourceValues.activeRoadInfoInstructionWidth);
                builder.with(nextInstructionXAnimator);
                builder.with(nextInstructionWidthAnimator);
                if (this.lastManeuverState == ManeuverState.NOW) {
                    builder.with(HomeScreenUtils.getFadeInAndScaleUpAnimator(this.nowIcon));
                } else {
                    builder.with(HomeScreenUtils.getAlphaAnimator(this.activeMapDistance, 1));
                }
                animatorSet.addListener(new DefaultAnimationListener() {
                    public void onAnimationStart(Animator animation) {
                    }

                    public void onAnimationEnd(Animator animation) {
                        if (TbtView.this.homeScreenView.isNavigationActive() && !TbtView.this.homeScreenView.isRecalculating() && TbtView.this.lastManeuverState != ManeuverState.NOW) {
                            TbtView.this.progressBar.setVisibility(0);
                            TbtView.this.activeMapDistance.setVisibility(0);
                        }
                    }
                });
                mainBuilder.with(animatorSet);
                return;
            case SHRINK_LEFT:
                animatorSet = new AnimatorSet();
                builder = animatorSet.play(HomeScreenUtils.getXPositionAnimator(this.activeMapIcon, (float) HomeScreenResourceValues.activeRoadInfoIconShrinkLeftX));
                builder.with(HomeScreenUtils.getXPositionAnimator(this.nextMapIconContainer, (float) HomeScreenResourceValues.activeRoadInfoIconShrinkLeftX));
                activeInstructionXAnimator = HomeScreenUtils.getXPositionAnimator(this.activeMapInstruction, (float) HomeScreenResourceValues.activeRoadInfoInstructionShrinkLeftX);
                activeInstructionWidthAnimator = HomeScreenUtils.getWidthAnimator(this.activeMapInstruction, HomeScreenResourceValues.activeRoadInfoInstructionShrinkWidth);
                builder.with(activeInstructionXAnimator);
                builder.with(activeInstructionWidthAnimator);
                nextInstructionXAnimator = HomeScreenUtils.getXPositionAnimator(this.nextMapInstructionContainer, (float) HomeScreenResourceValues.activeRoadInfoInstructionShrinkLeftX);
                nextInstructionWidthAnimator = HomeScreenUtils.getWidthAnimator(this.nextMapInstructionContainer, HomeScreenResourceValues.activeRoadInfoInstructionShrinkWidth);
                builder.with(nextInstructionXAnimator);
                builder.with(nextInstructionWidthAnimator);
                if (this.lastManeuverState == ManeuverState.NOW) {
                    builder.with(HomeScreenUtils.getFadeOutAndScaleDownAnimator(this.nowIcon));
                } else {
                    builder.with(HomeScreenUtils.getAlphaAnimator(this.activeMapDistance, 0));
                }
                animatorSet.addListener(new DefaultAnimationListener() {
                    public void onAnimationStart(Animator animation) {
                    }

                    public void onAnimationEnd(Animator animation) {
                    }
                });
                mainBuilder.with(animatorSet);
                return;
            default:
                return;
        }
    }

    public void showHideActiveDistance() {
        if (this.uiStateManager.isMainUIShrunk()) {
            this.activeMapDistance.setVisibility(4);
            this.progressBar.setVisibility(4);
            return;
        }
        this.activeMapDistance.setVisibility(0);
        this.progressBar.setVisibility(0);
    }

    @Subscribe
    public void updateDisplay(ManeuverDisplay event) {
        if (!this.homeScreenView.isNavigationActive()) {
            return;
        }
        if (event.empty) {
            clearState();
        } else if (event.pendingTurn == null) {
            this.logger.v("null pending turn");
            this.lastEvent = null;
        } else {
            this.lastEvent = event;
            if (!this.paused) {
                boolean hasSameInstruction = TextUtils.equals(event.pendingTurn, this.lastTurnText) && TextUtils.equals(event.pendingRoad, this.lastRoadText);
                setDistance(event.distanceToPendingRoadText, event.maneuverState);
                setProgressBar(event.maneuverState, event.distanceInMeters);
                AnimatorSet setIcon = setIcon(event.turnIconId, hasSameInstruction);
                AnimatorSet setInstruction = setInstruction(event.pendingTurn, event.pendingRoad);
                AnimatorSet animation = null;
                if (setIcon != null && setInstruction != null) {
                    animation = new AnimatorSet();
                    animation.play(setIcon).with(setInstruction);
                } else if (setIcon != null) {
                    animation = setIcon;
                } else if (setInstruction != null) {
                    animation = setInstruction;
                }
                if (animation != null) {
                    animation.setDuration(500).setInterpolator(maneuverSwapInterpolator);
                    animation.start();
                }
                this.lastManeuverState = event.maneuverState;
            }
        }
    }

    private void cleanInstructions() {
        this.activeMapDistance.setText("");
        HomeScreenUtils.getFadeOutAndScaleDownAnimator(this.nowIcon).setDuration(250).start();
        HomeScreenUtils.setWidth(this.progressBar, 0);
        this.activeMapIcon.setImageDrawable(null);
        this.nextMapIcon.setImageDrawable(null);
        this.activeMapInstruction.setText("");
        this.nextMapInstruction.setText("");
    }

    @Nullable
    private AnimatorSet setIcon(int imageResourceId, boolean hasSameInstruction) {
        if (this.lastTurnIconId == imageResourceId) {
            return null;
        }
        this.lastTurnIconId = imageResourceId;
        if (imageResourceId == -1) {
            this.activeMapIcon.setImageDrawable(null);
            return null;
        }
        this.nextMapIcon.setImageResource(imageResourceId);
        if (!hasSameInstruction) {
            return animateSetIcon(imageResourceId);
        }
        this.activeMapIcon.setImageResource(imageResourceId);
        return null;
    }

    @Nullable
    private AnimatorSet animateSetIcon(final int imageResourceId) {
        this.nextMapIconContainer.setVisibility(0);
        ObjectAnimator sendActiveDownAnim = HomeScreenUtils.getTranslationYPositionAnimator(this.activeMapIcon, HomeScreenResourceValues.activeRoadInfoInstructionTranslationYAnimation);
        ObjectAnimator sendNextDownAnim = HomeScreenUtils.getTranslationYPositionAnimator(this.nextMapIconContainer, HomeScreenResourceValues.activeRoadInfoInstructionTranslationYAnimation);
        sendActiveDownAnim.addListener(new AnimatorListener() {
            public void onAnimationStart(Animator animator) {
            }

            public void onAnimationEnd(Animator animator) {
                TbtView.this.activeMapIcon.setImageResource(imageResourceId);
                TbtView.this.activeMapIcon.setTranslationY(0.0f);
                TbtView.this.nextMapIconContainer.setTranslationY(0.0f);
                TbtView.this.nextMapIconContainer.setVisibility(8);
            }

            public void onAnimationCancel(Animator animator) {
            }

            public void onAnimationRepeat(Animator animator) {
            }
        });
        AnimatorSet swapIcons = new AnimatorSet();
        swapIcons.play(sendActiveDownAnim).with(sendNextDownAnim);
        return swapIcons;
    }

    private void setDistance(String text, ManeuverState maneuverState) {
        if (!TextUtils.equals(text, this.lastDistance)) {
            this.lastDistance = text;
            this.activeMapDistance.setText(text);
            if (this.activeMapDistance.getAlpha() == 0.0f && this.nowIcon.getAlpha() == 0.0f && !this.uiStateManager.isMainUIShrunk()) {
                this.activeMapDistance.setAlpha(1.0f);
            }
            animateSetDistance(maneuverState);
        }
    }

    private void animateSetDistance(ManeuverState maneuverState) {
        boolean isTransitioningToNowState;
        boolean isTransitioningFromNowState;
        if (this.lastManeuverState == ManeuverState.NOW || maneuverState != ManeuverState.NOW) {
            isTransitioningToNowState = false;
        } else {
            isTransitioningToNowState = true;
        }
        if (this.lastManeuverState != ManeuverState.NOW || maneuverState == ManeuverState.NOW) {
            isTransitioningFromNowState = false;
        } else {
            isTransitioningFromNowState = true;
        }
        AnimatorSet animatorSet;
        if (isTransitioningToNowState) {
            AnimatorSet fadeInAndScaleUpAnimator = HomeScreenUtils.getFadeInAndScaleUpAnimator(this.nowIcon);
            ObjectAnimator fadeOutActiveMapDistanceAnimator = HomeScreenUtils.getAlphaAnimator(this.activeMapDistance, 0);
            animatorSet = new AnimatorSet();
            animatorSet.play(fadeInAndScaleUpAnimator).with(fadeOutActiveMapDistanceAnimator);
            animatorSet.setDuration(250).start();
        } else if (isTransitioningFromNowState) {
            AnimatorSet fadeOutAndScaleDownAnimator = HomeScreenUtils.getFadeOutAndScaleDownAnimator(this.nowIcon);
            ObjectAnimator fadeInActiveMapDistanceAnimator = HomeScreenUtils.getAlphaAnimator(this.activeMapDistance, 1);
            animatorSet = new AnimatorSet();
            animatorSet.play(fadeOutAndScaleDownAnimator).with(fadeInActiveMapDistanceAnimator);
            animatorSet.setDuration(250).start();
        }
    }

    private AnimatorSet setInstruction(String turnText, String roadText) {
        if (TextUtils.equals(turnText, this.lastTurnText) && TextUtils.equals(roadText, this.lastRoadText)) {
            return null;
        }
        this.lastTurnText = turnText;
        this.lastRoadText = roadText;
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        if (this.lastTurnText != null && HereManeuverDisplayBuilder.canShowTurnText(this.lastTurnText)) {
            spannableStringBuilder.append(this.lastTurnText);
            spannableStringBuilder.append(" ");
        }
        if (this.lastRoadText != null) {
            spannableStringBuilder.setSpan(new StyleSpan(1), 0, spannableStringBuilder.length(), 33);
            spannableStringBuilder.append(this.lastRoadText);
        }
        return animateSetInstruction(spannableStringBuilder);
    }

    private AnimatorSet animateSetInstruction(final SpannableStringBuilder spannableStringBuilder) {
        this.nextMapInstructionContainer.setVisibility(0);
        this.nextMapInstruction.setText(spannableStringBuilder);
        ObjectAnimator sendActiveDownAnim = HomeScreenUtils.getTranslationYPositionAnimator(this.activeMapInstruction, HomeScreenResourceValues.activeRoadInfoInstructionTranslationYAnimation);
        ObjectAnimator sendNextDownAnim = HomeScreenUtils.getTranslationYPositionAnimator(this.nextMapInstructionContainer, HomeScreenResourceValues.activeRoadInfoInstructionTranslationYAnimation);
        sendActiveDownAnim.addListener(new AnimatorListener() {
            public void onAnimationStart(Animator animator) {
            }

            public void onAnimationEnd(Animator animator) {
                TbtView.this.activeMapInstruction.setText(spannableStringBuilder);
                TbtView.this.activeMapInstruction.setTranslationY(0.0f);
                TbtView.this.nextMapInstructionContainer.setTranslationY(0.0f);
                TbtView.this.nextMapInstructionContainer.setVisibility(8);
            }

            public void onAnimationCancel(Animator animator) {
            }

            public void onAnimationRepeat(Animator animator) {
            }
        });
        AnimatorSet swapInstructions = new AnimatorSet();
        swapInstructions.play(sendActiveDownAnim).with(sendNextDownAnim);
        return swapInstructions;
    }

    private void setProgressBar(ManeuverState maneuverState, long distanceInMeters) {
        boolean isTransitioningToApproachingState;
        boolean isTransitioningFromApproachingState;
        if (this.lastManeuverState == ManeuverState.SOON || maneuverState != ManeuverState.SOON) {
            isTransitioningToApproachingState = false;
        } else {
            isTransitioningToApproachingState = true;
        }
        if (this.lastManeuverState != ManeuverState.SOON || maneuverState == ManeuverState.SOON) {
            isTransitioningFromApproachingState = false;
        } else {
            isTransitioningFromApproachingState = true;
        }
        if (isTransitioningFromApproachingState) {
            HomeScreenUtils.setWidth(this.progressBar, 0);
            this.initialProgressBarDistance = 0;
        } else if (isTransitioningToApproachingState) {
            HomeScreenUtils.setWidth(this.progressBar, HomeScreenResourceValues.activeRoadProgressBarWidth);
            this.initialProgressBarDistance = distanceInMeters;
        } else if (maneuverState == ManeuverState.SOON) {
            HomeScreenUtils.getProgressBarAnimator(this.progressBar, (int) (100.0d * (1.0d - (((double) distanceInMeters) / ((double) this.initialProgressBarDistance))))).setDuration(250).start();
        }
    }

    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.logger.v("::onPause:tbt");
        }
    }

    public void onResume() {
        if (this.paused) {
            this.paused = false;
            this.logger.v("::onResume:tbt");
            ManeuverDisplay event = this.lastEvent;
            if (event != null) {
                this.lastEvent = null;
                this.logger.v("::onResume:tbt updated last event");
                updateDisplay(event);
            }
        }
    }
}
