package com.navdy.hud.app.ui.component.homescreen;

import android.animation.Animator;
import android.animation.AnimatorSet.Builder;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;

public class NoLocationView extends RelativeLayout {
    private Bus bus;
    private Logger logger;
    private ObjectAnimator noLocationAnimator;
    @InjectView(R.id.noLocationImage)
    ImageView noLocationImage;
    @InjectView(R.id.noLocationTextContainer)
    LinearLayout noLocationTextContainer;

    public NoLocationView(Context context) {
        this(context, null);
    }

    public NoLocationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NoLocationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        this.bus = RemoteDeviceManager.getInstance().getBus();
        this.bus.register(this);
    }

    public boolean isAcquiringLocation() {
        return getVisibility() == 0;
    }

    public void hideLocationUI() {
        this.logger.v("hideLocationUI");
        setVisibility(8);
        if (this.noLocationAnimator != null) {
            this.logger.v("cancelled animation");
            this.noLocationAnimator.cancel();
            this.noLocationImage.setRotation(0.0f);
        }
    }

    public void showLocationUI() {
        this.logger.v("showLocationUI");
        setVisibility(0);
        if (this.noLocationAnimator == null) {
            this.noLocationAnimator = ObjectAnimator.ofFloat(this.noLocationImage, View.ROTATION, new float[]{360.0f});
            this.noLocationAnimator.setDuration(500);
            this.noLocationAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
            this.noLocationAnimator.addListener(new DefaultAnimationListener() {
                public void onAnimationEnd(Animator animation) {
                    if (NoLocationView.this.getVisibility() == 0) {
                        NoLocationView.this.noLocationAnimator.setStartDelay(33);
                        NoLocationView.this.noLocationAnimator.start();
                    }
                }
            });
        }
        this.noLocationAnimator.start();
    }

    public void setView(CustomAnimationMode mode) {
        switch (mode) {
            case EXPAND:
                this.noLocationImage.setX((float) HomeScreenResourceValues.noLocationImageX);
                this.noLocationTextContainer.setX((float) HomeScreenResourceValues.noLocationTextX);
                return;
            case SHRINK_LEFT:
                this.noLocationImage.setX((float) HomeScreenResourceValues.noLocationImageShrinkLeftX);
                this.noLocationTextContainer.setX((float) HomeScreenResourceValues.noLocationTextShrinkLeftX);
                return;
            default:
                return;
        }
    }

    public void getCustomAnimator(CustomAnimationMode mode, Builder builder) {
        switch (mode) {
            case EXPAND:
                builder.with(HomeScreenUtils.getXPositionAnimator(this.noLocationImage, (float) HomeScreenResourceValues.noLocationImageX));
                builder.with(HomeScreenUtils.getXPositionAnimator(this.noLocationTextContainer, (float) HomeScreenResourceValues.noLocationTextX));
                return;
            case SHRINK_LEFT:
                builder.with(HomeScreenUtils.getXPositionAnimator(this.noLocationImage, (float) HomeScreenResourceValues.noLocationImageShrinkLeftX));
                builder.with(HomeScreenUtils.getXPositionAnimator(this.noLocationTextContainer, (float) HomeScreenResourceValues.noLocationTextShrinkLeftX));
                return;
            default:
                return;
        }
    }
}
