package com.navdy.hud.app.ui.component.mainmenu;

import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Route;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.contacts.Contact;
import com.navdy.hud.app.framework.contacts.ContactImageHelper;
import com.navdy.hud.app.framework.contacts.NumberType;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.ArrivalEvent;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.maps.MapEvents.ManeuverEvent;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.maps.util.DistanceConverter;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.TitleViewHolder;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.util.PhoneUtil;
import com.navdy.hud.app.util.ReportIssueService;
import com.navdy.hud.app.view.drawable.ETAProgressDrawable;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.contacts.PhoneNumber;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.navigation.NavigationSessionRouteChange;
import com.navdy.service.library.events.navigation.NavigationTurn;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.service.library.events.preferences.NavigationPreferences.Builder;
import com.navdy.service.library.events.preferences.NavigationPreferencesUpdate;
import com.navdy.service.library.events.settings.DateTimeConfiguration.Clock;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class ActiveTripMenu implements IMenu {
    private static final int ARRIVAL_EXPAND = 100;
    private static final int MIN_MANEUVER_DISTANCE = 100;
    private static final int TITLE_UPDATE_INTERVAL = 1000;
    private static final String TOWARDS_PATTERN = (HereManeuverDisplayBuilder.TOWARDS + " ");
    private static final String activeTripTitle = resources.getString(R.string.mm_active_trip);
    private static final String arriveTitle = resources.getString(R.string.mm_active_trip_arrive);
    private static final String arrivedDistance = resources.getString(R.string.mm_active_trip_arrived_dist);
    private static final String arrivedTitle = resources.getString(R.string.mm_active_trip_arrived_no_label);
    private static final Model back;
    private static final Model endTrip;
    private static final String feetLabel = resources.getString(R.string.unit_feet);
    private static final Handler handler = new Handler(Looper.getMainLooper());
    private static final Model infoModel = ScrollableViewHolder.buildModel(R.layout.active_trip_menu_lyt);
    private static final String kiloMetersLabel = resources.getString(R.string.unit_kilometers);
    public static final int maneuverIconSize = resources.getDimensionPixelSize(R.dimen.active_trip_maneuver_icon_size);
    private static final String metersLabel = resources.getString(R.string.unit_meters);
    private static final String milesLabel = resources.getString(R.string.unit_miles);
    private static final Model muteTbtAudio;
    private static final Model reportIssue;
    private static final Resources resources = HudApplication.getAppContext().getResources();
    private static final Logger sLogger = new Logger(ActiveTripMenu.class);
    private static final int size_18 = resources.getDimensionPixelSize(R.dimen.active_trip_18);
    private static final int size_22 = resources.getDimensionPixelSize(R.dimen.active_trip_22);
    private static final StringBuilder timeBuilder = new StringBuilder();
    private static final TimeHelper timeHelper = RemoteDeviceManager.getInstance().getTimeHelper();
    private static final Model tripManeuvers = TitleViewHolder.buildModel(resources.getString(R.string.trip_maneuvers));
    private static final int tripOptionsColor = resources.getColor(R.color.mm_active_trip);
    private static final HashSet<String> turnNotShown = new HashSet();
    private static final Model unmuteTbtAudio;
    private TextView activeTripDistance;
    private TextView activeTripDuration;
    private TextView activeTripEta;
    private View activeTripProgress;
    private TextView activeTripSubTitleView;
    private TextView activeTripTitleView;
    private boolean arrived;
    private int backSelection;
    private Bus bus;
    private List<Model> cachedList;
    private List<Contact> contactList;
    private ContactOptionsMenu contactOptionsMenu;
    private int currentSelection = -1;
    private ETAProgressDrawable etaProgressDrawable;
    private boolean getItemsCalled;
    private String label;
    private boolean maneuverSelected;
    private IMenu parent;
    private PhoneNumber phoneNumber;
    private boolean positionOnFirstManeuver;
    private Presenter presenter;
    private boolean registered;
    private ReportIssueMenu reportIssueMenu;
    private VerticalMenuComponent vscrollComponent;

    static {
        int backColor = resources.getColor(R.color.mm_back);
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, backColor, MainMenu.bkColorUnselected, backColor, resources.getString(R.string.back), null);
        String title = resources.getString(R.string.end_trip);
        int fluctuatorColor = resources.getColor(R.color.mm_options_end_trip);
        endTrip = IconBkColorViewHolder.buildModel(R.id.main_menu_options_end_trip, R.drawable.icon_active_nav_end_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.mute_tbt_2);
        fluctuatorColor = resources.getColor(R.color.mm_options_mute_tbt_audio);
        muteTbtAudio = IconBkColorViewHolder.buildModel(R.id.main_menu_options_mute_tbt, R.drawable.icon_active_nav_mute_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.unmute_tbt_2);
        fluctuatorColor = resources.getColor(R.color.mm_options_unmute_tbt_audio);
        unmuteTbtAudio = IconBkColorViewHolder.buildModel(R.id.main_menu_options_unmute_tbt, R.drawable.icon_active_nav_unmute_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.report_navigation_issue);
        fluctuatorColor = resources.getColor(R.color.mm_options_report_issue);
        reportIssue = IconBkColorViewHolder.buildModel(R.id.main_menu_options_report_issue, R.drawable.icon_options_report_issue_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        tripManeuvers.id = R.id.active_trip_maneuver_title;
        turnNotShown.add(HereManeuverDisplayBuilder.START_TURN);
        turnNotShown.add(HereManeuverDisplayBuilder.ARRIVED);
    }

    ActiveTripMenu(Bus bus, VerticalMenuComponent vscrollComponent, Presenter presenter, IMenu parent) {
        this.bus = bus;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
    }

    public List<Model> getItems() {
        int delay;
        this.getItemsCalled = true;
        this.phoneNumber = null;
        this.contactList = null;
        this.label = null;
        List<Model> list = new ArrayList();
        list.add(back);
        list.add(infoModel);
        HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
        boolean navigationOn = hereNavigationManager.isNavigationModeOn();
        if (navigationOn) {
            NavigationRouteRequest routeRequest = hereNavigationManager.getCurrentNavigationRouteRequest();
            if (!(routeRequest == null || routeRequest.requestDestination == null)) {
                List<PhoneNumber> phoneNumbers = routeRequest.requestDestination.phoneNumbers;
                if (phoneNumbers != null && phoneNumbers.size() > 0) {
                    PhoneNumber firstNumber = (PhoneNumber) phoneNumbers.get(0);
                    int callColor = resources.getColor(R.color.mm_places);
                    if (!TextUtils.isEmpty(routeRequest.label)) {
                        this.label = routeRequest.label;
                    } else if (TextUtils.isEmpty(routeRequest.streetAddress)) {
                        this.label = "";
                    } else {
                        this.label = routeRequest.streetAddress;
                    }
                    list.add(IconBkColorViewHolder.buildModel(R.id.menu_call, R.drawable.icon_mm_contacts_2, callColor, MainMenu.bkColorUnselected, callColor, resources.getString(R.string.call_person, new Object[]{this.label}), PhoneUtil.formatPhoneNumber(firstNumber.number)));
                    this.phoneNumber = firstNumber;
                } else if (routeRequest.requestDestination.contacts != null && routeRequest.requestDestination.contacts.size() > 0) {
                    this.contactList = new ArrayList(routeRequest.requestDestination.contacts.size());
                    for (com.navdy.service.library.events.contacts.Contact c : routeRequest.requestDestination.contacts) {
                        this.contactList.add(new Contact(c));
                    }
                    list.add(buildContactModel((Contact) this.contactList.get(0), this.contactList.size()));
                }
            }
            list.add(endTrip);
            if (!DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile()) {
                if (HereNavigationManager.getInstance().getNavigationSessionPreference().spokenTurnByTurn) {
                    muteTbtAudio.title = resources.getString(R.string.mute_tbt_2);
                    list.add(muteTbtAudio);
                } else {
                    unmuteTbtAudio.title = resources.getString(R.string.unmute_tbt_2);
                    list.add(unmuteTbtAudio);
                }
            }
        }
        if (navigationOn && ReportIssueService.canReportIssue()) {
            list.add(reportIssue);
        }
        if (navigationOn && !HereNavigationManager.getInstance().hasArrived()) {
            buildManeuvers(list);
        }
        this.presenter.showScrimCover();
        this.cachedList = list;
        sLogger.v("getItems:" + list.size());
        if (this.presenter.isMapShown()) {
            this.presenter.enableMapViews();
            delay = 100;
        } else {
            this.presenter.showMap();
            delay = 1000;
        }
        handler.postDelayed(new Runnable() {
            public void run() {
                if (ActiveTripMenu.this.getItemsCalled && ActiveTripMenu.this.presenter.isMapShown()) {
                    ActiveTripMenu.this.presenter.setViewBackgroundColor(0);
                    ActiveTripMenu.this.presenter.cleanMapFluctuator();
                }
            }
        }, (long) delay);
        return list;
    }

    public int getInitialSelection() {
        if (this.positionOnFirstManeuver) {
            this.positionOnFirstManeuver = false;
            if (this.cachedList != null) {
                int index = getFirstManeuverIndex();
                if (index > 0) {
                    sLogger.v("initialSelection-m:" + index);
                    return index;
                }
            }
        }
        if (this.currentSelection == -1) {
            return 1;
        }
        sLogger.v("initialSelection:" + this.currentSelection);
        int ret = this.currentSelection;
        this.currentSelection = -1;
        return ret;
    }

    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
    }

    public void setSelectedIcon() {
        int color = tripOptionsColor;
        String title = activeTripTitle;
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_badge_active_trip, color, null, 1.0f);
        this.vscrollComponent.selectedText.setText(title);
    }

    public boolean selectItem(ItemSelectionState selection) {
        sLogger.v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case R.id.active_trip_contact:
                sLogger.v("contact");
                if (this.contactOptionsMenu == null) {
                    this.contactOptionsMenu = new ContactOptionsMenu(this.contactList, this.vscrollComponent, this.presenter, this, this.bus);
                    this.contactOptionsMenu.setScrollModel(true);
                }
                this.presenter.loadMenu(this.contactOptionsMenu, MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            case R.id.main_menu_options_end_trip:
                sLogger.v("end trip");
                if (this.presenter.isMapShown()) {
                    this.presenter.setNavEnded();
                    this.presenter.setViewBackgroundColor(-16777216);
                }
                AnalyticsSupport.recordMenuSelection("End_Trip");
                this.presenter.performSelectionAnimation(new Runnable() {
                    public void run() {
                        ActiveTripMenu.this.presenter.close();
                        TaskManager.getInstance().execute(new Runnable() {
                            public void run() {
                                HereNavigationManager.getInstance().stopAndRemoveAllRoutes();
                            }
                        }, 3);
                    }
                });
                break;
            case R.id.main_menu_options_mute_tbt:
                sLogger.v("mute tbt");
                if (this.presenter.isMapShown()) {
                    this.presenter.setViewBackgroundColor(-16777216);
                }
                AnalyticsSupport.recordOptionSelection("mute_Turn_By_Turn");
                this.presenter.performSelectionAnimation(new Runnable() {
                    public void run() {
                        ActiveTripMenu.this.presenter.close();
                    }
                }, 1000);
                muteTbtAudio.title = resources.getString(R.string.navigation_muted);
                this.vscrollComponent.verticalList.unlock(false);
                this.presenter.refreshDataforPos(selection.pos, false);
                this.vscrollComponent.verticalList.lock();
                setSpokenTurnByTurn(false);
                break;
            case R.id.main_menu_options_report_issue:
                sLogger.v("report-issue");
                AnalyticsSupport.recordMenuSelection("report-issue");
                if (this.reportIssueMenu == null) {
                    this.reportIssueMenu = new ReportIssueMenu(this.vscrollComponent, this.presenter, this);
                }
                this.presenter.loadMenu(this.reportIssueMenu, MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            case R.id.main_menu_options_unmute_tbt:
                sLogger.v("unmute tbt");
                if (this.presenter.isMapShown()) {
                    this.presenter.setViewBackgroundColor(-16777216);
                }
                AnalyticsSupport.recordOptionSelection("unmute_Turn_By_Turn");
                this.presenter.performSelectionAnimation(new Runnable() {
                    public void run() {
                        ActiveTripMenu.this.presenter.close();
                    }
                }, 1000);
                unmuteTbtAudio.title = resources.getString(R.string.navigation_unmuted);
                this.vscrollComponent.verticalList.unlock(false);
                this.presenter.refreshDataforPos(selection.pos, false);
                this.vscrollComponent.verticalList.lock();
                setSpokenTurnByTurn(true);
                break;
            case R.id.menu_back:
                sLogger.v("back");
                if (this.presenter.isMapShown()) {
                    this.presenter.setViewBackgroundColor(-16777216);
                }
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, 0, false, IconOptionsViewHolder.getOffSetFromPos(((MainMenu) this.parent).getActivityTraySelection()));
                break;
            case R.id.menu_call:
                sLogger.v("call place");
                if (this.presenter.isMapShown()) {
                    this.presenter.setViewBackgroundColor(-16777216);
                }
                this.presenter.performSelectionAnimation(new Runnable() {
                    public void run() {
                        ActiveTripMenu.this.presenter.close(new Runnable() {
                            public void run() {
                                if (ActiveTripMenu.this.phoneNumber != null) {
                                    RemoteDeviceManager.getInstance().getCallManager().dial(ActiveTripMenu.this.phoneNumber.number, null, ActiveTripMenu.this.label);
                                }
                            }
                        });
                    }
                });
                break;
        }
        return true;
    }

    private void setSpokenTurnByTurn(boolean enabled) {
        HereNavigationManager.getInstance().getNavigationSessionPreference().spokenTurnByTurn = enabled;
        DriverProfile userProfile = DriverProfileHelper.getInstance().getCurrentProfile();
        if (!userProfile.isDefaultProfile()) {
            NavigationPreferences updatedPreferences = new Builder(userProfile.getNavigationPreferences()).spokenTurnByTurn(Boolean.valueOf(enabled)).build();
            NavigationPreferencesUpdate update = new NavigationPreferencesUpdate.Builder().status(RequestStatus.REQUEST_SUCCESS).serial_number(updatedPreferences.serial_number).preferences(updatedPreferences).build();
            this.bus.post(update);
            this.bus.post(new RemoteEvent(update));
        }
    }

    public Menu getType() {
        return Menu.ACTIVE_TRIP;
    }

    public boolean isItemClickable(int id, int pos) {
        if (pos == 1) {
            return false;
        }
        Model m = getModelfromPos(pos);
        if (m != null) {
            switch (m.id) {
                case R.id.active_trip_maneuver:
                case R.id.active_trip_maneuver_title:
                    return false;
            }
        }
        return true;
    }

    public Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return true;
    }

    public void onBindToView(Model model, View view, int pos, ModelState state) {
        if (model.type == ModelType.SCROLL_CONTENT) {
            ViewGroup viewGroup = (ViewGroup) ((ViewGroup) view).getChildAt(0);
            this.activeTripTitleView = (TextView) viewGroup.findViewById(R.id.active_trip_title);
            this.activeTripSubTitleView = (TextView) viewGroup.findViewById(R.id.active_trip_subtitle);
            this.activeTripDuration = (TextView) viewGroup.findViewById(R.id.active_trip_duration);
            this.activeTripDistance = (TextView) viewGroup.findViewById(R.id.active_trip_distance);
            this.activeTripEta = (TextView) viewGroup.findViewById(R.id.active_trip_eta);
            this.activeTripProgress = viewGroup.findViewById(R.id.active_trip_progress);
            this.etaProgressDrawable = new ETAProgressDrawable(this.activeTripProgress.getContext());
            this.etaProgressDrawable.setMinValue(0.0f);
            this.etaProgressDrawable.setMaxGaugeValue(100.0f);
            this.activeTripProgress.setBackground(this.etaProgressDrawable);
            update();
            if (!this.registered) {
                this.bus.register(this);
                this.registered = true;
                sLogger.v("registered bus");
            }
            HereNavigationManager.getInstance().postManeuverDisplay();
        }
    }

    public IMenu getChildMenu(IMenu parent, String args, String path) {
        return null;
    }

    public void onUnload(MenuLevel level) {
        this.getItemsCalled = false;
        this.presenter.hideScrimCover();
        if (this.presenter.isMapShown()) {
            this.presenter.setViewBackgroundColor(-16777216);
            this.presenter.disableMapViews();
        }
        if (this.registered) {
            this.registered = false;
            this.bus.unregister(this);
            sLogger.v("unregistered bus");
        }
        this.activeTripTitleView = null;
        this.activeTripSubTitleView = null;
        this.activeTripDuration = null;
        this.activeTripDistance = null;
        this.activeTripEta = null;
        this.activeTripProgress = null;
    }

    public void onItemSelected(ItemSelectionState selection) {
        if (selection.id == R.id.active_trip_maneuver) {
            this.maneuverSelected = true;
            Model model = getModelfromPos(selection.pos);
            if (model != null && model.id == R.id.active_trip_maneuver && (model.state instanceof GeoBoundingBox)) {
                this.presenter.showBoundingBox((GeoBoundingBox) model.state);
            } else {
                sLogger.v("leg - no bbox");
            }
        } else if (this.maneuverSelected) {
            this.maneuverSelected = false;
            this.presenter.showBoundingBox(this.presenter.getCurrentRouteBoundingBox());
            sLogger.v("route bbox");
        }
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }

    private void update() {
        if (isMenuLoaded()) {
            HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
            String label = hereNavigationManager.getDestinationLabel();
            String address = hereNavigationManager.getFullStreetAddress();
            if (TextUtils.isEmpty(label)) {
                label = address;
                if (TextUtils.isEmpty(label)) {
                    label = null;
                } else {
                    address = null;
                }
            }
            if (label != null) {
                this.activeTripTitleView.setText(label);
                this.activeTripTitleView.setVisibility(0);
            } else {
                this.activeTripTitleView.setVisibility(8);
            }
            if (address != null) {
                this.activeTripSubTitleView.setText(address);
                this.activeTripSubTitleView.setVisibility(0);
                this.activeTripSubTitleView.requestLayout();
            } else {
                this.activeTripSubTitleView.setVisibility(8);
            }
            if (hereNavigationManager.hasArrived()) {
                switchToArriveState();
            }
        }
    }

    private boolean isMenuLoaded() {
        return this.activeTripTitleView != null;
    }

    private void fillDurationVia(Date etaDate) {
        String duration = HereMapUtil.convertDateToEta(etaDate);
        String via = HereNavigationManager.getInstance().getCurrentVia();
        if (duration != null) {
            SpannableStringBuilder durationVia = new SpannableStringBuilder();
            durationVia.append(duration);
            int len = durationVia.length();
            durationVia.setSpan(new StyleSpan(1), 0, len, 34);
            durationVia.setSpan(new AbsoluteSizeSpan(size_22), 0, len, 34);
            if (via != null) {
                durationVia.append(" ");
                int start = duration.length();
                durationVia.append(resources.getString(R.string.via_desc, new Object[]{via}));
                durationVia.setSpan(new AbsoluteSizeSpan(size_18), start, durationVia.length(), 34);
            }
            this.activeTripDuration.setText(durationVia);
            return;
        }
        this.activeTripDuration.setText("");
    }

    @Subscribe
    public void onManeuverDisplayEvent(ManeuverDisplay maneuverDisplay) {
        if (!isMenuLoaded() || maneuverDisplay.isEmpty() || !maneuverDisplay.isNavigating()) {
            return;
        }
        if (HereNavigationManager.getInstance().hasArrived()) {
            if (this.arrived) {
                sLogger.v("already arrived");
                return;
            }
            this.arrived = true;
            this.currentSelection = -1;
            this.presenter.loadMenu(this, MenuLevel.REFRESH_CURRENT, 0, 0, true);
        } else if (maneuverDisplay.etaDate != null) {
            String eta;
            int percentage;
            fillDurationVia(maneuverDisplay.etaDate);
            if (timeHelper.getFormat() == Clock.CLOCK_24_HOUR) {
                eta = maneuverDisplay.eta;
            } else {
                timeBuilder.setLength(0);
                eta = timeHelper.formatTime12Hour(maneuverDisplay.etaDate, timeBuilder, false) + " " + timeBuilder.toString();
            }
            if (eta != null) {
                this.activeTripEta.setText(eta);
            } else {
                this.activeTripEta.setText("");
            }
            float totalDistanceInMeters = DistanceConverter.convertToMeters(maneuverDisplay.totalDistance, maneuverDisplay.totalDistanceUnit);
            float coveredDistanceInMeters = totalDistanceInMeters - DistanceConverter.convertToMeters(maneuverDisplay.totalDistanceRemaining, maneuverDisplay.totalDistanceRemainingUnit);
            if (totalDistanceInMeters <= 0.0f || totalDistanceInMeters <= coveredDistanceInMeters) {
                percentage = 0;
            } else {
                percentage = (int) ((coveredDistanceInMeters / totalDistanceInMeters) * 100.0f);
            }
            if (this.etaProgressDrawable != null) {
                this.etaProgressDrawable.setGaugeValue((float) percentage);
                this.activeTripProgress.invalidate();
            }
            String unit = "";
            switch (maneuverDisplay.totalDistanceRemainingUnit) {
                case DISTANCE_METERS:
                    unit = metersLabel;
                    break;
                case DISTANCE_KMS:
                    unit = kiloMetersLabel;
                    break;
                case DISTANCE_MILES:
                    unit = milesLabel;
                    break;
                case DISTANCE_FEET:
                    unit = feetLabel;
                    break;
            }
            this.activeTripDistance.setText(Float.toString(maneuverDisplay.totalDistanceRemaining) + " " + unit);
        }
    }

    @Subscribe
    public void onRouteChangeEvent(NavigationSessionRouteChange event) {
        int curPos = this.presenter.getCurrentSelection();
        sLogger.v("onRouteChangeEvent reason=" + event.reason + " curPos=" + curPos);
        if (curPos > 1) {
            int maneuverPos = getFirstManeuverIndex();
            if (curPos >= maneuverPos) {
                this.positionOnFirstManeuver = true;
                sLogger.v("onRouteChangeEvent showing maneuver curPos=" + curPos + " maneuverPos=" + maneuverPos);
            } else {
                sLogger.v("onRouteChangeEvent not showing maneuver curPos=" + curPos + " maneuverPos=" + maneuverPos);
            }
        }
        this.currentSelection = curPos;
        this.presenter.loadMenu(this, MenuLevel.REFRESH_CURRENT, 0, 0, true);
    }

    @Subscribe
    public void arrivalEvent(ArrivalEvent event) {
        if (this.arrived) {
            sLogger.v("arrival event");
            return;
        }
        this.arrived = true;
        this.currentSelection = -1;
        this.positionOnFirstManeuver = false;
        this.presenter.loadMenu(this, MenuLevel.REFRESH_CURRENT, 0, 0, true);
    }

    @Subscribe
    public void onManeuverChangeEvent(ManeuverEvent event) {
        sLogger.v("onManeuverChangeEvent:" + event.type);
        switch (event.type) {
            case INTERMEDIATE:
                int curPos = this.presenter.getCurrentSelection();
                if (curPos > 1 && curPos >= getFirstManeuverIndex()) {
                    this.positionOnFirstManeuver = true;
                }
                this.currentSelection = curPos;
                this.presenter.loadMenu(this, MenuLevel.REFRESH_CURRENT, 0, 0, true);
                return;
            default:
                return;
        }
    }

    private void switchToArriveState() {
        if (!TextUtils.equals(this.activeTripDuration.getText(), arrivedTitle)) {
            SpannableStringBuilder durationVia = new SpannableStringBuilder();
            durationVia.append(arrivedTitle);
            int len = durationVia.length();
            durationVia.setSpan(new StyleSpan(1), 0, len, 34);
            durationVia.setSpan(new AbsoluteSizeSpan(size_22), 0, len, 34);
            this.activeTripDuration.setText(durationVia);
            this.activeTripDistance.setText(arrivedDistance);
            this.activeTripEta.setText("");
            this.etaProgressDrawable.setGaugeValue(100.0f);
        }
    }

    private void buildManeuvers(List<Model> list) {
        try {
            long l1 = SystemClock.elapsedRealtime();
            HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
            Route route = hereNavigationManager.getCurrentRoute();
            String label = hereNavigationManager.getDestinationLabel();
            String address = hereNavigationManager.getFullStreetAddress();
            if (TextUtils.isEmpty(label)) {
                label = address;
                if (TextUtils.isEmpty(label)) {
                    label = null;
                } else {
                    address = null;
                }
            }
            if (route != null) {
                int i;
                Maneuver previous;
                List<Maneuver> maneuverList = new ArrayList();
                List<Maneuver> allManeuvers = route.getManeuvers();
                int len = allManeuvers.size();
                Maneuver current = null;
                Maneuver currentNavigationManeuver = hereNavigationManager.getNavManeuver();
                if (currentNavigationManeuver == null) {
                    sLogger.v("current maneuver not found");
                }
                NavigationRouteRequest routeRequest = hereNavigationManager.getCurrentNavigationRouteRequest();
                boolean foundCurrent = false;
                list.add(tripManeuvers);
                for (i = 0; i < len; i++) {
                    previous = current;
                    current = (Maneuver) allManeuvers.get(i);
                    if (!HereMapUtil.isStartManeuver(current)) {
                        if (foundCurrent) {
                            maneuverList.add(current);
                        } else if (currentNavigationManeuver == null) {
                            maneuverList.add(current);
                        } else if (HereMapUtil.areManeuverEqual(currentNavigationManeuver, current)) {
                            maneuverList.add(current);
                            foundCurrent = true;
                        }
                    }
                }
                len = maneuverList.size();
                current = null;
                for (i = 0; i < len; i++) {
                    String roadInfo;
                    previous = current;
                    current = (Maneuver) maneuverList.get(i);
                    Maneuver after = null;
                    if (i < len - 1) {
                        after = (Maneuver) maneuverList.get(i + 1);
                    }
                    ManeuverDisplay display = HereManeuverDisplayBuilder.getManeuverDisplay(current, after == null, (long) current.getDistanceToNextManeuver(), address, previous, routeRequest, after, true, false, null);
                    if (HereManeuverDisplayBuilder.shouldShowTurnText(display.pendingTurn) && !turnNotShown.contains(display.pendingTurn)) {
                        roadInfo = display.pendingTurn + " " + display.pendingRoad;
                    } else if (display.pendingRoad.indexOf(TOWARDS_PATTERN) == 0) {
                        roadInfo = display.pendingRoad.substring(TOWARDS_PATTERN.length());
                    } else {
                        roadInfo = display.pendingRoad;
                    }
                    Model maneuverModel = IconsTwoViewHolder.buildModel(R.id.active_trip_maneuver, display.turnIconNowId, display.turnIconSoonId, 0, 0, roadInfo, display.distanceToPendingRoadText);
                    if (i == len - 1) {
                        maneuverModel.state = buildArrivalBoundingBox(routeRequest);
                    } else {
                        int distance = current.getDistanceToNextManeuver();
                        if (distance < 100) {
                            maneuverModel.state = expandBoundingBox(current.getBoundingBox(), 100 - distance);
                        } else {
                            maneuverModel.state = current.getBoundingBox();
                        }
                    }
                    maneuverModel.noTextAnimation = true;
                    maneuverModel.noImageScaleAnimation = true;
                    if (display.navigationTurn == NavigationTurn.NAV_TURN_END) {
                        maneuverModel.title = arriveTitle;
                        if (label != null && address != null) {
                            maneuverModel.title = arriveTitle;
                            maneuverModel.subTitle = label;
                            maneuverModel.subTitle2 = address;
                        } else if (label != null) {
                            maneuverModel.title = arriveTitle;
                            maneuverModel.subTitle = label;
                        } else if (address != null) {
                            maneuverModel.title = arriveTitle;
                            maneuverModel.subTitle = address;
                        } else {
                            maneuverModel.title = arriveTitle;
                        }
                    } else {
                        maneuverModel.iconSize = maneuverIconSize;
                    }
                    list.add(maneuverModel);
                }
            }
            sLogger.v("time to build maneuvers:" + (SystemClock.elapsedRealtime() - l1));
        } catch (Throwable t) {
            sLogger.e("addManeuversToList", t);
        }
    }

    private int getFirstManeuverIndex() {
        int index = this.cachedList.indexOf(reportIssue);
        if (index >= 0 && index + 2 < this.cachedList.size()) {
            return index + 2;
        }
        index = this.cachedList.indexOf(unmuteTbtAudio);
        if (index >= 0 && index + 2 < this.cachedList.size()) {
            return index + 2;
        }
        index = this.cachedList.indexOf(muteTbtAudio);
        if (index < 0 || index + 2 >= this.cachedList.size()) {
            return -1;
        }
        return index + 2;
    }

    private GeoBoundingBox buildArrivalBoundingBox(NavigationRouteRequest request) {
        GeoCoordinate coordinate = null;
        try {
            if (request.destination.latitude.doubleValue() != 0.0d && request.destination.longitude.doubleValue() != 0.0d) {
                coordinate = new GeoCoordinate(request.destination.latitude.doubleValue(), request.destination.longitude.doubleValue());
            } else if (!(request.destinationDisplay == null || request.destinationDisplay.latitude.doubleValue() == 0.0d || request.destinationDisplay.longitude.doubleValue() == 0.0d)) {
                coordinate = new GeoCoordinate(request.destinationDisplay.latitude.doubleValue(), request.destinationDisplay.longitude.doubleValue());
            }
            if (coordinate != null) {
                GeoBoundingBox bbox = new GeoBoundingBox(coordinate, coordinate);
                bbox.expand(100.0f, 100.0f);
                return bbox;
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
        return null;
    }

    private GeoBoundingBox expandBoundingBox(GeoBoundingBox bbox, int distance) {
        try {
            bbox.expand((float) distance, (float) distance);
        } catch (Throwable t) {
            sLogger.e(t);
        }
        return bbox;
    }

    Model buildContactModel(Contact c, int size) {
        Model model;
        ContactImageHelper contactImageHelper = ContactImageHelper.getInstance();
        if (TextUtils.isEmpty(c.name)) {
            model = IconsTwoViewHolder.buildModel(R.id.active_trip_contact, R.drawable.icon_user_bg_4, R.drawable.icon_user_numberonly, resources.getColor(R.color.icon_user_bg_4), -1, c.formattedNumber, null);
        } else {
            int largeImageRes = contactImageHelper.getResourceId(c.defaultImageIndex);
            int fluctuator = contactImageHelper.getResourceColor(c.defaultImageIndex);
            String subTitle = null;
            if (size == 1) {
                if (c.numberType != NumberType.OTHER) {
                    subTitle = c.numberTypeStr;
                } else {
                    subTitle = c.formattedNumber;
                }
            }
            model = IconsTwoViewHolder.buildModel(R.id.active_trip_contact, largeImageRes, R.drawable.icon_user_grey, fluctuator, -1, resources.getString(R.string.contact_person, new Object[]{c.name}), subTitle);
        }
        model.extras = new HashMap();
        model.extras.put(Model.INITIALS, c.initials);
        return model;
    }
}
