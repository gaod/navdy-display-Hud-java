package com.navdy.hud.app.ui.component.homescreen;

import android.animation.AnimatorSet.Builder;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.ConversionUtil;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.service.library.log.Logger;

public class MpgView extends LinearLayout {
    private Logger logger;
    @InjectView(R.id.txt_mpg_label)
    TextView mpgLabelTextView;
    @InjectView(R.id.txt_mpg)
    TextView mpgTextView;
    private UIStateManager uiStateManager;

    public MpgView(Context context) {
        this(context, null);
    }

    public MpgView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MpgView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        this.uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
    }

    void updateInstantaneousFuelConsumption(double consumption) {
        String mpgText = "- -";
        if (ConversionUtil.convertLpHundredKmToMPG(consumption) > 0.0d) {
            mpgText = String.format("%d", new Object[]{Integer.valueOf((int) ConversionUtil.convertLpHundredKmToMPG(consumption))});
        }
        this.mpgTextView.setText(mpgText);
    }

    public void setView(CustomAnimationMode mode) {
        switch (mode) {
            case EXPAND:
                setX((float) HomeScreenResourceValues.speedX);
                return;
            case SHRINK_LEFT:
                setX((float) HomeScreenResourceValues.speedShrinkLeftX);
                return;
            default:
                return;
        }
    }

    public void getTopAnimator(Builder builder, boolean out) {
        if (out) {
            builder.with(HomeScreenUtils.getXPositionAnimator(this, (float) (((int) getX()) + HomeScreenResourceValues.topViewSpeedOut)));
            builder.with(ObjectAnimator.ofFloat(this, View.ALPHA, new float[]{1.0f, 0.0f}));
            return;
        }
        builder.with(HomeScreenUtils.getXPositionAnimator(this, (float) (((int) getX()) - HomeScreenResourceValues.topViewSpeedOut)));
        builder.with(ObjectAnimator.ofFloat(this, View.ALPHA, new float[]{0.0f, 1.0f}));
    }

    public void resetTopViewsAnimator() {
        setAlpha(1.0f);
        setView(this.uiStateManager.getCustomAnimateMode());
    }
}
