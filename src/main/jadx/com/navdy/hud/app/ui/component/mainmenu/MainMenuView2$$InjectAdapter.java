package com.navdy.hud.app.ui.component.mainmenu;

import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class MainMenuView2$$InjectAdapter extends Binding<MainMenuView2> implements MembersInjector<MainMenuView2> {
    private Binding<Presenter> presenter;

    public MainMenuView2$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.ui.component.mainmenu.MainMenuView2", false, MainMenuView2.class);
    }

    public void attach(Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter", MainMenuView2.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
    }

    public void injectMembers(MainMenuView2 object) {
        object.presenter = (Presenter) this.presenter.get();
    }
}
