package com.navdy.hud.app.ui.component.dashboard;

public interface IDashboardOptionsAdapter {
    public static final int LEFT = 0;
    public static final String PREFERENCE_LEFT_GAUGE_ID = "PREFERENCE_LEFT_GAUGE";
    public static final String PREFERENCE_MIDDLE_GAUGE = "PREFERENCE_MIDDLE_GAUGE";
    public static final String PREFERENCE_RIGHT_GAUGE_ID = "PREFERENCE_RIGHT_GAUGE";
    public static final String PREFERENCE_SCROLLABLE_SIDE = "PREFERENCE_SCROLLABLE_SIDE";
    public static final int RIGHT = 1;
    public static final int SPEEDOMETER_GAUGE = 1;
    public static final int TACHOMETER_GAUGE = 0;

    int getCurrentScrollableSideOption();

    int getMiddleGaugeOption();

    void onScrollableSideOptionSelected();

    void onSpeedoMeterSelected();

    void onTachoMeterSelected();
}
