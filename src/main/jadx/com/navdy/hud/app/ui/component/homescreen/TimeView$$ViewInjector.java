package com.navdy.hud.app.ui.component.homescreen;

import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class TimeView$$ViewInjector {
    public static void inject(Finder finder, TimeView target, Object source) {
        target.dayTextView = (TextView) finder.findRequiredView(source, R.id.txt_day, "field 'dayTextView'");
        target.timeTextView = (TextView) finder.findRequiredView(source, R.id.txt_time, "field 'timeTextView'");
        target.ampmTextView = (TextView) finder.findRequiredView(source, R.id.txt_ampm, "field 'ampmTextView'");
    }

    public static void reset(TimeView target) {
        target.dayTextView = null;
        target.timeTextView = null;
        target.ampmTextView = null;
    }
}
