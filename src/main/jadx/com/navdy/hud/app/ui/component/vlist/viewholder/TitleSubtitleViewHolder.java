package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State;
import com.navdy.service.library.log.Logger;

public class TitleSubtitleViewHolder extends VerticalViewHolder {
    private static final Logger sLogger = VerticalViewHolder.sLogger;
    private TextView subTitle;
    private TextView title;

    public static Model buildModel(String title, String subTitle) {
        Model model = new Model();
        model.type = ModelType.TITLE_SUBTITLE;
        model.id = R.id.vlist_title_subtitle;
        model.title = title;
        model.subTitle = subTitle;
        return model;
    }

    public static TitleSubtitleViewHolder buildViewHolder(ViewGroup parent, VerticalList vlist, Handler handler) {
        return new TitleSubtitleViewHolder((ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.vlist_title_subtitle, parent, false), vlist, handler);
    }

    public TitleSubtitleViewHolder(ViewGroup layout, VerticalList vlist, Handler handler) {
        super(layout, vlist, handler);
        this.title = (TextView) layout.findViewById(R.id.title);
        this.subTitle = (TextView) layout.findViewById(R.id.subTitle);
    }

    public ModelType getModelType() {
        return ModelType.TITLE_SUBTITLE;
    }

    public void setItemState(State state, AnimationType animation, int duration, boolean startFluctuator) {
    }

    public void bind(Model model, ModelState modelState) {
        this.title.setText(model.title);
        this.subTitle.setText(model.subTitle);
    }

    public void clearAnimation() {
    }

    public void select(Model model, int pos, int duration) {
    }

    public void copyAndPosition(ImageView imageC, TextView titleC, TextView subTitleC, TextView subTitle2C, boolean setImage) {
    }
}
