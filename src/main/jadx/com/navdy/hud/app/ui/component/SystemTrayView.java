package com.navdy.hud.app.ui.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.service.library.log.Logger;
import java.util.HashMap;

public class SystemTrayView extends LinearLayout {
    static HashMap<State, Integer> dialStateResMap = new HashMap();
    static HashMap<State, Integer> phoneStateResMap = new HashMap();
    private static final Logger sLogger = new Logger(SystemTrayView.class);
    private TextView debugGpsMarker;
    private State dialBattery;
    private boolean dialConnected;
    @InjectView(R.id.dial)
    ImageView dialImageView;
    @InjectView(R.id.gps)
    ImageView locationImageView;
    private boolean noNetwork;
    private State phoneBattery;
    private boolean phoneConnected;
    @InjectView(R.id.phone)
    ImageView phoneImageView;
    @InjectView(R.id.phoneNetwork)
    ImageView phoneNetworkImageView;

    public enum Device {
        Phone,
        Dial,
        Gps
    }

    public enum State {
        CONNECTED,
        DISCONNECTED,
        LOW_BATTERY,
        EXTREMELY_LOW_BATTERY,
        OK_BATTERY,
        NO_PHONE_NETWORK,
        PHONE_NETWORK_AVAILABLE,
        LOCATION_LOST,
        LOCATION_AVAILABLE
    }

    static {
        dialStateResMap.put(State.LOW_BATTERY, Integer.valueOf(R.drawable.icon_dial_batterylow_2_copy));
        dialStateResMap.put(State.EXTREMELY_LOW_BATTERY, Integer.valueOf(R.drawable.icon_dial_batterylow_copy));
        dialStateResMap.put(State.DISCONNECTED, Integer.valueOf(R.drawable.icon_dial_missing_copy));
        phoneStateResMap.put(State.LOW_BATTERY, Integer.valueOf(R.drawable.icon_device_batterylow_copy));
        phoneStateResMap.put(State.EXTREMELY_LOW_BATTERY, Integer.valueOf(R.drawable.icon_device_batterylow_2_copy));
        phoneStateResMap.put(State.DISCONNECTED, Integer.valueOf(R.drawable.icon_device_missing_copy));
    }

    public SystemTrayView(Context context) {
        this(context, null);
    }

    public SystemTrayView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SystemTrayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.phoneBattery = State.OK_BATTERY;
        this.dialBattery = State.OK_BATTERY;
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.system_tray, this, true);
        setOrientation(0);
        ButterKnife.inject((View) this);
        this.dialImageView.setVisibility(8);
        this.phoneImageView.setVisibility(8);
        this.phoneNetworkImageView.setVisibility(8);
        this.locationImageView.setVisibility(8);
    }

    public void setState(Device device, State state) {
        switch (device) {
            case Phone:
                handlePhoneState(state);
                return;
            case Dial:
                handleDialState(state);
                return;
            case Gps:
                handleGpsState(state);
                return;
            default:
                return;
        }
    }

    public void setDebugGpsMarker(String marker) {
        if (marker == null) {
            try {
                if (this.debugGpsMarker != null) {
                    removeView(this.debugGpsMarker);
                    this.debugGpsMarker = null;
                    return;
                }
                return;
            } catch (Throwable t) {
                sLogger.e(t);
                return;
            }
        }
        if (this.debugGpsMarker == null) {
            Context context = getContext();
            this.debugGpsMarker = new TextView(context);
            this.debugGpsMarker.setTextAppearance(context, R.style.glance_title_3);
            this.debugGpsMarker.setTextColor(-1);
            LayoutParams lytParams = new LayoutParams(-2, -2);
            lytParams.gravity = 16;
            addView(this.debugGpsMarker, 0, lytParams);
        }
        this.debugGpsMarker.setText(marker);
    }

    private void handlePhoneState(State state) {
        boolean connected = true;
        if (state == null) {
            this.phoneImageView.setImageResource(0);
            this.phoneImageView.setVisibility(8);
            this.phoneNetworkImageView.setVisibility(8);
            return;
        }
        switch (state) {
            case CONNECTED:
            case DISCONNECTED:
                if (state != State.CONNECTED) {
                    connected = false;
                }
                if (this.phoneConnected != connected) {
                    this.phoneConnected = connected;
                    this.noNetwork = false;
                    this.phoneBattery = State.OK_BATTERY;
                    updateViews();
                    return;
                }
                return;
            case OK_BATTERY:
            case LOW_BATTERY:
            case EXTREMELY_LOW_BATTERY:
                if (this.phoneBattery != state) {
                    this.phoneBattery = state;
                    updateViews();
                    return;
                }
                return;
            case PHONE_NETWORK_AVAILABLE:
            case NO_PHONE_NETWORK:
                boolean noNetworkAvailable;
                if (state == State.NO_PHONE_NETWORK) {
                    noNetworkAvailable = true;
                } else {
                    noNetworkAvailable = false;
                }
                if (this.noNetwork != noNetworkAvailable) {
                    this.noNetwork = noNetworkAvailable;
                    updateViews();
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void handleDialState(State state) {
        boolean connected = false;
        if (state == null) {
            this.dialImageView.setImageResource(0);
            this.dialImageView.setVisibility(8);
            return;
        }
        switch (state) {
            case CONNECTED:
            case DISCONNECTED:
                if (state == State.CONNECTED) {
                    connected = true;
                }
                if (this.dialConnected != connected) {
                    this.dialConnected = connected;
                    updateViews();
                    return;
                }
                return;
            case OK_BATTERY:
            case LOW_BATTERY:
            case EXTREMELY_LOW_BATTERY:
                if (this.dialBattery != state) {
                    this.dialBattery = state;
                    updateViews();
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void updateViews() {
        int i = 0;
        ImageView imageView = this.phoneNetworkImageView;
        int i2 = (this.noNetwork && this.phoneConnected) ? 0 : 8;
        imageView.setVisibility(i2);
        HashMap hashMap = phoneStateResMap;
        State state = State.OK_BATTERY;
        if (this.noNetwork) {
            i = R.drawable.icon_device_battery_ok;
        }
        hashMap.put(state, Integer.valueOf(i));
        updateDeviceStatus(this.phoneImageView, phoneStateResMap, this.phoneConnected ? this.phoneBattery : State.DISCONNECTED);
        updateDeviceStatus(this.dialImageView, dialStateResMap, this.dialConnected ? this.dialBattery : State.DISCONNECTED);
    }

    private void updateDeviceStatus(ImageView view, HashMap<State, Integer> mapping, State batteryState) {
        int resId;
        int i = 0;
        Integer resource = (Integer) mapping.get(batteryState);
        if (resource != null) {
            resId = resource.intValue();
        } else {
            resId = 0;
        }
        view.setImageResource(resId);
        if (resId == 0) {
            i = 8;
        }
        view.setVisibility(i);
    }

    private void handleGpsState(State state) {
        switch (state) {
            case LOCATION_LOST:
                this.locationImageView.setVisibility(0);
                setDebugGpsMarker(null);
                return;
            case LOCATION_AVAILABLE:
                this.locationImageView.setVisibility(8);
                return;
            default:
                return;
        }
    }
}
