package com.navdy.hud.app.ui.component;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.audio.SoundUtils;
import com.navdy.hud.app.audio.SoundUtils.Sound;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ChoiceLayout extends FrameLayout {
    private static final int ANIMATION_EXECUTE_DURATION = 200;
    private static final int ANIMATION_SLIDE_DURATION = 100;
    private static final Object ANIMATION_TAG = new Object();
    private static final String EMPTY = "";
    private static final int KEY_UP_DOWN_DURATION = 50;
    private static int MIN_HIGHLIGHT_VIEW_WIDTH;
    private static int choiceHighlightColor;
    private static int choiceHighlightMargin;
    private static int choiceTextPaddingBottomDefault;
    private static int choiceTextPaddingDefault;
    private static int choiceTextSizeDefault;
    private static int textDeselectionColor;
    private static int textSelectionColor = -1;
    private OnGlobalLayoutListener animationLayoutListener;
    private AnimatorSet animatorSet;
    protected LinearLayout choiceContainer;
    private int choiceTextPaddingBottom;
    private int choiceTextPaddingLeft;
    private int choiceTextPaddingRight;
    private int choiceTextPaddingTop;
    private int choiceTextSize;
    protected List<Choice> choices;
    private Mode currentMode;
    private View currentSelectionView;
    private DefaultAnimationListener executeAnimationListener;
    private AnimatorUpdateListener executeUpdateListener;
    private View executeView;
    private boolean executing;
    private Handler handler;
    private boolean highlightPersistent;
    protected View highlightView;
    private AnimatorListener highlightViewListener;
    private int highlightVisibility;
    private boolean initialized;
    private IListener listener;
    private View oldSelectionView;
    private Queue<Operation> operationQueue;
    private boolean operationRunning;
    private boolean pressedDown;
    private int selectedItem;
    private DefaultAnimationListener upDownAnimationListener;
    private AnimatorUpdateListener upDownUpdateListener;

    public interface IListener {
        void executeItem(int i, int i2);

        void itemSelected(int i, int i2);
    }

    public static class Choice implements Parcelable {
        public static final Creator<Choice> CREATOR = new Creator<Choice>() {
            public Choice createFromParcel(Parcel in) {
                return new Choice(in);
            }

            public Choice[] newArray(int size) {
                return new Choice[size];
            }
        };
        public Icon icon;
        public int id;
        public String label;

        public Choice(String label, int id) {
            this.label = label;
            this.id = id;
        }

        public Choice(Icon icon, int id) {
            this.icon = icon;
            this.id = id;
        }

        public Choice(Parcel in) {
            this.label = in.readString();
            if (TextUtils.isEmpty(this.label)) {
                this.label = null;
            }
            int i = in.readInt();
            int j = in.readInt();
            if (!(i == -1 || j == -1)) {
                this.icon = new Icon(i, j);
            }
            this.id = in.readInt();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            int i;
            int i2 = -1;
            dest.writeString(this.label != null ? this.label : "");
            if (this.icon != null) {
                i = this.icon.resIdSelected;
            } else {
                i = -1;
            }
            dest.writeInt(i);
            if (this.icon != null) {
                i2 = this.icon.resIdUnSelected;
            }
            dest.writeInt(i2);
            dest.writeInt(this.id);
        }
    }

    public static class Icon {
        public int resIdSelected;
        public int resIdUnSelected;

        public Icon(int resId) {
            this.resIdSelected = resId;
            this.resIdUnSelected = resId;
        }

        public Icon(int resIdSelected, int resIdUnSelected) {
            this.resIdSelected = resIdSelected;
            this.resIdUnSelected = resIdUnSelected;
        }
    }

    public enum Mode {
        LABEL,
        ICON
    }

    enum Operation {
        MOVE_LEFT,
        MOVE_RIGHT,
        EXECUTE,
        KEY_DOWN,
        KEY_UP
    }

    protected boolean isHighlightPersistent() {
        return this.highlightPersistent;
    }

    public void setHighlightPersistent(boolean highlightPersistent) {
        this.highlightPersistent = highlightPersistent;
    }

    public ChoiceLayout(Context context) {
        this(context, null);
    }

    public ChoiceLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChoiceLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.highlightPersistent = false;
        this.highlightVisibility = 0;
        this.listener = null;
        this.selectedItem = -1;
        this.handler = new Handler();
        this.operationQueue = new LinkedList();
        this.upDownAnimationListener = new DefaultAnimationListener() {
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
            }

            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                ChoiceLayout.this.runQueuedOperation();
            }
        };
        this.upDownUpdateListener = new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                ((MarginLayoutParams) ChoiceLayout.this.highlightView.getLayoutParams()).topMargin = ((Integer) animation.getAnimatedValue()).intValue();
                ChoiceLayout.this.highlightView.invalidate();
                ChoiceLayout.this.highlightView.requestLayout();
            }
        };
        this.executeAnimationListener = new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animation) {
                if (ChoiceLayout.this.executeView != null) {
                    if (ChoiceLayout.this.executeView.getTag() != null) {
                        ChoiceLayout.this.executeView.setTag(null);
                        if (ChoiceLayout.this.executeView instanceof TextView) {
                            ((TextView) ChoiceLayout.this.executeView).setTextColor(ChoiceLayout.choiceHighlightColor);
                        } else if (ChoiceLayout.this.executeView instanceof ImageView) {
                            ((ImageView) ChoiceLayout.this.executeView).setColorFilter(ChoiceLayout.choiceHighlightColor);
                        }
                        ValueAnimator valueAnimator = ValueAnimator.ofInt(new int[]{0, ChoiceLayout.choiceHighlightMargin});
                        valueAnimator.addUpdateListener(ChoiceLayout.this.executeUpdateListener);
                        valueAnimator.addListener(ChoiceLayout.this.executeAnimationListener);
                        ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(ChoiceLayout.this.highlightView, "alpha", new float[]{1.0f, 0.5f});
                        ChoiceLayout.this.animatorSet = new AnimatorSet();
                        ChoiceLayout.this.animatorSet.setDuration(200);
                        ChoiceLayout.this.animatorSet.playTogether(new Animator[]{valueAnimator, alphaAnimator});
                        ChoiceLayout.this.animatorSet.start();
                        return;
                    }
                    if (!ChoiceLayout.this.isHighlightPersistent()) {
                        ChoiceLayout.this.highlightView.setVisibility(4);
                    }
                    ChoiceLayout.this.highlightView.setAlpha(1.0f);
                    ((MarginLayoutParams) ChoiceLayout.this.highlightView.getLayoutParams()).topMargin = ChoiceLayout.choiceHighlightMargin;
                    ChoiceLayout.this.executing = false;
                    ChoiceLayout.this.animatorSet = null;
                    ChoiceLayout.this.clearOperationQueue();
                    if (ChoiceLayout.this.selectedItem != -1) {
                        ChoiceLayout.this.callListener(ChoiceLayout.this.selectedItem);
                    }
                    if (ChoiceLayout.this.executeView instanceof TextView) {
                        ((TextView) ChoiceLayout.this.currentSelectionView).setTextColor(ChoiceLayout.textSelectionColor);
                    } else if (ChoiceLayout.this.executeView instanceof ImageView) {
                        ((ImageView) ChoiceLayout.this.currentSelectionView).setColorFilter(ChoiceLayout.textSelectionColor);
                    }
                }
            }
        };
        this.executeUpdateListener = new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                if (ChoiceLayout.this.executeView != null) {
                    ((MarginLayoutParams) ChoiceLayout.this.highlightView.getLayoutParams()).topMargin = ((Integer) animation.getAnimatedValue()).intValue();
                    ChoiceLayout.this.highlightView.invalidate();
                    ChoiceLayout.this.highlightView.requestLayout();
                }
            }
        };
        this.animationLayoutListener = new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                View view = ChoiceLayout.this.choiceContainer.getChildAt(ChoiceLayout.this.selectedItem);
                if (view != null) {
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    ChoiceLayout.this.selectInitialItem(ChoiceLayout.this.selectedItem);
                }
            }
        };
        this.highlightViewListener = new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animator) {
                if (ChoiceLayout.this.oldSelectionView != null) {
                    if (ChoiceLayout.this.currentMode == Mode.LABEL) {
                        ((TextView) ChoiceLayout.this.oldSelectionView).setTextColor(ChoiceLayout.textDeselectionColor);
                    } else {
                        ((ImageView) ChoiceLayout.this.oldSelectionView).setColorFilter(ChoiceLayout.textDeselectionColor);
                    }
                }
                Integer pos = null;
                if (ChoiceLayout.this.currentSelectionView != null) {
                    Integer tag = ChoiceLayout.this.currentSelectionView.getTag();
                    if (tag instanceof Integer) {
                        pos = tag;
                    }
                    if (ChoiceLayout.this.currentMode == Mode.LABEL) {
                        ((TextView) ChoiceLayout.this.currentSelectionView).setTextColor(ChoiceLayout.textSelectionColor);
                    } else {
                        ((ImageView) ChoiceLayout.this.currentSelectionView).setColorFilter(ChoiceLayout.textSelectionColor);
                    }
                }
                ChoiceLayout.this.animatorSet = null;
                if (pos != null) {
                    ChoiceLayout.this.callSelectListener(pos.intValue());
                }
                ChoiceLayout.this.runQueuedOperation();
            }
        };
        init();
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ChoiceLayout, defStyleAttr, 0);
        if (a != null) {
            this.choiceTextPaddingLeft = (int) a.getDimension(0, (float) choiceTextPaddingDefault);
            this.choiceTextPaddingRight = (int) a.getDimension(1, (float) choiceTextPaddingDefault);
            this.choiceTextPaddingBottom = (int) a.getDimension(3, (float) choiceTextPaddingBottomDefault);
            this.choiceTextPaddingTop = (int) a.getDimension(2, (float) choiceTextPaddingDefault);
            this.choiceTextSize = (int) a.getDimension(4, (float) choiceTextSizeDefault);
            a.recycle();
        }
    }

    public void setHighlightVisibility(int highlightVisibility) {
        this.highlightVisibility = highlightVisibility;
        invalidate();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.choices_lyt, this, true);
        invalidate();
        this.choiceContainer = (LinearLayout) findViewById(R.id.choiceContainer);
        this.highlightView = findViewById(R.id.choiceHighlight);
        if (textSelectionColor == -1) {
            Resources resources = getResources();
            MIN_HIGHLIGHT_VIEW_WIDTH = resources.getDimensionPixelSize(R.dimen.min_highlight_view_width);
            textSelectionColor = resources.getColor(R.color.choice_selection);
            textDeselectionColor = resources.getColor(R.color.choice_deselection);
            choiceTextPaddingDefault = (int) resources.getDimension(R.dimen.choices_lyt_item_padding);
            choiceTextPaddingBottomDefault = (int) resources.getDimension(R.dimen.choices_lyt_item_padding_bottom);
            choiceTextSizeDefault = (int) resources.getDimension(R.dimen.choices_lyt_text_size);
            choiceHighlightColor = resources.getColor(R.color.cyan);
            choiceHighlightMargin = (int) resources.getDimension(R.dimen.choices_lyt_highlight_margin);
        }
    }

    public void setChoices(Mode mode, List<Choice> choices, int initialSelection, IListener listener) {
        this.currentMode = mode;
        this.choices = choices;
        this.listener = listener;
        this.initialized = false;
        Operation pendingOp = (Operation) this.operationQueue.peek();
        this.executeView = null;
        clearOperationQueue();
        this.executing = false;
        this.selectedItem = -1;
        if (choices != null && choices.size() > 0) {
            if (initialSelection < 0 || initialSelection >= choices.size()) {
                this.selectedItem = 0;
            } else {
                this.selectedItem = initialSelection;
            }
        }
        buildChoices();
        selectInitialItem(this.selectedItem);
        if (pendingOp == Operation.KEY_UP) {
            ((LayoutParams) this.highlightView.getLayoutParams()).topMargin = choiceHighlightMargin;
        }
    }

    public List<Choice> getChoices() {
        return this.choices;
    }

    private void buildChoices() {
        this.choiceContainer.removeAllViews();
        this.currentSelectionView = null;
        this.oldSelectionView = null;
        this.highlightView.setVisibility(4);
        if (this.choices != null) {
            int len = this.choices.size();
            for (int i = 0; i < len; i++) {
                View view;
                Choice choice = (Choice) this.choices.get(i);
                if (this.currentMode == Mode.LABEL) {
                    View textView = buildChoice("", i);
                    textView.setText(choice.label);
                    view = textView;
                } else {
                    View imageView = new ImageView(getContext());
                    imageView.setScaleType(ScaleType.FIT_CENTER);
                    imageView.setImageResource(choice.icon.resIdUnSelected);
                    imageView.setColorFilter(textDeselectionColor);
                    view = imageView;
                }
                LinearLayout.LayoutParams lytParams = new LinearLayout.LayoutParams(-2, -2);
                lytParams.setMargins(this.choiceTextPaddingLeft, 0, this.choiceTextPaddingRight, 0);
                this.choiceContainer.addView(view, lytParams);
            }
        }
        invalidate();
        requestLayout();
    }

    private boolean executeSelectedItemInternal() {
        if (!this.initialized || this.executing) {
            return false;
        }
        if (this.selectedItem != -1 && getItemCount() > 0) {
            this.executing = true;
            if (this.highlightVisibility == 0) {
                this.executeView = this.choiceContainer.getChildAt(this.selectedItem);
                if (this.executeView == null) {
                    callListener(this.selectedItem);
                    this.executing = false;
                } else {
                    this.executeView.setTag(ANIMATION_TAG);
                    ValueAnimator valueAnimator = ValueAnimator.ofInt(new int[]{choiceHighlightMargin, 0});
                    valueAnimator.addListener(this.executeAnimationListener);
                    valueAnimator.addUpdateListener(this.executeUpdateListener);
                    this.animatorSet = new AnimatorSet();
                    this.animatorSet.setDuration(200);
                    this.animatorSet.play(valueAnimator);
                    this.animatorSet.start();
                    return true;
                }
            }
            callListener(this.selectedItem);
            this.executing = false;
        }
        return false;
    }

    private boolean moveSelectionLeftInternal() {
        if (!this.initialized || this.executing || this.selectedItem == -1) {
            return false;
        }
        return setSelectedItem(this.selectedItem - 1);
    }

    private boolean moveSelectionRightInternal() {
        if (!this.initialized || this.executing || this.selectedItem == -1) {
            return false;
        }
        return setSelectedItem(this.selectedItem + 1);
    }

    private boolean keyDownSelectedItemInternal() {
        if (!this.initialized || this.pressedDown) {
            return false;
        }
        if (this.selectedItem != -1 && getItemCount() > 0) {
            if (this.currentMode == Mode.LABEL) {
                ((TextView) this.currentSelectionView).setTextColor(choiceHighlightColor);
            } else {
                ((ImageView) this.currentSelectionView).setColorFilter(choiceHighlightColor);
            }
            if (this.highlightVisibility == 0) {
                this.pressedDown = true;
                if (this.choiceContainer.getChildAt(this.selectedItem) != null) {
                    ValueAnimator valueAnimator = ValueAnimator.ofInt(new int[]{choiceHighlightMargin, 0});
                    valueAnimator.addListener(this.upDownAnimationListener);
                    valueAnimator.addUpdateListener(this.upDownUpdateListener);
                    this.animatorSet = new AnimatorSet();
                    this.animatorSet.setDuration(50);
                    this.animatorSet.play(valueAnimator);
                    this.animatorSet.start();
                    return true;
                }
            }
        }
        return true;
    }

    private boolean keyUpSelectedItemInternal() {
        if (!this.initialized || !this.pressedDown) {
            return false;
        }
        if (this.selectedItem != -1 && getItemCount() > 0) {
            if (this.currentMode == Mode.LABEL) {
                ((TextView) this.currentSelectionView).setTextColor(textSelectionColor);
            } else {
                ((ImageView) this.currentSelectionView).setColorFilter(textSelectionColor);
            }
            if (this.highlightVisibility == 0) {
                this.pressedDown = false;
                if (this.choiceContainer.getChildAt(this.selectedItem) != null) {
                    ValueAnimator valueAnimator = ValueAnimator.ofInt(new int[]{0, choiceHighlightMargin});
                    valueAnimator.addListener(this.upDownAnimationListener);
                    valueAnimator.addUpdateListener(this.upDownUpdateListener);
                    this.animatorSet = new AnimatorSet();
                    this.animatorSet.setDuration(50);
                    this.animatorSet.play(valueAnimator);
                    this.animatorSet.start();
                    return true;
                }
            }
        }
        return true;
    }

    public void moveSelectionLeft() {
        runOperation(Operation.MOVE_LEFT, false);
    }

    public void moveSelectionRight() {
        runOperation(Operation.MOVE_RIGHT, false);
    }

    public void keyDownSelectedItem() {
        runOperation(Operation.KEY_DOWN, false);
    }

    public void keyUpSelectedItem() {
        runOperation(Operation.KEY_UP, false);
    }

    public void executeSelectedItem(boolean animated) {
        runOperation(Operation.EXECUTE, false);
    }

    private TextView buildChoice(String label, int pos) {
        TextView textView = new TextView(getContext());
        textView.setGravity(17);
        textView.setSingleLine(true);
        textView.setText(label);
        textView.setIncludeFontPadding(false);
        textView.setTextSize(0, (float) this.choiceTextSize);
        textView.setTypeface(null, 1);
        textView.setTextColor(textDeselectionColor);
        return textView;
    }

    private int getItemCount() {
        if (this.choices != null) {
            return this.choices.size();
        }
        return 0;
    }

    private void callListener(int pos) {
        if (this.listener != null) {
            SoundUtils.playSound(Sound.MENU_SELECT);
            if (this.choices != null && pos >= 0 && pos < this.choices.size()) {
                this.listener.executeItem(pos, ((Choice) this.choices.get(pos)).id);
            }
        }
    }

    private void callSelectListener(int pos) {
        if (this.listener != null && this.choices != null && pos >= 0 && pos < this.choices.size()) {
            this.listener.itemSelected(pos, ((Choice) this.choices.get(pos)).id);
        }
    }

    protected boolean setSelectedItem(int pos) {
        if (this.selectedItem == pos) {
            return false;
        }
        int itemCount = getItemCount();
        if (pos < 0 || pos >= itemCount) {
            return false;
        }
        this.selectedItem = pos;
        View view = this.choiceContainer.getChildAt(pos);
        this.oldSelectionView = this.currentSelectionView;
        this.currentSelectionView = view;
        this.currentSelectionView.setTag(Integer.valueOf(pos));
        int width = Math.max(view.getMeasuredWidth(), MIN_HIGHLIGHT_VIEW_WIDTH);
        if (width == 0) {
            return false;
        }
        int oldWidth;
        width -= view.getPaddingLeft() + view.getPaddingRight();
        if (this.oldSelectionView != null) {
            oldWidth = Math.max(this.oldSelectionView.getMeasuredWidth() - (this.oldSelectionView.getPaddingLeft() + this.oldSelectionView.getPaddingRight()), MIN_HIGHLIGHT_VIEW_WIDTH);
        } else {
            oldWidth = width;
        }
        this.animatorSet = new AnimatorSet();
        ObjectAnimator a1 = ObjectAnimator.ofFloat(this.highlightView, "x", new float[]{getXPositionForHighlight(view)});
        ValueAnimator.ofInt(new int[]{oldWidth, width}).addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                ((LayoutParams) ChoiceLayout.this.highlightView.getLayoutParams()).width = ((Integer) valueAnimator.getAnimatedValue()).intValue();
                ChoiceLayout.this.highlightView.invalidate();
                ChoiceLayout.this.highlightView.requestLayout();
            }
        });
        this.highlightView.setVisibility(this.highlightVisibility);
        this.highlightView.invalidate();
        this.highlightView.requestLayout();
        this.animatorSet.playTogether(new Animator[]{a1, v1});
        this.animatorSet.setDuration(100);
        this.animatorSet.addListener(this.highlightViewListener);
        this.animatorSet.start();
        SoundUtils.playSound(Sound.MENU_MOVE);
        return true;
    }

    private float getXPositionForHighlight(View view) {
        int width = view.getMeasuredWidth() - (view.getPaddingLeft() + view.getPaddingRight());
        int left = view.getLeft() + view.getPaddingLeft();
        if (width >= MIN_HIGHLIGHT_VIEW_WIDTH) {
            return (float) left;
        }
        return (float) (left - ((MIN_HIGHLIGHT_VIEW_WIDTH - width) / 2));
    }

    private void selectInitialItem(int pos) {
        if (pos != -1) {
            View viewToHighlight = this.choiceContainer.getChildAt(pos);
            if (viewToHighlight.getMeasuredWidth() == 0) {
                viewToHighlight.getViewTreeObserver().addOnGlobalLayoutListener(this.animationLayoutListener);
                return;
            }
            int xPos = (int) getXPositionForHighlight(viewToHighlight);
            ((LayoutParams) this.highlightView.getLayoutParams()).width = Math.max(viewToHighlight.getMeasuredWidth() - (viewToHighlight.getPaddingLeft() + viewToHighlight.getPaddingRight()), MIN_HIGHLIGHT_VIEW_WIDTH);
            this.highlightView.setX((float) xPos);
            this.highlightView.setVisibility(this.highlightVisibility);
            this.highlightView.invalidate();
            this.highlightView.requestLayout();
            this.currentSelectionView = viewToHighlight;
            this.currentSelectionView.setTag(Integer.valueOf(pos));
            if (this.currentMode == Mode.LABEL) {
                ((TextView) this.currentSelectionView).setTextColor(textSelectionColor);
            } else {
                ((ImageView) this.currentSelectionView).setColorFilter(textSelectionColor);
            }
            this.initialized = true;
        }
    }

    public void setLabelTextSize(int size) {
        this.choiceTextSize = size;
    }

    public void setItemPadding(int size) {
        this.choiceTextPaddingLeft = size;
        this.choiceTextPaddingRight = size;
    }

    public void resetAnimationElements() {
        if (this.highlightVisibility == 0 && this.highlightView != null) {
            this.highlightView.setVisibility(0);
        }
        if (this.executeView == null) {
            return;
        }
        if (this.executeView instanceof TextView) {
            ((TextView) this.executeView).setTextColor(textSelectionColor);
            this.executeView = null;
        } else if (this.executeView instanceof ImageView) {
            ((ImageView) this.executeView).setColorFilter(textSelectionColor);
            this.executeView = null;
        }
    }

    public boolean hasChoiceId(int id) {
        for (Choice choice : this.choices) {
            if (choice.id == id) {
                return true;
            }
        }
        return false;
    }

    private void runOperation(Operation operation, boolean ignoreCurrent) {
        if (ignoreCurrent || !this.operationRunning) {
            this.operationRunning = true;
            switch (operation) {
                case MOVE_LEFT:
                    if (!moveSelectionLeftInternal()) {
                        runQueuedOperation();
                        return;
                    }
                    return;
                case MOVE_RIGHT:
                    if (!moveSelectionRightInternal()) {
                        runQueuedOperation();
                        return;
                    }
                    return;
                case EXECUTE:
                    if (!executeSelectedItemInternal()) {
                        runQueuedOperation();
                        return;
                    }
                    return;
                case KEY_DOWN:
                    if (!keyDownSelectedItemInternal()) {
                        runQueuedOperation();
                        return;
                    }
                    return;
                case KEY_UP:
                    if (!keyUpSelectedItemInternal()) {
                        runQueuedOperation();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
        this.operationQueue.add(operation);
    }

    @Nullable
    public Choice getSelectedItem() {
        if (this.choices == null || this.selectedItem < 0 || this.selectedItem >= this.choices.size()) {
            return null;
        }
        return (Choice) this.choices.get(this.selectedItem);
    }

    public int getSelectedItemIndex() {
        return this.selectedItem;
    }

    private void runQueuedOperation() {
        if (this.operationQueue.size() > 0) {
            final Operation operation = (Operation) this.operationQueue.remove();
            this.handler.post(new Runnable() {
                public void run() {
                    ChoiceLayout.this.runOperation(operation, true);
                }
            });
            return;
        }
        this.operationRunning = false;
    }

    public void clearOperationQueue() {
        if (this.animatorSet != null && this.animatorSet.isRunning()) {
            this.animatorSet.removeAllListeners();
            this.animatorSet.cancel();
            this.animatorSet = null;
        }
        this.operationQueue.clear();
        this.operationRunning = false;
    }
}
