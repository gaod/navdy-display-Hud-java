package com.navdy.hud.app.ui.component.homescreen;

import android.content.res.Resources;
import android.graphics.PointF;
import com.here.android.mpa.common.ViewRect;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;

public class HomeScreenConstants {
    public static final String EMPTY = "";
    public static final int INITIAL_ROUTE_TIMEOUT = 5000;
    public static final int MINUTES_DAY = 1440;
    public static final int MINUTES_HOUR = 60;
    public static final int MIN_RENAVIGATION_DISTANCE_METERS = 500;
    public static final double MIN_RENAVIGATION_DISTANCE_METERS_GAS = 500.0d;
    public static final int SELECTION_ROUTE_TIMEOUT = 10000;
    public static final String SPACE = " ";
    public static final String SPEED_KM;
    public static final String SPEED_METERS;
    public static final String SPEED_MPH;
    public static final int TOP_ANIMATION_INITIAL_OUT_INTERVAL = 30000;
    public static final int TOP_ANIMATION_OUT_INTERVAL = 10000;
    public static final ViewRect routeOverviewRect = new ViewRect(HomeScreenResourceValues.routeOverviewBoxX, HomeScreenResourceValues.routeOverviewBoxY, HomeScreenResourceValues.routeOverviewBoxWidth, HomeScreenResourceValues.routeOverviewBoxHeight);
    public static final ViewRect routePickerViewRect = new ViewRect(HomeScreenResourceValues.routePickerBoxX, HomeScreenResourceValues.routePickerBoxY, HomeScreenResourceValues.routePickerBoxWidth, HomeScreenResourceValues.routePickerBoxHeight);
    public static final PointF transformCenterLargeBottom = new PointF((float) (HomeScreenResourceValues.transformCenterX + (HomeScreenResourceValues.transformCenterIconWidth / 2)), (float) HomeScreenResourceValues.transformCenterOverviewRouteY);
    public static final PointF transformCenterLargeMiddle = new PointF((float) HomeScreenResourceValues.transformCenterOverviewX, (float) HomeScreenResourceValues.transformCenterOverviewY);
    public static final PointF transformCenterPicker = new PointF((float) HomeScreenResourceValues.transformCenterOverviewX, (float) HomeScreenResourceValues.transformCenterPickerY);
    public static final PointF transformCenterSmallBottom = new PointF((float) (HomeScreenResourceValues.transformCenterX + (HomeScreenResourceValues.transformCenterIconWidth / 2)), (float) HomeScreenResourceValues.transformCenterMapOnRouteY);
    public static final PointF transformCenterSmallMiddle = new PointF((float) HomeScreenResourceValues.transformCenterOverviewX, (float) HomeScreenResourceValues.transformCenterMapOnRouteOverviewY);

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        SPEED_MPH = resources.getString(R.string.miles_per_hour);
        SPEED_KM = resources.getString(R.string.kilometers_per_hour);
        SPEED_METERS = resources.getString(R.string.meters_per_second);
    }
}
