package com.navdy.hud.app.ui.component.mainmenu;

import android.content.res.Resources;
import android.graphics.LinearGradient;
import android.graphics.Shader.TileMode;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.phonecall.CallManager;
import com.navdy.hud.app.framework.phonecall.CallNotification;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager.ToastParams;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.framework.voice.VoiceSearchNotification;
import com.navdy.hud.app.manager.MusicManager;
import com.navdy.hud.app.manager.MusicManager.MediaControl;
import com.navdy.hud.app.manager.MusicManager.MusicUpdateListener;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.UpdateReminderManager;
import com.navdy.hud.app.maps.MapEvents.ArrivalEvent;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.screen.WelcomeScreen;
import com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable;
import com.navdy.hud.app.ui.component.UISettings;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.VerticalList.FontInfo;
import com.navdy.hud.app.ui.component.vlist.VerticalList.FontSize;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder;
import com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.ActionRunnable;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.hud.app.util.RemoteCapabilitiesUtil;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.DeviceInfo.Platform;
import com.navdy.service.library.events.audio.MusicPlaybackState;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction;
import com.navdy.service.library.events.preferences.NotificationPreferences;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import okio.ByteString;

public class MainMenu implements IMenu {
    private static final int ACTIVITY_TRAY_UPDATE = ((int) TimeUnit.SECONDS.toMillis(1));
    private static final int ETA_UPDATE_INTERVAL = ((int) TimeUnit.SECONDS.toMillis(30));
    private static final String activeTripTitle = resources.getString(R.string.mm_active_trip);
    private static final ArrayList<Integer> activityTrayIcon = new ArrayList();
    private static final ArrayList<Integer> activityTrayIconId = new ArrayList();
    private static final ArrayList<Integer> activityTraySelectedColor = new ArrayList();
    private static final ArrayList<Integer> activityTrayUnSelectedColor = new ArrayList();
    public static final int bkColorUnselected = resources.getColor(R.color.icon_bk_color_unselected);
    private static final CallManager callManager = remoteDeviceManager.getCallManager();
    static final HashMap<String, Menu> childMenus = new HashMap();
    private static final Model connectPhone;
    private static final Model contacts;
    private static final String dashTitle = resources.getString(R.string.carousel_menu_smartdash_options);
    public static final FontInfo defaultFontInfo = VerticalList.getFontInfo(FontSize.FONT_SIZE_26);
    private static final int glancesColor = resources.getColor(R.color.mm_glances);
    private static final Handler handler = new Handler(Looper.getMainLooper());
    private static final String mapTitle = resources.getString(R.string.carousel_menu_map_options);
    private static final Model maps;
    private static final Model musicControl;
    private static MusicManager musicManager = remoteDeviceManager.getMusicManager();
    private static final int nowPlayingColor = resources.getColor(R.color.music_now_playing);
    private static final Model places;
    private static final RemoteDeviceManager remoteDeviceManager = RemoteDeviceManager.getInstance();
    private static final Resources resources = HudApplication.getAppContext().getResources();
    private static final Logger sLogger = new Logger(MainMenu.class);
    private static final Model settings;
    private static final Model smartDash;
    private static final UIStateManager uiStateManager = remoteDeviceManager.getUiStateManager();
    private static final Model voiceAssistanceGoogle;
    private static final Model voiceAssistanceSiri;
    private static final Model voiceSearch;
    private ActiveTripMenu activeTripMenu;
    private Runnable activityTrayRunnable = new Runnable() {
        public void run() {
            if (!MainMenu.this.activityTrayRunning) {
                return;
            }
            if (MainMenu.this.curToolTipPos < 0) {
                MainMenu.this.activityTrayRunning = false;
                return;
            }
            String toolTip = MainMenu.this.getToolTipString(R.id.main_menu_call);
            if (toolTip != null) {
                MainMenu.this.presenter.showToolTip(MainMenu.this.curToolTipPos, toolTip);
                MainMenu.handler.postDelayed(this, (long) MainMenu.ACTIVITY_TRAY_UPDATE);
            }
        }
    };
    private boolean activityTrayRunning;
    private int activityTraySelection = -1;
    private int activityTraySelectionId = -1;
    private MusicTrackInfo addedTrackInfo;
    private Bus bus;
    private boolean busRegistered;
    private List<Model> cachedList;
    private ContactsMenu contactsMenu;
    private int curToolTipId = -1;
    private int curToolTipPos = -1;
    private String currentEta;
    private Runnable etaRunnable = new Runnable() {
        public void run() {
            MainMenu.this.fillEta();
            MainMenu.handler.postDelayed(this, (long) MainMenu.ETA_UPDATE_INTERVAL);
        }
    };
    private MainOptionsMenu mainOptionsMenu;
    private IMenu musicMenu;
    private MusicUpdateListener musicUpdateListener = new MusicUpdateListener() {
        public void onAlbumArtUpdate(@Nullable ByteString artwork, boolean animate) {
        }

        public void onTrackUpdated(final MusicTrackInfo trackInfo, Set<MediaControl> set, boolean willOpenNotification) {
            if (!MainMenu.this.isSameTrack(MainMenu.this.addedTrackInfo, trackInfo) && MainMenu.this.canShowTrackInfo(trackInfo)) {
                MainMenu.handler.post(new Runnable() {
                    public void run() {
                        MainMenu.this.addedTrackInfo = trackInfo;
                        if (MainMenu.this.curToolTipId == R.id.main_menu_now_playing && MainMenu.this.curToolTipPos >= 0) {
                            MainMenu.sLogger.v("update music tool tip");
                            String str = MainMenu.this.getMusicTrackInfo();
                            if (str != null) {
                                MainMenu.this.presenter.showToolTip(MainMenu.this.curToolTipPos, str);
                            }
                        }
                    }
                });
            }
        }
    };
    private PlacesMenu placesMenu;
    private Presenter presenter;
    private boolean registeredMusicCallback;
    private int selectedIndex;
    private SettingsMenu settingsMenu;
    private String swVersion;
    private VerticalMenuComponent vscrollComponent;

    static {
        childMenus.put(Menu.SETTINGS.name(), Menu.SETTINGS);
        childMenus.put(Menu.PLACES.name(), Menu.PLACES);
        childMenus.put(Menu.CONTACTS.name(), Menu.CONTACTS);
        childMenus.put(Menu.MUSIC.name(), Menu.MUSIC);
        String title = resources.getString(R.string.mm_voice_search);
        String subTitle = resources.getString(R.string.mm_voice_search_description);
        int fluctuatorColor = resources.getColor(R.color.mm_voice_search);
        voiceSearch = IconBkColorViewHolder.buildModel(R.id.main_menu_voice_search, R.drawable.icon_mm_voice_search_2, fluctuatorColor, bkColorUnselected, fluctuatorColor, title, subTitle);
        title = resources.getString(R.string.carousel_menu_smartdash_title);
        fluctuatorColor = resources.getColor(R.color.mm_dash);
        smartDash = IconBkColorViewHolder.buildModel(R.id.main_menu_smart_dash, R.drawable.icon_mm_dash_2, fluctuatorColor, bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_menu_map_title);
        fluctuatorColor = resources.getColor(R.color.mm_map);
        maps = IconBkColorViewHolder.buildModel(R.id.main_menu_maps, R.drawable.icon_mm_map_2, fluctuatorColor, bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_menu_map_places);
        fluctuatorColor = resources.getColor(R.color.mm_places);
        places = IconBkColorViewHolder.buildModel(R.id.main_menu_places, R.drawable.icon_mm_places_2, fluctuatorColor, bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_menu_music_control);
        fluctuatorColor = resources.getColor(R.color.mm_music);
        musicControl = IconBkColorViewHolder.buildModel(R.id.main_menu_music, R.drawable.icon_mm_music_2, fluctuatorColor, bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_menu_voice_control_siri);
        fluctuatorColor = resources.getColor(R.color.mm_voice_assist_siri);
        voiceAssistanceSiri = IconBkColorViewHolder.buildModel(R.id.main_menu_voice, R.drawable.icon_mm_siri_2, fluctuatorColor, bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_menu_voice_control_google);
        fluctuatorColor = resources.getColor(R.color.mm_voice_assist_gnow);
        voiceAssistanceGoogle = IconBkColorViewHolder.buildModel(R.id.main_menu_voice, R.drawable.icon_mm_googlenow_2, fluctuatorColor, bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_menu_contacts);
        fluctuatorColor = resources.getColor(R.color.mm_contacts);
        contacts = IconBkColorViewHolder.buildModel(R.id.main_contacts, R.drawable.icon_mm_contacts_2, fluctuatorColor, bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_menu_settings_title);
        fluctuatorColor = resources.getColor(R.color.mm_settings);
        settings = IconBkColorViewHolder.buildModel(R.id.main_menu_settings, R.drawable.icon_mm_settings_2, fluctuatorColor, bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_settings_connect_phone_title);
        fluctuatorColor = resources.getColor(R.color.mm_connnect_phone);
        connectPhone = IconBkColorViewHolder.buildModel(R.id.main_menu_settings_connect_phone, R.drawable.icon_settings_connect_phone_2, fluctuatorColor, bkColorUnselected, fluctuatorColor, title, null);
    }

    public MainMenu(Bus bus, VerticalMenuComponent vscrollComponent, Presenter presenter) {
        this.bus = bus;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
    }

    public List<Model> getItems() {
        clearNowPlayingState();
        stopActivityTrayRunnable();
        this.curToolTipId = -1;
        this.curToolTipPos = -1;
        DeviceInfo deviceInfo = remoteDeviceManager.getRemoteDeviceInfo();
        List<Model> list = new ArrayList();
        boolean isConnected = !DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        if (isConnected && deviceInfo == null) {
            isConnected = false;
        }
        sLogger.v("isConnected:" + isConnected);
        this.selectedIndex = -1;
        HereMapsManager hereMapsManager = HereMapsManager.getInstance();
        Model activityTray = getActivityTrayModel(isConnected);
        if (hereMapsManager.isInitialized() && hereMapsManager.isNavigationModeOn()) {
            this.selectedIndex = list.size();
            if (activityTray.iconIds != null) {
                for (int i = 0; i < activityTray.iconIds.length; i++) {
                    if (activityTray.iconIds[i] == R.id.main_menu_active_trip) {
                        activityTray.currentIconSelection = i;
                        break;
                    }
                }
            }
        }
        list.add(activityTray);
        boolean navPossible = HereMapsManager.getInstance().isNavigationPossible();
        boolean isDeviceCapableOfVoiceSearch = RemoteCapabilitiesUtil.supportsVoiceSearch();
        if (isConnected) {
            if (!navPossible) {
                sLogger.v("nav not possible");
            }
            addLongPressAction(deviceInfo, list, isDeviceCapableOfVoiceSearch, navPossible, true);
        } else {
            this.selectedIndex = list.size();
            list.add(connectPhone);
        }
        if (this.selectedIndex == -1) {
            this.selectedIndex = list.size();
        }
        list.add(maps);
        list.add(smartDash);
        if (isConnected) {
            if (navPossible) {
                list.add(places);
            }
            list.add(contacts);
        }
        Model glances = IconBkColorViewHolder.buildModel(R.id.main_menu_glances, R.drawable.icon_mm_glances_2, glancesColor, bkColorUnselected, glancesColor, resources.getString(R.string.mm_glances), null);
        NotificationPreferences prefs = DriverProfileHelper.getInstance().getCurrentProfile().getNotificationPreferences();
        if (prefs.enabled != null ? prefs.enabled.booleanValue() : false) {
            int notificationCount = NotificationManager.getInstance().getNotificationCount();
            glances.title = resources.getString(R.string.mm_glances);
            glances.subTitle = resources.getString(R.string.mm_glances_subtitle, new Object[]{Integer.valueOf(notificationCount)});
        } else {
            glances.title = resources.getString(R.string.mm_glances_disabled);
            glances.subTitle = null;
        }
        list.add(glances);
        if (isConnected) {
            list.add(musicControl);
            addLongPressAction(deviceInfo, list, isDeviceCapableOfVoiceSearch, navPossible, false);
        }
        list.add(settings);
        this.cachedList = list;
        if (!this.registeredMusicCallback) {
            musicManager.addMusicUpdateListener(this.musicUpdateListener);
            this.registeredMusicCallback = true;
            sLogger.v("register music");
        }
        if (!this.busRegistered) {
            this.bus.register(this);
            this.busRegistered = true;
            sLogger.v("registered bus");
        }
        return list;
    }

    private void addVoiceAssistance(DeviceInfo deviceInfo, List<Model> list) {
        if (deviceInfo.platform == Platform.PLATFORM_iOS) {
            list.add(voiceAssistanceSiri);
        } else {
            list.add(voiceAssistanceGoogle);
        }
    }

    private void addLongPressAction(DeviceInfo deviceInfo, List<Model> list, boolean isDeviceCapableOfVoiceSearch, boolean navPossible, boolean start) {
        if (start) {
            if (isLongPressActionPlaceSearch()) {
                if (this.selectedIndex == -1) {
                    this.selectedIndex = list.size();
                }
                addVoiceAssistance(deviceInfo, list);
            } else if (isDeviceCapableOfVoiceSearch && navPossible) {
                if (this.selectedIndex == -1) {
                    this.selectedIndex = list.size();
                }
                list.add(voiceSearch);
            }
        } else if (!isLongPressActionPlaceSearch()) {
            addVoiceAssistance(deviceInfo, list);
        } else if (isDeviceCapableOfVoiceSearch && navPossible) {
            list.add(voiceSearch);
        }
    }

    public int getInitialSelection() {
        return this.selectedIndex;
    }

    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
    }

    public void setBackSelectionId(int id) {
    }

    public void setSelectedIcon() {
        if (this.activityTraySelection != -1 && this.presenter.getCurrentSelection() == 0) {
            String toolTip = getToolTipString(this.activityTraySelectionId);
            if (toolTip != null) {
                this.presenter.showToolTip(this.activityTraySelection, toolTip);
            }
        }
        this.activityTraySelection = -1;
        this.activityTraySelectionId = -1;
        Resources resources = HudApplication.getAppContext().getResources();
        int size = resources.getDimensionPixelSize(R.dimen.vmenu_selected_image);
        int gradientStart = resources.getColor(R.color.mm_icon_gradient_start);
        int gradientEnd = resources.getColor(R.color.mm_icon_gradient_end);
        float f = 0.0f;
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_mm_navdy, -1, new LinearGradient(0.0f, f, (float) size, (float) size, new int[]{gradientStart, gradientEnd}, null, TileMode.MIRROR), 1.0f);
        this.vscrollComponent.selectedText.setText(resources.getString(R.string.menu_str));
    }

    public boolean isItemClickable(int id, int pos) {
        switch (id) {
            case R.id.main_menu_no_fav_contacts:
            case R.id.main_menu_no_fav_places:
            case R.id.main_menu_no_recent_contacts:
            case R.id.main_menu_no_suggested_places:
                return false;
            default:
                return true;
        }
    }

    public Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(Model model, View view, int pos, ModelState state) {
    }

    public void onItemSelected(ItemSelectionState selection) {
        switch (selection.id) {
            case R.id.main_menu_activity_tray:
                String toolTipStr = getToolTipString(selection.subId);
                if (toolTipStr != null) {
                    stopActivityTrayRunnable();
                    switch (selection.subId) {
                        case R.id.main_menu_call:
                            startActivityTrayRunnable(selection.subId, selection.subPosition);
                            break;
                        case R.id.main_menu_now_playing:
                            this.curToolTipId = selection.subId;
                            this.curToolTipPos = selection.subPosition;
                            break;
                        default:
                            this.curToolTipId = -1;
                            this.curToolTipPos = -1;
                            break;
                    }
                    this.presenter.showToolTip(selection.subPosition, toolTipStr);
                    return;
                }
                stopActivityTrayRunnable();
                this.presenter.hideToolTip();
                return;
            default:
                stopActivityTrayRunnable();
                this.presenter.hideToolTip();
                return;
        }
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
        int subId = this.presenter.getCurrentSubSelectionId();
        int subPos = this.presenter.getCurrentSubSelectionPos();
        String toolTipStr = getToolTipString(subId);
        if (toolTipStr != null) {
            stopActivityTrayRunnable();
            if (subId == R.id.main_menu_call) {
                startActivityTrayRunnable(subId, subPos);
            }
            this.presenter.showToolTip(subPos, toolTipStr);
            return;
        }
        stopActivityTrayRunnable();
        this.presenter.hideToolTip();
    }

    public boolean isFirstItemEmpty() {
        return true;
    }

    public boolean selectItem(ItemSelectionState selection) {
        sLogger.v("select id:" + selection.id + " pos:" + selection.pos);
        Bundle args;
        switch (selection.id) {
            case R.id.main_contacts:
                sLogger.v("contacts");
                AnalyticsSupport.recordMenuSelection("contacts");
                createChildMenu(Menu.CONTACTS, null);
                this.presenter.loadMenu(this.contactsMenu, MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            case R.id.main_menu_activity_tray:
                switch (selection.subId) {
                    case R.id.main_menu_active_trip:
                        sLogger.v("trip options");
                        AnalyticsSupport.recordMenuSelection("trip-options");
                        if (this.activeTripMenu == null) {
                            this.activeTripMenu = new ActiveTripMenu(this.bus, this.vscrollComponent, this.presenter, this);
                        }
                        this.activityTraySelection = selection.subPosition;
                        this.activityTraySelectionId = selection.subId;
                        this.presenter.loadMenu(this.activeTripMenu, MenuLevel.SUB_LEVEL, selection.pos, 1, true);
                        break;
                    case R.id.main_menu_call:
                        sLogger.v("phone call");
                        AnalyticsSupport.recordMenuSelection("phone_call_mm");
                        NotificationManager notificationManager = NotificationManager.getInstance();
                        if (notificationManager.getNotification(NotificationId.PHONE_CALL_NOTIFICATION_ID) != null && notificationManager.makeNotificationCurrent(true)) {
                            sLogger.d("phone call");
                            notificationManager.showNotification();
                            break;
                        }
                        this.presenter.performSelectionAnimation(new ActionRunnable(this.bus, Screen.SCREEN_BACK));
                        break;
                    case R.id.main_menu_glances:
                        handleGlances();
                        break;
                    case R.id.main_menu_maps_options:
                        sLogger.v("map options");
                        AnalyticsSupport.recordMenuSelection("hybrid_map-options");
                        if (this.mainOptionsMenu == null) {
                            this.mainOptionsMenu = new MainOptionsMenu(this.bus, this.vscrollComponent, this.presenter, this);
                        }
                        this.mainOptionsMenu.setMode(Mode.MAP);
                        this.activityTraySelection = selection.subPosition;
                        this.activityTraySelectionId = selection.subId;
                        this.presenter.loadMenu(this.mainOptionsMenu, MenuLevel.SUB_LEVEL, selection.pos, 0);
                        break;
                    case R.id.main_menu_now_playing:
                        sLogger.v("now_playing");
                        AnalyticsSupport.recordMenuSelection("now_playing");
                        this.presenter.performSelectionAnimation(new Runnable() {
                            public void run() {
                                MainMenu.this.bus.post(Screen.SCREEN_BACK);
                                MainMenu.musicManager.showMusicNotification();
                            }
                        });
                        break;
                    case R.id.main_menu_smart_dash_options:
                        sLogger.v("dash-options");
                        AnalyticsSupport.recordMenuSelection("dash-options");
                        if (this.mainOptionsMenu == null) {
                            this.mainOptionsMenu = new MainOptionsMenu(this.bus, this.vscrollComponent, this.presenter, this);
                        }
                        this.mainOptionsMenu.setMode(Mode.DASH);
                        this.activityTraySelection = selection.subPosition;
                        this.activityTraySelectionId = selection.subId;
                        this.presenter.loadMenu(this.mainOptionsMenu, MenuLevel.SUB_LEVEL, selection.pos, 0);
                        break;
                    case R.id.settings_menu_dial_update:
                        sLogger.v("dial update");
                        AnalyticsSupport.recordMenuSelection("dial_update_mm");
                        args = new Bundle();
                        args.putBoolean(UpdateReminderManager.EXTRA_UPDATE_REMINDER, false);
                        this.presenter.performSelectionAnimation(new ActionRunnable(this.bus, Screen.SCREEN_DIAL_UPDATE_CONFIRMATION, args, false));
                        break;
                    case R.id.settings_menu_software_update:
                        sLogger.v("software update");
                        AnalyticsSupport.recordMenuSelection("software_update_mm");
                        args = new Bundle();
                        args.putBoolean(UpdateReminderManager.EXTRA_UPDATE_REMINDER, false);
                        this.presenter.performSelectionAnimation(new ActionRunnable(this.bus, Screen.SCREEN_OTA_CONFIRMATION, args, false));
                        break;
                }
                break;
            case R.id.main_menu_glances:
                handleGlances();
                break;
            case R.id.main_menu_maps:
                sLogger.v("map");
                AnalyticsSupport.recordMenuSelection("hybrid_map");
                this.presenter.performSelectionAnimation(new ActionRunnable(this.bus, Screen.SCREEN_HYBRID_MAP));
                break;
            case R.id.main_menu_music:
                MusicManager musicManager = remoteDeviceManager.getMusicManager();
                if (!UISettings.isMusicBrowsingEnabled() || !musicManager.hasMusicCapabilities()) {
                    sLogger.v(MusicArtworkCacheTable.TYPE_MUSIC);
                    AnalyticsSupport.recordMenuSelection(MusicArtworkCacheTable.TYPE_MUSIC);
                    this.presenter.performSelectionAnimation(new ActionRunnable(this.bus, Screen.SCREEN_MUSIC));
                    break;
                }
                sLogger.v("music browse");
                AnalyticsSupport.recordMenuSelection(MusicArtworkCacheTable.TYPE_MUSIC);
                createChildMenu(Menu.MUSIC, null);
                this.presenter.loadMenu(this.musicMenu, MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
                break;
            case R.id.main_menu_places:
                sLogger.v("places");
                AnalyticsSupport.recordMenuSelection("places");
                createChildMenu(Menu.PLACES, null);
                this.presenter.loadMenu(this.placesMenu, MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            case R.id.main_menu_settings:
                sLogger.v("settings");
                AnalyticsSupport.recordMenuSelection("settings");
                createChildMenu(Menu.SETTINGS, null);
                this.presenter.loadMenu(this.settingsMenu, MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            case R.id.main_menu_settings_connect_phone:
                sLogger.v("connect phone");
                AnalyticsSupport.recordMenuSelection("connect_phone");
                args = new Bundle();
                args.putString(WelcomeScreen.ARG_ACTION, WelcomeScreen.ACTION_SWITCH_PHONE);
                this.presenter.performSelectionAnimation(new ActionRunnable(this.bus, Screen.SCREEN_WELCOME, args, false));
                break;
            case R.id.main_menu_smart_dash:
                sLogger.v("dash");
                AnalyticsSupport.recordMenuSelection("dash");
                this.presenter.performSelectionAnimation(new ActionRunnable(this.bus, Screen.SCREEN_DASHBOARD));
                break;
            case R.id.main_menu_voice:
                sLogger.v("voice-assist");
                AnalyticsSupport.recordMenuSelection("voice_assist");
                this.presenter.performSelectionAnimation(new ActionRunnable(this.bus, Screen.SCREEN_VOICE_CONTROL));
                break;
            case R.id.main_menu_voice_search:
                sLogger.v("voice_search");
                AnalyticsSupport.recordMenuSelection("voice_search");
                this.presenter.performSelectionAnimation(new Runnable() {
                    public void run() {
                        NotificationManager notificationManager = NotificationManager.getInstance();
                        VoiceSearchNotification voiceSearchNotification = (VoiceSearchNotification) notificationManager.getNotification(NotificationId.VOICE_SEARCH_NOTIFICATION_ID);
                        if (voiceSearchNotification == null) {
                            voiceSearchNotification = new VoiceSearchNotification();
                        }
                        notificationManager.addNotification(voiceSearchNotification);
                    }
                });
                break;
        }
        return true;
    }

    public Menu getType() {
        return Menu.MAIN;
    }

    public IMenu getChildMenu(IMenu parent, String args, String path) {
        sLogger.v("getChildMenu:" + args + HereManeuverDisplayBuilder.COMMA + path);
        Menu m = (Menu) childMenus.get(args);
        if (m != null) {
            return createChildMenu(m, path);
        }
        sLogger.v("getChildMenu: not found");
        return null;
    }

    public void onUnload(MenuLevel level) {
        handler.removeCallbacks(this.etaRunnable);
        if (this.registeredMusicCallback) {
            this.registeredMusicCallback = false;
            musicManager.removeMusicUpdateListener(this.musicUpdateListener);
            sLogger.v("unregister");
        }
        if (this.busRegistered) {
            this.bus.unregister(this);
            this.busRegistered = false;
            sLogger.v("bus unregistered");
        }
        clearNowPlayingState();
        if (level == MenuLevel.CLOSE) {
            if (this.placesMenu != null) {
                this.placesMenu.onUnload(MenuLevel.CLOSE);
            }
            if (this.contactsMenu != null) {
                this.contactsMenu.onUnload(MenuLevel.CLOSE);
            }
            if (this.musicMenu != null) {
                this.musicMenu.onUnload(MenuLevel.CLOSE);
            }
        }
    }

    public int getActivityTraySelection() {
        return this.activityTraySelection;
    }

    public void clearState() {
        this.settingsMenu = null;
        this.placesMenu = null;
        this.contactsMenu = null;
        this.mainOptionsMenu = null;
        this.activeTripMenu = null;
        this.musicMenu = null;
    }

    private IMenu createChildMenu(Menu menu, String path) {
        sLogger.v("createChildMenu:" + path);
        String element = null;
        if (path != null) {
            if (path.indexOf(HereManeuverDisplayBuilder.SLASH) == 0) {
                element = path.substring(1);
                int index = element.indexOf(HereManeuverDisplayBuilder.SLASH);
                if (index >= 0) {
                    path = path.substring(index + 1);
                    element = element.substring(0, index);
                } else {
                    path = null;
                }
            } else {
                path = null;
            }
        }
        IMenu child;
        switch (menu) {
            case CONTACTS:
                if (this.contactsMenu == null) {
                    this.contactsMenu = new ContactsMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                if (!TextUtils.isEmpty(element)) {
                    child = this.contactsMenu.getChildMenu(this, element, path);
                    if (child != null) {
                        return child;
                    }
                }
                this.contactsMenu.setBackSelectionId(R.id.main_contacts);
                return this.contactsMenu;
            case PLACES:
                if (this.placesMenu == null) {
                    this.placesMenu = new PlacesMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                if (!TextUtils.isEmpty(element)) {
                    child = this.placesMenu.getChildMenu(this, element, path);
                    if (child != null) {
                        return child;
                    }
                }
                this.placesMenu.setBackSelectionId(R.id.main_menu_places);
                return this.placesMenu;
            case SETTINGS:
                if (this.settingsMenu == null) {
                    this.settingsMenu = new SettingsMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                if (!TextUtils.isEmpty(element)) {
                    child = this.settingsMenu.getChildMenu(this, element, path);
                    if (child != null) {
                        return child;
                    }
                }
                this.settingsMenu.setBackSelectionId(R.id.main_menu_settings);
                return this.settingsMenu;
            case MUSIC:
                if (this.musicMenu == null) {
                    this.musicMenu = new MusicMenu2(this.bus, this.vscrollComponent, this.presenter, this, null);
                }
                this.musicMenu.setBackSelectionId(R.id.main_menu_music);
                if (!TextUtils.isEmpty(element)) {
                    child = this.musicMenu.getChildMenu(this, element, path);
                    if (child != null) {
                        return child;
                    }
                }
                return this.musicMenu;
            default:
                return null;
        }
    }

    private void fillEta() {
        try {
            String etaString = HereMapUtil.getCurrentEtaStringWithDestination();
            if (!TextUtils.isEmpty(etaString)) {
                this.currentEta = etaString;
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    @Subscribe
    public void arrivalEvent(ArrivalEvent event) {
        sLogger.v("arrival event");
        handler.removeCallbacks(this.etaRunnable);
        fillEta();
    }

    private boolean isLongPressActionPlaceSearch() {
        if (DriverProfileHelper.getInstance().getCurrentProfile().getLongPressAction() == DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH) {
            return true;
        }
        return false;
    }

    private Model getActivityTrayModel(boolean isConnected) {
        activityTrayIcon.clear();
        activityTrayIconId.clear();
        activityTraySelectedColor.clear();
        activityTrayUnSelectedColor.clear();
        boolean callAdded = false;
        if (isConnected && callManager.isCallInProgress()) {
            activityTrayIcon.add(Integer.valueOf(R.drawable.icon_mm_contacts_2));
            activityTrayIconId.add(Integer.valueOf(R.id.main_menu_call));
            activityTraySelectedColor.add(Integer.valueOf(resources.getColor(R.color.mm_connnect_phone)));
            activityTrayUnSelectedColor.add(Integer.valueOf(bkColorUnselected));
            callAdded = true;
        }
        handler.removeCallbacks(this.etaRunnable);
        boolean tripOptionSlot = false;
        if (HereMapsManager.getInstance().isInitialized() && HereNavigationManager.getInstance().isNavigationModeOn()) {
            sLogger.v("nav on adding trip menu");
            activityTrayIcon.add(Integer.valueOf(R.drawable.icon_badge_active_trip));
            activityTrayIconId.add(Integer.valueOf(R.id.main_menu_active_trip));
            activityTraySelectedColor.add(Integer.valueOf(resources.getColor(R.color.mm_active_trip)));
            activityTrayUnSelectedColor.add(Integer.valueOf(bkColorUnselected));
            fillEta();
            handler.postDelayed(this.etaRunnable, (long) ETA_UPDATE_INTERVAL);
            tripOptionSlot = true;
        }
        if (NotificationManager.getInstance().getNotificationCount() > 0) {
            activityTrayIcon.add(Integer.valueOf(R.drawable.icon_mm_glances_2));
            activityTrayIconId.add(Integer.valueOf(R.id.main_menu_glances));
            activityTraySelectedColor.add(Integer.valueOf(glancesColor));
            activityTrayUnSelectedColor.add(Integer.valueOf(bkColorUnselected));
        }
        if (isConnected && !callAdded) {
            MusicTrackInfo trackInfo = musicManager.getCurrentTrack();
            if (canShowTrackInfo(trackInfo)) {
                activityTrayIcon.add(Integer.valueOf(R.drawable.icon_mm_music_2));
                activityTrayIconId.add(Integer.valueOf(R.id.main_menu_now_playing));
                activityTraySelectedColor.add(Integer.valueOf(nowPlayingColor));
                activityTrayUnSelectedColor.add(Integer.valueOf(bkColorUnselected));
                this.addedTrackInfo = trackInfo;
                sLogger.v("added now playing");
            } else {
                sLogger.v("not added now playing");
            }
        }
        if (!tripOptionSlot && OTAUpdateService.isUpdateAvailable()) {
            this.swVersion = resources.getString(R.string.mm_navdy_update, new Object[]{OTAUpdateService.getSWUpdateVersion()});
            sLogger.v("add navdy s/w");
            activityTrayIcon.add(Integer.valueOf(R.drawable.icon_software_update_2));
            activityTrayIconId.add(Integer.valueOf(R.id.settings_menu_software_update));
            activityTraySelectedColor.add(Integer.valueOf(resources.getColor(R.color.mm_settings_update_display)));
            activityTrayUnSelectedColor.add(Integer.valueOf(bkColorUnselected));
            tripOptionSlot = true;
        }
        if (!tripOptionSlot) {
            DialManager dialManager = DialManager.getInstance();
            if (dialManager.isDialConnected() && dialManager.getDialFirmwareUpdater().getVersions().isUpdateAvailable()) {
                sLogger.v("add dial s/w");
                activityTrayIcon.add(Integer.valueOf(R.drawable.icon_dial_update_2));
                activityTrayIconId.add(Integer.valueOf(R.id.settings_menu_dial_update));
                activityTraySelectedColor.add(Integer.valueOf(resources.getColor(R.color.mm_settings_update_dial)));
                activityTrayUnSelectedColor.add(Integer.valueOf(bkColorUnselected));
            }
        }
        Screen screen = uiStateManager.getDefaultMainActiveScreen();
        boolean dashModeOn = false;
        boolean mapModeOn = false;
        if (screen != null && screen == Screen.SCREEN_HOME) {
            HomeScreenView homescreenView = uiStateManager.getHomescreenView();
            if (homescreenView != null) {
                switch (homescreenView.getDisplayMode()) {
                    case SMART_DASH:
                        dashModeOn = true;
                        break;
                    case MAP:
                        mapModeOn = true;
                        break;
                }
            }
        }
        if (dashModeOn) {
            activityTrayIcon.add(Integer.valueOf(R.drawable.icon_main_menu_dash_options));
            activityTrayIconId.add(Integer.valueOf(R.id.main_menu_smart_dash_options));
            activityTraySelectedColor.add(Integer.valueOf(resources.getColor(R.color.mm_dash_options)));
            activityTrayUnSelectedColor.add(Integer.valueOf(bkColorUnselected));
        } else if (mapModeOn) {
            activityTrayIcon.add(Integer.valueOf(R.drawable.icon_main_menu_map_options));
            activityTrayIconId.add(Integer.valueOf(R.id.main_menu_maps_options));
            activityTraySelectedColor.add(Integer.valueOf(resources.getColor(R.color.mm_map_options)));
            activityTrayUnSelectedColor.add(Integer.valueOf(bkColorUnselected));
        }
        int size = activityTrayIcon.size();
        int[] icons = new int[size];
        int[] iconIds = new int[size];
        int[] iconSelectedColor = new int[size];
        int[] iconUnselectedColor = new int[size];
        int counter = 0;
        for (int i = size - 1; i >= 0; i--) {
            icons[counter] = ((Integer) activityTrayIcon.get(i)).intValue();
            iconIds[counter] = ((Integer) activityTrayIconId.get(i)).intValue();
            iconSelectedColor[counter] = ((Integer) activityTraySelectedColor.get(i)).intValue();
            iconUnselectedColor[counter] = ((Integer) activityTrayUnSelectedColor.get(i)).intValue();
            counter++;
        }
        int selection = this.activityTraySelection;
        if (selection == -1) {
            selection = size - 1;
        }
        return IconOptionsViewHolder.buildModel(R.id.main_menu_activity_tray, icons, iconIds, iconSelectedColor, iconUnselectedColor, iconSelectedColor, selection, true);
    }

    private boolean isSameTrack(MusicTrackInfo oldTrackInfo, MusicTrackInfo newTrackInfo) {
        if (oldTrackInfo == null || newTrackInfo == null || !TextUtils.equals(oldTrackInfo.name, newTrackInfo.name) || !TextUtils.equals(oldTrackInfo.author, newTrackInfo.author)) {
            return false;
        }
        return true;
    }

    private boolean canShowTrackInfo(MusicTrackInfo musicTrackInfo) {
        if (musicTrackInfo == null || ((!MusicManager.tryingToPlay(musicTrackInfo.playbackState) && musicTrackInfo.playbackState != MusicPlaybackState.PLAYBACK_PAUSED) || TextUtils.isEmpty(musicTrackInfo.name))) {
            return false;
        }
        return true;
    }

    private void clearNowPlayingState() {
        this.addedTrackInfo = null;
    }

    private void handleGlances() {
        boolean isGlancedEnabled;
        sLogger.v("glances");
        AnalyticsSupport.recordMenuSelection("glances");
        Resources resources = HudApplication.getAppContext().getResources();
        NotificationPreferences prefs = DriverProfileHelper.getInstance().getCurrentProfile().getNotificationPreferences();
        if (prefs.enabled != null) {
            isGlancedEnabled = prefs.enabled.booleanValue();
        } else {
            isGlancedEnabled = false;
        }
        Bundle bundle;
        if (isGlancedEnabled) {
            NotificationManager notificationManager = NotificationManager.getInstance();
            if (notificationManager.makeNotificationCurrent(true)) {
                sLogger.d("glances available");
                AnalyticsSupport.setGlanceNavigationSource("dial");
                notificationManager.showNotification();
                return;
            }
            sLogger.d("no glances found");
            bundle = new Bundle();
            bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 1000);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_qm_glances_grey);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(R.string.glances_none));
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
            ToastManager.getInstance().addToast(new ToastParams("glance-none", bundle, null, true, false));
            this.presenter.performSelectionAnimation(new ActionRunnable(this.bus, Screen.SCREEN_BACK));
            return;
        }
        sLogger.v("glances are not enabled");
        bundle = new Bundle();
        bundle.putInt(ToastPresenter.EXTRA_CHOICE_LAYOUT_PADDING, (int) resources.getDimension(R.dimen.no_glances_choice_padding));
        bundle.putBoolean(ToastPresenter.EXTRA_DEFAULT_CHOICE, true);
        bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 5000);
        bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_glances_disabled_modal);
        bundle.putInt(ToastPresenter.EXTRA_SIDE_IMAGE, R.drawable.icon_disabled);
        bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(R.string.glances_disabled));
        bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_Disabled_1);
        bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_3, resources.getString(R.string.glances_disabled_msg));
        bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_3_STYLE, R.style.Glances_Disabled_2);
        bundle.putInt(ToastPresenter.EXTRA_INFO_CONTAINER_MAX_WIDTH, (int) resources.getDimension(R.dimen.no_glances_width));
        bundle.putString(ToastPresenter.EXTRA_TTS, TTSUtils.TTS_GLANCES_DISABLED);
        ToastManager.getInstance().addToast(new ToastParams("glance-disabled", bundle, null, true, false));
        this.presenter.performSelectionAnimation(new ActionRunnable(this.bus, Screen.SCREEN_BACK));
    }

    private String getToolTipString(int id) {
        switch (id) {
            case R.id.main_menu_active_trip:
                if (this.currentEta == null) {
                    return activeTripTitle;
                }
                return this.currentEta;
            case R.id.main_menu_call:
                CallNotification callNotification = (CallNotification) NotificationManager.getInstance().getNotification(NotificationId.PHONE_CALL_NOTIFICATION_ID);
                if (callNotification == null || !callManager.isCallInProgress() || callNotification.getCaller() == null) {
                    return null;
                }
                int time = callManager.getCurrentCallDuration();
                if (time < 0) {
                    return null;
                }
                String duration = getDurationStr((long) time);
                return resources.getString(R.string.mm_call_info, new Object[]{name, duration});
            case R.id.main_menu_glances:
                if (NotificationManager.getInstance().getNotificationCount() == 1) {
                    return resources.getString(R.string.mm_glances_single);
                }
                return resources.getString(R.string.mm_glances_subtitle, new Object[]{Integer.valueOf(notificationCount)});
            case R.id.main_menu_maps_options:
                return mapTitle;
            case R.id.main_menu_now_playing:
                return getMusicTrackInfo();
            case R.id.main_menu_smart_dash_options:
                return dashTitle;
            case R.id.settings_menu_dial_update:
                String updateTargetVersion = "1.0." + DialManager.getInstance().getDialFirmwareUpdater().getVersions().local.incrementalVersion;
                return resources.getString(R.string.mm_dial_update, new Object[]{updateTargetVersion});
            case R.id.settings_menu_software_update:
                return this.swVersion;
            default:
                return null;
        }
    }

    private String getDurationStr(long time) {
        int secs;
        if (time < 3600) {
            secs = (int) (time - ((long) ((((int) time) / 60) * 60)));
            return String.format("%02d:%02d", new Object[]{Integer.valueOf(((int) time) / 60), Integer.valueOf(secs)});
        }
        time = (long) (((int) time) - (((((int) time) / 3600) * 60) * 60));
        secs = (int) (time - ((long) ((((int) time) / 60) * 60)));
        return String.format("%02d:%02d:%02d", new Object[]{Integer.valueOf(hours), Integer.valueOf(((int) time) / 60), Integer.valueOf(secs)});
    }

    private String getMusicTrackInfo() {
        if (this.addedTrackInfo == null) {
            return null;
        }
        boolean isValidAuthor;
        boolean isValidName;
        String name = this.addedTrackInfo.name;
        String author = this.addedTrackInfo.author;
        if (TextUtils.isEmpty(author)) {
            isValidAuthor = false;
        } else {
            isValidAuthor = true;
        }
        if (TextUtils.isEmpty(name)) {
            isValidName = false;
        } else {
            isValidName = true;
        }
        if (isValidAuthor && isValidName) {
            return author + " - " + name;
        }
        if (isValidName) {
            return name;
        }
        return author;
    }

    private void startActivityTrayRunnable(int id, int pos) {
        this.curToolTipId = id;
        this.curToolTipPos = pos;
        this.activityTrayRunning = true;
        handler.postDelayed(this.activityTrayRunnable, (long) ACTIVITY_TRAY_UPDATE);
    }

    private void stopActivityTrayRunnable() {
        this.curToolTipId = -1;
        this.curToolTipPos = -1;
        this.activityTrayRunning = false;
        handler.removeCallbacks(this.activityTrayRunnable);
    }
}
