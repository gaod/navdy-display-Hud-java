package com.navdy.hud.app.ui.component.vlist;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.ViewGroup;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType;
import com.navdy.hud.app.ui.component.vlist.viewholder.BlankViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconOneViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.TitleSubtitleViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.TitleViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State;
import com.navdy.service.library.log.Logger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class VerticalAdapter extends Adapter<VerticalViewHolder> {
    private static final Logger sLogger = new Logger(VerticalAdapter.class);
    private List<Model> data;
    private Handler handler = new Handler(Looper.getMainLooper());
    private int highlightIndex = -1;
    private HashMap<VerticalViewHolder, Integer> holderToPosMap = new HashMap();
    private boolean initialState;
    private ModelState modelState = new ModelState();
    private HashMap<Integer, VerticalViewHolder> posToHolderMap = new HashMap();
    private HashSet<Integer> viewHolderCache = new HashSet();
    private VerticalList vlist;

    public VerticalAdapter(List<Model> data, VerticalList vlist) {
        this.data = data;
        this.vlist = vlist;
    }

    public VerticalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ModelType modelType = ModelType.values()[viewType];
        if (sLogger.isLoggable(2)) {
            sLogger.v("onCreateViewHolder: " + modelType);
        }
        switch (modelType) {
            case BLANK:
                return BlankViewHolder.buildViewHolder(parent, this.vlist, this.handler);
            case TITLE:
                return TitleViewHolder.buildViewHolder(parent, this.vlist, this.handler);
            case TITLE_SUBTITLE:
                return TitleSubtitleViewHolder.buildViewHolder(parent, this.vlist, this.handler);
            case ICON_BKCOLOR:
                return IconBkColorViewHolder.buildViewHolder(parent, this.vlist, this.handler);
            case TWO_ICONS:
                return IconsTwoViewHolder.buildViewHolder(parent, this.vlist, this.handler);
            case ICON:
                return IconOneViewHolder.buildViewHolder(parent, this.vlist, this.handler);
            case LOADING:
                return LoadingViewHolder.buildViewHolder(parent, this.vlist, this.handler);
            case ICON_OPTIONS:
                return IconOptionsViewHolder.buildViewHolder(parent, this.vlist, this.handler);
            case SCROLL_CONTENT:
                return ScrollableViewHolder.buildViewHolder(parent, this.vlist, this.handler);
            case LOADING_CONTENT:
                return ContentLoadingViewHolder.buildViewHolder(parent, this.vlist, this.handler);
            case SWITCH:
                return SwitchViewHolder.Companion.buildViewHolder(parent, this.vlist, this.handler);
            default:
                throw new RuntimeException("implement the model:" + modelType);
        }
    }

    public void onBindViewHolder(VerticalViewHolder holder, int position) {
        if (sLogger.isLoggable(2)) {
            sLogger.v("onBindViewHolder: {" + position + "}");
        }
        boolean highlightItem = false;
        clearPrevPos(holder);
        if (this.highlightIndex != -1) {
            if (this.highlightIndex == position) {
                highlightItem = true;
            }
            this.highlightIndex = -1;
        }
        if (this.viewHolderCache.contains(Integer.valueOf(position))) {
            this.holderToPosMap.put(holder, Integer.valueOf(position));
            this.posToHolderMap.put(Integer.valueOf(position), holder);
        }
        Model model = (Model) this.data.get(position);
        if (position != holder.getPos() || model.needsRebind) {
            boolean callBind = false;
            switch (holder.getModelType()) {
                case BLANK:
                    return;
                case ICON_BKCOLOR:
                case TWO_ICONS:
                case ICON:
                case SCROLL_CONTENT:
                    callBind = true;
                    break;
            }
            if (!model.fontSizeCheckDone) {
                VerticalList.setFontSize(model, this.vlist.allowsTwoLineTitles());
            }
            boolean rebind = model.needsRebind;
            boolean dontStartFluctuator = model.dontStartFluctuator;
            model.needsRebind = false;
            model.dontStartFluctuator = false;
            if (!rebind) {
                holder.clearAnimation();
            }
            holder.setExtras(model.extras);
            holder.setPos(position);
            this.modelState.reset();
            holder.preBind(model, this.modelState);
            if (this.vlist.bindCallbacks && callBind) {
                int userPos;
                if (this.vlist.firstEntryBlank) {
                    userPos = position - 1;
                } else {
                    userPos = position;
                }
                this.vlist.callback.onBindToView((Model) this.data.get(position), holder.layout, userPos, this.modelState);
            }
            holder.bind(model, this.modelState);
            if (this.initialState) {
                if (highlightItem || (rebind && this.vlist.getRawPosition() == position)) {
                    holder.setState(State.SELECTED, AnimationType.NONE, 0, !dontStartFluctuator);
                    return;
                } else {
                    holder.setState(State.UNSELECTED, AnimationType.NONE, 0);
                    return;
                }
            } else if (this.vlist.getRawPosition() == position) {
                this.initialState = true;
                holder.setState(State.SELECTED, AnimationType.INIT, 100);
                sLogger.v("initial state: position:" + position);
                return;
            } else {
                holder.setState(State.UNSELECTED, AnimationType.INIT, 100);
                return;
            }
        }
        sLogger.i("onBindViewHolder: already bind unselect it");
        holder.setState(State.UNSELECTED, AnimationType.NONE, 0);
    }

    public int getItemCount() {
        return this.data.size();
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public int getItemViewType(int position) {
        return ((Model) this.data.get(position)).type.ordinal();
    }

    public Model getModel(int pos) {
        if (pos < 0 || pos >= this.data.size()) {
            return null;
        }
        return (Model) this.data.get(pos);
    }

    public void updateModel(int pos, Model model) {
        if (pos >= 0 && pos < this.data.size()) {
            this.data.set(pos, model);
            sLogger.v("updated model:" + pos);
        }
    }

    public void onViewRecycled(VerticalViewHolder holder) {
        holder.clearAnimation();
        clearPrevPos(holder);
    }

    public boolean onFailedToRecycleView(VerticalViewHolder holder) {
        holder.clearAnimation();
        clearPrevPos(holder);
        return super.onFailedToRecycleView(holder);
    }

    public void setInitialState(boolean b) {
        this.initialState = b;
    }

    public void setViewHolderCacheIndex(int[] index) {
        this.viewHolderCache.clear();
        if (index != null) {
            for (int valueOf : index) {
                this.viewHolderCache.add(Integer.valueOf(valueOf));
            }
        }
    }

    private void clearPrevPos(VerticalViewHolder holder) {
        Integer prevPos = (Integer) this.holderToPosMap.remove(holder);
        if (prevPos != null) {
            sLogger.v("clearPrevPos removed:" + prevPos);
            this.posToHolderMap.remove(prevPos);
        }
    }

    public VerticalViewHolder getHolderForPos(int pos) {
        return (VerticalViewHolder) this.posToHolderMap.get(Integer.valueOf(pos));
    }

    public void setHighlightIndex(int n) {
        this.highlightIndex = n;
    }
}
