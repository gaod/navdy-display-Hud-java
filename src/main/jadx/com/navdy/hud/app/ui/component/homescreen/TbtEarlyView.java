package com.navdy.hud.app.ui.component.homescreen;

import android.animation.AnimatorSet.Builder;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;

public class TbtEarlyView extends LinearLayout implements IHomeScreenLifecycle {
    private static final String TBT_EARLY_SHRUNK_MODE_VISIBLE = "persist.sys.tbtearlyshrunk";
    private CustomAnimationMode currentMode;
    @InjectView(R.id.earlyManeuverImage)
    ImageView earlyManeuverImage;
    private HomeScreenView homeScreenView;
    private final boolean isShrunkVisible;
    private int lastNextTurnIconId;
    private Logger logger;
    private boolean paused;
    private UIStateManager uiStateManager;

    public TbtEarlyView(Context context) {
        this(context, null);
    }

    public TbtEarlyView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TbtEarlyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.lastNextTurnIconId = -1;
        this.isShrunkVisible = SystemProperties.getBoolean(TBT_EARLY_SHRUNK_MODE_VISIBLE, true);
    }

    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        RemoteDeviceManager remoteDeviceManager = RemoteDeviceManager.getInstance();
        this.uiStateManager = remoteDeviceManager.getUiStateManager();
        remoteDeviceManager.getBus().register(this);
    }

    private void setEarlyManeuver(int iconId) {
        if (this.lastNextTurnIconId != iconId) {
            this.lastNextTurnIconId = iconId;
            if (!this.paused) {
                if (this.lastNextTurnIconId != -1) {
                    this.earlyManeuverImage.setImageResource(this.lastNextTurnIconId);
                }
                showHideEarlyManeuver();
            }
        }
    }

    public void showHideEarlyManeuver() {
        if (this.lastNextTurnIconId != -1) {
            if (this.homeScreenView == null) {
                this.homeScreenView = this.uiStateManager.getHomescreenView();
            }
            boolean shrunkShouldHide = (this.currentMode == CustomAnimationMode.EXPAND || this.isShrunkVisible) ? false : true;
            if (shrunkShouldHide) {
                setVisibility(4);
                return;
            } else {
                setVisibility(0);
                return;
            }
        }
        setVisibility(4);
        this.earlyManeuverImage.setImageResource(0);
    }

    @Subscribe
    public void onManeuverDisplay(ManeuverDisplay event) {
        setEarlyManeuver(event.nextTurnIconId);
    }

    public void setViewXY() {
        setView(this.uiStateManager.getCustomAnimateMode());
    }

    public void setView(CustomAnimationMode mode) {
        this.currentMode = mode;
        switch (mode) {
            case EXPAND:
                setX((float) HomeScreenResourceValues.activeRoadEarlyManeuverX);
                setY((float) HomeScreenResourceValues.activeRoadEarlyManeuverY);
                return;
            case SHRINK_LEFT:
                setX((float) HomeScreenResourceValues.activeRoadEarlyManeuverShrinkLeftX);
                setY((float) HomeScreenResourceValues.activeRoadEarlyManeuverYShrunk);
                return;
            default:
                return;
        }
    }

    public void getCustomAnimator(CustomAnimationMode mode, Builder builder) {
        this.currentMode = mode;
        switch (mode) {
            case EXPAND:
                if (!this.isShrunkVisible) {
                    builder.with(HomeScreenUtils.getAlphaAnimator(this, 1));
                }
                builder.with(HomeScreenUtils.getWidthAnimator(this, HomeScreenResourceValues.activeRoadEarlyManeuverWidth));
                builder.with(HomeScreenUtils.getXPositionAnimator(this, (float) HomeScreenResourceValues.activeRoadEarlyManeuverX));
                builder.with(HomeScreenUtils.getYPositionAnimator(this, (float) HomeScreenResourceValues.activeRoadEarlyManeuverY));
                return;
            case SHRINK_LEFT:
                if (!this.isShrunkVisible) {
                    builder.with(HomeScreenUtils.getAlphaAnimator(this, 0));
                }
                builder.with(HomeScreenUtils.getWidthAnimator(this, HomeScreenResourceValues.activeRoadEarlyManeuverWidthShrunk));
                builder.with(HomeScreenUtils.getXPositionAnimator(this, (float) HomeScreenResourceValues.activeRoadEarlyManeuverShrinkLeftX));
                builder.with(HomeScreenUtils.getYPositionAnimator(this, (float) HomeScreenResourceValues.activeRoadEarlyManeuverYShrunk));
                return;
            default:
                return;
        }
    }

    public void clearState() {
        this.lastNextTurnIconId = -1;
        showHideEarlyManeuver();
    }

    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.logger.v("::onPause:tbtearly");
        }
    }

    public void onResume() {
        if (this.paused) {
            this.paused = false;
            this.logger.v("::onResume:tbtearly");
            int iconId = this.lastNextTurnIconId;
            if (iconId != -1) {
                this.logger.v("::onResume:tbtearly set last turn icon");
                this.lastNextTurnIconId = -1;
                setEarlyManeuver(iconId);
            }
        }
    }
}
