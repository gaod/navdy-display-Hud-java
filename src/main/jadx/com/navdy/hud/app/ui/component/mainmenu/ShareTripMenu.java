package com.navdy.hud.app.ui.component.mainmenu;

import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.mapping.MapMarker;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.contacts.Contact;
import com.navdy.hud.app.framework.glympse.GlympseManager;
import com.navdy.hud.app.framework.glympse.GlympseManager.Error;
import com.navdy.hud.app.framework.glympse.GlympseNotification;
import com.navdy.hud.app.framework.glympse.GlympseNotification.Type;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.DestinationPickerState;
import com.navdy.hud.app.ui.component.destination.IDestinationPicker;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.log.Logger;

class ShareTripMenu implements IDestinationPicker {
    private static final int SEND_WITHOUT_MESSAGE_POS = 0;
    private static final Logger logger = new Logger(ShareTripMenu.class);
    private final Contact contact;
    private String failedDestinationLabel;
    private double failedLatitude;
    private double failedLongitude;
    private String failedMessage;
    private final GlympseManager glympseManager = GlympseManager.getInstance();
    private boolean hasFailed;
    private boolean itemSelected = false;
    private final String notificationId;

    ShareTripMenu(Contact contact, String notificationId) {
        this.contact = contact;
        this.notificationId = notificationId;
        this.hasFailed = false;
    }

    public boolean onItemClicked(int id, int pos, DestinationPickerState state) {
        String message = GlympseManager.getInstance().getMessages()[pos];
        logger.v("selected option: " + message);
        AnalyticsSupport.recordMenuSelection("glympse_menu_" + message);
        this.itemSelected = true;
        if (pos == 0) {
            sendShare(this.contact);
        } else {
            sendShare(this.contact, message);
        }
        return true;
    }

    public boolean onItemSelected(int id, int pos, DestinationPickerState state) {
        return true;
    }

    public void onDestinationPickerClosed() {
        if (!this.itemSelected) {
            AnalyticsSupport.recordMenuSelection("back");
        }
        if (this.hasFailed) {
            addGlympseOfflineGlance(this.failedMessage, this.contact, this.failedDestinationLabel, this.failedLatitude, this.failedLongitude);
        }
    }

    private void addGlympseOfflineGlance(String message, Contact contact, String destinationLabel, double latitude, double longitude) {
        NotificationManager.getInstance().addNotification(new GlympseNotification(contact, Type.OFFLINE, message, destinationLabel, latitude, longitude));
    }

    private void sendShare(Contact contact) {
        sendShare(contact, null);
    }

    private void sendShare(Contact contact, String message) {
        String shareType;
        HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
        String destinationLabel = null;
        double latitude = 0.0d;
        double longitude = 0.0d;
        if (this.notificationId != null) {
            NotificationManager.getInstance().removeNotification(this.notificationId);
        }
        GeoCoordinate geoCoordinate = null;
        if (hereNavigationManager.isNavigationModeOn()) {
            shareType = GlympseManager.GLYMPSE_TYPE_SHARE_TRIP;
            destinationLabel = hereNavigationManager.getDestinationLabel();
            MapMarker destinationMarker = hereNavigationManager.getDestinationMarker();
            if (destinationMarker != null) {
                geoCoordinate = destinationMarker.getCoordinate();
            } else {
                NavigationRouteRequest navigationRouteRequest = hereNavigationManager.getCurrentNavigationRouteRequest();
                if (!(navigationRouteRequest == null || navigationRouteRequest.destination == null)) {
                    geoCoordinate = new GeoCoordinate(navigationRouteRequest.destination.latitude.doubleValue(), navigationRouteRequest.destination.longitude.doubleValue());
                }
            }
        } else {
            shareType = "Location";
        }
        if (geoCoordinate != null) {
            latitude = geoCoordinate.getLatitude();
            longitude = geoCoordinate.getLongitude();
        }
        StringBuilder builder = new StringBuilder();
        this.hasFailed = this.glympseManager.addMessage(contact, message, destinationLabel, latitude, longitude, builder) != Error.NONE;
        if (this.hasFailed) {
            this.failedMessage = message;
            this.failedDestinationLabel = destinationLabel;
            this.failedLatitude = latitude;
            this.failedLongitude = longitude;
        }
        AnalyticsSupport.recordGlympseSent(!this.hasFailed, shareType, message, builder.toString());
        logger.v("Glympse: success= " + (!this.hasFailed) + "share=" + shareType);
    }
}
