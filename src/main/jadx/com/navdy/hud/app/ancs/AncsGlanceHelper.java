package com.navdy.hud.app.ancs;

import android.text.TextUtils;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.component.UISettings;
import com.navdy.service.library.events.glances.CalendarConstants;
import com.navdy.service.library.events.glances.EmailConstants;
import com.navdy.service.library.events.glances.GenericConstants;
import com.navdy.service.library.events.glances.GlanceEvent;
import com.navdy.service.library.events.glances.GlanceEvent.Builder;
import com.navdy.service.library.events.glances.GlanceEvent.GlanceActions;
import com.navdy.service.library.events.glances.GlanceEvent.GlanceType;
import com.navdy.service.library.events.glances.KeyValue;
import com.navdy.service.library.events.glances.MessageConstants;
import com.navdy.service.library.events.glances.SocialConstants;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

public class AncsGlanceHelper {
    private static Logger sLogger = AncsServiceConnector.sLogger;

    public static GlanceEvent buildGoogleCalendarEvent(String appId, String id, String title, String subtitle, String message, Date time) {
        if (TextUtils.isEmpty(message)) {
            return null;
        }
        String meetingTitle = null;
        String meetingTime = null;
        String meetingLocation = null;
        StringTokenizer tokenizer = new StringTokenizer(message, GlanceConstants.NEWLINE);
        int counter = 0;
        while (tokenizer.hasMoreElements()) {
            String text = (String) tokenizer.nextElement();
            switch (counter) {
                case 0:
                    meetingTitle = text.trim();
                    break;
                case 1:
                    meetingTime = text.trim();
                    break;
                case 2:
                    meetingLocation = text.trim();
                    break;
                default:
                    break;
            }
            counter++;
        }
        sLogger.v("[ancs-gcalendar] title[" + meetingTitle + "] when[" + meetingTime + "] location[" + meetingLocation + "]");
        String notifId = GlanceHelper.getId();
        List<KeyValue> data = new ArrayList();
        if (meetingTitle != null) {
            data.add(new KeyValue(CalendarConstants.CALENDAR_TITLE.name(), meetingTitle));
        }
        if (meetingTime != null) {
            data.add(new KeyValue(CalendarConstants.CALENDAR_TIME_STR.name(), meetingTime));
        }
        data.add(new KeyValue(CalendarConstants.CALENDAR_TIME.name(), String.valueOf(time.getTime())));
        if (meetingLocation != null) {
            data.add(new KeyValue(CalendarConstants.CALENDAR_LOCATION.name(), meetingLocation));
        }
        return new Builder().glanceType(GlanceType.GLANCE_TYPE_CALENDAR).provider(appId).id(notifId).postTime(Long.valueOf(time.getTime())).glanceData(data).build();
    }

    public static GlanceEvent buildGoogleMailEvent(String appId, String id, String title, String subtitle, String message, Date time) {
        if (TextUtils.isEmpty(message)) {
            return null;
        }
        String body;
        String from = null;
        boolean isEmailAddress = false;
        String subject = null;
        int index = message.indexOf(GlanceConstants.NEWLINE);
        if (index != -1) {
            from = message.substring(0, index).trim();
            if (from.indexOf(GlanceConstants.AT_SEPARATOR) != -1) {
                isEmailAddress = true;
            }
            message = message.substring(index + 1);
        }
        index = message.indexOf(GlanceConstants.GMAIL_SEPARATOR);
        if (index != -1) {
            subject = message.substring(0, index).trim();
            body = message.substring(index + 1).trim();
        } else {
            body = message.trim();
        }
        sLogger.v("[ancs-gmail] from[" + from + "] subject[" + subject + "] body[" + body + "]");
        String notifId = GlanceHelper.getId();
        List<KeyValue> data = new ArrayList();
        if (from != null) {
            if (isEmailAddress) {
                data.add(new KeyValue(EmailConstants.EMAIL_FROM_EMAIL.name(), from));
            } else {
                data.add(new KeyValue(EmailConstants.EMAIL_FROM_NAME.name(), from));
            }
        }
        if (subject != null) {
            data.add(new KeyValue(EmailConstants.EMAIL_SUBJECT.name(), subject));
        }
        if (body != null) {
            data.add(new KeyValue(EmailConstants.EMAIL_BODY.name(), body));
        }
        return new Builder().glanceType(GlanceType.GLANCE_TYPE_EMAIL).provider(appId).id(notifId).postTime(Long.valueOf(time.getTime())).glanceData(data).build();
    }

    public static GlanceEvent buildGenericMailEvent(String appId, String id, String title, String subtitle, String message, Date time) {
        if (!TextUtils.isEmpty(message) && GlanceHelper.MICROSOFT_OUTLOOK.equals(appId)) {
            return buildOutlookMailEvent(appId, id, title, subtitle, message, time);
        }
        return null;
    }

    public static GlanceEvent buildOutlookMailEvent(String appId, String id, String title, String subtitle, String message, Date time) {
        String from = title;
        String subject = null;
        String body = null;
        if (!TextUtils.isEmpty(message)) {
            String[] fields = message.split(GlanceConstants.NEWLINE);
            if (fields.length > 1) {
                subject = fields[0];
                body = fields[1];
            } else {
                body = fields[0];
            }
        }
        sLogger.v("[ancs-outlook-email] from[" + from + "] subject[" + subject + "] body[" + body + "]");
        String notifId = GlanceHelper.getId();
        List<KeyValue> data = new ArrayList();
        if (!TextUtils.isEmpty(from)) {
            if (from.contains(GlanceConstants.AT_SEPARATOR)) {
                data.add(new KeyValue(EmailConstants.EMAIL_FROM_EMAIL.name(), from));
            } else {
                data.add(new KeyValue(EmailConstants.EMAIL_FROM_NAME.name(), from));
            }
        }
        if (!TextUtils.isEmpty(subject)) {
            data.add(new KeyValue(EmailConstants.EMAIL_SUBJECT.name(), subject));
        }
        if (!TextUtils.isEmpty(body)) {
            data.add(new KeyValue(EmailConstants.EMAIL_BODY.name(), body));
        }
        return new Builder().glanceType(GlanceType.GLANCE_TYPE_EMAIL).provider(appId).id(notifId).postTime(Long.valueOf(time.getTime())).glanceData(data).build();
    }

    public static GlanceEvent buildGoogleHangoutEvent(String appId, String id, String title, String subtitle, String message, Date time) {
        return buildMessageEvent(appId, message, time);
    }

    public static GlanceEvent buildSlackEvent(String appId, String id, String title, String subtitle, String message, Date time) {
        return buildMessageEvent(appId, message, time);
    }

    public static GlanceEvent buildWhatsappEvent(String appId, String id, String title, String subtitle, String message, Date time) {
        String notifId = GlanceHelper.getId();
        List<KeyValue> data = new ArrayList();
        data.add(new KeyValue(MessageConstants.MESSAGE_FROM.name(), title));
        data.add(new KeyValue(MessageConstants.MESSAGE_BODY.name(), message));
        return new Builder().glanceType(GlanceType.GLANCE_TYPE_MESSAGE).provider(appId).id(notifId).postTime(Long.valueOf(time.getTime())).glanceData(data).build();
    }

    public static GlanceEvent buildFacebookMessengerEvent(String appId, String id, String title, String subtitle, String message, Date time) {
        return buildMessageEvent(appId, message, time);
    }

    public static GlanceEvent buildFacebookEvent(String appId, String id, String title, String subtitle, String message, Date time) {
        String notifId = GlanceHelper.getId();
        List<KeyValue> data = new ArrayList();
        data.add(new KeyValue(SocialConstants.SOCIAL_MESSAGE.name(), message));
        return new Builder().glanceType(GlanceType.GLANCE_TYPE_SOCIAL).provider(appId).id(notifId).postTime(Long.valueOf(time.getTime())).glanceData(data).build();
    }

    public static GlanceEvent buildTwitterEvent(String appId, String id, String title, String subtitle, String message, Date time) {
        if (TextUtils.isEmpty(message)) {
            return null;
        }
        String from = null;
        String to = null;
        String body = null;
        int index = message.indexOf(GlanceConstants.COLON_SEPARATOR);
        String notifId = GlanceHelper.getId();
        List<KeyValue> data = new ArrayList();
        if (index != -1) {
            from = message.substring(0, index).trim();
            if (TextUtils.isEmpty(from)) {
                from = title;
            } else {
                int i = from.indexOf(GlanceConstants.AT_SEPARATOR);
                if (i != -1) {
                    from = from.substring(i).trim();
                }
            }
            message = message.substring(index + 1).trim();
            index = message.indexOf(" ");
            if (index != -1) {
                to = message.substring(0, index).trim();
                body = message.substring(index + 1).trim();
            } else {
                body = message.trim();
            }
        } else {
            index = message.indexOf(GlanceConstants.NEWLINE);
            if (index == -1) {
                index = message.indexOf(" ");
                if (index != -1) {
                    to = message.substring(0, index).trim();
                    body = message.substring(index + 1).trim();
                } else {
                    body = message.trim();
                }
            } else if (index != -1) {
                from = message.substring(0, index).trim();
                body = message.substring(index + 1).trim();
            }
        }
        if (from != null) {
            data.add(new KeyValue(SocialConstants.SOCIAL_FROM.name(), from));
        }
        if (to != null) {
            data.add(new KeyValue(SocialConstants.SOCIAL_TO.name(), to));
        }
        if (body != null) {
            data.add(new KeyValue(SocialConstants.SOCIAL_MESSAGE.name(), body));
        } else {
            data.add(new KeyValue(SocialConstants.SOCIAL_MESSAGE.name(), message));
        }
        return new Builder().glanceType(GlanceType.GLANCE_TYPE_SOCIAL).provider(appId).id(notifId).postTime(Long.valueOf(time.getTime())).glanceData(data).build();
    }

    public static GlanceEvent buildiMessageEvent(String appId, String id, String title, String subtitle, String message, Date time) {
        String notifId = GlanceHelper.getId();
        List<KeyValue> data = new ArrayList();
        if (ContactUtil.isValidNumber(title)) {
            data.add(new KeyValue(MessageConstants.MESSAGE_FROM_NUMBER.name(), title));
        } else {
            data.add(new KeyValue(MessageConstants.MESSAGE_FROM.name(), title));
        }
        data.add(new KeyValue(MessageConstants.MESSAGE_BODY.name(), message));
        Builder iMessageEventBuilder = new Builder().glanceType(GlanceType.GLANCE_TYPE_MESSAGE).provider(appId).id(notifId).postTime(Long.valueOf(time.getTime())).glanceData(data);
        if (UISettings.supportsIosSms()) {
            List<GlanceActions> action = new ArrayList(1);
            action.add(GlanceActions.REPLY);
            iMessageEventBuilder.actions(action);
        }
        return iMessageEventBuilder.build();
    }

    public static GlanceEvent buildAppleMailEvent(String appId, String id, String title, String subtitle, String message, Date time) {
        String from = title;
        String subject = subtitle;
        String body = message;
        boolean isEmailAddress = false;
        if (from.indexOf(GlanceConstants.AT_SEPARATOR) != -1) {
            isEmailAddress = true;
        }
        String notifId = GlanceHelper.getId();
        List<KeyValue> data = new ArrayList();
        if (isEmailAddress) {
            data.add(new KeyValue(EmailConstants.EMAIL_FROM_EMAIL.name(), from));
        } else {
            data.add(new KeyValue(EmailConstants.EMAIL_FROM_NAME.name(), from));
        }
        if (subject != null) {
            data.add(new KeyValue(EmailConstants.EMAIL_SUBJECT.name(), subject));
        }
        if (body != null) {
            if (body.toLowerCase().contains(GlanceConstants.INVALID_APPLE_MAIL_GLANCE_BODY)) {
                sLogger.v("invalid apple mail glance:" + body);
                return null;
            }
            data.add(new KeyValue(EmailConstants.EMAIL_BODY.name(), body));
        }
        return new Builder().glanceType(GlanceType.GLANCE_TYPE_EMAIL).provider(appId).id(notifId).postTime(Long.valueOf(time.getTime())).glanceData(data).build();
    }

    public static GlanceEvent buildAppleCalendarEvent(String appId, String id, String title, String subtitle, String message, Date time) {
        String notifId = GlanceHelper.getId();
        List<KeyValue> data = new ArrayList();
        if (title != null) {
            data.add(new KeyValue(CalendarConstants.CALENDAR_TITLE.name(), title));
        }
        if (message != null) {
            data.add(new KeyValue(CalendarConstants.CALENDAR_LOCATION.name(), message));
        }
        data.add(new KeyValue(CalendarConstants.CALENDAR_TIME_STR.name(), RemoteDeviceManager.getInstance().getTimeHelper().formatTime(time, null)));
        data.add(new KeyValue(CalendarConstants.CALENDAR_TIME.name(), String.valueOf(time.getTime())));
        return new Builder().glanceType(GlanceType.GLANCE_TYPE_CALENDAR).provider(appId).id(notifId).postTime(Long.valueOf(System.currentTimeMillis())).glanceData(data).build();
    }

    public static GlanceEvent buildGenericEvent(String appId, String id, String title, String subtitle, String message, Date time) {
        String notifId = GlanceHelper.getId();
        List<KeyValue> data = new ArrayList();
        if (title != null) {
            data.add(new KeyValue(GenericConstants.GENERIC_TITLE.name(), title));
        }
        if (message != null) {
            data.add(new KeyValue(GenericConstants.GENERIC_MESSAGE.name(), message));
        }
        return new Builder().glanceType(GlanceType.GLANCE_TYPE_GENERIC).provider(appId).id(notifId).postTime(Long.valueOf(time.getTime())).glanceData(data).build();
    }

    private static GlanceEvent buildMessageEvent(String appId, String message, Date time) {
        if (TextUtils.isEmpty(message)) {
            return null;
        }
        String body;
        String from = null;
        int index = message.indexOf(GlanceConstants.COLON_SEPARATOR);
        if (index != -1) {
            from = message.substring(0, index).trim();
            body = message.substring(index + 1).trim();
        } else {
            body = message.trim();
        }
        String notifId = GlanceHelper.getId();
        List<KeyValue> data = new ArrayList();
        data.add(new KeyValue(MessageConstants.MESSAGE_FROM.name(), from));
        data.add(new KeyValue(MessageConstants.MESSAGE_BODY.name(), body));
        return new Builder().glanceType(GlanceType.GLANCE_TYPE_MESSAGE).provider(appId).id(notifId).postTime(Long.valueOf(time.getTime())).glanceData(data).build();
    }
}
