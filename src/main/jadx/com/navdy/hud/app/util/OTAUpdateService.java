package com.navdy.hud.app.util;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build.VERSION;
import android.os.IBinder;
import android.os.RecoverySystem;
import android.os.RecoverySystem.ProgressListener;
import android.text.TextUtils;
import com.navdy.hud.app.BuildConfig;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.event.Shutdown.Reason;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.obd.ObdDeviceConfigurationManager;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.service.library.events.file.FileType;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Bus;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.security.GeneralSecurityException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.inject.Inject;
import mortar.Mortar;

public class OTAUpdateService extends Service {
    public static final int CAPTURE_GROUP_FROM_VERSION = 2;
    public static final int CAPTURE_GROUP_TARGET_VERSION = 3;
    public static final String COMMAND_CHECK_UPDATE_ON_REBOOT = "CHECK_UPDATE";
    public static final String COMMAND_DO_NOT_PROMPT = "DO_NOT_PROMPT";
    public static final String COMMAND_INSTALL_UPDATE = "INSTALL_UPDATE";
    public static final String COMMAND_INSTALL_UPDATE_REBOOT_QUIET = "INSTALL_UPDATE_REBOOT_QUIET";
    public static final String COMMAND_INSTALL_UPDATE_SHUTDOWN = "INSTALL_UPDATE_SHUTDOWN";
    public static final String COMMAND_VERIFY_UPDATE = "VERIFY_UPDATE";
    public static final String DO_NOT_PROMPT = "do_not_prompt";
    public static final String EXTRA_COMMAND = "COMMAND";
    private static final String FAILED_OTA_FILE = "failed_ota_file";
    private static final long MAX_FILE_SIZE = 2147483648L;
    public static final int MAX_RETRY_COUNT = 3;
    private static final int MAX_ZIP_ENTRIES = 1000;
    public static final String RECOVERY_LAST_INSTALL_FILE_PATH = "/cache/recovery/last_install";
    private static final String RECOVERY_LAST_LOG_FILE_PATH = "/cache/recovery/last_log";
    public static final String RETRY_COUNT = "retry_count";
    public static final String UPDATE_FILE = "last_update_file_received";
    private static final String UPDATE_FROM_VERSION = "update_from_version";
    public static final String VERIFIED = "verified";
    public static final String VERSION_PREFIX = "1.0.";
    private static final Pattern sFileNamePattern = Pattern.compile("^navdy_ota_(([0-9]+)_to_)?([0-9]+)\\.zip");
    private static final Logger sLogger = new Logger(OTAUpdateService.class);
    private static String swVersion;
    @Inject
    Bus bus;
    private boolean installing;
    @Inject
    SharedPreferences sharedPreferences;

    public static class IncrementalOTAFailureDetected {
    }

    public static class InstallingUpdate {
    }

    public static class OTADownloadIntentsReceiver extends BroadcastReceiver {
        private SharedPreferences sharedPreferences;

        public OTADownloadIntentsReceiver(SharedPreferences prefs) {
            this.sharedPreferences = prefs;
        }

        public void onReceive(final Context context, Intent intent) {
            final String path = intent.getStringExtra("path");
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    File file = new File(path);
                    if (OTAUpdateService.isOtaFile(file.getName())) {
                        String oldUpdateFile = OTADownloadIntentsReceiver.this.sharedPreferences.getString(OTAUpdateService.UPDATE_FILE, null);
                        String newFileName = file.getAbsolutePath();
                        OTADownloadIntentsReceiver.this.sharedPreferences.edit().putString(OTAUpdateService.UPDATE_FILE, newFileName).remove(OTAUpdateService.DO_NOT_PROMPT).remove(OTAUpdateService.RETRY_COUNT).remove(OTAUpdateService.VERIFIED).commit();
                        if (!newFileName.equals(oldUpdateFile)) {
                            if (oldUpdateFile != null) {
                                IOUtils.deleteFile(context, oldUpdateFile);
                                OTAUpdateService.sLogger.d("Delete the old update file " + oldUpdateFile + ", new update :" + newFileName);
                            }
                            AnalyticsSupport.recordDownloadOTAUpdate(OTADownloadIntentsReceiver.this.sharedPreferences);
                        }
                        OTAUpdateService.startServiceToVerifyUpdate();
                    }
                }
            }, 1);
        }
    }

    public static class OTAFailedException extends Exception {
        public String last_install;
        public String last_log;

        OTAFailedException(String install, String log) {
            super("OTA installation failed");
            this.last_install = install;
            this.last_log = log;
        }
    }

    public static class UpdateVerified {
    }

    public void onCreate() {
        super.onCreate();
        Mortar.inject(HudApplication.getAppContext(), this);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        String command = intent.getStringExtra("COMMAND");
        sLogger.d("OTA Update service started command[" + command + "]");
        if (intent.hasExtra("COMMAND")) {
            boolean z = true;
            switch (command.hashCode()) {
                case -2036275187:
                    if (command.equals(COMMAND_INSTALL_UPDATE)) {
                        z = true;
                        break;
                    }
                    break;
                case -1672139796:
                    if (command.equals(COMMAND_INSTALL_UPDATE_REBOOT_QUIET)) {
                        z = false;
                        break;
                    }
                    break;
                case 227671624:
                    if (command.equals(COMMAND_INSTALL_UPDATE_SHUTDOWN)) {
                        z = true;
                        break;
                    }
                    break;
                case 1813668687:
                    if (command.equals(COMMAND_VERIFY_UPDATE)) {
                        z = true;
                        break;
                    }
                    break;
                case 1870386340:
                    if (command.equals(COMMAND_DO_NOT_PROMPT)) {
                        z = true;
                        break;
                    }
                    break;
            }
            switch (z) {
                case false:
                    update("--reboot_quiet_after");
                    break;
                case true:
                    update("--shutdown_after");
                    break;
                case true:
                    update(null);
                    break;
                case true:
                    this.sharedPreferences.edit().putBoolean(DO_NOT_PROMPT, true).commit();
                    break;
                case true:
                    checkForUpdate();
                    cleanupOldFiles();
                    break;
                default:
                    sLogger.e("onStartCommand() invalid command: " + command);
                    break;
            }
        }
        return 2;
    }

    private void installPackage(Context context, File file, String postUpdateCommand) throws IOException {
        AnalyticsSupport.recordShutdown(Reason.OTA, false);
        try {
            RecoverySystem.class.getMethod("installPackage", new Class[]{Context.class, File.class, String.class}).invoke(null, new Object[]{context, file, postUpdateCommand});
        } catch (InvocationTargetException ie) {
            Throwable e = ie.getCause();
            if (e instanceof IOException) {
                throw ((IOException) e);
            }
            sLogger.e("unexpected exception from RecoverySystem.installPackage()" + e);
        } catch (Exception e2) {
            sLogger.e("error invoking Recovery.installPackage(): " + e2);
            RecoverySystem.installPackage(context, file);
        }
    }

    private void update(final String postUpdateCommand) {
        if (!this.installing) {
            this.installing = true;
            this.bus.post(new InstallingUpdate());
            TaskManager.getInstance().execute(new Runnable() {
                /* JADX WARNING: inconsistent code. */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    try {
                        OTAUpdateService.sLogger.d("Applying the update");
                        File file = OTAUpdateService.checkUpdateFile(OTAUpdateService.this, OTAUpdateService.this.sharedPreferences);
                        if (file != null) {
                            try {
                                int retryCount = OTAUpdateService.this.sharedPreferences.getInt(OTAUpdateService.RETRY_COUNT, 0);
                                OTAUpdateService.this.sharedPreferences.edit().putInt(OTAUpdateService.RETRY_COUNT, retryCount + 1).putString(OTAUpdateService.UPDATE_FROM_VERSION, VERSION.INCREMENTAL).commit();
                                OTAUpdateService.sLogger.d("Trying to the install the update, retry count :" + (retryCount + 1));
                                AnalyticsSupport.recordInstallOTAUpdate(OTAUpdateService.this.sharedPreferences);
                                CrashReporter.getInstance().stopCrashReporting(true);
                            } catch (Throwable e) {
                                OTAUpdateService.sLogger.e("Exception while running update OTA package", e);
                                CrashReporter.getInstance().reportNonFatalException(e);
                                OTAUpdateService.this.installing = false;
                                return;
                            }
                            OTAUpdateService.sLogger.i(HudApplication.NOT_A_CRASH);
                            ObdDeviceConfigurationManager.turnOffOBDSleep();
                            DialManager.getInstance().requestDialReboot(false);
                            Thread.sleep(2000);
                            OTAUpdateService.this.installPackage(OTAUpdateService.this, file, postUpdateCommand);
                        } else {
                            OTAUpdateService.clearUpdate(file, OTAUpdateService.this, OTAUpdateService.this.sharedPreferences);
                        }
                        OTAUpdateService.this.installing = false;
                    } catch (Exception e2) {
                        OTAUpdateService.sLogger.e("Failed to install OTA package", e2);
                    } catch (Throwable th) {
                        OTAUpdateService.this.installing = false;
                    }
                }
            }, 1);
        }
    }

    public void checkForUpdate() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                OTAUpdateService.sLogger.d("Checking the update");
                File file = OTAUpdateService.checkUpdateFile(OTAUpdateService.this, OTAUpdateService.this.sharedPreferences);
                if (file == null) {
                    OTAUpdateService.sLogger.e("No update file found");
                    OTAUpdateService.this.checkIfIncrementalUpdatesFailed(null);
                    OTAUpdateService.clearUpdate(file, OTAUpdateService.this, OTAUpdateService.this.sharedPreferences);
                    return;
                }
                boolean currentUpdateValid = OTAUpdateService.this.checkIfIncrementalUpdatesFailed(file);
                OTAUpdateService.sLogger.d("Current update valid ? :" + currentUpdateValid);
                if (currentUpdateValid) {
                    if (!OTAUpdateService.isOTAUpdateVerified(OTAUpdateService.this.sharedPreferences)) {
                        if (OTAUpdateService.this.bVerifyUpdate(file, OTAUpdateService.this.sharedPreferences)) {
                            OTAUpdateService.this.sharedPreferences.edit().putBoolean(OTAUpdateService.VERIFIED, true).commit();
                            OTAUpdateService.this.bus.post(new UpdateVerified());
                        } else {
                            OTAUpdateService.clearUpdate(file, OTAUpdateService.this, OTAUpdateService.this.sharedPreferences);
                            return;
                        }
                    }
                    OTAUpdateService.swVersion = OTAUpdateService.getUpdateVersion(OTAUpdateService.this.sharedPreferences);
                    OTAUpdateService.sLogger.v("verified swVersion:" + OTAUpdateService.swVersion);
                    return;
                }
                OTAUpdateService.clearUpdate(file, OTAUpdateService.this, OTAUpdateService.this.sharedPreferences);
            }
        }, 1);
    }

    private void reportOTAFailure(String updateFileName) {
        if (updateFileName.equals(this.sharedPreferences.getString(FAILED_OTA_FILE, ""))) {
            sLogger.v("update failure already reported for " + updateFileName);
            return;
        }
        String last_install = "";
        String last_log = "";
        try {
            last_install = "\n========== /cache/recovery/last_install\n" + IOUtils.convertFileToString(RECOVERY_LAST_INSTALL_FILE_PATH);
        } catch (IOException e) {
            last_install = "\n*** failed to read /cache/recovery/last_install";
            sLogger.e("exception reading /cache/recovery/last_install", e);
        }
        try {
            last_log = "\n========== /cache/recovery/last_log\n" + IOUtils.convertFileToString(RECOVERY_LAST_LOG_FILE_PATH);
        } catch (IOException e2) {
            last_log = "\n*** failed to read /cache/recovery/last_log";
            sLogger.e("exception reading /cache/recovery/last_log", e2);
        }
        final OTAFailedException exception = new OTAFailedException(last_install, last_log);
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                CrashReporter.getInstance().reportOTAFailure(exception);
            }
        }, 1);
        this.sharedPreferences.edit().putString(FAILED_OTA_FILE, updateFileName).commit();
    }

    private boolean checkIfIncrementalUpdatesFailed(File currentUpdate) {
        File lastInstallFile = new File(RECOVERY_LAST_INSTALL_FILE_PATH);
        if (!lastInstallFile.exists()) {
            return true;
        }
        try {
            String lastInstallInfo = IOUtils.convertFileToString(lastInstallFile.getAbsolutePath());
            if (TextUtils.isEmpty(lastInstallInfo)) {
                return true;
            }
            String[] lines = lastInstallInfo.split(GlanceConstants.NEWLINE);
            if (lines.length < 2) {
                return true;
            }
            String fileName = lines[0];
            sLogger.d("Last installed update was " + fileName);
            String updateBaseName = new File(fileName).getName();
            boolean lastInstallSucceeded = Integer.parseInt(lines[1]) != 0;
            if (!lastInstallSucceeded) {
                reportOTAFailure(updateBaseName);
            }
            sLogger.d("Last install succeeded: " + (lastInstallSucceeded ? ToastPresenter.EXTRA_MAIN_TITLE : "0"));
            int fromVersion = extractFromVersion(updateBaseName);
            boolean isIncremental = fromVersion != -1;
            String priorVersion = this.sharedPreferences.getString(UPDATE_FROM_VERSION, "");
            if (!TextUtils.isEmpty(priorVersion)) {
                AnalyticsSupport.recordOTAInstallResult(isIncremental, priorVersion, lastInstallSucceeded);
                this.sharedPreferences.edit().remove(UPDATE_FROM_VERSION).commit();
            }
            if (!isIncremental) {
                return true;
            }
            sLogger.d("Last install was an incremental update");
            int incrementalVersion = 0;
            try {
                incrementalVersion = Integer.parseInt(VERSION.INCREMENTAL);
            } catch (NumberFormatException e) {
                sLogger.e("Cannot parse the incremental version of the build :" + VERSION.INCREMENTAL);
            }
            if (incrementalVersion != fromVersion) {
                return true;
            }
            sLogger.d("The last installed incremental from version is same as current version :" + incrementalVersion);
            if (lastInstallSucceeded) {
                return true;
            }
            sLogger.e("Last incremental update has failed");
            this.bus.post(new IncrementalOTAFailureDetected());
            if (currentUpdate == null || !currentUpdate.getAbsolutePath().equals(fileName)) {
                return true;
            }
            sLogger.d("Current update is the same as the one failed during last install");
            return false;
        } catch (IOException e2) {
            sLogger.e("Error reading the file :" + lastInstallFile.getAbsolutePath(), e2);
            return true;
        }
    }

    private void cleanupOldFiles() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                int incrementalVersion = -1;
                try {
                    incrementalVersion = Integer.parseInt(VERSION.INCREMENTAL);
                } catch (NumberFormatException e) {
                    OTAUpdateService.sLogger.e("Cannot parse the incremental version of the build :" + VERSION.INCREMENTAL);
                }
                if (incrementalVersion == -1) {
                    String fileName = OTAUpdateService.this.sharedPreferences.getString(OTAUpdateService.UPDATE_FILE, null);
                    if (!TextUtils.isEmpty(fileName)) {
                        incrementalVersion = OTAUpdateService.extractIncrementalVersion(fileName);
                    }
                }
                if (incrementalVersion > 0) {
                    File otaUpdatesFileDirectory = new File(PathManager.getInstance().getDirectoryForFileType(FileType.FILE_TYPE_OTA));
                    if (otaUpdatesFileDirectory.exists()) {
                        File[] children = otaUpdatesFileDirectory.listFiles();
                        if (children != null) {
                            for (File file : children) {
                                if (file.isFile() && OTAUpdateService.isOtaFile(file.getName()) && OTAUpdateService.extractIncrementalVersion(file.getName()) <= incrementalVersion) {
                                    OTAUpdateService.sLogger.e("Clearing the old update file :" + file.getName());
                                    IOUtils.deleteFile(HudApplication.getAppContext(), file.getAbsolutePath());
                                }
                            }
                        }
                    }
                }
            }
        }, 1);
    }

    private static int extractIncrementalVersion(String fileName) {
        Matcher match = sFileNamePattern.matcher(fileName);
        if (match.find() && match.groupCount() > 0) {
            try {
                return Integer.parseInt(match.group(3));
            } catch (NumberFormatException e) {
                sLogger.e("Cannot parse the file to retrieve the target version" + e);
                e.printStackTrace();
            }
        }
        return -1;
    }

    private static int extractFromVersion(String fileName) {
        int i = -1;
        Matcher match = sFileNamePattern.matcher(fileName);
        if (!match.find() || match.groupCount() <= 0) {
            return i;
        }
        String versionString = match.group(2);
        if (versionString == null) {
            return i;
        }
        try {
            return Integer.parseInt(versionString);
        } catch (NumberFormatException e) {
            sLogger.e("Cannot parse the file to retrieve the from version" + e);
            e.printStackTrace();
            return i;
        }
    }

    public static boolean isOtaFile(String fileName) {
        Matcher match = sFileNamePattern.matcher(fileName);
        return match != null && match.find();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private static File checkUpdateFile(Context context, SharedPreferences sharedPreferences) {
        sLogger.d("Checking for update file");
        swVersion = null;
        String fileName = sharedPreferences.getString(UPDATE_FILE, null);
        if (fileName == null) {
            sLogger.e("No update available");
            return null;
        }
        File file = new File(fileName);
        if (file.isFile() && file.exists() && file.canRead()) {
            int incrementalVersion = 0;
            try {
                incrementalVersion = Integer.parseInt(VERSION.INCREMENTAL);
            } catch (NumberFormatException e) {
                sLogger.e("Cannot parse the incremental version of the build :" + VERSION.INCREMENTAL);
            }
            sLogger.d("Incremental version of the build on the device:" + incrementalVersion);
            int updateVersion = extractIncrementalVersion(file.getName());
            int fromVersion = extractFromVersion(file.getName());
            sLogger.d("Update from version " + fromVersion);
            if (fromVersion != -1 && incrementalVersion != 0 && fromVersion != incrementalVersion) {
                sLogger.e("Update from version mismatch " + fromVersion + " " + incrementalVersion);
                clearUpdate(file, context, sharedPreferences);
                return null;
            } else if (updateVersion <= incrementalVersion) {
                sLogger.e("Already up to date :" + incrementalVersion);
                clearUpdate(file, context, sharedPreferences);
                return null;
            } else if (sharedPreferences.getInt(RETRY_COUNT, 0) < 3) {
                return file;
            } else {
                sLogger.d("Maximum retries. Deleting the update file.");
                clearUpdate(file, context, sharedPreferences);
                return null;
            }
        }
        sLogger.e("Invalid update file");
        return null;
    }

    public static String getIncrementalUpdateVersion(SharedPreferences sharedPreferences) {
        String fileName = sharedPreferences.getString(UPDATE_FILE, null);
        if (fileName == null) {
            return null;
        }
        int updateVersion = extractIncrementalVersion(new File(fileName).getName());
        if (updateVersion > 0) {
            return String.valueOf(updateVersion);
        }
        return null;
    }

    private static String getUpdateVersion(SharedPreferences sharedPreferences) {
        IOException e;
        Throwable th;
        String fileName = sharedPreferences.getString(UPDATE_FILE, null);
        if (fileName == null) {
            return null;
        }
        ZipFile zipFile = null;
        InputStream inputStream = null;
        String versionName = null;
        try {
            ZipFile zipFile2 = new ZipFile(new File(fileName));
            try {
                ZipEntry entry = zipFile2.getEntry("META-INF/com/android/metadata");
                Properties properties = new Properties();
                inputStream = zipFile2.getInputStream(entry);
                properties.load(inputStream);
                versionName = shortVersion(properties.getProperty("version-name"));
                IOUtils.closeObject(inputStream);
                IOUtils.closeObject(zipFile2);
                zipFile = zipFile2;
            } catch (IOException e2) {
                e = e2;
                zipFile = zipFile2;
                try {
                    sLogger.w("Error extracting version-name from ota metadata ", e);
                    IOUtils.closeObject(inputStream);
                    IOUtils.closeObject(zipFile);
                    if (TextUtils.isEmpty(versionName)) {
                        return "1.0." + getIncrementalUpdateVersion(sharedPreferences);
                    }
                    return versionName;
                } catch (Throwable th2) {
                    th = th2;
                    IOUtils.closeObject(inputStream);
                    IOUtils.closeObject(zipFile);
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                zipFile = zipFile2;
                IOUtils.closeObject(inputStream);
                IOUtils.closeObject(zipFile);
                throw th;
            }
        } catch (IOException e3) {
            e = e3;
            sLogger.w("Error extracting version-name from ota metadata ", e);
            IOUtils.closeObject(inputStream);
            IOUtils.closeObject(zipFile);
            if (TextUtils.isEmpty(versionName)) {
                return versionName;
            }
            return "1.0." + getIncrementalUpdateVersion(sharedPreferences);
        }
        if (TextUtils.isEmpty(versionName)) {
            return "1.0." + getIncrementalUpdateVersion(sharedPreferences);
        }
        return versionName;
    }

    public static String shortVersion(String versionName) {
        return versionName != null ? versionName.split("-")[0] : null;
    }

    public static String getCurrentVersion() {
        return shortVersion(BuildConfig.VERSION_NAME);
    }

    public static void clearUpdate(File file, Context context, SharedPreferences sharedPreferences) {
        if (file != null) {
            sLogger.d("Clearing the update");
            IOUtils.deleteFile(context, file.getAbsolutePath());
        }
        sharedPreferences.edit().remove(UPDATE_FILE).remove(RETRY_COUNT).remove(DO_NOT_PROMPT).remove(VERIFIED).commit();
    }

    public static boolean isUpdateAvailable() {
        return !TextUtils.isEmpty(swVersion);
    }

    public boolean bVerifyUpdate(File file, SharedPreferences preferences) {
        Exception e;
        try {
            RecoverySystem.verifyPackage(file, new ProgressListener() {
                public void onProgress(int progress) {
                    OTAUpdateService.sLogger.d(String.format("Verify progress: %d%% completed", new Object[]{Integer.valueOf(progress)}));
                }
            }, null);
            verifyOTAZipFile(file, 1000, MAX_FILE_SIZE);
            return true;
        } catch (IOException e2) {
            e = e2;
            sLogger.e("Exception while verifying the update package " + e);
            return false;
        } catch (GeneralSecurityException e3) {
            e = e3;
            sLogger.e("Exception while verifying the update package " + e);
            return false;
        }
    }

    public static boolean isOTAUpdateVerified(SharedPreferences preferences) {
        return preferences.getBoolean(VERIFIED, false);
    }

    public static boolean verifyOTAZipFile(File file, int maxEntries, long maxSize) {
        IOException e;
        Throwable th;
        ZipFile zipFile = null;
        try {
            File destinationDirectory = file.getParentFile();
            String destinationCanonicalPath = destinationDirectory.getCanonicalPath();
            ZipFile zipFile2 = new ZipFile(file);
            try {
                Enumeration entries = zipFile2.entries();
                int entryCount = 0;
                long uncompressedSize = 0;
                while (entries.hasMoreElements()) {
                    ZipEntry entry = (ZipEntry) entries.nextElement();
                    entryCount++;
                    if (!entry.isDirectory()) {
                        long size = entry.getSize();
                        if (size > 0) {
                            uncompressedSize += size;
                        }
                    }
                    if (entryCount > maxEntries) {
                        sLogger.d("OTA zip file failed verification : Too many entries " + entryCount);
                        IOUtils.closeObject(zipFile2);
                        zipFile = zipFile2;
                        return false;
                    } else if (uncompressedSize > maxSize) {
                        sLogger.d("OTA zip file failed verification : Exceeded max uncompressed size " + maxSize);
                        IOUtils.closeObject(zipFile2);
                        zipFile = zipFile2;
                        return false;
                    } else {
                        String zipEntryCanonicalPath = new File(destinationDirectory, entry.getName()).getCanonicalPath();
                        if (!TextUtils.isEmpty(zipEntryCanonicalPath)) {
                            if (!zipEntryCanonicalPath.startsWith(destinationCanonicalPath)) {
                            }
                        }
                        sLogger.d("OTA zip failed verification : Zip entry tried to write outside destination " + entry.getName() + " , Canonical path " + zipEntryCanonicalPath);
                        IOUtils.closeObject(zipFile2);
                        zipFile = zipFile2;
                        return false;
                    }
                }
                IOUtils.closeObject(zipFile2);
                zipFile = zipFile2;
                return true;
            } catch (IOException e2) {
                e = e2;
                zipFile = zipFile2;
            } catch (Throwable th2) {
                th = th2;
                zipFile = zipFile2;
            }
        } catch (IOException e3) {
            e = e3;
            try {
                sLogger.e("Exception while verifying the OTA package " + e);
                e.printStackTrace();
                IOUtils.closeObject(zipFile);
                return false;
            } catch (Throwable th3) {
                th = th3;
                IOUtils.closeObject(zipFile);
                throw th;
            }
        }
    }

    public static void startServiceToVerifyUpdate() {
        Context context = HudApplication.getAppContext();
        Intent updateServiceIntent = new Intent(context, OTAUpdateService.class);
        updateServiceIntent.putExtra("COMMAND", COMMAND_VERIFY_UPDATE);
        context.startService(updateServiceIntent);
    }

    public static String getSWUpdateVersion() {
        return swVersion;
    }
}
