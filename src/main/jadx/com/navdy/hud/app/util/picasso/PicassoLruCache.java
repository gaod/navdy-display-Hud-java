package com.navdy.hud.app.util.picasso;

import android.graphics.Bitmap;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.squareup.picasso.LruCache;
import java.lang.reflect.Field;
import java.util.HashMap;

public class PicassoLruCache extends LruCache implements PicassoItemCacheListener<String> {
    private HashMap<String, String> keyMap = new HashMap();

    public PicassoLruCache(int maxSize) {
        super(maxSize);
    }

    public void init() throws Exception {
        PicassoCacheMap<String, Bitmap> map = new PicassoCacheMap(0, 0.75f);
        map.setListener(this);
        Field f = LruCache.class.getDeclaredField("map");
        f.setAccessible(true);
        f.set(this, map);
    }

    public synchronized void itemAdded(String key) {
        if (key != null) {
            this.keyMap.put(getKey(key), key);
        }
    }

    public synchronized void itemRemoved(String key) {
        if (key != null) {
            this.keyMap.remove(getKey(key));
        }
    }

    private String getKey(String key) {
        if (key == null) {
            return null;
        }
        int index = key.indexOf(GlanceConstants.NEWLINE);
        if (index != -1) {
            return key.substring(0, index);
        }
        return key;
    }

    public synchronized Bitmap getBitmap(String key) {
        Bitmap bitmap = null;
        synchronized (this) {
            if (key != null) {
                String mappedKey = (String) this.keyMap.get(key);
                if (mappedKey != null) {
                    bitmap = super.get(mappedKey);
                }
            }
        }
        return bitmap;
    }

    public synchronized void setBitmap(String key, Bitmap bitmap) {
        if (!(key == null || bitmap == null)) {
            this.keyMap.put(getKey(key), key);
            super.set(key, bitmap);
        }
    }
}
