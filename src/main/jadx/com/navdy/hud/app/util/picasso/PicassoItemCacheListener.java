package com.navdy.hud.app.util.picasso;

public interface PicassoItemCacheListener<K> {
    void itemAdded(K k);

    void itemRemoved(K k);
}
