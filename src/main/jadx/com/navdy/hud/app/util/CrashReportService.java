package com.navdy.hud.app.util;

import android.app.IntentService;
import android.content.Intent;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.util.os.PropsFileUpdater;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.LogUtils;
import com.navdy.service.library.util.MusicDataUtils;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.concurrent.PriorityBlockingQueue;

public class CrashReportService extends IntentService {
    private static final String ACTION_ADD_REPORT = "AddReport";
    private static final String ACTION_DUMP_CRASH_REPORT = "DumpCrashReport";
    private static String CRASH_INFO_TEXT_FILE_NAME = "info.txt";
    private static String CRASH_LOG_TEXT_FILE_NAME = "log.txt";
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'_'HH:mm:ss.SSS", Locale.US);
    public static final String EXTRA_CRASH_TYPE = "EXTRA_CRASH_TYPE";
    public static final String EXTRA_FILE_PATH = "EXTRA_FILE_PATH";
    private static int LOG_FILE_SIZE = 51200;
    private static final int MAX_NON_FATAL_CRASH_REPORTS_OUT_STANDING = 10;
    private static final SimpleDateFormat TIME_FORMAT_FOR_REPORT = new SimpleDateFormat("dd MM yyyy' 'HH:mm:ss.SSS", Locale.US);
    private static boolean mIsInitialized = false;
    private static String sCrashReportsFolder;
    private static PriorityBlockingQueue<File> sCrashReportsToSend = new PriorityBlockingQueue(10, new FilesModifiedTimeComparator());
    private static Logger sLogger = new Logger(CrashReportService.class);
    private static int sOutStandingCrashReportsToBeCleared = 0;

    public enum CrashType {
        BLUETOOTH_DISCONNECTED,
        OBD_RESET,
        GENERIC_NON_FATAL_CRASH
    }

    public static class FilesModifiedTimeComparator implements Comparator<File> {
        public int compare(File file, File t1) {
            return (int) (file.lastModified() - t1.lastModified());
        }
    }

    public CrashReportService() {
        super("CrashReportService");
    }

    public void onCreate() {
        super.onCreate();
        if (!mIsInitialized) {
            sCrashReportsFolder = PathManager.getInstance().getNonFatalCrashReportDir();
            File[] files = new File(sCrashReportsFolder).listFiles();
            sLogger.d("Number of Fatal crash reports :" + (files != null ? files.length : 0));
            if (files != null) {
                populateFilesQueue(files, sCrashReportsToSend, 10);
            }
            mIsInitialized = true;
        }
    }

    protected void onHandleIntent(Intent intent) {
        try {
            sLogger.d("onHandleIntent " + intent.toString());
            if (ACTION_DUMP_CRASH_REPORT.equals(intent.getAction())) {
                String crashName = intent.getStringExtra(EXTRA_CRASH_TYPE);
                if (crashName != null) {
                    sLogger.d("Dumping a crash " + crashName);
                    dumpCrashReport(crashName);
                    return;
                }
                sLogger.e("Missing crash type");
            } else if (ACTION_ADD_REPORT.equals(intent.getAction())) {
                String absolutePath = intent.getStringExtra(EXTRA_FILE_PATH);
                if (!TextUtils.isEmpty(absolutePath)) {
                    File reportFile = new File(absolutePath);
                    if (reportFile.exists() && reportFile.isFile()) {
                        addReport(reportFile);
                    }
                }
            }
        } catch (Throwable t) {
            sLogger.e("Exception while handling intent ", t);
        }
    }

    private void addReport(File file) {
        synchronized (sCrashReportsToSend) {
            sCrashReportsToSend.add(file);
            if (sCrashReportsToSend.size() > 10) {
                File oldestFile = (File) sCrashReportsToSend.poll();
                if (oldestFile != null) {
                    IOUtils.deleteFile(HudApplication.getAppContext(), oldestFile.getAbsolutePath());
                }
                if (sOutStandingCrashReportsToBeCleared > 0) {
                    sOutStandingCrashReportsToBeCleared--;
                }
            }
        }
    }

    public static CrashType getCrashTypeForId(int id) {
        CrashType[] values = CrashType.values();
        if (id < 0 || id >= values.length) {
            return null;
        }
        return values[id];
    }

    private void dumpCrashReport(String crashName) {
        Throwable e;
        Throwable th;
        StringBuilder amPmMarker = new StringBuilder();
        Date currentTime = new Date(System.currentTimeMillis());
        String time = TIME_FORMAT_FOR_REPORT.format(currentTime);
        File infoTextFile = new File(sCrashReportsFolder + File.separator + CRASH_INFO_TEXT_FILE_NAME);
        if (infoTextFile.exists()) {
            IOUtils.deleteFile(this, infoTextFile.getAbsolutePath());
        }
        File snapShotDirectory = infoTextFile.getParentFile();
        if (!snapShotDirectory.exists()) {
            snapShotDirectory.mkdirs();
        }
        FileWriter fileWriter = null;
        try {
            infoTextFile.createNewFile();
            FileWriter fileWriter2 = new FileWriter(infoTextFile);
            try {
                fileWriter2.write("Crash Type : " + crashName + GlanceConstants.NEWLINE);
                fileWriter2.write("Report Time : " + time + " , " + amPmMarker.toString() + GlanceConstants.NEWLINE);
                fileWriter2.write(PropsFileUpdater.readProps());
                DeviceInfo deviceInfo = RemoteDevice.sLastSeenDeviceInfo.getDeviceInfo();
                if (deviceInfo != null) {
                    fileWriter2.write(printDeviceInfo(deviceInfo));
                }
                fileWriter2.flush();
                fileWriter2.close();
                File file = new File(sCrashReportsFolder + File.separator + "temp");
                if (file.exists()) {
                    IOUtils.deleteDirectory(this, file);
                }
                file.mkdirs();
                LogUtils.copySnapshotSystemLogs(file.getAbsolutePath());
                File[] logFiles = file.listFiles();
                File[] files = new File[(logFiles != null ? logFiles.length + 1 : 1)];
                files[0] = infoTextFile;
                int i = 1;
                if (logFiles != null) {
                    int i2;
                    int length = logFiles.length;
                    int i3 = 0;
                    while (true) {
                        i2 = i;
                        if (i3 >= length) {
                            break;
                        }
                        i = i2 + 1;
                        files[i2] = logFiles[i3];
                        i3++;
                    }
                    i = i2;
                }
                File zipFile = new File(sCrashReportsFolder + File.separator + (DATE_FORMAT.format(currentTime) + MusicDataUtils.ALTERNATE_SEPARATOR + crashName + ".zip"));
                IOUtils.compressFilesToZip(HudApplication.getAppContext(), files, zipFile.getAbsolutePath());
                IOUtils.deleteFile(this, infoTextFile.getAbsolutePath());
                IOUtils.deleteDirectory(this, file);
                addReport(zipFile);
                try {
                    fileWriter2.close();
                    fileWriter = fileWriter2;
                } catch (IOException e2) {
                    sLogger.d("Error closing the File Writer");
                    fileWriter = fileWriter2;
                }
            } catch (Throwable th2) {
                th = th2;
                fileWriter = fileWriter2;
                fileWriter.close();
                throw th;
            }
        } catch (Throwable th3) {
            e = th3;
            sLogger.e("Exception while creating the crash report ", e);
            fileWriter.close();
        }
    }

    public static void compressCrashReportsToZip(File[] additionalFiles, String outputFilePath) {
        File[] files;
        File file = new File(outputFilePath);
        if (file.exists()) {
            IOUtils.deleteFile(HudApplication.getAppContext(), outputFilePath);
        }
        try {
            file.createNewFile();
        } catch (IOException e) {
            sLogger.e("Failed to create new file at the specified output file path " + outputFilePath, e);
        }
        int totalSize = 0;
        if (additionalFiles != null && additionalFiles.length > 0) {
            totalSize = additionalFiles.length;
        }
        synchronized (sCrashReportsToSend) {
            int i;
            files = new File[(totalSize + sCrashReportsToSend.size())];
            int i2 = 0;
            if (additionalFiles != null) {
                int length = additionalFiles.length;
                int i3 = 0;
                i = 0;
                while (i3 < length) {
                    i2 = i + 1;
                    files[i] = additionalFiles[i3];
                    i3++;
                    i = i2;
                }
                i2 = i;
            }
            sOutStandingCrashReportsToBeCleared = sCrashReportsToSend.size();
            Iterator<File> filesIterator = sCrashReportsToSend.iterator();
            while (true) {
                i = i2;
                if (filesIterator.hasNext()) {
                    i2 = i + 1;
                    files[i] = (File) filesIterator.next();
                }
            }
        }
        IOUtils.compressFilesToZip(HudApplication.getAppContext(), files, outputFilePath);
    }

    public static void clearCrashReports() {
        synchronized (sCrashReportsToSend) {
            while (sOutStandingCrashReportsToBeCleared > 0) {
                File oldestFile = (File) sCrashReportsToSend.poll();
                if (oldestFile != null) {
                    IOUtils.deleteFile(HudApplication.getAppContext(), oldestFile.getAbsolutePath());
                    sOutStandingCrashReportsToBeCleared--;
                }
            }
        }
    }

    public static void addSnapshotAsync(String absolutePath) {
        Intent intent = new Intent(HudApplication.getAppContext(), CrashReportService.class);
        intent.setAction(ACTION_ADD_REPORT);
        intent.putExtra(EXTRA_FILE_PATH, absolutePath);
        HudApplication.getAppContext().startService(intent);
    }

    public static void dumpCrashReportAsync(CrashType type) {
        Intent intent = new Intent(HudApplication.getAppContext(), CrashReportService.class);
        intent.setAction(ACTION_DUMP_CRASH_REPORT);
        intent.putExtra(EXTRA_CRASH_TYPE, type.name());
        HudApplication.getAppContext().startService(intent);
    }

    public static void populateFilesQueue(File[] files, PriorityBlockingQueue<File> filesQueue, int maxSize) {
        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {
                    sLogger.d("File " + file.getName());
                    filesQueue.add(file);
                    if (filesQueue.size() == maxSize) {
                        File oldestFile = (File) filesQueue.poll();
                        sLogger.d("Deleting the old file " + oldestFile.getName());
                        IOUtils.deleteFile(HudApplication.getAppContext(), oldestFile.getAbsolutePath());
                    }
                }
            }
        }
    }

    public static String printDeviceInfo(DeviceInfo deviceInfo) {
        StringBuilder builder = new StringBuilder();
        builder.append("\nDevice Information\n");
        builder.append("-----------------------\n\n");
        builder.append("Platform :" + deviceInfo.platform.name() + GlanceConstants.NEWLINE);
        builder.append("Name :" + deviceInfo.deviceName + GlanceConstants.NEWLINE);
        builder.append("System Version :" + deviceInfo.systemVersion + GlanceConstants.NEWLINE);
        builder.append("Model :" + deviceInfo.model + GlanceConstants.NEWLINE);
        builder.append("Device ID :" + deviceInfo.deviceId + GlanceConstants.NEWLINE);
        builder.append("Make :" + deviceInfo.deviceMake + GlanceConstants.NEWLINE);
        builder.append("Build Type :" + deviceInfo.buildType + GlanceConstants.NEWLINE);
        builder.append("API level :" + deviceInfo.systemApiLevel + GlanceConstants.NEWLINE);
        builder.append("Protocol version :" + deviceInfo.protocolVersion + GlanceConstants.NEWLINE);
        builder.append("Force Full Update Flag :" + deviceInfo.forceFullUpdate + GlanceConstants.NEWLINE);
        return builder.toString();
    }
}
