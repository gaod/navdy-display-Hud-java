package com.navdy.hud.app.screen;

import com.navdy.hud.app.screen.ForceUpdateScreen.Module;
import dagger.internal.BindingsGroup;
import dagger.internal.ModuleAdapter;
import dagger.internal.ProvidesBinding;
import javax.inject.Provider;

public final class ForceUpdateScreen$Module$$ModuleAdapter extends ModuleAdapter<Module> {
    private static final Class<?>[] INCLUDES = new Class[0];
    private static final String[] INJECTS = new String[]{"members/com.navdy.hud.app.view.ForceUpdateView"};
    private static final Class<?>[] STATIC_INJECTIONS = new Class[0];

    /* compiled from: ForceUpdateScreen$Module$$ModuleAdapter */
    public static final class ProvideScreenProvidesAdapter extends ProvidesBinding<ForceUpdateScreen> implements Provider<ForceUpdateScreen> {
        private final Module module;

        public ProvideScreenProvidesAdapter(Module module) {
            super("com.navdy.hud.app.screen.ForceUpdateScreen", false, "com.navdy.hud.app.screen.ForceUpdateScreen.Module", "provideScreen");
            this.module = module;
            setLibrary(false);
        }

        public ForceUpdateScreen get() {
            return this.module.provideScreen();
        }
    }

    public ForceUpdateScreen$Module$$ModuleAdapter() {
        super(Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }

    public void getBindings(BindingsGroup bindings, Module module) {
        bindings.contributeProvidesBinding("com.navdy.hud.app.screen.ForceUpdateScreen", new ProvideScreenProvidesAdapter(module));
    }
}
