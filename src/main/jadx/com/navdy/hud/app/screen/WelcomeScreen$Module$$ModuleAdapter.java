package com.navdy.hud.app.screen;

import com.navdy.hud.app.screen.WelcomeScreen.Module;
import dagger.internal.ModuleAdapter;

public final class WelcomeScreen$Module$$ModuleAdapter extends ModuleAdapter<Module> {
    private static final Class<?>[] INCLUDES = new Class[0];
    private static final String[] INJECTS = new String[]{"members/com.navdy.hud.app.view.WelcomeView"};
    private static final Class<?>[] STATIC_INJECTIONS = new Class[0];

    public WelcomeScreen$Module$$ModuleAdapter() {
        super(Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
