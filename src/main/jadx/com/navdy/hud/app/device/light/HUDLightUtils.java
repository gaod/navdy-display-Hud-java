package com.navdy.hud.app.device.light;

import android.content.Context;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.device.light.LED.Settings;
import com.navdy.hud.app.device.light.LED.Settings.Builder;
import com.navdy.service.library.log.Logger;

public class HUDLightUtils {
    public static Settings gestureDetectedLedSettings = new Builder().setName("GestureDetected").setColor(HudApplication.getAppContext().getResources().getColor(R.color.led_gesture_color)).setIsBlinking(true).setBlinkInfinite(false).setBlinkPulseCount(2).setBlinkFrequency(BlinkFrequency.HIGH).build();
    public static Settings pairingLedSettings = new Builder().setName("Pairing").setColor(HudApplication.getAppContext().getResources().getColor(R.color.led_dial_pairing_color)).setIsBlinking(true).setBlinkInfinite(true).setBlinkFrequency(BlinkFrequency.LOW).build();
    public static final Logger sLogger = new Logger(HUDLightUtils.class);
    public static Settings snapShotCollectionEnd;
    public static Settings snapshotCollectionStart;

    static {
        int color = HudApplication.getAppContext().getResources().getColor(R.color.led_usb_power_color);
        snapshotCollectionStart = new Builder().setName("SnapshotStart").setColor(color).setIsBlinking(false).build();
        snapShotCollectionEnd = new Builder().setName("SnapshotEnd").setColor(color).setIsBlinking(true).setBlinkInfinite(false).setBlinkPulseCount(3).setBlinkFrequency(BlinkFrequency.HIGH).build();
    }

    public static void showPairing(Context context, LightManager manager, boolean show) {
        LED frontLed = (LED) manager.getLight(0);
        if (frontLed == null) {
            return;
        }
        if (show) {
            sLogger.d("Front LED, showing dial pairing");
            frontLed.pushSetting(pairingLedSettings);
            return;
        }
        sLogger.d("Front LED, stop showing dial pairing");
        frontLed.removeSetting(pairingLedSettings);
    }

    public static Settings showGestureDetectionEnabled(Context context, LightManager manager, String logName) {
        LED frontLed = (LED) manager.getLight(0);
        if (frontLed == null) {
            return null;
        }
        sLogger.d("Front LED, showing gesture detection enabled");
        Settings settings = new Builder().setName("GestureDetectionEnabled-" + logName).setColor(context.getResources().getColor(R.color.led_gesture_color)).setIsBlinking(true).setBlinkInfinite(true).setBlinkFrequency(BlinkFrequency.LOW).build();
        frontLed.pushSetting(settings);
        return settings;
    }

    public static void showSnapshotCollection(Context context, LightManager manager, boolean finished) {
        Settings settings = finished ? snapShotCollectionEnd : snapshotCollectionStart;
        LED frontLed = (LED) manager.getLight(0);
        if (frontLed != null) {
            frontLed.pushSetting(settings);
            if (finished) {
                frontLed.removeSetting(snapShotCollectionEnd);
                frontLed.removeSetting(snapshotCollectionStart);
            }
        }
    }

    public static void removeSettings(Settings settings) {
        if (settings != null) {
            LED frontLed = (LED) LightManager.getInstance().getLight(0);
            if (frontLed != null) {
                frontLed.removeSetting(settings);
            }
        }
    }

    public static void showGestureDetected(Context context, LightManager manager) {
        LED frontLed = (LED) manager.getLight(0);
        if (frontLed != null) {
            sLogger.d("Front LED, showing gesture detected");
            frontLed.pushSetting(gestureDetectedLedSettings);
            frontLed.removeSetting(gestureDetectedLedSettings);
        }
    }

    public static void dialActionDetected(Context context, LightManager manager) {
        LED frontLed = (LED) manager.getLight(0);
        if (frontLed != null) {
            sLogger.d("Front LED, showing dial action detected");
            frontLed.startActivityBlink();
        }
    }

    public static void resetFrontLED(LightManager manager) {
        LED frontLed = (LED) manager.getLight(0);
        if (frontLed != null) {
            sLogger.d("resetFrontLED called");
            frontLed.reset();
        }
    }

    public static void turnOffFrontLED(LightManager manager) {
        LED frontLed = (LED) manager.getLight(0);
        if (frontLed != null) {
            sLogger.d("turnOffFrontLED called");
            frontLed.turnOff();
        }
    }

    public static void showShutDown(LightManager manager) {
        LED frontLed = (LED) manager.getLight(0);
        if (frontLed != null) {
            sLogger.d("Front LED, showing shutdown");
            frontLed.pushSetting(new Builder().setColor(LED.DEFAULT_COLOR).setIsBlinking(true).setBlinkInfinite(true).setBlinkFrequency(BlinkFrequency.LOW).build());
        }
    }

    public static void showUSBPowerShutDown(Context context, LightManager manager) {
        LED frontLed = (LED) manager.getLight(0);
        if (frontLed != null) {
            sLogger.d("Front LED, showing USB power shutdown");
            frontLed.pushSetting(new Builder().setColor(context.getResources().getColor(R.color.led_usb_power_color)).setIsBlinking(true).setBlinkInfinite(true).setBlinkFrequency(BlinkFrequency.LOW).build());
        }
    }

    public static void showUSBPowerOn(Context context, LightManager manager) {
        LED frontLed = (LED) manager.getLight(0);
        if (frontLed != null) {
            sLogger.d("Front LED, showing USB power on, not blinking");
            frontLed.pushSetting(new Builder().setColor(context.getResources().getColor(R.color.led_usb_power_color)).setIsBlinking(false).build());
        }
    }

    public static void showUSBTransfer(Context context, LightManager manager) {
        LED frontLed = (LED) manager.getLight(0);
        if (frontLed != null) {
            sLogger.d("Front LED, showing USB transfer");
            frontLed.pushSetting(new Builder().setColor(context.getResources().getColor(R.color.led_usb_power_color)).setIsBlinking(true).setBlinkInfinite(true).setBlinkFrequency(BlinkFrequency.HIGH).build());
        }
    }

    public static void showError(Context context, LightManager manager) {
        LED frontLed = (LED) manager.getLight(0);
        if (frontLed != null) {
            sLogger.d("Front LED, showing error, not blinking");
            frontLed.pushSetting(new Builder().setColor(context.getResources().getColor(R.color.led_error_color)).setIsBlinking(false).build());
        }
    }

    public static void showGestureNotRecognized(Context context, LightManager manager) {
        LED frontLed = (LED) manager.getLight(0);
        if (frontLed != null) {
            sLogger.d("Front LED, showing gesture not recognized");
            frontLed.pushSetting(new Builder().setColor(context.getResources().getColor(R.color.led_gesture_not_recognized_color)).setIsBlinking(false).setBlinkFrequency(BlinkFrequency.LOW).build());
        }
    }
}
