package com.navdy.hud.app.device.light;

public class LightSettings {
    protected int color;
    protected boolean isBlinking;
    protected boolean isTurnedOn;

    public LightSettings(int color, boolean isTurnedOn, boolean isBlinking) {
        this.color = color;
        this.isTurnedOn = isTurnedOn;
        this.isBlinking = isBlinking;
    }

    public int getColor() {
        return this.color;
    }

    public boolean isTurnedOn() {
        return this.isTurnedOn;
    }

    public boolean isBlinking() {
        return this.isBlinking;
    }
}
