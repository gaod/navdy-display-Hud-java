package com.navdy.hud.app.device.dial;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothProfile.ServiceListener;
import android.os.Handler;
import com.localytics.android.Localytics;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.bluetooth.utils.BluetoothUtils;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import java.util.HashMap;
import java.util.Map;

public class DialManagerHelper {
    private static final Logger sLogger = DialManager.sLogger;

    public interface IDialConnectionStatus {
        void onStatus(boolean z);
    }

    public interface IDialForgotten {
        void onForgotten();
    }

    public interface IDialConnection {
        void onAttemptConnection(boolean z);

        void onAttemptDisconnection(boolean z);
    }

    public static void isDialConnected(BluetoothAdapter bluetoothAdapter, final BluetoothDevice device, final IDialConnectionStatus callBack) {
        if (bluetoothAdapter == null || device == null || callBack == null) {
            try {
                throw new IllegalArgumentException();
            } catch (Throwable t) {
                sLogger.e("isDialConnected", t);
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        callBack.onStatus(false);
                    }
                }, 1);
                return;
            }
        }
        boolean ret = bluetoothAdapter.getProfileProxy(HudApplication.getAppContext(), new ServiceListener() {
            public void onServiceConnected(int profile, final BluetoothProfile proxy) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        try {
                            int state = proxy.getConnectionState(device);
                            DialManagerHelper.sLogger.v("[Dial]Device " + device.getName() + " state is " + BluetoothUtils.getConnectedState(state));
                            if (state == 2) {
                                DialManagerHelper.sLogger.v("[Dial]already connected");
                                callBack.onStatus(true);
                                return;
                            }
                            callBack.onStatus(false);
                        } catch (Throwable t) {
                            DialManagerHelper.sLogger.e("[Dial]", t);
                            callBack.onStatus(false);
                        }
                    }
                }, 1);
            }

            public void onServiceDisconnected(int profile) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        try {
                            DialManagerHelper.sLogger.v("[Dial]onServiceDisconnected: could not connect proxy");
                            callBack.onStatus(false);
                        } catch (Throwable t) {
                            DialManagerHelper.sLogger.e("[Dial]", t);
                        }
                    }
                }, 1);
            }
        }, 4);
        sLogger.v("[Dial]getProfileProxy returned:" + ret);
        if (!ret) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    callBack.onStatus(false);
                }
            }, 1);
        }
    }

    public static void connectToDial(BluetoothAdapter bluetoothAdapter, final BluetoothDevice device, final IDialConnection callBack) {
        if (bluetoothAdapter == null || device == null || callBack == null) {
            try {
                throw new IllegalArgumentException();
            } catch (Throwable t) {
                sLogger.e("connectToDial", t);
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        callBack.onAttemptConnection(false);
                    }
                }, 1);
                return;
            }
        }
        boolean ret = bluetoothAdapter.getProfileProxy(HudApplication.getAppContext(), new ServiceListener() {
            boolean eventSent = false;

            public void onServiceConnected(int profile, final BluetoothProfile proxy) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        try {
                            int state = proxy.getConnectionState(device);
                            DialManagerHelper.sLogger.v("[Dial]connectToDial Device " + device.getName() + " state is " + BluetoothUtils.getConnectedState(state));
                            if (state == 2) {
                                DialManagerHelper.sLogger.v("[Dial]connectToDial already connected:" + device);
                                if (!AnonymousClass4.this.eventSent) {
                                    AnonymousClass4.this.eventSent = true;
                                    callBack.onAttemptConnection(true);
                                }
                            } else if (DialManagerHelper.forceConnect(proxy, device)) {
                                DialManagerHelper.sLogger.v("[Dial]connectToDial proxy-connect to hid device successful");
                                if (!AnonymousClass4.this.eventSent) {
                                    AnonymousClass4.this.eventSent = true;
                                    callBack.onAttemptConnection(true);
                                }
                            } else {
                                DialManagerHelper.sLogger.v("[Dial]connectToDial proxy-connect to hid device failed");
                                if (!AnonymousClass4.this.eventSent) {
                                    AnonymousClass4.this.eventSent = true;
                                    callBack.onAttemptConnection(false);
                                }
                            }
                        } catch (Throwable t) {
                            DialManagerHelper.sLogger.e("[Dial] connectToDial", t);
                            if (!AnonymousClass4.this.eventSent) {
                                AnonymousClass4.this.eventSent = true;
                                callBack.onAttemptConnection(false);
                            }
                        }
                    }
                }, 1);
            }

            public void onServiceDisconnected(int profile) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        try {
                            DialManagerHelper.sLogger.v("[Dial]connectToDial onServiceDisconnected: could not connect proxy");
                            if (!AnonymousClass4.this.eventSent) {
                                AnonymousClass4.this.eventSent = true;
                                callBack.onAttemptConnection(false);
                            }
                        } catch (Throwable t) {
                            DialManagerHelper.sLogger.e("[Dial]", t);
                        }
                    }
                }, 1);
            }
        }, 4);
        sLogger.v("[Dial]connectToDial getProfileProxy returned:" + ret);
        if (!ret) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    callBack.onAttemptConnection(false);
                }
            }, 1);
        }
    }

    public static void disconnectFromDial(BluetoothAdapter bluetoothAdapter, final BluetoothDevice device, final IDialConnection callBack) {
        if (bluetoothAdapter == null || device == null || callBack == null) {
            try {
                throw new IllegalArgumentException();
            } catch (Throwable t) {
                sLogger.e("disconnectFromDial", t);
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        callBack.onAttemptDisconnection(false);
                    }
                }, 1);
                return;
            }
        }
        boolean ret = bluetoothAdapter.getProfileProxy(HudApplication.getAppContext(), new ServiceListener() {
            boolean eventSent = false;

            public void onServiceConnected(int profile, final BluetoothProfile proxy) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        try {
                            int state = proxy.getConnectionState(device);
                            DialManagerHelper.sLogger.v("[Dial]disconnectFromDial Device " + device.getName() + " state is " + BluetoothUtils.getConnectedState(state));
                            if (state != 2) {
                                DialManagerHelper.sLogger.v("[Dial]disconnectFromDial already disconnected:" + device);
                                if (!AnonymousClass7.this.eventSent) {
                                    AnonymousClass7.this.eventSent = true;
                                    callBack.onAttemptDisconnection(true);
                                }
                            } else if (DialManagerHelper.forceDisconnect(proxy, device)) {
                                DialManagerHelper.sLogger.v("[Dial]disconnectFromDial proxy-disconnect from hid device successful");
                                if (!AnonymousClass7.this.eventSent) {
                                    AnonymousClass7.this.eventSent = true;
                                    callBack.onAttemptDisconnection(true);
                                }
                            } else {
                                DialManagerHelper.sLogger.v("[Dial]disconnectFromDial proxy-disconnect from hid device failed");
                                if (!AnonymousClass7.this.eventSent) {
                                    AnonymousClass7.this.eventSent = true;
                                    callBack.onAttemptDisconnection(false);
                                }
                            }
                        } catch (Throwable t) {
                            DialManagerHelper.sLogger.e("[Dial] disconnectFromDial", t);
                            if (!AnonymousClass7.this.eventSent) {
                                AnonymousClass7.this.eventSent = true;
                                callBack.onAttemptDisconnection(false);
                            }
                        }
                    }
                }, 1);
            }

            public void onServiceDisconnected(int profile) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        try {
                            DialManagerHelper.sLogger.v("[Dial]disconnectFromDial onServiceDisconnected: could not connect proxy");
                            if (!AnonymousClass7.this.eventSent) {
                                AnonymousClass7.this.eventSent = true;
                                callBack.onAttemptDisconnection(false);
                            }
                        } catch (Throwable t) {
                            DialManagerHelper.sLogger.e("[Dial]", t);
                        }
                    }
                }, 1);
            }
        }, 4);
        sLogger.v("[Dial]disconnectFromDial getProfileProxy returned:" + ret);
        if (!ret) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    callBack.onAttemptDisconnection(false);
                }
            }, 1);
        }
    }

    private static boolean forceConnect(BluetoothProfile proxy, BluetoothDevice device) {
        return invokeMethod(proxy, device, "connect");
    }

    private static boolean forceDisconnect(BluetoothProfile proxy, BluetoothDevice device) {
        return invokeMethod(proxy, device, "disconnect");
    }

    private static boolean invokeMethod(BluetoothProfile proxy, BluetoothDevice device, String methodName) {
        Boolean returnValue;
        try {
            returnValue = Boolean.valueOf(false);
            returnValue = (Boolean) Class.forName("android.bluetooth.BluetoothInputDevice").getMethod(methodName, new Class[]{BluetoothDevice.class}).invoke(proxy, new Object[]{device});
        } catch (Throwable t) {
            sLogger.e("[Dial]", t);
            return false;
        }
        return returnValue.booleanValue();
    }

    public static void sendRebootLocalyticsEvent(Handler handler, final boolean successful, final BluetoothDevice device, final String result) {
        handler.post(new Runnable() {
            public void run() {
                try {
                    Map<String, String> map = new HashMap();
                    map.put(DialConstants.DIAL_EVENT_TYPE, DialConstants.DIAL_EVENT_REBOOT);
                    map.put(DialConstants.DIAL_RESULT, result);
                    map.put(DialConstants.DIAL_PAIRING_SUCCESS, successful ? "true" : "false");
                    String address = null;
                    if (device != null) {
                        address = device.getAddress();
                    }
                    map.put(DialConstants.DIAL_ADDRESS, address);
                    DialManager dialManager = DialManager.getInstance();
                    String battery = String.valueOf(dialManager.getLastKnownBatteryLevel());
                    String rawBattery = String.valueOf(dialManager.getLastKnownRawBatteryLevel());
                    String temperature = String.valueOf(dialManager.getLastKnownSystemTemperature());
                    String fw = dialManager.getFirmWareVersion();
                    String hw = dialManager.getHardwareVersion();
                    int incremental = dialManager.getDialFirmwareUpdater().getVersions().dial.incrementalVersion;
                    map.put(DialConstants.DIAL_BATTERY_LEVEL, battery);
                    map.put(DialConstants.DIAL_RAW_BATTERY_LEVEL, rawBattery);
                    map.put(DialConstants.DIAL_TEMPERATURE, temperature);
                    map.put(DialConstants.DIAL_FW_VERSION, fw);
                    map.put(DialConstants.DIAL_HW_VERSION, hw);
                    map.put(DialConstants.DIAL_INCREMENTAL_VERSION, Integer.toString(incremental));
                    DialManagerHelper.sLogger.v("sending foundDial[" + successful + "] battery[" + battery + "] rawBattery[" + rawBattery + "] temperature[" + temperature + "]" + " fw[" + fw + "] hw[" + hw + "] incremental[" + incremental + "] type[" + DialConstants.DIAL_EVENT_REBOOT + "] result [" + result + "] address [" + address + "]");
                    Localytics.tagEvent(DialConstants.DIAL_PAIRING_EVENT, map);
                } catch (Throwable t) {
                    DialManagerHelper.sLogger.e(t);
                }
            }
        });
    }

    public static void sendLocalyticsEvent(Handler handler, boolean firstTime, boolean foundDial, int delay, boolean pair) {
        sendLocalyticsEvent(handler, firstTime, foundDial, delay, pair, null);
    }

    public static void sendLocalyticsEvent(Handler handler, boolean firstTime, boolean foundDial, int delay, boolean pair, String result) {
        long j;
        final BluetoothDevice device = DialManager.getInstance().getDialDevice();
        final boolean z = firstTime;
        final boolean z2 = foundDial;
        final boolean z3 = pair;
        final String str = result;
        Runnable anonymousClass11 = new Runnable() {
            public void run() {
                try {
                    Map<String, String> map = new HashMap();
                    map.put(DialConstants.DIAL_FIRST_TIME, z ? "true" : "false");
                    map.put(DialConstants.DIAL_PAIRING_SUCCESS, z2 ? "true" : "false");
                    String type = z3 ? DialConstants.DIAL_EVENT_PAIR : DialConstants.DIAL_EVENT_CONNECT;
                    map.put(DialConstants.DIAL_EVENT_TYPE, type);
                    map.put(DialConstants.DIAL_RESULT, str);
                    String address = null;
                    if (device != null) {
                        address = device.getAddress();
                    }
                    map.put(DialConstants.DIAL_ADDRESS, address);
                    if (z2) {
                        DialManager dialManager = DialManager.getInstance();
                        String battery = String.valueOf(dialManager.getLastKnownBatteryLevel());
                        String rawBattery = String.valueOf(dialManager.getLastKnownRawBatteryLevel());
                        String temperature = String.valueOf(dialManager.getLastKnownSystemTemperature());
                        String fw = dialManager.getFirmWareVersion();
                        String hw = dialManager.getHardwareVersion();
                        int incremental = dialManager.getDialFirmwareUpdater().getVersions().dial.incrementalVersion;
                        map.put(DialConstants.DIAL_BATTERY_LEVEL, battery);
                        map.put(DialConstants.DIAL_RAW_BATTERY_LEVEL, rawBattery);
                        map.put(DialConstants.DIAL_TEMPERATURE, temperature);
                        map.put(DialConstants.DIAL_FW_VERSION, fw);
                        map.put(DialConstants.DIAL_HW_VERSION, hw);
                        map.put(DialConstants.DIAL_INCREMENTAL_VERSION, Integer.toString(incremental));
                        DialManagerHelper.sLogger.v("sending firstTime [" + z + "] foundDial[" + z2 + "] battery[" + battery + "] rawBattery[" + rawBattery + "] temperature[" + temperature + "]" + " fw[" + fw + "] hw[" + hw + "] incremental[" + incremental + "] type[" + type + "] result [" + str + "] address [" + address + "]");
                    } else {
                        DialManagerHelper.sLogger.v("sending firstTime [" + z + "] foundDial[" + z2 + "] type[" + type + "] result [" + str + "] address [" + address + "]");
                    }
                    Localytics.tagEvent(DialConstants.DIAL_PAIRING_EVENT, map);
                } catch (Throwable t) {
                    DialManagerHelper.sLogger.e(t);
                }
            }
        };
        if (foundDial) {
            j = (long) delay;
        } else {
            j = 0;
        }
        handler.postDelayed(anonymousClass11, j);
    }
}
