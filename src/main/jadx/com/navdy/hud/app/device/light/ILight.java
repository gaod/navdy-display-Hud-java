package com.navdy.hud.app.device.light;

public interface ILight {
    void popSetting();

    void pushSetting(LightSettings lightSettings);

    void removeSetting(LightSettings lightSettings);

    void reset();

    void turnOff();

    void turnOn();
}
