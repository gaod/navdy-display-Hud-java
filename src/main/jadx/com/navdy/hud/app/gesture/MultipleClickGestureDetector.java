package com.navdy.hud.app.gesture;

import android.os.Handler;
import android.os.Looper;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.service.library.events.input.GestureEvent;

public class MultipleClickGestureDetector implements IInputHandler {
    public static final int DOUBLE_CLICK = 2;
    private static final int MULTIPLE_CLICK_THRESHOLD = 400;
    private static final CustomKeyEvent MULTIPLIABLE_EVENT = CustomKeyEvent.SELECT;
    public static final int TRIPLE_CLICK = 3;
    private final Handler handler = new Handler(Looper.getMainLooper());
    private IMultipleClickKeyGesture listener;
    private int maxAcceptableClickCount;
    private int multipleClickCount;
    private final Runnable multipleClickTrigger = new Runnable() {
        public void run() {
            MultipleClickGestureDetector.this.processEvent(null);
        }
    };

    public interface IMultipleClickKeyGesture extends IInputHandler {
        void onMultipleClick(int i);
    }

    public MultipleClickGestureDetector(int maxAcceptableClickCount, IMultipleClickKeyGesture listener) {
        if (listener == null || maxAcceptableClickCount <= 1) {
            throw new IllegalArgumentException();
        }
        this.maxAcceptableClickCount = maxAcceptableClickCount;
        this.listener = listener;
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        return processEvent(event);
    }

    public IInputHandler nextHandler() {
        return null;
    }

    private synchronized boolean processEvent(CustomKeyEvent receivedEvent) {
        boolean res = false;
        synchronized (this) {
            this.handler.removeCallbacks(this.multipleClickTrigger);
            if (this.listener != null) {
                boolean isSelectEvent = MULTIPLIABLE_EVENT.equals(receivedEvent);
                if (isSelectEvent) {
                    this.multipleClickCount++;
                }
                while (this.multipleClickCount >= this.maxAcceptableClickCount) {
                    this.listener.onMultipleClick(this.maxAcceptableClickCount);
                    this.multipleClickCount -= this.maxAcceptableClickCount;
                }
                if (!isSelectEvent) {
                    if (this.multipleClickCount > 1) {
                        this.listener.onMultipleClick(this.multipleClickCount);
                    } else if (this.multipleClickCount == 1) {
                        this.listener.onKey(MULTIPLIABLE_EVENT);
                    }
                    this.multipleClickCount = 0;
                    if (receivedEvent != null) {
                        res = this.listener.onKey(receivedEvent);
                        switch (receivedEvent) {
                            case POWER_BUTTON_LONG_PRESS:
                            case LONG_PRESS:
                                break;
                        }
                    }
                } else if (this.multipleClickCount > 0) {
                    this.handler.postDelayed(this.multipleClickTrigger, 400);
                }
                res = true;
            }
        }
        return res;
    }
}
