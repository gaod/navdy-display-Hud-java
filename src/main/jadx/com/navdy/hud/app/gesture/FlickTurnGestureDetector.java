package com.navdy.hud.app.gesture;

import com.navdy.hud.app.gesture.MultipleClickGestureDetector.IMultipleClickKeyGesture;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.List;

public class FlickTurnGestureDetector extends MultipleClickGestureDetector {
    private static final List<CustomKeyEvent> FLICKABLE_EVENTS = new ArrayList() {
        {
            add(CustomKeyEvent.LEFT);
            add(CustomKeyEvent.RIGHT);
        }
    };
    private static final int FLICK_EVENT_COUNT = 4;
    private static final int FLICK_PAIRED_EVENT_TIMEOUT_MS = 500;
    private static final Logger sLogger = new Logger(FlickTurnGestureDetector.class);
    private short consecutiveTurnEventCount = (short) 0;
    private IFlickTurnKeyGesture listener;
    private CustomKeyEvent previousKeyEvent = null;
    private long previousKeyEventTime = 0;

    public interface IFlickTurnKeyGesture extends IMultipleClickKeyGesture {
        void onFlickLeft();

        void onFlickRight();
    }

    public FlickTurnGestureDetector(IMultipleClickKeyGesture listener) {
        super(2, listener);
        if (listener == null || !(listener instanceof IFlickTurnKeyGesture)) {
            throw new IllegalArgumentException();
        }
        this.listener = (IFlickTurnKeyGesture) listener;
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        boolean result = super.onKey(event);
        long time = System.currentTimeMillis();
        if (FLICKABLE_EVENTS.contains(event) && this.previousKeyEvent == event && time < this.previousKeyEventTime + 500) {
            short s = (short) (this.consecutiveTurnEventCount + 1);
            this.consecutiveTurnEventCount = s;
            if (s == (short) 4) {
                sLogger.d("Combined key event recognized from a single " + event);
                this.consecutiveTurnEventCount = (short) 0;
                if (CustomKeyEvent.LEFT.equals(event)) {
                    this.listener.onFlickLeft();
                } else if (CustomKeyEvent.RIGHT.equals(event)) {
                    this.listener.onFlickRight();
                } else {
                    throw new UnsupportedOperationException("Flick for " + event + " not handled");
                }
            }
        }
        this.consecutiveTurnEventCount = (short) 1;
        this.previousKeyEvent = event;
        this.previousKeyEventTime = time;
        return result;
    }

    public IInputHandler nextHandler() {
        return null;
    }
}
