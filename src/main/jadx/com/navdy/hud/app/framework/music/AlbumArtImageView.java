package com.navdy.hud.app.framework.music;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RadialGradient;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.navdy.hud.app.R;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.util.concurrent.atomic.AtomicBoolean;

public class AlbumArtImageView extends ImageView {
    private static final int ARTWORK_TRANSITION_DURATION = 1000;
    private static Logger logger = new Logger(AlbumArtImageView.class);
    private final Object artworkLock;
    private BitmapDrawable defaultArtwork;
    private Handler handler;
    private AtomicBoolean isAnimatingArtwork;
    private boolean mask;
    private Bitmap nextArtwork;
    private String nextArtworkHash;
    private Paint paint;
    private RadialGradient radialGradient;

    public AlbumArtImageView(Context context) {
        this(context, null);
    }

    public AlbumArtImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AlbumArtImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.isAnimatingArtwork = new AtomicBoolean(false);
        this.artworkLock = new Object();
        this.handler = new Handler(Looper.getMainLooper());
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        TypedArray a;
        int defaultArtworkResource = R.drawable.icon_mm_music_control_blank;
        if (attrs != null) {
            a = getContext().obtainStyledAttributes(attrs, R.styleable.AlbumArtImageView);
            this.mask = a.getBoolean(0, false);
            defaultArtworkResource = a.getResourceId(1, R.drawable.icon_mm_music_control_blank);
            a.recycle();
        }
        BitmapDrawable drawable = (BitmapDrawable) getResources().getDrawable(defaultArtworkResource);
        if (this.mask) {
            this.paint = new Paint();
            int[] colors = getResources().getIntArray(R.array.smart_dash_music_gauge_gradient);
            a = getContext().obtainStyledAttributes(attrs, new int[]{16842996});
            float radius = (float) (a.getDimensionPixelSize(0, 130) / 2);
            a.recycle();
            this.radialGradient = new RadialGradient(radius, radius, radius, colors, null, TileMode.CLAMP);
            this.defaultArtwork = new BitmapDrawable(getResources(), addMask(drawable.getBitmap()));
        } else {
            this.defaultArtwork = drawable;
        }
        setImageDrawable(new LayerDrawable(new Drawable[]{this.defaultArtwork}));
    }

    public void setArtworkBitmap(@Nullable Bitmap bitmap, boolean animate) {
        String hash = IOUtils.hashForBitmap(bitmap);
        if (TextUtils.equals(this.nextArtworkHash, hash)) {
            logger.d("Already set this artwork, ignoring");
            return;
        }
        synchronized (this) {
            this.nextArtworkHash = hash;
            if (this.mask) {
                bitmap = addMask(bitmap);
            }
            this.nextArtwork = bitmap;
        }
        if (!animate) {
            drawImmediately();
        } else if (this.isAnimatingArtwork.compareAndSet(false, true)) {
            animateArtwork();
        } else {
            logger.d("Already animating");
        }
    }

    private void animateArtwork() {
        final String artworkHash;
        Bitmap artwork;
        synchronized (this.artworkLock) {
            artworkHash = this.nextArtworkHash;
            artwork = this.nextArtwork;
        }
        final Drawable[] drawables = new Drawable[2];
        Drawable currentDrawable = getDrawable();
        if (currentDrawable == null) {
            drawables[0] = new ColorDrawable(0);
        } else {
            drawables[0] = ((LayerDrawable) currentDrawable).getDrawable(0);
        }
        if (artwork != null) {
            drawables[1] = new BitmapDrawable(getContext().getResources(), artwork);
        } else {
            logger.i("Bitmap image is null");
            drawables[1] = this.defaultArtwork;
        }
        TransitionDrawable drawable = new TransitionDrawable(drawables);
        setImageDrawable(drawable);
        drawable.setCrossFadeEnabled(true);
        drawable.startTransition(1000);
        this.handler.postDelayed(new Runnable() {
            public void run() {
                boolean sameArtwork;
                AlbumArtImageView.this.setImageDrawable(new LayerDrawable(new Drawable[]{drawables[1]}));
                synchronized (AlbumArtImageView.this.artworkLock) {
                    sameArtwork = TextUtils.equals(artworkHash, AlbumArtImageView.this.nextArtworkHash);
                }
                if (sameArtwork) {
                    AlbumArtImageView.this.isAnimatingArtwork.set(false);
                } else {
                    AlbumArtImageView.this.animateArtwork();
                }
            }
        }, 1000);
    }

    private void drawImmediately() {
        this.isAnimatingArtwork.set(false);
        this.handler.removeCallbacksAndMessages(null);
        Drawable[] layers = new Drawable[1];
        if (this.nextArtwork != null) {
            layers[0] = new BitmapDrawable(getContext().getResources(), this.nextArtwork);
        } else {
            logger.i("Bitmap image is null");
            layers[0] = this.defaultArtwork;
        }
        setImageDrawable(new LayerDrawable(layers));
    }

    @Nullable
    private Bitmap addMask(@Nullable Bitmap src) {
        if (src == null) {
            return null;
        }
        int w = src.getWidth();
        int h = src.getHeight();
        Bitmap bitmap = Bitmap.createBitmap(w, h, Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawBitmap(src, 0.0f, 0.0f, null);
        this.paint.setShader(this.radialGradient);
        this.paint.setXfermode(new PorterDuffXfermode(Mode.MULTIPLY));
        canvas.drawRect(0.0f, 0.0f, (float) w, (float) h, this.paint);
        return bitmap;
    }
}
