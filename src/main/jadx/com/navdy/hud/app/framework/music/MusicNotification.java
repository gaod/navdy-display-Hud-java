package com.navdy.hud.app.framework.music;

import android.animation.AnimatorSet;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.MusicManager;
import com.navdy.hud.app.manager.MusicManager.MediaControl;
import com.navdy.hud.app.manager.MusicManager.MusicUpdateListener;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.MusicDataUtils;
import com.navdy.service.library.util.ScalingUtilities;
import com.navdy.service.library.util.ScalingUtilities.ScalingLogic;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import okio.ByteString;

public class MusicNotification implements INotification, MusicUpdateListener {
    private static final float ARTWORK_ALPHA_NOT_PLAYING = 0.5f;
    private static final float ARTWORK_ALPHA_PLAYING = 1.0f;
    private static final String EMPTY = "";
    private static final int MUSIC_TIMEOUT = 5000;
    private static final Logger sLogger = new Logger(MusicNotification.class);
    private final ScalingLogic FIT = ScalingLogic.FIT;
    private Bus bus;
    private MediaControllerLayout choiceLayout;
    private ViewGroup container;
    private INotificationController controller;
    private Handler handler;
    private AlbumArtImageView image;
    private AtomicBoolean isKeyPressedDown = new AtomicBoolean(false);
    private String lastEventIdentifier;
    private MusicManager musicManager;
    private ProgressBar progressBar;
    private TextView subTitle;
    private TextView title;
    private MusicTrackInfo trackInfo;

    public MusicNotification(MusicManager musicManager, Bus bus) {
        this.musicManager = musicManager;
        this.bus = bus;
        this.handler = new Handler();
    }

    public NotificationType getType() {
        return NotificationType.MUSIC;
    }

    public String getId() {
        return NotificationId.MUSIC_NOTIFICATION_ID;
    }

    public View getView(Context context) {
        if (this.container == null) {
            this.container = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.notification_music, null);
            this.title = (TextView) this.container.findViewById(R.id.title);
            this.subTitle = (TextView) this.container.findViewById(R.id.subTitle);
            this.image = (AlbumArtImageView) this.container.findViewById(R.id.image);
            this.progressBar = (ProgressBar) this.container.findViewById(R.id.music_progress);
            this.choiceLayout = (MediaControllerLayout) this.container.findViewById(R.id.choiceLayout);
        }
        return this.container;
    }

    public View getExpandedView(Context context, Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(INotificationController controller) {
        this.controller = controller;
        this.bus.register(this);
        this.musicManager.addMusicUpdateListener(this);
        updateProgressBar(this.musicManager.getCurrentPosition());
    }

    public void onUpdate() {
        onTrackUpdated(this.musicManager.getCurrentTrack(), this.musicManager.getCurrentControls(), false);
        updateProgressBar(this.musicManager.getCurrentPosition());
    }

    public void onStop() {
        this.musicManager.removeMusicUpdateListener(this);
        this.bus.unregister(this);
        this.controller = null;
    }

    public int getTimeout() {
        return 5000;
    }

    public boolean isAlive() {
        return false;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return false;
    }

    public int getColor() {
        return 0;
    }

    public void onNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    public boolean supportScroll() {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        if (this.controller == null) {
            return false;
        }
        if (this.choiceLayout != null) {
            switch (event) {
                case LEFT:
                    this.controller.resetTimeout();
                    this.choiceLayout.moveSelectionLeft();
                    return true;
                case RIGHT:
                    this.controller.resetTimeout();
                    this.choiceLayout.moveSelectionRight();
                    return true;
                case SELECT:
                    this.controller.resetTimeout();
                    Choice choice = this.choiceLayout.getSelectedItem();
                    if (choice == null) {
                        sLogger.w("Choice layout selected item is null, returning");
                        return false;
                    }
                    MediaControl action = MusicManager.CONTROLS[choice.id];
                    switch (action) {
                        case PLAY:
                        case PAUSE:
                            if (this.trackInfo != null) {
                                this.musicManager.executeMediaControl(action, this.isKeyPressedDown.get());
                            }
                            this.choiceLayout.executeSelectedItem(true);
                            return true;
                        case MUSIC_MENU:
                            this.choiceLayout.executeSelectedItem(true);
                            Bundle args = new Bundle();
                            String path = this.musicManager.getMusicMenuPath();
                            if (path == null) {
                                path = HereManeuverDisplayBuilder.SLASH + Menu.MUSIC.name();
                            }
                            args.putString(MainMenuScreen2.ARG_MENU_PATH, path);
                            NotificationManager.getInstance().removeNotification(getId(), true, Screen.SCREEN_MAIN_MENU, args, null);
                            return true;
                        default:
                            return false;
                    }
                case LONG_PRESS:
                    this.controller.resetTimeout();
                    return true;
            }
        }
        return false;
    }

    @Subscribe
    public void onKeyEvent(KeyEvent event) {
        if (this.controller != null && this.choiceLayout != null && InputManager.isCenterKey(event.getKeyCode())) {
            Choice selectedItem = this.choiceLayout.getSelectedItem();
            if (selectedItem == null) {
                sLogger.w("Choice layout selected item is null, returning");
                return;
            }
            MediaControl action = MusicManager.CONTROLS[selectedItem.id];
            if (action == MediaControl.NEXT || action == MediaControl.PREVIOUS) {
                this.controller.resetTimeout();
                if (this.trackInfo != null) {
                    this.musicManager.handleKeyEvent(event, action, this.isKeyPressedDown.get());
                }
                if (event.getAction() == 0 && this.isKeyPressedDown.compareAndSet(false, true)) {
                    this.choiceLayout.keyDownSelectedItem();
                } else if (event.getAction() == 1 && this.isKeyPressedDown.compareAndSet(true, false)) {
                    this.choiceLayout.keyUpSelectedItem();
                }
            }
        }
    }

    public void updateProgressBar(int value) {
        if (this.controller != null) {
            this.progressBar.setProgress(value);
        }
    }

    public void onTrackUpdated(@NonNull MusicTrackInfo trackInfo, Set<MediaControl> mediaControls, boolean willOpenNotification) {
        this.trackInfo = trackInfo;
        if (this.controller != null) {
            String songIdentifier = MusicDataUtils.songIdentifierFromTrackInfo(trackInfo);
            if (!(this.lastEventIdentifier == null || this.lastEventIdentifier.equals(songIdentifier))) {
                this.controller.resetTimeout();
            }
            this.lastEventIdentifier = songIdentifier;
            if (trackInfo.name != null) {
                this.subTitle.setText(trackInfo.name);
            } else {
                this.subTitle.setText("");
            }
            if (trackInfo.author != null) {
                this.title.setText(trackInfo.author);
            } else {
                this.title.setText("");
            }
            if (!(trackInfo.duration == null || trackInfo.currentPosition == null)) {
                this.progressBar.setMax(trackInfo.duration.intValue());
                this.progressBar.setSecondaryProgress(trackInfo.duration.intValue());
            }
            if (MusicManager.tryingToPlay(trackInfo.playbackState)) {
                this.image.setAlpha(1.0f);
            } else {
                this.image.setAlpha(0.5f);
            }
            this.choiceLayout.updateControls(mediaControls);
        }
    }

    public void onAlbumArtUpdate(@Nullable final ByteString photo, final boolean animate) {
        if (photo == null) {
            sLogger.v("ByteString image to set in notification is null");
            setDefaultImage(animate);
            return;
        }
        TaskManager.getInstance().execute(new Runnable() {
            /* JADX WARNING: inconsistent code. */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                MusicNotification.sLogger.d("PhotoUpdate runnable " + photo);
                byte[] byteArray = photo.toByteArray();
                if (byteArray == null || byteArray.length <= 0) {
                    MusicNotification.sLogger.i("Received photo has null or empty byte array");
                    MusicNotification.this.setDefaultImage(true);
                    return;
                }
                int size = HudApplication.getAppContext().getResources().getDimensionPixelSize(R.dimen.music_notification_image_size);
                Bitmap artwork = null;
                Bitmap scaled = null;
                try {
                    artwork = ScalingUtilities.decodeByteArray(byteArray, size, size, MusicNotification.this.FIT);
                    scaled = ScalingUtilities.createScaledBitmap(artwork, size, size, MusicNotification.this.FIT);
                    MusicNotification.this.setImage(scaled, animate);
                    if (artwork != null && artwork != scaled) {
                        artwork.recycle();
                    }
                } catch (Exception e) {
                    MusicNotification.sLogger.e("Error updating the artwork received ", e);
                    MusicNotification.this.setDefaultImage(animate);
                    if (artwork != null && artwork != scaled) {
                        artwork.recycle();
                    }
                } catch (Throwable th) {
                    if (!(artwork == null || artwork == scaled)) {
                        artwork.recycle();
                    }
                }
            }
        }, 1);
    }

    private void setDefaultImage(final boolean animate) {
        this.handler.post(new Runnable() {
            public void run() {
                MusicNotification.this.image.setArtworkBitmap(null, animate);
            }
        });
    }

    private void setImage(final Bitmap artwork, final boolean animate) {
        this.handler.post(new Runnable() {
            public void run() {
                MusicNotification.this.image.setArtworkBitmap(artwork, animate);
            }
        });
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public IInputHandler nextHandler() {
        return null;
    }
}
