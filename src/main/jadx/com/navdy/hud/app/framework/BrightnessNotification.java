package com.navdy.hud.app.framework;

import android.animation.AnimatorSet;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.settings.HUDSettings;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout2.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Selection;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;
import com.navdy.hud.app.view.Gauge;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.List;

public class BrightnessNotification extends ContentObserver implements INotification, OnSharedPreferenceChangeListener {
    private static final int DEFAULT_AUTO_BRIGHTNESS_ADJUSTMENT = Integer.parseInt("0");
    private static final int DEFAULT_BRIGHTNESS_INT = Integer.parseInt("128");
    private static final long DETENT_TIMEOUT = 250;
    private static final Handler HANDLER = new Handler();
    private static final int INITIAL_CHANGING_STEP = 1;
    private static final int MAX_BRIGHTNESS = 255;
    private static final int MAX_STEP = 16;
    private static final int NOTIFICATION_TIMEOUT = 10000;
    private static final Uri SCREEN_BRIGHTNESS_SETTING_URI = System.getUriFor("screen_brightness");
    private static final Logger sLogger = new Logger(BrightnessNotification.class);
    private final boolean AUTO_BRIGHTNESS_ADJ_ENABLED;
    private String autoSuffix;
    private int changingStep;
    private ChoiceLayout2 choiceLayout;
    private ViewGroup container;
    private INotificationController controller;
    private int currentAdjustmentValue;
    private int currentValue;
    private boolean isAutoBrightnessOn;
    private boolean isLastChangeIncreasing;
    private long lastChangeActionTime;
    private int previousAdjustmentValue;
    private int previousBrightnessValue;
    private Gauge progressBar;
    private Resources resources;
    private SharedPreferences sharedPreferences;
    private Editor sharedPreferencesEditor;
    private boolean showAutoBrightnessAdjustmentUI;
    private TextView textIndicator;

    public BrightnessNotification() {
        super(new Handler());
        this.autoSuffix = "";
        this.changingStep = 1;
        this.lastChangeActionTime = -251;
        this.isLastChangeIncreasing = false;
        this.showAutoBrightnessAdjustmentUI = false;
        this.AUTO_BRIGHTNESS_ADJ_ENABLED = true;
        this.sharedPreferences = RemoteDeviceManager.getInstance().getSharedPreferences();
        this.sharedPreferencesEditor = this.sharedPreferences.edit();
        this.isAutoBrightnessOn = getCurrentIsAutoBrightnessOn();
        this.showAutoBrightnessAdjustmentUI = this.isAutoBrightnessOn;
        this.resources = HudApplication.getAppContext().getResources();
        this.autoSuffix = this.resources.getString(R.string.brightness_auto_suffix);
    }

    public void onChange(boolean selfChange, Uri uri) {
        if (!selfChange && SCREEN_BRIGHTNESS_SETTING_URI.equals(uri)) {
            updateState();
        }
    }

    public NotificationType getType() {
        return NotificationType.BRIGHTNESS;
    }

    public String getId() {
        return NotificationId.BRIGHTNESS_NOTIFICATION_ID;
    }

    public View getView(Context context) {
        if (this.container == null) {
            if (this.showAutoBrightnessAdjustmentUI) {
                this.container = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.notification_auto_brightness, null);
            } else {
                this.container = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.notification_brightness, null);
            }
            this.textIndicator = (TextView) this.container.findViewById(R.id.subTitle);
            this.progressBar = (Gauge) this.container.findViewById(R.id.circle_progress);
            this.progressBar.setAnimated(false);
            showFixedChoices(this.container);
        }
        return this.container;
    }

    public View getExpandedView(Context context, Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    private void showFixedChoices(ViewGroup container) {
        this.choiceLayout = (ChoiceLayout2) container.findViewById(R.id.choiceLayout);
        List<Choice> choices = new ArrayList(3);
        Resources resources = container.getResources();
        String dismiss = resources.getString(R.string.call_dismiss);
        int okColor = resources.getColor(R.color.glance_ok_blue);
        choices.add(new Choice(2, R.drawable.icon_glances_ok, okColor, R.drawable.icon_glances_ok, -16777216, dismiss, okColor));
        this.choiceLayout.setChoices(choices, 1, new IListener() {
            public void executeItem(Selection selection) {
                BrightnessNotification.this.close();
            }

            public void itemSelected(Selection selection) {
            }
        });
        this.choiceLayout.setVisibility(0);
    }

    public void onStart(INotificationController controller) {
        HudApplication.getAppContext().getContentResolver().registerContentObserver(SCREEN_BRIGHTNESS_SETTING_URI, false, this);
        this.controller = controller;
        this.sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        this.previousBrightnessValue = getCurrentBrightness();
        this.previousAdjustmentValue = getCurrentAutoBrigthnessAdjustment();
        updateState();
    }

    public void updateState() {
        if (this.controller == null) {
            sLogger.v("brightness notif offscreen");
            return;
        }
        this.currentValue = getCurrentBrightness();
        sLogger.v("Current brightness :" + this.currentValue);
        if (this.showAutoBrightnessAdjustmentUI) {
            this.currentAdjustmentValue = getCurrentAutoBrigthnessAdjustment();
            this.progressBar.setValue(this.currentAdjustmentValue + 64);
            if (this.currentValue >= 255) {
                this.textIndicator.setText(R.string.max);
                return;
            } else if (this.currentValue <= 0) {
                this.textIndicator.setText(R.string.minimum);
                return;
            } else {
                this.textIndicator.setText(null);
                return;
            }
        }
        TextView textView = this.textIndicator;
        Resources resources = this.resources;
        Object[] objArr = new Object[3];
        objArr[0] = Integer.valueOf(this.currentValue);
        objArr[1] = Integer.valueOf(255);
        objArr[2] = this.isAutoBrightnessOn ? this.autoSuffix : "";
        textView.setText(resources.getString(R.string.brightness_notification_subtitle, objArr));
        this.progressBar.setValue(this.currentValue);
    }

    public void onUpdate() {
        if (this.controller != null) {
            this.controller.resetTimeout();
        }
        updateState();
    }

    public void onStop() {
        HudApplication.getAppContext().getContentResolver().unregisterContentObserver(this);
        this.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
        this.controller = null;
    }

    public int getTimeout() {
        return 10000;
    }

    public boolean isAlive() {
        return false;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return false;
    }

    public int getColor() {
        return 0;
    }

    public void onNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    public boolean supportScroll() {
        return false;
    }

    public static void showNotification() {
        NotificationManager.getInstance().addNotification(new BrightnessNotification());
    }

    private void close() {
        NotificationManager.getInstance().removeNotification(getId());
        if (this.isAutoBrightnessOn) {
            AnalyticsSupport.recordAutobrightnessAdjustmentChanged(this.previousBrightnessValue, this.previousAdjustmentValue, getCurrentAutoBrigthnessAdjustment());
        } else {
            AnalyticsSupport.recordBrightnessChanged(this.previousBrightnessValue, getCurrentBrightness());
        }
    }

    public void onClick() {
        close();
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    private void detentChangingStep(boolean isChangeIncreasing) {
        long now = SystemClock.elapsedRealtime();
        if (isChangeIncreasing != this.isLastChangeIncreasing || this.lastChangeActionTime + 250 < now) {
            this.changingStep = 1;
        } else {
            this.changingStep <<= 1;
            if (this.changingStep > 16) {
                this.changingStep = 16;
            }
        }
        this.isLastChangeIncreasing = isChangeIncreasing;
        this.lastChangeActionTime = now;
    }

    public boolean onKey(CustomKeyEvent event) {
        if (this.controller == null) {
            return false;
        }
        if (event != CustomKeyEvent.SELECT) {
            this.controller.resetTimeout();
        }
        switch (event) {
            case SELECT:
                if (this.choiceLayout != null) {
                    this.choiceLayout.executeSelectedItem();
                } else {
                    close();
                }
                return true;
            case LEFT:
                sLogger.v("Decreasing brightness");
                detentChangingStep(false);
                updateBrightness(-this.changingStep);
                return true;
            case RIGHT:
                sLogger.v("Increasing brightness");
                detentChangingStep(true);
                updateBrightness(this.changingStep);
                return true;
            default:
                return false;
        }
    }

    public IInputHandler nextHandler() {
        return null;
    }

    private boolean getCurrentIsAutoBrightnessOn() {
        return "true".equals(this.sharedPreferences.getString(HUDSettings.AUTO_BRIGHTNESS, ""));
    }

    private int getCurrentBrightness() {
        if (this.isAutoBrightnessOn) {
            try {
                return System.getInt(HudApplication.getAppContext().getContentResolver(), "screen_brightness");
            } catch (SettingNotFoundException e) {
                sLogger.e("Settings not found exception ", e);
                return DEFAULT_BRIGHTNESS_INT;
            }
        }
        try {
            return Integer.parseInt(this.sharedPreferences.getString(HUDSettings.BRIGHTNESS, "128"));
        } catch (NumberFormatException e2) {
            sLogger.e("Cannot parse brightness from the shared preferences", e2);
            return DEFAULT_BRIGHTNESS_INT;
        }
    }

    private int getCurrentAutoBrigthnessAdjustment() {
        try {
            return Integer.parseInt(this.sharedPreferences.getString(HUDSettings.AUTO_BRIGHTNESS_ADJUSTMENT, "0"));
        } catch (NumberFormatException e) {
            sLogger.e("Cannot parse auto brightness adjustment from the shared preferences", e);
            return DEFAULT_AUTO_BRIGHTNESS_ADJUSTMENT;
        }
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(HUDSettings.BRIGHTNESS) || key.equals(HUDSettings.AUTO_BRIGHTNESS_ADJUSTMENT)) {
            updateState();
        } else if (key.equals(HUDSettings.AUTO_BRIGHTNESS)) {
            boolean newValue = "true".equals(key);
            if (this.isAutoBrightnessOn != newValue) {
                this.isAutoBrightnessOn = newValue;
                updateState();
            }
        }
    }

    private void updateBrightness(int delta) {
        if (delta != 0) {
            int newValue;
            if (this.showAutoBrightnessAdjustmentUI) {
                newValue = this.currentAdjustmentValue + delta;
                if (newValue < -64) {
                    newValue = -64;
                }
                if (newValue > 64) {
                    newValue = 64;
                }
                this.sharedPreferencesEditor.putString(HUDSettings.AUTO_BRIGHTNESS_ADJUSTMENT, String.valueOf(newValue));
                this.sharedPreferencesEditor.apply();
                return;
            }
            newValue = this.currentValue + delta;
            if (newValue < 0) {
                newValue = 0;
            }
            if (newValue > 255) {
                newValue = 255;
            }
            this.sharedPreferencesEditor.putString(HUDSettings.BRIGHTNESS, String.valueOf(newValue));
            this.sharedPreferencesEditor.apply();
        }
    }
}
