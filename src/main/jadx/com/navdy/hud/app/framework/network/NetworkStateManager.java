package com.navdy.hud.app.framework.network;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.settings.NetworkStateChange;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import javax.inject.Inject;
import mortar.Mortar;

public class NetworkStateManager {
    private static final String NAVDY_NETWORK_DOWN_INTENT = "com.navdy.hud.NetworkDown";
    private static final String NAVDY_NETWORK_UP_INTENT = "com.navdy.hud.NetworkUp";
    private static final Logger sLogger = new Logger(NetworkStateManager.class);
    private static final NetworkStateManager singleton = new NetworkStateManager();
    @Inject
    Bus bus;
    private int countDown = 3;
    private Handler handler = new Handler(Looper.getMainLooper());
    private NetworkStateChange lastPhoneNetworkState;
    private Runnable networkCheck = new Runnable() {
        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            NetworkStateManager.sLogger.v("checking n/w state");
            NetworkInfo[] networkInfos = ((ConnectivityManager) HudApplication.getAppContext().getSystemService("connectivity")).getAllNetworkInfo();
            if (networkInfos == null || networkInfos.length == 0) {
                NetworkStateManager.sLogger.v("no n/w info");
                if (!NetworkStateManager.this.networkStateInitialized) {
                    NetworkStateManager.this.handler.postDelayed(NetworkStateManager.this.triggerNetworkCheck, 1000);
                    return;
                }
                return;
            }
            int i = 0;
            while (i < networkInfos.length) {
                try {
                    if (networkInfos[i].getType() == 8) {
                        NetworkStateManager.sLogger.v("found dummy network we are up:" + networkInfos[i].getTypeName() + " count down=" + NetworkStateManager.this.countDown);
                        NetworkStateManager.access$306(NetworkStateManager.this);
                        if (NetworkStateManager.this.countDown <= 0) {
                            NetworkStateManager.sLogger.v("found dummy network starting init");
                            NetworkStateManager.this.networkStateInitialized = true;
                            if (!RemoteDeviceManager.getInstance().isRemoteDeviceConnected() || !RemoteDeviceManager.getInstance().isNetworkLinkReady()) {
                                NetworkStateManager.this.networkNotAvailable();
                            } else if (NetworkStateManager.this.lastPhoneNetworkState == null || !NetworkStateManager.this.lastPhoneNetworkState.networkAvailable.booleanValue()) {
                                NetworkStateManager.this.networkNotAvailable();
                            } else {
                                NetworkStateManager.this.networkAvailable();
                            }
                            NetworkStateManager.this.bus.post(new HudNetworkInitialized());
                            if (!NetworkStateManager.this.networkStateInitialized) {
                                NetworkStateManager.sLogger.v("network state not initialized");
                            }
                            if (!NetworkStateManager.this.networkStateInitialized) {
                                NetworkStateManager.this.handler.postDelayed(NetworkStateManager.this.triggerNetworkCheck, 1000);
                            }
                        } else if (!NetworkStateManager.this.networkStateInitialized) {
                            NetworkStateManager.this.handler.postDelayed(NetworkStateManager.this.triggerNetworkCheck, 1000);
                            return;
                        } else {
                            return;
                        }
                    }
                    i++;
                } catch (Throwable th) {
                    if (!NetworkStateManager.this.networkStateInitialized) {
                        NetworkStateManager.this.handler.postDelayed(NetworkStateManager.this.triggerNetworkCheck, 1000);
                    }
                }
            }
            if (NetworkStateManager.this.networkStateInitialized) {
                NetworkStateManager.sLogger.v("network state not initialized");
            }
            if (!NetworkStateManager.this.networkStateInitialized) {
                NetworkStateManager.this.handler.postDelayed(NetworkStateManager.this.triggerNetworkCheck, 1000);
            }
        }
    };
    private volatile boolean networkStateInitialized;
    private Runnable triggerNetworkCheck = new Runnable() {
        public void run() {
            if (!NetworkStateManager.this.networkStateInitialized) {
                TaskManager.getInstance().execute(NetworkStateManager.this.networkCheck, 10);
            }
        }
    };

    public static class HudNetworkInitialized {
    }

    static /* synthetic */ int access$306(NetworkStateManager x0) {
        int i = x0.countDown - 1;
        x0.countDown = i;
        return i;
    }

    public static NetworkStateManager getInstance() {
        return singleton;
    }

    private NetworkStateManager() {
        Mortar.inject(HudApplication.getAppContext(), this);
        this.bus.register(this);
        if (DeviceUtil.isNavdyDevice()) {
            this.handler.post(this.triggerNetworkCheck);
            return;
        }
        this.networkStateInitialized = true;
        this.bus.post(new HudNetworkInitialized());
    }

    @Subscribe
    public void onNetworkStateChange(NetworkStateChange networkStateChange) {
        try {
            this.lastPhoneNetworkState = networkStateChange;
            sLogger.v("NetworkStateChange available[" + networkStateChange.networkAvailable + "] cell[" + networkStateChange.cellNetwork + "] wifi[" + networkStateChange.wifiNetwork + "] reach[" + networkStateChange.reachability + "]");
            if (Boolean.TRUE.equals(networkStateChange.networkAvailable)) {
                sLogger.v("DummyNetNetworkFactory:phone n/w avaialble");
                networkAvailable();
                return;
            }
            sLogger.v("DummyNetNetworkFactory:phone n/w lost");
            networkNotAvailable();
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    @Subscribe
    public void onConnectionStateChange(ConnectionStateChange event) {
        sLogger.d("ConnectionStateChange - " + event.state);
        switch (event.state) {
            case CONNECTION_DISCONNECTED:
                this.lastPhoneNetworkState = null;
                return;
            default:
                return;
        }
    }

    public NetworkStateChange getLastPhoneNetworkState() {
        return this.lastPhoneNetworkState;
    }

    public void networkAvailable() {
        if (!this.networkStateInitialized) {
            sLogger.v("network not initialized yet");
        } else if (NetworkBandwidthController.getInstance().isNetworkDisabled()) {
            sLogger.i("network is permanently disabled");
            networkNotAvailable();
        } else {
            HudApplication.getAppContext().sendBroadcastAsUser(new Intent(NAVDY_NETWORK_UP_INTENT), Process.myUserHandle());
            sLogger.v("DummyNetNetworkFactory:sent:com.navdy.hud.NetworkUp");
        }
    }

    public void networkNotAvailable() {
        if (this.networkStateInitialized) {
            HudApplication.getAppContext().sendBroadcastAsUser(new Intent(NAVDY_NETWORK_DOWN_INTENT), Process.myUserHandle());
            sLogger.v("DummyNetNetworkFactory:sent:com.navdy.hud.NetworkDown");
            return;
        }
        sLogger.v("network not initialized yet");
    }

    public static boolean isConnectedToNetwork(Context context) {
        NetworkInfo activeNetwork = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        sLogger.v("setEngineOnlineState active network=" + (activeNetwork != null ? activeNetwork.getTypeName() : " no active network "));
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static boolean isConnectedToWifi(Context context) {
        NetworkInfo wifiNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1);
        if (wifiNetworkInfo == null || !wifiNetworkInfo.isConnected()) {
            return false;
        }
        return true;
    }

    public static boolean isConnectedToCellNetwork(Context context) {
        NetworkInfo wifiNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(0);
        if (wifiNetworkInfo == null || !wifiNetworkInfo.isConnected()) {
            return false;
        }
        return true;
    }

    public boolean isNetworkStateInitialized() {
        return this.networkStateInitialized;
    }
}
