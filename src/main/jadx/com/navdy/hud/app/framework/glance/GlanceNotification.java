package com.navdy.hud.app.framework.glance;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import com.makeramen.RoundedTransformationBuilder;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.event.LocalSpeechRequest;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.contacts.Contact;
import com.navdy.hud.app.framework.contacts.ContactImageHelper;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.framework.contacts.ContactUtil.IContactCallback;
import com.navdy.hud.app.framework.contacts.NumberType;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader.PhotoDownloadStatus;
import com.navdy.hud.app.framework.fuel.FuelRoutingManager;
import com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.framework.notifications.IProgressUpdate;
import com.navdy.hud.app.framework.notifications.IScrollEvent;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.framework.recentcall.RecentCallManager.ContactFound;
import com.navdy.hud.app.framework.toast.ToastManager.ShowToast;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout2.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Selection;
import com.navdy.hud.app.ui.component.image.ColorImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView.Style;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.MenuMode;
import com.navdy.hud.app.util.PhoneUtil;
import com.navdy.hud.app.view.ObservableScrollView;
import com.navdy.hud.app.view.ObservableScrollView.IScrollListener;
import com.navdy.service.library.events.audio.CancelSpeechRequest;
import com.navdy.service.library.events.audio.SpeechRequest.Builder;
import com.navdy.service.library.events.audio.SpeechRequest.Category;
import com.navdy.service.library.events.audio.SpeechRequestStatus;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState;
import com.navdy.service.library.events.glances.CalendarConstants;
import com.navdy.service.library.events.glances.EmailConstants;
import com.navdy.service.library.events.glances.FuelConstants;
import com.navdy.service.library.events.glances.GenericConstants;
import com.navdy.service.library.events.glances.GlanceEvent;
import com.navdy.service.library.events.glances.GlanceEvent.GlanceActions;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Transformation;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class GlanceNotification implements INotification, IScrollEvent, IContactCallback {
    private static final float IMAGE_SCALE = 0.5f;
    private static Handler handler = new Handler(Looper.getMainLooper());
    private static final Logger sLogger = new Logger(GlanceNotification.class);
    private boolean alive;
    private int appIcon;
    private ImageView audioFeedback;
    private View bottomScrub;
    private Bus bus;
    private CancelSpeechRequest cancelSpeechRequest;
    private List<String> cannedReplyMessages;
    private ChoiceLayout2 choiceLayout;
    private IListener choiceListener;
    private final int color;
    private ColorImageView colorImageView;
    private List<Contact> contacts;
    private INotificationController controller;
    private final Map<String, String> data;
    private final GlanceApp glanceApp;
    private ViewGroup glanceContainer;
    private final GlanceEvent glanceEvent;
    private ViewGroup glanceExtendedContainer;
    private boolean hasFuelLevelInfo;
    private final String id;
    private boolean initialReplyMode;
    private ViewType largeType;
    private InitialsImageView mainImage;
    private TextView mainTitle;
    private String messageStr;
    private String number;
    private boolean onBottom;
    private boolean onTop;
    private Mode operationMode;
    private boolean photoCheckRequired;
    private final Date postTime;
    private IProgressUpdate progressUpdate;
    private ViewGroup replyExitView;
    private ViewGroup replyMsgView;
    private Transformation roundTransformation;
    private IScrollListener scrollListener;
    private ObservableScrollView scrollView;
    private ImageView sideImage;
    private ViewType smallType;
    private String source;
    private StringBuilder stringBuilder1;
    private StringBuilder stringBuilder2;
    private TextView subTitle;
    private final boolean supportsScroll;
    private TextView text1;
    private TextView text2;
    private TextView text3;
    private TimeHelper timeHelper;
    private View topScrub;
    private String ttsMessage;
    private boolean ttsSent;
    private Runnable updateTimeRunnable;

    public enum Mode {
        REPLY,
        READ
    }

    public GlanceNotification(GlanceEvent event, GlanceApp app, Map<String, String> data) {
        this(event, app, null, data);
    }

    public GlanceNotification(GlanceEvent event, GlanceApp app, ViewType largeType, Map<String, String> eventData) {
        this.stringBuilder1 = new StringBuilder();
        this.stringBuilder2 = new StringBuilder();
        this.roundTransformation = new RoundedTransformationBuilder().oval(true).build();
        this.alive = true;
        this.updateTimeRunnable = new Runnable() {
            public void run() {
                if (GlanceNotification.this.controller != null) {
                    try {
                        if (GlanceNotification.this.glanceExtendedContainer != null) {
                            TextView title = (TextView) GlanceNotification.this.glanceExtendedContainer.findViewById(R.id.title);
                            switch (AnonymousClass5.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[GlanceNotification.this.glanceApp.ordinal()]) {
                                case 1:
                                case 2:
                                case 3:
                                    GlanceNotification.this.setMainImage();
                                    break;
                                case 8:
                                    if (GlanceNotification.this.hasFuelLevelInfo) {
                                        int fuelLevel = ObdManager.getInstance().getFuelLevel();
                                        if (fuelLevel > Integer.parseInt((String) GlanceNotification.this.data.get(FuelConstants.FUEL_LEVEL.name()))) {
                                            GlanceNotification.handler.postDelayed(GlanceNotification.this.updateTimeRunnable, 30000);
                                            return;
                                        }
                                        GlanceNotification.this.data.put(FuelConstants.FUEL_LEVEL.name(), String.valueOf(fuelLevel));
                                    }
                                    FuelRoutingManager fuelRoutingManager = FuelRoutingManager.getInstance();
                                    if (!(fuelRoutingManager == null || fuelRoutingManager.getCurrentGasStation() == null)) {
                                        GlanceNotification.this.data.put(FuelConstants.GAS_STATION_DISTANCE.name(), String.valueOf(((double) Math.round((fuelRoutingManager.getCurrentGasStation().getLocation().getCoordinate().distanceTo(HereMapsManager.getInstance().getLastGeoPosition().getCoordinate()) * 10.0d) / 1609.34d)) / 10.0d));
                                    }
                                    GlanceNotification.this.setTitle();
                                    GlanceNotification.this.setExpandedContent(GlanceNotification.this.glanceExtendedContainer);
                                    break;
                                case 9:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                case 16:
                                case 17:
                                case 18:
                                case 19:
                                    title.setText(GlanceHelper.getTimeStr(System.currentTimeMillis(), GlanceNotification.this.postTime, GlanceNotification.this.timeHelper));
                                    break;
                            }
                            GlanceNotification.handler.postDelayed(GlanceNotification.this.updateTimeRunnable, 30000);
                        }
                    } catch (Throwable t) {
                        GlanceNotification.sLogger.e(t);
                    } finally {
                        GlanceNotification.handler.postDelayed(GlanceNotification.this.updateTimeRunnable, 30000);
                    }
                }
            }
        };
        this.choiceListener = new IListener() {
            public void executeItem(Selection selection) {
                if (GlanceNotification.this.controller != null) {
                    FuelRoutingManager fuelRoutingManager;
                    switch (selection.id) {
                        case 1:
                            GlanceNotification.this.switchToReplyScreen();
                            return;
                        case 2:
                            GlanceNotification.this.switchToMode(Mode.READ, null);
                            return;
                        case 3:
                            GlanceNotification.this.call();
                            return;
                        case 4:
                            GlanceNotification.this.switchToMode(Mode.REPLY, GlanceNotification.this.number);
                            return;
                        case 5:
                            if (GlanceNotification.this.choiceLayout.getTag(R.id.message_secondary_screen) != null) {
                                GlanceNotification.this.controller.collapseNotification(false, false);
                                return;
                            }
                            GlanceNotification.this.revertChoice();
                            GlanceNotification.this.controller.startTimeout(GlanceNotification.this.getTimeout());
                            return;
                        case 6:
                            GlanceNotification.this.dismissNotification();
                            return;
                        case 7:
                            fuelRoutingManager = FuelRoutingManager.getInstance();
                            if (fuelRoutingManager != null) {
                                fuelRoutingManager.routeToGasStation();
                            }
                            GlanceNotification.this.dismissNotification();
                            return;
                        case 8:
                            fuelRoutingManager = FuelRoutingManager.getInstance();
                            if (fuelRoutingManager != null) {
                                fuelRoutingManager.dismissGasRoute();
                            }
                            GlanceNotification.this.dismissNotification();
                            return;
                        default:
                            return;
                    }
                }
            }

            public void itemSelected(Selection selection) {
                if (GlanceNotification.this.controller != null) {
                    GlanceNotification.this.controller.resetTimeout();
                }
            }
        };
        this.scrollListener = new IScrollListener() {
            public void onTop() {
                if (GlanceNotification.this.controller != null && GlanceNotification.this.supportsScroll && GlanceNotification.this.scrollView != null) {
                    GlanceNotification.this.topScrub.setVisibility(8);
                    GlanceNotification.this.onTop = true;
                    GlanceNotification.this.onBottom = false;
                    if (GlanceNotification.this.progressUpdate != null) {
                        GlanceNotification.this.progressUpdate.onPosChange(1);
                    }
                }
            }

            public void onBottom() {
                if (GlanceNotification.this.controller != null && GlanceNotification.this.supportsScroll && GlanceNotification.this.scrollView != null) {
                    GlanceNotification.this.onTop = false;
                    GlanceNotification.this.onBottom = true;
                    if (GlanceNotification.this.progressUpdate != null) {
                        GlanceNotification.this.progressUpdate.onPosChange(100);
                    }
                }
            }

            public void onScroll(int l, int t, int oldl, int oldt) {
                if (GlanceNotification.this.controller != null && GlanceNotification.this.supportsScroll && GlanceNotification.this.scrollView != null) {
                    GlanceNotification.this.topScrub.setVisibility(0);
                    GlanceNotification.this.onTop = false;
                    GlanceNotification.this.onBottom = false;
                    if (GlanceNotification.this.progressUpdate != null) {
                        GlanceNotification.this.progressUpdate.onPosChange((int) ((((double) t) * 100.0d) / ((double) (GlanceNotification.this.scrollView.getChildAt(0).getBottom() - GlanceNotification.this.scrollView.getHeight()))));
                    }
                }
            }
        };
        this.glanceEvent = event;
        this.glanceApp = app;
        this.id = GlanceHelper.getNotificationId(this.glanceEvent);
        this.color = this.glanceApp.getColor();
        this.appIcon = this.glanceApp.getSideIcon();
        if (eventData == null) {
            this.data = GlanceHelper.buildDataMap(event);
        } else {
            this.data = eventData;
        }
        switch (app) {
            case FUEL:
                this.hasFuelLevelInfo = this.data.containsKey(FuelConstants.FUEL_LEVEL);
                break;
            case GENERIC:
                String iconStr = (String) this.data.get(GenericConstants.GENERIC_SIDE_ICON.name());
                if (!TextUtils.isEmpty(iconStr)) {
                    int nIcon = GlanceHelper.getIcon(iconStr);
                    if (nIcon != -1) {
                        this.appIcon = nIcon;
                        break;
                    }
                }
                break;
        }
        this.photoCheckRequired = GlanceHelper.isPhotoCheckRequired(this.glanceApp);
        this.postTime = new Date(event.postTime.longValue());
        this.timeHelper = RemoteDeviceManager.getInstance().getTimeHelper();
        this.smallType = GlanceHelper.getSmallViewType(this.glanceApp);
        if (largeType == null) {
            this.largeType = GlanceHelper.getLargeViewType(this.glanceApp);
        } else {
            this.largeType = largeType;
        }
        this.ttsMessage = GlanceHelper.getTtsMessage(this.glanceApp, this.data, this.stringBuilder1, this.stringBuilder2, this.timeHelper);
        this.messageStr = GlanceHelper.getGlanceMessage(this.glanceApp, this.data);
        this.cancelSpeechRequest = new CancelSpeechRequest(this.id);
        this.supportsScroll = GlanceViewCache.supportScroll(this.largeType);
        this.bus = RemoteDeviceManager.getInstance().getBus();
    }

    public NotificationType getType() {
        return this.glanceApp.notificationType;
    }

    public String getId() {
        return this.id;
    }

    public GlanceApp getGlanceApp() {
        return this.glanceApp;
    }

    public View getView(Context context) {
        this.glanceContainer = (ViewGroup) GlanceViewCache.getView(this.smallType, context);
        this.mainTitle = (TextView) this.glanceContainer.findViewById(R.id.mainTitle);
        this.subTitle = (TextView) this.glanceContainer.findViewById(R.id.subTitle);
        this.sideImage = (ImageView) this.glanceContainer.findViewById(R.id.sideImage);
        this.choiceLayout = (ChoiceLayout2) this.glanceContainer.findViewById(R.id.choiceLayout);
        this.audioFeedback = (ImageView) this.glanceContainer.findViewById(R.id.audioFeedback);
        if (GlanceHelper.isCalendarApp(this.glanceApp)) {
            this.colorImageView = (ColorImageView) this.glanceContainer.findViewById(R.id.mainImage);
            this.text1 = (TextView) this.glanceContainer.findViewById(R.id.text1);
            this.text2 = (TextView) this.glanceContainer.findViewById(R.id.text2);
            this.text3 = (TextView) this.glanceContainer.findViewById(R.id.text3);
        } else {
            this.mainImage = (InitialsImageView) this.glanceContainer.findViewById(R.id.mainImage);
        }
        return this.glanceContainer;
    }

    public View getExpandedView(Context context, Object data) {
        if (this.operationMode == Mode.READ || this.operationMode == null) {
            this.glanceExtendedContainer = (ViewGroup) GlanceViewCache.getView(this.largeType, context);
            if (this.supportsScroll) {
                this.topScrub = this.glanceExtendedContainer.findViewById(R.id.topScrub);
                this.topScrub.setVisibility(8);
                this.bottomScrub = this.glanceExtendedContainer.findViewById(R.id.bottomScrub);
                this.bottomScrub.setVisibility(0);
                this.scrollView = (ObservableScrollView) this.glanceExtendedContainer.findViewById(R.id.scrollView);
                this.scrollView.setScrollListener(this.scrollListener);
            }
            setExpandedContent(this.glanceExtendedContainer);
            return this.glanceExtendedContainer;
        }
        int current;
        NotificationManager notificationManager = NotificationManager.getInstance();
        if (this.initialReplyMode) {
            this.initialReplyMode = false;
            current = 1;
        } else {
            current = ((Integer) data).intValue();
        }
        View view = notificationManager.getExpandedViewChild();
        if (view != null) {
            removeParent(view);
        }
        return getReplyView(current, context);
    }

    public int getExpandedViewIndicatorColor() {
        return GlanceConstants.colorWhite;
    }

    public void onStart(INotificationController controller) {
        sLogger.v("start called:" + System.identityHashCode(this) + " expanded:" + controller.isExpandedWithStack());
        this.bus.register(this);
        this.controller = controller;
        if (this.choiceLayout != null) {
            this.choiceLayout.setTag(null);
            this.choiceLayout.setTag(R.id.message_prev_choice, null);
            this.choiceLayout.setTag(R.id.message_secondary_screen, null);
        }
        handler.postDelayed(this.updateTimeRunnable, 30000);
        updateState();
    }

    public void onUpdate() {
    }

    public void onStop() {
        sLogger.v("stop called:" + System.identityHashCode(this));
        handler.removeCallbacks(this.updateTimeRunnable);
        this.bus.unregister(this);
        cancelTts();
        if (this.choiceLayout != null) {
            this.choiceLayout.clear();
        }
        if (this.supportsScroll && this.scrollView != null) {
            this.scrollView.setScrollListener(null);
        }
        this.controller = null;
        if (this.glanceContainer != null) {
            cleanupView(this.smallType, this.glanceContainer);
            this.glanceContainer = null;
        }
        if (this.glanceExtendedContainer != null) {
            cleanupView(this.largeType, this.glanceExtendedContainer);
            this.glanceExtendedContainer = null;
        }
        removeParent(this.replyMsgView);
        this.replyMsgView = null;
        removeParent(this.replyExitView);
        this.replyExitView = null;
        this.operationMode = null;
    }

    public int getTimeout() {
        return GlanceConstants.DEFAULT_TIMEOUT;
    }

    public boolean isAlive() {
        return this.alive;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return true;
    }

    public int getColor() {
        return this.color;
    }

    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
    }

    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode mode) {
        if (mode == com.navdy.hud.app.ui.framework.UIStateManager.Mode.COLLAPSE) {
            cancelTts();
            this.operationMode = null;
            if (!(this.choiceLayout.getTag(R.id.message_secondary_screen) == null || this.controller == null)) {
                revertChoice();
                this.controller.startTimeout(getTimeout());
            }
            removeParent(this.replyMsgView);
            this.replyMsgView = null;
            removeParent(this.replyExitView);
            this.replyExitView = null;
            this.initialReplyMode = false;
            switch (this.glanceApp) {
                case GOOGLE_MAIL:
                case APPLE_MAIL:
                case GENERIC_MAIL:
                case GOOGLE_INBOX:
                    this.subTitle.setVisibility(0);
                    return;
                default:
                    return;
            }
        } else if (this.operationMode == Mode.READ || this.operationMode == null) {
            sendTts();
        } else {
            NotificationManager.getInstance().updateExpandedIndicator(getCannedReplyMessages().size() + 1, 1, GlanceConstants.colorWhite);
        }
    }

    public void onExpandedNotificationSwitched() {
        if (this.controller != null && this.controller.isExpandedWithStack()) {
            sendTts();
        }
    }

    public AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        AnimatorSet set = new AnimatorSet();
        ObjectAnimator o1;
        ObjectAnimator o2;
        ObjectAnimator o3;
        if (viewIn) {
            o1 = ObjectAnimator.ofFloat(this.mainTitle, View.ALPHA, new float[]{0.0f, 1.0f});
            o2 = ObjectAnimator.ofFloat(this.subTitle, View.ALPHA, new float[]{0.0f, 1.0f});
            o3 = ObjectAnimator.ofFloat(this.choiceLayout, View.ALPHA, new float[]{0.0f, 1.0f});
            set.playTogether(new Animator[]{o1, o2, o3});
        } else {
            o1 = ObjectAnimator.ofFloat(this.mainTitle, View.ALPHA, new float[]{1.0f, 0.0f});
            o2 = ObjectAnimator.ofFloat(this.subTitle, View.ALPHA, new float[]{1.0f, 0.0f});
            o3 = ObjectAnimator.ofFloat(this.choiceLayout, View.ALPHA, new float[]{1.0f, 0.0f});
            set.playTogether(new Animator[]{o1, o2, o3});
        }
        return set;
    }

    private void updateState() {
        if (RemoteDeviceManager.getInstance().getRemoteDeviceInfo() == null) {
            dismissNotification();
            return;
        }
        setTitle();
        setSubTitle();
        setSideImage();
        setMainImage();
        setChoices();
        if (this.controller.isExpandedWithStack()) {
            this.mainTitle.setAlpha(0.0f);
            this.subTitle.setAlpha(0.0f);
            this.choiceLayout.setAlpha(0.0f);
        }
    }

    private void dismissNotification() {
        NotificationManager.getInstance().removeNotification(this.id);
    }

    @Subscribe
    public void onPhotoDownload(PhotoDownloadStatus status) {
        if (this.controller == null || !this.photoCheckRequired || status.photoType != PhotoType.PHOTO_CONTACT) {
            return;
        }
        if (!TextUtils.equals(status.sourceIdentifier, this.source) && (this.number == null || !TextUtils.equals(status.sourceIdentifier, this.number))) {
            return;
        }
        if (!status.alreadyDownloaded || this.mainImage.getTag() == null) {
            sLogger.v("photo available");
            ContactUtil.setContactPhoto(this.source, this.number, false, this.mainImage, this.roundTransformation, this);
        }
    }

    @Subscribe
    public void onContactFound(ContactFound event) {
        if (this.controller != null) {
            List tag = this.choiceLayout.getTag();
            if (tag != null && tag == GlanceConstants.noNumberandNoReplyBack && event.contact != null && TextUtils.equals(event.identifier, this.source)) {
                for (com.navdy.service.library.events.contacts.Contact c : event.contact) {
                    if (this.contacts == null) {
                        this.contacts = new ArrayList();
                    }
                    Contact obj = new Contact(c);
                    obj.setName(this.source);
                    this.contacts.add(obj);
                    if (event.contact.size() == 1) {
                        this.number = c.number;
                    }
                }
                sLogger.v("contact info found:" + event.identifier + " number:" + this.number + " size:" + event.contact.size());
                setSubTitle();
                if (hasReplyAction()) {
                    this.choiceLayout.setChoices(GlanceConstants.numberAndReplyBack_1, 1, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(GlanceConstants.numberAndReplyBack_1);
                } else {
                    this.choiceLayout.setChoices(GlanceConstants.numberAndNoReplyBack, 1, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(GlanceConstants.numberAndNoReplyBack);
                }
                if (this.contacts == null || this.contacts.size() <= 1) {
                    ContactUtil.setContactPhoto(this.source, event.identifier, false, this.mainImage, this.roundTransformation, this);
                    return;
                }
                ContactImageHelper contactImageHelper = ContactImageHelper.getInstance();
                this.mainImage.setImage(contactImageHelper.getResourceId(contactImageHelper.getContactImageIndex(this.source)), ContactUtil.getInitials(this.source), Style.LARGE);
            }
        }
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        if (this.controller == null) {
            return true;
        }
        if (this.supportsScroll && this.scrollView != null && this.controller.isExpanded()) {
            boolean handled;
            switch (event) {
                case LEFT:
                    handled = this.scrollView.arrowScroll(33);
                    if (this.onTop) {
                        return false;
                    }
                    return handled;
                case RIGHT:
                    handled = this.scrollView.arrowScroll(130);
                    if (this.onBottom) {
                        return false;
                    }
                    return handled;
            }
        }
        if (this.operationMode == Mode.REPLY) {
            int current = NotificationManager.getInstance().getExpandedIndicatorCurrentItem();
            switch (event) {
                case LEFT:
                    return false;
                case RIGHT:
                    return false;
                case SELECT:
                    if (current == 0) {
                        this.controller.collapseNotification(false, false);
                    } else {
                        reply((String) getCannedReplyMessages().get(current - 1), this.number, this.source);
                    }
                    return true;
                default:
                    return true;
            }
        }
        if (!false) {
            switch (event) {
                case LEFT:
                    this.choiceLayout.moveSelectionLeft();
                    return true;
                case RIGHT:
                    this.choiceLayout.moveSelectionRight();
                    return true;
                case SELECT:
                    Choice choice = this.choiceLayout.getCurrentSelectedChoice();
                    if (choice != null) {
                        switch (choice.getId()) {
                            case 2:
                                AnalyticsSupport.recordGlanceAction(AnalyticsSupport.ANALYTICS_EVENT_GLANCE_OPEN_FULL, this, "dial");
                                break;
                            case 5:
                                AnalyticsSupport.recordGlanceAction(AnalyticsSupport.ANALYTICS_EVENT_GLANCE_OPEN_MINI, this, "dial");
                                break;
                            case 6:
                                AnalyticsSupport.recordGlanceAction(AnalyticsSupport.ANALYTICS_EVENT_GLANCE_DISMISS, this, "dial");
                                break;
                        }
                    }
                    this.choiceLayout.executeSelectedItem();
                    return true;
            }
        }
        return false;
    }

    public IInputHandler nextHandler() {
        return null;
    }

    public boolean isContextValid() {
        return this.controller != null;
    }

    private void revertChoice() {
        List<Choice> choices = null;
        int defaultSelection = 0;
        if (this.choiceLayout.getTag(R.id.message_secondary_screen) != null) {
            List tag = this.choiceLayout.getTag(R.id.message_prev_choice);
            this.choiceLayout.setTag(R.id.message_prev_choice, null);
            this.choiceLayout.setTag(R.id.message_secondary_screen, null);
            if (tag == GlanceConstants.numberAndReplyBack_1) {
                choices = GlanceConstants.numberAndReplyBack_1;
                defaultSelection = 1;
            } else if (tag == GlanceConstants.numberAndNoReplyBack) {
                choices = GlanceConstants.numberAndNoReplyBack;
                defaultSelection = 1;
            } else if (tag == GlanceConstants.noMessage) {
                choices = GlanceConstants.noMessage;
            } else if (tag == GlanceConstants.noNumberandNoReplyBack) {
                choices = GlanceConstants.noNumberandNoReplyBack;
            } else {
                this.choiceLayout.setTag(null);
                setChoices();
                return;
            }
        }
        this.choiceLayout.setAlpha(1.0f);
        this.choiceLayout.setVisibility(0);
        this.choiceLayout.setChoices(choices, defaultSelection, this.choiceListener, 0.5f);
        this.choiceLayout.setTag(choices);
    }

    private void setTitle() {
        this.source = GlanceHelper.getTitle(this.glanceApp, this.data);
        if (this.number == null) {
            this.number = GlanceHelper.getNumber(this.glanceApp, this.data);
        }
        boolean isNumber = false;
        if (this.number != null && TextUtils.equals(this.source, this.number)) {
            isNumber = true;
        }
        if (isNumber) {
            this.mainTitle.setText(PhoneUtil.formatPhoneNumber(this.source));
        } else {
            this.mainTitle.setText(this.source);
        }
    }

    private void setSubTitle() {
        String text = GlanceHelper.getSubTitle(this.glanceApp, this.data);
        if (!(!TextUtils.isEmpty(text) || this.number == null || TextUtils.equals(this.number, this.source))) {
            text = PhoneUtil.formatPhoneNumber(this.number);
        }
        this.subTitle.setText(text);
    }

    private void setSideImage() {
        this.sideImage.setImageResource(this.appIcon);
    }

    private void setMainImage() {
        if (GlanceHelper.isCalendarApp(this.glanceApp)) {
            this.colorImageView.setColor(this.glanceApp.getColor());
            this.text1.setText(GlanceConstants.starts);
            long millis = 0;
            String str = (String) this.data.get(CalendarConstants.CALENDAR_TIME.name());
            if (str != null) {
                try {
                    millis = Long.parseLong(str);
                } catch (Throwable t) {
                    sLogger.e(t);
                }
            }
            if (millis > 0) {
                int margin;
                String data = GlanceHelper.getCalendarTime(millis, this.stringBuilder1, this.stringBuilder2, this.timeHelper);
                str = this.stringBuilder1.toString();
                if (TextUtils.isEmpty(str)) {
                    String pmMarker = this.stringBuilder2.toString();
                    if (TextUtils.isEmpty(pmMarker)) {
                        margin = GlanceConstants.calendarNowMargin;
                        this.text2.setTextAppearance(HudApplication.getAppContext(), R.style.small_glance_sign_text_2);
                        this.text2.setText(data);
                        this.text3.setText("");
                    } else {
                        margin = GlanceConstants.calendarTimeMargin;
                        this.text3.setText("");
                        this.text2.setTextAppearance(HudApplication.getAppContext(), R.style.small_glance_sign_text_3);
                        SpannableStringBuilder spannable = new SpannableStringBuilder();
                        spannable.append(data);
                        int len = spannable.length();
                        spannable.append(pmMarker);
                        spannable.setSpan(new AbsoluteSizeSpan(GlanceConstants.calendarpmMarker), len, spannable.length(), 33);
                        this.text2.setText(spannable);
                    }
                } else {
                    margin = GlanceConstants.calendarNormalMargin;
                    this.text2.setTextAppearance(HudApplication.getAppContext(), R.style.small_glance_sign_text_1);
                    this.text2.setText(data);
                    this.text3.setText(str);
                }
                ((MarginLayoutParams) this.text2.getLayoutParams()).topMargin = margin;
            } else {
                this.text1.setText("");
                this.text2.setTextAppearance(HudApplication.getAppContext(), R.style.small_glance_sign_text_2);
                this.text2.setText(GlanceConstants.questionMark);
                this.text3.setText("");
                ((MarginLayoutParams) this.text2.getLayoutParams()).topMargin = GlanceConstants.calendarNowMargin;
            }
            this.ttsMessage = GlanceHelper.getTtsMessage(this.glanceApp, this.data, this.stringBuilder1, this.stringBuilder2, this.timeHelper);
            this.stringBuilder1.setLength(0);
            this.stringBuilder2.setLength(0);
            return;
        }
        int image = this.glanceApp.getMainIcon();
        if (this.glanceApp == GlanceApp.GENERIC) {
            String iconStr = (String) this.data.get(GenericConstants.GENERIC_MAIN_ICON.name());
            if (!TextUtils.isEmpty(iconStr)) {
                int nIcon = GlanceHelper.getIcon(iconStr);
                if (nIcon != -1) {
                    image = nIcon;
                }
            }
        }
        ContactImageHelper contactImageHelper;
        if (image != -1) {
            this.mainImage.setImage(image, null, Style.DEFAULT);
        } else if (this.photoCheckRequired) {
            if (this.contacts == null || this.contacts.size() <= 1) {
                ContactUtil.setContactPhoto(this.source, this.number, this.photoCheckRequired, this.mainImage, this.roundTransformation, this);
            } else {
                contactImageHelper = ContactImageHelper.getInstance();
                this.mainImage.setImage(contactImageHelper.getResourceId(contactImageHelper.getContactImageIndex(this.source)), ContactUtil.getInitials(this.source), Style.LARGE);
            }
            this.mainImage.requestLayout();
        } else if (this.glanceApp.isDefaultIconBasedOnId()) {
            contactImageHelper = ContactImageHelper.getInstance();
            this.mainImage.setImage(contactImageHelper.getResourceId(contactImageHelper.getContactImageIndex(this.source)), ContactUtil.getInitials(this.source), Style.LARGE);
        }
    }

    private void setChoices() {
        if (this.controller.isExpandedWithStack()) {
            switchToMode(Mode.READ, null);
            return;
        }
        List<Choice> choices = null;
        int defaultSelection = 0;
        boolean replyAction = false;
        switch (this.glanceApp) {
            case FUEL:
                if (this.data.get(FuelConstants.NO_ROUTE.name()) == null) {
                    choices = GlanceConstants.fuelChoices;
                } else {
                    choices = GlanceConstants.fuelChoicesNoRoute;
                }
                defaultSelection = 0;
                break;
            default:
                if (this.choiceLayout.getTag() == null) {
                    if (hasReplyAction()) {
                        replyAction = true;
                    }
                    if (this.number != null || this.contacts != null) {
                        if (!replyAction) {
                            choices = GlanceConstants.numberAndNoReplyBack;
                            defaultSelection = 1;
                            break;
                        }
                        choices = GlanceConstants.numberAndReplyBack_1;
                        defaultSelection = 1;
                        break;
                    } else if (!GlanceHelper.isCalendarApp(this.glanceApp)) {
                        choices = GlanceConstants.noNumberandNoReplyBack;
                        break;
                    } else {
                        choices = GlanceConstants.calendarOptions;
                        break;
                    }
                }
                break;
        }
        this.choiceLayout.setAlpha(1.0f);
        this.choiceLayout.setVisibility(0);
        this.choiceLayout.setChoices(choices, defaultSelection, this.choiceListener, 0.5f);
        this.choiceLayout.setTag(choices);
    }

    private void call() {
        dismissNotification();
        RemoteDeviceManager.getInstance().getCallManager().dial(this.number, null, this.source);
    }

    private void switchToMode(Mode mode, String number) {
        if (this.controller != null) {
            this.controller.stopTimeout(true);
            this.choiceLayout.setTag(R.id.message_prev_choice, this.choiceLayout.getTag());
            this.choiceLayout.setTag(R.id.message_secondary_screen, Integer.valueOf(1));
            if (mode == Mode.READ) {
                this.operationMode = mode;
                this.choiceLayout.setChoices(GlanceConstants.backChoice2, 0, this.choiceListener, 0.5f);
                this.choiceLayout.setTag(GlanceConstants.backChoice);
                if (!this.controller.isExpandedWithStack()) {
                    this.controller.expandNotification(true);
                    return;
                }
                return;
            }
            this.operationMode = mode;
            this.choiceLayout.setChoices(null, 0, null, 0.5f);
            this.choiceLayout.setTag(GlanceConstants.backChoice);
            if (!this.controller.isExpandedWithStack()) {
                this.initialReplyMode = true;
                this.controller.expandNotification(false);
            }
        }
    }

    private void reply(String message, String number, String caller) {
        this.alive = false;
        GlanceHandler glanceHandler = GlanceHandler.getInstance();
        if (glanceHandler.sendMessage(number, message, caller)) {
            glanceHandler.sendSmsSuccessNotification(this.id, number, message, caller);
        } else {
            glanceHandler.sendSmsFailedNotification(number, message, caller);
        }
    }

    public boolean isShowingReadOption() {
        if (this.choiceLayout != null) {
            List tag = this.choiceLayout.getTag();
            if (tag == GlanceConstants.noNumberandNoReplyBack || tag == GlanceConstants.numberAndNoReplyBack || tag == GlanceConstants.numberAndReplyBack_1) {
                return true;
            }
        }
        return false;
    }

    public boolean expandNotification() {
        if (this.choiceLayout.getTag(R.id.message_secondary_screen) != null) {
            return false;
        }
        switchToMode(Mode.READ, null);
        return true;
    }

    public boolean supportScroll() {
        return this.supportsScroll;
    }

    private void setExpandedContent(View view) {
        if (this.glanceExtendedContainer != null) {
            switch (this.glanceApp) {
                case GOOGLE_CALENDAR:
                case APPLE_CALENDAR:
                case GENERIC_CALENDAR:
                    TextView textview = (TextView) view.findViewById(R.id.text1);
                    String s = (String) this.data.get(CalendarConstants.CALENDAR_TIME_STR.name());
                    if (TextUtils.isEmpty(s)) {
                        textview.setVisibility(8);
                    } else {
                        textview.setText(s);
                        textview.setVisibility(0);
                    }
                    textview = (TextView) view.findViewById(R.id.text2);
                    s = (String) this.data.get(CalendarConstants.CALENDAR_TITLE.name());
                    if (TextUtils.isEmpty(s)) {
                        textview.setVisibility(8);
                    } else {
                        textview.setText(s);
                        textview.setVisibility(0);
                    }
                    ViewGroup viewGroup = (ViewGroup) view.findViewById(R.id.locationContainer);
                    s = (String) this.data.get(CalendarConstants.CALENDAR_LOCATION.name());
                    if (TextUtils.isEmpty(s)) {
                        viewGroup.setVisibility(8);
                        return;
                    }
                    ((TextView) view.findViewById(R.id.text3)).setText(s);
                    viewGroup.setVisibility(0);
                    return;
                case GOOGLE_MAIL:
                case APPLE_MAIL:
                case GENERIC_MAIL:
                case GOOGLE_INBOX:
                case GOOGLE_HANGOUT:
                case SLACK:
                case WHATS_APP:
                case FACEBOOK_MESSENGER:
                case FACEBOOK:
                case GENERIC_MESSAGE:
                case TWITTER:
                case GENERIC_SOCIAL:
                case IMESSAGE:
                case SMS:
                case GENERIC:
                    TextView title = (TextView) view.findViewById(R.id.title);
                    TextView message = (TextView) view.findViewById(R.id.message);
                    switch (this.glanceApp) {
                        case GOOGLE_MAIL:
                        case APPLE_MAIL:
                        case GENERIC_MAIL:
                        case GOOGLE_INBOX:
                            title.setTextColor(GlanceConstants.colorWhite);
                            this.subTitle.setVisibility(4);
                            String subject = (String) this.data.get(EmailConstants.EMAIL_SUBJECT.name());
                            if (subject == null) {
                                subject = "";
                            }
                            title.setText(subject);
                            break;
                        default:
                            title.setText(GlanceHelper.getTimeStr(System.currentTimeMillis(), this.postTime, this.timeHelper));
                            break;
                    }
                    message.setText(this.messageStr);
                    return;
                case FUEL:
                    TextView textView1 = (TextView) view.findViewById(R.id.title1);
                    textView1.setTextAppearance(view.getContext(), R.style.glance_title_1);
                    TextView textView2 = (TextView) view.findViewById(R.id.title2);
                    textView2.setTextAppearance(view.getContext(), R.style.glance_title_2);
                    textView1.setText((CharSequence) this.data.get(FuelConstants.GAS_STATION_NAME.name()));
                    if (this.data.get(FuelConstants.NO_ROUTE.name()) == null) {
                        textView2.setText(view.getContext().getResources().getString(R.string.gas_station_detail, new Object[]{this.data.get(FuelConstants.GAS_STATION_ADDRESS.name()), this.data.get(FuelConstants.GAS_STATION_DISTANCE.name())}));
                    } else {
                        textView2.setText(view.getContext().getResources().getString(R.string.i_cannot_add_gasstation));
                    }
                    textView1.setVisibility(0);
                    textView2.setVisibility(0);
                    return;
                default:
                    return;
            }
        }
    }

    private void cleanupView(ViewType viewType, ViewGroup viewGroup) {
        ViewGroup parent = (ViewGroup) viewGroup.getParent();
        if (parent != null) {
            parent.removeView(viewGroup);
        }
        switch (viewType) {
            case SMALL_GLANCE_MESSAGE:
                this.mainTitle.setAlpha(1.0f);
                this.mainTitle.setVisibility(0);
                this.subTitle.setAlpha(1.0f);
                this.subTitle.setVisibility(0);
                this.choiceLayout.setAlpha(1.0f);
                this.choiceLayout.setVisibility(0);
                this.choiceLayout.setTag(null);
                this.choiceLayout.setTag(R.id.message_prev_choice, null);
                this.choiceLayout.setTag(R.id.message_secondary_screen, null);
                this.mainImage.setImage(0, null, Style.DEFAULT);
                this.mainImage.setTag(null);
                this.sideImage.setImageResource(0);
                viewGroup.setAlpha(1.0f);
                break;
            case BIG_GLANCE_MESSAGE:
                viewGroup.setAlpha(1.0f);
                if (this.scrollView != null) {
                    this.scrollView.fullScroll(33);
                    this.onTop = true;
                    this.onBottom = false;
                    break;
                }
                break;
            case BIG_GLANCE_MESSAGE_SINGLE:
                ((TextView) viewGroup.findViewById(R.id.title)).setTextAppearance(HudApplication.getAppContext(), R.style.glance_message_title);
                break;
            case BIG_MULTI_TEXT:
            case BIG_CALENDAR:
                break;
            case SMALL_SIGN:
                this.mainTitle.setAlpha(1.0f);
                this.mainTitle.setVisibility(0);
                this.subTitle.setAlpha(1.0f);
                this.subTitle.setVisibility(0);
                this.choiceLayout.setAlpha(1.0f);
                this.choiceLayout.setVisibility(0);
                this.choiceLayout.setTag(null);
                this.choiceLayout.setTag(R.id.message_prev_choice, null);
                this.choiceLayout.setTag(R.id.message_secondary_screen, null);
                this.sideImage.setImageResource(0);
                viewGroup.setAlpha(1.0f);
                break;
        }
        viewGroup.setAlpha(1.0f);
        GlanceViewCache.putView(viewType, viewGroup);
    }

    private void sendTts() {
        if (this.controller != null && this.controller.isTtsOn() && !this.ttsSent) {
            sLogger.v("tts-send [" + this.id + "]");
            this.bus.post(new LocalSpeechRequest(new Builder().words(this.ttsMessage).category(Category.SPEECH_MESSAGE_READ_OUT).id(this.id).sendStatus(Boolean.valueOf(true)).build()));
            this.ttsSent = true;
        }
    }

    private void cancelTts() {
        if (this.controller != null && this.controller.isTtsOn() && this.ttsSent) {
            sLogger.v("tts-cancelled [" + this.id + "]");
            this.bus.post(new RemoteEvent(this.cancelSpeechRequest));
            this.ttsSent = false;
            stopAudioAnimation(false);
        }
    }

    private ViewGroup getReplyView(int item, Context context) {
        if (item == 0) {
            if (this.replyExitView == null) {
                this.replyExitView = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.glance_reply_exit, null, false);
            } else {
                removeParent(this.replyExitView);
            }
            return this.replyExitView;
        }
        if (this.replyMsgView == null) {
            this.replyMsgView = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.glance_large_message_single, null, false);
        } else {
            removeParent(this.replyMsgView);
        }
        List<String> list = getCannedReplyMessages();
        if (item > list.size()) {
            item = list.size();
        }
        ((TextView) this.replyMsgView.findViewById(R.id.title)).setText(GlanceConstants.message);
        ((TextView) this.replyMsgView.findViewById(R.id.message)).setText((CharSequence) list.get(item - 1));
        return this.replyMsgView;
    }

    private void removeParent(View view) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
                sLogger.v("reply view removed");
            }
        }
    }

    @Subscribe
    public void onShowToast(ShowToast event) {
        if (TextUtils.equals(event.name, this.id + "message-sent-toast")) {
            sLogger.v("reply toast:animateOutExpandedView");
            NotificationManager.getInstance().collapseExpandedNotification(true, true);
        }
    }

    @Subscribe
    public void onSpeechRequestNotification(SpeechRequestStatus event) {
        if (this.audioFeedback != null && !TextUtils.isEmpty(this.id) && TextUtils.equals(this.id, event.id) && event.status != null) {
            switch (event.status) {
                case SPEECH_REQUEST_STARTING:
                    startAudioAnimation();
                    return;
                case SPEECH_REQUEST_STOPPED:
                    stopAudioAnimation(true);
                    return;
                default:
                    return;
            }
        }
    }

    @Subscribe
    public void onConnectionStateChange(ConnectionStateChange event) {
        if (event.state == ConnectionState.CONNECTION_DISCONNECTED) {
            stopAudioAnimation(true);
        }
    }

    public void setListener(IProgressUpdate callback) {
        this.progressUpdate = callback;
    }

    private void startAudioAnimation() {
        if (this.audioFeedback != null) {
            this.audioFeedback.animate().cancel();
            this.audioFeedback.setAlpha(1.0f);
            this.audioFeedback.setVisibility(0);
            this.audioFeedback.setImageResource(R.drawable.audio_loop);
            ((AnimationDrawable) this.audioFeedback.getDrawable()).start();
        }
    }

    private void stopAudioAnimation(boolean showFadeOutAnimation) {
        if (this.audioFeedback != null && this.audioFeedback.getVisibility() == 0) {
            AnimationDrawable animationDrawable = (AnimationDrawable) this.audioFeedback.getDrawable();
            if (animationDrawable.isRunning()) {
                animationDrawable.stop();
                if (showFadeOutAnimation) {
                    this.audioFeedback.animate().alpha(0.0f).setDuration(300).withEndAction(new Runnable() {
                        public void run() {
                            GlanceNotification.this.audioFeedback.setVisibility(8);
                            GlanceNotification.this.audioFeedback.setAlpha(1.0f);
                        }
                    });
                } else {
                    this.audioFeedback.setVisibility(8);
                }
            }
        }
    }

    private boolean hasReplyAction() {
        return this.glanceEvent.actions != null && this.glanceEvent.actions.contains(GlanceActions.REPLY);
    }

    private List<String> getCannedReplyMessages() {
        if (this.cannedReplyMessages == null) {
            this.cannedReplyMessages = GlanceConstants.getCannedMessages();
        }
        return this.cannedReplyMessages;
    }

    private void switchToReplyScreen() {
        Bundle args = new Bundle();
        ArrayList<Parcelable> contactList = new ArrayList();
        if (this.contacts == null) {
            contactList.add(new Contact(this.source, this.number, NumberType.OTHER, ContactImageHelper.getInstance().getContactImageIndex(this.number), 0));
        } else {
            contactList.addAll(this.contacts);
        }
        args.putParcelableArrayList(MainMenuScreen2.ARG_CONTACTS_LIST, contactList);
        args.putString(MainMenuScreen2.ARG_NOTIFICATION_ID, this.id);
        args.putInt(MainMenuScreen2.ARG_MENU_MODE, MenuMode.REPLY_PICKER.ordinal());
        NotificationManager.getInstance().removeNotification(this.id, false, Screen.SCREEN_MAIN_MENU, args, null);
    }

    public void setContactList(List<Contact> list) {
        this.contacts = list;
    }
}
