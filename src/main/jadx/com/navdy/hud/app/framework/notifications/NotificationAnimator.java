package com.navdy.hud.app.framework.notifications;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.widget.FrameLayout.LayoutParams;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;
import com.navdy.hud.app.view.NotificationView;
import com.navdy.service.library.log.Logger;

public class NotificationAnimator {
    private static Logger sLogger = NotificationManager.sLogger;

    public enum Operation {
        MOVE_PREV,
        MOVE_NEXT,
        SELECT,
        COLLAPSE_VIEW,
        UPDATE_INDICATOR,
        REMOVE_NOTIFICATION
    }

    public static class OperationInfo {
        boolean collapse;
        boolean gesture;
        boolean hideNotification;
        String id;
        Operation operation;
        boolean quick;

        OperationInfo(Operation operation, boolean hideNotification, boolean quick, boolean gesture, String id, boolean collapse) {
            this.operation = operation;
            this.hideNotification = hideNotification;
            this.quick = quick;
            this.gesture = gesture;
            this.id = id;
            this.collapse = collapse;
        }
    }

    static void animateNotifViews(View viewIn, View viewOut, ViewGroup parent, int translationBy, int duration, int startDelay, Runnable endAction) {
        ViewPropertyAnimator out = null;
        ViewPropertyAnimator in = null;
        if (viewOut != null) {
            out = animateOut(viewOut, parent, translationBy, duration, 0, null);
        }
        if (viewIn != null) {
            in = animateIn(viewIn, parent, translationBy, duration, startDelay, endAction);
        }
        if (out != null) {
            out.start();
        }
        if (in != null) {
            in.start();
        }
    }

    static void animateExpandedViews(View viewIn, View viewOut, ViewGroup parent, int translationBy, int duration, int startDelay, Runnable endAction) {
        ViewPropertyAnimator out = null;
        ViewPropertyAnimator in = null;
        if (viewOut != null) {
            out = animateOut(viewOut, parent, translationBy, duration, 0, endAction);
            endAction = null;
        }
        if (viewIn != null) {
            in = animateIn(viewIn, parent, translationBy, duration, startDelay, endAction);
        }
        if (out != null) {
            out.start();
        }
        if (in != null) {
            in.start();
        }
    }

    private static ViewPropertyAnimator animateIn(final View view, ViewGroup parent, int translationBy, int duration, int startDelay, Runnable endAction) {
        view.setLayoutParams(new LayoutParams(-1, -1));
        view.setTranslationY((float) translationBy);
        view.setAlpha(0.0f);
        parent.addView(view);
        ViewPropertyAnimator animator = view.animate().alpha(1.0f).translationY(0.0f).setDuration((long) duration).setStartDelay((long) startDelay).withStartAction(new Runnable() {
            public void run() {
                view.setVisibility(0);
            }
        });
        if (endAction != null) {
            animator.withEndAction(endAction);
        }
        return animator;
    }

    private static ViewPropertyAnimator animateOut(final View view, final ViewGroup parent, int translationBy, int duration, int startDelay, Runnable endAction) {
        ViewPropertyAnimator animator = view.animate().alpha(0.0f).translationY((float) (-translationBy)).setDuration((long) duration).setStartDelay((long) startDelay).withEndAction(new Runnable() {
            public void run() {
                parent.removeView(view);
            }
        });
        if (endAction != null) {
            animator.withEndAction(endAction);
        }
        return animator;
    }

    static void animateChildViewOut(View child, int duration, Runnable endAction) {
        child.animate().alpha(0.0f).setDuration((long) duration).setStartDelay(0).withEndAction(endAction).start();
    }

    static void animateExpandedViewOut(final NotificationView notificationView, int newX, final View expandedNotifView, final View indicator, final Runnable endAction, final Info notificationObj, boolean showOn, boolean isExpandNotificationViewVisible) {
        Runnable runnable1 = new Runnable() {
            public void run() {
                indicator.setVisibility(8);
            }
        };
        Runnable runnable2 = new Runnable() {
            public void run() {
                expandedNotifView.setVisibility(8);
                NotificationManager.getInstance().setExpandedStack(false);
                if (notificationObj != null && notificationObj.startCalled) {
                    NotificationAnimator.sLogger.v("calling onExpandedNotificationEvent-collapse");
                    notificationObj.notification.onExpandedNotificationEvent(Mode.COLLAPSE);
                    NotificationManager.getInstance().setNotificationColor();
                    notificationView.showNextNotificationColor();
                }
                if (endAction != null) {
                    endAction.run();
                }
            }
        };
        if (showOn || isExpandNotificationViewVisible) {
            notificationView.animate().x((float) newX).withStartAction(runnable1).withEndAction(runnable2).start();
            return;
        }
        runnable1.run();
        runnable2.run();
    }
}
