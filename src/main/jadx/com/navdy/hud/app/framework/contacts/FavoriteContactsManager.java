package com.navdy.hud.app.framework.contacts;

import android.text.TextUtils;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.storage.db.helper.FavoriteContactsPersistenceHelper;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.contacts.Contact;
import com.navdy.service.library.events.contacts.FavoriteContactsRequest;
import com.navdy.service.library.events.contacts.FavoriteContactsResponse;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class FavoriteContactsManager {
    private static final FavoriteContactsChanged FAVORITE_CONTACTS_CHANGED = new FavoriteContactsChanged();
    private static final int MAX_FAV_CONTACTS = 30;
    private static final FavoriteContactsManager sInstance = new FavoriteContactsManager();
    private static final Logger sLogger = new Logger(FavoriteContactsManager.class);
    private Bus bus = RemoteDeviceManager.getInstance().getBus();
    private volatile List<Contact> favContacts;
    private boolean fullSync = true;
    private boolean isAllowedToReceiveContacts = false;

    private static class FavoriteContactsChanged {
        private FavoriteContactsChanged() {
        }

        /* synthetic */ FavoriteContactsChanged(AnonymousClass1 x0) {
            this();
        }
    }

    public boolean isAllowedToReceiveContacts() {
        return this.isAllowedToReceiveContacts;
    }

    public static FavoriteContactsManager getInstance() {
        return sInstance;
    }

    private FavoriteContactsManager() {
        this.bus.register(this);
    }

    public List<Contact> getFavoriteContacts() {
        return this.favContacts;
    }

    public void setFavoriteContacts(List<Contact> favContacts) {
        this.favContacts = favContacts;
        this.bus.post(FAVORITE_CONTACTS_CHANGED);
    }

    public void clearFavoriteContacts() {
        setFavoriteContacts(null);
    }

    public void buildFavoriteContacts() {
        if (GenericUtil.isMainThread()) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    FavoriteContactsManager.this.load();
                }
            }, 1);
        } else {
            load();
        }
    }

    public void syncFavoriteContacts(boolean fullSync) {
        try {
            this.fullSync = fullSync;
            this.bus.post(new RemoteEvent(new FavoriteContactsRequest(Integer.valueOf(30))));
            sLogger.v("sent fav-contact request");
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    @Subscribe
    public void onFavoriteContactResponse(final FavoriteContactsResponse response) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    if (response.status == RequestStatus.REQUEST_SUCCESS) {
                        FavoriteContactsManager.this.isAllowedToReceiveContacts = true;
                        if (response.contacts != null) {
                            FavoriteContactsManager.sLogger.v("onFavoriteContactResponse size[" + response.contacts.size() + "]");
                            HashSet<String> seenNumbers = new HashSet();
                            List<Contact> list = new ArrayList();
                            for (Contact c : response.contacts) {
                                if (TextUtils.isEmpty(c.number)) {
                                    FavoriteContactsManager.sLogger.v("null number");
                                } else if (seenNumbers.contains(c.number)) {
                                    FavoriteContactsManager.sLogger.v("number already seen:" + c.number);
                                } else {
                                    NumberType numberType = ContactUtil.getNumberType(c.numberType);
                                    String displayName = c.name;
                                    if (!(TextUtils.isEmpty(c.name) || ContactUtil.isDisplayNameValid(c.name, c.number, ContactUtil.getPhoneNumber(c.number)))) {
                                        displayName = null;
                                    }
                                    list.add(new Contact(displayName, c.number, numberType, ContactImageHelper.getInstance().getContactImageIndex(c.number), 0));
                                    seenNumbers.add(c.number);
                                    if (list.size() == 30) {
                                        FavoriteContactsManager.sLogger.v("exceeded max size");
                                        break;
                                    }
                                }
                            }
                            FavoriteContactsManager.sLogger.v("favorite contact size[" + list.size() + "]");
                            FavoriteContactsPersistenceHelper.storeFavoriteContacts(DriverProfileHelper.getInstance().getCurrentProfile().getProfileName(), list, FavoriteContactsManager.this.fullSync);
                            return;
                        }
                        FavoriteContactsManager.sLogger.w("fav-contact list returned is null");
                    } else if (response.status == RequestStatus.REQUEST_NOT_AVAILABLE) {
                        FavoriteContactsManager.this.isAllowedToReceiveContacts = false;
                    } else {
                        FavoriteContactsManager.sLogger.e("sent fav-contact response error:" + response.status);
                    }
                } catch (Throwable t) {
                    FavoriteContactsManager.sLogger.e(t);
                }
            }
        }, 1);
    }

    private void load() {
        try {
            String id = DriverProfileHelper.getInstance().getCurrentProfile().getProfileName();
            this.favContacts = FavoriteContactsPersistenceHelper.getFavoriteContacts(id);
            sLogger.v("load favcontacts id[" + id + "] number of contacts[" + this.favContacts.size() + "]");
        } catch (Throwable t) {
            this.favContacts = null;
            sLogger.e(t);
        } finally {
            this.bus.post(FAVORITE_CONTACTS_CHANGED);
        }
    }
}
