package com.navdy.hud.app.framework.toast;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import java.util.Iterator;
import java.util.concurrent.LinkedBlockingDeque;

public class ToastManager {
    private static final ToastManager sInstance = new ToastManager();
    private static final Logger sLogger = new Logger(ToastManager.class);
    private Bus bus;
    private ToastInfo currentToast;
    private Handler handler = new Handler(Looper.getMainLooper());
    private boolean isToastDisplayDisabled = false;
    private boolean isToastScreenDisplayed;
    private Runnable nextToast = new Runnable() {
        public void run() {
            synchronized (ToastManager.this.queue) {
                if (ToastManager.this.queue.size() == 0) {
                    ToastManager.sLogger.v("no pending toast");
                } else if (ToastManager.this.isToastScreenDisplayed) {
                    ToastManager.sLogger.v("cannot display toast screen still on");
                } else if (ToastManager.this.isToastDisplayDisabled) {
                    ToastManager.sLogger.v("toasts disabled");
                } else {
                    ToastInfo info = (ToastInfo) ToastManager.this.queue.remove();
                    ToastManager.this.currentToast = info;
                    ToastManager.this.showToast(info);
                }
            }
        }
    };
    private LinkedBlockingDeque<ToastInfo> queue = new LinkedBlockingDeque();
    private UIStateManager uiStateManager;

    public static class DismissedToast {
        public String name;

        public DismissedToast(String name) {
            this.name = name;
        }
    }

    public static class ShowToast {
        public String name;

        public ShowToast(String name) {
            this.name = name;
        }
    }

    public static class ToastInfo {
        boolean dismissed;
        ToastParams toastParams;
        boolean ttsDone;

        ToastInfo(ToastParams toastParams) {
            this.toastParams = toastParams;
        }
    }

    public static class ToastParams {
        IToastCallback cb;
        Bundle data;
        String id;
        boolean lock;
        boolean makeCurrent;
        boolean removeOnDisable;

        public ToastParams(String id, Bundle data, IToastCallback cb, boolean removeOnDisable, boolean makeCurrent) {
            this(id, data, cb, removeOnDisable, makeCurrent, false);
        }

        public ToastParams(String id, Bundle data, IToastCallback cb, boolean removeOnDisable, boolean makeCurrent, boolean lock) {
            this.id = id;
            this.data = data;
            this.cb = cb;
            this.removeOnDisable = removeOnDisable;
            this.makeCurrent = makeCurrent;
            this.lock = lock;
        }
    }

    public static ToastManager getInstance() {
        return sInstance;
    }

    public void setToastDisplayFlag(boolean b) {
        this.isToastScreenDisplayed = b;
        if (!this.isToastScreenDisplayed) {
            this.currentToast = null;
            this.handler.removeCallbacks(this.nextToast);
            this.handler.post(this.nextToast);
        }
    }

    public boolean isToastDisplayed() {
        return this.isToastScreenDisplayed;
    }

    private ToastManager() {
        RemoteDeviceManager remoteDeviceManager = RemoteDeviceManager.getInstance();
        this.bus = remoteDeviceManager.getBus();
        this.uiStateManager = remoteDeviceManager.getUiStateManager();
    }

    public void addToast(final ToastParams toastParams) {
        if (toastParams == null) {
            sLogger.w("invalid toast params");
        } else if (GenericUtil.isMainThread()) {
            addToastInternal(toastParams);
        } else {
            this.handler.post(new Runnable() {
                public void run() {
                    ToastManager.this.addToastInternal(toastParams);
                }
            });
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void addToastInternal(ToastParams toastParams) {
        if (toastParams.id == null || toastParams.data == null) {
            sLogger.w("invalid toast passed");
        } else if (this.uiStateManager.getRootScreen() == null) {
            sLogger.w("toast [" + toastParams.id + "] not accepted, Main screen not on");
        } else if (isLocked()) {
            sLogger.v("locked, cannot add toast[" + toastParams.id + "]");
        } else {
            toastParams.data.putString("id", toastParams.id);
            ToastInfo info = new ToastInfo(toastParams);
            synchronized (this.queue) {
                if (toastParams.makeCurrent) {
                    if (this.currentToast == null || this.currentToast.dismissed) {
                        sLogger.v("replace current not reqd");
                        this.queue.addFirst(info);
                        if (!this.isToastScreenDisplayed) {
                            sLogger.v("replace called next:run");
                            this.nextToast.run();
                        }
                    } else {
                        this.queue.addFirst(this.currentToast);
                        sLogger.v("replace current, item to front:" + this.currentToast.toastParams.id);
                        this.queue.addFirst(info);
                        dismissCurrentToast();
                    }
                } else if (this.queue.size() > 0 || this.isToastScreenDisplayed || this.currentToast != null) {
                    sLogger.v("[" + toastParams.id + "] wait for display to be cleared size[" + this.queue.size() + "] current[" + this.currentToast + "] displayed[" + this.isToastScreenDisplayed + "]");
                    this.queue.add(info);
                } else if (this.isToastDisplayDisabled) {
                    sLogger.v("[" + toastParams.id + "] toasts disabled");
                    this.queue.add(info);
                } else {
                    this.currentToast = info;
                    sLogger.v("push toast to display");
                    showToast(info);
                }
            }
        }
    }

    private void showToast(ToastInfo info) {
        sLogger.v("[show-toast] id[" + info.toastParams.id + "]");
        ToastPresenter.present(this.uiStateManager.getToastView(), info);
    }

    public IToastCallback getCallback(String id) {
        if (id == null || this.currentToast == null || !TextUtils.equals(id, this.currentToast.toastParams.id)) {
            return null;
        }
        return this.currentToast.toastParams.cb;
    }

    public boolean isCurrentToast(String id) {
        if (id == null || this.currentToast == null || !TextUtils.equals(this.currentToast.toastParams.id, id)) {
            return false;
        }
        return true;
    }

    public String getCurrentToastId() {
        if (this.currentToast == null) {
            return null;
        }
        return this.currentToast.toastParams.id;
    }

    public void dismissCurrentToast(String... args) {
        for (String s : args) {
            dismissCurrentToast(s);
        }
    }

    public void dismissToast(String id) {
        dismissCurrentToast(id);
        clearPendingToast(id);
    }

    public void dismissCurrentToast() {
        dismissCurrentToast(getCurrentToastId(), false);
    }

    void dismissCurrentToast(boolean disable) {
        dismissCurrentToast(getCurrentToastId(), disable);
    }

    public void dismissCurrentToast(String id) {
        dismissCurrentToast(id, false);
    }

    private void dismissCurrentToast(final String id, final boolean disable) {
        if (GenericUtil.isMainThread()) {
            dismissCurrentToastInternal(id, disable);
        } else {
            this.handler.post(new Runnable() {
                public void run() {
                    ToastManager.this.dismissCurrentToastInternal(id, disable);
                }
            });
        }
    }

    private void dismissCurrentToastInternal(String id, boolean disable) {
        if (this.currentToast != null) {
            if (id == null || TextUtils.equals(id, this.currentToast.toastParams.id)) {
                if (disable && this.currentToast.toastParams.removeOnDisable) {
                    this.currentToast = null;
                    sLogger.v("dismiss current toast[" + id + "] removed");
                } else {
                    this.currentToast.dismissed = true;
                    sLogger.v("dismiss current toast[" + id + "] saved");
                }
                ToastPresenter.dismiss();
                return;
            }
            sLogger.v("current toast[" + this.currentToast.toastParams.id + "] does not match[" + id + "]");
        }
    }

    public void clearPendingToast(String... args) {
        for (String s : args) {
            clearPendingToast(s);
        }
    }

    public void clearAllPendingToast() {
        if (GenericUtil.isMainThread()) {
            clearPendingToastInternal(null);
        } else {
            this.handler.post(new Runnable() {
                public void run() {
                    ToastManager.this.clearPendingToastInternal(null);
                }
            });
        }
    }

    public void clearPendingToast(final String id) {
        if (GenericUtil.isMainThread()) {
            clearPendingToastInternal(id);
        } else {
            this.handler.post(new Runnable() {
                public void run() {
                    ToastManager.this.clearPendingToastInternal(id);
                }
            });
        }
    }

    private void clearPendingToastInternal(String id) {
        synchronized (this.queue) {
            if (id == null) {
                sLogger.v("removed-all:" + this.queue.size());
                this.queue.clear();
                this.currentToast = null;
                this.isToastScreenDisplayed = false;
            } else {
                Iterator<ToastInfo> iterator = this.queue.iterator();
                while (iterator.hasNext()) {
                    String toastId = ((ToastInfo) iterator.next()).toastParams.id;
                    if (TextUtils.equals(id, toastId)) {
                        sLogger.v("removed:" + toastId);
                        iterator.remove();
                    }
                }
            }
        }
    }

    public void disableToasts(boolean disable) {
        sLogger.v("disableToast [" + disable + "]");
        if (this.isToastDisplayDisabled == disable) {
            sLogger.v("disableToast same state as before");
            return;
        }
        this.isToastDisplayDisabled = disable;
        if (!disable) {
            synchronized (this.queue) {
                if (this.currentToast != null) {
                    showToast(this.currentToast);
                } else {
                    this.handler.post(this.nextToast);
                }
            }
        } else if (isLocked()) {
            sLogger.v("disableToast locked, cannot disable toast");
        } else {
            dismissCurrentToast(true);
        }
    }

    private boolean isLocked() {
        if (this.currentToast != null) {
            return this.currentToast.toastParams.lock;
        }
        return false;
    }
}
