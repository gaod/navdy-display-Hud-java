package com.navdy.hud.app.framework.music;

import com.navdy.service.library.events.audio.MusicCollectionInfo;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import java.util.ArrayList;
import java.util.List;

public class MusicCollection {
    public List<MusicCollectionInfo> collections = new ArrayList();
    public MusicCollectionInfo musicCollectionInfo;
    public List<MusicTrackInfo> tracks = new ArrayList();
}
