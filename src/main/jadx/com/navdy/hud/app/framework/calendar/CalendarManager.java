package com.navdy.hud.app.framework.calendar;

import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.service.library.events.calendars.CalendarEvent;
import com.navdy.service.library.events.calendars.CalendarEventUpdates;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class CalendarManager {
    private static final int CALENDAR_EVENTS_LIST_INITIAL_SIZE = 20;
    private static final Logger sLogger = new Logger(CalendarManager.class);
    public Bus mBus;
    private List<CalendarEvent> mCalendarEvents = new ArrayList(20);
    private CalendarEventUpdates mLastCalendarEventUpdate;
    private String profileName;

    public enum CalendarManagerEvent {
        UPDATED
    }

    @Inject
    public CalendarManager(Bus bus) {
        this.mBus = bus;
        this.mBus.register(this);
    }

    @Subscribe
    public void onCalendarEventsUpdate(CalendarEventUpdates calendarEventUpdates) {
        sLogger.d("Received Calendar updates :" + calendarEventUpdates);
        this.mLastCalendarEventUpdate = calendarEventUpdates;
        DriverProfile currentProfile = DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        if (currentProfile != null) {
            this.profileName = currentProfile.getProfileName();
        }
        this.mCalendarEvents.clear();
        if (!(calendarEventUpdates == null || calendarEventUpdates.calendar_events == null)) {
            sLogger.d("Calendar events count :" + calendarEventUpdates.calendar_events.size());
            this.mCalendarEvents.addAll(calendarEventUpdates.calendar_events);
        }
        this.mBus.post(CalendarManagerEvent.UPDATED);
    }

    @Subscribe
    public void onDriverProfileChanged(DriverProfileChanged driverProfileChanged) {
        if (DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile().isDefaultProfile()) {
            sLogger.d("Default profile loaded, not clearing the calendar events");
            return;
        }
        this.mCalendarEvents.clear();
        this.mBus.post(CalendarManagerEvent.UPDATED);
    }

    public List<CalendarEvent> getCalendarEvents() {
        return this.mCalendarEvents;
    }
}
