package com.navdy.hud.app.framework.phonecall;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.makeramen.RoundedTransformationBuilder;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.framework.contacts.ContactUtil.IContactCallback;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader.PhotoDownloadStatus;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.glance.GlanceViewCache;
import com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.framework.toast.IToastCallback;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager.ToastParams;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout2.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Selection;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;
import com.navdy.hud.app.util.PhoneUtil;
import com.navdy.hud.app.view.ToastView;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.DeviceInfo.Platform;
import com.navdy.service.library.events.callcontrol.CallAction;
import com.navdy.service.library.events.callcontrol.PhoneStatus;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Transformation;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class CallNotification implements INotification, IContactCallback {
    public static final String CALL_NOTIFICATION_TOAST_ID = "incomingcall#toast";
    private static final String EMPTY = "";
    private static final int END_NOTIFICATION_TIMEOUT = 3000;
    private static final int FAILED_NOTIFICATION_TIMEOUT = 10000;
    private static final float IMAGE_SCALE = 0.5f;
    private static final int IN_CALL_NOTIFICATION_TIMEOUT = 30000;
    private static final int MISSED_NOTIFICATION_TIMEOUT = 10000;
    private static final int TAG_ACCEPT = 101;
    private static final int TAG_CANCEL = 105;
    private static final int TAG_DISMISS = 107;
    private static final int TAG_END = 106;
    private static final int TAG_IGNORE = 102;
    private static final int TAG_MUTE = 103;
    private static final int TAG_UNMUTE = 104;
    private static final Logger sLogger = new Logger(CallNotification.class);
    private final String activeCall;
    private ArrayList<Choice> activeCallChoices = new ArrayList(2);
    private ArrayList<Choice> activeCallMutedChoices = new ArrayList(2);
    private ArrayList<Choice> activeCallNoMuteChoices = new ArrayList(1);
    private Bus bus;
    private final String callAccept;
    private final String callCancel;
    private final String callDialing;
    private final String callDismiss;
    private final String callEnd;
    private final String callEnded;
    private final String callFailed;
    private final String callIgnore;
    private CallManager callManager;
    private final String callMissed;
    private final String callMute;
    private final String callMuted;
    private ImageView callStatusImage;
    private final String callUnmute;
    private InitialsImageView callerImage;
    private TextView callerName;
    private TextView callerStatus;
    private final String callerUnknown;
    private ChoiceLayout2 choiceLayout;
    private IListener choiceListener = new IListener() {
        public void executeItem(Selection selection) {
            switch (selection.id) {
                case 5:
                    if (CallNotification.this.controller != null && CallNotification.this.controller.isExpandedWithStack()) {
                        CallNotification.this.controller.collapseNotification(false, false);
                        return;
                    }
                    return;
                case 103:
                    if (CallNotification.this.callManager.state != CallNotificationState.IN_PROGRESS) {
                        CallNotification.this.dismissNotification();
                        return;
                    }
                    CallNotification.this.callManager.sendCallAction(CallAction.CALL_MUTE, null);
                    CallNotification.this.callerStatus.setText(CallNotification.this.callMuted);
                    CallNotification.this.callStatusImage.setImageResource(R.drawable.icon_call_muted);
                    CallNotification.this.choiceLayout.setChoices(CallNotification.this.activeCallMutedChoices, 0, CallNotification.this.choiceListener, 0.5f);
                    CallNotification.this.choiceLayout.setTag(CallNotification.this.activeCallMutedChoices);
                    return;
                case 104:
                    if (CallNotification.this.callManager.state != CallNotificationState.IN_PROGRESS) {
                        CallNotification.this.dismissNotification();
                        return;
                    }
                    CallNotification.this.callManager.sendCallAction(CallAction.CALL_UNMUTE, null);
                    CallNotification.this.callerStatus.setText("");
                    CallNotification.this.callStatusImage.setImageResource(R.drawable.icon_call_green);
                    CallNotification.this.choiceLayout.setChoices(CallNotification.this.activeCallChoices, 0, CallNotification.this.choiceListener, 0.5f);
                    CallNotification.this.choiceLayout.setTag(CallNotification.this.activeCallChoices);
                    return;
                case 105:
                    if (CallNotification.this.callManager.state == CallNotificationState.DIALING) {
                        CallNotification.this.callManager.sendCallAction(CallAction.CALL_END, null);
                        return;
                    } else if (CallNotification.this.callManager.state != CallNotificationState.IN_PROGRESS) {
                        CallNotification.this.dismissNotification();
                        return;
                    } else {
                        return;
                    }
                case 106:
                    if (CallNotification.this.callManager.state != CallNotificationState.IN_PROGRESS) {
                        CallNotification.this.dismissNotification();
                        return;
                    } else {
                        CallNotification.this.callManager.sendCallAction(CallAction.CALL_END, null);
                        return;
                    }
                case CallNotification.TAG_DISMISS /*107*/:
                    CallNotification.this.dismissNotification();
                    return;
                default:
                    return;
            }
        }

        public void itemSelected(Selection selection) {
        }
    };
    private ViewGroup container;
    private int contentWidth;
    private INotificationController controller;
    private ArrayList<Choice> dialingCallChoices = new ArrayList(1);
    private final int dismisColor;
    private Runnable durationRunnable = new Runnable() {
        public void run() {
            try {
                CallNotification.this.setCallDuration(CallNotification.this.extendedTitle);
            } finally {
                CallNotification.this.handler.postDelayed(CallNotification.this.durationRunnable, TimeUnit.MINUTES.toMillis(1));
            }
        }
    };
    private final int endColor;
    private ArrayList<Choice> endedCallChoices = new ArrayList(1);
    private ViewGroup extendedContainer;
    private TextView extendedTitle;
    private ArrayList<Choice> failedCallChoices = new ArrayList(1);
    private Handler handler = new Handler(Looper.getMainLooper());
    private final String incomingCall;
    private ArrayList<ChoiceLayout.Choice> incomingCallChoices = new ArrayList(2);
    private ArrayList<Choice> missedCallChoices = new ArrayList(1);
    private final int muteColor;
    private Resources resources;
    private Transformation roundTransformation = new RoundedTransformationBuilder().oval(true).build();
    private final int unmuteColor;
    private final int unselectedColor;

    CallNotification(CallManager callManager, Bus bus) {
        this.callManager = callManager;
        this.bus = bus;
        this.resources = HudApplication.getAppContext().getResources();
        this.callEnded = this.resources.getString(R.string.call_ended);
        this.callDialing = this.resources.getString(R.string.call_dialing);
        this.callMuted = this.resources.getString(R.string.call_muted);
        this.callerUnknown = this.resources.getString(R.string.caller_unknown);
        this.callAccept = this.resources.getString(R.string.call_accept);
        this.callIgnore = this.resources.getString(R.string.call_ignore);
        this.callMute = this.resources.getString(R.string.call_mute);
        this.callEnd = this.resources.getString(R.string.call_end);
        this.callUnmute = this.resources.getString(R.string.call_unmute);
        this.callCancel = this.resources.getString(R.string.call_cancel);
        this.callDismiss = this.resources.getString(R.string.call_dismiss);
        this.callFailed = this.resources.getString(R.string.call_failed);
        this.callMissed = this.resources.getString(R.string.call_missed);
        this.incomingCall = this.resources.getString(R.string.incoming_call);
        this.activeCall = this.resources.getString(R.string.phone_call_duration);
        this.muteColor = this.resources.getColor(R.color.call_mute);
        this.unmuteColor = this.resources.getColor(R.color.call_unmute);
        this.endColor = this.resources.getColor(R.color.call_end);
        this.dismisColor = this.resources.getColor(R.color.glance_dismiss);
        this.unselectedColor = -16777216;
        this.contentWidth = (int) this.resources.getDimension(R.dimen.toast_app_disconnected_width);
        this.incomingCallChoices.add(new ChoiceLayout.Choice(this.callAccept, 101));
        this.incomingCallChoices.add(new ChoiceLayout.Choice(this.callIgnore, 102));
        this.activeCallChoices.add(new Choice(103, R.drawable.icon_glances_mute, this.muteColor, R.drawable.icon_glances_mute, this.unselectedColor, this.callMute, this.muteColor));
        this.activeCallChoices.add(new Choice(106, R.drawable.icon_glances_endcall, this.endColor, R.drawable.icon_glances_endcall, this.unselectedColor, this.callEnd, this.endColor));
        this.activeCallNoMuteChoices.add(new Choice(106, R.drawable.icon_glances_endcall, this.endColor, R.drawable.icon_glances_endcall, this.unselectedColor, this.callEnd, this.endColor));
        this.activeCallMutedChoices.add(new Choice(104, R.drawable.icon_glances_unmute, this.unmuteColor, R.drawable.icon_glances_unmute, this.unselectedColor, this.callUnmute, this.unmuteColor));
        this.activeCallMutedChoices.add(new Choice(106, R.drawable.icon_glances_endcall, this.endColor, R.drawable.icon_glances_endcall, this.unselectedColor, this.callEnd, this.endColor));
        this.dialingCallChoices.add(new Choice(105, R.drawable.icon_glances_endcall, this.endColor, R.drawable.icon_glances_endcall, this.unselectedColor, this.callEnd, this.endColor));
        this.failedCallChoices.add(new Choice(TAG_DISMISS, R.drawable.icon_glances_dismiss, this.dismisColor, R.drawable.icon_glances_dismiss, this.unselectedColor, this.callDismiss, this.dismisColor));
        this.endedCallChoices.add(new Choice(TAG_DISMISS, R.drawable.icon_glances_dismiss, this.dismisColor, R.drawable.icon_glances_dismiss, this.unselectedColor, this.callDismiss, this.dismisColor));
        this.missedCallChoices.add(new Choice(TAG_DISMISS, R.drawable.icon_glances_dismiss, this.dismisColor, R.drawable.icon_glances_dismiss, this.unselectedColor, this.callDismiss, this.dismisColor));
    }

    public NotificationType getType() {
        return NotificationType.PHONE_CALL;
    }

    public String getId() {
        return NotificationId.PHONE_CALL_NOTIFICATION_ID;
    }

    public View getView(Context context) {
        if (this.container == null) {
            this.container = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.notification_phonecall, null);
            this.callerName = (TextView) this.container.findViewById(R.id.callerName);
            this.callerStatus = (TextView) this.container.findViewById(R.id.callerStatus);
            this.callerImage = (InitialsImageView) this.container.findViewById(R.id.callerImage);
            this.callStatusImage = (ImageView) this.container.findViewById(R.id.callStatusImage);
            this.choiceLayout = (ChoiceLayout2) this.container.findViewById(R.id.choiceLayout);
        }
        return this.container;
    }

    public View getExpandedView(Context context, Object data) {
        this.extendedContainer = (ViewGroup) GlanceViewCache.getView(ViewType.BIG_MULTI_TEXT, context);
        this.extendedTitle = (TextView) this.extendedContainer.findViewById(R.id.title1);
        this.extendedTitle.setTextAppearance(context, R.style.glance_title_1);
        ((TextView) this.extendedContainer.findViewById(R.id.title2)).setVisibility(8);
        setCallDuration(this.extendedTitle);
        this.extendedTitle.setVisibility(0);
        this.handler.removeCallbacks(this.durationRunnable);
        this.handler.postDelayed(this.durationRunnable, TimeUnit.MINUTES.toMillis(1));
        return this.extendedContainer;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(INotificationController controller) {
        this.controller = controller;
        this.bus.register(this);
        updateState();
        if (!controller.isExpandedWithStack()) {
            this.container.setTranslationX(0.0f);
            this.container.setTranslationY(0.0f);
            this.container.setAlpha(1.0f);
            this.callerName.setAlpha(1.0f);
            this.callerStatus.setAlpha(1.0f);
            this.choiceLayout.setAlpha(1.0f);
        }
    }

    public void onUpdate() {
        updateState();
    }

    public void onStop() {
        this.handler.removeCallbacks(this.durationRunnable);
        this.bus.unregister(this);
        this.controller = null;
        if (this.choiceLayout != null) {
            this.choiceLayout.clear();
        }
        if (this.extendedContainer != null) {
            ViewGroup parent = (ViewGroup) this.extendedContainer.getParent();
            if (parent != null) {
                parent.removeView(this.extendedContainer);
            }
            GlanceViewCache.putView(ViewType.BIG_MULTI_TEXT, this.extendedContainer);
            this.extendedContainer = null;
        }
    }

    public int getTimeout() {
        return 0;
    }

    public boolean isAlive() {
        switch (this.callManager.state) {
            case IN_PROGRESS:
            case RINGING:
            case DIALING:
                return true;
            default:
                return false;
        }
    }

    public boolean isPurgeable() {
        return !isAlive();
    }

    public boolean canAddToStackIfCurrentExists() {
        return true;
    }

    public int getColor() {
        return GlanceConstants.colorPhoneCall;
    }

    public void onNotificationEvent(Mode mode) {
        if (mode == Mode.EXPAND) {
            CallNotificationState callState = this.callManager.state;
            if (isTimeoutRequired(callState)) {
                if (this.controller != null) {
                    sLogger.v("set timeout:" + callState);
                    this.controller.stopTimeout(true);
                    this.controller.startTimeout(getTimeout(callState));
                }
            } else if (this.controller != null) {
                sLogger.v("reset timeout:" + callState);
                this.controller.stopTimeout(true);
            }
        }
    }

    public void onExpandedNotificationEvent(Mode mode) {
        if (mode == Mode.COLLAPSE && this.controller != null) {
            updateState();
        }
    }

    public void onExpandedNotificationSwitched() {
    }

    public AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        AnimatorSet set = new AnimatorSet();
        ObjectAnimator o1;
        ObjectAnimator o2;
        ObjectAnimator o3;
        if (viewIn) {
            o1 = ObjectAnimator.ofFloat(this.callerName, View.ALPHA, new float[]{0.0f, 1.0f});
            o2 = ObjectAnimator.ofFloat(this.callerStatus, View.ALPHA, new float[]{0.0f, 1.0f});
            o3 = ObjectAnimator.ofFloat(this.choiceLayout, View.ALPHA, new float[]{0.0f, 1.0f});
            set.playTogether(new Animator[]{o1, o2, o3});
        } else {
            o1 = ObjectAnimator.ofFloat(this.callerName, View.ALPHA, new float[]{1.0f, 0.0f});
            o2 = ObjectAnimator.ofFloat(this.callerStatus, View.ALPHA, new float[]{1.0f, 0.0f});
            o3 = ObjectAnimator.ofFloat(this.choiceLayout, View.ALPHA, new float[]{1.0f, 0.0f});
            set.playTogether(new Animator[]{o1, o2, o3});
        }
        return set;
    }

    public boolean expandNotification() {
        if (this.controller == null) {
            return false;
        }
        ArrayList tag = this.choiceLayout.getTag();
        if (tag != this.activeCallChoices && tag != this.activeCallMutedChoices && tag != this.activeCallNoMuteChoices) {
            return false;
        }
        switchToExpandedMode();
        return true;
    }

    public boolean supportScroll() {
        return false;
    }

    private void switchToExpandedMode() {
        if (this.controller != null) {
            this.controller.stopTimeout(true);
            this.choiceLayout.setChoices(GlanceConstants.backChoice2, 0, this.choiceListener, 0.5f);
            this.choiceLayout.setTag(GlanceConstants.backChoice2);
            if (!this.controller.isExpandedWithStack()) {
                this.controller.expandNotification(true);
            }
        }
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        boolean handled = false;
        boolean ret = false;
        switch (event) {
            case LEFT:
                handled = this.choiceLayout.moveSelectionLeft();
                ret = true;
                break;
            case RIGHT:
                handled = this.choiceLayout.moveSelectionRight();
                ret = true;
                break;
            case SELECT:
                this.choiceLayout.executeSelectedItem();
                ret = true;
                break;
        }
        if (this.controller != null && handled && isTimeoutRequired(this.callManager.state) && getTimeout(this.callManager.state) > 0) {
            this.controller.resetTimeout();
        }
        return ret;
    }

    public IInputHandler nextHandler() {
        return null;
    }

    void updateState() {
        CallNotificationState callState = this.callManager.state;
        sLogger.v("call state:" + callState);
        if (!checkIfCollapseRequired()) {
            switch (callState) {
                case IDLE:
                    if (NotificationManager.getInstance().isCurrentNotificationId(NotificationId.PHONE_CALL_NOTIFICATION_ID)) {
                        dismissNotification();
                        break;
                    }
                    break;
                case MISSED:
                    setCaller();
                    this.callerStatus.setText(this.callMissed);
                    this.choiceLayout.setVisibility(0);
                    this.choiceLayout.setChoices(this.missedCallChoices, 0, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(this.missedCallChoices);
                    this.callStatusImage.setImageResource(R.drawable.icon_badge_callend);
                    this.callStatusImage.setVisibility(0);
                    ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, false, this.callerImage, this.roundTransformation, this);
                    break;
                case ENDED:
                    setCaller();
                    this.callerStatus.setText(this.callEnded);
                    this.choiceLayout.setVisibility(0);
                    this.choiceLayout.setChoices(this.endedCallChoices, 0, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(this.endedCallChoices);
                    this.callStatusImage.setImageResource(R.drawable.icon_badge_callend);
                    this.callStatusImage.setVisibility(0);
                    ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, false, this.callerImage, this.roundTransformation, this);
                    break;
                case FAILED:
                    setCaller();
                    this.callerStatus.setText(this.callFailed);
                    this.choiceLayout.setVisibility(0);
                    this.choiceLayout.setChoices(this.failedCallChoices, 0, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(this.failedCallChoices);
                    this.callStatusImage.setImageResource(R.drawable.icon_call_red);
                    this.callStatusImage.setVisibility(0);
                    ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, false, this.callerImage, this.roundTransformation, this);
                    break;
                case REJECTED:
                    dismissNotification();
                    break;
                case CANCELLED:
                    dismissNotification();
                    break;
                case IN_PROGRESS:
                    if (setCaller()) {
                        this.callerStatus.setText("");
                    } else {
                        String number = getCallerNumber();
                        if (ContactUtil.isValidNumber(number)) {
                            this.callerStatus.setText(PhoneUtil.formatPhoneNumber(number));
                        }
                    }
                    this.callStatusImage.setImageResource(R.drawable.icon_badge_call);
                    this.callStatusImage.setVisibility(0);
                    ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, false, this.callerImage, this.roundTransformation, this);
                    if (this.controller.isExpandedWithStack()) {
                        this.choiceLayout.setChoices(GlanceConstants.backChoice2, 0, this.choiceListener, 0.5f);
                        this.choiceLayout.setTag(GlanceConstants.backChoice2);
                    } else {
                        DeviceInfo deviceInfo = RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
                        if (deviceInfo == null || deviceInfo.platform == Platform.PLATFORM_Android) {
                            this.choiceLayout.setChoices(this.activeCallNoMuteChoices, 0, this.choiceListener, 0.5f);
                            this.choiceLayout.setTag(this.activeCallNoMuteChoices);
                        } else if (this.callManager.callMuted) {
                            this.choiceLayout.setChoices(this.activeCallMutedChoices, 0, this.choiceListener, 0.5f);
                            this.choiceLayout.setTag(this.activeCallMutedChoices);
                        } else {
                            this.choiceLayout.setChoices(this.activeCallChoices, 0, this.choiceListener, 0.5f);
                            this.choiceLayout.setTag(this.activeCallChoices);
                        }
                    }
                    this.choiceLayout.setVisibility(0);
                    break;
                case DIALING:
                    setCaller();
                    this.callerStatus.setText(this.callDialing);
                    this.callStatusImage.setVisibility(4);
                    ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, true, this.callerImage, this.roundTransformation, this);
                    this.choiceLayout.setChoices(this.dialingCallChoices, 0, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(this.dialingCallChoices);
                    this.choiceLayout.setVisibility(0);
                    break;
            }
            if (isTimeoutRequired(callState)) {
                if (this.controller != null) {
                    sLogger.v("set timeout:" + callState);
                    this.controller.stopTimeout(true);
                    this.controller.startTimeout(getTimeout(callState));
                }
            } else if (this.controller != null) {
                sLogger.v("reset timeout:" + callState);
                this.controller.stopTimeout(true);
            }
        }
    }

    @Subscribe
    public void onPhotoDownload(PhotoDownloadStatus status) {
        if (this.controller != null && !status.alreadyDownloaded && status.photoType == PhotoType.PHOTO_CONTACT && this.callManager.phoneStatus != PhoneStatus.PHONE_IDLE && TextUtils.equals(this.callManager.number, status.sourceIdentifier)) {
            ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, false, this.callerImage, this.roundTransformation, this);
        }
    }

    private boolean setCaller() {
        String text = getCaller();
        if (ContactUtil.isValidNumber(text)) {
            this.callerName.setText(PhoneUtil.formatPhoneNumber(text));
            return true;
        }
        this.callerName.setText(text);
        return false;
    }

    public String getCaller() {
        if (!TextUtils.isEmpty(this.callManager.contact)) {
            return this.callManager.contact;
        }
        if (TextUtils.isEmpty(this.callManager.number)) {
            return this.callerUnknown;
        }
        return this.callManager.number;
    }

    private String getCallerNumber() {
        return this.callManager.number;
    }

    private void dismissNotification() {
        NotificationManager.getInstance().removeNotification(NotificationId.PHONE_CALL_NOTIFICATION_ID);
        this.callManager.state = CallNotificationState.IDLE;
    }

    private boolean isTimeoutRequired(CallNotificationState state) {
        if (state == null) {
            return false;
        }
        switch (state) {
            case MISSED:
            case ENDED:
            case FAILED:
            case DIALING:
                return true;
            case IN_PROGRESS:
                if (this.controller == null || !this.controller.isExpandedWithStack()) {
                    return true;
                }
                return false;
            default:
                return false;
        }
    }

    private int getTimeout(CallNotificationState state) {
        if (state == null) {
            return 0;
        }
        switch (state) {
            case MISSED:
                return 10000;
            case ENDED:
                return 3000;
            case FAILED:
                return 10000;
            case IN_PROGRESS:
            case DIALING:
                return 30000;
            default:
                return 0;
        }
    }

    public boolean isContextValid() {
        return this.controller != null;
    }

    public void showIncomingCallToast() {
        final String caller = getCaller();
        String n = getCallerNumber();
        if (TextUtils.isEmpty(n)) {
            n = null;
        }
        final String number = n;
        Bundle bundle = new Bundle();
        bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE, this.incomingCall);
        bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_STYLE, R.style.ToastMainTitle);
        bundle.putInt(ToastPresenter.EXTRA_SIDE_IMAGE, R.drawable.icon_call_green);
        bundle.putParcelableArrayList(ToastPresenter.EXTRA_CHOICE_LIST, this.incomingCallChoices);
        bundle.putBoolean(ToastPresenter.EXTRA_SUPPORTS_GESTURE, true);
        bundle.putInt(ToastPresenter.EXTRA_INFO_CONTAINER_MAX_WIDTH, this.contentWidth);
        if (number == null || TextUtils.equals(caller, number)) {
            if (ContactUtil.isValidNumber(caller)) {
                bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, PhoneUtil.formatPhoneNumber(number));
            } else {
                bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, caller);
            }
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
        } else {
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, caller);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_3, PhoneUtil.formatPhoneNumber(number));
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_3_STYLE, R.style.Toast_1);
        }
        IToastCallback callback = new IToastCallback() {
            IContactCallback cb = new IContactCallback() {
                public boolean isContextValid() {
                    if (AnonymousClass3.this.toastView != null) {
                        return true;
                    }
                    return false;
                }
            };
            InitialsImageView toastImage;
            ToastView toastView;

            public void onStart(ToastView view) {
                this.toastView = view;
                this.toastImage = view.getMainLayout().screenImage;
                this.toastImage.setVisibility(0);
                CallNotification.this.bus.register(this);
                ContactUtil.setContactPhoto(caller, number, true, this.toastImage, CallNotification.this.roundTransformation, this.cb);
            }

            public void onStop() {
                CallNotification.this.bus.unregister(this);
                this.toastView = null;
                this.toastImage = null;
            }

            public boolean onKey(CustomKeyEvent event) {
                return false;
            }

            public void executeChoiceItem(int pos, int id) {
                switch (id) {
                    case 101:
                        if (CallNotification.this.callManager.state == CallNotificationState.RINGING) {
                            CallNotification.this.callManager.sendCallAction(CallAction.CALL_ACCEPT, null);
                        }
                        ToastManager.getInstance().dismissCurrentToast(CallNotification.CALL_NOTIFICATION_TOAST_ID);
                        return;
                    case 102:
                        if (CallNotification.this.callManager.state == CallNotificationState.RINGING) {
                            CallNotification.this.callManager.sendCallAction(CallAction.CALL_REJECT, null);
                        }
                        ToastManager.getInstance().dismissCurrentToast(CallNotification.CALL_NOTIFICATION_TOAST_ID);
                        return;
                    default:
                        return;
                }
            }

            @Subscribe
            public void onPhotoDownload(PhotoDownloadStatus status) {
                if (this.toastView != null && this.toastImage != null && !status.alreadyDownloaded && status.photoType == PhotoType.PHOTO_CONTACT && CallNotification.this.callManager.phoneStatus != PhoneStatus.PHONE_IDLE && TextUtils.equals(number, status.sourceIdentifier)) {
                    ContactUtil.setContactPhoto(caller, number, false, this.toastImage, CallNotification.this.roundTransformation, this.cb);
                }
            }
        };
        ToastManager toastManager = ToastManager.getInstance();
        clearToast();
        toastManager.addToast(new ToastParams(CALL_NOTIFICATION_TOAST_ID, bundle, callback, false, true, true));
    }

    public static void clearToast() {
        ToastManager toastManager = ToastManager.getInstance();
        toastManager.dismissCurrentToast(CALL_NOTIFICATION_TOAST_ID);
        toastManager.clearAllPendingToast();
        toastManager.disableToasts(false);
    }

    private void setCallDuration(TextView textView) {
        long start = this.callManager.callStartMs;
        if (start <= 0) {
            textView.setText(this.activeCall);
            return;
        }
        if (TimeUnit.MILLISECONDS.toMinutes(SystemClock.elapsedRealtime() - start) < 1) {
            textView.setText(this.activeCall);
            return;
        }
        textView.setText(this.resources.getString(R.string.phone_call_duration_min, new Object[]{Long.valueOf(minute)}));
    }

    private boolean checkIfCollapseRequired() {
        if (isAlive() || this.controller == null || !this.controller.isExpandedWithStack()) {
            return false;
        }
        sLogger.v("collapse required");
        this.controller.collapseNotification(false, false);
        return true;
    }
}
