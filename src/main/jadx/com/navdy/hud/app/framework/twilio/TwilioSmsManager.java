package com.navdy.hud.app.framework.twilio;

import android.content.Context;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.util.PhoneUtil;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.CredentialUtil;
import com.navdy.service.library.util.SystemUtils;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Credentials;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request.Builder;
import okhttp3.Response;

public class TwilioSmsManager {
    private static final String ACCOUNT_SID;
    private static final String AUTH_TOKEN;
    private static final int CONNECTION_TIMEOUT = ((int) TimeUnit.SECONDS.toMillis(15));
    private static final String CREDENTIALS = Credentials.basic(ACCOUNT_SID, AUTH_TOKEN);
    private static final String MESSAGE_SERVICE_ID;
    private static final int READ_TIMEOUT = ((int) TimeUnit.SECONDS.toMillis(30));
    private static final String SMS_ENDPOINT = ("https://api.twilio.com/2010-04-01/Accounts/" + ACCOUNT_SID + "/Messages.json");
    private static final int WRITE_TIMEOUT = ((int) TimeUnit.SECONDS.toMillis(30));
    private static TwilioSmsManager instance = new TwilioSmsManager();
    private static final Logger sLogger = new Logger(TwilioSmsManager.class);
    private final DriverProfileManager driverProfileManager = DriverProfileHelper.getInstance().getDriverProfileManager();
    private final OkHttpClient okHttpClient = RemoteDeviceManager.getInstance().getHttpManager().getClient().newBuilder().readTimeout((long) READ_TIMEOUT, TimeUnit.MILLISECONDS).writeTimeout((long) WRITE_TIMEOUT, TimeUnit.MILLISECONDS).connectTimeout((long) CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS).build();

    public interface Callback {
        void result(ErrorCode errorCode);
    }

    public enum ErrorCode {
        SUCCESS,
        INTERNAL_ERROR,
        NETWORK_ERROR,
        TWILIO_SERVER_ERROR,
        INVALID_PARAMETER
    }

    static {
        Context context = HudApplication.getAppContext();
        ACCOUNT_SID = CredentialUtil.getCredentials(context, "TWILIO_ACCOUNT_SID");
        AUTH_TOKEN = CredentialUtil.getCredentials(context, "TWILIO_AUTH_TOKEN");
        MESSAGE_SERVICE_ID = CredentialUtil.getCredentials(context, "TWILIO_MSG_SERVICE_ID");
    }

    public static TwilioSmsManager getInstance() {
        return instance;
    }

    private TwilioSmsManager() {
    }

    public ErrorCode sendSms(String number, String message, Callback cb) {
        if (number == null || message == null) {
            try {
                return ErrorCode.INVALID_PARAMETER;
            } catch (Throwable t) {
                sLogger.e("sendSms", t);
                return ErrorCode.INTERNAL_ERROR;
            }
        } else if (!SystemUtils.isConnectedToNetwork(HudApplication.getAppContext())) {
            return ErrorCode.NETWORK_ERROR;
        } else {
            final String profileId = this.driverProfileManager.getCurrentProfile().getProfileName();
            final String str = message;
            final String str2 = number;
            final Callback callback = cb;
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    try {
                        String formattedMessage;
                        TwilioSmsManager.sLogger.v("sendSms-start");
                        if (TextUtils.isEmpty(DriverProfileHelper.getInstance().getCurrentProfile().getFirstName())) {
                            formattedMessage = str;
                        } else {
                            formattedMessage = HudApplication.getAppContext().getString(R.string.ios_sms_format, new Object[]{driverName, str});
                        }
                        Response response = TwilioSmsManager.this.okHttpClient.newCall(new Builder().url(TwilioSmsManager.SMS_ENDPOINT).header("Authorization", TwilioSmsManager.CREDENTIALS).post(new FormBody.Builder().add("To", PhoneUtil.convertToE164Format(str2)).add("MessagingServiceSid", TwilioSmsManager.MESSAGE_SERVICE_ID).add("Body", formattedMessage).build()).build()).execute();
                        if (response.isSuccessful()) {
                            TwilioSmsManager.sLogger.v("sendSms-end-suc:" + response.code());
                            TwilioSmsManager.this.sendResult(ErrorCode.SUCCESS, profileId, callback);
                            return;
                        }
                        TwilioSmsManager.sLogger.e("sendSms-end-err:" + response.code() + HereManeuverDisplayBuilder.COMMA + response.message());
                        TwilioSmsManager.this.sendResult(ErrorCode.TWILIO_SERVER_ERROR, profileId, callback);
                    } catch (Throwable t) {
                        TwilioSmsManager.sLogger.e("sendSms-end-err", t);
                        if (t instanceof IOException) {
                            TwilioSmsManager.this.sendResult(ErrorCode.NETWORK_ERROR, profileId, callback);
                        } else {
                            TwilioSmsManager.this.sendResult(ErrorCode.INTERNAL_ERROR, profileId, callback);
                        }
                    }
                }
            }, 23);
            return ErrorCode.SUCCESS;
        }
    }

    private void sendResult(ErrorCode errorCode, String sendProfileId, Callback cb) {
        if (cb != null) {
            DriverProfile profile = this.driverProfileManager.getCurrentProfile();
            String currentProfileName = profile.getProfileName();
            if (profile.isDefaultProfile() || TextUtils.equals(sendProfileId, currentProfileName)) {
                switch (errorCode) {
                    case SUCCESS:
                        AnalyticsSupport.recordSmsSent(true, AnalyticsSupport.ATTR_PLATFORM_IOS, GlanceConstants.areMessageCanned());
                        break;
                    default:
                        AnalyticsSupport.recordSmsSent(false, AnalyticsSupport.ATTR_PLATFORM_IOS, GlanceConstants.areMessageCanned());
                        break;
                }
                cb.result(errorCode);
                return;
            }
            sLogger.v("profile changed:" + sendProfileId + HereManeuverDisplayBuilder.COMMA + currentProfileName);
        }
    }
}
