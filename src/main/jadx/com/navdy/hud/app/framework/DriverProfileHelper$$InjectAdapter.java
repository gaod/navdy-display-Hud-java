package com.navdy.hud.app.framework;

import com.navdy.hud.app.profile.DriverProfileManager;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class DriverProfileHelper$$InjectAdapter extends Binding<DriverProfileHelper> implements MembersInjector<DriverProfileHelper> {
    private Binding<DriverProfileManager> driverProfileManager;

    public DriverProfileHelper$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.framework.DriverProfileHelper", false, DriverProfileHelper.class);
    }

    public void attach(Linker linker) {
        this.driverProfileManager = linker.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", DriverProfileHelper.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.driverProfileManager);
    }

    public void injectMembers(DriverProfileHelper object) {
        object.driverProfileManager = (DriverProfileManager) this.driverProfileManager.get();
    }
}
