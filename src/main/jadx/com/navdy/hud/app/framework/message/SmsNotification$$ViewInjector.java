package com.navdy.hud.app.framework.message;

import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.ui.component.image.InitialsImageView;

public class SmsNotification$$ViewInjector {
    public static void inject(Finder finder, SmsNotification target, Object source) {
        target.title = (TextView) finder.findRequiredView(source, R.id.title, "field 'title'");
        target.subtitle = (TextView) finder.findRequiredView(source, R.id.subtitle, "field 'subtitle'");
        target.choiceLayout = (ChoiceLayout2) finder.findRequiredView(source, R.id.choice_layout, "field 'choiceLayout'");
        target.notificationUserImage = (InitialsImageView) finder.findRequiredView(source, R.id.notification_user_image, "field 'notificationUserImage'");
        target.sideImage = (ImageView) finder.findRequiredView(source, R.id.badge, "field 'sideImage'");
    }

    public static void reset(SmsNotification target) {
        target.title = null;
        target.subtitle = null;
        target.choiceLayout = null;
        target.notificationUserImage = null;
        target.sideImage = null;
    }
}
