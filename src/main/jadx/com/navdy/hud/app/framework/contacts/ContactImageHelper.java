package com.navdy.hud.app.framework.contacts;

import android.content.res.Resources;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.util.PhoneUtil;
import java.util.HashMap;
import java.util.Map;

public class ContactImageHelper {
    public static final int DEFAULT_IMAGE_INDEX = -1;
    public static int NO_CONTACT_COLOR = 0;
    public static final int NO_CONTACT_IMAGE = R.drawable.icon_user_numberonly;
    private static final Map<Integer, Integer> sContactColorMap = new HashMap();
    private static final Map<Integer, Integer> sContactImageMap = new HashMap();
    private static final ContactImageHelper sInstance = new ContactImageHelper();

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        NO_CONTACT_COLOR = resources.getColor(R.color.grey_4a);
        sContactColorMap.put(Integer.valueOf(0), Integer.valueOf(resources.getColor(R.color.icon_user_bg_0)));
        int counter = 0 + 1;
        sContactImageMap.put(Integer.valueOf(0), Integer.valueOf(R.drawable.icon_user_bg_0));
        sContactColorMap.put(Integer.valueOf(counter), Integer.valueOf(resources.getColor(R.color.icon_user_bg_1)));
        int counter2 = counter + 1;
        sContactImageMap.put(Integer.valueOf(counter), Integer.valueOf(R.drawable.icon_user_bg_1));
        sContactColorMap.put(Integer.valueOf(counter2), Integer.valueOf(resources.getColor(R.color.icon_user_bg_2)));
        counter = counter2 + 1;
        sContactImageMap.put(Integer.valueOf(counter2), Integer.valueOf(R.drawable.icon_user_bg_2));
        sContactColorMap.put(Integer.valueOf(counter), Integer.valueOf(resources.getColor(R.color.icon_user_bg_3)));
        counter2 = counter + 1;
        sContactImageMap.put(Integer.valueOf(counter), Integer.valueOf(R.drawable.icon_user_bg_3));
        sContactColorMap.put(Integer.valueOf(counter2), Integer.valueOf(resources.getColor(R.color.icon_user_bg_4)));
        counter = counter2 + 1;
        sContactImageMap.put(Integer.valueOf(counter2), Integer.valueOf(R.drawable.icon_user_bg_4));
        sContactColorMap.put(Integer.valueOf(counter), Integer.valueOf(resources.getColor(R.color.icon_user_bg_5)));
        counter2 = counter + 1;
        sContactImageMap.put(Integer.valueOf(counter), Integer.valueOf(R.drawable.icon_user_bg_5));
        sContactColorMap.put(Integer.valueOf(counter2), Integer.valueOf(resources.getColor(R.color.icon_user_bg_6)));
        counter = counter2 + 1;
        sContactImageMap.put(Integer.valueOf(counter2), Integer.valueOf(R.drawable.icon_user_bg_6));
        sContactColorMap.put(Integer.valueOf(counter), Integer.valueOf(resources.getColor(R.color.icon_user_bg_7)));
        counter2 = counter + 1;
        sContactImageMap.put(Integer.valueOf(counter), Integer.valueOf(R.drawable.icon_user_bg_7));
        sContactColorMap.put(Integer.valueOf(counter2), Integer.valueOf(resources.getColor(R.color.icon_user_bg_8)));
        counter = counter2 + 1;
        sContactImageMap.put(Integer.valueOf(counter2), Integer.valueOf(R.drawable.icon_user_bg_8));
    }

    public static ContactImageHelper getInstance() {
        return sInstance;
    }

    public int getContactImageIndex(String contactId) {
        if (contactId == null) {
            return -1;
        }
        contactId = GenericUtil.normalizeToFilename(contactId);
        if (ContactUtil.isValidNumber(contactId)) {
            contactId = PhoneUtil.convertToE164Format(contactId);
        }
        return Math.abs(contactId.hashCode() % sContactImageMap.size());
    }

    public int getResourceId(int index) {
        if (index < 0 || index >= sContactImageMap.size()) {
            return R.drawable.icon_user_numberonly;
        }
        return ((Integer) sContactImageMap.get(Integer.valueOf(index))).intValue();
    }

    public int getResourceColor(int index) {
        if (index < 0 || index >= sContactColorMap.size()) {
            return NO_CONTACT_COLOR;
        }
        return ((Integer) sContactColorMap.get(Integer.valueOf(index))).intValue();
    }

    public int getDriverImageResId(String deviceName) {
        return getResourceId(Math.abs(GenericUtil.normalizeToFilename(deviceName).hashCode() % sContactImageMap.size()));
    }
}
