package com.navdy.hud.app.framework.destinations;

import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.phonecall.CallNotification;
import com.navdy.hud.app.framework.toast.IToastCallback;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager.ToastParams;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.maps.util.RouteUtils;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.hud.app.view.ToastView;
import com.navdy.service.library.events.destination.Destination;
import com.navdy.service.library.events.destination.Destination.SuggestionType;
import com.navdy.service.library.events.places.SuggestedDestination;
import com.navdy.service.library.log.Logger;
import com.squareup.wire.Wire;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class DestinationSuggestionToast {
    public static final String DESTINATION_SUGGESTION_TOAST_ID = "connection#toast";
    private static final int TAG_ACCEPT = 1;
    private static final int TAG_IGNORE = 2;
    private static final int TOAST_TIMEOUT = 15000;
    private static final String accept;
    private static final String ignore;
    public static final Logger sLogger = new Logger(DestinationSuggestionToast.class);
    private static final ArrayList<Choice> suggestionChoices = new ArrayList(2);
    private static final String title;

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        title = resources.getString(R.string.suggested_trip);
        accept = resources.getString(R.string.accept);
        ignore = resources.getString(R.string.ignore);
        suggestionChoices.add(new Choice(accept, 1));
        suggestionChoices.add(new Choice(ignore, 2));
    }

    public static void showSuggestion(SuggestedDestination suggestion) {
        final Destination destination = suggestion.destination;
        int iconRes = R.drawable.icon_places_favorite;
        Bundle bundle = new Bundle();
        bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 15000);
        if (suggestion.type != null) {
            switch (suggestion.type) {
                case SUGGESTION_CALENDAR:
                    iconRes = R.drawable.icon_place_calendar;
                    break;
            }
        }
        if (iconRes == R.drawable.icon_places_favorite) {
            switch (destination.favorite_type) {
                case FAVORITE_HOME:
                    iconRes = R.drawable.icon_places_home;
                    break;
                case FAVORITE_WORK:
                    iconRes = R.drawable.icon_places_work;
                    break;
                case FAVORITE_CUSTOM:
                    iconRes = R.drawable.icon_places_favorite;
                    break;
                case FAVORITE_NONE:
                    iconRes = R.drawable.icon_mm_suggested_places;
                    break;
            }
        }
        bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, iconRes);
        bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_STYLE, R.style.ToastMainTitle);
        bundle.putBoolean(ToastPresenter.EXTRA_SUPPORTS_GESTURE, true);
        bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE, title);
        bundle.putParcelableArrayList(ToastPresenter.EXTRA_CHOICE_LIST, suggestionChoices);
        String title2 = !TextUtils.isEmpty(destination.destination_title) ? destination.destination_title : !TextUtils.isEmpty(destination.destination_subtitle) ? destination.destination_subtitle : destination.full_address;
        int durationWithTraffic = ((Integer) Wire.get(suggestion.duration_traffic, Integer.valueOf(-1))).intValue();
        String title3 = "";
        Resources resources = HudApplication.getAppContext().getResources();
        if (resources != null) {
            title3 = durationWithTraffic > 0 ? resources.getString(R.string.suggested_eta, new Object[]{RouteUtils.formatEtaMinutes(resources, (int) TimeUnit.SECONDS.toMinutes((long) durationWithTraffic))}) : !TextUtils.isEmpty(destination.destination_subtitle) ? destination.destination_subtitle : destination.full_address;
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, title2);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Toast_2);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_3, title3);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_3_STYLE, R.style.Toast_1);
            bundle.putInt(ToastPresenter.EXTRA_INFO_CONTAINER_MAX_WIDTH, resources.getDimensionPixelSize(R.dimen.expand_notif_width));
            IToastCallback callback = new IToastCallback() {
                ToastView toastView;

                public void onStart(ToastView view) {
                    this.toastView = view;
                    ConfirmationLayout layout = view.getConfirmation();
                    layout.screenTitle.setTextAppearance(view.getContext(), R.style.mainTitle);
                    layout.title3.setTextColor(view.getResources().getColor(17170443));
                }

                public void onStop() {
                    this.toastView = null;
                }

                public boolean onKey(CustomKeyEvent event) {
                    return false;
                }

                public void executeChoiceItem(int pos, int id) {
                    if (this.toastView != null) {
                        boolean accepted = false;
                        switch (id) {
                            case 1:
                                accepted = true;
                                this.toastView.dismissToast();
                                DestinationsManager.getInstance().goToSuggestedDestination();
                                break;
                            case 2:
                                accepted = false;
                                this.toastView.dismissToast();
                                break;
                        }
                        try {
                            AnalyticsSupport.recordDestinationSuggestion(accepted, destination.suggestion_type == SuggestionType.SUGGESTION_CALENDAR);
                        } catch (Throwable t) {
                            DestinationSuggestionToast.sLogger.e("Error while recording the destination suggestion", t);
                        }
                    }
                }
            };
            boolean makeCurrent = true;
            ToastManager toastManager = ToastManager.getInstance();
            if (TextUtils.equals(toastManager.getCurrentToastId(), CallNotification.CALL_NOTIFICATION_TOAST_ID)) {
                sLogger.v("phone toast active");
                makeCurrent = false;
            } else {
                toastManager.dismissCurrentToast(toastManager.getCurrentToastId());
                toastManager.clearAllPendingToast();
            }
            toastManager.addToast(new ToastParams("connection#toast", bundle, callback, true, makeCurrent));
        }
    }

    public static void dismissSuggestionToast() {
        ToastManager toastManager = ToastManager.getInstance();
        toastManager.clearPendingToast("connection#toast");
        toastManager.dismissCurrentToast("connection#toast");
    }
}
