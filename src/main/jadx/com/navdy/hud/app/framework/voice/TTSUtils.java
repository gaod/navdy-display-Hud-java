package com.navdy.hud.app.framework.voice;

import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.device.gps.GpsUtils;
import com.navdy.hud.app.event.LocalSpeechRequest;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager.ToastParams;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.service.library.events.audio.SpeechRequest.Builder;
import com.navdy.service.library.events.audio.SpeechRequest.Category;
import com.squareup.otto.Bus;
import java.io.File;
import java.util.Locale;

public class TTSUtils {
    public static final String DEBUG_GPS_CALIBRATION_IMU_DONE = "Ublox IMU Calibration Done";
    public static final String DEBUG_GPS_CALIBRATION_SENSOR_DONE = "Ublox Sensor Calibration Done";
    public static final String DEBUG_GPS_CALIBRATION_SENSOR_NOT_NEEDED = "Ublox Sensor Calibration Not Needed";
    public static final String DEBUG_GPS_CALIBRATION_STARTED = "Ublox Calibration started";
    private static final String DEBUG_GPS_SWITCH_TOAST_ID = "debug-tts-gps-switch";
    public static final String DEBUG_TTS_FASTER_ROUTE_AVAILABLE = "Faster route is available";
    public static final String DEBUG_TTS_TRAFFIC_NOT_USED_IN_ROUTE_CALC = "Traffic Not used for Route Calculation";
    public static final String DEBUG_TTS_UBLOX_DR_ENDED = "Dead Reckoning has Stopped";
    public static final String DEBUG_TTS_UBLOX_DR_STARTED = "Dead Reckoning has Started";
    public static final String DEBUG_TTS_UBLOX_HAS_LOCATION_FIX = "Ublox Has Location Fix";
    public static final String DEBUG_TTS_USING_GPS_SPEED = "Using GPS RawSpeed";
    public static final String DEBUG_TTS_USING_OBD_SPEED = "Using OBD RawSpeed";
    public static final String TTS_AUTOMATIC_ZOOM;
    public static final String TTS_DIAL_BATTERY_EXTREMELY_LOW;
    public static final String TTS_DIAL_BATTERY_LOW;
    public static final String TTS_DIAL_BATTERY_VERY_LOW;
    public static final String TTS_DIAL_CONNECTED;
    public static final String TTS_DIAL_DISCONNECTED;
    public static final String TTS_DIAL_FORGOTTEN;
    public static final String TTS_FUEL_GAUGE_ADDED;
    public static final String TTS_FUEL_RANGE_GAUGE_ADDED;
    public static final String TTS_GLANCES_DISABLED;
    public static final String TTS_HEADING_GAUGE_ADDED;
    public static final String TTS_MANUAL_ZOOM;
    public static final String TTS_NAV_STOPPED;
    public static final String TTS_PHONE_BATTERY_LOW;
    public static final String TTS_PHONE_BATTERY_VERY_LOW;
    public static final String TTS_PHONE_OFFLINE;
    public static final String TTS_RPM_GAUGE_ADDED;
    public static final String TTS_TBT_DISABLED;
    public static final String TTS_TBT_ENABLED;
    public static final String TTS_TRAFFIC_DISABLED;
    public static final String TTS_TRAFFIC_ENABLED;
    private static final Bus bus = RemoteDeviceManager.getInstance().getBus();
    private static final boolean isDebugTTSEnabled = new File("/sdcard/debug_tts").exists();

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        TTS_GLANCES_DISABLED = resources.getString(R.string.glances_disabled);
        TTS_DIAL_DISCONNECTED = resources.getString(R.string.tts_dial_disconnected);
        TTS_DIAL_CONNECTED = resources.getString(R.string.tts_dial_connected);
        TTS_DIAL_FORGOTTEN = resources.getString(R.string.tts_dial_forgotten);
        TTS_TRAFFIC_DISABLED = resources.getString(R.string.tts_traffic_disabled);
        TTS_TRAFFIC_ENABLED = resources.getString(R.string.tts_traffic_enabled);
        TTS_NAV_STOPPED = resources.getString(R.string.tts_nav_stopped);
        TTS_TBT_DISABLED = resources.getString(R.string.tts_tbt_disabled);
        TTS_TBT_ENABLED = resources.getString(R.string.tts_tbt_enabled);
        TTS_FUEL_RANGE_GAUGE_ADDED = resources.getString(R.string.tts_fuel_range_gauge_added);
        TTS_FUEL_GAUGE_ADDED = resources.getString(R.string.tts_fuel_gauge_added);
        TTS_RPM_GAUGE_ADDED = resources.getString(R.string.tts_rpm_gauge_added);
        TTS_HEADING_GAUGE_ADDED = resources.getString(R.string.tts_heading_gauge_added);
        TTS_DIAL_BATTERY_VERY_LOW = resources.getString(R.string.tts_dial_battery_very_low);
        TTS_DIAL_BATTERY_LOW = resources.getString(R.string.tts_dial_battery_low);
        TTS_DIAL_BATTERY_EXTREMELY_LOW = resources.getString(R.string.tts_dial_battery_extremely_low);
        TTS_PHONE_BATTERY_VERY_LOW = resources.getString(R.string.tts_phone_battery_very_low);
        TTS_PHONE_BATTERY_LOW = resources.getString(R.string.tts_phone_battery_low);
        TTS_PHONE_OFFLINE = resources.getString(R.string.phone_no_network);
        TTS_MANUAL_ZOOM = resources.getString(R.string.tts_manual_zoom);
        TTS_AUTOMATIC_ZOOM = resources.getString(R.string.tts_auto_zoon);
    }

    public static void sendSpeechRequest(String str, String id) {
        sendSpeechRequest(str, Category.SPEECH_NOTIFICATION, id);
    }

    public static void sendSpeechRequest(String str, Category category, String id) {
        if (!TextUtils.isEmpty(str)) {
            bus.post(new LocalSpeechRequest(new Builder().words(str).category(category).id(id).sendStatus(Boolean.valueOf(false)).language(Locale.getDefault().toLanguageTag()).build()));
        }
    }

    public static boolean isDebugTTSEnabled() {
        return isDebugTTSEnabled;
    }

    public static void debugShowNoTrafficToast() {
        if (isDebugTTSEnabled) {
            Bundle bundle = new Bundle();
            bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_open_nav_disable_traffic);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, DEBUG_TTS_TRAFFIC_NOT_USED_IN_ROUTE_CALC);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
            bundle.putString(ToastPresenter.EXTRA_TTS, DEBUG_TTS_TRAFFIC_NOT_USED_IN_ROUTE_CALC);
            ToastManager.getInstance().addToast(new ToastParams("debug-tts-no-traffic", bundle, null, true, false));
        }
    }

    public static void debugShowGotUbloxFix() {
        if (isDebugTTSEnabled && DeviceUtil.isNavdyDevice()) {
            Bundle bundle = new Bundle();
            bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_gpserror);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, DEBUG_TTS_UBLOX_HAS_LOCATION_FIX);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
            bundle.putString(ToastPresenter.EXTRA_TTS, DEBUG_TTS_UBLOX_HAS_LOCATION_FIX);
            ToastManager.getInstance().addToast(new ToastParams("debug-tts-ublox-fix", bundle, null, true, false));
        }
    }

    public static void debugShowGpsSwitch(String title, String info) {
        if (isDebugTTSEnabled && DeviceUtil.isNavdyDevice()) {
            Bundle bundle = new Bundle();
            bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_gpserror);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, title);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_3, info);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_3_STYLE, R.style.Glances_2);
            bundle.putInt(ToastPresenter.EXTRA_INFO_CONTAINER_MAX_WIDTH, (int) HudApplication.getAppContext().getResources().getDimension(R.dimen.no_glances_width));
            bundle.putString(ToastPresenter.EXTRA_TTS, title);
            ToastManager toastManager = ToastManager.getInstance();
            toastManager.dismissCurrentToast(DEBUG_GPS_SWITCH_TOAST_ID);
            toastManager.addToast(new ToastParams(DEBUG_GPS_SWITCH_TOAST_ID, bundle, null, true, false));
        }
    }

    public static void debugShowDRStarted(String drType) {
        if (isDebugTTSEnabled && DeviceUtil.isNavdyDevice()) {
            Bundle bundle = new Bundle();
            bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_gpserror);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_1, drType);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_1_STYLE, R.style.Glances_1);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, DEBUG_TTS_UBLOX_DR_STARTED);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
            bundle.putString(ToastPresenter.EXTRA_TTS, DEBUG_TTS_UBLOX_DR_STARTED);
            ToastManager.getInstance().addToast(new ToastParams("debug-tts-ublox-dr-start", bundle, null, true, false));
        }
    }

    public static void debugShowDREnded() {
        if (isDebugTTSEnabled && DeviceUtil.isNavdyDevice()) {
            Bundle bundle = new Bundle();
            bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_gpserror);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, DEBUG_TTS_UBLOX_DR_ENDED);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
            bundle.putString(ToastPresenter.EXTRA_TTS, DEBUG_TTS_UBLOX_DR_ENDED);
            ToastManager.getInstance().addToast(new ToastParams("debug-tts-ublox-dr-end", bundle, null, true, false));
        }
    }

    public static void debugShowSpeedInput(String str) {
        if (isDebugTTSEnabled && DeviceUtil.isNavdyDevice()) {
            Bundle bundle = new Bundle();
            bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_mm_dash);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, str);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
            bundle.putString(ToastPresenter.EXTRA_TTS, str);
            ToastManager.getInstance().addToast(new ToastParams("debug-tts-speed-input", bundle, null, true, false));
        }
    }

    public static void debugShowFasterRouteToast(String title, String info) {
        if (isDebugTTSEnabled) {
            Bundle bundle = new Bundle();
            bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 7000);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_open_nav_disable_traffic);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, title);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_3, info);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_3_STYLE, R.style.Glances_2);
            bundle.putString(ToastPresenter.EXTRA_TTS, DEBUG_TTS_FASTER_ROUTE_AVAILABLE);
            bundle.putBoolean(ToastPresenter.EXTRA_DEFAULT_CHOICE, true);
            ToastManager.getInstance().addToast(new ToastParams("debug-tts-faster-route", bundle, null, true, false));
        }
    }

    public static void debugShowGpsReset(String title) {
        if (isDebugTTSEnabled && DeviceUtil.isNavdyDevice()) {
            Bundle bundle = new Bundle();
            bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_gpserror);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, title);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
            bundle.putInt(ToastPresenter.EXTRA_INFO_CONTAINER_MAX_WIDTH, (int) HudApplication.getAppContext().getResources().getDimension(R.dimen.no_glances_width));
            bundle.putString(ToastPresenter.EXTRA_TTS, title);
            ToastManager.getInstance().addToast(new ToastParams("debug-tts-gps-reset", bundle, null, true, false));
        }
    }

    public static void debugShowGpsCalibrationStarted() {
        if (GpsUtils.isDebugRawGpsPosEnabled() && DeviceUtil.isNavdyDevice()) {
            Bundle bundle = new Bundle();
            bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_gpserror);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_1, DEBUG_GPS_CALIBRATION_STARTED);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_1_STYLE, R.style.Glances_1);
            ToastManager.getInstance().addToast(new ToastParams("debug-gps-calibration_start", bundle, null, true, false));
        }
    }

    public static void debugShowGpsImuCalibrationDone() {
        if (GpsUtils.isDebugRawGpsPosEnabled() && DeviceUtil.isNavdyDevice()) {
            Bundle bundle = new Bundle();
            bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_gpserror);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_1, DEBUG_GPS_CALIBRATION_IMU_DONE);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_1_STYLE, R.style.Glances_1);
            ToastManager.getInstance().addToast(new ToastParams("debug-gps-calibration_imu", bundle, null, true, false));
        }
    }

    public static void debugShowGpsSensorCalibrationDone() {
        if (GpsUtils.isDebugRawGpsPosEnabled() && DeviceUtil.isNavdyDevice()) {
            Bundle bundle = new Bundle();
            bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_gpserror);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_1, DEBUG_GPS_CALIBRATION_SENSOR_DONE);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_1_STYLE, R.style.Glances_1);
            ToastManager.getInstance().addToast(new ToastParams("debug-gps-calibration_sensor", bundle, null, true, false));
        }
    }

    public static void debugShowGpsSensorCalibrationNotNeeded() {
        if (GpsUtils.isDebugRawGpsPosEnabled() && DeviceUtil.isNavdyDevice()) {
            Bundle bundle = new Bundle();
            bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 3000);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_gpserror);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_1, DEBUG_GPS_CALIBRATION_SENSOR_NOT_NEEDED);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_1_STYLE, R.style.Glances_1);
            ToastManager.getInstance().addToast(new ToastParams("debug-gps-calibration_not_needed", bundle, null, true, false));
        }
    }
}
