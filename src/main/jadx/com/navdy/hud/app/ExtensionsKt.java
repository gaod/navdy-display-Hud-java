package com.navdy.hud.app;

import com.navdy.service.library.events.MessageStore;
import com.navdy.service.library.events.audio.MusicCollectionRequest;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0003\u001a\u00020\u0004*\u00020\u0004\u001a\u001a\u0010\u0005\u001a\u00020\u0004*\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0004\u001a\u001a\u0010\u0005\u001a\u00020\b*\u00020\b2\u0006\u0010\u0006\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\b\u00a8\u0006\t"}, d2 = {"cacheKey", "", "Lcom/navdy/service/library/events/audio/MusicCollectionRequest;", "celsiusToFahrenheit", "", "clamp", "min", "max", "", "app_hudRelease"}, k = 2, mv = {1, 1, 6})
/* compiled from: Extensions.kt */
public final class ExtensionsKt {
    public static final float clamp(float $receiver, float min, float max) {
        if ($receiver < min) {
            return min;
        }
        if ($receiver > max) {
            return max;
        }
        return $receiver;
    }

    public static final double clamp(double $receiver, double min, double max) {
        if ($receiver < min) {
            return min;
        }
        if ($receiver > max) {
            return max;
        }
        return $receiver;
    }

    public static final double celsiusToFahrenheit(double $receiver) {
        return ((((double) 9) * $receiver) / ((double) 5)) + ((double) 32);
    }

    @NotNull
    public static final String cacheKey(@NotNull MusicCollectionRequest $receiver) {
        Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
        MusicCollectionRequest defaults = (MusicCollectionRequest) MessageStore.removeNulls($receiver);
        return defaults.collectionSource + "#" + defaults.collectionType + "#" + defaults.collectionId + "#" + defaults.offset;
    }
}
