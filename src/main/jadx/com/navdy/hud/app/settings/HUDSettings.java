package com.navdy.hud.app.settings;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.events.settings.Setting;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HUDSettings {
    private static final String ADAPTIVE_AUTOBRIGHTNESS_PROP = "persist.sys.autobright_adaptive";
    public static final String AUTO_BRIGHTNESS = "screen.auto_brightness";
    public static final String AUTO_BRIGHTNESS_ADJUSTMENT = "screen.auto_brightness_adj";
    public static final String BRIGHTNESS = "screen.brightness";
    public static final float BRIGHTNESS_SCALE = 255.0f;
    public static final String GESTURE_ENGINE = "gesture.engine";
    public static final String GESTURE_PREVIEW = "gesture.preview";
    public static final String LED_BRIGHTNESS = "screen.led_brightness";
    public static final String MAP_ANIMATION_MODE = "map.animation.mode";
    public static final String MAP_SCHEME = "map.scheme";
    public static final String MAP_TILT = "map.tilt";
    public static final String MAP_ZOOM = "map.zoom";
    public static final boolean USE_ADAPTIVE_AUTOBRIGHTNESS = SystemProperties.getBoolean(ADAPTIVE_AUTOBRIGHTNESS_PROP, true);
    private static Logger sLogger = new Logger(HUDSettings.class);
    private SharedPreferences preferences;

    public HUDSettings(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    public List<String> availableSettings() {
        List<String> result = new ArrayList();
        result.addAll(this.preferences.getAll().keySet());
        return result;
    }

    public void updateSettings(List<Setting> newSettings) {
        Editor editor = this.preferences.edit();
        for (int i = 0; i < newSettings.size(); i++) {
            Setting newSetting = (Setting) newSettings.get(i);
            if (newSetting.value.equals("-1")) {
                String str = newSetting.key;
                Object obj = -1;
                switch (str.hashCode()) {
                    case 133816335:
                        if (str.equals(MAP_TILT)) {
                            obj = null;
                            break;
                        }
                        break;
                    case 134000933:
                        if (str.equals(MAP_ZOOM)) {
                            obj = 1;
                            break;
                        }
                        break;
                }
                switch (obj) {
                    case null:
                        set(editor, newSetting.key, String.valueOf(60.0f));
                        break;
                    case 1:
                        set(editor, newSetting.key, String.valueOf(16.5f));
                        break;
                    default:
                        break;
                }
            }
            set(editor, newSetting.key, newSetting.value);
        }
        editor.apply();
    }

    public List<Setting> readSettings(List<String> keys) {
        List<Setting> result = new ArrayList();
        Map<String, ?> allPreferences = this.preferences.getAll();
        for (int i = 0; i < keys.size(); i++) {
            String key = (String) keys.get(i);
            Object object = allPreferences.get(key);
            if (object != null) {
                result.add(new Setting(key, object.toString()));
            }
        }
        return result;
    }

    private Object get(String key) {
        return this.preferences.getAll().get(key);
    }

    private void set(Editor editor, String key, String value) {
        Object oldValue = get(key);
        if (oldValue instanceof String) {
            editor.putString(key, value);
        } else if (oldValue instanceof Boolean) {
            editor.putBoolean(key, Boolean.valueOf(value).booleanValue());
        }
    }
}
