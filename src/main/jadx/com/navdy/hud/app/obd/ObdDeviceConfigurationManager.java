package com.navdy.hud.app.obd;

import android.os.RemoteException;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.obd.ICarService;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.MusicDataUtils;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;

public class ObdDeviceConfigurationManager {
    public static final String ASSETS_FOLDER_PREFIX = "stn_obd";
    public static final String CAR_CONFIGURATION_MAPPING_FILE = "configuration_mapping.csv";
    private static Configuration[] CAR_DETAILS_CONFIGURATION_MAPPING = null;
    public static final String CONFIGURATION_FILE_EXTENSION = "conf";
    public static final String CONFIGURATION_NAME_PREFIX = "Navdy OBD-II ";
    public static final String DEBUG_CONFIGURATION = "debug";
    public static final String DEFAULT_OFF_CONFIGURATION = "no_trigger";
    public static final String DEFAULT_ON_CONFIGURATION = "default_on";
    public static final String STSATI = "STSATI";
    private static Configuration[] VIN_CONFIGURATION_MAPPING = null;
    public static final String VIN_CONFIGURATION_MAPPING_FILE = "vin_configuration_mapping.csv";
    private static final Logger sLogger = new Logger(ObdDeviceConfigurationManager.class);
    private CarServiceConnector mCarServiceConnector;
    private volatile boolean mConnected;
    private String mExpectedConfigurationFileName = null;
    private volatile boolean mIsAutoOnEnabled = true;
    private volatile boolean mIsVinRecognized;
    private volatile String mLastKnowVinNumber;

    static class Configuration {
        String configurationName;
        Pattern pattern;

        Configuration() {
        }
    }

    static class OBDConfiguration {
        String configurationData;
        String configurationIdentifier;

        public OBDConfiguration(String configurationData, String configurationIdentifier) {
            this.configurationData = configurationData;
            this.configurationIdentifier = configurationIdentifier;
        }
    }

    public ObdDeviceConfigurationManager(CarServiceConnector mCarServiceConnector) {
        this.mCarServiceConnector = mCarServiceConnector;
    }

    public void setConnectionState(boolean connected) {
        if (connected != this.mConnected) {
            this.mConnected = connected;
            sLogger.d("OBD Connection state changed");
            if (this.mConnected) {
                try {
                    this.mLastKnowVinNumber = this.mCarServiceConnector.getCarApi().getVIN();
                    sLogger.d("Vin number read :" + this.mLastKnowVinNumber);
                    decodeVin();
                    return;
                } catch (RemoteException e) {
                    sLogger.e("Remote exception while getting the VIN number ");
                    return;
                }
            }
            return;
        }
        this.mLastKnowVinNumber = "";
        this.mIsAutoOnEnabled = true;
        this.mIsVinRecognized = false;
    }

    private void syncConfiguration() {
        if (this.mConnected && !TextUtils.isEmpty(this.mExpectedConfigurationFileName)) {
            ICarService api = this.mCarServiceConnector.getCarApi();
            OBDConfiguration configuration = readConfiguration(this.mExpectedConfigurationFileName);
            try {
                String configurationName = api.getCurrentConfigurationName();
                String expectedConfigurationName = configuration.configurationIdentifier;
                if (expectedConfigurationName.equals(configurationName)) {
                    sLogger.d("The configuration on the obd chip is as expected");
                    return;
                }
                sLogger.d("The configuration on the obd chip is different");
                sLogger.d("Expected : " + expectedConfigurationName);
                sLogger.d("Actual : " + configurationName);
                sLogger.d("Syncing configuration...");
                applyConfiguration(configuration);
            } catch (RemoteException re) {
                sLogger.e("Error while applying configuration", re);
            }
        }
    }

    private void applyConfiguration(OBDConfiguration configuration) {
        if (configuration != null && this.mConnected) {
            ICarService api = this.mCarServiceConnector.getCarApi();
            if (!TextUtils.isEmpty(configuration.configurationData) && !TextUtils.isEmpty(configuration.configurationIdentifier)) {
                sLogger.d("Writing new configuration :" + configuration);
                try {
                    boolean applied = api.applyConfiguration(configuration.configurationData);
                    sLogger.d("New Configuration applied : " + applied + ", configuration : " + api.getCurrentConfigurationName());
                } catch (RemoteException re) {
                    sLogger.e("Failed to apply the configuration :", re.getCause());
                }
            }
        }
    }

    private OBDConfiguration readConfiguration(String name) {
        try {
            return extractOBDConfiguration(readObdAssetFile(name + GlanceConstants.PERIOD + CONFIGURATION_FILE_EXTENSION));
        } catch (IOException e) {
            sLogger.e("Error reading the configuration file :" + name);
            return null;
        }
    }

    static OBDConfiguration extractOBDConfiguration(String fileContent) {
        StringBuilder machineFriendlyConfiguration = new StringBuilder();
        String configurationIdentifier = null;
        for (String line : fileContent.split(GlanceConstants.NEWLINE)) {
            if (line.startsWith("ST") || line.startsWith("AT")) {
                machineFriendlyConfiguration.append(line).append(GlanceConstants.NEWLINE);
                if (line.startsWith(STSATI)) {
                    configurationIdentifier = line.substring(STSATI.length()).trim();
                }
            }
        }
        return new OBDConfiguration(machineFriendlyConfiguration.toString().trim(), configurationIdentifier);
    }

    private String readObdAssetFile(String assetFileName) throws IOException {
        InputStream is = HudApplication.getAppContext().getAssets().open("stn_obd/" + assetFileName);
        String content = IOUtils.convertInputStreamToString(is, "UTF-8");
        try {
            is.close();
        } catch (IOException e) {
            sLogger.d("Error closing the stream after reading the asset file");
        }
        return content;
    }

    private void bSetConfigurationFile(String configurationName) {
        if (validateConfigurationName(configurationName)) {
            this.mExpectedConfigurationFileName = configurationName;
            syncConfiguration();
            return;
        }
        sLogger.d("Received an invalid configuration name : " + configurationName);
        throw new IllegalArgumentException("Configuration does not exists");
    }

    public void setConfigurationFile(final String configurationFileName) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                ObdDeviceConfigurationManager.this.bSetConfigurationFile(configurationFileName);
            }
        }, 13);
    }

    private boolean validateConfigurationName(String configurationName) {
        if (TextUtils.isEmpty(configurationName)) {
            return false;
        }
        try {
            InputStream is = HudApplication.getAppContext().getAssets().open("stn_obd/" + configurationName + GlanceConstants.PERIOD + CONFIGURATION_FILE_EXTENSION);
            if (is == null) {
                return false;
            }
            try {
                is.close();
            } catch (IOException e) {
            }
            return true;
        } catch (IOException e2) {
            return false;
        }
    }

    public void setCarDetails(final String make, final String model, final String year) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (!ObdDeviceConfigurationManager.this.mIsAutoOnEnabled) {
                    ObdDeviceConfigurationManager.sLogger.d("setCarDetails: Auto on is de selected by the user , so skipping the overwriting of configuration");
                } else if (ObdDeviceConfigurationManager.this.mIsVinRecognized) {
                    ObdDeviceConfigurationManager.sLogger.d("setCarDetails: Vin is recognized and it is used as the preferred source");
                } else {
                    String name = (TextUtils.isEmpty(make) ? "*" : make) + MusicDataUtils.ALTERNATE_SEPARATOR + (TextUtils.isEmpty(model) ? "*" : model) + MusicDataUtils.ALTERNATE_SEPARATOR + (TextUtils.isEmpty(year) ? "*" : year);
                    ObdDeviceConfigurationManager.sLogger.d("Vehicle name :" + name);
                    if (ObdDeviceConfigurationManager.CAR_DETAILS_CONFIGURATION_MAPPING == null) {
                        ObdDeviceConfigurationManager.CAR_DETAILS_CONFIGURATION_MAPPING = ObdDeviceConfigurationManager.this.loadConfigurationMappingList(ObdDeviceConfigurationManager.CAR_CONFIGURATION_MAPPING_FILE);
                    }
                    Configuration matchingConfiguration = ObdDeviceConfigurationManager.pickConfiguration(ObdDeviceConfigurationManager.CAR_DETAILS_CONFIGURATION_MAPPING, name);
                    if (matchingConfiguration != null) {
                        ObdDeviceConfigurationManager.this.bSetConfigurationFile(matchingConfiguration.configurationName);
                    }
                }
            }
        }, 13);
    }

    private Configuration[] loadConfigurationMappingList(String fileName) {
        int i = 0;
        Configuration[] configurationsList = null;
        try {
            String[] configurationMappingEntries = readObdAssetFile(fileName).split(GlanceConstants.NEWLINE);
            configurationsList = new Configuration[configurationMappingEntries.length];
            int length = configurationMappingEntries.length;
            int i2 = 0;
            while (i < length) {
                String[] parts = configurationMappingEntries[i].split(HereManeuverDisplayBuilder.COMMA);
                Configuration configuration = new Configuration();
                configuration.pattern = Pattern.compile(parts[0], 2);
                configuration.configurationName = parts[1];
                int i3 = i2 + 1;
                configurationsList[i2] = configuration;
                i++;
                i2 = i3;
            }
        } catch (IOException e) {
            sLogger.d("Failed to load the configuration mapping file");
        }
        return configurationsList;
    }

    public static Configuration pickConfiguration(Configuration[] configurationMapping, String expression) {
        for (Configuration configuration : configurationMapping) {
            if (configuration.pattern.matcher(expression).matches()) {
                return configuration;
            }
        }
        return null;
    }

    public void setDecodedCarDetails(CarDetails decodedCarDetails) {
    }

    private void decodeVin() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (ObdDeviceConfigurationManager.isValidVin(ObdDeviceConfigurationManager.this.mLastKnowVinNumber) && ObdDeviceConfigurationManager.this.mIsAutoOnEnabled) {
                    if (ObdDeviceConfigurationManager.VIN_CONFIGURATION_MAPPING == null) {
                        ObdDeviceConfigurationManager.VIN_CONFIGURATION_MAPPING = ObdDeviceConfigurationManager.this.loadConfigurationMappingList(ObdDeviceConfigurationManager.VIN_CONFIGURATION_MAPPING_FILE);
                    }
                    Configuration matchingConfiguration = ObdDeviceConfigurationManager.pickConfiguration(ObdDeviceConfigurationManager.VIN_CONFIGURATION_MAPPING, ObdDeviceConfigurationManager.this.mLastKnowVinNumber);
                    if (matchingConfiguration != null) {
                        ObdDeviceConfigurationManager.sLogger.d("decodeVin : got the matching configuration :" + matchingConfiguration.configurationName + ", Vin :" + ObdDeviceConfigurationManager.this.mLastKnowVinNumber);
                        ObdDeviceConfigurationManager.this.mIsVinRecognized = true;
                        ObdDeviceConfigurationManager.this.bSetConfigurationFile(matchingConfiguration.configurationName);
                        return;
                    }
                    ObdDeviceConfigurationManager.this.mIsVinRecognized = false;
                }
            }
        }, 13);
    }

    public void setAutoOnEnabled(boolean enabled) {
        this.mIsAutoOnEnabled = enabled;
        if (!this.mIsAutoOnEnabled) {
            setConfigurationFile(DEFAULT_OFF_CONFIGURATION);
        }
    }

    public boolean isAutoOnEnabled() {
        return this.mIsAutoOnEnabled;
    }

    public static boolean isValidVin(String vinNumber) {
        return !TextUtils.isEmpty(vinNumber) && vinNumber.length() == 17;
    }

    public static void turnOffOBDSleep() {
        ObdManager.getInstance().getObdDeviceConfigurationManager().setConfigurationFile(DEBUG_CONFIGURATION);
    }
}
