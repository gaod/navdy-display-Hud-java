package com.navdy.hud.app.obd;

import com.navdy.service.library.network.http.IHttpManager;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class CarMDVinDecoder$$InjectAdapter extends Binding<CarMDVinDecoder> implements MembersInjector<CarMDVinDecoder> {
    private Binding<IHttpManager> mHttpManager;

    public CarMDVinDecoder$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.obd.CarMDVinDecoder", false, CarMDVinDecoder.class);
    }

    public void attach(Linker linker) {
        this.mHttpManager = linker.requestBinding("com.navdy.service.library.network.http.IHttpManager", CarMDVinDecoder.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mHttpManager);
    }

    public void injectMembers(CarMDVinDecoder object) {
        object.mHttpManager = (IHttpManager) this.mHttpManager.get();
    }
}
