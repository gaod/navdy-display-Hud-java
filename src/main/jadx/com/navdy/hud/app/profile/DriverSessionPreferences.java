package com.navdy.hud.app.profile;

import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.maps.NavSessionPreferences;
import com.navdy.service.library.events.preferences.LocalPreferences;
import com.navdy.service.library.events.settings.DateTimeConfiguration;
import com.navdy.service.library.events.settings.DateTimeConfiguration.Clock;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;

public class DriverSessionPreferences {
    private static final Logger sLogger = new Logger(DriverSessionPreferences.class);
    private Bus bus;
    private DateTimeConfiguration clockConfiguration;
    private DateTimeConfiguration defaultClockConfiguration = new DateTimeConfiguration(Long.valueOf(0), null, Clock.CLOCK_12_HOUR);
    private NavSessionPreferences navSessionPreferences;
    private TimeHelper timeHelper;

    public DriverSessionPreferences(Bus bus, DriverProfile profile, TimeHelper timeHelper) {
        this.bus = bus;
        this.timeHelper = timeHelper;
        timeHelper.setFormat(Clock.CLOCK_12_HOUR);
        this.navSessionPreferences = new NavSessionPreferences(profile.getNavigationPreferences());
        setClockConfiguration(this.defaultClockConfiguration);
    }

    public NavSessionPreferences getNavigationSessionPreference() {
        return this.navSessionPreferences;
    }

    public void setDefault(DriverProfile profile, boolean isDefault) {
        LocalPreferences localPreferences = profile.getLocalPreferences();
        if (localPreferences != null && localPreferences.clockFormat != null) {
            setClockConfiguration(localPreferences.clockFormat);
        } else if (isDefault) {
            setClockConfiguration(this.defaultClockConfiguration);
        }
        this.navSessionPreferences.setDefault(profile.getNavigationPreferences());
    }

    public void setClockConfiguration(DateTimeConfiguration clockConfiguration) {
        this.clockConfiguration = clockConfiguration;
        if (clockConfiguration.format != null) {
            setClockConfiguration(clockConfiguration.format);
        }
    }

    public void setClockConfiguration(Clock format) {
        if (format != null) {
            switch (format) {
                case CLOCK_12_HOUR:
                    this.timeHelper.setFormat(Clock.CLOCK_12_HOUR);
                    break;
                case CLOCK_24_HOUR:
                    this.timeHelper.setFormat(Clock.CLOCK_24_HOUR);
                    break;
            }
            sLogger.i("setClockConfiguration:" + format);
            this.bus.post(TimeHelper.CLOCK_CHANGED);
        }
    }
}
