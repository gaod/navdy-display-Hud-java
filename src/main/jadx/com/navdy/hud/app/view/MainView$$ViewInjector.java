package com.navdy.hud.app.view;

import android.widget.FrameLayout;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.SystemTrayView;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator;
import com.navdy.hud.app.ui.component.carousel.ProgressIndicator;
import com.navdy.hud.app.ui.component.image.ColorImageView;
import com.navdy.hud.app.ui.component.main.MainLowerView;

public class MainView$$ViewInjector {
    public static void inject(Finder finder, MainView target, Object source) {
        target.containerView = (ContainerView) finder.findRequiredView(source, R.id.container, "field 'containerView'");
        target.notificationView = (NotificationView) finder.findRequiredView(source, R.id.notification, "field 'notificationView'");
        target.notifIndicator = (CarouselIndicator) finder.findRequiredView(source, R.id.notifIndicator, "field 'notifIndicator'");
        target.notifScrollIndicator = (ProgressIndicator) finder.findRequiredView(source, R.id.notifScrollIndicator, "field 'notifScrollIndicator'");
        target.expandedNotificationView = (FrameLayout) finder.findRequiredView(source, R.id.expandedNotifView, "field 'expandedNotificationView'");
        target.notificationExtensionView = (FrameLayout) finder.findRequiredView(source, R.id.notificationExtensionView, "field 'notificationExtensionView'");
        target.mainLowerView = (MainLowerView) finder.findRequiredView(source, R.id.mainLowerView, "field 'mainLowerView'");
        target.expandedNotificationCoverView = finder.findRequiredView(source, R.id.expandedNotifCoverView, "field 'expandedNotificationCoverView'");
        target.splitterView = (FrameLayout) finder.findRequiredView(source, R.id.splitter, "field 'splitterView'");
        target.notificationColorView = (ColorImageView) finder.findRequiredView(source, R.id.notificationColorView, "field 'notificationColorView'");
        target.screenContainer = (FrameLayout) finder.findRequiredView(source, R.id.screenContainer, "field 'screenContainer'");
        target.systemTray = (SystemTrayView) finder.findRequiredView(source, R.id.systemTray, "field 'systemTray'");
        target.toastView = (ToastView) finder.findRequiredView(source, R.id.toastView, "field 'toastView'");
    }

    public static void reset(MainView target) {
        target.containerView = null;
        target.notificationView = null;
        target.notifIndicator = null;
        target.notifScrollIndicator = null;
        target.expandedNotificationView = null;
        target.notificationExtensionView = null;
        target.mainLowerView = null;
        target.expandedNotificationCoverView = null;
        target.splitterView = null;
        target.notificationColorView = null;
        target.screenContainer = null;
        target.systemTray = null;
        target.toastView = null;
    }
}
