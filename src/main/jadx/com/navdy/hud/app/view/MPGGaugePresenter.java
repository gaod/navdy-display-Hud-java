package com.navdy.hud.app.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager;
import com.navdy.hud.app.util.ConversionUtil;
import java.util.concurrent.TimeUnit;

public class MPGGaugePresenter extends DashboardWidgetPresenter {
    private static final int GAUGE_UPDATE_INTERVAL = 1000;
    private static final long MPG_AVERAGE_TIME_WINDOW = TimeUnit.MINUTES.toMillis(5);
    private Context mContext;
    @InjectView(R.id.txt_unit)
    TextView mFuelConsumptionUnit;
    @InjectView(R.id.txt_value)
    TextView mFuelConsumptionValue;
    private Handler mHandler = new Handler();
    private String mKmplLabel;
    private double mMpg;
    private String mMpgLabel;
    private Runnable mRunnable = new Runnable() {
        public void run() {
            MPGGaugePresenter.this.reDraw();
            MPGGaugePresenter.this.mHandler.postDelayed(MPGGaugePresenter.this.mRunnable, 1000);
        }
    };

    public MPGGaugePresenter(Context context) {
        this.mContext = context;
        Resources res = context.getResources();
        this.mMpgLabel = res.getString(R.string.mpg);
        this.mKmplLabel = res.getString(R.string.kmpl);
    }

    public void setView(DashboardWidgetView dashboardWidgetView, Bundle arguments) {
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView((int) R.layout.fuel_consumption_gauge);
            ButterKnife.inject( this, (View) dashboardWidgetView);
            this.mHandler.removeCallbacks(this.mRunnable);
            this.mHandler.post(this.mRunnable);
        } else {
            this.mHandler.removeCallbacks(this.mRunnable);
        }
        super.setView(dashboardWidgetView, arguments);
    }

    public void setCurrentMPG(double mpgValue) {
        this.mMpg = mpgValue;
        reDraw();
    }

    public Drawable getDrawable() {
        return null;
    }

    protected void updateGauge() {
        if (this.mWidgetView != null) {
            long runningAverageValueRounded = 0;
            switch (SpeedManager.getInstance().getSpeedUnit()) {
                case KILOMETERS_PER_HOUR:
                    runningAverageValueRounded = Math.round(ConversionUtil.convertLpHundredKmToKMPL(this.mMpg));
                    this.mFuelConsumptionUnit.setText(this.mKmplLabel);
                    break;
                case MILES_PER_HOUR:
                    runningAverageValueRounded = Math.round(ConversionUtil.convertLpHundredKmToMPG(this.mMpg));
                    this.mFuelConsumptionUnit.setText(this.mMpgLabel);
                    break;
            }
            if (runningAverageValueRounded > 0) {
                this.mFuelConsumptionValue.setText(Long.toString(runningAverageValueRounded));
            } else {
                this.mFuelConsumptionValue.setText("- -");
            }
        }
    }

    public String getWidgetIdentifier() {
        return SmartDashWidgetManager.MPG_AVG_WIDGET_ID;
    }

    public String getWidgetName() {
        switch (SpeedManager.getInstance().getSpeedUnit()) {
            case KILOMETERS_PER_HOUR:
                return this.mKmplLabel;
            case MILES_PER_HOUR:
                return this.mMpgLabel;
            default:
                return null;
        }
    }
}
