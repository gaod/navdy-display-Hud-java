package com.navdy.hud.app.view;

import com.navdy.hud.app.screen.FirstLaunchScreen.Presenter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class FirstLaunchView$$InjectAdapter extends Binding<FirstLaunchView> implements MembersInjector<FirstLaunchView> {
    private Binding<Presenter> presenter;

    public FirstLaunchView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.FirstLaunchView", false, FirstLaunchView.class);
    }

    public void attach(Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.screen.FirstLaunchScreen$Presenter", FirstLaunchView.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
    }

    public void injectMembers(FirstLaunchView object) {
        object.presenter = (Presenter) this.presenter.get();
    }
}
