package com.navdy.hud.app.view;

import com.navdy.hud.app.screen.DialUpdateConfirmationScreen.Presenter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class DialUpdateConfirmationView$$InjectAdapter extends Binding<DialUpdateConfirmationView> implements MembersInjector<DialUpdateConfirmationView> {
    private Binding<Presenter> mPresenter;

    public DialUpdateConfirmationView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.DialUpdateConfirmationView", false, DialUpdateConfirmationView.class);
    }

    public void attach(Linker linker) {
        this.mPresenter = linker.requestBinding("com.navdy.hud.app.screen.DialUpdateConfirmationScreen$Presenter", DialUpdateConfirmationView.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mPresenter);
    }

    public void injectMembers(DialUpdateConfirmationView object) {
        object.mPresenter = (Presenter) this.mPresenter.get();
    }
}
