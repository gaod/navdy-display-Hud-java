package com.navdy.hud.app.view;

import android.widget.ProgressBar;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class DialUpdateProgressView$$ViewInjector {
    public static void inject(Finder finder, DialUpdateProgressView target, Object source) {
        target.mProgress = (ProgressBar) finder.findRequiredView(source, R.id.progress, "field 'mProgress'");
    }

    public static void reset(DialUpdateProgressView target) {
        target.mProgress = null;
    }
}
