package com.navdy.hud.app.view;

import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class MPGGaugePresenter$$ViewInjector {
    public static void inject(Finder finder, MPGGaugePresenter target, Object source) {
        target.mFuelConsumptionUnit = (TextView) finder.findRequiredView(source, R.id.txt_unit, "field 'mFuelConsumptionUnit'");
        target.mFuelConsumptionValue = (TextView) finder.findRequiredView(source, R.id.txt_value, "field 'mFuelConsumptionValue'");
    }

    public static void reset(MPGGaugePresenter target) {
        target.mFuelConsumptionUnit = null;
        target.mFuelConsumptionValue = null;
    }
}
