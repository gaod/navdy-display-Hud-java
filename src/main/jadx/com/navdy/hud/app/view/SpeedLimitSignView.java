package com.navdy.hud.app.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnit;

public class SpeedLimitSignView extends FrameLayout {
    @InjectView(R.id.speed_limit_sign_eu)
    protected ViewGroup euSpeedLimitSignView;
    private int speedLimit;
    private TextView speedLimitTextView;
    private SpeedUnit speedLimitUnit;
    @InjectView(R.id.speed_limit_sign_us)
    protected ViewGroup usSpeedLimitSignView;

    public SpeedLimitSignView(Context context) {
        this(context, null);
    }

    public SpeedLimitSignView(Context context, AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public SpeedLimitSignView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, -1);
    }

    public SpeedLimitSignView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.speedLimitUnit = null;
        inflate(getContext(), R.layout.speed_sign_eu, this);
        inflate(getContext(), R.layout.speed_limit_sign_us, this);
        ButterKnife.inject( this, (View) this);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        setSpeedLimitUnit(this.speedLimitUnit);
    }

    public void setSpeedLimitUnit(SpeedUnit speedLimitUnit) {
        if (this.speedLimitUnit != speedLimitUnit) {
            this.speedLimitUnit = speedLimitUnit;
            ViewGroup speedLimitView = null;
            switch (this.speedLimitUnit) {
                case MILES_PER_HOUR:
                    speedLimitView = this.usSpeedLimitSignView;
                    this.euSpeedLimitSignView.setVisibility(8);
                    break;
                case KILOMETERS_PER_HOUR:
                    speedLimitView = this.euSpeedLimitSignView;
                    this.usSpeedLimitSignView.setVisibility(8);
                    break;
            }
            speedLimitView.setVisibility(0);
            this.speedLimitTextView = (TextView) speedLimitView.findViewById(R.id.txt_speed_limit);
            setSpeedLimit(this.speedLimit);
        }
    }

    public void setSpeedLimit(int speedLimit) {
        if (this.speedLimit != speedLimit) {
            this.speedLimit = speedLimit;
            if (this.speedLimitTextView != null) {
                this.speedLimitTextView.setText(Integer.toString(speedLimit));
            }
        }
    }

    public SpeedUnit getSpeedLimitUnit() {
        return this.speedLimitUnit;
    }
}
