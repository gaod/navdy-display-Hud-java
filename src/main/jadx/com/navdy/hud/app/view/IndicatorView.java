package com.navdy.hud.app.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Shader.TileMode;
import android.util.AttributeSet;
import android.view.View;
import com.navdy.hud.app.R;
import com.navdy.hud.app.util.CustomDimension;

public class IndicatorView extends View {
    private Paint indicatorPaint;
    private Path indicatorPath;
    private float mCurveRadius;
    private int mIndicatorHeight;
    private CustomDimension mIndicatorHeightAttribute;
    private float mIndicatorStrokeWidth;
    private int mIndicatorWidth;
    private CustomDimension mIndicatorWidthAttribute;
    private float mValue;

    public IndicatorView(Context context) {
        this(context, null);
    }

    public IndicatorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initFromAttributes(context, attrs);
    }

    public static void drawIndicatorPath(Path path, float left, float bottom, float curveRadius, float indicatorWidth, float indicatorHeight, float width) {
        drawIndicatorPath(path, 0.5f, left, bottom, curveRadius, indicatorWidth, indicatorHeight, width);
    }

    public static void drawIndicatorPath(Path path, float value, float left, float bottom, float curveRadius, float indicatorWidth, float indicatorHeight, float width) {
        path.moveTo(left, bottom);
        float right = left + width;
        double angle = Math.atan((double) (indicatorHeight / (indicatorWidth / 2.0f)));
        float rise = (float) (Math.sin(angle) * ((double) curveRadius));
        float run = (float) (Math.cos(angle) * ((double) curveRadius));
        float indicatorRadius = indicatorWidth / 2.0f;
        float middle = (left + indicatorRadius) + ((width - indicatorWidth) * value);
        path.lineTo((middle - indicatorRadius) - curveRadius, bottom);
        path.quadTo(middle - indicatorRadius, bottom, (middle - indicatorRadius) + run, bottom - rise);
        path.lineTo(middle, bottom - indicatorHeight);
        path.lineTo((middle + indicatorRadius) - run, bottom - rise);
        path.quadTo(middle + indicatorRadius, bottom, (middle + indicatorRadius) + curveRadius, bottom);
        path.lineTo(right, bottom);
    }

    private void initFromAttributes(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.Indicator, 0, 0);
        try {
            this.mIndicatorWidthAttribute = CustomDimension.getDimension(this, a, 0, 0.0f);
            this.mIndicatorHeightAttribute = CustomDimension.getDimension(this, a, 1, 0.0f);
            this.mCurveRadius = a.getDimension(2, 10.0f);
            this.mIndicatorStrokeWidth = a.getDimension(3, 4.0f);
            this.mValue = a.getFloat(4, 0.5f);
        } finally {
            a.recycle();
        }
    }

    public void evaluateDimensions(int w, int h) {
        this.mIndicatorHeight = (int) this.mIndicatorHeightAttribute.getSize(this, (float) w, 0.0f);
        this.mIndicatorWidth = (int) this.mIndicatorWidthAttribute.getSize(this, (float) w, 0.0f);
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        evaluateDimensions(w, h);
        initDrawingTools();
    }

    private void initDrawingTools() {
        this.indicatorPath = new Path();
        this.indicatorPaint = new Paint();
        this.indicatorPaint.setColor(-1);
        this.indicatorPaint.setAntiAlias(true);
        this.indicatorPaint.setStyle(Style.STROKE);
        this.indicatorPaint.setStrokeWidth(this.mIndicatorStrokeWidth);
        int[] colors = new int[]{0, -1, -1, 0};
        float[] positions = new float[]{0.0f, 0.45f, 0.55f, 1.0f};
        int left = getPaddingLeft();
        this.indicatorPaint.setShader(new LinearGradient((float) left, 0.0f, (float) (getWidth() - getPaddingRight()), 0.0f, colors, positions, TileMode.CLAMP));
        drawIndicatorPath(this.indicatorPath, this.mValue, (float) left, ((float) getHeight()) - this.mIndicatorStrokeWidth, this.mCurveRadius, (float) this.mIndicatorWidth, (float) this.mIndicatorHeight, (float) ((getWidth() - getPaddingRight()) - getPaddingLeft()));
    }

    public int getIndicatorHeight() {
        return this.mIndicatorHeight;
    }

    public int getIndicatorWidth() {
        return this.mIndicatorWidth;
    }

    public CustomDimension getIndicatorWidthAttribute() {
        return this.mIndicatorWidthAttribute;
    }

    public float getCurveRadius() {
        return this.mCurveRadius;
    }

    public float getStrokeWidth() {
        return this.mIndicatorStrokeWidth;
    }

    public void setValue(float value) {
        if (value != this.mValue) {
            this.mValue = value;
            initDrawingTools();
            invalidate();
        }
    }

    public float getValue() {
        return this.mValue;
    }

    protected void onDraw(Canvas canvas) {
        canvas.drawPath(this.indicatorPath, this.indicatorPaint);
    }
}
