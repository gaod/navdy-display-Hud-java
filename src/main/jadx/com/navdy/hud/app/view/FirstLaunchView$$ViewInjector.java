package com.navdy.hud.app.view;

import android.widget.ImageView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class FirstLaunchView$$ViewInjector {
    public static void inject(Finder finder, FirstLaunchView target, Object source) {
        target.firstLaunchLogo = (ImageView) finder.findRequiredView(source, R.id.firstLaunchLogo, "field 'firstLaunchLogo'");
    }

    public static void reset(FirstLaunchView target) {
        target.firstLaunchLogo = null;
    }
}
