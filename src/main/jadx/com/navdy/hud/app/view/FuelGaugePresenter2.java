package com.navdy.hud.app.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager;
import com.navdy.hud.app.view.drawable.FuelGaugeDrawable2;

public class FuelGaugePresenter2 extends DashboardWidgetPresenter {
    public static final int LOW_FUEL_WARNING_LIMIT = 25;
    public static final int VERY_LOW_FUEL_WARNING_LIMIT = 13;
    FuelGaugeDrawable2 fuelGaugeDrawable2;
    private String fuelGaugeName;
    @InjectView(R.id.fuel_tank_side_indicator_left)
    ImageView fuelTankIndicatorLeft;
    @InjectView(R.id.fuel_tank_side_indicator_right)
    ImageView fuelTankIndicatorRight;
    @InjectView(R.id.fuel_type_icon)
    ImageView fuelTypeIndicator;
    @InjectView(R.id.low_fuel_indicator_left)
    ImageView lowFuelIndicatorLeft;
    @InjectView(R.id.low_fuel_indicator_right)
    ImageView lowFuelIndicatorRight;
    private Context mContext;
    private int mFuelLevel;
    private int mFuelRange;
    private String mFuelRangeUnit = "";
    private boolean mLeftOriented = true;
    private FuelTankSide mTanksSide = FuelTankSide.UNKNOWN;
    @InjectView(R.id.txt_value)
    TextView rangeText;
    @InjectView(R.id.txt_unit)
    TextView rangeUnitText;

    public enum FuelTankSide {
        UNKNOWN,
        LEFT,
        RIGHT
    }

    public FuelGaugePresenter2(Context context) {
        this.mContext = context;
        this.fuelGaugeDrawable2 = new FuelGaugeDrawable2(context, R.array.smart_dash_fuel_gauge2_state_colors);
        this.fuelGaugeDrawable2.setMinValue(0.0f);
        this.fuelGaugeDrawable2.setMaxGaugeValue(100.0f);
        this.fuelGaugeName = context.getResources().getString(R.string.widget_fuel);
    }

    public void setFuelLevel(int mFuelLevel) {
        this.mFuelLevel = mFuelLevel;
        reDraw();
    }

    public void setFuelTankSide(FuelTankSide tankSide) {
        this.mTanksSide = tankSide;
    }

    public void setFuelRange(int value) {
        this.mFuelRange = value;
    }

    public void setFuelRangeUnit(String unitText) {
        this.mFuelRangeUnit = unitText;
    }

    public void setView(DashboardWidgetView dashboardWidgetView, Bundle arguments) {
        int layoutResourceId = R.layout.fuel_gauge_left;
        if (arguments != null) {
            switch (arguments.getInt(DashboardWidgetPresenter.EXTRA_GRAVITY)) {
                case 0:
                    layoutResourceId = R.layout.fuel_gauge_left;
                    this.mLeftOriented = true;
                    break;
                case 2:
                    layoutResourceId = R.layout.fuel_gauge_right;
                    this.mLeftOriented = false;
                    break;
            }
        }
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView(layoutResourceId);
            ButterKnife.inject( this, (View) dashboardWidgetView);
        }
        super.setView(dashboardWidgetView, arguments);
        reDraw();
    }

    public Drawable getDrawable() {
        return this.fuelGaugeDrawable2;
    }

    protected void updateGauge() {
        int i = 1;
        if (this.mWidgetView != null) {
            boolean veryLowFuel;
            boolean lowFuel = this.mFuelLevel <= 25;
            if (this.mFuelLevel <= 13) {
                veryLowFuel = true;
            } else {
                veryLowFuel = false;
            }
            this.fuelGaugeDrawable2.setGaugeValue((float) this.mFuelLevel);
            this.fuelGaugeDrawable2.setLeftOriented(this.mLeftOriented);
            FuelGaugeDrawable2 fuelGaugeDrawable2 = this.fuelGaugeDrawable2;
            if (!lowFuel) {
                i = 0;
            } else if (veryLowFuel) {
                i = 2;
            }
            fuelGaugeDrawable2.setState(i);
            this.lowFuelIndicatorLeft.setVisibility(8);
            this.lowFuelIndicatorRight.setVisibility(8);
            switch (this.mTanksSide) {
                case UNKNOWN:
                    this.fuelTankIndicatorLeft.setVisibility(8);
                    this.fuelTankIndicatorRight.setVisibility(8);
                    if (veryLowFuel) {
                        this.lowFuelIndicatorLeft.setVisibility(0);
                        break;
                    }
                    break;
                case LEFT:
                    this.fuelTankIndicatorLeft.setVisibility(0);
                    this.fuelTankIndicatorRight.setVisibility(8);
                    if (veryLowFuel) {
                        this.lowFuelIndicatorRight.setVisibility(0);
                        break;
                    }
                    break;
                case RIGHT:
                    this.fuelTankIndicatorLeft.setVisibility(8);
                    this.fuelTankIndicatorRight.setVisibility(0);
                    if (veryLowFuel) {
                        this.lowFuelIndicatorLeft.setVisibility(0);
                        break;
                    }
                    break;
            }
            if (this.mFuelRange > 0) {
                this.rangeText.setText(Integer.toString(this.mFuelRange));
            } else {
                this.rangeText.setText("");
            }
            this.rangeUnitText.setText(this.mFuelRangeUnit);
        }
    }

    public String getWidgetIdentifier() {
        return SmartDashWidgetManager.FUEL_GAUGE_ID;
    }

    public String getWidgetName() {
        return this.fuelGaugeName;
    }
}
