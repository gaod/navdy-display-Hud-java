package com.navdy.hud.app.view;

import android.support.constraint.ConstraintLayout;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class ETAGaugePresenter$$ViewInjector {
    public static void inject(Finder finder, ETAGaugePresenter target, Object source) {
        target.ttaText1 = (TextView) finder.findRequiredView(source, R.id.txt_tta1, "field 'ttaText1'");
        target.ttaText2 = (TextView) finder.findRequiredView(source, R.id.txt_tta2, "field 'ttaText2'");
        target.ttaText3 = (TextView) finder.findRequiredView(source, R.id.txt_tta3, "field 'ttaText3'");
        target.ttaText4 = (TextView) finder.findRequiredView(source, R.id.txt_tta4, "field 'ttaText4'");
        target.remainingDistanceText = (TextView) finder.findRequiredView(source, R.id.txt_remaining_distance, "field 'remainingDistanceText'");
        target.remainingDistanceUnitText = (TextView) finder.findRequiredView(source, R.id.txt_remaining_distance_unit, "field 'remainingDistanceUnitText'");
        target.etaText = (TextView) finder.findRequiredView(source, R.id.txt_eta, "field 'etaText'");
        target.etaAmPmText = (TextView) finder.findRequiredView(source, R.id.txt_eta_am_pm, "field 'etaAmPmText'");
        target.etaView = (ViewGroup) finder.findRequiredView(source, R.id.eta_view, "field 'etaView'");
        target.tripOpenMapView = (ConstraintLayout) finder.findRequiredView(source, R.id.tripOpenMap, "field 'tripOpenMapView'");
        target.tripDistanceView = (TextView) finder.findRequiredView(source, R.id.tripDistance, "field 'tripDistanceView'");
        target.tripTimeView = (TextView) finder.findRequiredView(source, R.id.tripTime, "field 'tripTimeView'");
        target.tripIconView = (ImageView) finder.findRequiredView(source, R.id.tripIcon, "field 'tripIconView'");
    }

    public static void reset(ETAGaugePresenter target) {
        target.ttaText1 = null;
        target.ttaText2 = null;
        target.ttaText3 = null;
        target.ttaText4 = null;
        target.remainingDistanceText = null;
        target.remainingDistanceUnitText = null;
        target.etaText = null;
        target.etaAmPmText = null;
        target.etaView = null;
        target.tripOpenMapView = null;
        target.tripDistanceView = null;
        target.tripTimeView = null;
        target.tripIconView = null;
    }
}
