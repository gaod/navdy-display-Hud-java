package com.navdy.hud.app.view;

import android.content.SharedPreferences;
import com.navdy.hud.app.ui.activity.Main.Presenter;
import com.navdy.hud.app.ui.framework.UIStateManager;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class MainView$$InjectAdapter extends Binding<MainView> implements MembersInjector<MainView> {
    private Binding<SharedPreferences> preferences;
    private Binding<Presenter> presenter;
    private Binding<UIStateManager> uiStateManager;

    public MainView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.MainView", false, MainView.class);
    }

    public void attach(Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.ui.activity.Main$Presenter", MainView.class, getClass().getClassLoader());
        this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", MainView.class, getClass().getClassLoader());
        this.preferences = linker.requestBinding("android.content.SharedPreferences", MainView.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
        injectMembersBindings.add(this.uiStateManager);
        injectMembersBindings.add(this.preferences);
    }

    public void injectMembers(MainView object) {
        object.presenter = (Presenter) this.presenter.get();
        object.uiStateManager = (UIStateManager) this.uiStateManager.get();
        object.preferences = (SharedPreferences) this.preferences.get();
    }
}
