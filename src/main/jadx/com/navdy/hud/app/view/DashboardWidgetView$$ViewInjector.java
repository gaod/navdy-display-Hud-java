package com.navdy.hud.app.view;

import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class DashboardWidgetView$$ViewInjector {
    public static void inject(Finder finder, DashboardWidgetView target, Object source) {
        target.mCustomView = finder.findOptionalView(source, R.id.custom_drawable);
    }

    public static void reset(DashboardWidgetView target) {
        target.mCustomView = null;
    }
}
