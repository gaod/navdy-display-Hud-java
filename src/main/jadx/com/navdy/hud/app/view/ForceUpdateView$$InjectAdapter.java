package com.navdy.hud.app.view;

import com.navdy.hud.app.screen.ForceUpdateScreen.Presenter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class ForceUpdateView$$InjectAdapter extends Binding<ForceUpdateView> implements MembersInjector<ForceUpdateView> {
    private Binding<Presenter> mPresenter;

    public ForceUpdateView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.ForceUpdateView", false, ForceUpdateView.class);
    }

    public void attach(Linker linker) {
        this.mPresenter = linker.requestBinding("com.navdy.hud.app.screen.ForceUpdateScreen$Presenter", ForceUpdateView.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mPresenter);
    }

    public void injectMembers(ForceUpdateView object) {
        object.mPresenter = (Presenter) this.mPresenter.get();
    }
}
