package com.navdy.hud.app.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.screen.OSUpdateConfirmationScreen.Presenter;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout.Mode;
import com.navdy.hud.app.util.ViewUtil;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import mortar.Mortar;

public class UpdateConfirmationView extends RelativeLayout implements IListener, IInputHandler {
    private static final long CONFIRMATION_TIMEOUT = 30000;
    private static final int UPDATE_MESSAGE_MAX_WIDTH = 360;
    private static final Logger sLogger = new Logger(UpdateConfirmationView.class);
    private Handler handler;
    private boolean isReminder;
    @InjectView(R.id.choiceLayout)
    ChoiceLayout mChoiceLayout;
    @InjectView(R.id.image)
    ImageView mIcon;
    @InjectView(R.id.title3)
    TextView mInfoText;
    @InjectView(R.id.leftSwipe)
    ImageView mLefttSwipe;
    @InjectView(R.id.title2)
    TextView mMainTitleText;
    @Inject
    Presenter mPresenter;
    @InjectView(R.id.rightSwipe)
    ImageView mRightSwipe;
    @InjectView(R.id.mainTitle)
    TextView mScreenTitleText;
    @InjectView(R.id.title4)
    TextView mVersionText;
    @InjectView(R.id.mainSection)
    RelativeLayout mainSection;
    private Runnable timeout;
    private String updateVersion;

    public UpdateConfirmationView(Context context) {
        this(context, null, 0);
    }

    public UpdateConfirmationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UpdateConfirmationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.handler = new Handler();
        this.timeout = new Runnable() {
            public void run() {
                UpdateConfirmationView.sLogger.v("timedout");
                UpdateConfirmationView.this.mPresenter.finish();
            }
        };
        this.updateVersion = "1.3.2887";
        this.isReminder = false;
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
    }

    @SuppressLint({"StringFormatMatches"})
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        Resources res = getContext().getResources();
        String currentVersion = "1.2.2884";
        if (this.mPresenter != null) {
            this.mPresenter.takeView(this);
            ToastManager.getInstance().disableToasts(true);
            NotificationManager.getInstance().enableNotifications(false);
            this.updateVersion = this.mPresenter.getUpdateVersion();
            currentVersion = this.mPresenter.getCurrentVersion();
            this.isReminder = this.mPresenter.isReminder();
        }
        this.mVersionText.setText(String.format(res.getString(R.string.update_navdy_will_upgrade_from_to), new Object[]{this.updateVersion, currentVersion}));
        this.mVersionText.setVisibility(0);
        this.mScreenTitleText.setVisibility(8);
        findViewById(R.id.title1).setVisibility(8);
        this.mMainTitleText.setText(R.string.update_ready_to_install);
        ViewUtil.adjustPadding(this.mainSection, 0, 0, 0, 10);
        ViewUtil.autosize(this.mMainTitleText, 2, (int) UPDATE_MESSAGE_MAX_WIDTH, (int) R.array.title_sizes);
        this.mMainTitleText.setVisibility(0);
        this.mInfoText.setText(R.string.ota_update_installation_will_take);
        this.mInfoText.setVisibility(0);
        this.mIcon.setImageResource(R.drawable.icon_software_update);
        ((MaxWidthLinearLayout) findViewById(R.id.infoContainer)).setMaxWidth(UPDATE_MESSAGE_MAX_WIDTH);
        List<Choice> list = new ArrayList();
        if (this.isReminder) {
            list.add(new Choice(res.getString(R.string.install_now), 0));
            list.add(new Choice(res.getString(R.string.install_later), 1));
        } else {
            list.add(new Choice(res.getString(R.string.install), 0));
            list.add(new Choice(res.getString(R.string.cancel), 1));
        }
        this.mChoiceLayout.setChoices(Mode.LABEL, list, 1, this);
        this.mRightSwipe.setVisibility(0);
        this.mLefttSwipe.setVisibility(0);
        this.handler.removeCallbacks(this.timeout);
        this.handler.postDelayed(this.timeout, 30000);
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    protected void onDetachedFromWindow() {
        this.handler.removeCallbacks(this.timeout);
        super.onDetachedFromWindow();
        ToastManager.getInstance().disableToasts(false);
        NotificationManager.getInstance().enableNotifications(true);
        if (this.mPresenter != null) {
            this.mPresenter.dropView((View) this);
        }
    }

    public void executeItem(int pos, int id) {
        this.handler.removeCallbacks(this.timeout);
        switch (pos) {
            case 0:
                this.mPresenter.install();
                this.mPresenter.finish();
                if (this.isReminder) {
                    AnalyticsSupport.recordUpdatePrompt(true, false, this.updateVersion);
                    return;
                }
                return;
            case 1:
                this.mPresenter.finish();
                if (this.isReminder) {
                    AnalyticsSupport.recordUpdatePrompt(false, false, this.updateVersion);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void itemSelected(int pos, int id) {
    }

    public boolean onGesture(GestureEvent event) {
        if (event.gesture != null) {
            switch (event.gesture) {
                case GESTURE_SWIPE_LEFT:
                    executeItem(0, 0);
                    return true;
                case GESTURE_SWIPE_RIGHT:
                    executeItem(1, 0);
                    return true;
            }
        }
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        this.handler.removeCallbacks(this.timeout);
        if (this.mChoiceLayout == null || this.mChoiceLayout.getVisibility() != 0) {
            return false;
        }
        switch (event) {
            case LEFT:
                this.mChoiceLayout.moveSelectionLeft();
                return true;
            case RIGHT:
                this.mChoiceLayout.moveSelectionRight();
                return true;
            case SELECT:
                this.mChoiceLayout.executeSelectedItem(true);
                return true;
            default:
                return true;
        }
    }

    public IInputHandler nextHandler() {
        return null;
    }
}
