package com.navdy.hud.app.view;

import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.hud.app.ui.component.FluctuatorAnimatorView;
import com.navdy.hud.app.ui.component.carousel.CarouselLayout;

public class WelcomeView$$ViewInjector {
    public static void inject(Finder finder, WelcomeView target, Object source) {
        target.titleView = (TextView) finder.findRequiredView(source, R.id.title, "field 'titleView'");
        target.subTitleView = (TextView) finder.findRequiredView(source, R.id.subTitle, "field 'subTitleView'");
        target.messageView = (TextView) finder.findRequiredView(source, R.id.message, "field 'messageView'");
        target.animatorView = (FluctuatorAnimatorView) finder.findRequiredView(source, R.id.animatorView, "field 'animatorView'");
        target.carousel = (CarouselLayout) finder.findRequiredView(source, R.id.carousel, "field 'carousel'");
        target.rootContainer = finder.findRequiredView(source, R.id.rootContainer, "field 'rootContainer'");
        target.downloadAppContainer = (RelativeLayout) finder.findRequiredView(source, R.id.download_app, "field 'downloadAppContainer'");
        target.downloadAppTextView1 = (TextView) finder.findRequiredView(source, R.id.download_text_1, "field 'downloadAppTextView1'");
        target.downloadAppTextView2 = (TextView) finder.findRequiredView(source, R.id.download_text_2, "field 'downloadAppTextView2'");
        target.appStoreImage1 = (ImageView) finder.findRequiredView(source, R.id.app_store_img_1, "field 'appStoreImage1'");
        target.appStoreImage2 = (ImageView) finder.findRequiredView(source, R.id.app_store_img_2, "field 'appStoreImage2'");
        target.playStoreImage1 = (ImageView) finder.findRequiredView(source, R.id.play_store_img_1, "field 'playStoreImage1'");
        target.playStoreImage2 = (ImageView) finder.findRequiredView(source, R.id.play_store_img_2, "field 'playStoreImage2'");
        target.choiceLayout = (ChoiceLayout) finder.findRequiredView(source, R.id.choiceLayout, "field 'choiceLayout'");
        target.leftDot = (ImageView) finder.findRequiredView(source, R.id.leftDot, "field 'leftDot'");
        target.rightDot = (ImageView) finder.findRequiredView(source, R.id.rightDot, "field 'rightDot'");
    }

    public static void reset(WelcomeView target) {
        target.titleView = null;
        target.subTitleView = null;
        target.messageView = null;
        target.animatorView = null;
        target.carousel = null;
        target.rootContainer = null;
        target.downloadAppContainer = null;
        target.downloadAppTextView1 = null;
        target.downloadAppTextView2 = null;
        target.appStoreImage1 = null;
        target.appStoreImage2 = null;
        target.playStoreImage1 = null;
        target.playStoreImage2 = null;
        target.choiceLayout = null;
        target.leftDot = null;
        target.rightDot = null;
    }
}
