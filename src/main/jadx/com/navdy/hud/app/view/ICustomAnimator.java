package com.navdy.hud.app.view;

import android.animation.Animator;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;

public interface ICustomAnimator {
    Animator getCustomAnimator(CustomAnimationMode customAnimationMode);
}
