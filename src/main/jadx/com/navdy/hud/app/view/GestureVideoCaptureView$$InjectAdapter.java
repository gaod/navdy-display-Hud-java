package com.navdy.hud.app.view;

import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.screen.GestureLearningScreen.Presenter;
import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class GestureVideoCaptureView$$InjectAdapter extends Binding<GestureVideoCaptureView> implements MembersInjector<GestureVideoCaptureView> {
    private Binding<Bus> bus;
    private Binding<GestureServiceConnector> gestureService;
    private Binding<Presenter> mPresenter;

    public GestureVideoCaptureView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.GestureVideoCaptureView", false, GestureVideoCaptureView.class);
    }

    public void attach(Linker linker) {
        this.mPresenter = linker.requestBinding("com.navdy.hud.app.screen.GestureLearningScreen$Presenter", GestureVideoCaptureView.class, getClass().getClassLoader());
        this.gestureService = linker.requestBinding("com.navdy.hud.app.gesture.GestureServiceConnector", GestureVideoCaptureView.class, getClass().getClassLoader());
        this.bus = linker.requestBinding("com.squareup.otto.Bus", GestureVideoCaptureView.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mPresenter);
        injectMembersBindings.add(this.gestureService);
        injectMembersBindings.add(this.bus);
    }

    public void injectMembers(GestureVideoCaptureView object) {
        object.mPresenter = (Presenter) this.mPresenter.get();
        object.gestureService = (GestureServiceConnector) this.gestureService.get();
        object.bus = (Bus) this.bus.get();
    }
}
