package com.navdy.hud.app.view;

import com.navdy.hud.app.presenter.NotificationPresenter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class NotificationView$$InjectAdapter extends Binding<NotificationView> implements MembersInjector<NotificationView> {
    private Binding<NotificationPresenter> presenter;

    public NotificationView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.NotificationView", false, NotificationView.class);
    }

    public void attach(Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.presenter.NotificationPresenter", NotificationView.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
    }

    public void injectMembers(NotificationView object) {
        object.presenter = (NotificationPresenter) this.presenter.get();
    }
}
