package com.navdy.hud.app.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.screen.FactoryResetScreen.Presenter;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.service.library.events.input.GestureEvent;
import javax.inject.Inject;
import mortar.Mortar;

public class FactoryResetView extends RelativeLayout implements IInputHandler {
    @InjectView(R.id.factory_reset_confirmation)
    ConfirmationLayout factoryResetConfirmation;
    @Inject
    Presenter presenter;

    public FactoryResetView(Context context) {
        this(context, null);
    }

    public FactoryResetView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FactoryResetView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView((View) this);
        }
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        return this.presenter.handleKey(event);
    }

    public IInputHandler nextHandler() {
        return InputManager.nextContainingHandler(this);
    }

    public ConfirmationLayout getConfirmation() {
        return this.factoryResetConfirmation;
    }
}
