package com.navdy.hud.app.view;

import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.ChoiceLayout;

public class ForceUpdateView$$ViewInjector {
    public static void inject(Finder finder, ForceUpdateView target, Object source) {
        target.mScreenTitleText = (TextView) finder.findRequiredView(source, R.id.mainTitle, "field 'mScreenTitleText'");
        target.mTextView1 = (TextView) finder.findRequiredView(source, R.id.title1, "field 'mTextView1'");
        target.mTextView2 = (TextView) finder.findRequiredView(source, R.id.title2, "field 'mTextView2'");
        target.mChoiceLayout = (ChoiceLayout) finder.findRequiredView(source, R.id.choiceLayout, "field 'mChoiceLayout'");
        target.mTextView3 = (TextView) finder.findRequiredView(source, R.id.title3, "field 'mTextView3'");
        target.mIcon = (ImageView) finder.findRequiredView(source, R.id.image, "field 'mIcon'");
    }

    public static void reset(ForceUpdateView target) {
        target.mScreenTitleText = null;
        target.mTextView1 = null;
        target.mTextView2 = null;
        target.mChoiceLayout = null;
        target.mTextView3 = null;
        target.mIcon = null;
    }
}
