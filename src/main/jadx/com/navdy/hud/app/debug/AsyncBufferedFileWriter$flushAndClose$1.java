package com.navdy.hud.app.debug;

import com.navdy.service.library.util.IOUtils;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 6})
/* compiled from: AsyncBufferedFileWriter.kt */
final class AsyncBufferedFileWriter$flushAndClose$1 implements Runnable {
    final /* synthetic */ AsyncBufferedFileWriter this$0;

    AsyncBufferedFileWriter$flushAndClose$1(AsyncBufferedFileWriter asyncBufferedFileWriter) {
        this.this$0 = asyncBufferedFileWriter;
    }

    public final void run() {
        this.this$0.getBufferedWriter().flush();
        IOUtils.closeStream(this.this$0.getBufferedWriter());
    }
}
