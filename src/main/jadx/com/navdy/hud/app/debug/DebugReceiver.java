package com.navdy.hud.app.debug;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.format.Formatter;
import android.util.Printer;
import com.amazonaws.services.s3.internal.Constants;
import com.glympse.android.lib.HttpJob;
import com.here.sdk.analytics.internal.HttpClient;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.device.light.HUDLightUtils;
import com.navdy.hud.app.device.light.LightManager;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.hud.app.event.Shutdown.Reason;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.obd.ObdDeviceConfigurationManager;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.service.library.device.connection.ConnectionService;
import com.navdy.service.library.events.audio.SpeechRequest.Category;
import com.navdy.service.library.events.destination.Destination;
import com.navdy.service.library.events.destination.Destination.Builder;
import com.navdy.service.library.events.destination.Destination.FavoriteType;
import com.navdy.service.library.events.dial.DialBondRequest;
import com.navdy.service.library.events.dial.DialBondRequest.DialAction;
import com.navdy.service.library.events.input.KeyEvent;
import com.navdy.service.library.events.input.LaunchAppEvent;
import com.navdy.service.library.events.input.MediaRemoteKey;
import com.navdy.service.library.events.input.MediaRemoteKeyEvent;
import com.navdy.service.library.events.location.LatLong;
import com.navdy.service.library.events.notification.NotificationCategory;
import com.navdy.service.library.events.notification.NotificationEvent;
import com.navdy.service.library.events.places.SuggestedDestination;
import com.navdy.service.library.events.places.SuggestedDestination.SuggestionType;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Bus;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import javax.inject.Inject;

public class DebugReceiver extends BroadcastReceiver {
    public static final String ACTION_ANR = "com.navdy.app.debug.ANR";
    public static final String ACTION_BROADCAST_ANR = "com.navdy.app.debug.BROADCAST_ANR";
    public static final String ACTION_CHANGE_HEADING = "com.navdy.app.debug.CHANGE_HEADING";
    public static final String ACTION_CHECK_FOR_MAP_UPDATES = "com.navdy.app.debug.CHECK_FOR_MAP_DATA_UPDATE";
    public static final String ACTION_CLEAR_MANEUVER = "com.navdy.app.debug.CLEAR_MANEUVER";
    public static final String ACTION_CRASH = "com.navdy.app.debug.CRASH";
    public static final String ACTION_DNS_LOOKUP = "com.navdy.app.debug.DNS_LOOKUP";
    public static final String ACTION_DNS_LOOKUP_TEST = "com.navdy.app.debug.DNS_LOOKUP_TEST";
    public static final String ACTION_DUMP_THREAD_STACK = "com.navdy.hud.debug.DUMP_THREAD_STACK";
    public static final String ACTION_EJECT_JUNCTION_VIEW = "com.navdy.app.debug.EJECT_JUNC_VIEW";
    public static final String ACTION_GET_GOOGLE = "com.navdy.app.debug.GET_GOOGLE";
    public static final String ACTION_HIDE_LANE_INFO = "com.navdy.app.debug.HIDE_LANE_INFO";
    public static final String ACTION_HIDE_RECALC = "com.navdy.app.debug.HIDE_RECALC";
    public static final String ACTION_INJECT_JUNCTION_VIEW = "com.navdy.app.debug.INJECT_JUNC_VIEW";
    public static final String ACTION_LAUNCH_MUSIC_DETAILS = "com.navdy.app.debug.MUSIC_DETAILS";
    public static final String ACTION_LAUNCH_PICKER = "com.navdy.app.debug.LAUNCH_PICKER";
    public static final String ACTION_NATIVE_CRASH = "com.navdy.app.debug.NATIVE_CRASH";
    public static final String ACTION_NATIVE_CRASH_SEPARATE_THREAD = "com.navdy.app.debug.NATIVE_CRASH_SEPARATE_THREAD";
    public static final String ACTION_NETSTAT = "com.navdy.app.debug.NETSTAT";
    public static final String ACTION_PLAYBACK_ROUTE = "com.navdy.app.debug.PLAYBACK_ROUTE";
    public static final String ACTION_PLAYBACK_ROUTE_SECONDARY = "com.navdy.app.debug.PLAYBACK_ROUTE_SECONDARY";
    public static final String ACTION_PRINT_BUS_REGISTRATION = "com.navdy.app.debug.PRINT_BUS_REGISTRATION";
    public static final String ACTION_PRINT_MAP_NAV_MODE = "com.navdy.app.debug.PRINT_MAP_NAV_MODE";
    public static final String ACTION_PRINT_MAP_ZOOM = "com.navdy.app.debug.PRINT_MAP_ZOOM";
    public static final String ACTION_SCALE_DISPLAY = "com.navdy.app.debug.SCALE_DISPLAY";
    public static final String ACTION_SEND_NOW_MANEUVER = "com.navdy.app.debug.NOW_MANEUVER";
    public static final String ACTION_SEND_SOON_MANEUVER = "com.navdy.app.debug.SOON_MANEUVER";
    public static final String ACTION_SEND_STAY_MANEUVER = "com.navdy.app.debug.STAY_MANEUVER";
    public static final String ACTION_SET_DISTANCE_MANEUVER = "com.navdy.app.debug.DISTANCE_MANEUVER";
    public static final String ACTION_SET_ICON_MANEUVER = "com.navdy.app.debug.ICON_MANEUVER";
    public static final String ACTION_SET_INSTRUCTION_MANEUVER = "com.navdy.app.debug.INSTRUCTION_MANEUVER";
    public static final String ACTION_SET_SIMULATION_SPEED = "com.navdy.app.debug.SET_SIM_SPEED";
    public static final String ACTION_SET_START_ROUTE_CALC_POINT = "com.navdy.app.debug.START_ROUTE_POINT";
    public static final String ACTION_SET_THEN_MANEUVER = "com.navdy.app.debug.THEN_MANEUVER";
    public static final String ACTION_SHOW_LANE_INFO = "com.navdy.app.debug.SHOW_LANE_INFO";
    public static final String ACTION_SHOW_RECALC = "com.navdy.app.debug.SHOW_RECALC";
    public static final String ACTION_START_BUS_PROFILING = "com.navdy.app.debug.START_BUS_PROFILING";
    public static final String ACTION_START_CPU_HOG = "com.navdy.app.debug.START_CPU_HOG";
    public static final String ACTION_START_MAIN_THREAD_PROFILING = "com.navdy.app.debug.START_MAIN_THREAD_PROFILING";
    public static final String ACTION_START_MAP_RENDERING = "com.navdy.app.debug.START_MAP_RENDERING";
    public static final String ACTION_STOP_BUS_PROFILING = "com.navdy.app.debug.STOP_BUS_PROFILING";
    public static final String ACTION_STOP_CPU_HOG = "com.navdy.app.debug.STOP_CPU_HOG";
    public static final String ACTION_STOP_MAIN_THREAD_PROFILING = "com.navdy.app.debug.STOP_MAIN_THREAD_PROFILING";
    public static final String ACTION_STOP_MAP_RENDERING = "com.navdy.app.debug.STOP_MAP_RENDERING";
    public static final String ACTION_STOP_PLAYBACK_ROUTE = "com.navdy.app.debug.STOP_PLAYBACK_ROUTE";
    public static final String ACTION_TEST = "com.navdy.hud.debug.TEST";
    public static final String ACTION_TEST_TBT_TTS = "com.navdy.app.debug.TEST_TBT_TTS";
    public static final String ACTION_TEST_TBT_TTS_CANCEL = "com.navdy.app.debug.TEST_TBT_TTS_CANCEL";
    public static final String ACTION_UPDATE_MAP_DATA = "com.navdy.app.debug.UPDATE_MAP_DATA";
    public static final String ACTION_WAKEUP = "com.navdy.app.debug.WAKEUP";
    public static final int CONNECTION_TIMEOUT_MILLIS = 30000;
    public static final int DESTINATION_SUGGESTION_TYPE_FAVORITE = 2;
    public static final int DESTINATION_SUGGESTION_TYPE_HOME = 0;
    public static final int DESTINATION_SUGGESTION_TYPE_NEW_PLACE = 3;
    public static final int DESTINATION_SUGGESTION_TYPE_WORK = 1;
    public static final String DISABLE_HERE_LOC_DBG = "com.navdy.app.debug.DISABLE_HERE_LOC_DBG";
    public static final String DNS_LOOKUP_HOST_NAME = "DNS_LOOKUP_HOST_NAME";
    private static final boolean ENABLED = true;
    public static final String ENABLE_HERE_LOC_DBG = "com.navdy.app.debug.ENABLE_HERE_LOC_DBG";
    public static final String EXTRA_BANDWIDTH_LEVEL = "BANDWIDTH_LEVEL";
    public static final String EXTRA_COMMAND = "COMMAND";
    public static final String EXTRA_DEEP = "DEEP";
    public static final String EXTRA_DESTINATION_SUGGESTION_TYPE = "DESTINATION_SUGGESTION_TYPE";
    public static final String EXTRA_KEY = "KEY";
    public static final String EXTRA_LIGHT = "LIGHT";
    public static final String EXTRA_MANEUVER_DISTANCE = "EXTRA_MANEUVER_DISTANCE";
    public static final String EXTRA_MANEUVER_ICON = "EXTRA_MANEUVER_ICON";
    public static final String EXTRA_MANEUVER_INSTRUCTION = "EXTRA_MANEUVER_INSTRUCTION";
    public static final String EXTRA_MANEUVER_THEN_ICON = "EXTRA_MANEUVER_THEN_ICON";
    public static final String EXTRA_MODE = "MODE";
    public static final String EXTRA_NOTIFICATION_EVENT_APP_ID = "appId";
    public static final String EXTRA_NOTIFICATION_EVENT_APP_NAME = "appName";
    public static final String EXTRA_NOTIFICATION_EVENT_ID = "id";
    public static final String EXTRA_NOTIFICATION_EVENT_MESSAGE = "message";
    public static final String EXTRA_NOTIFICATION_EVENT_SUB_TITLE = "subtitle";
    public static final String EXTRA_NOTIFICATION_EVENT_TITLE = "title";
    public static final String EXTRA_OBD_CONFIG = "CONFIG";
    public static final String EXTRA_SIZE = "SIZE";
    public static final String EXTRA_TEST = "TEST";
    public static final String EXTRA_URL = "URL";
    public static final String HEADING = "HEADING";
    private static final int MAIN_THREAD_DEFAULT_PROFILING_THRESHOLD = 0;
    public static final String MAIN_THREAD_EXTRA_PROFILING_THRESHOLD = "PROFILING_THRESHOLD";
    public static final int READ_TIMEOUT_MILLIS = 30000;
    public static final String SPEED = "SPEED";
    public static final String TEST_DESTINATION_SUGGESTION = "TEST_DESTINATION_SUGGESTION";
    public static final String TEST_DIAL = "TEST_DIAL";
    public static final String TEST_DISABLE_OBD_PIDS_SCANNING = "TEST_DISABLE_OBD";
    public static final String TEST_ENABLE_OBD_PIDS_SCANNING = "TEST_ENABLE_OBD";
    public static final String TEST_HID = "HID";
    public static final String TEST_LAUNCH_APP = "LAUNCH_APP";
    public static final String TEST_LIGHT = "TEST_LIGHT";
    public static final String TEST_OBD_CONFIG = "TEST_OBD";
    public static final String TEST_OBD_FLASH_NEW_FIRMWARE = "TEST_OBD_FLASH_NEW_FIRMWARE";
    public static final String TEST_OBD_FLASH_OLD_FIRMWARE = "TEST_OBD_FLASH_OLD_FIRMWARE";
    public static final String TEST_OBD_RESET = "TEST_OBD_RESET";
    public static final String TEST_OBD_SET_MODE_J1939 = "TEST_OBD_SET_MODE_J1939";
    public static final String TEST_OBD_SET_MODE_OBD2 = "TEST_OBD_SET_MODE_OBD2";
    public static final String TEST_OBD_SLEEP = "TEST_OBD_SLEEP";
    public static final String TEST_PROXY_FILE_DOWNLOAD = "TEST_PROXY_FILE_DOWNLOAD";
    public static final String TEST_PROXY_FILE_UPLOAD = "TEST_PROXY_FILE_UPLOAD";
    public static final String TEST_SEND_NOTIFICATION_EVENT = "TEST_SEND_NOTIFICATION_EVENT";
    public static final String TEST_SHUT_DOWN = "SHUTDOWN";
    public static final String TEST_SWITCH_BANDWIDTH_LEVEL = "TEST_SWITCH_BANDWIDTH";
    public static final String TEST_TTS = "TEST_TTS";
    private static final String[] TTS_TEXTS = new String[]{"Turn left after 100 feet", "Turn right after 500 feet. 1, 2, 3, 4, 5, 6, 7, 8,9,10", "There is a slight traffic ahead, be on the right lane.", "Hi how are you doing today? Testing a long long long long text", "Testing testing still testing."};
    static final Logger sLogger = new Logger(DebugReceiver.class);
    @Inject
    Bus mBus;
    private PrinterImpl mainThreadProfilingPrinter = new PrinterImpl();

    private static class PrinterImpl implements Printer {
        private PrinterImpl() {
        }

        /* synthetic */ PrinterImpl(AnonymousClass1 x0) {
            this();
        }

        public void println(String str) {
            DebugReceiver.sLogger.v(str);
        }
    }

    public void onReceive(Context context, Intent intent) {
    }

    public static void setHereDebugLocation(boolean b) {
        try {
            Looper.class.getDeclaredField("enableHereLocationDebugging").setBoolean(null, b);
            sLogger.v("setHereDebugLocation set to " + b);
        } catch (Throwable t) {
            sLogger.e("setHereDebugLocation", t);
        }
    }

    private void testHID(Intent intent) {
        try {
            final int mediaKey = intent.getIntExtra(EXTRA_KEY, 0);
            this.mBus.post(new RemoteEvent(new MediaRemoteKeyEvent(MediaRemoteKey.values()[mediaKey], KeyEvent.KEY_DOWN)));
            new Timer().schedule(new TimerTask() {
                public void run() {
                    DebugReceiver.this.mBus.post(new RemoteEvent(new MediaRemoteKeyEvent(MediaRemoteKey.values()[mediaKey], KeyEvent.KEY_UP)));
                }
            }, 100);
        } catch (Exception e) {
            sLogger.e("Exception getting the extra");
        }
    }

    private void testAppLaunch(Intent intent) {
        this.mBus.post(new RemoteEvent(new LaunchAppEvent(null)));
    }

    private void testShutDown() {
        this.mBus.post(new ShowScreenWithArgs(Screen.SCREEN_SHUTDOWN_CONFIRMATION, Reason.POWER_BUTTON.asBundle(), false));
    }

    private void testLight(Intent intent) {
        int light = intent.getIntExtra(EXTRA_LIGHT, 0);
        LightManager manager = LightManager.getInstance();
        switch (light) {
            case 0:
                HUDLightUtils.showPairing(HudApplication.getAppContext(), manager, true);
                return;
            case 1:
                HUDLightUtils.showShutDown(manager);
                return;
            case 2:
                HUDLightUtils.showGestureDetectionEnabled(HudApplication.getAppContext(), manager, "Debug");
                return;
            case 3:
                HUDLightUtils.showGestureNotRecognized(HudApplication.getAppContext(), manager);
                return;
            case 4:
                HUDLightUtils.showGestureDetected(HudApplication.getAppContext(), manager);
                return;
            case 5:
                HUDLightUtils.showUSBPowerOn(HudApplication.getAppContext(), manager);
                return;
            case 6:
                HUDLightUtils.showUSBPowerShutDown(HudApplication.getAppContext(), manager);
                return;
            case 7:
                HUDLightUtils.showUSBTransfer(HudApplication.getAppContext(), manager);
                return;
            case 8:
                HUDLightUtils.showError(HudApplication.getAppContext(), manager);
                return;
            default:
                return;
        }
    }

    private void testTTS(Intent intent) {
        TTSUtils.sendSpeechRequest(TTS_TEXTS[new Random().nextInt(TTS_TEXTS.length)], Category.SPEECH_TURN_BY_TURN, null);
    }

    private void testDial(Intent intent) {
        String cmd = intent.getStringExtra("COMMAND");
        if (cmd.equals("PowerButtonDoubleClick")) {
            RemoteDeviceManager.getInstance().getInputManager().injectKey(CustomKeyEvent.POWER_BUTTON_DOUBLE_CLICK);
        } else if (cmd.equals("rebond")) {
            this.mBus.post(new DialBondRequest(DialAction.DIAL_CLEAR_BOND));
            this.mBus.post(new DialBondRequest(DialAction.DIAL_BOND));
        }
    }

    private void testObdConfig(Intent intent) {
        switch (intent.getIntExtra(EXTRA_OBD_CONFIG, 1)) {
            case 0:
                ObdManager.getInstance().getObdDeviceConfigurationManager().setConfigurationFile(ObdDeviceConfigurationManager.DEBUG_CONFIGURATION);
                return;
            case 1:
                ObdManager.getInstance().getObdDeviceConfigurationManager().setConfigurationFile(ObdDeviceConfigurationManager.DEFAULT_ON_CONFIGURATION);
                return;
            default:
                return;
        }
    }

    private void testDestinationSuggestion(Intent intent) {
        SuggestedDestination suggestedDestination = null;
        switch (intent.getIntExtra(EXTRA_DESTINATION_SUGGESTION_TYPE, 0)) {
            case 0:
                suggestedDestination = new SuggestedDestination(new Builder().navigation_position(new LatLong(Double.valueOf(37.7786444d), Double.valueOf(-122.4462313d))).display_position(new LatLong(Double.valueOf(37.7786444d), Double.valueOf(-122.4462313d))).full_address("201 8th St, San Francisco, CA 94103").destination_title("Home").destination_subtitle("").favorite_type(FavoriteType.FAVORITE_HOME).identifier(ToastPresenter.EXTRA_MAIN_TITLE).suggestion_type(Destination.DEFAULT_SUGGESTION_TYPE).is_recommendation(Boolean.valueOf(true)).last_navigated_to(Long.valueOf(0)).build(), Integer.valueOf(600), SuggestionType.SUGGESTION_RECOMMENDATION);
                break;
            case 1:
                suggestedDestination = new SuggestedDestination(new Builder().navigation_position(new LatLong(Double.valueOf(37.7735078d), Double.valueOf(-122.4055828d))).display_position(new LatLong(Double.valueOf(37.7735078d), Double.valueOf(-122.4055828d))).full_address("575 7th St, San Francisco, CA 94103").destination_title("Work").destination_subtitle("").favorite_type(FavoriteType.FAVORITE_WORK).identifier(ToastPresenter.EXTRA_MAIN_TITLE_1).suggestion_type(Destination.DEFAULT_SUGGESTION_TYPE).is_recommendation(Boolean.valueOf(true)).last_navigated_to(Long.valueOf(0)).build(), Integer.valueOf(3600), SuggestionType.SUGGESTION_RECOMMENDATION);
                break;
            case 2:
                suggestedDestination = new SuggestedDestination(new Builder().navigation_position(new LatLong(Double.valueOf(37.7879373d), Double.valueOf(-122.4096868d))).display_position(new LatLong(Double.valueOf(37.7879373d), Double.valueOf(-122.4096868d))).full_address("Union Square, San Francisco, CA 94108").destination_title("Union Square").destination_subtitle("").favorite_type(FavoriteType.FAVORITE_CUSTOM).identifier(ToastPresenter.EXTRA_MAIN_TITLE_1_STYLE).suggestion_type(Destination.DEFAULT_SUGGESTION_TYPE).is_recommendation(Boolean.valueOf(true)).last_navigated_to(Long.valueOf(0)).build(), Integer.valueOf(HttpJob.RESPONSE_CODE_MAX_RETRY_LOWER_BOUND), SuggestionType.SUGGESTION_RECOMMENDATION);
                break;
            case 3:
                suggestedDestination = new SuggestedDestination(new Builder().navigation_position(new LatLong(Double.valueOf(37.7766916d), Double.valueOf(-122.3970457d))).display_position(new LatLong(Double.valueOf(37.7766916d), Double.valueOf(-122.3970457d))).full_address("700 4th St, San Francisco, CA 94107").destination_title("").destination_subtitle("").favorite_type(FavoriteType.FAVORITE_NONE).identifier(ToastPresenter.EXTRA_MAIN_TITLE_2).suggestion_type(Destination.DEFAULT_SUGGESTION_TYPE).is_recommendation(Boolean.valueOf(true)).last_navigated_to(Long.valueOf(0)).build(), Integer.valueOf(-1), SuggestionType.SUGGESTION_RECOMMENDATION);
                break;
        }
        this.mBus.post(suggestedDestination);
    }

    private void testSwitchBandwidthLevel(Intent intent) {
        int bandwidthLevel = intent.getIntExtra(EXTRA_BANDWIDTH_LEVEL, 1);
        Intent boradcast = new Intent();
        boradcast.setAction(ConnectionService.ACTION_LINK_BANDWIDTH_LEVEL_CHANGED);
        boradcast.addCategory(ConnectionService.CATEGORY_NAVDY_LINK);
        boradcast.putExtra(ConnectionService.EXTRA_BANDWIDTH_LEVEL, bandwidthLevel);
        HudApplication.getAppContext().sendBroadcast(boradcast);
    }

    private void testProxyFileUpload(Intent intent) {
        final int size = intent.getIntExtra(EXTRA_SIZE, 100);
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                double timeElapsed;
                try {
                    int finalSize;
                    HttpURLConnection urlConnection = (HttpURLConnection) new URL("http://test.mjpmz4phvm.us-west-2.elasticbeanstalk.com/file").openConnection();
                    urlConnection.setConnectTimeout(30000);
                    urlConnection.setReadTimeout(30000);
                    urlConnection.setRequestMethod(HttpClient.METHOD_POST);
                    OutputStream os = urlConnection.getOutputStream();
                    if (size < 0 || size > 2000) {
                        finalSize = 100000;
                    } else {
                        finalSize = size * 1000;
                    }
                    DebugReceiver.sLogger.d("Uploading " + Formatter.formatFileSize(HudApplication.getAppContext(), (long) finalSize));
                    byte[] data = new byte[finalSize];
                    new Random().nextBytes(data);
                    os.write(data);
                    if (urlConnection.getResponseCode() == 200) {
                        DebugReceiver.sLogger.d("POST succeeded");
                    }
                    timeElapsed = (double) (SystemClock.elapsedRealtime() - 0);
                    DebugReceiver.sLogger.e("Upload finished, Size : " + (0 / 1000) + ", Time Taken :" + ((long) timeElapsed) + " milliseconds , RawSpeed :" + ((float) (((double) (((float) 0) / 1000.0f)) / (timeElapsed / 1000.0d))) + " kBps");
                    IOUtils.closeStream(null);
                } catch (SocketTimeoutException se) {
                    DebugReceiver.sLogger.e("Socket timed out Exception ", se);
                    timeElapsed = (double) (SystemClock.elapsedRealtime() - 0);
                    DebugReceiver.sLogger.e("Upload finished, Size : " + (0 / 1000) + ", Time Taken :" + ((long) timeElapsed) + " milliseconds , RawSpeed :" + ((float) (((double) (((float) 0) / 1000.0f)) / (timeElapsed / 1000.0d))) + " kBps");
                    IOUtils.closeStream(null);
                } catch (IOException e) {
                    DebugReceiver.sLogger.e("IOException ", e);
                    timeElapsed = (double) (SystemClock.elapsedRealtime() - 0);
                    DebugReceiver.sLogger.e("Upload finished, Size : " + (0 / 1000) + ", Time Taken :" + ((long) timeElapsed) + " milliseconds , RawSpeed :" + ((float) (((double) (((float) 0) / 1000.0f)) / (timeElapsed / 1000.0d))) + " kBps");
                    IOUtils.closeStream(null);
                } catch (Exception e2) {
                    DebugReceiver.sLogger.e("Exception ", e2);
                    timeElapsed = (double) (SystemClock.elapsedRealtime() - 0);
                    DebugReceiver.sLogger.e("Upload finished, Size : " + (0 / 1000) + ", Time Taken :" + ((long) timeElapsed) + " milliseconds , RawSpeed :" + ((float) (((double) (((float) 0) / 1000.0f)) / (timeElapsed / 1000.0d))) + " kBps");
                    IOUtils.closeStream(null);
                } catch (Throwable th) {
                    timeElapsed = (double) (SystemClock.elapsedRealtime() - 0);
                    DebugReceiver.sLogger.e("Upload finished, Size : " + (0 / 1000) + ", Time Taken :" + ((long) timeElapsed) + " milliseconds , RawSpeed :" + ((float) (((double) (((float) 0) / 1000.0f)) / (timeElapsed / 1000.0d))) + " kBps");
                    IOUtils.closeStream(null);
                }
            }
        }, 1);
    }

    private void testProxyFileDownload(Intent intent) {
        String address;
        final int size = intent.getIntExtra(EXTRA_SIZE, 100);
        String defaultAddress = "http://test.mjpmz4phvm.us-west-2.elasticbeanstalk.com/file?size=" + size;
        String extra = intent.getStringExtra("URL");
        if (extra != null) {
            address = extra;
        } else {
            address = defaultAddress;
        }
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                InputStream in = null;
                long startTime = 0;
                long totalBytesRead = 0;
                double timeElapsed;
                try {
                    DebugReceiver.sLogger.d("Intended Size to be dowloaded is " + size);
                    HttpURLConnection urlConnection = (HttpURLConnection) new URL(address).openConnection();
                    urlConnection.setRequestMethod(HttpClient.METHOD_GET);
                    urlConnection.setConnectTimeout(30000);
                    urlConnection.setReadTimeout(30000);
                    int responseCode = urlConnection.getResponseCode();
                    if (responseCode == 200) {
                        DebugReceiver.sLogger.d("Starting to download");
                        in = urlConnection.getInputStream();
                        byte[] buffer = new byte[102400];
                        startTime = SystemClock.elapsedRealtime();
                        while (true) {
                            long bytesRead = (long) in.read(buffer);
                            if (bytesRead == -1) {
                                break;
                            }
                            totalBytesRead += bytesRead;
                        }
                    } else {
                        DebugReceiver.sLogger.e("Response code not OK " + responseCode);
                    }
                    timeElapsed = (double) (SystemClock.elapsedRealtime() - startTime);
                    DebugReceiver.sLogger.e("Download finished, Size : " + (totalBytesRead / 1000) + ", Time Taken :" + ((long) timeElapsed) + " milliseconds , RawSpeed :" + ((float) (((double) (((float) totalBytesRead) / 1000.0f)) / (timeElapsed / 1000.0d))) + " kBps");
                    IOUtils.closeStream(in);
                } catch (SocketTimeoutException se) {
                    DebugReceiver.sLogger.e("Socket timed out Exception ", se);
                    timeElapsed = (double) (SystemClock.elapsedRealtime() - 0);
                    DebugReceiver.sLogger.e("Download finished, Size : " + (0 / 1000) + ", Time Taken :" + ((long) timeElapsed) + " milliseconds , RawSpeed :" + ((float) (((double) (((float) 0) / 1000.0f)) / (timeElapsed / 1000.0d))) + " kBps");
                    IOUtils.closeStream(null);
                } catch (IOException e) {
                    DebugReceiver.sLogger.e("IOException ", e);
                    timeElapsed = (double) (SystemClock.elapsedRealtime() - 0);
                    DebugReceiver.sLogger.e("Download finished, Size : " + (0 / 1000) + ", Time Taken :" + ((long) timeElapsed) + " milliseconds , RawSpeed :" + ((float) (((double) (((float) 0) / 1000.0f)) / (timeElapsed / 1000.0d))) + " kBps");
                    IOUtils.closeStream(null);
                } catch (Exception e2) {
                    DebugReceiver.sLogger.e("Exception ", e2);
                    timeElapsed = (double) (SystemClock.elapsedRealtime() - 0);
                    DebugReceiver.sLogger.e("Download finished, Size : " + (0 / 1000) + ", Time Taken :" + ((long) timeElapsed) + " milliseconds , RawSpeed :" + ((float) (((double) (((float) 0) / 1000.0f)) / (timeElapsed / 1000.0d))) + " kBps");
                    IOUtils.closeStream(null);
                } catch (Throwable th) {
                    timeElapsed = (double) (SystemClock.elapsedRealtime() - 0);
                    DebugReceiver.sLogger.e("Download finished, Size : " + (0 / 1000) + ", Time Taken :" + ((long) timeElapsed) + " milliseconds , RawSpeed :" + ((float) (((double) (((float) 0) / 1000.0f)) / (timeElapsed / 1000.0d))) + " kBps");
                    IOUtils.closeStream(null);
                }
            }
        }, 1);
    }

    private void triggerCrash() {
        throw new IllegalStateException("Oops");
    }

    private void triggerANR() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                DebugReceiver.this.busywait(10000);
            }
        });
    }

    private void triggerBroadcastANR() {
        busywait(12000);
    }

    private void busywait(int ms) {
        long start = SystemClock.elapsedRealtime();
        for (long now = SystemClock.elapsedRealtime(); now - start < ((long) ms); now = SystemClock.elapsedRealtime()) {
        }
    }

    private void getGoogleHomePage() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                HttpURLConnection connection = null;
                InputStream inputStream = null;
                try {
                    DebugReceiver.sLogger.v("getGoogleHomePage");
                    connection = (HttpURLConnection) new URL("https://www.google.com").openConnection();
                    connection.setRequestMethod(HttpClient.METHOD_GET);
                    inputStream = connection.getInputStream();
                    String data = IOUtils.convertInputStreamToString(inputStream, "UTF-8");
                    Map<String, List<String>> headers = connection.getHeaderFields();
                    for (String key : headers.keySet()) {
                        String val = (String) ((List) headers.get(key)).get(0);
                        if (Constants.NULL_VERSION_ID.equals(key) || key == null) {
                            DebugReceiver.sLogger.v("getGoogleHomePage " + val);
                        } else {
                            DebugReceiver.sLogger.v("getGoogleHomePage " + key + ":  " + val);
                        }
                    }
                    DebugReceiver.sLogger.v("getGoogleHomePage " + data);
                    DebugReceiver.sLogger.v("getGoogleHomePage LEN = " + data.length());
                    IOUtils.closeStream(inputStream);
                    if (connection != null) {
                        try {
                            connection.disconnect();
                        } catch (Throwable t) {
                            DebugReceiver.sLogger.e(t);
                        }
                    }
                } catch (Throwable t2) {
                    DebugReceiver.sLogger.e(t2);
                }
            }
        }, 1);
    }

    private void dnsLookup(final String host) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    String hostName = host;
                    if (hostName == null) {
                        hostName = "analytics.localytics.com";
                    }
                    DebugReceiver.sLogger.v("dnslookup :" + hostName);
                    InetAddress[] addresses = InetAddress.getAllByName(hostName);
                    if (addresses != null) {
                        DebugReceiver.sLogger.v("dnslookup total:" + addresses.length);
                        for (InetAddress hostAddress : addresses) {
                            DebugReceiver.sLogger.v("dnslookup address :" + hostAddress.getHostAddress());
                        }
                        return;
                    }
                    DebugReceiver.sLogger.v("dnslookup no address");
                } catch (Throwable t) {
                    DebugReceiver.sLogger.e(t);
                }
            }
        }, 1);
    }

    private void testSendNotificationEvent(Intent intent) {
        try {
            NotificationEvent.Builder notificationEventBuilder = new NotificationEvent.Builder();
            if (intent != null) {
                notificationEventBuilder.id(Integer.valueOf(intent.getIntExtra("id", 1)));
                notificationEventBuilder.category(NotificationCategory.CATEGORY_OTHER);
                notificationEventBuilder.title(intent.getStringExtra("title"));
                notificationEventBuilder.subtitle(intent.getStringExtra(EXTRA_NOTIFICATION_EVENT_SUB_TITLE));
                notificationEventBuilder.message(intent.getStringExtra(EXTRA_NOTIFICATION_EVENT_MESSAGE));
                notificationEventBuilder.appId(intent.getStringExtra(EXTRA_NOTIFICATION_EVENT_APP_ID));
                notificationEventBuilder.appName(intent.getStringExtra(EXTRA_NOTIFICATION_EVENT_APP_NAME));
                this.mBus.post(new RemoteEvent(notificationEventBuilder.build()));
            }
        } catch (Exception e) {
            sLogger.e("Error while sending test Notification event ", e);
        }
    }

    private void dnsLookupTest() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    String[] hosts = new String[]{"version.hybrid.api.here.com", "reverse.geocoder.api.here.com", "tpeg.traffic.api.here.com", "tpeg.hybrid.api.here.com", "download.hybrid.api.here.com", "v102-62-30-8.route.hybrid.api.here.com", "analytics.localytics.com", "profile.localytics.com", "sdk.hockeyapp.net", "navdyhud.atlassian.net"};
                    DebugReceiver.sLogger.v("dns_lookup_test");
                    for (int i = 0; i < hosts.length; i++) {
                        InetAddress[] addresses = InetAddress.getAllByName(hosts[i]);
                        if (addresses != null) {
                            DebugReceiver.sLogger.v("dns_lookup_test[" + hosts[i] + "] addresses = " + addresses.length);
                            for (InetAddress hostAddress : addresses) {
                                DebugReceiver.sLogger.v("dns_lookup_test[" + hosts[i] + "]  addr[" + hostAddress.getHostAddress() + "]");
                            }
                        } else {
                            DebugReceiver.sLogger.v("dns_lookup_test[" + hosts[i] + "] no address");
                        }
                    }
                } catch (Throwable t) {
                    DebugReceiver.sLogger.e(t);
                }
            }
        }, 1);
    }
}
