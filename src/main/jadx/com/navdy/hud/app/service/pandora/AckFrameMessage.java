package com.navdy.hud.app.service.pandora;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class AckFrameMessage extends FrameMessage {
    private static final ByteBuffer ACK_FRAME_BASE = ByteBuffer.allocate(6).put((byte) 1).put((byte) 0).putInt(0);
    public static final AckFrameMessage ACK_WITH_SEQUENCE_0 = new AckFrameMessage(true);
    private static final byte[] ACK_WITH_SEQUENCE_0_FRAME = FrameMessage.buildFrameFromCRCPart(ACK_FRAME_BASE);
    public static final AckFrameMessage ACK_WITH_SEQUENCE_1 = new AckFrameMessage(false);
    private static final byte[] ACK_WITH_SEQUENCE_1_FRAME = FrameMessage.buildFrameFromCRCPart(ACK_FRAME_BASE.put(1, (byte) 1));

    private AckFrameMessage(boolean isSequence0) {
        super(isSequence0, ACK_PAYLOAD);
    }

    public byte[] buildFrame() throws IllegalArgumentException {
        return this.isSequence0 ? ACK_WITH_SEQUENCE_0_FRAME : ACK_WITH_SEQUENCE_1_FRAME;
    }

    protected static AckFrameMessage parseAckFrame(byte[] ackFrame) throws IllegalArgumentException {
        if (Arrays.equals(ackFrame, ACK_WITH_SEQUENCE_0_FRAME)) {
            return ACK_WITH_SEQUENCE_0;
        }
        if (Arrays.equals(ackFrame, ACK_WITH_SEQUENCE_1_FRAME)) {
            return ACK_WITH_SEQUENCE_1;
        }
        throw new IllegalArgumentException("Not a valid ACK message frame");
    }
}
