package com.navdy.hud.app.service;

import android.app.IntentService;
import android.content.Intent;
import android.text.TextUtils;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.CredentialUtil;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.SystemUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class S3FileUploadService extends IntentService {
    public static final String ACTION_SYNC = "SYNC";
    private static final String AWS_ACCOUNT_ID = "AWS_ACCESS_KEY_ID";
    private static final String AWS_SECRET = "AWS_SECRET_ACCESS_KEY";
    public static final int RETRY_DELAY = 10000;
    public static final int S3_CONNECTION_TIMEOUT = 15000;
    public static final int S3_UPLOAD_TIMEOUT = 300000;
    protected Logger logger = getLogger();
    private AmazonS3Client s3Client;
    private TransferUtility transferUtility;

    /* renamed from: com.navdy.hud.app.service.S3FileUploadService$2 */
    static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState = new int[TransferState.values().length];

        static {
            try {
                $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[TransferState.COMPLETED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[TransferState.CANCELED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[TransferState.WAITING_FOR_NETWORK.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[TransferState.IN_PROGRESS.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[TransferState.FAILED.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
        }
    }

    protected static class Request {
        public final File file;
        public final String userTag;

        public Request(File file, String userTag) {
            this.file = file;
            this.userTag = userTag;
        }
    }

    public static class RequestTimeComparator implements Comparator<Request> {
        public int compare(Request a, Request b) {
            return Long.compare(b.file.lastModified(), a.file.lastModified());
        }
    }

    public static class UploadFinished {
        public final String filePath;
        public final boolean succeeded;
        public final String userTag;

        UploadFinished(boolean succeeded, String filePath, String userTag) {
            this.succeeded = succeeded;
            this.filePath = filePath;
            this.userTag = userTag;
        }
    }

    public static class UploadQueue {
        private final PriorityQueue<Request> internalQueue = new PriorityQueue(15, new RequestTimeComparator());

        public Request pop() {
            return (Request) this.internalQueue.poll();
        }

        public Request peek() {
            return (Request) this.internalQueue.peek();
        }

        public void add(Request request) {
            this.internalQueue.add(request);
        }

        public int size() {
            return this.internalQueue.size();
        }
    }

    public abstract boolean canCompleteRequest(Request request);

    protected abstract String getAWSBucket();

    protected abstract Request getCurrentRequest();

    protected abstract AtomicBoolean getIsUploading();

    protected abstract String getKeyPrefix(File file);

    protected abstract Logger getLogger();

    protected abstract UploadQueue getUploadQueue();

    protected abstract void initialize();

    public abstract void reSchedule();

    protected abstract void setCurrentRequest(Request request);

    public abstract void sync();

    protected abstract void uploadFinished(boolean z, String str, String str2);

    public S3FileUploadService(String name) {
        super(name);
    }

    public AmazonS3Client createS3Client() {
        AWSCredentials credentials = new BasicAWSCredentials(CredentialUtil.getCredentials(HudApplication.getAppContext(), "AWS_ACCESS_KEY_ID"), CredentialUtil.getCredentials(HudApplication.getAppContext(), "AWS_SECRET_ACCESS_KEY"));
        ClientConfiguration config = new ClientConfiguration();
        config.setConnectionTimeout(15000);
        config.setSocketTimeout(300000);
        return new AmazonS3Client(credentials, config);
    }

    public void onCreate() {
        super.onCreate();
        if (this.s3Client == null) {
            this.s3Client = createS3Client();
            this.transferUtility = new TransferUtility(this.s3Client, HudApplication.getAppContext());
        }
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public static void populateFilesQueue(ArrayList<File> files, UploadQueue filesQueue, int maxSize) {
        if (files != null) {
            Iterator it = files.iterator();
            while (it.hasNext()) {
                File file = (File) it.next();
                if (file.isFile()) {
                    filesQueue.add(new Request(file, null));
                    if (filesQueue.size() == maxSize) {
                        filesQueue.pop();
                    }
                }
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected void onHandleIntent(Intent intent) {
        if (ACTION_SYNC.equals(intent != null ? intent.getAction() : "")) {
            boolean connectedNetwork = SystemUtils.isConnectedToNetwork(HudApplication.getAppContext());
            this.logger.d("Performing sync , connected to network ? : " + connectedNetwork);
            if (connectedNetwork) {
                initialize();
                UploadQueue gestureUploadQueue = getUploadQueue();
                synchronized (gestureUploadQueue) {
                    if (gestureUploadQueue.size() > 0) {
                        final AtomicBoolean isUploading = getIsUploading();
                        if (isUploading.compareAndSet(false, true)) {
                            Request currentRequest = null;
                            do {
                                if (currentRequest != null) {
                                    this.logger.e("File to upload " + currentRequest.file + ", Does not exist anymore");
                                }
                                if (gestureUploadQueue.size() > 0) {
                                    currentRequest = gestureUploadQueue.pop();
                                } else {
                                    isUploading.set(false);
                                    return;
                                }
                            } while (!currentRequest.file.exists());
                            setCurrentRequest(currentRequest);
                            final File file = currentRequest.file;
                            String prefix = getKeyPrefix(file);
                            String key = !TextUtils.isEmpty(prefix) ? prefix + File.separator + file.getName() : file.getName();
                            this.logger.d("Trying to upload : " + file.getName() + ", Under :" + prefix + ", Key :" + key);
                            try {
                                final TransferObserver observer = this.transferUtility.upload(getAWSBucket(), key, file);
                                this.logger.d("Transfer id " + observer.getId());
                                observer.setTransferListener(new TransferListener() {
                                    public void onStateChanged(int id, TransferState state) {
                                        S3FileUploadService.this.logger.d("State changed : " + state);
                                        Request currentRequest = S3FileUploadService.this.getCurrentRequest();
                                        boolean shouldPutBack = true;
                                        boolean shouldReschedule = true;
                                        switch (AnonymousClass2.$SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[state.ordinal()]) {
                                            case 1:
                                                S3FileUploadService.this.logger.d("Upload completed");
                                                shouldPutBack = false;
                                                if (currentRequest != null) {
                                                    IOUtils.deleteFile(HudApplication.getAppContext(), currentRequest.file.getAbsolutePath());
                                                    isUploading.set(false);
                                                    S3FileUploadService.this.uploadFinished(true, file.getPath(), currentRequest.userTag);
                                                    S3FileUploadService.this.setCurrentRequest(null);
                                                }
                                                S3FileUploadService.this.sync();
                                                break;
                                            case 2:
                                                S3FileUploadService.this.logger.d("Upload canceled");
                                                break;
                                            case 3:
                                                S3FileUploadService.this.logger.d("Waiting for network");
                                                S3FileUploadService.this.transferUtility.cancel(id);
                                                shouldReschedule = false;
                                                break;
                                            case 4:
                                                S3FileUploadService.this.logger.d("In progress " + observer.getBytesTotal() + " bytes");
                                                shouldPutBack = false;
                                                break;
                                            case 5:
                                                S3FileUploadService.this.logger.d("Failed");
                                                break;
                                        }
                                        if (shouldPutBack) {
                                            UploadQueue gestureUploadQueue = S3FileUploadService.this.getUploadQueue();
                                            synchronized (gestureUploadQueue) {
                                                if (currentRequest != null) {
                                                    gestureUploadQueue.add(currentRequest);
                                                    S3FileUploadService.this.setCurrentRequest(null);
                                                } else {
                                                    S3FileUploadService.this.logger.d("request object in upload callback is null, network anomaly?");
                                                }
                                            }
                                            isUploading.compareAndSet(true, false);
                                            if (shouldReschedule) {
                                                S3FileUploadService.this.reSchedule();
                                            }
                                        }
                                    }

                                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                                    }

                                    public void onError(int id, Exception ex) {
                                        S3FileUploadService.this.logger.e("onError , while uploading, id " + id, ex);
                                        Request currentRequest = S3FileUploadService.this.getCurrentRequest();
                                        UploadQueue gestureUploadQueue = S3FileUploadService.this.getUploadQueue();
                                        synchronized (gestureUploadQueue) {
                                            if (currentRequest != null) {
                                                gestureUploadQueue.add(currentRequest);
                                                S3FileUploadService.this.setCurrentRequest(null);
                                            }
                                        }
                                        isUploading.compareAndSet(true, false);
                                        S3FileUploadService.this.reSchedule();
                                    }
                                });
                            } catch (IllegalArgumentException e) {
                                e.printStackTrace();
                                isUploading.set(false);
                                uploadFinished(false, file.getPath(), currentRequest.userTag);
                                sync();
                                return;
                            }
                        }
                    }
                    this.logger.d("Nothing to upload");
                }
            }
        }
    }
}
