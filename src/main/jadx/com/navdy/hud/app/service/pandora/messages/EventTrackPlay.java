package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException;
import java.io.IOException;

public class EventTrackPlay extends BaseOutgoingEmptyMessage {
    public static final EventTrackPlay INSTANCE = new EventTrackPlay();

    public /* bridge */ /* synthetic */ byte[] buildPayload() throws IOException, StringOverflowException, UnexpectedEndOfStringException, MessageWrongWayException {
        return super.buildPayload();
    }

    private EventTrackPlay() {
    }

    protected byte getMessageType() {
        return BaseMessage.PNDR_EVENT_TRACK_PLAY;
    }

    public String toString() {
        return "Play music action";
    }
}
