package com.navdy.hud.app.service.pandora;

import android.bluetooth.BluetoothSocket;
import android.support.v4.media.TransportMediator;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class ReadMessagesThread implements Runnable {
    private static final Logger sLogger = PandoraManager.sLogger;
    private PandoraManager parentManager;

    public ReadMessagesThread(PandoraManager parentManager) {
        this.parentManager = parentManager;
    }

    public void run() {
        Throwable e;
        Throwable th;
        InputStream is = null;
        ByteArrayOutputStream buffer = null;
        BluetoothSocket socket = this.parentManager.getSocket();
        is = socket.getInputStream();
        int b = is.read();
        ByteArrayOutputStream buffer2 = null;
        while (b >= 0 && socket != null) {
            if (buffer2 != null) {
                try {
                    buffer2.write(b);
                    if (b == 124) {
                        this.parentManager.onFrameReceived(buffer2.toByteArray());
                        buffer = null;
                    }
                    buffer = buffer2;
                } catch (Throwable th2) {
                    th = th2;
                    buffer = buffer2;
                }
            } else if (b == TransportMediator.KEYCODE_MEDIA_PLAY) {
                buffer = new ByteArrayOutputStream();
                try {
                    buffer.write(b);
                } catch (Throwable th3) {
                    e = th3;
                }
            } else {
                sLogger.e("Unexpected byte received - skipping: " + String.format("%02X", new Object[]{Integer.valueOf(b)}));
                buffer = buffer2;
            }
            b = is.read();
            buffer2 = buffer;
        }
        sLogger.w("End of input read from the stream");
        IOUtils.closeStream(buffer2);
        IOUtils.closeStream(is);
        this.parentManager.terminateAndClose();
        buffer = buffer2;
        sLogger.v("exiting thread:" + Thread.currentThread().getName());
        try {
            sLogger.e(Thread.currentThread().getName(), e);
            IOUtils.closeStream(buffer);
            IOUtils.closeStream(is);
            this.parentManager.terminateAndClose();
            sLogger.v("exiting thread:" + Thread.currentThread().getName());
        } catch (Throwable th4) {
            th = th4;
            IOUtils.closeStream(buffer);
            IOUtils.closeStream(is);
            this.parentManager.terminateAndClose();
            throw th;
        }
    }
}
