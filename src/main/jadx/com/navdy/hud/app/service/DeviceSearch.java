package com.navdy.hud.app.service;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.device.RemoteDevice.Listener;
import com.navdy.service.library.device.RemoteDeviceRegistry;
import com.navdy.service.library.device.connection.Connection.ConnectionFailureCause;
import com.navdy.service.library.device.connection.Connection.DisconnectCause;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.connection.ConnectionStatus;
import com.navdy.service.library.events.connection.ConnectionStatus.Status;
import com.navdy.service.library.log.Logger;
import com.squareup.wire.Message;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;

class DeviceSearch implements Listener {
    private static final int SEARCH_TIMEOUT = 15000;
    private Deque<RemoteDevice> connectedDevices = new ArrayDeque();
    private Deque<RemoteDevice> connectingDevices = new ArrayDeque();
    private EventSink eventSink;
    private final Handler handler = new Handler();
    protected final Logger logger = new Logger(getClass());
    private Deque<RemoteDevice> remoteDevices = new ArrayDeque();
    private long startTime = SystemClock.elapsedRealtime();
    private boolean started = false;

    public interface EventSink {
        void onEvent(Message message);
    }

    public DeviceSearch(Context context, EventSink eventSink, boolean parseEvents) {
        this.eventSink = eventSink;
        List<ConnectionInfo> infos = RemoteDeviceRegistry.getInstance(context).getPairedConnections();
        for (int i = 0; i < infos.size(); i++) {
            RemoteDevice device = new RemoteDevice(context, (ConnectionInfo) infos.get(i), parseEvents);
            device.addListener(this);
            this.remoteDevices.add(device);
        }
    }

    public void start() {
        if (!this.started) {
            this.started = true;
            sendEvent(new ConnectionStatus(Status.CONNECTION_SEARCH_STARTED, null));
        }
    }

    public void stop() {
        if (this.started) {
            this.started = false;
            sendEvent(new ConnectionStatus(Status.CONNECTION_SEARCH_FINISHED, null));
        }
    }

    public synchronized boolean next() {
        boolean z = false;
        synchronized (this) {
            if (SystemClock.elapsedRealtime() - this.startTime > 15000) {
                stop();
            } else if (this.remoteDevices.size() > 0) {
                start();
                RemoteDevice device = (RemoteDevice) this.remoteDevices.removeFirst();
                if (device.connect()) {
                    this.connectingDevices.add(device);
                } else {
                    this.remoteDevices.addLast(device);
                }
                z = true;
            } else if (this.connectingDevices.size() > 0) {
                z = true;
            } else {
                stop();
            }
        }
        return z;
    }

    public synchronized RemoteDevice select(NavdyDeviceId deviceId) {
        RemoteDevice missingDevice;
        String bluetoothAddress = deviceId.getBluetoothAddress();
        missingDevice = findDevice(bluetoothAddress, this.remoteDevices);
        RemoteDevice connectingDevice = findDevice(bluetoothAddress, this.connectingDevices);
        RemoteDevice connectedDevice = findDevice(bluetoothAddress, this.connectedDevices);
        if (missingDevice != null) {
            this.remoteDevices.remove(missingDevice);
            missingDevice.removeListener(this);
        } else if (connectingDevice != null) {
            this.connectingDevices.remove(connectingDevice);
            connectingDevice.removeListener(this);
            missingDevice = connectingDevice;
        } else if (connectedDevice != null) {
            this.connectedDevices.remove(connectedDevice);
            connectedDevice.removeListener(this);
            missingDevice = connectedDevice;
        } else {
            missingDevice = null;
        }
        return missingDevice;
    }

    public synchronized void close() {
        stop();
        closeAll(this.connectingDevices);
        closeAll(this.connectedDevices);
    }

    private void closeAll(Deque<RemoteDevice> queue) {
        for (RemoteDevice device : queue) {
            device.removeListener(this);
            queueDisconnect(device);
        }
    }

    private RemoteDevice findDevice(String bluetoothAddress, Deque<RemoteDevice> queue) {
        for (RemoteDevice remoteDevice : queue) {
            if (remoteDevice.getDeviceId().getBluetoothAddress().equals(bluetoothAddress)) {
                return remoteDevice;
            }
        }
        return null;
    }

    private RemoteDevice findDevice(BluetoothDevice device, Deque<RemoteDevice> queue) {
        return findDevice(device.getAddress(), (Deque) queue);
    }

    private void queueDisconnect(final RemoteDevice remoteDevice) {
        if (remoteDevice != null) {
            this.handler.post(new Runnable() {
                public void run() {
                    remoteDevice.disconnect();
                }
            });
        }
    }

    public synchronized void handleDisconnect(BluetoothDevice device) {
        queueDisconnect(findDevice(device, this.connectedDevices));
    }

    public synchronized boolean forgetDevice(BluetoothDevice device) {
        boolean z = true;
        synchronized (this) {
            this.logger.i("DeviceSearch forgetting:" + device);
            RemoteDevice remoteDevice = findDevice(device, this.connectingDevices);
            if (remoteDevice != null) {
                this.logger.i("DeviceSearch removing device from connecting devices");
                this.connectingDevices.remove(remoteDevice);
                queueDisconnect(remoteDevice);
            } else {
                remoteDevice = findDevice(device, this.remoteDevices);
                if (remoteDevice != null) {
                    this.logger.i("DeviceSearch removing device from remote devices");
                    this.remoteDevices.remove(remoteDevice);
                } else {
                    z = false;
                }
            }
        }
        return z;
    }

    public void onDeviceConnecting(RemoteDevice device) {
    }

    public synchronized void onDeviceConnected(RemoteDevice device) {
        this.logger.i("DeviceSearch connected:" + device.getDeviceId());
        this.connectingDevices.remove(device);
        this.connectedDevices.add(device);
        sendEvent(new ConnectionStatus(Status.CONNECTION_FOUND, device.getDeviceId().toString()));
    }

    public synchronized void onDeviceConnectFailure(RemoteDevice device, ConnectionFailureCause cause) {
        this.logger.i("DeviceSearch connect failure:" + device.getDeviceId());
        if (this.connectingDevices.remove(device)) {
            this.remoteDevices.addLast(device);
        }
    }

    public synchronized void onDeviceDisconnected(RemoteDevice device, DisconnectCause cause) {
        this.logger.i("DeviceSearch disconnected:" + device.getDeviceId());
        this.connectedDevices.remove(device);
        this.remoteDevices.addLast(device);
        sendEvent(new ConnectionStatus(Status.CONNECTION_LOST, device.getDeviceId().toString()));
    }

    public void onNavdyEventReceived(RemoteDevice device, byte[] event) {
    }

    public void onNavdyEventReceived(RemoteDevice device, NavdyEvent event) {
    }

    private void sendEvent(final Message message) {
        if (this.eventSink != null) {
            this.handler.post(new Runnable() {
                public void run() {
                    DeviceSearch.this.eventSink.onEvent(message);
                }
            });
        }
    }
}
