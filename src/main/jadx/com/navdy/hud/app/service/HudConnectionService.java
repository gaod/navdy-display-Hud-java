package com.navdy.hud.app.service;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.SystemClock;
import android.text.TextUtils;
import com.navdy.hud.app.debug.RouteRecorder;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.device.gps.GpsManager;
import com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.IAPListenerReceiver;
import com.navdy.hud.app.service.DeviceSearch.EventSink;
import com.navdy.hud.app.util.BluetoothUtil;
import com.navdy.hud.app.util.CrashReportService;
import com.navdy.hud.app.util.CrashReportService.CrashType;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.hud.device.connection.EASessionSocketAdapter;
import com.navdy.hud.device.connection.iAP2Link;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.device.connection.AcceptorListener;
import com.navdy.service.library.device.connection.Connection.DisconnectCause;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.device.connection.ConnectionListener;
import com.navdy.service.library.device.connection.ConnectionService;
import com.navdy.service.library.device.connection.ConnectionService.State;
import com.navdy.service.library.device.connection.ConnectionType;
import com.navdy.service.library.device.connection.TCPConnectionInfo;
import com.navdy.service.library.device.connection.tunnel.Tunnel;
import com.navdy.service.library.device.discovery.BTDeviceBroadcaster;
import com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster;
import com.navdy.service.library.device.discovery.TCPRemoteDeviceBroadcaster;
import com.navdy.service.library.device.link.Link;
import com.navdy.service.library.device.link.LinkManager;
import com.navdy.service.library.device.link.LinkManager.LinkFactory;
import com.navdy.service.library.events.DeviceInfo.Platform;
import com.navdy.service.library.events.Ext_NavdyEvent;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.NavdyEvent.MessageType;
import com.navdy.service.library.events.NavdyEventUtil;
import com.navdy.service.library.events.audio.AudioStatus;
import com.navdy.service.library.events.audio.AudioStatus.ProfileType;
import com.navdy.service.library.events.connection.ConnectionRequest;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.connection.ConnectionStatus;
import com.navdy.service.library.events.connection.ConnectionStatus.Status;
import com.navdy.service.library.events.debug.StartDrivePlaybackEvent;
import com.navdy.service.library.events.debug.StartDriveRecordingEvent;
import com.navdy.service.library.events.file.FileTransferData;
import com.navdy.service.library.events.file.FileTransferRequest;
import com.navdy.service.library.events.file.FileTransferStatus;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.network.BTSocketAcceptor;
import com.navdy.service.library.network.BTSocketFactory;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.network.SocketFactory;
import com.navdy.service.library.network.TCPSocketAcceptor;
import com.navdy.service.library.util.NetworkActivityTracker;
import com.squareup.wire.Message;
import com.squareup.wire.Wire;
import java.io.IOException;

public class HudConnectionService extends ConnectionService {
    public static final String ACTION_DEVICE_FORCE_RECONNECT = "com.navdy.hud.app.force_reconnect";
    private static final String APP_TERMINATION_RECONNECT_DELAY_MS = "persist.sys.app_reconnect_ms";
    private static final String AUTO_DRIVERECORD_PROP = "persist.sys.driverecord.auto";
    public static final String EXTRA_HUD_FORCE_RECONNECT_REASON = "force_reconnect_reason";
    private static final int PROXY_ENTRY_LOCAL_PORT = 3000;
    public static final String REASON_CONNECTION_DISCONNECTED = "CONNECTION_DISCONNECTED";
    private BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
        private BluetoothDevice cancelledDevice;
        private long cancelledTime;

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            if (action.equals("android.bluetooth.device.action.BOND_STATE_CHANGED")) {
                int bondState = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", Integer.MIN_VALUE);
                HudConnectionService.this.logger.d("Bond state change - device:" + device + " state:" + bondState);
                if (bondState != 11) {
                    return;
                }
                if ((HudConnectionService.this.search != null && HudConnectionService.this.search.forgetDevice(device)) || (HudConnectionService.this.mRemoteDevice != null && HudConnectionService.this.mRemoteDevice.getDeviceId().getBluetoothAddress().equals(device.getAddress()))) {
                    HudConnectionService.this.logger.i("Unexpected bonding with known device - removing bond");
                    HudConnectionService.this.forgetPairedDevice(device);
                    BluetoothUtil.cancelBondProcess(device);
                    BluetoothUtil.removeBond(device);
                    this.cancelledDevice = device;
                    this.cancelledTime = SystemClock.elapsedRealtime();
                    HudConnectionService.this.serviceHandler.removeCallbacks(HudConnectionService.this.reconnectRunnable);
                    HudConnectionService.this.setActiveDevice(null);
                }
            } else if (action.equals("android.bluetooth.device.action.PAIRING_REQUEST")) {
                if (device.equals(this.cancelledDevice) && SystemClock.elapsedRealtime() - this.cancelledTime < 250) {
                    HudConnectionService.this.logger.d("Aborting pairing request for cancelled bonding");
                    abortBroadcast();
                }
                this.cancelledDevice = null;
            } else if (action.equals("android.bluetooth.device.action.ACL_DISCONNECTED") && HudConnectionService.this.search != null) {
                HudConnectionService.this.search.handleDisconnect(device);
            }
        }
    };
    private BroadcastReceiver debugReceiver;
    private ReconnectRunnable deviceReconnectRunnable = new ReconnectRunnable();
    private FileTransferHandler fileTransferHandler;
    private final GpsManager gpsManager = GpsManager.getInstance();
    private LinkFactory iAPLinkFactory = new LinkFactory() {
        public Link build(ConnectionInfo connectionInfo) {
            return new iAP2Link(new IAPListenerReceiver(), HudConnectionService.this.getApplicationContext());
        }
    };
    private boolean isSimulatingGpsCoordinates;
    private boolean needAutoSearch = true;
    private final RouteRecorder routeRecorder = RouteRecorder.getInstance();
    private DeviceSearch search;

    public class ReconnectRunnable implements Runnable {
        private String reason;

        public void run() {
            HudConnectionService.this.reconnect(this.reason);
        }

        public void setReason(String reason) {
            this.reason = reason;
        }
    }

    public void onCreate() {
        super.onCreate();
        LinkManager.registerFactory(ConnectionType.BT_IAP2_LINK, this.iAPLinkFactory);
        LinkManager.registerFactory(ConnectionType.EA_PROTOBUF, this.iAPLinkFactory);
        this.gpsManager.setConnectionService(this);
        this.routeRecorder.setConnectionService(this);
        this.serverMode = true;
        this.inProcess = false;
        NetworkActivityTracker.getInstance().start();
        this.fileTransferHandler = new FileTransferHandler(getApplicationContext());
        if (!DeviceUtil.isUserBuild()) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ConnectionService.ACTION_LINK_BANDWIDTH_LEVEL_CHANGED);
            intentFilter.addCategory(ConnectionService.CATEGORY_NAVDY_LINK);
            this.debugReceiver = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    HudConnectionService.this.setBandwidthLevel(intent.getIntExtra(ConnectionService.EXTRA_BANDWIDTH_LEVEL, 1));
                }
            };
            registerReceiver(this.debugReceiver, intentFilter);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.routeRecorder.stopRecording(false);
        this.gpsManager.shutdown();
        unregisterReceiver(this.bluetoothReceiver);
        if (this.debugReceiver != null) {
            unregisterReceiver(this.debugReceiver);
        }
    }

    public boolean isPromiscuous() {
        return PowerManager.isAwake();
    }

    protected RemoteDeviceBroadcaster[] getRemoteDeviceBroadcasters() {
        if (DeviceUtil.isNavdyDevice()) {
            return new RemoteDeviceBroadcaster[]{new BTDeviceBroadcaster()};
        }
        return new RemoteDeviceBroadcaster[]{new BTDeviceBroadcaster(), new TCPRemoteDeviceBroadcaster(getApplicationContext())};
    }

    protected ConnectionListener[] getConnectionListeners(Context context) {
        if (DeviceUtil.isNavdyDevice()) {
            return new ConnectionListener[]{new AcceptorListener(context, new BTSocketAcceptor("Navdy", NAVDY_PROTO_SERVICE_UUID), ConnectionType.BT_PROTOBUF), new AcceptorListener(context, new BTSocketAcceptor(ConnectionService.NAVDY_IAP_NAME, ACCESSORY_IAP2), ConnectionType.BT_IAP2_LINK)};
        }
        return new ConnectionListener[]{new AcceptorListener(context, new TCPSocketAcceptor(TCPConnectionInfo.SERVICE_PORT), ConnectionType.TCP_PROTOBUF), new AcceptorListener(context, new BTSocketAcceptor("Navdy", NAVDY_PROTO_SERVICE_UUID), ConnectionType.BT_PROTOBUF)};
    }

    protected void exitState(State state) {
        super.exitState(state);
        switch (state) {
            case START:
                IntentFilter filter = new IntentFilter("android.bluetooth.device.action.PAIRING_REQUEST");
                filter.setPriority(1);
                filter.addAction("android.bluetooth.device.action.ACL_DISCONNECTED");
                filter.addAction("android.bluetooth.device.action.BOND_STATE_CHANGED");
                registerReceiver(this.bluetoothReceiver, filter);
                return;
            default:
                return;
        }
    }

    protected void enterState(State state) {
        switch (state) {
            case IDLE:
                if (this.search != null) {
                    setState(State.SEARCHING);
                    break;
                }
                break;
            case SEARCHING:
                if (isPromiscuous()) {
                    startBroadcasters();
                }
                startListeners();
                if (this.search == null) {
                    this.search = new DeviceSearch(this, new EventSink() {
                        public void onEvent(Message message) {
                            if ((message instanceof ConnectionStatus) && ((ConnectionStatus) message).status == Status.CONNECTION_SEARCH_FINISHED && HudConnectionService.this.state == State.SEARCHING) {
                                if (HudConnectionService.this.search != null) {
                                    HudConnectionService.this.search.close();
                                    HudConnectionService.this.search = null;
                                }
                                HudConnectionService.this.setState(State.IDLE);
                            }
                            HudConnectionService.this.forwardEventLocally(message);
                        }
                    }, this.inProcess);
                    this.search.next();
                }
                if (this.mRemoteDevice != null && (this.mRemoteDevice.isConnected() || this.mRemoteDevice.isConnecting())) {
                    setState(State.DISCONNECTING);
                    break;
                }
            case CONNECTED:
                this.needAutoSearch = false;
                if (this.search != null) {
                    this.search.close();
                    this.search = null;
                }
                if (this.mRemoteDevice != null && this.deviceRegistry.findDevice(this.mRemoteDevice.getDeviceId()) == null) {
                    ConnectionServiceAnalyticsSupport.recordNewDevice();
                    break;
                }
            case CONNECTING:
            case RECONNECTING:
                this.needAutoSearch = false;
                break;
        }
        super.enterState(state);
    }

    public boolean needAutoSearch() {
        return this.needAutoSearch && PowerManager.isAwake();
    }

    protected void heartBeat() {
        switch (this.state) {
            case SEARCHING:
                this.search.next();
                break;
        }
        super.heartBeat();
    }

    protected void forgetPairedDevice(BluetoothDevice device) {
        if (this.search != null) {
            this.search.forgetDevice(device);
        }
        super.forgetPairedDevice(device);
    }

    public void onDeviceConnected(RemoteDevice device) {
        super.onDeviceConnected(device);
        this.fileTransferHandler.onDeviceConnected(device);
        startProxyService();
    }

    protected void handleDeviceDisconnect(RemoteDevice device, DisconnectCause cause) {
        super.handleDeviceDisconnect(device, cause);
        this.logger.d("HUDConnectionService: handleDeviceDisconnect " + device + ", Cause :" + cause);
        if (this.mRemoteDevice == device && cause == DisconnectCause.ABORTED && this.serverMode) {
            this.logger.d("HUDConnectionService: Reporting the non fatal crash, Bluetooth disconnected");
            CrashReportService.dumpCrashReportAsync(CrashType.BLUETOOTH_DISCONNECTED);
        }
    }

    public void onDeviceDisconnected(RemoteDevice device, DisconnectCause cause) {
        super.onDeviceDisconnected(device, cause);
        this.fileTransferHandler.onDeviceDisconnected();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected boolean processEvent(byte[] eventData, MessageType messageType) {
        switch (messageType) {
            case Coordinate:
                try {
                    if (this.isSimulatingGpsCoordinates) {
                        return true;
                    }
                    this.gpsManager.feedLocation((Coordinate) NavdyEventUtil.messageFromEvent((NavdyEvent) this.mWire.parseFrom(eventData, NavdyEvent.class)));
                    return true;
                } catch (Throwable t) {
                    this.logger.e(t);
                    return true;
                }
            case StartDriveRecordingEvent:
                return parseStartDriveRecordingEvent(eventData);
            case StopDriveRecordingEvent:
                this.routeRecorder.stopRecording(false);
                this.logger.v("onStopDriveRecording");
                return true;
            case DriveRecordingsRequest:
                this.routeRecorder.requestRecordings();
                this.logger.v("onDriveRecordingsRequest");
                return true;
            case StartDrivePlaybackEvent:
                parseStartDrivePlaybackEvent(eventData);
                return false;
            case StopDrivePlaybackEvent:
                this.routeRecorder.stopPlayback();
                this.logger.v("onStopDrivePlayback");
                return false;
            case FileTransferRequest:
                try {
                    this.fileTransferHandler.onFileTransferRequest((FileTransferRequest) NavdyEventUtil.messageFromEvent((NavdyEvent) this.mWire.parseFrom(eventData, NavdyEvent.class)));
                    return true;
                } catch (Throwable t2) {
                    this.logger.e(t2);
                    return true;
                }
            case FileTransferStatus:
                try {
                    this.fileTransferHandler.onFileTransferStatus((FileTransferStatus) NavdyEventUtil.messageFromEvent((NavdyEvent) this.mWire.parseFrom(eventData, NavdyEvent.class)));
                    return true;
                } catch (Throwable t22) {
                    this.logger.e(t22);
                    return true;
                }
            case FileTransferData:
                try {
                    this.fileTransferHandler.onFileTransferData((FileTransferData) NavdyEventUtil.messageFromEvent((NavdyEvent) this.mWire.parseFrom(eventData, NavdyEvent.class)));
                    return true;
                } catch (Throwable t222) {
                    this.logger.e(t222);
                    return true;
                }
            case ConnectionStateChange:
                try {
                    switch (((ConnectionStateChange) NavdyEventUtil.messageFromEvent((NavdyEvent) this.mWire.parseFrom(eventData, NavdyEvent.class))).state) {
                        case CONNECTION_DISCONNECTED:
                            int delay = SystemProperties.getInt(APP_TERMINATION_RECONNECT_DELAY_MS, 1000);
                            this.deviceReconnectRunnable.setReason(REASON_CONNECTION_DISCONNECTED);
                            this.serviceHandler.postDelayed(this.deviceReconnectRunnable, (long) delay);
                            break;
                        case CONNECTION_CONNECTED:
                        case CONNECTION_LINK_LOST:
                            this.serviceHandler.removeCallbacks(this.deviceReconnectRunnable);
                            break;
                    }
                } catch (Throwable t2222) {
                    this.logger.e(t2222);
                }
                return false;
            case AudioStatus:
                try {
                    AudioStatus message = (AudioStatus) NavdyEventUtil.messageFromEvent((NavdyEvent) this.mWire.parseFrom(eventData, NavdyEvent.class));
                    if (message != null && message.profileType == ProfileType.AUDIO_PROFILE_HFP) {
                        this.logger.d("AudioStatus for HFP received");
                        if (!((Boolean) Wire.get(message.isConnected, Boolean.valueOf(false))).booleanValue()) {
                            this.logger.d("AudioStatus: HFP disconnected");
                            setBandwidthLevel(1);
                            break;
                        }
                        this.logger.d("AudioStatus: HFP connected");
                        if (!((Boolean) Wire.get(message.hasSCOConnection, Boolean.valueOf(false))).booleanValue()) {
                            this.logger.d("AudioStatus: does not have SCO connection");
                            setBandwidthLevel(1);
                            break;
                        }
                        this.logger.d("AudioStatus: has SCO connection");
                        setBandwidthLevel(0);
                        break;
                    }
                } catch (Throwable t22222) {
                    this.logger.e(t22222);
                    break;
                }
        }
        return false;
    }

    protected boolean processLocalEvent(byte[] eventData, MessageType messageType) {
        if (messageType == MessageType.ConnectionRequest) {
            ConnectionRequest request;
            try {
                request = (ConnectionRequest) ((NavdyEvent) this.mWire.parseFrom(eventData, NavdyEvent.class)).getExtension(Ext_NavdyEvent.connectionRequest);
            } catch (Throwable t) {
                this.logger.e(t);
            }
            if (request == null) {
                return true;
            }
            switch (request.action) {
                case CONNECTION_SELECT:
                    if (request.remoteDeviceId != null) {
                        selectDevice(new NavdyDeviceId(request.remoteDeviceId));
                        return true;
                    }
                    setActiveDevice(null);
                    return true;
                case CONNECTION_START_SEARCH:
                    setState(State.SEARCHING);
                    return true;
                case CONNECTION_STOP_SEARCH:
                    this.serviceHandler.post(new Runnable() {
                        public void run() {
                            HudConnectionService.this.logger.i("Ending search");
                            if (HudConnectionService.this.search != null) {
                                HudConnectionService.this.search.close();
                                HudConnectionService.this.search = null;
                            }
                            HudConnectionService.this.setState(State.IDLE);
                        }
                    });
                    return true;
                default:
                    return true;
            }
            this.logger.e(t);
            return false;
        } else if (messageType == MessageType.StartDrivePlaybackEvent) {
            parseStartDrivePlaybackEvent(eventData);
            forwardEventLocally(eventData);
            return true;
        } else if (messageType == MessageType.StopDrivePlaybackEvent) {
            this.routeRecorder.stopPlayback();
            forwardEventLocally(eventData);
            this.logger.v("onStopDrivePlayback");
            return true;
        } else if (messageType == MessageType.StartDriveRecordingEvent) {
            return parseStartDriveRecordingEvent(eventData);
        } else {
            if (messageType == MessageType.StopDriveRecordingEvent) {
                this.routeRecorder.stopRecording(false);
                forwardEventLocally(eventData);
            }
            return false;
        }
    }

    private boolean parseStartDrivePlaybackEvent(byte[] eventData) {
        boolean z = false;
        try {
            StartDrivePlaybackEvent startDrivePlaybackEvent = (StartDrivePlaybackEvent) NavdyEventUtil.messageFromEvent((NavdyEvent) this.mWire.parseFrom(eventData, NavdyEvent.class));
            RouteRecorder routeRecorder = this.routeRecorder;
            String str = startDrivePlaybackEvent.label;
            if (startDrivePlaybackEvent.playSecondaryLocation != null) {
                z = startDrivePlaybackEvent.playSecondaryLocation.booleanValue();
            }
            routeRecorder.startPlayback(str, z, false);
            this.logger.v("onStartDrivePlayback, name=" + startDrivePlaybackEvent.label);
        } catch (Throwable t) {
            this.logger.e(t);
        }
        return true;
    }

    private boolean parseStartDriveRecordingEvent(byte[] eventData) {
        try {
            StartDriveRecordingEvent startDriveRecordingEvent = (StartDriveRecordingEvent) NavdyEventUtil.messageFromEvent((NavdyEvent) this.mWire.parseFrom(eventData, NavdyEvent.class));
            if (startDriveRecordingEvent != null) {
                String fileName = this.routeRecorder.startRecording(startDriveRecordingEvent.label, false);
                if (TextUtils.isEmpty(fileName)) {
                    this.logger.e("startDriveRecording event fileName is empty");
                } else {
                    forwardEventLocally((Message) new StartDriveRecordingEvent(fileName));
                    this.logger.v("onStartDriveRecording, name=" + startDriveRecordingEvent.label);
                }
            } else {
                this.logger.e("startDriveRecording event is null");
            }
        } catch (Throwable t) {
            this.logger.e(t);
        }
        return true;
    }

    private void selectDevice(NavdyDeviceId deviceId) {
        if (this.search != null) {
            this.logger.i("Connecting to " + deviceId);
            RemoteDevice device = this.search.select(deviceId);
            if (device != null) {
                this.logger.i("Found corresponding remote device:" + device);
                setActiveDevice(device);
                return;
            }
            return;
        }
        ConnectionInfo info = this.deviceRegistry.findDevice(deviceId);
        if (info != null) {
            this.logger.i("Found corresponding connection info:" + info);
            connect(info);
        }
    }

    protected Tunnel createProxyService() throws IOException {
        return new Tunnel(new TCPSocketAcceptor(3000, true), new SocketFactory() {
            public SocketAdapter build() throws IOException {
                if (iAP2Link.proxyEASession != null) {
                    return new EASessionSocketAdapter(iAP2Link.proxyEASession);
                }
                if (HudConnectionService.this.mRemoteDevice != null) {
                    return new BTSocketFactory(HudConnectionService.this.mRemoteDevice.getDeviceId().getBluetoothAddress(), ConnectionService.NAVDY_PROXY_TUNNEL_UUID).build();
                }
                throw new IOException("can't create proxy tunnel because HUD is not connected to remote device");
            }
        });
    }

    public void setSimulatingGpsCoordinates(boolean isSimulatingGpsCoordinates) {
        this.isSimulatingGpsCoordinates = isSimulatingGpsCoordinates;
    }

    protected void sendEventsOnLocalConnect() {
        this.logger.v("sendEventsOnLocalConnect");
        RouteRecorder routeRecorder = RouteRecorder.getInstance();
        if (routeRecorder.isRecording()) {
            String label = routeRecorder.getLabel();
            if (label != null) {
                forwardEventLocally((Message) new StartDriveRecordingEvent(label));
                this.logger.v("sendEventsOnLocalConnect: send recording event:" + label);
                return;
            }
            this.logger.v("sendEventsOnLocalConnect: invalid label");
        }
    }

    public synchronized void reconnect(String reason) {
        broadcastReconnectingIntent(reason);
        super.reconnect(reason);
    }

    public Platform getDevicePlatform() {
        try {
            if (this.mRemoteDevice == null || !this.mRemoteDevice.isConnected()) {
                return null;
            }
            return this.mRemoteDevice.getDeviceInfo().platform;
        } catch (Throwable th) {
            return null;
        }
    }

    public void broadcastReconnectingIntent(String reason) {
        Intent intent = new Intent(ACTION_DEVICE_FORCE_RECONNECT);
        intent.putExtra(EXTRA_HUD_FORCE_RECONNECT_REASON, reason);
        sendBroadcast(intent);
    }

    public boolean reconnectAfterDeadConnection() {
        return true;
    }
}
