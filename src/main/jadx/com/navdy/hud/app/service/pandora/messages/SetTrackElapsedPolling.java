package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class SetTrackElapsedPolling extends BaseOutgoingConstantMessage {
    public static final SetTrackElapsedPolling DISABLED = new SetTrackElapsedPolling(false);
    public static final SetTrackElapsedPolling ENABLED = new SetTrackElapsedPolling(true);
    public boolean isOn;

    public /* bridge */ /* synthetic */ byte[] buildPayload() throws IOException, StringOverflowException, UnexpectedEndOfStringException, MessageWrongWayException {
        return super.buildPayload();
    }

    private SetTrackElapsedPolling(boolean isOn) {
        this.isOn = isOn;
    }

    protected ByteArrayOutputStream putThis(ByteArrayOutputStream os) throws IOException, StringOverflowException, UnexpectedEndOfStringException, MessageWrongWayException {
        BaseOutgoingMessage.putByte(os, BaseMessage.PNDR_SET_TRACK_ELAPSED_POLLING);
        BaseOutgoingMessage.putBoolean(os, this.isOn);
        return os;
    }

    public String toString() {
        return "Setting track elapsed polling to: " + this.isOn;
    }
}
