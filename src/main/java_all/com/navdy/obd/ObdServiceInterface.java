package com.navdy.obd;

import android.content.Intent;
import android.content.Context;
import android.content.ComponentName;

public class ObdServiceInterface
{
    public static final String ACTION_RESCAN = "com.navdy.obd.action.RESCAN";
    public static final String ACTION_START_AUTO_CONNECT = "com.navdy.obd.action.START_AUTO_CONNECT";
    public static final String ACTION_STOP_AUTO_CONNECT = "com.navdy.obd.action.STOP_AUTO_CONNECT";
    public static final String CAN_BUS_MONITOR_ERROR_MESSAGE_BAD_STATE = "BAD_STATE";
    public static final String GENERIC_CAN_BUS_MONITOR_ERROR_MESSAGE = "NO_DATA";
    public static final int MODE_J1939 = 1;
    public static final int MODE_OBD2 = 0;
    public static final String OBD_CLASS_NAME = "com.navdy.obd.ObdService";
    public static final String OBD_PACKAGE_NAME = "com.navdy.obd.app.ObdTestApp";
    
    public static ComponentName getObdServiceComponent() {
        return new ComponentName("com.navdy.obd.app.ObdTestApp", "com.navdy.obd.ObdService");
    }
    
    public static void startObdService(final Context context) {
        final Intent intent = new Intent();
        intent.setComponent(getObdServiceComponent());
        intent.setAction("com.navdy.obd.action.START_AUTO_CONNECT");
        context.startService(intent);
    }
    
    public static void stopObdService(final Context context) {
        final Intent intent = new Intent();
        intent.setComponent(getObdServiceComponent());
        intent.setAction("com.navdy.obd.action.STOP_AUTO_CONNECT");
        context.startService(intent);
    }
}
