package com.navdy.obd;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public class ECU implements Parcelable
{
    public static final Parcelable.Creator<ECU> CREATOR;
    public final int address;
    public final PidSet supportedPids;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<ECU>() {
            public ECU createFromParcel(final Parcel parcel) {
                return new ECU(parcel);
            }
            
            public ECU[] newArray(final int n) {
                return new ECU[n];
            }
        };
    }
    
    public ECU(final int address, final PidSet supportedPids) {
        this.address = address;
        this.supportedPids = supportedPids;
    }
    
    public ECU(final Parcel parcel) {
        this.address = parcel.readInt();
        this.supportedPids = (PidSet)parcel.readParcelable(PidSet.class.getClassLoader());
    }
    
    public int describeContents() {
        return 0;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeInt(this.address);
        parcel.writeParcelable((Parcelable)this.supportedPids, 0);
    }
}
