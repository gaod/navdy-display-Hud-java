package com.navdy.service.library.device.connection;

public class ServiceAddress
{
    private final String address;
    private final String protocol;
    private final String service;
    
    public ServiceAddress(final String address, final String service) {
        this.address = address;
        this.service = service;
        this.protocol = "NA";
    }
    
    public ServiceAddress(final String address, final String service, final String protocol) {
        this.address = address;
        this.service = service;
        this.protocol = protocol;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = false;
        boolean equals;
        if (this == o) {
            equals = true;
        }
        else {
            equals = b;
            if (o != null) {
                equals = b;
                if (this.getClass() == o.getClass()) {
                    final ServiceAddress serviceAddress = (ServiceAddress)o;
                    equals = b;
                    if (this.address.equals(serviceAddress.address)) {
                        equals = b;
                        if (this.service.equals(serviceAddress.service)) {
                            equals = this.protocol.equals(serviceAddress.protocol);
                        }
                    }
                }
            }
        }
        return equals;
    }
    
    public String getAddress() {
        return this.address;
    }
    
    public String getProtocol() {
        return this.protocol;
    }
    
    public String getService() {
        return this.service;
    }
    
    @Override
    public int hashCode() {
        return (this.address.hashCode() * 31 + this.service.hashCode()) * 31 + this.protocol.hashCode();
    }
    
    @Override
    public String toString() {
        return this.address + "/" + this.service + "/" + this.protocol;
    }
}
