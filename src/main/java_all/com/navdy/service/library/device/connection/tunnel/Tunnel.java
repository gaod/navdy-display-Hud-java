package com.navdy.service.library.device.connection.tunnel;

import java.io.OutputStream;
import java.io.InputStream;
import com.navdy.service.library.network.SocketAdapter;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import java.util.Iterator;
import java.io.IOException;
import android.util.Log;
import java.util.ArrayList;
import com.navdy.service.library.network.SocketFactory;
import com.navdy.service.library.network.SocketAcceptor;
import com.navdy.service.library.device.connection.ProxyService;

public class Tunnel extends ProxyService
{
    static final String TAG;
    private final SocketAcceptor acceptor;
    private final SocketFactory connector;
    final ArrayList<TransferThread> transferThreads;
    
    static {
        TAG = Tunnel.class.getSimpleName();
    }
    
    public Tunnel(final SocketAcceptor acceptor, final SocketFactory connector) {
        this.transferThreads = new ArrayList<TransferThread>();
        this.setName(Tunnel.TAG);
        this.acceptor = acceptor;
        this.connector = connector;
    }
    
    @Override
    public void cancel() {
        while (true) {
            try {
                this.acceptor.close();
                final Iterator<TransferThread> iterator = ((ArrayList)this.transferThreads.clone()).iterator();
                while (iterator.hasNext()) {
                    iterator.next().cancel();
                }
            }
            catch (IOException ex) {
                Log.e(Tunnel.TAG, "close failed:" + ex.getMessage());
                continue;
            }
            break;
        }
    }
    
    @Override
    public void run() {
        Log.d(Tunnel.TAG, String.format("start: %s -> %s", this.acceptor, this.connector));
        try {
            while (true) {
                final SocketAdapter accept = this.acceptor.accept();
                Log.v(Tunnel.TAG, "accepted connection (" + accept + ")");
                Closeable closeable = null;
                final Closeable closeable2 = null;
                final Closeable closeable3 = null;
                final Closeable closeable4 = null;
                final Closeable closeable5 = null;
                Closeable inputStream = closeable2;
                Closeable closeable6 = closeable4;
                Closeable closeable7 = closeable3;
                Closeable closeable8 = closeable5;
                try {
                    final SocketAdapter build = this.connector.build();
                    inputStream = closeable2;
                    closeable6 = closeable4;
                    closeable7 = closeable3;
                    closeable8 = closeable5;
                    closeable = build;
                    build.connect();
                    inputStream = closeable2;
                    closeable6 = closeable4;
                    closeable7 = closeable3;
                    closeable8 = closeable5;
                    closeable = build;
                    final String tag = Tunnel.TAG;
                    inputStream = closeable2;
                    closeable6 = closeable4;
                    closeable7 = closeable3;
                    closeable8 = closeable5;
                    closeable = build;
                    inputStream = closeable2;
                    closeable6 = closeable4;
                    closeable7 = closeable3;
                    closeable8 = closeable5;
                    closeable = build;
                    final StringBuilder sb = new StringBuilder();
                    inputStream = closeable2;
                    closeable6 = closeable4;
                    closeable7 = closeable3;
                    closeable8 = closeable5;
                    closeable = build;
                    Log.v(tag, sb.append("connected (").append(build).append(")").toString());
                    inputStream = closeable2;
                    closeable6 = closeable4;
                    closeable7 = closeable3;
                    closeable8 = closeable5;
                    closeable = build;
                    Log.d(Tunnel.TAG, String.format("starting transfer: %s -> %s", accept, build));
                    inputStream = closeable2;
                    closeable6 = closeable4;
                    closeable7 = closeable3;
                    closeable8 = closeable5;
                    closeable = build;
                    final InputStream inputStream2 = (InputStream)(inputStream = accept.getInputStream());
                    closeable6 = closeable4;
                    closeable7 = closeable3;
                    closeable8 = closeable5;
                    closeable = build;
                    final OutputStream outputStream = build.getOutputStream();
                    inputStream = inputStream2;
                    closeable6 = closeable4;
                    closeable7 = closeable3;
                    closeable8 = outputStream;
                    closeable = build;
                    inputStream = inputStream2;
                    closeable6 = closeable4;
                    closeable7 = closeable3;
                    closeable8 = outputStream;
                    closeable = build;
                    final TransferThread transferThread = new TransferThread(this, inputStream2, outputStream, true);
                    inputStream = inputStream2;
                    closeable6 = closeable4;
                    closeable7 = closeable3;
                    closeable8 = outputStream;
                    closeable = build;
                    final InputStream inputStream3 = build.getInputStream();
                    inputStream = inputStream2;
                    closeable6 = closeable4;
                    closeable7 = inputStream3;
                    closeable8 = outputStream;
                    closeable = build;
                    final OutputStream outputStream2 = accept.getOutputStream();
                    inputStream = inputStream2;
                    closeable6 = outputStream2;
                    closeable7 = inputStream3;
                    closeable8 = outputStream;
                    closeable = build;
                    inputStream = inputStream2;
                    closeable6 = outputStream2;
                    closeable7 = inputStream3;
                    closeable8 = outputStream;
                    closeable = build;
                    final TransferThread transferThread2 = new TransferThread(this, inputStream3, outputStream2, false);
                    inputStream = inputStream2;
                    closeable6 = outputStream2;
                    closeable7 = inputStream3;
                    closeable8 = outputStream;
                    closeable = build;
                    transferThread.start();
                    inputStream = inputStream2;
                    closeable6 = outputStream2;
                    closeable7 = inputStream3;
                    closeable8 = outputStream;
                    closeable = build;
                    transferThread2.start();
                    inputStream = inputStream2;
                    closeable6 = outputStream2;
                    closeable7 = inputStream3;
                    closeable8 = outputStream;
                    closeable = build;
                    transferThread.join();
                    inputStream = inputStream2;
                    closeable6 = outputStream2;
                    closeable7 = inputStream3;
                    closeable8 = outputStream;
                    closeable = build;
                    transferThread2.join();
                    inputStream = inputStream2;
                    closeable6 = outputStream2;
                    closeable7 = inputStream3;
                    closeable8 = outputStream;
                    closeable = build;
                    accept.close();
                    inputStream = inputStream2;
                    closeable6 = outputStream2;
                    closeable7 = inputStream3;
                    closeable8 = outputStream;
                    closeable = build;
                    build.close();
                }
                catch (InterruptedException ex2) {}
                catch (IOException ex) {
                    Log.e(Tunnel.TAG, "transfer failed:" + ex.getMessage());
                    IOUtils.closeStream(closeable7);
                    IOUtils.closeStream(closeable8);
                    IOUtils.closeStream(closeable);
                    IOUtils.closeStream(inputStream);
                    IOUtils.closeStream(closeable6);
                    IOUtils.closeStream(accept);
                }
            }
        }
        catch (Exception ex3) {}
    }
}
