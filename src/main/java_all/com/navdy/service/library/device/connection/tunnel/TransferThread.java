package com.navdy.service.library.device.connection.tunnel;

import java.util.Arrays;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import java.io.OutputStream;
import java.io.InputStream;
import com.navdy.service.library.util.NetworkActivityTracker;
import com.navdy.service.library.log.Logger;

class TransferThread extends Thread
{
    static final Logger sLogger;
    final NetworkActivityTracker activityTracker;
    private volatile boolean canceled;
    final InputStream inStream;
    final OutputStream outStream;
    final Tunnel parentThread;
    final boolean sending;
    
    static {
        sLogger = new Logger(TransferThread.class);
    }
    
    public TransferThread(final Tunnel parentThread, final InputStream inStream, final OutputStream outStream, final boolean sending) {
        TransferThread.sLogger.v("new transfer thread (" + this + "): " + inStream + " -> " + outStream);
        this.setName(TransferThread.class.getSimpleName());
        this.parentThread = parentThread;
        this.inStream = inStream;
        this.outStream = outStream;
        this.sending = sending;
        parentThread.transferThreads.add(this);
        this.activityTracker = NetworkActivityTracker.getInstance();
    }
    
    public void cancel() {
        this.canceled = true;
        IOUtils.closeStream(this.inStream);
        IOUtils.closeStream(this.outStream);
        this.parentThread.transferThreads.remove(this);
    }
    
    @Override
    public void run() {
        final byte[] array = new byte[16384];
        while (true) {
            while (true) {
                int read = 0;
                Label_0144: {
                    while (true) {
                        try {
                            while (true) {
                                read = this.inStream.read(array);
                                if (read < 0) {
                                    break;
                                }
                                if (TransferThread.sLogger.isLoggable(2)) {
                                    Arrays.copyOf(array, read);
                                    TransferThread.sLogger.v(String.format("transfer (%s -> %s): %d bytes%s", this.inStream, this.outStream, read, ""));
                                }
                                this.outStream.write(array, 0, read);
                                this.outStream.flush();
                                if (!this.sending) {
                                    break Label_0144;
                                }
                                this.activityTracker.addBytesSent(read);
                            }
                            TransferThread.sLogger.d("socket was closed");
                            this.cancel();
                            return;
                        }
                        catch (Throwable t) {
                            if (!this.canceled) {
                                TransferThread.sLogger.e("Exception", t);
                            }
                            continue;
                        }
                        break;
                    }
                }
                this.activityTracker.addBytesReceived(read);
                continue;
            }
        }
    }
}
