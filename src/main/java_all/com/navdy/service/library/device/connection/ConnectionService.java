package com.navdy.service.library.device.connection;

import android.os.Handler;
import android.os.Process;
import com.navdy.service.library.events.connection.LinkPropertiesChanged;
import android.bluetooth.BluetoothAdapter;
import android.content.IntentFilter;
import android.os.HandlerThread;
import com.navdy.service.library.events.Ext_NavdyEvent;
import android.os.IBinder;
import com.navdy.service.library.events.NavdyEventUtil;
import android.bluetooth.BluetoothDevice;
import java.io.IOException;
import com.navdy.service.library.events.Ping;
import com.navdy.service.library.events.connection.DisconnectRequest;
import java.io.Serializable;
import android.os.SystemClock;
import android.content.Intent;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.WireUtil;
import android.content.Context;
import com.navdy.service.library.device.NavdyDeviceId;
import android.os.TransactionTooLargeException;
import android.os.DeadObjectException;
import android.os.RemoteException;
import com.navdy.service.library.events.DeviceInfo;
import com.squareup.wire.Message;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.util.SystemUtils;
import java.util.ArrayList;
import android.os.Looper;
import com.squareup.wire.Wire;
import com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster;
import java.util.List;
import com.navdy.hud.app.IEventSource;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.IEventListener;
import com.navdy.service.library.device.RemoteDeviceRegistry;
import android.content.BroadcastReceiver;
import com.navdy.service.library.events.connection.ConnectionStatus;
import java.util.UUID;
import com.navdy.service.library.device.RemoteDevice;
import android.app.Service;

public abstract class ConnectionService extends Service implements ConnectionListener.Listener, RemoteDevice.Listener
{
    public static final UUID ACCESSORY_IAP2;
    public static final String ACTION_LINK_BANDWIDTH_LEVEL_CHANGED = "LINK_BANDWIDTH_LEVEL_CHANGED";
    public static final String CATEGORY_NAVDY_LINK = "NAVDY_LINK";
    private static final int CONNECT_TIMEOUT = 10000;
    private static final int DEAD_CONNECTION_TIME = 60000;
    private static final ConnectionStatus DEVICES_CHANGED_EVENT;
    public static final UUID DEVICE_IAP2;
    private static final int EVENT_DISCONNECT = 3;
    protected static final int EVENT_HEARTBEAT = 2;
    private static final int EVENT_RESTART_LISTENERS = 4;
    private static final int EVENT_STATE_CHANGE = 1;
    public static final String EXTRA_BANDWIDTH_LEVEL = "EXTRA_BANDWIDTH_MODE";
    private static final int HEARTBEAT_INTERVAL = 4000;
    private static final int IDLE_TIMEOUT = 1000;
    public static final String ILLEGAL_ARGUMENT = "ILLEGAL_ARGUMENT";
    public static final String NAVDY_IAP_NAME = "Navdy iAP";
    public static final String NAVDY_PROTO_SERVICE_NAME = "Navdy";
    public static final UUID NAVDY_PROTO_SERVICE_UUID;
    public static final UUID NAVDY_PROXY_TUNNEL_UUID;
    private static final int PAIRING_TIMEOUT = 30000;
    public static final String REASON_DEAD_CONNECTION = "DEAD_CONNECTION";
    private static final int RECONNECT_DELAY = 2000;
    private static final int RECONNECT_TIMEOUT = 10000;
    protected static final int RETRY_CONNECTING_AFTER_FAILURE_TIMEOUT = 30000;
    private static final int SLEEP_TIMEOUT = 60000;
    private BroadcastReceiver bluetoothReceiver;
    private boolean broadcasting;
    protected final Object connectionLock;
    protected RemoteDeviceRegistry deviceRegistry;
    private boolean forceReconnect;
    protected boolean inProcess;
    private long lastMessageReceivedTime;
    protected final Object listenerLock;
    private IEventListener[] listenersArray;
    private boolean listening;
    protected final Logger logger;
    protected ConnectionListener[] mConnectionListeners;
    private IEventSource.Stub mEventSource;
    private final List<IEventListener> mListeners;
    protected RemoteDevice mRemoteDevice;
    protected RemoteDeviceBroadcaster[] mRemoteDeviceBroadcasters;
    protected Wire mWire;
    private PendingConnectHandler pendingConnectHandler;
    private ProxyService proxyService;
    private volatile boolean quitting;
    protected Runnable reconnectRunnable;
    protected boolean serverMode;
    protected volatile ServiceHandler serviceHandler;
    private volatile Looper serviceLooper;
    protected State state;
    private long stateAge;
    
    static {
        DEVICES_CHANGED_EVENT = new ConnectionStatus(ConnectionStatus.Status.CONNECTION_PAIRED_DEVICES_CHANGED, null);
        NAVDY_PROTO_SERVICE_UUID = UUID.fromString("1992B7D7-C9B9-4F4F-AA7F-9FE33D95851E");
        NAVDY_PROXY_TUNNEL_UUID = UUID.fromString("D72BC85F-F015-4F24-A72F-35924E10888F");
        DEVICE_IAP2 = UUID.fromString("00000000-deca-fade-deca-deafdecacafe");
        ACCESSORY_IAP2 = UUID.fromString("00000000-deca-fade-deca-deafdecacaff");
    }
    
    public ConnectionService() {
        this.logger = new Logger(this.getClass());
        this.connectionLock = new Object();
        this.listenerLock = new Object();
        this.mListeners = new ArrayList<IEventListener>();
        this.listenersArray = new IEventListener[0];
        this.stateAge = 0L;
        this.serverMode = false;
        this.inProcess = true;
        this.listening = false;
        this.broadcasting = false;
        this.forceReconnect = false;
        this.mEventSource = new IEventSource.Stub() {
            public void addEventListener(final IEventListener eventListener) throws RemoteException {
                while (true) {
                    final Object listenerLock = ConnectionService.this.listenerLock;
                    while (true) {
                        synchronized (listenerLock) {
                            ConnectionService.this.mListeners.add(eventListener);
                            ConnectionService.this.createListenerList();
                            // monitorexit(listenerLock)
                            ConnectionService.this.logger.v("listener added: pid:" + SystemUtils.getProcessName(ConnectionService.this.getApplicationContext(), getCallingPid()));
                            if (ConnectionService.this.mRemoteDevice != null && ConnectionService.this.mRemoteDevice.isConnected()) {
                                ConnectionService.this.forwardEventLocally(new ConnectionStateChange(ConnectionService.this.mRemoteDevice.getDeviceId().toString(), ConnectionStateChange.ConnectionState.CONNECTION_LINK_ESTABLISHED));
                                ConnectionService.this.forwardEventLocally(new ConnectionStateChange(ConnectionService.this.mRemoteDevice.getDeviceId().toString(), ConnectionStateChange.ConnectionState.CONNECTION_CONNECTED));
                                final DeviceInfo deviceInfo = ConnectionService.this.mRemoteDevice.getDeviceInfo();
                                if (deviceInfo != null) {
                                    ConnectionService.this.logger.v("send device info");
                                    ConnectionService.this.forwardEventLocally(deviceInfo);
                                }
                                ConnectionService.this.sendEventsOnLocalConnect();
                                return;
                            }
                        }
                        ConnectionService.this.forwardEventLocally(new ConnectionStateChange("", ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED));
                        ConnectionService.this.forwardEventLocally(new ConnectionStateChange("", ConnectionStateChange.ConnectionState.CONNECTION_LINK_LOST));
                        continue;
                    }
                }
            }
            
            public void postEvent(final byte[] array) throws RemoteException {
                boolean b = false;
                boolean b2 = false;
                // monitorenter(listenerLock = this.this$0.listenerLock)
                int i = 0;
                try {
                    while (i < ConnectionService.this.listenersArray.length) {
                        try {
                            ConnectionService.this.listenersArray[i].onEvent(array);
                            b = true;
                            ++i;
                        }
                        catch (DeadObjectException ex) {
                            ConnectionService.this.logger.w("Reaping dead listener", (Throwable)ex);
                            ConnectionService.this.mListeners.remove(ConnectionService.this.listenersArray[i]);
                            b2 = true;
                        }
                        catch (TransactionTooLargeException ex2) {
                            ConnectionService.this.logger.w("Communication Pipe is full:", (Throwable)ex2);
                        }
                        catch (Throwable t) {
                            ConnectionService.this.logger.w("Exception throws by remote:", t);
                        }
                    }
                }
                finally {}
                if (ConnectionService.this.logger.isLoggable(2) && !b) {
                    final Throwable t2;
                    ConnectionService.this.logger.d("No one listening for event - byte length " + t2.length);
                }
                if (b2) {
                    ConnectionService.this.createListenerList();
                }
            }
            // monitorexit(listenerLock)
            
            public void postRemoteEvent(final String s, final byte[] array) throws RemoteException {
                if (s == null || array == null) {
                    ConnectionService.this.logger.e("illegal argument");
                    throw new RemoteException("ILLEGAL_ARGUMENT");
                }
                final NavdyDeviceId navdyDeviceId = new NavdyDeviceId(s);
                if (navdyDeviceId.equals(NavdyDeviceId.getThisDevice((Context)ConnectionService.this))) {
                    final NavdyEvent.MessageType eventType = WireUtil.getEventType(array);
                    if (eventType == null || !ConnectionService.this.processLocalEvent(array, eventType)) {
                        ConnectionService.this.logger.w("Connection service ignored message:" + eventType);
                    }
                }
                else if (ConnectionService.this.mRemoteDevice == null || !ConnectionService.this.mRemoteDevice.isConnected()) {
                    if (ConnectionService.this.logger.isLoggable(2)) {
                        ConnectionService.this.logger.e("app not connected");
                    }
                }
                else if (ConnectionService.this.mRemoteDevice.getDeviceId().equals(navdyDeviceId)) {
                    ConnectionService.this.mRemoteDevice.postEvent(array);
                }
                else {
                    ConnectionService.this.logger.i("Device id mismatch: deviceId=" + navdyDeviceId + " remote:" + ConnectionService.this.mRemoteDevice.getDeviceId());
                }
            }
            
            public void removeEventListener(final IEventListener eventListener) throws RemoteException {
                final Object listenerLock = ConnectionService.this.listenerLock;
                synchronized (listenerLock) {
                    ConnectionService.this.mListeners.remove(eventListener);
                    ConnectionService.this.createListenerList();
                    // monitorexit(listenerLock)
                    ConnectionService.this.logger.v("listener removed: pid:" + SystemUtils.getProcessName(ConnectionService.this.getApplicationContext(), getCallingPid()));
                }
            }
        };
        this.state = State.START;
        this.reconnectRunnable = new Runnable() {
            @Override
            public void run() {
                if (ConnectionService.this.state == State.RECONNECTING && ConnectionService.this.mRemoteDevice != null && !ConnectionService.this.mRemoteDevice.isConnecting() && !ConnectionService.this.mRemoteDevice.isConnected()) {
                    ConnectionService.this.logger.i("Retrying to connect to " + ConnectionService.this.mRemoteDevice.getDeviceId());
                    ConnectionService.this.mRemoteDevice.connect();
                }
            }
        };
        this.bluetoothReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if (intent.getAction().equals("android.bluetooth.adapter.action.STATE_CHANGED") && intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE) == 12) {
                    if (ConnectionService.this.state == State.START) {
                        ConnectionService.this.logger.i("bluetooth turned on - exiting START state");
                        ConnectionService.this.setState(State.IDLE);
                    }
                    else {
                        ConnectionService.this.logger.i("bluetooth turned on - restarting listeners");
                        ConnectionService.this.serviceHandler.sendEmptyMessage(4);
                    }
                }
            }
        };
    }
    
    private void checkConnection() {
        if (this.mRemoteDevice == null || !this.mRemoteDevice.isConnected()) {
            final Logger logger = this.logger;
            final StringBuilder append = new StringBuilder().append("dead connection remotedevice=");
            String s;
            if (this.mRemoteDevice == null) {
                s = "null";
            }
            else {
                s = "not null";
            }
            final StringBuilder append2 = append.append(s).append(" isConnected:");
            Serializable value;
            if (this.mRemoteDevice == null) {
                value = "false";
            }
            else {
                value = this.mRemoteDevice.isConnected();
            }
            logger.v(append2.append(value).toString());
            this.setState(State.DISCONNECTING);
        }
        else if (this.mRemoteDevice.getLinkStatus() == LinkStatus.CONNECTED) {
            final long n = SystemClock.elapsedRealtime() - this.lastMessageReceivedTime;
            if (n > 60000L) {
                this.logger.v("dead connection timed out:" + n);
                if (this.reconnectAfterDeadConnection()) {
                    this.logger.d("Reconncting after dead connection timed out disconnect");
                    this.reconnect("DEAD_CONNECTION");
                }
                else {
                    this.setState(State.DISCONNECTING);
                }
            }
        }
    }
    
    private void createListenerList() {
        this.listenersArray = new IEventListener[this.mListeners.size()];
        this.mListeners.<IEventListener>toArray(this.listenersArray);
    }
    
    private void handleDeviceConnect(final RemoteDevice remoteDevice) {
        this.logger.v("onDeviceConnected:remembering device");
        this.rememberPairedDevice(remoteDevice);
        this.logger.v("onDeviceConnected:heartBeat started");
        this.lastMessageReceivedTime = SystemClock.elapsedRealtime();
        this.logger.d("Connecting with app context: " + this.getApplicationContext());
        if (!this.mRemoteDevice.startLink()) {
            this.setState(State.DISCONNECTING);
        }
    }
    
    private void handleRemoteDisconnect(final DisconnectRequest disconnectRequest) {
        if (this.mRemoteDevice != null) {
            if (disconnectRequest.forget != null && disconnectRequest.forget) {
                this.forgetPairedDevice(this.mRemoteDevice);
            }
            this.setState(State.DISCONNECTING);
        }
    }
    
    private boolean hasPaired() {
        return this.deviceRegistry.hasPaired();
    }
    
    private void sendPingIfNeeded() {
        try {
            if (this.mRemoteDevice != null && this.mRemoteDevice.isConnected() && SystemClock.elapsedRealtime() - this.mRemoteDevice.lastMessageSentTime > 4000L) {
                this.mRemoteDevice.postEvent(new Ping());
                if (this.serverMode && this.logger.isLoggable(2)) {
                    this.logger.v("NAVDY-PACKET [H2P-Outgoing-Event] Ping");
                }
            }
        }
        catch (Throwable t) {
            this.logger.e(t);
        }
    }
    
    public void connect(final ConnectionInfo connectionInfo) {
        this.setActiveDevice(new RemoteDevice((Context)this, connectionInfo, this.inProcess));
    }
    
    protected abstract ProxyService createProxyService() throws IOException;
    
    protected void enterState(final State state) {
        this.logger.d("Entering state:" + state);
        if (state == State.DESTROYED) {
            if (this.listening) {
                this.stopListeners();
            }
            if (this.broadcasting) {
                this.stopBroadcasters();
            }
            this.serviceHandler.removeCallbacksAndMessages(null);
        }
        else {
            this.stateAge = 0L;
            final int n = 1000;
            int n2 = 0;
            switch (state) {
                default:
                    n2 = n;
                    break;
                case IDLE:
                    n2 = 1000;
                    break;
                case PAIRING:
                    n2 = 30000;
                    this.startBroadcasters();
                    this.startListeners();
                    this.forwardEventLocally(new ConnectionStatus(ConnectionStatus.Status.CONNECTION_PAIRING, ""));
                    break;
                case LISTENING:
                    if (this.isPromiscuous()) {
                        this.startBroadcasters();
                    }
                    this.startListeners();
                    n2 = n;
                    if (this.serverMode) {
                        n2 = 30000;
                        break;
                    }
                    break;
                case CONNECTING:
                    n2 = 10000;
                    break;
                case RECONNECTING:
                    n2 = 10000;
                    break;
                case DISCONNECTING:
                    n2 = n;
                    if (this.mRemoteDevice != null) {
                        this.mRemoteDevice.disconnect();
                        n2 = n;
                        break;
                    }
                    break;
                case CONNECTED:
                    this.stopListeners();
                    this.stopBroadcasters();
                    this.handleDeviceConnect(this.mRemoteDevice);
                    n2 = n;
                    break;
            }
            this.serviceHandler.removeMessages(2);
            this.serviceHandler.sendEmptyMessageDelayed(2, (long)n2);
        }
    }
    
    protected void exitState(final State state) {
        this.logger.d("Exiting state:" + state);
        switch (state) {
            case START:
                this.startProxyService();
                break;
            case RECONNECTING:
                this.serviceHandler.removeCallbacks(this.reconnectRunnable);
                break;
            case PAIRING:
                if (!this.isPromiscuous()) {
                    this.stopBroadcasters();
                    break;
                }
                break;
        }
    }
    
    protected void forgetPairedDevice(final BluetoothDevice bluetoothDevice) {
        this.deviceRegistry.removePairedConnection(bluetoothDevice);
        this.forwardEventLocally(ConnectionService.DEVICES_CHANGED_EVENT);
    }
    
    protected void forgetPairedDevice(final RemoteDevice remoteDevice) {
        this.deviceRegistry.removePairedConnection(remoteDevice.getActiveConnectionInfo());
        this.forwardEventLocally(ConnectionService.DEVICES_CHANGED_EVENT);
    }
    
    protected void forwardEventLocally(final Message message) {
        this.forwardEventLocally(NavdyEventUtil.eventFromMessage(message).toByteArray());
    }
    
    protected void forwardEventLocally(final byte[] array) {
        try {
            this.mEventSource.postEvent(array);
        }
        catch (Throwable t) {
            this.logger.e(t);
        }
    }
    
    protected abstract ConnectionListener[] getConnectionListeners(final Context p0);
    
    protected abstract RemoteDeviceBroadcaster[] getRemoteDeviceBroadcasters();
    
    protected void handleDeviceDisconnect(final RemoteDevice remoteDevice, final DisconnectCause disconnectCause) {
        this.logger.v("device disconnect:" + remoteDevice + " cause:" + disconnectCause);
        if (remoteDevice != null) {
            remoteDevice.stopLink();
        }
        if (this.mRemoteDevice == remoteDevice) {
            if ((disconnectCause == DisconnectCause.ABORTED || this.forceReconnect) && this.serverMode) {
                this.forceReconnect = false;
                new PendingConnectHandler(this.mRemoteDevice, null).run();
            }
            else {
                this.setRemoteDevice(null);
                this.setState(State.IDLE);
            }
        }
    }
    
    protected void heartBeat() {
        if (this.stateAge % 10L == 0L) {
            this.logger.d("Heartbeat: in state:" + this.state);
        }
        ++this.stateAge;
        int n2;
        final int n = n2 = 1000;
        while (true) {
            switch (this.state) {
                default:
                    n2 = n;
                    break Label_0114;
                case CONNECTED:
                    n2 = 4000;
                    this.sendPingIfNeeded();
                    this.checkConnection();
                    break Label_0114;
                case CONNECTING:
                case RECONNECTING:
                    this.setState(State.IDLE);
                    n2 = n;
                    break Label_0114;
                case PAIRING:
                    n2 = 30000;
                case DISCONNECTING:
                    this.serviceHandler.removeMessages(2);
                    this.serviceHandler.sendEmptyMessageDelayed(2, (long)n2);
                case IDLE:
                    if (this.pendingConnectHandler != null) {
                        this.pendingConnectHandler.run();
                        n2 = n;
                        continue;
                    }
                    if (!this.hasPaired() && this.serverMode) {
                        this.setState(State.PAIRING);
                        n2 = n;
                        continue;
                    }
                    if (this.hasPaired()) {
                        this.setState(State.LISTENING);
                        n2 = n;
                        continue;
                    }
                    n2 = 60000;
                    continue;
                case LISTENING:
                    if (!this.serverMode) {
                        n2 = 60000;
                        continue;
                    }
                    if (this.mRemoteDevice != null) {
                        this.setState(State.RECONNECTING);
                        this.serviceHandler.post(this.reconnectRunnable);
                        n2 = n;
                        continue;
                    }
                    if (this.needAutoSearch()) {
                        this.setState(State.SEARCHING);
                        n2 = n;
                        continue;
                    }
                    n2 = 60000;
                    continue;
            }
            break;
        }
    }
    
    public boolean isConnected() {
        return this.mRemoteDevice != null && this.mRemoteDevice.isConnected();
    }
    
    public boolean isPromiscuous() {
        return false;
    }
    
    public boolean needAutoSearch() {
        return true;
    }
    
    public IBinder onBind(final Intent intent) {
        Object mEventSource;
        if (IEventSource.class.getName().equals(intent.getAction())) {
            this.logger.d("returning IEventSource API endpoint");
            mEventSource = this.mEventSource;
        }
        else {
            this.logger.w("invalid action:" + intent.getAction());
            mEventSource = null;
        }
        return (IBinder)mEventSource;
    }
    
    public void onConnected(final ConnectionListener connectionListener, final Connection connection) {
        this.logger.v("listener connected");
        this.setActiveDevice(new RemoteDevice(this.getApplicationContext(), connection.getConnectionInfo(), this.inProcess), connection);
    }
    
    public void onConnectionFailed(final ConnectionListener connectionListener) {
        this.logger.e("onConnectionFailed:restart listeners");
        this.setState(State.IDLE);
    }
    
    public void onCreate() {
        this.logger.e("ConnectionService created: mainthread:" + Looper.getMainLooper().getThread().getId());
        this.mWire = new Wire((Class<?>[])new Class[] { Ext_NavdyEvent.class });
        final HandlerThread handlerThread = new HandlerThread("ConnectionService");
        handlerThread.start();
        this.serviceLooper = handlerThread.getLooper();
        this.serviceHandler = new ServiceHandler(this.serviceLooper);
        this.registerReceiver(this.bluetoothReceiver, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
        this.deviceRegistry = RemoteDeviceRegistry.getInstance((Context)this);
    }
    
    public void onDestroy() {
        this.logger.v("ConnectionService destroyed.  mwhahahaha.");
        try {
            this.setState(State.DESTROYED);
            this.unregisterReceiver(this.bluetoothReceiver);
            this.serviceLooper.quitSafely();
            this.stopProxyService();
        }
        catch (Throwable t) {
            this.logger.e(t);
        }
    }
    
    public void onDeviceConnectFailure(final RemoteDevice remoteDevice, final ConnectionFailureCause connectionFailureCause) {
        if (this.state == State.RECONNECTING) {
            this.logger.i("onDeviceConnectFailure - retrying");
            this.serviceHandler.postDelayed(this.reconnectRunnable, 2000L);
        }
        else {
            this.logger.i("onDeviceConnectFailure - switching to idle state");
            if (remoteDevice == this.mRemoteDevice && !this.serverMode) {
                this.setRemoteDevice(null);
            }
            else {
                this.logger.d("Not clearing the remote device, to attempt reconnect");
            }
            this.setState(State.IDLE);
        }
    }
    
    public void onDeviceConnected(final RemoteDevice remoteDevice) {
        this.setState(State.CONNECTED);
    }
    
    public void onDeviceConnecting(final RemoteDevice remoteDevice) {
    }
    
    public void onDeviceDisconnected(final RemoteDevice remoteDevice, final DisconnectCause disconnectCause) {
        this.serviceHandler.sendMessage(this.serviceHandler.obtainMessage(3, disconnectCause.ordinal(), -1, remoteDevice));
    }
    
    public void onNavdyEventReceived(final RemoteDevice remoteDevice, final NavdyEvent navdyEvent) {
        this.lastMessageReceivedTime = SystemClock.elapsedRealtime();
        if (navdyEvent.type == NavdyEvent.MessageType.DisconnectRequest) {
            this.handleRemoteDisconnect(navdyEvent.<DisconnectRequest>getExtension(Ext_NavdyEvent.disconnectRequest));
        }
    }
    
    public void onNavdyEventReceived(final RemoteDevice remoteDevice, final byte[] array) {
        this.lastMessageReceivedTime = SystemClock.elapsedRealtime();
        final NavdyEvent.MessageType eventType = WireUtil.getEventType(array);
        if (this.logger.isLoggable(2)) {
            String name;
            if (eventType != null) {
                name = eventType.name();
            }
            else {
                name = "UNKNOWN";
            }
            final Logger logger = this.logger;
            final StringBuilder sb = new StringBuilder();
            String s;
            if (this.serverMode) {
                s = "NAVDY-PACKET [P2H";
            }
            else {
                s = "NAVDY-PACKET [H2P";
            }
            logger.v(sb.append(s).append("-Event] ").append(name).append(" size:").append(array.length).toString());
        }
        if (eventType != null && eventType != NavdyEvent.MessageType.Ping) {
            if (eventType == NavdyEvent.MessageType.DisconnectRequest) {
                try {
                    this.handleRemoteDisconnect(this.mWire.<NavdyEvent>parseFrom(array, NavdyEvent.class).<DisconnectRequest>getExtension(Ext_NavdyEvent.disconnectRequest));
                }
                catch (Throwable t) {
                    this.logger.e("Failed to parse event", t);
                }
            }
            else if (!this.processEvent(array, eventType)) {
                if (eventType == NavdyEvent.MessageType.DeviceInfo) {
                    try {
                        if (this.mRemoteDevice != null && this.mRemoteDevice.isConnected()) {
                            final DeviceInfo build = new DeviceInfo.Builder((DeviceInfo)NavdyEventUtil.messageFromEvent(this.mWire.<NavdyEvent>parseFrom(array, NavdyEvent.class))).deviceId(this.mRemoteDevice.getDeviceId().toString()).build();
                            this.mRemoteDevice.setDeviceInfo(build);
                            this.logger.v("set remote device info");
                            this.forwardEventLocally(NavdyEventUtil.eventFromMessage(build).toByteArray());
                        }
                    }
                    catch (Throwable t2) {
                        this.logger.e("Failed to parse deviceinfo", t2);
                    }
                }
                else {
                    this.forwardEventLocally(array);
                }
            }
        }
    }
    
    public int onStartCommand(final Intent intent, final int n, final int n2) {
        if (intent != null && BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            if (this.state == State.CONNECTED) {
                this.logger.i("BT enabled, disconnecting since app likely crashed");
                this.setState(State.DISCONNECTING);
            }
            else {
                this.logger.i("BT enabled, switching to IDLE state");
                this.setState(State.IDLE);
            }
        }
        return 1;
    }
    
    public void onStartFailure(final ConnectionListener connectionListener) {
        this.logger.e("failed to start listening:" + connectionListener);
    }
    
    public void onStarted(final ConnectionListener connectionListener) {
        this.logger.v("started listening");
    }
    
    public void onStopped(final ConnectionListener connectionListener) {
        this.logger.v("stopped listening:" + connectionListener);
    }
    
    protected boolean processEvent(final byte[] array, final NavdyEvent.MessageType messageType) {
        return false;
    }
    
    protected boolean processLocalEvent(final byte[] array, final NavdyEvent.MessageType messageType) {
        return false;
    }
    
    public void reconnect(final String s) {
        synchronized (this) {
            if (this.mRemoteDevice != null) {
                this.forceReconnect = true;
                this.setState(State.DISCONNECTING);
            }
        }
    }
    
    public boolean reconnectAfterDeadConnection() {
        return false;
    }
    
    protected void rememberPairedDevice(final RemoteDevice remoteDevice) {
        this.deviceRegistry.addPairedConnection(remoteDevice.getActiveConnectionInfo());
        this.forwardEventLocally(ConnectionService.DEVICES_CHANGED_EVENT);
    }
    
    protected void sendEventsOnLocalConnect() {
    }
    
    public void sendMessage(final Message message) {
        if (this.isConnected()) {
            this.mRemoteDevice.postEvent(message);
        }
    }
    
    public boolean setActiveDevice(final RemoteDevice remoteDevice) {
        return this.setActiveDevice(remoteDevice, null);
    }
    
    public boolean setActiveDevice(final RemoteDevice remoteDevice, final Connection connection) {
        while (true) {
            final PendingConnectHandler pendingConnectHandler;
            Label_0084: {
                synchronized (this) {
                    if (this.mRemoteDevice != null) {
                        if (this.mRemoteDevice.isConnected() || this.mRemoteDevice.isConnecting()) {
                            this.setState(State.DISCONNECTING);
                        }
                        else {
                            this.setRemoteDevice(null);
                        }
                    }
                    if (remoteDevice != null) {
                        pendingConnectHandler = new PendingConnectHandler(remoteDevice, connection);
                        if (this.mRemoteDevice != null) {
                            break Label_0084;
                        }
                        pendingConnectHandler.run();
                    }
                    return true;
                }
            }
            this.pendingConnectHandler = pendingConnectHandler;
            return true;
        }
    }
    
    protected void setBandwidthLevel(final int linkBandwidthLevel) {
        if (this.state == State.CONNECTED && this.mRemoteDevice != null && (linkBandwidthLevel == 0 || linkBandwidthLevel == 1)) {
            this.mRemoteDevice.setLinkBandwidthLevel(linkBandwidthLevel);
            final LinkPropertiesChanged.Builder builder = new LinkPropertiesChanged.Builder();
            builder.bandwidthLevel(linkBandwidthLevel);
            this.forwardEventLocally(builder.build());
        }
    }
    
    protected void setRemoteDevice(final RemoteDevice mRemoteDevice) {
        synchronized (this) {
            if (mRemoteDevice != this.mRemoteDevice) {
                if (this.mRemoteDevice != null) {
                    this.mRemoteDevice.removeListener((RemoteDevice.Listener)this);
                }
                this.mRemoteDevice = mRemoteDevice;
                if (this.mRemoteDevice != null) {
                    this.mRemoteDevice.addListener((RemoteDevice.Listener)this);
                }
            }
        }
    }
    
    public void setState(final State state) {
        if (!this.quitting && this.state != state) {
            if (state == State.DESTROYED) {
                this.quitting = true;
            }
            this.serviceHandler.sendMessage(this.serviceHandler.obtainMessage(1, state));
        }
    }
    
    protected void startBroadcasters() {
        final Object connectionLock = this.connectionLock;
        synchronized (connectionLock) {
            this.broadcasting = true;
            if (this.mRemoteDeviceBroadcasters == null) {
                this.mRemoteDeviceBroadcasters = this.getRemoteDeviceBroadcasters();
            }
            int i = 0;
            while (i < this.mRemoteDeviceBroadcasters.length) {
                this.logger.v("starting connection broadcaster:" + this.mRemoteDeviceBroadcasters[i].getClass().getName());
                try {
                    this.mRemoteDeviceBroadcasters[i].start();
                    ++i;
                }
                catch (Throwable t) {
                    this.logger.e(t);
                }
            }
        }
    }
    // monitorexit(o)
    
    protected void startListeners() {
        final Object connectionLock = this.connectionLock;
        synchronized (connectionLock) {
            this.listening = true;
            if (this.mConnectionListeners == null) {
                this.logger.v("initializing connector listener");
                this.mConnectionListeners = this.getConnectionListeners(this.getApplicationContext());
                for (int i = 0; i < this.mConnectionListeners.length; ++i) {
                    this.mConnectionListeners[i].addListener(this);
                }
            }
            int j = 0;
            while (j < this.mConnectionListeners.length) {
                this.logger.v("starting connection listener:" + this.mConnectionListeners[j]);
                try {
                    this.mConnectionListeners[j].start();
                    ++j;
                }
                catch (Throwable t) {
                    this.logger.e(t);
                }
            }
        }
    }
    // monitorexit(o)
    
    protected void startProxyService() {
        if (this.proxyService != null && this.proxyService.isAlive()) {
            return;
        }
        this.logger.v(SystemUtils.getProcessName(this.getApplicationContext(), Process.myPid()) + ": start service for proxy");
        try {
            (this.proxyService = this.createProxyService()).start();
        }
        catch (Exception ex) {
            this.logger.e("Failed to start proxy service", ex);
        }
    }
    
    protected void stopBroadcasters() {
        this.logger.v("stopping all broadcasters");
        final Object connectionLock = this.connectionLock;
        synchronized (connectionLock) {
            this.broadcasting = false;
            if (this.mRemoteDeviceBroadcasters != null) {
                int i = 0;
                while (i < this.mRemoteDeviceBroadcasters.length) {
                    this.logger.v("stopping connection broadcaster:" + this.mRemoteDeviceBroadcasters[i].getClass().getName());
                    try {
                        this.mRemoteDeviceBroadcasters[i].stop();
                        ++i;
                    }
                    catch (Throwable t) {
                        this.logger.e(t);
                    }
                }
            }
        }
    }
    // monitorexit(o)
    
    protected void stopListeners() {
        final Object connectionLock = this.connectionLock;
        synchronized (connectionLock) {
            this.listening = false;
            if (this.mConnectionListeners != null) {
                int i = 0;
                while (i < this.mConnectionListeners.length) {
                    this.logger.v("stopping:" + this.mConnectionListeners[i]);
                    try {
                        this.mConnectionListeners[i].stop();
                        ++i;
                    }
                    catch (Throwable t) {
                        this.logger.e(t);
                    }
                }
            }
        }
    }
    // monitorexit(o)
    
    protected void stopProxyService() {
        if (this.proxyService != null) {
            this.proxyService.cancel();
            this.proxyService = null;
        }
    }
    
    private class PendingConnectHandler implements Runnable
    {
        private Connection connection;
        private RemoteDevice device;
        
        public PendingConnectHandler(final RemoteDevice device, final Connection connection) {
            this.device = device;
            this.connection = connection;
        }
        
        @Override
        public void run() {
            boolean b;
            if (ConnectionService.this.mRemoteDevice != this.device) {
                b = true;
            }
            else {
                b = false;
            }
            final Logger logger = ConnectionService.this.logger;
            final StringBuilder append = new StringBuilder().append("Trying to ");
            String s;
            if (b) {
                s = "connect";
            }
            else {
                s = "reconnect";
            }
            logger.d(append.append(s).append(" to ").append(this.device.getDeviceId().getDisplayName()).toString());
            ConnectionService.this.setRemoteDevice(this.device);
            if (this.connection != null) {
                ConnectionService.this.mRemoteDevice.setActiveConnection(this.connection);
            }
            else if (ConnectionService.this.mRemoteDevice.isConnected()) {
                ConnectionService.this.onDeviceConnected(ConnectionService.this.mRemoteDevice);
            }
            else {
                final ConnectionService this$0 = ConnectionService.this;
                State state;
                if (b) {
                    state = State.CONNECTING;
                }
                else {
                    state = State.RECONNECTING;
                }
                this$0.setState(state);
                ConnectionService.this.mRemoteDevice.connect();
            }
            ConnectionService.this.pendingConnectHandler = null;
        }
    }
    
    protected final class ServiceHandler extends Handler
    {
        public ServiceHandler(final Looper looper) {
            super(looper);
        }
        
        public void handleMessage(final android.os.Message message) {
            switch (message.what) {
                default:
                    ConnectionService.this.logger.e("Unknown message: " + message);
                    break;
                case 1: {
                    final State state = (State)message.obj;
                    if (state != ConnectionService.this.state) {
                        ConnectionService.this.exitState(ConnectionService.this.state);
                        ConnectionService.this.state = state;
                        ConnectionService.this.enterState(ConnectionService.this.state);
                        break;
                    }
                    break;
                }
                case 2:
                    ConnectionService.this.heartBeat();
                    break;
                case 3:
                    ConnectionService.this.handleDeviceDisconnect((RemoteDevice)message.obj, DisconnectCause.values()[message.arg1]);
                    break;
                case 4:
                    if (ConnectionService.this.listening) {
                        ConnectionService.this.logger.i("stopping/starting listeners");
                        ConnectionService.this.stopListeners();
                        ConnectionService.this.startListeners();
                    }
                    if (ConnectionService.this.broadcasting) {
                        ConnectionService.this.logger.i("restarting broadcasters");
                        ConnectionService.this.stopBroadcasters();
                        ConnectionService.this.startBroadcasters();
                    }
                    ConnectionService.this.stopProxyService();
                    ConnectionService.this.startProxyService();
                    break;
            }
        }
    }
    
    public enum State
    {
        CONNECTED, 
        CONNECTING, 
        DESTROYED, 
        DISCONNECTED, 
        DISCONNECTING, 
        IDLE, 
        LISTENING, 
        PAIRING, 
        RECONNECTING, 
        SEARCHING, 
        START;
    }
}
