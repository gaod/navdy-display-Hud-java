package com.navdy.service.library.device.link;

import com.navdy.service.library.log.Logger;
import com.navdy.service.library.device.connection.Connection;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import java.io.InputStream;
import com.navdy.service.library.events.NavdyEventReader;
import com.navdy.service.library.device.connection.ConnectionType;

public class ReaderThread extends IOThread
{
    private final ConnectionType connectionType;
    private boolean isNetworkLinkReadyByDefault;
    private final LinkListener linkListener;
    private final NavdyEventReader mmEventReader;
    private InputStream mmInStream;
    
    public ReaderThread(final ConnectionType connectionType, final InputStream mmInStream, final LinkListener linkListener, final boolean isNetworkLinkReadyByDefault) {
        this.isNetworkLinkReadyByDefault = false;
        this.setName("ReaderThread");
        this.logger.d("create ReaderThread");
        this.mmInStream = mmInStream;
        this.mmEventReader = new NavdyEventReader(this.mmInStream);
        this.linkListener = linkListener;
        this.connectionType = connectionType;
        this.isNetworkLinkReadyByDefault = isNetworkLinkReadyByDefault;
    }
    
    @Override
    public void cancel() {
        super.cancel();
        IOUtils.closeStream(this.mmInStream);
        this.mmInStream = null;
    }
    
    @Override
    public void run() {
        this.logger.i("BEGIN");
        final Connection.DisconnectCause normal = Connection.DisconnectCause.NORMAL;
        this.linkListener.linkEstablished(this.connectionType);
        if (this.isNetworkLinkReadyByDefault) {
            this.linkListener.onNetworkLinkReady();
        }
        while (true) {
            Block_5: {
                Enum<Connection.DisconnectCause> enum1;
                while (true) {
                    enum1 = normal;
                    if (this.closing) {
                        break;
                    }
                    byte[] bytes = null;
                    Label_0252: {
                        try {
                            if (this.logger.isLoggable(3)) {
                                this.logger.v("before read");
                            }
                            bytes = this.mmEventReader.readBytes();
                            if (bytes != null && bytes.length > 524288) {
                                throw new RuntimeException("reader Max packet size exceeded [" + bytes.length + "] bytes[" + IOUtils.bytesToHexString(bytes, 0, 50) + "]");
                            }
                            break Label_0252;
                        }
                        catch (Exception ex) {
                            enum1 = normal;
                            if (!this.closing) {
                                this.logger.e("disconnected: " + ex.getMessage());
                                enum1 = Connection.DisconnectCause.ABORTED;
                            }
                        }
                        break;
                    }
                    if (this.logger.isLoggable(3)) {
                        final Logger logger = this.logger;
                        final StringBuilder append = new StringBuilder().append("after read len:");
                        int length;
                        if (bytes == null) {
                            length = -1;
                        }
                        else {
                            length = bytes.length;
                        }
                        logger.v(append.append(length).toString());
                    }
                    if (bytes == null) {
                        break Block_5;
                    }
                    this.linkListener.onNavdyEventReceived(bytes);
                }
                this.logger.i("Signaling connection lost cause:" + enum1);
                this.linkListener.linkLost(this.connectionType, (Connection.DisconnectCause)enum1);
                this.logger.i("Exiting thread");
                IOUtils.closeStream(this.mmInStream);
                this.mmInStream = null;
                return;
            }
            Enum<Connection.DisconnectCause> enum1 = normal;
            if (!this.closing) {
                this.logger.e("no event data parsed. assuming disconnect.");
                enum1 = Connection.DisconnectCause.ABORTED;
            }
            continue;
        }
    }
}
