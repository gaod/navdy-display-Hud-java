package com.navdy.service.library.device.discovery;

import java.util.TimerTask;
import java.util.Timer;
import android.content.Context;
import android.net.nsd.NsdServiceInfo;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdManager;
import com.navdy.service.library.log.Logger;

public abstract class MDNSDeviceBroadcaster implements RemoteDeviceBroadcaster
{
    public static final Logger sLogger;
    protected NsdManager mNsdManager;
    protected NsdManager$RegistrationListener mRegistrationListener;
    protected NsdServiceInfo mServiceInfo;
    protected boolean registered;
    
    static {
        sLogger = new Logger(MDNSDeviceBroadcaster.class);
    }
    
    protected MDNSDeviceBroadcaster() {
    }
    
    public MDNSDeviceBroadcaster(final Context context, final NsdServiceInfo mServiceInfo) {
        this.mNsdManager = (NsdManager)context.getSystemService("servicediscovery");
        if (mServiceInfo == null) {
            throw new IllegalArgumentException("Service info required.");
        }
        this.mServiceInfo = mServiceInfo;
    }
    
    protected NsdManager$RegistrationListener getRegistrationListener() {
        return (NsdManager$RegistrationListener)new NsdManager$RegistrationListener() {
            public void onRegistrationFailed(final NsdServiceInfo nsdServiceInfo, final int n) {
                MDNSDeviceBroadcaster.sLogger.d("SERVICE REGISTRATION FAILED" + MDNSDeviceBroadcaster.this.mServiceInfo.getServiceType());
            }
            
            public void onServiceRegistered(final NsdServiceInfo nsdServiceInfo) {
                MDNSDeviceBroadcaster.sLogger.d("SERVICE REGISTERED: " + MDNSDeviceBroadcaster.this.mServiceInfo.getServiceType());
            }
            
            public void onServiceUnregistered(final NsdServiceInfo nsdServiceInfo) {
                MDNSDeviceBroadcaster.sLogger.d("SERVICE UNREGISTERED" + MDNSDeviceBroadcaster.this.mServiceInfo.getServiceType());
            }
            
            public void onUnregistrationFailed(final NsdServiceInfo nsdServiceInfo, final int n) {
                MDNSDeviceBroadcaster.sLogger.d("SERVICE UNREGISTRATION FAILED: " + MDNSDeviceBroadcaster.this.mServiceInfo.getServiceType());
            }
        };
    }
    
    protected boolean registerRecord(final NsdServiceInfo nsdServiceInfo, final NsdManager$RegistrationListener nsdManager$RegistrationListener) {
        MDNSDeviceBroadcaster.sLogger.e("Registering: " + nsdServiceInfo.getServiceType());
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                MDNSDeviceBroadcaster.sLogger.e("Register timer fired: " + nsdServiceInfo.getServiceType());
                MDNSDeviceBroadcaster.this.mNsdManager.registerService(nsdServiceInfo, 1, nsdManager$RegistrationListener);
                MDNSDeviceBroadcaster.this.registered = true;
                MDNSDeviceBroadcaster.sLogger.e("Registered: " + nsdServiceInfo.getServiceType());
            }
        }, 2000L);
        return true;
    }
    
    @Override
    public boolean start() {
        boolean registerRecord;
        if (this.mRegistrationListener != null) {
            MDNSDeviceBroadcaster.sLogger.e("Already started: " + this.mServiceInfo.getServiceType());
            registerRecord = false;
        }
        else {
            this.mRegistrationListener = this.getRegistrationListener();
            registerRecord = this.registerRecord(this.mServiceInfo, this.mRegistrationListener);
        }
        return registerRecord;
    }
    
    @Override
    public boolean stop() {
        boolean unregisterRecord;
        if (this.mRegistrationListener == null) {
            MDNSDeviceBroadcaster.sLogger.e("Already stopped: " + this.mServiceInfo.getServiceType());
            unregisterRecord = false;
        }
        else {
            MDNSDeviceBroadcaster.sLogger.e("Unregistering: " + this.mServiceInfo.getServiceType());
            unregisterRecord = this.unregisterRecord(this.mRegistrationListener);
        }
        return unregisterRecord;
    }
    
    protected boolean unregisterRecord(final NsdManager$RegistrationListener nsdManager$RegistrationListener) {
        if (this.registered) {
            this.registered = false;
            this.mNsdManager.unregisterService(nsdManager$RegistrationListener);
        }
        return true;
    }
}
