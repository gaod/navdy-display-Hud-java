package com.navdy.service.library.events.contacts;

import com.squareup.wire.ProtoEnum;

public enum PhoneNumberType implements ProtoEnum
{
    PHONE_NUMBER_HOME(1), 
    PHONE_NUMBER_MOBILE(3), 
    PHONE_NUMBER_OTHER(4), 
    PHONE_NUMBER_WORK(2);
    
    private final int value;
    
    private PhoneNumberType(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
}
