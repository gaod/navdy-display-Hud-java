package com.navdy.service.library.events.preferences;

import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.settings.DateTimeConfiguration;
import com.squareup.wire.Message;

public final class LocalPreferences extends Message
{
    public static final DateTimeConfiguration.Clock DEFAULT_CLOCKFORMAT;
    public static final Boolean DEFAULT_MANUALZOOM;
    public static final Float DEFAULT_MANUALZOOMLEVEL;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final DateTimeConfiguration.Clock clockFormat;
    @ProtoField(tag = 1, type = Datatype.BOOL)
    public final Boolean manualZoom;
    @ProtoField(tag = 2, type = Datatype.FLOAT)
    public final Float manualZoomLevel;
    
    static {
        DEFAULT_MANUALZOOM = false;
        DEFAULT_MANUALZOOMLEVEL = 0.0f;
        DEFAULT_CLOCKFORMAT = DateTimeConfiguration.Clock.CLOCK_24_HOUR;
    }
    
    private LocalPreferences(final Builder builder) {
        this(builder.manualZoom, builder.manualZoomLevel, builder.clockFormat);
        this.setBuilder((Message.Builder)builder);
    }
    
    public LocalPreferences(final Boolean manualZoom, final Float manualZoomLevel, final DateTimeConfiguration.Clock clockFormat) {
        this.manualZoom = manualZoom;
        this.manualZoomLevel = manualZoomLevel;
        this.clockFormat = clockFormat;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof LocalPreferences)) {
                b = false;
            }
            else {
                final LocalPreferences localPreferences = (LocalPreferences)o;
                if (!this.equals(this.manualZoom, localPreferences.manualZoom) || !this.equals(this.manualZoomLevel, localPreferences.manualZoomLevel) || !this.equals(this.clockFormat, localPreferences.clockFormat)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.manualZoom != null) {
                hashCode3 = this.manualZoom.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.manualZoomLevel != null) {
                hashCode4 = this.manualZoomLevel.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.clockFormat != null) {
                hashCode = this.clockFormat.hashCode();
            }
            hashCode2 = (hashCode3 * 37 + hashCode4) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<LocalPreferences>
    {
        public DateTimeConfiguration.Clock clockFormat;
        public Boolean manualZoom;
        public Float manualZoomLevel;
        
        public Builder() {
        }
        
        public Builder(final LocalPreferences localPreferences) {
            super(localPreferences);
            if (localPreferences != null) {
                this.manualZoom = localPreferences.manualZoom;
                this.manualZoomLevel = localPreferences.manualZoomLevel;
                this.clockFormat = localPreferences.clockFormat;
            }
        }
        
        public LocalPreferences build() {
            return new LocalPreferences(this, null);
        }
        
        public Builder clockFormat(final DateTimeConfiguration.Clock clockFormat) {
            this.clockFormat = clockFormat;
            return this;
        }
        
        public Builder manualZoom(final Boolean manualZoom) {
            this.manualZoom = manualZoom;
            return this;
        }
        
        public Builder manualZoomLevel(final Float manualZoomLevel) {
            this.manualZoomLevel = manualZoomLevel;
            return this;
        }
    }
}
