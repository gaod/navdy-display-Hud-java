package com.navdy.service.library.events.settings;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class NetworkStateChange extends Message
{
    public static final Boolean DEFAULT_CELLNETWORK;
    public static final Boolean DEFAULT_NETWORKAVAILABLE;
    public static final Boolean DEFAULT_REACHABILITY;
    public static final Boolean DEFAULT_WIFINETWORK;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 2, type = Datatype.BOOL)
    public final Boolean cellNetwork;
    @ProtoField(tag = 1, type = Datatype.BOOL)
    public final Boolean networkAvailable;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean reachability;
    @ProtoField(tag = 3, type = Datatype.BOOL)
    public final Boolean wifiNetwork;
    
    static {
        DEFAULT_NETWORKAVAILABLE = false;
        DEFAULT_CELLNETWORK = false;
        DEFAULT_WIFINETWORK = false;
        DEFAULT_REACHABILITY = false;
    }
    
    private NetworkStateChange(final Builder builder) {
        this(builder.networkAvailable, builder.cellNetwork, builder.wifiNetwork, builder.reachability);
        this.setBuilder((Message.Builder)builder);
    }
    
    public NetworkStateChange(final Boolean networkAvailable, final Boolean cellNetwork, final Boolean wifiNetwork, final Boolean reachability) {
        this.networkAvailable = networkAvailable;
        this.cellNetwork = cellNetwork;
        this.wifiNetwork = wifiNetwork;
        this.reachability = reachability;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof NetworkStateChange)) {
                b = false;
            }
            else {
                final NetworkStateChange networkStateChange = (NetworkStateChange)o;
                if (!this.equals(this.networkAvailable, networkStateChange.networkAvailable) || !this.equals(this.cellNetwork, networkStateChange.cellNetwork) || !this.equals(this.wifiNetwork, networkStateChange.wifiNetwork) || !this.equals(this.reachability, networkStateChange.reachability)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.networkAvailable != null) {
                hashCode3 = this.networkAvailable.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.cellNetwork != null) {
                hashCode4 = this.cellNetwork.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.wifiNetwork != null) {
                hashCode5 = this.wifiNetwork.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            if (this.reachability != null) {
                hashCode = this.reachability.hashCode();
            }
            hashCode2 = ((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<NetworkStateChange>
    {
        public Boolean cellNetwork;
        public Boolean networkAvailable;
        public Boolean reachability;
        public Boolean wifiNetwork;
        
        public Builder() {
        }
        
        public Builder(final NetworkStateChange networkStateChange) {
            super(networkStateChange);
            if (networkStateChange != null) {
                this.networkAvailable = networkStateChange.networkAvailable;
                this.cellNetwork = networkStateChange.cellNetwork;
                this.wifiNetwork = networkStateChange.wifiNetwork;
                this.reachability = networkStateChange.reachability;
            }
        }
        
        public NetworkStateChange build() {
            return new NetworkStateChange(this, null);
        }
        
        public Builder cellNetwork(final Boolean cellNetwork) {
            this.cellNetwork = cellNetwork;
            return this;
        }
        
        public Builder networkAvailable(final Boolean networkAvailable) {
            this.networkAvailable = networkAvailable;
            return this;
        }
        
        public Builder reachability(final Boolean reachability) {
            this.reachability = reachability;
            return this;
        }
        
        public Builder wifiNetwork(final Boolean wifiNetwork) {
            this.wifiNetwork = wifiNetwork;
            return this;
        }
    }
}
