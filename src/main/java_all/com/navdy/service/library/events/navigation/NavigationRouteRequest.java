package com.navdy.service.library.events.navigation;

import com.squareup.wire.ProtoEnum;
import java.util.Collections;
import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.location.Coordinate;
import java.util.List;
import com.navdy.service.library.events.destination.Destination;
import com.squareup.wire.Message;

public final class NavigationRouteRequest extends Message
{
    public static final Boolean DEFAULT_AUTONAVIGATE;
    public static final Boolean DEFAULT_CANCELCURRENT;
    public static final Destination.FavoriteType DEFAULT_DESTINATIONTYPE;
    public static final String DEFAULT_DESTINATION_IDENTIFIER = "";
    public static final Boolean DEFAULT_GEOCODESTREETADDRESS;
    public static final String DEFAULT_LABEL = "";
    public static final Boolean DEFAULT_ORIGINDISPLAY;
    public static final String DEFAULT_REQUESTID = "";
    public static final List<RouteAttribute> DEFAULT_ROUTEATTRIBUTES;
    public static final String DEFAULT_STREETADDRESS = "";
    public static final List<Coordinate> DEFAULT_WAYPOINTS;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 6, type = Datatype.BOOL)
    public final Boolean autoNavigate;
    @ProtoField(tag = 10, type = Datatype.BOOL)
    public final Boolean cancelCurrent;
    @ProtoField(label = Label.REQUIRED, tag = 1)
    public final Coordinate destination;
    @ProtoField(tag = 12)
    public final Coordinate destinationDisplay;
    @ProtoField(tag = 8, type = Datatype.ENUM)
    public final Destination.FavoriteType destinationType;
    @ProtoField(tag = 7, type = Datatype.STRING)
    public final String destination_identifier;
    @ProtoField(tag = 5, type = Datatype.BOOL)
    public final Boolean geoCodeStreetAddress;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String label;
    @ProtoField(tag = 9, type = Datatype.BOOL)
    public final Boolean originDisplay;
    @ProtoField(tag = 14)
    public final Destination requestDestination;
    @ProtoField(tag = 11, type = Datatype.STRING)
    public final String requestId;
    @ProtoField(enumType = RouteAttribute.class, label = Label.REPEATED, tag = 13, type = Datatype.ENUM)
    public final List<RouteAttribute> routeAttributes;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String streetAddress;
    @ProtoField(label = Label.REPEATED, messageType = Coordinate.class, tag = 3)
    public final List<Coordinate> waypoints;
    
    static {
        DEFAULT_WAYPOINTS = Collections.<Coordinate>emptyList();
        DEFAULT_GEOCODESTREETADDRESS = false;
        DEFAULT_AUTONAVIGATE = false;
        DEFAULT_DESTINATIONTYPE = Destination.FavoriteType.FAVORITE_NONE;
        DEFAULT_ORIGINDISPLAY = false;
        DEFAULT_CANCELCURRENT = false;
        DEFAULT_ROUTEATTRIBUTES = Collections.<RouteAttribute>emptyList();
    }
    
    public NavigationRouteRequest(final Coordinate destination, final String label, final List<Coordinate> list, final String streetAddress, final Boolean geoCodeStreetAddress, final Boolean autoNavigate, final String destination_identifier, final Destination.FavoriteType destinationType, final Boolean originDisplay, final Boolean cancelCurrent, final String requestId, final Coordinate destinationDisplay, final List<RouteAttribute> list2, final Destination requestDestination) {
        this.destination = destination;
        this.label = label;
        this.waypoints = Message.<Coordinate>immutableCopyOf(list);
        this.streetAddress = streetAddress;
        this.geoCodeStreetAddress = geoCodeStreetAddress;
        this.autoNavigate = autoNavigate;
        this.destination_identifier = destination_identifier;
        this.destinationType = destinationType;
        this.originDisplay = originDisplay;
        this.cancelCurrent = cancelCurrent;
        this.requestId = requestId;
        this.destinationDisplay = destinationDisplay;
        this.routeAttributes = Message.<RouteAttribute>immutableCopyOf(list2);
        this.requestDestination = requestDestination;
    }
    
    private NavigationRouteRequest(final Builder builder) {
        this(builder.destination, builder.label, builder.waypoints, builder.streetAddress, builder.geoCodeStreetAddress, builder.autoNavigate, builder.destination_identifier, builder.destinationType, builder.originDisplay, builder.cancelCurrent, builder.requestId, builder.destinationDisplay, builder.routeAttributes, builder.requestDestination);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof NavigationRouteRequest)) {
                b = false;
            }
            else {
                final NavigationRouteRequest navigationRouteRequest = (NavigationRouteRequest)o;
                if (!this.equals(this.destination, navigationRouteRequest.destination) || !this.equals(this.label, navigationRouteRequest.label) || !this.equals(this.waypoints, navigationRouteRequest.waypoints) || !this.equals(this.streetAddress, navigationRouteRequest.streetAddress) || !this.equals(this.geoCodeStreetAddress, navigationRouteRequest.geoCodeStreetAddress) || !this.equals(this.autoNavigate, navigationRouteRequest.autoNavigate) || !this.equals(this.destination_identifier, navigationRouteRequest.destination_identifier) || !this.equals(this.destinationType, navigationRouteRequest.destinationType) || !this.equals(this.originDisplay, navigationRouteRequest.originDisplay) || !this.equals(this.cancelCurrent, navigationRouteRequest.cancelCurrent) || !this.equals(this.requestId, navigationRouteRequest.requestId) || !this.equals(this.destinationDisplay, navigationRouteRequest.destinationDisplay) || !this.equals(this.routeAttributes, navigationRouteRequest.routeAttributes) || !this.equals(this.requestDestination, navigationRouteRequest.requestDestination)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 1;
        int hashCode2 = 0;
        int hashCode3;
        if ((hashCode3 = this.hashCode) == 0) {
            int hashCode4;
            if (this.destination != null) {
                hashCode4 = this.destination.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.label != null) {
                hashCode5 = this.label.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.waypoints != null) {
                hashCode6 = this.waypoints.hashCode();
            }
            else {
                hashCode6 = 1;
            }
            int hashCode7;
            if (this.streetAddress != null) {
                hashCode7 = this.streetAddress.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.geoCodeStreetAddress != null) {
                hashCode8 = this.geoCodeStreetAddress.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            int hashCode9;
            if (this.autoNavigate != null) {
                hashCode9 = this.autoNavigate.hashCode();
            }
            else {
                hashCode9 = 0;
            }
            int hashCode10;
            if (this.destination_identifier != null) {
                hashCode10 = this.destination_identifier.hashCode();
            }
            else {
                hashCode10 = 0;
            }
            int hashCode11;
            if (this.destinationType != null) {
                hashCode11 = this.destinationType.hashCode();
            }
            else {
                hashCode11 = 0;
            }
            int hashCode12;
            if (this.originDisplay != null) {
                hashCode12 = this.originDisplay.hashCode();
            }
            else {
                hashCode12 = 0;
            }
            int hashCode13;
            if (this.cancelCurrent != null) {
                hashCode13 = this.cancelCurrent.hashCode();
            }
            else {
                hashCode13 = 0;
            }
            int hashCode14;
            if (this.requestId != null) {
                hashCode14 = this.requestId.hashCode();
            }
            else {
                hashCode14 = 0;
            }
            int hashCode15;
            if (this.destinationDisplay != null) {
                hashCode15 = this.destinationDisplay.hashCode();
            }
            else {
                hashCode15 = 0;
            }
            if (this.routeAttributes != null) {
                hashCode = this.routeAttributes.hashCode();
            }
            if (this.requestDestination != null) {
                hashCode2 = this.requestDestination.hashCode();
            }
            hashCode3 = ((((((((((((hashCode4 * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode9) * 37 + hashCode10) * 37 + hashCode11) * 37 + hashCode12) * 37 + hashCode13) * 37 + hashCode14) * 37 + hashCode15) * 37 + hashCode) * 37 + hashCode2;
            this.hashCode = hashCode3;
        }
        return hashCode3;
    }
    
    public static final class Builder extends Message.Builder<NavigationRouteRequest>
    {
        public Boolean autoNavigate;
        public Boolean cancelCurrent;
        public Coordinate destination;
        public Coordinate destinationDisplay;
        public Destination.FavoriteType destinationType;
        public String destination_identifier;
        public Boolean geoCodeStreetAddress;
        public String label;
        public Boolean originDisplay;
        public Destination requestDestination;
        public String requestId;
        public List<RouteAttribute> routeAttributes;
        public String streetAddress;
        public List<Coordinate> waypoints;
        
        public Builder() {
        }
        
        public Builder(final NavigationRouteRequest navigationRouteRequest) {
            super(navigationRouteRequest);
            if (navigationRouteRequest != null) {
                this.destination = navigationRouteRequest.destination;
                this.label = navigationRouteRequest.label;
                this.waypoints = (List<Coordinate>)Message.<Object>copyOf((List<Object>)navigationRouteRequest.waypoints);
                this.streetAddress = navigationRouteRequest.streetAddress;
                this.geoCodeStreetAddress = navigationRouteRequest.geoCodeStreetAddress;
                this.autoNavigate = navigationRouteRequest.autoNavigate;
                this.destination_identifier = navigationRouteRequest.destination_identifier;
                this.destinationType = navigationRouteRequest.destinationType;
                this.originDisplay = navigationRouteRequest.originDisplay;
                this.cancelCurrent = navigationRouteRequest.cancelCurrent;
                this.requestId = navigationRouteRequest.requestId;
                this.destinationDisplay = navigationRouteRequest.destinationDisplay;
                this.routeAttributes = (List<RouteAttribute>)Message.<Object>copyOf((List<Object>)navigationRouteRequest.routeAttributes);
                this.requestDestination = navigationRouteRequest.requestDestination;
            }
        }
        
        public Builder autoNavigate(final Boolean autoNavigate) {
            this.autoNavigate = autoNavigate;
            return this;
        }
        
        public NavigationRouteRequest build() {
            ((Message.Builder)this).checkRequiredFields();
            return new NavigationRouteRequest(this, null);
        }
        
        public Builder cancelCurrent(final Boolean cancelCurrent) {
            this.cancelCurrent = cancelCurrent;
            return this;
        }
        
        public Builder destination(final Coordinate destination) {
            this.destination = destination;
            return this;
        }
        
        public Builder destinationDisplay(final Coordinate destinationDisplay) {
            this.destinationDisplay = destinationDisplay;
            return this;
        }
        
        public Builder destinationType(final Destination.FavoriteType destinationType) {
            this.destinationType = destinationType;
            return this;
        }
        
        public Builder destination_identifier(final String destination_identifier) {
            this.destination_identifier = destination_identifier;
            return this;
        }
        
        public Builder geoCodeStreetAddress(final Boolean geoCodeStreetAddress) {
            this.geoCodeStreetAddress = geoCodeStreetAddress;
            return this;
        }
        
        public Builder label(final String label) {
            this.label = label;
            return this;
        }
        
        public Builder originDisplay(final Boolean originDisplay) {
            this.originDisplay = originDisplay;
            return this;
        }
        
        public Builder requestDestination(final Destination requestDestination) {
            this.requestDestination = requestDestination;
            return this;
        }
        
        public Builder requestId(final String requestId) {
            this.requestId = requestId;
            return this;
        }
        
        public Builder routeAttributes(final List<RouteAttribute> list) {
            this.routeAttributes = Message.Builder.<RouteAttribute>checkForNulls(list);
            return this;
        }
        
        public Builder streetAddress(final String streetAddress) {
            this.streetAddress = streetAddress;
            return this;
        }
        
        public Builder waypoints(final List<Coordinate> list) {
            this.waypoints = Message.Builder.<Coordinate>checkForNulls(list);
            return this;
        }
    }
    
    public enum RouteAttribute implements ProtoEnum
    {
        ROUTE_ATTRIBUTE_GAS(0), 
        ROUTE_ATTRIBUTE_SAVED_ROUTE(1);
        
        private final int value;
        
        private RouteAttribute(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
