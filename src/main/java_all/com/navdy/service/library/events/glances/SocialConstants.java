package com.navdy.service.library.events.glances;

import com.squareup.wire.ProtoEnum;

public enum SocialConstants implements ProtoEnum
{
    SOCIAL_FROM(0), 
    SOCIAL_MESSAGE(2), 
    SOCIAL_TO(1);
    
    private final int value;
    
    private SocialConstants(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
}
