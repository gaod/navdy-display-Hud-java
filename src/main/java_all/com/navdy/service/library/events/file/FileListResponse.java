package com.navdy.service.library.events.file;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import java.util.List;
import com.squareup.wire.Message;

public final class FileListResponse extends Message
{
    public static final List<String> DEFAULT_FILES;
    public static final FileType DEFAULT_FILE_TYPE;
    public static final RequestStatus DEFAULT_STATUS;
    public static final String DEFAULT_STATUS_DETAIL = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.ENUM)
    public final FileType file_type;
    @ProtoField(label = Label.REPEATED, tag = 4, type = Datatype.STRING)
    public final List<String> files;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String status_detail;
    
    static {
        DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
        DEFAULT_FILE_TYPE = FileType.FILE_TYPE_OTA;
        DEFAULT_FILES = Collections.<String>emptyList();
    }
    
    public FileListResponse(final RequestStatus status, final String status_detail, final FileType file_type, final List<String> list) {
        this.status = status;
        this.status_detail = status_detail;
        this.file_type = file_type;
        this.files = Message.<String>immutableCopyOf(list);
    }
    
    private FileListResponse(final Builder builder) {
        this(builder.status, builder.status_detail, builder.file_type, builder.files);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof FileListResponse)) {
                b = false;
            }
            else {
                final FileListResponse fileListResponse = (FileListResponse)o;
                if (!this.equals(this.status, fileListResponse.status) || !this.equals(this.status_detail, fileListResponse.status_detail) || !this.equals(this.file_type, fileListResponse.file_type) || !this.equals(this.files, fileListResponse.files)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.status != null) {
                hashCode3 = this.status.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.status_detail != null) {
                hashCode4 = this.status_detail.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.file_type != null) {
                hashCode = this.file_type.hashCode();
            }
            int hashCode5;
            if (this.files != null) {
                hashCode5 = this.files.hashCode();
            }
            else {
                hashCode5 = 1;
            }
            hashCode2 = ((hashCode3 * 37 + hashCode4) * 37 + hashCode) * 37 + hashCode5;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<FileListResponse>
    {
        public FileType file_type;
        public List<String> files;
        public RequestStatus status;
        public String status_detail;
        
        public Builder() {
        }
        
        public Builder(final FileListResponse fileListResponse) {
            super(fileListResponse);
            if (fileListResponse != null) {
                this.status = fileListResponse.status;
                this.status_detail = fileListResponse.status_detail;
                this.file_type = fileListResponse.file_type;
                this.files = (List<String>)Message.<Object>copyOf((List<Object>)fileListResponse.files);
            }
        }
        
        public FileListResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new FileListResponse(this, null);
        }
        
        public Builder file_type(final FileType file_type) {
            this.file_type = file_type;
            return this;
        }
        
        public Builder files(final List<String> list) {
            this.files = Message.Builder.<String>checkForNulls(list);
            return this;
        }
        
        public Builder status(final RequestStatus status) {
            this.status = status;
            return this;
        }
        
        public Builder status_detail(final String status_detail) {
            this.status_detail = status_detail;
            return this;
        }
    }
}
