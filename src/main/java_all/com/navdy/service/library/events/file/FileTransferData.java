package com.navdy.service.library.events.file;

import com.squareup.wire.ProtoField;
import okio.ByteString;
import com.squareup.wire.Message;

public final class FileTransferData extends Message
{
    public static final Integer DEFAULT_CHUNKINDEX;
    public static final ByteString DEFAULT_DATABYTES;
    public static final String DEFAULT_FILECHECKSUM = "";
    public static final Boolean DEFAULT_LASTCHUNK;
    public static final Integer DEFAULT_TRANSFERID;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.INT32)
    public final Integer chunkIndex;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.BYTES)
    public final ByteString dataBytes;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String fileCheckSum;
    @ProtoField(label = Label.REQUIRED, tag = 4, type = Datatype.BOOL)
    public final Boolean lastChunk;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT32)
    public final Integer transferId;
    
    static {
        DEFAULT_TRANSFERID = 0;
        DEFAULT_CHUNKINDEX = 0;
        DEFAULT_DATABYTES = ByteString.EMPTY;
        DEFAULT_LASTCHUNK = false;
    }
    
    private FileTransferData(final Builder builder) {
        this(builder.transferId, builder.chunkIndex, builder.dataBytes, builder.lastChunk, builder.fileCheckSum);
        this.setBuilder((Message.Builder)builder);
    }
    
    public FileTransferData(final Integer transferId, final Integer chunkIndex, final ByteString dataBytes, final Boolean lastChunk, final String fileCheckSum) {
        this.transferId = transferId;
        this.chunkIndex = chunkIndex;
        this.dataBytes = dataBytes;
        this.lastChunk = lastChunk;
        this.fileCheckSum = fileCheckSum;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof FileTransferData)) {
                b = false;
            }
            else {
                final FileTransferData fileTransferData = (FileTransferData)o;
                if (!this.equals(this.transferId, fileTransferData.transferId) || !this.equals(this.chunkIndex, fileTransferData.chunkIndex) || !this.equals(this.dataBytes, fileTransferData.dataBytes) || !this.equals(this.lastChunk, fileTransferData.lastChunk) || !this.equals(this.fileCheckSum, fileTransferData.fileCheckSum)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.transferId != null) {
                hashCode3 = this.transferId.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.chunkIndex != null) {
                hashCode4 = this.chunkIndex.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.dataBytes != null) {
                hashCode5 = this.dataBytes.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.lastChunk != null) {
                hashCode6 = this.lastChunk.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            if (this.fileCheckSum != null) {
                hashCode = this.fileCheckSum.hashCode();
            }
            hashCode2 = (((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<FileTransferData>
    {
        public Integer chunkIndex;
        public ByteString dataBytes;
        public String fileCheckSum;
        public Boolean lastChunk;
        public Integer transferId;
        
        public Builder() {
        }
        
        public Builder(final FileTransferData fileTransferData) {
            super(fileTransferData);
            if (fileTransferData != null) {
                this.transferId = fileTransferData.transferId;
                this.chunkIndex = fileTransferData.chunkIndex;
                this.dataBytes = fileTransferData.dataBytes;
                this.lastChunk = fileTransferData.lastChunk;
                this.fileCheckSum = fileTransferData.fileCheckSum;
            }
        }
        
        public FileTransferData build() {
            ((Message.Builder)this).checkRequiredFields();
            return new FileTransferData(this, null);
        }
        
        public Builder chunkIndex(final Integer chunkIndex) {
            this.chunkIndex = chunkIndex;
            return this;
        }
        
        public Builder dataBytes(final ByteString dataBytes) {
            this.dataBytes = dataBytes;
            return this;
        }
        
        public Builder fileCheckSum(final String fileCheckSum) {
            this.fileCheckSum = fileCheckSum;
            return this;
        }
        
        public Builder lastChunk(final Boolean lastChunk) {
            this.lastChunk = lastChunk;
            return this;
        }
        
        public Builder transferId(final Integer transferId) {
            this.transferId = transferId;
            return this;
        }
    }
}
