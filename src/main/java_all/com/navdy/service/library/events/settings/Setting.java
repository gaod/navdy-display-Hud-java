package com.navdy.service.library.events.settings;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class Setting extends Message
{
    public static final String DEFAULT_KEY = "";
    public static final String DEFAULT_VALUE = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String key;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.STRING)
    public final String value;
    
    private Setting(final Builder builder) {
        this(builder.key, builder.value);
        this.setBuilder((Message.Builder)builder);
    }
    
    public Setting(final String key, final String value) {
        this.key = key;
        this.value = value;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof Setting)) {
                b = false;
            }
            else {
                final Setting setting = (Setting)o;
                if (!this.equals(this.key, setting.key) || !this.equals(this.value, setting.value)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.key != null) {
                hashCode3 = this.key.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.value != null) {
                hashCode = this.value.hashCode();
            }
            hashCode2 = hashCode3 * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<Setting>
    {
        public String key;
        public String value;
        
        public Builder() {
        }
        
        public Builder(final Setting setting) {
            super(setting);
            if (setting != null) {
                this.key = setting.key;
                this.value = setting.value;
            }
        }
        
        public Setting build() {
            ((Message.Builder)this).checkRequiredFields();
            return new Setting(this, null);
        }
        
        public Builder key(final String key) {
            this.key = key;
            return this;
        }
        
        public Builder value(final String value) {
            this.value = value;
            return this;
        }
    }
}
