package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class MusicTrackInfo extends Message
{
    public static final String DEFAULT_ALBUM = "";
    public static final String DEFAULT_AUTHOR = "";
    public static final String DEFAULT_COLLECTIONID = "";
    public static final MusicCollectionSource DEFAULT_COLLECTIONSOURCE;
    public static final MusicCollectionType DEFAULT_COLLECTIONTYPE;
    public static final Integer DEFAULT_CURRENTPOSITION;
    public static final MusicDataSource DEFAULT_DATASOURCE;
    public static final Integer DEFAULT_DURATION;
    public static final Long DEFAULT_INDEX;
    public static final Boolean DEFAULT_ISNEXTALLOWED;
    public static final Boolean DEFAULT_ISONLINESTREAM;
    public static final Boolean DEFAULT_ISPREVIOUSALLOWED;
    public static final String DEFAULT_NAME = "";
    public static final MusicPlaybackState DEFAULT_PLAYBACKSTATE;
    public static final Integer DEFAULT_PLAYCOUNT;
    public static final MusicRepeatMode DEFAULT_REPEATMODE;
    public static final MusicShuffleMode DEFAULT_SHUFFLEMODE;
    public static final String DEFAULT_TRACKID = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String album;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String author;
    @ProtoField(tag = 14, type = Datatype.STRING)
    public final String collectionId;
    @ProtoField(tag = 12, type = Datatype.ENUM)
    public final MusicCollectionSource collectionSource;
    @ProtoField(tag = 13, type = Datatype.ENUM)
    public final MusicCollectionType collectionType;
    @ProtoField(tag = 7, type = Datatype.INT32)
    public final Integer currentPosition;
    @ProtoField(tag = 10, type = Datatype.ENUM)
    public final MusicDataSource dataSource;
    @ProtoField(tag = 6, type = Datatype.INT32)
    public final Integer duration;
    @ProtoField(tag = 2, type = Datatype.INT64)
    public final Long index;
    @ProtoField(tag = 9, type = Datatype.BOOL)
    public final Boolean isNextAllowed;
    @ProtoField(tag = 11, type = Datatype.BOOL)
    public final Boolean isOnlineStream;
    @ProtoField(tag = 8, type = Datatype.BOOL)
    public final Boolean isPreviousAllowed;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String name;
    @ProtoField(tag = 16, type = Datatype.INT32)
    public final Integer playCount;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final MusicPlaybackState playbackState;
    @ProtoField(tag = 18, type = Datatype.ENUM)
    public final MusicRepeatMode repeatMode;
    @ProtoField(tag = 17, type = Datatype.ENUM)
    public final MusicShuffleMode shuffleMode;
    @ProtoField(tag = 15, type = Datatype.STRING)
    public final String trackId;
    
    static {
        DEFAULT_PLAYBACKSTATE = MusicPlaybackState.PLAYBACK_NONE;
        DEFAULT_INDEX = 0L;
        DEFAULT_DURATION = 0;
        DEFAULT_CURRENTPOSITION = 0;
        DEFAULT_ISPREVIOUSALLOWED = false;
        DEFAULT_ISNEXTALLOWED = false;
        DEFAULT_DATASOURCE = MusicDataSource.MUSIC_SOURCE_NONE;
        DEFAULT_ISONLINESTREAM = false;
        DEFAULT_COLLECTIONSOURCE = MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
        DEFAULT_COLLECTIONTYPE = MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
        DEFAULT_PLAYCOUNT = 0;
        DEFAULT_SHUFFLEMODE = MusicShuffleMode.MUSIC_SHUFFLE_MODE_UNKNOWN;
        DEFAULT_REPEATMODE = MusicRepeatMode.MUSIC_REPEAT_MODE_UNKNOWN;
    }
    
    public MusicTrackInfo(final MusicPlaybackState playbackState, final Long index, final String name, final String author, final String album, final Integer duration, final Integer currentPosition, final Boolean isPreviousAllowed, final Boolean isNextAllowed, final MusicDataSource dataSource, final Boolean isOnlineStream, final MusicCollectionSource collectionSource, final MusicCollectionType collectionType, final String collectionId, final String trackId, final Integer playCount, final MusicShuffleMode shuffleMode, final MusicRepeatMode repeatMode) {
        this.playbackState = playbackState;
        this.index = index;
        this.name = name;
        this.author = author;
        this.album = album;
        this.duration = duration;
        this.currentPosition = currentPosition;
        this.isPreviousAllowed = isPreviousAllowed;
        this.isNextAllowed = isNextAllowed;
        this.dataSource = dataSource;
        this.isOnlineStream = isOnlineStream;
        this.collectionSource = collectionSource;
        this.collectionType = collectionType;
        this.collectionId = collectionId;
        this.trackId = trackId;
        this.playCount = playCount;
        this.shuffleMode = shuffleMode;
        this.repeatMode = repeatMode;
    }
    
    private MusicTrackInfo(final Builder builder) {
        this(builder.playbackState, builder.index, builder.name, builder.author, builder.album, builder.duration, builder.currentPosition, builder.isPreviousAllowed, builder.isNextAllowed, builder.dataSource, builder.isOnlineStream, builder.collectionSource, builder.collectionType, builder.collectionId, builder.trackId, builder.playCount, builder.shuffleMode, builder.repeatMode);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof MusicTrackInfo)) {
                b = false;
            }
            else {
                final MusicTrackInfo musicTrackInfo = (MusicTrackInfo)o;
                if (!this.equals(this.playbackState, musicTrackInfo.playbackState) || !this.equals(this.index, musicTrackInfo.index) || !this.equals(this.name, musicTrackInfo.name) || !this.equals(this.author, musicTrackInfo.author) || !this.equals(this.album, musicTrackInfo.album) || !this.equals(this.duration, musicTrackInfo.duration) || !this.equals(this.currentPosition, musicTrackInfo.currentPosition) || !this.equals(this.isPreviousAllowed, musicTrackInfo.isPreviousAllowed) || !this.equals(this.isNextAllowed, musicTrackInfo.isNextAllowed) || !this.equals(this.dataSource, musicTrackInfo.dataSource) || !this.equals(this.isOnlineStream, musicTrackInfo.isOnlineStream) || !this.equals(this.collectionSource, musicTrackInfo.collectionSource) || !this.equals(this.collectionType, musicTrackInfo.collectionType) || !this.equals(this.collectionId, musicTrackInfo.collectionId) || !this.equals(this.trackId, musicTrackInfo.trackId) || !this.equals(this.playCount, musicTrackInfo.playCount) || !this.equals(this.shuffleMode, musicTrackInfo.shuffleMode) || !this.equals(this.repeatMode, musicTrackInfo.repeatMode)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.playbackState != null) {
                hashCode3 = this.playbackState.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.index != null) {
                hashCode4 = this.index.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.name != null) {
                hashCode5 = this.name.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.author != null) {
                hashCode6 = this.author.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.album != null) {
                hashCode7 = this.album.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.duration != null) {
                hashCode8 = this.duration.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            int hashCode9;
            if (this.currentPosition != null) {
                hashCode9 = this.currentPosition.hashCode();
            }
            else {
                hashCode9 = 0;
            }
            int hashCode10;
            if (this.isPreviousAllowed != null) {
                hashCode10 = this.isPreviousAllowed.hashCode();
            }
            else {
                hashCode10 = 0;
            }
            int hashCode11;
            if (this.isNextAllowed != null) {
                hashCode11 = this.isNextAllowed.hashCode();
            }
            else {
                hashCode11 = 0;
            }
            int hashCode12;
            if (this.dataSource != null) {
                hashCode12 = this.dataSource.hashCode();
            }
            else {
                hashCode12 = 0;
            }
            int hashCode13;
            if (this.isOnlineStream != null) {
                hashCode13 = this.isOnlineStream.hashCode();
            }
            else {
                hashCode13 = 0;
            }
            int hashCode14;
            if (this.collectionSource != null) {
                hashCode14 = this.collectionSource.hashCode();
            }
            else {
                hashCode14 = 0;
            }
            int hashCode15;
            if (this.collectionType != null) {
                hashCode15 = this.collectionType.hashCode();
            }
            else {
                hashCode15 = 0;
            }
            int hashCode16;
            if (this.collectionId != null) {
                hashCode16 = this.collectionId.hashCode();
            }
            else {
                hashCode16 = 0;
            }
            int hashCode17;
            if (this.trackId != null) {
                hashCode17 = this.trackId.hashCode();
            }
            else {
                hashCode17 = 0;
            }
            int hashCode18;
            if (this.playCount != null) {
                hashCode18 = this.playCount.hashCode();
            }
            else {
                hashCode18 = 0;
            }
            int hashCode19;
            if (this.shuffleMode != null) {
                hashCode19 = this.shuffleMode.hashCode();
            }
            else {
                hashCode19 = 0;
            }
            if (this.repeatMode != null) {
                hashCode = this.repeatMode.hashCode();
            }
            hashCode2 = ((((((((((((((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode9) * 37 + hashCode10) * 37 + hashCode11) * 37 + hashCode12) * 37 + hashCode13) * 37 + hashCode14) * 37 + hashCode15) * 37 + hashCode16) * 37 + hashCode17) * 37 + hashCode18) * 37 + hashCode19) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<MusicTrackInfo>
    {
        public String album;
        public String author;
        public String collectionId;
        public MusicCollectionSource collectionSource;
        public MusicCollectionType collectionType;
        public Integer currentPosition;
        public MusicDataSource dataSource;
        public Integer duration;
        public Long index;
        public Boolean isNextAllowed;
        public Boolean isOnlineStream;
        public Boolean isPreviousAllowed;
        public String name;
        public Integer playCount;
        public MusicPlaybackState playbackState;
        public MusicRepeatMode repeatMode;
        public MusicShuffleMode shuffleMode;
        public String trackId;
        
        public Builder() {
        }
        
        public Builder(final MusicTrackInfo musicTrackInfo) {
            super(musicTrackInfo);
            if (musicTrackInfo != null) {
                this.playbackState = musicTrackInfo.playbackState;
                this.index = musicTrackInfo.index;
                this.name = musicTrackInfo.name;
                this.author = musicTrackInfo.author;
                this.album = musicTrackInfo.album;
                this.duration = musicTrackInfo.duration;
                this.currentPosition = musicTrackInfo.currentPosition;
                this.isPreviousAllowed = musicTrackInfo.isPreviousAllowed;
                this.isNextAllowed = musicTrackInfo.isNextAllowed;
                this.dataSource = musicTrackInfo.dataSource;
                this.isOnlineStream = musicTrackInfo.isOnlineStream;
                this.collectionSource = musicTrackInfo.collectionSource;
                this.collectionType = musicTrackInfo.collectionType;
                this.collectionId = musicTrackInfo.collectionId;
                this.trackId = musicTrackInfo.trackId;
                this.playCount = musicTrackInfo.playCount;
                this.shuffleMode = musicTrackInfo.shuffleMode;
                this.repeatMode = musicTrackInfo.repeatMode;
            }
        }
        
        public Builder album(final String album) {
            this.album = album;
            return this;
        }
        
        public Builder author(final String author) {
            this.author = author;
            return this;
        }
        
        public MusicTrackInfo build() {
            ((Message.Builder)this).checkRequiredFields();
            return new MusicTrackInfo(this, null);
        }
        
        public Builder collectionId(final String collectionId) {
            this.collectionId = collectionId;
            return this;
        }
        
        public Builder collectionSource(final MusicCollectionSource collectionSource) {
            this.collectionSource = collectionSource;
            return this;
        }
        
        public Builder collectionType(final MusicCollectionType collectionType) {
            this.collectionType = collectionType;
            return this;
        }
        
        public Builder currentPosition(final Integer currentPosition) {
            this.currentPosition = currentPosition;
            return this;
        }
        
        public Builder dataSource(final MusicDataSource dataSource) {
            this.dataSource = dataSource;
            return this;
        }
        
        public Builder duration(final Integer duration) {
            this.duration = duration;
            return this;
        }
        
        public Builder index(final Long index) {
            this.index = index;
            return this;
        }
        
        public Builder isNextAllowed(final Boolean isNextAllowed) {
            this.isNextAllowed = isNextAllowed;
            return this;
        }
        
        public Builder isOnlineStream(final Boolean isOnlineStream) {
            this.isOnlineStream = isOnlineStream;
            return this;
        }
        
        public Builder isPreviousAllowed(final Boolean isPreviousAllowed) {
            this.isPreviousAllowed = isPreviousAllowed;
            return this;
        }
        
        public Builder name(final String name) {
            this.name = name;
            return this;
        }
        
        public Builder playCount(final Integer playCount) {
            this.playCount = playCount;
            return this;
        }
        
        public Builder playbackState(final MusicPlaybackState playbackState) {
            this.playbackState = playbackState;
            return this;
        }
        
        public Builder repeatMode(final MusicRepeatMode repeatMode) {
            this.repeatMode = repeatMode;
            return this;
        }
        
        public Builder shuffleMode(final MusicShuffleMode shuffleMode) {
            this.shuffleMode = shuffleMode;
            return this;
        }
        
        public Builder trackId(final String trackId) {
            this.trackId = trackId;
            return this;
        }
    }
}
