package com.navdy.service.library.events.contacts;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class Contact extends Message
{
    public static final String DEFAULT_LABEL = "";
    public static final String DEFAULT_NAME = "";
    public static final String DEFAULT_NUMBER = "";
    public static final PhoneNumberType DEFAULT_NUMBERTYPE;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String label;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String name;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String number;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final PhoneNumberType numberType;
    
    static {
        DEFAULT_NUMBERTYPE = PhoneNumberType.PHONE_NUMBER_HOME;
    }
    
    private Contact(final Builder builder) {
        this(builder.name, builder.number, builder.numberType, builder.label);
        this.setBuilder((Message.Builder)builder);
    }
    
    public Contact(final String name, final String number, final PhoneNumberType numberType, final String label) {
        this.name = name;
        this.number = number;
        this.numberType = numberType;
        this.label = label;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof Contact)) {
                b = false;
            }
            else {
                final Contact contact = (Contact)o;
                if (!this.equals(this.name, contact.name) || !this.equals(this.number, contact.number) || !this.equals(this.numberType, contact.numberType) || !this.equals(this.label, contact.label)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.name != null) {
                hashCode3 = this.name.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.number != null) {
                hashCode4 = this.number.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.numberType != null) {
                hashCode5 = this.numberType.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            if (this.label != null) {
                hashCode = this.label.hashCode();
            }
            hashCode2 = ((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<Contact>
    {
        public String label;
        public String name;
        public String number;
        public PhoneNumberType numberType;
        
        public Builder() {
        }
        
        public Builder(final Contact contact) {
            super(contact);
            if (contact != null) {
                this.name = contact.name;
                this.number = contact.number;
                this.numberType = contact.numberType;
                this.label = contact.label;
            }
        }
        
        public Contact build() {
            return new Contact(this, null);
        }
        
        public Builder label(final String label) {
            this.label = label;
            return this;
        }
        
        public Builder name(final String name) {
            this.name = name;
            return this;
        }
        
        public Builder number(final String number) {
            this.number = number;
            return this;
        }
        
        public Builder numberType(final PhoneNumberType numberType) {
            this.numberType = numberType;
            return this;
        }
    }
}
