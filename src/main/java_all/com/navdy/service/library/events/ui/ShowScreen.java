package com.navdy.service.library.events.ui;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.glances.KeyValue;
import java.util.List;
import com.squareup.wire.Message;

public final class ShowScreen extends Message
{
    public static final List<KeyValue> DEFAULT_ARGUMENTS;
    public static final Screen DEFAULT_SCREEN;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REPEATED, messageType = KeyValue.class, tag = 2)
    public final List<KeyValue> arguments;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final Screen screen;
    
    static {
        DEFAULT_SCREEN = Screen.SCREEN_DASHBOARD;
        DEFAULT_ARGUMENTS = Collections.<KeyValue>emptyList();
    }
    
    public ShowScreen(final Screen screen, final List<KeyValue> list) {
        this.screen = screen;
        this.arguments = Message.<KeyValue>immutableCopyOf(list);
    }
    
    private ShowScreen(final Builder builder) {
        this(builder.screen, builder.arguments);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof ShowScreen)) {
                b = false;
            }
            else {
                final ShowScreen showScreen = (ShowScreen)o;
                if (!this.equals(this.screen, showScreen.screen) || !this.equals(this.arguments, showScreen.arguments)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode;
        if ((hashCode = this.hashCode) == 0) {
            int hashCode2;
            if (this.screen != null) {
                hashCode2 = this.screen.hashCode();
            }
            else {
                hashCode2 = 0;
            }
            int hashCode3;
            if (this.arguments != null) {
                hashCode3 = this.arguments.hashCode();
            }
            else {
                hashCode3 = 1;
            }
            hashCode = hashCode2 * 37 + hashCode3;
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<ShowScreen>
    {
        public List<KeyValue> arguments;
        public Screen screen;
        
        public Builder() {
        }
        
        public Builder(final ShowScreen showScreen) {
            super(showScreen);
            if (showScreen != null) {
                this.screen = showScreen.screen;
                this.arguments = (List<KeyValue>)Message.<Object>copyOf((List<Object>)showScreen.arguments);
            }
        }
        
        public Builder arguments(final List<KeyValue> list) {
            this.arguments = Message.Builder.<KeyValue>checkForNulls(list);
            return this;
        }
        
        public ShowScreen build() {
            ((Message.Builder)this).checkRequiredFields();
            return new ShowScreen(this, null);
        }
        
        public Builder screen(final Screen screen) {
            this.screen = screen;
            return this;
        }
    }
}
