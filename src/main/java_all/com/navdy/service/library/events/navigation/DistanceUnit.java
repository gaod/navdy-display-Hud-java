package com.navdy.service.library.events.navigation;

import com.squareup.wire.ProtoEnum;

public enum DistanceUnit implements ProtoEnum
{
    DISTANCE_FEET(3), 
    DISTANCE_KMS(2), 
    DISTANCE_METERS(4), 
    DISTANCE_MILES(1);
    
    private final int value;
    
    private DistanceUnit(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
}
