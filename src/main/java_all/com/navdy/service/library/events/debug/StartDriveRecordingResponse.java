package com.navdy.service.library.events.debug;

import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;

public final class StartDriveRecordingResponse extends Message
{
    public static final RequestStatus DEFAULT_STATUS;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    
    static {
        DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    }
    
    public StartDriveRecordingResponse(final RequestStatus status) {
        this.status = status;
    }
    
    private StartDriveRecordingResponse(final Builder builder) {
        this(builder.status);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof StartDriveRecordingResponse && this.equals(this.status, ((StartDriveRecordingResponse)o).status));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.status != null) {
                hashCode = this.status.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<StartDriveRecordingResponse>
    {
        public RequestStatus status;
        
        public Builder() {
        }
        
        public Builder(final StartDriveRecordingResponse startDriveRecordingResponse) {
            super(startDriveRecordingResponse);
            if (startDriveRecordingResponse != null) {
                this.status = startDriveRecordingResponse.status;
            }
        }
        
        public StartDriveRecordingResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new StartDriveRecordingResponse(this, null);
        }
        
        public Builder status(final RequestStatus status) {
            this.status = status;
            return this;
        }
    }
}
