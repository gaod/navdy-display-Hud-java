package com.navdy.service.library.events.callcontrol;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class PhoneEvent extends Message
{
    public static final String DEFAULT_CALLUUID = "";
    public static final String DEFAULT_CONTACT_NAME = "";
    public static final String DEFAULT_LABEL = "";
    public static final String DEFAULT_NUMBER = "";
    public static final PhoneStatus DEFAULT_STATUS;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String callUUID;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String contact_name;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String label;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String number;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final PhoneStatus status;
    
    static {
        DEFAULT_STATUS = PhoneStatus.PHONE_IDLE;
    }
    
    private PhoneEvent(final Builder builder) {
        this(builder.status, builder.number, builder.contact_name, builder.label, builder.callUUID);
        this.setBuilder((Message.Builder)builder);
    }
    
    public PhoneEvent(final PhoneStatus status, final String number, final String contact_name, final String label, final String callUUID) {
        this.status = status;
        this.number = number;
        this.contact_name = contact_name;
        this.label = label;
        this.callUUID = callUUID;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof PhoneEvent)) {
                b = false;
            }
            else {
                final PhoneEvent phoneEvent = (PhoneEvent)o;
                if (!this.equals(this.status, phoneEvent.status) || !this.equals(this.number, phoneEvent.number) || !this.equals(this.contact_name, phoneEvent.contact_name) || !this.equals(this.label, phoneEvent.label) || !this.equals(this.callUUID, phoneEvent.callUUID)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.status != null) {
                hashCode3 = this.status.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.number != null) {
                hashCode4 = this.number.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.contact_name != null) {
                hashCode5 = this.contact_name.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.label != null) {
                hashCode6 = this.label.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            if (this.callUUID != null) {
                hashCode = this.callUUID.hashCode();
            }
            hashCode2 = (((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<PhoneEvent>
    {
        public String callUUID;
        public String contact_name;
        public String label;
        public String number;
        public PhoneStatus status;
        
        public Builder() {
        }
        
        public Builder(final PhoneEvent phoneEvent) {
            super(phoneEvent);
            if (phoneEvent != null) {
                this.status = phoneEvent.status;
                this.number = phoneEvent.number;
                this.contact_name = phoneEvent.contact_name;
                this.label = phoneEvent.label;
                this.callUUID = phoneEvent.callUUID;
            }
        }
        
        public PhoneEvent build() {
            ((Message.Builder)this).checkRequiredFields();
            return new PhoneEvent(this, null);
        }
        
        public Builder callUUID(final String callUUID) {
            this.callUUID = callUUID;
            return this;
        }
        
        public Builder contact_name(final String contact_name) {
            this.contact_name = contact_name;
            return this;
        }
        
        public Builder label(final String label) {
            this.label = label;
            return this;
        }
        
        public Builder number(final String number) {
            this.number = number;
            return this;
        }
        
        public Builder status(final PhoneStatus status) {
            this.status = status;
            return this;
        }
    }
}
