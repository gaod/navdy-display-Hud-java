package com.navdy.service.library.events.callcontrol;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class TelephonyRequest extends Message
{
    public static final CallAction DEFAULT_ACTION;
    public static final String DEFAULT_CALLUUID = "";
    public static final String DEFAULT_NUMBER = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final CallAction action;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String callUUID;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String number;
    
    static {
        DEFAULT_ACTION = CallAction.CALL_ACCEPT;
    }
    
    public TelephonyRequest(final CallAction action, final String number, final String callUUID) {
        this.action = action;
        this.number = number;
        this.callUUID = callUUID;
    }
    
    private TelephonyRequest(final Builder builder) {
        this(builder.action, builder.number, builder.callUUID);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof TelephonyRequest)) {
                b = false;
            }
            else {
                final TelephonyRequest telephonyRequest = (TelephonyRequest)o;
                if (!this.equals(this.action, telephonyRequest.action) || !this.equals(this.number, telephonyRequest.number) || !this.equals(this.callUUID, telephonyRequest.callUUID)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.action != null) {
                hashCode3 = this.action.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.number != null) {
                hashCode4 = this.number.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.callUUID != null) {
                hashCode = this.callUUID.hashCode();
            }
            hashCode2 = (hashCode3 * 37 + hashCode4) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<TelephonyRequest>
    {
        public CallAction action;
        public String callUUID;
        public String number;
        
        public Builder() {
        }
        
        public Builder(final TelephonyRequest telephonyRequest) {
            super(telephonyRequest);
            if (telephonyRequest != null) {
                this.action = telephonyRequest.action;
                this.number = telephonyRequest.number;
                this.callUUID = telephonyRequest.callUUID;
            }
        }
        
        public Builder action(final CallAction action) {
            this.action = action;
            return this;
        }
        
        public TelephonyRequest build() {
            ((Message.Builder)this).checkRequiredFields();
            return new TelephonyRequest(this, null);
        }
        
        public Builder callUUID(final String callUUID) {
            this.callUUID = callUUID;
            return this;
        }
        
        public Builder number(final String number) {
            this.number = number;
            return this;
        }
    }
}
