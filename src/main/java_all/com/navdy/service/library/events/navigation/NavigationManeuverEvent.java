package com.navdy.service.library.events.navigation;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class NavigationManeuverEvent extends Message
{
    public static final String DEFAULT_CURRENTROAD = "";
    public static final String DEFAULT_DISTANCE_TO_PENDING_TURN = "";
    public static final String DEFAULT_ETA = "";
    public static final Long DEFAULT_ETAUTCTIME;
    public static final NavigationSessionState DEFAULT_NAVIGATIONSTATE;
    public static final String DEFAULT_PENDING_STREET = "";
    public static final NavigationTurn DEFAULT_PENDING_TURN;
    public static final String DEFAULT_SPEED = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String currentRoad;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.STRING)
    public final String distance_to_pending_turn;
    @ProtoField(label = Label.REQUIRED, tag = 5, type = Datatype.STRING)
    public final String eta;
    @ProtoField(tag = 8, type = Datatype.INT64)
    public final Long etaUtcTime;
    @ProtoField(tag = 7, type = Datatype.ENUM)
    public final NavigationSessionState navigationState;
    @ProtoField(label = Label.REQUIRED, tag = 4, type = Datatype.STRING)
    public final String pending_street;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.ENUM)
    public final NavigationTurn pending_turn;
    @ProtoField(label = Label.REQUIRED, tag = 6, type = Datatype.STRING)
    public final String speed;
    
    static {
        DEFAULT_PENDING_TURN = NavigationTurn.NAV_TURN_START;
        DEFAULT_NAVIGATIONSTATE = NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
        DEFAULT_ETAUTCTIME = 0L;
    }
    
    private NavigationManeuverEvent(final Builder builder) {
        this(builder.currentRoad, builder.pending_turn, builder.distance_to_pending_turn, builder.pending_street, builder.eta, builder.speed, builder.navigationState, builder.etaUtcTime);
        this.setBuilder((Message.Builder)builder);
    }
    
    public NavigationManeuverEvent(final String currentRoad, final NavigationTurn pending_turn, final String distance_to_pending_turn, final String pending_street, final String eta, final String speed, final NavigationSessionState navigationState, final Long etaUtcTime) {
        this.currentRoad = currentRoad;
        this.pending_turn = pending_turn;
        this.distance_to_pending_turn = distance_to_pending_turn;
        this.pending_street = pending_street;
        this.eta = eta;
        this.speed = speed;
        this.navigationState = navigationState;
        this.etaUtcTime = etaUtcTime;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof NavigationManeuverEvent)) {
                b = false;
            }
            else {
                final NavigationManeuverEvent navigationManeuverEvent = (NavigationManeuverEvent)o;
                if (!this.equals(this.currentRoad, navigationManeuverEvent.currentRoad) || !this.equals(this.pending_turn, navigationManeuverEvent.pending_turn) || !this.equals(this.distance_to_pending_turn, navigationManeuverEvent.distance_to_pending_turn) || !this.equals(this.pending_street, navigationManeuverEvent.pending_street) || !this.equals(this.eta, navigationManeuverEvent.eta) || !this.equals(this.speed, navigationManeuverEvent.speed) || !this.equals(this.navigationState, navigationManeuverEvent.navigationState) || !this.equals(this.etaUtcTime, navigationManeuverEvent.etaUtcTime)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.currentRoad != null) {
                hashCode3 = this.currentRoad.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.pending_turn != null) {
                hashCode4 = this.pending_turn.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.distance_to_pending_turn != null) {
                hashCode5 = this.distance_to_pending_turn.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.pending_street != null) {
                hashCode6 = this.pending_street.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.eta != null) {
                hashCode7 = this.eta.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.speed != null) {
                hashCode8 = this.speed.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            int hashCode9;
            if (this.navigationState != null) {
                hashCode9 = this.navigationState.hashCode();
            }
            else {
                hashCode9 = 0;
            }
            if (this.etaUtcTime != null) {
                hashCode = this.etaUtcTime.hashCode();
            }
            hashCode2 = ((((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode9) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<NavigationManeuverEvent>
    {
        public String currentRoad;
        public String distance_to_pending_turn;
        public String eta;
        public Long etaUtcTime;
        public NavigationSessionState navigationState;
        public String pending_street;
        public NavigationTurn pending_turn;
        public String speed;
        
        public Builder() {
        }
        
        public Builder(final NavigationManeuverEvent navigationManeuverEvent) {
            super(navigationManeuverEvent);
            if (navigationManeuverEvent != null) {
                this.currentRoad = navigationManeuverEvent.currentRoad;
                this.pending_turn = navigationManeuverEvent.pending_turn;
                this.distance_to_pending_turn = navigationManeuverEvent.distance_to_pending_turn;
                this.pending_street = navigationManeuverEvent.pending_street;
                this.eta = navigationManeuverEvent.eta;
                this.speed = navigationManeuverEvent.speed;
                this.navigationState = navigationManeuverEvent.navigationState;
                this.etaUtcTime = navigationManeuverEvent.etaUtcTime;
            }
        }
        
        public NavigationManeuverEvent build() {
            ((Message.Builder)this).checkRequiredFields();
            return new NavigationManeuverEvent(this, null);
        }
        
        public Builder currentRoad(final String currentRoad) {
            this.currentRoad = currentRoad;
            return this;
        }
        
        public Builder distance_to_pending_turn(final String distance_to_pending_turn) {
            this.distance_to_pending_turn = distance_to_pending_turn;
            return this;
        }
        
        public Builder eta(final String eta) {
            this.eta = eta;
            return this;
        }
        
        public Builder etaUtcTime(final Long etaUtcTime) {
            this.etaUtcTime = etaUtcTime;
            return this;
        }
        
        public Builder navigationState(final NavigationSessionState navigationState) {
            this.navigationState = navigationState;
            return this;
        }
        
        public Builder pending_street(final String pending_street) {
            this.pending_street = pending_street;
            return this;
        }
        
        public Builder pending_turn(final NavigationTurn pending_turn) {
            this.pending_turn = pending_turn;
            return this;
        }
        
        public Builder speed(final String speed) {
            this.speed = speed;
            return this;
        }
    }
}
