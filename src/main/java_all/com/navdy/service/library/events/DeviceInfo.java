package com.navdy.service.library.events;

import com.squareup.wire.ProtoEnum;
import java.util.Collections;
import com.squareup.wire.ProtoField;
import java.util.List;
import com.squareup.wire.Message;

public final class DeviceInfo extends Message
{
    public static final String DEFAULT_BUILDTYPE = "user";
    public static final String DEFAULT_CLIENTVERSION = "";
    public static final String DEFAULT_DEVICEID = "";
    public static final String DEFAULT_DEVICEMAKE = "unknown";
    public static final String DEFAULT_DEVICENAME = "";
    public static final String DEFAULT_DEVICEUUID = "";
    public static final Boolean DEFAULT_FORCEFULLUPDATE;
    public static final String DEFAULT_KERNELVERSION = "";
    public static final List<LegacyCapability> DEFAULT_LEGACYCAPABILITIES;
    public static final String DEFAULT_MODEL = "";
    public static final List<String> DEFAULT_MUSICPLAYERS_OBSOLETE;
    public static final Platform DEFAULT_PLATFORM;
    public static final String DEFAULT_PROTOCOLVERSION = "";
    public static final Integer DEFAULT_SYSTEMAPILEVEL;
    public static final String DEFAULT_SYSTEMVERSION = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 11, type = Datatype.STRING)
    public final String buildType;
    @ProtoField(tag = 16)
    public final Capabilities capabilities;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.STRING)
    public final String clientVersion;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String deviceId;
    @ProtoField(tag = 12, type = Datatype.STRING)
    public final String deviceMake;
    @ProtoField(label = Label.REQUIRED, tag = 4, type = Datatype.STRING)
    public final String deviceName;
    @ProtoField(label = Label.REQUIRED, tag = 7, type = Datatype.STRING)
    public final String deviceUuid;
    @ProtoField(tag = 13, type = Datatype.BOOL)
    public final Boolean forceFullUpdate;
    @ProtoField(tag = 9, type = Datatype.STRING)
    public final String kernelVersion;
    @ProtoField(deprecated = true, enumType = LegacyCapability.class, label = Label.REPEATED, tag = 14, type = Datatype.ENUM)
    @Deprecated
    public final List<LegacyCapability> legacyCapabilities;
    @ProtoField(label = Label.REQUIRED, tag = 6, type = Datatype.STRING)
    public final String model;
    @ProtoField(deprecated = true, label = Label.REPEATED, tag = 15, type = Datatype.STRING)
    @Deprecated
    public final List<String> musicPlayers_OBSOLETE;
    @ProtoField(tag = 10, type = Datatype.ENUM)
    public final Platform platform;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.STRING)
    public final String protocolVersion;
    @ProtoField(tag = 8, type = Datatype.INT32)
    public final Integer systemApiLevel;
    @ProtoField(label = Label.REQUIRED, tag = 5, type = Datatype.STRING)
    public final String systemVersion;
    
    static {
        DEFAULT_SYSTEMAPILEVEL = 0;
        DEFAULT_PLATFORM = Platform.PLATFORM_iOS;
        DEFAULT_FORCEFULLUPDATE = false;
        DEFAULT_LEGACYCAPABILITIES = Collections.<LegacyCapability>emptyList();
        DEFAULT_MUSICPLAYERS_OBSOLETE = Collections.<String>emptyList();
    }
    
    private DeviceInfo(final Builder builder) {
        this(builder.deviceId, builder.clientVersion, builder.protocolVersion, builder.deviceName, builder.systemVersion, builder.model, builder.deviceUuid, builder.systemApiLevel, builder.kernelVersion, builder.platform, builder.buildType, builder.deviceMake, builder.forceFullUpdate, builder.legacyCapabilities, builder.musicPlayers_OBSOLETE, builder.capabilities);
        this.setBuilder((Message.Builder)builder);
    }
    
    public DeviceInfo(final String deviceId, final String clientVersion, final String protocolVersion, final String deviceName, final String systemVersion, final String model, final String deviceUuid, final Integer systemApiLevel, final String kernelVersion, final Platform platform, final String buildType, final String deviceMake, final Boolean forceFullUpdate, final List<LegacyCapability> list, final List<String> list2, final Capabilities capabilities) {
        this.deviceId = deviceId;
        this.clientVersion = clientVersion;
        this.protocolVersion = protocolVersion;
        this.deviceName = deviceName;
        this.systemVersion = systemVersion;
        this.model = model;
        this.deviceUuid = deviceUuid;
        this.systemApiLevel = systemApiLevel;
        this.kernelVersion = kernelVersion;
        this.platform = platform;
        this.buildType = buildType;
        this.deviceMake = deviceMake;
        this.forceFullUpdate = forceFullUpdate;
        this.legacyCapabilities = Message.<LegacyCapability>immutableCopyOf(list);
        this.musicPlayers_OBSOLETE = Message.<String>immutableCopyOf(list2);
        this.capabilities = capabilities;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof DeviceInfo)) {
                b = false;
            }
            else {
                final DeviceInfo deviceInfo = (DeviceInfo)o;
                if (!this.equals(this.deviceId, deviceInfo.deviceId) || !this.equals(this.clientVersion, deviceInfo.clientVersion) || !this.equals(this.protocolVersion, deviceInfo.protocolVersion) || !this.equals(this.deviceName, deviceInfo.deviceName) || !this.equals(this.systemVersion, deviceInfo.systemVersion) || !this.equals(this.model, deviceInfo.model) || !this.equals(this.deviceUuid, deviceInfo.deviceUuid) || !this.equals(this.systemApiLevel, deviceInfo.systemApiLevel) || !this.equals(this.kernelVersion, deviceInfo.kernelVersion) || !this.equals(this.platform, deviceInfo.platform) || !this.equals(this.buildType, deviceInfo.buildType) || !this.equals(this.deviceMake, deviceInfo.deviceMake) || !this.equals(this.forceFullUpdate, deviceInfo.forceFullUpdate) || !this.equals(this.legacyCapabilities, deviceInfo.legacyCapabilities) || !this.equals(this.musicPlayers_OBSOLETE, deviceInfo.musicPlayers_OBSOLETE) || !this.equals(this.capabilities, deviceInfo.capabilities)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 1;
        int hashCode2 = 0;
        int hashCode3;
        if ((hashCode3 = this.hashCode) == 0) {
            int hashCode4;
            if (this.deviceId != null) {
                hashCode4 = this.deviceId.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.clientVersion != null) {
                hashCode5 = this.clientVersion.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.protocolVersion != null) {
                hashCode6 = this.protocolVersion.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.deviceName != null) {
                hashCode7 = this.deviceName.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.systemVersion != null) {
                hashCode8 = this.systemVersion.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            int hashCode9;
            if (this.model != null) {
                hashCode9 = this.model.hashCode();
            }
            else {
                hashCode9 = 0;
            }
            int hashCode10;
            if (this.deviceUuid != null) {
                hashCode10 = this.deviceUuid.hashCode();
            }
            else {
                hashCode10 = 0;
            }
            int hashCode11;
            if (this.systemApiLevel != null) {
                hashCode11 = this.systemApiLevel.hashCode();
            }
            else {
                hashCode11 = 0;
            }
            int hashCode12;
            if (this.kernelVersion != null) {
                hashCode12 = this.kernelVersion.hashCode();
            }
            else {
                hashCode12 = 0;
            }
            int hashCode13;
            if (this.platform != null) {
                hashCode13 = this.platform.hashCode();
            }
            else {
                hashCode13 = 0;
            }
            int hashCode14;
            if (this.buildType != null) {
                hashCode14 = this.buildType.hashCode();
            }
            else {
                hashCode14 = 0;
            }
            int hashCode15;
            if (this.deviceMake != null) {
                hashCode15 = this.deviceMake.hashCode();
            }
            else {
                hashCode15 = 0;
            }
            int hashCode16;
            if (this.forceFullUpdate != null) {
                hashCode16 = this.forceFullUpdate.hashCode();
            }
            else {
                hashCode16 = 0;
            }
            int hashCode17;
            if (this.legacyCapabilities != null) {
                hashCode17 = this.legacyCapabilities.hashCode();
            }
            else {
                hashCode17 = 1;
            }
            if (this.musicPlayers_OBSOLETE != null) {
                hashCode = this.musicPlayers_OBSOLETE.hashCode();
            }
            if (this.capabilities != null) {
                hashCode2 = this.capabilities.hashCode();
            }
            hashCode3 = ((((((((((((((hashCode4 * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode9) * 37 + hashCode10) * 37 + hashCode11) * 37 + hashCode12) * 37 + hashCode13) * 37 + hashCode14) * 37 + hashCode15) * 37 + hashCode16) * 37 + hashCode17) * 37 + hashCode) * 37 + hashCode2;
            this.hashCode = hashCode3;
        }
        return hashCode3;
    }
    
    public static final class Builder extends Message.Builder<DeviceInfo>
    {
        public String buildType;
        public Capabilities capabilities;
        public String clientVersion;
        public String deviceId;
        public String deviceMake;
        public String deviceName;
        public String deviceUuid;
        public Boolean forceFullUpdate;
        public String kernelVersion;
        public List<LegacyCapability> legacyCapabilities;
        public String model;
        public List<String> musicPlayers_OBSOLETE;
        public Platform platform;
        public String protocolVersion;
        public Integer systemApiLevel;
        public String systemVersion;
        
        public Builder() {
        }
        
        public Builder(final DeviceInfo deviceInfo) {
            super(deviceInfo);
            if (deviceInfo != null) {
                this.deviceId = deviceInfo.deviceId;
                this.clientVersion = deviceInfo.clientVersion;
                this.protocolVersion = deviceInfo.protocolVersion;
                this.deviceName = deviceInfo.deviceName;
                this.systemVersion = deviceInfo.systemVersion;
                this.model = deviceInfo.model;
                this.deviceUuid = deviceInfo.deviceUuid;
                this.systemApiLevel = deviceInfo.systemApiLevel;
                this.kernelVersion = deviceInfo.kernelVersion;
                this.platform = deviceInfo.platform;
                this.buildType = deviceInfo.buildType;
                this.deviceMake = deviceInfo.deviceMake;
                this.forceFullUpdate = deviceInfo.forceFullUpdate;
                this.legacyCapabilities = (List<LegacyCapability>)Message.<Object>copyOf((List<Object>)deviceInfo.legacyCapabilities);
                this.musicPlayers_OBSOLETE = (List<String>)Message.<Object>copyOf((List<Object>)deviceInfo.musicPlayers_OBSOLETE);
                this.capabilities = deviceInfo.capabilities;
            }
        }
        
        public DeviceInfo build() {
            ((Message.Builder)this).checkRequiredFields();
            return new DeviceInfo(this, null);
        }
        
        public Builder buildType(final String buildType) {
            this.buildType = buildType;
            return this;
        }
        
        public Builder capabilities(final Capabilities capabilities) {
            this.capabilities = capabilities;
            return this;
        }
        
        public Builder clientVersion(final String clientVersion) {
            this.clientVersion = clientVersion;
            return this;
        }
        
        public Builder deviceId(final String deviceId) {
            this.deviceId = deviceId;
            return this;
        }
        
        public Builder deviceMake(final String deviceMake) {
            this.deviceMake = deviceMake;
            return this;
        }
        
        public Builder deviceName(final String deviceName) {
            this.deviceName = deviceName;
            return this;
        }
        
        public Builder deviceUuid(final String deviceUuid) {
            this.deviceUuid = deviceUuid;
            return this;
        }
        
        public Builder forceFullUpdate(final Boolean forceFullUpdate) {
            this.forceFullUpdate = forceFullUpdate;
            return this;
        }
        
        public Builder kernelVersion(final String kernelVersion) {
            this.kernelVersion = kernelVersion;
            return this;
        }
        
        @Deprecated
        public Builder legacyCapabilities(final List<LegacyCapability> list) {
            this.legacyCapabilities = Message.Builder.<LegacyCapability>checkForNulls(list);
            return this;
        }
        
        public Builder model(final String model) {
            this.model = model;
            return this;
        }
        
        @Deprecated
        public Builder musicPlayers_OBSOLETE(final List<String> list) {
            this.musicPlayers_OBSOLETE = Message.Builder.<String>checkForNulls(list);
            return this;
        }
        
        public Builder platform(final Platform platform) {
            this.platform = platform;
            return this;
        }
        
        public Builder protocolVersion(final String protocolVersion) {
            this.protocolVersion = protocolVersion;
            return this;
        }
        
        public Builder systemApiLevel(final Integer systemApiLevel) {
            this.systemApiLevel = systemApiLevel;
            return this;
        }
        
        public Builder systemVersion(final String systemVersion) {
            this.systemVersion = systemVersion;
            return this;
        }
    }
    
    public enum Platform implements ProtoEnum
    {
        PLATFORM_Android(2), 
        PLATFORM_iOS(1);
        
        private final int value;
        
        private Platform(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
