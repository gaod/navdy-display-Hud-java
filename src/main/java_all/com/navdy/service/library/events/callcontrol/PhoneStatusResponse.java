package com.navdy.service.library.events.callcontrol;

import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;

public final class PhoneStatusResponse extends Message
{
    public static final RequestStatus DEFAULT_STATUS;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 2)
    public final PhoneEvent callStatus;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    
    static {
        DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    }
    
    public PhoneStatusResponse(final RequestStatus status, final PhoneEvent callStatus) {
        this.status = status;
        this.callStatus = callStatus;
    }
    
    private PhoneStatusResponse(final Builder builder) {
        this(builder.status, builder.callStatus);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof PhoneStatusResponse)) {
                b = false;
            }
            else {
                final PhoneStatusResponse phoneStatusResponse = (PhoneStatusResponse)o;
                if (!this.equals(this.status, phoneStatusResponse.status) || !this.equals(this.callStatus, phoneStatusResponse.callStatus)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.status != null) {
                hashCode3 = this.status.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.callStatus != null) {
                hashCode = this.callStatus.hashCode();
            }
            hashCode2 = hashCode3 * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<PhoneStatusResponse>
    {
        public PhoneEvent callStatus;
        public RequestStatus status;
        
        public Builder() {
        }
        
        public Builder(final PhoneStatusResponse phoneStatusResponse) {
            super(phoneStatusResponse);
            if (phoneStatusResponse != null) {
                this.status = phoneStatusResponse.status;
                this.callStatus = phoneStatusResponse.callStatus;
            }
        }
        
        public PhoneStatusResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new PhoneStatusResponse(this, null);
        }
        
        public Builder callStatus(final PhoneEvent callStatus) {
            this.callStatus = callStatus;
            return this;
        }
        
        public Builder status(final RequestStatus status) {
            this.status = status;
            return this;
        }
    }
}
