package com.navdy.service.library.events.audio;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import java.util.List;
import com.squareup.wire.Message;

public final class MusicCollectionRequest extends Message
{
    public static final String DEFAULT_COLLECTIONID = "";
    public static final MusicCollectionSource DEFAULT_COLLECTIONSOURCE;
    public static final MusicCollectionType DEFAULT_COLLECTIONTYPE;
    public static final List<MusicCollectionFilter> DEFAULT_FILTERS;
    public static final MusicCollectionType DEFAULT_GROUPBY;
    public static final Boolean DEFAULT_INCLUDECHARACTERMAP;
    public static final Integer DEFAULT_LIMIT;
    public static final Integer DEFAULT_OFFSET;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String collectionId;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final MusicCollectionSource collectionSource;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final MusicCollectionType collectionType;
    @ProtoField(label = Label.REPEATED, messageType = MusicCollectionFilter.class, tag = 3)
    public final List<MusicCollectionFilter> filters;
    @ProtoField(tag = 8, type = Datatype.ENUM)
    public final MusicCollectionType groupBy;
    @ProtoField(tag = 7, type = Datatype.BOOL)
    public final Boolean includeCharacterMap;
    @ProtoField(tag = 5, type = Datatype.INT32)
    public final Integer limit;
    @ProtoField(tag = 6, type = Datatype.INT32)
    public final Integer offset;
    
    static {
        DEFAULT_COLLECTIONSOURCE = MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
        DEFAULT_COLLECTIONTYPE = MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
        DEFAULT_FILTERS = Collections.<MusicCollectionFilter>emptyList();
        DEFAULT_LIMIT = 0;
        DEFAULT_OFFSET = 0;
        DEFAULT_INCLUDECHARACTERMAP = false;
        DEFAULT_GROUPBY = MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
    }
    
    private MusicCollectionRequest(final Builder builder) {
        this(builder.collectionSource, builder.collectionType, builder.filters, builder.collectionId, builder.limit, builder.offset, builder.includeCharacterMap, builder.groupBy);
        this.setBuilder((Message.Builder)builder);
    }
    
    public MusicCollectionRequest(final MusicCollectionSource collectionSource, final MusicCollectionType collectionType, final List<MusicCollectionFilter> list, final String collectionId, final Integer limit, final Integer offset, final Boolean includeCharacterMap, final MusicCollectionType groupBy) {
        this.collectionSource = collectionSource;
        this.collectionType = collectionType;
        this.filters = Message.<MusicCollectionFilter>immutableCopyOf(list);
        this.collectionId = collectionId;
        this.limit = limit;
        this.offset = offset;
        this.includeCharacterMap = includeCharacterMap;
        this.groupBy = groupBy;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof MusicCollectionRequest)) {
                b = false;
            }
            else {
                final MusicCollectionRequest musicCollectionRequest = (MusicCollectionRequest)o;
                if (!this.equals(this.collectionSource, musicCollectionRequest.collectionSource) || !this.equals(this.collectionType, musicCollectionRequest.collectionType) || !this.equals(this.filters, musicCollectionRequest.filters) || !this.equals(this.collectionId, musicCollectionRequest.collectionId) || !this.equals(this.limit, musicCollectionRequest.limit) || !this.equals(this.offset, musicCollectionRequest.offset) || !this.equals(this.includeCharacterMap, musicCollectionRequest.includeCharacterMap) || !this.equals(this.groupBy, musicCollectionRequest.groupBy)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.collectionSource != null) {
                hashCode3 = this.collectionSource.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.collectionType != null) {
                hashCode4 = this.collectionType.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.filters != null) {
                hashCode5 = this.filters.hashCode();
            }
            else {
                hashCode5 = 1;
            }
            int hashCode6;
            if (this.collectionId != null) {
                hashCode6 = this.collectionId.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.limit != null) {
                hashCode7 = this.limit.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.offset != null) {
                hashCode8 = this.offset.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            int hashCode9;
            if (this.includeCharacterMap != null) {
                hashCode9 = this.includeCharacterMap.hashCode();
            }
            else {
                hashCode9 = 0;
            }
            if (this.groupBy != null) {
                hashCode = this.groupBy.hashCode();
            }
            hashCode2 = ((((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode9) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<MusicCollectionRequest>
    {
        public String collectionId;
        public MusicCollectionSource collectionSource;
        public MusicCollectionType collectionType;
        public List<MusicCollectionFilter> filters;
        public MusicCollectionType groupBy;
        public Boolean includeCharacterMap;
        public Integer limit;
        public Integer offset;
        
        public Builder() {
        }
        
        public Builder(final MusicCollectionRequest musicCollectionRequest) {
            super(musicCollectionRequest);
            if (musicCollectionRequest != null) {
                this.collectionSource = musicCollectionRequest.collectionSource;
                this.collectionType = musicCollectionRequest.collectionType;
                this.filters = (List<MusicCollectionFilter>)Message.<Object>copyOf((List<Object>)musicCollectionRequest.filters);
                this.collectionId = musicCollectionRequest.collectionId;
                this.limit = musicCollectionRequest.limit;
                this.offset = musicCollectionRequest.offset;
                this.includeCharacterMap = musicCollectionRequest.includeCharacterMap;
                this.groupBy = musicCollectionRequest.groupBy;
            }
        }
        
        public MusicCollectionRequest build() {
            return new MusicCollectionRequest(this, null);
        }
        
        public Builder collectionId(final String collectionId) {
            this.collectionId = collectionId;
            return this;
        }
        
        public Builder collectionSource(final MusicCollectionSource collectionSource) {
            this.collectionSource = collectionSource;
            return this;
        }
        
        public Builder collectionType(final MusicCollectionType collectionType) {
            this.collectionType = collectionType;
            return this;
        }
        
        public Builder filters(final List<MusicCollectionFilter> list) {
            this.filters = Message.Builder.<MusicCollectionFilter>checkForNulls(list);
            return this;
        }
        
        public Builder groupBy(final MusicCollectionType groupBy) {
            this.groupBy = groupBy;
            return this;
        }
        
        public Builder includeCharacterMap(final Boolean includeCharacterMap) {
            this.includeCharacterMap = includeCharacterMap;
            return this;
        }
        
        public Builder limit(final Integer limit) {
            this.limit = limit;
            return this;
        }
        
        public Builder offset(final Integer offset) {
            this.offset = offset;
            return this;
        }
    }
}
