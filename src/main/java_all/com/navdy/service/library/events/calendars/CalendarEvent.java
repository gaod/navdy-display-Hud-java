package com.navdy.service.library.events.calendars;

import com.navdy.service.library.events.destination.Destination;
import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class CalendarEvent extends Message
{
    public static final Boolean DEFAULT_ALL_DAY;
    public static final String DEFAULT_CALENDAR_NAME = "";
    public static final Integer DEFAULT_COLOR;
    public static final String DEFAULT_DISPLAY_NAME = "";
    public static final Long DEFAULT_END_TIME;
    public static final String DEFAULT_LOCATION = "";
    public static final Long DEFAULT_START_TIME;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean all_day;
    @ProtoField(tag = 7, type = Datatype.STRING)
    public final String calendar_name;
    @ProtoField(tag = 5, type = Datatype.FIXED32)
    public final Integer color;
    @ProtoField(tag = 8)
    public final Destination destination;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String display_name;
    @ProtoField(tag = 3, type = Datatype.INT64)
    public final Long end_time;
    @ProtoField(tag = 6, type = Datatype.STRING)
    public final String location;
    @ProtoField(tag = 2, type = Datatype.INT64)
    public final Long start_time;
    
    static {
        DEFAULT_START_TIME = 0L;
        DEFAULT_END_TIME = 0L;
        DEFAULT_ALL_DAY = false;
        DEFAULT_COLOR = 0;
    }
    
    private CalendarEvent(final Builder builder) {
        this(builder.display_name, builder.start_time, builder.end_time, builder.all_day, builder.color, builder.location, builder.calendar_name, builder.destination);
        this.setBuilder((Message.Builder)builder);
    }
    
    public CalendarEvent(final String display_name, final Long start_time, final Long end_time, final Boolean all_day, final Integer color, final String location, final String calendar_name, final Destination destination) {
        this.display_name = display_name;
        this.start_time = start_time;
        this.end_time = end_time;
        this.all_day = all_day;
        this.color = color;
        this.location = location;
        this.calendar_name = calendar_name;
        this.destination = destination;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof CalendarEvent)) {
                b = false;
            }
            else {
                final CalendarEvent calendarEvent = (CalendarEvent)o;
                if (!this.equals(this.display_name, calendarEvent.display_name) || !this.equals(this.start_time, calendarEvent.start_time) || !this.equals(this.end_time, calendarEvent.end_time) || !this.equals(this.all_day, calendarEvent.all_day) || !this.equals(this.color, calendarEvent.color) || !this.equals(this.location, calendarEvent.location) || !this.equals(this.calendar_name, calendarEvent.calendar_name) || !this.equals(this.destination, calendarEvent.destination)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.display_name != null) {
                hashCode3 = this.display_name.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.start_time != null) {
                hashCode4 = this.start_time.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.end_time != null) {
                hashCode5 = this.end_time.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.all_day != null) {
                hashCode6 = this.all_day.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.color != null) {
                hashCode7 = this.color.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.location != null) {
                hashCode8 = this.location.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            int hashCode9;
            if (this.calendar_name != null) {
                hashCode9 = this.calendar_name.hashCode();
            }
            else {
                hashCode9 = 0;
            }
            if (this.destination != null) {
                hashCode = this.destination.hashCode();
            }
            hashCode2 = ((((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode9) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<CalendarEvent>
    {
        public Boolean all_day;
        public String calendar_name;
        public Integer color;
        public Destination destination;
        public String display_name;
        public Long end_time;
        public String location;
        public Long start_time;
        
        public Builder() {
        }
        
        public Builder(final CalendarEvent calendarEvent) {
            super(calendarEvent);
            if (calendarEvent != null) {
                this.display_name = calendarEvent.display_name;
                this.start_time = calendarEvent.start_time;
                this.end_time = calendarEvent.end_time;
                this.all_day = calendarEvent.all_day;
                this.color = calendarEvent.color;
                this.location = calendarEvent.location;
                this.calendar_name = calendarEvent.calendar_name;
                this.destination = calendarEvent.destination;
            }
        }
        
        public Builder all_day(final Boolean all_day) {
            this.all_day = all_day;
            return this;
        }
        
        public CalendarEvent build() {
            return new CalendarEvent(this, null);
        }
        
        public Builder calendar_name(final String calendar_name) {
            this.calendar_name = calendar_name;
            return this;
        }
        
        public Builder color(final Integer color) {
            this.color = color;
            return this;
        }
        
        public Builder destination(final Destination destination) {
            this.destination = destination;
            return this;
        }
        
        public Builder display_name(final String display_name) {
            this.display_name = display_name;
            return this;
        }
        
        public Builder end_time(final Long end_time) {
            this.end_time = end_time;
            return this;
        }
        
        public Builder location(final String location) {
            this.location = location;
            return this;
        }
        
        public Builder start_time(final Long start_time) {
            this.start_time = start_time;
            return this;
        }
    }
}
