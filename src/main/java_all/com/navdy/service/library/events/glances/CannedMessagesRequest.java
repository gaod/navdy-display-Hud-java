package com.navdy.service.library.events.glances;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class CannedMessagesRequest extends Message
{
    public static final Long DEFAULT_SERIAL_NUMBER;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.INT64)
    public final Long serial_number;
    
    static {
        DEFAULT_SERIAL_NUMBER = 0L;
    }
    
    private CannedMessagesRequest(final Builder builder) {
        this(builder.serial_number);
        this.setBuilder((Message.Builder)builder);
    }
    
    public CannedMessagesRequest(final Long serial_number) {
        this.serial_number = serial_number;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof CannedMessagesRequest && this.equals(this.serial_number, ((CannedMessagesRequest)o).serial_number));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.serial_number != null) {
                hashCode = this.serial_number.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<CannedMessagesRequest>
    {
        public Long serial_number;
        
        public Builder() {
        }
        
        public Builder(final CannedMessagesRequest cannedMessagesRequest) {
            super(cannedMessagesRequest);
            if (cannedMessagesRequest != null) {
                this.serial_number = cannedMessagesRequest.serial_number;
            }
        }
        
        public CannedMessagesRequest build() {
            return new CannedMessagesRequest(this, null);
        }
        
        public Builder serial_number(final Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }
    }
}
