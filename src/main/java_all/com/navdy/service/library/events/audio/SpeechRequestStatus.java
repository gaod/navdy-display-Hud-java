package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class SpeechRequestStatus extends Message
{
    public static final String DEFAULT_ID = "";
    public static final SpeechRequestStatusType DEFAULT_STATUS;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String id;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final SpeechRequestStatusType status;
    
    static {
        DEFAULT_STATUS = SpeechRequestStatusType.SPEECH_REQUEST_STARTING;
    }
    
    private SpeechRequestStatus(final Builder builder) {
        this(builder.id, builder.status);
        this.setBuilder((Message.Builder)builder);
    }
    
    public SpeechRequestStatus(final String id, final SpeechRequestStatusType status) {
        this.id = id;
        this.status = status;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof SpeechRequestStatus)) {
                b = false;
            }
            else {
                final SpeechRequestStatus speechRequestStatus = (SpeechRequestStatus)o;
                if (!this.equals(this.id, speechRequestStatus.id) || !this.equals(this.status, speechRequestStatus.status)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.id != null) {
                hashCode3 = this.id.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.status != null) {
                hashCode = this.status.hashCode();
            }
            hashCode2 = hashCode3 * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<SpeechRequestStatus>
    {
        public String id;
        public SpeechRequestStatusType status;
        
        public Builder() {
        }
        
        public Builder(final SpeechRequestStatus speechRequestStatus) {
            super(speechRequestStatus);
            if (speechRequestStatus != null) {
                this.id = speechRequestStatus.id;
                this.status = speechRequestStatus.status;
            }
        }
        
        public SpeechRequestStatus build() {
            return new SpeechRequestStatus(this, null);
        }
        
        public Builder id(final String id) {
            this.id = id;
            return this;
        }
        
        public Builder status(final SpeechRequestStatusType status) {
            this.status = status;
            return this;
        }
    }
    
    public enum SpeechRequestStatusType implements ProtoEnum
    {
        SPEECH_REQUEST_STARTING(1), 
        SPEECH_REQUEST_STOPPED(2);
        
        private final int value;
        
        private SpeechRequestStatusType(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
