package com.navdy.service.library.events.notification;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class NotificationsStatusRequest extends Message
{
    public static final NotificationsState DEFAULT_NEWSTATE;
    public static final ServiceType DEFAULT_SERVICE;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final NotificationsState newState;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final ServiceType service;
    
    static {
        DEFAULT_NEWSTATE = NotificationsState.NOTIFICATIONS_ENABLED;
        DEFAULT_SERVICE = ServiceType.SERVICE_ANCS;
    }
    
    public NotificationsStatusRequest(final NotificationsState newState, final ServiceType service) {
        this.newState = newState;
        this.service = service;
    }
    
    private NotificationsStatusRequest(final Builder builder) {
        this(builder.newState, builder.service);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof NotificationsStatusRequest)) {
                b = false;
            }
            else {
                final NotificationsStatusRequest notificationsStatusRequest = (NotificationsStatusRequest)o;
                if (!this.equals(this.newState, notificationsStatusRequest.newState) || !this.equals(this.service, notificationsStatusRequest.service)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.newState != null) {
                hashCode3 = this.newState.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.service != null) {
                hashCode = this.service.hashCode();
            }
            hashCode2 = hashCode3 * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<NotificationsStatusRequest>
    {
        public NotificationsState newState;
        public ServiceType service;
        
        public Builder() {
        }
        
        public Builder(final NotificationsStatusRequest notificationsStatusRequest) {
            super(notificationsStatusRequest);
            if (notificationsStatusRequest != null) {
                this.newState = notificationsStatusRequest.newState;
                this.service = notificationsStatusRequest.service;
            }
        }
        
        public NotificationsStatusRequest build() {
            return new NotificationsStatusRequest(this, null);
        }
        
        public Builder newState(final NotificationsState newState) {
            this.newState = newState;
            return this;
        }
        
        public Builder service(final ServiceType service) {
            this.service = service;
            return this;
        }
    }
}
