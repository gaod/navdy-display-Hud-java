package com.navdy.service.library.events.navigation;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class RouteManeuver extends Message
{
    public static final String DEFAULT_CURRENTROAD = "";
    public static final Float DEFAULT_DISTANCETOPENDINGROAD;
    public static final DistanceUnit DEFAULT_DISTANCETOPENDINGROADUNIT;
    public static final Integer DEFAULT_MANEUVERTIME;
    public static final String DEFAULT_PENDINGROAD = "";
    public static final NavigationTurn DEFAULT_PENDINGTURN;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String currentRoad;
    @ProtoField(tag = 4, type = Datatype.FLOAT)
    public final Float distanceToPendingRoad;
    @ProtoField(tag = 5, type = Datatype.ENUM)
    public final DistanceUnit distanceToPendingRoadUnit;
    @ProtoField(tag = 6, type = Datatype.UINT32)
    public final Integer maneuverTime;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String pendingRoad;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final NavigationTurn pendingTurn;
    
    static {
        DEFAULT_PENDINGTURN = NavigationTurn.NAV_TURN_START;
        DEFAULT_DISTANCETOPENDINGROAD = 0.0f;
        DEFAULT_DISTANCETOPENDINGROADUNIT = DistanceUnit.DISTANCE_MILES;
        DEFAULT_MANEUVERTIME = 0;
    }
    
    private RouteManeuver(final Builder builder) {
        this(builder.currentRoad, builder.pendingTurn, builder.pendingRoad, builder.distanceToPendingRoad, builder.distanceToPendingRoadUnit, builder.maneuverTime);
        this.setBuilder((Message.Builder)builder);
    }
    
    public RouteManeuver(final String currentRoad, final NavigationTurn pendingTurn, final String pendingRoad, final Float distanceToPendingRoad, final DistanceUnit distanceToPendingRoadUnit, final Integer maneuverTime) {
        this.currentRoad = currentRoad;
        this.pendingTurn = pendingTurn;
        this.pendingRoad = pendingRoad;
        this.distanceToPendingRoad = distanceToPendingRoad;
        this.distanceToPendingRoadUnit = distanceToPendingRoadUnit;
        this.maneuverTime = maneuverTime;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof RouteManeuver)) {
                b = false;
            }
            else {
                final RouteManeuver routeManeuver = (RouteManeuver)o;
                if (!this.equals(this.currentRoad, routeManeuver.currentRoad) || !this.equals(this.pendingTurn, routeManeuver.pendingTurn) || !this.equals(this.pendingRoad, routeManeuver.pendingRoad) || !this.equals(this.distanceToPendingRoad, routeManeuver.distanceToPendingRoad) || !this.equals(this.distanceToPendingRoadUnit, routeManeuver.distanceToPendingRoadUnit) || !this.equals(this.maneuverTime, routeManeuver.maneuverTime)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.currentRoad != null) {
                hashCode3 = this.currentRoad.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.pendingTurn != null) {
                hashCode4 = this.pendingTurn.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.pendingRoad != null) {
                hashCode5 = this.pendingRoad.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.distanceToPendingRoad != null) {
                hashCode6 = this.distanceToPendingRoad.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.distanceToPendingRoadUnit != null) {
                hashCode7 = this.distanceToPendingRoadUnit.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            if (this.maneuverTime != null) {
                hashCode = this.maneuverTime.hashCode();
            }
            hashCode2 = ((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<RouteManeuver>
    {
        public String currentRoad;
        public Float distanceToPendingRoad;
        public DistanceUnit distanceToPendingRoadUnit;
        public Integer maneuverTime;
        public String pendingRoad;
        public NavigationTurn pendingTurn;
        
        public Builder() {
        }
        
        public Builder(final RouteManeuver routeManeuver) {
            super(routeManeuver);
            if (routeManeuver != null) {
                this.currentRoad = routeManeuver.currentRoad;
                this.pendingTurn = routeManeuver.pendingTurn;
                this.pendingRoad = routeManeuver.pendingRoad;
                this.distanceToPendingRoad = routeManeuver.distanceToPendingRoad;
                this.distanceToPendingRoadUnit = routeManeuver.distanceToPendingRoadUnit;
                this.maneuverTime = routeManeuver.maneuverTime;
            }
        }
        
        public RouteManeuver build() {
            return new RouteManeuver(this, null);
        }
        
        public Builder currentRoad(final String currentRoad) {
            this.currentRoad = currentRoad;
            return this;
        }
        
        public Builder distanceToPendingRoad(final Float distanceToPendingRoad) {
            this.distanceToPendingRoad = distanceToPendingRoad;
            return this;
        }
        
        public Builder distanceToPendingRoadUnit(final DistanceUnit distanceToPendingRoadUnit) {
            this.distanceToPendingRoadUnit = distanceToPendingRoadUnit;
            return this;
        }
        
        public Builder maneuverTime(final Integer maneuverTime) {
            this.maneuverTime = maneuverTime;
            return this;
        }
        
        public Builder pendingRoad(final String pendingRoad) {
            this.pendingRoad = pendingRoad;
            return this;
        }
        
        public Builder pendingTurn(final NavigationTurn pendingTurn) {
            this.pendingTurn = pendingTurn;
            return this;
        }
    }
}
