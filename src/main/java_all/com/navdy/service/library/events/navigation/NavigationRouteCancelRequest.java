package com.navdy.service.library.events.navigation;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class NavigationRouteCancelRequest extends Message
{
    public static final String DEFAULT_HANDLE = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String handle;
    
    private NavigationRouteCancelRequest(final Builder builder) {
        this(builder.handle);
        this.setBuilder((Message.Builder)builder);
    }
    
    public NavigationRouteCancelRequest(final String handle) {
        this.handle = handle;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof NavigationRouteCancelRequest && this.equals(this.handle, ((NavigationRouteCancelRequest)o).handle));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.handle != null) {
                hashCode = this.handle.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<NavigationRouteCancelRequest>
    {
        public String handle;
        
        public Builder() {
        }
        
        public Builder(final NavigationRouteCancelRequest navigationRouteCancelRequest) {
            super(navigationRouteCancelRequest);
            if (navigationRouteCancelRequest != null) {
                this.handle = navigationRouteCancelRequest.handle;
            }
        }
        
        public NavigationRouteCancelRequest build() {
            return new NavigationRouteCancelRequest(this, null);
        }
        
        public Builder handle(final String handle) {
            this.handle = handle;
            return this;
        }
    }
}
