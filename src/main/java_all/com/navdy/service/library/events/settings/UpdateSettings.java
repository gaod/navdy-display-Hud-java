package com.navdy.service.library.events.settings;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import java.util.List;
import com.squareup.wire.Message;

public final class UpdateSettings extends Message
{
    public static final List<Setting> DEFAULT_SETTINGS;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1)
    public final ScreenConfiguration screenConfiguration;
    @ProtoField(label = Label.REPEATED, messageType = Setting.class, tag = 2)
    public final List<Setting> settings;
    
    static {
        DEFAULT_SETTINGS = Collections.<Setting>emptyList();
    }
    
    public UpdateSettings(final ScreenConfiguration screenConfiguration, final List<Setting> list) {
        this.screenConfiguration = screenConfiguration;
        this.settings = Message.<Setting>immutableCopyOf(list);
    }
    
    private UpdateSettings(final Builder builder) {
        this(builder.screenConfiguration, builder.settings);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof UpdateSettings)) {
                b = false;
            }
            else {
                final UpdateSettings updateSettings = (UpdateSettings)o;
                if (!this.equals(this.screenConfiguration, updateSettings.screenConfiguration) || !this.equals(this.settings, updateSettings.settings)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode;
        if ((hashCode = this.hashCode) == 0) {
            int hashCode2;
            if (this.screenConfiguration != null) {
                hashCode2 = this.screenConfiguration.hashCode();
            }
            else {
                hashCode2 = 0;
            }
            int hashCode3;
            if (this.settings != null) {
                hashCode3 = this.settings.hashCode();
            }
            else {
                hashCode3 = 1;
            }
            hashCode = hashCode2 * 37 + hashCode3;
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<UpdateSettings>
    {
        public ScreenConfiguration screenConfiguration;
        public List<Setting> settings;
        
        public Builder() {
        }
        
        public Builder(final UpdateSettings updateSettings) {
            super(updateSettings);
            if (updateSettings != null) {
                this.screenConfiguration = updateSettings.screenConfiguration;
                this.settings = (List<Setting>)Message.<Object>copyOf((List<Object>)updateSettings.settings);
            }
        }
        
        public UpdateSettings build() {
            return new UpdateSettings(this, null);
        }
        
        public Builder screenConfiguration(final ScreenConfiguration screenConfiguration) {
            this.screenConfiguration = screenConfiguration;
            return this;
        }
        
        public Builder settings(final List<Setting> list) {
            this.settings = Message.Builder.<Setting>checkForNulls(list);
            return this;
        }
    }
}
