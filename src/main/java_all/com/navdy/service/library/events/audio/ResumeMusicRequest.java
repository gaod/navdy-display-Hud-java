package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;

public final class ResumeMusicRequest extends Message
{
    private static final long serialVersionUID = 0L;
    
    public ResumeMusicRequest() {
    }
    
    private ResumeMusicRequest(final Builder builder) {
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof ResumeMusicRequest;
    }
    
    @Override
    public int hashCode() {
        return 0;
    }
    
    public static final class Builder extends Message.Builder<ResumeMusicRequest>
    {
        public Builder() {
        }
        
        public Builder(final ResumeMusicRequest resumeMusicRequest) {
            super(resumeMusicRequest);
        }
        
        public ResumeMusicRequest build() {
            return new ResumeMusicRequest(this, null);
        }
    }
}
