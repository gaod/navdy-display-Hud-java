package com.navdy.service.library.events.debug;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class StartDriveRecordingEvent extends Message
{
    public static final String DEFAULT_LABEL = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String label;
    
    private StartDriveRecordingEvent(final Builder builder) {
        this(builder.label);
        this.setBuilder((Message.Builder)builder);
    }
    
    public StartDriveRecordingEvent(final String label) {
        this.label = label;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof StartDriveRecordingEvent && this.equals(this.label, ((StartDriveRecordingEvent)o).label));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.label != null) {
                hashCode = this.label.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<StartDriveRecordingEvent>
    {
        public String label;
        
        public Builder() {
        }
        
        public Builder(final StartDriveRecordingEvent startDriveRecordingEvent) {
            super(startDriveRecordingEvent);
            if (startDriveRecordingEvent != null) {
                this.label = startDriveRecordingEvent.label;
            }
        }
        
        public StartDriveRecordingEvent build() {
            ((Message.Builder)this).checkRequiredFields();
            return new StartDriveRecordingEvent(this, null);
        }
        
        public Builder label(final String label) {
            this.label = label;
            return this;
        }
    }
}
