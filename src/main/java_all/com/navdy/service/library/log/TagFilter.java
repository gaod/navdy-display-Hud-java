package com.navdy.service.library.log;

import android.text.TextUtils;
import java.util.regex.Pattern;

public class TagFilter implements Filter
{
    private boolean invert;
    private Pattern pattern;
    
    public TagFilter(final String s) {
        this(s, 0, false);
    }
    
    public TagFilter(final String s, final int n, final boolean invert) {
        this.invert = false;
        this.pattern = Pattern.compile(s, n);
        this.invert = invert;
    }
    
    public TagFilter(final Pattern pattern, final boolean invert) {
        this.invert = false;
        this.pattern = pattern;
        this.invert = invert;
    }
    
    public static TagFilter block(final String... array) {
        return new TagFilter(startsWithAny(array), 0, true);
    }
    
    public static TagFilter pass(final String... array) {
        return new TagFilter(startsWithAny(array));
    }
    
    public static String startsWithAny(final String... array) {
        final StringBuilder sb = new StringBuilder("^(?:");
        sb.append(TextUtils.join((CharSequence)"|", (Object[])array));
        sb.append(")");
        return sb.toString();
    }
    
    @Override
    public boolean matches(final int n, final String s, final String s2) {
        boolean find = this.pattern.matcher(s).find();
        if (this.invert) {
            find = !find;
        }
        return find;
    }
}
