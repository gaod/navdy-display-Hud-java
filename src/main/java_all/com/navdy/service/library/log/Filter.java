package com.navdy.service.library.log;

public interface Filter
{
    boolean matches(final int p0, final String p1, final String p2);
}
