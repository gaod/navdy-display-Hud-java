package com.navdy.service.library.network;

import android.bluetooth.BluetoothDevice;
import com.navdy.service.library.device.NavdyDeviceId;
import java.io.OutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.io.IOException;
import com.navdy.service.library.util.IOUtils;
import android.os.Build;
import android.bluetooth.BluetoothSocket;
import android.os.ParcelFileDescriptor;
import com.navdy.service.library.log.Logger;

public class BTSocketAdapter implements SocketAdapter
{
    private static final boolean needsPatch;
    private static Logger sLogger;
    private boolean closed;
    private int mFd;
    private ParcelFileDescriptor mPfd;
    final BluetoothSocket socket;
    
    static {
        BTSocketAdapter.sLogger = new Logger(BTSocketAdapter.class);
        needsPatch = (Build$VERSION.SDK_INT >= 17 && Build$VERSION.SDK_INT <= 20);
    }
    
    public BTSocketAdapter(final BluetoothSocket socket) {
        this.mFd = -1;
        this.closed = false;
        if (socket == null) {
            throw new IllegalArgumentException("Socket must not be null");
        }
        this.socket = socket;
        if (BTSocketAdapter.needsPatch) {
            this.storeFd();
        }
    }
    
    private void cleanupFd() throws IOException {
        synchronized (this) {
            if (this.mPfd != null) {
                BTSocketAdapter.sLogger.d("Closing mPfd");
                this.mPfd.close();
                this.mPfd = null;
            }
            if (this.mFd != -1) {
                BTSocketAdapter.sLogger.d("Closing mFd:" + this.mFd);
                IOUtils.closeFD(this.mFd);
                this.mFd = -1;
            }
        }
    }
    
    private void storeFd() {
        synchronized (this) {
            if (this.mPfd == null || this.mFd == -1) {
                try {
                    final Field declaredField = this.socket.getClass().getDeclaredField("mPfd");
                    declaredField.setAccessible(true);
                    final ParcelFileDescriptor mPfd = (ParcelFileDescriptor)declaredField.get(this.socket);
                    if (mPfd != null) {
                        this.mPfd = mPfd;
                    }
                    final int socketFD = IOUtils.getSocketFD(this.socket);
                    if (socketFD != -1) {
                        this.mFd = socketFD;
                    }
                    BTSocketAdapter.sLogger.d("Stored " + this.mPfd + " fd:" + this.mFd);
                }
                catch (Exception ex) {
                    BTSocketAdapter.sLogger.w("Exception storing socket internals", ex);
                }
            }
        }
    }
    
    @Override
    public void close() throws IOException {
        final BluetoothSocket socket = this.socket;
        synchronized (socket) {
            if (!this.closed) {
                if (BTSocketAdapter.needsPatch) {
                    this.storeFd();
                }
                this.socket.close();
                if (BTSocketAdapter.needsPatch) {
                    this.cleanupFd();
                }
                this.closed = true;
            }
        }
    }
    
    @Override
    public void connect() throws IOException {
        try {
            this.socket.connect();
            if (BTSocketAdapter.needsPatch) {
                this.storeFd();
            }
        }
        catch (IOException ex) {
            if (BTSocketAdapter.needsPatch) {
                this.storeFd();
            }
            throw ex;
        }
    }
    
    @Override
    public InputStream getInputStream() throws IOException {
        return this.socket.getInputStream();
    }
    
    @Override
    public OutputStream getOutputStream() throws IOException {
        return this.socket.getOutputStream();
    }
    
    @Override
    public NavdyDeviceId getRemoteDevice() {
        final BluetoothDevice remoteDevice = this.socket.getRemoteDevice();
        NavdyDeviceId navdyDeviceId;
        if (remoteDevice != null) {
            navdyDeviceId = new NavdyDeviceId(remoteDevice);
        }
        else {
            navdyDeviceId = null;
        }
        return navdyDeviceId;
    }
    
    @Override
    public boolean isConnected() {
        return this.socket.isConnected();
    }
}
