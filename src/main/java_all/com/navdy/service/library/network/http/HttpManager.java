package com.navdy.service.library.network.http;

import okhttp3.OkHttpClient;

public class HttpManager implements IHttpManager
{
    protected OkHttpClient mHttpClient;
    
    public HttpManager() {
        this.initHttpClient();
    }
    
    private void initHttpClient() {
        this.mHttpClient = new OkHttpClient.Builder().retryOnConnectionFailure(false).build();
    }
    
    @Override
    public OkHttpClient getClient() {
        return this.mHttpClient;
    }
    
    @Override
    public OkHttpClient.Builder getClientCopy() {
        return this.mHttpClient.newBuilder();
    }
}
