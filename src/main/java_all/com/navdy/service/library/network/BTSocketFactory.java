package com.navdy.service.library.network;

import java.io.IOException;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothAdapter;
import com.navdy.service.library.device.connection.ServiceAddress;
import java.util.UUID;

public class BTSocketFactory implements SocketFactory
{
    private final String address;
    private final boolean secure;
    private final UUID serviceUUID;
    
    public BTSocketFactory(final ServiceAddress serviceAddress) {
        this(serviceAddress.getAddress(), UUID.fromString(serviceAddress.getService()));
    }
    
    public BTSocketFactory(final String s, final UUID uuid) {
        this(s, uuid, true);
    }
    
    public BTSocketFactory(final String address, final UUID serviceUUID, final boolean secure) {
        this.address = address;
        this.serviceUUID = serviceUUID;
        this.secure = secure;
    }
    
    @Override
    public SocketAdapter build() throws IOException {
        final BluetoothDevice remoteDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(this.address);
        BTSocketAdapter btSocketAdapter;
        if (this.secure) {
            btSocketAdapter = new BTSocketAdapter(remoteDevice.createRfcommSocketToServiceRecord(this.serviceUUID));
        }
        else {
            btSocketAdapter = new BTSocketAdapter(remoteDevice.createInsecureRfcommSocketToServiceRecord(this.serviceUUID));
        }
        return btSocketAdapter;
    }
}
