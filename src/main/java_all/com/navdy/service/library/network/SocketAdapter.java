package com.navdy.service.library.network;

import com.navdy.service.library.device.NavdyDeviceId;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.Closeable;

public interface SocketAdapter extends Closeable
{
    void close() throws IOException;
    
    void connect() throws IOException;
    
    InputStream getInputStream() throws IOException;
    
    OutputStream getOutputStream() throws IOException;
    
    NavdyDeviceId getRemoteDevice();
    
    boolean isConnected();
}
