package com.navdy.service.library.network.http;

import okhttp3.OkHttpClient;

public interface IHttpManager
{
    OkHttpClient getClient();
    
    OkHttpClient.Builder getClientCopy();
}
