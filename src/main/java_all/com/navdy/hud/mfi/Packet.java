package com.navdy.hud.mfi;

public class Packet
{
    public byte[] data;
    
    public Packet(final byte[] data) {
        this.data = data;
    }
}
