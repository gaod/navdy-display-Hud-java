package com.navdy.hud.mfi;

public interface LinkPacketReceiver
{
    void queue(final LinkPacket p0);
}
