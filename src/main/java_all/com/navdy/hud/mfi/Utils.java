package com.navdy.hud.mfi;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import android.util.Log;
import android.util.SparseArray;
import java.util.Locale;

public class Utils
{
    public static final String MFI_TAG = "MFi";
    protected static final String TAG = "navdy-mfi-Utils";
    protected static final char[] hexArray;
    
    static {
        hexArray = "0123456789ABCDEF".toCharArray();
    }
    
    public static String bytesToHex(final byte[] array) {
        return bytesToHex(array, false);
    }
    
    public static String bytesToHex(final byte[] array, final boolean b) {
        int n;
        if (b) {
            n = 3;
        }
        else {
            n = 2;
        }
        final char[] array2 = new char[array.length * n];
        for (int i = 0; i < array.length; ++i) {
            final int n2 = array[i] & 0xFF;
            array2[i * n] = Utils.hexArray[n2 >>> 4];
            array2[i * n + 1] = Utils.hexArray[n2 & 0xF];
            if (b) {
                array2[i * n + 2] = 32;
            }
        }
        return new String(array2);
    }
    
    public static String bytesToMacAddress(final byte[] array) {
        return String.format(Locale.US, "%02X:%02X:%02X:%02X:%02X:%02X", array[0], array[1], array[2], array[3], array[4], array[5]);
    }
    
    public static SparseArray<String> getConstantsMap(final Class clazz) {
        return getConstantsMap(clazz, null);
    }
    
    public static SparseArray<String> getConstantsMap(final Class p0, final String p1) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   android/util/SparseArray.<init>:()V
        //     7: astore_2       
        //     8: aload_0        
        //     9: invokevirtual   java/lang/Class.getDeclaredFields:()[Ljava/lang/reflect/Field;
        //    12: astore_0       
        //    13: aload_0        
        //    14: arraylength    
        //    15: istore_3       
        //    16: iconst_0       
        //    17: istore          4
        //    19: iload           4
        //    21: iload_3        
        //    22: if_icmpge       108
        //    25: aload_0        
        //    26: iload           4
        //    28: aaload         
        //    29: astore          5
        //    31: aload           5
        //    33: invokevirtual   java/lang/reflect/Field.getType:()Ljava/lang/Class;
        //    36: getstatic       java/lang/Integer.TYPE:Ljava/lang/Class;
        //    39: invokevirtual   java/lang/Object.equals:(Ljava/lang/Object;)Z
        //    42: ifeq            87
        //    45: aload           5
        //    47: invokevirtual   java/lang/reflect/Field.getModifiers:()I
        //    50: invokestatic    java/lang/reflect/Modifier.isStatic:(I)Z
        //    53: ifeq            87
        //    56: aload_1        
        //    57: ifnull          72
        //    60: aload           5
        //    62: invokevirtual   java/lang/reflect/Field.getName:()Ljava/lang/String;
        //    65: aload_1        
        //    66: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //    69: ifeq            87
        //    72: aload_2        
        //    73: aload           5
        //    75: aconst_null    
        //    76: invokevirtual   java/lang/reflect/Field.getInt:(Ljava/lang/Object;)I
        //    79: aload           5
        //    81: invokevirtual   java/lang/reflect/Field.getName:()Ljava/lang/String;
        //    84: invokevirtual   android/util/SparseArray.put:(ILjava/lang/Object;)V
        //    87: iinc            4, 1
        //    90: goto            19
        //    93: astore          5
        //    95: ldc             "navdy-mfi-Utils"
        //    97: ldc             ""
        //    99: aload           5
        //   101: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   104: pop            
        //   105: goto            87
        //   108: aload_2        
        //   109: areturn        
        //    Signature:
        //  (Ljava/lang/Class;Ljava/lang/String;)Landroid/util/SparseArray<Ljava/lang/String;>;
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                              
        //  -----  -----  -----  -----  ----------------------------------
        //  31     56     93     108    Ljava/lang/IllegalAccessException;
        //  60     72     93     108    Ljava/lang/IllegalAccessException;
        //  72     87     93     108    Ljava/lang/IllegalAccessException;
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static byte[] intsToBytes(final int[] array) {
        final byte[] array2 = new byte[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = (byte)array[i];
        }
        return array2;
    }
    
    public static void logTransfer(final String s, final String s2, final Object... array) {
        if (Log.isLoggable(s, 3)) {
            final boolean loggable = Log.isLoggable(s, 2);
            for (int i = 0; i < array.length; ++i) {
                if (array[i] instanceof byte[]) {
                    final byte[] array2 = (byte[])array[i];
                    String s3;
                    if (loggable) {
                        s3 = bytesToHex(array2, true);
                    }
                    else {
                        s3 = String.format("%d bytes", array2.length);
                    }
                    array[i] = s3;
                }
            }
            Log.d(s, String.format(s2, array));
        }
    }
    
    public static int nibbleToByte(final byte b) {
        int n = b & 0xFF;
        if (n <= 57) {
            n -= 48;
        }
        else {
            n = n - 65 + 10;
        }
        return n;
    }
    
    public static byte[] packUInt16(final int n) {
        return new byte[] { (byte)(n >> 8 & 0xFF), (byte)(n & 0xFF) };
    }
    
    public static byte[] packUInt8(final int n) {
        return new byte[] { (byte)(n & 0xFF) };
    }
    
    public static byte[] parseMACAddress(final String s) {
        final byte[] bytes = s.toUpperCase().getBytes();
        final byte[] array = new byte[6];
        for (int i = 0; i < 6; ++i) {
            array[i] = (byte)(nibbleToByte(bytes[i * 3]) << 4 | nibbleToByte(bytes[i * 3 + 1]));
        }
        return array;
    }
    
    public static String toASCII(final byte[] array) {
        String string = "";
        for (int i = 0; i < array.length; ++i) {
            final int n = array[i] & 0xFF;
            final StringBuilder append = new StringBuilder().append(string);
            char c;
            if (n >= 32 && n < 128) {
                c = (char)n;
            }
            else {
                c = '.';
            }
            string = append.append(c).toString();
        }
        return string;
    }
    
    public static long unpackInt64(final byte[] array, final int n) {
        return ByteBuffer.wrap(array, n, array.length - n).getLong();
    }
    
    public static int unpackUInt16(final byte[] array, final int n) {
        return (array[n] & 0xFF) << 8 | (array[n + 1] & 0xFF);
    }
    
    public static long unpackUInt32(final byte[] array, final int n) {
        return (array[n] & 0xFF) << 24 | (array[n + 1] & 0xFF) << 16 | (array[n + 2] & 0xFF) << 8 | (array[n + 3] & 0xFF);
    }
    
    public static BigInteger unpackUInt64(final byte[] array, final int n) {
        return new BigInteger(new byte[] { (byte)(array[n] & 0xFF), (byte)(array[n + 1] & 0xFF), (byte)(array[n + 2] & 0xFF), (byte)(array[n + 3] & 0xFF), (byte)(array[n + 4] & 0xFF), (byte)(array[n + 5] & 0xFF), (byte)(array[n + 6] & 0xFF), (byte)(array[n + 7] & 0xFF) });
    }
    
    public static int unpackUInt8(final byte[] array, final int n) {
        return array[n] & 0xFF;
    }
}
