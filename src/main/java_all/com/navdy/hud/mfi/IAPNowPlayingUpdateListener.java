package com.navdy.hud.mfi;

public interface IAPNowPlayingUpdateListener
{
    void onNowPlayingUpdate(final NowPlayingUpdate p0);
}
