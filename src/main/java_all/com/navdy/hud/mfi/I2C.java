package com.navdy.hud.mfi;

import java.io.IOException;

public class I2C
{
    public static final int MAX_BLOCK_SIZE = 32;
    private int mBus;
    private int mFD;
    
    static {
        System.loadLibrary("iap2");
        classInitNative();
    }
    
    public I2C(final int mBus) {
        this.mFD = -1;
        this.mBus = mBus;
    }
    
    private static native void classInitNative();
    
    private native void native_close();
    
    private native int native_open(final int p0);
    
    private native int native_read(final int p0, final int p1, final byte[] p2, final int p3, final int p4);
    
    private native int native_write(final int p0, final int p1, final byte[] p2, final int p3, final int p4);
    
    public void close() {
        if (this.mFD >= 0) {
            this.native_close();
        }
    }
    
    public void open() throws IOException {
        this.native_open(this.mBus);
        if (this.mFD < 0) {
            throw new IOException("Failed to open i2c device");
        }
    }
    
    public int read(final int n, final int n2, final byte[] array) {
        return this.read(n, n2, array, 0, array.length);
    }
    
    public int read(final int n, final int n2, final byte[] array, final int n3, final int n4) {
        return this.native_read(n, n2, array, n3, n4);
    }
    
    public int readByte(final int n, final int n2) throws IOException {
        final byte[] array = { 0 };
        if (this.read(n, n2, array) != 0) {
            throw new IOException("Unable to read byte");
        }
        return array[0] & 0xFF;
    }
    
    public int readShort(final int n, final int n2) throws IOException {
        final byte[] array = new byte[2];
        if (this.read(n, n2, array) != 0) {
            throw new IOException("Unable to read short");
        }
        return (array[0] & 0xFF) << 8 | (array[1] & 0xFF);
    }
    
    public int write(final int n, final int n2, final byte[] array, final int n3) {
        return this.native_write(n, n2, array, 0, n3);
    }
    
    public int write(final int n, final int n2, final byte[] array, final int n3, final int n4) {
        return this.native_write(n, n2, array, 0, n4);
    }
    
    public int writeByte(final int n, final int n2, final int n3) {
        final byte[] array = { (byte)n3 };
        return this.write(n, n2, array, array.length);
    }
    
    public int writeShort(final int n, final int n2, final int n3) {
        final byte[] array = { (byte)(n3 >> 8 & 0xFF), (byte)(n3 & 0xFF) };
        return this.write(n, n2, array, array.length);
    }
}
