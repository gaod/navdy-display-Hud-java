package com.navdy.hud.app.maps.here;

import java.util.Calendar;
import java.util.StringTokenizer;
import org.json.JSONArray;
import java.util.ArrayList;
import org.json.JSONObject;
import java.util.List;
import java.util.Date;
import com.navdy.service.library.log.Logger;

public class HereOfflineMapsVersion
{
    static final Logger sLogger;
    private Date date;
    private List<String> packages;
    private String rawDate;
    private String version;
    
    static {
        sLogger = new Logger(HereOfflineMapsVersion.class);
    }
    
    public HereOfflineMapsVersion(final String s) {
        try {
            final JSONObject jsonObject = new JSONObject(s);
            this.rawDate = jsonObject.getString("version");
            this.date = this.parseMapDate(this.rawDate);
            this.version = jsonObject.getString("here_map_version");
            final JSONArray jsonArray = jsonObject.getJSONArray("map_packages");
            final int length = jsonArray.length();
            this.packages = new ArrayList<String>(length);
            for (int i = 0; i < length; ++i) {
                this.packages.add(jsonArray.getString(i));
            }
        }
        catch (Exception ex) {
            HereOfflineMapsVersion.sLogger.e("Failed to parse off-line maps version info", ex);
        }
    }
    
    private Date parseMapDate(final String s) throws Exception {
        int int1 = -1;
        int int2 = -1;
        int int3 = -1;
        final StringTokenizer stringTokenizer = new StringTokenizer(s, ".");
        while (stringTokenizer.hasMoreElements()) {
            final String nextToken = stringTokenizer.nextToken();
            if (int1 == -1) {
                int1 = Integer.parseInt(nextToken);
            }
            else if (int2 == -1) {
                int2 = Integer.parseInt(nextToken);
            }
            else {
                int3 = Integer.parseInt(nextToken);
            }
        }
        Date time;
        if (int1 == -1 || int2 == -1 || int3 == -1) {
            time = null;
        }
        else {
            final Calendar instance = Calendar.getInstance();
            instance.set(1, int1);
            instance.set(2, int2 - 1);
            instance.set(5, int3);
            time = instance.getTime();
        }
        return time;
    }
    
    public Date getDate() {
        return this.date;
    }
    
    public List<String> getPackages() {
        return this.packages;
    }
    
    public String getRawDate() {
        return this.rawDate;
    }
    
    public String getVersion() {
        return this.version;
    }
}
