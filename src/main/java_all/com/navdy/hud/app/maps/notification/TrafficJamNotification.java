package com.navdy.hud.app.maps.notification;

import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.ui.framework.UIStateManager;
import android.animation.AnimatorSet;
import android.view.LayoutInflater;
import com.navdy.hud.app.framework.notifications.NotificationType;
import android.view.View;
import android.content.Context;
import com.navdy.hud.app.maps.util.MapUtils;
import android.content.res.Resources;
import com.navdy.hud.app.HudApplication;
import java.util.ArrayList;
import com.navdy.hud.app.maps.MapEvents;
import android.widget.TextView;
import com.navdy.hud.app.view.Gauge;
import android.view.ViewGroup;
import com.squareup.otto.Bus;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import java.util.List;

public class TrafficJamNotification extends BaseTrafficNotification
{
    public static final float REGULAR_SIZE = 64.0f;
    public static final float SMALL_SIZE = 46.0f;
    private static final int TAG_DISMISS = 1;
    private static String delay;
    private static String dismiss;
    private static List<ChoiceLayout2.Choice> dismissChoices;
    private static String hr;
    private static String min;
    private static String trafficJam;
    private Bus bus;
    private ChoiceLayout2.IListener choiceListener;
    private ViewGroup container;
    private Gauge gauge;
    private int initialRemainingTime;
    private int notifColor;
    private TextView title;
    private TextView title1;
    private TextView title2;
    private TextView title3;
    private MapEvents.TrafficJamProgressEvent trafficJamEvent;
    
    static {
        TrafficJamNotification.dismissChoices = new ArrayList<ChoiceLayout2.Choice>(1);
    }
    
    public TrafficJamNotification(final Bus bus, final MapEvents.TrafficJamProgressEvent trafficJamProgressEvent) {
        this.choiceListener = new ChoiceLayout2.IListener() {
            @Override
            public void executeItem(final Selection selection) {
                TrafficJamNotification.this.dismissNotification();
                TrafficJamNotification.this.trafficJamEvent = null;
            }
            
            @Override
            public void itemSelected(final Selection selection) {
            }
        };
        if (TrafficJamNotification.trafficJam == null) {
            final Resources resources = HudApplication.getAppContext().getResources();
            TrafficJamNotification.trafficJam = resources.getString(R.string.traffic_jam_title);
            TrafficJamNotification.delay = resources.getString(R.string.traffic_notification_delay);
            TrafficJamNotification.min = resources.getString(R.string.min);
            TrafficJamNotification.hr = resources.getString(R.string.hr);
            TrafficJamNotification.dismiss = resources.getString(R.string.traffic_notification_dismiss);
            final int color = resources.getColor(R.color.glance_dismiss);
            TrafficJamNotification.dismissChoices.add(new ChoiceLayout2.Choice(1, R.drawable.icon_glances_dismiss, color, R.drawable.icon_glances_dismiss, -16777216, TrafficJamNotification.dismiss, color));
            this.notifColor = resources.getColor(R.color.traffic_bad);
        }
        this.bus = bus;
        this.initialRemainingTime = trafficJamProgressEvent.remainingTime;
        this.logger.v("TrafficJamNotification: initialRemainingTime=" + trafficJamProgressEvent.remainingTime);
    }
    
    private void updateState() {
        if (this.trafficJamEvent != null) {
            this.title.setText((CharSequence)TrafficJamNotification.trafficJam);
            this.title1.setText((CharSequence)TrafficJamNotification.delay);
            if (this.trafficJamEvent.remainingTime >= 3600) {
                this.title2.setTextSize(2, 46.0f);
                this.title3.setText((CharSequence)TrafficJamNotification.hr);
            }
            else {
                this.title2.setTextSize(2, 64.0f);
                this.title3.setText((CharSequence)TrafficJamNotification.min);
            }
            this.title2.setText((CharSequence)MapUtils.formatTime(this.trafficJamEvent.remainingTime));
            this.gauge.setValue(this.trafficJamEvent.remainingTime);
            this.logger.v("TrafficJamNotification: trafficJamEvent.remainingTime=" + this.trafficJamEvent.remainingTime);
            this.choiceLayout.setChoices(TrafficJamNotification.dismissChoices, 0, this.choiceListener, 0.5f);
        }
    }
    
    @Override
    public boolean canAddToStackIfCurrentExists() {
        return true;
    }
    
    @Override
    public boolean expandNotification() {
        return false;
    }
    
    @Override
    public int getColor() {
        return this.notifColor;
    }
    
    @Override
    public View getExpandedView(final Context context, final Object o) {
        return null;
    }
    
    @Override
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    @Override
    public String getId() {
        return "navdy#traffic#jam#notif";
    }
    
    @Override
    public int getTimeout() {
        return 0;
    }
    
    @Override
    public NotificationType getType() {
        return NotificationType.TRAFFIC_JAM;
    }
    
    @Override
    public View getView(final Context context) {
        if (this.container == null) {
            this.container = (ViewGroup)LayoutInflater.from(context).inflate(R.layout.notification_traffic_jam, (ViewGroup)null);
            this.title = (TextView)this.container.findViewById(R.id.title);
            this.title1 = (TextView)this.container.findViewById(R.id.title1);
            this.title2 = (TextView)this.container.findViewById(R.id.title2);
            this.title3 = (TextView)this.container.findViewById(R.id.title3);
            (this.gauge = (Gauge)this.container.findViewById(R.id.traffic_jam_progress)).setMaxValue(this.initialRemainingTime);
            this.choiceLayout = (ChoiceLayout2)this.container.findViewById(R.id.choiceLayout);
        }
        return (View)this.container;
    }
    
    @Override
    public AnimatorSet getViewSwitchAnimation(final boolean b) {
        return null;
    }
    
    @Override
    public boolean isAlive() {
        return true;
    }
    
    @Override
    public boolean isPurgeable() {
        return false;
    }
    
    @Override
    public void onExpandedNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    @Override
    public void onExpandedNotificationSwitched() {
    }
    
    @Override
    public void onNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    @Override
    public void onStart(final INotificationController notificationController) {
        super.onStart(notificationController);
        this.updateState();
    }
    
    @Override
    public void onStop() {
        super.onStop();
    }
    
    @Override
    public void onUpdate() {
        this.updateState();
    }
    
    public void setTrafficEvent(final MapEvents.TrafficJamProgressEvent trafficJamEvent) {
        this.trafficJamEvent = trafficJamEvent;
    }
}
