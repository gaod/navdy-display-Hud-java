package com.navdy.hud.app.maps.here;

import java.util.List;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.routing.Maneuver;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.device.NavdyDeviceId;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.service.library.events.navigation.NavigationSessionRouteChange;
import com.navdy.hud.app.maps.MapEvents;

public class HereNavigationInfo
{
    MapEvents.ManeuverDisplay arrivedManeuverDisplay;
    NavigationSessionRouteChange.RerouteReason currentRerouteReason;
    GeoCoordinate destination;
    MapEvents.DestinationDirection destinationDirection;
    String destinationDirectionStr;
    int destinationIconId;
    public String destinationIdentifier;
    String destinationLabel;
    NavdyDeviceId deviceId;
    boolean firstManeuverShown;
    long firstManeuverShownTime;
    boolean hasArrived;
    boolean ignoreArrived;
    boolean lastManeuver;
    long lastManeuverPostTime;
    NavigationRouteRequest lastRequest;
    String lastRouteId;
    long lastTrafficRerouteTime;
    Maneuver maneuverAfterCurrent;
    int maneuverAfterCurrentIconid;
    HereManeuverDisplayBuilder.ManeuverState maneuverState;
    MapMarker mapDestinationMarker;
    MapRoute mapRoute;
    NavigationRouteRequest navigationRouteRequest;
    Route route;
    String routeId;
    RouteOptions routeOptions;
    int simulationSpeed;
    GeoCoordinate startLocation;
    String streetAddress;
    int trafficRerouteCount;
    boolean trafficRerouteOnce;
    List<GeoCoordinate> waypoints;
    
    public HereNavigationInfo() {
        this.maneuverAfterCurrentIconid = -1;
    }
    
    void clear() {
        if (this.navigationRouteRequest != null) {
            this.lastRequest = this.navigationRouteRequest;
            this.lastRouteId = this.routeId;
        }
        else {
            this.lastRequest = null;
            this.lastRouteId = null;
        }
        this.navigationRouteRequest = null;
        this.routeId = null;
        this.route = null;
        this.simulationSpeed = -1;
        this.streetAddress = null;
        this.destinationLabel = null;
        this.destinationIdentifier = null;
        this.lastManeuverPostTime = 0L;
        this.deviceId = null;
        this.destinationDirection = null;
        this.destinationDirectionStr = null;
        this.destinationIconId = -1;
        this.maneuverAfterCurrent = null;
        this.maneuverAfterCurrentIconid = -1;
        this.hasArrived = false;
        this.ignoreArrived = false;
        this.lastManeuver = false;
        this.arrivedManeuverDisplay = null;
        this.destination = null;
        this.trafficRerouteCount = 0;
        this.lastTrafficRerouteTime = 0L;
        this.currentRerouteReason = null;
        this.trafficRerouteOnce = false;
        this.firstManeuverShown = false;
        this.firstManeuverShownTime = 0L;
        this.routeOptions = null;
        this.startLocation = null;
    }
    
    void copy(final HereNavigationInfo hereNavigationInfo) {
        this.clear();
        if (hereNavigationInfo != null) {
            this.navigationRouteRequest = hereNavigationInfo.navigationRouteRequest;
            if (this.navigationRouteRequest != null) {
                this.destination = new GeoCoordinate(this.navigationRouteRequest.destination.latitude, this.navigationRouteRequest.destination.longitude);
            }
            this.routeId = hereNavigationInfo.routeId;
            this.routeOptions = hereNavigationInfo.routeOptions;
            this.route = hereNavigationInfo.route;
            this.simulationSpeed = hereNavigationInfo.simulationSpeed;
            this.streetAddress = hereNavigationInfo.streetAddress;
            this.destinationLabel = hereNavigationInfo.destinationLabel;
            this.destinationIdentifier = hereNavigationInfo.destinationIdentifier;
            this.deviceId = hereNavigationInfo.deviceId;
            this.destinationDirection = hereNavigationInfo.destinationDirection;
            this.destinationDirectionStr = hereNavigationInfo.destinationDirectionStr;
            this.destinationIconId = hereNavigationInfo.destinationIconId;
            this.maneuverAfterCurrent = hereNavigationInfo.maneuverAfterCurrent;
            this.maneuverAfterCurrentIconid = hereNavigationInfo.maneuverAfterCurrentIconid;
            this.lastRequest = null;
            this.lastRouteId = null;
            this.currentRerouteReason = null;
            this.trafficRerouteOnce = false;
            this.startLocation = hereNavigationInfo.startLocation;
        }
    }
    
    NavigationRouteRequest getLastNavigationRequest() {
        final NavigationRouteRequest lastRequest = this.lastRequest;
        this.lastRequest = null;
        return lastRequest;
    }
    
    String getLastRouteId() {
        final String lastRouteId = this.lastRouteId;
        this.lastRouteId = null;
        return lastRouteId;
    }
    
    long getLastTrafficRerouteTime() {
        return this.lastTrafficRerouteTime;
    }
    
    int getTrafficReRouteCount() {
        return this.trafficRerouteCount;
    }
    
    void incrTrafficRerouteCount() {
        ++this.trafficRerouteCount;
    }
    
    void setLastTrafficRerouteTime(final long lastTrafficRerouteTime) {
        this.lastTrafficRerouteTime = lastTrafficRerouteTime;
    }
}
