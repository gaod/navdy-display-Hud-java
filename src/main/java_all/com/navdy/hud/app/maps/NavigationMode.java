package com.navdy.hud.app.maps;

public enum NavigationMode
{
    MAP, 
    MAP_ON_ROUTE, 
    TBT_ON_ROUTE;
}
