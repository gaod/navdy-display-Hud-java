package com.navdy.hud.app.maps.here;

import com.navdy.hud.app.maps.util.RouteUtils;
import com.navdy.service.library.events.location.Coordinate;
import com.here.android.mpa.routing.Router;
import java.util.EnumSet;
import java.util.Iterator;
import com.navdy.hud.app.maps.MapSettings;
import com.navdy.hud.app.HudApplication;
import java.util.UUID;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import java.util.ArrayList;
import com.navdy.service.library.task.TaskManager;
import com.here.android.mpa.routing.RouteResult;
import com.here.android.mpa.routing.RoutingError;
import android.os.SystemClock;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.DynamicPenalty;
import com.here.android.mpa.routing.RouteWaypoint;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteOptions;
import java.util.List;
import com.here.android.mpa.common.GeoCoordinate;
import android.text.TextUtils;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.log.Logger;
import com.here.android.mpa.routing.CoreRouter;
import java.util.concurrent.atomic.AtomicLong;

public class HereRouteCalculator
{
    private static AtomicLong ID;
    private static final int PROGRESS_INTERVAL_MS = 200;
    private CoreRouter coreRouter;
    private Logger logger;
    private String tag;
    private boolean verbose;
    
    static {
        HereRouteCalculator.ID = new AtomicLong(1L);
    }
    
    public HereRouteCalculator(final Logger logger, final boolean verbose) {
        this.logger = logger;
        this.verbose = verbose;
        this.tag = "[" + HereRouteCalculator.ID.getAndIncrement() + "]";
    }
    
    private boolean isRouteCancelled(final NavigationRouteRequest navigationRouteRequest) {
        boolean b = false;
        if (navigationRouteRequest != null) {
            final String activeRouteCalcId = HereRouteManager.getActiveRouteCalcId();
            if (!TextUtils.equals((CharSequence)navigationRouteRequest.requestId, (CharSequence)activeRouteCalcId)) {
                this.logger.v("route request [" + navigationRouteRequest.requestId + "] is not active anymore, current [" + activeRouteCalcId + "]");
                b = true;
            }
        }
        return b;
    }
    
    public void calculateRoute(final NavigationRouteRequest navigationRouteRequest, final GeoCoordinate geoCoordinate, final List<GeoCoordinate> list, final GeoCoordinate geoCoordinate2, final boolean b, final RouteCalculatorListener routeCalculatorListener, final int n, final RouteOptions routeOptions, final boolean b2) {
        this.calculateRoute(navigationRouteRequest, geoCoordinate, list, geoCoordinate2, b, routeCalculatorListener, n, routeOptions, true, true, b2);
    }
    
    public void calculateRoute(final NavigationRouteRequest navigationRouteRequest, final GeoCoordinate geoCoordinate, final List<GeoCoordinate> list, final GeoCoordinate geoCoordinate2, final boolean b, final RouteCalculatorListener routeCalculatorListener, final int routeCount, final RouteOptions routeOptions, final boolean b2, final boolean b3, final boolean b4) {
        this.coreRouter = new CoreRouter();
        final RoutePlan routePlan = new RoutePlan();
        routeOptions.setRouteCount(routeCount);
        routePlan.setRouteOptions(routeOptions);
        routePlan.addWaypoint(new RouteWaypoint(geoCoordinate));
        if (list != null) {
            final Iterator<GeoCoordinate> iterator = list.iterator();
            while (iterator.hasNext()) {
                routePlan.addWaypoint(new RouteWaypoint(iterator.next()));
            }
        }
        routePlan.addWaypoint(new RouteWaypoint(geoCoordinate2));
        final DynamicPenalty dynamicPenalty = new DynamicPenalty();
        if (b) {
            this.logger.v(this.tag + "Factoring traffic in route calc[ONLINE]");
            dynamicPenalty.setTrafficPenaltyMode(Route.TrafficPenaltyMode.OPTIMAL);
            this.coreRouter.setConnectivity(CoreRouter.Connectivity.ONLINE);
        }
        else {
            this.logger.v(this.tag + "Not factoring traffic in route calc[OFFLINE]");
            dynamicPenalty.setTrafficPenaltyMode(Route.TrafficPenaltyMode.DISABLED);
            this.coreRouter.setConnectivity(CoreRouter.Connectivity.OFFLINE);
        }
        this.coreRouter.setDynamicPenalty(dynamicPenalty);
        this.logger.i(this.tag + "Generating route with " + routePlan.getWaypointCount() + " waypoints");
        this.coreRouter.calculateRoute(routePlan, new CoreRouter.Listener() {
            private int lastPercentage = -1;
            private long lastProgress = 0L;
            final /* synthetic */ long val$startTime = SystemClock.elapsedRealtime();
            
            @Override
            public void onCalculateRouteFinished(final List<RouteResult> list, final RoutingError routingError) {
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                    Label_0694_Outer:
                        while (true) {
                            while (true) {
                                Label_0760: {
                                    long elapsedRealtime = 0L;
                                    ArrayList<NavigationRouteResult> list = null;
                                Label_0650:
                                    while (true) {
                                        Route route;
                                        String string;
                                        int n2;
                                        String string2;
                                        try {
                                            elapsedRealtime = SystemClock.elapsedRealtime();
                                            if ((routingError != RoutingError.NONE && routingError != RoutingError.VIOLATES_OPTIONS) || list.size() <= 0) {
                                                break Label_0760;
                                            }
                                            routeCalculatorListener.preSuccess();
                                            list = new ArrayList<NavigationRouteResult>(list.size());
                                            String requestId = null;
                                            if (b4) {
                                                requestId = requestId;
                                                if (navigationRouteRequest != null) {
                                                    requestId = navigationRouteRequest.requestId;
                                                }
                                            }
                                            final String[] generateVia = HereRouteViaGenerator.generateVia(list, requestId);
                                            final Iterator iterator = list.iterator();
                                            final int n = 0;
                                            if (!iterator.hasNext()) {
                                                break Label_0650;
                                            }
                                            final RouteResult routeResult = iterator.next();
                                            if (b4 && HereRouteCalculator.this.isRouteCancelled(navigationRouteRequest)) {
                                                return;
                                            }
                                            route = routeResult.getRoute();
                                            string = UUID.randomUUID().toString();
                                            n2 = n + 1;
                                            if (TextUtils.isEmpty((CharSequence)(string2 = generateVia[n]))) {
                                                string2 = HudApplication.getAppContext().getResources().getString(R.string.route_num, new Object[] { n2 });
                                            }
                                            final EnumSet<RouteResult.ViolatedOption> violatedOptions = routeResult.getViolatedOptions();
                                            if (violatedOptions != null && violatedOptions.size() > 0) {
                                                final Iterator<Object> iterator2 = violatedOptions.iterator();
                                                while (iterator2.hasNext()) {
                                                    HereRouteCalculator.this.logger.v(HereRouteCalculator.this.tag + " violated options [" + iterator2.next().name() + "]");
                                                }
                                            }
                                        }
                                        catch (Throwable t) {
                                            HereRouteCalculator.this.logger.e(HereRouteCalculator.this.tag, t);
                                            routeCalculatorListener.error(null, t);
                                            return;
                                        }
                                        NavigationRouteResult navigationRouteResult;
                                        if (navigationRouteRequest != null) {
                                            navigationRouteResult = HereRouteCalculator.this.getRouteResultFromRoute(string, route, string2, navigationRouteRequest.label, navigationRouteRequest.streetAddress, b3);
                                        }
                                        else {
                                            navigationRouteResult = HereRouteCalculator.this.getRouteResultFromRoute(string, route, string2, null, null, b3);
                                        }
                                        list.add(navigationRouteResult);
                                        if (b2) {
                                            HereRouteCache.getInstance().addRoute(string, new HereRouteCache.RouteInfo(route, navigationRouteRequest, navigationRouteResult, b, geoCoordinate));
                                        }
                                        if (HereRouteCalculator.this.logger.isLoggable(2)) {
                                            String streetAddress;
                                            if (navigationRouteRequest == null) {
                                                streetAddress = "";
                                            }
                                            else {
                                                streetAddress = navigationRouteRequest.streetAddress;
                                            }
                                            HereMapUtil.printRouteDetails(route, streetAddress, navigationRouteRequest, null);
                                        }
                                        if (MapSettings.isGenerateRouteIcons()) {
                                            HereMapUtil.generateRouteIcons(string, navigationRouteRequest.label, route);
                                        }
                                        final int n = n2;
                                        continue Label_0694_Outer;
                                    }
                                    if (b4 && HereRouteCalculator.this.isRouteCancelled(navigationRouteRequest)) {
                                        return;
                                    }
                                    routeCalculatorListener.postSuccess(list);
                                    HereRouteCalculator.this.logger.v(HereRouteCalculator.this.tag + "handleSearchRequest- time-2=" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                    return;
                                }
                                int size = -1;
                                if (list != null) {
                                    size = list.size();
                                }
                                HereRouteCalculator.this.logger.e(HereRouteCalculator.this.tag + "got error:" + routingError + " results=" + size);
                                routeCalculatorListener.error(routingError, null);
                                continue;
                            }
                        }
                    }
                }, 2);
            }
            
            @Override
            public void onProgress(final int lastPercentage) {
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                if (lastPercentage != this.lastPercentage && (elapsedRealtime > this.lastProgress + 200L || lastPercentage == 100)) {
                    this.lastPercentage = lastPercentage;
                    this.lastProgress = elapsedRealtime;
                    HereRouteCalculator.this.logger.i(HereRouteCalculator.this.tag + "route calculation: [" + lastPercentage + "%] done timeElapsed:" + (elapsedRealtime - this.val$startTime));
                    routeCalculatorListener.progress(lastPercentage);
                }
            }
        });
    }
    
    public void cancel() {
        try {
            if (this.coreRouter != null) {
                this.coreRouter.cancel();
            }
        }
        catch (Throwable t) {
            this.logger.e(this.tag, t);
            this.coreRouter = null;
        }
        finally {
            this.coreRouter = null;
        }
    }
    
    public NavigationRouteResult getRouteResultFromRoute(final String s, final Route route, final String s2, final String s3, final String s4, final boolean b) {
        List<Float> simplify = null;
        ArrayList<Coordinate> list = new ArrayList<Coordinate>();
        if (b) {
            final long elapsedRealtime = SystemClock.elapsedRealtime();
            final List<GeoCoordinate> routeGeometry = route.getRouteGeometry();
            this.logger.v(this.tag + "getting geometry took " + (SystemClock.elapsedRealtime() - elapsedRealtime) + " ms");
            this.logger.v(this.tag + "points before simplification: " + routeGeometry.size());
            simplify = RouteUtils.simplify(routeGeometry);
            this.logger.v(this.tag + "points after simplification: " + simplify.size() / 2);
            final Coordinate coordinate = new Coordinate(routeGeometry.get(0).getLatitude(), routeGeometry.get(0).getLongitude(), null, null, null, null, null, null);
            list = new ArrayList<Coordinate>();
            list.add(coordinate);
        }
        else {
            this.logger.v(this.tag + " poly line calc off");
        }
        final int duration = route.getTta(Route.TrafficPenaltyMode.OPTIMAL, 268435455).getDuration();
        final int length = route.getLength();
        final int duration2 = route.getTta(Route.TrafficPenaltyMode.DISABLED, 268435455).getDuration();
        final NavigationRouteResult navigationRouteResult = new NavigationRouteResult(s, s3, list, length, duration2, duration, simplify, s2, s4);
        this.logger.v(this.tag + "route id[" + s + "] via[" + s2 + "] length[" + length + "] duration[" + duration + "] freeFlowDuration[" + duration2 + "]");
        return navigationRouteResult;
    }
    
    public interface RouteCalculatorListener
    {
        void error(final RoutingError p0, final Throwable p1);
        
        void postSuccess(final ArrayList<NavigationRouteResult> p0);
        
        void preSuccess();
        
        void progress(final int p0);
    }
}
