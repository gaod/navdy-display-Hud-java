package com.navdy.hud.app.maps.here;

import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.service.library.events.audio.SpeechRequest;
import com.navdy.hud.app.framework.phonecall.CallUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.MapsEventHandler;
import com.navdy.service.library.log.Logger;
import android.content.Context;
import com.squareup.otto.Bus;
import com.here.android.mpa.guidance.NavigationManager;

public class HereSpeedWarningListener extends SpeedWarningListener
{
    private static final int BUFFER_SPEED = 7;
    private Bus bus;
    private Context context;
    private HereNavigationManager hereNavigationManager;
    private Logger logger;
    private MapsEventHandler mapsEventHandler;
    private SpeedManager speedManager;
    private boolean speedWarningOn;
    private String tag;
    private boolean verbose;
    private int warningSpeed;
    
    HereSpeedWarningListener(final Logger logger, final String tag, final boolean verbose, final Bus bus, final MapsEventHandler mapsEventHandler, final HereNavigationManager hereNavigationManager) {
        this.context = HudApplication.getAppContext();
        this.speedManager = SpeedManager.getInstance();
        this.logger = logger;
        this.tag = tag;
        this.verbose = verbose;
        this.bus = bus;
        this.mapsEventHandler = mapsEventHandler;
        this.hereNavigationManager = hereNavigationManager;
    }
    
    @Override
    public void onSpeedExceeded(String s, final float n) {
        this.bus.post(HereNavigationManager.SPEED_EXCEEDED);
        this.logger.w(this.tag + " speed exceeded:" + s + "," + n);
        if (!this.speedWarningOn) {
            this.warningSpeed = -1;
        }
        this.speedWarningOn = true;
        if (Boolean.TRUE.equals(MapsEventHandler.getInstance().getNavigationPreferences().spokenSpeedLimitWarnings) && !CallUtils.isPhoneCallInProgress()) {
            final int currentSpeed = this.speedManager.getCurrentSpeed();
            if (currentSpeed <= 0) {
                this.logger.w(this.tag + "no obd or gps speed available");
            }
            else {
                final SpeedManager.SpeedUnit speedUnit = this.speedManager.getSpeedUnit();
                final int convert = SpeedManager.convert(n, SpeedManager.SpeedUnit.METERS_PER_SECOND, speedUnit);
                int n2;
                if (this.warningSpeed == -1) {
                    n2 = convert + 7;
                }
                else {
                    n2 = this.warningSpeed + this.warningSpeed / 10;
                }
                if (currentSpeed >= n2) {
                    this.logger.w(this.tag + " speed exceeded current[" + currentSpeed + "] threshold[" + n2 + "] allowed[" + convert + "] " + speedUnit.name());
                    this.warningSpeed = currentSpeed;
                    switch (speedUnit) {
                        case MILES_PER_HOUR:
                            s = this.hereNavigationManager.TTS_MILES;
                            break;
                        case KILOMETERS_PER_HOUR:
                            s = this.hereNavigationManager.TTS_KMS;
                            break;
                        case METERS_PER_SECOND:
                            s = this.hereNavigationManager.TTS_METERS;
                            break;
                    }
                    TTSUtils.sendSpeechRequest(this.context.getString(R.string.tts_speed_warning, new Object[] { convert }), SpeechRequest.Category.SPEECH_SPEED_WARNING, null);
                }
                else {
                    this.logger.w(this.tag + " speed exceeded current[" + currentSpeed + "] threshold[" + n2 + "] allowed[" + convert + "] " + speedUnit.name());
                }
            }
        }
    }
    
    @Override
    public void onSpeedExceededEnd(final String s, final float n) {
        this.speedWarningOn = false;
        this.warningSpeed = -1;
        this.bus.post(HereNavigationManager.SPEED_NORMAL);
        this.logger.w(this.tag + "speed normal:" + s + "," + n);
    }
}
