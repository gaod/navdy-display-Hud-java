package com.navdy.hud.app.maps.here;

import com.navdy.service.library.task.TaskManager;
import com.here.android.mpa.common.Image;
import java.lang.ref.WeakReference;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.maps.MapEvents;
import com.here.android.mpa.guidance.NavigationManager;

public class HereRealisticViewListener extends RealisticViewListener
{
    private static final MapEvents.HideSignPostJunction HIDE_SIGN_POST_JUNCTION;
    private static final Logger sLogger;
    private final Bus bus;
    private final HereNavController navController;
    private boolean running;
    private Runnable start;
    private Runnable stop;
    
    static {
        sLogger = new Logger(HereRealisticViewListener.class);
        HIDE_SIGN_POST_JUNCTION = new MapEvents.HideSignPostJunction();
    }
    
    HereRealisticViewListener(final Bus bus, final HereNavController navController) {
        this.start = new Runnable() {
            @Override
            public void run() {
                HereRealisticViewListener.this.navController.setRealisticViewMode(RealisticViewMode.NIGHT);
                HereRealisticViewListener.this.navController.addRealisticViewAspectRatio(AspectRatio.AR_3x5);
                HereRealisticViewListener.this.navController.addRealisticViewListener(new WeakReference<RealisticViewListener>(HereRealisticViewListener.this));
                HereRealisticViewListener.sLogger.v("added realistic view listener");
            }
        };
        this.stop = new Runnable() {
            @Override
            public void run() {
                HereRealisticViewListener.this.navController.setRealisticViewMode(RealisticViewMode.OFF);
                HereRealisticViewListener.this.navController.removeRealisticViewListener(HereRealisticViewListener.this);
                HereRealisticViewListener.sLogger.v("removed realistic view listener");
            }
        };
        this.navController = navController;
        this.bus = bus;
    }
    
    private void clearDisplayEvent() {
        this.bus.post(HereRealisticViewListener.HIDE_SIGN_POST_JUNCTION);
    }
    
    @Override
    public void onRealisticViewHide() {
        this.clearDisplayEvent();
    }
    
    @Override
    public void onRealisticViewNextManeuver(final AspectRatio aspectRatio, final Image image, final Image image2) {
    }
    
    @Override
    public void onRealisticViewShow(final AspectRatio aspectRatio, final Image image, final Image image2) {
        this.bus.post(new MapEvents.DisplayJunction(image, image2));
    }
    
    void start() {
        synchronized (this) {
            if (!this.running) {
                TaskManager.getInstance().execute(this.start, 15);
                this.running = true;
                HereRealisticViewListener.sLogger.v("start:added realistic view listener");
            }
        }
    }
    
    void stop() {
        synchronized (this) {
            if (this.running) {
                TaskManager.getInstance().execute(this.stop, 15);
                this.running = false;
                this.clearDisplayEvent();
                HereRealisticViewListener.sLogger.v("stop:remove realistic view listener");
            }
        }
    }
}
