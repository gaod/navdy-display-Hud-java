package com.navdy.hud.app.maps;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.navdy.hud.app.device.gps.GpsDeadReckoningManager;
import com.navdy.hud.app.device.gps.GpsUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.framework.voice.TTSUtils;
import android.content.Intent;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.event.DrivingStateChange;
import android.content.BroadcastReceiver;

public class GpsEventsReceiver extends BroadcastReceiver
{
    private static final DrivingStateChange DRIVING_STARTED;
    private static final DrivingStateChange DRIVING_STOPPED;
    private Logger sLogger;
    
    static {
        DRIVING_STARTED = new DrivingStateChange(true);
        DRIVING_STOPPED = new DrivingStateChange(false);
    }
    
    public GpsEventsReceiver() {
        this.sLogger = new Logger(GpsEventsReceiver.class);
    }
    
    private void reportGpsEvent(final Intent intent) {
        while (true) {
            Label_0499: {
                Label_0464: {
                    Label_0446: {
                        Label_0423: {
                            Label_0399: {
                                Label_0375: {
                                    Bundle extras = null;
                                    Label_0317: {
                                        Label_0311: {
                                            try {
                                                final String action = intent.getAction();
                                                extras = intent.getExtras();
                                                switch (action) {
                                                    case "GPS_DR_STARTED":
                                                        TTSUtils.debugShowDRStarted("");
                                                        break;
                                                    case "GPS_DR_STOPPED":
                                                        break Label_0311;
                                                    case "GPS_Switch":
                                                        break Label_0317;
                                                    case "driving_started":
                                                        break Label_0375;
                                                    case "driving_stopped":
                                                        break Label_0399;
                                                    case "GPS_WARM_RESET_UBLOX":
                                                        break Label_0423;
                                                    case "GPS_ENABLE_ESF_RAW":
                                                        break Label_0446;
                                                    case "GPS_SATELLITE_STATUS":
                                                        break Label_0464;
                                                    case "GPS_COLLECT_LOGS":
                                                        break Label_0499;
                                                }
                                                return;
                                            }
                                            catch (Throwable t) {
                                                this.sLogger.e(t);
                                                return;
                                            }
                                        }
                                        TTSUtils.debugShowDREnded();
                                        return;
                                    }
                                    TTSUtils.debugShowGpsSwitch(extras.getString("title"), extras.getString("info"));
                                    RemoteDeviceManager.getInstance().getBus().post(new GpsUtils.GpsSwitch(extras.getBoolean("phone"), extras.getBoolean("ublox")));
                                    return;
                                }
                                this.sLogger.i("Driving started");
                                RemoteDeviceManager.getInstance().getBus().post(GpsEventsReceiver.DRIVING_STARTED);
                                return;
                            }
                            this.sLogger.i("Driving stopped");
                            RemoteDeviceManager.getInstance().getBus().post(GpsEventsReceiver.DRIVING_STOPPED);
                            return;
                        }
                        this.sLogger.v("warm reset ublox");
                        GpsDeadReckoningManager.getInstance().sendWarmReset();
                        TTSUtils.debugShowGpsReset("Warm Reset Ublox");
                        return;
                    }
                    this.sLogger.v("esf-raw");
                    GpsDeadReckoningManager.getInstance().enableEsfRaw();
                    return;
                }
                final Bundle bundleExtra = intent.getBundleExtra("satellite_data");
                if (bundleExtra != null) {
                    RemoteDeviceManager.getInstance().getBus().post(new GpsUtils.GpsSatelliteData(bundleExtra));
                    return;
                }
                return;
            }
            final String stringExtra = intent.getStringExtra("logPath");
            if (!TextUtils.isEmpty((CharSequence)stringExtra)) {
                GpsDeadReckoningManager.getInstance().dumpGpsInfo(stringExtra);
                return;
            }
            this.sLogger.w("Invalid gps log path:" + stringExtra);
        }
    }
    
    public void onReceive(final Context context, final Intent intent) {
        this.reportGpsEvent(intent);
    }
}
