package com.navdy.hud.app.maps.notification;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.ui.framework.UIStateManager;
import android.animation.ObjectAnimator;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.content.Context;
import android.view.ViewGroup.MarginLayoutParams;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.service.library.events.audio.SpeechRequest;
import android.text.TextUtils;
import android.view.View;
import com.navdy.hud.app.framework.glance.GlanceViewCache;
import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.HudApplication;
import java.util.ArrayList;
import com.navdy.hud.app.framework.notifications.NotificationType;
import android.widget.ImageView;
import android.widget.TextView;
import com.navdy.hud.app.ui.component.image.ColorImageView;
import com.navdy.hud.app.maps.MapEvents;
import android.view.ViewGroup;
import com.navdy.service.library.events.audio.CancelSpeechRequest;
import com.squareup.otto.Bus;
import android.content.res.Resources;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import java.util.List;

public class RouteTimeUpdateNotification extends BaseTrafficNotification
{
    private static final int TAG_DISMISS = 102;
    private static final int TAG_GO = 101;
    private static final int TAG_READ = 103;
    private static StringBuilder ampmMarker;
    private static int badTrafficColor;
    private static String delay;
    private static List<ChoiceLayout2.Choice> delayChoices;
    private static String done;
    private static String fasterRoute;
    private static String fasterRouteAvailable;
    private static String go;
    private static String hour;
    private static String hours;
    private static String hr;
    private static String min;
    private static String minute;
    private static String minutes;
    private static int normalTrafficColor;
    private static List<ChoiceLayout2.Choice> reRouteChoices;
    private static Resources resources;
    private static String save;
    private static int trafficRerouteColor;
    private static String trafficUpdate;
    private static String via;
    private Bus bus;
    private CancelSpeechRequest cancelSpeechRequest;
    private ChoiceLayout2.IListener choiceListener;
    private ViewGroup container;
    private String distanceDiffStr;
    private String distanceStr;
    private String etaStr;
    private String etaUnitStr;
    private ViewGroup extendedContainer;
    private MapEvents.TrafficRerouteEvent fasterRouteEvent;
    private String id;
    private ColorImageView mainImage;
    private TextView mainTitle;
    private ImageView sideImage;
    private TextView subTitle;
    private TextView text1;
    private TextView text2;
    private TextView text3;
    private String timeStr;
    private String timeStrExpanded;
    private String timeUnitStr;
    private MapEvents.TrafficDelayEvent trafficDelayEvent;
    private String ttsMessage;
    private NotificationType type;
    
    static {
        RouteTimeUpdateNotification.delayChoices = new ArrayList<ChoiceLayout2.Choice>(1);
        RouteTimeUpdateNotification.reRouteChoices = new ArrayList<ChoiceLayout2.Choice>(2);
        RouteTimeUpdateNotification.ampmMarker = new StringBuilder();
    }
    
    public RouteTimeUpdateNotification(String string, final NotificationType type, final Bus bus) {
        this.etaStr = "";
        this.etaUnitStr = "";
        this.timeStr = "";
        this.timeUnitStr = "";
        this.timeStrExpanded = "";
        this.distanceStr = "";
        this.distanceDiffStr = "";
        this.choiceListener = new ChoiceLayout2.IListener() {
            @Override
            public void executeItem(final Selection selection) {
                if (RouteTimeUpdateNotification.this.controller != null) {
                    switch (selection.id) {
                        case 5:
                            if (RouteTimeUpdateNotification.this.controller != null && RouteTimeUpdateNotification.this.controller.isExpandedWithStack()) {
                                RouteTimeUpdateNotification.this.controller.collapseNotification(false, false);
                                break;
                            }
                            break;
                        case 101:
                            if (RouteTimeUpdateNotification.this.fasterRouteEvent != null) {
                                RouteTimeUpdateNotification.this.bus.post(MapEvents.TrafficRerouteAction.REROUTE);
                                RouteTimeUpdateNotification.this.fasterRouteEvent = null;
                            }
                            RouteTimeUpdateNotification.this.dismissNotification();
                            break;
                        case 102:
                            if (RouteTimeUpdateNotification.this.fasterRouteEvent != null) {
                                RouteTimeUpdateNotification.this.bus.post(MapEvents.TrafficRerouteAction.DISMISS);
                                RouteTimeUpdateNotification.this.fasterRouteEvent = null;
                            }
                            else if (RouteTimeUpdateNotification.this.trafficDelayEvent != null) {
                                RouteTimeUpdateNotification.this.trafficDelayEvent = null;
                            }
                            RouteTimeUpdateNotification.this.dismissNotification();
                            break;
                        case 103:
                            if (RouteTimeUpdateNotification.this.fasterRouteEvent != null) {
                                RouteTimeUpdateNotification.this.switchToExpandedMode();
                                break;
                            }
                            break;
                    }
                }
            }
            
            @Override
            public void itemSelected(final Selection selection) {
            }
        };
        this.id = string;
        this.type = type;
        this.cancelSpeechRequest = new CancelSpeechRequest(string);
        if (RouteTimeUpdateNotification.trafficUpdate == null) {
            RouteTimeUpdateNotification.resources = HudApplication.getAppContext().getResources();
            RouteTimeUpdateNotification.trafficUpdate = RouteTimeUpdateNotification.resources.getString(R.string.traffic_notification_delay_title);
            RouteTimeUpdateNotification.fasterRoute = RouteTimeUpdateNotification.resources.getString(R.string.traffic_reroute_faster_route);
            RouteTimeUpdateNotification.delay = RouteTimeUpdateNotification.resources.getString(R.string.traffic_notification_delay);
            RouteTimeUpdateNotification.done = RouteTimeUpdateNotification.resources.getString(R.string.done);
            RouteTimeUpdateNotification.min = RouteTimeUpdateNotification.resources.getString(R.string.min);
            RouteTimeUpdateNotification.hr = RouteTimeUpdateNotification.resources.getString(R.string.hr);
            RouteTimeUpdateNotification.fasterRouteAvailable = RouteTimeUpdateNotification.resources.getString(R.string.traffic_faster_route_available);
            RouteTimeUpdateNotification.hour = RouteTimeUpdateNotification.resources.getString(R.string.hour);
            RouteTimeUpdateNotification.hours = RouteTimeUpdateNotification.resources.getString(R.string.hours);
            RouteTimeUpdateNotification.minute = RouteTimeUpdateNotification.resources.getString(R.string.minute);
            RouteTimeUpdateNotification.minutes = RouteTimeUpdateNotification.resources.getString(R.string.minutes);
            RouteTimeUpdateNotification.save = RouteTimeUpdateNotification.resources.getString(R.string.traffic_notification_save);
            RouteTimeUpdateNotification.go = RouteTimeUpdateNotification.resources.getString(R.string.traffic_reroute_go);
            RouteTimeUpdateNotification.via = RouteTimeUpdateNotification.resources.getString(R.string.traffic_reroute_via);
            RouteTimeUpdateNotification.normalTrafficColor = RouteTimeUpdateNotification.resources.getColor(R.color.traffic_good);
            RouteTimeUpdateNotification.badTrafficColor = RouteTimeUpdateNotification.resources.getColor(R.color.traffic_bad);
            RouteTimeUpdateNotification.trafficRerouteColor = RouteTimeUpdateNotification.resources.getColor(R.color.grey_4a);
            final int color = RouteTimeUpdateNotification.resources.getColor(R.color.glance_dismiss);
            final int color2 = RouteTimeUpdateNotification.resources.getColor(R.color.glance_ok_blue);
            final int color3 = RouteTimeUpdateNotification.resources.getColor(R.color.glance_ok_go);
            string = RouteTimeUpdateNotification.resources.getString(R.string.dismiss);
            RouteTimeUpdateNotification.delayChoices.add(new ChoiceLayout2.Choice(102, R.drawable.icon_glances_dismiss, color, R.drawable.icon_glances_dismiss, -16777216, string, color));
            RouteTimeUpdateNotification.reRouteChoices.add(new ChoiceLayout2.Choice(103, R.drawable.icon_glances_read, color2, R.drawable.icon_glances_read, -16777216, GlanceConstants.read, color2));
            RouteTimeUpdateNotification.reRouteChoices.add(new ChoiceLayout2.Choice(101, R.drawable.icon_glances_ok_strong, color3, R.drawable.icon_glances_ok_strong, -16777216, RouteTimeUpdateNotification.go, color3));
            RouteTimeUpdateNotification.reRouteChoices.add(new ChoiceLayout2.Choice(102, R.drawable.icon_glances_dismiss, color, R.drawable.icon_glances_dismiss, -16777216, string, color));
        }
        (this.bus = bus).register(this);
    }
    
    private void buildData() {
        if (this.fasterRouteEvent != null) {
            if (this.fasterRouteEvent.etaDifference != 0L) {
                final long minutes = TimeUnit.SECONDS.toMinutes(this.fasterRouteEvent.etaDifference);
                if (minutes > 99L) {
                    final long hours = TimeUnit.SECONDS.toHours(this.fasterRouteEvent.etaDifference);
                    this.timeStr = String.valueOf(hours);
                    this.timeUnitStr = RouteTimeUpdateNotification.hr;
                    if (hours > 1L) {
                        this.timeStrExpanded = RouteTimeUpdateNotification.hours;
                    }
                    else {
                        this.timeStrExpanded = RouteTimeUpdateNotification.hour;
                    }
                }
                else {
                    this.timeStr = String.valueOf(minutes);
                    this.timeUnitStr = RouteTimeUpdateNotification.min;
                    if (minutes > 1L) {
                        this.timeStrExpanded = RouteTimeUpdateNotification.minutes;
                    }
                    else {
                        this.timeStrExpanded = RouteTimeUpdateNotification.minute;
                    }
                }
            }
            Date date = null;
            if (this.fasterRouteEvent.newEta != 0L) {
                date = new Date(this.fasterRouteEvent.newEta);
            }
            else if (this.fasterRouteEvent.currentEta != 0L) {
                date = new Date(this.fasterRouteEvent.currentEta - this.fasterRouteEvent.etaDifference * 1000L);
            }
            if (date != null) {
                RouteTimeUpdateNotification.ampmMarker.setLength(0);
                this.etaStr = RemoteDeviceManager.getInstance().getTimeHelper().formatTime12Hour(date, RouteTimeUpdateNotification.ampmMarker, false);
                this.etaUnitStr = RouteTimeUpdateNotification.ampmMarker.toString();
            }
            long distanceDifference = this.fasterRouteEvent.distanceDifference;
            if (distanceDifference > 0L) {
                this.distanceDiffStr = RouteTimeUpdateNotification.resources.getString(R.string.longer);
            }
            else {
                this.distanceDiffStr = RouteTimeUpdateNotification.resources.getString(R.string.shorter);
                distanceDifference = -distanceDifference;
            }
            final MapEvents.ManeuverDisplay maneuverDisplay = new MapEvents.ManeuverDisplay();
            HereManeuverDisplayBuilder.setNavigationDistance(distanceDifference, maneuverDisplay, true, true);
            this.distanceStr = maneuverDisplay.distanceToPendingRoadText;
            if (this.fasterRouteEvent.etaDifference != 0L) {
                this.ttsMessage = RouteTimeUpdateNotification.resources.getString(R.string.traffic_faster_route_tts, new Object[] { this.timeStr, this.timeStrExpanded });
            }
            else {
                this.ttsMessage = RouteTimeUpdateNotification.resources.getString(R.string.traffic_faster_route_tts_eta_only);
            }
        }
    }
    
    private void cancelTts() {
        if (this.controller != null && this.controller.isTtsOn()) {
            this.logger.v("tts-cancelled [" + this.id + "]");
            this.bus.post(new RemoteEvent(this.cancelSpeechRequest));
        }
    }
    
    private void cleanupView(final GlanceViewCache.ViewType viewType, final ViewGroup viewGroup) {
        final ViewGroup viewGroup2 = (ViewGroup)viewGroup.getParent();
        if (viewGroup2 != null) {
            viewGroup2.removeView((View)viewGroup);
        }
        switch (viewType) {
            case SMALL_SIGN:
                this.mainTitle.setAlpha(1.0f);
                this.mainTitle.setVisibility(View.VISIBLE);
                this.subTitle.setAlpha(1.0f);
                this.subTitle.setVisibility(View.VISIBLE);
                this.choiceLayout.setAlpha(1.0f);
                this.choiceLayout.setVisibility(View.VISIBLE);
                this.choiceLayout.setTag(null);
                this.sideImage.setImageResource(0);
                viewGroup.setAlpha(1.0f);
                break;
            case BIG_MULTI_TEXT:
                viewGroup.setAlpha(1.0f);
                break;
        }
        GlanceViewCache.putView(viewType, (View)viewGroup);
    }
    
    private void sendtts() {
        if (this.controller != null && this.controller.isTtsOn() && !TextUtils.isEmpty((CharSequence)this.ttsMessage)) {
            TTSUtils.sendSpeechRequest(this.ttsMessage, SpeechRequest.Category.SPEECH_REROUTE, this.id);
        }
    }
    
    private void switchToExpandedMode() {
        this.choiceLayout.setChoices(GlanceConstants.backChoice2, 0, this.choiceListener, 0.5f);
        if (!this.controller.isExpandedWithStack()) {
            this.controller.expandNotification(true);
        }
    }
    
    private void updateState() {
        if (this.controller != null) {
            this.logger.v("updateState");
            if (this.trafficDelayEvent == null && this.fasterRouteEvent != null) {
                this.mainTitle.setText((CharSequence)RouteTimeUpdateNotification.fasterRoute);
                if (this.fasterRouteEvent.via != null) {
                    this.subTitle.setText((CharSequence)(RouteTimeUpdateNotification.via + " " + this.fasterRouteEvent.via));
                }
                else {
                    this.subTitle.setText((CharSequence)"");
                }
                this.mainImage.setColor(RouteTimeUpdateNotification.trafficRerouteColor);
                this.sideImage.setImageResource(R.drawable.icon_badge_route);
                this.buildData();
                if (this.fasterRouteEvent.etaDifference != 0L) {
                    this.text1.setText((CharSequence)RouteTimeUpdateNotification.save);
                    ((ViewGroup.MarginLayoutParams)this.text2.getLayoutParams()).topMargin = GlanceConstants.calendarNormalMargin;
                    this.text2.setTextAppearance(HudApplication.getAppContext(), R.style.small_glance_sign_text_1);
                    this.text2.setText((CharSequence)this.timeStr);
                    this.text3.setText((CharSequence)this.timeUnitStr);
                }
                if (this.controller.isExpandedWithStack()) {
                    this.choiceLayout.setChoices(GlanceConstants.backChoice2, 0, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(GlanceConstants.backChoice);
                }
                else {
                    this.choiceLayout.setChoices(RouteTimeUpdateNotification.reRouteChoices, 1, this.choiceListener, 0.5f);
                }
                if (this.extendedContainer != null && this.controller.isExpandedWithStack()) {
                    this.setExpandedContent(HudApplication.getAppContext());
                }
            }
        }
    }
    
    @Override
    public boolean canAddToStackIfCurrentExists() {
        return true;
    }
    
    @Override
    public boolean expandNotification() {
        boolean b;
        if (this.controller == null) {
            b = false;
        }
        else {
            this.switchToExpandedMode();
            b = true;
        }
        return b;
    }
    
    @Override
    public int getColor() {
        int n;
        if (this.fasterRouteEvent != null) {
            n = GlanceConstants.colorFasterRoute;
        }
        else {
            n = GlanceConstants.colorTrafficDelay;
        }
        return n;
    }
    
    @Override
    public View getExpandedView(final Context expandedContent, final Object o) {
        this.extendedContainer = (ViewGroup)GlanceViewCache.getView(GlanceViewCache.ViewType.BIG_MULTI_TEXT, expandedContent);
        this.setExpandedContent(expandedContent);
        return (View)this.extendedContainer;
    }
    
    @Override
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    @Override
    public String getId() {
        return this.id;
    }
    
    @Override
    public int getTimeout() {
        return 0;
    }
    
    @Override
    public NotificationType getType() {
        return this.type;
    }
    
    @Override
    public View getView(final Context context) {
        if (this.container == null) {
            this.container = (ViewGroup)GlanceViewCache.getView(GlanceViewCache.ViewType.SMALL_SIGN, context);
            this.mainTitle = (TextView)this.container.findViewById(R.id.mainTitle);
            this.subTitle = (TextView)this.container.findViewById(R.id.subTitle);
            this.mainImage = (ColorImageView)this.container.findViewById(R.id.mainImage);
            this.sideImage = (ImageView)this.container.findViewById(R.id.sideImage);
            this.text1 = (TextView)this.container.findViewById(R.id.text1);
            this.text2 = (TextView)this.container.findViewById(R.id.text2);
            this.text3 = (TextView)this.container.findViewById(R.id.text3);
            this.choiceLayout = (ChoiceLayout2)this.container.findViewById(R.id.choiceLayout);
        }
        return (View)this.container;
    }
    
    @Override
    public AnimatorSet getViewSwitchAnimation(final boolean b) {
        final AnimatorSet set = new AnimatorSet();
        if (!b) {
            set.playTogether(new Animator[] { ObjectAnimator.ofFloat(this.mainTitle, View.ALPHA, new float[] { 1.0f, 0.0f }), ObjectAnimator.ofFloat(this.subTitle, View.ALPHA, new float[] { 1.0f, 0.0f }), ObjectAnimator.ofFloat(this.choiceLayout, View.ALPHA, new float[] { 1.0f, 0.0f }) });
        }
        else {
            set.playTogether(new Animator[] { ObjectAnimator.ofFloat(this.mainTitle, View.ALPHA, new float[] { 0.0f, 1.0f }), ObjectAnimator.ofFloat(this.subTitle, View.ALPHA, new float[] { 0.0f, 1.0f }), ObjectAnimator.ofFloat(this.choiceLayout, View.ALPHA, new float[] { 0.0f, 1.0f }) });
        }
        return set;
    }
    
    @Override
    public boolean isAlive() {
        return true;
    }
    
    @Override
    public boolean isPurgeable() {
        return false;
    }
    
    @Override
    public void onExpandedNotificationEvent(final UIStateManager.Mode mode) {
        if (mode == UIStateManager.Mode.COLLAPSE) {
            if (this.controller != null) {
                this.cancelTts();
                if (NotificationManager.getInstance().isNotificationMarkedForRemoval(this.id)) {
                    this.dismissNotification();
                }
                else {
                    this.updateState();
                }
            }
        }
        else {
            this.sendtts();
        }
    }
    
    @Override
    public void onExpandedNotificationSwitched() {
        if (this.controller != null && this.controller.isExpandedWithStack()) {
            this.sendtts();
        }
    }
    
    @Override
    public void onNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    @Override
    public void onStart(final INotificationController notificationController) {
        super.onStart(notificationController);
        this.updateState();
        if (!notificationController.isExpandedWithStack()) {
            this.container.setTranslationX(0.0f);
            this.container.setTranslationY(0.0f);
            this.container.setAlpha(1.0f);
            this.mainTitle.setAlpha(1.0f);
            this.subTitle.setAlpha(1.0f);
            this.choiceLayout.setAlpha(1.0f);
        }
        else {
            this.mainTitle.setAlpha(0.0f);
            this.subTitle.setAlpha(0.0f);
            this.choiceLayout.setAlpha(0.0f);
        }
    }
    
    @Override
    public void onStop() {
        this.cancelTts();
        super.onStop();
        if (this.container != null) {
            this.cleanupView(GlanceViewCache.ViewType.SMALL_SIGN, this.container);
            this.container = null;
        }
        if (this.extendedContainer != null) {
            this.cleanupView(GlanceViewCache.ViewType.BIG_MULTI_TEXT, this.extendedContainer);
            this.extendedContainer = null;
        }
    }
    
    @Override
    public void onUpdate() {
        this.updateState();
    }
    
    public void setDelayEvent(final MapEvents.TrafficDelayEvent trafficDelayEvent) {
        this.trafficDelayEvent = trafficDelayEvent;
    }
    
    void setExpandedContent(final Context context) {
        final TextView textView = (TextView)this.extendedContainer.findViewById(R.id.title1);
        textView.setTextAppearance(context, R.style.glance_title_1);
        final TextView textView2 = (TextView)this.extendedContainer.findViewById(R.id.title2);
        textView2.setTextAppearance(context, R.style.glance_title_2);
        if (this.fasterRouteEvent != null) {
            textView.setText((CharSequence)RouteTimeUpdateNotification.fasterRouteAvailable);
            if (this.timeStr == null) {
                this.buildData();
            }
            String text;
            if (this.fasterRouteEvent.etaDifference != 0L) {
                text = RouteTimeUpdateNotification.resources.getString(R.string.traffic_reroute_msg, new Object[] { this.timeStr, this.timeStrExpanded, this.fasterRouteEvent.additionalVia, this.etaStr, this.etaUnitStr, this.distanceStr, this.distanceDiffStr });
            }
            else {
                text = RouteTimeUpdateNotification.resources.getString(R.string.traffic_reroute_msg_eta_only, new Object[] { this.fasterRouteEvent.additionalVia, this.etaStr, this.etaUnitStr, this.distanceStr, this.distanceDiffStr });
            }
            textView2.setText((CharSequence)text);
        }
        textView.setVisibility(View.VISIBLE);
        textView2.setVisibility(View.VISIBLE);
    }
    
    public void setFasterRouteEvent(final MapEvents.TrafficRerouteEvent fasterRouteEvent) {
        this.fasterRouteEvent = fasterRouteEvent;
    }
}
