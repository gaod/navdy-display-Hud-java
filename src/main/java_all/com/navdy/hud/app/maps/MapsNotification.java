package com.navdy.hud.app.maps;

import com.here.android.mpa.common.OnEngineInitListener;
import android.content.res.Resources;
import com.navdy.hud.app.framework.toast.IToastCallback;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.maps.here.HereMapsManager;
import android.os.Bundle;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.log.Logger;

public class MapsNotification
{
    private static final String MAP_ENGINE_NOT_INIT_ID = "maps-no-init";
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(MapsNotification.class);
    }
    
    public static void showMapsEngineNotInitializedToast() {
        MapsNotification.sLogger.d("maps engine not init toast");
        final Resources resources = HudApplication.getAppContext().getResources();
        final Bundle bundle = new Bundle();
        bundle.putInt("13", 2000);
        bundle.putInt("8", R.drawable.icon_mm_map);
        final OnEngineInitListener.Error error = HereMapsManager.getInstance().getError();
        String name;
        if (error == null) {
            name = "";
        }
        else {
            name = error.name();
        }
        bundle.putString("4", resources.getString(R.string.map_engine_not_ready));
        bundle.putString("6", name);
        ToastManager.getInstance().addToast(new ToastManager.ToastParams("maps-no-init", bundle, null, true, false));
    }
}
