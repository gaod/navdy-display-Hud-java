package com.navdy.hud.app.maps.util;

import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.hud.app.maps.here.HereLocationFixManager;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import android.support.v4.content.ContextCompat;
import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.manager.SpeedManager;
import java.util.ArrayList;
import com.navdy.service.library.events.location.LatLong;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.ui.component.destination.DestinationParcelable;
import com.navdy.service.library.events.destination.Destination;
import java.util.List;
import android.content.Context;

public class DestinationUtil
{
    public static List<DestinationParcelable> convert(final Context context, final List<Destination> list, final int n, final int n2, final boolean b) {
        final HereLocationFixManager locationFixManager = HereMapsManager.getInstance().getLocationFixManager();
        GeoCoordinate lastGeoCoordinate = null;
        LatLong latLong = null;
        if (locationFixManager != null) {
            lastGeoCoordinate = locationFixManager.getLastGeoCoordinate();
        }
        if (lastGeoCoordinate != null) {
            latLong = new LatLong(Double.valueOf(lastGeoCoordinate.getLatitude()), Double.valueOf(lastGeoCoordinate.getLongitude()));
        }
        final ArrayList<DestinationParcelable> list2 = new ArrayList<DestinationParcelable>(list.size());
        for (int i = 0; i < list.size(); ++i) {
            final Destination destination = list.get(i);
            final float n3 = -1.0f;
            double doubleValue = 0.0;
            double doubleValue2 = 0.0;
            double n4 = 0.0;
            double n5 = 0.0;
            final String s = null;
            float n6 = n3;
            if (destination.navigation_position != null) {
                final double doubleValue3 = destination.navigation_position.latitude;
                final double doubleValue4 = destination.navigation_position.longitude;
                if (destination.navigation_position.latitude != 0.0 && destination.navigation_position.longitude != 0.0) {
                    n6 = MapUtils.distanceBetween(destination.navigation_position, latLong);
                    n5 = doubleValue4;
                    n4 = doubleValue3;
                }
                else {
                    n4 = doubleValue3;
                    n5 = doubleValue4;
                    n6 = n3;
                    if (destination.display_position != null) {
                        n4 = doubleValue3;
                        n5 = doubleValue4;
                        n6 = n3;
                        if (destination.display_position.latitude != 0.0) {
                            n4 = doubleValue3;
                            n5 = doubleValue4;
                            n6 = n3;
                            if (destination.display_position.longitude != 0.0) {
                                n6 = MapUtils.distanceBetween(destination.display_position, latLong);
                                n4 = doubleValue3;
                                n5 = doubleValue4;
                            }
                        }
                    }
                }
            }
            String distanceStr = null;
            String string = s;
            if (n6 != -1.0f) {
                final DistanceConverter.Distance distance = new DistanceConverter.Distance();
                DistanceConverter.convertToDistance(SpeedManager.getInstance().getSpeedUnit(), n6, distance);
                final String formattedDistance = HereManeuverDisplayBuilder.getFormattedDistance(distance.value, distance.unit, b);
                final int index = formattedDistance.indexOf(" ");
                string = s;
                distanceStr = formattedDistance;
                if (index != -1) {
                    string = "<b>" + formattedDistance.substring(0, index) + "</b>" + formattedDistance.substring(index);
                    distanceStr = formattedDistance;
                }
            }
            if (destination.display_position != null) {
                doubleValue = destination.display_position.latitude;
                doubleValue2 = destination.display_position.longitude;
            }
            final DestinationPickerScreen.PlaceTypeResourceHolder placeTypeHolder = DestinationPickerScreen.getPlaceTypeHolder(destination.place_type);
            int iconRes;
            int color;
            if (placeTypeHolder != null) {
                iconRes = placeTypeHolder.iconRes;
                color = ContextCompat.getColor(context, placeTypeHolder.colorRes);
            }
            else {
                iconRes = R.drawable.icon_mm_places_2;
                color = n;
            }
            final DestinationParcelable destinationParcelable = new DestinationParcelable(0, destination.destination_title, destination.destination_subtitle, false, string, true, destination.full_address, n4, n5, doubleValue, doubleValue2, iconRes, 0, color, n2, DestinationParcelable.DestinationType.DESTINATION, destination.place_type);
            destinationParcelable.setIdentifier(destination.identifier);
            destinationParcelable.setPlaceId(destination.place_id);
            destinationParcelable.distanceStr = distanceStr;
            destinationParcelable.setContacts(ContactUtil.fromContacts(destination.contacts));
            destinationParcelable.setPhoneNumbers(ContactUtil.fromPhoneNumbers(destination.phoneNumbers));
            list2.add(destinationParcelable);
        }
        return list2;
    }
}
