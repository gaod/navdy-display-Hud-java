package com.navdy.hud.app.maps.here;

import android.content.res.Resources;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.util.GenericUtil;
import java.util.List;
import java.util.Iterator;
import java.util.EnumSet;
import com.navdy.hud.app.maps.MapEvents;
import java.util.ArrayList;
import com.navdy.service.library.log.Logger;
import com.here.android.mpa.guidance.LaneInformation;
import android.graphics.drawable.Drawable;
import java.util.HashMap;

public class HereLaneInfoBuilder
{
    private static boolean initialized;
    private static HashMap<Integer, Drawable> sLaneDrawableCache;
    private static HashMap<LaneInformation.Direction, Integer> sLeftOnlyNotRecommendedDirection;
    private static HashMap<LaneInformation.Direction, Integer> sLeftOnlyRecommendedDirection;
    private static HashMap<LaneInformation.Direction, Integer> sLeftRightNotRecommendedDirection;
    private static HashMap<LaneInformation.Direction, Integer> sLeftRightRecommendedDirection;
    private static final Logger sLogger;
    private static HashMap<LaneInformation.Direction, Integer> sRightOnlyNotRecommendedDirection;
    private static HashMap<LaneInformation.Direction, Integer> sRightOnlyRecommendedDirection;
    private static HashMap<LaneInformation.Direction, Integer> sSingleNotRecommendedDirection;
    private static HashMap<LaneInformation.Direction, Integer> sSingleRecommendedDirection;
    
    static {
        sLogger = new Logger(HereLaneInfoBuilder.class);
    }
    
    public static boolean compareLaneData(final ArrayList<MapEvents.LaneData> list, final ArrayList<MapEvents.LaneData> list2) {
        boolean b2;
        final boolean b = b2 = false;
        if (list != null) {
            b2 = b;
            if (list2 != null) {
                final int size = list.size();
                if (size != list2.size()) {
                    b2 = b;
                }
                else {
                    for (int i = 0; i < size; ++i) {
                        final MapEvents.LaneData laneData = list.get(i);
                        final MapEvents.LaneData laneData2 = list2.get(i);
                        b2 = b;
                        if (laneData == null) {
                            return b2;
                        }
                        b2 = b;
                        if (laneData2 == null) {
                            return b2;
                        }
                        b2 = b;
                        if (laneData.position != laneData2.position) {
                            return b2;
                        }
                        b2 = b;
                        if (laneData.icons == null) {
                            return b2;
                        }
                        b2 = b;
                        if (laneData2.icons == null) {
                            return b2;
                        }
                        b2 = b;
                        if (laneData.icons.length != laneData2.icons.length) {
                            return b2;
                        }
                        for (int j = 0; j < laneData.icons.length; ++j) {
                            b2 = b;
                            if (laneData.icons[j] != laneData2.icons[j]) {
                                return b2;
                            }
                        }
                    }
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    public static LaneInformation.Direction getDirection(final EnumSet<LaneInformation.Direction> set, final DirectionType directionType) {
        for (final LaneInformation.Direction direction : set) {
            switch (direction) {
                default:
                    continue;
                case SLIGHTLY_RIGHT:
                case RIGHT:
                case SHARP_RIGHT:
                    if (directionType == DirectionType.RIGHT) {
                        break;
                    }
                    continue;
                case SHARP_LEFT:
                case LEFT:
                case SLIGHTLY_LEFT:
                    if (directionType == DirectionType.LEFT) {
                        break;
                    }
                    continue;
            }
            return direction;
        }
        return null;
    }
    
    private static DirectionType getDirectionType(final List<LaneInformation.Direction> list) {
        int n = 0;
        int n2 = 0;
        final Iterator<LaneInformation.Direction> iterator = list.iterator();
        while (iterator.hasNext()) {
            switch (iterator.next()) {
                case U_TURN_RIGHT:
                    ++n2;
                    continue;
                case SLIGHTLY_LEFT:
                    ++n;
                    continue;
                case LEFT:
                    ++n;
                    continue;
                case SHARP_LEFT:
                    ++n;
                    continue;
                case U_TURN_LEFT:
                    ++n;
                    continue;
                case SHARP_RIGHT:
                    ++n2;
                    continue;
                case RIGHT:
                    ++n2;
                    continue;
                case SLIGHTLY_RIGHT:
                    ++n2;
                case STRAIGHT:
                default:
                    continue;
            }
        }
        DirectionType directionType;
        if (n > 0 && n2 > 0) {
            directionType = DirectionType.LEFT_RIGHT;
        }
        else if (n > 0) {
            directionType = DirectionType.LEFT;
        }
        else {
            directionType = DirectionType.RIGHT;
        }
        return directionType;
    }
    
    public static Drawable[] getDrawable(final List<LaneInformation.Direction> list, final MapEvents.LaneData.Position position, final LaneInformation.Direction direction) {
        Drawable[] array;
        if (list == null) {
            array = null;
        }
        else {
            Drawable[] array2;
            if (list.size() == 1) {
                array2 = new Drawable[] { getDrawableForDirection(list.get(0), position, HereLaneInfoBuilder.sSingleRecommendedDirection, HereLaneInfoBuilder.sSingleNotRecommendedDirection) };
            }
            else {
                final DirectionType directionType = getDirectionType(list);
                final Drawable[] array3 = new Drawable[list.size()];
                int n = 0;
                for (final LaneInformation.Direction direction2 : list) {
                    Enum<MapEvents.LaneData.Position> enum1;
                    if ((enum1 = position) == MapEvents.LaneData.Position.ON_ROUTE) {
                        enum1 = position;
                        if (direction != null) {
                            if (direction2 == direction) {
                                enum1 = MapEvents.LaneData.Position.ON_ROUTE;
                            }
                            else {
                                enum1 = MapEvents.LaneData.Position.OFF_ROUTE;
                            }
                        }
                    }
                    switch (directionType) {
                        default:
                            continue;
                        case LEFT:
                            array3[n] = getDrawableForDirection(direction2, (MapEvents.LaneData.Position)enum1, HereLaneInfoBuilder.sLeftOnlyRecommendedDirection, HereLaneInfoBuilder.sLeftOnlyNotRecommendedDirection);
                            ++n;
                            continue;
                        case RIGHT:
                            array3[n] = getDrawableForDirection(direction2, (MapEvents.LaneData.Position)enum1, HereLaneInfoBuilder.sRightOnlyRecommendedDirection, HereLaneInfoBuilder.sRightOnlyNotRecommendedDirection);
                            ++n;
                            continue;
                        case LEFT_RIGHT:
                            array3[n] = getDrawableForDirection(direction2, (MapEvents.LaneData.Position)enum1, HereLaneInfoBuilder.sLeftRightRecommendedDirection, HereLaneInfoBuilder.sLeftRightNotRecommendedDirection);
                            ++n;
                            continue;
                    }
                }
                array2 = array3;
            }
            array = array2;
            if (array2 != null) {
                int n2 = 0;
                int n3 = 0;
                for (int i = 0; i < array2.length; ++i) {
                    if (array2[i] == null) {
                        ++n3;
                    }
                    else {
                        ++n2;
                    }
                }
                if (n2 == 0) {
                    array = null;
                }
                else {
                    array = array2;
                    if (n3 > 0) {
                        array = new Drawable[n2];
                        int n4 = 0;
                        int n5;
                        for (int j = 0; j < array2.length; ++j, n4 = n5) {
                            n5 = n4;
                            if (array2[j] != null) {
                                array[n4] = array2[j];
                                n5 = n4 + 1;
                            }
                        }
                    }
                }
            }
        }
        return array;
    }
    
    private static Drawable getDrawableForDirection(final LaneInformation.Direction direction, final MapEvents.LaneData.Position position, final HashMap<LaneInformation.Direction, Integer> hashMap, final HashMap<LaneInformation.Direction, Integer> hashMap2) {
        Integer n;
        if (position == MapEvents.LaneData.Position.ON_ROUTE) {
            n = hashMap.get(direction);
        }
        else {
            n = hashMap2.get(direction);
        }
        Drawable drawable;
        if (n == null) {
            drawable = null;
        }
        else {
            drawable = HereLaneInfoBuilder.sLaneDrawableCache.get(n);
        }
        return drawable;
    }
    
    public static int getNumDirections(final EnumSet<LaneInformation.Direction> set, final DirectionType directionType) {
        int n = 0;
        final Iterator<LaneInformation.Direction> iterator = (Iterator<LaneInformation.Direction>)set.iterator();
        while (iterator.hasNext()) {
            switch (iterator.next()) {
                default:
                    continue;
                case SLIGHTLY_RIGHT:
                case RIGHT:
                case SHARP_RIGHT:
                    if (directionType == DirectionType.RIGHT) {
                        ++n;
                        continue;
                    }
                    continue;
                case SHARP_LEFT:
                case LEFT:
                case SLIGHTLY_LEFT:
                    if (directionType == DirectionType.LEFT) {
                        ++n;
                        continue;
                    }
                    continue;
            }
        }
        return n;
    }
    
    public static void init() {
        synchronized (HereLaneInfoBuilder.class) {
            if (!HereLaneInfoBuilder.initialized) {
                GenericUtil.checkNotOnMainThread();
                HereLaneInfoBuilder.initialized = true;
                final Resources resources = HudApplication.getAppContext().getResources();
                HereLaneInfoBuilder.sLaneDrawableCache = new HashMap<Integer, Drawable>();
                (HereLaneInfoBuilder.sSingleRecommendedDirection = new HashMap<LaneInformation.Direction, Integer>()).put(LaneInformation.Direction.STRAIGHT, R.drawable.icon_lg_rec_straight_only);
                HereLaneInfoBuilder.sSingleRecommendedDirection.put(LaneInformation.Direction.SLIGHTLY_RIGHT, R.drawable.icon_lg_rec_slight_right_only);
                HereLaneInfoBuilder.sSingleRecommendedDirection.put(LaneInformation.Direction.RIGHT, R.drawable.icon_lg_rec_right_only);
                HereLaneInfoBuilder.sSingleRecommendedDirection.put(LaneInformation.Direction.SHARP_RIGHT, R.drawable.icon_lg_rec_heavy_right_only);
                HereLaneInfoBuilder.sSingleRecommendedDirection.put(LaneInformation.Direction.U_TURN_RIGHT, R.drawable.icon_lg_rec_uturn_right_only);
                HereLaneInfoBuilder.sSingleRecommendedDirection.put(LaneInformation.Direction.SECOND_RIGHT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sSingleRecommendedDirection.put(LaneInformation.Direction.MERGE_RIGHT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sSingleRecommendedDirection.put(LaneInformation.Direction.U_TURN_LEFT, R.drawable.icon_lg_rec_uturn_left_only);
                HereLaneInfoBuilder.sSingleRecommendedDirection.put(LaneInformation.Direction.SHARP_LEFT, R.drawable.icon_lg_rec_heavy_left_only);
                HereLaneInfoBuilder.sSingleRecommendedDirection.put(LaneInformation.Direction.LEFT, R.drawable.icon_lg_rec_left_only);
                HereLaneInfoBuilder.sSingleRecommendedDirection.put(LaneInformation.Direction.SLIGHTLY_LEFT, R.drawable.icon_lg_rec_slight_left_only);
                HereLaneInfoBuilder.sSingleRecommendedDirection.put(LaneInformation.Direction.SECOND_LEFT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sSingleRecommendedDirection.put(LaneInformation.Direction.MERGE_LEFT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sSingleRecommendedDirection.put(LaneInformation.Direction.MERGE_LANES, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_rec_straight_only, resources.getDrawable(R.drawable.icon_lg_rec_straight_only));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_rec_slight_right_only, resources.getDrawable(R.drawable.icon_lg_rec_slight_right_only));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_rec_right_only, resources.getDrawable(R.drawable.icon_lg_rec_right_only));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_rec_heavy_right_only, resources.getDrawable(R.drawable.icon_lg_rec_heavy_right_only));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_rec_uturn_right_only, resources.getDrawable(R.drawable.icon_lg_rec_uturn_right_only));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_rec_uturn_left_only, resources.getDrawable(R.drawable.icon_lg_rec_uturn_left_only));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_rec_heavy_left_only, resources.getDrawable(R.drawable.icon_lg_rec_heavy_left_only));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_rec_left_only, resources.getDrawable(R.drawable.icon_lg_rec_left_only));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_rec_slight_left_only, resources.getDrawable(R.drawable.icon_lg_rec_slight_left_only));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_glance_whatsapp, resources.getDrawable(R.drawable.icon_glance_whatsapp));
                (HereLaneInfoBuilder.sSingleNotRecommendedDirection = new HashMap<LaneInformation.Direction, Integer>()).put(LaneInformation.Direction.STRAIGHT, R.drawable.icon_lg_straight_only);
                HereLaneInfoBuilder.sSingleNotRecommendedDirection.put(LaneInformation.Direction.SLIGHTLY_RIGHT, R.drawable.icon_lg_slight_right_only);
                HereLaneInfoBuilder.sSingleNotRecommendedDirection.put(LaneInformation.Direction.RIGHT, R.drawable.icon_lg_right_only);
                HereLaneInfoBuilder.sSingleNotRecommendedDirection.put(LaneInformation.Direction.SHARP_RIGHT, R.drawable.icon_lg_heavy_right_only);
                HereLaneInfoBuilder.sSingleNotRecommendedDirection.put(LaneInformation.Direction.U_TURN_RIGHT, R.drawable.icon_lg_uturn_right_only);
                HereLaneInfoBuilder.sSingleNotRecommendedDirection.put(LaneInformation.Direction.SECOND_RIGHT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sSingleNotRecommendedDirection.put(LaneInformation.Direction.MERGE_RIGHT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sSingleNotRecommendedDirection.put(LaneInformation.Direction.U_TURN_LEFT, R.drawable.icon_lg_uturn_left_only);
                HereLaneInfoBuilder.sSingleNotRecommendedDirection.put(LaneInformation.Direction.SHARP_LEFT, R.drawable.icon_lg_heavy_left_only);
                HereLaneInfoBuilder.sSingleNotRecommendedDirection.put(LaneInformation.Direction.LEFT, R.drawable.icon_lg_left_only);
                HereLaneInfoBuilder.sSingleNotRecommendedDirection.put(LaneInformation.Direction.SLIGHTLY_LEFT, R.drawable.icon_lg_slight_left_only);
                HereLaneInfoBuilder.sSingleNotRecommendedDirection.put(LaneInformation.Direction.SECOND_LEFT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sSingleNotRecommendedDirection.put(LaneInformation.Direction.MERGE_LEFT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sSingleNotRecommendedDirection.put(LaneInformation.Direction.MERGE_LANES, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_straight_only, resources.getDrawable(R.drawable.icon_lg_straight_only));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_slight_right_only, resources.getDrawable(R.drawable.icon_lg_slight_right_only));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_right_only, resources.getDrawable(R.drawable.icon_lg_right_only));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_heavy_right_only, resources.getDrawable(R.drawable.icon_lg_heavy_right_only));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_uturn_right_only, resources.getDrawable(R.drawable.icon_lg_uturn_right_only));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_uturn_left_only, resources.getDrawable(R.drawable.icon_lg_uturn_left_only));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_heavy_left_only, resources.getDrawable(R.drawable.icon_lg_heavy_left_only));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_left_only, resources.getDrawable(R.drawable.icon_lg_left_only));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_slight_left_only, resources.getDrawable(R.drawable.icon_lg_slight_left_only));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_glance_whatsapp, resources.getDrawable(R.drawable.icon_glance_whatsapp));
                (HereLaneInfoBuilder.sRightOnlyRecommendedDirection = new HashMap<LaneInformation.Direction, Integer>()).put(LaneInformation.Direction.STRAIGHT, R.drawable.icon_lg_compright_rec_straight);
                HereLaneInfoBuilder.sRightOnlyRecommendedDirection.put(LaneInformation.Direction.SLIGHTLY_RIGHT, R.drawable.icon_lg_compright_rec_slight_right);
                HereLaneInfoBuilder.sRightOnlyRecommendedDirection.put(LaneInformation.Direction.RIGHT, R.drawable.icon_lg_compright_rec_right);
                HereLaneInfoBuilder.sRightOnlyRecommendedDirection.put(LaneInformation.Direction.SHARP_RIGHT, R.drawable.icon_lg_compright_rec_heavy_right);
                HereLaneInfoBuilder.sRightOnlyRecommendedDirection.put(LaneInformation.Direction.U_TURN_RIGHT, R.drawable.icon_lg_compright_rec_uturn_right);
                HereLaneInfoBuilder.sRightOnlyRecommendedDirection.put(LaneInformation.Direction.SECOND_RIGHT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sRightOnlyRecommendedDirection.put(LaneInformation.Direction.MERGE_RIGHT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compright_rec_straight, resources.getDrawable(R.drawable.icon_lg_compright_rec_straight));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compright_rec_slight_right, resources.getDrawable(R.drawable.icon_lg_compright_rec_slight_right));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compright_rec_right, resources.getDrawable(R.drawable.icon_lg_compright_rec_right));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compright_rec_heavy_right, resources.getDrawable(R.drawable.icon_lg_compright_rec_heavy_right));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compright_rec_uturn_right, resources.getDrawable(R.drawable.icon_lg_compright_rec_uturn_right));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_glance_whatsapp, resources.getDrawable(R.drawable.icon_glance_whatsapp));
                (HereLaneInfoBuilder.sRightOnlyNotRecommendedDirection = new HashMap<LaneInformation.Direction, Integer>()).put(LaneInformation.Direction.STRAIGHT, R.drawable.icon_lg_compright_straight);
                HereLaneInfoBuilder.sRightOnlyNotRecommendedDirection.put(LaneInformation.Direction.SLIGHTLY_RIGHT, R.drawable.icon_lg_compright_slight_right);
                HereLaneInfoBuilder.sRightOnlyNotRecommendedDirection.put(LaneInformation.Direction.RIGHT, R.drawable.icon_lg_compright_right);
                HereLaneInfoBuilder.sRightOnlyNotRecommendedDirection.put(LaneInformation.Direction.SHARP_RIGHT, R.drawable.icon_lg_compright_heavy_right);
                HereLaneInfoBuilder.sRightOnlyNotRecommendedDirection.put(LaneInformation.Direction.U_TURN_RIGHT, R.drawable.icon_lg_compright_uturn_right);
                HereLaneInfoBuilder.sRightOnlyNotRecommendedDirection.put(LaneInformation.Direction.SECOND_RIGHT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sRightOnlyNotRecommendedDirection.put(LaneInformation.Direction.MERGE_RIGHT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compright_straight, resources.getDrawable(R.drawable.icon_lg_compright_straight));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compright_slight_right, resources.getDrawable(R.drawable.icon_lg_compright_slight_right));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compright_right, resources.getDrawable(R.drawable.icon_lg_compright_right));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compright_heavy_right, resources.getDrawable(R.drawable.icon_lg_compright_heavy_right));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compright_uturn_right, resources.getDrawable(R.drawable.icon_lg_compright_uturn_right));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_glance_whatsapp, resources.getDrawable(R.drawable.icon_glance_whatsapp));
                (HereLaneInfoBuilder.sLeftOnlyRecommendedDirection = new HashMap<LaneInformation.Direction, Integer>()).put(LaneInformation.Direction.STRAIGHT, R.drawable.icon_lg_compleft_rec_straight);
                HereLaneInfoBuilder.sLeftOnlyRecommendedDirection.put(LaneInformation.Direction.SLIGHTLY_LEFT, R.drawable.icon_lg_compleft_rec_slight_left);
                HereLaneInfoBuilder.sLeftOnlyRecommendedDirection.put(LaneInformation.Direction.LEFT, R.drawable.icon_lg_compleft_rec_left);
                HereLaneInfoBuilder.sLeftOnlyRecommendedDirection.put(LaneInformation.Direction.SHARP_LEFT, R.drawable.icon_lg_compleft_rec_heavy_left);
                HereLaneInfoBuilder.sLeftOnlyRecommendedDirection.put(LaneInformation.Direction.U_TURN_LEFT, R.drawable.icon_lg_compleft_rec_uturn_left);
                HereLaneInfoBuilder.sLeftOnlyRecommendedDirection.put(LaneInformation.Direction.SECOND_LEFT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sLeftOnlyRecommendedDirection.put(LaneInformation.Direction.MERGE_LEFT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compleft_rec_straight, resources.getDrawable(R.drawable.icon_lg_compleft_rec_straight));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compleft_rec_slight_left, resources.getDrawable(R.drawable.icon_lg_compleft_rec_slight_left));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compleft_rec_left, resources.getDrawable(R.drawable.icon_lg_compleft_rec_left));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compleft_rec_heavy_left, resources.getDrawable(R.drawable.icon_lg_compleft_rec_heavy_left));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compleft_rec_uturn_left, resources.getDrawable(R.drawable.icon_lg_compleft_rec_uturn_left));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_glance_whatsapp, resources.getDrawable(R.drawable.icon_glance_whatsapp));
                (HereLaneInfoBuilder.sLeftOnlyNotRecommendedDirection = new HashMap<LaneInformation.Direction, Integer>()).put(LaneInformation.Direction.STRAIGHT, R.drawable.icon_lg_compleft_straight);
                HereLaneInfoBuilder.sLeftOnlyNotRecommendedDirection.put(LaneInformation.Direction.SLIGHTLY_LEFT, R.drawable.icon_lg_compleft_slight_left);
                HereLaneInfoBuilder.sLeftOnlyNotRecommendedDirection.put(LaneInformation.Direction.LEFT, R.drawable.icon_lg_compleft_left);
                HereLaneInfoBuilder.sLeftOnlyNotRecommendedDirection.put(LaneInformation.Direction.SHARP_LEFT, R.drawable.icon_lg_compleft_heavy_left);
                HereLaneInfoBuilder.sLeftOnlyNotRecommendedDirection.put(LaneInformation.Direction.U_TURN_LEFT, R.drawable.icon_lg_compleft_uturn_left);
                HereLaneInfoBuilder.sLeftOnlyNotRecommendedDirection.put(LaneInformation.Direction.SECOND_LEFT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sLeftOnlyNotRecommendedDirection.put(LaneInformation.Direction.MERGE_LEFT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compleft_straight, resources.getDrawable(R.drawable.icon_lg_compleft_straight));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compleft_slight_left, resources.getDrawable(R.drawable.icon_lg_compleft_slight_left));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compleft_left, resources.getDrawable(R.drawable.icon_lg_compleft_left));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compleft_heavy_left, resources.getDrawable(R.drawable.icon_lg_compleft_heavy_left));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compleft_uturn_left, resources.getDrawable(R.drawable.icon_lg_compleft_uturn_left));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_glance_whatsapp, resources.getDrawable(R.drawable.icon_glance_whatsapp));
                (HereLaneInfoBuilder.sLeftRightRecommendedDirection = new HashMap<LaneInformation.Direction, Integer>()).put(LaneInformation.Direction.STRAIGHT, R.drawable.icon_lg_compboth_rec_straight);
                HereLaneInfoBuilder.sLeftRightRecommendedDirection.put(LaneInformation.Direction.SLIGHTLY_RIGHT, R.drawable.icon_lg_compboth_rec_slight_right);
                HereLaneInfoBuilder.sLeftRightRecommendedDirection.put(LaneInformation.Direction.RIGHT, R.drawable.icon_lg_compboth_rec_right);
                HereLaneInfoBuilder.sLeftRightRecommendedDirection.put(LaneInformation.Direction.SHARP_RIGHT, R.drawable.icon_lg_compboth_rec_heavy_right);
                HereLaneInfoBuilder.sLeftRightRecommendedDirection.put(LaneInformation.Direction.SECOND_RIGHT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sLeftRightRecommendedDirection.put(LaneInformation.Direction.MERGE_RIGHT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sLeftRightRecommendedDirection.put(LaneInformation.Direction.SLIGHTLY_LEFT, R.drawable.icon_lg_compboth_rec_slight_left);
                HereLaneInfoBuilder.sLeftRightRecommendedDirection.put(LaneInformation.Direction.LEFT, R.drawable.icon_lg_compboth_rec_left);
                HereLaneInfoBuilder.sLeftRightRecommendedDirection.put(LaneInformation.Direction.SHARP_LEFT, R.drawable.icon_lg_compboth_rec_heavy_left);
                HereLaneInfoBuilder.sLeftRightRecommendedDirection.put(LaneInformation.Direction.MERGE_LEFT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compboth_rec_straight, resources.getDrawable(R.drawable.icon_lg_compboth_rec_straight));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compboth_rec_slight_right, resources.getDrawable(R.drawable.icon_lg_compboth_rec_slight_right));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compboth_rec_right, resources.getDrawable(R.drawable.icon_lg_compboth_rec_right));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compboth_rec_heavy_right, resources.getDrawable(R.drawable.icon_lg_compboth_rec_heavy_right));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compboth_rec_slight_left, resources.getDrawable(R.drawable.icon_lg_compboth_rec_slight_left));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compboth_rec_left, resources.getDrawable(R.drawable.icon_lg_compboth_rec_left));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compboth_rec_heavy_left, resources.getDrawable(R.drawable.icon_lg_compboth_rec_heavy_left));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_glance_whatsapp, resources.getDrawable(R.drawable.icon_glance_whatsapp));
                (HereLaneInfoBuilder.sLeftRightNotRecommendedDirection = new HashMap<LaneInformation.Direction, Integer>()).put(LaneInformation.Direction.STRAIGHT, R.drawable.icon_lg_compboth_straight);
                HereLaneInfoBuilder.sLeftRightNotRecommendedDirection.put(LaneInformation.Direction.SLIGHTLY_RIGHT, R.drawable.icon_lg_compboth_slight_right);
                HereLaneInfoBuilder.sLeftRightNotRecommendedDirection.put(LaneInformation.Direction.RIGHT, R.drawable.icon_lg_compboth_right);
                HereLaneInfoBuilder.sLeftRightNotRecommendedDirection.put(LaneInformation.Direction.SHARP_RIGHT, R.drawable.icon_lg_compboth_heavy_right);
                HereLaneInfoBuilder.sLeftRightNotRecommendedDirection.put(LaneInformation.Direction.MERGE_RIGHT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sLeftRightNotRecommendedDirection.put(LaneInformation.Direction.SLIGHTLY_LEFT, R.drawable.icon_lg_compboth_slight_left);
                HereLaneInfoBuilder.sLeftRightNotRecommendedDirection.put(LaneInformation.Direction.LEFT, R.drawable.icon_lg_compboth_left);
                HereLaneInfoBuilder.sLeftRightNotRecommendedDirection.put(LaneInformation.Direction.SHARP_LEFT, R.drawable.icon_lg_compboth_heavy_left);
                HereLaneInfoBuilder.sLeftRightNotRecommendedDirection.put(LaneInformation.Direction.MERGE_LEFT, R.drawable.icon_glance_whatsapp);
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compboth_straight, resources.getDrawable(R.drawable.icon_lg_compboth_straight));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compboth_slight_right, resources.getDrawable(R.drawable.icon_lg_compboth_slight_right));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compboth_right, resources.getDrawable(R.drawable.icon_lg_compboth_right));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compboth_heavy_right, resources.getDrawable(R.drawable.icon_lg_compboth_heavy_right));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compboth_slight_left, resources.getDrawable(R.drawable.icon_lg_compboth_slight_left));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compboth_left, resources.getDrawable(R.drawable.icon_lg_compboth_left));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_lg_compboth_heavy_left, resources.getDrawable(R.drawable.icon_lg_compboth_heavy_left));
                HereLaneInfoBuilder.sLaneDrawableCache.put(R.drawable.icon_glance_whatsapp, resources.getDrawable(R.drawable.icon_glance_whatsapp));
            }
        }
    }
    
    public enum DirectionType
    {
        LEFT, 
        LEFT_RIGHT, 
        RIGHT;
    }
}
