package com.navdy.hud.app.maps.here;

import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.maps.MapSettings;
import android.os.SystemClock;
import java.util.concurrent.TimeUnit;
import com.here.android.mpa.routing.RouteTta;
import java.util.Date;
import com.here.android.mpa.routing.Maneuver;
import com.navdy.hud.app.framework.glympse.GlympseManager;
import com.navdy.hud.app.util.GenericUtil;
import java.lang.ref.WeakReference;
import com.navdy.service.library.task.TaskManager;
import android.os.Looper;
import com.here.android.mpa.routing.Route;
import com.navdy.hud.app.analytics.NavigationQualityTracker;
import com.here.android.mpa.guidance.NavigationManager;
import android.os.Handler;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.framework.trips.TripManager;

public class HereNavController
{
    private static final int ANIMATE_STATE_CLEAR_DELAY = 2000;
    private static final TripManager.FinishedTripRouteEvent TRIP_END;
    private static final TripManager.NewTripEvent TRIP_START;
    private static final Logger sLogger;
    private final Bus bus;
    private Runnable clearAnimatorState;
    private Runnable clearAnimatorStateBk;
    private boolean firstTrip;
    private Handler handler;
    private boolean initialized;
    private final NavigationManager navigationManager;
    private final NavigationQualityTracker navigationQualityTracker;
    private Route route;
    private volatile State state;
    
    static {
        sLogger = new Logger(HereNavController.class);
        TRIP_START = new TripManager.NewTripEvent();
        TRIP_END = new TripManager.FinishedTripRouteEvent();
    }
    
    HereNavController(final NavigationManager navigationManager, final Bus bus) {
        this.handler = new Handler(Looper.getMainLooper());
        this.firstTrip = true;
        this.clearAnimatorState = new Runnable() {
            @Override
            public void run() {
                TaskManager.getInstance().execute(HereNavController.this.clearAnimatorStateBk, 17);
            }
        };
        this.clearAnimatorStateBk = new Runnable() {
            @Override
            public void run() {
                HereMapsManager.getInstance().getMapAnimator().clearState();
            }
        };
        this.navigationManager = navigationManager;
        this.bus = bus;
        HereNavController.sLogger.v(":ctor: state:" + this.state);
        this.navigationQualityTracker = NavigationQualityTracker.getInstance();
    }
    
    private void startTrip() {
        if (this.firstTrip) {
            this.firstTrip = false;
            HereNavController.sLogger.v("first trip started");
        }
        else {
            HereNavController.sLogger.v("trip started");
        }
        this.bus.post(HereNavController.TRIP_START);
    }
    
    public void addLaneInfoListener(final WeakReference<NavigationManager.LaneInformationListener> weakReference) {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.addLaneInformationListener(weakReference);
    }
    
    public void addRealisticViewAspectRatio(final NavigationManager.AspectRatio aspectRatio) {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.addRealisticViewAspectRatio(aspectRatio);
    }
    
    public void addRealisticViewListener(final WeakReference<NavigationManager.RealisticViewListener> weakReference) {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.addRealisticViewListener(weakReference);
    }
    
    public void addTrafficRerouteListener(final WeakReference<NavigationManager.TrafficRerouteListener> weakReference) {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.addTrafficRerouteListener(weakReference);
    }
    
    public void arrivedAtDestination() {
        synchronized (this) {
            GenericUtil.checkNotOnMainThread();
            HereNavController.sLogger.v("arrivedAtDestination state[" + this.state + "]");
            if (this.state == State.NAVIGATING) {
                this.navigationQualityTracker.trackTripEnded(this.getElapsedDistance());
                this.expireGlympseTickets();
                this.navigationManager.stop();
                HereNavController.sLogger.v("arrivedAtDestination: tracking error state =" + this.navigationManager.startTracking());
            }
            else {
                HereNavController.sLogger.v("arrivedAtDestination: not navigating:" + this.state);
            }
        }
    }
    
    public void endTrip() {
        HereNavController.sLogger.v("trip ended");
        this.bus.post(HereNavController.TRIP_END);
    }
    
    void expireGlympseTickets() {
        this.handler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                GlympseManager.getInstance().expireActiveTickets();
            }
        });
    }
    
    public Maneuver getAfterNextManeuver() {
        GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getAfterNextManeuver();
    }
    
    public long getDestinationDistance() {
        GenericUtil.checkNotOnMainThread();
        long destinationDistance;
        if (this.state == State.TRACKING) {
            destinationDistance = 0L;
        }
        else {
            destinationDistance = this.navigationManager.getDestinationDistance();
        }
        return destinationDistance;
    }
    
    public long getElapsedDistance() {
        GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getElapsedDistance();
    }
    
    public Date getEta(final boolean b, final Route.TrafficPenaltyMode trafficPenaltyMode) {
        GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getEta(b, trafficPenaltyMode);
    }
    
    public Maneuver getNextManeuver() {
        GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getNextManeuver();
    }
    
    public long getNextManeuverDistance() {
        GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getNextManeuverDistance();
    }
    
    public State getState() {
        return this.state;
    }
    
    public RouteTta getTta(final Route.TrafficPenaltyMode trafficPenaltyMode, final boolean b) {
        GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getTta(trafficPenaltyMode, b);
    }
    
    public Date getTtaDate(final boolean b, final Route.TrafficPenaltyMode trafficPenaltyMode) {
        final Date date = null;
        GenericUtil.checkNotOnMainThread();
        final RouteTta tta = this.navigationManager.getTta(trafficPenaltyMode, b);
        Date date2 = date;
        if (tta != null) {
            final int duration = tta.getDuration();
            date2 = date;
            if (duration > 0) {
                date2 = new Date(System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(duration));
            }
        }
        return date2;
    }
    
    public void initialize() {
        synchronized (this) {
            if (!this.initialized) {
                this.initialized = true;
                this.state = State.TRACKING;
                HereNavController.sLogger.v("initialize state:" + this.state + " error = " + this.navigationManager.startTracking());
                this.startTrip();
            }
        }
    }
    
    public boolean isInitialized() {
        synchronized (this) {
            return this.initialized;
        }
    }
    
    public void pause() {
        synchronized (this) {
            GenericUtil.checkNotOnMainThread();
            this.navigationManager.pause();
        }
    }
    
    public void removeLaneInfoListener(final NavigationManager.LaneInformationListener laneInformationListener) {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.removeLaneInformationListener(laneInformationListener);
    }
    
    public void removeRealisticViewListener(final NavigationManager.RealisticViewListener realisticViewListener) {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.removeRealisticViewListener(realisticViewListener);
    }
    
    public void removeTrafficRerouteListener(final NavigationManager.TrafficRerouteListener trafficRerouteListener) {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.removeTrafficRerouteListener(trafficRerouteListener);
    }
    
    public NavigationManager.Error resume() {
        synchronized (this) {
            GenericUtil.checkNotOnMainThread();
            return this.navigationManager.resume();
        }
    }
    
    public void setDistanceUnit(final NavigationManager.UnitSystem distanceUnit) {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.setDistanceUnit(distanceUnit);
    }
    
    public void setRealisticViewMode(final NavigationManager.RealisticViewMode realisticViewMode) {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.setRealisticViewMode(realisticViewMode);
    }
    
    public NavigationManager.Error setRoute(final Route route) {
        GenericUtil.checkNotOnMainThread();
        return this.navigationManager.setRoute(route);
    }
    
    public void setTrafficAvoidanceMode(final NavigationManager.TrafficAvoidanceMode trafficAvoidanceMode) {
        GenericUtil.checkNotOnMainThread();
        this.navigationManager.setTrafficAvoidanceMode(trafficAvoidanceMode);
    }
    
    public NavigationManager.Error startNavigation(final Route route, final int n) {
        synchronized (this) {
            GenericUtil.checkNotOnMainThread();
            HereNavController.sLogger.v("startNavigation state[" + this.state + "]");
            HereNavController.sLogger.v("startNavigation simulationSpeed:" + n + " route-id=" + System.identityHashCode(route));
            if (this.state == State.NAVIGATING) {
                HereNavController.sLogger.v("startNavigation stop existing nav");
                this.stopNavigation(false);
            }
            final long elapsedRealtime = SystemClock.elapsedRealtime();
            int simulationSpeed;
            if ((simulationSpeed = n) == 0) {
                simulationSpeed = MapSettings.getSimulationSpeed();
            }
            NavigationManager.Error error;
            if (simulationSpeed > 0) {
                error = this.navigationManager.simulate(route, simulationSpeed);
            }
            else {
                error = this.navigationManager.startNavigation(route);
            }
            HereNavController.sLogger.v("startNavigation took [" + (SystemClock.elapsedRealtime() - elapsedRealtime) + "]");
            if (error == NavigationManager.Error.NONE) {
                this.state = State.NAVIGATING;
                this.route = route;
                HereNavController.sLogger.v("startNavigation: success [" + this.state + "] route-id=" + System.identityHashCode(route));
                this.endTrip();
                this.startTrip();
                this.navigationQualityTracker.trackTripStarted(route.getTta(Route.TrafficPenaltyMode.OPTIMAL, 268435455).getDuration(), route.getTta(Route.TrafficPenaltyMode.DISABLED, 268435455).getDuration(), route.getLength());
                AnalyticsSupport.recordNavigation(true);
            }
            else {
                HereNavController.sLogger.e("startNavigation error [" + this.state + "] " + error.name());
            }
            return error;
        }
    }
    
    public void stopNavigation(final boolean b) {
        boolean b2 = false;
        synchronized (this) {
            GenericUtil.checkNotOnMainThread();
            HereNavController.sLogger.v("stopNavigation state[" + this.state + "]");
            if (this.state == State.NAVIGATING) {
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                HereNavController.sLogger.v("stopNavigation:stopping nav state[" + this.state + " ] route-id=" + System.identityHashCode(this.route));
                if (this.navigationManager.getNavigationMode() == NavigationManager.NavigationMode.SIMULATION) {
                    b2 = true;
                }
                this.expireGlympseTickets();
                this.navigationManager.stop();
                if (b2) {
                    this.handler.postDelayed(this.clearAnimatorState, 2000L);
                }
                HereNavController.sLogger.v("stopNavigation:stopped nav state[" + this.state + " ] took [" + (SystemClock.elapsedRealtime() - elapsedRealtime) + "]");
                HereNavController.sLogger.v("stopNavigation: tracking error state =" + this.navigationManager.startTracking());
                this.state = State.TRACKING;
                AnalyticsSupport.recordNavigation(false);
                if (b) {
                    this.endTrip();
                    this.startTrip();
                }
                this.route = null;
                this.navigationQualityTracker.cancelTrip();
            }
            else {
                HereNavController.sLogger.v("stopNavigation: already " + this.state);
            }
        }
    }
    
    public enum State
    {
        NAVIGATING, 
        TRACKING;
    }
}
