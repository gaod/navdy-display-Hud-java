package com.navdy.hud.app.maps;

import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.navigation.NavigationRouteCancelRequest;
import com.navdy.service.library.events.places.PlacesSearchRequest;
import com.navdy.service.library.events.navigation.RouteManeuver;
import java.util.List;
import com.navdy.service.library.events.navigation.RouteManeuverResponse;
import com.navdy.hud.app.maps.here.HereRouteManager;
import com.navdy.service.library.events.navigation.RouteManeuverRequest;
import com.navdy.service.library.events.navigation.GetNavigationSessionState;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.maps.here.HerePlacesManager;
import com.navdy.service.library.events.places.AutoCompleteRequest;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.service.library.events.navigation.NavigationSessionResponse;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.navigation.NavigationSessionRequest;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.events.navigation.NavigationSessionStatusEvent;
import com.navdy.service.library.events.navigation.NavigationSessionState;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import mortar.Mortar;
import com.navdy.hud.app.HudApplication;
import android.content.SharedPreferences;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.service.ConnectionHandler;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public final class MapsEventHandler
{
    private static final boolean VERBOSE = false;
    private static final Logger sLogger;
    private static final MapsEventHandler sSingleton;
    @Inject
    Bus bus;
    @Inject
    ConnectionHandler connectionHandler;
    @Inject
    DriverProfileManager mDriverProfileManager;
    @Inject
    protected SharedPreferences sharedPreferences;
    
    static {
        sLogger = new Logger(MapsEventHandler.class);
        sSingleton = new MapsEventHandler();
    }
    
    private MapsEventHandler() {
        Mortar.inject(HudApplication.getAppContext(), this);
        this.bus.register(this);
    }
    
    public static final MapsEventHandler getInstance() {
        return MapsEventHandler.sSingleton;
    }
    
    private void handleConnectionStateChange(final ConnectionStateChange connectionStateChange) {
        if (!HereMapsManager.getInstance().isInitialized()) {
            MapsEventHandler.sLogger.v("handleConnectionStateChange:engine not initiazed");
        }
        else {
            boolean b = false;
            if (connectionStateChange.state == ConnectionStateChange.ConnectionState.CONNECTION_VERIFIED) {
                b = true;
                this.bus.post(new RemoteEvent(new NavigationSessionStatusEvent(NavigationSessionState.NAV_SESSION_ENGINE_READY, null, null, null, null)));
            }
            HereNavigationManager.getInstance().handleConnectionState(b);
        }
    }
    
    private void handleNavigationRequest(final NavigationSessionRequest navigationSessionRequest) {
        if (!HereMapsManager.getInstance().isInitialized()) {
            this.sendEventToClient(new NavigationSessionResponse(RequestStatus.REQUEST_NOT_READY, HudApplication.getAppContext().getString(R.string.map_engine_not_ready), navigationSessionRequest.newState, navigationSessionRequest.routeId));
        }
        else {
            HereNavigationManager.getInstance().handleNavigationSessionRequest(navigationSessionRequest);
        }
    }
    
    public Bus getBus() {
        return this.bus;
    }
    
    public NavigationPreferences getNavigationPreferences() {
        return this.mDriverProfileManager.getCurrentProfile().getNavigationPreferences();
    }
    
    public SharedPreferences getSharedPreferences() {
        return this.sharedPreferences;
    }
    
    @Subscribe
    public void onAutoCompleteRequest(final AutoCompleteRequest autoCompleteRequest) {
        HerePlacesManager.handleAutoCompleteRequest(autoCompleteRequest);
    }
    
    @Subscribe
    public void onConnectionStatusChange(final ConnectionStateChange connectionStateChange) {
        this.handleConnectionStateChange(connectionStateChange);
    }
    
    @Subscribe
    public void onGetNavigationSessionState(final GetNavigationSessionState getNavigationSessionState) {
        if (!HereMapsManager.getInstance().isInitialized()) {
            MapsEventHandler.sLogger.i("onGetNavigationSessionState:engine not ready");
            this.bus.post(new RemoteEvent(new NavigationSessionStatusEvent(NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY, null, null, null, null)));
        }
        else {
            HereNavigationManager.getInstance().postNavigationSessionStatusEvent(true);
        }
    }
    
    @Subscribe
    public void onGetRouteManeuverRequest(final RouteManeuverRequest routeManeuverRequest) {
        try {
            HereRouteManager.handleRouteManeuverRequest(routeManeuverRequest);
        }
        catch (Throwable t) {
            this.sendEventToClient(new RouteManeuverResponse(RequestStatus.REQUEST_SERVICE_ERROR, t.toString(), null));
            MapsEventHandler.sLogger.e(t);
        }
    }
    
    @Subscribe
    public void onNavigationRequest(final NavigationSessionRequest navigationSessionRequest) {
        this.handleNavigationRequest(navigationSessionRequest);
    }
    
    @Subscribe
    public void onPlaceSearchRequest(final PlacesSearchRequest placesSearchRequest) {
        HerePlacesManager.handlePlacesSearchRequest(placesSearchRequest);
    }
    
    @Subscribe
    public void onRouteCancelRequest(final NavigationRouteCancelRequest navigationRouteCancelRequest) {
        HereRouteManager.handleRouteCancelRequest(navigationRouteCancelRequest, false);
    }
    
    @Subscribe
    public void onRouteSearchRequest(final NavigationRouteRequest navigationRouteRequest) {
        HereRouteManager.handleRouteRequest(navigationRouteRequest);
    }
    
    public void sendEventToClient(final Message message) {
        try {
            final RemoteDevice remoteDevice = this.connectionHandler.getRemoteDevice();
            if (remoteDevice != null) {
                remoteDevice.postEvent(message);
            }
        }
        catch (Throwable t) {
            MapsEventHandler.sLogger.e(t);
        }
    }
}
