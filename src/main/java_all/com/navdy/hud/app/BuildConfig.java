package com.navdy.hud.app;

public final class BuildConfig
{
    public static final String APPLICATION_ID = "com.navdy.hud.app";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = true;
    public static final String FLAVOR = "hud";
    public static final int VERSION_CODE = 3049;
    public static final String VERSION_NAME = "1.3.3051-corona";
}
