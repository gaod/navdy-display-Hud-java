package com.navdy.hud.app.framework.notifications;

import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout.LayoutParams;
import android.view.ViewPropertyAnimator;
import android.view.ViewGroup;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.view.NotificationView;
import android.view.View;
import com.navdy.service.library.log.Logger;

public class NotificationAnimator
{
    private static Logger sLogger;
    
    static {
        NotificationAnimator.sLogger = NotificationManager.sLogger;
    }
    
    static void animateChildViewOut(final View view, final int n, final Runnable runnable) {
        view.animate().alpha(0.0f).setDuration((long)n).setStartDelay(0L).withEndAction(runnable).start();
    }
    
    static void animateExpandedViewOut(final NotificationView notificationView, final int n, final View view, final View view2, final Runnable runnable, final NotificationManager.Info info, final boolean b, final boolean b2) {
        final Runnable runnable2 = new Runnable() {
            @Override
            public void run() {
                view2.setVisibility(GONE);
            }
        };
        final Runnable runnable3 = new Runnable() {
            @Override
            public void run() {
                view.setVisibility(GONE);
                NotificationManager.getInstance().setExpandedStack(false);
                if (info != null && info.startCalled) {
                    NotificationAnimator.sLogger.v("calling onExpandedNotificationEvent-collapse");
                    info.notification.onExpandedNotificationEvent(UIStateManager.Mode.COLLAPSE);
                    NotificationManager.getInstance().setNotificationColor();
                    notificationView.showNextNotificationColor();
                }
                if (runnable != null) {
                    runnable.run();
                }
            }
        };
        if (b || b2) {
            notificationView.animate().x((float)n).withStartAction((Runnable)runnable2).withEndAction((Runnable)runnable3).start();
        }
        else {
            runnable2.run();
            runnable3.run();
        }
    }
    
    static void animateExpandedViews(final View view, final View view2, final ViewGroup viewGroup, final int n, final int n2, final int n3, final Runnable runnable) {
        ViewPropertyAnimator animateOut = null;
        final ViewPropertyAnimator viewPropertyAnimator = null;
        Runnable runnable2 = runnable;
        if (view2 != null) {
            animateOut = animateOut(view2, viewGroup, n, n2, 0, runnable);
            runnable2 = null;
        }
        ViewPropertyAnimator animateIn = viewPropertyAnimator;
        if (view != null) {
            animateIn = animateIn(view, viewGroup, n, n2, n3, runnable2);
        }
        if (animateOut != null) {
            animateOut.start();
        }
        if (animateIn != null) {
            animateIn.start();
        }
    }
    
    private static ViewPropertyAnimator animateIn(final View view, final ViewGroup viewGroup, final int n, final int n2, final int n3, final Runnable runnable) {
        view.setLayoutParams((ViewGroup.LayoutParams)new FrameLayout.LayoutParams(-1, -1));
        view.setTranslationY((float)n);
        view.setAlpha(0.0f);
        viewGroup.addView(view);
        final ViewPropertyAnimator withStartAction = view.animate().alpha(1.0f).translationY(0.0f).setDuration((long)n2).setStartDelay((long)n3).withStartAction((Runnable)new Runnable() {
            @Override
            public void run() {
                view.setVisibility(View.VISIBLE);
            }
        });
        if (runnable != null) {
            withStartAction.withEndAction(runnable);
        }
        return withStartAction;
    }
    
    static void animateNotifViews(final View view, final View view2, final ViewGroup viewGroup, final int n, final int n2, final int n3, final Runnable runnable) {
        ViewPropertyAnimator animateOut = null;
        final ViewPropertyAnimator viewPropertyAnimator = null;
        if (view2 != null) {
            animateOut = animateOut(view2, viewGroup, n, n2, 0, null);
        }
        ViewPropertyAnimator animateIn = viewPropertyAnimator;
        if (view != null) {
            animateIn = animateIn(view, viewGroup, n, n2, n3, runnable);
        }
        if (animateOut != null) {
            animateOut.start();
        }
        if (animateIn != null) {
            animateIn.start();
        }
    }
    
    private static ViewPropertyAnimator animateOut(final View view, final ViewGroup viewGroup, final int n, final int n2, final int n3, final Runnable runnable) {
        final ViewPropertyAnimator withEndAction = view.animate().alpha(0.0f).translationY((float)(-n)).setDuration((long)n2).setStartDelay((long)n3).withEndAction((Runnable)new Runnable() {
            @Override
            public void run() {
                viewGroup.removeView(view);
            }
        });
        if (runnable != null) {
            withEndAction.withEndAction(runnable);
        }
        return withEndAction;
    }
    
    public enum Operation
    {
        COLLAPSE_VIEW, 
        MOVE_NEXT, 
        MOVE_PREV, 
        REMOVE_NOTIFICATION, 
        SELECT, 
        UPDATE_INDICATOR;
    }
    
    public static class OperationInfo
    {
        boolean collapse;
        boolean gesture;
        boolean hideNotification;
        String id;
        Operation operation;
        boolean quick;
        
        OperationInfo(final Operation operation, final boolean hideNotification, final boolean quick, final boolean gesture, final String id, final boolean collapse) {
            this.operation = operation;
            this.hideNotification = hideNotification;
            this.quick = quick;
            this.gesture = gesture;
            this.id = id;
            this.collapse = collapse;
        }
    }
}
