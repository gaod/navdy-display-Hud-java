package com.navdy.hud.app.framework.network;

import com.navdy.hud.app.event.DriverProfileUpdated;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.HudApplication;
import java.util.List;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.service.library.task.TaskManager;
import java.io.Serializable;
import org.json.JSONArray;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import android.os.SystemClock;
import android.text.TextUtils;
import org.json.JSONObject;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import android.os.Looper;
import com.navdy.hud.app.util.os.SystemProperties;
import java.util.concurrent.TimeUnit;
import android.os.Handler;
import java.util.HashMap;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class NetworkBandwidthController
{
    private static final int ACTIVE;
    private static final UserBandwidthSettingChanged BW_SETTING_CHANGED;
    private static final String DATA = "data";
    private static final int DATA_COLLECTION_INITIAL_INTERVAL;
    private static final int DATA_COLLECTION_INTERVAL;
    private static final int DATA_COLLECTION_INTERVAL_ENG;
    private static final String EVENT = "event";
    private static final String EVENT_DNS_INFO = "dnslookup";
    private static final String EVENT_NETSTAT = "netstat";
    private static final String FD = "fd";
    private static final String FROM = "from";
    private static final String GOOGLE_DNS_IP = "8.8.4.4";
    private static final String HERE_ROUTE_END_POINT_PATTERN = "route.hybrid.api.here.com";
    private static final String HOST = "host";
    private static final String IP = "ip";
    private static final int NETWORK_STAT_INFO_PORT = 23655;
    private static final String PERM_DISABLE_NETWORK = "persist.sys.perm_disable_nw";
    private static final String RX = "rx";
    private static final String TO = "to";
    private static final String TX = "tx";
    private static final boolean networkDisabled;
    private static final Logger sLogger;
    private static final NetworkBandwidthController singleton;
    private Bus bus;
    private final HashMap<Component, ComponentInfo> componentInfoMap;
    private Runnable dataCollectionRunnable;
    private Runnable dataCollectionRunnableBk;
    private DnsCache dnsCache;
    private Handler handler;
    private boolean limitBandwidthModeOn;
    private Runnable networkStatRunnable;
    private Thread networkStatThread;
    private NetworkStatCache statCache;
    private boolean trafficDataDownloadedOnce;
    private final HashMap<String, Component> urlToComponentMap;
    
    static {
        sLogger = new Logger("NetworkBandwidthControl");
        ACTIVE = (int)TimeUnit.SECONDS.toMillis(15L);
        BW_SETTING_CHANGED = new UserBandwidthSettingChanged();
        networkDisabled = SystemProperties.getBoolean("persist.sys.perm_disable_nw", false);
        if (NetworkBandwidthController.networkDisabled) {
            NetworkBandwidthController.sLogger.v("n/w disabled permanently");
        }
        DATA_COLLECTION_INITIAL_INTERVAL = (int)TimeUnit.SECONDS.toMillis(25L);
        DATA_COLLECTION_INTERVAL_ENG = (int)TimeUnit.MINUTES.toMillis(1L);
        DATA_COLLECTION_INTERVAL = (int)TimeUnit.MINUTES.toMillis(5L);
        singleton = new NetworkBandwidthController();
    }
    
    private NetworkBandwidthController() {
        this.componentInfoMap = new HashMap<Component, ComponentInfo>();
        this.urlToComponentMap = new HashMap<String, Component>();
        this.dnsCache = new DnsCache();
        this.statCache = new NetworkStatCache(this.dnsCache);
        this.handler = new Handler(Looper.getMainLooper());
        this.networkStatRunnable = new Runnable() {
            @Override
            public void run() {
                Object o = null;
                while (true) {
                    try {
                        Object o2 = null;
                    Label_0052_Outer:
                        while (true) {
                            NetworkBandwidthController.sLogger.v("networkStat thread enter");
                            final DatagramSocket datagramSocket = new DatagramSocket(23655, InetAddress.getByName("127.0.0.1"));
                            int int2 = 0;
                            Serializable here_ROUTE = null;
                        Label_0052:
                            while (true) {
                                Label_0399: {
                                Label_0270:
                                    while (true) {
                                        String string = null;
                                        Label_0254: {
                                            Label_0238: {
                                                try {
                                                    datagramSocket.setReceiveBufferSize(16384);
                                                    final byte[] array = new byte[2048];
                                                    final DatagramPacket datagramPacket = new DatagramPacket(array, array.length);
                                                    while (true) {
                                                        try {
                                                            while (true) {
                                                                datagramSocket.receive(datagramPacket);
                                                                o = new JSONObject(new String(array, 0, datagramPacket.getLength()));
                                                                string = ((JSONObject)o).getString("event");
                                                                final int n = -1;
                                                                switch (string.hashCode()) {
                                                                    default:
                                                                        switch (n) {
                                                                            default:
                                                                                NetworkBandwidthController.sLogger.v("invalid command:" + string);
                                                                                continue Label_0052_Outer;
                                                                            case 0:
                                                                                break Label_0270;
                                                                            case 1:
                                                                                break Label_0399;
                                                                        }
                                                                        break;
                                                                    case 260369379:
                                                                        break Label_0238;
                                                                    case 1843370353:
                                                                        break Label_0254;
                                                                }
                                                            }
                                                        }
                                                        catch (Throwable t) {
                                                            NetworkBandwidthController.sLogger.e("[networkStat]", t);
                                                            continue Label_0052_Outer;
                                                        }
                                                        continue Label_0052;
                                                    }
                                                }
                                                catch (Throwable o) {
                                                    o2 = datagramSocket;
                                                }
                                                break;
                                            }
                                            if (string.equals("dnslookup")) {
                                                final int n = 0;
                                                continue;
                                            }
                                            continue;
                                        }
                                        if (string.equals("netstat")) {
                                            final int n = 1;
                                            continue;
                                        }
                                        continue;
                                    }
                                    final String string2 = ((JSONObject)o).getString("host");
                                    if ("localhost".equals(string2)) {
                                        continue Label_0052;
                                    }
                                    o = ((JSONObject)o).getJSONArray("ip");
                                    final int length = ((JSONArray)o).length();
                                    if (length != 0) {
                                        for (int i = 0; i < length; ++i) {
                                            final String string3 = ((JSONArray)o).getString(i);
                                            NetworkBandwidthController.this.dnsCache.addEntry(string3, string2);
                                            if (NetworkBandwidthController.sLogger.isLoggable(2)) {
                                                NetworkBandwidthController.sLogger.v("dnslookup " + string2 + " : " + string3);
                                            }
                                        }
                                        continue Label_0052;
                                    }
                                    continue Label_0052;
                                }
                                final JSONArray jsonArray = ((JSONObject)o).getJSONArray("data");
                                if (jsonArray.length() == 0) {
                                    continue Label_0052;
                                }
                                o = jsonArray.getJSONObject(0);
                                final String string4 = ((JSONObject)o).getString("from");
                                final String string5 = ((JSONObject)o).getString("to");
                                final int int1 = ((JSONObject)o).getInt("tx");
                                int2 = ((JSONObject)o).getInt("rx");
                                final int int3 = ((JSONObject)o).getInt("fd");
                                if (NetworkBandwidthController.sLogger.isLoggable(2)) {
                                    final Logger access$000 = NetworkBandwidthController.sLogger;
                                    o = new StringBuilder();
                                    access$000.v(((StringBuilder)o).append("[").append(int3).append("] tx[").append(int1).append("] rx[").append(int2).append("] from[").append(string4).append("] to[").append(string5).append("]").toString());
                                }
                                o = string5;
                                if (TextUtils.isEmpty((CharSequence)string5)) {
                                    o = string4;
                                }
                                if ("8.8.4.4".equals(o)) {
                                    continue Label_0052;
                                }
                                NetworkBandwidthController.this.statCache.addStat((String)o, int1, int2, int3);
                                if (int1 <= 0 && int2 <= 0) {
                                    continue Label_0052;
                                }
                                final String hostnamefromIP = NetworkBandwidthController.this.dnsCache.getHostnamefromIP((String)o);
                                if (hostnamefromIP == null) {
                                    continue Label_0052;
                                }
                                o = NetworkBandwidthController.this.urlToComponentMap.get(hostnamefromIP);
                                if ((here_ROUTE = (Serializable)o) == null) {
                                    here_ROUTE = (Serializable)o;
                                    if (hostnamefromIP.contains("route.hybrid.api.here.com")) {
                                        here_ROUTE = Component.HERE_ROUTE;
                                    }
                                }
                                if (here_ROUTE == null) {
                                    continue Label_0052;
                                }
                                break;
                            }
                            if (!NetworkBandwidthController.this.trafficDataDownloadedOnce && here_ROUTE == Component.HERE_TRAFFIC && int2 > 0) {
                                NetworkBandwidthController.sLogger.i("traffic data downloaded once");
                                NetworkBandwidthController.this.trafficDataDownloadedOnce = true;
                            }
                            final ComponentInfo componentInfo = NetworkBandwidthController.this.componentInfoMap.get(here_ROUTE);
                            synchronized (componentInfo) {
                                componentInfo.lastActivity = SystemClock.elapsedRealtime();
                            }
                        }
                        NetworkBandwidthController.sLogger.e((Throwable)o);
                        if (o2 != null) {
                            IOUtils.closeStream((Closeable)o2);
                        }
                        NetworkBandwidthController.sLogger.v("networkStat thread exit");
                    }
                    catch (Throwable t2) {
                        final Object o2 = o;
                        o = t2;
                        continue;
                    }
                    break;
                }
            }
        };
        this.dataCollectionRunnable = new Runnable() {
            @Override
            public void run() {
                TaskManager.getInstance().execute(NetworkBandwidthController.this.dataCollectionRunnableBk, 1);
            }
        };
        this.dataCollectionRunnableBk = new Runnable() {
            @Override
            public void run() {
                try {
                    NetworkBandwidthController.this.statCache.dump(NetworkBandwidthController.sLogger, true);
                    final Handler access$900 = NetworkBandwidthController.this.handler;
                    long n;
                    if (DeviceUtil.isUserBuild()) {
                        n = NetworkBandwidthController.DATA_COLLECTION_INTERVAL;
                    }
                    else {
                        n = NetworkBandwidthController.DATA_COLLECTION_INTERVAL_ENG;
                    }
                    access$900.postDelayed((Runnable)this, n);
                }
                catch (Throwable t) {
                    NetworkBandwidthController.sLogger.e(t);
                    final Handler access$901 = NetworkBandwidthController.this.handler;
                    long n2;
                    if (DeviceUtil.isUserBuild()) {
                        n2 = NetworkBandwidthController.DATA_COLLECTION_INTERVAL;
                    }
                    else {
                        n2 = NetworkBandwidthController.DATA_COLLECTION_INTERVAL_ENG;
                    }
                    access$901.postDelayed((Runnable)this, n2);
                }
                finally {
                    final Handler access$902 = NetworkBandwidthController.this.handler;
                    long n3;
                    if (DeviceUtil.isUserBuild()) {
                        n3 = NetworkBandwidthController.DATA_COLLECTION_INTERVAL;
                    }
                    else {
                        n3 = NetworkBandwidthController.DATA_COLLECTION_INTERVAL_ENG;
                    }
                    while (true) {
                        access$902.postDelayed((Runnable)this, n3);
                        continue;
                    }
                }
            }
        };
        this.bus = RemoteDeviceManager.getInstance().getBus();
        this.urlToComponentMap.put("v102-62-30-8.route.hybrid.api.here.com", Component.HERE_ROUTE);
        this.urlToComponentMap.put("tpeg.traffic.api.here.com", Component.HERE_TRAFFIC);
        this.urlToComponentMap.put("tpeg.hybrid.api.here.com", Component.HERE_TRAFFIC);
        this.urlToComponentMap.put("download.hybrid.api.here.com", Component.HERE_MAP_DOWNLOAD);
        this.urlToComponentMap.put("reverse.geocoder.api.here.com", Component.HERE_REVERSE_GEO);
        this.urlToComponentMap.put("analytics.localytics.com", Component.LOCALYTICS);
        this.urlToComponentMap.put("profile.localytics.com", Component.LOCALYTICS);
        this.urlToComponentMap.put("sdk.hockeyapp.net", Component.HOCKEY);
        this.urlToComponentMap.put("navdyhud.atlassian.net", Component.JIRA);
        this.componentInfoMap.put(Component.LOCALYTICS, new ComponentInfo());
        this.componentInfoMap.put(Component.HERE_ROUTE, new ComponentInfo());
        this.componentInfoMap.put(Component.HERE_TRAFFIC, new ComponentInfo());
        this.componentInfoMap.put(Component.HERE_MAP_DOWNLOAD, new ComponentInfo());
        this.componentInfoMap.put(Component.HERE_REVERSE_GEO, new ComponentInfo());
        this.componentInfoMap.put(Component.HOCKEY, new ComponentInfo());
        this.componentInfoMap.put(Component.JIRA, new ComponentInfo());
        (this.networkStatThread = new Thread(this.networkStatRunnable)).setName("hudNetStatThread");
        this.networkStatThread.start();
        NetworkBandwidthController.sLogger.v("networkStat thread started");
        this.handler.postDelayed(this.dataCollectionRunnable, (long)NetworkBandwidthController.DATA_COLLECTION_INITIAL_INTERVAL);
        this.limitBandwidthModeOn = DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile().isLimitBandwidthModeOn();
        this.bus.register(this);
        NetworkBandwidthController.sLogger.v("registered bus");
    }
    
    public static NetworkBandwidthController getInstance() {
        return NetworkBandwidthController.singleton;
    }
    
    private void handleBandwidthPreferenceChange() {
        final boolean limitBandwidthModeOn = DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile().isLimitBandwidthModeOn();
        if (limitBandwidthModeOn == this.limitBandwidthModeOn) {
            NetworkBandwidthController.sLogger.v("limitbandwidth: no-op current[" + this.limitBandwidthModeOn + "] new [" + limitBandwidthModeOn + "]");
        }
        else {
            NetworkBandwidthController.sLogger.v("limitbandwidth: changed current[" + this.limitBandwidthModeOn + "] new [" + limitBandwidthModeOn + "]");
            this.limitBandwidthModeOn = limitBandwidthModeOn;
            if (limitBandwidthModeOn) {
                NetworkBandwidthController.sLogger.v("limitbandwidth: on");
            }
            else {
                NetworkBandwidthController.sLogger.v("limitbandwidth: off");
            }
            HereMapsManager.getInstance().handleBandwidthPreferenceChange();
            this.bus.post(NetworkBandwidthController.BW_SETTING_CHANGED);
        }
    }
    
    private boolean isComponentActive(final Component component) {
        final long lastActivityTime = this.getLastActivityTime(component);
        if (lastActivityTime <= 0L) {
            return false;
        }
        final long n = SystemClock.elapsedRealtime() - lastActivityTime;
        if (n > NetworkBandwidthController.ACTIVE) {
            return false;
        }
        NetworkBandwidthController.sLogger.v("component is ACTIVE:" + component + " diff:" + n);
        return true;
        b = false;
        return b;
    }
    
    public List<NetworkStatCache.NetworkStatCacheInfo> getBootStat() {
        return this.statCache.getBootStat();
    }
    
    public long getLastActivityTime(final Component component) {
        final ComponentInfo componentInfo = this.componentInfoMap.get(component);
        long lastActivity;
        if (componentInfo == null) {
            lastActivity = 0L;
        }
        else {
            synchronized (componentInfo) {
                lastActivity = componentInfo.lastActivity;
            }
        }
        return lastActivity;
    }
    
    public List<NetworkStatCache.NetworkStatCacheInfo> getSessionStat() {
        return this.statCache.getSessionStat();
    }
    
    public boolean isLimitBandwidthModeOn() {
        return this.limitBandwidthModeOn;
    }
    
    public boolean isNetworkAccessAllowed(final Component component) {
        boolean b = false;
        if (!NetworkStateManager.isConnectedToNetwork(HudApplication.getAppContext())) {
            NetworkBandwidthController.sLogger.v("n/w access: not connected to network:" + component);
        }
        else {
            switch (component) {
                default:
                    NetworkBandwidthController.sLogger.v("n/w access allowed:" + component);
                    b = true;
                    break;
                case LOCALYTICS:
                case JIRA:
                case HOCKEY:
                    if (this.isComponentActive(Component.HERE_ROUTE)) {
                        NetworkBandwidthController.sLogger.v("n/w access not allowed:" + component);
                        break;
                    }
                    if (this.isComponentActive(Component.HERE_TRAFFIC)) {
                        NetworkBandwidthController.sLogger.v("n/w access not allowed:" + component);
                        break;
                    }
                    NetworkBandwidthController.sLogger.v("n/w access allowed:" + component);
                    b = true;
                    break;
            }
        }
        return b;
    }
    
    public boolean isNetworkDisabled() {
        return NetworkBandwidthController.networkDisabled;
    }
    
    public boolean isTrafficDataDownloadedOnce() {
        return this.trafficDataDownloadedOnce;
    }
    
    public void netStat() {
        this.handler.removeCallbacks(this.dataCollectionRunnable);
        this.dataCollectionRunnable.run();
    }
    
    @Subscribe
    public void onDriverProfileChanged(final DriverProfileChanged driverProfileChanged) {
        NetworkBandwidthController.sLogger.v("driver profile changed");
        this.handleBandwidthPreferenceChange();
    }
    
    @Subscribe
    public void onDriverProfileUpdated(final DriverProfileUpdated driverProfileUpdated) {
        NetworkBandwidthController.sLogger.v("driver profile updated");
        if (driverProfileUpdated.state == DriverProfileUpdated.State.UPDATED) {
            this.handleBandwidthPreferenceChange();
        }
    }
    
    public enum Component
    {
        HERE_MAP_DOWNLOAD, 
        HERE_REVERSE_GEO, 
        HERE_ROUTE, 
        HERE_TRAFFIC, 
        HOCKEY, 
        JIRA, 
        LOCALYTICS;
    }
    
    private static class ComponentInfo
    {
        public long lastActivity;
    }
    
    public static class UserBandwidthSettingChanged
    {
    }
}
