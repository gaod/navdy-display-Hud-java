package com.navdy.hud.app.framework.destinations;

import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.service.library.events.destination.Destination;
import com.navdy.hud.app.view.ToastView;
import com.navdy.hud.app.framework.toast.IToastCallback;
import com.navdy.hud.app.maps.util.RouteUtils;
import java.util.concurrent.TimeUnit;
import com.squareup.wire.Wire;
import android.text.TextUtils;
import android.os.Bundle;
import com.navdy.service.library.events.places.SuggestedDestination;
import com.navdy.hud.app.framework.toast.ToastManager;
import android.content.res.Resources;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import java.util.ArrayList;
import com.navdy.service.library.log.Logger;

public class DestinationSuggestionToast
{
    public static final String DESTINATION_SUGGESTION_TOAST_ID = "connection#toast";
    private static final int TAG_ACCEPT = 1;
    private static final int TAG_IGNORE = 2;
    private static final int TOAST_TIMEOUT = 15000;
    private static final String accept;
    private static final String ignore;
    public static final Logger sLogger;
    private static final ArrayList<ChoiceLayout.Choice> suggestionChoices;
    private static final String title;
    
    static {
        sLogger = new Logger(DestinationSuggestionToast.class);
        suggestionChoices = new ArrayList<ChoiceLayout.Choice>(2);
        final Resources resources = HudApplication.getAppContext().getResources();
        title = resources.getString(R.string.suggested_trip);
        accept = resources.getString(R.string.accept);
        ignore = resources.getString(R.string.ignore);
        DestinationSuggestionToast.suggestionChoices.add(new ChoiceLayout.Choice(DestinationSuggestionToast.accept, 1));
        DestinationSuggestionToast.suggestionChoices.add(new ChoiceLayout.Choice(DestinationSuggestionToast.ignore, 2));
    }
    
    public static void dismissSuggestionToast() {
        final ToastManager instance = ToastManager.getInstance();
        instance.clearPendingToast("connection#toast");
        instance.dismissCurrentToast("connection#toast");
    }
    
    public static void showSuggestion(final SuggestedDestination suggestedDestination) {
        final Destination destination = suggestedDestination.destination;
        final int n = R.drawable.icon_places_favorite;
        final Bundle bundle = new Bundle();
        bundle.putInt("13", 15000);
        int n2 = n;
        if (suggestedDestination.type != null) {
            switch (suggestedDestination.type) {
                default:
                    n2 = n;
                    break;
                case SUGGESTION_CALENDAR:
                    n2 = R.drawable.icon_place_calendar;
                    break;
            }
        }
        int n3;
        if ((n3 = n2) == R.drawable.icon_places_favorite) {
            switch (destination.favorite_type) {
                default:
                    n3 = n2;
                    break;
                case FAVORITE_HOME:
                    n3 = R.drawable.icon_places_home;
                    break;
                case FAVORITE_WORK:
                    n3 = R.drawable.icon_places_work;
                    break;
                case FAVORITE_CUSTOM:
                    n3 = R.drawable.icon_places_favorite;
                    break;
                case FAVORITE_NONE:
                    n3 = R.drawable.icon_mm_suggested_places;
                    break;
            }
        }
        bundle.putInt("8", n3);
        bundle.putInt("1_1", R.style.ToastMainTitle);
        bundle.putBoolean("19", true);
        bundle.putString("1", DestinationSuggestionToast.title);
        bundle.putParcelableArrayList("20", (ArrayList)DestinationSuggestionToast.suggestionChoices);
        String s;
        if (!TextUtils.isEmpty((CharSequence)destination.destination_title)) {
            s = destination.destination_title;
        }
        else if (!TextUtils.isEmpty((CharSequence)destination.destination_subtitle)) {
            s = destination.destination_subtitle;
        }
        else {
            s = destination.full_address;
        }
        final int intValue = Wire.<Integer>get(suggestedDestination.duration_traffic, -1);
        final Resources resources = HudApplication.getAppContext().getResources();
        if (resources != null) {
            String s2;
            if (intValue > 0) {
                s2 = resources.getString(R.string.suggested_eta, new Object[] { RouteUtils.formatEtaMinutes(resources, (int)TimeUnit.SECONDS.toMinutes(intValue)) });
            }
            else if (!TextUtils.isEmpty((CharSequence)destination.destination_subtitle)) {
                s2 = destination.destination_subtitle;
            }
            else {
                s2 = destination.full_address;
            }
            bundle.putString("4", s);
            bundle.putInt("5", R.style.Toast_2);
            bundle.putString("6", s2);
            bundle.putInt("7", R.style.Toast_1);
            bundle.putInt("16", resources.getDimensionPixelSize(R.dimen.expand_notif_width));
            final IToastCallback toastCallback = new IToastCallback() {
                ToastView toastView;
                
                @Override
                public void executeChoiceItem(final int n, final int n2) {
                    if (this.toastView != null) {
                        boolean b = false;
                        switch (n2) {
                            case 1:
                                b = true;
                                this.toastView.dismissToast();
                                DestinationsManager.getInstance().goToSuggestedDestination();
                                break;
                            case 2:
                                b = false;
                                this.toastView.dismissToast();
                                break;
                        }
                        while (true) {
                            while (true) {
                                Label_0102: {
                                    try {
                                        if (destination.suggestion_type != Destination.SuggestionType.SUGGESTION_CALENDAR) {
                                            break Label_0102;
                                        }
                                        final boolean b2 = true;
                                        AnalyticsSupport.recordDestinationSuggestion(b, b2);
                                    }
                                    catch (Throwable t) {
                                        DestinationSuggestionToast.sLogger.e("Error while recording the destination suggestion", t);
                                    }
                                    break;
                                }
                                final boolean b2 = false;
                                continue;
                            }
                        }
                    }
                }
                
                @Override
                public boolean onKey(final InputManager.CustomKeyEvent customKeyEvent) {
                    return false;
                }
                
                @Override
                public void onStart(final ToastView toastView) {
                    this.toastView = toastView;
                    final ConfirmationLayout confirmation = toastView.getConfirmation();
                    confirmation.screenTitle.setTextAppearance(toastView.getContext(), R.style.mainTitle);
                    confirmation.title3.setTextColor(toastView.getResources().getColor(R.color.hud_white));
                }
                
                @Override
                public void onStop() {
                    this.toastView = null;
                }
            };
            boolean b = true;
            final ToastManager instance = ToastManager.getInstance();
            if (TextUtils.equals((CharSequence)instance.getCurrentToastId(), (CharSequence)"incomingcall#toast")) {
                DestinationSuggestionToast.sLogger.v("phone toast active");
                b = false;
            }
            else {
                instance.dismissCurrentToast(instance.getCurrentToastId());
                instance.clearAllPendingToast();
            }
            instance.addToast(new ToastManager.ToastParams("connection#toast", bundle, toastCallback, true, b));
        }
    }
}
