package com.navdy.hud.app.framework.toast;

import com.navdy.hud.app.view.ToastView;
import com.navdy.hud.app.manager.InputManager;

public interface IToastCallback
{
    void executeChoiceItem(final int p0, final int p1);
    
    boolean onKey(final InputManager.CustomKeyEvent p0);
    
    void onStart(final ToastView p0);
    
    void onStop();
}
