package com.navdy.hud.app.framework.music;

import com.navdy.service.library.util.IOUtils;
import android.content.res.TypedArray;
import android.graphics.Shader;
import com.navdy.hud.app.R;
import android.text.TextUtils;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.Xfermode;
import android.graphics.PorterDuffXfermode;
import android.graphics.PorterDuff;
import android.graphics.Shader;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.os.Looper;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.RadialGradient;
import android.graphics.Paint;
import android.graphics.Bitmap;
import java.util.concurrent.atomic.AtomicBoolean;
import android.os.Handler;
import android.graphics.drawable.BitmapDrawable;
import com.navdy.service.library.log.Logger;
import android.widget.ImageView;

public class AlbumArtImageView extends ImageView
{
    private static final int ARTWORK_TRANSITION_DURATION = 1000;
    private static Logger logger;
    private final Object artworkLock;
    private BitmapDrawable defaultArtwork;
    private Handler handler;
    private AtomicBoolean isAnimatingArtwork;
    private boolean mask;
    private Bitmap nextArtwork;
    private String nextArtworkHash;
    private Paint paint;
    private RadialGradient radialGradient;
    
    static {
        AlbumArtImageView.logger = new Logger(AlbumArtImageView.class);
    }
    
    public AlbumArtImageView(final Context context) {
        this(context, null);
    }
    
    public AlbumArtImageView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public AlbumArtImageView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.isAnimatingArtwork = new AtomicBoolean(false);
        this.artworkLock = new Object();
        this.handler = new Handler(Looper.getMainLooper());
        this.init(set);
    }
    
    @Nullable
    private Bitmap addMask(@Nullable Bitmap bitmap) {
        if (bitmap == null) {
            bitmap = null;
        }
        else {
            final int width = bitmap.getWidth();
            final int height = bitmap.getHeight();
            final Bitmap bitmap2 = Bitmap.createBitmap(width, height, Bitmap$Config.ARGB_8888);
            final Canvas canvas = new Canvas(bitmap2);
            canvas.drawBitmap(bitmap, 0.0f, 0.0f, (Paint)null);
            this.paint.setShader((Shader)this.radialGradient);
            this.paint.setXfermode((Xfermode)new PorterDuffXfermode(PorterDuff$Mode.MULTIPLY));
            canvas.drawRect(0.0f, 0.0f, (float)width, (float)height, this.paint);
            bitmap = bitmap2;
        }
        return bitmap;
    }
    
    private void animateArtwork() {
    Label_0073_Outer:
        while (true) {
            Object artworkLock = this.artworkLock;
            while (true) {
            Label_0145:
                while (true) {
                    final Drawable drawable;
                    synchronized (artworkLock) {
                        final String nextArtworkHash = this.nextArtworkHash;
                        Object nextArtwork = this.nextArtwork;
                        // monitorexit(artworkLock)
                        artworkLock = new Drawable[2];
                        drawable = this.getDrawable();
                        if (drawable == null) {
                            artworkLock[0] = new ColorDrawable(0);
                            if (nextArtwork != null) {
                                artworkLock[1] = new BitmapDrawable(this.getContext().getResources(), (Bitmap)nextArtwork);
                                nextArtwork = new TransitionDrawable((Drawable[])artworkLock);
                                this.setImageDrawable((Drawable)nextArtwork);
                                ((TransitionDrawable)nextArtwork).setCrossFadeEnabled(true);
                                ((TransitionDrawable)nextArtwork).startTransition(1000);
                                this.handler.postDelayed((Runnable)new Runnable() {
                                    @Override
                                    public void run() {
                                        while (true) {
                                            AlbumArtImageView.this.setImageDrawable((Drawable)new LayerDrawable(new Drawable[] { artworkLock[1] }));
                                            final Object access$000 = AlbumArtImageView.this.artworkLock;
                                            synchronized (access$000) {
                                                final boolean equals = TextUtils.equals((CharSequence)nextArtworkHash, (CharSequence)AlbumArtImageView.this.nextArtworkHash);
                                                // monitorexit(access$000)
                                                if (equals) {
                                                    AlbumArtImageView.this.isAnimatingArtwork.set(false);
                                                    return;
                                                }
                                            }
                                            AlbumArtImageView.this.animateArtwork();
                                        }
                                    }
                                }, 1000L);
                                return;
                            }
                            break Label_0145;
                        }
                    }
                    artworkLock[0] = ((LayerDrawable)drawable).getDrawable(0);
                    continue Label_0073_Outer;
                }
                AlbumArtImageView.logger.i("Bitmap image is null");
                artworkLock[1] = this.defaultArtwork;
                continue;
            }
        }
    }
    
    private void drawImmediately() {
        this.isAnimatingArtwork.set(false);
        this.handler.removeCallbacksAndMessages(null);
        final Drawable[] array = { null };
        if (this.nextArtwork != null) {
            array[0] = (Drawable)new BitmapDrawable(this.getContext().getResources(), this.nextArtwork);
        }
        else {
            AlbumArtImageView.logger.i("Bitmap image is null");
            array[0] = (Drawable)this.defaultArtwork;
        }
        this.setImageDrawable((Drawable)new LayerDrawable(array));
    }
    
    private void init(final AttributeSet set) {
        int resourceId = R.drawable.icon_mm_music_control_blank;
        if (set != null) {
            final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.AlbumArtImageView);
            this.mask = obtainStyledAttributes.getBoolean(0, false);
            resourceId = obtainStyledAttributes.getResourceId(1, R.drawable.icon_mm_music_control_blank);
            obtainStyledAttributes.recycle();
        }
        final BitmapDrawable defaultArtwork = (BitmapDrawable)this.getResources().getDrawable(resourceId);
        if (this.mask) {
            this.paint = new Paint();
            final int[] intArray = this.getResources().getIntArray(R.array.smart_dash_music_gauge_gradient);
            final TypedArray obtainStyledAttributes2 = this.getContext().obtainStyledAttributes(set, new int[] { 16842996 });
            final float n = obtainStyledAttributes2.getDimensionPixelSize(0, 130) / 2;
            obtainStyledAttributes2.recycle();
            this.radialGradient = new RadialGradient(n, n, n, intArray, (float[])null, Shader$TileMode.CLAMP);
            this.defaultArtwork = new BitmapDrawable(this.getResources(), this.addMask(defaultArtwork.getBitmap()));
        }
        else {
            this.defaultArtwork = defaultArtwork;
        }
        this.setImageDrawable((Drawable)new LayerDrawable(new Drawable[] { this.defaultArtwork }));
    }
    
    public void setArtworkBitmap(@Nullable final Bitmap bitmap, final boolean b) {
        final String hashForBitmap = IOUtils.hashForBitmap(bitmap);
        if (TextUtils.equals((CharSequence)this.nextArtworkHash, (CharSequence)hashForBitmap)) {
            AlbumArtImageView.logger.d("Already set this artwork, ignoring");
        }
        else {
            synchronized (this) {
                this.nextArtworkHash = hashForBitmap;
                Bitmap addMask = bitmap;
                if (this.mask) {
                    addMask = this.addMask(bitmap);
                }
                this.nextArtwork = addMask;
                // monitorexit(this)
                if (!b) {
                    this.drawImmediately();
                    return;
                }
            }
            if (!this.isAnimatingArtwork.compareAndSet(false, true)) {
                AlbumArtImageView.logger.d("Already animating");
            }
            else {
                this.animateArtwork();
            }
        }
    }
}
