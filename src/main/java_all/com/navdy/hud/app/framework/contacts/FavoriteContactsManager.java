package com.navdy.hud.app.framework.contacts;

import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.service.library.events.contacts.FavoriteContactsRequest;
import com.squareup.otto.Subscribe;
import java.util.Iterator;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashSet;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.contacts.FavoriteContactsResponse;
import com.navdy.service.library.task.TaskManager;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.storage.db.helper.FavoriteContactsPersistenceHelper;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import java.util.List;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class FavoriteContactsManager
{
    private static final FavoriteContactsChanged FAVORITE_CONTACTS_CHANGED;
    private static final int MAX_FAV_CONTACTS = 30;
    private static final FavoriteContactsManager sInstance;
    private static final Logger sLogger;
    private Bus bus;
    private volatile List<Contact> favContacts;
    private boolean fullSync;
    private boolean isAllowedToReceiveContacts;
    
    static {
        sLogger = new Logger(FavoriteContactsManager.class);
        FAVORITE_CONTACTS_CHANGED = new FavoriteContactsChanged();
        sInstance = new FavoriteContactsManager();
    }
    
    private FavoriteContactsManager() {
        this.isAllowedToReceiveContacts = false;
        this.fullSync = true;
        (this.bus = RemoteDeviceManager.getInstance().getBus()).register(this);
    }
    
    public static FavoriteContactsManager getInstance() {
        return FavoriteContactsManager.sInstance;
    }
    
    private void load() {
        try {
            final String profileName = DriverProfileHelper.getInstance().getCurrentProfile().getProfileName();
            this.favContacts = FavoriteContactsPersistenceHelper.getFavoriteContacts(profileName);
            FavoriteContactsManager.sLogger.v("load favcontacts id[" + profileName + "] number of contacts[" + this.favContacts.size() + "]");
        }
        catch (Throwable t) {
            this.favContacts = null;
            FavoriteContactsManager.sLogger.e(t);
            this.bus.post(FavoriteContactsManager.FAVORITE_CONTACTS_CHANGED);
        }
        finally {
            this.bus.post(FavoriteContactsManager.FAVORITE_CONTACTS_CHANGED);
        }
    }
    
    public void buildFavoriteContacts() {
        if (GenericUtil.isMainThread()) {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    FavoriteContactsManager.this.load();
                }
            }, 1);
        }
        else {
            this.load();
        }
    }
    
    public void clearFavoriteContacts() {
        this.setFavoriteContacts(null);
    }
    
    public List<Contact> getFavoriteContacts() {
        return this.favContacts;
    }
    
    public boolean isAllowedToReceiveContacts() {
        return this.isAllowedToReceiveContacts;
    }
    
    @Subscribe
    public void onFavoriteContactResponse(final FavoriteContactsResponse favoriteContactsResponse) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                ArrayList<Contact> list = null;
                Label_0338: {
                    while (true) {
                        Label_0413: {
                        Label_0402:
                            while (true) {
                                HashSet<String> set = null;
                                Label_0162: {
                                    try {
                                        if (favoriteContactsResponse.status != RequestStatus.REQUEST_SUCCESS) {
                                            break Label_0413;
                                        }
                                        FavoriteContactsManager.this.isAllowedToReceiveContacts = true;
                                        if (favoriteContactsResponse.contacts != null) {
                                            FavoriteContactsManager.sLogger.v("onFavoriteContactResponse size[" + favoriteContactsResponse.contacts.size() + "]");
                                            set = new HashSet<String>();
                                            list = new ArrayList<Contact>();
                                            for (final com.navdy.service.library.events.contacts.Contact contact : favoriteContactsResponse.contacts) {
                                                if (!TextUtils.isEmpty((CharSequence)contact.number)) {
                                                    break Label_0162;
                                                }
                                                FavoriteContactsManager.sLogger.v("null number");
                                            }
                                            break Label_0338;
                                        }
                                        break Label_0402;
                                    }
                                    catch (Throwable t) {
                                        FavoriteContactsManager.sLogger.e(t);
                                    }
                                    break;
                                }
                                final com.navdy.service.library.events.contacts.Contact contact;
                                if (set.contains(contact.number)) {
                                    FavoriteContactsManager.sLogger.v("number already seen:" + contact.number);
                                    continue;
                                }
                                final NumberType numberType = ContactUtil.getNumberType(contact.numberType);
                                String name = contact.name;
                                if (!TextUtils.isEmpty((CharSequence)contact.name)) {
                                    name = name;
                                    if (!ContactUtil.isDisplayNameValid(contact.name, contact.number, ContactUtil.getPhoneNumber(contact.number))) {
                                        name = null;
                                    }
                                }
                                list.add(new Contact(name, contact.number, numberType, ContactImageHelper.getInstance().getContactImageIndex(contact.number), 0L));
                                set.add(contact.number);
                                if (list.size() == 30) {
                                    FavoriteContactsManager.sLogger.v("exceeded max size");
                                    break Label_0338;
                                }
                                continue;
                            }
                            FavoriteContactsManager.sLogger.w("fav-contact list returned is null");
                            break;
                        }
                        if (favoriteContactsResponse.status == RequestStatus.REQUEST_NOT_AVAILABLE) {
                            FavoriteContactsManager.this.isAllowedToReceiveContacts = false;
                            break;
                        }
                        FavoriteContactsManager.sLogger.e("sent fav-contact response error:" + favoriteContactsResponse.status);
                        break;
                    }
                    return;
                }
                FavoriteContactsManager.sLogger.v("favorite contact size[" + list.size() + "]");
                FavoriteContactsPersistenceHelper.storeFavoriteContacts(DriverProfileHelper.getInstance().getCurrentProfile().getProfileName(), list, FavoriteContactsManager.this.fullSync);
            }
        }, 1);
    }
    
    public void setFavoriteContacts(final List<Contact> favContacts) {
        this.favContacts = favContacts;
        this.bus.post(FavoriteContactsManager.FAVORITE_CONTACTS_CHANGED);
    }
    
    public void syncFavoriteContacts(final boolean fullSync) {
        try {
            this.fullSync = fullSync;
            this.bus.post(new RemoteEvent(new FavoriteContactsRequest(Integer.valueOf(30))));
            FavoriteContactsManager.sLogger.v("sent fav-contact request");
        }
        catch (Throwable t) {
            FavoriteContactsManager.sLogger.e(t);
        }
    }
    
    private static class FavoriteContactsChanged
    {
    }
}
