package com.navdy.hud.app.framework.music;

import java.util.ArrayList;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.events.audio.MusicCollectionInfo;
import java.util.List;

public class MusicCollection
{
    public List<MusicCollectionInfo> collections;
    public MusicCollectionInfo musicCollectionInfo;
    public List<MusicTrackInfo> tracks;
    
    public MusicCollection() {
        this.collections = new ArrayList<MusicCollectionInfo>();
        this.tracks = new ArrayList<MusicTrackInfo>();
    }
}
