package com.navdy.hud.app.framework.notifications;

import android.content.Context;

public interface INotificationController
{
    void collapseNotification(final boolean p0, final boolean p1);
    
    void expandNotification(final boolean p0);
    
    Context getUIContext();
    
    boolean isExpanded();
    
    boolean isExpandedWithStack();
    
    boolean isShowOn();
    
    boolean isTtsOn();
    
    void moveNext(final boolean p0);
    
    void movePrevious(final boolean p0);
    
    void resetTimeout();
    
    void startTimeout(final int p0);
    
    void stopTimeout(final boolean p0);
}
