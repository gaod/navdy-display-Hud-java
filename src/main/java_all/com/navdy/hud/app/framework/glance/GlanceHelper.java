package com.navdy.hud.app.framework.glance;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import com.navdy.hud.app.framework.toast.IToastCallback;
import com.navdy.hud.app.framework.toast.ToastManager;
import android.os.Bundle;
import com.navdy.hud.app.framework.DriverProfileHelper;
import android.view.View;
import android.content.Context;
import android.text.StaticLayout;
import android.text.Layout;
import android.widget.TextView;
import com.navdy.service.library.events.glances.FuelConstants;
import com.navdy.service.library.events.glances.CalendarConstants;
import android.text.TextUtils;
import com.navdy.service.library.events.glances.GenericConstants;
import com.navdy.service.library.events.glances.SocialConstants;
import com.navdy.service.library.events.glances.EmailConstants;
import com.navdy.service.library.events.glances.MessageConstants;
import java.util.Date;
import com.navdy.hud.app.common.TimeHelper;
import java.util.Iterator;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.service.library.events.glances.KeyValue;
import java.util.Map;
import com.navdy.service.library.events.glances.GlanceEvent;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.log.Logger;
import android.content.res.Resources;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import java.util.concurrent.atomic.AtomicLong;
import java.util.HashMap;

public class GlanceHelper
{
    private static final HashMap<String, GlanceApp> APP_ID_MAP;
    public static final String DELETE_ALL_GLANCES_TOAST_ID = "glance-deleted";
    public static final String FUEL_PACKAGE = "com.navdy.fuel";
    private static final AtomicLong ID;
    public static final String IOS_PHONE_PACKAGE = "com.apple.mobilephone";
    public static final String MICROSOFT_OUTLOOK = "com.microsoft.Office.Outlook";
    public static final String MUSIC_PACKAGE = "com.navdy.music";
    public static final String PHONE_PACKAGE = "com.navdy.phone";
    public static final String SMS_PACKAGE = "com.navdy.sms";
    public static final String TRAFFIC_PACKAGE = "com.navdy.traffic";
    public static final String VOICE_SEARCH_PACKAGE = "com.navdy.voice.search";
    private static final StringBuilder builder;
    private static final GestureServiceConnector gestureServiceConnector;
    private static final Resources resources;
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(GlanceHelper.class);
        ID = new AtomicLong(1L);
        builder = new StringBuilder();
        (APP_ID_MAP = new HashMap<String, GlanceApp>()).put("com.navdy.fuel", GlanceApp.FUEL);
        GlanceHelper.APP_ID_MAP.put("com.google.android.calendar", GlanceApp.GOOGLE_CALENDAR);
        GlanceHelper.APP_ID_MAP.put("com.google.android.gm", GlanceApp.GOOGLE_MAIL);
        GlanceHelper.APP_ID_MAP.put("com.google.android.talk", GlanceApp.GOOGLE_HANGOUT);
        GlanceHelper.APP_ID_MAP.put("com.Slack", GlanceApp.SLACK);
        GlanceHelper.APP_ID_MAP.put("com.whatsapp", GlanceApp.WHATS_APP);
        GlanceHelper.APP_ID_MAP.put("com.facebook.orca", GlanceApp.FACEBOOK_MESSENGER);
        GlanceHelper.APP_ID_MAP.put("com.facebook.katana", GlanceApp.FACEBOOK);
        GlanceHelper.APP_ID_MAP.put("com.twitter.android", GlanceApp.TWITTER);
        GlanceHelper.APP_ID_MAP.put("com.navdy.sms", GlanceApp.SMS);
        GlanceHelper.APP_ID_MAP.put("com.google.android.apps.inbox", GlanceApp.GOOGLE_INBOX);
        GlanceHelper.APP_ID_MAP.put("com.google.calendar", GlanceApp.GOOGLE_CALENDAR);
        GlanceHelper.APP_ID_MAP.put("com.google.Gmail", GlanceApp.GOOGLE_MAIL);
        GlanceHelper.APP_ID_MAP.put("com.google.hangouts", GlanceApp.GOOGLE_HANGOUT);
        GlanceHelper.APP_ID_MAP.put("com.tinyspeck.chatlyio", GlanceApp.SLACK);
        GlanceHelper.APP_ID_MAP.put("net.whatsapp.WhatsApp", GlanceApp.WHATS_APP);
        GlanceHelper.APP_ID_MAP.put("com.facebook.Messenger", GlanceApp.FACEBOOK_MESSENGER);
        GlanceHelper.APP_ID_MAP.put("com.facebook.Facebook", GlanceApp.FACEBOOK);
        GlanceHelper.APP_ID_MAP.put("com.atebits.Tweetie2", GlanceApp.TWITTER);
        GlanceHelper.APP_ID_MAP.put("com.apple.MobileSMS", GlanceApp.IMESSAGE);
        GlanceHelper.APP_ID_MAP.put("com.apple.mobilemail", GlanceApp.APPLE_MAIL);
        GlanceHelper.APP_ID_MAP.put("com.apple.mobilecal", GlanceApp.APPLE_CALENDAR);
        GlanceHelper.APP_ID_MAP.put("com.microsoft.Office.Outlook", GlanceApp.GENERIC_MAIL);
        resources = HudApplication.getAppContext().getResources();
        gestureServiceConnector = RemoteDeviceManager.getInstance().getGestureServiceConnector();
    }
    
    public static Map<String, String> buildDataMap(final GlanceEvent glanceEvent) {
        final HashMap<String, String> hashMap = new HashMap<String, String>();
        if (glanceEvent.glanceData != null) {
            for (final KeyValue keyValue : glanceEvent.glanceData) {
                hashMap.put(keyValue.key, ContactUtil.sanitizeString(keyValue.value));
            }
        }
        return hashMap;
    }
    
    public static String getCalendarTime(final long n, final StringBuilder sb, final StringBuilder sb2, final TimeHelper timeHelper) {
        final long n2 = Math.round((n - System.currentTimeMillis()) / 60000.0f);
        sb.setLength(0);
        sb2.setLength(0);
        String s;
        if (n2 >= 60L || n2 < -5L) {
            s = timeHelper.formatTime12Hour(new Date(n), sb2, true);
        }
        else if (n2 <= 0L) {
            s = GlanceConstants.nowStr;
        }
        else {
            final String value = String.valueOf(n2);
            sb.append(GlanceConstants.minute);
            s = value;
        }
        return s;
    }
    
    public static String getFrom(final GlanceApp glanceApp, final Map<String, String> map) {
        final String s = null;
        String s2 = null;
        if (map == null) {
            s2 = null;
        }
        else {
            switch (glanceApp) {
                default:
                    s2 = s;
                    break;
                case GOOGLE_HANGOUT:
                case IMESSAGE:
                case SMS:
                    s2 = map.get(MessageConstants.MESSAGE_FROM.name());
                    break;
            }
        }
        return s2;
    }
    
    public static GestureServiceConnector getGestureService() {
        return GlanceHelper.gestureServiceConnector;
    }
    
    public static String getGlanceMessage(final GlanceApp glanceApp, final Map<String, String> map) {
        while (true) {
            while (true) {
                Label_0220: {
                    Label_0189: {
                        Label_0158: {
                            synchronized (GlanceHelper.class) {
                                switch (glanceApp) {
                                    case GOOGLE_MAIL:
                                    case APPLE_MAIL:
                                    case GENERIC_MAIL:
                                    case GOOGLE_INBOX: {
                                        final String s = map.get(EmailConstants.EMAIL_BODY.name());
                                        if (s != null) {
                                            GlanceHelper.builder.append(s);
                                            break;
                                        }
                                        break;
                                    }
                                    case GOOGLE_HANGOUT:
                                    case SLACK:
                                    case WHATS_APP:
                                    case FACEBOOK_MESSENGER:
                                    case GENERIC_MESSAGE:
                                    case IMESSAGE:
                                    case SMS:
                                        break Label_0158;
                                    case FACEBOOK:
                                    case TWITTER:
                                    case GENERIC_SOCIAL:
                                        break Label_0189;
                                    case GENERIC:
                                        break Label_0220;
                                }
                                final String string = GlanceHelper.builder.toString();
                                GlanceHelper.builder.setLength(0);
                                return string;
                            }
                        }
                        final String s2 = map.get(MessageConstants.MESSAGE_BODY.name());
                        if (s2 != null) {
                            GlanceHelper.builder.append(s2);
                            continue;
                        }
                        continue;
                    }
                    final String s3 = map.get(SocialConstants.SOCIAL_MESSAGE.name());
                    if (s3 != null) {
                        GlanceHelper.builder.append(s3);
                        continue;
                    }
                    continue;
                }
                final String s4 = map.get(GenericConstants.GENERIC_MESSAGE.name());
                if (s4 != null) {
                    GlanceHelper.builder.append(s4);
                    continue;
                }
                continue;
            }
        }
    }
    
    public static GlanceApp getGlancesApp(final GlanceEvent glanceEvent) {
        GlanceApp glanceApp;
        if (glanceEvent == null) {
            glanceApp = null;
        }
        else if ((glanceApp = GlanceHelper.APP_ID_MAP.get(glanceEvent.provider)) == null) {
            switch (glanceEvent.glanceType) {
                default:
                    glanceApp = null;
                    break;
                case GLANCE_TYPE_EMAIL:
                    glanceApp = GlanceApp.GENERIC_MAIL;
                    break;
                case GLANCE_TYPE_CALENDAR:
                    glanceApp = GlanceApp.GENERIC_CALENDAR;
                    break;
                case GLANCE_TYPE_MESSAGE:
                    glanceApp = GlanceApp.GENERIC_MESSAGE;
                    break;
                case GLANCE_TYPE_SOCIAL:
                    glanceApp = GlanceApp.GENERIC_SOCIAL;
                    break;
            }
        }
        return glanceApp;
    }
    
    public static GlanceApp getGlancesApp(final String s) {
        return GlanceHelper.APP_ID_MAP.get(s);
    }
    
    public static int getIcon(final String s) {
        final int n = -1;
        int n2 = 0;
        if (TextUtils.isEmpty((CharSequence)s)) {
            n2 = n;
        }
        else {
            int n3 = 0;
            Label_0046: {
                switch (s.hashCode()) {
                    case -9470533:
                        if (s.equals("GLANCE_ICON_NAVDY_MAIN")) {
                            n3 = 0;
                            break Label_0046;
                        }
                        break;
                    case -30680433:
                        if (s.equals("GLANCE_ICON_MESSAGE_SIDE_BLUE")) {
                            n3 = 1;
                            break Label_0046;
                        }
                        break;
                }
                n3 = -1;
            }
            switch (n3) {
                default:
                    n2 = n;
                    break;
                case 0:
                    n2 = R.drawable.icon_user_navdy;
                    break;
                case 1:
                    n2 = R.drawable.icon_message_blue;
                    break;
            }
        }
        return n2;
    }
    
    public static String getId() {
        return String.valueOf(GlanceHelper.ID.getAndIncrement());
    }
    
    public static GlanceViewCache.ViewType getLargeViewType(final GlanceApp glanceApp) {
        GlanceViewCache.ViewType viewType = null;
        switch (glanceApp) {
            default:
                viewType = GlanceViewCache.ViewType.BIG_GLANCE_MESSAGE_SINGLE;
                break;
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                viewType = GlanceViewCache.ViewType.BIG_CALENDAR;
                break;
            case FUEL:
                viewType = GlanceViewCache.ViewType.BIG_MULTI_TEXT;
                break;
        }
        return viewType;
    }
    
    public static String getNotificationId(final GlanceEvent.GlanceType glanceType, final String s) {
        String s2 = null;
        switch (glanceType) {
            default:
                s2 = "glance_gen_";
                break;
            case GLANCE_TYPE_CALENDAR:
                s2 = "glance_cal_";
                break;
            case GLANCE_TYPE_EMAIL:
                s2 = "glance_email_";
                break;
            case GLANCE_TYPE_MESSAGE:
                s2 = "glance_msg_";
                break;
            case GLANCE_TYPE_SOCIAL:
                s2 = "glance_soc_";
                break;
            case GLANCE_TYPE_FUEL:
                s2 = "glance_fuel_";
                break;
        }
        return s2 + s;
    }
    
    public static String getNotificationId(final GlanceEvent glanceEvent) {
        return getNotificationId(glanceEvent.glanceType, glanceEvent.id);
    }
    
    public static String getNumber(final GlanceApp glanceApp, final Map<String, String> map) {
        final String s = null;
        String s2 = null;
        if (map == null) {
            s2 = null;
        }
        else {
            switch (glanceApp) {
                default:
                    s2 = s;
                    break;
                case GOOGLE_HANGOUT:
                case IMESSAGE:
                case SMS:
                    s2 = map.get(MessageConstants.MESSAGE_FROM_NUMBER.name());
                    break;
            }
        }
        return s2;
    }
    
    public static GlanceViewCache.ViewType getSmallViewType(final GlanceApp glanceApp) {
        GlanceViewCache.ViewType viewType = null;
        switch (glanceApp) {
            default:
                viewType = GlanceViewCache.ViewType.SMALL_GLANCE_MESSAGE;
                break;
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                viewType = GlanceViewCache.ViewType.SMALL_SIGN;
                break;
        }
        return viewType;
    }
    
    public static String getSubTitle(final GlanceApp glanceApp, final Map<String, String> map) {
        final String s = "";
        String s2 = null;
        if (map == null) {
            s2 = "";
        }
        else {
            String s3 = s;
            while (true) {
                switch (glanceApp) {
                    default:
                        s3 = s;
                        break Label_0118;
                    case GENERIC:
                        s3 = map.get(GenericConstants.GENERIC_TITLE.name());
                        break Label_0118;
                    case GOOGLE_MAIL:
                    case APPLE_MAIL:
                    case GENERIC_MAIL:
                    case GOOGLE_INBOX:
                        s3 = map.get(EmailConstants.EMAIL_SUBJECT.name());
                        break Label_0118;
                    case GOOGLE_CALENDAR:
                    case APPLE_CALENDAR:
                    case GENERIC_CALENDAR:
                        s3 = map.get(CalendarConstants.CALENDAR_TIME_STR.name());
                    case GOOGLE_HANGOUT:
                    case WHATS_APP:
                    case FACEBOOK_MESSENGER:
                    case GENERIC_MESSAGE:
                    case FACEBOOK:
                    case TWITTER:
                    case GENERIC_SOCIAL:
                    case IMESSAGE:
                    case SMS:
                        if ((s2 = s3) == null) {
                            s2 = "";
                        }
                        break;
                    case SLACK: {
                        final String s4 = s3 = map.get(MessageConstants.MESSAGE_DOMAIN.name());
                        if (s4 != null) {
                            s3 = GlanceHelper.resources.getString(R.string.slack_team, new Object[] { s4 });
                        }
                        continue;
                    }
                    case FUEL:
                        s3 = s;
                        if (map.get(FuelConstants.NO_ROUTE.name()) == null) {
                            s3 = GlanceConstants.fuelNotificationSubtitle;
                        }
                        continue;
                }
                break;
            }
        }
        return s2;
    }
    
    public static String getTimeStr(final long n, final Date date, final TimeHelper timeHelper) {
        String s;
        if (n - date.getTime() <= 60000L) {
            s = GlanceConstants.nowStr;
        }
        else {
            s = timeHelper.formatTime(date, null);
        }
        return s;
    }
    
    public static String getTitle(final GlanceApp glanceApp, final Map<String, String> map) {
        String s;
        if (map == null) {
            s = "";
        }
        else {
            String s2 = null;
            switch (glanceApp) {
                default:
                    s2 = GlanceConstants.notification;
                    break;
                case GOOGLE_HANGOUT:
                    s2 = map.get(MessageConstants.MESSAGE_FROM.name());
                    break;
                case SLACK:
                case WHATS_APP:
                case FACEBOOK_MESSENGER:
                case GENERIC_MESSAGE:
                    s2 = map.get(MessageConstants.MESSAGE_FROM.name());
                    break;
                case FACEBOOK:
                    if (TextUtils.isEmpty((CharSequence)(s2 = map.get(SocialConstants.SOCIAL_FROM.name())))) {
                        s2 = GlanceConstants.facebook;
                        break;
                    }
                    break;
                case TWITTER:
                case GENERIC_SOCIAL:
                    s2 = map.get(SocialConstants.SOCIAL_FROM.name());
                    break;
                case IMESSAGE:
                    if ((s2 = map.get(MessageConstants.MESSAGE_FROM.name())) == null) {
                        s2 = map.get(MessageConstants.MESSAGE_FROM_NUMBER.name());
                        break;
                    }
                    break;
                case GOOGLE_CALENDAR:
                case APPLE_CALENDAR:
                case GENERIC_CALENDAR:
                    s2 = map.get(CalendarConstants.CALENDAR_TITLE.name());
                    break;
                case GOOGLE_MAIL:
                case APPLE_MAIL:
                case GENERIC_MAIL:
                case GOOGLE_INBOX:
                    if ((s2 = map.get(EmailConstants.EMAIL_FROM_NAME.name())) == null) {
                        s2 = map.get(EmailConstants.EMAIL_FROM_EMAIL.name());
                        break;
                    }
                    break;
                case SMS:
                    if ((s2 = map.get(MessageConstants.MESSAGE_FROM.name())) == null) {
                        s2 = map.get(MessageConstants.MESSAGE_FROM_NUMBER.name());
                        break;
                    }
                    break;
                case FUEL:
                    if (map.get(FuelConstants.FUEL_LEVEL.name()) != null) {
                        s2 = String.format("%s %s%%", GlanceConstants.fuelNotificationTitle, map.get(FuelConstants.FUEL_LEVEL.name()));
                        break;
                    }
                    s2 = GlanceHelper.resources.getString(R.string.gas_station);
                    break;
            }
            String s3;
            if ((s3 = s2) == null) {
                s3 = "";
            }
            s = s3;
        }
        return s;
    }
    
    public static String getTtsMessage(final GlanceApp p0, final Map<String, String> p1, final StringBuilder p2, final StringBuilder p3, final TimeHelper p4) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: dup            
        //     3: astore          5
        //     5: monitorenter   
        //     6: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper$1.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I
        //     9: aload_0        
        //    10: invokevirtual   com/navdy/hud/app/framework/glance/GlanceApp.ordinal:()I
        //    13: iaload         
        //    14: tableswitch {
        //                2: 576
        //                3: 576
        //                4: 576
        //                5: 576
        //                6: 576
        //                7: 607
        //                8: 607
        //                9: 607
        //               10: 576
        //               11: 123
        //               12: 123
        //               13: 123
        //               14: 488
        //               15: 488
        //               16: 488
        //               17: 488
        //               18: 576
        //               19: 638
        //               20: 717
        //          default: 104
        //        }
        //   104: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.builder:Ljava/lang/StringBuilder;
        //   107: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   110: astore_0       
        //   111: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.builder:Ljava/lang/StringBuilder;
        //   114: iconst_0       
        //   115: invokevirtual   java/lang/StringBuilder.setLength:(I)V
        //   118: aload           5
        //   120: monitorexit    
        //   121: aload_0        
        //   122: areturn        
        //   123: aload_1        
        //   124: getstatic       com/navdy/service/library/events/glances/CalendarConstants.CALENDAR_LOCATION:Lcom/navdy/service/library/events/glances/CalendarConstants;
        //   127: invokevirtual   com/navdy/service/library/events/glances/CalendarConstants.name:()Ljava/lang/String;
        //   130: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   135: checkcast       Ljava/lang/String;
        //   138: astore          6
        //   140: aload_1        
        //   141: getstatic       com/navdy/service/library/events/glances/CalendarConstants.CALENDAR_TITLE:Lcom/navdy/service/library/events/glances/CalendarConstants;
        //   144: invokevirtual   com/navdy/service/library/events/glances/CalendarConstants.name:()Ljava/lang/String;
        //   147: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   152: checkcast       Ljava/lang/String;
        //   155: astore          7
        //   157: lconst_0       
        //   158: lstore          8
        //   160: aload_1        
        //   161: getstatic       com/navdy/service/library/events/glances/CalendarConstants.CALENDAR_TIME:Lcom/navdy/service/library/events/glances/CalendarConstants;
        //   164: invokevirtual   com/navdy/service/library/events/glances/CalendarConstants.name:()Ljava/lang/String;
        //   167: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   172: checkcast       Ljava/lang/String;
        //   175: astore_0       
        //   176: lload           8
        //   178: lstore          10
        //   180: aload_0        
        //   181: ifnull          190
        //   184: aload_0        
        //   185: invokestatic    java/lang/Long.parseLong:(Ljava/lang/String;)J
        //   188: lstore          10
        //   190: aconst_null    
        //   191: astore_1       
        //   192: aload_1        
        //   193: astore_0       
        //   194: lload           10
        //   196: lconst_0       
        //   197: lcmp           
        //   198: ifle            271
        //   201: lload           10
        //   203: aload_2        
        //   204: aload_3        
        //   205: aload           4
        //   207: invokestatic    com/navdy/hud/app/framework/glance/GlanceHelper.getCalendarTime:(JLjava/lang/StringBuilder;Ljava/lang/StringBuilder;Lcom/navdy/hud/app/common/TimeHelper;)Ljava/lang/String;
        //   210: astore          4
        //   212: aload_2        
        //   213: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   216: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   219: ifeq            421
        //   222: aload_3        
        //   223: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   226: astore_0       
        //   227: aload_0        
        //   228: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   231: ifne            394
        //   234: aload_0        
        //   235: getstatic       com/navdy/hud/app/framework/glance/GlanceConstants.pmMarker:Ljava/lang/String;
        //   238: invokestatic    android/text/TextUtils.equals:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
        //   241: ifeq            387
        //   244: getstatic       com/navdy/hud/app/framework/glance/GlanceConstants.pm:Ljava/lang/String;
        //   247: astore_0       
        //   248: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.resources:Landroid/content/res/Resources;
        //   251: ldc_w           R.string.tts_calendar_time_at
        //   254: iconst_2       
        //   255: anewarray       Ljava/lang/Object;
        //   258: dup            
        //   259: iconst_0       
        //   260: aload           4
        //   262: aastore        
        //   263: dup            
        //   264: iconst_1       
        //   265: aload_0        
        //   266: aastore        
        //   267: invokevirtual   android/content/res/Resources.getString:(I[Ljava/lang/Object;)Ljava/lang/String;
        //   270: astore_0       
        //   271: aload           7
        //   273: ifnull          301
        //   276: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.builder:Ljava/lang/StringBuilder;
        //   279: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.resources:Landroid/content/res/Resources;
        //   282: ldc_w           R.string.tts_calendar_title
        //   285: iconst_1       
        //   286: anewarray       Ljava/lang/Object;
        //   289: dup            
        //   290: iconst_0       
        //   291: aload           7
        //   293: aastore        
        //   294: invokevirtual   android/content/res/Resources.getString:(I[Ljava/lang/Object;)Ljava/lang/String;
        //   297: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   300: pop            
        //   301: aload_0        
        //   302: ifnull          323
        //   305: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.builder:Ljava/lang/StringBuilder;
        //   308: ldc_w           " "
        //   311: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   314: pop            
        //   315: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.builder:Ljava/lang/StringBuilder;
        //   318: aload_0        
        //   319: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   322: pop            
        //   323: aload           6
        //   325: ifnull          104
        //   328: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.builder:Ljava/lang/StringBuilder;
        //   331: ldc_w           " "
        //   334: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   337: pop            
        //   338: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.builder:Ljava/lang/StringBuilder;
        //   341: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.resources:Landroid/content/res/Resources;
        //   344: ldc_w           R.string.tts_calendar_location
        //   347: iconst_1       
        //   348: anewarray       Ljava/lang/Object;
        //   351: dup            
        //   352: iconst_0       
        //   353: aload           6
        //   355: aastore        
        //   356: invokevirtual   android/content/res/Resources.getString:(I[Ljava/lang/Object;)Ljava/lang/String;
        //   359: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   362: pop            
        //   363: goto            104
        //   366: astore_0       
        //   367: aload           5
        //   369: monitorexit    
        //   370: aload_0        
        //   371: athrow         
        //   372: astore_0       
        //   373: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.sLogger:Lcom/navdy/service/library/log/Logger;
        //   376: aload_0        
        //   377: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   380: lload           8
        //   382: lstore          10
        //   384: goto            190
        //   387: getstatic       com/navdy/hud/app/framework/glance/GlanceConstants.am:Ljava/lang/String;
        //   390: astore_0       
        //   391: goto            248
        //   394: new             Ljava/lang/StringBuilder;
        //   397: astore_0       
        //   398: aload_0        
        //   399: invokespecial   java/lang/StringBuilder.<init>:()V
        //   402: aload_0        
        //   403: aload           4
        //   405: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   408: ldc_w           "."
        //   411: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   414: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   417: astore_0       
        //   418: goto            271
        //   421: aload           4
        //   423: invokestatic    java/lang/Long.parseLong:(Ljava/lang/String;)J
        //   426: lconst_1       
        //   427: lcmp           
        //   428: ifle            453
        //   431: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.resources:Landroid/content/res/Resources;
        //   434: ldc_w           R.string.tts_calendar_time_mins
        //   437: iconst_1       
        //   438: anewarray       Ljava/lang/Object;
        //   441: dup            
        //   442: iconst_0       
        //   443: aload           4
        //   445: aastore        
        //   446: invokevirtual   android/content/res/Resources.getString:(I[Ljava/lang/Object;)Ljava/lang/String;
        //   449: astore_0       
        //   450: goto            271
        //   453: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.resources:Landroid/content/res/Resources;
        //   456: ldc_w           R.string.tts_calendar_time_min
        //   459: iconst_1       
        //   460: anewarray       Ljava/lang/Object;
        //   463: dup            
        //   464: iconst_0       
        //   465: aload           4
        //   467: aastore        
        //   468: invokevirtual   android/content/res/Resources.getString:(I[Ljava/lang/Object;)Ljava/lang/String;
        //   471: astore_0       
        //   472: goto            271
        //   475: astore_0       
        //   476: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.sLogger:Lcom/navdy/service/library/log/Logger;
        //   479: aload_0        
        //   480: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   483: aload_1        
        //   484: astore_0       
        //   485: goto            271
        //   488: aload_1        
        //   489: getstatic       com/navdy/service/library/events/glances/EmailConstants.EMAIL_SUBJECT:Lcom/navdy/service/library/events/glances/EmailConstants;
        //   492: invokevirtual   com/navdy/service/library/events/glances/EmailConstants.name:()Ljava/lang/String;
        //   495: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   500: checkcast       Ljava/lang/String;
        //   503: astore_0       
        //   504: aload_0        
        //   505: ifnull          516
        //   508: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.builder:Ljava/lang/StringBuilder;
        //   511: aload_0        
        //   512: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   515: pop            
        //   516: aload_1        
        //   517: getstatic       com/navdy/service/library/events/glances/EmailConstants.EMAIL_BODY:Lcom/navdy/service/library/events/glances/EmailConstants;
        //   520: invokevirtual   com/navdy/service/library/events/glances/EmailConstants.name:()Ljava/lang/String;
        //   523: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   528: checkcast       Ljava/lang/String;
        //   531: astore_0       
        //   532: aload_0        
        //   533: ifnull          104
        //   536: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.builder:Ljava/lang/StringBuilder;
        //   539: invokevirtual   java/lang/StringBuilder.length:()I
        //   542: ifle            565
        //   545: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.builder:Ljava/lang/StringBuilder;
        //   548: ldc_w           "."
        //   551: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   554: pop            
        //   555: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.builder:Ljava/lang/StringBuilder;
        //   558: ldc_w           " "
        //   561: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   564: pop            
        //   565: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.builder:Ljava/lang/StringBuilder;
        //   568: aload_0        
        //   569: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   572: pop            
        //   573: goto            104
        //   576: aload_1        
        //   577: getstatic       com/navdy/service/library/events/glances/MessageConstants.MESSAGE_BODY:Lcom/navdy/service/library/events/glances/MessageConstants;
        //   580: invokevirtual   com/navdy/service/library/events/glances/MessageConstants.name:()Ljava/lang/String;
        //   583: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   588: checkcast       Ljava/lang/String;
        //   591: astore_0       
        //   592: aload_0        
        //   593: ifnull          104
        //   596: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.builder:Ljava/lang/StringBuilder;
        //   599: aload_0        
        //   600: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   603: pop            
        //   604: goto            104
        //   607: aload_1        
        //   608: getstatic       com/navdy/service/library/events/glances/SocialConstants.SOCIAL_MESSAGE:Lcom/navdy/service/library/events/glances/SocialConstants;
        //   611: invokevirtual   com/navdy/service/library/events/glances/SocialConstants.name:()Ljava/lang/String;
        //   614: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   619: checkcast       Ljava/lang/String;
        //   622: astore_0       
        //   623: aload_0        
        //   624: ifnull          104
        //   627: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.builder:Ljava/lang/StringBuilder;
        //   630: aload_0        
        //   631: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   634: pop            
        //   635: goto            104
        //   638: aload_1        
        //   639: getstatic       com/navdy/service/library/events/glances/FuelConstants.FUEL_LEVEL:Lcom/navdy/service/library/events/glances/FuelConstants;
        //   642: invokevirtual   com/navdy/service/library/events/glances/FuelConstants.name:()Ljava/lang/String;
        //   645: invokeinterface java/util/Map.containsKey:(Ljava/lang/Object;)Z
        //   650: ifeq            669
        //   653: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.builder:Ljava/lang/StringBuilder;
        //   656: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.resources:Landroid/content/res/Resources;
        //   659: ldc_w           R.string.your_fuel_level_is_low_tts
        //   662: invokevirtual   android/content/res/Resources.getString:(I)Ljava/lang/String;
        //   665: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   668: pop            
        //   669: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.builder:Ljava/lang/StringBuilder;
        //   672: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.resources:Landroid/content/res/Resources;
        //   675: ldc_w           R.string.i_can_add_trip_gas_station
        //   678: iconst_1       
        //   679: anewarray       Ljava/lang/Object;
        //   682: dup            
        //   683: iconst_0       
        //   684: aload_1        
        //   685: getstatic       com/navdy/service/library/events/glances/FuelConstants.GAS_STATION_NAME:Lcom/navdy/service/library/events/glances/FuelConstants;
        //   688: invokevirtual   com/navdy/service/library/events/glances/FuelConstants.name:()Ljava/lang/String;
        //   691: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   696: aastore        
        //   697: invokevirtual   android/content/res/Resources.getString:(I[Ljava/lang/Object;)Ljava/lang/String;
        //   700: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   703: pop            
        //   704: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.builder:Ljava/lang/StringBuilder;
        //   707: ldc_w           "."
        //   710: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   713: pop            
        //   714: goto            104
        //   717: aload_1        
        //   718: getstatic       com/navdy/service/library/events/glances/GenericConstants.GENERIC_MESSAGE:Lcom/navdy/service/library/events/glances/GenericConstants;
        //   721: invokevirtual   com/navdy/service/library/events/glances/GenericConstants.name:()Ljava/lang/String;
        //   724: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   729: checkcast       Ljava/lang/String;
        //   732: astore_0       
        //   733: aload_0        
        //   734: ifnull          104
        //   737: getstatic       com/navdy/hud/app/framework/glance/GlanceHelper.builder:Ljava/lang/StringBuilder;
        //   740: aload_0        
        //   741: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   744: pop            
        //   745: goto            104
        //    Signature:
        //  (Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Lcom/navdy/hud/app/common/TimeHelper;)Ljava/lang/String;
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  6      104    366    372    Any
        //  104    118    366    372    Any
        //  123    157    366    372    Any
        //  160    176    366    372    Any
        //  184    190    372    387    Ljava/lang/Throwable;
        //  184    190    366    372    Any
        //  201    248    366    372    Any
        //  248    271    366    372    Any
        //  276    301    366    372    Any
        //  305    323    366    372    Any
        //  328    363    366    372    Any
        //  373    380    366    372    Any
        //  387    391    366    372    Any
        //  394    418    366    372    Any
        //  421    450    475    488    Ljava/lang/Throwable;
        //  421    450    366    372    Any
        //  453    472    475    488    Ljava/lang/Throwable;
        //  453    472    366    372    Any
        //  476    483    366    372    Any
        //  488    504    366    372    Any
        //  508    516    366    372    Any
        //  516    532    366    372    Any
        //  536    565    366    372    Any
        //  565    573    366    372    Any
        //  576    592    366    372    Any
        //  596    604    366    372    Any
        //  607    623    366    372    Any
        //  627    635    366    372    Any
        //  638    669    366    372    Any
        //  669    714    366    372    Any
        //  717    733    366    372    Any
        //  737    745    366    372    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0190:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void initMessageAttributes() {
        final Context appContext = HudApplication.getAppContext();
        final TextView textView = new TextView(appContext);
        textView.setTextAppearance(appContext, R.style.glance_message_title);
        final StaticLayout staticLayout = new StaticLayout((CharSequence)"RockgOn", textView.getPaint(), GlanceConstants.messageWidthBound, Layout$Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        final int messageTitleHeight = staticLayout.getHeight() + (GlanceConstants.messageTitleMarginTop + GlanceConstants.messageTitleMarginBottom);
        GlanceHelper.sLogger.v("message-title-ht = " + messageTitleHeight + " layout=" + staticLayout.getHeight());
        GlanceConstants.setMessageTitleHeight(messageTitleHeight);
    }
    
    public static boolean isCalendarApp(final GlanceApp glanceApp) {
        boolean b = false;
        switch (glanceApp) {
            default:
                b = false;
                break;
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                b = true;
                break;
        }
        return b;
    }
    
    public static boolean isDeleteAllView(final View view) {
        return view != null && view.getTag() == GlanceConstants.DELETE_ALL_VIEW_TAG;
    }
    
    public static boolean isFuelNotificationEnabled() {
        return isPackageEnabled("com.navdy.fuel");
    }
    
    public static boolean isMusicNotificationEnabled() {
        return isPackageEnabled("com.navdy.music");
    }
    
    public static boolean isPackageEnabled(final String s) {
        return DriverProfileHelper.getInstance().getCurrentProfile().getNotificationPreferences().enabled && Boolean.TRUE.equals(DriverProfileHelper.getInstance().getCurrentProfile().getNotificationSettings().enabled(s));
    }
    
    public static boolean isPhoneNotificationEnabled() {
        return isPackageEnabled("com.navdy.phone") || isPackageEnabled("com.apple.mobilephone");
    }
    
    public static boolean isPhotoCheckRequired(final GlanceApp glanceApp) {
        boolean b = false;
        switch (glanceApp) {
            default:
                b = false;
                break;
            case GOOGLE_HANGOUT:
            case IMESSAGE:
            case SMS:
                b = true;
                break;
        }
        return b;
    }
    
    public static boolean isTrafficNotificationEnabled() {
        return isPackageEnabled("com.navdy.traffic");
    }
    
    public static boolean needsScrollLayout(final String s) {
        boolean b = false;
        if (!TextUtils.isEmpty((CharSequence)s)) {
            final Context appContext = HudApplication.getAppContext();
            final TextView textView = new TextView(appContext);
            textView.setTextAppearance(appContext, R.style.glance_message_body);
            final float n = GlanceConstants.messageHeightBound / 2 - new StaticLayout((CharSequence)s, textView.getPaint(), GlanceConstants.messageWidthBound, Layout$Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false).getHeight() / 2;
            final int messageTitleHeight = GlanceConstants.getMessageTitleHeight();
            b = (n <= messageTitleHeight);
            GlanceHelper.sLogger.v("message-tittle-calc left[" + n + "] titleHt [" + messageTitleHeight + "]");
        }
        return b;
    }
    
    public static void showGlancesDeletedToast() {
        final Bundle bundle = new Bundle();
        bundle.putInt("13", 1000);
        bundle.putInt("8", R.drawable.icon_qm_glances_grey);
        bundle.putString("4", GlanceHelper.resources.getString(R.string.glances_deleted));
        bundle.putInt("5", R.style.Glances_1);
        final ToastManager instance = ToastManager.getInstance();
        instance.dismissCurrentToast();
        instance.clearAllPendingToast();
        instance.addToast(new ToastManager.ToastParams("glance-deleted", bundle, null, true, true));
    }
    
    public static boolean usesMessageLayout(final GlanceApp glanceApp) {
        boolean b = false;
        switch (glanceApp) {
            default:
                b = false;
                break;
            case GOOGLE_HANGOUT:
            case SLACK:
            case WHATS_APP:
            case FACEBOOK_MESSENGER:
            case GENERIC_MESSAGE:
            case FACEBOOK:
            case TWITTER:
            case GENERIC_SOCIAL:
            case IMESSAGE:
            case GOOGLE_MAIL:
            case APPLE_MAIL:
            case GENERIC_MAIL:
            case GOOGLE_INBOX:
            case SMS:
            case GENERIC:
                b = true;
                break;
        }
        return b;
    }
}
