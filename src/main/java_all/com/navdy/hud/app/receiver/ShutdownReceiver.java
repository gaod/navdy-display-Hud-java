package com.navdy.hud.app.receiver;

import com.navdy.hud.app.HudApplication;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;

public class ShutdownReceiver extends BroadcastReceiver
{
    public void onReceive(final Context context, final Intent intent) {
        HudApplication.getApplication().shutdown();
    }
}
