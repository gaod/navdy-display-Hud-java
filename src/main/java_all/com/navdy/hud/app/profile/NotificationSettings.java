package com.navdy.hud.app.profile;

import com.navdy.service.library.events.notification.NotificationSetting;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import com.navdy.ancs.AppleNotification;
import java.util.HashMap;
import com.navdy.service.library.events.preferences.NotificationPreferences;
import java.util.Map;
import com.navdy.service.library.log.Logger;

public class NotificationSettings
{
    public static final String APP_ID_CALENDAR = "com.apple.mobilecal";
    public static final String APP_ID_MAIL = "com.apple.mobilemail";
    public static final String APP_ID_PHONE = "com.apple.mobilephone";
    public static final String APP_ID_REMINDERS = "com.apple.reminders";
    public static final String APP_ID_SMS = "com.apple.MobileSMS";
    private static Logger sLogger;
    private Map<String, Boolean> enabledApps;
    private final Object lock;
    
    static {
        NotificationSettings.sLogger = new Logger(NotificationSettings.class);
    }
    
    public NotificationSettings(final NotificationPreferences notificationPreferences) {
        this.enabledApps = new HashMap<String, Boolean>();
        this.lock = new Object();
        this.update(notificationPreferences);
    }
    
    public Boolean enabled(final AppleNotification appleNotification) {
        return this.enabled(appleNotification.getAppId());
    }
    
    public Boolean enabled(final String s) {
        final Object lock = this.lock;
        synchronized (lock) {
            return this.enabledApps.get(s);
        }
    }
    
    public List<String> enabledApps() {
        final ArrayList<Object> list = new ArrayList<Object>();
        final Object lock = this.lock;
        synchronized (lock) {
            for (final Map.Entry<String, Boolean> entry : this.enabledApps.entrySet()) {
                if (Boolean.TRUE.equals(entry.getValue())) {
                    list.add(entry.getKey());
                }
            }
        }
        // monitorexit(o)
        return (List<String>)list;
    }
    
    public void update(final NotificationPreferences notificationPreferences) {
        final Object lock = this.lock;
        synchronized (lock) {
            this.enabledApps.clear();
            if (notificationPreferences != null && notificationPreferences.settings != null) {
                for (final NotificationSetting notificationSetting : notificationPreferences.settings) {
                    this.enabledApps.put(notificationSetting.app, notificationSetting.enabled);
                }
            }
        }
    }
    // monitorexit(o)
}
