package com.navdy.hud.app.profile;

import com.navdy.hud.app.framework.DriverProfileHelper;
import com.squareup.wire.Wire;
import com.navdy.hud.app.ui.component.UISettings;
import android.text.TextUtils;
import com.navdy.service.library.util.IOUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.events.NavdyEventUtil;
import com.squareup.wire.Message;
import java.io.IOException;
import com.navdy.service.library.events.preferences.DisplaySpeakerPreferences;
import com.navdy.service.library.events.preferences.NotificationPreferences;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.service.library.events.MessageStore;
import java.util.Locale;
import com.navdy.service.library.events.preferences.LocalPreferences;
import com.navdy.service.library.events.preferences.InputPreferences;
import com.navdy.service.library.events.preferences.DriverProfilePreferences;
import android.graphics.Bitmap;
import java.io.File;
import com.navdy.service.library.events.glances.CannedMessagesUpdate;
import com.navdy.service.library.log.Logger;

public class DriverProfile
{
    private static final String CANNED_MESSAGES = "CannedMessages";
    private static final String CONTACTS_IMAGE_CACHE_DIR = "contacts";
    public static final String DRIVER_PROFILE_IMAGE = "DriverImage";
    private static final String DRIVER_PROFILE_PREFERENCES = "DriverProfilePreferences";
    private static final String INPUT_PREFERENCES = "InputPreferences";
    private static final String LOCALE_SEPARATOR = "_";
    private static final String LOCAL_PREFERENCES = "LocalPreferences";
    private static final String MUSIC_IMAGE_CACHE_DIR = "music";
    private static final String NAVIGATION_PREFERENCES = "NavigationPreferences";
    private static final String NOTIFICATION_PREFERENCES = "NotificationPreferences";
    private static final String PLACES_IMAGE_CACHE_DIR = "places";
    private static final String PREFERENCES_DIRECTORY = "Preferences";
    private static final String SPEAKER_PREFERENCES = "SpeakerPreferences";
    private static final Logger sLogger;
    CannedMessagesUpdate mCannedMessages;
    private File mContactsImageDirectory;
    private Bitmap mDriverImage;
    DriverProfilePreferences mDriverProfilePreferences;
    InputPreferences mInputPreferences;
    private LocalPreferences mLocalPreferences;
    private Locale mLocale;
    private MessageStore mMessageStore;
    private File mMusicImageDirectory;
    NavigationPreferences mNavigationPreferences;
    NotificationPreferences mNotificationPreferences;
    private NotificationSettings mNotificationSettings;
    private File mPlacesImageDirectory;
    private File mPreferencesDirectory;
    private File mProfileDirectory;
    private String mProfileName;
    private DisplaySpeakerPreferences mSpeakerPreferences;
    private boolean mTrafficEnabled;
    
    static {
        sLogger = new Logger(DriverProfile.class);
    }
    
    protected DriverProfile(final File p0) throws IOException {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokespecial   java/lang/Object.<init>:()V
        //     4: aload_0        
        //     5: aload_1        
        //     6: invokevirtual   java/io/File.getName:()Ljava/lang/String;
        //     9: putfield        com/navdy/hud/app/profile/DriverProfile.mProfileName:Ljava/lang/String;
        //    12: aload_0        
        //    13: aload_1        
        //    14: putfield        com/navdy/hud/app/profile/DriverProfile.mProfileDirectory:Ljava/io/File;
        //    17: aload_0        
        //    18: new             Ljava/io/File;
        //    21: dup            
        //    22: aload_1        
        //    23: ldc             "Preferences"
        //    25: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //    28: putfield        com/navdy/hud/app/profile/DriverProfile.mPreferencesDirectory:Ljava/io/File;
        //    31: aload_0        
        //    32: new             Lcom/navdy/service/library/events/MessageStore;
        //    35: dup            
        //    36: aload_0        
        //    37: getfield        com/navdy/hud/app/profile/DriverProfile.mPreferencesDirectory:Ljava/io/File;
        //    40: invokespecial   com/navdy/service/library/events/MessageStore.<init>:(Ljava/io/File;)V
        //    43: putfield        com/navdy/hud/app/profile/DriverProfile.mMessageStore:Lcom/navdy/service/library/events/MessageStore;
        //    46: aload_0        
        //    47: new             Ljava/io/File;
        //    50: dup            
        //    51: aload_1        
        //    52: ldc             "places"
        //    54: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //    57: putfield        com/navdy/hud/app/profile/DriverProfile.mPlacesImageDirectory:Ljava/io/File;
        //    60: aload_0        
        //    61: getfield        com/navdy/hud/app/profile/DriverProfile.mPlacesImageDirectory:Ljava/io/File;
        //    64: invokestatic    com/navdy/service/library/util/IOUtils.createDirectory:(Ljava/io/File;)Z
        //    67: pop            
        //    68: aload_0        
        //    69: new             Ljava/io/File;
        //    72: dup            
        //    73: aload_1        
        //    74: ldc             "contacts"
        //    76: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //    79: putfield        com/navdy/hud/app/profile/DriverProfile.mContactsImageDirectory:Ljava/io/File;
        //    82: aload_0        
        //    83: getfield        com/navdy/hud/app/profile/DriverProfile.mContactsImageDirectory:Ljava/io/File;
        //    86: invokestatic    com/navdy/service/library/util/IOUtils.createDirectory:(Ljava/io/File;)Z
        //    89: pop            
        //    90: aload_0        
        //    91: new             Ljava/io/File;
        //    94: dup            
        //    95: aload_1        
        //    96: ldc             "music"
        //    98: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //   101: putfield        com/navdy/hud/app/profile/DriverProfile.mMusicImageDirectory:Ljava/io/File;
        //   104: aload_0        
        //   105: getfield        com/navdy/hud/app/profile/DriverProfile.mMusicImageDirectory:Ljava/io/File;
        //   108: invokestatic    com/navdy/service/library/util/IOUtils.createDirectory:(Ljava/io/File;)Z
        //   111: pop            
        //   112: aload_0        
        //   113: aload_0        
        //   114: ldc             "DriverProfilePreferences"
        //   116: ldc             Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;.class
        //   118: new             Lcom/navdy/hud/app/profile/DriverProfile$1;
        //   121: dup            
        //   122: aload_0        
        //   123: invokespecial   com/navdy/hud/app/profile/DriverProfile$1.<init>:(Lcom/navdy/hud/app/profile/DriverProfile;)V
        //   126: invokespecial   com/navdy/hud/app/profile/DriverProfile.readPreference:(Ljava/lang/String;Ljava/lang/Class;Lcom/navdy/service/library/events/NavdyEventUtil$Initializer;)Lcom/squareup/wire/Message;
        //   129: checkcast       Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;
        //   132: putfield        com/navdy/hud/app/profile/DriverProfile.mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;
        //   135: aload_0        
        //   136: aload_0        
        //   137: ldc             "NavigationPreferences"
        //   139: ldc             Lcom/navdy/service/library/events/preferences/NavigationPreferences;.class
        //   141: invokespecial   com/navdy/hud/app/profile/DriverProfile.readPreference:(Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/wire/Message;
        //   144: checkcast       Lcom/navdy/service/library/events/preferences/NavigationPreferences;
        //   147: putfield        com/navdy/hud/app/profile/DriverProfile.mNavigationPreferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;
        //   150: aload_0        
        //   151: invokespecial   com/navdy/hud/app/profile/DriverProfile.setTraffic:()V
        //   154: aload_0        
        //   155: aload_0        
        //   156: ldc             "InputPreferences"
        //   158: ldc             Lcom/navdy/service/library/events/preferences/InputPreferences;.class
        //   160: invokespecial   com/navdy/hud/app/profile/DriverProfile.readPreference:(Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/wire/Message;
        //   163: checkcast       Lcom/navdy/service/library/events/preferences/InputPreferences;
        //   166: putfield        com/navdy/hud/app/profile/DriverProfile.mInputPreferences:Lcom/navdy/service/library/events/preferences/InputPreferences;
        //   169: aload_0        
        //   170: aload_0        
        //   171: ldc             "SpeakerPreferences"
        //   173: ldc             Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;.class
        //   175: invokespecial   com/navdy/hud/app/profile/DriverProfile.readPreference:(Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/wire/Message;
        //   178: checkcast       Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;
        //   181: putfield        com/navdy/hud/app/profile/DriverProfile.mSpeakerPreferences:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;
        //   184: aload_0        
        //   185: aload_0        
        //   186: ldc             "NotificationPreferences"
        //   188: ldc             Lcom/navdy/service/library/events/preferences/NotificationPreferences;.class
        //   190: invokespecial   com/navdy/hud/app/profile/DriverProfile.readPreference:(Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/wire/Message;
        //   193: checkcast       Lcom/navdy/service/library/events/preferences/NotificationPreferences;
        //   196: putfield        com/navdy/hud/app/profile/DriverProfile.mNotificationPreferences:Lcom/navdy/service/library/events/preferences/NotificationPreferences;
        //   199: aload_0        
        //   200: new             Lcom/navdy/hud/app/profile/NotificationSettings;
        //   203: dup            
        //   204: aload_0        
        //   205: getfield        com/navdy/hud/app/profile/DriverProfile.mNotificationPreferences:Lcom/navdy/service/library/events/preferences/NotificationPreferences;
        //   208: invokespecial   com/navdy/hud/app/profile/NotificationSettings.<init>:(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)V
        //   211: putfield        com/navdy/hud/app/profile/DriverProfile.mNotificationSettings:Lcom/navdy/hud/app/profile/NotificationSettings;
        //   214: aload_0        
        //   215: aload_0        
        //   216: ldc             "LocalPreferences"
        //   218: ldc             Lcom/navdy/service/library/events/preferences/LocalPreferences;.class
        //   220: invokespecial   com/navdy/hud/app/profile/DriverProfile.readPreference:(Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/wire/Message;
        //   223: checkcast       Lcom/navdy/service/library/events/preferences/LocalPreferences;
        //   226: putfield        com/navdy/hud/app/profile/DriverProfile.mLocalPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;
        //   229: aload_0        
        //   230: aload_0        
        //   231: ldc             "CannedMessages"
        //   233: ldc             Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;.class
        //   235: invokespecial   com/navdy/hud/app/profile/DriverProfile.readPreference:(Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/wire/Message;
        //   238: checkcast       Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;
        //   241: putfield        com/navdy/hud/app/profile/DriverProfile.mCannedMessages:Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;
        //   244: aconst_null    
        //   245: astore_2       
        //   246: aconst_null    
        //   247: astore_3       
        //   248: aload_2        
        //   249: astore_1       
        //   250: new             Ljava/io/File;
        //   253: astore          4
        //   255: aload_2        
        //   256: astore_1       
        //   257: aload           4
        //   259: aload_0        
        //   260: getfield        com/navdy/hud/app/profile/DriverProfile.mPreferencesDirectory:Ljava/io/File;
        //   263: ldc             "DriverImage"
        //   265: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //   268: aload_2        
        //   269: astore_1       
        //   270: new             Ljava/io/FileInputStream;
        //   273: astore          5
        //   275: aload_2        
        //   276: astore_1       
        //   277: aload           5
        //   279: aload           4
        //   281: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //   284: aload_0        
        //   285: aload           5
        //   287: invokestatic    android/graphics/BitmapFactory.decodeStream:(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
        //   290: putfield        com/navdy/hud/app/profile/DriverProfile.mDriverImage:Landroid/graphics/Bitmap;
        //   293: aload           5
        //   295: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   298: aload_0        
        //   299: invokespecial   com/navdy/hud/app/profile/DriverProfile.setLocale:()V
        //   302: return         
        //   303: astore_1       
        //   304: aload_3        
        //   305: astore          5
        //   307: aload           5
        //   309: astore_1       
        //   310: aload_0        
        //   311: aconst_null    
        //   312: putfield        com/navdy/hud/app/profile/DriverProfile.mDriverImage:Landroid/graphics/Bitmap;
        //   315: aload           5
        //   317: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   320: goto            298
        //   323: astore_3       
        //   324: aload_1        
        //   325: astore          5
        //   327: aload           5
        //   329: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   332: aload_3        
        //   333: athrow         
        //   334: astore_1       
        //   335: aload_1        
        //   336: astore_3       
        //   337: goto            327
        //   340: astore_1       
        //   341: goto            307
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                           
        //  -----  -----  -----  -----  -------------------------------
        //  250    255    303    307    Ljava/io/FileNotFoundException;
        //  250    255    323    327    Any
        //  257    268    303    307    Ljava/io/FileNotFoundException;
        //  257    268    323    327    Any
        //  270    275    303    307    Ljava/io/FileNotFoundException;
        //  270    275    323    327    Any
        //  277    284    303    307    Ljava/io/FileNotFoundException;
        //  277    284    323    327    Any
        //  284    293    340    344    Ljava/io/FileNotFoundException;
        //  284    293    334    340    Any
        //  310    315    323    327    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0298:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:692)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:529)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    static DriverProfile createProfileForId(final String s, String string) throws IOException {
        if (s.contains(File.separator) || s.startsWith(".")) {
            throw new IllegalArgumentException("Profile names can't refer to directories");
        }
        string = string + File.separator + s;
        final File file = new File(string);
        if (!file.mkdir() && !file.isDirectory()) {
            throw new IOException("could not create profile");
        }
        final File file2 = new File(string + File.separator + "Preferences");
        if (!file2.mkdir() && !file2.isDirectory()) {
            throw new IOException("could not create preferences directory");
        }
        return new DriverProfile(file);
    }
    
    private <T extends Message> T readPreference(final String s, final Class<T> clazz) throws IOException {
        return this.mMessageStore.<T>readMessage(s, clazz);
    }
    
    private <T extends Message> T readPreference(final String s, final Class<T> clazz, final NavdyEventUtil.Initializer<T> initializer) throws IOException {
        return this.mMessageStore.<T>readMessage(s, clazz, initializer);
    }
    
    private void removeDriverImage() {
        this.mDriverImage = null;
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                IOUtils.deleteFile(HudApplication.getAppContext(), new File(DriverProfile.this.mPreferencesDirectory, "DriverImage").getAbsolutePath());
            }
        }, 1);
    }
    
    private <T extends Message> T removeNulls(final T t) {
        return NavdyEventUtil.<T>applyDefaults(t);
    }
    
    private void setLocale() {
        while (true) {
            try {
                this.mLocale = null;
                final String locale = this.mDriverProfilePreferences.locale;
                if (!TextUtils.isEmpty((CharSequence)locale)) {
                    final Locale localeForID = HudLocale.getLocaleForID(locale);
                    DriverProfile.sLogger.v("locale string:" + locale + " locale:" + localeForID + " profile:" + this.getProfileName() + " email:" + this.getDriverEmail());
                    try {
                        if (TextUtils.isEmpty((CharSequence)localeForID.getISO3Language())) {
                            DriverProfile.sLogger.v("locale no language");
                        }
                        else {
                            this.mLocale = localeForID;
                        }
                    }
                    catch (Throwable t) {
                        DriverProfile.sLogger.v("locale not valid", t);
                    }
                    return;
                }
            }
            catch (Throwable t2) {
                DriverProfile.sLogger.e("setLocale", t2);
                return;
            }
            DriverProfile.sLogger.v("no locale string");
        }
    }
    
    private void setTraffic() {
        this.mTrafficEnabled = true;
    }
    
    private void writeMessageToFile(final Message message, final String s) {
        this.mMessageStore.writeMessage(message, s);
    }
    
    void copy(final DriverProfile driverProfile) {
        this.writeMessageToFile(this.mDriverProfilePreferences = new DriverProfilePreferences.Builder(driverProfile.mDriverProfilePreferences).build(), "DriverProfilePreferences");
        this.writeMessageToFile(this.mNavigationPreferences = new NavigationPreferences.Builder(driverProfile.mNavigationPreferences).build(), "NavigationPreferences");
        this.writeMessageToFile(this.mInputPreferences = new InputPreferences.Builder(driverProfile.mInputPreferences).build(), "InputPreferences");
        this.writeMessageToFile(this.mSpeakerPreferences = new DisplaySpeakerPreferences.Builder(driverProfile.mSpeakerPreferences).build(), "SpeakerPreferences");
        this.writeMessageToFile(this.mNotificationPreferences = new NotificationPreferences.Builder(driverProfile.mNotificationPreferences).build(), "NotificationPreferences");
        this.writeMessageToFile(this.mLocalPreferences = new LocalPreferences.Builder(driverProfile.mLocalPreferences).build(), "LocalPreferences");
        DriverProfile.sLogger.v("copy done:" + this.mPreferencesDirectory);
    }
    
    public CannedMessagesUpdate getCannedMessages() {
        return this.mCannedMessages;
    }
    
    public String getCarMake() {
        return this.mDriverProfilePreferences.car_make;
    }
    
    public String getCarModel() {
        return this.mDriverProfilePreferences.car_model;
    }
    
    public String getCarYear() {
        return this.mDriverProfilePreferences.car_year;
    }
    
    public File getContactsImageDir() {
        return this.mContactsImageDirectory;
    }
    
    public String getDeviceName() {
        return this.mDriverProfilePreferences.device_name;
    }
    
    public DriverProfilePreferences.DisplayFormat getDisplayFormat() {
        return this.mDriverProfilePreferences.display_format;
    }
    
    public String getDriverEmail() {
        return this.mDriverProfilePreferences.driver_email;
    }
    
    public Bitmap getDriverImage() {
        return this.mDriverImage;
    }
    
    public File getDriverImageFile() {
        return new File(this.mPreferencesDirectory, "DriverImage");
    }
    
    public String getDriverName() {
        return this.mDriverProfilePreferences.driver_name;
    }
    
    public DriverProfilePreferences.FeatureMode getFeatureMode() {
        return this.mDriverProfilePreferences.feature_mode;
    }
    
    public String getFirstName() {
        String s = this.mDriverProfilePreferences.driver_name;
        if (!TextUtils.isEmpty((CharSequence)s)) {
            s = s.trim();
            final int index = s.indexOf(" ");
            if (index != -1) {
                s = s.substring(0, index);
            }
        }
        return s;
    }
    
    public InputPreferences getInputPreferences() {
        return this.mInputPreferences;
    }
    
    public LocalPreferences getLocalPreferences() {
        return this.mLocalPreferences;
    }
    
    public Locale getLocale() {
        Locale locale;
        if (this.mLocale != null) {
            locale = this.mLocale;
        }
        else {
            locale = Locale.getDefault();
        }
        return locale;
    }
    
    public DriverProfilePreferences.DialLongPressAction getLongPressAction() {
        DriverProfilePreferences.DialLongPressAction dialLongPressAction;
        if (UISettings.isLongPressActionPlaceSearch()) {
            dialLongPressAction = DriverProfilePreferences.DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH;
        }
        else {
            dialLongPressAction = this.mDriverProfilePreferences.dial_long_press_action;
        }
        return dialLongPressAction;
    }
    
    public File getMusicImageDir() {
        return this.mMusicImageDirectory;
    }
    
    public NavigationPreferences getNavigationPreferences() {
        return this.mNavigationPreferences;
    }
    
    public NotificationPreferences getNotificationPreferences() {
        return this.mNotificationPreferences;
    }
    
    public NotificationSettings getNotificationSettings() {
        return this.mNotificationSettings;
    }
    
    public long getObdBlacklistModificationTime() {
        long longValue;
        if (this.mDriverProfilePreferences.obdBlacklistLastModified != null) {
            longValue = this.mDriverProfilePreferences.obdBlacklistLastModified;
        }
        else {
            longValue = 0L;
        }
        return longValue;
    }
    
    public DriverProfilePreferences.ObdScanSetting getObdScanSetting() {
        return this.mDriverProfilePreferences.obdScanSetting;
    }
    
    public File getPlacesImageDir() {
        return this.mPlacesImageDirectory;
    }
    
    public File getPreferencesDirectory() {
        return this.mPreferencesDirectory;
    }
    
    public String getProfileName() {
        return this.mProfileName;
    }
    
    public DisplaySpeakerPreferences getSpeakerPreferences() {
        return this.mSpeakerPreferences;
    }
    
    public DriverProfilePreferences.UnitSystem getUnitSystem() {
        return this.mDriverProfilePreferences.unit_system;
    }
    
    public boolean isAutoOnEnabled() {
        return Wire.<Boolean>get(this.mDriverProfilePreferences.auto_on_enabled, DriverProfilePreferences.DEFAULT_AUTO_ON_ENABLED);
    }
    
    public boolean isDefaultProfile() {
        return DriverProfileHelper.getInstance().getDriverProfileManager().isDefaultProfile(this);
    }
    
    public boolean isLimitBandwidthModeOn() {
        return Boolean.TRUE.equals(this.mDriverProfilePreferences.limit_bandwidth);
    }
    
    public boolean isProfilePublic() {
        return Boolean.TRUE.equals(this.mDriverProfilePreferences.profile_is_public);
    }
    
    public boolean isTrafficEnabled() {
        return this.mTrafficEnabled;
    }
    
    void setCannedMessages(final CannedMessagesUpdate cannedMessagesUpdate) {
        this.mCannedMessages = this.<CannedMessagesUpdate>removeNulls(cannedMessagesUpdate);
        DriverProfile.sLogger.i("[" + this.mProfileName + "] updating canned messages preferences to ver[" + cannedMessagesUpdate.serial_number + "]");
        this.writeMessageToFile(this.mCannedMessages, "CannedMessages");
    }
    
    void setDriverProfilePreferences(final DriverProfilePreferences driverProfilePreferences) {
        this.mDriverProfilePreferences = this.<DriverProfilePreferences>removeNulls(driverProfilePreferences);
        this.setLocale();
        DriverProfile.sLogger.i("[" + this.mProfileName + "] updating driver profile preferences to ver[" + driverProfilePreferences.serial_number + "] " + this.mDriverProfilePreferences);
        this.writeMessageToFile(this.mDriverProfilePreferences, "DriverProfilePreferences");
    }
    
    void setInputPreferences(final InputPreferences inputPreferences) {
        this.mInputPreferences = this.<InputPreferences>removeNulls(inputPreferences);
        DriverProfile.sLogger.i("[" + this.mProfileName + "] updating input preferences to ver[" + inputPreferences.serial_number + "] " + this.mInputPreferences);
        this.writeMessageToFile(this.mInputPreferences, "InputPreferences");
    }
    
    public void setLocalPreferences(final LocalPreferences localPreferences) {
        this.mLocalPreferences = this.<LocalPreferences>removeNulls(localPreferences);
        DriverProfile.sLogger.i("[" + this.mProfileName + "] updating local preferences to " + this.mLocalPreferences);
        this.writeMessageToFile(this.mLocalPreferences, "LocalPreferences");
    }
    
    void setNavigationPreferences(final NavigationPreferences navigationPreferences) {
        this.mNavigationPreferences = this.<NavigationPreferences>removeNulls(navigationPreferences);
        DriverProfile.sLogger.i("[" + this.mProfileName + "] updating nav preferences to ver[" + navigationPreferences.serial_number + "] " + this.mNavigationPreferences);
        this.writeMessageToFile(this.mNavigationPreferences, "NavigationPreferences");
        this.setTraffic();
    }
    
    void setNotificationPreferences(final NotificationPreferences notificationPreferences) {
        this.mNotificationPreferences = this.<NotificationPreferences>removeNulls(notificationPreferences);
        DriverProfile.sLogger.i("[" + this.mProfileName + "] updating notification preferences to ver[" + notificationPreferences.serial_number + "] " + this.mNotificationPreferences);
        this.writeMessageToFile(this.mNotificationPreferences, "NotificationPreferences");
        this.mNotificationSettings.update(this.mNotificationPreferences);
    }
    
    void setSpeakerPreferences(final DisplaySpeakerPreferences displaySpeakerPreferences) {
        this.mSpeakerPreferences = this.<DisplaySpeakerPreferences>removeNulls(displaySpeakerPreferences);
        DriverProfile.sLogger.i("[" + this.mProfileName + "] updating speaker preferences to ver[" + displaySpeakerPreferences.serial_number + "] " + this.mSpeakerPreferences);
        this.writeMessageToFile(this.mSpeakerPreferences, "SpeakerPreferences");
    }
    
    public void setTrafficEnabled(final boolean mTrafficEnabled) {
        this.mTrafficEnabled = mTrafficEnabled;
    }
    
    @Override
    public String toString() {
        return "DriverProfile{mProfileName='" + this.mProfileName + '\'' + '}';
    }
}
