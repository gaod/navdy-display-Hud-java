package com.navdy.hud.app.view;

public abstract class GaugeViewPresenter extends DashboardWidgetPresenter
{
    protected GaugeView mGaugeView;
    
    @Override
    public void setView(final DashboardWidgetView view) {
        super.setView(view);
        if (view == null) {
            this.mGaugeView = null;
        }
        else {
            if (!(view instanceof GaugeView)) {
                throw new IllegalArgumentException("The view has to be of type GaugeView");
            }
            this.mGaugeView = (GaugeView)view;
        }
    }
}
