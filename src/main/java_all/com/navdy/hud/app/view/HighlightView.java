package com.navdy.hud.app.view;

import android.graphics.Paint;
import android.graphics.Canvas;
import android.content.res.TypedArray;
import com.navdy.hud.app.R;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Paint;
import android.view.View;

public class HighlightView extends View
{
    private Paint paint;
    private int radius;
    private float strokeWidth;
    
    public HighlightView(final Context context) {
        this(context, null);
    }
    
    public HighlightView(final Context context, final AttributeSet set) {
        super(context, set);
        this.initFromAttributes(context, set);
        (this.paint = new Paint()).setStrokeWidth(this.strokeWidth);
        this.paint.setAntiAlias(true);
        this.paint.setColor(this.getResources().getColor(R.color.hud_cyan));
    }
    
    private void initFromAttributes(final Context context, final AttributeSet set) {
        final TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(set, R.styleable.Indicator, 0, 0);
        this.strokeWidth = obtainStyledAttributes.getDimension(3, (float)(int)this.getResources().getDimension(R.dimen.default_highlight_stroke_width));
        obtainStyledAttributes.recycle();
    }
    
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle((float)(this.getWidth() / 2), (float)(this.getHeight() / 2), (float)this.radius, this.paint);
    }
    
    public void setPaintStyle(final Paint$Style style) {
        if (style == Paint$Style.FILL_AND_STROKE) {
            throw new IllegalArgumentException();
        }
        this.paint.setStyle(style);
    }
    
    public void setRadius(final int radius) {
        this.radius = radius;
    }
    
    public void setStrokeWidth(final float strokeWidth) {
        this.strokeWidth = strokeWidth;
    }
}
