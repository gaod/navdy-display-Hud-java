package com.navdy.hud.app.view;

import com.navdy.service.library.events.input.GestureEvent;
import java.util.List;
import java.util.ArrayList;
import android.widget.TextView;
import android.widget.ImageView;
import android.view.View;
import butterknife.ButterKnife;
import mortar.Mortar;
import android.util.AttributeSet;
import android.content.Context;
import javax.inject.Inject;
import com.navdy.hud.app.screen.GestureLearningScreen;
import butterknife.InjectView;
import android.view.ViewGroup;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.hud.app.manager.InputManager;
import android.widget.FrameLayout;

public class LearnGestureScreenLayout extends FrameLayout implements IInputHandler, IListener
{
    public static final int TAG_BACK = 0;
    public static final int TAG_TIPS = 1;
    @InjectView(R.id.sensor_blocked_message)
    ViewGroup mCameraBlockedMessage;
    ChoiceLayout mCameraSensorBlockedChoiceLayout;
    private Mode mCurrentMode;
    @InjectView(R.id.gesture_learning_view)
    GestureLearningView mGestureLearningView;
    @Inject
    GestureLearningScreen.Presenter mPresenter;
    @InjectView(R.id.scrollable_text_presenter)
    ScrollableTextPresenterLayout mScrollableTextPresenter;
    @InjectView(R.id.capture_instructions_lyt)
    GestureVideoCaptureView mVideoCaptureInstructionsLayout;
    
    public LearnGestureScreenLayout(final Context context) {
        this(context, null);
    }
    
    public LearnGestureScreenLayout(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public LearnGestureScreenLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mCurrentMode = Mode.GESTURE;
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
    }
    
    public void executeItem(final int n, final int n2) {
        switch (n2) {
            case 0:
                this.mPresenter.hideCameraSensorBlocked();
                break;
            case 1:
                this.mPresenter.showTips();
                break;
        }
    }
    
    public void hideCaptureGestureVideosView() {
        this.mGestureLearningView.setVisibility(View.VISIBLE);
        this.mCameraBlockedMessage.setVisibility(GONE);
        this.mVideoCaptureInstructionsLayout.setVisibility(GONE);
        this.mScrollableTextPresenter.setVisibility(GONE);
        this.mCurrentMode = Mode.GESTURE;
    }
    
    public void hideSensorBlocked() {
        this.mGestureLearningView.setVisibility(View.VISIBLE);
        this.mCameraBlockedMessage.setVisibility(GONE);
        this.mVideoCaptureInstructionsLayout.setVisibility(GONE);
        this.mScrollableTextPresenter.setVisibility(GONE);
        this.mCurrentMode = Mode.GESTURE;
    }
    
    public void hideTips() {
        this.mGestureLearningView.setVisibility(View.VISIBLE);
        this.mVideoCaptureInstructionsLayout.setVisibility(GONE);
        this.mCameraBlockedMessage.setVisibility(GONE);
        this.mScrollableTextPresenter.setVisibility(GONE);
        this.mCurrentMode = Mode.GESTURE;
    }
    
    public void itemSelected(final int n, final int n2) {
    }
    
    public IInputHandler nextHandler() {
        IInputHandler inputHandler = null;
        switch (this.mCurrentMode) {
            default:
                inputHandler = null;
                break;
            case GESTURE:
                inputHandler = this.mGestureLearningView;
                break;
            case TIPS:
                inputHandler = this.mScrollableTextPresenter;
                break;
            case CAPTURE:
                inputHandler = this.mVideoCaptureInstructionsLayout;
                break;
        }
        return inputHandler;
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mPresenter != null) {
            this.mPresenter.dropView(this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        final ImageView imageView = (ImageView)this.mCameraBlockedMessage.findViewById(R.id.sideImage);
        imageView.setVisibility(View.VISIBLE);
        imageView.setImageResource(R.drawable.icon_alert_sensor_blocked);
        final ImageView imageView2 = (ImageView)this.mCameraBlockedMessage.findViewById(R.id.image);
        imageView2.setVisibility(View.VISIBLE);
        imageView2.setImageResource(R.drawable.icon_settings_learning_to_gesture_gray);
        ((TextView)this.mCameraBlockedMessage.findViewById(R.id.title1)).setText(R.string.sensor_is_blocked);
        final TextView textView = (TextView)this.mCameraBlockedMessage.findViewById(R.id.title3);
        ((MaxWidthLinearLayout)this.mCameraBlockedMessage.findViewById(R.id.infoContainer)).setMaxWidth(this.getResources().getDimensionPixelSize(R.dimen.gesture_sensor_blocked_max_width));
        textView.setText(R.string.sensor_is_blocked_message);
        this.mCameraBlockedMessage.findViewById(R.id.title2).setVisibility(GONE);
        this.mCameraBlockedMessage.findViewById(R.id.title4).setVisibility(GONE);
        textView.setSingleLine(false);
        textView.setTextSize(1, 17.0f);
        final ArrayList<Choice> list = new ArrayList<Choice>();
        list.add(new ChoiceLayout.Choice(this.getContext().getString(R.string.back), 0));
        list.add(new ChoiceLayout.Choice(this.getContext().getString(R.string.tips), 1));
        (this.mCameraSensorBlockedChoiceLayout = (ChoiceLayout)this.mCameraBlockedMessage.findViewById(R.id.choiceLayout)).setChoices(ChoiceLayout.Mode.LABEL, list, 0, (ChoiceLayout.IListener)this);
        this.mCameraSensorBlockedChoiceLayout.setHighlightPersistent(true);
        final String[] stringArray = this.getResources().getStringArray(R.array.gesture_tips);
        final CharSequence[] textContents = new CharSequence[stringArray.length];
        for (int i = 0; i < stringArray.length; ++i) {
            textContents[i] = stringArray[i];
        }
        this.mScrollableTextPresenter.setTextContents(textContents);
        if (this.mPresenter != null) {
            this.mPresenter.takeView(this);
        }
        this.mGestureLearningView.setVisibility(View.VISIBLE);
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        boolean b = true;
        if (this.mCurrentMode == Mode.SENSOR_BLOCKED) {
            switch (customKeyEvent) {
                case LEFT:
                    this.mCameraSensorBlockedChoiceLayout.moveSelectionLeft();
                    break;
                case RIGHT:
                    this.mCameraSensorBlockedChoiceLayout.moveSelectionRight();
                    break;
                case SELECT:
                    this.mCameraSensorBlockedChoiceLayout.executeSelectedItem(true);
                    break;
            }
        }
        else {
            b = false;
        }
        return b;
    }
    
    public void showCaptureGestureVideosView() {
        this.mGestureLearningView.setVisibility(GONE);
        this.mCameraBlockedMessage.setVisibility(GONE);
        this.mVideoCaptureInstructionsLayout.setVisibility(View.VISIBLE);
        this.mScrollableTextPresenter.setVisibility(GONE);
        this.mCurrentMode = Mode.CAPTURE;
    }
    
    public void showSensorBlocked() {
        this.mGestureLearningView.setVisibility(GONE);
        this.mVideoCaptureInstructionsLayout.setVisibility(GONE);
        this.mCameraBlockedMessage.setVisibility(View.VISIBLE);
        this.mScrollableTextPresenter.setVisibility(GONE);
        this.mCurrentMode = Mode.SENSOR_BLOCKED;
    }
    
    public void showTips() {
        this.mGestureLearningView.setVisibility(GONE);
        this.mCameraBlockedMessage.setVisibility(GONE);
        this.mVideoCaptureInstructionsLayout.setVisibility(GONE);
        this.mScrollableTextPresenter.setVisibility(View.VISIBLE);
        this.mCurrentMode = Mode.TIPS;
    }
    
    enum Mode
    {
        CAPTURE, 
        GESTURE, 
        SENSOR_BLOCKED, 
        TIPS;
    }
}
