package com.navdy.hud.app.view.drawable;

import android.graphics.ColorFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

public class CustomDrawable extends Drawable
{
    protected Paint mPaint;
    
    public CustomDrawable() {
        this.mPaint = new Paint();
    }
    
    public void draw(final Canvas canvas) {
    }
    
    public int getOpacity() {
        return -3;
    }
    
    public void setAlpha(final int alpha) {
        this.mPaint.setAlpha(alpha);
    }
    
    public void setColorFilter(final ColorFilter colorFilter) {
        this.mPaint.setColorFilter(colorFilter);
    }
}
