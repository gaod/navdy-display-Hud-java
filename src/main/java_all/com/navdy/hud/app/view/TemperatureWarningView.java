package com.navdy.hud.app.view;

import com.navdy.service.library.events.input.Gesture;
import com.navdy.service.library.events.input.GestureEvent;
import java.util.List;
import java.util.ArrayList;
import android.view.View;
import butterknife.ButterKnife;
import android.os.SystemClock;
import com.navdy.hud.app.framework.toast.ToastManager;
import android.util.AttributeSet;
import mortar.Mortar;
import android.content.Context;
import javax.inject.Inject;
import com.navdy.hud.app.screen.TemperatureWarningScreen;
import android.widget.TextView;
import android.widget.ImageView;
import butterknife.InjectView;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import android.widget.RelativeLayout;

public class TemperatureWarningView extends RelativeLayout implements IListener, IInputHandler
{
    private static final int WARNING_MESSAGE_MAX_WIDTH = 320;
    private static long lastDismissedTime;
    private static final Logger sLogger;
    @InjectView(R.id.choiceLayout)
    ChoiceLayout mChoiceLayout;
    @InjectView(R.id.image)
    ImageView mIcon;
    @InjectView(R.id.title2)
    TextView mMainTitleText;
    @Inject
    TemperatureWarningScreen.Presenter mPresenter;
    @InjectView(R.id.rightSwipe)
    ImageView mRightSwipe;
    @InjectView(R.id.mainTitle)
    TextView mScreenTitleText;
    @InjectView(R.id.title1)
    TextView mTitle1;
    @InjectView(R.id.title3)
    TextView mWarningMessage;
    
    static {
        sLogger = new Logger(TemperatureWarningView.class);
        TemperatureWarningView.lastDismissedTime = -1L;
    }
    
    public TemperatureWarningView(final Context context) {
        super(context);
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
    }
    
    public TemperatureWarningView(final Context context, final AttributeSet set) {
        super(context, set);
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
    }
    
    public static long getLastDismissedTime() {
        return TemperatureWarningView.lastDismissedTime;
    }
    
    public void executeItem(final int n, final int n2) {
        this.mPresenter.finish();
    }
    
    public void itemSelected(final int n, final int n2) {
    }
    
    public IInputHandler nextHandler() {
        return null;
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ToastManager.getInstance().disableToasts(false);
        TemperatureWarningView.lastDismissedTime = SystemClock.elapsedRealtime();
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        ToastManager.getInstance().disableToasts(true);
        this.mScreenTitleText.setText(R.string.temperature_title);
        this.mMainTitleText.setText(R.string.navdy_too_hot);
        this.mIcon.setImageResource(R.drawable.icon_warning_temperature);
        ((MaxWidthLinearLayout)this.findViewById(R.id.infoContainer)).setMaxWidth(320);
        this.mTitle1.setVisibility(GONE);
        this.mWarningMessage.setVisibility(View.VISIBLE);
        this.mWarningMessage.setText(R.string.avoid_overheating);
        this.mWarningMessage.setSingleLine(false);
        final ArrayList<Choice> list = new ArrayList<Choice>();
        list.add(new ChoiceLayout.Choice(this.getContext().getString(R.string.dismiss), 0));
        this.mChoiceLayout.setChoices(Mode.LABEL, list, 0, (ChoiceLayout.IListener)this);
        this.mRightSwipe.setVisibility(View.VISIBLE);
        TemperatureWarningView.lastDismissedTime = 0L;
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        boolean b;
        if (this.mRightSwipe != null && this.mRightSwipe.getVisibility() == 0 && gestureEvent.gesture == Gesture.GESTURE_SWIPE_RIGHT) {
            this.mPresenter.finish();
            b = true;
        }
        else {
            b = false;
        }
        return b;
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        boolean b = true;
        if (this.mChoiceLayout != null && this.mChoiceLayout.getVisibility() == 0 && customKeyEvent == CustomKeyEvent.SELECT) {
            this.mChoiceLayout.executeSelectedItem(true);
        }
        else {
            b = false;
        }
        return b;
    }
}
