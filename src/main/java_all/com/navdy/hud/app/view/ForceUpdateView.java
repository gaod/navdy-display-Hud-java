package com.navdy.hud.app.view;

import com.squareup.otto.Subscribe;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.service.library.events.input.GestureEvent;
import java.util.List;
import com.navdy.hud.app.ui.activity.Main;
import android.view.View;
import butterknife.ButterKnife;
import java.util.ArrayList;
import android.util.AttributeSet;
import mortar.Mortar;
import android.content.Context;
import android.widget.TextView;
import javax.inject.Inject;
import com.navdy.hud.app.screen.ForceUpdateScreen;
import android.widget.ImageView;
import butterknife.InjectView;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import android.widget.RelativeLayout;

public class ForceUpdateView extends RelativeLayout implements IListener, IInputHandler
{
    private static final int TAG_DISMISS = 2;
    private static final int TAG_INSTALL = 1;
    private static final int TAG_SHUT_DOWN = 0;
    private static final int UPDATE_MESSAGE_MAX_WIDTH = 350;
    private static final Logger sLogger;
    @InjectView(R.id.choiceLayout)
    ChoiceLayout mChoiceLayout;
    @InjectView(R.id.image)
    ImageView mIcon;
    @Inject
    ForceUpdateScreen.Presenter mPresenter;
    @InjectView(R.id.mainTitle)
    TextView mScreenTitleText;
    @InjectView(R.id.title1)
    TextView mTextView1;
    @InjectView(R.id.title2)
    TextView mTextView2;
    @InjectView(R.id.title3)
    TextView mTextView3;
    private boolean waitingForUpdate;
    
    static {
        sLogger = new Logger(ForceUpdateView.class);
    }
    
    public ForceUpdateView(final Context context) {
        super(context);
        this.waitingForUpdate = false;
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
    }
    
    public ForceUpdateView(final Context context, final AttributeSet set) {
        super(context, set);
        this.waitingForUpdate = false;
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
    }
    
    public ForceUpdateView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.waitingForUpdate = false;
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
    }
    
    private void performAction(final int n) {
        switch (n) {
            default:
                ForceUpdateView.sLogger.e("unknown action received: " + n);
                break;
            case 0:
                this.mPresenter.shutDown();
                break;
            case 1:
                this.mPresenter.install();
                break;
            case 2:
                this.mPresenter.dismiss();
                break;
        }
    }
    
    public void executeItem(final int n, final int n2) {
        this.performAction(n2);
    }
    
    public void itemSelected(final int n, final int n2) {
    }
    
    public IInputHandler nextHandler() {
        return null;
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mPresenter.getBus().unregister(this);
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        final boolean softwareUpdatePending = this.mPresenter.isSoftwareUpdatePending();
        final ArrayList<Choice> list = new ArrayList<Choice>();
        ButterKnife.inject((View)this);
        this.mPresenter.takeView(this);
        this.mPresenter.getBus().register(this);
        ((MaxWidthLinearLayout)this.findViewById(R.id.infoContainer)).setMaxWidth(350);
        this.mIcon.setImageResource(R.drawable.icon_software_update);
        this.mTextView1.setVisibility(GONE);
        this.mTextView2.setVisibility(GONE);
        this.mTextView3.setSingleLine(false);
        this.mTextView3.setVisibility(View.VISIBLE);
        if (Main.mProtocolStatus == Main.ProtocolStatus.PROTOCOL_REMOTE_NEEDS_UPDATE) {
            this.mScreenTitleText.setText(R.string.title_app_update_required);
            this.mTextView3.setText(R.string.phone_update_required);
            list.add(new ChoiceLayout.Choice(this.getContext().getString(R.string.dismiss), 2));
        }
        else {
            this.mScreenTitleText.setText(R.string.title_display_update_required);
            if (!softwareUpdatePending) {
                this.mTextView3.setText(R.string.hud_update_download);
                this.waitingForUpdate = true;
            }
            else {
                this.mTextView3.setText(R.string.hud_update_install);
                list.add(new ChoiceLayout.Choice(this.getContext().getString(R.string.install), 1));
            }
            list.add(new ChoiceLayout.Choice(this.getContext().getString(R.string.shutdown), 0));
        }
        this.mChoiceLayout.setChoices(Mode.LABEL, list, 0, (ChoiceLayout.IListener)this);
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        boolean b = true;
        if (this.mChoiceLayout != null && this.mChoiceLayout.getVisibility() == 0) {
            switch (customKeyEvent) {
                case LEFT:
                    this.mChoiceLayout.moveSelectionLeft();
                    break;
                case RIGHT:
                    this.mChoiceLayout.moveSelectionRight();
                    break;
                case SELECT:
                    this.mChoiceLayout.executeSelectedItem(true);
                    break;
            }
        }
        else {
            b = false;
        }
        return b;
    }
    
    @Subscribe
    public void onUpdateReady(final OTAUpdateService.UpdateVerified updateVerified) {
        if (this.waitingForUpdate) {
            final ArrayList<Choice> list = new ArrayList<Choice>();
            list.add(new ChoiceLayout.Choice(this.getContext().getString(R.string.install), 1));
            list.add(new ChoiceLayout.Choice(this.getContext().getString(R.string.shutdown), 0));
            this.mChoiceLayout.setChoices(Mode.LABEL, list, 0, (ChoiceLayout.IListener)this);
            this.mTextView3.setText(R.string.hud_update_install);
            this.waitingForUpdate = false;
        }
    }
}
