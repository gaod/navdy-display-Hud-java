package com.navdy.hud.app.view;

import android.content.res.TypedArray;
import com.navdy.service.library.events.input.GestureEvent;
import butterknife.ButterKnife;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.device.dial.DialConstants;
import com.navdy.hud.app.device.dial.DialManagerHelper;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.toast.ToastManager;
import android.animation.Animator.AnimatorListener;
import android.animation.Animator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.animation.TimeInterpolator;
import android.view.animation.AccelerateDecelerateInterpolator;
import com.navdy.hud.app.event.LocalSpeechRequest;
import java.util.Locale;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.service.library.events.audio.SpeechRequest;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.text.TextUtils;
import android.media.MediaPlayer;
import android.media.MediaPlayer;
import android.media.MediaPlayer;
import android.content.res.Resources;
import java.util.ArrayList;
import android.text.style.ImageSpan;
import android.text.SpannableStringBuilder;
import com.navdy.hud.app.device.dial.DialNotification;
import com.navdy.hud.app.device.dial.DialManager;
import android.view.View;
import android.animation.LayoutTransition;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import java.util.List;
import java.util.Collections;
import android.view.ViewGroup.MarginLayoutParams;
import com.navdy.hud.app.R;
import com.navdy.hud.app.HudApplication;
import mortar.Mortar;
import android.os.Looper;
import android.util.AttributeSet;
import android.content.Context;
import java.util.concurrent.TimeUnit;
import android.widget.VideoView;
import android.view.ViewGroup;
import android.animation.ObjectAnimator;
import android.content.SharedPreferences;
import android.widget.TextView;
import javax.inject.Inject;
import com.navdy.hud.app.screen.DialManagerScreen;
import android.os.Handler;
import butterknife.InjectView;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.service.library.log.Logger;
import android.net.Uri;
import com.navdy.hud.app.manager.InputManager;
import android.widget.RelativeLayout;

public class DialManagerView extends RelativeLayout implements IInputHandler
{
    private static final int CONNECTED_TIMEOUT = 30000;
    public static final int DIAL_CONNECTED = 5;
    public static final int DIAL_CONNECTING = 4;
    public static final int DIAL_CONNECTION_FAILED = 6;
    public static final int DIAL_PAIRED = 3;
    public static final int DIAL_PAIRING = 1;
    public static final int DIAL_SEARCHING = 2;
    private static final String EMPTY = "";
    private static final long POST_OTA_TIMEOUT;
    private static final int POS_DISMISS = 0;
    private static final int RESTART_INTERVAL = 2000;
    private static Uri firstLaunchVideoUri;
    private static Uri repairVideoUri;
    private static final Logger sLogger;
    private int batteryLevel;
    private int completeCount;
    private ChoiceLayout.IListener connectedChoiceListener;
    private Runnable connectedTimeoutRunnable;
    @InjectView(R.id.connectedView)
    public ConfirmationLayout connectedView;
    private String dialName;
    private boolean firstTime;
    private boolean foundDial;
    private Handler handler;
    private int incrementalVersion;
    private int initialState;
    @Inject
    DialManagerScreen.Presenter presenter;
    @InjectView(R.id.repair_text)
    public TextView rePairTextView;
    private boolean registered;
    private Runnable restartScanRunnable;
    @Inject
    SharedPreferences sharedPreferences;
    private ObjectAnimator spinner;
    private int state;
    @InjectView(R.id.videoContainer)
    public ViewGroup videoContainer;
    private Uri videoUri;
    @InjectView(R.id.videoView)
    public VideoView videoView;
    private boolean videoViewInitialized;
    
    static {
        sLogger = new Logger(DialManagerView.class);
        POST_OTA_TIMEOUT = TimeUnit.SECONDS.toMillis(15L);
    }
    
    public DialManagerView(final Context context) {
        this(context, null);
    }
    
    public DialManagerView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public DialManagerView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.initialState = 0;
        this.state = 0;
        this.dialName = "Navdy Dial";
        this.handler = new Handler(Looper.getMainLooper());
        this.connectedTimeoutRunnable = new Runnable() {
            @Override
            public void run() {
                DialManagerView.sLogger.v("connected timeout");
                DialManagerView.this.presenter.dismiss();
            }
        };
        this.connectedChoiceListener = new ChoiceLayout.IListener() {
            @Override
            public void executeItem(final int n, final int n2) {
                if (n == 0) {
                    DialManagerView.this.presenter.dismiss();
                }
            }
            
            @Override
            public void itemSelected(final int n, final int n2) {
            }
        };
        this.restartScanRunnable = new Runnable() {
            @Override
            public void run() {
                DialManagerView.this.handler.removeCallbacks((Runnable)this);
                DialManagerView.this.showDialPairing();
            }
        };
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
            if (DialManagerView.firstLaunchVideoUri == null) {
                final String packageName = HudApplication.getAppContext().getPackageName();
                DialManagerView.firstLaunchVideoUri = Uri.parse("android.resource://" + packageName + "/raw/dial_pairing_flow");
                DialManagerView.repairVideoUri = Uri.parse("android.resource://" + packageName + "/raw/dial_repairing_flow");
            }
        }
        else {
            HudApplication.setContext(context);
        }
        this.initFromAttributes(context, set);
    }
    
    private void clearConnectedTimeout() {
        this.presenter.getHandler().removeCallbacks(this.connectedTimeoutRunnable);
    }
    
    private void initFromAttributes(Context obtainStyledAttributes, final AttributeSet set) {
        obtainStyledAttributes = (Context)obtainStyledAttributes.getTheme().obtainStyledAttributes(set, R.styleable.DialManagerScreen, 0, 0);
        try {
            this.initialState = ((TypedArray)obtainStyledAttributes).getInt(0, 1);
            this.batteryLevel = ((TypedArray)obtainStyledAttributes).getInt(1, -1);
        }
        finally {
            ((TypedArray)obtainStyledAttributes).recycle();
        }
    }
    
    private boolean isInfoViewVisible() {
        return this.connectedView.getVisibility() == 0 && this.connectedView.title4.getVisibility() == 0;
    }
    
    private void resetConnectedTimeout() {
        final Handler handler = this.presenter.getHandler();
        handler.removeCallbacks(this.connectedTimeoutRunnable);
        handler.postDelayed(this.connectedTimeoutRunnable, 30000L);
    }
    
    private void resetConnectedView() {
        this.connectedView.title1.setVisibility(GONE);
        this.connectedView.title1.setTextAppearance(this.getContext(), R.style.title1);
        this.connectedView.title2.setTextAppearance(this.getContext(), R.style.title2);
        this.connectedView.title3.setVisibility(GONE);
        this.connectedView.title4.setVisibility(GONE);
        this.connectedView.sideImage.setVisibility(GONE);
        ((ViewGroup.MarginLayoutParams)this.connectedView.choiceLayout.getLayoutParams()).bottomMargin = (int)this.getResources().getDimension(R.dimen.dial_pairing_status_margin_bottom);
        this.connectedView.choiceLayout.setChoices(ChoiceLayout.Mode.LABEL, Collections.EMPTY_LIST, 0, this.connectedChoiceListener);
    }
    
    private void setConnectingUI() {
        this.stopVideo();
        this.connectedView.screenImage.setImage(R.drawable.icon_dial_2, null, InitialsImageView.Style.DEFAULT);
        this.connectedView.screenImage.setVisibility(View.VISIBLE);
        this.connectedView.mainSection.setLayoutTransition(new LayoutTransition());
        this.connectedView.mainSection.removeView((View)this.connectedView.titleContainer);
        this.connectedView.sideImage.setImageResource(R.drawable.icon_sm_spinner);
        this.connectedView.sideImage.setVisibility(View.VISIBLE);
        this.startSpinner();
        this.connectedView.setVisibility(View.VISIBLE);
    }
    
    private void setState(final int state) {
        if (this.state != state) {
            DialManagerView.sLogger.i("Switching to state:" + state);
        }
        switch (this.state = state) {
            case 1:
                this.showDialPairingState();
                break;
            case 2:
                this.showDialSearchingState();
                break;
            case 3:
                this.showPairedState();
                break;
            case 4:
                this.setConnectingUI();
                break;
            case 5:
                this.showDialConnectedState();
                break;
            case 6:
                this.showConnectionFailedState();
                break;
        }
    }
    
    private void setVideoURI(final Uri uri) {
        Label_0029: {
            if (uri == null) {
                break Label_0029;
            }
            while (true) {
                while (true) {
                    try {
                        if (!uri.equals(this.videoUri)) {
                            this.videoView.setVideoURI(uri);
                        }
                        else if (uri == null) {
                            this.videoView.stopPlayback();
                        }
                        this.videoUri = uri;
                        return;
                    }
                    catch (Throwable t) {
                        DialManagerView.sLogger.e(t);
                        continue;
                    }
                    continue;
                }
            }
        }
    }
    
    private void showConnectionFailedState() {
        this.stopVideo();
        this.connectedView.screenImage.setImage(R.drawable.icon_dial_2, null, InitialsImageView.Style.DEFAULT);
        this.connectedView.screenImage.setVisibility(View.VISIBLE);
        this.connectedView.sideImage.setImageResource(R.drawable.icon_connection_failed);
        this.connectedView.sideImage.setVisibility(View.VISIBLE);
        this.connectedView.setVisibility(View.VISIBLE);
    }
    
    private void showDialConnectedState() {
        this.stopVideo();
        final Context context = this.getContext();
        final Resources resources = context.getResources();
        this.connectedView.screenImage.setImage(R.drawable.icon_dial_2, null, InitialsImageView.Style.DEFAULT);
        this.connectedView.sideImage.setImageResource(R.drawable.icon_sm_success);
        this.connectedView.sideImage.setVisibility(View.VISIBLE);
        this.connectedView.titleContainer.setVisibility(View.VISIBLE);
        this.connectedView.title1.setTextAppearance(context, R.style.title2);
        this.connectedView.title1.setText((CharSequence)resources.getString(R.string.dial_paired_text));
        this.connectedView.title2.setTextAppearance(context, R.style.title1);
        if (this.dialName != null) {
            this.connectedView.title2.setText((CharSequence)this.dialName);
        }
        else {
            this.connectedView.title2.setVisibility(GONE);
        }
        switch (DialManager.getBatteryLevelCategory(this.batteryLevel)) {
            default:
                this.connectedView.title3.setText((CharSequence)resources.getString(R.string.dial_battery_level_ok));
                break;
            case LOW_BATTERY: {
                final SpannableStringBuilder text = new SpannableStringBuilder((CharSequence)("  " + resources.getString(R.string.dial_battery_level_low, new Object[] { DialNotification.getDialBatteryLevel() })));
                text.setSpan(new ImageSpan(context, R.drawable.icon_dial_batterylow_2_copy, 1), 0, 1, 33);
                this.connectedView.title3.setText((CharSequence)text);
                break;
            }
            case VERY_LOW_BATTERY: {
                final SpannableStringBuilder text2 = new SpannableStringBuilder((CharSequence)("  " + resources.getString(R.string.dial_battery_level_very_low, new Object[] { DialNotification.getDialBatteryLevel() })));
                text2.setSpan(new ImageSpan(context, R.drawable.icon_dial_batterylow_copy, 1), 0, 1, 33);
                this.connectedView.title3.setText((CharSequence)text2);
                break;
            }
            case EXTREMELY_LOW_BATTERY: {
                final SpannableStringBuilder text3 = new SpannableStringBuilder((CharSequence)("  " + resources.getString(R.string.dial_battery_level_extremely_low, new Object[] { DialNotification.getDialBatteryLevel() })));
                text3.setSpan(new ImageSpan(context, R.drawable.icon_dial_batterylow_copy, 1), 0, 1, 33);
                this.connectedView.title3.setText((CharSequence)text3);
                break;
            }
        }
        final int incrementalVersion = this.incrementalVersion;
        String string;
        if (incrementalVersion == -1) {
            string = "";
        }
        else {
            string = "1.0." + incrementalVersion;
        }
        this.connectedView.title4.setText((CharSequence)resources.getString(R.string.dial_firmware_rev, new Object[] { string }));
        this.connectedView.title4.setVisibility(View.VISIBLE);
        ((ViewGroup.MarginLayoutParams)this.connectedView.choiceLayout.getLayoutParams()).bottomMargin = (int)resources.getDimension(R.dimen.dial_connected_status_margin_bottom);
        final ArrayList<ChoiceLayout.Choice> list = new ArrayList<ChoiceLayout.Choice>(2);
        list.add(new ChoiceLayout.Choice(resources.getString(R.string.dial_paired_choice_dismiss), 0));
        this.connectedView.choiceLayout.setChoices(ChoiceLayout.Mode.LABEL, list, 0, this.connectedChoiceListener);
        this.connectedView.setVisibility(View.VISIBLE);
        if (this.presenter != null) {
            final Handler handler = this.presenter.getHandler();
            handler.removeCallbacks(this.connectedTimeoutRunnable);
            handler.postDelayed(this.connectedTimeoutRunnable, 30000L);
        }
    }
    
    private void showDialPairingState() {
        this.firstTime = (DialManager.getInstance().getBondedDialCount() == 0);
        this.connectedView.setVisibility(GONE);
        this.resetConnectedView();
        if (!this.videoViewInitialized) {
            this.videoView.setOnPreparedListener((MediaPlayer$OnPreparedListener)new MediaPlayer$OnPreparedListener() {
                public void onPrepared(final MediaPlayer mediaPlayer) {
                    mediaPlayer.setLooping(true);
                    mediaPlayer.start();
                    DialManagerView.sLogger.v("Duration= " + mediaPlayer.getDuration());
                }
            });
            this.videoView.setOnCompletionListener((MediaPlayer$OnCompletionListener)new MediaPlayer$OnCompletionListener() {
                public void onCompletion(final MediaPlayer mediaPlayer) {
                    ++DialManagerView.this.completeCount;
                    DialManagerView.sLogger.v("completed:" + DialManagerView.this.completeCount);
                }
            });
            this.videoViewInitialized = true;
        }
        this.videoContainer.setVisibility(View.VISIBLE);
        if (this.sharedPreferences != null && this.sharedPreferences.getBoolean("first_run_dial_video_shown", false)) {
            this.setVideoURI(DialManagerView.repairVideoUri);
            this.rePairTextView.setText((CharSequence)this.getContext().getString(R.string.navdy_dial_repair_text));
            this.rePairTextView.setVisibility(View.VISIBLE);
        }
        else {
            this.setVideoURI(DialManagerView.firstLaunchVideoUri);
            this.rePairTextView.setVisibility(GONE);
        }
        DialManagerView.sLogger.v("starting scanning");
        DialManager.getInstance().startScan();
    }
    
    private void showDialSearchingState() {
        this.firstTime = false;
        this.resetConnectedView();
        this.setConnectingUI();
        DialManagerView.sLogger.v("starting scanning");
        DialManager.getInstance().startScan();
        this.handler.postDelayed(this.restartScanRunnable, DialManagerView.POST_OTA_TIMEOUT);
    }
    
    private void showPairedState() {
        DialManagerView.sLogger.v("showPairingSuccessUI=" + this.dialName);
        final Context context = this.getContext();
        this.stopVideo();
        final Resources resources = context.getResources();
        this.connectedView.mainSection.removeView((View)this.connectedView.titleContainer);
        this.connectedView.mainSection.setLayoutTransition(new LayoutTransition());
        this.connectedView.screenImage.setImage(R.drawable.icon_dial_2, null, InitialsImageView.Style.DEFAULT);
        this.connectedView.screenImage.setVisibility(View.VISIBLE);
        this.connectedView.sideImage.setImageResource(R.drawable.icon_sm_success);
        this.connectedView.sideImage.setVisibility(View.VISIBLE);
        this.connectedView.title1.setVisibility(GONE);
        this.connectedView.title2.setTextAppearance(context, R.style.Glances_1_bold);
        this.connectedView.title2.setText((CharSequence)resources.getString(R.string.navdy_dial));
        this.dialName = DialNotification.getDialAddressPart(this.dialName);
        if (!TextUtils.isEmpty((CharSequence)this.dialName)) {
            this.connectedView.title3.setText((CharSequence)resources.getString(R.string.navdy_dial_name, new Object[] { this.dialName }));
            this.connectedView.title3.setVisibility(View.VISIBLE);
        }
        else {
            this.connectedView.title3.setVisibility(GONE);
        }
        this.connectedView.titleContainer.setVisibility(View.VISIBLE);
        this.connectedView.mainSection.addView((View)this.connectedView.titleContainer);
        this.connectedView.setVisibility(View.VISIBLE);
    }
    
    private void showPairingSuccessUI(final String dialName) {
        this.dialName = dialName;
        this.setState(3);
        RemoteDeviceManager.getInstance().getBus().post(new LocalSpeechRequest(new SpeechRequest.Builder().words(TTSUtils.TTS_DIAL_CONNECTED).language(Locale.getDefault().toLanguageTag()).category(SpeechRequest.Category.SPEECH_NOTIFICATION).build()));
        this.handler.postDelayed((Runnable)new Runnable() {
            @Override
            public void run() {
                if (DialManagerView.this.presenter != null && DialManagerView.this.presenter.isActive()) {
                    DialManagerView.this.presenter.dismiss();
                }
                else {
                    DialManagerView.sLogger.v("showPairingSuccessUI:hide not active");
                }
            }
        }, 5000L);
    }
    
    private void startSpinner() {
        if (this.spinner == null) {
            (this.spinner = ObjectAnimator.ofFloat(this.connectedView.sideImage, View.ROTATION, new float[] { 360.0f })).setDuration(500L);
            this.spinner.setInterpolator((TimeInterpolator)new AccelerateDecelerateInterpolator());
            this.spinner.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
                @Override
                public void onAnimationEnd(final Animator animator) {
                    if (DialManagerView.this.spinner != null) {
                        DialManagerView.this.spinner.setStartDelay(33L);
                        DialManagerView.this.spinner.start();
                    }
                }
            });
        }
        if (!this.spinner.isRunning()) {
            this.spinner.start();
        }
    }
    
    private void stopSpinner() {
        if (this.spinner != null) {
            this.spinner.removeAllListeners();
            this.spinner.cancel();
            this.connectedView.sideImage.setRotation(0.0f);
            this.spinner = null;
        }
    }
    
    private void stopVideo() {
        if (this.videoView.isPlaying()) {
            this.setVideoURI(null);
            DialManagerView.sLogger.v("stopped video");
        }
        else {
            DialManagerView.sLogger.v("video not playing");
        }
        this.videoContainer.setVisibility(GONE);
    }
    
    public IInputHandler nextHandler() {
        return null;
    }
    
    protected void onAttachedToWindow() {
        DialManagerView.sLogger.v("onAttachToWindow");
        super.onAttachedToWindow();
        if (this.presenter != null) {
            ToastManager.getInstance().disableToasts(true);
            NotificationManager.getInstance().enableNotifications(false);
            this.presenter.takeView(this);
            this.presenter.getBus().register(this);
            this.registered = true;
        }
        else {
            this.setState(this.initialState);
        }
    }
    
    protected void onDetachedFromWindow() {
        DialManagerView.sLogger.v("onDetachToWindow");
        this.clearConnectedTimeout();
        if (this.videoContainer.getVisibility() == 0) {
            DialManagerView.sLogger.v("stop dial scan");
            DialManager.getInstance().stopScan(false);
            this.stopVideo();
        }
        ToastManager.getInstance().disableToasts(false);
        NotificationManager.getInstance().enableNotifications(true);
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView(this);
            if (this.registered) {
                this.presenter.getBus().unregister(this);
            }
        }
        this.stopSpinner();
        if (!this.isInfoViewVisible()) {
            DialManagerHelper.sendLocalyticsEvent(this.presenter.getHandler(), this.firstTime, this.foundDial, 10000, true);
        }
    }
    
    @Subscribe
    public void onDialConnectionStatus(final DialConstants.DialConnectionStatus dialConnectionStatus) {
        if (!this.isInfoViewVisible()) {
            DialManagerView.sLogger.v("onDialConnectionStatus:" + dialConnectionStatus.status + " name:" + dialConnectionStatus.dialName);
            if (this.foundDial) {
                DialManagerView.sLogger.v("already found dial");
            }
            else {
                final DialManager instance = DialManager.getInstance();
                this.handler.removeCallbacks(this.restartScanRunnable);
                this.stopSpinner();
                switch (dialConnectionStatus.status) {
                    case CONNECTED: {
                        DialManagerView.sLogger.v("dial connected:" + dialConnectionStatus.dialName);
                        this.foundDial = true;
                        instance.stopScan(false);
                        final ToastManager instance2 = ToastManager.getInstance();
                        instance2.dismissCurrentToast();
                        instance2.clearAllPendingToast();
                        instance2.disableToasts(false);
                        DialManagerView.sLogger.v("show connected state");
                        this.showPairingSuccessUI(dialConnectionStatus.dialName);
                        break;
                    }
                    case CONNECTING:
                        DialManagerView.sLogger.v("dial connnecting:" + dialConnectionStatus.dialName);
                        this.stopVideo();
                        this.setConnectingUI();
                        break;
                    case DISCONNECTED:
                        DialManagerView.sLogger.v("dial connection disconnected:" + dialConnectionStatus.dialName);
                        this.handler.post(this.restartScanRunnable);
                        break;
                    case CONNECTION_FAILED:
                        DialManagerView.sLogger.v("dial connection failed:" + dialConnectionStatus.dialName);
                        this.setState(6);
                        this.handler.postDelayed(this.restartScanRunnable, 2000L);
                        break;
                    case NO_DIAL_FOUND:
                        DialManagerView.sLogger.v("no dials found");
                        this.showDialPairing();
                        break;
                }
            }
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        ((MaxWidthLinearLayout)this.findViewById(R.id.infoContainer)).setMaxWidth(this.getContext().getResources().getDimensionPixelOffset(R.dimen.dial_message_max_width));
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        switch (customKeyEvent) {
            case POWER_BUTTON_CLICK:
            case POWER_BUTTON_DOUBLE_CLICK:
                this.clearConnectedTimeout();
                this.presenter.dismiss();
                break;
            case LEFT:
                if (this.isInfoViewVisible()) {
                    this.resetConnectedTimeout();
                    this.connectedView.choiceLayout.moveSelectionLeft();
                    break;
                }
                break;
            case RIGHT:
                if (this.isInfoViewVisible()) {
                    this.resetConnectedTimeout();
                    this.connectedView.choiceLayout.moveSelectionRight();
                    break;
                }
                break;
            case SELECT:
                if (this.isInfoViewVisible()) {
                    this.clearConnectedTimeout();
                    this.connectedView.choiceLayout.executeSelectedItem(true);
                    break;
                }
                break;
            case POWER_BUTTON_LONG_PRESS:
                this.clearConnectedTimeout();
                DialManagerView.sLogger.v("pwrbtn long press in connected mode");
                DialManager.getInstance().forgetAllDials(true, new DialManagerHelper.IDialForgotten() {
                    @Override
                    public void onForgotten() {
                        DialManagerView.sLogger.v("all forgotten");
                        DialManagerView.this.presenter.getHandler().post((Runnable)new Runnable() {
                            @Override
                            public void run() {
                                DialManagerView.this.presenter.updateView(null);
                            }
                        });
                    }
                });
                break;
        }
        return true;
    }
    
    @Subscribe
    public void onShowToast(final ToastManager.ShowToast showToast) {
        DialManagerView.sLogger.v("got toast:" + showToast.name);
        if (TextUtils.equals((CharSequence)showToast.name, (CharSequence)"dial-connect") && this.presenter != null) {
            this.presenter.dismiss();
        }
    }
    
    public void showDialConnected() {
        this.dialName = DialNotification.getDialName();
        this.batteryLevel = DialManager.getInstance().getLastKnownBatteryLevel();
        this.incrementalVersion = DialManager.getInstance().getDialFirmwareUpdater().getVersions().dial.incrementalVersion;
        this.setState(5);
    }
    
    public void showDialPairing() {
        this.setState(1);
    }
    
    public void showDialSearching(final String dialName) {
        this.dialName = dialName;
        this.setState(2);
    }
}
