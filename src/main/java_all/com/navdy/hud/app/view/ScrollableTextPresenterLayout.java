package com.navdy.hud.app.view;

import com.navdy.service.library.events.input.GestureEvent;
import java.util.List;
import butterknife.ButterKnife;
import android.animation.AnimatorSet;
import android.animation.Animator.AnimatorListener;
import android.animation.Animator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import android.graphics.RectF;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import mortar.Mortar;
import android.util.AttributeSet;
import android.content.Context;
import java.util.ArrayList;
import javax.inject.Inject;
import com.navdy.hud.app.screen.GestureLearningScreen;
import com.navdy.hud.app.ui.component.carousel.ProgressIndicator;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator;
import android.widget.TextView;
import android.widget.ImageView;
import butterknife.InjectView;
import android.view.View;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import android.widget.RelativeLayout;

public class ScrollableTextPresenterLayout extends RelativeLayout implements IListener, IScrollListener, IInputHandler
{
    public static final int TAG_BACK = 0;
    @InjectView(R.id.bottomScrub)
    View bottomScrub;
    @InjectView(R.id.choiceLayout)
    ChoiceLayout mChoiceLayout;
    boolean mContentNeedsScrolling;
    private boolean mContentReachedBottom;
    private boolean mContentReachedTop;
    private int mCount;
    private int mCurrentItem;
    @InjectView(R.id.mainImage)
    ImageView mMainImageView;
    @InjectView(R.id.mainTitle)
    TextView mMainTitleText;
    @InjectView(R.id.message)
    TextView mMessageText;
    @InjectView(R.id.notifIndicator)
    CarouselIndicator mNotificationIndicator;
    @InjectView(R.id.notifScrollIndicator)
    ProgressIndicator mNotificationScrollIndicator;
    @InjectView(R.id.scrollView)
    ObservableScrollView mObservableScrollView;
    @Inject
    GestureLearningScreen.Presenter mPresenter;
    ArrayList<CharSequence> mTextContents;
    @InjectView(R.id.title)
    TextView mTitleText;
    private int scrollColor;
    @InjectView(R.id.topScrub)
    View topScrub;
    
    public ScrollableTextPresenterLayout(final Context context) {
        this(context, null);
    }
    
    public ScrollableTextPresenterLayout(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public ScrollableTextPresenterLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mTextContents = new ArrayList<CharSequence>();
        this.mContentNeedsScrolling = false;
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
    }
    
    private void displayScrollIndicator(final boolean b) {
        if (this.mCount > 0) {
            this.mNotificationIndicator.setVisibility(View.VISIBLE);
        }
        this.mNotificationIndicator.setCurrentItem(this.mCurrentItem, this.scrollColor);
        if (this.mContentNeedsScrolling) {
            if (b) {
                this.mNotificationIndicator.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)new ViewTreeObserver.OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        ScrollableTextPresenterLayout.this.layoutScrollIndicator();
                        ScrollableTextPresenterLayout.this.mNotificationIndicator.getViewTreeObserver().removeOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)this);
                    }
                });
            }
            else {
                this.layoutScrollIndicator();
            }
        }
        else {
            ((View)this.mNotificationScrollIndicator.getParent()).setVisibility(GONE);
        }
    }
    
    private void handleLeft() {
        boolean b = false;
        if (this.mContentNeedsScrolling) {
            if (this.mObservableScrollView.arrowScroll(33) && !this.mContentReachedTop) {
                b = true;
            }
            else {
                b = false;
            }
        }
        if (!b && this.mCurrentItem > 0) {
            this.setCurrentItem(this.mCurrentItem - 1, true, true);
        }
    }
    
    private void handleRight() {
        boolean b = false;
        if (this.mContentNeedsScrolling) {
            if (this.mObservableScrollView.arrowScroll(130) && !this.mContentReachedBottom) {
                b = true;
            }
            else {
                b = false;
            }
        }
        if (!b && this.mCurrentItem < this.mCount - 1) {
            this.setCurrentItem(this.mCurrentItem + 1, true, false);
        }
    }
    
    private void layoutScrollIndicator() {
        final RectF itemPos = this.mNotificationIndicator.getItemPos(this.mCurrentItem);
        if (itemPos != null && (itemPos.left != 0.0f || itemPos.top != 0.0f)) {
            this.mNotificationScrollIndicator.setCurrentItem(1);
            final View view = (View)this.mNotificationScrollIndicator.getParent();
            view.setX(this.mNotificationIndicator.getX() + GlanceConstants.scrollingIndicatorLeftPadding);
            view.setY(this.mNotificationIndicator.getY() + itemPos.top + GlanceConstants.scrollingIndicatorCircleSize / 2 - GlanceConstants.scrollingIndicatorHeight / 2);
            view.setVisibility(View.VISIBLE);
        }
    }
    
    private void setCurrentItem(final int mCurrentItem, final boolean b, final boolean b2) {
        if (mCurrentItem >= 0 && mCurrentItem < this.mCount) {
            this.mCurrentItem = mCurrentItem;
            final CharSequence text = this.mTextContents.get(this.mCurrentItem);
            this.mContentNeedsScrolling = GlanceHelper.needsScrollLayout(text.toString());
            this.mObservableScrollView.fullScroll(33);
            this.mMessageText.setText(text);
            this.mMessageText.setVisibility(View.VISIBLE);
            this.topScrub.setVisibility(GONE);
            this.bottomScrub.setVisibility(View.VISIBLE);
            this.mContentReachedTop = true;
            this.mContentReachedBottom = false;
            if (b) {
                final AnimatorSet itemMoveAnimator = this.mNotificationIndicator.getItemMoveAnimator(this.mCurrentItem, this.scrollColor);
                if (itemMoveAnimator != null) {
                    itemMoveAnimator.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
                        @Override
                        public void onAnimationEnd(final Animator animator) {
                            super.onAnimationEnd(animator);
                            ScrollableTextPresenterLayout.this.displayScrollIndicator(false);
                        }
                    });
                    itemMoveAnimator.start();
                }
                else {
                    this.displayScrollIndicator(false);
                }
            }
            else {
                this.displayScrollIndicator(true);
            }
        }
    }
    
    public void executeItem(final int n, final int n2) {
        switch (n2) {
            case 0:
                this.mPresenter.hideTips();
                break;
        }
    }
    
    public void itemSelected(final int n, final int n2) {
    }
    
    public IInputHandler nextHandler() {
        return null;
    }
    
    public void onBottom() {
        if (this.mContentNeedsScrolling) {
            this.mContentReachedTop = false;
            this.mContentReachedBottom = true;
            this.onPosChange(100);
        }
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        this.mChoiceLayout.setHighlightPersistent(true);
        this.scrollColor = this.getResources().getColor(R.color.scroll_option_color);
        this.mObservableScrollView.setScrollListener((ObservableScrollView.IScrollListener)this);
        final ArrayList<Choice> list = new ArrayList<Choice>();
        list.add(new ChoiceLayout.Choice(this.getContext().getString(R.string.back), 0));
        this.mChoiceLayout.setChoices(Mode.LABEL, list, 0, (ChoiceLayout.IListener)this);
        this.mMainImageView.setImageResource(R.drawable.icon_settings_learning_to_gesture);
        this.mMainTitleText.setText(R.string.gesture_tips_title);
        this.mNotificationScrollIndicator.setOrientation(CarouselIndicator.Orientation.VERTICAL);
        this.mNotificationScrollIndicator.setProperties(GlanceConstants.scrollingRoundSize, GlanceConstants.scrollingIndicatorProgressSize, 0, 0, GlanceConstants.colorWhite, GlanceConstants.colorWhite, true, GlanceConstants.scrollingIndicatorPadding, -1, -1);
        this.mNotificationScrollIndicator.setItemCount(100);
        this.mNotificationIndicator.setOrientation(CarouselIndicator.Orientation.VERTICAL);
        this.mNotificationScrollIndicator.setBackgroundColor(this.scrollColor);
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        boolean b = true;
        switch (customKeyEvent) {
            default:
                b = false;
                break;
            case LEFT:
                this.handleLeft();
                break;
            case RIGHT:
                this.handleRight();
                break;
            case SELECT:
                this.mChoiceLayout.executeSelectedItem(true);
                break;
        }
        return b;
    }
    
    public void onPosChange(final int n) {
        int currentItem;
        if (n <= 0) {
            currentItem = 1;
        }
        else if ((currentItem = n) > 100) {
            currentItem = 100;
        }
        if (this.mNotificationScrollIndicator.getCurrentItem() != currentItem) {
            if (currentItem == 1 || currentItem == 100) {}
            this.mNotificationScrollIndicator.setCurrentItem(currentItem);
        }
    }
    
    public void onScroll(int bottom, final int n, int height, final int n2) {
        if (this.mContentNeedsScrolling) {
            this.topScrub.setVisibility(View.VISIBLE);
            this.mContentReachedTop = false;
            this.mContentReachedBottom = false;
            bottom = this.mObservableScrollView.getChildAt(0).getBottom();
            height = this.mObservableScrollView.getHeight();
            this.onPosChange((int)(n * 100.0 / (bottom - height)));
        }
    }
    
    public void onTop() {
        if (this.mContentNeedsScrolling) {
            this.topScrub.setVisibility(GONE);
            this.mContentReachedTop = true;
            this.mContentReachedBottom = false;
            this.onPosChange(1);
        }
    }
    
    public void setTextContents(final CharSequence[] array) {
        this.mTextContents.clear();
        for (int length = array.length, i = 0; i < length; ++i) {
            this.mTextContents.add(array[i]);
        }
        this.mCount = this.mTextContents.size();
        this.mNotificationIndicator.setItemCount(this.mCount);
        this.setCurrentItem(0, false, false);
    }
    
    public void setVisibility(final int visibility) {
        super.setVisibility(visibility);
        if (visibility == 0) {
            this.setCurrentItem(this.mCurrentItem, false, false);
        }
    }
}
