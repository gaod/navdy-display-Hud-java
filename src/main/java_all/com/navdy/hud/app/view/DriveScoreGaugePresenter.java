package com.navdy.hud.app.view;

import android.view.View;
import kotlin.TypeCastException;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.os.Bundle;
import com.squareup.otto.Subscribe;
import org.jetbrains.annotations.Nullable;
import android.graphics.drawable.Drawable;
import android.content.res.Resources;
import com.navdy.hud.app.analytics.TelemetrySession;
import android.os.Looper;
import kotlin.jvm.internal.Intrinsics;
import android.widget.ImageView;
import android.os.Handler;
import com.navdy.hud.app.analytics.TelemetryDataManager;
import android.widget.TextView;
import com.navdy.hud.app.view.drawable.DriveScoreGaugeDrawable;
import android.content.Context;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u001d\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\n\u0010P\u001a\u0004\u0018\u00010QH\u0016J\b\u0010R\u001a\u00020\u001cH\u0016J\b\u0010S\u001a\u00020\u001cH\u0016J\b\u0010T\u001a\u00020\u0007H\u0014J\u0010\u0010U\u001a\u00020V2\u0006\u0010'\u001a\u00020&H\u0007J\b\u0010W\u001a\u00020VH\u0016J\u001c\u0010X\u001a\u00020V2\b\u0010Y\u001a\u0004\u0018\u00010Z2\b\u0010[\u001a\u0004\u0018\u00010\\H\u0016J\u0010\u0010]\u001a\u00020V2\u0006\u0010^\u001a\u00020\u0007H\u0016J\u0006\u0010_\u001a\u00020VJ\u0006\u0010`\u001a\u00020VJ\b\u0010a\u001a\u00020VH\u0014R\u0014\u0010\t\u001a\u00020\nX\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u001a\u0010\r\u001a\u00020\u000eX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u00020\u0016X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u0011\u0010\u001b\u001a\u00020\u001c¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u001a\u0010\u001f\u001a\u00020 X\u0086.¢\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R$\u0010'\u001a\u00020&2\u0006\u0010%\u001a\u00020&@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u0011\u0010,\u001a\u00020-¢\u0006\b\n\u0000\u001a\u0004\b.\u0010/R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b0\u00101R\u001e\u00102\u001a\u00020\u00072\u0006\u0010%\u001a\u00020\u0007@BX\u0082\u000e¢\u0006\b\n\u0000\"\u0004\b3\u00104R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b5\u00106R\u001a\u00107\u001a\u00020 X\u0086.¢\u0006\u000e\n\u0000\u001a\u0004\b8\u0010\"\"\u0004\b9\u0010$R\u0011\u0010:\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b;\u00101R\u0011\u0010<\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b=\u00101R\u0011\u0010>\u001a\u00020\u001c¢\u0006\b\n\u0000\u001a\u0004\b?\u0010\u001eR\u0011\u0010@\u001a\u00020\u001c¢\u0006\b\n\u0000\u001a\u0004\bA\u0010\u001eR\u0011\u0010B\u001a\u00020\u001c¢\u0006\b\n\u0000\u001a\u0004\bC\u0010\u001eR\u0011\u0010D\u001a\u00020\u001c¢\u0006\b\n\u0000\u001a\u0004\bE\u0010\u001eR\u0011\u0010F\u001a\u00020\u001c¢\u0006\b\n\u0000\u001a\u0004\bG\u0010\u001eR\u0011\u0010H\u001a\u00020\u001c¢\u0006\b\n\u0000\u001a\u0004\bI\u0010\u001eR\u001a\u0010J\u001a\u00020KX\u0086.¢\u0006\u000e\n\u0000\u001a\u0004\bL\u0010M\"\u0004\bN\u0010O¨\u0006b" }, d2 = { "Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;", "Lcom/navdy/hud/app/view/DashboardWidgetPresenter;", "context", "Landroid/content/Context;", "layoutId", "", "showScore", "", "(Landroid/content/Context;IZ)V", "CLEAR_WARNING_TIME_MS", "", "getCLEAR_WARNING_TIME_MS", "()J", "clearWarning", "Ljava/lang/Runnable;", "getClearWarning", "()Ljava/lang/Runnable;", "setClearWarning", "(Ljava/lang/Runnable;)V", "getContext", "()Landroid/content/Context;", "driveScoreGaugeDrawable", "Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;", "getDriveScoreGaugeDrawable", "()Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;", "setDriveScoreGaugeDrawable", "(Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;)V", "driveScoreGaugeName", "", "getDriveScoreGaugeName", "()Ljava/lang/String;", "driveScoreText", "Landroid/widget/TextView;", "getDriveScoreText", "()Landroid/widget/TextView;", "setDriveScoreText", "(Landroid/widget/TextView;)V", "value", "Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;", "driveScoreUpdated", "getDriveScoreUpdated", "()Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;", "setDriveScoreUpdated", "(Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;)V", "handler", "Landroid/os/Handler;", "getHandler", "()Landroid/os/Handler;", "getLayoutId", "()I", "newEvent", "setNewEvent", "(Z)V", "getShowScore", "()Z", "summaryText", "getSummaryText", "setSummaryText", "summaryTextColorNormal", "getSummaryTextColorNormal", "summaryTextColorWarning", "getSummaryTextColorWarning", "summaryTextDriveScore", "getSummaryTextDriveScore", "summaryTextExcessiveSpeeding", "getSummaryTextExcessiveSpeeding", "summaryTextHardAcceleration", "getSummaryTextHardAcceleration", "summaryTextHardBraking", "getSummaryTextHardBraking", "summaryTextHighG", "getSummaryTextHighG", "summaryTextSpeeding", "getSummaryTextSpeeding", "warningImage", "Landroid/widget/ImageView;", "getWarningImage", "()Landroid/widget/ImageView;", "setWarningImage", "(Landroid/widget/ImageView;)V", "getDrawable", "Landroid/graphics/drawable/Drawable;", "getWidgetIdentifier", "getWidgetName", "isRegisteringToBusRequired", "onDriveScoreUpdated", "", "onResume", "setView", "dashboardWidgetView", "Lcom/navdy/hud/app/view/DashboardWidgetView;", "arguments", "Landroid/os/Bundle;", "setWidgetVisibleToUser", "b", "showHighG", "showSpeeding", "updateGauge", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class DriveScoreGaugePresenter extends DashboardWidgetPresenter
{
    private final long CLEAR_WARNING_TIME_MS;
    @NotNull
    private Runnable clearWarning;
    @NotNull
    private final Context context;
    @NotNull
    private DriveScoreGaugeDrawable driveScoreGaugeDrawable;
    @NotNull
    private final String driveScoreGaugeName;
    @NotNull
    public TextView driveScoreText;
    @NotNull
    private TelemetryDataManager.DriveScoreUpdated driveScoreUpdated;
    @NotNull
    private final Handler handler;
    private final int layoutId;
    private boolean newEvent;
    private final boolean showScore;
    @NotNull
    public TextView summaryText;
    private final int summaryTextColorNormal;
    private final int summaryTextColorWarning;
    @NotNull
    private final String summaryTextDriveScore;
    @NotNull
    private final String summaryTextExcessiveSpeeding;
    @NotNull
    private final String summaryTextHardAcceleration;
    @NotNull
    private final String summaryTextHardBraking;
    @NotNull
    private final String summaryTextHighG;
    @NotNull
    private final String summaryTextSpeeding;
    @NotNull
    public ImageView warningImage;
    
    public DriveScoreGaugePresenter(@NotNull final Context context, final int layoutId, final boolean showScore) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.context = context;
        this.layoutId = layoutId;
        this.showScore = showScore;
        this.CLEAR_WARNING_TIME_MS = 2000L;
        this.driveScoreGaugeDrawable = new DriveScoreGaugeDrawable(this.context, R.array.drive_state_colors);
        final DriveScoreGaugeDrawable driveScoreGaugeDrawable = this.driveScoreGaugeDrawable;
        if (driveScoreGaugeDrawable != null) {
            driveScoreGaugeDrawable.setMinValue(0.0f);
        }
        final DriveScoreGaugeDrawable driveScoreGaugeDrawable2 = this.driveScoreGaugeDrawable;
        if (driveScoreGaugeDrawable2 != null) {
            driveScoreGaugeDrawable2.setMaxGaugeValue(100.0f);
        }
        this.handler = new Handler(Looper.getMainLooper());
        Resources resources = this.context.getResources();
        String string = resources.getString(R.string.drive_score);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.string.drive_score)");
        this.summaryTextDriveScore = string;
        string = resources.getString(R.string.hard_accel);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.string.hard_accel)");
        this.summaryTextHardAcceleration = string;
        string = resources.getString(R.string.hard_braking);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.string.hard_braking)");
        this.summaryTextHardBraking = string;
        string = resources.getString(R.string.speeding);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.string.speeding)");
        this.summaryTextSpeeding = string;
        string = resources.getString(R.string.widget_drive_score);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.string.widget_drive_score)");
        this.driveScoreGaugeName = string;
        string = resources.getString(R.string.excessive_speeding);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.string.excessive_speeding)");
        this.summaryTextExcessiveSpeeding = string;
        string = resources.getString(R.string.high_g);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.string.high_g)");
        this.summaryTextHighG = string;
        this.summaryTextColorNormal = resources.getColor(R.color.hud_white);
        this.summaryTextColorWarning = resources.getColor(R.color.driving_score_warning_color);
        this.clearWarning = (Runnable)new DriveScoreGaugePresenter$clearWarning.DriveScoreGaugePresenter$clearWarning$1(this);
        this.driveScoreUpdated = new TelemetryDataManager.DriveScoreUpdated(TelemetrySession.InterestingEvent.NONE, false, false, false, 100);
    }
    
    private final void setNewEvent(final boolean newEvent) {
        this.newEvent = newEvent;
    }
    
    public final long getCLEAR_WARNING_TIME_MS() {
        return this.CLEAR_WARNING_TIME_MS;
    }
    
    @NotNull
    public final Runnable getClearWarning() {
        return this.clearWarning;
    }
    
    @NotNull
    public final Context getContext() {
        return this.context;
    }
    
    @Nullable
    @Override
    public Drawable getDrawable() {
        Drawable drawable;
        if (this.showScore) {
            drawable = this.driveScoreGaugeDrawable;
        }
        else {
            drawable = null;
        }
        return drawable;
    }
    
    @NotNull
    public final DriveScoreGaugeDrawable getDriveScoreGaugeDrawable() {
        return this.driveScoreGaugeDrawable;
    }
    
    @NotNull
    public final String getDriveScoreGaugeName() {
        return this.driveScoreGaugeName;
    }
    
    @NotNull
    public final TextView getDriveScoreText() {
        final TextView driveScoreText = this.driveScoreText;
        if (driveScoreText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
        }
        return driveScoreText;
    }
    
    @NotNull
    public final TelemetryDataManager.DriveScoreUpdated getDriveScoreUpdated() {
        return this.driveScoreUpdated;
    }
    
    @NotNull
    public final Handler getHandler() {
        return this.handler;
    }
    
    public final int getLayoutId() {
        return this.layoutId;
    }
    
    public final boolean getShowScore() {
        return this.showScore;
    }
    
    @NotNull
    public final TextView getSummaryText() {
        final TextView summaryText = this.summaryText;
        if (summaryText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("summaryText");
        }
        return summaryText;
    }
    
    public final int getSummaryTextColorNormal() {
        return this.summaryTextColorNormal;
    }
    
    public final int getSummaryTextColorWarning() {
        return this.summaryTextColorWarning;
    }
    
    @NotNull
    public final String getSummaryTextDriveScore() {
        return this.summaryTextDriveScore;
    }
    
    @NotNull
    public final String getSummaryTextExcessiveSpeeding() {
        return this.summaryTextExcessiveSpeeding;
    }
    
    @NotNull
    public final String getSummaryTextHardAcceleration() {
        return this.summaryTextHardAcceleration;
    }
    
    @NotNull
    public final String getSummaryTextHardBraking() {
        return this.summaryTextHardBraking;
    }
    
    @NotNull
    public final String getSummaryTextHighG() {
        return this.summaryTextHighG;
    }
    
    @NotNull
    public final String getSummaryTextSpeeding() {
        return this.summaryTextSpeeding;
    }
    
    @NotNull
    public final ImageView getWarningImage() {
        final ImageView warningImage = this.warningImage;
        if (warningImage == null) {
            Intrinsics.throwUninitializedPropertyAccessException("warningImage");
        }
        return warningImage;
    }
    
    @NotNull
    @Override
    public String getWidgetIdentifier() {
        return "DRIVE_SCORE_GAUGE_ID";
    }
    
    @NotNull
    @Override
    public String getWidgetName() {
        return this.driveScoreGaugeName;
    }
    
    @Override
    protected boolean isRegisteringToBusRequired() {
        return true;
    }
    
    @Subscribe
    public final void onDriveScoreUpdated(@NotNull final TelemetryDataManager.DriveScoreUpdated driveScoreUpdated) {
        Intrinsics.checkParameterIsNotNull(driveScoreUpdated, "driveScoreUpdated");
        this.setDriveScoreUpdated(driveScoreUpdated);
    }
    
    @Override
    public void onResume() {
        this.setNewEvent(false);
        super.onResume();
    }
    
    public final void setClearWarning(@NotNull final Runnable clearWarning) {
        Intrinsics.checkParameterIsNotNull(clearWarning, "set");
        this.clearWarning = clearWarning;
    }
    
    public final void setDriveScoreGaugeDrawable(@NotNull final DriveScoreGaugeDrawable driveScoreGaugeDrawable) {
        Intrinsics.checkParameterIsNotNull(driveScoreGaugeDrawable, "set");
        this.driveScoreGaugeDrawable = driveScoreGaugeDrawable;
    }
    
    public final void setDriveScoreText(@NotNull final TextView driveScoreText) {
        Intrinsics.checkParameterIsNotNull(driveScoreText, "set");
        this.driveScoreText = driveScoreText;
    }
    
    public final void setDriveScoreUpdated(@NotNull final TelemetryDataManager.DriveScoreUpdated driveScoreUpdated) {
        Intrinsics.checkParameterIsNotNull(driveScoreUpdated, "value");
        this.driveScoreUpdated = driveScoreUpdated;
        if (Intrinsics.areEqual(driveScoreUpdated.getInterestingEvent(), TelemetrySession.InterestingEvent.NONE) ^ true) {
            this.setNewEvent(true);
        }
        this.reDraw();
    }
    
    public final void setSummaryText(@NotNull final TextView summaryText) {
        Intrinsics.checkParameterIsNotNull(summaryText, "set");
        this.summaryText = summaryText;
    }
    
    @Override
    public void setView(@Nullable final DashboardWidgetView dashboardWidgetView, @Nullable final Bundle bundle) {
        final int layoutId = this.layoutId;
        if (dashboardWidgetView != null) {
            this.setDriveScoreUpdated(RemoteDeviceManager.getInstance().getTelemetryDataManager().getDriveScoreUpdatedEvent());
            this.setNewEvent(false);
            dashboardWidgetView.setContentView(layoutId);
            if (this.showScore) {
                final View viewById = dashboardWidgetView.findViewById(R.id.txt_value);
                if (viewById == null) {
                    throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
                }
                this.driveScoreText = (TextView)viewById;
            }
            final View viewById2 = dashboardWidgetView.findViewById(R.id.txt_summary);
            if (viewById2 == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
            }
            this.summaryText = (TextView)viewById2;
            final View viewById3 = dashboardWidgetView.findViewById(R.id.img_warning);
            if (viewById3 == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.widget.ImageView");
            }
            this.warningImage = (ImageView)viewById3;
        }
        super.setView(dashboardWidgetView, bundle);
    }
    
    public final void setWarningImage(@NotNull final ImageView warningImage) {
        Intrinsics.checkParameterIsNotNull(warningImage, "set");
        this.warningImage = warningImage;
    }
    
    @Override
    public void setWidgetVisibleToUser(final boolean widgetVisibleToUser) {
        this.setNewEvent(false);
        super.setWidgetVisibleToUser(widgetVisibleToUser);
    }
    
    public final void showHighG() {
        if (this.showScore) {
            this.driveScoreGaugeDrawable.setState(1);
            final TextView driveScoreText = this.driveScoreText;
            if (driveScoreText == null) {
                Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
            }
            driveScoreText.setVisibility(INVISIBLE);
        }
        final ImageView warningImage = this.warningImage;
        if (warningImage == null) {
            Intrinsics.throwUninitializedPropertyAccessException("warningImage");
        }
        warningImage.setVisibility(View.VISIBLE);
        final TextView summaryText = this.summaryText;
        if (summaryText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("summaryText");
        }
        summaryText.setTextColor(this.summaryTextColorWarning);
        final ImageView warningImage2 = this.warningImage;
        if (warningImage2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("warningImage");
        }
        warningImage2.setImageResource(R.drawable.high_g);
        final TextView summaryText2 = this.summaryText;
        if (summaryText2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("summaryText");
        }
        summaryText2.setText((CharSequence)this.summaryTextHighG);
    }
    
    public final void showSpeeding() {
        if (this.showScore) {
            this.driveScoreGaugeDrawable.setState(1);
            final TextView driveScoreText = this.driveScoreText;
            if (driveScoreText == null) {
                Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
            }
            driveScoreText.setVisibility(INVISIBLE);
        }
        final ImageView warningImage = this.warningImage;
        if (warningImage == null) {
            Intrinsics.throwUninitializedPropertyAccessException("warningImage");
        }
        warningImage.setVisibility(View.VISIBLE);
        final TextView summaryText = this.summaryText;
        if (summaryText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("summaryText");
        }
        summaryText.setTextColor(this.summaryTextColorWarning);
        if (this.driveScoreUpdated.isExcessiveSpeeding()) {
            final ImageView warningImage2 = this.warningImage;
            if (warningImage2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("warningImage");
            }
            warningImage2.setImageResource(R.drawable.excessive_speed);
            final TextView summaryText2 = this.summaryText;
            if (summaryText2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("summaryText");
            }
            summaryText2.setText((CharSequence)this.summaryTextExcessiveSpeeding);
        }
        else {
            final ImageView warningImage3 = this.warningImage;
            if (warningImage3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("warningImage");
            }
            warningImage3.setImageResource(R.drawable.speed);
            final TextView summaryText3 = this.summaryText;
            if (summaryText3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("summaryText");
            }
            summaryText3.setText((CharSequence)this.summaryTextSpeeding);
        }
    }
    
    @Override
    protected void updateGauge() {
        this.logger.d("Update Drive score " + this.driveScoreUpdated);
        if (this.mWidgetView != null) {
            if (this.showScore) {
                this.driveScoreGaugeDrawable.setGaugeValue(this.driveScoreUpdated.getDriveScore());
            }
            if (this.newEvent) {
                if (this.showScore) {
                    this.driveScoreGaugeDrawable.setState(1);
                    final TextView driveScoreText = this.driveScoreText;
                    if (driveScoreText == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
                    }
                    driveScoreText.setVisibility(INVISIBLE);
                }
                final ImageView warningImage = this.warningImage;
                if (warningImage == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("warningImage");
                }
                warningImage.setVisibility(View.VISIBLE);
                final TextView summaryText = this.summaryText;
                if (summaryText == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                }
                summaryText.setTextColor(this.summaryTextColorWarning);
                if (Intrinsics.areEqual(this.driveScoreUpdated.getInterestingEvent(), TelemetrySession.InterestingEvent.HARD_ACCELERATION)) {
                    final ImageView warningImage2 = this.warningImage;
                    if (warningImage2 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("warningImage");
                    }
                    warningImage2.setImageResource(R.drawable.accel);
                    final TextView summaryText2 = this.summaryText;
                    if (summaryText2 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                    }
                    summaryText2.setText((CharSequence)this.summaryTextHardAcceleration);
                }
                else if (Intrinsics.areEqual(this.driveScoreUpdated.getInterestingEvent(), TelemetrySession.InterestingEvent.HARD_BRAKING)) {
                    final ImageView warningImage3 = this.warningImage;
                    if (warningImage3 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("warningImage");
                    }
                    warningImage3.setImageResource(R.drawable.brake);
                    final TextView summaryText3 = this.summaryText;
                    if (summaryText3 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                    }
                    summaryText3.setText((CharSequence)this.summaryTextHardBraking);
                }
                else if (this.driveScoreUpdated.isDoingHighGManeuver()) {
                    this.showHighG();
                }
                else if (this.driveScoreUpdated.isSpeeding()) {
                    this.showSpeeding();
                }
                this.handler.removeCallbacks(this.clearWarning);
                this.handler.postDelayed(this.clearWarning, this.CLEAR_WARNING_TIME_MS);
            }
            else if (this.driveScoreUpdated.isDoingHighGManeuver()) {
                this.showHighG();
            }
            else if (this.driveScoreUpdated.isSpeeding()) {
                this.showSpeeding();
            }
            else {
                this.handler.removeCallbacks(this.clearWarning);
                if (this.showScore) {
                    this.driveScoreGaugeDrawable.setState(0);
                    final TextView driveScoreText2 = this.driveScoreText;
                    if (driveScoreText2 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
                    }
                    driveScoreText2.setVisibility(View.VISIBLE);
                    final TextView driveScoreText3 = this.driveScoreText;
                    if (driveScoreText3 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
                    }
                    driveScoreText3.setText((CharSequence)String.valueOf(this.driveScoreUpdated.getDriveScore()));
                    final TextView summaryText4 = this.summaryText;
                    if (summaryText4 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                    }
                    summaryText4.setTextColor(this.summaryTextColorNormal);
                    final TextView summaryText5 = this.summaryText;
                    if (summaryText5 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                    }
                    summaryText5.setText((CharSequence)this.summaryTextDriveScore);
                }
                else {
                    final TextView summaryText6 = this.summaryText;
                    if (summaryText6 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                    }
                    summaryText6.setText((CharSequence)null);
                }
                final ImageView warningImage4 = this.warningImage;
                if (warningImage4 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("warningImage");
                }
                warningImage4.setVisibility(INVISIBLE);
            }
        }
    }
}
