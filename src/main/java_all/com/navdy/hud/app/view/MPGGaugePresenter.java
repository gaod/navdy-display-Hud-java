package com.navdy.hud.app.view;

import com.navdy.hud.app.util.ConversionUtil;
import android.view.View;
import butterknife.ButterKnife;
import android.os.Bundle;
import com.navdy.hud.app.manager.SpeedManager;
import android.graphics.drawable.Drawable;
import android.content.res.Resources;
import java.util.concurrent.TimeUnit;
import android.os.Handler;
import butterknife.InjectView;
import android.widget.TextView;
import android.content.Context;

public class MPGGaugePresenter extends DashboardWidgetPresenter
{
    private static final int GAUGE_UPDATE_INTERVAL = 1000;
    private static final long MPG_AVERAGE_TIME_WINDOW;
    private Context mContext;
    @InjectView(R.id.txt_unit)
    TextView mFuelConsumptionUnit;
    @InjectView(R.id.txt_value)
    TextView mFuelConsumptionValue;
    private Handler mHandler;
    private String mKmplLabel;
    private double mMpg;
    private String mMpgLabel;
    private Runnable mRunnable;
    
    static {
        MPG_AVERAGE_TIME_WINDOW = TimeUnit.MINUTES.toMillis(5L);
    }
    
    public MPGGaugePresenter(final Context mContext) {
        this.mContext = mContext;
        final Resources resources = mContext.getResources();
        this.mMpgLabel = resources.getString(R.string.mpg);
        this.mKmplLabel = resources.getString(R.string.kmpl);
        this.mHandler = new Handler();
        this.mRunnable = new Runnable() {
            @Override
            public void run() {
                MPGGaugePresenter.this.reDraw();
                MPGGaugePresenter.this.mHandler.postDelayed(MPGGaugePresenter.this.mRunnable, 1000L);
            }
        };
    }
    
    @Override
    public Drawable getDrawable() {
        return null;
    }
    
    @Override
    public String getWidgetIdentifier() {
        return "MPG_AVG_WIDGET";
    }
    
    @Override
    public String getWidgetName() {
        String s = null;
        switch (SpeedManager.getInstance().getSpeedUnit()) {
            default:
                s = null;
                break;
            case KILOMETERS_PER_HOUR:
                s = this.mKmplLabel;
                break;
            case MILES_PER_HOUR:
                s = this.mMpgLabel;
                break;
        }
        return s;
    }
    
    public void setCurrentMPG(final double mMpg) {
        this.mMpg = mMpg;
        this.reDraw();
    }
    
    @Override
    public void setView(final DashboardWidgetView dashboardWidgetView, final Bundle bundle) {
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView(R.layout.fuel_consumption_gauge);
            ButterKnife.inject(this, (View)dashboardWidgetView);
            this.mHandler.removeCallbacks(this.mRunnable);
            this.mHandler.post(this.mRunnable);
        }
        else {
            this.mHandler.removeCallbacks(this.mRunnable);
        }
        super.setView(dashboardWidgetView, bundle);
    }
    
    @Override
    protected void updateGauge() {
        if (this.mWidgetView != null) {
            final SpeedManager.SpeedUnit speedUnit = SpeedManager.getInstance().getSpeedUnit();
            long n = 0L;
            switch (speedUnit) {
                case KILOMETERS_PER_HOUR:
                    n = Math.round(ConversionUtil.convertLpHundredKmToKMPL(this.mMpg));
                    this.mFuelConsumptionUnit.setText((CharSequence)this.mKmplLabel);
                    break;
                case MILES_PER_HOUR:
                    n = Math.round(ConversionUtil.convertLpHundredKmToMPG(this.mMpg));
                    this.mFuelConsumptionUnit.setText((CharSequence)this.mMpgLabel);
                    break;
            }
            if (n > 0L) {
                this.mFuelConsumptionValue.setText((CharSequence)Long.toString(n));
            }
            else {
                this.mFuelConsumptionValue.setText((CharSequence)"- -");
            }
        }
    }
}
