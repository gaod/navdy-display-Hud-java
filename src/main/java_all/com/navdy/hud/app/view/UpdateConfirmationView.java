package com.navdy.hud.app.view;

import com.navdy.service.library.events.input.Gesture;
import com.navdy.service.library.events.input.GestureEvent;
import android.annotation.SuppressLint;
import android.content.res.Resources;
import java.util.List;
import java.util.ArrayList;
import com.navdy.hud.app.util.ViewUtil;
import android.view.View;
import butterknife.ButterKnife;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import mortar.Mortar;
import android.util.AttributeSet;
import android.content.Context;
import javax.inject.Inject;
import com.navdy.hud.app.screen.OSUpdateConfirmationScreen;
import android.widget.TextView;
import android.widget.ImageView;
import butterknife.InjectView;
import android.os.Handler;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import android.widget.RelativeLayout;

public class UpdateConfirmationView extends RelativeLayout implements IListener, IInputHandler
{
    private static final long CONFIRMATION_TIMEOUT = 30000L;
    private static final int UPDATE_MESSAGE_MAX_WIDTH = 360;
    private static final Logger sLogger;
    private Handler handler;
    private boolean isReminder;
    @InjectView(R.id.choiceLayout)
    ChoiceLayout mChoiceLayout;
    @InjectView(R.id.image)
    ImageView mIcon;
    @InjectView(R.id.title3)
    TextView mInfoText;
    @InjectView(R.id.leftSwipe)
    ImageView mLefttSwipe;
    @InjectView(R.id.title2)
    TextView mMainTitleText;
    @Inject
    OSUpdateConfirmationScreen.Presenter mPresenter;
    @InjectView(R.id.rightSwipe)
    ImageView mRightSwipe;
    @InjectView(R.id.mainTitle)
    TextView mScreenTitleText;
    @InjectView(R.id.title4)
    TextView mVersionText;
    @InjectView(R.id.mainSection)
    RelativeLayout mainSection;
    private Runnable timeout;
    private String updateVersion;
    
    static {
        sLogger = new Logger(UpdateConfirmationView.class);
    }
    
    public UpdateConfirmationView(final Context context) {
        this(context, null, 0);
    }
    
    public UpdateConfirmationView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public UpdateConfirmationView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.handler = new Handler();
        this.timeout = new Runnable() {
            @Override
            public void run() {
                UpdateConfirmationView.sLogger.v("timedout");
                UpdateConfirmationView.this.mPresenter.finish();
            }
        };
        this.updateVersion = "1.3.2887";
        this.isReminder = false;
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
    }
    
    public void executeItem(final int n, final int n2) {
        this.handler.removeCallbacks(this.timeout);
        switch (n) {
            case 0:
                this.mPresenter.install();
                this.mPresenter.finish();
                if (this.isReminder) {
                    AnalyticsSupport.recordUpdatePrompt(true, false, this.updateVersion);
                    break;
                }
                break;
            case 1:
                this.mPresenter.finish();
                if (this.isReminder) {
                    AnalyticsSupport.recordUpdatePrompt(false, false, this.updateVersion);
                    break;
                }
                break;
        }
    }
    
    public void itemSelected(final int n, final int n2) {
    }
    
    public IInputHandler nextHandler() {
        return null;
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }
    
    protected void onDetachedFromWindow() {
        this.handler.removeCallbacks(this.timeout);
        super.onDetachedFromWindow();
        ToastManager.getInstance().disableToasts(false);
        NotificationManager.getInstance().enableNotifications(true);
        if (this.mPresenter != null) {
            this.mPresenter.dropView(this);
        }
    }
    
    @SuppressLint({ "StringFormatMatches" })
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        final Resources resources = this.getContext().getResources();
        String currentVersion = "1.2.2884";
        if (this.mPresenter != null) {
            this.mPresenter.takeView(this);
            ToastManager.getInstance().disableToasts(true);
            NotificationManager.getInstance().enableNotifications(false);
            this.updateVersion = this.mPresenter.getUpdateVersion();
            currentVersion = this.mPresenter.getCurrentVersion();
            this.isReminder = this.mPresenter.isReminder();
        }
        this.mVersionText.setText((CharSequence)String.format(resources.getString(R.string.update_navdy_will_upgrade_from_to), this.updateVersion, currentVersion));
        this.mVersionText.setVisibility(View.VISIBLE);
        this.mScreenTitleText.setVisibility(GONE);
        this.findViewById(R.id.title1).setVisibility(GONE);
        this.mMainTitleText.setText(R.string.update_ready_to_install);
        ViewUtil.adjustPadding((View)this.mainSection, 0, 0, 0, 10);
        ViewUtil.autosize(this.mMainTitleText, 2, 360, R.array.title_sizes);
        this.mMainTitleText.setVisibility(View.VISIBLE);
        this.mInfoText.setText(R.string.ota_update_installation_will_take);
        this.mInfoText.setVisibility(View.VISIBLE);
        this.mIcon.setImageResource(R.drawable.icon_software_update);
        ((MaxWidthLinearLayout)this.findViewById(R.id.infoContainer)).setMaxWidth(360);
        final ArrayList<Choice> list = new ArrayList<Choice>();
        if (this.isReminder) {
            list.add(new ChoiceLayout.Choice(resources.getString(R.string.install_now), 0));
            list.add(new ChoiceLayout.Choice(resources.getString(R.string.install_later), 1));
        }
        else {
            list.add(new ChoiceLayout.Choice(resources.getString(R.string.install), 0));
            list.add(new ChoiceLayout.Choice(resources.getString(R.string.cancel), 1));
        }
        this.mChoiceLayout.setChoices(Mode.LABEL, list, 1, (ChoiceLayout.IListener)this);
        this.mRightSwipe.setVisibility(View.VISIBLE);
        this.mLefttSwipe.setVisibility(View.VISIBLE);
        this.handler.removeCallbacks(this.timeout);
        this.handler.postDelayed(this.timeout, 30000L);
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        boolean b = true;
        if (gestureEvent.gesture != null) {
            switch (gestureEvent.gesture) {
                case GESTURE_SWIPE_LEFT:
                    this.executeItem(0, 0);
                    return b;
                case GESTURE_SWIPE_RIGHT:
                    this.executeItem(1, 0);
                    return b;
            }
        }
        b = false;
        return b;
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        boolean b = true;
        this.handler.removeCallbacks(this.timeout);
        if (this.mChoiceLayout != null && this.mChoiceLayout.getVisibility() == 0) {
            switch (customKeyEvent) {
                case LEFT:
                    this.mChoiceLayout.moveSelectionLeft();
                    break;
                case RIGHT:
                    this.mChoiceLayout.moveSelectionRight();
                    break;
                case SELECT:
                    this.mChoiceLayout.executeSelectedItem(true);
                    break;
            }
        }
        else {
            b = false;
        }
        return b;
    }
}
