package com.navdy.hud.app.view.drawable;

import android.graphics.Rect;
import android.graphics.Canvas;
import android.content.Context;
import android.graphics.drawable.Drawable;

public class GaugeDrawable extends CustomDrawable
{
    private Drawable mBackgroundDrawable;
    protected int[] mColorTable;
    protected int mDefaultColor;
    protected float mMaxValue;
    protected float mMinValue;
    protected float mValue;
    
    public GaugeDrawable(final Context context, final int n) {
        if (n != 0) {
            this.mBackgroundDrawable = context.getResources().getDrawable(n);
        }
        this.mPaint.setAntiAlias(true);
    }
    
    public GaugeDrawable(final Context context, final int n, final int n2) {
        this(context, n);
        this.mColorTable = context.getResources().getIntArray(n2);
        if (this.mColorTable != null && this.mColorTable.length > 0) {
            this.mDefaultColor = this.mColorTable[0];
        }
    }
    
    @Override
    public void draw(final Canvas canvas) {
        super.draw(canvas);
        if (this.mBackgroundDrawable != null) {
            this.mBackgroundDrawable.draw(canvas);
        }
    }
    
    public int getHeight() {
        final Rect bounds = this.getBounds();
        int height;
        if (bounds != null) {
            height = bounds.height();
        }
        else {
            height = 0;
        }
        return height;
    }
    
    public int getWidth() {
        final Rect bounds = this.getBounds();
        int width;
        if (bounds != null) {
            width = bounds.width();
        }
        else {
            width = 0;
        }
        return width;
    }
    
    public void setBounds(final int n, final int n2, final int n3, final int n4) {
        super.setBounds(n, n2, n3, n4);
        if (this.mBackgroundDrawable != null) {
            this.mBackgroundDrawable.setBounds(n, n2, n3, n4);
        }
    }
    
    public void setGaugeValue(final float mValue) {
        this.mValue = mValue;
        if (mValue > this.mMaxValue) {
            this.mValue = this.mMaxValue;
        }
        else if (mValue < this.mMinValue) {
            this.mValue = this.mMinValue;
        }
    }
    
    public void setMaxGaugeValue(final float mMaxValue) {
        this.mMaxValue = mMaxValue;
    }
    
    public void setMinValue(final float mMinValue) {
        this.mMinValue = mMinValue;
    }
    
    public void setState(final int n) {
        if (this.mColorTable != null && n < this.mColorTable.length && n >= 0) {
            this.mDefaultColor = this.mColorTable[n];
            return;
        }
        throw new IllegalArgumentException();
    }
}
