package com.navdy.hud.app.view;

import android.view.View;
import butterknife.ButterKnife;
import android.util.AttributeSet;
import android.content.Context;
import com.navdy.hud.app.manager.SpeedManager;
import android.widget.TextView;
import butterknife.InjectView;
import android.view.ViewGroup;
import android.widget.FrameLayout;

public class SpeedLimitSignView extends FrameLayout
{
    @InjectView(R.id.speed_limit_sign_eu)
    protected ViewGroup euSpeedLimitSignView;
    private int speedLimit;
    private TextView speedLimitTextView;
    private SpeedManager.SpeedUnit speedLimitUnit;
    @InjectView(R.id.speed_limit_sign_us)
    protected ViewGroup usSpeedLimitSignView;
    
    public SpeedLimitSignView(final Context context) {
        this(context, null);
    }
    
    public SpeedLimitSignView(final Context context, final AttributeSet set) {
        this(context, set, -1);
    }
    
    public SpeedLimitSignView(final Context context, final AttributeSet set, final int n) {
        this(context, set, n, -1);
    }
    
    public SpeedLimitSignView(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.speedLimitUnit = null;
        inflate(this.getContext(), R.layout.speed_sign_eu, (ViewGroup)this);
        inflate(this.getContext(), R.layout.speed_limit_sign_us, (ViewGroup)this);
        ButterKnife.inject(this, (View)this);
    }
    
    public SpeedManager.SpeedUnit getSpeedLimitUnit() {
        return this.speedLimitUnit;
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.setSpeedLimitUnit(this.speedLimitUnit);
    }
    
    public void setSpeedLimit(final int speedLimit) {
        if (this.speedLimit != speedLimit) {
            this.speedLimit = speedLimit;
            if (this.speedLimitTextView != null) {
                this.speedLimitTextView.setText((CharSequence)Integer.toString(speedLimit));
            }
        }
    }
    
    public void setSpeedLimitUnit(final SpeedManager.SpeedUnit speedLimitUnit) {
        if (this.speedLimitUnit != speedLimitUnit) {
            this.speedLimitUnit = speedLimitUnit;
            ViewGroup viewGroup = null;
            switch (this.speedLimitUnit) {
                case MILES_PER_HOUR:
                    viewGroup = this.usSpeedLimitSignView;
                    this.euSpeedLimitSignView.setVisibility(GONE);
                    break;
                case KILOMETERS_PER_HOUR:
                    viewGroup = this.euSpeedLimitSignView;
                    this.usSpeedLimitSignView.setVisibility(GONE);
                    break;
            }
            viewGroup.setVisibility(View.VISIBLE);
            this.speedLimitTextView = (TextView)viewGroup.findViewById(R.id.txt_speed_limit);
            this.setSpeedLimit(this.speedLimit);
        }
    }
}
