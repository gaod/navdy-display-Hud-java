package com.navdy.hud.app.view.drawable;

import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Canvas;
import android.content.res.Resources;
import android.content.Context;
import android.graphics.drawable.Drawable;

public class ETAProgressDrawable extends GaugeDrawable
{
    private int backgroundColor;
    private int foreGroundColor;
    private Drawable progressDrawable;
    private int verticalMargin;
    
    public ETAProgressDrawable(final Context context) {
        super(context, 0);
        final Resources resources = context.getResources();
        this.backgroundColor = resources.getColor(R.color.cyan);
        this.foreGroundColor = resources.getColor(R.color.grey_4a);
        this.progressDrawable = resources.getDrawable(R.drawable.trip_progress_point_indicator);
        this.verticalMargin = resources.getDimensionPixelSize(R.dimen.eta_progress_vertical_margin);
    }
    
    @Override
    public void draw(final Canvas canvas) {
        super.draw(canvas);
        final Rect bounds = this.getBounds();
        final float n = bounds.height();
        final float n2 = bounds.width();
        final Rect rect = new Rect(bounds);
        rect.inset(0, this.verticalMargin);
        this.mPaint.setColor(this.backgroundColor);
        this.mPaint.setStyle(Paint$Style.FILL);
        canvas.drawRect((float)rect.left, (float)rect.top, (float)rect.right, (float)rect.bottom, this.mPaint);
        final int n3 = (int)(n2 * (this.mValue / (this.mMaxValue - this.mMinValue)));
        this.mPaint.setColor(this.foreGroundColor);
        canvas.drawRect((float)rect.left, (float)rect.top, (float)(rect.left + n3), (float)rect.bottom, this.mPaint);
        final int n4 = (int)Math.min(rect.left + n3, n2 - n);
        this.progressDrawable.setBounds(n4, bounds.top, (int)(n4 + n), bounds.bottom);
        this.progressDrawable.draw(canvas);
    }
}
