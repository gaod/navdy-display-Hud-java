package com.navdy.hud.app.view.drawable;

import android.graphics.Rect;
import com.navdy.hud.app.util.DateUtil;
import android.graphics.Paint;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Canvas;
import android.content.Context;

public class SmallAnalogClockDrawable extends CustomDrawable
{
    private int frameColor;
    private int frameStrokeWidth;
    private int hour;
    private int hourHandColor;
    private int hourHandStrokeWidth;
    private int minute;
    private int minuteHandColor;
    private int minuteHandStrokeWidth;
    private int seconds;
    
    public SmallAnalogClockDrawable(final Context context) {
        this.frameColor = context.getResources().getColor(R.color.small_analog_clock_frame_color);
        this.frameStrokeWidth = context.getResources().getDimensionPixelSize(R.dimen.small_analog_clock_frame_stroke_width);
        this.hourHandColor = context.getResources().getColor(R.color.cyan);
        this.hourHandStrokeWidth = context.getResources().getDimensionPixelSize(R.dimen.small_analog_clock_hour_hand_width);
        this.minuteHandStrokeWidth = context.getResources().getDimensionPixelSize(R.dimen.small_analog_clock_minute_hand_width);
        this.minuteHandColor = -1;
    }
    
    @Override
    public void draw(final Canvas canvas) {
        super.draw(canvas);
        final Rect bounds = this.getBounds();
        new RectF(bounds);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStyle(Paint$Style.STROKE);
        this.mPaint.setStrokeWidth((float)this.frameStrokeWidth);
        this.mPaint.setColor(this.frameColor);
        final RectF rectF = new RectF(bounds);
        rectF.inset(this.frameStrokeWidth / 2 + 0.5f, this.frameStrokeWidth / 2 + 0.5f);
        canvas.drawArc(rectF, 0.0f, 360.0f, true, this.mPaint);
        this.mPaint.setStyle(Paint$Style.STROKE);
        this.mPaint.setColor(this.hourHandColor);
        this.mPaint.setStrokeCap(Paint$Cap.ROUND);
        this.mPaint.setStrokeWidth((float)this.hourHandStrokeWidth);
        final float clockAngleForHour = DateUtil.getClockAngleForHour(this.hour, this.minute);
        final float n = bounds.width() / 2 - 7;
        canvas.drawLine((float)bounds.centerX(), (float)bounds.centerY(), (float)(int)(bounds.centerX() + n * Math.cos(Math.toRadians(clockAngleForHour))), (float)(int)(bounds.centerY() + n * Math.sin(Math.toRadians(clockAngleForHour))), this.mPaint);
        this.mPaint.setStyle(Paint$Style.STROKE);
        this.mPaint.setColor(this.minuteHandColor);
        this.mPaint.setStrokeCap(Paint$Cap.ROUND);
        this.mPaint.setStrokeWidth((float)this.minuteHandStrokeWidth);
        final float clockAngleForMinutes = DateUtil.getClockAngleForMinutes(this.minute);
        final float n2 = bounds.width() / 2 - 6;
        canvas.drawLine((float)bounds.centerX(), (float)bounds.centerY(), (float)(int)(bounds.centerX() + n2 * Math.cos(Math.toRadians(clockAngleForMinutes))), (float)(int)(bounds.centerY() + n2 * Math.sin(Math.toRadians(clockAngleForMinutes))), this.mPaint);
    }
    
    public void setTime(final int hour, final int minute, final int seconds) {
        this.hour = hour;
        this.minute = minute;
        this.seconds = seconds;
    }
}
