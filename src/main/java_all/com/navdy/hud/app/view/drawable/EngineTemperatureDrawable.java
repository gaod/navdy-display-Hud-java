package com.navdy.hud.app.view.drawable;

import android.graphics.Rect;
import android.graphics.Paint;
import android.graphics.RectF;
import org.jetbrains.annotations.Nullable;
import android.graphics.Canvas;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.content.Context;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0012\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016J\u000e\u0010\u000f\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\nR\u000e\u0010\u0007\u001a\u00020\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u0011" }, d2 = { "Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;", "Lcom/navdy/hud/app/view/drawable/GaugeDrawable;", "context", "Landroid/content/Context;", "stateColorsResId", "", "(Landroid/content/Context;I)V", "mBackgroundColor", "mFuelGaugeWidth", "mLeftOriented", "", "draw", "", "canvas", "Landroid/graphics/Canvas;", "setLeftOriented", "leftOriented", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class EngineTemperatureDrawable extends GaugeDrawable
{
    private int mBackgroundColor;
    private int mFuelGaugeWidth;
    private boolean mLeftOriented;
    
    public EngineTemperatureDrawable(@NotNull final Context context, final int n) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        super(context, 0, n);
        this.mLeftOriented = true;
        this.mFuelGaugeWidth = context.getResources().getDimensionPixelSize(R.dimen.fuel_gauge_width);
        this.mBackgroundColor = this.mColorTable[3];
    }
    
    @Override
    public void draw(@Nullable final Canvas canvas) {
        final int n = 90;
        super.draw(canvas);
        final Rect bounds = this.getBounds();
        RectF rectF;
        if (this.mLeftOriented) {
            rectF = new RectF((float)bounds.left, (float)bounds.top, (float)(bounds.right + bounds.width()), (float)bounds.bottom);
        }
        else {
            rectF = new RectF((float)(bounds.left - bounds.width()), (float)bounds.top, (float)bounds.right, (float)bounds.bottom);
        }
        rectF.inset((float)(this.mFuelGaugeWidth / 2 + 1), (float)(this.mFuelGaugeWidth / 2 + 1));
        this.mPaint.setStyle(Paint$Style.STROKE);
        this.mPaint.setStrokeWidth((float)this.mFuelGaugeWidth);
        this.mPaint.setColor(this.mBackgroundColor);
        int n2;
        if (this.mLeftOriented) {
            n2 = 90;
        }
        else {
            n2 = 270;
        }
        if (canvas != null) {
            canvas.drawArc(rectF, (float)n2, (float)180, false, this.mPaint);
        }
        this.mPaint.setColor(this.mDefaultColor);
        final int n3 = (int)((this.mValue - this.mMinValue) / (this.mMaxValue - this.mMinValue) * 180);
        int n4;
        if (this.mLeftOriented) {
            n4 = n;
        }
        else {
            n4 = 450 - n3;
        }
        final boolean mLeftOriented = this.mLeftOriented;
        if (mLeftOriented) {
            if (canvas != null) {
                canvas.drawArc(rectF, (float)n4, n3 - 5.0f, false, this.mPaint);
            }
        }
        else if (!mLeftOriented && canvas != null) {
            canvas.drawArc(rectF, n4 + 5.0f, (float)n3, false, this.mPaint);
        }
        this.mPaint.setColor(-16777216);
        final boolean mLeftOriented2 = this.mLeftOriented;
        if (mLeftOriented2) {
            if (canvas != null) {
                canvas.drawArc(rectF, n3 + n4, -5.0f, false, this.mPaint);
            }
        }
        else if (!mLeftOriented2 && canvas != null) {
            canvas.drawArc(rectF, (float)n4, 5.0f, false, this.mPaint);
        }
    }
    
    public final void setLeftOriented(final boolean mLeftOriented) {
        this.mLeftOriented = mLeftOriented;
    }
}
