package com.navdy.hud.app.view;

import com.navdy.service.library.events.input.GestureEvent;
import butterknife.ButterKnife;
import com.navdy.hud.app.settings.BrightnessControl;
import com.navdy.hud.app.settings.AdaptiveBrightnessControl;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.settings.HUDSettings;
import android.animation.AnimatorSet.Builder;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import android.animation.AnimatorSet;
import android.content.res.Resources;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.util.DeviceUtil;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.animation.ObjectAnimator;
import android.animation.Animator.AnimatorListener;
import android.view.ViewGroup;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.animation.AnimatorInflater;
import android.animation.Animator;
import com.navdy.hud.app.util.os.SystemProperties;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout.LayoutParams;
import android.util.TypedValue;
import com.navdy.hud.app.HudApplication;
import android.view.WindowManager;
import android.util.DisplayMetrics;
import mortar.Mortar;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.framework.notifications.NotificationType;
import android.util.AttributeSet;
import android.content.Context;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.ui.component.SystemTrayView;
import com.navdy.hud.app.ui.framework.IScreenAnimationListener;
import com.navdy.hud.app.ui.activity.Main;
import javax.inject.Inject;
import android.content.SharedPreferences;
import com.navdy.hud.app.ui.component.image.ColorImageView;
import com.navdy.hud.app.ui.framework.INotificationAnimationListener;
import com.navdy.hud.app.ui.component.carousel.ProgressIndicator;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator;
import com.navdy.hud.app.ui.component.main.MainLowerView;
import android.os.Handler;
import com.navdy.service.library.events.preferences.DriverProfilePreferences;
import android.view.View;
import butterknife.InjectView;
import android.content.SharedPreferences;
import com.navdy.hud.app.ui.framework.AnimationQueue;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.manager.InputManager;
import android.widget.FrameLayout;

public class MainView extends FrameLayout implements IInputHandler
{
    private static final String COMPACT_MODE_SCALE_PROPERTY = "persist.sys.compact.scale";
    private static final float DEFAULT_COMPACT_MODE_SCALE = 0.87f;
    private static final int SHOW_OPTION_LAG = 350;
    private static final Logger sLogger;
    private AnimationQueue animationQueue;
    private int basePaddingTop;
    private SharedPreferences$OnSharedPreferenceChangeListener brightnessControl;
    @InjectView(R.id.container)
    ContainerView containerView;
    @InjectView(R.id.expandedNotifCoverView)
    View expandedNotificationCoverView;
    @InjectView(R.id.expandedNotifView)
    FrameLayout expandedNotificationView;
    private DriverProfilePreferences.DisplayFormat format;
    private Handler handler;
    private boolean isNotificationCollapsing;
    private boolean isNotificationExpanding;
    private boolean isScreenAnimating;
    @InjectView(R.id.mainLowerView)
    MainLowerView mainLowerView;
    private int mainPanelWidth;
    @InjectView(R.id.notifIndicator)
    CarouselIndicator notifIndicator;
    @InjectView(R.id.notifScrollIndicator)
    ProgressIndicator notifScrollIndicator;
    private INotificationAnimationListener notificationAnimationListener;
    @InjectView(R.id.notificationColorView)
    ColorImageView notificationColorView;
    @InjectView(R.id.notificationExtensionView)
    FrameLayout notificationExtensionView;
    @InjectView(R.id.notification)
    NotificationView notificationView;
    @Inject
    SharedPreferences preferences;
    @Inject
    Main.Presenter presenter;
    private int rightPanelStart;
    private IScreenAnimationListener screenAnimationListener;
    @InjectView(R.id.screenContainer)
    FrameLayout screenContainer;
    private boolean showingNotificationExtension;
    private int sidePanelWidth;
    @InjectView(R.id.splitter)
    FrameLayout splitterView;
    @InjectView(R.id.systemTray)
    SystemTrayView systemTray;
    @InjectView(R.id.toastView)
    ToastView toastView;
    @Inject
    UIStateManager uiStateManager;
    private int verticalOffset;
    
    static {
        sLogger = new Logger(MainView.class);
    }
    
    public MainView(final Context context) {
        this(context, null);
    }
    
    public MainView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public MainView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.basePaddingTop = -1;
        this.animationQueue = new AnimationQueue();
        this.handler = new Handler();
        this.notificationAnimationListener = new INotificationAnimationListener() {
            @Override
            public void onStart(final String s, final NotificationType notificationType, final UIStateManager.Mode mode) {
                if (mode == UIStateManager.Mode.EXPAND) {
                    MainView.this.isNotificationExpanding = true;
                }
                else {
                    MainView.this.isNotificationCollapsing = true;
                }
            }
            
            @Override
            public void onStop(final String s, final NotificationType notificationType, final UIStateManager.Mode mode) {
                if (mode == UIStateManager.Mode.EXPAND) {
                    MainView.this.isNotificationExpanding = false;
                }
                else {
                    MainView.this.isNotificationCollapsing = false;
                }
            }
        };
        this.screenAnimationListener = new IScreenAnimationListener() {
            @Override
            public void onStart(final BaseScreen baseScreen, final BaseScreen baseScreen2) {
                MainView.this.isScreenAnimating = true;
            }
            
            @Override
            public void onStop(final BaseScreen baseScreen, final BaseScreen baseScreen2) {
                MainView.this.isScreenAnimating = false;
            }
        };
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
        this.initLayout();
    }
    
    private void addDebugMarkers(final Context context) {
        final int color = this.getResources().getColor(R.color.hud_white);
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager)HudApplication.getAppContext().getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        final int n = (int)TypedValue.applyDimension(1, 1.0f, displayMetrics);
        final FrameLayout.LayoutParams FrameLayout.LayoutParams = new FrameLayout.LayoutParams(-1, n);
        FrameLayout.LayoutParams.gravity = 48;
        final View view = new View(context);
        view.setBackgroundColor(color);
        this.splitterView.addView(view, (ViewGroup.LayoutParams)FrameLayout.LayoutParams);
        final FrameLayout.LayoutParams FrameLayout.LayoutParams2 = new FrameLayout.LayoutParams(-1, n);
        FrameLayout.LayoutParams2.gravity = 80;
        final View view2 = new View(context);
        view2.setBackgroundColor(color);
        this.splitterView.addView(view2, (ViewGroup.LayoutParams)FrameLayout.LayoutParams2);
    }
    
    private float compactScale() {
        return SystemProperties.getFloat("persist.sys.compact.scale", 0.87f);
    }
    
    private Animator getRemoveNotificationExtensionAnimator() {
        final FrameLayout notificationExtensionView = this.getNotificationExtensionView();
        final Animator loadAnimator = AnimatorInflater.loadAnimator(((ViewGroup)notificationExtensionView).getContext(), 17498113);
        loadAnimator.setTarget(notificationExtensionView);
        loadAnimator.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                super.onAnimationEnd(animator);
                final View child = ((ViewGroup)notificationExtensionView).getChildAt(0);
                if (child instanceof Main.INotificationExtensionView) {
                    ((Main.INotificationExtensionView)child).onStop();
                }
                ((ViewGroup)notificationExtensionView).setVisibility(GONE);
                ((ViewGroup)notificationExtensionView).removeAllViews();
            }
        });
        return loadAnimator;
    }
    
    private ObjectAnimator getXPositionAnimator(final ViewGroup viewGroup, final int n) {
        return ObjectAnimator.ofFloat(viewGroup, "x", new float[] { n });
    }
    
    private void initLayout() {
        this.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                MainView.this.getViewTreeObserver().removeOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)this);
                final Resources resources = MainView.this.getResources();
                final int n = (int)resources.getDimension(R.dimen.dashboard_height);
                MainView.this.basePaddingTop = n / 2 - (int)MainView.this.getResources().getDimension(R.dimen.dashboard_box_height) / 2;
                ((FrameLayout.LayoutParams)MainView.this.splitterView.getLayoutParams()).topMargin = MainView.this.basePaddingTop + MainView.this.verticalOffset;
                MainView.this.mainPanelWidth = (int)resources.getDimension(R.dimen.dashboard_width);
                if (DeviceUtil.isNavdyDevice()) {
                    MainView.this.sidePanelWidth = (int)resources.getDimension(R.dimen.side_panel_width);
                }
                else {
                    MainView.this.sidePanelWidth = (int)(MainView.this.mainPanelWidth * 0.33f);
                }
                MainView.this.uiStateManager.setMainPanelWidth(MainView.this.mainPanelWidth);
                MainView.this.uiStateManager.setSidePanelWidth(MainView.this.sidePanelWidth);
                MainView.sLogger.v("side panel width=" + MainView.this.sidePanelWidth);
                MainView.this.rightPanelStart = MainView.this.mainPanelWidth - MainView.this.sidePanelWidth;
                final DisplayMetrics displayMetrics = MainView.this.getResources().getDisplayMetrics();
                MainView.sLogger.v("Density:" + displayMetrics.density + ", dpi:" + displayMetrics.densityDpi + ", width:" + displayMetrics.widthPixels + ", height:" + displayMetrics.heightPixels);
                MainView.sLogger.v("onGlobalLayout mainPanelWidth = " + MainView.this.mainPanelWidth + " sidePanelWidth = " + MainView.this.sidePanelWidth + " rightPanelStart = " + MainView.this.rightPanelStart + " paddingTop = " + MainView.this.basePaddingTop + " mh =" + n);
                MainView.this.containerView.setX(0.0f);
                MainView.sLogger.v("containerView x: 0");
                ((FrameLayout.LayoutParams)MainView.this.containerView.getLayoutParams()).width = MainView.this.mainPanelWidth;
                ((FrameLayout.LayoutParams)MainView.this.notificationView.getLayoutParams()).width = MainView.this.sidePanelWidth;
                MainView.this.notificationView.setX((float)MainView.this.mainPanelWidth);
                MainView.sLogger.v("notifView x: " + MainView.this.mainPanelWidth);
                MainView.this.expandedNotificationView.setX((float)MainView.this.sidePanelWidth);
                MainView.sLogger.v("expand notifView x: " + MainView.this.sidePanelWidth);
                final int n2 = MainView.this.sidePanelWidth + (int)resources.getDimension(R.dimen.expand_notif_width) + (int)resources.getDimension(R.dimen.expand_notif_indicator_left_margin);
                MainView.this.notifIndicator.setOrientation(CarouselIndicator.Orientation.VERTICAL);
                MainView.this.notifIndicator.setX((float)n2);
                MainView.sLogger.v("notif indicator x: " + n2);
                MainView.this.notifScrollIndicator.setOrientation(CarouselIndicator.Orientation.VERTICAL);
                MainView.this.notifScrollIndicator.setProperties(GlanceConstants.scrollingRoundSize, GlanceConstants.scrollingIndicatorProgressSize, 0, 0, GlanceConstants.colorWhite, GlanceConstants.colorWhite, true, GlanceConstants.scrollingIndicatorPadding, -1, -1);
                MainView.this.notifScrollIndicator.setItemCount(100);
                MainView.this.handler.postDelayed((Runnable)new Runnable() {
                    @Override
                    public void run() {
                        MainView.this.notificationView.setX((float)MainView.this.mainPanelWidth);
                        MainView.sLogger.v("notifView x: " + MainView.this.mainPanelWidth);
                    }
                }, 2000L);
                MainView.this.presenter.createScreens();
                MainView.this.presenter.setInputFocus();
            }
        });
    }
    
    private Animator middleAnimate(final AnimationMode animationMode) {
        Animator customContainerAnimator = null;
        Enum<CustomAnimationMode> enum1 = null;
        if (animationMode == AnimationMode.MOVE_LEFT_SHRINK) {
            enum1 = CustomAnimationMode.SHRINK_LEFT;
        }
        else if (animationMode == AnimationMode.RIGHT_EXPAND) {
            enum1 = CustomAnimationMode.EXPAND;
        }
        if (enum1 != null) {
            customContainerAnimator = this.containerView.getCustomContainerAnimator((CustomAnimationMode)enum1);
        }
        final AnimatorSet set = new AnimatorSet();
        Animator animator = null;
        switch (animationMode) {
            default:
                animator = null;
                break;
            case MOVE_LEFT_SHRINK: {
                final ObjectAnimator xPositionAnimator = this.getXPositionAnimator((ViewGroup)this.containerView, -this.sidePanelWidth);
                if (customContainerAnimator != null) {
                    set.playTogether(new Animator[] { xPositionAnimator, customContainerAnimator });
                    animator = (Animator)set;
                    break;
                }
                set.playTogether(new Animator[] { xPositionAnimator });
                animator = (Animator)set;
                break;
            }
            case RIGHT_EXPAND: {
                final ObjectAnimator xPositionAnimator2 = this.getXPositionAnimator((ViewGroup)this.containerView, 0);
                if (customContainerAnimator != null) {
                    set.playTogether(new Animator[] { xPositionAnimator2, customContainerAnimator });
                    animator = (Animator)set;
                    break;
                }
                set.play((Animator)xPositionAnimator2);
                animator = (Animator)set;
                break;
            }
        }
        return animator;
    }
    
    private void postNotificationCollapseEvent(final boolean b) {
        String id = null;
        NotificationType type = null;
        final INotification currentNotification = NotificationManager.getInstance().getCurrentNotification();
        if (currentNotification != null) {
            id = currentNotification.getId();
            type = currentNotification.getType();
        }
        this.uiStateManager.postNotificationAnimationEvent(b, id, type, UIStateManager.Mode.COLLAPSE);
    }
    
    private Animator rightCollapse() {
        return (Animator)this.getXPositionAnimator((ViewGroup)this.notificationView, this.mainPanelWidth);
    }
    
    private ObjectAnimator rightExpand() {
        return this.getXPositionAnimator((ViewGroup)this.notificationView, this.rightPanelStart);
    }
    
    public void addNotificationExtensionView(final View view) {
        MainView.sLogger.d("addNotificationExtensionView");
        final FrameLayout notificationExtensionView = this.getNotificationExtensionView();
        ((ViewGroup)notificationExtensionView).addView(view);
        final Animator loadAnimator = AnimatorInflater.loadAnimator(((ViewGroup)notificationExtensionView).getContext(), 17498112);
        loadAnimator.setTarget(notificationExtensionView);
        loadAnimator.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                super.onAnimationEnd(animator);
                if (view instanceof Main.INotificationExtensionView) {
                    ((Main.INotificationExtensionView)view).onStart();
                }
            }
            
            @Override
            public void onAnimationStart(final Animator animator) {
                super.onAnimationStart(animator);
                ((ViewGroup)notificationExtensionView).setVisibility(View.VISIBLE);
                MainView.this.showingNotificationExtension = true;
            }
        });
        loadAnimator.start();
    }
    
    public void dismissNotification() {
        final AnimatorSet set = new AnimatorSet();
        MainView.sLogger.d("dismissNotification");
        if (this.notificationView.getX() < this.mainPanelWidth) {
            final Builder with = set.play(this.rightCollapse()).with(this.middleAnimate(AnimationMode.RIGHT_EXPAND));
            if (this.showingNotificationExtension) {
                MainView.sLogger.d("hiding notification extension along with notification");
                with.with(this.getRemoveNotificationExtensionAnimator());
            }
        }
        set.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                MainView.this.postNotificationCollapseEvent(false);
            }
            
            @Override
            public void onAnimationStart(final Animator animator) {
                MainView.this.postNotificationCollapseEvent(true);
            }
        });
        this.animationQueue.startAnimation(set);
    }
    
    public void ejectMainLowerView() {
        this.mainLowerView.ejectView();
    }
    
    public int getBasePaddingTop() {
        return this.basePaddingTop;
    }
    
    public FrameLayout getBoxView() {
        return this.splitterView;
    }
    
    public ContainerView getContainerView() {
        return this.containerView;
    }
    
    public View getExpandedNotificationCoverView() {
        return this.expandedNotificationCoverView;
    }
    
    public FrameLayout getExpandedNotificationView() {
        return this.expandedNotificationView;
    }
    
    public int getNotificationColorVisibility() {
        return this.notificationColorView.getVisibility();
    }
    
    public FrameLayout getNotificationExtensionView() {
        return this.notificationExtensionView;
    }
    
    public CarouselIndicator getNotificationIndicator() {
        return this.notifIndicator;
    }
    
    public ProgressIndicator getNotificationScrollIndicator() {
        return this.notifScrollIndicator;
    }
    
    public NotificationView getNotificationView() {
        return this.notificationView;
    }
    
    public SystemTrayView getSystemTray() {
        return this.systemTray;
    }
    
    public ToastView getToastView() {
        return this.toastView;
    }
    
    public int getVerticalOffset() {
        return this.verticalOffset;
    }
    
    public void injectMainLowerView(final View view) {
        this.mainLowerView.injectView(view);
    }
    
    public boolean isExpandedNotificationViewShowing() {
        return this.expandedNotificationView.getVisibility() == 0;
    }
    
    public boolean isMainLowerViewVisible() {
        return this.mainLowerView.getVisibility() == 0;
    }
    
    public boolean isMainUIShrunk() {
        return (this.isNotificationExpanding || this.isNotificationViewShowing()) && !this.isNotificationCollapsing;
    }
    
    public boolean isNotificationCollapsing() {
        return this.isNotificationCollapsing;
    }
    
    public boolean isNotificationExpanding() {
        return this.isNotificationExpanding;
    }
    
    public boolean isNotificationViewShowing() {
        return this.notificationView.getX() < this.mainPanelWidth;
    }
    
    public boolean isScreenAnimating() {
        return this.isScreenAnimating;
    }
    
    public IInputHandler nextHandler() {
        return this.presenter;
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
        if (this.preferences != null) {
            if (HUDSettings.USE_ADAPTIVE_AUTOBRIGHTNESS) {
                this.brightnessControl = (SharedPreferences$OnSharedPreferenceChangeListener)new AdaptiveBrightnessControl(this.getContext(), RemoteDeviceManager.getInstance().getBus(), this.preferences, "screen.brightness", "screen.auto_brightness", "screen.auto_brightness_adj", "screen.led_brightness");
            }
            else {
                this.brightnessControl = (SharedPreferences$OnSharedPreferenceChangeListener)new BrightnessControl(this.getContext(), RemoteDeviceManager.getInstance().getBus(), this.preferences, "screen.brightness", "screen.auto_brightness", "screen.auto_brightness_adj", "screen.led_brightness");
            }
        }
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
        if (this.preferences != null) {
            this.preferences.unregisterOnSharedPreferenceChangeListener(this.brightnessControl);
            this.brightnessControl = null;
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        if (!this.isInEditMode()) {
            if (!DeviceUtil.isNavdyDevice()) {
                this.addDebugMarkers(this.getContext());
            }
            this.uiStateManager.setToastView(this.toastView);
            this.uiStateManager.addScreenAnimationListener(this.screenAnimationListener);
            this.uiStateManager.addNotificationAnimationListener(this.notificationAnimationListener);
        }
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        return false;
    }
    
    public void removeNotificationExtensionView() {
        MainView.sLogger.d("addNotificationExtensionView, showing extension ? : " + this.showingNotificationExtension);
        final FrameLayout notificationExtensionView = this.getNotificationExtensionView();
        if (notificationExtensionView != null) {
            if (this.showingNotificationExtension) {
                this.getRemoveNotificationExtensionAnimator().start();
            }
            else {
                this.notificationExtensionView.setVisibility(GONE);
                ((ViewGroup)notificationExtensionView).removeAllViews();
            }
        }
    }
    
    public void setDisplayFormat(final DriverProfilePreferences.DisplayFormat format) {
        if (format != this.format) {
            float compactScale;
            if ((this.format = format) == DriverProfilePreferences.DisplayFormat.DISPLAY_FORMAT_COMPACT) {
                compactScale = this.compactScale();
            }
            else {
                compactScale = 1.0f;
            }
            this.setScaleX(compactScale);
            this.setScaleY(compactScale);
        }
    }
    
    public void setNotificationColor(final int color) {
        this.notificationColorView.setColor(color);
    }
    
    public void setNotificationColorVisibility(final int visibility) {
        this.notificationColorView.setVisibility(visibility);
    }
    
    public void setVerticalOffset(int n) {
        if (n != this.verticalOffset) {
            this.verticalOffset = n;
            if (this.basePaddingTop != -1) {
                n = this.basePaddingTop + this.verticalOffset;
                if (n >= 0) {
                    final FrameLayout boxView = this.getBoxView();
                    final FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)boxView.getLayoutParams();
                    layoutParams.topMargin = n;
                    boxView.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
                }
            }
        }
    }
    
    public void showNotification(final String s, final NotificationType notificationType) {
        final AnimatorSet set = new AnimatorSet();
        final AnimatorSet set2 = new AnimatorSet();
        set2.playTogether(new Animator[] { this.rightExpand(), this.middleAnimate(AnimationMode.MOVE_LEFT_SHRINK) });
        if (this.notificationView.getX() < this.mainPanelWidth) {
            this.uiStateManager.postNotificationAnimationEvent(true, s, notificationType, UIStateManager.Mode.EXPAND);
            this.uiStateManager.postNotificationAnimationEvent(false, s, notificationType, UIStateManager.Mode.EXPAND);
        }
        else {
            set.play((Animator)set2);
            this.uiStateManager.getCurrentScreen();
            set.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
                @Override
                public void onAnimationEnd(final Animator animator) {
                    MainView.this.uiStateManager.postNotificationAnimationEvent(false, s, notificationType, UIStateManager.Mode.EXPAND);
                }
                
                @Override
                public void onAnimationStart(final Animator animator) {
                    MainView.this.uiStateManager.postNotificationAnimationEvent(true, s, notificationType, UIStateManager.Mode.EXPAND);
                }
            });
            this.animationQueue.startAnimation(set);
        }
    }
    
    public enum AnimationMode
    {
        MOVE_LEFT_SHRINK, 
        RIGHT_EXPAND;
    }
    
    public enum CustomAnimationMode
    {
        EXPAND, 
        SHRINK_LEFT, 
        SHRINK_MODE;
    }
}
