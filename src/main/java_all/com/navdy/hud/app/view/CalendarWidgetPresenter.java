package com.navdy.hud.app.view;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import android.widget.TextView;
import android.os.Bundle;
import java.util.Iterator;
import java.util.TimeZone;
import java.util.List;
import com.squareup.wire.Wire;
import com.navdy.service.library.events.calendars.CalendarEvent;
import java.util.Calendar;
import com.squareup.otto.Subscribe;
import android.graphics.drawable.Drawable;
import java.util.Date;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import java.util.concurrent.TimeUnit;
import com.navdy.hud.app.common.TimeHelper;
import android.os.Handler;
import android.content.Context;
import com.navdy.hud.app.framework.calendar.CalendarManager;
import com.navdy.service.library.log.Logger;

public class CalendarWidgetPresenter extends DashboardWidgetPresenter
{
    private static final long MILLISECONDS_IN_A_DAY = 86400000L;
    private static final int REFRESH_INTERVAL;
    private static final Logger sLogger;
    private String calendarGaugeName;
    CalendarManager calendarManager;
    private Context context;
    private String formattedText;
    private Handler handler;
    private boolean nextEventExists;
    private int paddingLeft;
    private Runnable refreshRunnable;
    private TimeHelper timeHelper;
    private String title;
    
    static {
        sLogger = new Logger(CalendarWidgetPresenter.class);
        REFRESH_INTERVAL = (int)TimeUnit.MINUTES.toMillis(5L);
    }
    
    public CalendarWidgetPresenter(final Context context) {
        this.timeHelper = RemoteDeviceManager.getInstance().getTimeHelper();
        this.context = context;
        this.paddingLeft = context.getResources().getDimensionPixelSize(R.dimen.calendar_widget_left_padding);
        this.calendarGaugeName = context.getResources().getString(R.string.widget_calendar);
        this.calendarManager = RemoteDeviceManager.getInstance().getCalendarManager();
        this.handler = new Handler();
        this.refreshRunnable = new Runnable() {
            @Override
            public void run() {
                CalendarWidgetPresenter.this.refreshCalendarEvent();
            }
        };
    }
    
    private String formatTime(final long n) {
        final StringBuilder sb = new StringBuilder();
        String s = this.timeHelper.formatTime(new Date(n), sb);
        if (sb.length() > 0) {
            s = s + " " + sb.toString();
        }
        return s;
    }
    
    @Override
    public Drawable getDrawable() {
        return null;
    }
    
    @Override
    public String getWidgetIdentifier() {
        return "CALENDAR_WIDGET";
    }
    
    @Override
    public String getWidgetName() {
        return this.calendarGaugeName;
    }
    
    @Override
    protected boolean isRegisteringToBusRequired() {
        return true;
    }
    
    @Subscribe
    public void onCalendarManagerEvent(final CalendarManager.CalendarManagerEvent calendarManagerEvent) {
        switch (calendarManagerEvent) {
            case UPDATED:
                this.refreshCalendarEvent();
                break;
        }
    }
    
    @Subscribe
    public void onClockChanged(final TimeHelper.UpdateClock updateClock) {
        this.refreshCalendarEvent();
    }
    
    public void refreshCalendarEvent() {
        final List<CalendarEvent> calendarEvents = this.calendarManager.getCalendarEvents();
        CalendarEvent calendarEvent = null;
        CalendarEvent calendarEvent2 = null;
        final long currentTimeMillis = System.currentTimeMillis();
        final TimeZone timeZone = this.timeHelper.getTimeZone();
        final Calendar instance = Calendar.getInstance(timeZone);
        instance.setTime(new Date(currentTimeMillis));
        if (calendarEvents != null) {
            final Iterator<CalendarEvent> iterator = calendarEvents.iterator();
            while (true) {
                calendarEvent = calendarEvent2;
                if (!iterator.hasNext()) {
                    break;
                }
                calendarEvent = iterator.next();
                CalendarWidgetPresenter.sLogger.d("Event :" + calendarEvent.display_name + ", From : " + calendarEvent.start_time + ", To : " + calendarEvent.end_time + ", All day : " + calendarEvent.all_day);
                final long longValue = Wire.<Long>get(calendarEvent.end_time, 0L);
                final long longValue2 = Wire.<Long>get(calendarEvent.start_time, 0L);
                final Calendar instance2 = Calendar.getInstance(timeZone);
                instance2.setTime(new Date(longValue2));
                if (longValue <= currentTimeMillis || instance.get(1) != instance2.get(1) || instance.get(6) != instance2.get(6)) {
                    continue;
                }
                if (!calendarEvent.all_day) {
                    break;
                }
                if (calendarEvent2 != null) {
                    continue;
                }
                calendarEvent2 = calendarEvent;
            }
        }
        if (calendarEvent != null) {
            CalendarWidgetPresenter.sLogger.d("Next event shown : " + calendarEvent.display_name);
            this.nextEventExists = true;
            this.formattedText = this.formatTime(calendarEvent.start_time);
            this.title = calendarEvent.display_name;
        }
        else {
            CalendarWidgetPresenter.sLogger.d("No event shown");
            this.nextEventExists = false;
            this.formattedText = "";
            this.title = "";
        }
        this.handler.removeCallbacks(this.refreshRunnable);
        this.handler.postDelayed(this.refreshRunnable, (long)CalendarWidgetPresenter.REFRESH_INTERVAL);
        this.reDraw();
    }
    
    @Override
    public void setView(final DashboardWidgetView dashboardWidgetView, final Bundle bundle) {
        if (dashboardWidgetView != null) {
            int int1;
            if (bundle != null) {
                int1 = bundle.getInt("EXTRA_GRAVITY", 0);
            }
            else {
                int1 = 0;
            }
            dashboardWidgetView.setContentView(R.layout.calendar_widget);
            int paddingLeft;
            if (int1 == 2) {
                paddingLeft = this.paddingLeft;
            }
            else {
                paddingLeft = 0;
            }
            dashboardWidgetView.getChildAt(0).setPadding(paddingLeft, 0, 0, 0);
            this.handler.removeCallbacks(this.refreshRunnable);
            this.refreshCalendarEvent();
        }
        else {
            this.handler.removeCallbacks(this.refreshRunnable);
        }
        super.setView(dashboardWidgetView, bundle);
    }
    
    @Override
    protected void updateGauge() {
        if (this.mWidgetView != null) {
            final TextView textView = (TextView)this.mWidgetView.findViewById(R.id.txt_unit);
            final TextView textView2 = (TextView)this.mWidgetView.findViewById(R.id.txt_value);
            if (this.nextEventExists) {
                textView.setText((CharSequence)this.formattedText);
                textView2.setText((CharSequence)this.title);
            }
            else {
                textView.setText(R.string.today);
                textView2.setText(R.string.no_more_events);
            }
        }
    }
}
