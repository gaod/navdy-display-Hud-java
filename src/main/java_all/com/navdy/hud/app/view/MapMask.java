package com.navdy.hud.app.view;

import android.graphics.Canvas;
import android.graphics.PointF;
import android.content.res.TypedArray;
import com.navdy.hud.app.R;
import android.graphics.Shader;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.content.Context;
import com.navdy.hud.app.util.CustomDimension;
import android.graphics.Path;
import android.graphics.Paint;
import android.view.View;

public class MapMask extends View
{
    private static final String TAG;
    private int bottom;
    private Paint fadePaint;
    private Path fadePath;
    private int left;
    private int mHorizontalRadius;
    private CustomDimension mHorizontalRadiusAttribute;
    private IndicatorView mIndicator;
    private int mMaskColor;
    private int mVerticalRadius;
    private CustomDimension mVerticalRadiusAttribute;
    private Paint maskPaint;
    private Path maskPath;
    private int right;
    private int top;
    
    static {
        TAG = MapMask.class.getSimpleName();
    }
    
    public MapMask(final Context context) {
        this(context, null);
    }
    
    public MapMask(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public MapMask(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.initFromAttributes(context, set);
        this.mIndicator = new IndicatorView(context, set);
        this.initDrawingTools();
    }
    
    private void evaluateDimensions(final int n, final int n2) {
        this.mHorizontalRadius = (int)this.mHorizontalRadiusAttribute.getSize(this, n, 0.0f);
        this.mVerticalRadius = (int)this.mVerticalRadiusAttribute.getSize(this, n2, 0.0f);
        this.mIndicator.evaluateDimensions(this.mHorizontalRadius * 2, n2);
    }
    
    private void initDrawingTools() {
        this.maskPath = new Path();
        (this.maskPaint = new Paint()).setColor(this.mMaskColor);
        this.maskPaint.setAntiAlias(true);
        this.maskPaint.setStyle(Paint$Style.FILL_AND_STROKE);
        final int width = this.getWidth();
        final int height = this.getHeight();
        this.maskPath.moveTo(0.0f, 0.0f);
        this.maskPath.lineTo(0.0f, (float)height);
        this.maskPath.lineTo((float)width, (float)height);
        this.maskPath.lineTo((float)width, 0.0f);
        this.maskPath.close();
        this.top = (height - this.mVerticalRadius) / 2;
        this.bottom = this.top + this.mVerticalRadius;
        final int bottom = this.bottom;
        final int mVerticalRadius = this.mVerticalRadius;
        this.left = (width - this.mHorizontalRadius * 2) / 2;
        this.right = this.left + this.mHorizontalRadius * 2;
        this.maskPath.moveTo((float)this.left, (float)this.bottom);
        final RectF rectF = new RectF((float)this.left, (float)this.top, (float)this.right, (float)(bottom + mVerticalRadius));
        this.maskPath.arcTo(rectF, 180.0f, 180.0f, false);
        IndicatorView.drawIndicatorPath(this.maskPath, this.left, this.bottom, this.mIndicator.getCurveRadius(), this.mIndicator.getIndicatorWidth(), this.mIndicator.getIndicatorHeight(), this.mHorizontalRadius * 2);
        this.maskPath.setFillType(Path$FillType.EVEN_ODD);
        final float strokeWidth = this.mIndicator.getStrokeWidth();
        float n;
        if ((n = this.mHorizontalRadius) < 60.0f) {
            n = 60.0f;
        }
        (this.fadePaint = new Paint()).setStrokeWidth(60.0f);
        this.fadePaint.setAntiAlias(true);
        this.fadePaint.setStrokeCap(Paint$Cap.BUTT);
        this.fadePaint.setStyle(Paint$Style.STROKE);
        this.fadePaint.setShader((Shader)new RadialGradient(this.left + n, this.bottom + strokeWidth, n, new int[] { 0, 0, -16777216 }, new float[] { 0.0f, (n - 60.0f) / n, 1.0f }, Shader$TileMode.CLAMP));
        (this.fadePath = new Path()).moveTo((float)this.left, (float)this.bottom);
        rectF.inset(60.0f / 2.0f - strokeWidth, 60.0f / 2.0f - strokeWidth);
        rectF.top += strokeWidth;
        rectF.bottom += strokeWidth;
        this.fadePath.arcTo(rectF, 180.0f, 180.0f, false);
    }
    
    private void initFromAttributes(final Context context, AttributeSet obtainStyledAttributes) {
        obtainStyledAttributes = (AttributeSet)context.getTheme().obtainStyledAttributes(obtainStyledAttributes, R.styleable.MapMask, 0, 0);
        try {
            this.mHorizontalRadiusAttribute = CustomDimension.getDimension(this, (TypedArray)obtainStyledAttributes, 0, 0.0f);
            this.mVerticalRadiusAttribute = CustomDimension.getDimension(this, (TypedArray)obtainStyledAttributes, 1, 0.0f);
            this.mMaskColor = ((TypedArray)obtainStyledAttributes).getColor(2, -16777216);
        }
        finally {
            ((TypedArray)obtainStyledAttributes).recycle();
        }
    }
    
    public PointF getIndicatorPoint() {
        return new PointF((float)(this.left + this.mHorizontalRadius), (float)(this.bottom - this.mIndicator.getIndicatorHeight()));
    }
    
    public void layout(final int n, final int n2, final int n3, final int n4) {
        super.layout(n, n2, n3, n4);
        this.mIndicator.layout(this.left, 0, this.right, this.mIndicator.getIndicatorHeight());
    }
    
    protected void onDraw(final Canvas canvas) {
        canvas.drawPath(this.maskPath, this.maskPaint);
        final int n = this.bottom - this.mIndicator.getIndicatorHeight();
        canvas.translate((float)this.left, (float)n);
        this.mIndicator.draw(canvas);
        canvas.translate((float)(-this.left), (float)(-n));
        canvas.drawPath(this.fadePath, this.fadePaint);
    }
    
    protected void onSizeChanged(final int n, final int n2, final int n3, final int n4) {
        this.evaluateDimensions(n, n2);
        this.initDrawingTools();
    }
}
