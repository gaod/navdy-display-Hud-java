package com.navdy.hud.app.view;

import com.navdy.service.library.events.input.GestureEvent;
import butterknife.ButterKnife;
import android.view.View;
import mortar.Mortar;
import android.util.AttributeSet;
import android.content.Context;
import javax.inject.Inject;
import com.navdy.hud.app.screen.FactoryResetScreen;
import butterknife.InjectView;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.hud.app.manager.InputManager;
import android.widget.RelativeLayout;

public class FactoryResetView extends RelativeLayout implements IInputHandler
{
    @InjectView(R.id.factory_reset_confirmation)
    ConfirmationLayout factoryResetConfirmation;
    @Inject
    FactoryResetScreen.Presenter presenter;
    
    public FactoryResetView(final Context context) {
        this(context, null);
    }
    
    public FactoryResetView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public FactoryResetView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
    }
    
    public ConfirmationLayout getConfirmation() {
        return this.factoryResetConfirmation;
    }
    
    public IInputHandler nextHandler() {
        return InputManager.nextContainingHandler((View)this);
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        return this.presenter.handleKey(customKeyEvent);
    }
}
