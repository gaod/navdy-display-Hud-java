package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException;
import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import java.io.IOException;

abstract class BaseOutgoingConstantMessage extends BaseOutgoingMessage
{
    private byte[] preBuiltPayload;
    
    BaseOutgoingConstantMessage() {
        this.preBuiltPayload = null;
    }
    
    @Override
    public byte[] buildPayload() throws IOException, StringOverflowException, UnexpectedEndOfStringException, MessageWrongWayException {
        if (this.preBuiltPayload == null) {
            this.preBuiltPayload = super.buildPayload();
        }
        return this.preBuiltPayload;
    }
}
