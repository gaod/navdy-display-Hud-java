package com.navdy.hud.app.service.pandora;

import java.util.Arrays;
import java.nio.ByteBuffer;

class CRC16CCITT
{
    private static final int CRC_INITIAL_VALUE = 65535;
    
    public static ByteBuffer calculate(final ByteBuffer byteBuffer) {
        int n = 65535;
        for (int i = 0; i < byteBuffer.limit(); ++i) {
            final int n2 = ((n >>> 8 | n << 8) & 0xFFFF) ^ (byteBuffer.get(i) & 0xFF);
            final int n3 = n2 ^ (n2 & 0xFF) >> 4;
            final int n4 = n3 ^ (n3 << 12 & 0xFFFF);
            n = (n4 ^ ((n4 & 0xFF) << 5 & 0xFFFF));
        }
        return ByteBuffer.allocate(2).putShort((short)n);
    }
    
    public static boolean checkCRC(final byte[] array, final byte[] array2) {
        return Arrays.equals(calculate(ByteBuffer.wrap(array)).array(), array2);
    }
}
