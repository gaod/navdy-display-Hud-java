package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException;
import java.nio.ByteBuffer;
import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import java.io.IOException;
import java.io.ByteArrayOutputStream;

public abstract class BaseOutgoingMessage extends BaseMessage
{
    protected static ByteArrayOutputStream putBoolean(final ByteArrayOutputStream byteArrayOutputStream, final boolean b) throws IOException, StringOverflowException {
        int n;
        if (b) {
            n = 1;
        }
        else {
            n = 0;
        }
        byteArrayOutputStream.write(n);
        return byteArrayOutputStream;
    }
    
    protected static ByteArrayOutputStream putByte(final ByteArrayOutputStream byteArrayOutputStream, final byte b) throws IOException, StringOverflowException {
        byteArrayOutputStream.write(b);
        return byteArrayOutputStream;
    }
    
    protected static ByteArrayOutputStream putFixedLengthASCIIString(final ByteArrayOutputStream byteArrayOutputStream, final int n, final String s) throws IOException, StringOverflowException {
        final byte[] bytes = s.getBytes(BaseOutgoingMessage.FIXED_LENGTH_STRING_ENCODING);
        if (bytes.length > n) {
            throw new StringOverflowException();
        }
        byteArrayOutputStream.write(bytes);
        for (int i = bytes.length; i < n; ++i) {
            byteArrayOutputStream.write(0);
        }
        return byteArrayOutputStream;
    }
    
    protected static ByteArrayOutputStream putInt(final ByteArrayOutputStream byteArrayOutputStream, final int n) throws IOException, StringOverflowException {
        byteArrayOutputStream.write(ByteBuffer.allocate(4).putInt(n).array());
        return byteArrayOutputStream;
    }
    
    protected static ByteArrayOutputStream putShort(final ByteArrayOutputStream byteArrayOutputStream, final short n) throws IOException, StringOverflowException {
        byteArrayOutputStream.write(ByteBuffer.allocate(2).putShort(n).array());
        return byteArrayOutputStream;
    }
    
    protected static ByteArrayOutputStream putString(final ByteArrayOutputStream byteArrayOutputStream, final String s) throws IOException, StringOverflowException, UnexpectedEndOfStringException {
        if (s.length() > 247) {
            throw new StringOverflowException();
        }
        if (s.indexOf(0) >= 0) {
            throw new UnexpectedEndOfStringException();
        }
        byteArrayOutputStream.write(s.getBytes(BaseOutgoingMessage.STRING_ENCODING));
        byteArrayOutputStream.write(0);
        return byteArrayOutputStream;
    }
    
    public byte[] buildPayload() throws IOException, StringOverflowException, UnexpectedEndOfStringException, MessageWrongWayException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            this.putThis(byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        }
        finally {
            IOUtils.closeStream(byteArrayOutputStream);
        }
    }
    
    protected ByteArrayOutputStream putThis(final ByteArrayOutputStream byteArrayOutputStream) throws IOException, StringOverflowException, UnexpectedEndOfStringException, MessageWrongWayException {
        throw new MessageWrongWayException();
    }
}
