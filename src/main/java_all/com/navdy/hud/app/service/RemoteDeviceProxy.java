package com.navdy.hud.app.service;

import com.squareup.otto.Subscribe;
import com.navdy.service.library.device.connection.Connection;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.util.Listenable;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.device.NavdyDeviceId;
import android.content.Context;
import com.navdy.service.library.device.RemoteDevice;

public class RemoteDeviceProxy extends RemoteDevice
{
    private volatile int bandwidthLevel;
    private boolean connected;
    protected ConnectionServiceProxy proxy;
    
    public RemoteDeviceProxy(final ConnectionServiceProxy proxy, final Context context, final NavdyDeviceId navdyDeviceId) {
        super(context, navdyDeviceId, true);
        this.bandwidthLevel = 1;
        this.proxy = proxy;
        this.proxy.getBus().register(this);
        this.bandwidthLevel = 1;
        this.connected = true;
    }
    
    private void notSupported() {
        throw new UnsupportedOperationException("Can't call this method on remoteDeviceProxy");
    }
    
    @Override
    public boolean connect() {
        this.notSupported();
        return false;
    }
    
    @Override
    public boolean disconnect() {
        this.notSupported();
        return false;
    }
    
    @Override
    protected void dispatchNavdyEvent(final NavdyEvent navdyEvent) {
        this.dispatchToListeners((Listenable.EventDispatcher)new EventDispatcher() {
            public void dispatchEvent(final RemoteDevice remoteDevice, final RemoteDevice.Listener listener) {
                listener.onNavdyEventReceived(remoteDevice, navdyEvent);
            }
        });
    }
    
    @Override
    public int getLinkBandwidthLevel() {
        return this.bandwidthLevel;
    }
    
    @Override
    public boolean isConnected() {
        return this.connected;
    }
    
    @Subscribe
    public void onConnectionStateChange(final ConnectionStateChange connectionStateChange) {
        switch (connectionStateChange.state) {
            case CONNECTION_CONNECTED:
                this.connected = true;
                this.dispatchConnectEvent();
                break;
            case CONNECTION_DISCONNECTED:
                this.connected = false;
                this.dispatchDisconnectEvent(DisconnectCause.NORMAL);
                break;
        }
    }
    
    @Subscribe
    public void onNavdyEvent(final NavdyEvent navdyEvent) {
        this.dispatchNavdyEvent(navdyEvent);
    }
    
    @Override
    public boolean postEvent(final NavdyEvent navdyEvent) {
        this.proxy.postRemoteEvent(this.getDeviceId(), navdyEvent);
        return true;
    }
    
    @Override
    public boolean postEvent(final NavdyEvent navdyEvent, final PostEventHandler postEventHandler) {
        boolean b;
        if (postEventHandler != null) {
            this.notSupported();
            b = false;
        }
        else {
            this.proxy.postRemoteEvent(this.getDeviceId(), navdyEvent);
            b = true;
        }
        return b;
    }
    
    @Override
    public void setLinkBandwidthLevel(final int bandwidthLevel) {
        this.bandwidthLevel = bandwidthLevel;
    }
}
