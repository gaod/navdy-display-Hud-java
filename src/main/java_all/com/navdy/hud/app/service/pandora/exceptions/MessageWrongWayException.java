package com.navdy.hud.app.service.pandora.exceptions;

public class MessageWrongWayException extends Exception
{
    public MessageWrongWayException() {
        super("Trying to send/receive message which is not supported");
    }
}
