package com.navdy.hud.app.service.pandora;

import com.navdy.service.library.log.Logger;

public class ReadMessagesThread implements Runnable
{
    private static final Logger sLogger;
    private PandoraManager parentManager;
    
    static {
        sLogger = PandoraManager.sLogger;
    }
    
    public ReadMessagesThread(final PandoraManager parentManager) {
        this.parentManager = parentManager;
    }
    
    @Override
    public void run() {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_1       
        //     2: aconst_null    
        //     3: astore_2       
        //     4: aconst_null    
        //     5: astore_3       
        //     6: aconst_null    
        //     7: astore          4
        //     9: aload           4
        //    11: astore          5
        //    13: aload_2        
        //    14: astore          6
        //    16: aload_3        
        //    17: astore          7
        //    19: aload_1        
        //    20: astore          8
        //    22: aload_0        
        //    23: getfield        com/navdy/hud/app/service/pandora/ReadMessagesThread.parentManager:Lcom/navdy/hud/app/service/pandora/PandoraManager;
        //    26: invokevirtual   com/navdy/hud/app/service/pandora/PandoraManager.getSocket:()Landroid/bluetooth/BluetoothSocket;
        //    29: astore          9
        //    31: aload           4
        //    33: astore          5
        //    35: aload_2        
        //    36: astore          6
        //    38: aload_3        
        //    39: astore          7
        //    41: aload_1        
        //    42: astore          8
        //    44: aload           9
        //    46: invokevirtual   android/bluetooth/BluetoothSocket.getInputStream:()Ljava/io/InputStream;
        //    49: astore_2       
        //    50: aload           4
        //    52: astore          5
        //    54: aload_2        
        //    55: astore          6
        //    57: aload_3        
        //    58: astore          7
        //    60: aload_2        
        //    61: astore          8
        //    63: aload_2        
        //    64: invokevirtual   java/io/InputStream.read:()I
        //    67: istore          10
        //    69: aconst_null    
        //    70: astore          4
        //    72: iload           10
        //    74: iflt            304
        //    77: aload           9
        //    79: ifnull          304
        //    82: aload           4
        //    84: ifnull          139
        //    87: aload           4
        //    89: iload           10
        //    91: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //    94: iload           10
        //    96: bipush          124
        //    98: if_icmpne       301
        //   101: aload_0        
        //   102: getfield        com/navdy/hud/app/service/pandora/ReadMessagesThread.parentManager:Lcom/navdy/hud/app/service/pandora/PandoraManager;
        //   105: aload           4
        //   107: invokevirtual   java/io/ByteArrayOutputStream.toByteArray:()[B
        //   110: invokevirtual   com/navdy/hud/app/service/pandora/PandoraManager.onFrameReceived:([B)V
        //   113: aconst_null    
        //   114: astore          4
        //   116: aload           4
        //   118: astore          5
        //   120: aload_2        
        //   121: astore          6
        //   123: aload           4
        //   125: astore          7
        //   127: aload_2        
        //   128: astore          8
        //   130: aload_2        
        //   131: invokevirtual   java/io/InputStream.read:()I
        //   134: istore          10
        //   136: goto            72
        //   139: iload           10
        //   141: bipush          126
        //   143: if_icmpne       251
        //   146: new             Ljava/io/ByteArrayOutputStream;
        //   149: dup            
        //   150: invokespecial   java/io/ByteArrayOutputStream.<init>:()V
        //   153: astore_3       
        //   154: aload_3        
        //   155: astore          5
        //   157: aload_2        
        //   158: astore          6
        //   160: aload_3        
        //   161: astore          7
        //   163: aload_2        
        //   164: astore          8
        //   166: aload_3        
        //   167: iload           10
        //   169: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //   172: aload_3        
        //   173: astore          4
        //   175: goto            116
        //   178: astore          4
        //   180: aload           6
        //   182: astore_2       
        //   183: aload           5
        //   185: astore          7
        //   187: aload_2        
        //   188: astore          8
        //   190: getstatic       com/navdy/hud/app/service/pandora/ReadMessagesThread.sLogger:Lcom/navdy/service/library/log/Logger;
        //   193: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   196: invokevirtual   java/lang/Thread.getName:()Ljava/lang/String;
        //   199: aload           4
        //   201: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   204: aload           5
        //   206: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   209: aload_2        
        //   210: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   213: aload_0        
        //   214: getfield        com/navdy/hud/app/service/pandora/ReadMessagesThread.parentManager:Lcom/navdy/hud/app/service/pandora/PandoraManager;
        //   217: invokevirtual   com/navdy/hud/app/service/pandora/PandoraManager.terminateAndClose:()V
        //   220: getstatic       com/navdy/hud/app/service/pandora/ReadMessagesThread.sLogger:Lcom/navdy/service/library/log/Logger;
        //   223: new             Ljava/lang/StringBuilder;
        //   226: dup            
        //   227: invokespecial   java/lang/StringBuilder.<init>:()V
        //   230: ldc             "exiting thread:"
        //   232: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   235: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   238: invokevirtual   java/lang/Thread.getName:()Ljava/lang/String;
        //   241: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   244: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   247: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   250: return         
        //   251: getstatic       com/navdy/hud/app/service/pandora/ReadMessagesThread.sLogger:Lcom/navdy/service/library/log/Logger;
        //   254: astore          8
        //   256: new             Ljava/lang/StringBuilder;
        //   259: astore          7
        //   261: aload           7
        //   263: invokespecial   java/lang/StringBuilder.<init>:()V
        //   266: aload           8
        //   268: aload           7
        //   270: ldc             "Unexpected byte received - skipping: "
        //   272: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   275: ldc             "%02X"
        //   277: iconst_1       
        //   278: anewarray       Ljava/lang/Object;
        //   281: dup            
        //   282: iconst_0       
        //   283: iload           10
        //   285: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //   288: aastore        
        //   289: invokestatic    java/lang/String.format:(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //   292: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   295: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   298: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //   301: goto            116
        //   304: getstatic       com/navdy/hud/app/service/pandora/ReadMessagesThread.sLogger:Lcom/navdy/service/library/log/Logger;
        //   307: ldc             "End of input read from the stream"
        //   309: invokevirtual   com/navdy/service/library/log/Logger.w:(Ljava/lang/String;)V
        //   312: aload           4
        //   314: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   317: aload_2        
        //   318: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   321: aload_0        
        //   322: getfield        com/navdy/hud/app/service/pandora/ReadMessagesThread.parentManager:Lcom/navdy/hud/app/service/pandora/PandoraManager;
        //   325: invokevirtual   com/navdy/hud/app/service/pandora/PandoraManager.terminateAndClose:()V
        //   328: goto            220
        //   331: astore          4
        //   333: aload           8
        //   335: astore_2       
        //   336: aload           7
        //   338: astore          8
        //   340: aload           8
        //   342: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   345: aload_2        
        //   346: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   349: aload_0        
        //   350: getfield        com/navdy/hud/app/service/pandora/ReadMessagesThread.parentManager:Lcom/navdy/hud/app/service/pandora/PandoraManager;
        //   353: invokevirtual   com/navdy/hud/app/service/pandora/PandoraManager.terminateAndClose:()V
        //   356: aload           4
        //   358: athrow         
        //   359: astore          7
        //   361: aload           4
        //   363: astore          8
        //   365: aload           7
        //   367: astore          4
        //   369: goto            340
        //   372: astore          7
        //   374: aload           4
        //   376: astore          5
        //   378: aload           7
        //   380: astore          4
        //   382: goto            183
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  22     31     178    183    Ljava/lang/Throwable;
        //  22     31     331    340    Any
        //  44     50     178    183    Ljava/lang/Throwable;
        //  44     50     331    340    Any
        //  63     69     178    183    Ljava/lang/Throwable;
        //  63     69     331    340    Any
        //  87     94     372    385    Ljava/lang/Throwable;
        //  87     94     359    372    Any
        //  101    113    372    385    Ljava/lang/Throwable;
        //  101    113    359    372    Any
        //  130    136    178    183    Ljava/lang/Throwable;
        //  130    136    331    340    Any
        //  146    154    372    385    Ljava/lang/Throwable;
        //  146    154    359    372    Any
        //  166    172    178    183    Ljava/lang/Throwable;
        //  166    172    331    340    Any
        //  190    204    331    340    Any
        //  251    301    372    385    Ljava/lang/Throwable;
        //  251    301    359    372    Any
        //  304    312    372    385    Ljava/lang/Throwable;
        //  304    312    359    372    Any
        // 
        // The error that occurred was:
        // 
        // java.util.ConcurrentModificationException
        //     at java.util.ArrayList$Itr.checkForComodification(Unknown Source)
        //     at java.util.ArrayList$Itr.next(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertLocalVariables(AstBuilder.java:2863)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2445)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
