package com.navdy.hud.app.service.pandora;

import android.bluetooth.BluetoothDevice;
import com.navdy.hud.app.service.pandora.messages.SetTrackElapsedPolling;
import com.navdy.hud.app.service.pandora.messages.SessionStart;
import com.navdy.service.library.device.NavdyDeviceId;
import android.bluetooth.BluetoothAdapter;
import com.navdy.hud.app.service.pandora.messages.SessionTerminate;
import com.navdy.hud.app.service.pandora.messages.EventTrackPlay;
import com.navdy.service.library.events.notification.NotificationsState;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.notification.ServiceType;
import com.navdy.service.library.events.notification.NotificationsStatusUpdate;
import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.UnsupportedMessageReceivedException;
import com.squareup.otto.Subscribe;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import java.io.OutputStream;
import com.navdy.service.library.util.IOUtils;
import com.navdy.hud.app.service.pandora.messages.GetTrackAlbumArt;
import com.navdy.hud.app.service.pandora.messages.GetTrackInfoExtended;
import android.text.TextUtils;
import java.io.IOException;
import com.navdy.service.library.events.photo.PhotoType;
import okio.ByteString;
import com.navdy.service.library.events.photo.PhotoUpdate;
import com.navdy.hud.app.service.pandora.messages.UpdateTrackElapsed;
import com.navdy.hud.app.service.pandora.messages.ReturnTrackAlbumArtSegment;
import com.navdy.hud.app.service.pandora.messages.UpdateTrackAlbumArt;
import com.navdy.hud.app.service.pandora.messages.ReturnTrackInfoExtended;
import com.navdy.hud.app.service.pandora.messages.UpdateTrackCompleted;
import com.navdy.hud.app.service.pandora.messages.UpdateTrack;
import com.navdy.hud.app.service.pandora.messages.UpdateStatus;
import com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage;
import android.content.res.Resources;
import com.navdy.service.library.events.audio.MusicDataSource;
import com.navdy.service.library.events.audio.MusicPlaybackState;
import android.bluetooth.BluetoothSocket;
import com.navdy.hud.app.service.pandora.messages.BaseOutgoingMessage;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.io.ByteArrayOutputStream;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import android.os.Handler;
import java.util.UUID;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import java.util.concurrent.atomic.AtomicInteger;
import java.io.Closeable;

public class PandoraManager implements Closeable
{
    private static final int ACK_WAITING_TIME = 750;
    private static final AtomicInteger CONNECT_THREAD_COUNTER;
    private static final MusicTrackInfo.Builder INITIAL_TRACK_INFO_BUILDER;
    private static final int MAX_SEND_MESSAGE_RETRIES = 10;
    private static final UUID PANDORA_UUID;
    private static final AtomicInteger READ_THREAD_COUNTER;
    private static final Handler handler;
    static final Logger sLogger;
    private Runnable activeAckMessageTimeoutCounter;
    private String adTitle;
    private Bus bus;
    private ByteArrayOutputStream currentArtwork;
    private MusicTrackInfo currentTrack;
    private byte expectedArtworkSegmentIndex;
    private AtomicBoolean isConnectingInProgress;
    private boolean isCurrentSequence0;
    private ConcurrentLinkedQueue<BaseOutgoingMessage> msgQueue;
    private String remoteDeviceId;
    private BluetoothSocket socket;
    
    static {
        sLogger = new Logger(PandoraManager.class);
        PANDORA_UUID = UUID.fromString("453994D5-D58B-96F9-6616-B37F586BA2EC");
        CONNECT_THREAD_COUNTER = new AtomicInteger(1);
        READ_THREAD_COUNTER = new AtomicInteger(1);
        handler = new Handler();
        INITIAL_TRACK_INFO_BUILDER = new MusicTrackInfo.Builder().playbackState(MusicPlaybackState.PLAYBACK_NONE).dataSource(MusicDataSource.MUSIC_SOURCE_PANDORA_API);
    }
    
    public PandoraManager(final Bus bus, final Resources resources) {
        this.remoteDeviceId = null;
        this.socket = null;
        this.isConnectingInProgress = new AtomicBoolean(false);
        this.isCurrentSequence0 = true;
        this.activeAckMessageTimeoutCounter = null;
        this.msgQueue = new ConcurrentLinkedQueue<BaseOutgoingMessage>();
        this.currentTrack = PandoraManager.INITIAL_TRACK_INFO_BUILDER.build();
        this.currentArtwork = null;
        this.expectedArtworkSegmentIndex = 0;
        this.adTitle = "";
        this.bus = bus;
        this.adTitle = resources.getString(R.string.music_pandora_ad_title);
    }
    
    private boolean isConnected() {
        final BluetoothSocket socket = this.getSocket();
        return socket != null && socket.isConnected();
    }
    
    private boolean isFlagInField(final byte b, final byte b2) {
        return (b2 & b) != 0x0;
    }
    
    private void onMessageReceived(final BaseIncomingMessage baseIncomingMessage) {
        if (baseIncomingMessage instanceof UpdateStatus) {
            this.onUpdateStatus((UpdateStatus)baseIncomingMessage);
        }
        else if (baseIncomingMessage instanceof UpdateTrack) {
            this.onUpdateTrack((UpdateTrack)baseIncomingMessage);
        }
        else if (baseIncomingMessage instanceof UpdateTrackCompleted) {
            this.onUpdateTrackCompleted((UpdateTrackCompleted)baseIncomingMessage);
        }
        else if (baseIncomingMessage instanceof ReturnTrackInfoExtended) {
            this.onReturnTrackInfoExtended((ReturnTrackInfoExtended)baseIncomingMessage);
        }
        else if (baseIncomingMessage instanceof UpdateTrackAlbumArt) {
            this.onUpdateTrackAlbumArt((UpdateTrackAlbumArt)baseIncomingMessage);
        }
        else if (baseIncomingMessage instanceof ReturnTrackAlbumArtSegment) {
            this.onReturnTrackAlbumArtSegment((ReturnTrackAlbumArtSegment)baseIncomingMessage);
        }
        else if (baseIncomingMessage instanceof UpdateTrackElapsed) {
            this.onUpdateTrackElapsed((UpdateTrackElapsed)baseIncomingMessage);
        }
        else {
            PandoraManager.sLogger.w("Received unsupported message from Pandora - ignoring: " + baseIncomingMessage.toString());
        }
    }
    
    private void onReturnTrackAlbumArtSegment(final ReturnTrackAlbumArtSegment returnTrackAlbumArtSegment) {
        if (this.currentTrack != null && this.currentTrack.index != null) {
            if (returnTrackAlbumArtSegment.trackToken != this.currentTrack.index || returnTrackAlbumArtSegment.segmentIndex != this.expectedArtworkSegmentIndex) {
                PandoraManager.sLogger.e("Wrong artwork's segment sent - ignoring it");
            }
            else {
                if (this.currentArtwork == null) {
                    if (returnTrackAlbumArtSegment.segmentIndex != 0) {
                        PandoraManager.sLogger.e("Wrong initial artwork's segment sent - ignoring it");
                        return;
                    }
                    this.currentArtwork = new ByteArrayOutputStream();
                }
                try {
                    this.currentArtwork.write(returnTrackAlbumArtSegment.data);
                    this.currentArtwork.flush();
                    this.expectedArtworkSegmentIndex = (byte)(returnTrackAlbumArtSegment.segmentIndex + 1);
                    if (this.expectedArtworkSegmentIndex == returnTrackAlbumArtSegment.totalSegments) {
                        this.bus.post(new PhotoUpdate.Builder().identifier(String.valueOf(returnTrackAlbumArtSegment.trackToken)).photo(ByteString.of(this.currentArtwork.toByteArray())).photoType(PhotoType.PHOTO_ALBUM_ART).build());
                        this.resetCurrentArtwork();
                    }
                }
                catch (IOException ex) {
                    PandoraManager.sLogger.e("Exception while processing artwork chunk: " + returnTrackAlbumArtSegment.segmentIndex);
                    this.resetCurrentArtwork();
                }
            }
        }
    }
    
    private void onReturnTrackInfoExtended(final ReturnTrackInfoExtended returnTrackInfoExtended) {
        final String title = returnTrackInfoExtended.title;
        String adTitle;
        if (TextUtils.isEmpty((CharSequence)title) && TextUtils.isEmpty((CharSequence)returnTrackInfoExtended.album) && TextUtils.isEmpty((CharSequence)returnTrackInfoExtended.artist) && returnTrackInfoExtended.duration > 0) {
            adTitle = this.adTitle;
            this.resetCurrentArtwork();
        }
        else {
            adTitle = title;
            if (returnTrackInfoExtended.albumArtLength > 0) {
                this.requestArtwork();
                adTitle = title;
            }
        }
        this.setCurrentTrack(new MusicTrackInfo.Builder(this.currentTrack).index((long)returnTrackInfoExtended.trackToken).name(adTitle).album(returnTrackInfoExtended.album).author(returnTrackInfoExtended.artist).duration(returnTrackInfoExtended.duration * 1000).currentPosition(this.secondsToMilliseconds(returnTrackInfoExtended.elapsed)).isPreviousAllowed(false).isNextAllowed(this.isFlagInField((byte)2, returnTrackInfoExtended.permissionFlags)).isOnlineStream(true).build());
    }
    
    private void onUpdateStatus(final UpdateStatus updateStatus) {
        final MusicTrackInfo.Builder builder = new MusicTrackInfo.Builder(this.currentTrack);
        switch (updateStatus.value) {
            default:
                builder.playbackState(MusicPlaybackState.PLAYBACK_NONE);
                break;
            case PLAYING:
                builder.playbackState(MusicPlaybackState.PLAYBACK_PLAYING);
                break;
            case PAUSED:
                builder.playbackState(MusicPlaybackState.PLAYBACK_PAUSED);
                break;
        }
        this.setCurrentTrack(builder.build());
        if (builder.playbackState == MusicPlaybackState.PLAYBACK_NONE) {
            PandoraManager.sLogger.w("Receive bad status from Pandora: " + updateStatus.value);
            this.terminateAndClose();
        }
    }
    
    private void onUpdateTrack(final UpdateTrack updateTrack) {
        if (updateTrack.value != 0) {
            try {
                this.sendOrQueueMessage(GetTrackInfoExtended.INSTANCE);
            }
            catch (Exception ex) {
                PandoraManager.sLogger.e("Cannot send request message for track's info - disconnecting", ex);
                this.terminateAndClose();
            }
        }
    }
    
    private void onUpdateTrackAlbumArt(final UpdateTrackAlbumArt updateTrackAlbumArt) {
        if (this.currentTrack != null && this.currentTrack.index != null) {
            if (this.currentTrack.index != updateTrackAlbumArt.trackToken) {
                PandoraManager.sLogger.w("Received update for artwork on the wrong song - ignoring");
            }
            else {
                this.requestArtwork();
            }
        }
    }
    
    private void onUpdateTrackCompleted(final UpdateTrackCompleted updateTrackCompleted) {
        this.resetCurrentArtwork();
    }
    
    private void onUpdateTrackElapsed(final UpdateTrackElapsed updateTrackElapsed) {
        if (this.currentTrack != null && this.currentTrack.index != null) {
            if (updateTrackElapsed.trackToken != this.currentTrack.index) {
                PandoraManager.sLogger.e("Elapsed time received for wrong song - ignoring");
            }
            else {
                final int secondsToMilliseconds = this.secondsToMilliseconds(updateTrackElapsed.elapsed);
                if (secondsToMilliseconds != this.currentTrack.currentPosition) {
                    this.setCurrentTrack(new MusicTrackInfo.Builder(this.currentTrack).currentPosition(secondsToMilliseconds).build());
                }
            }
        }
    }
    
    private void requestArtwork() {
        this.resetCurrentArtwork();
        try {
            this.sendOrQueueMessage(GetTrackAlbumArt.INSTANCE);
        }
        catch (Exception ex) {
            PandoraManager.sLogger.e("Exception while requesting pre-loaded artwork - ignoring", ex);
        }
    }
    
    private void resetCurrentArtwork() {
        IOUtils.closeStream(this.currentArtwork);
        this.currentArtwork = null;
        this.expectedArtworkSegmentIndex = 0;
    }
    
    private int secondsToMilliseconds(final short n) {
        return n * 1000;
    }
    
    private void sendAckMessage(final boolean b) {
        Label_0057: {
            if (!b) {
                break Label_0057;
            }
            byte[] array = AckFrameMessage.ACK_WITH_SEQUENCE_0.buildFrame();
            try {
                while (true) {
                    this.sendByteArray(array);
                    if (PandoraManager.sLogger.isLoggable(2)) {
                        PandoraManager.sLogger.i("Ack message sent to Pandora - is sequence 0: " + b);
                    }
                    return;
                    array = AckFrameMessage.ACK_WITH_SEQUENCE_1.buildFrame();
                    continue;
                }
            }
            catch (IOException ex) {
                PandoraManager.sLogger.e("Cannot send ACK message frame through the socket - disconnecting", ex);
                this.close();
            }
        }
    }
    
    private void sendByteArray(final byte[] array) throws IOException {
        final OutputStream outputStream = this.getSocket().getOutputStream();
        outputStream.write(array);
        outputStream.flush();
    }
    
    private void sendDataMessage(final byte[] array, final int n) {
        if (n <= 0) {
            PandoraManager.sLogger.e("Max number of retries for the message achieved - closing connection");
            this.close();
        }
        else {
            try {
                this.sendByteArray(array);
                this.activeAckMessageTimeoutCounter = new Runnable() {
                    final /* synthetic */ int val$newValueLeftAttempts = n - 1;
                    
                    @Override
                    public void run() {
                        PandoraManager.this.sendDataMessage(array, this.val$newValueLeftAttempts);
                    }
                };
                PandoraManager.handler.postDelayed(this.activeAckMessageTimeoutCounter, 750L);
            }
            catch (IOException ex) {
                PandoraManager.sLogger.e("Cannot send message frame through the socket - disconnecting", ex);
                this.close();
            }
        }
    }
    
    private void sendNextMessage() {
        try {
            final BaseOutgoingMessage baseOutgoingMessage = this.msgQueue.poll();
            if (baseOutgoingMessage == null) {
                PandoraManager.sLogger.v("Queue empty: no new message waiting to be sent");
            }
            else {
                PandoraManager.sLogger.i("Sending message to Pandora: " + baseOutgoingMessage.toString());
                this.sendDataMessage(new DataFrameMessage(this.isCurrentSequence0, baseOutgoingMessage.buildPayload()).buildFrame(), 10);
            }
        }
        catch (Exception ex) {
            PandoraManager.sLogger.e("Cannot send data message - disconnecting", ex);
            this.close();
        }
    }
    
    private void setCurrentTrack(final MusicTrackInfo currentTrack) {
        if (!this.currentTrack.equals(currentTrack)) {
            this.currentTrack = currentTrack;
            this.bus.post(this.currentTrack);
        }
    }
    
    private void setSocket(final BluetoothSocket socket) {
        synchronized (this) {
            if (this.socket != socket) {
                if (socket == null) {
                    IOUtils.closeStream((Closeable)this.socket);
                }
                this.socket = socket;
            }
        }
    }
    
    private void startConnecting(final boolean b) {
        if (this.isConnectingInProgress.compareAndSet(false, true)) {
            final Thread thread = new Thread(new ConnectRunnable(b));
            thread.setName("Pandora-Connect-" + PandoraManager.CONNECT_THREAD_COUNTER.getAndIncrement());
            thread.start();
        }
        else {
            PandoraManager.sLogger.w("Connecting thread is already running - ignoring the request");
        }
    }
    
    @Override
    public void close() {
        if (this.activeAckMessageTimeoutCounter != null) {
            PandoraManager.handler.removeCallbacks(this.activeAckMessageTimeoutCounter);
            this.activeAckMessageTimeoutCounter = null;
        }
        this.resetCurrentArtwork();
        this.setCurrentTrack(PandoraManager.INITIAL_TRACK_INFO_BUILDER.build());
        IOUtils.closeStream((Closeable)this.socket);
        this.setSocket(null);
        this.msgQueue.clear();
    }
    
    public BluetoothSocket getSocket() {
        synchronized (this) {
            return this.socket;
        }
    }
    
    @Subscribe
    public void onDeviceConnectionStateChange(final ConnectionStateChange connectionStateChange) {
        switch (connectionStateChange.state) {
            case CONNECTION_VERIFIED:
                this.remoteDeviceId = connectionStateChange.remoteDeviceId;
                break;
            case CONNECTION_DISCONNECTED:
                this.remoteDeviceId = null;
                this.close();
                break;
        }
    }
    
    protected void onFrameReceived(final byte[] array) {
        while (true) {
            FrameMessage frame = null;
            Label_0123: {
                try {
                    frame = FrameMessage.parseFrame(array);
                    if (!(frame instanceof AckFrameMessage)) {
                        break Label_0123;
                    }
                    if (PandoraManager.sLogger.isLoggable(2)) {
                        PandoraManager.sLogger.i("Ack message sent to Pandora - is sequence 0: " + frame.isSequence0);
                    }
                    if (this.activeAckMessageTimeoutCounter != null && this.isCurrentSequence0 != frame.isSequence0) {
                        PandoraManager.handler.removeCallbacks(this.activeAckMessageTimeoutCounter);
                        this.isCurrentSequence0 = frame.isSequence0;
                        this.activeAckMessageTimeoutCounter = null;
                        this.sendNextMessage();
                        return;
                    }
                }
                catch (IllegalArgumentException ex) {
                    PandoraManager.sLogger.e("Cannot parse received message frame", ex);
                    return;
                }
                PandoraManager.sLogger.e("Invalid ACK message received - ignoring it");
                return;
            }
            if (!(frame instanceof DataFrameMessage)) {
                goto Label_0316;
            }
            Label_0268: {
                if (frame.isSequence0) {
                    break Label_0268;
                }
                boolean b = true;
                while (true) {
                    this.sendAckMessage(b);
                    try {
                        BaseIncomingMessage buildFromPayload;
                        final MessageWrongWayException ex3;
                        final BaseIncomingMessage baseIncomingMessage2;
                        final MessageWrongWayException ex2;
                        final BaseIncomingMessage baseIncomingMessage = (BaseIncomingMessage)(ex2 = (MessageWrongWayException)(baseIncomingMessage2 = (BaseIncomingMessage)(ex3 = (MessageWrongWayException)(buildFromPayload = BaseIncomingMessage.buildFromPayload(frame.payload)))));
                        if (PandoraManager.sLogger.isLoggable(2)) {
                            PandoraManager.sLogger.i("Received (and ACKed) message from Pandora: " + baseIncomingMessage.toString());
                            buildFromPayload = baseIncomingMessage;
                        }
                        if (buildFromPayload != null) {
                            this.onMessageReceived(buildFromPayload);
                            return;
                        }
                        return;
                        b = false;
                    }
                    catch (UnsupportedMessageReceivedException baseIncomingMessage2) {}
                    catch (MessageWrongWayException ex3) {
                        goto Label_0280;
                    }
                    catch (CorruptedPayloadException ex3) {
                        goto Label_0280;
                    }
                }
            }
        }
    }
    
    @Subscribe
    public void onNotificationsStatusUpdate(final NotificationsStatusUpdate notificationsStatusUpdate) {
        if (ServiceType.SERVICE_PANDORA.equals(notificationsStatusUpdate.service)) {
            if (this.remoteDeviceId == null) {
                PandoraManager.sLogger.e("Received Pandora app state message while remote device ID is unknown");
            }
            else if (!DeviceInfo.Platform.PLATFORM_Android.equals(RemoteDeviceManager.getInstance().getRemoteDevicePlatform())) {
                PandoraManager.sLogger.e("Pandora is supported only for Android clients for now");
            }
            else {
                final boolean equals = NotificationsState.NOTIFICATIONS_ENABLED.equals(notificationsStatusUpdate.state);
                PandoraManager.sLogger.d("Received Pandora's running status: " + equals);
                if (equals) {
                    if (this.isConnected()) {
                        PandoraManager.sLogger.i("Pandora is already connected - restarting connection");
                        this.terminateAndClose();
                    }
                    this.startConnecting(false);
                }
                else {
                    this.terminateAndClose();
                }
            }
        }
    }
    
    public void sendOrQueueMessage(final BaseOutgoingMessage baseOutgoingMessage) {
        this.msgQueue.add(baseOutgoingMessage);
        if (this.activeAckMessageTimeoutCounter == null) {
            this.sendNextMessage();
        }
    }
    
    public void startAndPlay() {
        if (!this.isConnected()) {
            this.startConnecting(true);
        }
        else {
            this.sendOrQueueMessage(EventTrackPlay.INSTANCE);
        }
    }
    
    protected void terminateAndClose() {
        this.sendOrQueueMessage(SessionTerminate.INSTANCE);
        this.close();
    }
    
    class ConnectRunnable implements Runnable
    {
        private static final int MAX_CONNECTING_RETRIES = 10;
        private static final int PAUSE_BETWEEN_CONNECTING_RETRIES = 2000;
        private boolean isStartMusicOn;
        
        public ConnectRunnable(final boolean isStartMusicOn) {
            this.isStartMusicOn = false;
            this.isStartMusicOn = isStartMusicOn;
        }
        
        private void safeSleepPause() {
            try {
                Thread.sleep(2000L);
            }
            catch (InterruptedException ex) {
                PandoraManager.sLogger.e("Interrupted while pausing between Pandora connection retries");
            }
        }
        
        @Override
        public void run() {
            while (true) {
                try {
                    if (PandoraManager.this.isConnected()) {
                        PandoraManager.this.terminateAndClose();
                    }
                    final BluetoothDevice remoteDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(new NavdyDeviceId(PandoraManager.this.remoteDeviceId).getBluetoothAddress());
                    Object rfcommSocketToServiceRecord = null;
                    int n = 0;
                    while (true) {
                        Object o = rfcommSocketToServiceRecord;
                        Label_0075: {
                            if (n >= 10) {
                                break Label_0075;
                            }
                            try {
                                o = (rfcommSocketToServiceRecord = remoteDevice.createRfcommSocketToServiceRecord(PandoraManager.PANDORA_UUID));
                                ((BluetoothSocket)o).connect();
                                PandoraManager.this.setSocket((BluetoothSocket)o);
                                if (PandoraManager.this.isConnected()) {
                                    PandoraManager.this.sendOrQueueMessage(SessionStart.INSTANCE);
                                    PandoraManager.this.sendOrQueueMessage(GetTrackInfoExtended.INSTANCE);
                                    PandoraManager.this.sendOrQueueMessage(SetTrackElapsedPolling.ENABLED);
                                    if (this.isStartMusicOn) {
                                        PandoraManager.this.sendOrQueueMessage(EventTrackPlay.INSTANCE);
                                    }
                                    rfcommSocketToServiceRecord = new Thread(new ReadMessagesThread(PandoraManager.this));
                                    ((Thread)rfcommSocketToServiceRecord).setName("Pandora-Read-" + PandoraManager.READ_THREAD_COUNTER.getAndIncrement());
                                    ((Thread)rfcommSocketToServiceRecord).start();
                                }
                                PandoraManager.this.isConnectingInProgress.set(false);
                                PandoraManager.sLogger.v("exiting thread:" + Thread.currentThread().getName());
                            }
                            catch (IOException ex) {
                                PandoraManager.sLogger.e("Cannot connect to Pandora on remote device: " + PandoraManager.this.remoteDeviceId);
                                IOUtils.closeObject((Closeable)rfcommSocketToServiceRecord);
                                this.safeSleepPause();
                                ++n;
                            }
                        }
                    }
                }
                catch (Throwable rfcommSocketToServiceRecord) {
                    PandoraManager.sLogger.e(Thread.currentThread().getName(), (Throwable)rfcommSocketToServiceRecord);
                    continue;
                }
                break;
            }
        }
    }
}
