package com.navdy.hud.app.service.pandora.exceptions;

public class CorruptedPayloadException extends Exception
{
    public CorruptedPayloadException() {
        super("Message's payload is corrupted");
    }
}
