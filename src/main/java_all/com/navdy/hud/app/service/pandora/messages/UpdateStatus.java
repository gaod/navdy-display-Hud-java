package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import java.util.HashMap;
import java.util.Map;

public class UpdateStatus extends BaseIncomingMessage
{
    private static int MESSAGE_LENGTH;
    private static Map<Byte, UpdateStatus> SINGLETONS_MAP;
    public Value value;
    
    static {
        UpdateStatus.MESSAGE_LENGTH = 2;
        UpdateStatus.SINGLETONS_MAP = new HashMap<Byte, UpdateStatus>() {
            {
                this.put((byte)1, new UpdateStatus(Value.PLAYING, null));
                this.put((byte)2, new UpdateStatus(Value.PAUSED, null));
                this.put((byte)3, new UpdateStatus(Value.INCOMPATIBLE_API_VERSION, null));
                this.put((byte)4, new UpdateStatus(Value.UNKNOWN_ERROR, null));
                this.put((byte)5, new UpdateStatus(Value.NO_STATIONS, null));
                this.put((byte)6, new UpdateStatus(Value.NO_STATION_ACTIVE, null));
                this.put((byte)7, new UpdateStatus(Value.INSUFFICIENT_CONNECTIVITY, null));
                this.put((byte)8, new UpdateStatus(Value.LISTENING_RESTRICTIONS, null));
                this.put((byte)9, new UpdateStatus(Value.INVALID_LOGIN, null));
            }
        };
    }
    
    private UpdateStatus(final Value value) {
        this.value = value;
    }
    
    protected static BaseIncomingMessage innerBuildFromPayload(final byte[] array) throws MessageWrongWayException, CorruptedPayloadException {
        if (array.length != UpdateStatus.MESSAGE_LENGTH) {
            throw new CorruptedPayloadException();
        }
        final UpdateStatus updateStatus = UpdateStatus.SINGLETONS_MAP.get(array[1]);
        if (updateStatus == null) {
            throw new CorruptedPayloadException();
        }
        return updateStatus;
    }
    
    @Override
    public String toString() {
        return "Update Status: " + this.value;
    }
    
    public enum Value
    {
        INCOMPATIBLE_API_VERSION, 
        INSUFFICIENT_CONNECTIVITY, 
        INVALID_LOGIN, 
        LISTENING_RESTRICTIONS, 
        NO_STATIONS, 
        NO_STATION_ACTIVE, 
        PAUSED, 
        PLAYING, 
        UNKNOWN_ERROR;
    }
}
