package com.navdy.hud.app.service.pandora.messages;

import java.nio.charset.StandardCharsets;
import java.nio.charset.Charset;

public abstract class BaseMessage
{
    public static final String ACCESSORY_ID = "CFDA92FC";
    public static final short API_VERSION = 3;
    protected static final Charset FIXED_LENGTH_STRING_ENCODING;
    protected static final int INT_BYTES_SIZE = 4;
    public static final int MAX_ARTWORK_PAYLOAD_SIZE = 9025;
    protected static final int MAX_STRING_BYTE_LENGTH = 247;
    private static final short PNDR_API_VERSION_1 = 1;
    private static final short PNDR_API_VERSION_3 = 3;
    public static final byte PNDR_EVENT_TRACK_PLAY = 48;
    protected static final byte PNDR_FALSE = 0;
    public static final byte PNDR_GET_TRACK_ALBUM_ART = 20;
    public static final byte PNDR_GET_TRACK_INFO_EXTENDED = 22;
    private static final byte PNDR_IMAGE_JPEG = 1;
    private static final byte PNDR_IMAGE_NONE = 0;
    private static final byte PNDR_IMAGE_PNG = 2;
    private static final byte PNDR_IMAGE_RGB565 = 3;
    public static final byte PNDR_RETURN_TRACK_ALBUM_ART_SEGMENT = -107;
    public static final byte PNDR_RETURN_TRACK_INFO_EXTENDED = -99;
    private static final byte PNDR_SESSION_FLAG_PAUSE_ON_START = 2;
    public static final byte PNDR_SESSION_START = 0;
    public static final byte PNDR_SESSION_TERMINATE = 5;
    public static final byte PNDR_SET_TRACK_ELAPSED_POLLING = 21;
    public static final byte PNDR_STATUS_INCOMPATIBLE_API_VERSION = 3;
    public static final byte PNDR_STATUS_INSUFFICIENT_CONNECTIVITY = 7;
    public static final byte PNDR_STATUS_INVALID_LOGIN = 9;
    public static final byte PNDR_STATUS_LICENSING_RESTRICTIONS = 8;
    public static final byte PNDR_STATUS_NO_STATIONS = 5;
    public static final byte PNDR_STATUS_NO_STATION_ACTIVE = 6;
    public static final byte PNDR_STATUS_PAUSED = 2;
    public static final byte PNDR_STATUS_PLAYING = 1;
    public static final byte PNDR_STATUS_UNKNOWN_ERROR = 4;
    public static final byte PNDR_TRACK_FLAG_ALLOW_SKIP = 2;
    public static final int PNDR_TRACK_NONE = 0;
    protected static final byte PNDR_TRUE = 1;
    public static final byte PNDR_UPDATE_STATION_ACTIVE = -70;
    public static final byte PNDR_UPDATE_STATUS = -127;
    public static final byte PNDR_UPDATE_TRACK = -112;
    public static final byte PNDR_UPDATE_TRACK_ALBUM_ART = -106;
    public static final byte PNDR_UPDATE_TRACK_COMPLETED = -98;
    public static final byte PNDR_UPDATE_TRACK_ELAPSED = -105;
    public static final byte REQUIRED_ARTWORK_IMAGE_TYPE = 1;
    public static final short REQUIRED_ARTWORK_SIZE = 100;
    public static final short REQUIRED_STATION_ARTWORK_SIZE = 0;
    public static final byte SESSION_START_FLAGS = 2;
    protected static final int SHORT_BYTES_SIZE = 2;
    protected static final byte STRING_BORDER_BYTE = 0;
    protected static final Charset STRING_ENCODING;
    
    static {
        STRING_ENCODING = StandardCharsets.UTF_8;
        FIXED_LENGTH_STRING_ENCODING = StandardCharsets.US_ASCII;
    }
    
    @Override
    public abstract String toString();
}
