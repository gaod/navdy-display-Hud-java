package com.navdy.hud.app.util;

import java.util.Calendar;
import com.navdy.hud.app.HudApplication;
import android.text.format.DateUtils;
import java.util.Date;
import java.util.Locale;
import com.navdy.service.library.log.Logger;
import java.text.SimpleDateFormat;

public class DateUtil
{
    private static final int HOUR_HAND_ANGLE_PER_HOUR = 30;
    private static final int MINUTE_HAND_ANGLE_PER_MINUTE = 6;
    private static SimpleDateFormat dateLabelFormat;
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(DateUtil.class);
        DateUtil.dateLabelFormat = new SimpleDateFormat("d MMMM", Locale.US);
    }
    
    public static float getClockAngleForHour(final int n, final int n2) {
        return ((n % 12 - 3) * 30 + 360) % 360 + n2 / 60.0f * 30.0f;
    }
    
    public static float getClockAngleForMinutes(final int n) {
        return ((n - 15) * 6 + 360) % 360;
    }
    
    public static String getDateLabel(final Date time) {
        try {
            String s;
            if (DateUtils.isToday(time.getTime())) {
                s = HudApplication.getAppContext().getResources().getString(R.string.today);
            }
            else {
                final Date time2 = new Date();
                final Calendar instance = Calendar.getInstance();
                instance.setTime(time2);
                final Calendar instance2 = Calendar.getInstance();
                instance2.setTime(time);
                final int value = instance.get(1);
                final int value2 = instance2.get(1);
                final int value3 = instance.get(6);
                final int value4 = instance2.get(6);
                if (value == value2 && value3 - value4 == 1) {
                    s = HudApplication.getAppContext().getResources().getString(R.string.yesterday);
                }
                else {
                    final SimpleDateFormat dateLabelFormat = DateUtil.dateLabelFormat;
                    synchronized (dateLabelFormat) {
                        DateUtil.dateLabelFormat.format(time);
                    }
                }
            }
            return s;
        }
        catch (Throwable t) {
            DateUtil.sLogger.e(t);
            return null;
        }
    }
    
    public static Date parseIrmcDateStr(final String s) {
        final Date date = null;
        Date time;
        if (s == null) {
            time = date;
        }
        else {
            time = date;
            try {
                if (s.length() == 15) {
                    final Calendar instance = Calendar.getInstance();
                    instance.setTimeInMillis(0L);
                    instance.set(1, Integer.parseInt(s.substring(0, 4)));
                    instance.set(2, Integer.parseInt(s.substring(4, 6)) - 1);
                    instance.set(5, Integer.parseInt(s.substring(6, 8)));
                    instance.set(11, Integer.parseInt(s.substring(9, 11)));
                    instance.set(12, Integer.parseInt(s.substring(11, 13)));
                    instance.set(13, Integer.parseInt(s.substring(13, 15)));
                    time = instance.getTime();
                }
            }
            catch (Throwable t) {
                DateUtil.sLogger.e(t);
                time = date;
            }
        }
        return time;
    }
}
