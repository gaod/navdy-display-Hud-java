package com.navdy.hud.app.util.os;

import android.os.Process;
import java.util.Iterator;
import android.text.TextUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import java.util.ArrayList;
import com.navdy.service.library.task.TaskManager;
import android.os.Looper;
import java.util.concurrent.TimeUnit;
import com.navdy.service.library.util.SystemUtils;
import android.os.Handler;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class CpuProfiler
{
    private static final CpuUsage HIGH_CPU;
    private static final String[] HIGH_CPU_THREAD_NAME_PATTERN;
    private static final int HIGH_CPU_USAGE_THRESHOLD = 85;
    private static final int INITIAL_PERIODIC_INTERVAL;
    private static final int LOW_NICE_PRIORITY = 19;
    private static final String NAVDY_PROCESS_NAME = "com.navdy.hud.app";
    private static final CpuUsage NORMAL_CPU;
    private static final int NORMAL_CPU_USAGE_THRESHOLD = 70;
    private static final int PERIODIC_INTERVAL;
    private static final int PROCESS_CPU_USAGE_THRESHOLD = 20;
    private static final Logger sLogger;
    private static final CpuProfiler singleton;
    private Bus bus;
    private Handler handler;
    private SystemUtils.ProcessCpuInfo highCpuThreadInfo;
    private int highCpuThreadOrigPrio;
    private boolean highCpuUsage;
    private Runnable periodicRunnable;
    private Runnable periodicRunnableBk;
    private boolean running;
    
    static {
        sLogger = new Logger(CpuProfiler.class);
        HIGH_CPU = new CpuUsage(Usage.HIGH);
        NORMAL_CPU = new CpuUsage(Usage.NORMAL);
        INITIAL_PERIODIC_INTERVAL = (int)TimeUnit.SECONDS.toMillis(45L);
        PERIODIC_INTERVAL = (int)TimeUnit.SECONDS.toMillis(10L);
        HIGH_CPU_THREAD_NAME_PATTERN = new String[] { "NAVDY-HOGGER-", "Thread-" };
        singleton = new CpuProfiler();
    }
    
    private CpuProfiler() {
        this.handler = new Handler(Looper.getMainLooper());
        this.periodicRunnable = new Runnable() {
            @Override
            public void run() {
                TaskManager.getInstance().execute(CpuProfiler.this.periodicRunnableBk, 1);
            }
        };
        this.periodicRunnableBk = new Runnable() {
            @Override
            public void run() {
                while (true) {
                Block_9_Outer:
                    while (true) {
                        int cpuUser = 0;
                        int cpuSystem = 0;
                        int n = 0;
                        final SystemUtils.CpuInfo cpuInfo;
                        Label_0240: {
                            try {
                                if (CpuProfiler.this.running) {
                                    final SystemUtils.CpuInfo cpuUsage = SystemUtils.getCpuUsage();
                                    cpuUser = cpuUsage.getCpuUser();
                                    cpuSystem = cpuUsage.getCpuSystem();
                                    n = cpuUser + cpuSystem;
                                    if (CpuProfiler.this.highCpuUsage) {
                                        break Label_0240;
                                    }
                                    if (n > 85) {
                                        CpuProfiler.this.highCpuUsage = true;
                                        CpuProfiler.this.bus.post(CpuProfiler.HIGH_CPU);
                                        CpuProfiler.this.takeCorrectiveAction(cpuUsage);
                                        CpuProfiler.this.printCpuInfo("HIGH", n, cpuUser, cpuSystem, cpuUsage.getList());
                                    }
                                    else if (CpuProfiler.sLogger.isLoggable(2)) {
                                        CpuProfiler.this.printCpuInfo("NORMAL", n, cpuUser, cpuSystem, cpuUsage.getList());
                                    }
                                    if (CpuProfiler.this.running) {
                                        CpuProfiler.this.handler.postDelayed(CpuProfiler.this.periodicRunnable, (long)CpuProfiler.PERIODIC_INTERVAL);
                                    }
                                }
                                return;
                            }
                            catch (Throwable t) {
                                CpuProfiler.sLogger.e(t);
                                if (CpuProfiler.this.running) {
                                    CpuProfiler.this.handler.postDelayed(CpuProfiler.this.periodicRunnable, (long)CpuProfiler.PERIODIC_INTERVAL);
                                    return;
                                }
                                return;
                                while (true) {
                                    CpuProfiler.this.highCpuUsage = false;
                                    CpuProfiler.this.bus.post(CpuProfiler.NORMAL_CPU);
                                    CpuProfiler.this.revertCorrectiveAction(cpuInfo);
                                    CpuProfiler.this.printCpuInfo("NOW NORMAL", n, cpuUser, cpuSystem, cpuInfo.getList());
                                    continue Block_9_Outer;
                                    continue;
                                }
                            }
                            // iftrue(Label_0333:, n >= 70)
                            finally {
                                if (CpuProfiler.this.running) {
                                    CpuProfiler.this.handler.postDelayed(CpuProfiler.this.periodicRunnable, (long)CpuProfiler.PERIODIC_INTERVAL);
                                }
                            }
                        }
                        Label_0333: {
                            CpuProfiler.this.printCpuInfo("STILL HIGH", n, cpuUser, cpuSystem, cpuInfo.getList());
                        }
                        if (CpuProfiler.this.highCpuThreadInfo == null) {
                            CpuProfiler.this.takeCorrectiveAction(cpuInfo);
                            continue;
                        }
                        continue;
                    }
                }
            }
        };
        this.bus = RemoteDeviceManager.getInstance().getBus();
    }
    
    public static CpuProfiler getInstance() {
        return CpuProfiler.singleton;
    }
    
    private SystemUtils.ProcessCpuInfo getKnownHighCpuThread(final SystemUtils.CpuInfo cpuInfo) {
        for (final Object o : cpuInfo.getList()) {
            if (TextUtils.equals((CharSequence)((SystemUtils.ProcessCpuInfo)o).getProcessName(), (CharSequence)"com.navdy.hud.app")) {
                final String threadName = ((SystemUtils.ProcessCpuInfo)o).getThreadName();
                if (threadName != null) {
                    for (int i = 0; i < CpuProfiler.HIGH_CPU_THREAD_NAME_PATTERN.length; ++i) {
                        if (threadName.startsWith(CpuProfiler.HIGH_CPU_THREAD_NAME_PATTERN[i])) {
                            if (((SystemUtils.ProcessCpuInfo)o).getCpu() >= 20) {
                                CpuProfiler.sLogger.v("high cpu thread found " + ((SystemUtils.ProcessCpuInfo)o).getThreadName() + " " + ((SystemUtils.ProcessCpuInfo)o).getCpu() + "%");
                                return (SystemUtils.ProcessCpuInfo)o;
                            }
                            CpuProfiler.sLogger.v("Thread found but cpu usage < threshold" + ((SystemUtils.ProcessCpuInfo)o).getThreadName() + " " + ((SystemUtils.ProcessCpuInfo)o).getCpu() + "%");
                        }
                    }
                    continue;
                }
                continue;
            }
        }
        Object o = null;
        return (SystemUtils.ProcessCpuInfo)o;
    }
    
    private void printCpuInfo(final String s, final int n, final int n2, final int n3, final ArrayList<SystemUtils.ProcessCpuInfo> list) {
        CpuProfiler.sLogger.v("[CPU] " + s + " cpu=" + n + "% user=" + n2 + "% system=" + n3 + "%");
        for (final SystemUtils.ProcessCpuInfo processCpuInfo : list) {
            if (!"top".equals(processCpuInfo.getProcessName())) {
                CpuProfiler.sLogger.v("[CPU] " + s + " " + processCpuInfo.getCpu() + "% process=" + processCpuInfo.getProcessName() + " thread=" + processCpuInfo.getThreadName() + " tid=" + processCpuInfo.getTid() + " pid=" + processCpuInfo.getPid() + "");
            }
        }
    }
    
    private void revertCorrectiveAction(final SystemUtils.CpuInfo cpuInfo) {
        while (true) {
            try {
                CpuProfiler.sLogger.i("revertCorrectiveAction");
                if (this.highCpuThreadInfo != null) {
                    try {
                        Process.setThreadPriority(this.highCpuThreadInfo.getTid(), this.highCpuThreadOrigPrio);
                        CpuProfiler.sLogger.i("revertCorrectiveAction high cpu thread priority reduced from 19 to " + this.highCpuThreadOrigPrio);
                        this.highCpuThreadInfo = null;
                        this.highCpuThreadOrigPrio = 0;
                        return;
                    }
                    catch (Throwable t) {
                        CpuProfiler.sLogger.e(t);
                    }
                }
            }
            catch (Throwable t2) {
                CpuProfiler.sLogger.e(t2);
                return;
            }
            CpuProfiler.sLogger.i("revertCorrectiveAction high cpu thread was not detected previously");
        }
    }
    
    private void takeCorrectiveAction(final SystemUtils.CpuInfo cpuInfo) {
        while (true) {
            Label_0207: {
                Label_0195: {
                    try {
                        CpuProfiler.sLogger.i("takeCorrectiveAction");
                        final SystemUtils.ProcessCpuInfo knownHighCpuThread = this.getKnownHighCpuThread(cpuInfo);
                        if (knownHighCpuThread == null) {
                            break Label_0207;
                        }
                        if (knownHighCpuThread.getTid() == 0) {
                            break Label_0195;
                        }
                        final int threadPriority = Process.getThreadPriority(knownHighCpuThread.getTid());
                        CpuProfiler.sLogger.v("takeCorrectiveAction: high cpu thread, priority[" + threadPriority + "]");
                        if (this.highCpuThreadInfo == null) {
                            this.highCpuThreadInfo = knownHighCpuThread;
                            this.highCpuThreadOrigPrio = threadPriority;
                            try {
                                Process.setThreadPriority(this.highCpuThreadInfo.getTid(), 19);
                                CpuProfiler.sLogger.i("takeCorrectiveAction high cpu thread, priority changed from " + threadPriority + " to " + Process.getThreadPriority(knownHighCpuThread.getTid()));
                                return;
                            }
                            catch (Throwable t) {
                                CpuProfiler.sLogger.e(t);
                                this.highCpuThreadInfo = null;
                                this.highCpuThreadOrigPrio = 0;
                            }
                        }
                    }
                    catch (Throwable t2) {
                        CpuProfiler.sLogger.e(t2);
                        return;
                    }
                    CpuProfiler.sLogger.i("takeCorrectiveAction high cpu thread, priority already reduced");
                    return;
                }
                CpuProfiler.sLogger.i("takeCorrectiveAction high cpu thread tid is not valid");
                return;
            }
            CpuProfiler.sLogger.i("takeCorrectiveAction high cpu thread not found");
        }
    }
    
    public void start() {
        synchronized (this) {
            if (this.running) {
                CpuProfiler.sLogger.v("already running");
            }
            else {
                this.handler.removeCallbacks(this.periodicRunnable);
                this.handler.postDelayed(this.periodicRunnable, (long)CpuProfiler.INITIAL_PERIODIC_INTERVAL);
                this.running = true;
                CpuProfiler.sLogger.v("running");
            }
        }
    }
    
    public void stop() {
        synchronized (this) {
            if (!this.running) {
                CpuProfiler.sLogger.v("not running");
            }
            else {
                this.handler.removeCallbacks(this.periodicRunnable);
                this.running = false;
                CpuProfiler.sLogger.v("stopped");
            }
        }
    }
    
    public static class CpuUsage
    {
        public Usage usage;
        
        CpuUsage(final Usage usage) {
            this.usage = usage;
        }
    }
    
    public enum Usage
    {
        HIGH, 
        NORMAL;
    }
}
