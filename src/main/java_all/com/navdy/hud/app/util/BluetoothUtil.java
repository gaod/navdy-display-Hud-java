package com.navdy.hud.app.util;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import android.bluetooth.BluetoothDevice;
import com.navdy.service.library.log.Logger;

public class BluetoothUtil
{
    public static final int PAIRING_CONSENT = 3;
    public static final int PAIRING_VARIANT_DISPLAY_PASSKEY = 4;
    public static final int PAIRING_VARIANT_DISPLAY_PIN = 5;
    public static final Logger sLogger;
    
    static {
        sLogger = new Logger(BluetoothUtil.class);
    }
    
    public static void cancelBondProcess(final BluetoothDevice bluetoothDevice) {
        try {
            final Method method = bluetoothDevice.getClass().getMethod("cancelBondProcess", (Class<?>[])null);
            if (method != null) {
                BluetoothUtil.sLogger.i("cancellingBond for " + bluetoothDevice);
                method.invoke(bluetoothDevice, (Object[])null);
                BluetoothUtil.sLogger.i("cancelledBond for " + bluetoothDevice);
            }
            else {
                BluetoothUtil.sLogger.e("cannot get to cancelBondProcess api");
            }
        }
        catch (Throwable t) {
            BluetoothUtil.sLogger.e("Failed to cancelBondProcess", t);
        }
    }
    
    public static void cancelUserConfirmation(final BluetoothDevice bluetoothDevice) {
        try {
            BluetoothUtil.sLogger.e("cancelling cancelPairingUserInput");
            bluetoothDevice.getClass().getMethod("cancelPairingUserInput", Boolean.TYPE).invoke(bluetoothDevice, new Object[0]);
            BluetoothUtil.sLogger.e("cancelled cancelPairingUserInput");
        }
        catch (Throwable t) {
            BluetoothUtil.sLogger.e(t);
        }
    }
    
    public static void confirmPairing(final BluetoothDevice bluetoothDevice, final int n, final int n2) {
        BluetoothUtil.sLogger.i("Confirming pairing for " + bluetoothDevice + " variant" + n + " pin:" + n2);
        if (n == 4 || n == 2) {
            bluetoothDevice.setPairingConfirmation(true);
        }
        else if (n == 5) {
            bluetoothDevice.setPin(convertPinToBytes(String.format("%04d", n2)));
        }
        else if (n == 3) {
            bluetoothDevice.setPairingConfirmation(true);
        }
    }
    
    public static byte[] convertPinToBytes(final String s) {
        byte[] array;
        if (s == null) {
            array = null;
        }
        else {
            try {
                final byte[] bytes = s.getBytes("UTF-8");
                if (bytes.length > 0) {
                    array = bytes;
                    if (bytes.length <= 16) {
                        return array;
                    }
                }
                array = null;
            }
            catch (UnsupportedEncodingException ex) {
                BluetoothUtil.sLogger.e("UTF-8 not supported?!?");
                array = null;
            }
        }
        return array;
    }
    
    public static void removeBond(final BluetoothDevice bluetoothDevice) {
        try {
            final Method method = bluetoothDevice.getClass().getMethod("removeBond", (Class<?>[])null);
            if (method != null) {
                BluetoothUtil.sLogger.i("removingBond for " + bluetoothDevice);
                method.invoke(bluetoothDevice, (Object[])null);
                BluetoothUtil.sLogger.i("removedBond for " + bluetoothDevice);
            }
            else {
                BluetoothUtil.sLogger.e("cannot get to removeBond api");
            }
        }
        catch (Throwable t) {
            BluetoothUtil.sLogger.e("Failed to removeBond", t);
        }
    }
}
