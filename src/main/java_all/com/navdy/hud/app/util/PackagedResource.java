package com.navdy.hud.app.util;

import android.content.res.Resources;
import android.text.TextUtils;
import android.content.Context;
import java.io.Serializable;
import java.util.zip.ZipEntry;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import java.io.FileOutputStream;
import java.util.zip.ZipInputStream;
import com.navdy.service.library.log.Logger;

public class PackagedResource
{
    private static final int BUFFER_SIZE = 16384;
    private static final String MD5_SUFFIX = ".md5";
    private static final Object lockObject;
    private static final Logger sLogger;
    private String basePath;
    private String destinationFilename;
    
    static {
        sLogger = new Logger(PackagedResource.class);
        lockObject = new Object();
    }
    
    public PackagedResource(final String destinationFilename, final String basePath) {
        this.destinationFilename = destinationFilename;
        this.basePath = basePath;
    }
    
    private static void extractFile(final ZipInputStream zipInputStream, final String s, final byte[] array) {
        final Closeable closeable = null;
        final Closeable closeable2 = null;
        Closeable closeable3 = closeable;
        while (true) {
            try {
                try {
                    closeable3 = closeable;
                    final FileOutputStream fileOutputStream = new FileOutputStream(s);
                    Label_0068: {
                        try {
                            while (true) {
                                final int read = zipInputStream.read(array);
                                if (read == -1) {
                                    break Label_0068;
                                }
                                fileOutputStream.write(array, 0, read);
                            }
                        }
                        catch (Throwable t) {
                            final Closeable closeable4 = fileOutputStream;
                            closeable3 = closeable4;
                            PackagedResource.sLogger.e("error extracting file", t);
                            IOUtils.closeStream(closeable4);
                            return;
                            IOUtils.closeStream(closeable3);
                            throw;
                            IOUtils.fileSync(fileOutputStream);
                            IOUtils.closeStream(fileOutputStream);
                        }
                        finally {
                            closeable3 = fileOutputStream;
                        }
                    }
                }
                finally {}
            }
            catch (Throwable t) {
                final Closeable closeable4 = closeable2;
                continue;
            }
            break;
        }
    }
    
    private static boolean unpack(final String s, InputStream inputStream) {
        final File file = new File(s);
        final byte[] array = new byte[16384];
        if (!file.exists()) {
            file.mkdirs();
        }
        inputStream = new ZipInputStream(new BufferedInputStream(inputStream));
        while (true) {
            Label_0127: {
                while (true) {
                    try {
                        while (true) {
                            final ZipEntry nextEntry = ((ZipInputStream)inputStream).getNextEntry();
                            if (nextEntry == null) {
                                break;
                            }
                            Serializable string = new StringBuilder();
                            string = ((StringBuilder)string).append(s).append(File.separator).append(nextEntry.getName()).toString();
                            if (nextEntry.isDirectory()) {
                                break Label_0127;
                            }
                            extractFile((ZipInputStream)inputStream, (String)string, array);
                            ((ZipInputStream)inputStream).closeEntry();
                        }
                    }
                    catch (Throwable t) {
                        PackagedResource.sLogger.e("Error expanding zipEntry", t);
                        return false;
                        final Serializable string;
                        new File((String)string).mkdirs();
                        continue;
                    }
                    finally {
                        IOUtils.closeStream(inputStream);
                    }
                    break;
                }
            }
            IOUtils.closeStream(inputStream);
            return true;
        }
    }
    
    public static boolean unpackZip(final String p0, final String p1) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_2       
        //     2: aconst_null    
        //     3: astore_3       
        //     4: aload_2        
        //     5: astore          4
        //     7: new             Ljava/io/FileInputStream;
        //    10: astore          5
        //    12: aload_2        
        //    13: astore          4
        //    15: new             Ljava/lang/StringBuilder;
        //    18: astore          6
        //    20: aload_2        
        //    21: astore          4
        //    23: aload           6
        //    25: invokespecial   java/lang/StringBuilder.<init>:()V
        //    28: aload_2        
        //    29: astore          4
        //    31: aload           5
        //    33: aload           6
        //    35: aload_0        
        //    36: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    39: getstatic       java/io/File.separator:Ljava/lang/String;
        //    42: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    45: aload_1        
        //    46: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    49: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    52: invokespecial   java/io/FileInputStream.<init>:(Ljava/lang/String;)V
        //    55: aload_0        
        //    56: aload           5
        //    58: invokestatic    com/navdy/hud/app/util/PackagedResource.unpack:(Ljava/lang/String;Ljava/io/InputStream;)Z
        //    61: istore          7
        //    63: aload           5
        //    65: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //    68: iload           7
        //    70: ireturn        
        //    71: astore_1       
        //    72: aload_3        
        //    73: astore_0       
        //    74: aload_0        
        //    75: astore          4
        //    77: getstatic       com/navdy/hud/app/util/PackagedResource.sLogger:Lcom/navdy/service/library/log/Logger;
        //    80: ldc             "Error reading resource"
        //    82: aload_1        
        //    83: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //    86: iconst_0       
        //    87: istore          7
        //    89: aload_0        
        //    90: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //    93: goto            68
        //    96: astore_0       
        //    97: aload           4
        //    99: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   102: aload_0        
        //   103: athrow         
        //   104: astore_0       
        //   105: aload           5
        //   107: astore          4
        //   109: goto            97
        //   112: astore_1       
        //   113: aload           5
        //   115: astore_0       
        //   116: goto            74
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  7      12     71     74     Ljava/lang/Throwable;
        //  7      12     96     97     Any
        //  15     20     71     74     Ljava/lang/Throwable;
        //  15     20     96     97     Any
        //  23     28     71     74     Ljava/lang/Throwable;
        //  23     28     96     97     Any
        //  31     55     71     74     Ljava/lang/Throwable;
        //  31     55     96     97     Any
        //  55     63     112    119    Ljava/lang/Throwable;
        //  55     63     104    112    Any
        //  77     86     96     97     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0068:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void updateFromResources(final Context context, final int n) throws Throwable {
        this.updateFromResources(context, n, -1);
    }
    
    public void updateFromResources(final Context context, final int n, final int n2) throws Throwable {
        this.updateFromResources(context, n, n2, false);
    }
    
    public void updateFromResources(final Context context, final int n, int n2, final boolean b) throws Throwable {
        while (true) {
            // monitorenter(lockObject = PackagedResource.lockObject)
            final int n3 = 1;
            int n4 = 1;
            Object openRawResource = null;
            Resources resources = null;
            Object o = null;
            Object sLogger = null;
            File file = null;
            File file2 = null;
        Label_0454:
            while (true) {
                Label_0436: {
                    try {
                        resources = context.getResources();
                        o = this.basePath;
                        sLogger = new StringBuilder();
                        file = new File((String)o, ((StringBuilder)sLogger).append(this.destinationFilename).append(".md5").toString());
                        file2 = new File(this.basePath, this.destinationFilename);
                        sLogger = PackagedResource.sLogger;
                        o = new StringBuilder();
                        ((Logger)sLogger).v(((StringBuilder)o).append("updateFromResources [").append(this.destinationFilename).append("]").toString());
                        sLogger = null;
                        Label_0172: {
                            if (n2 <= 0) {
                                break Label_0172;
                            }
                            try {
                                sLogger = (openRawResource = resources.openRawResource(n2));
                                o = IOUtils.convertInputStreamToString((InputStream)sLogger, "UTF-8");
                                openRawResource = sLogger;
                                IOUtils.closeStream((Closeable)sLogger);
                                sLogger = o;
                                o = (openRawResource = null);
                                if (file.exists()) {
                                    if (!b) {
                                        openRawResource = o;
                                        if (!file2.exists()) {
                                            break Label_0436;
                                        }
                                    }
                                    n2 = n3;
                                    openRawResource = o;
                                    try {
                                        final String convertFileToString = IOUtils.convertFileToString(file.getAbsolutePath());
                                        n2 = n3;
                                        openRawResource = o;
                                        if (TextUtils.equals((CharSequence)convertFileToString, (CharSequence)sLogger)) {
                                            final int n5 = 0;
                                            n4 = 0;
                                            n2 = n5;
                                            openRawResource = o;
                                            final Logger sLogger2 = PackagedResource.sLogger;
                                            n2 = n5;
                                            openRawResource = o;
                                            n2 = n5;
                                            openRawResource = o;
                                            final StringBuilder sb = new StringBuilder();
                                            n2 = n5;
                                            openRawResource = o;
                                            sLogger2.d(sb.append("no update required signature matches:").append(convertFileToString).toString());
                                            n2 = n4;
                                        }
                                        else {
                                            n2 = n3;
                                            openRawResource = o;
                                            final Logger sLogger3 = PackagedResource.sLogger;
                                            n2 = n3;
                                            openRawResource = o;
                                            n2 = n3;
                                            openRawResource = o;
                                            final StringBuilder sb2 = new StringBuilder();
                                            n2 = n3;
                                            openRawResource = o;
                                            sLogger3.d(sb2.append("update required, device:").append(convertFileToString).append(" apk:").append((String)sLogger).toString());
                                            n2 = n4;
                                        }
                                        if (n2 == 0) {
                                            IOUtils.closeStream(null);
                                            return;
                                        }
                                        break Label_0454;
                                    }
                                    catch (Throwable t) {
                                        openRawResource = o;
                                        PackagedResource.sLogger.e(t);
                                        continue;
                                    }
                                    continue;
                                }
                            }
                            finally {
                                IOUtils.closeStream((Closeable)openRawResource);
                            }
                        }
                    }
                    finally {
                    }
                    // monitorexit(lockObject)
                }
                openRawResource = o;
                PackagedResource.sLogger.d("update required, no file");
                n2 = n4;
                continue;
            }
            openRawResource = o;
            IOUtils.copyFile(file2.getAbsolutePath(), resources.openRawResource(n));
            if (b) {
                unpackZip(this.basePath, file2.getName());
                openRawResource = o;
                final Context context2;
                IOUtils.deleteFile(context2, file2.getAbsolutePath());
            }
            if (sLogger != null) {
                openRawResource = o;
                IOUtils.copyFile(file.getAbsolutePath(), ((String)sLogger).getBytes());
            }
            IOUtils.closeStream(null);
        }
        // monitorexit(lockObject)
    }
}
