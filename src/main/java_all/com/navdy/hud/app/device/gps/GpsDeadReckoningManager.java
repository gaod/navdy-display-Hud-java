package com.navdy.hud.app.device.gps;

import com.navdy.hud.app.event.DrivingStateChange;
import android.content.res.Resources;
import com.navdy.hud.app.HudApplication;
import com.squareup.otto.Subscribe;
import android.os.SystemClock;
import android.os.Bundle;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import android.text.TextUtils;
import com.navdy.hud.app.storage.db.helper.VinInformationHelper;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;
import com.navdy.hud.app.util.GenericUtil;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.obd.ObdManager;
import android.os.Message;
import android.os.Handler;
import java.io.IOException;
import com.navdy.hud.app.manager.SpeedManager;
import java.net.Socket;
import com.navdy.hud.app.util.GForceRawSensorDataProcessor;
import org.json.JSONObject;
import java.io.OutputStream;
import java.io.InputStream;
import android.os.HandlerThread;
import android.os.Handler;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import java.util.concurrent.atomic.AtomicInteger;

public class GpsDeadReckoningManager implements Runnable
{
    private static final int ALIGNMENT_DONE = 3;
    private static final int ALIGNMENT_PACKET_LENGTH = 24;
    private static final int ALIGNMENT_PACKET_PAYLOAD_LENGTH = 16;
    private static final byte[] ALIGNMENT_PATTERN;
    private static final long ALIGNMENT_POLL_FREQUENCY = 30000L;
    private static final String ALIGNMENT_TIME = "alignment_time";
    private static final byte[] AUTO_ALIGNMENT;
    private static final byte[] CFG_ESF_RAW;
    private static final byte[] CFG_ESF_RAW_OFF;
    private static final byte[] CFG_RST_COLD;
    private static final byte[] CFG_RST_WARM;
    private static final byte DEAD_RECKONING_ONLY = 1;
    private static final int ESF_PACKET_LENGTH = 19;
    private static final byte[] ESF_RAW_PATTERN;
    private static final byte[] ESF_STATUS_PATTERN;
    private static final int FAILURE_RETRY_INTERVAL = 10000;
    private static final byte FIX_2D = 2;
    private static final byte FIX_3D = 3;
    private static final int FUSION_DONE = 1;
    private static final byte[] GET_ALIGNMENT;
    private static final byte[] GET_ESF_STATUS;
    private static final String GPS = "gps";
    private static final byte GPS_DEAD_RECKONING_COMBINED = 4;
    private static final String GPS_LOG = "gps.log";
    private static final char[] HEX;
    private static final byte[] INJECT_OBD_SPEED;
    private static final int MSG_ALIGNMENT_VALUE = 1;
    private static final byte[] NAV_STATUS_PATTERN;
    private static final byte NO_FIX = 0;
    private static final byte[] PASSPHRASE;
    private static final String PITCH = "pitch";
    private static final long POLL_FREQUENCY = 10000L;
    private static final byte[] READ_BUF;
    private static final String ROLL = "roll";
    private static final int SPEED_INJECTION_FREQUENCY = 100;
    private static final int SPEED_TIME_TAG_COUNTER = 100;
    private static final String TCP_SERVER_HOST = "127.0.0.1";
    private static final int TCP_SERVER_PORT = 42434;
    private static final byte[] TEMP_BUF;
    private static final AtomicInteger THREAD_COUNTER;
    private static final byte TIME_ONLY = 5;
    private static final String YAW = "yaw";
    private static final GpsDeadReckoningManager sInstance;
    private static final Logger sLogger;
    private volatile boolean alignmentChecked;
    private Alignment alignmentInfo;
    private final Bus bus;
    private volatile boolean deadReckoningInjectionStarted;
    private boolean deadReckoningOn;
    private byte deadReckoningType;
    private Runnable enableEsfRunnable;
    private final Runnable getAlignmentRunnable;
    private final Runnable getAlignmentRunnableRetry;
    private final Runnable getFusionStatusRunnable;
    private final Runnable getFusionStatusRunnableRetry;
    private Handler handler;
    private HandlerThread handlerThread;
    private final Runnable injectRunnable;
    private final CommandWriter injectSpeed;
    private InputStream inputStream;
    private long lastConnectionFailure;
    private OutputStream outputStream;
    private Runnable resetRunnable;
    private JSONObject rootInfo;
    private volatile boolean runThread;
    private GForceRawSensorDataProcessor sensorDataProcessor;
    private Socket socket;
    private final SpeedManager speedManager;
    private Thread thread;
    private int timeStampCounter;
    private boolean waitForAutoAlignment;
    private boolean waitForFusionStatus;
    
    static {
        sLogger = new Logger(GpsDeadReckoningManager.class);
        sInstance = new GpsDeadReckoningManager();
        HEX = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        THREAD_COUNTER = new AtomicInteger(1);
        INJECT_OBD_SPEED = new byte[] { -75, 98, 16, 2, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 11, 0, 0 };
        CFG_RST_WARM = new byte[] { -75, 98, 6, 4, 4, 0, 1, 0, 2, 0, 17, 108 };
        CFG_ESF_RAW = new byte[] { -75, 98, 6, 1, 3, 0, 16, 3, 1, 0, 0 };
        CFG_ESF_RAW_OFF = new byte[] { -75, 98, 6, 1, 3, 0, 16, 3, 0, 0, 0 };
        calculateChecksum(GpsDeadReckoningManager.CFG_ESF_RAW);
        calculateChecksum(GpsDeadReckoningManager.CFG_ESF_RAW_OFF);
        CFG_RST_COLD = new byte[] { -75, 98, 6, 4, 4, 0, -1, -1, 2, 0, 14, 97 };
        NAV_STATUS_PATTERN = new byte[] { -75, 98, 1, 3 };
        ESF_RAW_PATTERN = new byte[] { -75, 98, 16, 3 };
        ALIGNMENT_PATTERN = new byte[] { -75, 98, 16, 20 };
        GET_ALIGNMENT = new byte[] { -75, 98, 16, 20, 0, 0, 36, 124 };
        AUTO_ALIGNMENT = new byte[] { -75, 98, 6, 86, 12, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 105, 29 };
        GET_ESF_STATUS = new byte[] { -75, 98, 16, 16, 0, 0, 32, 112 };
        ESF_STATUS_PATTERN = new byte[] { -75, 98, 16, 16 };
        TEMP_BUF = new byte[128];
        READ_BUF = new byte[8192];
        PASSPHRASE = new byte[] { 110, 97, 118, 100, 121 };
    }
    
    private GpsDeadReckoningManager() {
        this.speedManager = SpeedManager.getInstance();
        this.timeStampCounter = 0;
        this.deadReckoningType = -1;
        this.injectSpeed = (CommandWriter)new CommandWriter() {
            @Override
            public void run() throws IOException {
                GpsDeadReckoningManager.this.sendObdSpeed();
            }
        };
        this.injectRunnable = new Runnable() {
            @Override
            public void run() {
                GpsDeadReckoningManager.this.invokeUblox(GpsDeadReckoningManager.this.injectSpeed);
                if (GpsDeadReckoningManager.this.deadReckoningInjectionStarted && GpsDeadReckoningManager.this.lastConnectionFailure == 0L) {
                    GpsDeadReckoningManager.this.handler.postDelayed((Runnable)this, 100L);
                }
                else {
                    GpsDeadReckoningManager.this.handler.removeCallbacks((Runnable)this);
                }
            }
        };
        this.getFusionStatusRunnable = new Runnable() {
            @Override
            public void run() {
                if (GpsDeadReckoningManager.this.invokeUblox(GpsDeadReckoningManager.GET_ESF_STATUS, "get fusion status")) {
                    GpsDeadReckoningManager.this.handler.removeCallbacks(GpsDeadReckoningManager.this.getFusionStatusRunnableRetry);
                    GpsDeadReckoningManager.this.handler.postDelayed(GpsDeadReckoningManager.this.getFusionStatusRunnableRetry, 10000L);
                }
                else {
                    GpsDeadReckoningManager.this.postFusionRunnable(true);
                }
            }
        };
        this.getFusionStatusRunnableRetry = new Runnable() {
            @Override
            public void run() {
                GpsDeadReckoningManager.sLogger.v("getFusionStatusRunnableRetry");
                GpsDeadReckoningManager.this.getFusionStatusRunnable.run();
            }
        };
        this.getAlignmentRunnable = new Runnable() {
            @Override
            public void run() {
                if (GpsDeadReckoningManager.this.invokeUblox(GpsDeadReckoningManager.GET_ALIGNMENT, "get alignment")) {
                    GpsDeadReckoningManager.this.handler.removeCallbacks(GpsDeadReckoningManager.this.getAlignmentRunnableRetry);
                    GpsDeadReckoningManager.this.handler.postDelayed(GpsDeadReckoningManager.this.getAlignmentRunnableRetry, 30000L);
                }
                else {
                    GpsDeadReckoningManager.this.postAlignmentRunnable(true);
                }
            }
        };
        this.getAlignmentRunnableRetry = new Runnable() {
            @Override
            public void run() {
                GpsDeadReckoningManager.sLogger.v("getAlignmentRunnableRetry");
                GpsDeadReckoningManager.this.getAlignmentRunnable.run();
            }
        };
        this.enableEsfRunnable = new Runnable() {
            @Override
            public void run() {
                GpsDeadReckoningManager.this.invokeUblox(GpsDeadReckoningManager.CFG_ESF_RAW, "ESF-RAW");
                GpsDeadReckoningManager.this.sensorDataProcessor.setCalibrated(false);
                GpsDeadReckoningManager.this.bus.post(new CalibratedGForceData(0.0f, 0.0f, 0.0f));
            }
        };
        this.resetRunnable = new Runnable() {
            @Override
            public void run() {
                GpsDeadReckoningManager.this.invokeUblox(GpsDeadReckoningManager.CFG_RST_WARM, "RST-WARM (warm reset)");
            }
        };
        GpsDeadReckoningManager.sLogger.v("ctor()");
        this.sensorDataProcessor = new GForceRawSensorDataProcessor();
        (this.handlerThread = new HandlerThread("gpsDeadReckoningHandler")).start();
        this.handler = new Handler(this.handlerThread.getLooper(), (Handler$Callback)new Handler$Callback() {
            public boolean handleMessage(final Message message) {
                switch (message.what) {
                    case 1: {
                        final Alignment alignment = (Alignment)message.obj;
                        try {
                            GpsDeadReckoningManager.this.handleAutoAlignmentResult(alignment);
                        }
                        catch (Throwable t) {
                            GpsDeadReckoningManager.sLogger.e(t);
                            if (!GpsDeadReckoningManager.this.waitForAutoAlignment) {
                                GpsDeadReckoningManager.this.postAlignmentRunnable(true);
                            }
                            else if (GpsDeadReckoningManager.this.waitForFusionStatus) {
                                GpsDeadReckoningManager.this.postFusionRunnable(true);
                            }
                        }
                        break;
                    }
                }
                return false;
            }
        });
        final ObdManager instance = ObdManager.getInstance();
        if (instance.isConnected()) {
            GpsDeadReckoningManager.sLogger.v("ctor() obd is connected");
            if (instance.isSpeedPidAvailable()) {
                GpsDeadReckoningManager.sLogger.v("ctor() speed pid is available");
                this.checkAlignment();
                this.startDeadReckoning();
            }
            else {
                GpsDeadReckoningManager.sLogger.v("ctor() speed pid is not available");
            }
        }
        else {
            GpsDeadReckoningManager.sLogger.v("ctor() obd is not connected, waiting for obd to connect");
        }
        (this.bus = RemoteDeviceManager.getInstance().getBus()).register(this);
        GpsDeadReckoningManager.sLogger.v("ctor() initialized");
        this.handler.post(this.enableEsfRunnable);
    }
    
    private static String bytesToHex(final byte[] array, final int n, final int n2) {
        final char[] array2 = new char[n2 * 2];
        for (int i = 0; i < n2; ++i) {
            final int n3 = array[i + n] & 0xFF;
            array2[i * 2] = GpsDeadReckoningManager.HEX[n3 >>> 4];
            array2[i * 2 + 1] = GpsDeadReckoningManager.HEX[n3 & 0xF];
        }
        return new String(array2);
    }
    
    private static void calculateChecksum(final byte[] array) {
        final int length = array.length;
        byte b = 0;
        byte b2 = 0;
        for (int i = 2; i < length - 2; ++i) {
            b += array[i];
            b2 += b;
        }
        array[length - 2] = b;
        array[length - 1] = b2;
    }
    
    private void checkAlignment() {
        if (!this.alignmentChecked) {
            GpsDeadReckoningManager.sLogger.v("checking alignment");
            this.postAlignmentRunnable(false);
        }
        else {
            GpsDeadReckoningManager.sLogger.v("alignment already checked");
        }
    }
    
    private void closeSocket() {
        this.runThread = false;
        IOUtils.closeStream(this.inputStream);
        IOUtils.closeStream(this.outputStream);
        IOUtils.closeStream(this.socket);
        this.inputStream = null;
        this.outputStream = null;
        this.socket = null;
        while (true) {
            if (this.thread == null) {
                break Label_0095;
            }
            try {
                if (this.thread.isAlive()) {
                    this.thread.interrupt();
                    GpsDeadReckoningManager.sLogger.v("waiting for thread");
                    this.thread.join();
                    GpsDeadReckoningManager.sLogger.v("waited");
                }
                else {
                    GpsDeadReckoningManager.sLogger.v("thread is not alive");
                }
                this.thread = null;
                this.deadReckoningOn = false;
                this.deadReckoningType = -1;
            }
            catch (Throwable t) {
                GpsDeadReckoningManager.sLogger.e(t);
                this.thread = null;
                continue;
            }
            finally {
                this.thread = null;
            }
            break;
        }
    }
    
    private boolean connectSocket() {
        boolean b = true;
        try {
            if (this.socket == null) {
                this.socket = new Socket("127.0.0.1", 42434);
                this.inputStream = this.socket.getInputStream();
                this.outputStream = this.socket.getOutputStream();
                GpsDeadReckoningManager.sLogger.v("connected to 42434");
                this.outputStream.write(GpsDeadReckoningManager.PASSPHRASE);
                GpsDeadReckoningManager.sLogger.v("sent passphrase");
                (this.thread = new Thread(this)).setName("GpsDeadReckoningManager-" + GpsDeadReckoningManager.THREAD_COUNTER.getAndIncrement());
                this.runThread = true;
                this.thread.start();
                GenericUtil.sleep(2000);
                b = b;
            }
            return b;
        }
        catch (Throwable t) {
            GpsDeadReckoningManager.sLogger.e(t);
            this.closeSocket();
            b = false;
            return b;
        }
    }
    
    private static String getDRType(final byte b) {
        String s = null;
        switch (b) {
            default:
                s = "UNKNOWN";
                break;
            case 0:
                s = "NO_FIX";
                break;
            case 1:
                s = "DEAD_RECKONING_ONLY";
                break;
            case 2:
                s = "FIX_2D";
                break;
            case 3:
                s = "FIX_3D";
                break;
            case 4:
                s = "GPS_DEAD_RECKONING_COMBINED";
                break;
            case 5:
                s = "TIME_ONLY";
                break;
        }
        return s;
    }
    
    public static GpsDeadReckoningManager getInstance() {
        return GpsDeadReckoningManager.sInstance;
    }
    
    private void handleAlignment(int n, int n2) throws Throwable {
        if (n + 24 <= n2) {
            GpsDeadReckoningManager.sLogger.v("alignment raw data[" + bytesToHex(GpsDeadReckoningManager.READ_BUF, n, 24) + "]");
            final ByteBuffer wrap = ByteBuffer.wrap(GpsDeadReckoningManager.READ_BUF, n, 24);
            wrap.order(ByteOrder.LITTLE_ENDIAN);
            wrap.get(GpsDeadReckoningManager.TEMP_BUF, 0, GpsDeadReckoningManager.ALIGNMENT_PATTERN.length);
            n = wrap.getShort();
            if (n != 16) {
                throw new RuntimeException("len[" + n + "] expected [" + 16 + "]");
            }
            wrap.get(GpsDeadReckoningManager.TEMP_BUF, 0, 4);
            n = wrap.get();
            final byte value = wrap.get();
            n2 = (value & 0xE) >> 1;
            final boolean b = (value & 0x1) == 0x1;
            final boolean b2 = n2 == 3;
            wrap.get();
            wrap.get();
            final float n3 = wrap.getInt() / 100.0f;
            final float n4 = wrap.getShort() / 100.0f;
            final float n5 = wrap.getShort() / 100.0f;
            final Message obtain = Message.obtain();
            obtain.what = 1;
            final Alignment obj = new Alignment(n3, n4, n5, b2);
            obtain.obj = obj;
            GpsDeadReckoningManager.sLogger.v("Alignment bitField[" + value + "] alignment[" + n2 + "] autoAlignment[" + b + "] version[" + n + "] " + obj);
            this.handler.removeCallbacks(this.getAlignmentRunnableRetry);
            this.handler.sendMessage(obtain);
        }
        else {
            this.postAlignmentRunnable(true);
        }
    }
    
    private void handleAutoAlignmentResult(final Alignment alignmentInfo) {
        if (!this.waitForAutoAlignment) {
            GpsDeadReckoningManager.sLogger.v("waiting for align:false");
            String vin;
            if ((vin = ObdManager.getInstance().getVin()) == null) {
                vin = "UNKNOWN_VIN";
            }
            GpsDeadReckoningManager.sLogger.v("Vin is " + vin);
            final String vinInfo = VinInformationHelper.getVinInfo(vin);
            final boolean b = false;
            final boolean b2 = false;
            int n = 0;
            Label_0089: {
                if (TextUtils.isEmpty((CharSequence)vinInfo)) {
                    GpsDeadReckoningManager.sLogger.v("no info found for vin,  need auto alignment");
                    n = 1;
                }
                else {
                    n = (b ? 1 : 0);
                    Label_0386: {
                        try {
                            final Logger sLogger = GpsDeadReckoningManager.sLogger;
                            n = (b ? 1 : 0);
                            n = (b ? 1 : 0);
                            final StringBuilder sb = new StringBuilder();
                            n = (b ? 1 : 0);
                            sLogger.v(sb.append("VinInfo is ").append(vinInfo).toString());
                            n = (b ? 1 : 0);
                            if (alignmentInfo.yaw != 0.0f) {
                                break Label_0386;
                            }
                            n = (b ? 1 : 0);
                            if (alignmentInfo.pitch != 0.0f) {
                                break Label_0386;
                            }
                            n = (b ? 1 : 0);
                            if (alignmentInfo.roll != 0.0f) {
                                break Label_0386;
                            }
                            n = (b ? 1 : 0);
                            GpsDeadReckoningManager.sLogger.w("VinInfo exists but u-blox alignment is lost, trigger alignment");
                            n = (b ? 1 : 0);
                            AnalyticsSupport.localyticsSendEvent("GPS_Calibration_Lost", new String[0]);
                            n = 1;
                            final boolean b3 = true;
                            VinInformationHelper.deleteVinInfo(vin);
                            n = (b3 ? 1 : 0);
                        }
                        catch (Throwable t) {
                            GpsDeadReckoningManager.sLogger.e(t);
                        }
                        break Label_0089;
                    }
                    this.rootInfo = new JSONObject(vinInfo);
                    GpsDeadReckoningManager.sLogger.v("elapsed=" + (System.currentTimeMillis() - Long.parseLong(this.rootInfo.getJSONObject("gps").getString("alignment_time"))));
                    n = (b2 ? 1 : 0);
                }
            }
            final String string = VinInformationHelper.getVinPreference().getString("vin", (String)null);
            GpsDeadReckoningManager.sLogger.v("last vin:" + string + " current vin:" + vin);
            if (!TextUtils.isEmpty((CharSequence)string) && !TextUtils.equals((CharSequence)string, (CharSequence)vin)) {
                GpsDeadReckoningManager.sLogger.v("vin has switched: trigger auto alignment");
                n = 1;
                AnalyticsSupport.localyticsSendEvent("GPS_Calibration_VinSwitch", new String[0]);
            }
            else {
                GpsDeadReckoningManager.sLogger.v("vin switch not detected");
            }
            if (n != 0) {
                GpsDeadReckoningManager.sLogger.v("alignment required");
                VinInformationHelper.getVinPreference().edit().remove("vin").commit();
                GpsDeadReckoningManager.sLogger.v("last pref removed");
                if (!this.invokeUblox((CommandWriter)new CommandWriter() {
                    @Override
                    public void run() throws IOException {
                        GpsDeadReckoningManager.this.sendAutoAlignment();
                    }
                })) {
                    this.postAlignmentRunnable(true);
                }
            }
            else {
                this.alignmentChecked = true;
                GpsDeadReckoningManager.sLogger.v("alignment not reqd");
                TTSUtils.debugShowGpsSensorCalibrationNotNeeded();
            }
        }
        else if (!alignmentInfo.done) {
            GpsDeadReckoningManager.sLogger.v("alignment not done yet, try again");
            this.postAlignmentRunnable(true);
        }
        else {
            this.alignmentInfo = alignmentInfo;
            AnalyticsSupport.localyticsSendEvent("GPS_Calibration_IMU_Done", new String[0]);
            TTSUtils.debugShowGpsImuCalibrationDone();
            this.waitForAutoAlignment = false;
            this.waitForFusionStatus = true;
            this.postFusionRunnable(false);
            GpsDeadReckoningManager.sLogger.v("got alignment, wait for fusion status");
        }
    }
    
    private void handleEsfRaw(final int n, final int n2) {
        this.sensorDataProcessor.onRawData(new RawSensorData(GpsDeadReckoningManager.READ_BUF, n));
        if (this.sensorDataProcessor.isCalibrated()) {
            this.bus.post(new CalibratedGForceData(this.sensorDataProcessor.xAccel, this.sensorDataProcessor.yAccel, this.sensorDataProcessor.zAccel));
        }
    }
    
    private void handleFusion(int value, int value2) throws Throwable {
        if (value + 19 <= value2) {
            GpsDeadReckoningManager.sLogger.v("esf raw data[" + bytesToHex(GpsDeadReckoningManager.READ_BUF, value, 19) + "]");
            final ByteBuffer wrap = ByteBuffer.wrap(GpsDeadReckoningManager.READ_BUF, value, 19);
            wrap.order(ByteOrder.LITTLE_ENDIAN);
            wrap.get(GpsDeadReckoningManager.TEMP_BUF, 0, GpsDeadReckoningManager.ALIGNMENT_PATTERN.length);
            final short short1 = wrap.getShort();
            wrap.get(GpsDeadReckoningManager.TEMP_BUF, 0, 4);
            final byte value3 = wrap.get();
            value = wrap.get();
            final byte value4 = wrap.get();
            wrap.get(GpsDeadReckoningManager.TEMP_BUF, 0, 5);
            value2 = wrap.get();
            this.handler.removeCallbacks(this.getFusionStatusRunnableRetry);
            if (value2 != 1) {
                GpsDeadReckoningManager.sLogger.v("Fusion not done retry len[" + short1 + "] fusionMode[" + value2 + "] initStatus1[" + value + "] initStatus2[" + value4 + "] version[" + value3 + "]");
                this.postFusionRunnable(true);
            }
            else {
                GpsDeadReckoningManager.sLogger.v("Fusion Done len[" + short1 + "] fusionMode[" + value2 + "] initStatus1[" + value + "] initStatus2[" + value4 + "] version[" + value3 + "]");
                this.storeVinInfoInDb();
            }
        }
        else {
            this.postFusionRunnable(true);
        }
    }
    
    private void handleNavStatus(int n, final int n2) throws Throwable {
        n += 10;
        if (n <= n2 - 1) {
            final byte b = GpsDeadReckoningManager.READ_BUF[n];
            switch (b) {
                default:
                    if (this.deadReckoningOn) {
                        this.deadReckoningOn = false;
                        this.deadReckoningType = -1;
                        TTSUtils.debugShowDREnded();
                        GpsDeadReckoningManager.sLogger.v("dead reckoning stopped:" + b);
                        GpsUtils.sendEventBroadcast("GPS_DR_STOPPED", null);
                        break;
                    }
                    break;
                case 1:
                case 4:
                    if (!this.deadReckoningOn) {
                        this.deadReckoningOn = true;
                        this.deadReckoningType = b;
                        final String drType = getDRType(b);
                        TTSUtils.debugShowDRStarted(drType);
                        GpsDeadReckoningManager.sLogger.v("dead reckoning on[" + b + "] " + drType);
                        final Bundle bundle = new Bundle();
                        String s;
                        if (b == 1) {
                            s = "DEAD_RECKONING_ONLY";
                        }
                        else {
                            s = "GPS_DEAD_RECKONING_COMBINED";
                        }
                        bundle.putString("drtype", s);
                        GpsUtils.sendEventBroadcast("GPS_DR_STARTED", bundle);
                        break;
                    }
                    if (this.deadReckoningType != b) {
                        final String drType2 = getDRType(b);
                        GpsDeadReckoningManager.sLogger.v("dead reckoning type changed from [" + this.deadReckoningType + "] to [" + b + "] " + drType2);
                        this.deadReckoningType = b;
                        TTSUtils.debugShowDRStarted(drType2);
                        final Bundle bundle2 = new Bundle();
                        String s2;
                        if (b == 1) {
                            s2 = "DEAD_RECKONING_ONLY";
                        }
                        else {
                            s2 = "GPS_DEAD_RECKONING_COMBINED";
                        }
                        bundle2.putString("drtype", s2);
                        GpsUtils.sendEventBroadcast("GPS_DR_STARTED", bundle2);
                        break;
                    }
                    break;
            }
        }
    }
    
    private void initDeadReckoning() {
        if (ObdManager.getInstance().isSpeedPidAvailable()) {
            GpsDeadReckoningManager.sLogger.v("speed pid is available");
            this.checkAlignment();
            this.startDeadReckoning();
        }
        else {
            GpsDeadReckoningManager.sLogger.v("speed pid is not available");
        }
    }
    
    private boolean invokeUblox(final CommandWriter commandWriter) {
        boolean b = false;
        try {
            Label_0080: {
                if (this.lastConnectionFailure <= 0L) {
                    break Label_0080;
                }
                final long n = SystemClock.elapsedRealtime() - this.lastConnectionFailure;
                if (n >= 10000L) {
                    break Label_0080;
                }
                if (GpsDeadReckoningManager.sLogger.isLoggable(2)) {
                    GpsDeadReckoningManager.sLogger.v("would retry time[" + n + "]");
                }
                b = false;
                return b;
            }
            if (this.connectSocket()) {
                this.lastConnectionFailure = 0L;
                commandWriter.run();
                b = true;
            }
            else {
                this.lastConnectionFailure = SystemClock.elapsedRealtime();
            }
            return b;
        }
        catch (IOException ex) {
            GpsDeadReckoningManager.sLogger.e("Failed to ");
            this.closeSocket();
            this.lastConnectionFailure = SystemClock.elapsedRealtime();
            return b;
        }
        return b;
    }
    
    private boolean invokeUblox(final byte[] array, final String s) {
        return this.invokeUblox((CommandWriter)new CommandWriter() {
            @Override
            public void run() throws IOException {
                if (GpsDeadReckoningManager.this.outputStream != null) {
                    if (s != null) {
                        GpsDeadReckoningManager.sLogger.v(s + " [" + bytesToHex(array, 0, array.length) + "]");
                    }
                    GpsDeadReckoningManager.this.outputStream.write(array);
                    return;
                }
                throw new IOException("Disconnected socket - failed to " + s);
            }
        });
    }
    
    private void postAlignmentRunnable(final boolean b) {
        this.handler.removeCallbacks(this.getAlignmentRunnable);
        this.handler.removeCallbacks(this.getAlignmentRunnableRetry);
        if (b) {
            this.handler.postDelayed(this.getAlignmentRunnable, 30000L);
        }
        else {
            this.handler.post(this.getAlignmentRunnable);
        }
    }
    
    private void postFusionRunnable(final boolean b) {
        this.handler.removeCallbacks(this.getFusionStatusRunnable);
        this.handler.removeCallbacks(this.getFusionStatusRunnableRetry);
        if (b) {
            this.handler.postDelayed(this.getFusionStatusRunnable, 10000L);
        }
        else {
            this.handler.post(this.getFusionStatusRunnable);
        }
    }
    
    private void sendAutoAlignment() throws IOException {
        if (!ObdManager.getInstance().isConnected()) {
            GpsDeadReckoningManager.sLogger.v("not connected to obd manager any more");
        }
        else {
            GpsDeadReckoningManager.sLogger.v("send auto alignment [" + bytesToHex(GpsDeadReckoningManager.AUTO_ALIGNMENT, 0, GpsDeadReckoningManager.AUTO_ALIGNMENT.length) + "]");
            this.waitForAutoAlignment = true;
            this.outputStream.write(GpsDeadReckoningManager.AUTO_ALIGNMENT);
            this.postAlignmentRunnable(true);
            AnalyticsSupport.localyticsSendEvent("GPS_Calibration_Start", new String[0]);
            TTSUtils.debugShowGpsCalibrationStarted();
        }
    }
    
    private void sendObdSpeed() throws IOException {
        final boolean loggable = GpsDeadReckoningManager.sLogger.isLoggable(2);
        final long n = this.speedManager.getRawObdSpeed();
        if (n < 0L) {
            if (loggable) {
                GpsDeadReckoningManager.sLogger.i("invalid obd speed:" + n);
            }
        }
        else {
            this.timeStampCounter += 100;
            if (this.timeStampCounter < 0) {
                this.timeStampCounter = 100;
            }
            GpsDeadReckoningManager.INJECT_OBD_SPEED[6] = (byte)(this.timeStampCounter >> 0);
            GpsDeadReckoningManager.INJECT_OBD_SPEED[7] = (byte)(this.timeStampCounter >> 8);
            GpsDeadReckoningManager.INJECT_OBD_SPEED[8] = (byte)(this.timeStampCounter >> 16);
            GpsDeadReckoningManager.INJECT_OBD_SPEED[9] = (byte)(this.timeStampCounter >> 24);
            final long n2 = (int)(SpeedManager.convertWithPrecision(n, SpeedManager.SpeedUnit.KILOMETERS_PER_HOUR, SpeedManager.SpeedUnit.METERS_PER_SECOND) * 1000.0f);
            GpsDeadReckoningManager.INJECT_OBD_SPEED[14] = (byte)(n2 >> 0);
            GpsDeadReckoningManager.INJECT_OBD_SPEED[15] = (byte)(n2 >> 8);
            GpsDeadReckoningManager.INJECT_OBD_SPEED[16] = (byte)(n2 >> 16);
            calculateChecksum(GpsDeadReckoningManager.INJECT_OBD_SPEED);
            if (loggable) {
                GpsDeadReckoningManager.sLogger.v("inject obd speed [" + bytesToHex(GpsDeadReckoningManager.INJECT_OBD_SPEED, 0, GpsDeadReckoningManager.INJECT_OBD_SPEED.length) + "]");
            }
            this.outputStream.write(GpsDeadReckoningManager.INJECT_OBD_SPEED);
        }
    }
    
    private void startDeadReckoning() {
        if (!this.deadReckoningInjectionStarted) {
            GpsDeadReckoningManager.sLogger.v("starting dead reckoning injection");
            this.deadReckoningInjectionStarted = true;
            this.handler.removeCallbacks(this.injectRunnable);
            this.handler.postDelayed(this.injectRunnable, 100L);
        }
    }
    
    private void stopDeadReckoning() {
        if (this.deadReckoningInjectionStarted) {
            this.waitForAutoAlignment = false;
            this.waitForFusionStatus = false;
            this.alignmentInfo = null;
            this.rootInfo = null;
            this.alignmentChecked = false;
            GpsDeadReckoningManager.sLogger.v("stopping dead rekoning injection");
            this.deadReckoningInjectionStarted = false;
            this.handler.removeCallbacks(this.injectRunnable);
            this.handler.removeCallbacks(this.getAlignmentRunnable);
            this.handler.removeCallbacks(this.getAlignmentRunnableRetry);
            this.handler.removeCallbacks(this.getFusionStatusRunnable);
            this.handler.removeCallbacks(this.getFusionStatusRunnableRetry);
        }
    }
    
    private void storeVinInfoInDb() {
        try {
            String vin;
            if ((vin = ObdManager.getInstance().getVin()) == null) {
                vin = "UNKNOWN_VIN";
            }
            GpsDeadReckoningManager.sLogger.v("store alignment info in db for vin[" + vin + "]");
            JSONObject jsonObject;
            if (this.rootInfo == null) {
                this.rootInfo = new JSONObject();
                jsonObject = new JSONObject();
                this.rootInfo.put("gps", jsonObject);
            }
            else {
                jsonObject = this.rootInfo.getJSONObject("gps");
            }
            jsonObject.put("yaw", String.valueOf(this.alignmentInfo.yaw));
            jsonObject.put("pitch", String.valueOf(this.alignmentInfo.pitch));
            jsonObject.put("roll", String.valueOf(this.alignmentInfo.roll));
            jsonObject.put("alignment_time", String.valueOf(System.currentTimeMillis()));
            VinInformationHelper.storeVinInfo(vin, this.rootInfo.toString());
            VinInformationHelper.getVinPreference().edit().putString("vin", vin).commit();
            GpsDeadReckoningManager.sLogger.v("store last vin pref [" + vin + "]");
            this.waitForAutoAlignment = false;
            this.waitForFusionStatus = false;
            this.alignmentChecked = true;
            AnalyticsSupport.localyticsSendEvent("GPS_Calibration_Sensor_Done", new String[0]);
            TTSUtils.debugShowGpsSensorCalibrationDone();
        }
        catch (Throwable t) {
            GpsDeadReckoningManager.sLogger.e(t);
        }
    }
    
    @Subscribe
    public void ObdConnectionStatusEvent(final ObdManager.ObdConnectionStatusEvent obdConnectionStatusEvent) {
        if (obdConnectionStatusEvent.connected) {
            GpsDeadReckoningManager.sLogger.v("got obd connected");
            this.initDeadReckoning();
        }
        else {
            GpsDeadReckoningManager.sLogger.v("got obd dis-connected");
            this.stopDeadReckoning();
        }
    }
    
    public void dumpGpsInfo(final String p0) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_2       
        //     2: aconst_null    
        //     3: astore_3       
        //     4: aload_2        
        //     5: astore          4
        //     7: new             Ljava/io/FileOutputStream;
        //    10: astore          5
        //    12: aload_2        
        //    13: astore          4
        //    15: new             Ljava/lang/StringBuilder;
        //    18: astore          6
        //    20: aload_2        
        //    21: astore          4
        //    23: aload           6
        //    25: invokespecial   java/lang/StringBuilder.<init>:()V
        //    28: aload_2        
        //    29: astore          4
        //    31: aload           5
        //    33: aload           6
        //    35: aload_1        
        //    36: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    39: getstatic       java/io/File.separator:Ljava/lang/String;
        //    42: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    45: ldc             "gps.log"
        //    47: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    50: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    53: invokespecial   java/io/FileOutputStream.<init>:(Ljava/lang/String;)V
        //    56: new             Ljava/io/PrintWriter;
        //    59: astore_2       
        //    60: new             Ljava/io/OutputStreamWriter;
        //    63: astore          4
        //    65: aload           4
        //    67: aload           5
        //    69: invokespecial   java/io/OutputStreamWriter.<init>:(Ljava/io/OutputStream;)V
        //    72: aload_2        
        //    73: aload           4
        //    75: invokespecial   java/io/PrintWriter.<init>:(Ljava/io/Writer;)V
        //    78: invokestatic    com/navdy/hud/app/obd/ObdManager.getInstance:()Lcom/navdy/hud/app/obd/ObdManager;
        //    81: astore_3       
        //    82: new             Ljava/lang/StringBuilder;
        //    85: astore          4
        //    87: aload           4
        //    89: invokespecial   java/lang/StringBuilder.<init>:()V
        //    92: aload_2        
        //    93: aload           4
        //    95: ldc_w           "obd_connected="
        //    98: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   101: aload_3        
        //   102: invokevirtual   com/navdy/hud/app/obd/ObdManager.isConnected:()Z
        //   105: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
        //   108: ldc_w           "\n"
        //   111: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   114: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   117: invokevirtual   java/io/PrintWriter.write:(Ljava/lang/String;)V
        //   120: aload_0        
        //   121: getfield        com/navdy/hud/app/device/gps/GpsDeadReckoningManager.deadReckoningInjectionStarted:Z
        //   124: ifeq            228
        //   127: aload_3        
        //   128: invokevirtual   com/navdy/hud/app/obd/ObdManager.getVin:()Ljava/lang/String;
        //   131: astore_3       
        //   132: aload_3        
        //   133: astore          4
        //   135: aload_3        
        //   136: ifnonnull       144
        //   139: ldc_w           "UNKNOWN_VIN"
        //   142: astore          4
        //   144: new             Ljava/lang/StringBuilder;
        //   147: astore_3       
        //   148: aload_3        
        //   149: invokespecial   java/lang/StringBuilder.<init>:()V
        //   152: aload_2        
        //   153: aload_3        
        //   154: ldc_w           "vin="
        //   157: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   160: aload           4
        //   162: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   165: ldc_w           "\n"
        //   168: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   171: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   174: invokevirtual   java/io/PrintWriter.write:(Ljava/lang/String;)V
        //   177: new             Ljava/lang/StringBuilder;
        //   180: astore          4
        //   182: aload           4
        //   184: invokespecial   java/lang/StringBuilder.<init>:()V
        //   187: aload           4
        //   189: ldc_w           "calibrated="
        //   192: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   195: astore_3       
        //   196: aload_0        
        //   197: getfield        com/navdy/hud/app/device/gps/GpsDeadReckoningManager.rootInfo:Lorg/json/JSONObject;
        //   200: ifnonnull       276
        //   203: ldc_w           "no"
        //   206: astore          4
        //   208: aload_2        
        //   209: aload_3        
        //   210: aload           4
        //   212: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   215: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   218: invokevirtual   java/io/PrintWriter.write:(Ljava/lang/String;)V
        //   221: aload_2        
        //   222: ldc_w           "\n"
        //   225: invokevirtual   java/io/PrintWriter.write:(Ljava/lang/String;)V
        //   228: aload_2        
        //   229: invokevirtual   java/io/PrintWriter.flush:()V
        //   232: getstatic       com/navdy/hud/app/device/gps/GpsDeadReckoningManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   235: astore_3       
        //   236: new             Ljava/lang/StringBuilder;
        //   239: astore          4
        //   241: aload           4
        //   243: invokespecial   java/lang/StringBuilder.<init>:()V
        //   246: aload_3        
        //   247: aload           4
        //   249: ldc_w           "gps log written:"
        //   252: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   255: aload_1        
        //   256: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   259: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   262: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/String;)V
        //   265: aload           5
        //   267: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
        //   270: aload           5
        //   272: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   275: return         
        //   276: aload_0        
        //   277: getfield        com/navdy/hud/app/device/gps/GpsDeadReckoningManager.rootInfo:Lorg/json/JSONObject;
        //   280: invokevirtual   org/json/JSONObject.toString:()Ljava/lang/String;
        //   283: astore          4
        //   285: goto            208
        //   288: astore          5
        //   290: aload_3        
        //   291: astore_1       
        //   292: aload_1        
        //   293: astore          4
        //   295: getstatic       com/navdy/hud/app/device/gps/GpsDeadReckoningManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   298: aload           5
        //   300: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   303: aload_1        
        //   304: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
        //   307: aload_1        
        //   308: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   311: goto            275
        //   314: astore_1       
        //   315: aload           4
        //   317: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
        //   320: aload           4
        //   322: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   325: aload_1        
        //   326: athrow         
        //   327: astore_1       
        //   328: aload           5
        //   330: astore          4
        //   332: goto            315
        //   335: astore          4
        //   337: aload           5
        //   339: astore_1       
        //   340: aload           4
        //   342: astore          5
        //   344: goto            292
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  7      12     288    292    Ljava/lang/Throwable;
        //  7      12     314    315    Any
        //  15     20     288    292    Ljava/lang/Throwable;
        //  15     20     314    315    Any
        //  23     28     288    292    Ljava/lang/Throwable;
        //  23     28     314    315    Any
        //  31     56     288    292    Ljava/lang/Throwable;
        //  31     56     314    315    Any
        //  56     132    335    347    Ljava/lang/Throwable;
        //  56     132    327    335    Any
        //  144    203    335    347    Ljava/lang/Throwable;
        //  144    203    327    335    Any
        //  208    228    335    347    Ljava/lang/Throwable;
        //  208    228    327    335    Any
        //  228    265    335    347    Ljava/lang/Throwable;
        //  228    265    327    335    Any
        //  276    285    335    347    Ljava/lang/Throwable;
        //  276    285    327    335    Any
        //  295    303    314    315    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0144:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void enableEsfRaw() {
        this.handler.post(this.enableEsfRunnable);
    }
    
    public String getDeadReckoningStatus() {
        final Resources resources = HudApplication.getAppContext().getResources();
        final ObdManager instance = ObdManager.getInstance();
        String s;
        if (!instance.isConnected()) {
            s = resources.getString(R.string.obd_not_connected);
        }
        else if (!instance.isSpeedPidAvailable()) {
            s = resources.getString(R.string.obd_no_speed_pid);
        }
        else if (this.deadReckoningInjectionStarted) {
            if (this.rootInfo != null) {
                s = resources.getString(R.string.gps_calibrated) + " " + this.rootInfo.toString();
            }
            else if (this.waitForAutoAlignment) {
                s = resources.getString(R.string.gps_calibrating_sensor);
            }
            else if (this.waitForFusionStatus) {
                s = resources.getString(R.string.gps_calibrating_fusion);
            }
            else {
                s = resources.getString(R.string.gps_calibration_unknown);
            }
        }
        else {
            s = resources.getString(R.string.gps_calibration_unknown);
        }
        return s;
    }
    
    @Subscribe
    public void onDrivingStateChange(final DrivingStateChange drivingStateChange) {
        if (!drivingStateChange.driving) {
            this.sensorDataProcessor.setCalibrated(false);
        }
    }
    
    @Subscribe
    public void onSupportedPidEventsChange(final ObdManager.ObdSupportedPidsChangedEvent obdSupportedPidsChangedEvent) {
        this.initDeadReckoning();
    }
    
    @Override
    public void run() {
        GpsDeadReckoningManager.sLogger.v("start thread");
        while (this.runThread) {
            int read = 0;
            Label_0072: {
                try {
                    read = this.inputStream.read(GpsDeadReckoningManager.READ_BUF);
                    if (read != -1) {
                        break Label_0072;
                    }
                    GpsDeadReckoningManager.sLogger.v("eof");
                    this.runThread = false;
                }
                catch (Throwable t) {
                    if (!(t instanceof InterruptedException)) {
                        GpsDeadReckoningManager.sLogger.e(t);
                    }
                    this.runThread = false;
                }
                continue;
            }
            if (read > 0) {
                Label_0115: {
                    try {
                        final int index = GenericUtil.indexOf(GpsDeadReckoningManager.READ_BUF, GpsDeadReckoningManager.ESF_RAW_PATTERN, 0, read - 1);
                        if (index == -1) {
                            break Label_0115;
                        }
                        this.handleEsfRaw(index, read);
                    }
                    catch (Throwable t2) {
                        GpsDeadReckoningManager.sLogger.e(t2);
                    }
                    continue;
                }
                final int index2 = GenericUtil.indexOf(GpsDeadReckoningManager.READ_BUF, GpsDeadReckoningManager.NAV_STATUS_PATTERN, 0, read - 1);
                if (index2 != -1) {
                    this.handleNavStatus(index2, read);
                }
                else {
                    if (!this.alignmentChecked) {
                        final int index3 = GenericUtil.indexOf(GpsDeadReckoningManager.READ_BUF, GpsDeadReckoningManager.ALIGNMENT_PATTERN, 0, read - 1);
                        if (index3 != -1) {
                            try {
                                this.handleAlignment(index3, read);
                            }
                            catch (Throwable t3) {
                                GpsDeadReckoningManager.sLogger.e(t3);
                                this.postAlignmentRunnable(true);
                            }
                            continue;
                        }
                    }
                    if (!this.waitForFusionStatus) {
                        continue;
                    }
                    final int index4 = GenericUtil.indexOf(GpsDeadReckoningManager.READ_BUF, GpsDeadReckoningManager.ESF_STATUS_PATTERN, 0, read - 1);
                    if (index4 == -1) {
                        continue;
                    }
                    try {
                        this.handleFusion(index4, read);
                    }
                    catch (Throwable t4) {
                        GpsDeadReckoningManager.sLogger.e(t4);
                        this.postFusionRunnable(true);
                    }
                }
            }
        }
        GpsDeadReckoningManager.sLogger.v("end thread");
    }
    
    public void sendWarmReset() {
        this.handler.postAtFrontOfQueue(this.resetRunnable);
        this.handler.postDelayed(this.enableEsfRunnable, 2000L);
    }
    
    private static class Alignment
    {
        boolean done;
        float pitch;
        float roll;
        float yaw;
        
        Alignment(final float yaw, final float pitch, final float roll, final boolean done) {
            this.yaw = yaw;
            this.pitch = pitch;
            this.roll = roll;
            this.done = done;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("Alignment{");
            sb.append("yaw=").append(this.yaw);
            sb.append(", pitch=").append(this.pitch);
            sb.append(", roll=").append(this.roll);
            sb.append(", done=").append(this.done);
            sb.append('}');
            return sb.toString();
        }
    }
    
    interface CommandWriter
    {
        void run() throws IOException;
    }
}
