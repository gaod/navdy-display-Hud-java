package com.navdy.hud.app.event;

import com.navdy.service.library.events.audio.SpeechRequest;

public class LocalSpeechRequest
{
    public SpeechRequest speechRequest;
    
    public LocalSpeechRequest(final SpeechRequest speechRequest) {
        this.speechRequest = speechRequest;
    }
}
