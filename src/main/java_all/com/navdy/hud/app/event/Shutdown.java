package com.navdy.hud.app.event;

import android.os.Bundle;

public class Shutdown
{
    public static final String EXTRA_SHUTDOWN_CAUSE = "SHUTDOWN_CAUSE";
    public final Reason reason;
    public final State state;
    
    public Shutdown(final Reason reason) {
        this.reason = reason;
        State state;
        if (reason.requireConfirmation) {
            state = State.CONFIRMATION;
        }
        else {
            state = State.CONFIRMED;
        }
        this.state = state;
    }
    
    public Shutdown(final Reason reason, final State state) {
        this.reason = reason;
        this.state = state;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Shutdown{");
        sb.append("reason=").append(this.reason);
        sb.append(", state=").append(this.state);
        sb.append('}');
        return sb.toString();
    }
    
    public enum Reason
    {
        ACCELERATE_SHUTDOWN("Accelerate_Shutdown", true), 
        CRITICAL_VOLTAGE("Critical_Voltage", false), 
        DIAL_OTA("Dial_OTA", false), 
        ENGINE_OFF("Engine_Off", true), 
        FACTORY_RESET("Factory_Reset", false), 
        FORCED_UPDATE("Forced_Update", false), 
        HIGH_TEMPERATURE("High_Temperature", false), 
        INACTIVITY("Inactivity", true), 
        LOW_VOLTAGE("Low_Voltage", false), 
        MENU("Menu", true), 
        OTA("OTA", false), 
        POWER_BUTTON("Power_Button", true), 
        POWER_LOSS("Power_Loss", false), 
        TIMEOUT("Timeout", false), 
        UNKNOWN("Unknown", false);
        
        public final String attr;
        public final boolean requireConfirmation;
        
        private Reason(final String attr, final boolean requireConfirmation) {
            this.attr = attr;
            this.requireConfirmation = requireConfirmation;
        }
        
        public Bundle asBundle() {
            final Bundle bundle = new Bundle();
            bundle.putString("SHUTDOWN_CAUSE", this.toString());
            return bundle;
        }
    }
    
    public enum State
    {
        CANCELED, 
        CONFIRMATION, 
        CONFIRMED, 
        SHUTTING_DOWN;
    }
}
