package com.navdy.hud.app.analytics;

import kotlin.text.CharsKt__CharJVMKt;
import kotlin.ReplaceWith;
import kotlin.Deprecated;
import kotlin.text.StringsKt__StringsKt$splitToSequence;
import kotlin.collections.CollectionsKt__IterablesKt;
import java.util.ArrayList;
import kotlin.text.MatchResult;
import kotlin.jvm.functions.Function1;
import kotlin.text.StringsKt__StringsKt$rangesDelimitedBy;
import kotlin.jvm.functions.Function2;
import kotlin.text.DelimitedRangesSequence;
import kotlin.sequences.SequencesKt___SequencesKt;
import kotlin.sequences.Sequence;
import kotlin.text.StringsKt__StringsKt$iterator;
import kotlin.collections.CharIterator;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.ranges.IntProgression;
import kotlin.ranges.RangesKt___RangesKt;
import kotlin.ranges.IntRange;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.internal.InlineOnly;
import kotlin.text.Regex;
import kotlin.text.CharsKt__CharKt;
import org.jetbrains.annotations.Nullable;
import java.util.Collection;
import kotlin.text.StringsKt__StringsJVMKt;
import java.util.Iterator;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;
import android.text.TextUtils;
import java.util.HashSet;
import com.navdy.hud.app.event.Wakeup;
import kotlin.Pair;
import java.util.List;
import com.navdy.hud.app.event.InitEvents;
import com.navdy.hud.app.device.gps.GpsUtils;
import com.navdy.hud.app.device.gps.CalibratedGForceData;
import com.navdy.hud.app.util.ConversionUtil;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.obd.ObdManager;
import android.os.Looper;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.manager.SpeedManager;
import android.os.SystemClock;
import kotlin.jvm.internal.Intrinsics;
import com.navdy.hud.app.util.os.SystemProperties;
import kotlin.jvm.internal.DefaultConstructorMarker;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.framework.trips.TripManager;
import android.content.SharedPreferences;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import com.navdy.hud.app.device.PowerManager;
import android.os.Handler;
import org.jetbrains.annotations.NotNull;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000¤\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 D2\u00020\u0001:\u0002DEB-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0010\u0010&\u001a\u00020#2\u0006\u0010'\u001a\u00020(H\u0007J\u0010\u0010)\u001a\u00020#2\u0006\u0010'\u001a\u00020*H\u0007J\u0010\u0010+\u001a\u00020#2\u0006\u0010,\u001a\u00020-H\u0007J\u0010\u0010.\u001a\u00020#2\u0006\u0010/\u001a\u000200H\u0007J\u0010\u00101\u001a\u00020#2\u0006\u0010'\u001a\u000202H\u0007J\u0010\u00103\u001a\u00020#2\u0006\u0010'\u001a\u000204H\u0007J\u0010\u00105\u001a\u00020#2\u0006\u00106\u001a\u000207H\u0007J\u0010\u00108\u001a\u00020#2\u0006\u00109\u001a\u00020:H\u0007J\u0010\u0010;\u001a\u00020#2\u0006\u0010<\u001a\u00020=H\u0007J\b\u0010>\u001a\u00020#H\u0002J\u0006\u0010?\u001a\u00020#J\u0010\u0010@\u001a\u00020#2\b\b\u0002\u0010A\u001a\u00020BJ\b\u0010C\u001a\u00020#H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u00020\u000eX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u0015\u001a\u00020\u0016X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u001a\u0010\u001b\u001a\u00020\u001cX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010!\u001a\b\u0012\u0004\u0012\u00020#0\"X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006F" }, d2 = { "Lcom/navdy/hud/app/analytics/TelemetryDataManager;", "", "uiStateManager", "Lcom/navdy/hud/app/ui/framework/UIStateManager;", "powerManager", "Lcom/navdy/hud/app/device/PowerManager;", "bus", "Lcom/squareup/otto/Bus;", "sharedPreferences", "Landroid/content/SharedPreferences;", "tripManager", "Lcom/navdy/hud/app/framework/trips/TripManager;", "(Lcom/navdy/hud/app/ui/framework/UIStateManager;Lcom/navdy/hud/app/device/PowerManager;Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Lcom/navdy/hud/app/framework/trips/TripManager;)V", "driveScoreUpdatedEvent", "Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;", "getDriveScoreUpdatedEvent", "()Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;", "setDriveScoreUpdatedEvent", "(Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;)V", "handler", "Landroid/os/Handler;", "highAccuracySpeedAvailable", "", "getHighAccuracySpeedAvailable", "()Z", "setHighAccuracySpeedAvailable", "(Z)V", "lastDriveScore", "", "getLastDriveScore", "()I", "setLastDriveScore", "(I)V", "reportRunnable", "Lkotlin/Function0;", "", "updateDriveScoreRunnable", "Ljava/lang/Runnable;", "GPSSpeedChangeEvent", "event", "Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;", "ObdPidChangeEvent", "Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;", "onCalibratedGForceData", "calibratedGForceData", "Lcom/navdy/hud/app/device/gps/CalibratedGForceData;", "onGpsSwitched", "gpsSwitch", "Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;", "onInit", "Lcom/navdy/hud/app/event/InitEvents$InitPhase;", "onMapEvent", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "onObdConnectionStateChanged", "connectionStateChangeEvent", "Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;", "onSpeedDataExpired", "speedDataExpired", "Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataExpired;", "onWakeup", "wakeUpEvent", "Lcom/navdy/hud/app/event/Wakeup;", "reportTelemetryData", "startTelemetrySession", "updateDriveScore", "interestingEvent", "Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;", "updateSpeed", "Companion", "DriveScoreUpdated", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class TelemetryDataManager
{
    private static final long ANALYTICS_REPORTING_INTERVAL = 300000L;
    public static final Companion Companion;
    private static final long DRIVE_SCORE_PUBLISH_INTERVAL = 3000L;
    private static final boolean LOG_TELEMETRY_DATA;
    private static final float MAX_ACCEL = 2.0f;
    private static final String PREFERENCE_TROUBLE_CODES = "PREFRENCE_TROUBLE_CODES";
    private static final Logger sLogger;
    private final Bus bus;
    @NotNull
    private DriveScoreUpdated driveScoreUpdatedEvent;
    private Handler handler;
    private boolean highAccuracySpeedAvailable;
    private int lastDriveScore;
    private final PowerManager powerManager;
    private final Function0<Unit> reportRunnable;
    private final SharedPreferences sharedPreferences;
    private final TripManager tripManager;
    private final UIStateManager uiStateManager;
    private final Runnable updateDriveScoreRunnable;
    
    static {
        Companion = new Companion();
        sLogger = new Logger("TelemetryDataManager");
        LOG_TELEMETRY_DATA = SystemProperties.getBoolean("persist.sys.prop.log.telemetry", false);
    }
    
    public TelemetryDataManager(@NotNull final UIStateManager uiStateManager, @NotNull final PowerManager powerManager, @NotNull final Bus bus, @NotNull final SharedPreferences sharedPreferences, @NotNull final TripManager tripManager) {
        Intrinsics.checkParameterIsNotNull(uiStateManager, "uiStateManager");
        Intrinsics.checkParameterIsNotNull(powerManager, "powerManager");
        Intrinsics.checkParameterIsNotNull(bus, "bus");
        Intrinsics.checkParameterIsNotNull(sharedPreferences, "sharedPreferences");
        Intrinsics.checkParameterIsNotNull(tripManager, "tripManager");
        this.uiStateManager = uiStateManager;
        this.powerManager = powerManager;
        this.bus = bus;
        this.sharedPreferences = sharedPreferences;
        this.tripManager = tripManager;
        this.reportRunnable = (Function0<Unit>)new TelemetryDataManager$reportRunnable.TelemetryDataManager$reportRunnable$1(this);
        TelemetrySession.INSTANCE.setDataSource((TelemetrySession.DataSource)new TelemetrySession.DataSource() {
            final /* synthetic */ TelemetryDataManager this$0;
            
            @Override
            public long absoluteCurrentTime() {
                return SystemClock.elapsedRealtime();
            }
            
            @NotNull
            @Override
            public RawSpeed currentSpeed() {
                final RawSpeed currentSpeedInMetersPerSecond = SpeedManager.getInstance().getCurrentSpeedInMetersPerSecond();
                Intrinsics.checkExpressionValueIsNotNull(currentSpeedInMetersPerSecond, "SpeedManager.getInstance…entSpeedInMetersPerSecond");
                return currentSpeedInMetersPerSecond;
            }
            
            @Override
            public void interestingEventDetected(@NotNull final InterestingEvent interestingEvent) {
                Intrinsics.checkParameterIsNotNull(interestingEvent, "event");
                this.this$0.updateDriveScore(interestingEvent);
            }
            
            @Override
            public boolean isHighAccuracySpeedAvailable() {
                return this.this$0.getHighAccuracySpeedAvailable();
            }
            
            @Override
            public boolean isVerboseLoggingNeeded() {
                return !DeviceUtil.isUserBuild() && Intrinsics.areEqual(TelemetryDataManager.access$getUiStateManager$p(this.this$0).getHomescreenView().getDisplayMode(), HomeScreen.DisplayMode.SMART_DASH) && TelemetryDataManager.access$getUiStateManager$p(this.this$0).getSmartDashView().isShowingDriveScoreGauge;
            }
            
            @Override
            public long totalDistanceTravelledWithMeters() {
                return TelemetryDataManager.access$getTripManager$p(this.this$0).getTotalDistanceTravelledWithNavdy();
            }
        });
        this.bus.register(this);
        this.handler = new Handler(Looper.getMainLooper());
        this.updateDriveScoreRunnable = (Runnable)new TelemetryDataManager$updateDriveScoreRunnable.TelemetryDataManager$updateDriveScoreRunnable$1(this);
        this.driveScoreUpdatedEvent = new DriveScoreUpdated(TelemetrySession.InterestingEvent.NONE, false, false, false, 100);
    }
    
    public static final /* synthetic */ long access$getANALYTICS_REPORTING_INTERVAL$cp() {
        return TelemetryDataManager.ANALYTICS_REPORTING_INTERVAL;
    }
    
    public static final /* synthetic */ long access$getDRIVE_SCORE_PUBLISH_INTERVAL$cp() {
        return TelemetryDataManager.DRIVE_SCORE_PUBLISH_INTERVAL;
    }
    
    public static final /* synthetic */ boolean access$getLOG_TELEMETRY_DATA$cp() {
        return TelemetryDataManager.LOG_TELEMETRY_DATA;
    }
    
    public static final /* synthetic */ float access$getMAX_ACCEL$cp() {
        return TelemetryDataManager.MAX_ACCEL;
    }
    
    @NotNull
    public static final /* synthetic */ String access$getPREFERENCE_TROUBLE_CODES$cp() {
        return TelemetryDataManager.PREFERENCE_TROUBLE_CODES;
    }
    
    @NotNull
    public static final /* synthetic */ Logger access$getSLogger$cp() {
        return TelemetryDataManager.sLogger;
    }
    
    @NotNull
    public static final /* synthetic */ TripManager access$getTripManager$p(final TelemetryDataManager telemetryDataManager) {
        return telemetryDataManager.tripManager;
    }
    
    @NotNull
    public static final /* synthetic */ UIStateManager access$getUiStateManager$p(final TelemetryDataManager telemetryDataManager) {
        return telemetryDataManager.uiStateManager;
    }
    
    private final void reportTelemetryData() {
        if (TelemetryDataManager.Companion.getLOG_TELEMETRY_DATA()) {
            TelemetryDataManager.Companion.getSLogger().d("Distance = " + TelemetrySession.INSTANCE.getSessionDistance() + "," + ("Duration = " + TelemetrySessionKt.milliSecondsToSeconds(TelemetrySession.INSTANCE.getSessionDuration()) + ", ") + ("RollingDuration = " + TelemetrySessionKt.milliSecondsToSeconds(TelemetrySession.INSTANCE.getSessionRollingDuration()) + ", ") + ("AverageSpeed = " + TelemetrySession.INSTANCE.getSessionAverageSpeed() + ", ") + ("AverageRollingSpeed = " + TelemetrySession.INSTANCE.getSessionAverageRollingSpeed() + ", ") + ("MaxSpeed = " + TelemetrySession.INSTANCE.getSessionMaxSpeed() + ", ") + ("HardBrakingCount = " + TelemetrySession.INSTANCE.getSessionHardBrakingCount() + ", ") + ("HardAccelerationCount = " + TelemetrySession.INSTANCE.getSessionHardAccelerationCount() + ", ") + ("SpeedingPercentage = " + TelemetrySession.INSTANCE.getSessionSpeedingPercentage() + ",") + ("TroubleCodeCount = " + TelemetrySession.INSTANCE.getSessionTroubleCodeCount() + ",") + ("TroubleCodesReport = " + TelemetrySession.INSTANCE.getSessionTroubleCodesReport()) + ("MaxRPM = " + TelemetrySession.INSTANCE.getMaxRpm() + ","));
            TelemetryDataManager.Companion.getSLogger().d("EVENT , averageMpg = " + TelemetrySession.INSTANCE.getEventAverageMpg() + "," + ("averageRPM = " + TelemetrySession.INSTANCE.getEventAverageRpm() + ", ") + ("Fuel level = " + ObdManager.getInstance().getFuelLevel() + ", ") + ("Engine Temperature = " + ObdManager.getInstance().getPidValue(5) + ", "));
        }
        AnalyticsSupport.recordVehicleTelemetryData(TelemetrySession.INSTANCE.getSessionDistance(), TelemetrySessionKt.milliSecondsToSeconds(TelemetrySession.INSTANCE.getSessionDuration()), TelemetrySessionKt.milliSecondsToSeconds(TelemetrySession.INSTANCE.getSessionRollingDuration()), TelemetrySession.INSTANCE.getSessionAverageSpeed(), TelemetrySession.INSTANCE.getSessionAverageRollingSpeed(), TelemetrySession.INSTANCE.getSessionMaxSpeed(), TelemetrySession.INSTANCE.getSessionHardBrakingCount(), TelemetrySession.INSTANCE.getSessionHardAccelerationCount(), TelemetrySession.INSTANCE.getSessionSpeedingPercentage(), TelemetrySession.INSTANCE.getSessionExcessiveSpeedingPercentage(), TelemetrySession.INSTANCE.getSessionTroubleCodeCount(), TelemetrySession.INSTANCE.getSessionTroubleCodesReport(), (int)TelemetrySession.INSTANCE.getSessionAverageMpg(), TelemetrySession.INSTANCE.getMaxRpm(), (int)TelemetrySession.INSTANCE.getEventAverageMpg(), TelemetrySession.INSTANCE.getEventAverageRpm(), ObdManager.getInstance().getFuelLevel(), (float)ObdManager.getInstance().getPidValue(5), TelemetrySession.INSTANCE.getMaxG(), TelemetrySession.INSTANCE.getMaxGAngle(), TelemetrySession.INSTANCE.getSessionHighGCount());
        TelemetrySession.INSTANCE.eventReported();
        final Handler handler = this.handler;
        Runnable runnable = new(com.navdy.hud.app.analytics.TelemetryDataManagerKt$sam$Runnable$cc6381d8.class);
        final Function0<Unit> reportRunnable = this.reportRunnable;
        if (reportRunnable == null) {
            runnable = null;
        }
        else {
            new TelemetryDataManagerKt$sam$Runnable$cc6381d8(reportRunnable);
        }
        handler.postDelayed((Runnable)runnable, TelemetryDataManager.Companion.getANALYTICS_REPORTING_INTERVAL());
    }
    
    private final void updateSpeed() {
        if (TelemetrySession.INSTANCE.getSessionStarted()) {
            final TelemetrySession instance = TelemetrySession.INSTANCE;
            final RawSpeed currentSpeedInMetersPerSecond = SpeedManager.getInstance().getCurrentSpeedInMetersPerSecond();
            Intrinsics.checkExpressionValueIsNotNull(currentSpeedInMetersPerSecond, "SpeedManager.getInstance…entSpeedInMetersPerSecond");
            instance.setCurrentSpeed(currentSpeedInMetersPerSecond);
        }
    }
    
    @Subscribe
    public final void GPSSpeedChangeEvent(@NotNull final MapEvents.GPSSpeedEvent gpsSpeedEvent) {
        Intrinsics.checkParameterIsNotNull(gpsSpeedEvent, "event");
        this.updateSpeed();
    }
    
    @Subscribe
    public final void ObdPidChangeEvent(@NotNull final ObdManager.ObdPidChangeEvent obdPidChangeEvent) {
        Intrinsics.checkParameterIsNotNull(obdPidChangeEvent, "event");
        switch (obdPidChangeEvent.pid.getId()) {
            case 13:
                this.updateSpeed();
                break;
            case 256:
                TelemetrySession.INSTANCE.setCurrentMpg(ConversionUtil.convertLpHundredKmToMPG(obdPidChangeEvent.pid.getValue()));
                break;
            case 12:
                TelemetrySession.INSTANCE.setCurrentRpm((int)obdPidChangeEvent.pid.getValue());
                break;
        }
    }
    
    @NotNull
    public final DriveScoreUpdated getDriveScoreUpdatedEvent() {
        return this.driveScoreUpdatedEvent;
    }
    
    public final boolean getHighAccuracySpeedAvailable() {
        return this.highAccuracySpeedAvailable;
    }
    
    public final int getLastDriveScore() {
        return this.lastDriveScore;
    }
    
    @Subscribe
    public final void onCalibratedGForceData(@NotNull final CalibratedGForceData calibratedGForceData) {
        Intrinsics.checkParameterIsNotNull(calibratedGForceData, "calibratedGForceData");
        if (TelemetrySession.INSTANCE.getSessionStarted()) {
            TelemetrySession.INSTANCE.setGForce(calibratedGForceData.getXAccel(), calibratedGForceData.getYAccel(), calibratedGForceData.getZAccel());
        }
    }
    
    @Subscribe
    public final void onGpsSwitched(@NotNull final GpsUtils.GpsSwitch gpsSwitch) {
        Intrinsics.checkParameterIsNotNull(gpsSwitch, "gpsSwitch");
        TelemetryDataManager.Companion.getSLogger().d("onGpsSwitched " + gpsSwitch);
        TelemetrySession.INSTANCE.speedSourceChanged();
    }
    
    @Subscribe
    public final void onInit(@NotNull final InitEvents.InitPhase initPhase) {
        Intrinsics.checkParameterIsNotNull(initPhase, "event");
        final InitEvents.Phase phase = initPhase.phase;
        if (phase != null) {
            switch (TelemetryDataManager$WhenMappings.$EnumSwitchMapping$0[phase.ordinal()]) {
                case 1:
                    TelemetryDataManager.Companion.getSLogger().i("Post Start , Quiet mode : " + this.powerManager.inQuietMode());
                    if (!this.powerManager.inQuietMode()) {
                        this.startTelemetrySession();
                        break;
                    }
                    break;
            }
        }
    }
    
    @Subscribe
    public final void onMapEvent(@NotNull final MapEvents.ManeuverDisplay maneuverDisplay) {
        Intrinsics.checkParameterIsNotNull(maneuverDisplay, "event");
        TelemetrySession.INSTANCE.setCurrentSpeedLimit(maneuverDisplay.currentSpeedLimit);
    }
    
    @Subscribe
    public final void onObdConnectionStateChanged(@NotNull final ObdManager.ObdConnectionStatusEvent obdConnectionStatusEvent) {
        Intrinsics.checkParameterIsNotNull(obdConnectionStatusEvent, "connectionStateChangeEvent");
        TelemetrySession.INSTANCE.speedSourceChanged();
        if (obdConnectionStatusEvent.connected) {
            ObdManager.getInstance().getSupportedPids();
            this.highAccuracySpeedAvailable = true;
            final String string = this.sharedPreferences.getString(TelemetryDataManager.Companion.getPREFERENCE_TROUBLE_CODES(), "");
            final List<String> troubleCodes = ObdManager.getInstance().getTroubleCodes();
            if (Intrinsics.areEqual(troubleCodes, null)) {
                this.sharedPreferences.edit().putString(TelemetryDataManager.Companion.getPREFERENCE_TROUBLE_CODES(), "").apply();
                TelemetrySession.INSTANCE.setSessionTroubleCodesReport("");
            }
            else {
                final Companion companion = TelemetryDataManager.Companion;
                Intrinsics.checkExpressionValueIsNotNull(string, "troubleCodesString");
                final Pair<String, String> serializeTroubleCodes = companion.serializeTroubleCodes(troubleCodes, string);
                final String s = serializeTroubleCodes.component1();
                final String sessionTroubleCodesReport = serializeTroubleCodes.component2();
                this.sharedPreferences.edit().putString(TelemetryDataManager.Companion.getPREFERENCE_TROUBLE_CODES(), s).apply();
                TelemetrySession.INSTANCE.setSessionTroubleCodesReport(sessionTroubleCodesReport);
            }
        }
        else {
            this.highAccuracySpeedAvailable = false;
        }
    }
    
    @Subscribe
    public final void onSpeedDataExpired(@NotNull final SpeedManager.SpeedDataExpired speedDataExpired) {
        Intrinsics.checkParameterIsNotNull(speedDataExpired, "speedDataExpired");
        TelemetrySession.INSTANCE.speedSourceChanged();
        this.updateSpeed();
    }
    
    @Subscribe
    public final void onWakeup(@NotNull final Wakeup wakeup) {
        Intrinsics.checkParameterIsNotNull(wakeup, "wakeUpEvent");
        this.startTelemetrySession();
    }
    
    public final void setDriveScoreUpdatedEvent(@NotNull final DriveScoreUpdated driveScoreUpdatedEvent) {
        Intrinsics.checkParameterIsNotNull(driveScoreUpdatedEvent, "set");
        this.driveScoreUpdatedEvent = driveScoreUpdatedEvent;
    }
    
    public final void setHighAccuracySpeedAvailable(final boolean highAccuracySpeedAvailable) {
        this.highAccuracySpeedAvailable = highAccuracySpeedAvailable;
    }
    
    public final void setLastDriveScore(final int lastDriveScore) {
        this.lastDriveScore = lastDriveScore;
    }
    
    public final void startTelemetrySession() {
        if (!TelemetrySession.INSTANCE.getSessionStarted()) {
            TelemetryDataManager.Companion.getSLogger().i("Starting telemetry session");
            TelemetrySession.INSTANCE.startSession();
            final Handler handler = this.handler;
            Runnable runnable = new(com.navdy.hud.app.analytics.TelemetryDataManagerKt$sam$Runnable$cc6381d8.class);
            final Function0<Unit> reportRunnable = this.reportRunnable;
            if (reportRunnable == null) {
                runnable = null;
            }
            else {
                new TelemetryDataManagerKt$sam$Runnable$cc6381d8(reportRunnable);
            }
            handler.removeCallbacks((Runnable)runnable);
            final Handler handler2 = this.handler;
            Runnable runnable2 = new(com.navdy.hud.app.analytics.TelemetryDataManagerKt$sam$Runnable$cc6381d8.class);
            final Function0<Unit> reportRunnable2 = this.reportRunnable;
            if (reportRunnable2 == null) {
                runnable2 = null;
            }
            else {
                new TelemetryDataManagerKt$sam$Runnable$cc6381d8(reportRunnable2);
            }
            handler2.postDelayed((Runnable)runnable2, TelemetryDataManager.Companion.getANALYTICS_REPORTING_INTERVAL());
            this.handler.postDelayed(this.updateDriveScoreRunnable, TelemetryDataManager.Companion.getDRIVE_SCORE_PUBLISH_INTERVAL());
        }
        else {
            TelemetryDataManager.Companion.getSLogger().d("Telemetry session is already started");
        }
    }
    
    public final void updateDriveScore(@NotNull final TelemetrySession.InterestingEvent interestingEvent) {
        Intrinsics.checkParameterIsNotNull(interestingEvent, "interestingEvent");
        final TelemetrySession instance = TelemetrySession.INSTANCE;
        double n;
        if (instance.getSessionRollingDuration() > 0) {
            n = (instance.getSessionHardAccelerationCount() + instance.getSessionHardBrakingCount() + instance.getSessionHighGCount()) * 540.0 / Math.sqrt(instance.getSessionRollingDuration() / 1000.0f + 60);
        }
        else {
            n = 0.0;
        }
        final float n2 = instance.getSessionSpeedingPercentage() * 50;
        final float n3 = instance.getSessionExcessiveSpeedingPercentage() * 50;
        final int lastDriveScore = (int)Math.ceil(Math.max(0.0, 100 - (n2 + n + n3)));
        this.driveScoreUpdatedEvent = new DriveScoreUpdated(interestingEvent, instance.isSpeeding(), instance.isExcessiveSpeeding(), instance.isDoingHighGManeuver(), lastDriveScore);
        if ((Intrinsics.areEqual(interestingEvent, TelemetrySession.InterestingEvent.NONE) ^ true) || lastDriveScore != this.lastDriveScore) {
            TelemetryDataManager.Companion.getSLogger().d("HA : " + instance.getSessionHardAccelerationCount() + " , HB : " + instance.getSessionHardBrakingCount() + " , HG : " + instance.getSessionHighGCount());
            TelemetryDataManager.Companion.getSLogger().d("Speeding percentage : " + instance.getSessionSpeedingPercentage());
            TelemetryDataManager.Companion.getSLogger().d("Excessive Speeding percentage : " + instance.getSessionExcessiveSpeedingPercentage());
            TelemetryDataManager.Companion.getSLogger().d("Hard Acceleration Factor : " + n + " , Speeding Factor : " + n2 + " , excessive Speeding factor " + n3);
            TelemetryDataManager.Companion.getSLogger().d("DriveScoreUpdate : " + this.driveScoreUpdatedEvent);
            this.bus.post(this.driveScoreUpdatedEvent);
        }
        this.lastDriveScore = lastDriveScore;
        this.handler.removeCallbacks(this.updateDriveScoreRunnable);
        this.handler.postDelayed(this.updateDriveScoreRunnable, TelemetryDataManager.Companion.getDRIVE_SCORE_PUBLISH_INTERVAL());
    }
    
    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J(\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u00120\u001a2\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00120\u001c2\u0006\u0010\u001d\u001a\u00020\u0012R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082D¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0082D¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006R\u0014\u0010\t\u001a\u00020\nX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\u00020\u000eX\u0082D¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u0012X\u0082D¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0014\u0010\u0015\u001a\u00020\u0016X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018¨\u0006\u001e" }, d2 = { "Lcom/navdy/hud/app/analytics/TelemetryDataManager$Companion;", "", "()V", "ANALYTICS_REPORTING_INTERVAL", "", "getANALYTICS_REPORTING_INTERVAL", "()J", "DRIVE_SCORE_PUBLISH_INTERVAL", "getDRIVE_SCORE_PUBLISH_INTERVAL", "LOG_TELEMETRY_DATA", "", "getLOG_TELEMETRY_DATA", "()Z", "MAX_ACCEL", "", "getMAX_ACCEL", "()F", "PREFERENCE_TROUBLE_CODES", "", "getPREFERENCE_TROUBLE_CODES", "()Ljava/lang/String;", "sLogger", "Lcom/navdy/service/library/log/Logger;", "getSLogger", "()Lcom/navdy/service/library/log/Logger;", "serializeTroubleCodes", "Lkotlin/Pair;", "troubleCodes", "", "savedTroubleCodesStringFromPreviousSession", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    public static final class Companion
    {
        private final long getANALYTICS_REPORTING_INTERVAL() {
            return TelemetryDataManager.access$getANALYTICS_REPORTING_INTERVAL$cp();
        }
        
        private final long getDRIVE_SCORE_PUBLISH_INTERVAL() {
            return TelemetryDataManager.access$getDRIVE_SCORE_PUBLISH_INTERVAL$cp();
        }
        
        private final boolean getLOG_TELEMETRY_DATA() {
            return TelemetryDataManager.access$getLOG_TELEMETRY_DATA$cp();
        }
        
        private final float getMAX_ACCEL() {
            return TelemetryDataManager.access$getMAX_ACCEL$cp();
        }
        
        private final String getPREFERENCE_TROUBLE_CODES() {
            return TelemetryDataManager.access$getPREFERENCE_TROUBLE_CODES$cp();
        }
        
        private final Logger getSLogger() {
            return TelemetryDataManager.access$getSLogger$cp();
        }
        
        @NotNull
        public final Pair<String, String> serializeTroubleCodes(@NotNull final List<String> list, @NotNull String string) {
            Intrinsics.checkParameterIsNotNull(list, "troubleCodes");
            Intrinsics.checkParameterIsNotNull(string, "savedTroubleCodesStringFromPreviousSession");
            final HashSet<String> set = new HashSet<String>();
            if (!TextUtils.isEmpty((CharSequence)string)) {
                final Iterator<String> iterator = StringsKt__StringsKt.split$default((CharSequence)string, new String[] { " " }, false, 0, 6, null).iterator();
                while (iterator.hasNext()) {
                    set.add(iterator.next());
                }
            }
            final StringBuilder sb = new StringBuilder();
            final StringBuilder sb2 = new StringBuilder();
            if (list.size() > 0) {
                TelemetrySession.INSTANCE.setSessionTroubleCodeCount(list.size());
                for (final String s : list) {
                    if (!set.contains(s)) {
                        sb2.append(s + " ");
                    }
                    sb.append(s + " ");
                }
            }
            final String string2 = sb.toString();
            if (string2 == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
            }
            final String string3 = StringsKt__StringsKt.trim((CharSequence)string2).toString();
            string = sb2.toString();
            if (string == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
            }
            return new Pair<String, String>(string3, StringsKt__StringsKt.trim((CharSequence)string).toString());
        }
    }
    
    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0018\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\t\u0010\u0018\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0019\u001a\u00020\u0005HÆ\u0003J\t\u0010\u001a\u001a\u00020\u0005HÆ\u0003J\t\u0010\u001b\u001a\u00020\u0005HÆ\u0003J\t\u0010\u001c\u001a\u00020\tHÆ\u0003J;\u0010\u001d\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\tHÆ\u0001J\u0013\u0010\u001e\u001a\u00020\u00052\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010 \u001a\u00020\tHÖ\u0001J\t\u0010!\u001a\u00020\"HÖ\u0001R\u001a\u0010\b\u001a\u00020\tX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u001a\u0010\u0007\u001a\u00020\u0005X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001a\u0010\u0006\u001a\u00020\u0005X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0013\"\u0004\b\u0016\u0010\u0015R\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0004\u0010\u0013\"\u0004\b\u0017\u0010\u0015¨\u0006#" }, d2 = { "Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;", "", "interestingEvent", "Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;", "isSpeeding", "", "isExcessiveSpeeding", "isDoingHighGManeuver", "driveScore", "", "(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;ZZZI)V", "getDriveScore", "()I", "setDriveScore", "(I)V", "getInterestingEvent", "()Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;", "setInterestingEvent", "(Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;)V", "()Z", "setDoingHighGManeuver", "(Z)V", "setExcessiveSpeeding", "setSpeeding", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "other", "hashCode", "toString", "", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    public static final class DriveScoreUpdated
    {
        private int driveScore;
        @NotNull
        private TelemetrySession.InterestingEvent interestingEvent;
        private boolean isDoingHighGManeuver;
        private boolean isExcessiveSpeeding;
        private boolean isSpeeding;
        
        public DriveScoreUpdated(@NotNull final TelemetrySession.InterestingEvent interestingEvent, final boolean isSpeeding, final boolean isExcessiveSpeeding, final boolean isDoingHighGManeuver, final int driveScore) {
            Intrinsics.checkParameterIsNotNull(interestingEvent, "interestingEvent");
            this.interestingEvent = interestingEvent;
            this.isSpeeding = isSpeeding;
            this.isExcessiveSpeeding = isExcessiveSpeeding;
            this.isDoingHighGManeuver = isDoingHighGManeuver;
            this.driveScore = driveScore;
        }
        
        @NotNull
        public final TelemetrySession.InterestingEvent component1() {
            return this.interestingEvent;
        }
        
        public final boolean component2() {
            return this.isSpeeding;
        }
        
        public final boolean component3() {
            return this.isExcessiveSpeeding;
        }
        
        public final boolean component4() {
            return this.isDoingHighGManeuver;
        }
        
        public final int component5() {
            return this.driveScore;
        }
        
        @NotNull
        public final DriveScoreUpdated copy(@NotNull final TelemetrySession.InterestingEvent interestingEvent, final boolean b, final boolean b2, final boolean b3, final int n) {
            Intrinsics.checkParameterIsNotNull(interestingEvent, "interestingEvent");
            return new DriveScoreUpdated(interestingEvent, b, b2, b3, n);
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = false;
            if (this != o) {
                boolean b2 = b;
                if (!(o instanceof DriveScoreUpdated)) {
                    return b2;
                }
                final DriveScoreUpdated driveScoreUpdated = (DriveScoreUpdated)o;
                b2 = b;
                if (!Intrinsics.areEqual(this.interestingEvent, driveScoreUpdated.interestingEvent)) {
                    return b2;
                }
                int n;
                if (this.isSpeeding == driveScoreUpdated.isSpeeding) {
                    n = 1;
                }
                else {
                    n = 0;
                }
                b2 = b;
                if (n == 0) {
                    return b2;
                }
                int n2;
                if (this.isExcessiveSpeeding == driveScoreUpdated.isExcessiveSpeeding) {
                    n2 = 1;
                }
                else {
                    n2 = 0;
                }
                b2 = b;
                if (n2 == 0) {
                    return b2;
                }
                int n3;
                if (this.isDoingHighGManeuver == driveScoreUpdated.isDoingHighGManeuver) {
                    n3 = 1;
                }
                else {
                    n3 = 0;
                }
                b2 = b;
                if (n3 == 0) {
                    return b2;
                }
                int n4;
                if (this.driveScore == driveScoreUpdated.driveScore) {
                    n4 = 1;
                }
                else {
                    n4 = 0;
                }
                b2 = b;
                if (n4 == 0) {
                    return b2;
                }
            }
            return true;
        }
        
        public final int getDriveScore() {
            return this.driveScore;
        }
        
        @NotNull
        public final TelemetrySession.InterestingEvent getInterestingEvent() {
            return this.interestingEvent;
        }
        
        @Override
        public int hashCode() {
            int n = 1;
            final TelemetrySession.InterestingEvent interestingEvent = this.interestingEvent;
            int hashCode;
            if (interestingEvent != null) {
                hashCode = interestingEvent.hashCode();
            }
            else {
                hashCode = 0;
            }
            int isSpeeding;
            if ((isSpeeding = (this.isSpeeding ? 1 : 0)) != 0) {
                isSpeeding = 1;
            }
            int isExcessiveSpeeding;
            if ((isExcessiveSpeeding = (this.isExcessiveSpeeding ? 1 : 0)) != 0) {
                isExcessiveSpeeding = 1;
            }
            final int isDoingHighGManeuver = this.isDoingHighGManeuver ? 1 : 0;
            if (isDoingHighGManeuver == 0) {
                n = isDoingHighGManeuver;
            }
            return ((isExcessiveSpeeding + (isSpeeding + hashCode * 31) * 31) * 31 + n) * 31 + this.driveScore;
        }
        
        public final boolean isDoingHighGManeuver() {
            return this.isDoingHighGManeuver;
        }
        
        public final boolean isExcessiveSpeeding() {
            return this.isExcessiveSpeeding;
        }
        
        public final boolean isSpeeding() {
            return this.isSpeeding;
        }
        
        public final void setDoingHighGManeuver(final boolean isDoingHighGManeuver) {
            this.isDoingHighGManeuver = isDoingHighGManeuver;
        }
        
        public final void setDriveScore(final int driveScore) {
            this.driveScore = driveScore;
        }
        
        public final void setExcessiveSpeeding(final boolean isExcessiveSpeeding) {
            this.isExcessiveSpeeding = isExcessiveSpeeding;
        }
        
        public final void setInterestingEvent(@NotNull final TelemetrySession.InterestingEvent interestingEvent) {
            Intrinsics.checkParameterIsNotNull(interestingEvent, "set");
            this.interestingEvent = interestingEvent;
        }
        
        public final void setSpeeding(final boolean isSpeeding) {
            this.isSpeeding = isSpeeding;
        }
        
        @Override
        public String toString() {
            return "DriveScoreUpdated(interestingEvent=" + this.interestingEvent + ", isSpeeding=" + this.isSpeeding + ", isExcessiveSpeeding=" + this.isExcessiveSpeeding + ", isDoingHighGManeuver=" + this.isDoingHighGManeuver + ", driveScore=" + this.driveScore + ")";
        }
    }
}
