package com.navdy.hud.app.analytics;

import android.os.SystemClock;
import com.navdy.service.library.log.Logger;

public class NavigationQualityTracker
{
    private static final int INVALID = -1;
    private static final NavigationQualityTracker instance;
    private static final Logger logger;
    private boolean isRoutingWithTraffic;
    private Report report;
    private long tripDurationIntervalInit;
    private long tripStartUtc;
    
    static {
        logger = new Logger(NavigationQualityTracker.class);
        instance = new NavigationQualityTracker();
    }
    
    private NavigationQualityTracker() {
        this.tripStartUtc = -1L;
        this.tripDurationIntervalInit = -1L;
    }
    
    public static NavigationQualityTracker getInstance() {
        return NavigationQualityTracker.instance;
    }
    
    public void cancelTrip() {
        synchronized (this) {
            this.tripStartUtc = -1L;
            this.tripDurationIntervalInit = -1L;
            this.isRoutingWithTraffic = false;
            this.report = null;
        }
    }
    
    public int getActualDurationSoFar() {
        int n;
        if (this.tripDurationIntervalInit != -1L) {
            n = (int)((SystemClock.elapsedRealtime() - this.tripDurationIntervalInit) / 1000L);
        }
        else {
            n = -1;
        }
        return n;
    }
    
    public int getExpectedDistance() {
        int n;
        if (this.report == null) {
            n = -1;
        }
        else {
            n = (int)this.report.expectedDistance;
        }
        return n;
    }
    
    public int getExpectedDuration() {
        int n;
        if (this.report == null) {
            n = -1;
        }
        else {
            n = (int)this.report.expectedDuration;
        }
        return n;
    }
    
    public long getTripStartUtc() {
        return this.tripStartUtc;
    }
    
    public void trackCalculationWithTraffic(final boolean isRoutingWithTraffic) {
        synchronized (this) {
            this.isRoutingWithTraffic = isRoutingWithTraffic;
        }
    }
    
    public void trackTripEnded(final long actualDistance) {
        synchronized (this) {
            if (this.report == null) {
                NavigationQualityTracker.logger.w("report is null, no-op");
            }
            else {
                final long currentTimeMillis = System.currentTimeMillis();
                NavigationQualityTracker.logger.v("tripEndUtc: " + currentTimeMillis);
                NavigationQualityTracker.logger.v("tripEndUtc - tripStartUtc, should be same as actualDuration: " + (currentTimeMillis - this.tripStartUtc));
                this.report.actualDuration = (SystemClock.elapsedRealtime() - this.tripDurationIntervalInit) / 1000L;
                this.report.actualDistance = actualDistance;
                NavigationQualityTracker.logger.i("route has ended, submitting navigation quality report: " + this.report);
                if (this.report.isValid()) {
                    AnalyticsSupport.submitNavigationQualityReport(this.report);
                }
                this.report = null;
            }
        }
    }
    
    public void trackTripRecalculated() {
        synchronized (this) {
            if (this.report == null) {
                NavigationQualityTracker.logger.w("report is null, no-op");
            }
            else {
                final Report report = this.report;
                ++report.nRecalculations;
            }
        }
    }
    
    public void trackTripStarted(final long n, final long n2, final long n3) {
        synchronized (this) {
            NavigationQualityTracker.logger.i("trip started, init new navigation quality report");
            this.tripStartUtc = System.currentTimeMillis();
            this.tripDurationIntervalInit = SystemClock.elapsedRealtime();
            NavigationQualityTracker.logger.v("tripStartUtc: " + this.tripStartUtc);
            this.report = new Report(n, n2, n3, this.isRoutingWithTraffic);
        }
    }
    
    static class Report
    {
        private static final double HEAVY_TRAFFIC_RATIO = 1.25;
        private static final double MODERATE_TRAFFIC_RATIO = 1.1;
        long actualDistance;
        long actualDuration;
        final long expectedDistance;
        final long expectedDuration;
        int nRecalculations;
        final TrafficLevel trafficLevel;
        
        private Report(final long expectedDuration, final long n, final long expectedDistance, final boolean b) {
            this.expectedDuration = expectedDuration;
            this.expectedDistance = expectedDistance;
            this.nRecalculations = 0;
            if (b) {
                final double n2 = expectedDuration / n;
                if (n2 < 1.1) {
                    this.trafficLevel = TrafficLevel.LIGHT;
                }
                else if (n2 >= 1.1 && n2 < 1.25) {
                    this.trafficLevel = TrafficLevel.MODERATE;
                }
                else {
                    this.trafficLevel = TrafficLevel.HEAVY;
                }
            }
            else {
                this.trafficLevel = TrafficLevel.NO_DATA;
            }
        }
        
        double getDistanceVariance() {
            return 100L * (this.actualDistance - this.expectedDistance) / this.expectedDuration;
        }
        
        double getDurationVariance() {
            return 100L * (this.actualDuration - this.expectedDuration) / this.expectedDuration;
        }
        
        public boolean isValid() {
            return this.expectedDistance > 0L && this.expectedDuration > 0L && this.actualDistance > 0L && this.actualDuration > 0L;
        }
        
        @Override
        public String toString() {
            return "Report{trafficLevel=" + this.trafficLevel + ", expectedDuration=" + this.expectedDuration + ", expectedDistance=" + this.expectedDistance + ", actualDuration=" + this.actualDuration + ", actualDistance=" + this.actualDistance + ", nRecalculations=" + this.nRecalculations + '}';
        }
    }
    
    enum TrafficLevel
    {
        HEAVY, 
        LIGHT, 
        MODERATE, 
        NO_DATA;
    }
}
