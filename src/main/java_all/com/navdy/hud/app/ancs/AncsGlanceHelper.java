package com.navdy.hud.app.ancs;

import com.navdy.hud.app.ui.component.UISettings;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.service.library.events.glances.MessageConstants;
import java.util.StringTokenizer;
import android.text.TextUtils;
import com.navdy.service.library.events.glances.GenericConstants;
import com.navdy.service.library.events.glances.SocialConstants;
import com.navdy.service.library.events.glances.EmailConstants;
import java.util.List;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.service.library.events.glances.CalendarConstants;
import com.navdy.service.library.events.glances.KeyValue;
import java.util.ArrayList;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.service.library.events.glances.GlanceEvent;
import java.util.Date;
import com.navdy.service.library.log.Logger;

public class AncsGlanceHelper
{
    private static Logger sLogger;
    
    static {
        AncsGlanceHelper.sLogger = AncsServiceConnector.sLogger;
    }
    
    public static GlanceEvent buildAppleCalendarEvent(final String s, String id, final String s2, final String s3, final String s4, final Date date) {
        id = GlanceHelper.getId();
        final ArrayList<KeyValue> list = new ArrayList<KeyValue>();
        if (s2 != null) {
            list.add(new KeyValue(CalendarConstants.CALENDAR_TITLE.name(), s2));
        }
        if (s4 != null) {
            list.add(new KeyValue(CalendarConstants.CALENDAR_LOCATION.name(), s4));
        }
        list.add(new KeyValue(CalendarConstants.CALENDAR_TIME_STR.name(), RemoteDeviceManager.getInstance().getTimeHelper().formatTime(date, null)));
        list.add(new KeyValue(CalendarConstants.CALENDAR_TIME.name(), String.valueOf(date.getTime())));
        return new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_CALENDAR).provider(s).id(id).postTime(System.currentTimeMillis()).glanceData(list).build();
    }
    
    public static GlanceEvent buildAppleMailEvent(final String s, String id, final String s2, final String s3, final String s4, final Date date) {
        boolean b = false;
        if (s2.indexOf("@") != -1) {
            b = true;
        }
        id = GlanceHelper.getId();
        final ArrayList<KeyValue> list = new ArrayList<KeyValue>();
        if (b) {
            list.add(new KeyValue(EmailConstants.EMAIL_FROM_EMAIL.name(), s2));
        }
        else {
            list.add(new KeyValue(EmailConstants.EMAIL_FROM_NAME.name(), s2));
        }
        if (s3 != null) {
            list.add(new KeyValue(EmailConstants.EMAIL_SUBJECT.name(), s3));
        }
        if (s4 == null) {
            return new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_EMAIL).provider(s).id(id).postTime(date.getTime()).glanceData(list).build();
        }
        if (!s4.toLowerCase().contains("this message has no content")) {
            list.add(new KeyValue(EmailConstants.EMAIL_BODY.name(), s4));
            return new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_EMAIL).provider(s).id(id).postTime(date.getTime()).glanceData(list).build();
        }
        AncsGlanceHelper.sLogger.v("invalid apple mail glance:" + s4);
        return null;
        build = new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_EMAIL).provider(s).id(id).postTime(date.getTime()).glanceData(list).build();
        return build;
    }
    
    public static GlanceEvent buildFacebookEvent(final String s, String id, final String s2, final String s3, final String s4, final Date date) {
        id = GlanceHelper.getId();
        final ArrayList<KeyValue> list = new ArrayList<KeyValue>();
        list.add(new KeyValue(SocialConstants.SOCIAL_MESSAGE.name(), s4));
        return new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_SOCIAL).provider(s).id(id).postTime(date.getTime()).glanceData(list).build();
    }
    
    public static GlanceEvent buildFacebookMessengerEvent(final String s, final String s2, final String s3, final String s4, final String s5, final Date date) {
        return buildMessageEvent(s, s5, date);
    }
    
    public static GlanceEvent buildGenericEvent(final String s, final String s2, final String s3, String id, final String s4, final Date date) {
        id = GlanceHelper.getId();
        final ArrayList<KeyValue> list = new ArrayList<KeyValue>();
        if (s3 != null) {
            list.add(new KeyValue(GenericConstants.GENERIC_TITLE.name(), s3));
        }
        if (s4 != null) {
            list.add(new KeyValue(GenericConstants.GENERIC_MESSAGE.name(), s4));
        }
        return new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_GENERIC).provider(s).id(id).postTime(date.getTime()).glanceData(list).build();
    }
    
    public static GlanceEvent buildGenericMailEvent(final String s, final String s2, final String s3, final String s4, final String s5, final Date date) {
        GlanceEvent buildOutlookMailEvent = null;
        if (!TextUtils.isEmpty((CharSequence)s5) && "com.microsoft.Office.Outlook".equals(s)) {
            buildOutlookMailEvent = buildOutlookMailEvent(s, s2, s3, s4, s5, date);
        }
        return buildOutlookMailEvent;
    }
    
    public static GlanceEvent buildGoogleCalendarEvent(final String s, String trim, String trim2, String trim3, String id, final Date date) {
        GlanceEvent build;
        if (TextUtils.isEmpty((CharSequence)id)) {
            build = null;
        }
        else {
            trim = null;
            trim2 = null;
            trim3 = null;
            final StringTokenizer stringTokenizer = new StringTokenizer(id, "\n");
            int n = 0;
            while (stringTokenizer.hasMoreElements()) {
                final String s2 = (String)stringTokenizer.nextElement();
                switch (n) {
                    case 0:
                        trim = s2.trim();
                        break;
                    case 1:
                        trim2 = s2.trim();
                        break;
                    case 2:
                        trim3 = s2.trim();
                        break;
                }
                ++n;
            }
            AncsGlanceHelper.sLogger.v("[ancs-gcalendar] title[" + trim + "] when[" + trim2 + "] location[" + trim3 + "]");
            id = GlanceHelper.getId();
            final ArrayList<KeyValue> list = new ArrayList<KeyValue>();
            if (trim != null) {
                list.add(new KeyValue(CalendarConstants.CALENDAR_TITLE.name(), trim));
            }
            if (trim2 != null) {
                list.add(new KeyValue(CalendarConstants.CALENDAR_TIME_STR.name(), trim2));
            }
            list.add(new KeyValue(CalendarConstants.CALENDAR_TIME.name(), String.valueOf(date.getTime())));
            if (trim3 != null) {
                list.add(new KeyValue(CalendarConstants.CALENDAR_LOCATION.name(), trim3));
            }
            build = new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_CALENDAR).provider(s).id(id).postTime(date.getTime()).glanceData(list).build();
        }
        return build;
    }
    
    public static GlanceEvent buildGoogleHangoutEvent(final String s, final String s2, final String s3, final String s4, final String s5, final Date date) {
        return buildMessageEvent(s, s5, date);
    }
    
    public static GlanceEvent buildGoogleMailEvent(final String s, String trim, String s2, String trim2, String id, final Date date) {
        GlanceEvent build;
        if (TextUtils.isEmpty((CharSequence)id)) {
            build = null;
        }
        else {
            trim = null;
            boolean b = false;
            final boolean b2 = false;
            trim2 = null;
            final int index = id.indexOf("\n");
            s2 = id;
            if (index != -1) {
                trim = id.substring(0, index).trim();
                b = b2;
                if (trim.indexOf("@") != -1) {
                    b = true;
                }
                s2 = id.substring(index + 1);
            }
            final int index2 = s2.indexOf("•");
            if (index2 != -1) {
                trim2 = s2.substring(0, index2).trim();
                s2 = s2.substring(index2 + 1).trim();
            }
            else {
                s2 = s2.trim();
            }
            AncsGlanceHelper.sLogger.v("[ancs-gmail] from[" + trim + "] subject[" + trim2 + "] body[" + s2 + "]");
            id = GlanceHelper.getId();
            final ArrayList<KeyValue> list = new ArrayList<KeyValue>();
            if (trim != null) {
                if (b) {
                    list.add(new KeyValue(EmailConstants.EMAIL_FROM_EMAIL.name(), trim));
                }
                else {
                    list.add(new KeyValue(EmailConstants.EMAIL_FROM_NAME.name(), trim));
                }
            }
            if (trim2 != null) {
                list.add(new KeyValue(EmailConstants.EMAIL_SUBJECT.name(), trim2));
            }
            if (s2 != null) {
                list.add(new KeyValue(EmailConstants.EMAIL_BODY.name(), s2));
            }
            build = new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_EMAIL).provider(s).id(id).postTime(date.getTime()).glanceData(list).build();
        }
        return build;
    }
    
    private static GlanceEvent buildMessageEvent(final String s, String s2, final Date date) {
        GlanceEvent build;
        if (TextUtils.isEmpty((CharSequence)s2)) {
            build = null;
        }
        else {
            String trim = null;
            final int index = s2.indexOf(":");
            if (index != -1) {
                trim = s2.substring(0, index).trim();
                s2 = s2.substring(index + 1).trim();
            }
            else {
                s2 = s2.trim();
            }
            final String id = GlanceHelper.getId();
            final ArrayList<KeyValue> list = new ArrayList<KeyValue>();
            list.add(new KeyValue(MessageConstants.MESSAGE_FROM.name(), trim));
            list.add(new KeyValue(MessageConstants.MESSAGE_BODY.name(), s2));
            build = new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).provider(s).id(id).postTime(date.getTime()).glanceData(list).build();
        }
        return build;
    }
    
    public static GlanceEvent buildOutlookMailEvent(final String s, String s2, final String s3, String s4, String id, final Date date) {
        final String s5 = null;
        s2 = null;
        s4 = s5;
        if (!TextUtils.isEmpty((CharSequence)id)) {
            final String[] split = id.split("\n");
            if (split.length > 1) {
                s4 = split[0];
                s2 = split[1];
            }
            else {
                s2 = split[0];
                s4 = s5;
            }
        }
        AncsGlanceHelper.sLogger.v("[ancs-outlook-email] from[" + s3 + "] subject[" + s4 + "] body[" + s2 + "]");
        id = GlanceHelper.getId();
        final ArrayList<KeyValue> list = new ArrayList<KeyValue>();
        if (!TextUtils.isEmpty((CharSequence)s3)) {
            if (s3.contains("@")) {
                list.add(new KeyValue(EmailConstants.EMAIL_FROM_EMAIL.name(), s3));
            }
            else {
                list.add(new KeyValue(EmailConstants.EMAIL_FROM_NAME.name(), s3));
            }
        }
        if (!TextUtils.isEmpty((CharSequence)s4)) {
            list.add(new KeyValue(EmailConstants.EMAIL_SUBJECT.name(), s4));
        }
        if (!TextUtils.isEmpty((CharSequence)s2)) {
            list.add(new KeyValue(EmailConstants.EMAIL_BODY.name(), s2));
        }
        return new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_EMAIL).provider(s).id(id).postTime(date.getTime()).glanceData(list).build();
    }
    
    public static GlanceEvent buildSlackEvent(final String s, final String s2, final String s3, final String s4, final String s5, final Date date) {
        return buildMessageEvent(s, s5, date);
    }
    
    public static GlanceEvent buildTwitterEvent(final String s, String s2, String s3, String trim, final String s4, final Date date) {
        GlanceEvent build;
        if (TextUtils.isEmpty((CharSequence)s4)) {
            build = null;
        }
        else {
            final String s5 = null;
            final String s6 = null;
            s2 = null;
            final int index = s4.indexOf(":");
            final String id = GlanceHelper.getId();
            final ArrayList<KeyValue> list = new ArrayList<KeyValue>();
            String trim2;
            if (index != -1) {
                s2 = s4.substring(0, index).trim();
                if (!TextUtils.isEmpty((CharSequence)s2)) {
                    final int index2 = s2.indexOf("@");
                    s3 = s2;
                    if (index2 != -1) {
                        s3 = s2.substring(index2).trim();
                    }
                }
                trim2 = s4.substring(index + 1).trim();
                final int index3 = trim2.indexOf(" ");
                if (index3 != -1) {
                    trim = trim2.substring(0, index3).trim();
                    s2 = trim2.substring(index3 + 1).trim();
                }
                else {
                    s2 = trim2.trim();
                    trim = s6;
                }
            }
            else {
                final int index4 = s4.indexOf("\n");
                if (index4 != -1) {
                    s3 = s5;
                    trim = s6;
                    trim2 = s4;
                    if (index4 != -1) {
                        s3 = s4.substring(0, index4).trim();
                        s2 = s4.substring(index4 + 1).trim();
                        trim = s6;
                        trim2 = s4;
                    }
                }
                else {
                    final int index5 = s4.indexOf(" ");
                    if (index5 != -1) {
                        final String trim3 = s4.substring(0, index5).trim();
                        s2 = s4.substring(index5 + 1).trim();
                        s3 = s5;
                        trim = trim3;
                        trim2 = s4;
                    }
                    else {
                        s2 = s4.trim();
                        s3 = s5;
                        trim = s6;
                        trim2 = s4;
                    }
                }
            }
            if (s3 != null) {
                list.add(new KeyValue(SocialConstants.SOCIAL_FROM.name(), s3));
            }
            if (trim != null) {
                list.add(new KeyValue(SocialConstants.SOCIAL_TO.name(), trim));
            }
            if (s2 != null) {
                list.add(new KeyValue(SocialConstants.SOCIAL_MESSAGE.name(), s2));
            }
            else {
                list.add(new KeyValue(SocialConstants.SOCIAL_MESSAGE.name(), trim2));
            }
            build = new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_SOCIAL).provider(s).id(id).postTime(date.getTime()).glanceData(list).build();
        }
        return build;
    }
    
    public static GlanceEvent buildWhatsappEvent(final String s, final String s2, final String s3, String id, final String s4, final Date date) {
        id = GlanceHelper.getId();
        final ArrayList<KeyValue> list = new ArrayList<KeyValue>();
        list.add(new KeyValue(MessageConstants.MESSAGE_FROM.name(), s3));
        list.add(new KeyValue(MessageConstants.MESSAGE_BODY.name(), s4));
        return new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).provider(s).id(id).postTime(date.getTime()).glanceData(list).build();
    }
    
    public static GlanceEvent buildiMessageEvent(final String s, final String s2, final String s3, String id, final String s4, final Date date) {
        id = GlanceHelper.getId();
        final ArrayList<KeyValue> list = new ArrayList<KeyValue>();
        if (ContactUtil.isValidNumber(s3)) {
            list.add(new KeyValue(MessageConstants.MESSAGE_FROM_NUMBER.name(), s3));
        }
        else {
            list.add(new KeyValue(MessageConstants.MESSAGE_FROM.name(), s3));
        }
        list.add(new KeyValue(MessageConstants.MESSAGE_BODY.name(), s4));
        final GlanceEvent.Builder glanceData = new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).provider(s).id(id).postTime(date.getTime()).glanceData(list);
        if (UISettings.supportsIosSms()) {
            final ArrayList<GlanceEvent.GlanceActions> list2 = new ArrayList<GlanceEvent.GlanceActions>(1);
            list2.add(GlanceEvent.GlanceActions.REPLY);
            glanceData.actions(list2);
        }
        return glanceData.build();
    }
}
