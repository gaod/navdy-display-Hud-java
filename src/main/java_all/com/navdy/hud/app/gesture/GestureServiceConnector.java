package com.navdy.hud.app.gesture;

import com.navdy.hud.app.util.os.SystemProperties;
import java.nio.CharBuffer;
import java.nio.ByteBuffer;
import com.navdy.hud.app.event.Wakeup;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.service.library.events.preferences.InputPreferences;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.task.TaskManager;
import java.io.InputStream;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import java.util.ArrayList;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.input.Gesture;
import java.io.IOException;
import android.os.SystemClock;
import com.navdy.service.library.util.SystemUtils;
import android.net.LocalSocketAddress;
import android.net.LocalSocketAddress;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import java.nio.charset.Charset;
import android.net.LocalSocket;
import com.navdy.hud.app.device.PowerManager;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import java.nio.charset.CharsetEncoder;

public class GestureServiceConnector
{
    private static final String AUTO_EXPOSURE_OFF = "autoexpo off";
    private static final String AUTO_EXPOSURE_ON = "autoexpo on";
    private static final String CALIBRATE_MASK = "calibrate-mask";
    private static final String CALIBRATE_MASK_FORCE = "calibrate-mask-force";
    private static final String CALIBRATE_SAVE_SNAPSHOT_ON = "calibrate-save-snapshot on";
    private static final String DEV_SOCKET_SWIPED = "/dev/socket/swiped";
    private static final String EXPOSURE = "exposure";
    private static final String FORMAT = "format";
    private static final String GAIN = "gain";
    public static final String GESTURE_ENABLED = "gesture.enabled";
    public static final String GESTURE_LEFT = "gesture left";
    public static final String GESTURE_RIGHT = "gesture right";
    public static final String GESTURE_VERSION = "persist.sys.gesture.version";
    public static final String LEFT = "left";
    public static final char LEFT_FIRST_CHAR = 'l';
    public static final int MAX_RECORD_TIME = 6000;
    private static final int MINIMUM_INTERVAL_BETWEEN_SNAPSHOTS = 5000;
    private static final String NOTIFY = "notify";
    private static final int PID_CHECK_INTERVAL = 10000;
    public static final String RECORDING_SAVED = "recording-saved:";
    public static final String RECORD_SAVE = "record save";
    public static final String RIGHT = "right";
    public static final char RIGHT_FIRST_CHAR = 'r';
    public static final String SNAPSHOT = "snapshot ";
    public static final String SNAPSHOT_PATH = "/data/misc/swiped/camera.png";
    private static final String SOCKET_NAME = "swiped";
    private static final int SO_TIMEOUT = 2000;
    public static final String SWIPED_DISABLE_CALIBRATION = "calibration pause";
    public static final String SWIPED_ENABLE_CALIBRATION = "calibration resume";
    private static final String SWIPED_PROCESS_NAME = "/system/xbin/swiped";
    public static final String SWIPED_RECORD_MODE_ONE_SHOT = "record-mode oneshot";
    public static final String SWIPED_RECORD_MODE_ROLLING = "record-mode rolling";
    public static final String SWIPED_START_RECORDING = "record on";
    public static final String SWIPED_STOP_RECORDING = "record off";
    public static final String SWIPE_PROGRESS = "swipe-progress:";
    public static final int SWIPE_PROGRESS_COMMAND_DATA_OFFSET;
    public static final String SWIPE_PROGRESS_LEFT = "swipe-progress: left,";
    public static final String SWIPE_PROGRESS_RIGHT = "swipe-progress: right,";
    public static final String SWIPE_PROGRESS_UNKNOWN = "swipe-progress: unknown,";
    public static final String UNKNOWN = "unknown";
    private static final CharsetEncoder encoder;
    private static final Logger sLogger;
    private static final byte[] temp;
    private Bus bus;
    private long lastSnapshotTime;
    private PowerManager powerManager;
    private String recordingPath;
    private volatile boolean running;
    private boolean shuttingDown;
    private volatile Thread swipedReader;
    private volatile LocalSocket swipedSocket;
    private String vin;
    
    static {
        sLogger = new Logger(GestureServiceConnector.class);
        SWIPE_PROGRESS_COMMAND_DATA_OFFSET = "swipe-progress:".length();
        encoder = Charset.defaultCharset().newEncoder();
        temp = new byte[1024];
    }
    
    public GestureServiceConnector(final Bus bus, final PowerManager powerManager) {
        this.lastSnapshotTime = 0L;
        this.vin = null;
        this.bus = bus;
        this.powerManager = powerManager;
        bus.register(this);
    }
    
    private void closeSocket() {
        if (this.swipedSocket == null) {
            return;
        }
        Closeable closeable = null;
        while (true) {
            try {
                closeable = this.swipedSocket.getInputStream();
                IOUtils.closeStream(closeable);
                closeable = null;
                try {
                    closeable = this.swipedSocket.getOutputStream();
                    IOUtils.closeStream(closeable);
                    IOUtils.closeStream((Closeable)this.swipedSocket);
                    this.swipedSocket = null;
                    GestureServiceConnector.sLogger.v("swipedconnector socket closed");
                }
                catch (Throwable t) {}
            }
            catch (Throwable t2) {
                continue;
            }
            break;
        }
    }
    
    private void closeThread() {
        if (this.swipedReader == null) {
            return;
        }
        Label_0058: {
            if (!this.swipedReader.isAlive()) {
                break Label_0058;
            }
            GestureServiceConnector.sLogger.v("swipedconnector thread alive");
            this.swipedReader.interrupt();
            GestureServiceConnector.sLogger.v("swipedconnector thread waiting");
            while (true) {
                try {
                    this.swipedReader.join();
                    GestureServiceConnector.sLogger.v("swipedconnector thread waited");
                    this.swipedReader = null;
                }
                catch (Throwable t) {
                    continue;
                }
                break;
            }
        }
    }
    
    private Error communicateWithSwipeDaemon(LocalSocket localSocket) {
        while (true) {
            int i;
            final int n = i = 0;
            while (true) {
                try {
                    i = n;
                    final LocalSocketAddress localSocketAddress = new LocalSocketAddress("swiped", LocalSocketAddress$Namespace.RESERVED);
                    i = n;
                    localSocket.connect(localSocketAddress);
                    i = n;
                    this.sendCommand("notify");
                    final int n2 = 1;
                    final int n3 = 1;
                    i = n2;
                    this.sendCommand("calibration resume");
                    i = n2;
                    this.sendCommand("record off");
                    i = n2;
                    this.sendCommand("record-mode rolling");
                    i = n3;
                    if (!this.running) {
                        GestureServiceConnector.sLogger.v("swipedconnector is not running");
                        localSocket = (LocalSocket)Error.CANNOT_CONNECT;
                        return (Error)localSocket;
                    }
                }
                catch (Throwable t) {
                    GestureServiceConnector.sLogger.d("socket connect to reserved namespace failed", t);
                    try {
                        localSocket.connect(new LocalSocketAddress("/dev/socket/swiped", LocalSocketAddress$Namespace.FILESYSTEM));
                        this.sendCommand("notify");
                        i = 1;
                    }
                    catch (Throwable t5) {
                        GestureServiceConnector.sLogger.d("socket connect to filesystem namespace failed", t);
                    }
                    continue;
                }
                break;
            }
            if (i == 0) {
                GestureServiceConnector.sLogger.v("swipedconnector could not connect");
                localSocket = (LocalSocket)Error.CANNOT_CONNECT;
                return (Error)localSocket;
            }
        Label_0307_Outer:
            while (true) {
            Label_0524_Outer:
                while (true) {
                    String s = null;
                    Label_0913: {
                        Label_0821: {
                            Label_0784: {
                            Label_0747:
                                while (true) {
                                    Label_0732: {
                                    Label_0358:
                                        while (true) {
                                            try {
                                                final int nativeProcessId = SystemUtils.getNativeProcessId("/system/xbin/swiped");
                                                GestureServiceConnector.sLogger.v("swipedconnector connected");
                                                this.bus.post(new ConnectedEvent());
                                                final InputStream inputStream = localSocket.getInputStream();
                                                GestureServiceConnector.sLogger.d("read starting");
                                                localSocket = (LocalSocket)new byte[1000];
                                                GestureServiceConnector.sLogger.v("setting timeout");
                                                this.swipedSocket.setSoTimeout(2000);
                                                GestureServiceConnector.sLogger.v("set timeout");
                                                long elapsedRealtime = SystemClock.elapsedRealtime();
                                                GestureServiceConnector.sLogger.v("swipedconnector pid  [" + nativeProcessId + "]");
                                                while (true) {
                                                    if (!this.running) {
                                                        break Label_0358;
                                                    }
                                                    try {
                                                        i = inputStream.read((byte[])localSocket);
                                                        if (i < 0) {
                                                            GestureServiceConnector.sLogger.v("swipedconnector closed the socket");
                                                            localSocket = (LocalSocket)Error.COMMUNICATION_LOST;
                                                            return (Error)localSocket;
                                                        }
                                                    }
                                                    catch (IOException ex) {
                                                        if (!this.running || !"Try again".equals(ex.getMessage())) {
                                                            localSocket = (LocalSocket)Error.COMMUNICATION_LOST;
                                                            return (Error)localSocket;
                                                        }
                                                        final long elapsedRealtime2 = SystemClock.elapsedRealtime();
                                                        if (elapsedRealtime2 - elapsedRealtime <= 10000L) {
                                                            continue Label_0307_Outer;
                                                        }
                                                        i = SystemUtils.getNativeProcessId("/system/xbin/swiped");
                                                        if (i != nativeProcessId) {
                                                            GestureServiceConnector.sLogger.e("swipedconnector pid has changed from [" + nativeProcessId + "] to [" + i + "]");
                                                            localSocket = (LocalSocket)Error.SWIPED_RESTARTED;
                                                            return (Error)localSocket;
                                                        }
                                                        elapsedRealtime = elapsedRealtime2;
                                                        continue Label_0307_Outer;
                                                    }
                                                    if (i <= 1) {
                                                        continue Label_0307_Outer;
                                                    }
                                                    s = new String((byte[])localSocket, 0, i - 1);
                                                    i = -1;
                                                    switch (s.hashCode()) {
                                                        default:
                                                            switch (i) {
                                                                default:
                                                                    if (!s.startsWith("swipe-progress:") || s.length() <= GestureServiceConnector.SWIPE_PROGRESS_COMMAND_DATA_OFFSET + 1) {
                                                                        break Label_0913;
                                                                    }
                                                                    i = s.charAt(GestureServiceConnector.SWIPE_PROGRESS_COMMAND_DATA_OFFSET + 1);
                                                                    switch (i) {
                                                                        default:
                                                                            continue Label_0307_Outer;
                                                                        case 108:
                                                                            try {
                                                                                this.bus.post(new GestureProgress(GestureDirection.LEFT, Float.valueOf(s.substring("swipe-progress: left,".length()))));
                                                                            }
                                                                            catch (Throwable t2) {
                                                                                GestureServiceConnector.sLogger.e("Error parsing the progress " + s, t2);
                                                                            }
                                                                            continue Label_0307_Outer;
                                                                        case 114:
                                                                            break Label_0821;
                                                                    }
                                                                    break;
                                                                case 0:
                                                                    break Label_0747;
                                                                case 1:
                                                                    break Label_0784;
                                                            }
                                                            break;
                                                        case 2037586046:
                                                            break Label_0358;
                                                        case -1253681019:
                                                            break Label_0732;
                                                    }
                                                }
                                            }
                                            catch (Throwable t3) {
                                                GestureServiceConnector.sLogger.e("swipedconnector", t3);
                                                continue Label_0358;
                                            }
                                            break;
                                        }
                                        if (s.equals("gesture left")) {
                                            i = 0;
                                            continue;
                                        }
                                        continue;
                                    }
                                    if (s.equals("gesture right")) {
                                        i = 1;
                                        continue;
                                    }
                                    continue;
                                }
                                this.bus.post(new GestureEvent(Gesture.GESTURE_SWIPE_LEFT, 0, 0));
                                continue Label_0524_Outer;
                            }
                            this.bus.post(new GestureEvent(Gesture.GESTURE_SWIPE_RIGHT, 0, 0));
                            continue Label_0524_Outer;
                            try {
                                this.bus.post(new GestureProgress(GestureDirection.RIGHT, Float.valueOf(s.substring("swipe-progress: right,".length()))));
                                continue Label_0524_Outer;
                            }
                            catch (Throwable t4) {
                                GestureServiceConnector.sLogger.e("Error parsing the progress " + s, t4);
                                continue Label_0524_Outer;
                            }
                        }
                        continue Label_0524_Outer;
                    }
                    if (s.startsWith("recording-saved:")) {
                        this.recordingPath = s.substring("recording-saved:".length() + 1);
                        GestureServiceConnector.sLogger.i("recording saved at " + this.recordingPath);
                        synchronized (this) {
                            this.notify();
                            // monitorexit(this)
                            this.bus.post(new RecordingSaved(this.recordingPath));
                            continue Label_0524_Outer;
                        }
                    }
                    if (!s.startsWith("swiped event: ")) {
                        continue Label_0524_Outer;
                    }
                    final String[] split = s.split(" ");
                    if (split.length >= 3) {
                        final String s2 = split[2];
                        final ArrayList<String> list = new ArrayList<String>();
                        for (i = 3; i < split.length; ++i) {
                            final String[] split2 = split[i].split("=");
                            final String s3 = split2[0];
                            final String s4 = split2[1];
                            list.add(s3);
                            list.add(s4);
                        }
                        AnalyticsSupport.recordSwipedCalibration(s2, list.<String>toArray(new String[list.size()]));
                        continue Label_0524_Outer;
                    }
                    continue Label_0524_Outer;
                }
            }
        }
    }
    
    private void reStart() {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                GestureServiceConnector.this.stop();
                GenericUtil.sleep(2000);
                GestureServiceConnector.this.start();
            }
        }, 1);
    }
    
    @Subscribe
    public void ObdStateChangeEvent(final ObdManager.ObdConnectionStatusEvent obdConnectionStatusEvent) {
        if (DeviceUtil.isNavdyDevice() && obdConnectionStatusEvent.connected) {
            final String vin = ObdManager.getInstance().getVin();
            if (vin != null && !vin.equals(this.vin)) {
                this.vin = vin;
                this.sendCommandAsync("set-id " + this.vin);
            }
        }
    }
    
    public String dumpRecording() {
        synchronized (this) {
            try {
                this.sendCommand("record save");
                this.wait(6000L);
                return this.recordingPath;
            }
            catch (InterruptedException ex) {
                return null;
            }
            catch (IOException ex2) {
                return null;
            }
        }
    }
    
    public void enablePreview(final boolean b) {
    }
    
    public boolean isRunning() {
        return this.swipedSocket != null;
    }
    
    @Subscribe
    public void onDriverProfileChanged(final DriverProfileChanged driverProfileChanged) {
        if (DeviceUtil.isNavdyDevice()) {
            final InputPreferences inputPreferences = DriverProfileHelper.getInstance().getCurrentProfile().getInputPreferences();
            if (inputPreferences != null) {
                this.onInputPreferenceUpdate(inputPreferences);
            }
        }
    }
    
    @Subscribe
    public void onInputPreferenceUpdate(final InputPreferences inputPreferences) {
        if (DeviceUtil.isNavdyDevice() && !this.shuttingDown && !this.powerManager.inQuietMode()) {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    if (inputPreferences.use_gestures) {
                        if (GestureServiceConnector.this.swipedSocket != null) {
                            GestureServiceConnector.sLogger.v("already running");
                        }
                        else {
                            GestureServiceConnector.this.start();
                        }
                    }
                    else if (GestureServiceConnector.this.swipedSocket == null) {
                        GestureServiceConnector.sLogger.v("not running");
                    }
                    else {
                        GestureServiceConnector.this.stop();
                    }
                }
            }, 1);
        }
    }
    
    @Subscribe
    public void onShutdown(final Shutdown shutdown) {
        if (shutdown.state == Shutdown.State.CONFIRMED) {
            this.shuttingDown = true;
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    GestureServiceConnector.this.stop();
                }
            }, 1);
        }
    }
    
    @Subscribe
    public void onTakeSnapshot(final TakeSnapshot takeSnapshot) {
        if (DeviceUtil.isNavdyDevice()) {
            GestureServiceConnector.sLogger.d("Request to take snap shot");
            try {
                this.takeSnapShot("/data/misc/swiped/camera.png");
            }
            catch (IOException ex) {
                GestureServiceConnector.sLogger.e("Exception while taking the snapshot ", ex);
            }
        }
    }
    
    @Subscribe
    public void onWakeup(final Wakeup wakeup) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                GestureServiceConnector.this.start();
            }
        }, 1);
    }
    
    public void sendCommand(final String s) throws IOException {
        synchronized (this) {
            if (this.swipedSocket != null) {
                final ByteBuffer wrap = ByteBuffer.wrap(GestureServiceConnector.temp);
                GestureServiceConnector.encoder.encode(CharBuffer.wrap(s), wrap, true);
                wrap.put((byte)0);
                this.swipedSocket.getOutputStream().write(GestureServiceConnector.temp, 0, wrap.position());
            }
        }
    }
    
    public void sendCommand(final Object... array) throws IOException {
        if (array != null && array.length > 0) {
            final StringBuilder sb = new StringBuilder();
            for (final Object o : array) {
                if (o != null) {
                    if (o == array[array.length - 1]) {
                        sb.append(o.toString());
                    }
                    else {
                        sb.append(o.toString() + " ");
                    }
                }
            }
            this.sendCommand(sb.toString());
        }
    }
    
    public void sendCommandAsync(final String s) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    GestureServiceConnector.this.sendCommand(s);
                }
                catch (Throwable t) {
                    GestureServiceConnector.sLogger.e(t);
                }
            }
        }, 10);
    }
    
    public void setCalibrationEnabled(final boolean b) {
        Label_0013: {
            if (!b) {
                break Label_0013;
            }
            String s = "calibration resume";
            try {
                while (true) {
                    this.sendCommand(s);
                    return;
                    s = "calibration pause";
                    continue;
                }
            }
            catch (IOException ex) {
                GestureServiceConnector.sLogger.e("Exception while setting calibration state ", ex);
            }
        }
    }
    
    public void setDiscreteMode(final boolean b) {
    }
    
    public void setRecordMode(final boolean b) {
        GestureServiceConnector.sLogger.d("setRecordMode " + b);
        Label_0039: {
            if (!b) {
                break Label_0039;
            }
            String s = "record-mode oneshot";
            try {
                while (true) {
                    this.sendCommand(s);
                    return;
                    s = "record-mode rolling";
                    continue;
                }
            }
            catch (IOException ex) {
                GestureServiceConnector.sLogger.e("Exception while setting the record mode ", ex);
            }
        }
    }
    
    public void start() {
        while (true) {
            Label_0042: {
                synchronized (this) {
                    GenericUtil.checkNotOnMainThread();
                    if (DeviceUtil.isNavdyDevice()) {
                        if (!this.running) {
                            break Label_0042;
                        }
                        GestureServiceConnector.sLogger.v("swipedconnector already running");
                    }
                    return;
                }
            }
            this.closeSocket();
            this.closeThread();
            if (this.powerManager.inQuietMode()) {
                GestureServiceConnector.sLogger.v("Not starting gesture engine due to quiet mode");
                return;
            }
            this.running = true;
            String s;
            if (DeviceUtil.isUserBuild()) {
                s = "1";
            }
            else {
                s = "beta";
            }
            SystemProperties.set("gesture.enabled", SystemProperties.get("persist.sys.gesture.version", s));
            (this.swipedReader = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        if (!GestureServiceConnector.this.running) {
                            return;
                        }
                        Label_0239: {
                            try {
                                GestureServiceConnector.sLogger.v("creating socket");
                                GestureServiceConnector.this.swipedSocket = new LocalSocket(1);
                                GestureServiceConnector.sLogger.v("bind socket");
                                GestureServiceConnector.this.swipedSocket.bind(new LocalSocketAddress(""));
                                GestureServiceConnector.sLogger.v("calling communicateWithSwipeDaemon");
                                final Error access$300 = GestureServiceConnector.this.communicateWithSwipeDaemon(GestureServiceConnector.this.swipedSocket);
                                GestureServiceConnector.sLogger.v("called communicateWithSwipeDaemon");
                                if (!GestureServiceConnector.this.running) {
                                    continue;
                                }
                                GestureServiceConnector.this.closeSocket();
                                switch (access$300) {
                                    case CANNOT_CONNECT:
                                        GestureServiceConnector.sLogger.v("communicateWithSwipeDaemon  cannot connect");
                                        Thread.sleep(5000L);
                                        continue;
                                    case COMMUNICATION_LOST:
                                        break Label_0239;
                                    case SWIPED_RESTARTED:
                                        break Label_0239;
                                }
                            }
                            catch (Throwable t) {
                                GestureServiceConnector.sLogger.e(t);
                                if (!GestureServiceConnector.this.running) {
                                    continue;
                                }
                                GenericUtil.sleep(5000);
                                continue;
                                GestureServiceConnector.sLogger.v("communicateWithSwipeDaemon  swiped restarted, restart");
                                GestureServiceConnector.this.reStart();
                                GestureServiceConnector.sLogger.v("swipedconnector connect thread exit");
                                return;
                                GestureServiceConnector.sLogger.v("communicateWithSwipeDaemon  communication lost, restart");
                                GestureServiceConnector.this.reStart();
                            }
                            finally {
                                GestureServiceConnector.sLogger.v("swipedconnector connect thread exit");
                            }
                        }
                    }
                }
            })).setName("SwipedReaderThread");
            this.swipedReader.start();
            GestureServiceConnector.sLogger.v("swipedconnector connect thread started");
        }
    }
    
    public void startRecordingVideo() {
        try {
            GestureServiceConnector.sLogger.d("startRecordingVideo");
            this.sendCommand("record on");
        }
        catch (IOException ex) {}
    }
    
    public void stop() {
        synchronized (this) {
            GenericUtil.checkNotOnMainThread();
            if (DeviceUtil.isNavdyDevice() && this.running) {
                this.running = false;
                SystemProperties.set("gesture.enabled", "0");
                this.closeSocket();
                this.closeThread();
            }
        }
    }
    
    public void stopRecordingVideo() {
        GestureServiceConnector.sLogger.d("stopRecordingVideo");
        try {
            this.sendCommand("record off");
        }
        catch (IOException ex) {}
    }
    
    public void takeSnapShot(final String s) throws IOException {
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        if (elapsedRealtime - this.lastSnapshotTime > 5000L) {
            this.lastSnapshotTime = elapsedRealtime;
            this.sendCommand("snapshot " + s);
        }
    }
    
    public static class ConnectedEvent
    {
    }
    
    private enum Error
    {
        CANNOT_CONNECT, 
        COMMUNICATION_LOST, 
        SWIPED_RESTARTED;
    }
    
    public enum GestureDirection
    {
        LEFT, 
        RIGHT, 
        UNKNOWN;
    }
    
    public static class GestureProgress
    {
        public GestureDirection direction;
        public float progress;
        
        public GestureProgress(final GestureDirection direction, final float progress) {
            this.direction = direction;
            this.progress = progress;
        }
    }
    
    public static class RecordingSaved
    {
        public String path;
        
        public RecordingSaved(final String path) {
            this.path = path;
        }
    }
    
    public static class TakeSnapshot
    {
    }
}
