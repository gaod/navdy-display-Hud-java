package com.navdy.hud.app.gesture;

import com.navdy.service.library.events.input.Gesture;
import com.navdy.service.library.events.input.GestureEvent;

public class GestureDetector
{
    private static final float FOV_RANGE = 240.0f;
    private static final float FULL_RANGE = 640.0f;
    public float defaultOffset;
    public float defaultSensitivity;
    protected GestureEvent lastEvent;
    private GestureListener listener;
    protected float offset;
    protected int origin;
    protected int pinchDuration;
    protected GestureEvent startEvent;
    boolean useRelativeOrigin;
    protected boolean useRelativePosition;
    
    public GestureDetector(final GestureListener listener) {
        this.useRelativePosition = true;
        this.useRelativeOrigin = false;
        this.offset = -1.0f;
        this.defaultOffset = 0.5f;
        this.defaultSensitivity = 1.0f;
        this.pinchDuration = 0;
        this.listener = listener;
    }
    
    private float calculatePosition(final GestureEvent gestureEvent) {
        float n;
        if (gestureEvent == null) {
            n = -1.0f;
        }
        else {
            final int intValue = gestureEvent.x;
            if (this.useRelativePosition) {
                n = this.clamp(this.offset + (intValue - this.origin) / (this.defaultSensitivity * 120.0f), 0.0f, 1.0f);
            }
            else {
                n = this.clamp((intValue - 200.0f) / 240.0f, 0.0f, 1.0f);
            }
        }
        return n;
    }
    
    private float clamp(final float n, final float n2, final float n3) {
        return Math.min(Math.max(n2, n), n3);
    }
    
    private boolean hasPosition(final GestureEvent gestureEvent) {
        return gestureEvent.x != null && gestureEvent.y != null;
    }
    
    private void trackHand(final GestureEvent startEvent) {
        if (this.hasPosition(startEvent)) {
            if (this.startEvent == null) {
                this.startEvent = startEvent;
                final int intValue = startEvent.x;
                if (this.offset < 0.0f) {
                    this.offset = this.defaultOffset;
                }
                this.origin = intValue;
            }
            this.listener.onTrackHand(this.calculatePosition(startEvent));
        }
    }
    
    public boolean onGesture(final GestureEvent lastEvent) {
        switch (lastEvent.gesture) {
            case GESTURE_FINGER_DOWN:
            case GESTURE_HAND_DOWN:
                if (this.useRelativeOrigin && this.hasPosition(this.lastEvent)) {
                    this.offset = this.calculatePosition(this.lastEvent);
                }
                else {
                    this.offset = -1.0f;
                }
                this.startEvent = null;
                break;
            case GESTURE_FINGER_TO_PINCH:
            case GESTURE_HAND_TO_FIST:
                this.pinchDuration = 0;
                break;
            case GESTURE_PINCH:
            case GESTURE_FIST:
                ++this.pinchDuration;
                if (this.pinchDuration == 1) {
                    this.listener.onClick();
                    break;
                }
                break;
            case GESTURE_FINGER:
            case GESTURE_PALM:
                this.trackHand(lastEvent);
                break;
        }
        this.lastEvent = lastEvent;
        return true;
    }
    
    public interface GestureListener
    {
        void onClick();
        
        void onTrackHand(final float p0);
    }
}
