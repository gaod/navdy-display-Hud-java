package com.navdy.hud.app.storage;

import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.navdy.hud.app.util.CrashReportService;
import com.navdy.hud.app.util.ReportIssueService;
import com.navdy.service.library.util.LogUtils;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.events.file.FileType;
import com.navdy.hud.app.util.GenericUtil;
import android.os.Process;
import android.content.Intent;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.hud.app.HudApplication;
import android.os.Environment;
import com.navdy.hud.app.util.DeviceUtil;
import java.io.File;
import com.navdy.service.library.log.Logger;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;
import com.navdy.service.library.file.IFileTransferAuthority;

public class PathManager implements IFileTransferAuthority
{
    private static final String ACTIVE_ROUTE_DIR = ".activeroute";
    private static final String CAR_MD_RESPONSE_DISK_CACHE_FILES_DIR = "car_md_cache";
    private static String CRASH_INFO_TEXT_FILE_NAME;
    private static final String DATA_LOGS_DIR = "DataLogs";
    private static final String DRIVER_PROFILES_DIR = "DriverProfiles";
    private static final String GESTURE_VIDEOS_SYNC_FOLDER = "gesture_videos";
    private static final String HERE_MAPS_CONFIG_BASE_PATH = "/.here-maps";
    private static final Pattern HERE_MAPS_CONFIG_DIRS_PATTERN;
    private static final String HERE_MAPS_CONFIG_DIRS_REGEX = "[0-9]{10}";
    private static final String HERE_MAP_DATA_DIR = ".here-maps";
    public static final String HERE_MAP_META_JSON_FILE = "meta.json";
    private static final String HUD_DATABASE_DIR = "/.db";
    private static final String IMAGE_DISK_CACHE_FILES_DIR = "img_disk_cache";
    private static final String KERNEL_CRASH_CONSOLE_RAMOOPS = "/sys/fs/pstore/console-ramoops";
    private static final String KERNEL_CRASH_CONSOLE_RAMOOPS_0 = "/sys/fs/pstore/console-ramoops-0";
    private static final String KERNEL_CRASH_DMESG_RAMOOPS = "/sys/fs/pstore/dmesg-ramoops-0";
    private static final String[] KERNEL_CRASH_FILES;
    private static final String LOGS_FOLDER = "templogs";
    private static final String MUSIC_LIBRARY_DISK_CACHE_FILES_DIR = "music_disk_cache";
    private static final String NAVIGATION_FILES_DIR = "navigation_issues";
    private static final String NON_FATAL_CRASH_REPORT_DIR = "/sdcard/.logs/snapshot/";
    private static final String SYSTEM_CACHE_DIR = "/cache";
    private static final String TEMP_FILE_TIMESTAMP_FORMAT = "'display_log'_yyyy_MM_dd-HH_mm_ss'.zip'";
    private static final String TIMESTAMP_MWCONFIG_LATEST;
    private static SimpleDateFormat format;
    private static final Logger sLogger;
    private static final PathManager sSingleton;
    private String activeRouteInfoDir;
    private String carMdResponseDiskCacheFolder;
    private String databaseDir;
    private String driverProfilesDir;
    private String gestureVideosSyncFolder;
    private File hereMapsConfigDirs;
    private String hereMapsDataDirectory;
    private String hereVoiceSkinsPath;
    private String imageDiskCacheFolder;
    private volatile boolean initialized;
    private final String mapsPartitionPath;
    private String musicDiskCacheFolder;
    private String navigationIssuesFolder;
    private String tempLogsFolder;
    
    static {
        sLogger = new Logger(PathManager.class);
        PathManager.CRASH_INFO_TEXT_FILE_NAME = "info.txt";
        HERE_MAPS_CONFIG_DIRS_PATTERN = Pattern.compile("[0-9]{10}");
        TIMESTAMP_MWCONFIG_LATEST = DeviceUtil.getCurrentHereSdkTimestamp();
        KERNEL_CRASH_FILES = new String[] { "/sys/fs/pstore/console-ramoops", "/sys/fs/pstore/console-ramoops-0", "/sys/fs/pstore/dmesg-ramoops-0" };
        PathManager.format = new SimpleDateFormat("'display_log'_yyyy_MM_dd-HH_mm_ss'.zip'");
        sSingleton = new PathManager();
    }
    
    private PathManager() {
        final String absolutePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        final String absolutePath2 = HudApplication.getAppContext().getFilesDir().getAbsolutePath();
        this.tempLogsFolder = HudApplication.getAppContext().getCacheDir().getAbsolutePath() + File.separator + "templogs";
        this.driverProfilesDir = absolutePath2 + File.separator + "DriverProfiles";
        this.mapsPartitionPath = SystemProperties.get("ro.maps_partition", absolutePath);
        this.hereMapsConfigDirs = new File(this.mapsPartitionPath + File.separator + "/.here-maps");
        this.databaseDir = absolutePath2 + File.separator + "/.db";
        this.activeRouteInfoDir = absolutePath2 + File.separator + ".activeroute";
        this.navigationIssuesFolder = absolutePath2 + File.separator + "navigation_issues";
        this.carMdResponseDiskCacheFolder = absolutePath2 + File.separator + "car_md_cache";
        this.imageDiskCacheFolder = absolutePath2 + File.separator + "img_disk_cache";
        this.musicDiskCacheFolder = absolutePath2 + File.separator + "music_disk_cache";
        this.gestureVideosSyncFolder = this.mapsPartitionPath + File.separator + "gesture_videos";
        final File file = new File(this.mapsPartitionPath + File.separator + ".here-maps/voices-download");
        PathManager.sLogger.v("voiceSkins path=" + file);
        this.hereVoiceSkinsPath = file.getAbsolutePath();
        this.hereMapsDataDirectory = this.mapsPartitionPath + File.separator + ".here-maps";
    }
    
    private void collectGpsLog(final String s) {
        try {
            final Intent intent = new Intent("GPS_COLLECT_LOGS");
            intent.putExtra("logPath", s);
            HudApplication.getAppContext().sendBroadcastAsUser(intent, Process.myUserHandle());
            GenericUtil.sleep(3000);
        }
        catch (Throwable t) {
            PathManager.sLogger.e(t);
        }
    }
    
    public static String generateTempFileName() {
        synchronized (PathManager.class) {
            return PathManager.format.format(System.currentTimeMillis());
        }
    }
    
    public static PathManager getInstance() {
        return PathManager.sSingleton;
    }
    
    private void init() {
        if (!this.initialized) {
            this.initialized = true;
            new File(this.tempLogsFolder).mkdirs();
            new File(this.databaseDir).mkdirs();
            new File(this.driverProfilesDir).mkdirs();
            new File(this.activeRouteInfoDir).mkdirs();
            new File(this.navigationIssuesFolder).mkdirs();
            new File(this.carMdResponseDiskCacheFolder).mkdirs();
            new File("/sdcard/.logs/snapshot/").mkdirs();
            new File(this.imageDiskCacheFolder).mkdirs();
            new File(this.musicDiskCacheFolder).mkdirs();
            new File(this.hereVoiceSkinsPath).mkdirs();
            new File(this.gestureVideosSyncFolder).mkdirs();
        }
    }
    
    public void collectEnvironmentInfo(final String p0) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_2       
        //     2: aconst_null    
        //     3: astore_3       
        //     4: aload_2        
        //     5: astore          4
        //     7: new             Ljava/io/File;
        //    10: astore          5
        //    12: aload_2        
        //    13: astore          4
        //    15: new             Ljava/lang/StringBuilder;
        //    18: astore          6
        //    20: aload_2        
        //    21: astore          4
        //    23: aload           6
        //    25: invokespecial   java/lang/StringBuilder.<init>:()V
        //    28: aload_2        
        //    29: astore          4
        //    31: aload           5
        //    33: aload           6
        //    35: aload_1        
        //    36: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    39: getstatic       java/io/File.separator:Ljava/lang/String;
        //    42: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    45: getstatic       com/navdy/hud/app/storage/PathManager.CRASH_INFO_TEXT_FILE_NAME:Ljava/lang/String;
        //    48: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    51: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    54: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    57: aload_2        
        //    58: astore          4
        //    60: new             Ljava/io/FileWriter;
        //    63: astore_1       
        //    64: aload_2        
        //    65: astore          4
        //    67: aload_1        
        //    68: aload           5
        //    70: invokespecial   java/io/FileWriter.<init>:(Ljava/io/File;)V
        //    73: aload_1        
        //    74: invokestatic    com/navdy/hud/app/util/os/PropsFileUpdater.readProps:()Ljava/lang/String;
        //    77: invokevirtual   java/io/FileWriter.write:(Ljava/lang/String;)V
        //    80: aload_1        
        //    81: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //    84: return         
        //    85: astore_2       
        //    86: aload_3        
        //    87: astore_1       
        //    88: aload_1        
        //    89: astore          4
        //    91: getstatic       com/navdy/hud/app/storage/PathManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //    94: aload_2        
        //    95: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //    98: aload_1        
        //    99: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   102: goto            84
        //   105: astore_1       
        //   106: aload           4
        //   108: astore_2       
        //   109: aload_2        
        //   110: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   113: aload_1        
        //   114: athrow         
        //   115: astore          4
        //   117: aload_1        
        //   118: astore_2       
        //   119: aload           4
        //   121: astore_1       
        //   122: goto            109
        //   125: astore          4
        //   127: aload           4
        //   129: astore_2       
        //   130: goto            88
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  7      12     85     88     Ljava/lang/Throwable;
        //  7      12     105    109    Any
        //  15     20     85     88     Ljava/lang/Throwable;
        //  15     20     105    109    Any
        //  23     28     85     88     Ljava/lang/Throwable;
        //  23     28     105    109    Any
        //  31     57     85     88     Ljava/lang/Throwable;
        //  31     57     105    109    Any
        //  60     64     85     88     Ljava/lang/Throwable;
        //  60     64     105    109    Any
        //  67     73     85     88     Ljava/lang/Throwable;
        //  67     73     105    109    Any
        //  73     80     125    133    Ljava/lang/Throwable;
        //  73     80     115    125    Any
        //  91     98     105    109    Any
        // 
        // The error that occurred was:
        // 
        // java.util.ConcurrentModificationException
        //     at java.util.ArrayList$Itr.checkForComodification(Unknown Source)
        //     at java.util.ArrayList$Itr.next(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertLocalVariables(AstBuilder.java:2863)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2445)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public String getActiveRouteInfoDir() {
        if (!this.initialized) {
            this.init();
        }
        return this.activeRouteInfoDir;
    }
    
    public String getCarMdResponseDiskCacheFolder() {
        if (!this.initialized) {
            this.init();
        }
        return this.carMdResponseDiskCacheFolder;
    }
    
    public String getDatabaseDir() {
        if (!this.initialized) {
            this.init();
        }
        return this.databaseDir;
    }
    
    @Override
    public String getDirectoryForFileType(final FileType fileType) {
        if (!this.initialized) {
            this.init();
        }
        String tempLogsFolder = null;
        switch (fileType) {
            default:
                tempLogsFolder = null;
                break;
            case FILE_TYPE_OTA:
                tempLogsFolder = "/cache";
                break;
            case FILE_TYPE_LOGS:
                tempLogsFolder = this.tempLogsFolder;
                break;
        }
        return tempLogsFolder;
    }
    
    public String getDriverProfilesDir() {
        if (!this.initialized) {
            this.init();
        }
        return this.driverProfilesDir;
    }
    
    @Override
    public String getFileToSend(final FileType fileType) {
        if (fileType != FileType.FILE_TYPE_LOGS) {
            return null;
        }
        final String directoryForFileType = this.getDirectoryForFileType(fileType);
        final String string = directoryForFileType + File.separator + "stage";
        final File file = new File(string);
        IOUtils.deleteDirectory(HudApplication.getAppContext(), file);
        IOUtils.createDirectory(file);
        Label_0215: {
            String string2;
            try {
                LogUtils.copyComprehensiveSystemLogs(string);
                this.collectEnvironmentInfo(string);
                this.collectGpsLog(string);
                DeviceUtil.copyHEREMapsDataInfo(string);
                DeviceUtil.takeDeviceScreenShot(file.getAbsolutePath() + File.separator + "HUDScreenShot.png");
                final List<File> latestDriveLogFiles = ReportIssueService.getLatestDriveLogFiles(1);
                if (latestDriveLogFiles != null && latestDriveLogFiles.size() > 0) {
                    for (final File file2 : latestDriveLogFiles) {
                        IOUtils.copyFile(file2.getAbsolutePath(), file.getAbsolutePath() + File.separator + file2.getName());
                    }
                }
                break Label_0215;
            }
            catch (Throwable t) {
                string2 = null;
            }
            return string2;
        }
        final File[] listFiles = file.listFiles();
        String string2 = directoryForFileType + File.separator + generateTempFileName();
        CrashReportService.compressCrashReportsToZip(listFiles, string2);
        if (!new File(string2).exists()) {
            return null;
        }
        return string2;
        string2 = null;
        return string2;
    }
    
    public String getGestureVideosSyncFolder() {
        if (!this.initialized) {
            this.init();
        }
        return this.gestureVideosSyncFolder;
    }
    
    public List<String> getHereMapsConfigDirs() {
        if (!this.initialized) {
            this.init();
        }
        final ArrayList<String> list = new ArrayList<String>();
        final File[] listFiles = this.hereMapsConfigDirs.listFiles(new FileFilter() {
            @Override
            public boolean accept(final File file) {
                return file.isDirectory() && PathManager.HERE_MAPS_CONFIG_DIRS_PATTERN.matcher(file.getName()).matches();
            }
        });
        if (listFiles != null) {
            for (int length = listFiles.length, i = 0; i < length; ++i) {
                list.add(listFiles[i].getAbsolutePath());
            }
        }
        return list;
    }
    
    public String getHereMapsDataDirectory() {
        if (!this.initialized) {
            this.init();
        }
        return this.hereMapsDataDirectory;
    }
    
    public String getHereVoiceSkinsPath() {
        if (!this.initialized) {
            this.init();
        }
        return this.hereVoiceSkinsPath;
    }
    
    public String getImageDiskCacheFolder() {
        if (!this.initialized) {
            this.init();
        }
        return this.imageDiskCacheFolder;
    }
    
    public String[] getKernelCrashFiles() {
        if (!this.initialized) {
            this.init();
        }
        return PathManager.KERNEL_CRASH_FILES;
    }
    
    public String getLatestHereMapsConfigPath() {
        if (!this.initialized) {
            this.init();
        }
        return this.hereMapsConfigDirs.getAbsolutePath() + File.separator + PathManager.TIMESTAMP_MWCONFIG_LATEST;
    }
    
    public String getMapsPartitionPath() {
        if (!this.initialized) {
            this.init();
        }
        return this.mapsPartitionPath;
    }
    
    public String getMusicDiskCacheFolder() {
        if (!this.initialized) {
            this.init();
        }
        return this.musicDiskCacheFolder;
    }
    
    public String getNavigationIssuesDir() {
        if (!this.initialized) {
            this.init();
        }
        return this.navigationIssuesFolder;
    }
    
    public String getNonFatalCrashReportDir() {
        if (!this.initialized) {
            this.init();
        }
        return "/sdcard/.logs/snapshot/";
    }
    
    @Override
    public boolean isFileTypeAllowed(final FileType fileType) {
        boolean b = false;
        switch (fileType) {
            default:
                b = false;
                break;
            case FILE_TYPE_OTA:
            case FILE_TYPE_LOGS:
            case FILE_TYPE_PERF_TEST:
                b = true;
                break;
        }
        return b;
    }
    
    @Override
    public void onFileSent(final FileType fileType) {
        if (fileType == FileType.FILE_TYPE_LOGS) {
            CrashReportService.clearCrashReports();
        }
    }
}
