package com.navdy.hud.app.storage.db.helper;

import android.database.sqlite.SQLiteStatement;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader;
import com.navdy.hud.app.framework.recentcall.RecentCallManager;
import java.util.Iterator;
import java.util.Collections;
import java.util.Collection;
import android.text.TextUtils;
import java.util.HashMap;
import android.database.Cursor;
import java.util.Date;
import com.navdy.hud.app.framework.contacts.NumberType;
import java.util.ArrayList;
import com.navdy.hud.app.framework.contacts.ContactImageHelper;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.util.GenericUtil;
import android.database.sqlite.SQLiteDatabase;
import com.navdy.hud.app.storage.db.DatabaseUtil;
import com.navdy.hud.app.storage.db.HudDatabase;
import java.util.LinkedList;
import java.util.Comparator;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.framework.recentcall.RecentCall;
import java.util.List;

public class RecentCallPersistenceHelper
{
    private static final String BULK_INSERT_SQL = "INSERT INTO recent_calls(device_id,category,name,number,number_type,call_time,call_type,def_image,number_numeric) VALUES (?,?,?,?,?,?,?,?,?)";
    private static final int CALL_TIME_ORDINAL = 5;
    private static final int CALL_TYPE_ORDINAL = 4;
    private static final int CATEGORY_ORDINAL = 0;
    private static final int DEFAULT_IMAGE_INDEX_ORDINAL = 6;
    private static final List<RecentCall> EMPTY_LIST;
    private static final int NAME_ORDINAL = 1;
    private static final int NUMBER_NUMERIC_ORDINAL = 7;
    private static final int NUMBER_ORDINAL = 2;
    private static final int NUMBER_TYPE_ORDINAL = 3;
    private static final String[] RECENT_CALL_ARGS;
    private static final String RECENT_CALL_ORDER_BY = "call_time DESC";
    private static final String[] RECENT_CALL_PROJECTION;
    private static final String RECENT_CALL_WHERE = "device_id=?";
    private static final Object lockObj;
    private static final Logger sLogger;
    private static final Comparator<RecentCall> sReverseComparator;
    
    static {
        sLogger = new Logger(RecentCallPersistenceHelper.class);
        lockObj = new Object();
        EMPTY_LIST = new LinkedList<RecentCall>();
        RECENT_CALL_PROJECTION = new String[] { "category", "name", "number", "number_type", "call_type", "call_time", "def_image", "number_numeric" };
        RECENT_CALL_ARGS = new String[1];
        sReverseComparator = new Comparator<RecentCall>() {
            @Override
            public int compare(final RecentCall recentCall, final RecentCall recentCall2) {
                return recentCall2.compareTo(recentCall);
            }
        };
    }
    
    private static void deleteRecentCalls(final String s) {
        SQLiteDatabase writableDatabase = null;
        Label_0043: {
            try {
                final Object lockObj = RecentCallPersistenceHelper.lockObj;
                synchronized (lockObj) {
                    writableDatabase = HudDatabase.getInstance().getWritableDatabase();
                    if (writableDatabase == null) {
                        throw new DatabaseUtil.DatabaseNotAvailable();
                    }
                    break Label_0043;
                }
            }
            catch (Throwable t) {
                RecentCallPersistenceHelper.sLogger.e(t);
            }
            return;
        }
        final String s2;
        RecentCallPersistenceHelper.RECENT_CALL_ARGS[0] = s2;
        RecentCallPersistenceHelper.sLogger.v("recent-calls rows deleted:" + writableDatabase.delete("recent_calls", "device_id=?", RecentCallPersistenceHelper.RECENT_CALL_ARGS));
    }
    // monitorexit(o)
    
    public static List<RecentCall> getRecentsCalls(final String s) {
        GenericUtil.checkNotOnMainThread();
        final List<RecentCall> empty_LIST;
        if (DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile()) {
            empty_LIST = RecentCallPersistenceHelper.EMPTY_LIST;
        }
        else {
            final Object lockObj = RecentCallPersistenceHelper.lockObj;
            final SQLiteDatabase writableDatabase;
            synchronized (lockObj) {
                writableDatabase = HudDatabase.getInstance().getWritableDatabase();
                if (writableDatabase == null) {
                    throw new DatabaseUtil.DatabaseNotAvailable();
                }
            }
            final String s2;
            RecentCallPersistenceHelper.RECENT_CALL_ARGS[0] = s2;
            final Cursor query = writableDatabase.query("recent_calls", RecentCallPersistenceHelper.RECENT_CALL_PROJECTION, "device_id=?", RecentCallPersistenceHelper.RECENT_CALL_ARGS, (String)null, (String)null, "call_time DESC");
            Label_0093: {
                if (query == null) {
                    break Label_0093;
                }
                try {
                    if (!query.moveToFirst()) {
                        final List<RecentCall> empty_LIST2 = RecentCallPersistenceHelper.EMPTY_LIST;
                        IOUtils.closeStream((Closeable)query);
                    }
                    // monitorexit(o)
                    else {
                        ContactImageHelper.getInstance();
                        final ArrayList<RecentCall> list = new ArrayList<RecentCall>();
                        do {
                            list.add(new RecentCall(query.getString(1), RecentCall.Category.buildFromValue(query.getInt(0)), query.getString(2), NumberType.buildFromValue(query.getInt(3)), new Date(1000L * query.getLong(5)), RecentCall.CallType.buildFromValue(query.getInt(4)), query.getInt(6), query.getLong(7)));
                        } while (query.moveToNext());
                        IOUtils.closeStream((Closeable)query);
                    }
                    // monitorexit(o)
                }
                finally {
                    IOUtils.closeStream((Closeable)query);
                }
            }
        }
        return empty_LIST;
    }
    
    private static List<RecentCall> mergeRecentCalls(final List<RecentCall> list, final List<RecentCall> list2, final boolean b) {
        final HashMap<Long, RecentCall> hashMap = new HashMap<Long, RecentCall>(52);
        if (list != null) {
            for (final RecentCall recentCall : list) {
                if (!b || recentCall.category != RecentCall.Category.PHONE_CALL) {
                    hashMap.put(recentCall.numericNumber, recentCall);
                }
            }
        }
        if (list2 != null) {
            for (final RecentCall recentCall2 : list2) {
                final RecentCall recentCall3 = hashMap.get(recentCall2.numericNumber);
                if (recentCall3 == null) {
                    hashMap.put(recentCall2.numericNumber, recentCall2);
                }
                else if (b) {
                    hashMap.put(recentCall2.numericNumber, recentCall2);
                }
                else {
                    if (recentCall2.callTime.getTime() < recentCall3.callTime.getTime()) {
                        continue;
                    }
                    if (recentCall2.category == RecentCall.Category.MESSAGE && TextUtils.isEmpty((CharSequence)recentCall2.name)) {
                        recentCall2.name = recentCall3.name;
                    }
                    hashMap.put(recentCall2.numericNumber, recentCall2);
                }
            }
        }
        final ArrayList list3 = new ArrayList<Object>(hashMap.values());
        Collections.<E>sort((List<E>)list3, (Comparator<? super E>)RecentCallPersistenceHelper.sReverseComparator);
        ArrayList<Object> list4 = (ArrayList<Object>)list3;
        if (list3.size() > 30) {
            list4 = new ArrayList<Object>(30);
            for (int i = 0; i < 30; ++i) {
                list4.add(list3.get(i));
            }
        }
        return (List<RecentCall>)list4;
    }
    
    public static void storeRecentCalls(final String s, final List<RecentCall> list, final boolean b) {
        GenericUtil.checkNotOnMainThread();
        Object o = RecentCallManager.getInstance();
        final List<RecentCall> mergeRecentCalls = mergeRecentCalls(((RecentCallManager)o).getRecentCalls(), list, b);
        deleteRecentCalls(s);
        if (mergeRecentCalls.size() == 0) {
            ((RecentCallManager)o).setRecentCalls(null);
        }
        else {
            if (b) {
                PhoneImageDownloader.getInstance().clearAllPhotoCheckEntries();
            }
            final Object lockObj = RecentCallPersistenceHelper.lockObj;
            synchronized (lockObj) {
                o = HudDatabase.getInstance().getWritableDatabase();
                if (o == null) {
                    throw new DatabaseUtil.DatabaseNotAvailable();
                }
            }
            int n = 0;
        Label_0184_Outer:
            while (true) {
                final SQLiteStatement compileStatement = ((SQLiteDatabase)o).compileStatement("INSERT INTO recent_calls(device_id,category,name,number,number_type,call_time,call_type,def_image,number_numeric) VALUES (?,?,?,?,?,?,?,?,?)");
                n = 0;
                while (true) {
                    Label_0340: {
                        try {
                            final ContactImageHelper instance = ContactImageHelper.getInstance();
                            ((SQLiteDatabase)o).beginTransaction();
                            for (final RecentCall recentCall : mergeRecentCalls) {
                                compileStatement.clearBindings();
                                final String s2;
                                compileStatement.bindString(1, s2);
                                compileStatement.bindLong(2, (long)recentCall.category.getValue());
                                if (TextUtils.isEmpty((CharSequence)recentCall.name)) {
                                    break Label_0340;
                                }
                                compileStatement.bindString(3, recentCall.name);
                                compileStatement.bindString(4, recentCall.number);
                                compileStatement.bindLong(5, (long)recentCall.numberType.getValue());
                                compileStatement.bindLong(6, recentCall.callTime.getTime() / 1000L);
                                compileStatement.bindLong(7, (long)recentCall.callType.getValue());
                                recentCall.defaultImageIndex = instance.getContactImageIndex(recentCall.number);
                                compileStatement.bindLong(8, (long)recentCall.defaultImageIndex);
                                compileStatement.bindLong(9, recentCall.numericNumber);
                                compileStatement.execute();
                                final int n2 = ++n;
                                if (!b) {
                                    continue Label_0184_Outer;
                                }
                                PhoneImageDownloader.getInstance().submitDownload(recentCall.number, PhoneImageDownloader.Priority.NORMAL, PhotoType.PHOTO_CONTACT, recentCall.name);
                                n = n2;
                            }
                            break;
                        }
                        finally {
                            ((SQLiteDatabase)o).endTransaction();
                        }
                    }
                    compileStatement.bindNull(3);
                    continue;
                }
            }
            ((SQLiteDatabase)o).setTransactionSuccessful();
            RecentCallManager.getInstance().setRecentCalls(mergeRecentCalls);
            ((SQLiteDatabase)o).endTransaction();
            RecentCallPersistenceHelper.sLogger.v("recent-calls rows added:" + n);
        }
        // monitorexit(o2)
    }
}
