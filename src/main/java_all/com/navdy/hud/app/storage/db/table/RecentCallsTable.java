package com.navdy.hud.app.storage.db.table;

import com.navdy.hud.app.storage.db.DatabaseUtil;
import android.database.sqlite.SQLiteDatabase;
import com.navdy.service.library.log.Logger;

public class RecentCallsTable
{
    public static final String CALL_TIME = "call_time";
    public static final String CALL_TYPE = "call_type";
    public static final String CATEGORY = "category";
    public static final String DEFAULT_IMAGE_INDEX = "def_image";
    public static final String DRIVER_ID = "device_id";
    public static final String NAME = "name";
    public static final String NUMBER = "number";
    public static final String NUMBER_NUMERIC = "number_numeric";
    public static final String NUMBER_TYPE = "number_type";
    public static final String TABLE_NAME = "recent_calls";
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(RecentCallsTable.class);
    }
    
    public static void createTable(final SQLiteDatabase sqLiteDatabase) {
        createTable_2(sqLiteDatabase);
    }
    
    public static void createTable_2(final SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " + "recent_calls" + " (" + "device_id" + " TEXT NOT NULL," + "category" + " INTEGER NOT NULL," + "name" + " TEXT," + "number" + " TEXT NOT NULL," + "number_type" + " INTEGER NOT NULL," + "call_time" + " INTEGER," + "call_type" + " INTEGER," + "def_image" + " INTEGER," + "number_numeric" + " INTEGER" + ");");
        RecentCallsTable.sLogger.v("createdTable:" + "recent_calls");
        final String string = "recent_calls" + "_" + "device_id";
        sqLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS " + string + " ON " + "recent_calls" + "(" + "device_id" + ");");
        RecentCallsTable.sLogger.v("createdIndex:" + string);
        final String string2 = "recent_calls" + "_" + "category";
        sqLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS " + string2 + " ON " + "recent_calls" + "(" + "category" + ");");
        RecentCallsTable.sLogger.v("createdIndex:" + string2);
        final String string3 = "recent_calls" + "_" + "number";
        sqLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS " + string3 + " ON " + "recent_calls" + "(" + "number" + ");");
        RecentCallsTable.sLogger.v("createdIndex:" + string3);
        final String string4 = "recent_calls" + "_" + "number_type";
        sqLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS " + string4 + " ON " + "recent_calls" + "(" + "number_type" + ");");
        RecentCallsTable.sLogger.v("createdIndex:" + string4);
        final String string5 = "recent_calls" + "_" + "call_type";
        sqLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS " + string5 + " ON " + "recent_calls" + "(" + "call_type" + ");");
        RecentCallsTable.sLogger.v("createdIndex:" + string5);
        final String string6 = "recent_calls" + "_" + "def_image";
        sqLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS " + string6 + " ON " + "recent_calls" + "(" + "def_image" + ");");
        RecentCallsTable.sLogger.v("createdIndex:" + string6);
        final String string7 = "recent_calls" + "_" + "number_numeric";
        sqLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS " + string7 + " ON " + "recent_calls" + "(" + "number_numeric" + ");");
        RecentCallsTable.sLogger.v("createdIndex:" + string7);
    }
    
    public static void upgradeDatabase_2(final SQLiteDatabase sqLiteDatabase) {
        DatabaseUtil.dropTable(sqLiteDatabase, "recent_calls", RecentCallsTable.sLogger);
        createTable_2(sqLiteDatabase);
    }
}
