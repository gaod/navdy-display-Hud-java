package com.navdy.hud.app.storage.db.helper;

import android.content.ContentValues;
import com.navdy.hud.app.HudApplication;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import com.navdy.hud.app.storage.db.DatabaseUtil;
import com.navdy.hud.app.storage.db.HudDatabase;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.log.Logger;

public class VinInformationHelper
{
    private static final String[] DATASET_ARGS;
    private static final String[] DATASET_PROJECTION;
    private static final String DATASET_WHERE = "vin=?";
    private static final int VIN_INFO_ORDINAL = 0;
    public static final String VIN_SHARED_PREF = "vin";
    private static final String VIN_SHARED_PREF_NAME = "activeVin";
    private static final Object lockObj;
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(VinInformationHelper.class);
        DATASET_PROJECTION = new String[] { "info" };
        DATASET_ARGS = new String[1];
        lockObj = new Object();
    }
    
    public static void deleteVinInfo(final String s) {
        GenericUtil.checkNotOnMainThread();
        // monitorenter(lockObj = VinInformationHelper.lockObj)
        Label_0043: {
            try {
                final Object writableDatabase = HudDatabase.getInstance().getWritableDatabase();
                if (writableDatabase == null) {
                    throw new DatabaseUtil.DatabaseNotAvailable();
                }
                break Label_0043;
            }
            catch (Throwable t) {
                try {
                    VinInformationHelper.sLogger.e(t);
                    return;
                    final String s2;
                    VinInformationHelper.DATASET_ARGS[0] = s2;
                    final Object writableDatabase;
                    final int delete = ((SQLiteDatabase)writableDatabase).delete("vininfo", "vin=?", VinInformationHelper.DATASET_ARGS);
                    final Logger sLogger = VinInformationHelper.sLogger;
                    writableDatabase = new StringBuilder();
                    sLogger.v(((StringBuilder)writableDatabase).append("vin info deleted:").append(s2).append(" result=").append(delete).toString());
                }
                finally {
                }
                // monitorexit(lockObj)
            }
        }
    }
    
    public static String getVinInfo(String string) {
        GenericUtil.checkNotOnMainThread();
        final Object lockObj = VinInformationHelper.lockObj;
        final SQLiteDatabase writableDatabase;
        synchronized (lockObj) {
            writableDatabase = HudDatabase.getInstance().getWritableDatabase();
            if (writableDatabase == null) {
                throw new DatabaseUtil.DatabaseNotAvailable();
            }
        }
        final String s;
        VinInformationHelper.DATASET_ARGS[0] = s;
        final Cursor query = writableDatabase.query("vininfo", VinInformationHelper.DATASET_PROJECTION, "vin=?", VinInformationHelper.DATASET_ARGS, (String)null, (String)null, (String)null);
        Label_0078: {
            if (query == null) {
                break Label_0078;
            }
            try {
                if (!query.moveToFirst()) {
                    IOUtils.closeStream((Closeable)query);
                    // monitorexit(o)
                    string = null;
                }
                else {
                    string = query.getString(0);
                    IOUtils.closeStream((Closeable)query);
                }
                // monitorexit(o)
                return string;
            }
            finally {
                IOUtils.closeStream((Closeable)query);
            }
        }
    }
    
    public static SharedPreferences getVinPreference() {
        return HudApplication.getAppContext().getSharedPreferences("activeVin", 4);
    }
    
    public static void storeVinInfo(final String s, final String s2) {
        GenericUtil.checkNotOnMainThread();
        deleteVinInfo(s);
        // monitorenter(lockObj = VinInformationHelper.lockObj)
        Label_0049: {
            try {
                final Object o = HudDatabase.getInstance().getWritableDatabase();
                if (o == null) {
                    throw new DatabaseUtil.DatabaseNotAvailable();
                }
                break Label_0049;
            }
            catch (Throwable t) {
                try {
                    VinInformationHelper.sLogger.e(t);
                    return;
                    final ContentValues contentValues = new ContentValues();
                    final String s3;
                    contentValues.put("vin", s3);
                    contentValues.put("info", s2);
                    final Object o;
                    ((SQLiteDatabase)o).insert("vininfo", (String)null, contentValues);
                    o = VinInformationHelper.sLogger;
                    ((Logger)o).v("added vin info [ " + s3 + " , " + s2 + "]");
                }
                finally {
                }
                // monitorexit(lockObj)
            }
        }
    }
}
