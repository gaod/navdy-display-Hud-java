package com.navdy.hud.app.common;

import java.util.List;
import android.content.pm.ResolveInfo;
import android.os.Looper;
import android.os.IBinder;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import com.navdy.service.library.log.Logger;
import android.os.Handler;
import android.content.Context;

public abstract class ServiceReconnector
{
    private static final int RECONNECT_INTERVAL_MS = 15000;
    private static final int RETRY_INTERVAL_MS = 60000;
    private String action;
    private Runnable connectRunnable;
    private Context context;
    private Handler handler;
    private final Logger logger;
    private ServiceConnection serviceConnection;
    protected Intent serviceIntent;
    boolean shuttingDown;
    
    public ServiceReconnector(final Context context, final Intent serviceIntent, final String action) {
        this.logger = new Logger(this.getClass());
        this.connectRunnable = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Label_0174: {
                        try {
                            ComponentName component;
                            if ((component = ServiceReconnector.this.serviceIntent.getComponent()) == null) {
                                component = ServiceReconnector.this.getServiceComponent(ServiceReconnector.this.serviceIntent);
                            }
                            if (component == null) {
                                ServiceReconnector.this.handler.postDelayed((Runnable)this, 60000L);
                            }
                            else {
                                ServiceReconnector.this.serviceIntent.setComponent(component);
                                ServiceReconnector.this.logger.i("Trying to start service");
                                if (ServiceReconnector.this.context.startService(ServiceReconnector.this.serviceIntent) == null) {
                                    break Label_0174;
                                }
                                ServiceReconnector.this.serviceIntent.setAction(ServiceReconnector.this.action);
                                if (!ServiceReconnector.this.context.bindService(ServiceReconnector.this.serviceIntent, ServiceReconnector.this.serviceConnection, 0)) {
                                    ServiceReconnector.this.logger.e("Unable to bind to service - aborting");
                                }
                            }
                            return;
                        }
                        catch (SecurityException ex) {
                            ServiceReconnector.this.logger.e("Security exception connecting to service - aborting", ex);
                            return;
                        }
                    }
                    ServiceReconnector.this.logger.e("Service doesn't exist (uninstalled?) - retrying");
                    ServiceReconnector.this.handler.postDelayed((Runnable)this, 60000L);
                }
            }
        };
        this.serviceConnection = (ServiceConnection)new ServiceConnection() {
            public void onServiceConnected(final ComponentName componentName, final IBinder binder) {
                ServiceReconnector.this.logger.i("ServiceConnection established with " + componentName);
                ServiceReconnector.this.onConnected(componentName, binder);
            }
            
            public void onServiceDisconnected(final ComponentName componentName) {
                ServiceReconnector.this.onDisconnected(componentName);
                if (!ServiceReconnector.this.shuttingDown) {
                    ServiceReconnector.this.logger.i("Service disconnected - will try reconnecting");
                    ServiceReconnector.this.handler.postDelayed(ServiceReconnector.this.connectRunnable, 15000L);
                }
            }
        };
        this.logger.i("Establishing service connection");
        this.handler = new Handler(Looper.getMainLooper());
        this.context = context;
        this.serviceIntent = serviceIntent;
        this.action = action;
        this.handler.post(this.connectRunnable);
    }
    
    private ComponentName getServiceComponent(final Intent intent) {
        final List queryIntentServices = this.context.getPackageManager().queryIntentServices(intent, 0);
        ComponentName componentName;
        if (queryIntentServices.isEmpty()) {
            componentName = null;
        }
        else {
            final ResolveInfo resolveInfo = queryIntentServices.get(0);
            componentName = new ComponentName(resolveInfo.serviceInfo.applicationInfo.packageName, resolveInfo.serviceInfo.name);
        }
        return componentName;
    }
    
    protected abstract void onConnected(final ComponentName p0, final IBinder p1);
    
    protected abstract void onDisconnected(final ComponentName p0);
    
    public void restart() {
        this.context.stopService(this.serviceIntent);
    }
    
    public void shutdown() {
        this.logger.i("shutting down service");
        this.shuttingDown = true;
        this.handler.removeCallbacks(this.connectRunnable);
        this.context.stopService(this.serviceIntent);
    }
}
