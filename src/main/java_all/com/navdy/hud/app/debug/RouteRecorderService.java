package com.navdy.hud.app.debug;

import android.os.RemoteException;
import android.os.IBinder;
import android.content.Intent;
import android.app.Service;

public class RouteRecorderService extends Service
{
    private RouteRecorder mRouteRecorder;
    
    public RouteRecorderService() {
        this.mRouteRecorder = RouteRecorder.getInstance();
    }
    
    public IBinder onBind(final Intent intent) {
        return (IBinder)new IRouteRecorder.Stub() {
            public boolean isPaused() throws RemoteException {
                return RouteRecorderService.this.mRouteRecorder.isPaused();
            }
            
            public boolean isPlaying() throws RemoteException {
                return RouteRecorderService.this.mRouteRecorder.isPlaying();
            }
            
            public boolean isRecording() throws RemoteException {
                return RouteRecorderService.this.mRouteRecorder.isRecording();
            }
            
            public boolean isStopped() throws RemoteException {
                return RouteRecorderService.this.mRouteRecorder.isStopped();
            }
            
            public void pausePlayback() throws RemoteException {
                RouteRecorderService.this.mRouteRecorder.pausePlayback();
            }
            
            public void prepare(final String s, final boolean b) throws RemoteException {
                RouteRecorderService.this.mRouteRecorder.prepare(s, b);
            }
            
            public boolean restartPlayback() throws RemoteException {
                return RouteRecorderService.this.mRouteRecorder.restartPlayback();
            }
            
            public void resumePlayback() throws RemoteException {
                RouteRecorderService.this.mRouteRecorder.resumePlayback();
            }
            
            public void startPlayback(final String s, final boolean b, final boolean b2) throws RemoteException {
                RouteRecorderService.this.mRouteRecorder.startPlayback(s, b, b2, true);
            }
            
            public String startRecording(final String s) throws RemoteException {
                return RouteRecorderService.this.mRouteRecorder.startRecording(s, true);
            }
            
            public void stopPlayback() throws RemoteException {
                RouteRecorderService.this.mRouteRecorder.stopPlayback();
            }
            
            public void stopRecording() throws RemoteException {
                RouteRecorderService.this.mRouteRecorder.stopRecording(true);
            }
        };
    }
}
