package com.navdy.hud.app.debug;

import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.events.debug.StartDrivePlaybackResponse;
import com.navdy.service.library.events.debug.DriveRecordingsResponse;
import java.util.ArrayList;
import com.navdy.service.library.task.TaskManager;
import java.util.Date;
import java.io.IOException;
import com.navdy.service.library.events.debug.StartDriveRecordingResponse;
import java.io.Writer;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.File;
import com.squareup.wire.Message;
import com.navdy.service.library.events.debug.StopDriveRecordingResponse;
import com.navdy.service.library.events.RequestStatus;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import android.os.Looper;
import com.navdy.hud.app.HudApplication;
import android.os.Bundle;
import android.location.Location;
import java.util.Locale;
import com.navdy.hud.app.storage.PathManager;
import android.location.LocationManager;
import android.location.LocationListener;
import java.util.TimeZone;
import android.os.Handler;
import com.navdy.hud.app.device.gps.GpsManager;
import java.io.BufferedWriter;
import com.navdy.service.library.events.location.Coordinate;
import java.util.List;
import android.content.Context;
import com.navdy.hud.app.service.HudConnectionService;
import com.navdy.service.library.log.Logger;
import java.text.SimpleDateFormat;

public class RouteRecorder
{
    private static final SimpleDateFormat DATE_FORMAT;
    public static final String SECONDARY_LOCATION_TAG = "[secondary] ";
    private static final RouteRecorder sInstance;
    public static final Logger sLogger;
    private HudConnectionService connectionService;
    private final Context context;
    private volatile int currentInjectionIndex;
    private List<Coordinate> currentLocations;
    private BufferedWriter driveLogWriter;
    private final GpsManager gpsManager;
    private final Handler handler;
    private final Runnable injectFakeLocationRunnable;
    private volatile boolean isLooping;
    private volatile boolean isRecording;
    boolean lastReportedDST;
    private TimeZone lastReportedTimeZone;
    private final LocationListener locationListener;
    private final LocationManager locationManager;
    private final PathManager pathManager;
    private DriveRecorder.State playbackState;
    private volatile boolean prepared;
    private volatile long preparedFileLastModifiedTime;
    private volatile String preparedFileName;
    private String recordingLabel;
    private Runnable startLocationUpdates;
    
    static {
        sLogger = new Logger(RouteRecorder.class);
        DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'_'HH_mm_ss.SSS", Locale.US);
        sInstance = new RouteRecorder();
    }
    
    private RouteRecorder() {
        this.lastReportedTimeZone = null;
        this.lastReportedDST = false;
        this.playbackState = DriveRecorder.State.STOPPED;
        this.startLocationUpdates = new Runnable() {
            @Override
            public void run() {
                if (RouteRecorder.this.locationManager.isProviderEnabled("NAVDY_GPS_PROVIDER")) {
                    RouteRecorder.this.locationManager.requestLocationUpdates("NAVDY_GPS_PROVIDER", 0L, 0.0f, RouteRecorder.this.locationListener);
                }
                if (RouteRecorder.this.locationManager.isProviderEnabled("network")) {
                    RouteRecorder.this.locationManager.requestLocationUpdates("network", 0L, 0.0f, RouteRecorder.this.locationListener);
                }
            }
        };
        this.injectFakeLocationRunnable = new Runnable() {
            @Override
            public void run() {
                if (RouteRecorder.this.currentLocations != null && RouteRecorder.this.currentLocations.size() > RouteRecorder.this.currentInjectionIndex) {
                    RouteRecorder.this.gpsManager.feedLocation(new Coordinate.Builder(RouteRecorder.this.currentLocations.get(RouteRecorder.this.currentInjectionIndex)).timestamp(System.currentTimeMillis()).build());
                    if (RouteRecorder.this.currentLocations.size() > RouteRecorder.this.currentInjectionIndex + 1) {
                        final long longValue = RouteRecorder.this.currentLocations.get(RouteRecorder.this.currentInjectionIndex + 1).timestamp;
                        final long longValue2 = RouteRecorder.this.currentLocations.get(RouteRecorder.this.currentInjectionIndex).timestamp;
                        RouteRecorder.this.currentInjectionIndex++;
                        RouteRecorder.this.handler.postDelayed((Runnable)this, longValue - longValue2);
                    }
                    else if (RouteRecorder.this.isLooping) {
                        RouteRecorder.this.currentInjectionIndex = 0;
                        RouteRecorder.this.handler.post(RouteRecorder.this.injectFakeLocationRunnable);
                    }
                    else {
                        RouteRecorder.this.stopPlayback();
                    }
                }
                else if (RouteRecorder.this.isLooping && RouteRecorder.this.currentLocations != null) {
                    RouteRecorder.this.currentInjectionIndex = 0;
                    RouteRecorder.this.handler.post(RouteRecorder.this.injectFakeLocationRunnable);
                }
                else {
                    RouteRecorder.this.stopPlayback();
                }
            }
        };
        this.locationListener = (LocationListener)new LocationListener() {
            public void onLocationChanged(final Location location) {
                RouteRecorder.this.persistLocationToFile(location, false);
            }
            
            public void onProviderDisabled(final String s) {
            }
            
            public void onProviderEnabled(final String s) {
            }
            
            public void onStatusChanged(final String s, final int n, final Bundle bundle) {
            }
        };
        this.context = HudApplication.getAppContext();
        this.locationManager = (LocationManager)this.context.getSystemService("location");
        this.gpsManager = GpsManager.getInstance();
        this.handler = new Handler(Looper.getMainLooper());
        this.pathManager = PathManager.getInstance();
        this.isRecording = false;
        this.currentInjectionIndex = 0;
    }
    
    private void bStopRecording(final boolean b) {
        IOUtils.closeStream(this.driveLogWriter);
        this.isRecording = false;
        this.recordingLabel = null;
        if (!b) {
            this.connectionService.sendMessage(new StopDriveRecordingResponse(RequestStatus.REQUEST_SUCCESS));
        }
        RouteRecorder.sLogger.v("recording stopped");
    }
    
    private Runnable createDriveRecord(final String s, final boolean b) {
        return new Runnable() {
            @Override
            public void run() {
                final File driveLogsDir = DriveRecorder.getDriveLogsDir(s);
                if (!driveLogsDir.exists()) {
                    driveLogsDir.mkdirs();
                }
                final File file = new File(driveLogsDir, s);
                try {
                    if (file.createNewFile()) {
                        RouteRecorder.this.driveLogWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "utf-8"));
                        RouteRecorder.sLogger.v("writing drive log: " + s);
                        RouteRecorder.this.handler.post(RouteRecorder.this.startLocationUpdates);
                        if (!b) {
                            RouteRecorder.this.connectionService.sendMessage(new StartDriveRecordingResponse(RequestStatus.REQUEST_SUCCESS));
                        }
                    }
                }
                catch (IOException ex) {
                    RouteRecorder.sLogger.e(ex);
                }
            }
        };
    }
    
    public static RouteRecorder getInstance() {
        return RouteRecorder.sInstance;
    }
    
    private void persistLocationToFile(final Location location, final boolean b) {
        if (this.isRecording) {
            String provider;
            if ("NAVDY_GPS_PROVIDER".equals(provider = location.getProvider())) {
                provider = "gps";
            }
            String format = "";
            final TimeZone default1 = TimeZone.getDefault();
            final boolean inDaylightTime = default1.inDaylightTime(new Date());
            if (!default1.equals(this.lastReportedTimeZone) || inDaylightTime != this.lastReportedDST) {
                final String id = default1.getID();
                String s;
                if (inDaylightTime) {
                    s = "T";
                }
                else {
                    s = "F";
                }
                format = String.format("#zone=\"%s\", dst=%s\n", id, s);
                this.lastReportedTimeZone = default1;
                this.lastReportedDST = inDaylightTime;
                RouteRecorder.sLogger.v(String.format("setting zoneinfo:  %s dst=%b", default1.getID(), inDaylightTime));
            }
            final StringBuilder sb = new StringBuilder();
            String s2;
            if (b) {
                s2 = "[secondary] ";
            }
            else {
                s2 = "";
            }
            TaskManager.getInstance().execute(this.writeLocation(format + sb.append(s2).append(location.getLatitude()).append(",").append(location.getLongitude()).append(",").append(location.getBearing()).append(",").append(location.getSpeed()).append(",").append(location.getAccuracy()).append(",").append(location.getAltitude()).append(",").append(location.getTime()).append(",").append(provider).toString()), 9);
        }
    }
    
    private Runnable writeLocation(final String s) {
        return new Runnable() {
            @Override
            public void run() {
                if (RouteRecorder.this.isRecording) {
                    try {
                        RouteRecorder.this.driveLogWriter.write(s + "\n");
                        RouteRecorder.this.driveLogWriter.flush();
                    }
                    catch (IOException ex) {
                        RouteRecorder.sLogger.e(ex);
                    }
                }
            }
        };
    }
    
    private Runnable writeMarker(final String s) {
        return new Runnable() {
            @Override
            public void run() {
                if (RouteRecorder.this.isRecording) {
                    try {
                        RouteRecorder.this.driveLogWriter.write(s + "\n");
                    }
                    catch (IOException ex) {
                        RouteRecorder.sLogger.e(ex);
                    }
                }
            }
        };
    }
    
    public boolean bPrepare(final String p0, final boolean p1) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: dup            
        //     2: astore_3       
        //     3: monitorenter   
        //     4: aload_1        
        //     5: invokestatic    com/navdy/hud/app/debug/DriveRecorder.getDriveLogsDir:(Ljava/lang/String;)Ljava/io/File;
        //     8: astore          4
        //    10: new             Ljava/io/File;
        //    13: astore          5
        //    15: aload           5
        //    17: aload           4
        //    19: aload_1        
        //    20: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //    23: aload           5
        //    25: invokevirtual   java/io/File.lastModified:()J
        //    28: lstore          6
        //    30: aload_1        
        //    31: aload_0        
        //    32: getfield        com/navdy/hud/app/debug/RouteRecorder.preparedFileName:Ljava/lang/String;
        //    35: invokestatic    android/text/TextUtils.equals:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
        //    38: ifeq            66
        //    41: aload_0        
        //    42: getfield        com/navdy/hud/app/debug/RouteRecorder.preparedFileLastModifiedTime:J
        //    45: lload           6
        //    47: lcmp           
        //    48: ifne            66
        //    51: getstatic       com/navdy/hud/app/debug/RouteRecorder.sLogger:Lcom/navdy/service/library/log/Logger;
        //    54: ldc_w           "Already prepared, ready for playback"
        //    57: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //    60: iconst_1       
        //    61: istore_2       
        //    62: aload_3        
        //    63: monitorexit    
        //    64: iload_2        
        //    65: ireturn        
        //    66: getstatic       com/navdy/hud/app/debug/RouteRecorder.sLogger:Lcom/navdy/service/library/log/Logger;
        //    69: astore          8
        //    71: new             Ljava/lang/StringBuilder;
        //    74: astore          4
        //    76: aload           4
        //    78: invokespecial   java/lang/StringBuilder.<init>:()V
        //    81: aload           8
        //    83: aload           4
        //    85: ldc_w           "Preparing for playback from file : "
        //    88: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    91: aload_1        
        //    92: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    95: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    98: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   101: aload_0        
        //   102: aload_1        
        //   103: putfield        com/navdy/hud/app/debug/RouteRecorder.preparedFileName:Ljava/lang/String;
        //   106: aload_0        
        //   107: lload           6
        //   109: putfield        com/navdy/hud/app/debug/RouteRecorder.preparedFileLastModifiedTime:J
        //   112: aconst_null    
        //   113: astore          8
        //   115: aconst_null    
        //   116: astore          9
        //   118: aload           8
        //   120: astore_1       
        //   121: new             Ljava/io/BufferedReader;
        //   124: astore          4
        //   126: aload           8
        //   128: astore_1       
        //   129: new             Ljava/io/InputStreamReader;
        //   132: astore          10
        //   134: aload           8
        //   136: astore_1       
        //   137: new             Ljava/io/FileInputStream;
        //   140: astore          11
        //   142: aload           8
        //   144: astore_1       
        //   145: aload           11
        //   147: aload           5
        //   149: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //   152: aload           8
        //   154: astore_1       
        //   155: aload           10
        //   157: aload           11
        //   159: ldc_w           "utf-8"
        //   162: invokespecial   java/io/InputStreamReader.<init>:(Ljava/io/InputStream;Ljava/lang/String;)V
        //   165: aload           8
        //   167: astore_1       
        //   168: aload           4
        //   170: aload           10
        //   172: invokespecial   java/io/BufferedReader.<init>:(Ljava/io/Reader;)V
        //   175: new             Ljava/util/ArrayList;
        //   178: astore_1       
        //   179: aload_1        
        //   180: invokespecial   java/util/ArrayList.<init>:()V
        //   183: aload_0        
        //   184: aload_1        
        //   185: putfield        com/navdy/hud/app/debug/RouteRecorder.currentLocations:Ljava/util/List;
        //   188: aload           4
        //   190: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //   193: astore          8
        //   195: aload           8
        //   197: ifnull          561
        //   200: aload           8
        //   202: ldc_w           "#"
        //   205: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   208: ifne            188
        //   211: aload           8
        //   213: ldc             "[secondary] "
        //   215: invokevirtual   java/lang/String.indexOf:(Ljava/lang/String;)I
        //   218: istore          12
        //   220: iload           12
        //   222: iflt            420
        //   225: iconst_1       
        //   226: istore          13
        //   228: aload           8
        //   230: astore_1       
        //   231: iload           13
        //   233: ifeq            250
        //   236: aload           8
        //   238: ldc             "[secondary] "
        //   240: invokevirtual   java/lang/String.length:()I
        //   243: iload           12
        //   245: iadd           
        //   246: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
        //   249: astore_1       
        //   250: iload_2        
        //   251: ifne            426
        //   254: iload           13
        //   256: ifne            188
        //   259: aload_1        
        //   260: ldc_w           ","
        //   263: invokevirtual   java/lang/String.split:(Ljava/lang/String;)[Ljava/lang/String;
        //   266: astore_1       
        //   267: aload_1        
        //   268: arraylength    
        //   269: bipush          8
        //   271: if_icmpne       434
        //   274: new             Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //   277: astore          8
        //   279: aload           8
        //   281: invokespecial   com/navdy/service/library/events/location/Coordinate$Builder.<init>:()V
        //   284: aload           8
        //   286: aload_1        
        //   287: iconst_0       
        //   288: aaload         
        //   289: invokestatic    java/lang/Double.parseDouble:(Ljava/lang/String;)D
        //   292: invokestatic    java/lang/Double.valueOf:(D)Ljava/lang/Double;
        //   295: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.latitude:(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //   298: aload_1        
        //   299: iconst_1       
        //   300: aaload         
        //   301: invokestatic    java/lang/Double.parseDouble:(Ljava/lang/String;)D
        //   304: invokestatic    java/lang/Double.valueOf:(D)Ljava/lang/Double;
        //   307: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.longitude:(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //   310: aload_1        
        //   311: iconst_2       
        //   312: aaload         
        //   313: invokestatic    java/lang/Float.parseFloat:(Ljava/lang/String;)F
        //   316: invokestatic    java/lang/Float.valueOf:(F)Ljava/lang/Float;
        //   319: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.bearing:(Ljava/lang/Float;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //   322: aload_1        
        //   323: iconst_3       
        //   324: aaload         
        //   325: invokestatic    java/lang/Float.parseFloat:(Ljava/lang/String;)F
        //   328: invokestatic    java/lang/Float.valueOf:(F)Ljava/lang/Float;
        //   331: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.speed:(Ljava/lang/Float;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //   334: aload_1        
        //   335: iconst_4       
        //   336: aaload         
        //   337: invokestatic    java/lang/Float.parseFloat:(Ljava/lang/String;)F
        //   340: invokestatic    java/lang/Float.valueOf:(F)Ljava/lang/Float;
        //   343: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.accuracy:(Ljava/lang/Float;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //   346: aload_1        
        //   347: iconst_5       
        //   348: aaload         
        //   349: invokestatic    java/lang/Double.parseDouble:(Ljava/lang/String;)D
        //   352: invokestatic    java/lang/Double.valueOf:(D)Ljava/lang/Double;
        //   355: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.altitude:(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //   358: aload_1        
        //   359: bipush          6
        //   361: aaload         
        //   362: invokestatic    java/lang/Long.parseLong:(Ljava/lang/String;)J
        //   365: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //   368: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.timestamp:(Ljava/lang/Long;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //   371: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.build:()Lcom/navdy/service/library/events/location/Coordinate;
        //   374: astore_1       
        //   375: aload_0        
        //   376: getfield        com/navdy/hud/app/debug/RouteRecorder.currentLocations:Ljava/util/List;
        //   379: aload_1        
        //   380: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   385: pop            
        //   386: goto            188
        //   389: astore          8
        //   391: aload           4
        //   393: astore_1       
        //   394: getstatic       com/navdy/hud/app/debug/RouteRecorder.sLogger:Lcom/navdy/service/library/log/Logger;
        //   397: ldc_w           "Error parsing the file, prepare failed"
        //   400: aload           8
        //   402: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   405: iconst_0       
        //   406: istore_2       
        //   407: aload           4
        //   409: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   412: goto            62
        //   415: astore_1       
        //   416: aload_3        
        //   417: monitorexit    
        //   418: aload_1        
        //   419: athrow         
        //   420: iconst_0       
        //   421: istore          13
        //   423: goto            228
        //   426: iload           13
        //   428: ifne            259
        //   431: goto            188
        //   434: aload_1        
        //   435: arraylength    
        //   436: bipush          6
        //   438: if_icmpne       188
        //   441: new             Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //   444: astore          8
        //   446: aload           8
        //   448: invokespecial   com/navdy/service/library/events/location/Coordinate$Builder.<init>:()V
        //   451: aload           8
        //   453: aload_1        
        //   454: iconst_0       
        //   455: aaload         
        //   456: invokestatic    java/lang/Double.parseDouble:(Ljava/lang/String;)D
        //   459: invokestatic    java/lang/Double.valueOf:(D)Ljava/lang/Double;
        //   462: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.latitude:(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //   465: aload_1        
        //   466: iconst_1       
        //   467: aaload         
        //   468: invokestatic    java/lang/Double.parseDouble:(Ljava/lang/String;)D
        //   471: invokestatic    java/lang/Double.valueOf:(D)Ljava/lang/Double;
        //   474: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.longitude:(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //   477: aload_1        
        //   478: iconst_2       
        //   479: aaload         
        //   480: invokestatic    java/lang/Float.parseFloat:(Ljava/lang/String;)F
        //   483: invokestatic    java/lang/Float.valueOf:(F)Ljava/lang/Float;
        //   486: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.bearing:(Ljava/lang/Float;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //   489: aload_1        
        //   490: iconst_3       
        //   491: aaload         
        //   492: invokestatic    java/lang/Float.parseFloat:(Ljava/lang/String;)F
        //   495: invokestatic    java/lang/Float.valueOf:(F)Ljava/lang/Float;
        //   498: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.speed:(Ljava/lang/Float;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //   501: fconst_1       
        //   502: invokestatic    java/lang/Float.valueOf:(F)Ljava/lang/Float;
        //   505: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.accuracy:(Ljava/lang/Float;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //   508: dconst_0       
        //   509: invokestatic    java/lang/Double.valueOf:(D)Ljava/lang/Double;
        //   512: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.altitude:(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //   515: aload_1        
        //   516: iconst_4       
        //   517: aaload         
        //   518: invokestatic    java/lang/Long.parseLong:(Ljava/lang/String;)J
        //   521: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //   524: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.timestamp:(Ljava/lang/Long;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //   527: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.build:()Lcom/navdy/service/library/events/location/Coordinate;
        //   530: astore_1       
        //   531: aload_0        
        //   532: getfield        com/navdy/hud/app/debug/RouteRecorder.currentLocations:Ljava/util/List;
        //   535: aload_1        
        //   536: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   541: pop            
        //   542: goto            188
        //   545: astore          8
        //   547: aload           4
        //   549: astore_1       
        //   550: aload           8
        //   552: astore          4
        //   554: aload_1        
        //   555: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   558: aload           4
        //   560: athrow         
        //   561: iconst_1       
        //   562: istore_2       
        //   563: aload           4
        //   565: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   568: goto            62
        //   571: astore          4
        //   573: goto            554
        //   576: astore          8
        //   578: aload           9
        //   580: astore          4
        //   582: goto            391
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  4      60     415    420    Any
        //  66     112    415    420    Any
        //  121    126    576    585    Ljava/lang/Exception;
        //  121    126    571    576    Any
        //  129    134    576    585    Ljava/lang/Exception;
        //  129    134    571    576    Any
        //  137    142    576    585    Ljava/lang/Exception;
        //  137    142    571    576    Any
        //  145    152    576    585    Ljava/lang/Exception;
        //  145    152    571    576    Any
        //  155    165    576    585    Ljava/lang/Exception;
        //  155    165    571    576    Any
        //  168    175    576    585    Ljava/lang/Exception;
        //  168    175    571    576    Any
        //  175    188    389    391    Ljava/lang/Exception;
        //  175    188    545    554    Any
        //  188    195    389    391    Ljava/lang/Exception;
        //  188    195    545    554    Any
        //  200    220    389    391    Ljava/lang/Exception;
        //  200    220    545    554    Any
        //  236    250    389    391    Ljava/lang/Exception;
        //  236    250    545    554    Any
        //  259    386    389    391    Ljava/lang/Exception;
        //  259    386    545    554    Any
        //  394    405    571    576    Any
        //  407    412    415    420    Any
        //  434    542    389    391    Ljava/lang/Exception;
        //  434    542    545    554    Any
        //  554    561    415    420    Any
        //  563    568    415    420    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Could not infer any expression.
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:374)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:96)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:109)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public String getLabel() {
        return this.recordingLabel;
    }
    
    public void injectLocation(final Location location, final boolean b) {
        this.persistLocationToFile(location, b);
    }
    
    public void injectMarker(final String s) {
        if (this.isRecording) {
            TaskManager.getInstance().execute(this.writeMarker(s), 9);
        }
    }
    
    public boolean isPaused() {
        return this.playbackState == DriveRecorder.State.PAUSED;
    }
    
    public boolean isPlaying() {
        return this.playbackState == DriveRecorder.State.PLAYING;
    }
    
    public boolean isRecording() {
        return this.isRecording;
    }
    
    public boolean isStopped() {
        return this.playbackState == DriveRecorder.State.STOPPED;
    }
    
    public void pausePlayback() {
        if (!this.isPlaying()) {
            RouteRecorder.sLogger.v("already stopped, no-op");
        }
        else {
            this.handler.removeCallbacks(this.injectFakeLocationRunnable);
            this.playbackState = DriveRecorder.State.PAUSED;
            this.gpsManager.startUbloxReporting();
            this.connectionService.setSimulatingGpsCoordinates(false);
        }
    }
    
    public void prepare(final String s, final boolean b) {
        if (this.isPlaying() || this.isPaused()) {
            RouteRecorder.sLogger.e("Playback is not stopped , current state " + this.playbackState.name());
        }
        else {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    if (!RouteRecorder.this.bPrepare(s, b)) {
                        RouteRecorder.sLogger.e("Prepare failed ");
                    }
                    else {
                        RouteRecorder.sLogger.e("Prepare succeeded ");
                    }
                }
            }, 9);
        }
    }
    
    public void release() {
        synchronized (this) {
            if (this.currentLocations != null) {
                this.currentLocations.clear();
                this.currentLocations = null;
            }
            this.preparedFileName = null;
            this.preparedFileLastModifiedTime = -1L;
        }
    }
    
    public void requestRecordings() {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                final ArrayList<String> list = new ArrayList<String>();
                final File[] listFiles = new File(RouteRecorder.this.pathManager.getMapsPartitionPath() + File.separator + "drive_logs").listFiles();
                if (listFiles != null) {
                    for (final File file : listFiles) {
                        if (!file.getName().endsWith(".obd")) {
                            list.add(file.getName());
                        }
                    }
                }
                RouteRecorder.this.connectionService.sendMessage(new DriveRecordingsResponse(list));
            }
        }, 1);
    }
    
    public boolean restartPlayback() {
        boolean b = false;
        if (this.isPlaying() || this.isPaused()) {
            if (this.isPaused()) {
                this.gpsManager.stopUbloxReporting();
                this.connectionService.setSimulatingGpsCoordinates(false);
            }
            this.playbackState = DriveRecorder.State.PLAYING;
            this.currentInjectionIndex = 0;
            this.handler.removeCallbacks(this.injectFakeLocationRunnable);
            this.handler.post(this.injectFakeLocationRunnable);
            b = true;
        }
        return b;
    }
    
    public void resumePlayback() {
        if (this.isPaused()) {
            this.gpsManager.stopUbloxReporting();
            this.playbackState = DriveRecorder.State.PLAYING;
            this.connectionService.setSimulatingGpsCoordinates(true);
            this.handler.post(this.injectFakeLocationRunnable);
        }
    }
    
    public void setConnectionService(final HudConnectionService connectionService) {
        this.connectionService = connectionService;
    }
    
    public void startPlayback(final String s, final boolean b, final boolean b2) {
        this.startPlayback(s, b, false, b2);
    }
    
    public void startPlayback(final String s, final boolean b, final boolean isLooping, final boolean b2) {
        if (this.isPlaying() || this.isRecording) {
            RouteRecorder.sLogger.v("already busy, no-op");
            if (!b2) {
                this.connectionService.sendMessage(new StartDrivePlaybackResponse(RequestStatus.REQUEST_INVALID_STATE));
            }
        }
        else {
            this.playbackState = DriveRecorder.State.PLAYING;
            this.isLooping = isLooping;
            this.gpsManager.stopUbloxReporting();
            this.connectionService.setSimulatingGpsCoordinates(true);
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (!RouteRecorder.this.bPrepare(s, b)) {
                            if (!b2) {
                                RouteRecorder.this.connectionService.sendMessage(new StartDrivePlaybackResponse(RequestStatus.REQUEST_SERVICE_ERROR));
                            }
                            RouteRecorder.this.stopPlayback();
                        }
                        else {
                            RouteRecorder.sLogger.d("Prepare succeeded");
                            if (!b2) {
                                RouteRecorder.this.connectionService.sendMessage(new StartDrivePlaybackResponse(RequestStatus.REQUEST_SUCCESS));
                            }
                            RouteRecorder.sLogger.d("Starting playback");
                            RouteRecorder.this.currentInjectionIndex = 0;
                            RouteRecorder.this.handler.post(RouteRecorder.this.injectFakeLocationRunnable);
                        }
                    }
                    catch (Throwable t) {
                        if (!b2) {
                            RouteRecorder.this.connectionService.sendMessage(new StartDrivePlaybackResponse(RequestStatus.REQUEST_SERVICE_ERROR));
                        }
                        RouteRecorder.this.stopPlayback();
                        RouteRecorder.sLogger.e(t);
                    }
                }
            }, 1);
        }
    }
    
    public String startRecording(String string, final boolean b) {
        if (this.isRecording || this.isPlaying()) {
            RouteRecorder.sLogger.v("already busy");
            if (!b) {
                this.connectionService.sendMessage(new StartDriveRecordingResponse(RequestStatus.REQUEST_INVALID_STATE));
            }
            string = null;
        }
        else {
            RouteRecorder.sLogger.d("Starting the drive recording");
            this.isRecording = true;
            this.recordingLabel = string;
            string = GenericUtil.normalizeToFilename(string) + "_" + RouteRecorder.DATE_FORMAT.format(new Date()) + ".log";
            TaskManager.getInstance().execute(this.createDriveRecord(string, b), 9);
        }
        return string;
    }
    
    public void stopPlayback() {
        if (this.isStopped()) {
            RouteRecorder.sLogger.v("already stopped, no-op");
        }
        else {
            this.handler.removeCallbacks(this.injectFakeLocationRunnable);
            this.currentInjectionIndex = 0;
            this.playbackState = DriveRecorder.State.STOPPED;
            this.gpsManager.startUbloxReporting();
            this.isLooping = false;
            this.connectionService.setSimulatingGpsCoordinates(false);
        }
    }
    
    public void stopRecording(final boolean b) {
        if (!this.isRecording) {
            RouteRecorder.sLogger.v("already stopped, no-op");
        }
        else {
            RouteRecorder.sLogger.d("Stopping the drive recording");
            this.handler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    RouteRecorder.this.locationManager.removeUpdates(RouteRecorder.this.locationListener);
                    TaskManager.getInstance().execute(new Runnable() {
                        @Override
                        public void run() {
                            RouteRecorder.this.bStopRecording(b);
                        }
                    }, 9);
                }
            });
        }
    }
}
