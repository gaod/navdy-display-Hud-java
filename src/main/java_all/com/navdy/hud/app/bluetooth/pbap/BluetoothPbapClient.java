package com.navdy.hud.app.bluetooth.pbap;

import android.os.Message;
import java.lang.ref.WeakReference;
import android.util.Log;
import android.bluetooth.BluetoothDevice;
import android.os.Handler;

public class BluetoothPbapClient
{
    public static final String CCH_PATH = "telecom/cch.vcf";
    public static final int EVENT_PULL_PHONE_BOOK_DONE = 2;
    public static final int EVENT_PULL_PHONE_BOOK_ERROR = 102;
    public static final int EVENT_PULL_PHONE_BOOK_SIZE_DONE = 5;
    public static final int EVENT_PULL_PHONE_BOOK_SIZE_ERROR = 105;
    public static final int EVENT_PULL_VCARD_ENTRY_DONE = 4;
    public static final int EVENT_PULL_VCARD_ENTRY_ERROR = 104;
    public static final int EVENT_PULL_VCARD_LISTING_DONE = 3;
    public static final int EVENT_PULL_VCARD_LISTING_ERROR = 103;
    public static final int EVENT_PULL_VCARD_LISTING_SIZE_DONE = 6;
    public static final int EVENT_PULL_VCARD_LISTING_SIZE_ERROR = 106;
    public static final int EVENT_SESSION_AUTH_REQUESTED = 203;
    public static final int EVENT_SESSION_AUTH_TIMEOUT = 204;
    public static final int EVENT_SESSION_CONNECTED = 201;
    public static final int EVENT_SESSION_DISCONNECTED = 202;
    public static final int EVENT_SET_PHONE_BOOK_DONE = 1;
    public static final int EVENT_SET_PHONE_BOOK_ERROR = 101;
    public static final String ICH_PATH = "telecom/ich.vcf";
    public static final short MAX_LIST_COUNT = -1;
    public static final String MCH_PATH = "telecom/mch.vcf";
    public static final String OCH_PATH = "telecom/och.vcf";
    public static final byte ORDER_BY_ALPHABETICAL = 1;
    public static final byte ORDER_BY_DEFAULT = -1;
    public static final byte ORDER_BY_INDEXED = 0;
    public static final byte ORDER_BY_PHONETIC = 2;
    public static final String PB_PATH = "telecom/pb.vcf";
    public static final byte SEARCH_ATTR_NAME = 0;
    public static final byte SEARCH_ATTR_NUMBER = 1;
    public static final byte SEARCH_ATTR_SOUND = 2;
    public static final String SIM_CCH_PATH = "SIM1/telecom/cch.vcf";
    public static final String SIM_ICH_PATH = "SIM1/telecom/ich.vcf";
    public static final String SIM_MCH_PATH = "SIM1/telecom/mch.vcf";
    public static final String SIM_OCH_PATH = "SIM1/telecom/och.vcf";
    public static final String SIM_PB_PATH = "SIM1/telecom/pb.vcf";
    private static final String TAG;
    public static final long VCARD_ATTR_ADDR = 32L;
    public static final long VCARD_ATTR_AGENT = 32768L;
    public static final long VCARD_ATTR_BDAY = 16L;
    public static final long VCARD_ATTR_CATEGORIES = 16777216L;
    public static final long VCARD_ATTR_CLASS = 67108864L;
    public static final long VCARD_ATTR_EMAIL = 256L;
    public static final long VCARD_ATTR_FN = 2L;
    public static final long VCARD_ATTR_GEO = 2048L;
    public static final long VCARD_ATTR_KEY = 4194304L;
    public static final long VCARD_ATTR_LABEL = 64L;
    public static final long VCARD_ATTR_LOGO = 16384L;
    public static final long VCARD_ATTR_MAILER = 512L;
    public static final long VCARD_ATTR_N = 4L;
    public static final long VCARD_ATTR_NICKNAME = 8388608L;
    public static final long VCARD_ATTR_NOTE = 131072L;
    public static final long VCARD_ATTR_ORG = 65536L;
    public static final long VCARD_ATTR_PHOTO = 8L;
    public static final long VCARD_ATTR_PROID = 33554432L;
    public static final long VCARD_ATTR_REV = 262144L;
    public static final long VCARD_ATTR_ROLE = 8192L;
    public static final long VCARD_ATTR_SORT_STRING = 134217728L;
    public static final long VCARD_ATTR_SOUND = 524288L;
    public static final long VCARD_ATTR_TEL = 128L;
    public static final long VCARD_ATTR_TITLE = 4096L;
    public static final long VCARD_ATTR_TZ = 1024L;
    public static final long VCARD_ATTR_UID = 2097152L;
    public static final long VCARD_ATTR_URL = 1048576L;
    public static final long VCARD_ATTR_VERSION = 1L;
    public static final long VCARD_ATTR_X_IRMC_CALL_DATETIME = 268435456L;
    public static final byte VCARD_TYPE_21 = 0;
    public static final byte VCARD_TYPE_30 = 1;
    private final Handler mClientHandler;
    private ConnectionState mConnectionState;
    private final BluetoothPbapSession mSession;
    private SessionHandler mSessionHandler;
    
    static {
        TAG = BluetoothPbapClient.class.getSimpleName();
    }
    
    public BluetoothPbapClient(final BluetoothDevice bluetoothDevice, final Handler mClientHandler) {
        this.mConnectionState = ConnectionState.DISCONNECTED;
        if (bluetoothDevice == null) {
            throw new NullPointerException("BluetothDevice is null");
        }
        this.mClientHandler = mClientHandler;
        this.mSessionHandler = new SessionHandler(this);
        this.mSession = new BluetoothPbapSession(bluetoothDevice, this.mSessionHandler);
    }
    
    private void sendToClient(final int n) {
        this.sendToClient(n, 0, null);
    }
    
    private void sendToClient(final int n, final int n2) {
        this.sendToClient(n, n2, null);
    }
    
    private void sendToClient(final int n, final int n2, final Object o) {
        this.mClientHandler.obtainMessage(n, n2, 0, o).sendToTarget();
    }
    
    private void sendToClient(final int n, final Object o) {
        this.sendToClient(n, 0, o);
    }
    
    public void abort() {
        this.mSession.abort();
    }
    
    public void connect() {
        this.mSession.start();
    }
    
    public void disconnect() {
        this.mSession.stop();
    }
    
    public void finalize() {
        if (this.mSession != null) {
            this.mSession.stop();
        }
    }
    
    public ConnectionState getState() {
        return this.mConnectionState;
    }
    
    public boolean pullPhoneBook(final String s) {
        return this.pullPhoneBook(s, 0L, (byte)0, 0, 0);
    }
    
    public boolean pullPhoneBook(final String s, final int n, final int n2) {
        return this.pullPhoneBook(s, 0L, (byte)0, n, n2);
    }
    
    public boolean pullPhoneBook(final String s, final long n, final byte b) {
        return this.pullPhoneBook(s, n, b, 0, 0);
    }
    
    public boolean pullPhoneBook(final String s, final long n, final byte b, final int n2, final int n3) {
        return this.mSession.makeRequest(new BluetoothPbapRequestPullPhoneBook(s, n, b, n2, n3));
    }
    
    public boolean pullPhoneBookSize(final String s) {
        return this.mSession.makeRequest(new BluetoothPbapRequestPullPhoneBookSize(s));
    }
    
    public boolean pullVcardEntry(final String s) {
        return this.pullVcardEntry(s, 0L, (byte)0);
    }
    
    public boolean pullVcardEntry(final String s, final long n, final byte b) {
        return this.mSession.makeRequest(new BluetoothPbapRequestPullVcardEntry(s, n, b));
    }
    
    public boolean pullVcardListing(final String s) {
        return this.pullVcardListing(s, (byte)(-1), (byte)0, null, 0, 0);
    }
    
    public boolean pullVcardListing(final String s, final byte b) {
        return this.pullVcardListing(s, b, (byte)0, null, 0, 0);
    }
    
    public boolean pullVcardListing(final String s, final byte b, final byte b2, final String s2, final int n, final int n2) {
        return this.mSession.makeRequest(new BluetoothPbapRequestPullVcardListing(s, b, b2, s2, n, n2));
    }
    
    public boolean pullVcardListing(final String s, final byte b, final int n, final int n2) {
        return this.pullVcardListing(s, b, (byte)0, null, n, n2);
    }
    
    public boolean pullVcardListing(final String s, final byte b, final String s2) {
        return this.pullVcardListing(s, (byte)(-1), b, s2, 0, 0);
    }
    
    public boolean pullVcardListing(final String s, final int n, final int n2) {
        return this.pullVcardListing(s, (byte)(-1), (byte)0, null, n, n2);
    }
    
    public boolean pullVcardListingSize(final String s) {
        return this.mSession.makeRequest(new BluetoothPbapRequestPullVcardListingSize(s));
    }
    
    public boolean setAuthResponse(final String authResponse) {
        Log.d(BluetoothPbapClient.TAG, " setAuthResponse key=" + authResponse);
        return this.mSession.setAuthResponse(authResponse);
    }
    
    public boolean setPhoneBookFolderDown(final String s) {
        return this.mSession.makeRequest(new BluetoothPbapRequestSetPath(s));
    }
    
    public boolean setPhoneBookFolderRoot() {
        return this.mSession.makeRequest(new BluetoothPbapRequestSetPath(false));
    }
    
    public boolean setPhoneBookFolderUp() {
        return this.mSession.makeRequest(new BluetoothPbapRequestSetPath(true));
    }
    
    public enum ConnectionState
    {
        CONNECTED, 
        CONNECTING, 
        DISCONNECTED, 
        DISCONNECTING;
    }
    
    private static class SessionHandler extends Handler
    {
        private final WeakReference<BluetoothPbapClient> mClient;
        
        SessionHandler(final BluetoothPbapClient bluetoothPbapClient) {
            this.mClient = new WeakReference<BluetoothPbapClient>(bluetoothPbapClient);
        }
        
        public void handleMessage(final Message message) {
            Log.d(BluetoothPbapClient.TAG, "handleMessage: what=" + message.what);
            final BluetoothPbapClient bluetoothPbapClient = this.mClient.get();
            if (bluetoothPbapClient != null) {
                switch (message.what) {
                    case 3: {
                        final BluetoothPbapRequest bluetoothPbapRequest = (BluetoothPbapRequest)message.obj;
                        if (bluetoothPbapRequest instanceof BluetoothPbapRequestPullPhoneBookSize) {
                            bluetoothPbapClient.sendToClient(5, ((BluetoothPbapRequestPullPhoneBookSize)bluetoothPbapRequest).getSize());
                            break;
                        }
                        if (bluetoothPbapRequest instanceof BluetoothPbapRequestPullVcardListingSize) {
                            bluetoothPbapClient.sendToClient(6, ((BluetoothPbapRequestPullVcardListingSize)bluetoothPbapRequest).getSize());
                            break;
                        }
                        if (bluetoothPbapRequest instanceof BluetoothPbapRequestPullPhoneBook) {
                            final BluetoothPbapRequestPullPhoneBook bluetoothPbapRequestPullPhoneBook = (BluetoothPbapRequestPullPhoneBook)bluetoothPbapRequest;
                            bluetoothPbapClient.sendToClient(2, bluetoothPbapRequestPullPhoneBook.getNewMissedCalls(), bluetoothPbapRequestPullPhoneBook.getList());
                            break;
                        }
                        if (bluetoothPbapRequest instanceof BluetoothPbapRequestPullVcardListing) {
                            final BluetoothPbapRequestPullVcardListing bluetoothPbapRequestPullVcardListing = (BluetoothPbapRequestPullVcardListing)bluetoothPbapRequest;
                            bluetoothPbapClient.sendToClient(3, bluetoothPbapRequestPullVcardListing.getNewMissedCalls(), bluetoothPbapRequestPullVcardListing.getList());
                            break;
                        }
                        if (bluetoothPbapRequest instanceof BluetoothPbapRequestPullVcardEntry) {
                            bluetoothPbapClient.sendToClient(4, ((BluetoothPbapRequestPullVcardEntry)bluetoothPbapRequest).getVcard());
                            break;
                        }
                        if (bluetoothPbapRequest instanceof BluetoothPbapRequestSetPath) {
                            bluetoothPbapClient.sendToClient(1);
                            break;
                        }
                        break;
                    }
                    case 4: {
                        final BluetoothPbapRequest bluetoothPbapRequest2 = (BluetoothPbapRequest)message.obj;
                        if (bluetoothPbapRequest2 instanceof BluetoothPbapRequestPullPhoneBookSize) {
                            bluetoothPbapClient.sendToClient(105);
                            break;
                        }
                        if (bluetoothPbapRequest2 instanceof BluetoothPbapRequestPullVcardListingSize) {
                            bluetoothPbapClient.sendToClient(106);
                            break;
                        }
                        if (bluetoothPbapRequest2 instanceof BluetoothPbapRequestPullPhoneBook) {
                            bluetoothPbapClient.sendToClient(102);
                            break;
                        }
                        if (bluetoothPbapRequest2 instanceof BluetoothPbapRequestPullVcardListing) {
                            bluetoothPbapClient.sendToClient(103);
                            break;
                        }
                        if (bluetoothPbapRequest2 instanceof BluetoothPbapRequestPullVcardEntry) {
                            bluetoothPbapClient.sendToClient(104);
                            break;
                        }
                        if (bluetoothPbapRequest2 instanceof BluetoothPbapRequestSetPath) {
                            bluetoothPbapClient.sendToClient(101);
                            break;
                        }
                        break;
                    }
                    case 8:
                        bluetoothPbapClient.sendToClient(203);
                        break;
                    case 9:
                        bluetoothPbapClient.sendToClient(204);
                        break;
                    case 5:
                        bluetoothPbapClient.mConnectionState = ConnectionState.CONNECTING;
                        break;
                    case 6:
                        bluetoothPbapClient.mConnectionState = ConnectionState.CONNECTED;
                        bluetoothPbapClient.sendToClient(201);
                        break;
                    case 7:
                        bluetoothPbapClient.mConnectionState = ConnectionState.DISCONNECTED;
                        bluetoothPbapClient.sendToClient(202);
                        break;
                }
            }
        }
    }
}
