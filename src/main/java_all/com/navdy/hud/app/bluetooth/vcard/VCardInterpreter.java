package com.navdy.hud.app.bluetooth.vcard;

public interface VCardInterpreter
{
    void onEntryEnded();
    
    void onEntryStarted();
    
    void onPropertyCreated(final VCardProperty p0);
    
    void onVCardEnded();
    
    void onVCardStarted();
}
