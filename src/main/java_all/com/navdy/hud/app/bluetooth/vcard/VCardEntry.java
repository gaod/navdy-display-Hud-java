package com.navdy.hud.app.bluetooth.vcard;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import android.provider.ContactsContract;
import android.content.ContentProviderOperation;
import android.provider.ContactsContract;
import android.content.ContentProviderOperation;
import com.navdy.hud.app.util.DateUtil;
import android.text.TextUtils;
import java.util.Iterator;
import android.util.Log;
import java.util.Arrays;
import java.util.Collection;
import android.net.Uri;
import android.provider.ContactsContract;
import android.content.ContentResolver;
import java.util.Collections;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Date;
import android.accounts.Account;
import java.util.Map;
import java.util.List;

public class VCardEntry
{
    private static final int DEFAULT_ORGANIZATION_TYPE = 1;
    private static final String LOG_TAG = "vCard";
    private static final List<String> sEmptyList;
    private static final Map<String, Integer> sImMap;
    private final Account mAccount;
    private List<AndroidCustomData> mAndroidCustomDataList;
    private AnniversaryData mAnniversary;
    private BirthdayData mBirthday;
    private Date mCallTime;
    private int mCallType;
    private List<VCardEntry> mChildren;
    private List<EmailData> mEmailList;
    private List<ImData> mImList;
    private final NameData mNameData;
    private List<NicknameData> mNicknameList;
    private List<NoteData> mNoteList;
    private List<OrganizationData> mOrganizationList;
    private List<PhoneData> mPhoneList;
    private List<PhotoData> mPhotoList;
    private List<PostalData> mPostalList;
    private List<SipData> mSipList;
    private final int mVCardType;
    private List<WebsiteData> mWebsiteList;
    
    static {
        (sImMap = new HashMap<String, Integer>()).put("X-AIM", 0);
        VCardEntry.sImMap.put("X-MSN", 1);
        VCardEntry.sImMap.put("X-YAHOO", 2);
        VCardEntry.sImMap.put("X-ICQ", 6);
        VCardEntry.sImMap.put("X-JABBER", 7);
        VCardEntry.sImMap.put("X-SKYPE-USERNAME", 3);
        VCardEntry.sImMap.put("X-GOOGLE-TALK", 5);
        VCardEntry.sImMap.put("X-GOOGLE TALK", 5);
        sEmptyList = Collections.<String>unmodifiableList((List<? extends String>)new ArrayList<String>(0));
    }
    
    public VCardEntry() {
        this(-1073741824);
    }
    
    public VCardEntry(final int n) {
        this(n, null);
    }
    
    public VCardEntry(final int mvCardType, final Account mAccount) {
        this.mNameData = new NameData();
        this.mVCardType = mvCardType;
        this.mAccount = mAccount;
    }
    
    private void addCallTime(final int mCallType, final Date mCallTime) {
        this.mCallType = mCallType;
        this.mCallTime = mCallTime;
    }
    
    private void addEmail(final int n, final String s, final String s2, final boolean b) {
        if (this.mEmailList == null) {
            this.mEmailList = new ArrayList<EmailData>();
        }
        this.mEmailList.add(new EmailData(s, n, s2, b));
    }
    
    private void addIm(final int n, final String s, final String s2, final int n2, final boolean b) {
        if (this.mImList == null) {
            this.mImList = new ArrayList<ImData>();
        }
        this.mImList.add(new ImData(n, s, s2, n2, b));
    }
    
    private void addNewOrganization(final String s, final String s2, final String s3, final String s4, final int n, final boolean b) {
        if (this.mOrganizationList == null) {
            this.mOrganizationList = new ArrayList<OrganizationData>();
        }
        this.mOrganizationList.add(new OrganizationData(s, s2, s3, s4, n, b));
    }
    
    private void addNickName(final String s) {
        if (this.mNicknameList == null) {
            this.mNicknameList = new ArrayList<NicknameData>();
        }
        this.mNicknameList.add(new NicknameData(s));
    }
    
    private void addNote(final String s) {
        if (this.mNoteList == null) {
            this.mNoteList = new ArrayList<NoteData>(1);
        }
        this.mNoteList.add(new NoteData(s));
    }
    
    private void addPhone(final int n, String s, final String s2, final boolean b) {
        if (this.mPhoneList == null) {
            this.mPhoneList = new ArrayList<PhoneData>();
        }
        final StringBuilder sb = new StringBuilder();
        s = s.trim();
        if (n != 6 && !VCardConfig.refrainPhoneNumberFormatting(this.mVCardType)) {
            int n2 = 0;
            int n3;
            for (int length = s.length(), i = 0; i < length; ++i, n2 = n3) {
                final char char1 = s.charAt(i);
                if (char1 == 'p' || char1 == 'P') {
                    sb.append(',');
                    n3 = 1;
                }
                else if (char1 == 'w' || char1 == 'W') {
                    sb.append(';');
                    n3 = 1;
                }
                else {
                    if ('0' > char1 || char1 > '9') {
                        n3 = n2;
                        if (i != 0) {
                            continue;
                        }
                        n3 = n2;
                        if (char1 != '+') {
                            continue;
                        }
                    }
                    sb.append(char1);
                    n3 = n2;
                }
            }
            if (n2 == 0) {
                s = VCardUtils.PhoneNumberUtilsPort.formatNumber(sb.toString(), VCardUtils.getPhoneNumberFormat(this.mVCardType));
            }
            else {
                s = sb.toString();
            }
        }
        this.mPhoneList.add(new PhoneData(s, n, s2, b));
    }
    
    private void addPhotoBytes(final String s, final byte[] array, final boolean b) {
        if (this.mPhotoList == null) {
            this.mPhotoList = new ArrayList<PhotoData>(1);
        }
        this.mPhotoList.add(new PhotoData(s, array, b));
    }
    
    private void addPostal(final int n, final List<String> list, final String s, final boolean b) {
        if (this.mPostalList == null) {
            this.mPostalList = new ArrayList<PostalData>(0);
        }
        this.mPostalList.add(PostalData.constructPostalData(list, n, s, b, this.mVCardType));
    }
    
    private void addSip(final String s, final int n, final String s2, final boolean b) {
        if (this.mSipList == null) {
            this.mSipList = new ArrayList<SipData>();
        }
        this.mSipList.add(new SipData(s, n, s2, b));
    }
    
    public static VCardEntry buildFromResolver(final ContentResolver contentResolver) {
        return buildFromResolver(contentResolver, ContactsContract$Contacts.CONTENT_URI);
    }
    
    public static VCardEntry buildFromResolver(final ContentResolver contentResolver, final Uri uri) {
        return null;
    }
    
    private String buildSinglePhoneticNameFromSortAsParam(final Map<String, Collection<String>> map) {
        final Collection<String> collection = map.get("SORT-AS");
        String string;
        if (collection != null && collection.size() != 0) {
            if (collection.size() > 1) {
                Log.w("vCard", "Incorrect multiple SORT_AS parameters detected: " + Arrays.toString(collection.toArray()));
            }
            final List<String> constructListFromValue = VCardUtils.constructListFromValue(collection.iterator().next(), this.mVCardType);
            final StringBuilder sb = new StringBuilder();
            final Iterator<String> iterator = constructListFromValue.iterator();
            while (iterator.hasNext()) {
                sb.append(iterator.next());
            }
            string = sb.toString();
        }
        else {
            string = null;
        }
        return string;
    }
    
    private String constructDisplayName() {
        final String s = null;
        String s2;
        if (!TextUtils.isEmpty((CharSequence)this.mNameData.mFormatted)) {
            s2 = this.mNameData.mFormatted;
        }
        else if (!this.mNameData.emptyStructuredName()) {
            s2 = VCardUtils.constructNameFromElements(this.mVCardType, this.mNameData.mFamily, this.mNameData.mMiddle, this.mNameData.mGiven, this.mNameData.mPrefix, this.mNameData.mSuffix);
        }
        else if (!this.mNameData.emptyPhoneticStructuredName()) {
            s2 = VCardUtils.constructNameFromElements(this.mVCardType, this.mNameData.mPhoneticFamily, this.mNameData.mPhoneticMiddle, this.mNameData.mPhoneticGiven);
        }
        else if (this.mEmailList != null && this.mEmailList.size() > 0) {
            s2 = this.mEmailList.get(0).mAddress;
        }
        else if (this.mPhoneList != null && this.mPhoneList.size() > 0) {
            s2 = this.mPhoneList.get(0).mNumber;
        }
        else if (this.mPostalList != null && this.mPostalList.size() > 0) {
            s2 = this.mPostalList.get(0).getFormattedAddress(this.mVCardType);
        }
        else {
            s2 = s;
            if (this.mOrganizationList != null) {
                s2 = s;
                if (this.mOrganizationList.size() > 0) {
                    s2 = this.mOrganizationList.get(0).getFormattedString();
                }
            }
        }
        String s3;
        if ((s3 = s2) == null) {
            s3 = "";
        }
        return s3;
    }
    
    private void handleAndroidCustomProperty(final List<String> list) {
        if (this.mAndroidCustomDataList == null) {
            this.mAndroidCustomDataList = new ArrayList<AndroidCustomData>();
        }
        this.mAndroidCustomDataList.add(AndroidCustomData.constructAndroidCustomData(list));
    }
    
    private void handleNProperty(final List<String> list, final Map<String, Collection<String>> map) {
        this.tryHandleSortAsName(map);
        if (list != null) {
            final int size = list.size();
            if (size >= 1) {
                int n;
                if ((n = size) > 5) {
                    n = 5;
                }
                switch (n) {
                    case 5:
                        this.mNameData.mSuffix = list.get(4);
                    case 4:
                        this.mNameData.mPrefix = list.get(3);
                    case 3:
                        this.mNameData.mMiddle = list.get(2);
                    case 2:
                        this.mNameData.mGiven = list.get(1);
                        break;
                }
                this.mNameData.mFamily = list.get(0);
            }
        }
    }
    
    private void handleOrgValue(final int n, final List<String> list, final Map<String, Collection<String>> map, final boolean b) {
        final String buildSinglePhoneticNameFromSortAsParam = this.buildSinglePhoneticNameFromSortAsParam(map);
        List<String> sEmptyList = list;
        if (list == null) {
            sEmptyList = VCardEntry.sEmptyList;
        }
        final int size = sEmptyList.size();
        String s = null;
        String string = null;
        switch (size) {
            default: {
                s = sEmptyList.get(0);
                final StringBuilder sb = new StringBuilder();
                for (int i = 1; i < size; ++i) {
                    if (i > 1) {
                        sb.append(' ');
                    }
                    sb.append(sEmptyList.get(i));
                }
                string = sb.toString();
                break;
            }
            case 0:
                s = "";
                string = null;
                break;
            case 1:
                s = sEmptyList.get(0);
                string = null;
                break;
        }
        if (this.mOrganizationList == null) {
            this.addNewOrganization(s, string, null, buildSinglePhoneticNameFromSortAsParam, n, b);
        }
        else {
            for (final OrganizationData organizationData : this.mOrganizationList) {
                if (organizationData.mOrganizationName == null && organizationData.mDepartmentName == null) {
                    organizationData.mOrganizationName = s;
                    organizationData.mDepartmentName = string;
                    organizationData.mIsPrimary = b;
                    return;
                }
            }
            this.addNewOrganization(s, string, null, buildSinglePhoneticNameFromSortAsParam, n, b);
        }
    }
    
    private void handlePhoneticNameFromSound(final List<String> list) {
        if (TextUtils.isEmpty((CharSequence)this.mNameData.mPhoneticFamily) && TextUtils.isEmpty((CharSequence)this.mNameData.mPhoneticMiddle) && TextUtils.isEmpty((CharSequence)this.mNameData.mPhoneticGiven) && list != null) {
            final int size = list.size();
            if (size >= 1) {
                int n;
                if ((n = size) > 3) {
                    n = 3;
                }
                if (list.get(0).length() > 0) {
                    final boolean b = true;
                    int n2 = 1;
                    boolean b2;
                    while (true) {
                        b2 = b;
                        if (n2 >= n) {
                            break;
                        }
                        if (list.get(n2).length() > 0) {
                            b2 = false;
                            break;
                        }
                        ++n2;
                    }
                    if (b2) {
                        final String[] split = list.get(0).split(" ");
                        final int length = split.length;
                        if (length == 3) {
                            this.mNameData.mPhoneticFamily = split[0];
                            this.mNameData.mPhoneticMiddle = split[1];
                            this.mNameData.mPhoneticGiven = split[2];
                            return;
                        }
                        if (length == 2) {
                            this.mNameData.mPhoneticFamily = split[0];
                            this.mNameData.mPhoneticGiven = split[1];
                            return;
                        }
                        this.mNameData.mPhoneticGiven = list.get(0);
                        return;
                    }
                }
                switch (n) {
                    case 3:
                        this.mNameData.mPhoneticMiddle = list.get(2);
                    case 2:
                        this.mNameData.mPhoneticGiven = list.get(1);
                        break;
                }
                this.mNameData.mPhoneticFamily = list.get(0);
            }
        }
    }
    
    private void handleSipCase(String substring, final Collection<String> collection) {
        if (!TextUtils.isEmpty((CharSequence)substring)) {
            String substring2 = substring;
            if (substring.startsWith("sip:")) {
                substring2 = substring.substring(4);
                if (substring2.length() == 0) {
                    return;
                }
            }
            int n = -1;
            String s = null;
            substring = null;
            boolean b = false;
            boolean b2 = false;
            int n2 = n;
            if (collection != null) {
                final Iterator<String> iterator = collection.iterator();
                while (true) {
                    b = b2;
                    s = substring;
                    n2 = n;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    final String s2 = iterator.next();
                    final String upperCase = s2.toUpperCase();
                    if (upperCase.equals("PREF")) {
                        b2 = true;
                    }
                    else if (upperCase.equals("HOME")) {
                        n = 1;
                    }
                    else if (upperCase.equals("WORK")) {
                        n = 2;
                    }
                    else {
                        if (n >= 0) {
                            continue;
                        }
                        if (upperCase.startsWith("X-")) {
                            substring = s2.substring(2);
                        }
                        else {
                            substring = s2;
                        }
                        n = 0;
                    }
                }
            }
            int n3;
            if ((n3 = n2) < 0) {
                n3 = 3;
            }
            this.addSip(substring2, n3, s, b);
        }
    }
    
    private void handleTitleValue(final String s) {
        if (this.mOrganizationList == null) {
            this.addNewOrganization(null, null, s, null, 1, false);
        }
        else {
            for (final OrganizationData organizationData : this.mOrganizationList) {
                if (organizationData.mTitle == null) {
                    organizationData.mTitle = s;
                    return;
                }
            }
            this.addNewOrganization(null, null, s, null, 1, false);
        }
    }
    
    private void iterateOneList(final List<? extends EntryElement> list, final EntryElementIterator entryElementIterator) {
        if (list != null && list.size() > 0) {
            entryElementIterator.onElementGroupStarted(((EntryElement)list.get(0)).getEntryLabel());
            final Iterator<? extends EntryElement> iterator = list.iterator();
            while (iterator.hasNext()) {
                entryElementIterator.onElement((EntryElement)iterator.next());
            }
            entryElementIterator.onElementGroupEnded();
        }
    }
    
    private String listToString(final List<String> list) {
        final int size = list.size();
        String string;
        if (size > 1) {
            final StringBuilder sb = new StringBuilder();
            final Iterator<String> iterator = list.iterator();
            while (iterator.hasNext()) {
                sb.append(iterator.next());
                if (size - 1 < 0) {
                    sb.append(";");
                }
            }
            string = sb.toString();
        }
        else if (size == 1) {
            string = list.get(0);
        }
        else {
            string = "";
        }
        return string;
    }
    
    private void tryHandleSortAsName(final Map<String, Collection<String>> map) {
        if (!VCardConfig.isVersion30(this.mVCardType) || (TextUtils.isEmpty((CharSequence)this.mNameData.mPhoneticFamily) && TextUtils.isEmpty((CharSequence)this.mNameData.mPhoneticMiddle) && TextUtils.isEmpty((CharSequence)this.mNameData.mPhoneticGiven))) {
            final Collection<String> collection = map.get("SORT-AS");
            if (collection != null && collection.size() != 0) {
                if (collection.size() > 1) {
                    Log.w("vCard", "Incorrect multiple SORT_AS parameters detected: " + Arrays.toString(collection.toArray()));
                }
                final List<String> constructListFromValue = VCardUtils.constructListFromValue(collection.iterator().next(), this.mVCardType);
                int size;
                if ((size = constructListFromValue.size()) > 3) {
                    size = 3;
                }
                switch (size) {
                    case 3:
                        this.mNameData.mPhoneticMiddle = constructListFromValue.get(2);
                    case 2:
                        this.mNameData.mPhoneticGiven = constructListFromValue.get(1);
                        break;
                }
                this.mNameData.mPhoneticFamily = constructListFromValue.get(0);
            }
        }
    }
    
    public void addChild(final VCardEntry vCardEntry) {
        if (this.mChildren == null) {
            this.mChildren = new ArrayList<VCardEntry>();
        }
        this.mChildren.add(vCardEntry);
    }
    
    public void addProperty(final VCardProperty vCardProperty) {
        final String name = vCardProperty.getName();
        final Map<String, Collection<String>> parameterMap = vCardProperty.getParameterMap();
        final List<String> valueList = vCardProperty.getValueList();
        final byte[] byteValue = vCardProperty.getByteValue();
        if ((valueList != null && valueList.size() != 0) || byteValue != null) {
            String trim;
            if (valueList != null) {
                trim = this.listToString(valueList).trim();
            }
            else {
                trim = null;
            }
            if (!name.equals("VERSION")) {
                if (name.equals("FN")) {
                    this.mNameData.mFormatted = trim;
                }
                else if (name.equals("NAME")) {
                    if (TextUtils.isEmpty((CharSequence)this.mNameData.mFormatted)) {
                        this.mNameData.mFormatted = trim;
                    }
                }
                else if (name.equals("N")) {
                    this.handleNProperty(valueList, parameterMap);
                }
                else if (name.equals("SORT-STRING")) {
                    this.mNameData.mSortString = trim;
                }
                else if (name.equals("NICKNAME") || name.equals("X-NICKNAME")) {
                    this.addNickName(trim);
                }
                else if (name.equals("SOUND")) {
                    final Collection<String> collection = parameterMap.get("TYPE");
                    if (collection != null && collection.contains("X-IRMC-N")) {
                        this.handlePhoneticNameFromSound(VCardUtils.constructListFromValue(trim, this.mVCardType));
                    }
                }
                else {
                    if (name.equals("ADR")) {
                        final boolean b = true;
                        final Iterator<String> iterator = valueList.iterator();
                        while (true) {
                            do {
                                final boolean b2 = b;
                                if (iterator.hasNext()) {
                                    continue;
                                }
                                if (!b2) {
                                    int n = -1;
                                    String s = null;
                                    String substring = null;
                                    boolean b3 = false;
                                    boolean b4 = false;
                                    final Collection<String> collection2 = parameterMap.get("TYPE");
                                    int n2 = n;
                                    if (collection2 != null) {
                                        final Iterator<String> iterator2 = collection2.iterator();
                                        while (true) {
                                            n2 = n;
                                            b3 = b4;
                                            s = substring;
                                            if (!iterator2.hasNext()) {
                                                break;
                                            }
                                            final String s2 = iterator2.next();
                                            final String upperCase = s2.toUpperCase();
                                            if (upperCase.equals("PREF")) {
                                                b4 = true;
                                            }
                                            else if (upperCase.equals("HOME")) {
                                                n = 1;
                                                substring = null;
                                            }
                                            else if (upperCase.equals("WORK") || upperCase.equalsIgnoreCase("COMPANY")) {
                                                n = 2;
                                                substring = null;
                                            }
                                            else {
                                                if (upperCase.equals("PARCEL") || upperCase.equals("DOM") || upperCase.equals("INTL") || n >= 0) {
                                                    continue;
                                                }
                                                n = 0;
                                                if (upperCase.startsWith("X-")) {
                                                    substring = s2.substring(2);
                                                }
                                                else {
                                                    substring = s2;
                                                }
                                            }
                                        }
                                    }
                                    int n3;
                                    if ((n3 = n2) < 0) {
                                        n3 = 1;
                                    }
                                    this.addPostal(n3, valueList, s, b3);
                                }
                                return;
                            } while (TextUtils.isEmpty((CharSequence)iterator.next()));
                            final boolean b2 = false;
                            continue;
                        }
                    }
                    if (name.equals("EMAIL")) {
                        int n4 = -1;
                        String s3 = null;
                        String substring2 = null;
                        boolean b5 = false;
                        boolean b6 = false;
                        final Collection<String> collection3 = parameterMap.get("TYPE");
                        int n5 = n4;
                        if (collection3 != null) {
                            final Iterator<String> iterator3 = collection3.iterator();
                            while (true) {
                                n5 = n4;
                                b5 = b6;
                                s3 = substring2;
                                if (!iterator3.hasNext()) {
                                    break;
                                }
                                final String s4 = iterator3.next();
                                final String upperCase2 = s4.toUpperCase();
                                if (upperCase2.equals("PREF")) {
                                    b6 = true;
                                }
                                else if (upperCase2.equals("HOME")) {
                                    n4 = 1;
                                }
                                else if (upperCase2.equals("WORK")) {
                                    n4 = 2;
                                }
                                else if (upperCase2.equals("CELL")) {
                                    n4 = 4;
                                }
                                else {
                                    if (n4 >= 0) {
                                        continue;
                                    }
                                    if (upperCase2.startsWith("X-")) {
                                        substring2 = s4.substring(2);
                                    }
                                    else {
                                        substring2 = s4;
                                    }
                                    n4 = 0;
                                }
                            }
                        }
                        int n6;
                        if ((n6 = n5) < 0) {
                            n6 = 3;
                        }
                        this.addEmail(n6, trim, s3, b5);
                    }
                    else if (name.equals("ORG")) {
                        boolean b7 = false;
                        boolean b8 = false;
                        final Collection<String> collection4 = parameterMap.get("TYPE");
                        if (collection4 != null) {
                            final Iterator<String> iterator4 = collection4.iterator();
                            while (true) {
                                b7 = b8;
                                if (!iterator4.hasNext()) {
                                    break;
                                }
                                if (!iterator4.next().equals("PREF")) {
                                    continue;
                                }
                                b8 = true;
                            }
                        }
                        this.handleOrgValue(1, valueList, parameterMap, b7);
                    }
                    else if (name.equals("TITLE")) {
                        this.handleTitleValue(trim);
                    }
                    else if (!name.equals("ROLE")) {
                        if (name.equals("PHOTO") || name.equals("LOGO")) {
                            final Collection<String> collection5 = parameterMap.get("VALUE");
                            if (collection5 == null || !collection5.contains("URL")) {
                                final Collection<String> collection6 = parameterMap.get("TYPE");
                                String s5 = null;
                                String s6 = null;
                                boolean b9 = false;
                                boolean b10 = false;
                                if (collection6 != null) {
                                    final Iterator<String> iterator5 = collection6.iterator();
                                    while (true) {
                                        b9 = b10;
                                        s5 = s6;
                                        if (!iterator5.hasNext()) {
                                            break;
                                        }
                                        final String s7 = iterator5.next();
                                        if ("PREF".equals(s7)) {
                                            b10 = true;
                                        }
                                        else {
                                            if (s6 != null) {
                                                continue;
                                            }
                                            s6 = s7;
                                        }
                                    }
                                }
                                this.addPhotoBytes(s5, byteValue, b9);
                            }
                        }
                        else if (name.equals("TEL")) {
                            String substring3 = null;
                            boolean b11 = false;
                            if (VCardConfig.isVersion40(this.mVCardType)) {
                                if (trim.startsWith("sip:")) {
                                    b11 = true;
                                }
                                else if (trim.startsWith("tel:")) {
                                    substring3 = trim.substring(4);
                                }
                                else {
                                    substring3 = trim;
                                }
                            }
                            else {
                                substring3 = trim;
                            }
                            if (b11) {
                                this.handleSipCase(trim, parameterMap.get("TYPE"));
                            }
                            else if (trim.length() != 0) {
                                final Collection<String> collection7 = parameterMap.get("TYPE");
                                final Object phoneTypeFromStrings = VCardUtils.getPhoneTypeFromStrings(collection7, substring3);
                                int intValue;
                                String string;
                                if (phoneTypeFromStrings instanceof Integer) {
                                    intValue = (int)phoneTypeFromStrings;
                                    string = null;
                                }
                                else {
                                    intValue = 0;
                                    string = phoneTypeFromStrings.toString();
                                }
                                this.addPhone(intValue, substring3, string, collection7 != null && collection7.contains("PREF"));
                            }
                        }
                        else if (name.equals("X-IRMC-CALL-DATETIME")) {
                            if (!TextUtils.isEmpty((CharSequence)trim)) {
                                final Collection<String> collection8 = parameterMap.get("TYPE");
                                final int n7 = 0;
                                int n8 = 0;
                                Label_1314: {
                                    if (collection8 != null) {
                                        n8 = n7;
                                        if (collection8.size() <= 0) {
                                            break Label_1314;
                                        }
                                    }
                                    final String next = collection8.iterator().next();
                                    n8 = n7;
                                    if (next instanceof String) {
                                        final String s8 = next;
                                        if ("DIALED".equalsIgnoreCase(s8)) {
                                            n8 = 11;
                                        }
                                        else if ("MISSED".equalsIgnoreCase(s8)) {
                                            n8 = 12;
                                        }
                                        else {
                                            n8 = n7;
                                            if ("RECEIVED".equalsIgnoreCase(s8)) {
                                                n8 = 10;
                                            }
                                        }
                                    }
                                }
                                final Date irmcDateStr = DateUtil.parseIrmcDateStr(trim);
                                if (irmcDateStr != null) {
                                    this.addCallTime(n8, irmcDateStr);
                                }
                            }
                        }
                        else if (name.equals("X-SKYPE-PSTNNUMBER")) {
                            final Collection<String> collection9 = parameterMap.get("TYPE");
                            this.addPhone(7, trim, null, collection9 != null && collection9.contains("PREF"));
                        }
                        else if (VCardEntry.sImMap.containsKey(name)) {
                            final int intValue2 = VCardEntry.sImMap.get(name);
                            boolean b12 = false;
                            boolean b13 = false;
                            int n9 = -1;
                            final Collection<String> collection10 = parameterMap.get("TYPE");
                            int n10 = n9;
                            if (collection10 != null) {
                                final Iterator<String> iterator6 = collection10.iterator();
                                while (true) {
                                    n10 = n9;
                                    b12 = b13;
                                    if (!iterator6.hasNext()) {
                                        break;
                                    }
                                    final String s9 = iterator6.next();
                                    if (s9.equals("PREF")) {
                                        b13 = true;
                                    }
                                    else {
                                        if (n9 >= 0) {
                                            continue;
                                        }
                                        if (s9.equalsIgnoreCase("HOME")) {
                                            n9 = 1;
                                        }
                                        else {
                                            if (!s9.equalsIgnoreCase("WORK")) {
                                                continue;
                                            }
                                            n9 = 2;
                                        }
                                    }
                                }
                            }
                            int n11;
                            if ((n11 = n10) < 0) {
                                n11 = 1;
                            }
                            this.addIm(intValue2, null, trim, n11, b12);
                        }
                        else if (name.equals("NOTE")) {
                            this.addNote(trim);
                        }
                        else if (name.equals("URL")) {
                            if (this.mWebsiteList == null) {
                                this.mWebsiteList = new ArrayList<WebsiteData>(1);
                            }
                            this.mWebsiteList.add(new WebsiteData(trim));
                        }
                        else if (name.equals("BDAY")) {
                            this.mBirthday = new BirthdayData(trim);
                        }
                        else if (name.equals("ANNIVERSARY")) {
                            this.mAnniversary = new AnniversaryData(trim);
                        }
                        else if (name.equals("X-PHONETIC-FIRST-NAME")) {
                            this.mNameData.mPhoneticGiven = trim;
                        }
                        else if (name.equals("X-PHONETIC-MIDDLE-NAME")) {
                            this.mNameData.mPhoneticMiddle = trim;
                        }
                        else if (name.equals("X-PHONETIC-LAST-NAME")) {
                            this.mNameData.mPhoneticFamily = trim;
                        }
                        else if (name.equals("IMPP")) {
                            if (trim.startsWith("sip:")) {
                                this.handleSipCase(trim, parameterMap.get("TYPE"));
                            }
                        }
                        else if (name.equals("X-SIP")) {
                            if (!TextUtils.isEmpty((CharSequence)trim)) {
                                this.handleSipCase(trim, parameterMap.get("TYPE"));
                            }
                        }
                        else if (name.equals("X-ANDROID-CUSTOM")) {
                            this.handleAndroidCustomProperty(VCardUtils.constructListFromValue(trim, this.mVCardType));
                        }
                    }
                }
            }
        }
    }
    
    public void consolidateFields() {
        this.mNameData.displayName = this.constructDisplayName();
    }
    
    public ArrayList<ContentProviderOperation> constructInsertOperations(final ContentResolver contentResolver, final ArrayList<ContentProviderOperation> list) {
        ArrayList<ContentProviderOperation> list2 = list;
        if (list == null) {
            list2 = new ArrayList<ContentProviderOperation>();
        }
        if (!this.isIgnorable()) {
            final int size = list2.size();
            final ContentProviderOperation$Builder insert = ContentProviderOperation.newInsert(ContactsContract$RawContacts.CONTENT_URI);
            if (this.mAccount != null) {
                insert.withValue("account_name", this.mAccount.name);
                insert.withValue("account_type", this.mAccount.type);
            }
            else {
                insert.withValue("account_name", null);
                insert.withValue("account_type", null);
            }
            list2.add(insert.build());
            list2.size();
            this.iterateAllData((EntryElementIterator)new InsertOperationConstrutor(list2, size));
            list2.size();
        }
        return list2;
    }
    
    public final String getBirthday() {
        String access$1800;
        if (this.mBirthday != null) {
            access$1800 = this.mBirthday.mBirthday;
        }
        else {
            access$1800 = null;
        }
        return access$1800;
    }
    
    public Date getCallTime() {
        return this.mCallTime;
    }
    
    public int getCallType() {
        return this.mCallType;
    }
    
    public final List<VCardEntry> getChildlen() {
        return this.mChildren;
    }
    
    public String getDisplayName() {
        if (this.mNameData.displayName == null) {
            this.mNameData.displayName = this.constructDisplayName();
        }
        return this.mNameData.displayName;
    }
    
    public final List<EmailData> getEmailList() {
        return this.mEmailList;
    }
    
    public final List<ImData> getImList() {
        return this.mImList;
    }
    
    public final NameData getNameData() {
        return this.mNameData;
    }
    
    public final List<NicknameData> getNickNameList() {
        return this.mNicknameList;
    }
    
    public final List<NoteData> getNotes() {
        return this.mNoteList;
    }
    
    public final List<OrganizationData> getOrganizationList() {
        return this.mOrganizationList;
    }
    
    public final List<PhoneData> getPhoneList() {
        return this.mPhoneList;
    }
    
    public final List<PhotoData> getPhotoList() {
        return this.mPhotoList;
    }
    
    public final List<PostalData> getPostalList() {
        return this.mPostalList;
    }
    
    public final List<WebsiteData> getWebsiteList() {
        return this.mWebsiteList;
    }
    
    public boolean isIgnorable() {
        final IsIgnorableIterator isIgnorableIterator = new IsIgnorableIterator();
        this.iterateAllData((EntryElementIterator)isIgnorableIterator);
        return isIgnorableIterator.getResult();
    }
    
    public final void iterateAllData(final EntryElementIterator entryElementIterator) {
        entryElementIterator.onIterationStarted();
        entryElementIterator.onElementGroupStarted(this.mNameData.getEntryLabel());
        entryElementIterator.onElement(this.mNameData);
        entryElementIterator.onElementGroupEnded();
        this.iterateOneList((List<? extends EntryElement>)this.mPhoneList, entryElementIterator);
        this.iterateOneList((List<? extends EntryElement>)this.mEmailList, entryElementIterator);
        this.iterateOneList((List<? extends EntryElement>)this.mPostalList, entryElementIterator);
        this.iterateOneList((List<? extends EntryElement>)this.mOrganizationList, entryElementIterator);
        this.iterateOneList((List<? extends EntryElement>)this.mImList, entryElementIterator);
        this.iterateOneList((List<? extends EntryElement>)this.mPhotoList, entryElementIterator);
        this.iterateOneList((List<? extends EntryElement>)this.mWebsiteList, entryElementIterator);
        this.iterateOneList((List<? extends EntryElement>)this.mSipList, entryElementIterator);
        this.iterateOneList((List<? extends EntryElement>)this.mNicknameList, entryElementIterator);
        this.iterateOneList((List<? extends EntryElement>)this.mNoteList, entryElementIterator);
        this.iterateOneList((List<? extends EntryElement>)this.mAndroidCustomDataList, entryElementIterator);
        if (this.mBirthday != null) {
            entryElementIterator.onElementGroupStarted(this.mBirthday.getEntryLabel());
            entryElementIterator.onElement(this.mBirthday);
            entryElementIterator.onElementGroupEnded();
        }
        if (this.mAnniversary != null) {
            entryElementIterator.onElementGroupStarted(this.mAnniversary.getEntryLabel());
            entryElementIterator.onElement(this.mAnniversary);
            entryElementIterator.onElementGroupEnded();
        }
        entryElementIterator.onIterationEnded();
    }
    
    @Override
    public String toString() {
        final ToStringIterator toStringIterator = new ToStringIterator();
        this.iterateAllData((EntryElementIterator)toStringIterator);
        return toStringIterator.toString();
    }
    
    public static class AndroidCustomData implements EntryElement
    {
        private final List<String> mDataList;
        private final String mMimeType;
        
        public AndroidCustomData(final String mMimeType, final List<String> mDataList) {
            this.mMimeType = mMimeType;
            this.mDataList = mDataList;
        }
        
        public static AndroidCustomData constructAndroidCustomData(final List<String> list) {
            int size = 16;
            String s;
            List<String> subList;
            if (list == null) {
                s = null;
                subList = null;
            }
            else if (list.size() < 2) {
                s = list.get(0);
                subList = null;
            }
            else {
                if (list.size() < 16) {
                    size = list.size();
                }
                s = list.get(0);
                subList = list.subList(1, size);
            }
            return new AndroidCustomData(s, subList);
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, int i) {
            final ContentProviderOperation$Builder insert = ContentProviderOperation.newInsert(ContactsContract$Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", i);
            insert.withValue("mimetype", this.mMimeType);
            String s;
            for (i = 0; i < this.mDataList.size(); ++i) {
                s = this.mDataList.get(i);
                if (!TextUtils.isEmpty((CharSequence)s)) {
                    insert.withValue("data" + (i + 1), s);
                }
            }
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = false;
            boolean b2;
            if (this == o) {
                b2 = true;
            }
            else {
                b2 = b;
                if (o instanceof AndroidCustomData) {
                    final AndroidCustomData androidCustomData = (AndroidCustomData)o;
                    b2 = b;
                    if (TextUtils.equals((CharSequence)this.mMimeType, (CharSequence)androidCustomData.mMimeType)) {
                        if (this.mDataList == null) {
                            b2 = (androidCustomData.mDataList == null);
                        }
                        else {
                            final int size = this.mDataList.size();
                            b2 = b;
                            if (size == androidCustomData.mDataList.size()) {
                                for (int i = 0; i < size; ++i) {
                                    b2 = b;
                                    if (!TextUtils.equals((CharSequence)this.mDataList.get(i), (CharSequence)androidCustomData.mDataList.get(i))) {
                                        return b2;
                                    }
                                }
                                b2 = true;
                            }
                        }
                    }
                }
            }
            return b2;
        }
        
        public List<String> getDataList() {
            return this.mDataList;
        }
        
        @Override
        public EntryLabel getEntryLabel() {
            return EntryLabel.ANDROID_CUSTOM;
        }
        
        public String getMimeType() {
            return this.mMimeType;
        }
        
        @Override
        public int hashCode() {
            int hashCode;
            if (this.mMimeType != null) {
                hashCode = this.mMimeType.hashCode();
            }
            else {
                hashCode = 0;
            }
            int n = hashCode;
            if (this.mDataList != null) {
                final Iterator<String> iterator = this.mDataList.iterator();
                while (true) {
                    n = hashCode;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    final String s = iterator.next();
                    int hashCode2;
                    if (s != null) {
                        hashCode2 = s.hashCode();
                    }
                    else {
                        hashCode2 = 0;
                    }
                    hashCode = hashCode * 31 + hashCode2;
                }
            }
            return n;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mMimeType) || this.mDataList == null || this.mDataList.size() == 0;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("android-custom: " + this.mMimeType + ", data: ");
            String string;
            if (this.mDataList == null) {
                string = "null";
            }
            else {
                string = Arrays.toString(this.mDataList.toArray());
            }
            sb.append(string);
            return sb.toString();
        }
    }
    
    public static class AnniversaryData implements EntryElement
    {
        private final String mAnniversary;
        
        public AnniversaryData(final String mAnniversary) {
            this.mAnniversary = mAnniversary;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation$Builder insert = ContentProviderOperation.newInsert(ContactsContract$Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/contact_event");
            insert.withValue("data1", this.mAnniversary);
            insert.withValue("data2", 1);
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof AnniversaryData && TextUtils.equals((CharSequence)this.mAnniversary, (CharSequence)((AnniversaryData)o).mAnniversary));
        }
        
        public String getAnniversary() {
            return this.mAnniversary;
        }
        
        @Override
        public EntryLabel getEntryLabel() {
            return EntryLabel.ANNIVERSARY;
        }
        
        @Override
        public int hashCode() {
            int hashCode;
            if (this.mAnniversary != null) {
                hashCode = this.mAnniversary.hashCode();
            }
            else {
                hashCode = 0;
            }
            return hashCode;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mAnniversary);
        }
        
        @Override
        public String toString() {
            return "anniversary: " + this.mAnniversary;
        }
    }
    
    public static class BirthdayData implements EntryElement
    {
        private final String mBirthday;
        
        public BirthdayData(final String mBirthday) {
            this.mBirthday = mBirthday;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation$Builder insert = ContentProviderOperation.newInsert(ContactsContract$Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/contact_event");
            insert.withValue("data1", this.mBirthday);
            insert.withValue("data2", 3);
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof BirthdayData && TextUtils.equals((CharSequence)this.mBirthday, (CharSequence)((BirthdayData)o).mBirthday));
        }
        
        public String getBirthday() {
            return this.mBirthday;
        }
        
        @Override
        public EntryLabel getEntryLabel() {
            return EntryLabel.BIRTHDAY;
        }
        
        @Override
        public int hashCode() {
            int hashCode;
            if (this.mBirthday != null) {
                hashCode = this.mBirthday.hashCode();
            }
            else {
                hashCode = 0;
            }
            return hashCode;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mBirthday);
        }
        
        @Override
        public String toString() {
            return "birthday: " + this.mBirthday;
        }
    }
    
    public static class EmailData implements EntryElement
    {
        private final String mAddress;
        private final boolean mIsPrimary;
        private final String mLabel;
        private final int mType;
        
        public EmailData(final String mAddress, final int mType, final String mLabel, final boolean mIsPrimary) {
            this.mType = mType;
            this.mAddress = mAddress;
            this.mLabel = mLabel;
            this.mIsPrimary = mIsPrimary;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation$Builder insert = ContentProviderOperation.newInsert(ContactsContract$Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/email_v2");
            insert.withValue("data2", this.mType);
            if (this.mType == 0) {
                insert.withValue("data3", this.mLabel);
            }
            insert.withValue("data1", this.mAddress);
            if (this.mIsPrimary) {
                insert.withValue("is_primary", 1);
            }
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this != o) {
                if (!(o instanceof EmailData)) {
                    b = false;
                }
                else {
                    final EmailData emailData = (EmailData)o;
                    if (this.mType != emailData.mType || !TextUtils.equals((CharSequence)this.mAddress, (CharSequence)emailData.mAddress) || !TextUtils.equals((CharSequence)this.mLabel, (CharSequence)emailData.mLabel) || this.mIsPrimary != emailData.mIsPrimary) {
                        b = false;
                    }
                }
            }
            return b;
        }
        
        public String getAddress() {
            return this.mAddress;
        }
        
        @Override
        public final EntryLabel getEntryLabel() {
            return EntryLabel.EMAIL;
        }
        
        public String getLabel() {
            return this.mLabel;
        }
        
        public int getType() {
            return this.mType;
        }
        
        @Override
        public int hashCode() {
            int hashCode = 0;
            final int mType = this.mType;
            int hashCode2;
            if (this.mAddress != null) {
                hashCode2 = this.mAddress.hashCode();
            }
            else {
                hashCode2 = 0;
            }
            if (this.mLabel != null) {
                hashCode = this.mLabel.hashCode();
            }
            int n;
            if (this.mIsPrimary) {
                n = 1231;
            }
            else {
                n = 1237;
            }
            return ((mType * 31 + hashCode2) * 31 + hashCode) * 31 + n;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mAddress);
        }
        
        public boolean isPrimary() {
            return this.mIsPrimary;
        }
        
        @Override
        public String toString() {
            return String.format("type: %d, data: %s, label: %s, isPrimary: %s", this.mType, this.mAddress, this.mLabel, this.mIsPrimary);
        }
    }
    
    public interface EntryElement
    {
        void constructInsertOperation(final List<ContentProviderOperation> p0, final int p1);
        
        EntryLabel getEntryLabel();
        
        boolean isEmpty();
    }
    
    public interface EntryElementIterator
    {
        boolean onElement(final EntryElement p0);
        
        void onElementGroupEnded();
        
        void onElementGroupStarted(final EntryLabel p0);
        
        void onIterationEnded();
        
        void onIterationStarted();
    }
    
    public enum EntryLabel
    {
        ANDROID_CUSTOM, 
        ANNIVERSARY, 
        BIRTHDAY, 
        EMAIL, 
        IM, 
        NAME, 
        NICKNAME, 
        NOTE, 
        ORGANIZATION, 
        PHONE, 
        PHOTO, 
        POSTAL_ADDRESS, 
        SIP, 
        WEBSITE;
    }
    
    public static class ImData implements EntryElement
    {
        private final String mAddress;
        private final String mCustomProtocol;
        private final boolean mIsPrimary;
        private final int mProtocol;
        private final int mType;
        
        public ImData(final int mProtocol, final String mCustomProtocol, final String mAddress, final int mType, final boolean mIsPrimary) {
            this.mProtocol = mProtocol;
            this.mCustomProtocol = mCustomProtocol;
            this.mType = mType;
            this.mAddress = mAddress;
            this.mIsPrimary = mIsPrimary;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation$Builder insert = ContentProviderOperation.newInsert(ContactsContract$Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/im");
            insert.withValue("data2", this.mType);
            insert.withValue("data5", this.mProtocol);
            insert.withValue("data1", this.mAddress);
            if (this.mProtocol == -1) {
                insert.withValue("data6", this.mCustomProtocol);
            }
            if (this.mIsPrimary) {
                insert.withValue("is_primary", 1);
            }
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this != o) {
                if (!(o instanceof ImData)) {
                    b = false;
                }
                else {
                    final ImData imData = (ImData)o;
                    if (this.mType != imData.mType || this.mProtocol != imData.mProtocol || !TextUtils.equals((CharSequence)this.mCustomProtocol, (CharSequence)imData.mCustomProtocol) || !TextUtils.equals((CharSequence)this.mAddress, (CharSequence)imData.mAddress) || this.mIsPrimary != imData.mIsPrimary) {
                        b = false;
                    }
                }
            }
            return b;
        }
        
        public String getAddress() {
            return this.mAddress;
        }
        
        public String getCustomProtocol() {
            return this.mCustomProtocol;
        }
        
        @Override
        public final EntryLabel getEntryLabel() {
            return EntryLabel.IM;
        }
        
        public int getProtocol() {
            return this.mProtocol;
        }
        
        public int getType() {
            return this.mType;
        }
        
        @Override
        public int hashCode() {
            int hashCode = 0;
            final int mType = this.mType;
            final int mProtocol = this.mProtocol;
            int hashCode2;
            if (this.mCustomProtocol != null) {
                hashCode2 = this.mCustomProtocol.hashCode();
            }
            else {
                hashCode2 = 0;
            }
            if (this.mAddress != null) {
                hashCode = this.mAddress.hashCode();
            }
            int n;
            if (this.mIsPrimary) {
                n = 1231;
            }
            else {
                n = 1237;
            }
            return (((mType * 31 + mProtocol) * 31 + hashCode2) * 31 + hashCode) * 31 + n;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mAddress);
        }
        
        public boolean isPrimary() {
            return this.mIsPrimary;
        }
        
        @Override
        public String toString() {
            return String.format("type: %d, protocol: %d, custom_protcol: %s, data: %s, isPrimary: %s", this.mType, this.mProtocol, this.mCustomProtocol, this.mAddress, this.mIsPrimary);
        }
    }
    
    private class InsertOperationConstrutor implements EntryElementIterator
    {
        private final int mBackReferenceIndex;
        private final List<ContentProviderOperation> mOperationList;
        
        public InsertOperationConstrutor(final List<ContentProviderOperation> mOperationList, final int mBackReferenceIndex) {
            this.mOperationList = mOperationList;
            this.mBackReferenceIndex = mBackReferenceIndex;
        }
        
        @Override
        public boolean onElement(final EntryElement entryElement) {
            if (!entryElement.isEmpty()) {
                entryElement.constructInsertOperation(this.mOperationList, this.mBackReferenceIndex);
            }
            return true;
        }
        
        @Override
        public void onElementGroupEnded() {
        }
        
        @Override
        public void onElementGroupStarted(final EntryLabel entryLabel) {
        }
        
        @Override
        public void onIterationEnded() {
        }
        
        @Override
        public void onIterationStarted() {
        }
    }
    
    private class IsIgnorableIterator implements EntryElementIterator
    {
        private boolean mEmpty;
        
        private IsIgnorableIterator() {
            this.mEmpty = true;
        }
        
        public boolean getResult() {
            return this.mEmpty;
        }
        
        @Override
        public boolean onElement(final EntryElement entryElement) {
            boolean b = false;
            if (!entryElement.isEmpty()) {
                this.mEmpty = false;
            }
            else {
                b = true;
            }
            return b;
        }
        
        @Override
        public void onElementGroupEnded() {
        }
        
        @Override
        public void onElementGroupStarted(final EntryLabel entryLabel) {
        }
        
        @Override
        public void onIterationEnded() {
        }
        
        @Override
        public void onIterationStarted() {
        }
    }
    
    public static class NameData implements EntryElement
    {
        public String displayName;
        private String mFamily;
        private String mFormatted;
        private String mGiven;
        private String mMiddle;
        private String mPhoneticFamily;
        private String mPhoneticGiven;
        private String mPhoneticMiddle;
        private String mPrefix;
        private String mSortString;
        private String mSuffix;
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, int n) {
            final ContentProviderOperation$Builder insert = ContentProviderOperation.newInsert(ContactsContract$Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/name");
            if (!TextUtils.isEmpty((CharSequence)this.mGiven)) {
                insert.withValue("data2", this.mGiven);
            }
            if (!TextUtils.isEmpty((CharSequence)this.mFamily)) {
                insert.withValue("data3", this.mFamily);
            }
            if (!TextUtils.isEmpty((CharSequence)this.mMiddle)) {
                insert.withValue("data5", this.mMiddle);
            }
            if (!TextUtils.isEmpty((CharSequence)this.mPrefix)) {
                insert.withValue("data4", this.mPrefix);
            }
            if (!TextUtils.isEmpty((CharSequence)this.mSuffix)) {
                insert.withValue("data6", this.mSuffix);
            }
            n = 0;
            if (!TextUtils.isEmpty((CharSequence)this.mPhoneticGiven)) {
                insert.withValue("data7", this.mPhoneticGiven);
                n = 1;
            }
            if (!TextUtils.isEmpty((CharSequence)this.mPhoneticFamily)) {
                insert.withValue("data9", this.mPhoneticFamily);
                n = 1;
            }
            if (!TextUtils.isEmpty((CharSequence)this.mPhoneticMiddle)) {
                insert.withValue("data8", this.mPhoneticMiddle);
                n = 1;
            }
            if (n == 0) {
                insert.withValue("data7", this.mSortString);
            }
            insert.withValue("data1", this.displayName);
            list.add(insert.build());
        }
        
        public boolean emptyPhoneticStructuredName() {
            return TextUtils.isEmpty((CharSequence)this.mPhoneticFamily) && TextUtils.isEmpty((CharSequence)this.mPhoneticGiven) && TextUtils.isEmpty((CharSequence)this.mPhoneticMiddle);
        }
        
        public boolean emptyStructuredName() {
            return TextUtils.isEmpty((CharSequence)this.mFamily) && TextUtils.isEmpty((CharSequence)this.mGiven) && TextUtils.isEmpty((CharSequence)this.mMiddle) && TextUtils.isEmpty((CharSequence)this.mPrefix) && TextUtils.isEmpty((CharSequence)this.mSuffix);
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this != o) {
                if (!(o instanceof NameData)) {
                    b = false;
                }
                else {
                    final NameData nameData = (NameData)o;
                    if (!TextUtils.equals((CharSequence)this.mFamily, (CharSequence)nameData.mFamily) || !TextUtils.equals((CharSequence)this.mMiddle, (CharSequence)nameData.mMiddle) || !TextUtils.equals((CharSequence)this.mGiven, (CharSequence)nameData.mGiven) || !TextUtils.equals((CharSequence)this.mPrefix, (CharSequence)nameData.mPrefix) || !TextUtils.equals((CharSequence)this.mSuffix, (CharSequence)nameData.mSuffix) || !TextUtils.equals((CharSequence)this.mFormatted, (CharSequence)nameData.mFormatted) || !TextUtils.equals((CharSequence)this.mPhoneticFamily, (CharSequence)nameData.mPhoneticFamily) || !TextUtils.equals((CharSequence)this.mPhoneticMiddle, (CharSequence)nameData.mPhoneticMiddle) || !TextUtils.equals((CharSequence)this.mPhoneticGiven, (CharSequence)nameData.mPhoneticGiven) || !TextUtils.equals((CharSequence)this.mSortString, (CharSequence)nameData.mSortString)) {
                        b = false;
                    }
                }
            }
            return b;
        }
        
        @Override
        public final EntryLabel getEntryLabel() {
            return EntryLabel.NAME;
        }
        
        public String getFamily() {
            return this.mFamily;
        }
        
        public String getFormatted() {
            return this.mFormatted;
        }
        
        public String getGiven() {
            return this.mGiven;
        }
        
        public String getMiddle() {
            return this.mMiddle;
        }
        
        public String getPrefix() {
            return this.mPrefix;
        }
        
        public String getSortString() {
            return this.mSortString;
        }
        
        public String getSuffix() {
            return this.mSuffix;
        }
        
        @Override
        public int hashCode() {
            final String[] array = { this.mFamily, this.mMiddle, this.mGiven, this.mPrefix, this.mSuffix, this.mFormatted, this.mPhoneticFamily, this.mPhoneticMiddle, this.mPhoneticGiven, this.mSortString };
            int n = 0;
            for (final String s : array) {
                int hashCode;
                if (s != null) {
                    hashCode = s.hashCode();
                }
                else {
                    hashCode = 0;
                }
                n = n * 31 + hashCode;
            }
            return n;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mFamily) && TextUtils.isEmpty((CharSequence)this.mMiddle) && TextUtils.isEmpty((CharSequence)this.mGiven) && TextUtils.isEmpty((CharSequence)this.mPrefix) && TextUtils.isEmpty((CharSequence)this.mSuffix) && TextUtils.isEmpty((CharSequence)this.mFormatted) && TextUtils.isEmpty((CharSequence)this.mPhoneticFamily) && TextUtils.isEmpty((CharSequence)this.mPhoneticMiddle) && TextUtils.isEmpty((CharSequence)this.mPhoneticGiven) && TextUtils.isEmpty((CharSequence)this.mSortString);
        }
        
        public void setFamily(final String mFamily) {
            this.mFamily = mFamily;
        }
        
        public void setGiven(final String mGiven) {
            this.mGiven = mGiven;
        }
        
        public void setMiddle(final String mMiddle) {
            this.mMiddle = mMiddle;
        }
        
        public void setPrefix(final String mPrefix) {
            this.mPrefix = mPrefix;
        }
        
        public void setSuffix(final String mSuffix) {
            this.mSuffix = mSuffix;
        }
        
        @Override
        public String toString() {
            return String.format("family: %s, given: %s, middle: %s, prefix: %s, suffix: %s", this.mFamily, this.mGiven, this.mMiddle, this.mPrefix, this.mSuffix);
        }
    }
    
    public static class NicknameData implements EntryElement
    {
        private final String mNickname;
        
        public NicknameData(final String mNickname) {
            this.mNickname = mNickname;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation$Builder insert = ContentProviderOperation.newInsert(ContactsContract$Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/nickname");
            insert.withValue("data2", 1);
            insert.withValue("data1", this.mNickname);
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof NicknameData && TextUtils.equals((CharSequence)this.mNickname, (CharSequence)((NicknameData)o).mNickname);
        }
        
        @Override
        public EntryLabel getEntryLabel() {
            return EntryLabel.NICKNAME;
        }
        
        public String getNickname() {
            return this.mNickname;
        }
        
        @Override
        public int hashCode() {
            int hashCode;
            if (this.mNickname != null) {
                hashCode = this.mNickname.hashCode();
            }
            else {
                hashCode = 0;
            }
            return hashCode;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mNickname);
        }
        
        @Override
        public String toString() {
            return "nickname: " + this.mNickname;
        }
    }
    
    public static class NoteData implements EntryElement
    {
        public final String mNote;
        
        public NoteData(final String mNote) {
            this.mNote = mNote;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation$Builder insert = ContentProviderOperation.newInsert(ContactsContract$Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/note");
            insert.withValue("data1", this.mNote);
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof NoteData && TextUtils.equals((CharSequence)this.mNote, (CharSequence)((NoteData)o).mNote));
        }
        
        @Override
        public EntryLabel getEntryLabel() {
            return EntryLabel.NOTE;
        }
        
        public String getNote() {
            return this.mNote;
        }
        
        @Override
        public int hashCode() {
            int hashCode;
            if (this.mNote != null) {
                hashCode = this.mNote.hashCode();
            }
            else {
                hashCode = 0;
            }
            return hashCode;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mNote);
        }
        
        @Override
        public String toString() {
            return "note: " + this.mNote;
        }
    }
    
    public static class OrganizationData implements EntryElement
    {
        private String mDepartmentName;
        private boolean mIsPrimary;
        private String mOrganizationName;
        private final String mPhoneticName;
        private String mTitle;
        private final int mType;
        
        public OrganizationData(final String mOrganizationName, final String mDepartmentName, final String mTitle, final String mPhoneticName, final int mType, final boolean mIsPrimary) {
            this.mType = mType;
            this.mOrganizationName = mOrganizationName;
            this.mDepartmentName = mDepartmentName;
            this.mTitle = mTitle;
            this.mPhoneticName = mPhoneticName;
            this.mIsPrimary = mIsPrimary;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation$Builder insert = ContentProviderOperation.newInsert(ContactsContract$Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/organization");
            insert.withValue("data2", this.mType);
            if (this.mOrganizationName != null) {
                insert.withValue("data1", this.mOrganizationName);
            }
            if (this.mDepartmentName != null) {
                insert.withValue("data5", this.mDepartmentName);
            }
            if (this.mTitle != null) {
                insert.withValue("data4", this.mTitle);
            }
            if (this.mPhoneticName != null) {
                insert.withValue("data8", this.mPhoneticName);
            }
            if (this.mIsPrimary) {
                insert.withValue("is_primary", 1);
            }
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this != o) {
                if (!(o instanceof OrganizationData)) {
                    b = false;
                }
                else {
                    final OrganizationData organizationData = (OrganizationData)o;
                    if (this.mType != organizationData.mType || !TextUtils.equals((CharSequence)this.mOrganizationName, (CharSequence)organizationData.mOrganizationName) || !TextUtils.equals((CharSequence)this.mDepartmentName, (CharSequence)organizationData.mDepartmentName) || !TextUtils.equals((CharSequence)this.mTitle, (CharSequence)organizationData.mTitle) || this.mIsPrimary != organizationData.mIsPrimary) {
                        b = false;
                    }
                }
            }
            return b;
        }
        
        public String getDepartmentName() {
            return this.mDepartmentName;
        }
        
        @Override
        public final EntryLabel getEntryLabel() {
            return EntryLabel.ORGANIZATION;
        }
        
        public String getFormattedString() {
            final StringBuilder sb = new StringBuilder();
            if (!TextUtils.isEmpty((CharSequence)this.mOrganizationName)) {
                sb.append(this.mOrganizationName);
            }
            if (!TextUtils.isEmpty((CharSequence)this.mDepartmentName)) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(this.mDepartmentName);
            }
            if (!TextUtils.isEmpty((CharSequence)this.mTitle)) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(this.mTitle);
            }
            return sb.toString();
        }
        
        public String getOrganizationName() {
            return this.mOrganizationName;
        }
        
        public String getPhoneticName() {
            return this.mPhoneticName;
        }
        
        public String getTitle() {
            return this.mTitle;
        }
        
        public int getType() {
            return this.mType;
        }
        
        @Override
        public int hashCode() {
            int hashCode = 0;
            final int mType = this.mType;
            int hashCode2;
            if (this.mOrganizationName != null) {
                hashCode2 = this.mOrganizationName.hashCode();
            }
            else {
                hashCode2 = 0;
            }
            int hashCode3;
            if (this.mDepartmentName != null) {
                hashCode3 = this.mDepartmentName.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.mTitle != null) {
                hashCode = this.mTitle.hashCode();
            }
            int n;
            if (this.mIsPrimary) {
                n = 1231;
            }
            else {
                n = 1237;
            }
            return (((mType * 31 + hashCode2) * 31 + hashCode3) * 31 + hashCode) * 31 + n;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mOrganizationName) && TextUtils.isEmpty((CharSequence)this.mDepartmentName) && TextUtils.isEmpty((CharSequence)this.mTitle) && TextUtils.isEmpty((CharSequence)this.mPhoneticName);
        }
        
        public boolean isPrimary() {
            return this.mIsPrimary;
        }
        
        @Override
        public String toString() {
            return String.format("type: %d, organization: %s, department: %s, title: %s, isPrimary: %s", this.mType, this.mOrganizationName, this.mDepartmentName, this.mTitle, this.mIsPrimary);
        }
    }
    
    public static class PhoneData implements EntryElement
    {
        private boolean mIsPrimary;
        private final String mLabel;
        private final String mNumber;
        private final int mType;
        
        public PhoneData(final String mNumber, final int mType, final String mLabel, final boolean mIsPrimary) {
            this.mNumber = mNumber;
            this.mType = mType;
            this.mLabel = mLabel;
            this.mIsPrimary = mIsPrimary;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation$Builder insert = ContentProviderOperation.newInsert(ContactsContract$Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/phone_v2");
            insert.withValue("data2", this.mType);
            if (this.mType == 0) {
                insert.withValue("data3", this.mLabel);
            }
            insert.withValue("data1", this.mNumber);
            if (this.mIsPrimary) {
                insert.withValue("is_primary", 1);
            }
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this != o) {
                if (!(o instanceof PhoneData)) {
                    b = false;
                }
                else {
                    final PhoneData phoneData = (PhoneData)o;
                    if (this.mType != phoneData.mType || !TextUtils.equals((CharSequence)this.mNumber, (CharSequence)phoneData.mNumber) || !TextUtils.equals((CharSequence)this.mLabel, (CharSequence)phoneData.mLabel) || this.mIsPrimary != phoneData.mIsPrimary) {
                        b = false;
                    }
                }
            }
            return b;
        }
        
        @Override
        public final EntryLabel getEntryLabel() {
            return EntryLabel.PHONE;
        }
        
        public String getLabel() {
            return this.mLabel;
        }
        
        public String getNumber() {
            return this.mNumber;
        }
        
        public int getType() {
            return this.mType;
        }
        
        @Override
        public int hashCode() {
            int hashCode = 0;
            final int mType = this.mType;
            int hashCode2;
            if (this.mNumber != null) {
                hashCode2 = this.mNumber.hashCode();
            }
            else {
                hashCode2 = 0;
            }
            if (this.mLabel != null) {
                hashCode = this.mLabel.hashCode();
            }
            int n;
            if (this.mIsPrimary) {
                n = 1231;
            }
            else {
                n = 1237;
            }
            return ((mType * 31 + hashCode2) * 31 + hashCode) * 31 + n;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mNumber);
        }
        
        public boolean isPrimary() {
            return this.mIsPrimary;
        }
        
        @Override
        public String toString() {
            return String.format("type: %d, data: %s, label: %s, isPrimary: %s", this.mType, this.mNumber, this.mLabel, this.mIsPrimary);
        }
    }
    
    public static class PhotoData implements EntryElement
    {
        private final byte[] mBytes;
        private final String mFormat;
        private Integer mHashCode;
        private final boolean mIsPrimary;
        
        public PhotoData(final String mFormat, final byte[] mBytes, final boolean mIsPrimary) {
            this.mHashCode = null;
            this.mFormat = mFormat;
            this.mBytes = mBytes;
            this.mIsPrimary = mIsPrimary;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation$Builder insert = ContentProviderOperation.newInsert(ContactsContract$Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/photo");
            insert.withValue("data15", this.mBytes);
            if (this.mIsPrimary) {
                insert.withValue("is_primary", 1);
            }
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this != o) {
                if (!(o instanceof PhotoData)) {
                    b = false;
                }
                else {
                    final PhotoData photoData = (PhotoData)o;
                    if (!TextUtils.equals((CharSequence)this.mFormat, (CharSequence)photoData.mFormat) || !Arrays.equals(this.mBytes, photoData.mBytes) || this.mIsPrimary != photoData.mIsPrimary) {
                        b = false;
                    }
                }
            }
            return b;
        }
        
        public byte[] getBytes() {
            return this.mBytes;
        }
        
        @Override
        public final EntryLabel getEntryLabel() {
            return EntryLabel.PHOTO;
        }
        
        public String getFormat() {
            return this.mFormat;
        }
        
        @Override
        public int hashCode() {
            int n = 0;
            int intValue;
            if (this.mHashCode != null) {
                intValue = this.mHashCode;
            }
            else {
                int hashCode;
                if (this.mFormat != null) {
                    hashCode = this.mFormat.hashCode();
                }
                else {
                    hashCode = 0;
                }
                int n3;
                int n2 = n3 = hashCode * 31;
                if (this.mBytes != null) {
                    final byte[] mBytes = this.mBytes;
                    final int length = mBytes.length;
                    while (true) {
                        n3 = n2;
                        if (n >= length) {
                            break;
                        }
                        n2 += mBytes[n];
                        ++n;
                    }
                }
                int n4;
                if (this.mIsPrimary) {
                    n4 = 1231;
                }
                else {
                    n4 = 1237;
                }
                intValue = n3 * 31 + n4;
                this.mHashCode = intValue;
            }
            return intValue;
        }
        
        @Override
        public boolean isEmpty() {
            return this.mBytes == null || this.mBytes.length == 0;
        }
        
        public boolean isPrimary() {
            return this.mIsPrimary;
        }
        
        @Override
        public String toString() {
            return String.format("format: %s: size: %d, isPrimary: %s", this.mFormat, this.mBytes.length, this.mIsPrimary);
        }
    }
    
    public static class PostalData implements EntryElement
    {
        private static final int ADDR_MAX_DATA_SIZE = 7;
        private final String mCountry;
        private final String mExtendedAddress;
        private boolean mIsPrimary;
        private final String mLabel;
        private final String mLocalty;
        private final String mPobox;
        private final String mPostalCode;
        private final String mRegion;
        private final String mStreet;
        private final int mType;
        private int mVCardType;
        
        public PostalData(final String mPobox, final String mExtendedAddress, final String mStreet, final String mLocalty, final String mRegion, final String mPostalCode, final String mCountry, final int mType, final String mLabel, final boolean mIsPrimary, final int mvCardType) {
            this.mType = mType;
            this.mPobox = mPobox;
            this.mExtendedAddress = mExtendedAddress;
            this.mStreet = mStreet;
            this.mLocalty = mLocalty;
            this.mRegion = mRegion;
            this.mPostalCode = mPostalCode;
            this.mCountry = mCountry;
            this.mLabel = mLabel;
            this.mIsPrimary = mIsPrimary;
            this.mVCardType = mvCardType;
        }
        
        public static PostalData constructPostalData(final List<String> list, final int n, final String s, final boolean b, final int n2) {
            final String[] array = new String[7];
            int size;
            if ((size = list.size()) > 7) {
                size = 7;
            }
            int i = 0;
            final Iterator<String> iterator = list.iterator();
            while (true) {
                while (iterator.hasNext()) {
                    array[i] = iterator.next();
                    final int n3 = i + 1;
                    if ((i = n3) >= size) {
                        i = n3;
                        while (i < 7) {
                            array[i] = null;
                            ++i;
                        }
                        return new PostalData(array[0], array[1], array[2], array[3], array[4], array[5], array[6], n, s, b, n2);
                    }
                }
                continue;
            }
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation$Builder insert = ContentProviderOperation.newInsert(ContactsContract$Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/postal-address_v2");
            insert.withValue("data2", this.mType);
            if (this.mType == 0) {
                insert.withValue("data3", this.mLabel);
            }
            Object o;
            if (TextUtils.isEmpty((CharSequence)this.mStreet)) {
                if (TextUtils.isEmpty((CharSequence)this.mExtendedAddress)) {
                    o = null;
                }
                else {
                    o = this.mExtendedAddress;
                }
            }
            else if (TextUtils.isEmpty((CharSequence)this.mExtendedAddress)) {
                o = this.mStreet;
            }
            else {
                o = this.mStreet + " " + this.mExtendedAddress;
            }
            insert.withValue("data5", this.mPobox);
            insert.withValue("data4", o);
            insert.withValue("data7", this.mLocalty);
            insert.withValue("data8", this.mRegion);
            insert.withValue("data9", this.mPostalCode);
            insert.withValue("data10", this.mCountry);
            insert.withValue("data1", this.getFormattedAddress(this.mVCardType));
            if (this.mIsPrimary) {
                insert.withValue("is_primary", 1);
            }
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this != o) {
                if (!(o instanceof PostalData)) {
                    b = false;
                }
                else {
                    final PostalData postalData = (PostalData)o;
                    if (this.mType != postalData.mType || (this.mType == 0 && !TextUtils.equals((CharSequence)this.mLabel, (CharSequence)postalData.mLabel)) || this.mIsPrimary != postalData.mIsPrimary || !TextUtils.equals((CharSequence)this.mPobox, (CharSequence)postalData.mPobox) || !TextUtils.equals((CharSequence)this.mExtendedAddress, (CharSequence)postalData.mExtendedAddress) || !TextUtils.equals((CharSequence)this.mStreet, (CharSequence)postalData.mStreet) || !TextUtils.equals((CharSequence)this.mLocalty, (CharSequence)postalData.mLocalty) || !TextUtils.equals((CharSequence)this.mRegion, (CharSequence)postalData.mRegion) || !TextUtils.equals((CharSequence)this.mPostalCode, (CharSequence)postalData.mPostalCode) || !TextUtils.equals((CharSequence)this.mCountry, (CharSequence)postalData.mCountry)) {
                        b = false;
                    }
                }
            }
            return b;
        }
        
        public String getCountry() {
            return this.mCountry;
        }
        
        @Override
        public final EntryLabel getEntryLabel() {
            return EntryLabel.POSTAL_ADDRESS;
        }
        
        public String getExtendedAddress() {
            return this.mExtendedAddress;
        }
        
        public String getFormattedAddress(int n) {
            final StringBuilder sb = new StringBuilder();
            final int n2 = 1;
            final int n3 = 1;
            final String[] array = { this.mPobox, this.mExtendedAddress, this.mStreet, this.mLocalty, this.mRegion, this.mPostalCode, this.mCountry };
            if (VCardConfig.isJapaneseDevice(n)) {
                int i = 6;
                n = n3;
                while (i >= 0) {
                    final String s = array[i];
                    int n4 = n;
                    if (!TextUtils.isEmpty((CharSequence)s)) {
                        if (n == 0) {
                            sb.append(' ');
                        }
                        else {
                            n = 0;
                        }
                        sb.append(s);
                        n4 = n;
                    }
                    --i;
                    n = n4;
                }
            }
            else {
                int j = 0;
                n = n2;
                while (j < 7) {
                    final String s2 = array[j];
                    int n5 = n;
                    if (!TextUtils.isEmpty((CharSequence)s2)) {
                        if (n == 0) {
                            sb.append(' ');
                        }
                        else {
                            n = 0;
                        }
                        sb.append(s2);
                        n5 = n;
                    }
                    ++j;
                    n = n5;
                }
            }
            return sb.toString().trim();
        }
        
        public String getLabel() {
            return this.mLabel;
        }
        
        public String getLocalty() {
            return this.mLocalty;
        }
        
        public String getPobox() {
            return this.mPobox;
        }
        
        public String getPostalCode() {
            return this.mPostalCode;
        }
        
        public String getRegion() {
            return this.mRegion;
        }
        
        public String getStreet() {
            return this.mStreet;
        }
        
        public int getType() {
            return this.mType;
        }
        
        @Override
        public int hashCode() {
            final int mType = this.mType;
            int hashCode;
            if (this.mLabel != null) {
                hashCode = this.mLabel.hashCode();
            }
            else {
                hashCode = 0;
            }
            int n;
            if (this.mIsPrimary) {
                n = 1231;
            }
            else {
                n = 1237;
            }
            int n2 = (mType * 31 + hashCode) * 31 + n;
            for (final String s : new String[] { this.mPobox, this.mExtendedAddress, this.mStreet, this.mLocalty, this.mRegion, this.mPostalCode, this.mCountry }) {
                int hashCode2;
                if (s != null) {
                    hashCode2 = s.hashCode();
                }
                else {
                    hashCode2 = 0;
                }
                n2 = n2 * 31 + hashCode2;
            }
            return n2;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mPobox) && TextUtils.isEmpty((CharSequence)this.mExtendedAddress) && TextUtils.isEmpty((CharSequence)this.mStreet) && TextUtils.isEmpty((CharSequence)this.mLocalty) && TextUtils.isEmpty((CharSequence)this.mRegion) && TextUtils.isEmpty((CharSequence)this.mPostalCode) && TextUtils.isEmpty((CharSequence)this.mCountry);
        }
        
        public boolean isPrimary() {
            return this.mIsPrimary;
        }
        
        @Override
        public String toString() {
            return String.format("type: %d, label: %s, isPrimary: %s, pobox: %s, extendedAddress: %s, street: %s, localty: %s, region: %s, postalCode %s, country: %s", this.mType, this.mLabel, this.mIsPrimary, this.mPobox, this.mExtendedAddress, this.mStreet, this.mLocalty, this.mRegion, this.mPostalCode, this.mCountry);
        }
    }
    
    public static class SipData implements EntryElement
    {
        private final String mAddress;
        private final boolean mIsPrimary;
        private final String mLabel;
        private final int mType;
        
        public SipData(final String mAddress, final int mType, final String mLabel, final boolean mIsPrimary) {
            if (mAddress.startsWith("sip:")) {
                this.mAddress = mAddress.substring(4);
            }
            else {
                this.mAddress = mAddress;
            }
            this.mType = mType;
            this.mLabel = mLabel;
            this.mIsPrimary = mIsPrimary;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation$Builder insert = ContentProviderOperation.newInsert(ContactsContract$Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/sip_address");
            insert.withValue("data1", this.mAddress);
            insert.withValue("data2", this.mType);
            if (this.mType == 0) {
                insert.withValue("data3", this.mLabel);
            }
            if (this.mIsPrimary) {
                insert.withValue("is_primary", this.mIsPrimary);
            }
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this != o) {
                if (!(o instanceof SipData)) {
                    b = false;
                }
                else {
                    final SipData sipData = (SipData)o;
                    if (this.mType != sipData.mType || !TextUtils.equals((CharSequence)this.mLabel, (CharSequence)sipData.mLabel) || !TextUtils.equals((CharSequence)this.mAddress, (CharSequence)sipData.mAddress) || this.mIsPrimary != sipData.mIsPrimary) {
                        b = false;
                    }
                }
            }
            return b;
        }
        
        public String getAddress() {
            return this.mAddress;
        }
        
        @Override
        public EntryLabel getEntryLabel() {
            return EntryLabel.SIP;
        }
        
        public String getLabel() {
            return this.mLabel;
        }
        
        public int getType() {
            return this.mType;
        }
        
        @Override
        public int hashCode() {
            int hashCode = 0;
            final int mType = this.mType;
            int hashCode2;
            if (this.mLabel != null) {
                hashCode2 = this.mLabel.hashCode();
            }
            else {
                hashCode2 = 0;
            }
            if (this.mAddress != null) {
                hashCode = this.mAddress.hashCode();
            }
            int n;
            if (this.mIsPrimary) {
                n = 1231;
            }
            else {
                n = 1237;
            }
            return ((mType * 31 + hashCode2) * 31 + hashCode) * 31 + n;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mAddress);
        }
        
        @Override
        public String toString() {
            return "sip: " + this.mAddress;
        }
    }
    
    private class ToStringIterator implements EntryElementIterator
    {
        private StringBuilder mBuilder;
        private boolean mFirstElement;
        
        @Override
        public boolean onElement(final EntryElement entryElement) {
            if (!this.mFirstElement) {
                this.mBuilder.append(", ");
                this.mFirstElement = false;
            }
            this.mBuilder.append("[").append(entryElement.toString()).append("]");
            return true;
        }
        
        @Override
        public void onElementGroupEnded() {
            this.mBuilder.append("\n");
        }
        
        @Override
        public void onElementGroupStarted(final EntryLabel entryLabel) {
            this.mBuilder.append(entryLabel.toString() + ": ");
            this.mFirstElement = true;
        }
        
        @Override
        public void onIterationEnded() {
            this.mBuilder.append("]]\n");
        }
        
        @Override
        public void onIterationStarted() {
            (this.mBuilder = new StringBuilder()).append("[[hash: " + VCardEntry.this.hashCode() + "\n");
        }
        
        @Override
        public String toString() {
            return this.mBuilder.toString();
        }
    }
    
    public static class WebsiteData implements EntryElement
    {
        private final String mWebsite;
        
        public WebsiteData(final String mWebsite) {
            this.mWebsite = mWebsite;
        }
        
        @Override
        public void constructInsertOperation(final List<ContentProviderOperation> list, final int n) {
            final ContentProviderOperation$Builder insert = ContentProviderOperation.newInsert(ContactsContract$Data.CONTENT_URI);
            insert.withValueBackReference("raw_contact_id", n);
            insert.withValue("mimetype", "vnd.android.cursor.item/website");
            insert.withValue("data1", this.mWebsite);
            insert.withValue("data2", 1);
            list.add(insert.build());
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof WebsiteData && TextUtils.equals((CharSequence)this.mWebsite, (CharSequence)((WebsiteData)o).mWebsite));
        }
        
        @Override
        public EntryLabel getEntryLabel() {
            return EntryLabel.WEBSITE;
        }
        
        public String getWebsite() {
            return this.mWebsite;
        }
        
        @Override
        public int hashCode() {
            int hashCode;
            if (this.mWebsite != null) {
                hashCode = this.mWebsite.hashCode();
            }
            else {
                hashCode = 0;
            }
            return hashCode;
        }
        
        @Override
        public boolean isEmpty() {
            return TextUtils.isEmpty((CharSequence)this.mWebsite);
        }
        
        @Override
        public String toString() {
            return "website: " + this.mWebsite;
        }
    }
}
