package com.navdy.hud.app.bluetooth.pbap;

import java.io.InputStream;
import com.navdy.hud.app.bluetooth.vcard.VCardEntry;
import java.io.IOException;
import android.util.Log;
import com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters;

final class BluetoothPbapRequestPullVcardEntry extends BluetoothPbapRequest
{
    private static final String TAG = "BTPbapReqPullVcardEntry";
    private static final String TYPE = "x-bt/vcard";
    private final byte mFormat;
    private BluetoothPbapVcardList mResponse;
    
    public BluetoothPbapRequestPullVcardEntry(final String s, final long n, final byte b) {
        this.mHeaderSet.setHeader(1, s);
        this.mHeaderSet.setHeader(66, "x-bt/vcard");
        byte mFormat = b;
        if (b != 0 && (mFormat = b) != 1) {
            mFormat = 0;
        }
        final ObexAppParameters obexAppParameters = new ObexAppParameters();
        if (n != 0L) {
            obexAppParameters.add((byte)6, n);
        }
        obexAppParameters.add((byte)7, mFormat);
        obexAppParameters.addToHeaderSet(this.mHeaderSet);
        this.mFormat = mFormat;
    }
    
    @Override
    protected void checkResponseCode(final int n) throws IOException {
        Log.v("BTPbapReqPullVcardEntry", "checkResponseCode");
        if (this.mResponse.getCount() == 0) {
            if (n != 196 && n != 198) {
                throw new IOException("Invalid response received:" + n);
            }
            Log.v("BTPbapReqPullVcardEntry", "Vcard Entry not found");
        }
    }
    
    public VCardEntry getVcard() {
        return this.mResponse.getFirst();
    }
    
    @Override
    protected void readResponse(final InputStream inputStream) throws IOException {
        Log.v("BTPbapReqPullVcardEntry", "readResponse");
        this.mResponse = new BluetoothPbapVcardList(inputStream, this.mFormat);
    }
}
