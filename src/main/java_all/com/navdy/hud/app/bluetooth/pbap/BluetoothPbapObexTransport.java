package com.navdy.hud.app.bluetooth.pbap;

import java.io.OutputStream;
import java.io.InputStream;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import android.bluetooth.BluetoothSocket;
import com.navdy.hud.app.bluetooth.obex.ObexTransport;

class BluetoothPbapObexTransport implements ObexTransport
{
    private BluetoothSocket mSocket;
    
    public BluetoothPbapObexTransport(final BluetoothSocket mSocket) {
        this.mSocket = null;
        this.mSocket = mSocket;
    }
    
    @Override
    public void close() throws IOException {
        this.mSocket.close();
    }
    
    @Override
    public void connect() throws IOException {
    }
    
    @Override
    public void create() throws IOException {
    }
    
    @Override
    public void disconnect() throws IOException {
    }
    
    @Override
    public int getMaxReceivePacketSize() {
        return -1;
    }
    
    @Override
    public int getMaxTransmitPacketSize() {
        return -1;
    }
    
    public boolean isConnected() throws IOException {
        return this.mSocket.isConnected();
    }
    
    @Override
    public boolean isSrmSupported() {
        return false;
    }
    
    @Override
    public void listen() throws IOException {
    }
    
    @Override
    public DataInputStream openDataInputStream() throws IOException {
        return new DataInputStream(this.openInputStream());
    }
    
    @Override
    public DataOutputStream openDataOutputStream() throws IOException {
        return new DataOutputStream(this.openOutputStream());
    }
    
    @Override
    public InputStream openInputStream() throws IOException {
        return this.mSocket.getInputStream();
    }
    
    @Override
    public OutputStream openOutputStream() throws IOException {
        return this.mSocket.getOutputStream();
    }
}
