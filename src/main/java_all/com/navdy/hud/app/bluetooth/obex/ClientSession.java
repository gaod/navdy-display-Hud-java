package com.navdy.hud.app.bluetooth.obex;

import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.InputStream;

public final class ClientSession extends ObexSession
{
    private static final String TAG = "ClientSession";
    private byte[] mConnectionId;
    private final InputStream mInput;
    private final boolean mLocalSrmSupported;
    private int mMaxTxPacketSize;
    private boolean mObexConnected;
    private boolean mOpen;
    private final OutputStream mOutput;
    private boolean mRequestActive;
    private final ObexTransport mTransport;
    
    public ClientSession(final ObexTransport mTransport) throws IOException {
        this.mConnectionId = null;
        this.mMaxTxPacketSize = 255;
        this.mInput = mTransport.openInputStream();
        this.mOutput = mTransport.openOutputStream();
        this.mOpen = true;
        this.mRequestActive = false;
        this.mLocalSrmSupported = mTransport.isSrmSupported();
        this.mTransport = mTransport;
    }
    
    public ClientSession(final ObexTransport mTransport, final boolean mLocalSrmSupported) throws IOException {
        this.mConnectionId = null;
        this.mMaxTxPacketSize = 255;
        this.mInput = mTransport.openInputStream();
        this.mOutput = mTransport.openOutputStream();
        this.mOpen = true;
        this.mRequestActive = false;
        this.mLocalSrmSupported = mLocalSrmSupported;
        this.mTransport = mTransport;
    }
    
    private void setRequestActive() throws IOException {
        synchronized (this) {
            if (this.mRequestActive) {
                throw new IOException("OBEX request is already being performed");
            }
        }
        this.mRequestActive = true;
    }
    // monitorexit(this)
    
    public void close() throws IOException {
        this.mOpen = false;
        this.mInput.close();
        this.mOutput.close();
    }
    
    public HeaderSet connect(final HeaderSet set) throws IOException {
        this.ensureOpen();
        if (this.mObexConnected) {
            throw new IOException("Already connected to server");
        }
        this.setRequestActive();
        int n = 4;
        Object header = null;
        if (set != null) {
            if (set.nonce != null) {
                this.mChallengeDigest = new byte[16];
                System.arraycopy(set.nonce, 0, this.mChallengeDigest, 0, 16);
            }
            header = ObexHelper.createHeader(set, false);
            n = 4 + header.length;
        }
        final byte[] array = new byte[n];
        final int maxRxPacketSize = ObexHelper.getMaxRxPacketSize(this.mTransport);
        array[0] = 16;
        array[1] = 0;
        array[2] = (byte)(maxRxPacketSize >> 8);
        array[3] = (byte)(maxRxPacketSize & 0xFF);
        if (header != null) {
            System.arraycopy(header, 0, array, 4, header.length);
        }
        if (array.length + 3 > 65534) {
            throw new IOException("Packet size exceeds max packet size for connect");
        }
        final HeaderSet set2 = new HeaderSet();
        this.sendRequest(128, array, set2, null, false);
        if (set2.responseCode == 160) {
            this.mObexConnected = true;
        }
        this.setRequestInactive();
        return set2;
    }
    
    public HeaderSet delete(final HeaderSet set) throws IOException {
        final Operation put = this.put(set);
        put.getResponseCode();
        final HeaderSet receivedHeader = put.getReceivedHeader();
        put.close();
        return receivedHeader;
    }
    
    public HeaderSet disconnect(final HeaderSet set) throws IOException {
        if (!this.mObexConnected) {
            throw new IOException("Not connected to the server");
        }
        this.setRequestActive();
        this.ensureOpen();
        final byte[] array = null;
        byte[] header;
        if (set != null) {
            if (set.nonce != null) {
                this.mChallengeDigest = new byte[16];
                System.arraycopy(set.nonce, 0, this.mChallengeDigest, 0, 16);
            }
            if (this.mConnectionId != null) {
                set.mConnectionID = new byte[4];
                System.arraycopy(this.mConnectionId, 0, set.mConnectionID, 0, 4);
            }
            if ((header = ObexHelper.createHeader(set, false)).length + 3 > this.mMaxTxPacketSize) {
                throw new IOException("Packet size exceeds max packet size");
            }
        }
        else {
            header = array;
            if (this.mConnectionId != null) {
                header = new byte[5];
                header[0] = -53;
                System.arraycopy(this.mConnectionId, 0, header, 1, 4);
            }
        }
        final HeaderSet set2 = new HeaderSet();
        this.sendRequest(129, header, set2, null, false);
        synchronized (this) {
            this.mObexConnected = false;
            this.setRequestInactive();
            return set2;
        }
    }
    
    public void ensureOpen() throws IOException {
        synchronized (this) {
            if (!this.mOpen) {
                throw new IOException("Connection closed");
            }
        }
    }
    // monitorexit(this)
    
    public Operation get(HeaderSet set) throws IOException {
        if (!this.mObexConnected) {
            throw new IOException("Not connected to the server");
        }
        this.setRequestActive();
        this.ensureOpen();
        if (set == null) {
            set = new HeaderSet();
        }
        else {
            final HeaderSet set2 = set = set;
            if (set2.nonce != null) {
                this.mChallengeDigest = new byte[16];
                System.arraycopy(set2.nonce, 0, this.mChallengeDigest, 0, 16);
                set = set2;
            }
        }
        if (this.mConnectionId != null) {
            set.mConnectionID = new byte[4];
            System.arraycopy(this.mConnectionId, 0, set.mConnectionID, 0, 4);
        }
        if (this.mLocalSrmSupported) {
            set.setHeader(151, (byte)1);
        }
        return new ClientOperation(this.mMaxTxPacketSize, this, set, true);
    }
    
    public long getConnectionID() {
        long convertToLong;
        if (this.mConnectionId == null) {
            convertToLong = -1L;
        }
        else {
            convertToLong = ObexHelper.convertToLong(this.mConnectionId);
        }
        return convertToLong;
    }
    
    public boolean isSrmSupported() {
        return this.mLocalSrmSupported;
    }
    
    public Operation put(final HeaderSet set) throws IOException {
        if (!this.mObexConnected) {
            throw new IOException("Not connected to the server");
        }
        this.setRequestActive();
        this.ensureOpen();
        HeaderSet set2;
        if (set == null) {
            set2 = new HeaderSet();
        }
        else {
            set2 = set;
            if (set.nonce != null) {
                this.mChallengeDigest = new byte[16];
                System.arraycopy(set.nonce, 0, this.mChallengeDigest, 0, 16);
                set2 = set;
            }
        }
        if (this.mConnectionId != null) {
            set2.mConnectionID = new byte[4];
            System.arraycopy(this.mConnectionId, 0, set2.mConnectionID, 0, 4);
        }
        if (this.mLocalSrmSupported) {
            set2.setHeader(151, (byte)1);
        }
        return new ClientOperation(this.mMaxTxPacketSize, this, set2, false);
    }
    
    public boolean sendRequest(final int n, byte[] updateHeaderSet, final HeaderSet set, final PrivateInputStream privateInputStream, final boolean b) throws IOException {
        if (updateHeaderSet != null && updateHeaderSet.length + 3 > 65534) {
            throw new IOException("header too large ");
        }
        final boolean b2 = false;
        int n3;
        final int n2 = n3 = 0;
        int n4 = b2 ? 1 : 0;
        if (b) {
            if (n == 2) {
                n3 = 1;
                n4 = (b2 ? 1 : 0);
            }
            else if (n == 3) {
                n3 = 1;
                n4 = (b2 ? 1 : 0);
            }
            else {
                n3 = n2;
                n4 = (b2 ? 1 : 0);
                if (n == 131) {
                    n4 = 1;
                    n3 = n2;
                }
            }
        }
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write((byte)n);
        if (updateHeaderSet == null) {
            byteArrayOutputStream.write(0);
            byteArrayOutputStream.write(3);
        }
        else {
            byteArrayOutputStream.write((byte)(updateHeaderSet.length + 3 >> 8));
            byteArrayOutputStream.write((byte)(updateHeaderSet.length + 3));
            byteArrayOutputStream.write(updateHeaderSet);
        }
        if (n4 == 0) {
            this.mOutput.write(byteArrayOutputStream.toByteArray());
            this.mOutput.flush();
        }
        if (n3 != 0) {
            return true;
        }
        set.responseCode = this.mInput.read();
        final int n5 = this.mInput.read() << 8 | this.mInput.read();
        if (n5 > ObexHelper.getMaxRxPacketSize(this.mTransport)) {
            throw new IOException("Packet received exceeds packet size limit");
        }
        if (n5 <= 3) {
            return true;
        }
        if (n == 128) {
            this.mInput.read();
            this.mInput.read();
            this.mMaxTxPacketSize = (this.mInput.read() << 8) + this.mInput.read();
            if (this.mMaxTxPacketSize > 64512) {
                this.mMaxTxPacketSize = 64512;
            }
            if (this.mMaxTxPacketSize > ObexHelper.getMaxTxPacketSize(this.mTransport)) {
                Log.w("ClientSession", "An OBEX packet size of " + this.mMaxTxPacketSize + "was" + " requested. Transport only allows: " + ObexHelper.getMaxTxPacketSize(this.mTransport) + " Lowering limit to this value.");
                this.mMaxTxPacketSize = ObexHelper.getMaxTxPacketSize(this.mTransport);
            }
            if (n5 <= 7) {
                return true;
            }
            final byte[] array = new byte[n5 - 7];
            int read = this.mInput.read(array);
            while (true) {
                updateHeaderSet = array;
                if (read == n5 - 7) {
                    break;
                }
                read += this.mInput.read(array, read, array.length - read);
            }
        }
        else {
            updateHeaderSet = new byte[n5 - 3];
            for (int i = this.mInput.read(updateHeaderSet); i != n5 - 3; i += this.mInput.read(updateHeaderSet, i, updateHeaderSet.length - i)) {}
            if (n == 255) {
                return true;
            }
        }
        updateHeaderSet = ObexHelper.updateHeaderSet(set, updateHeaderSet);
        if (privateInputStream != null && updateHeaderSet != null) {
            privateInputStream.writeBytes(updateHeaderSet, 1);
        }
        if (set.mConnectionID != null) {
            this.mConnectionId = new byte[4];
            System.arraycopy(set.mConnectionID, 0, this.mConnectionId, 0, 4);
        }
        if (set.mAuthResp != null && !this.handleAuthResp(set.mAuthResp)) {
            this.setRequestInactive();
            throw new IOException("Authentication Failed");
        }
        if (set.responseCode != 193 || set.mAuthChall == null || !this.handleAuthChall(set)) {
            return true;
        }
        byteArrayOutputStream.write(78);
        byteArrayOutputStream.write((byte)(set.mAuthResp.length + 3 >> 8));
        byteArrayOutputStream.write((byte)(set.mAuthResp.length + 3));
        byteArrayOutputStream.write(set.mAuthResp);
        set.mAuthChall = null;
        set.mAuthResp = null;
        updateHeaderSet = new byte[byteArrayOutputStream.size() - 3];
        System.arraycopy(byteArrayOutputStream.toByteArray(), 3, updateHeaderSet, 0, updateHeaderSet.length);
        return this.sendRequest(n, updateHeaderSet, set, privateInputStream, false);
        sendRequest = true;
        return sendRequest;
    }
    
    public void setAuthenticator(final Authenticator mAuthenticator) throws IOException {
        if (mAuthenticator == null) {
            throw new IOException("Authenticator may not be null");
        }
        this.mAuthenticator = mAuthenticator;
    }
    
    public void setConnectionID(final long n) {
        if (n < 0L || n > 4294967295L) {
            throw new IllegalArgumentException("Connection ID is not in a valid range");
        }
        this.mConnectionId = ObexHelper.convertToByteArray(n);
    }
    
    public HeaderSet setPath(HeaderSet set, final boolean b, final boolean b2) throws IOException {
        if (!this.mObexConnected) {
            throw new IOException("Not connected to the server");
        }
        this.setRequestActive();
        this.ensureOpen();
        if (set == null) {
            set = new HeaderSet();
        }
        else {
            final HeaderSet set2 = set = set;
            if (set2.nonce != null) {
                this.mChallengeDigest = new byte[16];
                System.arraycopy(set2.nonce, 0, this.mChallengeDigest, 0, 16);
                set = set2;
            }
        }
        if (set.nonce != null) {
            this.mChallengeDigest = new byte[16];
            System.arraycopy(set.nonce, 0, this.mChallengeDigest, 0, 16);
        }
        if (this.mConnectionId != null) {
            set.mConnectionID = new byte[4];
            System.arraycopy(this.mConnectionId, 0, set.mConnectionID, 0, 4);
        }
        final byte[] header = ObexHelper.createHeader(set, false);
        final int n = 2 + header.length;
        if (n > this.mMaxTxPacketSize) {
            throw new IOException("Packet size exceeds max packet size");
        }
        int n2 = 0;
        if (b) {
            n2 = 0 + 1;
        }
        int n3 = n2;
        if (!b2) {
            n3 = (n2 | 0x2);
        }
        final byte[] array = new byte[n];
        array[0] = (byte)n3;
        array[1] = 0;
        if (set != null) {
            System.arraycopy(header, 0, array, 2, header.length);
        }
        final HeaderSet set3 = new HeaderSet();
        this.sendRequest(133, array, set3, null, false);
        this.setRequestInactive();
        return set3;
    }
    
    void setRequestInactive() {
        synchronized (this) {
            this.mRequestActive = false;
        }
    }
}
