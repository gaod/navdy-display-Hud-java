package com.navdy.hud.app.bluetooth.pbap;

import java.io.InputStream;
import com.navdy.hud.app.bluetooth.obex.ClientSession;
import java.io.IOException;
import android.util.Log;
import com.navdy.hud.app.bluetooth.obex.ClientOperation;
import com.navdy.hud.app.bluetooth.obex.HeaderSet;

abstract class BluetoothPbapRequest
{
    protected static final byte OAP_TAGID_FILTER = 6;
    protected static final byte OAP_TAGID_FORMAT = 7;
    protected static final byte OAP_TAGID_LIST_START_OFFSET = 5;
    protected static final byte OAP_TAGID_MAX_LIST_COUNT = 4;
    protected static final byte OAP_TAGID_NEW_MISSED_CALLS = 9;
    protected static final byte OAP_TAGID_ORDER = 1;
    protected static final byte OAP_TAGID_PHONEBOOK_SIZE = 8;
    protected static final byte OAP_TAGID_SEARCH_ATTRIBUTE = 3;
    protected static final byte OAP_TAGID_SEARCH_VALUE = 2;
    private static final String TAG = "BTPbapRequest";
    private boolean mAborted;
    protected HeaderSet mHeaderSet;
    private ClientOperation mOp;
    protected int mResponseCode;
    
    public BluetoothPbapRequest() {
        this.mAborted = false;
        this.mOp = null;
        this.mHeaderSet = new HeaderSet();
    }
    
    public void abort() {
        this.mAborted = true;
        if (this.mOp == null) {
            return;
        }
        try {
            this.mOp.abort();
        }
        catch (IOException ex) {
            Log.e("BTPbapRequest", "Exception occured when trying to abort", (Throwable)ex);
        }
    }
    
    protected void checkResponseCode(final int n) throws IOException {
        Log.v("BTPbapRequest", "checkResponseCode");
    }
    
    public void execute(final ClientSession clientSession) throws IOException {
        Log.v("BTPbapRequest", "execute");
        if (this.mAborted) {
            this.mResponseCode = 208;
        }
        else {
            try {
                (this.mOp = (ClientOperation)clientSession.get(this.mHeaderSet)).setGetFinalFlag(true);
                this.mOp.continueOperation(true, false);
                this.readResponseHeaders(this.mOp.getReceivedHeader());
                final InputStream openInputStream = this.mOp.openInputStream();
                this.readResponse(openInputStream);
                openInputStream.close();
                this.mOp.close();
                this.mResponseCode = this.mOp.getResponseCode();
                Log.d("BTPbapRequest", "mResponseCode=" + this.mResponseCode);
                this.checkResponseCode(this.mResponseCode);
            }
            catch (IOException ex) {
                Log.e("BTPbapRequest", "IOException occured when processing request", (Throwable)ex);
                this.mResponseCode = 208;
                throw ex;
            }
        }
    }
    
    public final boolean isSuccess() {
        return this.mResponseCode == 160;
    }
    
    protected void readResponse(final InputStream inputStream) throws IOException {
        Log.v("BTPbapRequest", "readResponse");
    }
    
    protected void readResponseHeaders(final HeaderSet set) {
        Log.v("BTPbapRequest", "readResponseHeaders");
    }
}
