package com.navdy.hud.app.bluetooth.pbap.utils;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.TimeZone;
import java.util.Calendar;
import java.util.regex.Pattern;
import java.util.Date;

public final class ObexTime
{
    private Date mDate;
    
    public ObexTime(final String s) {
        final Matcher matcher = Pattern.compile("(\\d{4})(\\d{2})(\\d{2})T(\\d{2})(\\d{2})(\\d{2})(([+-])(\\d{2})(\\d{2}))?").matcher(s);
        if (matcher.matches()) {
            final Calendar instance = Calendar.getInstance();
            instance.set(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)) - 1, Integer.parseInt(matcher.group(3)), Integer.parseInt(matcher.group(4)), Integer.parseInt(matcher.group(5)), Integer.parseInt(matcher.group(6)));
            if (matcher.group(7) != null) {
                int rawOffset = (Integer.parseInt(matcher.group(9)) * 60 + Integer.parseInt(matcher.group(10))) * 60 * 1000;
                if (matcher.group(8).equals("-")) {
                    rawOffset = -rawOffset;
                }
                final TimeZone timeZone = TimeZone.getTimeZone("UTC");
                timeZone.setRawOffset(rawOffset);
                instance.setTimeZone(timeZone);
            }
            this.mDate = instance.getTime();
        }
    }
    
    public ObexTime(final Date mDate) {
        this.mDate = mDate;
    }
    
    public Date getTime() {
        return this.mDate;
    }
    
    @Override
    public String toString() {
        String format;
        if (this.mDate == null) {
            format = null;
        }
        else {
            final Calendar instance = Calendar.getInstance();
            instance.setTime(this.mDate);
            format = String.format(Locale.US, "%04d%02d%02dT%02d%02d%02d", instance.get(1), instance.get(2) + 1, instance.get(5), instance.get(11), instance.get(12), instance.get(13));
        }
        return format;
    }
}
