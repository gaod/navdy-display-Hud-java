package com.navdy.hud.app.bluetooth.obex;

import java.io.IOException;
import java.io.InputStream;

public final class PrivateInputStream extends InputStream
{
    private byte[] mData;
    private int mIndex;
    private boolean mOpen;
    private BaseStream mParent;
    
    public PrivateInputStream(final BaseStream mParent) {
        this.mParent = mParent;
        this.mData = new byte[0];
        this.mIndex = 0;
        this.mOpen = true;
    }
    
    private void ensureOpen() throws IOException {
        this.mParent.ensureOpen();
        if (!this.mOpen) {
            throw new IOException("Input stream is closed");
        }
    }
    
    @Override
    public int available() throws IOException {
        synchronized (this) {
            this.ensureOpen();
            return this.mData.length - this.mIndex;
        }
    }
    
    @Override
    public void close() throws IOException {
        this.mOpen = false;
        this.mParent.streamClosed(true);
    }
    
    @Override
    public int read() throws IOException {
        synchronized (this) {
            this.ensureOpen();
            while (this.mData.length == this.mIndex) {
                if (!this.mParent.continueOperation(true, true)) {
                    return -1;
                }
            }
            return this.mData[this.mIndex++] & 0xFF;
        }
    }
    
    @Override
    public int read(final byte[] array) throws IOException {
        return this.read(array, 0, array.length);
    }
    
    @Override
    public int read(final byte[] array, int n, int i) throws IOException {
        // monitorenter(this)
        if (array == null) {
            try {
                throw new IOException("buffer is null");
            }
            finally {
            }
            // monitorexit(this)
        }
        if ((n | i) < 0 || i > array.length - n) {
            throw new ArrayIndexOutOfBoundsException("index outof bound");
        }
        this.ensureOpen();
        final int n2 = this.mData.length - this.mIndex;
        int n3 = n;
        n = 0;
        while (true) {
            int n4;
            for (n4 = i, i = n2; i <= n4; i = this.mData.length - this.mIndex) {
                System.arraycopy(this.mData, this.mIndex, array, n3, i);
                this.mIndex += i;
                n3 += i;
                n += i;
                n4 -= i;
                if (!this.mParent.continueOperation(true, true)) {
                    if (n == 0) {
                        n = -1;
                    }
                    // monitorexit(this)
                    return n;
                }
            }
            i = n;
            if (n4 > 0) {
                System.arraycopy(this.mData, this.mIndex, array, n3, n4);
                this.mIndex += n4;
                i = n + n4;
            }
            n = i;
            continue;
        }
    }
    
    public void writeBytes(final byte[] array, final int n) {
        synchronized (this) {
            final byte[] mData = new byte[array.length - n + (this.mData.length - this.mIndex)];
            System.arraycopy(this.mData, this.mIndex, mData, 0, this.mData.length - this.mIndex);
            System.arraycopy(array, n, mData, this.mData.length - this.mIndex, array.length - n);
            this.mData = mData;
            this.mIndex = 0;
            this.notifyAll();
        }
    }
}
