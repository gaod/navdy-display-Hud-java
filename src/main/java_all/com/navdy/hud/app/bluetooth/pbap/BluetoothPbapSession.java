package com.navdy.hud.app.bluetooth.pbap;

import java.util.UUID;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import android.bluetooth.BluetoothSocket;
import android.os.Message;
import com.navdy.hud.app.bluetooth.obex.ObexTransport;
import android.util.Log;
import android.os.Handler;
import android.os.HandlerThread;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothAdapter;
import android.os.Handler;

class BluetoothPbapSession implements Handler$Callback
{
    public static final int ACTION_LISTING = 14;
    public static final int ACTION_PHONEBOOK_SIZE = 16;
    public static final int ACTION_VCARD = 15;
    public static final int AUTH_REQUESTED = 8;
    public static final int AUTH_TIMEOUT = 9;
    private static final String PBAP_UUID = "0000112f-0000-1000-8000-00805f9b34fb";
    public static final int REQUEST_COMPLETED = 3;
    public static final int REQUEST_FAILED = 4;
    private static final int RFCOMM_CONNECTED = 1;
    private static final int RFCOMM_FAILED = 2;
    public static final int SESSION_CONNECTED = 6;
    public static final int SESSION_CONNECTING = 5;
    public static final int SESSION_DISCONNECTED = 7;
    private static final String TAG = "BTPbapSession";
    private final BluetoothAdapter mAdapter;
    private RfcommConnectThread mConnectThread;
    private final BluetoothDevice mDevice;
    private final HandlerThread mHandlerThread;
    private BluetoothPbapObexSession mObexSession;
    private final Handler mParentHandler;
    private BluetoothPbapRequest mPendingRequest;
    private final Handler mSessionHandler;
    private BluetoothPbapObexTransport mTransport;
    
    public BluetoothPbapSession(final BluetoothDevice mDevice, final Handler mParentHandler) {
        this.mPendingRequest = null;
        this.mAdapter = BluetoothAdapter.getDefaultAdapter();
        if (this.mAdapter == null) {
            throw new NullPointerException("No Bluetooth adapter in the system");
        }
        this.mDevice = mDevice;
        this.mParentHandler = mParentHandler;
        this.mConnectThread = null;
        this.mTransport = null;
        this.mObexSession = null;
        (this.mHandlerThread = new HandlerThread("PBAP session handler", 10)).start();
        this.mSessionHandler = new Handler(this.mHandlerThread.getLooper(), (Handler$Callback)this);
    }
    
    private void startObexSession() {
        Log.d("BTPbapSession", "startObexSession");
        (this.mObexSession = new BluetoothPbapObexSession(this.mTransport)).start(this.mSessionHandler);
    }
    
    private void startRfcomm() {
        Log.d("BTPbapSession", "startRfcomm");
        if (this.mConnectThread == null && this.mObexSession == null) {
            this.mParentHandler.obtainMessage(5).sendToTarget();
            (this.mConnectThread = new RfcommConnectThread()).start();
        }
    }
    
    private void stopObexSession() {
        Log.d("BTPbapSession", "stopObexSession");
        try {
            if (this.mObexSession != null) {
                this.mObexSession.stop();
            }
        }
        catch (Throwable t) {
            Log.e("BTPbapSession", "", t);
            this.mObexSession = null;
        }
        finally {
            this.mObexSession = null;
        }
    }
    
    private void stopRfcomm() {
        Log.d("BTPbapSession", "stopRfcomm");
        Label_0027: {
            if (this.mConnectThread == null) {
                break Label_0027;
            }
            while (true) {
                try {
                    this.mConnectThread.join();
                    this.mConnectThread = null;
                    if (this.mTransport == null) {
                        return;
                    }
                    try {
                        this.mTransport.close();
                        this.mTransport = null;
                    }
                    catch (Throwable t) {}
                }
                catch (Throwable t2) {
                    continue;
                }
                break;
            }
        }
    }
    
    public void abort() {
        Log.d("BTPbapSession", "abort");
        if (this.mPendingRequest != null) {
            this.mParentHandler.obtainMessage(4, this.mPendingRequest).sendToTarget();
            this.mPendingRequest = null;
        }
        if (this.mObexSession != null) {
            this.mObexSession.abort();
        }
    }
    
    public boolean handleMessage(final Message message) {
        Log.d("BTPbapSession", "Handler: msg: " + message.what);
        boolean b = false;
        switch (message.what) {
            default:
                b = false;
                break;
            case 2:
                this.mConnectThread = null;
                this.mParentHandler.obtainMessage(7).sendToTarget();
                if (this.mPendingRequest != null) {
                    this.mParentHandler.obtainMessage(4, this.mPendingRequest).sendToTarget();
                    this.mPendingRequest = null;
                    return true;
                }
                return true;
            case 1:
                this.mConnectThread = null;
                this.mTransport = (BluetoothPbapObexTransport)message.obj;
                this.startObexSession();
                return true;
            case 101:
                this.stopObexSession();
                this.mParentHandler.obtainMessage(7).sendToTarget();
                if (this.mPendingRequest != null) {
                    this.mParentHandler.obtainMessage(4, this.mPendingRequest).sendToTarget();
                    this.mPendingRequest = null;
                    return true;
                }
                return true;
            case 100:
                this.mParentHandler.obtainMessage(6).sendToTarget();
                if (this.mPendingRequest != null) {
                    this.mObexSession.schedule(this.mPendingRequest);
                    this.mPendingRequest = null;
                    return true;
                }
                return true;
            case 102:
                this.mParentHandler.obtainMessage(7).sendToTarget();
                this.stopRfcomm();
                return true;
            case 103:
                this.mParentHandler.obtainMessage(3, message.obj).sendToTarget();
                return true;
            case 104:
                this.mParentHandler.obtainMessage(4, message.obj).sendToTarget();
                return true;
            case 105:
                this.mParentHandler.obtainMessage(8).sendToTarget();
                this.mSessionHandler.sendMessageDelayed(this.mSessionHandler.obtainMessage(106), 30000L);
                return true;
            case 106:
                this.setAuthResponse(null);
                this.mParentHandler.obtainMessage(9).sendToTarget();
                return true;
        }
        return b;
        b = true;
        return b;
    }
    
    public boolean makeRequest(final BluetoothPbapRequest mPendingRequest) {
        Log.v("BTPbapSession", "makeRequest: " + mPendingRequest.getClass().getSimpleName());
        boolean schedule;
        if (this.mPendingRequest != null) {
            Log.w("BTPbapSession", "makeRequest: request already queued, exiting");
            schedule = false;
        }
        else if (this.mObexSession == null) {
            this.mPendingRequest = mPendingRequest;
            this.startRfcomm();
            schedule = true;
        }
        else {
            schedule = this.mObexSession.schedule(mPendingRequest);
        }
        return schedule;
    }
    
    public boolean setAuthResponse(final String authReply) {
        Log.d("BTPbapSession", "setAuthResponse key=" + authReply);
        this.mSessionHandler.removeMessages(106);
        return this.mObexSession != null && this.mObexSession.setAuthReply(authReply);
    }
    
    public void start() {
        Log.d("BTPbapSession", "start");
        this.startRfcomm();
    }
    
    public void stop() {
        Log.d("BTPbapSession", "Stop");
        this.stopObexSession();
        this.stopRfcomm();
        if (this.mHandlerThread.isAlive()) {
            Log.d("BTPbapSession", "Stopped handler thread:" + this.mHandlerThread.quit() + " id[" + System.identityHashCode(this.mHandlerThread) + "]");
        }
        else {
            Log.d("BTPbapSession", "handler thread not running id[" + System.identityHashCode(this.mHandlerThread) + "]");
        }
    }
    
    private class RfcommConnectThread extends Thread
    {
        private static final String TAG = "RfcommConnectThread";
        private BluetoothSocket mSocket;
        
        public RfcommConnectThread() {
            super("RfcommConnectThread");
        }
        
        private void closeSocket() {
            IOUtils.closeStream((Closeable)this.mSocket);
        }
        
        @Override
        public void run() {
            if (BluetoothPbapSession.this.mAdapter.isDiscovering()) {
                Log.w("RfcommConnectThread", "pbap device currently discovering, might be slow to connect");
            }
            try {
                (this.mSocket = BluetoothPbapSession.this.mDevice.createRfcommSocketToServiceRecord(UUID.fromString("0000112f-0000-1000-8000-00805f9b34fb"))).connect();
                BluetoothPbapSession.this.mSessionHandler.obtainMessage(1, new BluetoothPbapObexTransport(this.mSocket)).sendToTarget();
            }
            catch (Throwable t) {
                this.closeSocket();
                BluetoothPbapSession.this.mSessionHandler.obtainMessage(2).sendToTarget();
            }
        }
    }
}
