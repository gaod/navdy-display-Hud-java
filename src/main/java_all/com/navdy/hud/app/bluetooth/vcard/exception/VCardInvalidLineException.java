package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardInvalidLineException extends VCardException
{
    public VCardInvalidLineException() {
    }
    
    public VCardInvalidLineException(final String s) {
        super(s);
    }
}
