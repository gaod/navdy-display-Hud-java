package com.navdy.hud.app.bluetooth.pbap;

import com.navdy.service.library.events.DeviceInfo;
import android.os.SystemClock;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.service.ConnectionHandler;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.framework.contacts.FavoriteContactsManager;
import com.navdy.hud.app.framework.recentcall.RecentCallManager;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import java.util.Iterator;
import java.util.Collection;
import com.navdy.hud.app.framework.contacts.NumberType;
import com.navdy.hud.app.framework.recentcall.RecentCall;
import com.navdy.hud.app.bluetooth.vcard.VCardEntry;
import com.navdy.hud.app.HudApplication;
import android.bluetooth.BluetoothManager;
import java.util.ArrayList;
import android.os.Message;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.os.HandlerThread;
import android.os.Handler;
import android.os.Handler;
import android.bluetooth.BluetoothAdapter;
import com.navdy.service.library.log.Logger;

public class PBAPClientManager
{
    private static final int CHECK_PBAP_CONNECTION_HANG_INTERVAL = 60000;
    private static final int CHECK_PBAP_PULL_PHONEBOOK_HANG_INTERVAL = 45000;
    private static final int INITIAL_CONNECTION_DELAY = 30000;
    public static final int MAX_RECENT_CALLS = 30;
    private static final int TYPE_HOME = 1;
    private static final int TYPE_MOBILE = 2;
    private static final int TYPE_WORK = 3;
    private static final int TYPE_WORK_MOBILE = 17;
    private static final Logger sLogger;
    private static final PBAPClientManager singleton;
    private BluetoothAdapter bluetoothAdapter;
    private Runnable checkPbapConnectionHang;
    private Handler handler;
    private Handler$Callback handlerCallback;
    private HandlerThread handlerThread;
    private long lastSyncAttemptTime;
    private BluetoothPbapClient pbapClient;
    private RemoteDeviceManager remoteDeviceManager;
    private Runnable stopPbapClientRunnable;
    private Runnable syncRunnable;
    
    static {
        sLogger = new Logger(PBAPClientManager.class);
        singleton = new PBAPClientManager();
    }
    
    public PBAPClientManager() {
        this.handlerCallback = (Handler$Callback)new Handler$Callback() {
            public boolean handleMessage(final Message message) {
                while (true) {
                    Label_0452: {
                        Label_0441: {
                            Label_0430: {
                                Label_0419: {
                                    Label_0408: {
                                        Label_0397: {
                                            Label_0362: {
                                                Label_0327: {
                                                    Label_0285: {
                                                        Label_0274: {
                                                            Label_0263: {
                                                                Label_0252: {
                                                                    Label_0241: {
                                                                        Label_0230: {
                                                                            Label_0219: {
                                                                                try {
                                                                                    switch (message.what) {
                                                                                        case 201:
                                                                                            PBAPClientManager.sLogger.i("pbap client connected, pulling phonebook [telecom/cch.vcf]");
                                                                                            PBAPClientManager.this.handler.removeCallbacks(PBAPClientManager.this.checkPbapConnectionHang);
                                                                                            PBAPClientManager.this.pbapClient.pullPhoneBook("telecom/cch.vcf");
                                                                                            PBAPClientManager.this.handler.postDelayed(PBAPClientManager.this.checkPbapConnectionHang, 45000L);
                                                                                            break;
                                                                                        case 202:
                                                                                            break Label_0219;
                                                                                        case 204:
                                                                                            break Label_0230;
                                                                                        case 203:
                                                                                            break Label_0241;
                                                                                        case 1:
                                                                                            break Label_0252;
                                                                                        case 4:
                                                                                            break Label_0263;
                                                                                        case 3:
                                                                                            break Label_0274;
                                                                                        case 2:
                                                                                            break Label_0285;
                                                                                        case 6:
                                                                                            break Label_0327;
                                                                                        case 5:
                                                                                            break Label_0362;
                                                                                        case 105:
                                                                                            break Label_0397;
                                                                                        case 106:
                                                                                            break Label_0408;
                                                                                        case 102:
                                                                                            break Label_0419;
                                                                                        case 103:
                                                                                            break Label_0430;
                                                                                        case 104:
                                                                                            break Label_0441;
                                                                                        case 101:
                                                                                            break Label_0452;
                                                                                    }
                                                                                    return false;
                                                                                }
                                                                                catch (Throwable t) {
                                                                                    PBAPClientManager.sLogger.e(t);
                                                                                    return false;
                                                                                }
                                                                            }
                                                                            PBAPClientManager.sLogger.i("pbap client dis-connected");
                                                                            return false;
                                                                        }
                                                                        PBAPClientManager.sLogger.w("pbap client auth-timeout");
                                                                        return false;
                                                                    }
                                                                    PBAPClientManager.sLogger.w("pbap client auth-requested");
                                                                    return false;
                                                                }
                                                                PBAPClientManager.sLogger.v("pbap client set phone book done");
                                                                return false;
                                                            }
                                                            PBAPClientManager.sLogger.v("pbap client pull vcard entry done");
                                                            return false;
                                                        }
                                                        PBAPClientManager.sLogger.v("pbap client vcard listing done");
                                                        return false;
                                                    }
                                                    PBAPClientManager.sLogger.v("pbap client pull phone book done");
                                                    PBAPClientManager.this.handler.removeCallbacks(PBAPClientManager.this.checkPbapConnectionHang);
                                                    PBAPClientManager.this.buildRecentCallList((ArrayList)message.obj);
                                                    return false;
                                                }
                                                PBAPClientManager.sLogger.v("pbap client vcard listing size done:" + message.arg1);
                                                return false;
                                            }
                                            PBAPClientManager.sLogger.v("pbap client phone book size done:" + message.arg1);
                                            return false;
                                        }
                                        PBAPClientManager.sLogger.e("pbap client phone book size error");
                                        return false;
                                    }
                                    PBAPClientManager.sLogger.e("pbap client vcard listing size error");
                                    return false;
                                }
                                PBAPClientManager.sLogger.e("pbap client pull phone book error");
                                return false;
                            }
                            PBAPClientManager.sLogger.e("pbap client vcard listing error");
                            return false;
                        }
                        PBAPClientManager.sLogger.e("pbap client vcard entry error");
                        return false;
                    }
                    PBAPClientManager.sLogger.e("pbap client set phone book error");
                    return false;
                }
            }
        };
        this.checkPbapConnectionHang = new Runnable() {
            @Override
            public void run() {
                try {
                    PBAPClientManager.sLogger.v("pbap hang detected, reattempting...");
                    PBAPClientManager.this.stopPbapClient();
                    PBAPClientManager.this.handler.removeCallbacks(PBAPClientManager.this.syncRunnable);
                    PBAPClientManager.this.handler.post(PBAPClientManager.this.syncRunnable);
                }
                catch (Throwable t) {
                    PBAPClientManager.sLogger.e(t);
                }
            }
        };
        this.syncRunnable = new Runnable() {
            @Override
            public void run() {
                PBAPClientManager.this.syncRecentCalls();
            }
        };
        this.stopPbapClientRunnable = new Runnable() {
            @Override
            public void run() {
                PBAPClientManager.this.stopPbapClient();
            }
        };
        this.remoteDeviceManager = RemoteDeviceManager.getInstance();
        this.remoteDeviceManager.getBus().register(this);
        (this.handlerThread = new HandlerThread("HudPBAPClientManager")).start();
        this.handler = new Handler(this.handlerThread.getLooper(), this.handlerCallback);
        final BluetoothManager bluetoothManager = (BluetoothManager)HudApplication.getAppContext().getSystemService("bluetooth");
        if (bluetoothManager != null) {
            this.bluetoothAdapter = bluetoothManager.getAdapter();
        }
        this.handler.removeCallbacks(this.syncRunnable);
        this.handler.postDelayed(this.syncRunnable, 30000L);
    }
    
    private void buildRecentCallList(final ArrayList<VCardEntry> p0) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: astore_2       
        //     4: new             Ljava/lang/StringBuilder;
        //     7: astore_3       
        //     8: aload_3        
        //     9: invokespecial   java/lang/StringBuilder.<init>:()V
        //    12: aload_2        
        //    13: aload_3        
        //    14: ldc             "recent calls vcard-list size["
        //    16: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    19: aload_1        
        //    20: invokevirtual   java/util/ArrayList.size:()I
        //    23: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    26: ldc             "]"
        //    28: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    31: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    34: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //    37: invokestatic    com/navdy/hud/app/util/DeviceUtil.isUserBuild:()Z
        //    40: ifne            262
        //    43: iconst_1       
        //    44: istore          4
        //    46: new             Ljava/util/LinkedHashMap;
        //    49: astore          5
        //    51: aload           5
        //    53: invokespecial   java/util/LinkedHashMap.<init>:()V
        //    56: invokestatic    com/google/i18n/phonenumbers/PhoneNumberUtil.getInstance:()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;
        //    59: astore          6
        //    61: invokestatic    com/navdy/hud/app/framework/DriverProfileHelper.getInstance:()Lcom/navdy/hud/app/framework/DriverProfileHelper;
        //    64: invokevirtual   com/navdy/hud/app/framework/DriverProfileHelper.getCurrentLocale:()Ljava/util/Locale;
        //    67: invokevirtual   java/util/Locale.getCountry:()Ljava/lang/String;
        //    70: astore          7
        //    72: aload_1        
        //    73: invokevirtual   java/util/ArrayList.iterator:()Ljava/util/Iterator;
        //    76: astore          8
        //    78: aload           8
        //    80: invokeinterface java/util/Iterator.hasNext:()Z
        //    85: ifeq            545
        //    88: aload           8
        //    90: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //    95: checkcast       Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;
        //    98: astore          9
        //   100: iload           4
        //   102: ifeq            135
        //   105: getstatic       com/navdy/hud/app/bluetooth/pbap/PBAPClientManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   108: astore_1       
        //   109: new             Ljava/lang/StringBuilder;
        //   112: astore_3       
        //   113: aload_3        
        //   114: invokespecial   java/lang/StringBuilder.<init>:()V
        //   117: aload_1        
        //   118: aload_3        
        //   119: ldc             "vcard entry:"
        //   121: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   124: aload           9
        //   126: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   129: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   132: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   135: aload           9
        //   137: invokevirtual   com/navdy/hud/app/bluetooth/vcard/VCardEntry.getPhoneList:()Ljava/util/List;
        //   140: astore_2       
        //   141: aconst_null    
        //   142: astore_3       
        //   143: iconst_0       
        //   144: istore          10
        //   146: aload_3        
        //   147: astore_1       
        //   148: iload           10
        //   150: istore          11
        //   152: aload_2        
        //   153: ifnull          224
        //   156: aload_3        
        //   157: astore_1       
        //   158: iload           10
        //   160: istore          11
        //   162: aload_2        
        //   163: invokeinterface java/util/List.size:()I
        //   168: ifle            224
        //   171: aload_2        
        //   172: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //   177: astore          12
        //   179: aload_3        
        //   180: astore_1       
        //   181: iload           10
        //   183: istore          11
        //   185: aload           12
        //   187: invokeinterface java/util/Iterator.hasNext:()Z
        //   192: ifeq            224
        //   195: aload           12
        //   197: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   202: checkcast       Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;
        //   205: astore_2       
        //   206: aload_2        
        //   207: invokevirtual   com/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData.getNumber:()Ljava/lang/String;
        //   210: astore_1       
        //   211: aload_1        
        //   212: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   215: ifne            179
        //   218: aload_2        
        //   219: invokevirtual   com/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData.getType:()I
        //   222: istore          11
        //   224: aload_1        
        //   225: ifnonnull       268
        //   228: getstatic       com/navdy/hud/app/bluetooth/pbap/PBAPClientManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   231: ldc_w           "no phone number in vcard"
        //   234: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   237: goto            78
        //   240: astore_1       
        //   241: getstatic       com/navdy/hud/app/bluetooth/pbap/PBAPClientManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   244: aload_1        
        //   245: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   248: getstatic       com/navdy/hud/app/bluetooth/pbap/PBAPClientManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   251: ldc_w           "buildRecentCallList: stopping client"
        //   254: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   257: aload_0        
        //   258: invokespecial   com/navdy/hud/app/bluetooth/pbap/PBAPClientManager.stopPbapClient:()V
        //   261: return         
        //   262: iconst_0       
        //   263: istore          4
        //   265: goto            46
        //   268: aload           6
        //   270: aload_1        
        //   271: aload           7
        //   273: invokevirtual   com/google/i18n/phonenumbers/PhoneNumberUtil.parse:(Ljava/lang/String;Ljava/lang/String;)Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
        //   276: astore_3       
        //   277: aload_3        
        //   278: invokevirtual   com/google/i18n/phonenumbers/Phonenumber$PhoneNumber.getNationalNumber:()J
        //   281: lstore          13
        //   283: aload           5
        //   285: lload           13
        //   287: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //   290: invokevirtual   java/util/HashMap.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   293: checkcast       Lcom/navdy/hud/app/framework/recentcall/RecentCall;
        //   296: astore_3       
        //   297: aload_3        
        //   298: ifnull          377
        //   301: aload_3        
        //   302: getfield        com/navdy/hud/app/framework/recentcall/RecentCall.name:Ljava/lang/String;
        //   305: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   308: ifne            377
        //   311: iload           4
        //   313: ifeq            78
        //   316: getstatic       com/navdy/hud/app/bluetooth/pbap/PBAPClientManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   319: astore_3       
        //   320: new             Ljava/lang/StringBuilder;
        //   323: astore_1       
        //   324: aload_1        
        //   325: invokespecial   java/lang/StringBuilder.<init>:()V
        //   328: aload_3        
        //   329: aload_1        
        //   330: ldc_w           "Ignoring entry with duplicate phone number: "
        //   333: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   336: lload           13
        //   338: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   341: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   344: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   347: goto            78
        //   350: astore_1       
        //   351: getstatic       com/navdy/hud/app/bluetooth/pbap/PBAPClientManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   354: ldc_w           "buildRecentCallList: stopping client"
        //   357: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   360: aload_0        
        //   361: invokespecial   com/navdy/hud/app/bluetooth/pbap/PBAPClientManager.stopPbapClient:()V
        //   364: aload_1        
        //   365: athrow         
        //   366: astore_1       
        //   367: getstatic       com/navdy/hud/app/bluetooth/pbap/PBAPClientManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   370: aload_1        
        //   371: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   374: goto            78
        //   377: aload           9
        //   379: invokevirtual   com/navdy/hud/app/bluetooth/vcard/VCardEntry.getDisplayName:()Ljava/lang/String;
        //   382: astore_2       
        //   383: aload_2        
        //   384: astore_3       
        //   385: aload_2        
        //   386: aload_1        
        //   387: lload           13
        //   389: invokestatic    com/navdy/hud/app/framework/contacts/ContactUtil.isDisplayNameValid:(Ljava/lang/String;Ljava/lang/String;J)Z
        //   392: ifne            397
        //   395: aconst_null    
        //   396: astore_3       
        //   397: aload           9
        //   399: invokevirtual   com/navdy/hud/app/bluetooth/vcard/VCardEntry.getCallType:()I
        //   402: istore          10
        //   404: aload           9
        //   406: invokevirtual   com/navdy/hud/app/bluetooth/vcard/VCardEntry.getCallTime:()Ljava/util/Date;
        //   409: astore          9
        //   411: aload           9
        //   413: ifnonnull       449
        //   416: getstatic       com/navdy/hud/app/bluetooth/pbap/PBAPClientManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   419: astore_3       
        //   420: new             Ljava/lang/StringBuilder;
        //   423: astore_2       
        //   424: aload_2        
        //   425: invokespecial   java/lang/StringBuilder.<init>:()V
        //   428: aload_3        
        //   429: aload_2        
        //   430: ldc_w           "ignoring spam number:"
        //   433: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   436: aload_1        
        //   437: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   440: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   443: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   446: goto            78
        //   449: new             Lcom/navdy/hud/app/framework/recentcall/RecentCall;
        //   452: astore_2       
        //   453: aload_2        
        //   454: aload_3        
        //   455: getstatic       com/navdy/hud/app/framework/recentcall/RecentCall$Category.PHONE_CALL:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;
        //   458: aload_1        
        //   459: aload_0        
        //   460: iload           11
        //   462: invokespecial   com/navdy/hud/app/bluetooth/pbap/PBAPClientManager.getNumberType:(I)Lcom/navdy/hud/app/framework/contacts/NumberType;
        //   465: aload           9
        //   467: aload_0        
        //   468: iload           10
        //   470: invokespecial   com/navdy/hud/app/bluetooth/pbap/PBAPClientManager.getCallType:(I)Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;
        //   473: iconst_m1      
        //   474: lload           13
        //   476: invokespecial   com/navdy/hud/app/framework/recentcall/RecentCall.<init>:(Ljava/lang/String;Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/NumberType;Ljava/util/Date;Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;IJ)V
        //   479: iload           4
        //   481: ifeq            514
        //   484: getstatic       com/navdy/hud/app/bluetooth/pbap/PBAPClientManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   487: astore_1       
        //   488: new             Ljava/lang/StringBuilder;
        //   491: astore_3       
        //   492: aload_3        
        //   493: invokespecial   java/lang/StringBuilder.<init>:()V
        //   496: aload_1        
        //   497: aload_3        
        //   498: ldc_w           "adding recent call:"
        //   501: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   504: aload_2        
        //   505: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   508: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   511: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   514: aload           5
        //   516: lload           13
        //   518: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //   521: aload_2        
        //   522: invokevirtual   java/util/HashMap.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   525: pop            
        //   526: aload           5
        //   528: invokevirtual   java/util/HashMap.size:()I
        //   531: bipush          30
        //   533: if_icmpne       78
        //   536: getstatic       com/navdy/hud/app/bluetooth/pbap/PBAPClientManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   539: ldc_w           "exceeded max size"
        //   542: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   545: getstatic       com/navdy/hud/app/bluetooth/pbap/PBAPClientManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   548: astore_3       
        //   549: new             Ljava/lang/StringBuilder;
        //   552: astore_1       
        //   553: aload_1        
        //   554: invokespecial   java/lang/StringBuilder.<init>:()V
        //   557: aload_3        
        //   558: aload_1        
        //   559: ldc_w           "recent calls map size["
        //   562: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   565: aload           5
        //   567: invokevirtual   java/util/HashMap.size:()I
        //   570: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   573: ldc             "]"
        //   575: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   578: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   581: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   584: invokestatic    com/navdy/hud/app/framework/DriverProfileHelper.getInstance:()Lcom/navdy/hud/app/framework/DriverProfileHelper;
        //   587: invokevirtual   com/navdy/hud/app/framework/DriverProfileHelper.getCurrentProfile:()Lcom/navdy/hud/app/profile/DriverProfile;
        //   590: astore_3       
        //   591: new             Ljava/util/ArrayList;
        //   594: astore_1       
        //   595: aload_1        
        //   596: invokespecial   java/util/ArrayList.<init>:()V
        //   599: aload           5
        //   601: invokevirtual   java/util/HashMap.values:()Ljava/util/Collection;
        //   604: invokeinterface java/util/Collection.iterator:()Ljava/util/Iterator;
        //   609: astore_2       
        //   610: aload_2        
        //   611: invokeinterface java/util/Iterator.hasNext:()Z
        //   616: ifeq            638
        //   619: aload_1        
        //   620: aload_2        
        //   621: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   626: checkcast       Lcom/navdy/hud/app/framework/recentcall/RecentCall;
        //   629: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   634: pop            
        //   635: goto            610
        //   638: aload_3        
        //   639: invokevirtual   com/navdy/hud/app/profile/DriverProfile.getProfileName:()Ljava/lang/String;
        //   642: aload_1        
        //   643: iconst_1       
        //   644: invokestatic    com/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper.storeRecentCalls:(Ljava/lang/String;Ljava/util/List;Z)V
        //   647: getstatic       com/navdy/hud/app/bluetooth/pbap/PBAPClientManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   650: ldc_w           "buildRecentCallList: stopping client"
        //   653: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   656: aload_0        
        //   657: invokespecial   com/navdy/hud/app/bluetooth/pbap/PBAPClientManager.stopPbapClient:()V
        //   660: goto            261
        //    Signature:
        //  (Ljava/util/ArrayList<Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;>;)V
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  0      43     240    261    Ljava/lang/Throwable;
        //  0      43     350    366    Any
        //  46     78     240    261    Ljava/lang/Throwable;
        //  46     78     350    366    Any
        //  78     100    240    261    Ljava/lang/Throwable;
        //  78     100    350    366    Any
        //  105    135    240    261    Ljava/lang/Throwable;
        //  105    135    350    366    Any
        //  135    141    240    261    Ljava/lang/Throwable;
        //  135    141    350    366    Any
        //  162    179    240    261    Ljava/lang/Throwable;
        //  162    179    350    366    Any
        //  185    218    240    261    Ljava/lang/Throwable;
        //  185    218    350    366    Any
        //  218    224    240    261    Ljava/lang/Throwable;
        //  218    224    350    366    Any
        //  228    237    240    261    Ljava/lang/Throwable;
        //  228    237    350    366    Any
        //  241    248    350    366    Any
        //  268    277    366    377    Ljava/lang/Throwable;
        //  268    277    350    366    Any
        //  277    297    240    261    Ljava/lang/Throwable;
        //  277    297    350    366    Any
        //  301    311    240    261    Ljava/lang/Throwable;
        //  301    311    350    366    Any
        //  316    347    240    261    Ljava/lang/Throwable;
        //  316    347    350    366    Any
        //  367    374    240    261    Ljava/lang/Throwable;
        //  367    374    350    366    Any
        //  377    383    240    261    Ljava/lang/Throwable;
        //  377    383    350    366    Any
        //  385    395    240    261    Ljava/lang/Throwable;
        //  385    395    350    366    Any
        //  397    411    240    261    Ljava/lang/Throwable;
        //  397    411    350    366    Any
        //  416    446    240    261    Ljava/lang/Throwable;
        //  416    446    350    366    Any
        //  449    479    240    261    Ljava/lang/Throwable;
        //  449    479    350    366    Any
        //  484    514    240    261    Ljava/lang/Throwable;
        //  484    514    350    366    Any
        //  514    545    240    261    Ljava/lang/Throwable;
        //  514    545    350    366    Any
        //  545    610    240    261    Ljava/lang/Throwable;
        //  545    610    350    366    Any
        //  610    635    240    261    Ljava/lang/Throwable;
        //  610    635    350    366    Any
        //  638    647    240    261    Ljava/lang/Throwable;
        //  638    647    350    366    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0268:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private RecentCall.CallType getCallType(final int n) {
        RecentCall.CallType callType = null;
        switch (n) {
            default:
                callType = RecentCall.CallType.UNNKNOWN;
                break;
            case 10:
                callType = RecentCall.CallType.INCOMING;
                break;
            case 12:
                callType = RecentCall.CallType.MISSED;
                break;
            case 11:
                callType = RecentCall.CallType.OUTGOING;
                break;
        }
        return callType;
    }
    
    public static PBAPClientManager getInstance() {
        return PBAPClientManager.singleton;
    }
    
    private NumberType getNumberType(final int n) {
        NumberType numberType = null;
        switch (n) {
            default:
                numberType = NumberType.OTHER;
                break;
            case 1:
                numberType = NumberType.HOME;
                break;
            case 3:
                numberType = NumberType.WORK;
                break;
            case 2:
                numberType = NumberType.MOBILE;
                break;
            case 17:
                numberType = NumberType.WORK_MOBILE;
                break;
        }
        return numberType;
    }
    
    private void printCallMap(final Collection<RecentCall> collection) {
        for (final RecentCall recentCall : collection) {
            PBAPClientManager.sLogger.v("[" + recentCall.category + "] " + "[" + recentCall.number + "]" + "[" + recentCall.numberType + "]" + "[" + recentCall.name + "]" + "[" + recentCall.callTime + "]" + "[" + recentCall.callType + "]");
        }
    }
    
    private void startPbapClient(final String s) {
        try {
            if (this.pbapClient == null) {
                PBAPClientManager.sLogger.v("startPbapClient");
                (this.pbapClient = new BluetoothPbapClient(this.bluetoothAdapter.getRemoteDevice(s), this.handler)).connect();
                PBAPClientManager.sLogger.v("started pbap client");
            }
        }
        catch (Throwable t) {
            PBAPClientManager.sLogger.e("could not start pbap client", t);
        }
    }
    
    private void stopPbapClient() {
        try {
            if (this.pbapClient != null) {
                PBAPClientManager.sLogger.v("stopPbapClient");
                this.pbapClient.disconnect();
                PBAPClientManager.sLogger.v("stopped pbap client");
                this.pbapClient = null;
                this.lastSyncAttemptTime = 0L;
                this.handler.removeCallbacks(this.checkPbapConnectionHang);
            }
        }
        catch (Throwable t) {
            PBAPClientManager.sLogger.e("error stopping pbap client", t);
            this.pbapClient = null;
            this.lastSyncAttemptTime = 0L;
            this.handler.removeCallbacks(this.checkPbapConnectionHang);
        }
        finally {
            this.pbapClient = null;
            this.lastSyncAttemptTime = 0L;
            this.handler.removeCallbacks(this.checkPbapConnectionHang);
        }
    }
    
    @Subscribe
    public void onConnectionStatusChange(final ConnectionStateChange connectionStateChange) {
        switch (connectionStateChange.state) {
            case CONNECTION_LINK_LOST:
                this.handler.post(this.stopPbapClientRunnable);
                RecentCallManager.getInstance().clearRecentCalls();
                FavoriteContactsManager.getInstance().clearFavoriteContacts();
                break;
        }
    }
    
    @Subscribe
    public void onDeviceSyncRequired(final ConnectionHandler.DeviceSyncEvent deviceSyncEvent) {
        boolean b = true;
        if (deviceSyncEvent.amount != 1) {
            b = false;
        }
        if (b) {
            this.handler.removeCallbacks(this.syncRunnable);
            PBAPClientManager.sLogger.v("trigger PBAP download");
            this.handler.post(this.syncRunnable);
        }
        FavoriteContactsManager.getInstance().syncFavoriteContacts(b);
    }
    
    @Subscribe
    public void onDriverProfileChanged(final DriverProfileChanged driverProfileChanged) {
        final RecentCallManager instance = RecentCallManager.getInstance();
        instance.clearContactLookupMap();
        instance.buildRecentCalls();
        FavoriteContactsManager.getInstance().buildFavoriteContacts();
    }
    
    public void syncRecentCalls() {
        while (true) {
            DeviceInfo remoteDeviceInfo = null;
            Label_0081: {
                try {
                    PBAPClientManager.sLogger.v("syncRecentCalls");
                    if (this.lastSyncAttemptTime > 0L) {
                        PBAPClientManager.sLogger.v("syncRecentCalls in progress");
                    }
                    else {
                        this.handler.removeCallbacks(this.checkPbapConnectionHang);
                        remoteDeviceInfo = this.remoteDeviceManager.getRemoteDeviceInfo();
                        if (remoteDeviceInfo != null) {
                            break Label_0081;
                        }
                        PBAPClientManager.sLogger.v("no remote device, bailing out");
                    }
                    return;
                }
                catch (Throwable t) {
                    PBAPClientManager.sLogger.e("syncRecentCalls", t);
                    this.stopPbapClient();
                    return;
                }
            }
            final String bluetoothAddress = new NavdyDeviceId(remoteDeviceInfo.deviceId).getBluetoothAddress();
            if (bluetoothAddress == null) {
                PBAPClientManager.sLogger.i("BT Address n/a deviceId:" + remoteDeviceInfo.deviceId);
                return;
            }
            this.stopPbapClient();
            this.lastSyncAttemptTime = SystemClock.elapsedRealtime();
            PBAPClientManager.sLogger.v("syncRecentCalls-attempting to connect");
            this.startPbapClient(bluetoothAddress);
            this.handler.postDelayed(this.checkPbapConnectionHang, 60000L);
        }
    }
}
