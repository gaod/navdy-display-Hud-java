package com.navdy.hud.app.ui.component;

import com.navdy.hud.app.util.os.SystemProperties;

public class UISettings
{
    private static final String ADVANCED_GPS_STATS = "persist.sys.gps.stats";
    private static final String DIAL_LONG_PRESS_ACTION_PLACE_SEARCH = "persist.sys.dlpress_search";
    private static final boolean dialLongPressPlaceSearchAction;
    
    static {
        dialLongPressPlaceSearchAction = SystemProperties.getBoolean("persist.sys.dlpress_search", false);
    }
    
    public static boolean advancedGpsStatsEnabled() {
        return SystemProperties.getBoolean("persist.sys.gps.stats", false);
    }
    
    public static boolean isLongPressActionPlaceSearch() {
        return UISettings.dialLongPressPlaceSearchAction;
    }
    
    public static boolean isMusicBrowsingEnabled() {
        return true;
    }
    
    public static boolean isVerticalListNoCloseTimeout() {
        return true;
    }
    
    public static boolean supportsIosSms() {
        return true;
    }
}
