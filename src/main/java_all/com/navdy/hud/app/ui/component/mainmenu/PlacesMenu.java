package com.navdy.hud.app.ui.component.mainmenu;

import android.graphics.Shader;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.ui.component.vlist.VerticalModelCache;
import android.view.View;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.hud.app.ui.component.destination.DestinationConfirmationHelper;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.maps.MapsNotification;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import java.util.Iterator;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.hud.app.framework.destinations.DestinationsManager;
import com.navdy.hud.app.util.RemoteCapabilitiesUtil;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import java.util.HashMap;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder;
import com.navdy.hud.app.framework.destinations.Destination;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.HudApplication;
import java.util.ArrayList;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import java.util.HashSet;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import android.content.res.Resources;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import java.util.List;

public class PlacesMenu implements IMenu
{
    public static final int CANCEL_POSITION = 1;
    private static List<String> CONFIRMATION_CHOICES;
    public static final int NAVIGATE_POSITION = 0;
    private static final int SELECTION_ANIMATION_DELAY = 1000;
    private static final VerticalList.Model back;
    private static final int backColor;
    public static final int bkColorUnselected;
    private static final int contactColor;
    private static final int favColor;
    private static final int gasColor;
    private static final int homeColor;
    public static final String places;
    private static final int placesColor;
    private static final int recentColor;
    private static final VerticalList.Model recentPlaces;
    private static final Resources resources;
    private static final Logger sLogger;
    private static final VerticalList.Model search;
    public static final String suggested;
    public static final int suggestedColor;
    private static final int workColor;
    private int backSelection;
    private int backSelectionId;
    private Bus bus;
    private List<VerticalList.Model> cachedList;
    private HashSet<String> latlngSet;
    private NearbyPlacesMenu nearbyPlacesMenu;
    private IMenu parent;
    private MainMenuScreen2.Presenter presenter;
    private RecentPlacesMenu recentPlacesMenu;
    private List<VerticalList.Model> returnToCacheList;
    private VerticalMenuComponent vscrollComponent;
    
    static {
        sLogger = new Logger(PlacesMenu.class);
        PlacesMenu.CONFIRMATION_CHOICES = new ArrayList<String>();
        resources = HudApplication.getAppContext().getResources();
        bkColorUnselected = PlacesMenu.resources.getColor(R.color.icon_bk_color_unselected);
        recentColor = PlacesMenu.resources.getColor(R.color.mm_recent_places);
        favColor = PlacesMenu.resources.getColor(R.color.mm_place_favorite);
        homeColor = PlacesMenu.resources.getColor(R.color.mm_place_home);
        workColor = PlacesMenu.resources.getColor(R.color.mm_place_work);
        contactColor = PlacesMenu.resources.getColor(R.color.mm_place_contact);
        backColor = PlacesMenu.resources.getColor(R.color.mm_back);
        suggestedColor = PlacesMenu.resources.getColor(R.color.mm_place_suggested);
        placesColor = PlacesMenu.resources.getColor(R.color.mm_places);
        gasColor = PlacesMenu.resources.getColor(R.color.mm_place_gas);
        suggested = PlacesMenu.resources.getString(R.string.suggested);
        places = PlacesMenu.resources.getString(R.string.carousel_menu_map_places);
        final String string = PlacesMenu.resources.getString(R.string.back);
        final int backColor2 = PlacesMenu.backColor;
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, backColor2, MainMenu.bkColorUnselected, backColor2, string, null);
        final String string2 = PlacesMenu.resources.getString(R.string.carousel_menu_recent_place);
        final int color = PlacesMenu.resources.getColor(R.color.mm_recent_places);
        recentPlaces = IconBkColorViewHolder.buildModel(R.id.places_menu_recent, R.drawable.icon_place_recent_2, color, MainMenu.bkColorUnselected, color, string2, null);
        final String string3 = PlacesMenu.resources.getString(R.string.carousel_menu_search_title);
        final int color2 = PlacesMenu.resources.getColor(R.color.mm_search);
        search = IconBkColorViewHolder.buildModel(R.id.main_menu_search, R.drawable.icon_mm_search_2, color2, PlacesMenu.bkColorUnselected, color2, string3, null);
        PlacesMenu.CONFIRMATION_CHOICES.add(PlacesMenu.resources.getString(R.string.destination_start_navigation_yes));
        PlacesMenu.CONFIRMATION_CHOICES.add(PlacesMenu.resources.getString(R.string.destination_start_navigation_cancel));
    }
    
    public PlacesMenu(final Bus bus, final VerticalMenuComponent vscrollComponent, final MainMenuScreen2.Presenter presenter, final IMenu parent) {
        this.latlngSet = new HashSet<String>();
        this.bus = bus;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
    }
    
    public static VerticalList.Model buildModel(final Destination destination, final int n) {
        final String destinationTitle = destination.destinationTitle;
        String s;
        if (destination.recentTimeLabel != null) {
            s = destination.recentTimeLabel;
        }
        else {
            s = destination.destinationSubtitle;
        }
        return getPlaceModel(destination, n, destinationTitle, s);
    }
    
    public static VerticalList.Model getPlaceModel(final Destination destination, final int n, final String s, final String s2) {
        VerticalList.Model model = null;
        Label_0022: {
            if (destination == null) {
                model = IconBkColorViewHolder.buildModel(n, R.drawable.icon_mm_places_2, PlacesMenu.placesColor, MainMenu.bkColorUnselected, PlacesMenu.placesColor, s, s2);
            }
            else {
                switch (destination.favoriteDestinationType) {
                    default:
                        if (destination.placeCategory == Destination.PlaceCategory.SUGGESTED_RECENT) {
                            model = IconBkColorViewHolder.buildModel(n, R.drawable.icon_place_recent_2, PlacesMenu.recentColor, MainMenu.bkColorUnselected, PlacesMenu.recentColor, s, s2);
                            break;
                        }
                        switch (destination.favoriteDestinationType) {
                            default:
                                if (destination.destinationType == Destination.DestinationType.FIND_GAS) {
                                    model = IconBkColorViewHolder.buildModel(n, R.drawable.icon_places_gas_2, PlacesMenu.gasColor, MainMenu.bkColorUnselected, PlacesMenu.gasColor, s, s2);
                                    break Label_0022;
                                }
                                if (!destination.recommendation && destination.placeCategory == Destination.PlaceCategory.RECENT) {
                                    model = IconBkColorViewHolder.buildModel(n, R.drawable.icon_place_recent_2, PlacesMenu.recentColor, MainMenu.bkColorUnselected, PlacesMenu.recentColor, s, s2);
                                    break Label_0022;
                                }
                                model = IconBkColorViewHolder.buildModel(n, R.drawable.icon_place_favorite_2, PlacesMenu.favColor, MainMenu.bkColorUnselected, PlacesMenu.favColor, s, s2);
                                break Label_0022;
                            case FAVORITE_CUSTOM:
                                model = IconBkColorViewHolder.buildModel(n, R.drawable.icon_place_favorite_2, PlacesMenu.favColor, MainMenu.bkColorUnselected, PlacesMenu.favColor, s, s2);
                                break Label_0022;
                            case FAVORITE_CALENDAR:
                                model = IconsTwoViewHolder.buildModel(n, R.drawable.icon_place_calendar, R.drawable.icon_place_calendar_sm, PlacesMenu.homeColor, -1, s, s2);
                                break Label_0022;
                        }
                        break;
                    case FAVORITE_HOME:
                        model = IconBkColorViewHolder.buildModel(n, R.drawable.icon_place_home_2, PlacesMenu.homeColor, MainMenu.bkColorUnselected, PlacesMenu.homeColor, s, s2);
                        break;
                    case FAVORITE_WORK:
                        model = IconBkColorViewHolder.buildModel(n, R.drawable.icon_place_work_2, PlacesMenu.workColor, MainMenu.bkColorUnselected, PlacesMenu.workColor, s, s2);
                        break;
                    case FAVORITE_CONTACT: {
                        final VerticalList.Model buildModel = IconsTwoViewHolder.buildModel(n, R.drawable.icon_user_bg_1, 0, PlacesMenu.contactColor, MainMenu.bkColorUnselected, s, s2);
                        (buildModel.extras = new HashMap<String, String>()).put("INITIAL", destination.initials);
                        model = buildModel;
                        break;
                    }
                }
            }
        }
        return model;
    }
    
    @Override
    public IMenu getChildMenu(final IMenu menu, final String s, final String s2) {
        return null;
    }
    
    @Override
    public int getInitialSelection() {
        int index;
        if (this.cachedList == null) {
            index = 0;
        }
        else if ((index = this.cachedList.indexOf(PlacesMenu.recentPlaces)) == -1) {
            if (this.cachedList.size() >= 3) {
                index = 2;
            }
            else {
                index = 1;
            }
        }
        return index;
    }
    
    @Override
    public List<VerticalList.Model> getItems() {
        List<VerticalList.Model> cachedList;
        if (this.cachedList != null) {
            cachedList = this.cachedList;
        }
        else {
            cachedList = new ArrayList<VerticalList.Model>();
            this.returnToCacheList = new ArrayList<VerticalList.Model>();
            cachedList.add(PlacesMenu.back);
            final DeviceInfo remoteDeviceInfo = RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
            boolean b2;
            final boolean b = b2 = !DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
            if (b) {
                b2 = b;
                if (remoteDeviceInfo == null) {
                    b2 = false;
                }
            }
            PlacesMenu.sLogger.v("isConnected:" + b2);
            if (b2 && RemoteCapabilitiesUtil.supportsPlaceSearch()) {
                cachedList.add(PlacesMenu.search);
            }
            cachedList.add(PlacesMenu.recentPlaces);
            final int size = cachedList.size();
            int n = 0;
            int n2 = 0;
            final DestinationsManager instance = DestinationsManager.getInstance();
            this.latlngSet.clear();
            final List<Destination> suggestedDestinations = instance.getSuggestedDestinations();
            if (suggestedDestinations != null && suggestedDestinations.size() > 0) {
                PlacesMenu.sLogger.v("suggested destinations:" + suggestedDestinations.size());
                final Iterator<Destination> iterator = suggestedDestinations.iterator();
                while (true) {
                    n = n2;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    final Destination state = iterator.next();
                    final VerticalList.Model buildModel = buildModel(state, n2);
                    buildModel.subTitle = PlacesMenu.suggested;
                    buildModel.state = state;
                    (buildModel.extras = new HashMap<String, String>()).put("SUBTITLE_COLOR", String.valueOf(PlacesMenu.suggestedColor));
                    cachedList.add(buildModel);
                    this.returnToCacheList.add(buildModel);
                    this.latlngSet.add(state.displayPositionLatitude + "," + state.displayPositionLongitude + "," + state.navigationPositionLatitude + "," + state.navigationPositionLongitude);
                    ++n2;
                }
            }
            else {
                PlacesMenu.sLogger.v("no suggested destinations");
            }
            final List<Destination> favoriteDestinations = instance.getFavoriteDestinations();
            int n3;
            if (favoriteDestinations != null && favoriteDestinations.size() > 0) {
                PlacesMenu.sLogger.v("favorite destinations:" + favoriteDestinations.size());
                final Iterator<Destination> iterator2 = favoriteDestinations.iterator();
                while (true) {
                    n3 = n;
                    if (!iterator2.hasNext()) {
                        break;
                    }
                    final Destination state2 = iterator2.next();
                    if (this.latlngSet.contains(state2.displayPositionLatitude + "," + state2.displayPositionLongitude + "," + state2.navigationPositionLatitude + "," + state2.navigationPositionLongitude)) {
                        PlacesMenu.sLogger.v("latlng already seen in suggested:" + state2.destinationTitle);
                    }
                    else {
                        final VerticalList.Model buildModel2 = buildModel(state2, n);
                        buildModel2.state = state2;
                        cachedList.add(buildModel2);
                        this.returnToCacheList.add(buildModel2);
                        ++n;
                    }
                }
            }
            else {
                PlacesMenu.sLogger.v("no favorite destinations");
                n3 = n;
            }
            final DestinationsManager.GasDestination gasDestination = DestinationsManager.getInstance().getGasDestination();
            if (gasDestination != null) {
                final VerticalList.Model buildModel3 = buildModel(gasDestination.destination, n3);
                buildModel3.subTitle = PlacesMenu.suggested;
                buildModel3.state = gasDestination.destination;
                (buildModel3.extras = new HashMap<String, String>()).put("SUBTITLE_COLOR", String.valueOf(PlacesMenu.suggestedColor));
                if (gasDestination.showFirst) {
                    cachedList.add(size, buildModel3);
                }
                else {
                    cachedList.add(buildModel3);
                }
                this.returnToCacheList.add(buildModel3);
            }
            this.cachedList = cachedList;
        }
        return cachedList;
    }
    
    @Override
    public VerticalList.Model getModelfromPos(final int n) {
        VerticalList.Model model;
        if (this.cachedList != null && this.cachedList.size() > n) {
            model = this.cachedList.get(n);
        }
        else {
            model = null;
        }
        return model;
    }
    
    @Override
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    @Override
    public Menu getType() {
        return Menu.PLACES;
    }
    
    @Override
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    @Override
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    @Override
    public boolean isItemClickable(final int n, final int n2) {
        return true;
    }
    
    void launchDestination(VerticalList.Model model) {
        if (!HereMapsManager.getInstance().isInitialized()) {
            PlacesMenu.sLogger.w("Here maps engine not initialized, exit");
            this.presenter.performSelectionAnimation(new Runnable() {
                @Override
                public void run() {
                    PlacesMenu.this.presenter.close();
                    MapsNotification.showMapsEngineNotInitializedToast();
                }
            });
        }
        else {
            model = new VerticalList.Model(model);
            final HereNavigationManager instance = HereNavigationManager.getInstance();
            if (!instance.isNavigationModeOn() || instance.hasArrived()) {
                PlacesMenu.sLogger.v("called requestNavigation");
                this.presenter.performSelectionAnimation(new Runnable() {
                    @Override
                    public void run() {
                        PlacesMenu.this.presenter.close(new Runnable() {
                            @Override
                            public void run() {
                                DestinationsManager.getInstance().requestNavigation((Destination)model.state);
                            }
                        });
                    }
                }, 1000);
            }
            else {
                final Destination destination = (Destination)model.state;
                final ConfirmationLayout confirmationLayout = this.presenter.getConfirmationLayout();
                if (confirmationLayout == null) {
                    PlacesMenu.sLogger.v("confirmation layout not found");
                }
                else {
                    DestinationConfirmationHelper.configure(confirmationLayout, model, destination);
                    confirmationLayout.setChoices(PlacesMenu.CONFIRMATION_CHOICES, 0, new ChoiceLayout.IListener() {
                        @Override
                        public void executeItem(final int n, final int n2) {
                            final ConfirmationLayout confirmationLayout = PlacesMenu.this.presenter.getConfirmationLayout();
                            if (confirmationLayout == null) {
                                PlacesMenu.sLogger.v("confirmation layout not found");
                            }
                            else {
                                switch (n) {
                                    case 0:
                                        PlacesMenu.sLogger.v("called requestNavigation");
                                        PlacesMenu.this.presenter.close(new Runnable() {
                                            @Override
                                            public void run() {
                                                DestinationsManager.getInstance().requestNavigation(destination);
                                            }
                                        });
                                        break;
                                    case 1:
                                        confirmationLayout.setVisibility(GONE);
                                        PlacesMenu.this.presenter.reset();
                                        PlacesMenu.this.vscrollComponent.verticalList.unlock();
                                        break;
                                }
                            }
                        }
                        
                        @Override
                        public void itemSelected(final int n, final int n2) {
                        }
                    });
                    confirmationLayout.setVisibility(View.VISIBLE);
                }
            }
        }
    }
    
    @Override
    public void onBindToView(final VerticalList.Model model, final View view, final int n, final VerticalList.ModelState modelState) {
    }
    
    @Override
    public void onFastScrollEnd() {
    }
    
    @Override
    public void onFastScrollStart() {
    }
    
    @Override
    public void onItemSelected(final VerticalList.ItemSelectionState itemSelectionState) {
    }
    
    @Override
    public void onScrollIdle() {
    }
    
    @Override
    public void onUnload(final MenuLevel menuLevel) {
        switch (menuLevel) {
            case CLOSE:
                if (this.returnToCacheList != null) {
                    PlacesMenu.sLogger.v("pm:unload add to cache");
                    VerticalModelCache.addToCache(this.returnToCacheList);
                    this.returnToCacheList = null;
                }
                if (this.recentPlacesMenu != null) {
                    this.recentPlacesMenu.onUnload(MenuLevel.CLOSE);
                    break;
                }
                break;
        }
    }
    
    @Override
    public boolean selectItem(final VerticalList.ItemSelectionState itemSelectionState) {
        PlacesMenu.sLogger.v("select id:" + itemSelectionState.id + " pos:" + itemSelectionState.pos);
        switch (itemSelectionState.id) {
            default:
                this.launchDestination(this.cachedList.get(itemSelectionState.pos));
                break;
            case R.id.menu_back:
                PlacesMenu.sLogger.v("back");
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                this.backSelectionId = 0;
                break;
            case R.id.places_menu_recent:
                PlacesMenu.sLogger.v("recent places");
                AnalyticsSupport.recordMenuSelection("recent_places");
                if (this.recentPlacesMenu == null) {
                    this.recentPlacesMenu = new RecentPlacesMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                this.presenter.loadMenu(this.recentPlacesMenu, MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
                break;
            case R.id.main_menu_search:
                PlacesMenu.sLogger.v("search");
                AnalyticsSupport.recordMenuSelection("search");
                if (this.nearbyPlacesMenu == null) {
                    this.nearbyPlacesMenu = new NearbyPlacesMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                this.presenter.loadMenu(this.nearbyPlacesMenu, MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
                break;
        }
        return true;
    }
    
    @Override
    public void setBackSelectionId(final int backSelectionId) {
        this.backSelectionId = backSelectionId;
    }
    
    @Override
    public void setBackSelectionPos(final int backSelection) {
        this.backSelection = backSelection;
    }
    
    @Override
    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_mm_places_2, PlacesMenu.placesColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText((CharSequence)PlacesMenu.places);
    }
    
    @Override
    public void showToolTip() {
    }
}
