package com.navdy.hud.app.ui.component.carousel;

import android.content.res.TypedArray;
import com.navdy.service.library.events.input.Gesture;
import com.navdy.service.library.events.input.GestureEvent;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import android.animation.ObjectAnimator;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import android.widget.ImageView;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import com.navdy.hud.app.audio.SoundUtils;
import android.content.res.Resources;
import android.view.animation.AnimationUtils;
import com.navdy.hud.app.R;
import java.util.Collection;
import java.util.ArrayList;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.animation.AnimatorSet.Builder;
import android.animation.TimeInterpolator;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.Animator.AnimatorListener;
import android.graphics.Rect;
import android.view.View;
import android.view.MotionEvent;
import com.navdy.hud.app.util.DeviceUtil;
import java.util.LinkedList;
import android.os.Looper;
import android.util.AttributeSet;
import android.content.Context;
import java.util.Queue;
import com.navdy.hud.app.gesture.MultipleClickGestureDetector;
import java.util.List;
import android.view.animation.Interpolator;
import android.view.LayoutInflater;
import android.os.Handler;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.gesture.GestureDetector;
import com.navdy.hud.app.manager.InputManager;
import android.widget.FrameLayout;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.gesture.GestureDetector.GestureListener;

public class CarouselLayout extends FrameLayout implements IInputHandler, GestureListener
{
    static final /* synthetic */ boolean $assertionsDisabled = (!CarouselLayout.class.desiredAssertionStatus());
    private static final int DEFAULT_ANIM_DURATION = 250;
    private static final Logger sLogger;
    private int animationDuration;
    boolean animationRunning;
    private AnimationStrategy animator;
    CarouselAdapter carouselAdapter;
    CarouselIndicator carouselIndicator;
    int currentItem;
    protected GestureDetector detector;
    private boolean exitOnDoubleClick;
    private boolean fastScrollAnimation;
    private Handler handler;
    int imageLytResourceId;
    LayoutInflater inflater;
    int infoLayoutResourceId;
    Interpolator interpolator;
    Carousel.Listener itemChangeListener;
    Operation lastScrollAnimationOperation;
    private int lastTouchX;
    private int lastTouchY;
    View leftView;
    int mainImageSize;
    int mainLeftPadding;
    int mainRightPadding;
    int mainViewDividerPadding;
    View middleLeftView;
    View middleRightView;
    List<Carousel.Model> model;
    private MultipleClickGestureDetector multipleClickGestureDetector;
    View newLeftView;
    View newMiddleLeftView;
    View newMiddleRightView;
    View newRightView;
    private Queue<OperationInfo> operationQueue;
    int rightImageStart;
    int rightSectionHeight;
    int rightSectionWidth;
    View rightView;
    View rootContainer;
    View selectedItemView;
    int sideImageSize;
    private Carousel.ViewCacheManager viewCacheManager;
    int viewPadding;
    Carousel.ViewProcessor viewProcessor;
    boolean viewsScaled;
    
    static {
        sLogger = new Logger(CarouselLayout.class);
    }
    
    public CarouselLayout(final Context context) {
        super(context, (AttributeSet)null);
        this.handler = new Handler(Looper.getMainLooper());
        this.operationQueue = new LinkedList<OperationInfo>();
        this.currentItem = -1;
        this.viewCacheManager = new Carousel.ViewCacheManager(3);
        this.detector = new GestureDetector((GestureDetector.GestureListener)this);
    }
    
    public CarouselLayout(final Context context, final AttributeSet set) {
        super(context, set);
        this.handler = new Handler(Looper.getMainLooper());
        this.operationQueue = new LinkedList<OperationInfo>();
        this.currentItem = -1;
        this.viewCacheManager = new Carousel.ViewCacheManager(3);
        this.detector = new GestureDetector((GestureDetector.GestureListener)this);
        this.inflater = LayoutInflater.from(context);
        this.initFromAttributes(context, set);
        this.setWillNotDraw(false);
        if (!DeviceUtil.isNavdyDevice()) {
            this.setOnTouchListener((View.OnTouchListener)new View.OnTouchListener() {
                public boolean onTouch(final View view, final MotionEvent motionEvent) {
                    if (motionEvent.getAction() == 1) {
                        CarouselLayout.this.lastTouchX = (int)motionEvent.getX();
                        CarouselLayout.this.lastTouchY = (int)motionEvent.getY();
                    }
                    return false;
                }
            });
            this.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                public void onClick(final View view) {
                    final Rect rect = new Rect();
                    final int[] array = new int[2];
                    final int[] array2 = new int[2];
                    CarouselLayout.this.getLocationInWindow(array);
                    final View[] array3 = { CarouselLayout.this.leftView, CarouselLayout.this.middleLeftView, CarouselLayout.this.rightView };
                    int i = 0;
                    while (i < array3.length) {
                        final View view2 = array3[i];
                        ((View)view2.getParent()).getLocationInWindow(array2);
                        final int access$000 = CarouselLayout.this.lastTouchX;
                        final int n = array2[0];
                        final int n2 = array[0];
                        final int access$2 = CarouselLayout.this.lastTouchY;
                        final int n3 = array2[1];
                        final int n4 = array[1];
                        view2.getHitRect(rect);
                        if (rect.contains(access$000 - (n - n2), access$2 - (n3 - n4))) {
                            if (i == 1) {
                                CarouselLayout.this.selectItem();
                                break;
                            }
                            CarouselLayout.this.move(null, i > 1, true, false, CarouselLayout.this.animationDuration);
                            break;
                        }
                        else {
                            ++i;
                        }
                    }
                }
            });
        }
    }
    
    private AnimatorSet buildLayoutAnimation(final Animator.AnimatorListener listener, int n, final int n2) {
        final AnimatorSet set = new AnimatorSet();
        final boolean b = n2 > n;
        AnimationStrategy.Direction direction;
        if (b) {
            direction = AnimationStrategy.Direction.LEFT;
        }
        else {
            direction = AnimationStrategy.Direction.RIGHT;
        }
        n = this.mainViewDividerPadding + this.sideImageSize;
        if (b) {
            this.newMiddleRightView = this.addMiddleRightView(n2, (int)this.rightView.getX() + n, false);
            this.newRightView = this.addHiddenView(n2 + 1, AnimationStrategy.Direction.RIGHT, n2 + 1 < this.model.size());
        }
        else {
            this.newLeftView = this.addHiddenView(n2 - 1, AnimationStrategy.Direction.LEFT, n2 > 0);
            (this.newMiddleRightView = this.addMiddleRightView(n2, (int)this.leftView.getX() + n, false)).setAlpha(0.0f);
        }
        final Builder play = set.play(this.animator.createViewOutAnimation(this, direction));
        play.with((Animator)this.animator.createMiddleLeftViewAnimation(this, direction));
        play.with((Animator)this.animator.createMiddleRightViewAnimation(this, direction));
        play.with((Animator)this.animator.createSideViewToMiddleAnimation(this, direction));
        play.with((Animator)this.animator.createNewMiddleRightViewAnimation(this, direction));
        play.with((Animator)this.animator.createHiddenViewAnimation(this, direction));
        if (this.carouselIndicator != null) {
            final AnimatorSet itemMoveAnimator = this.carouselIndicator.getItemMoveAnimator(n2, -1);
            if (itemMoveAnimator != null) {
                play.with((Animator)itemMoveAnimator);
            }
        }
        set.setInterpolator((TimeInterpolator)this.interpolator);
        set.addListener(this.buildListener(listener, b, this.currentItem, n2));
        return set;
    }
    
    private Animator.AnimatorListener buildListener(final Animator.AnimatorListener animatorListener, final boolean b, final int n, final int n2) {
        return (Animator.AnimatorListener)new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                CarouselLayout.this.currentItem = n2;
                if (animatorListener != null) {
                    animatorListener.onAnimationEnd(animator);
                }
                CarouselLayout.this.changeSelectedItem(b, true);
                if (b) {
                    if (CarouselLayout.this.leftView != null) {
                        CarouselLayout.this.recycleView(Carousel.ViewType.SIDE, CarouselLayout.this.leftView);
                        CarouselLayout.this.leftView = null;
                    }
                    CarouselLayout.this.leftView = CarouselLayout.this.middleLeftView;
                    CarouselLayout.this.middleLeftView = CarouselLayout.this.rightView;
                    CarouselLayout.this.rightView = CarouselLayout.this.newRightView;
                }
                else {
                    if (CarouselLayout.this.rightView != null) {
                        CarouselLayout.this.recycleView(Carousel.ViewType.SIDE, CarouselLayout.this.rightView);
                        CarouselLayout.this.rightView = null;
                    }
                    CarouselLayout.this.rightView = CarouselLayout.this.middleLeftView;
                    final CarouselLayout this$0 = CarouselLayout.this;
                    View middleLeftView;
                    if (CarouselLayout.this.newMiddleLeftView != null) {
                        middleLeftView = CarouselLayout.this.newMiddleLeftView;
                    }
                    else {
                        middleLeftView = CarouselLayout.this.leftView;
                    }
                    this$0.middleLeftView = middleLeftView;
                    CarouselLayout.this.leftView = CarouselLayout.this.newLeftView;
                }
                if (CarouselLayout.this.newMiddleRightView != null) {
                    CarouselLayout.this.recycleView(Carousel.ViewType.MIDDLE_RIGHT, CarouselLayout.this.middleRightView);
                    CarouselLayout.this.middleRightView = CarouselLayout.this.newMiddleRightView;
                    CarouselLayout.this.newMiddleRightView = null;
                }
                CarouselLayout.this.newMiddleLeftView = null;
                CarouselLayout.this.newLeftView = null;
                CarouselLayout.this.newRightView = null;
                if (CarouselLayout.this.leftView == null) {
                    CarouselLayout.this.leftView = CarouselLayout.this.addSideView(n2 - 1, CarouselLayout.this.viewPadding, false);
                }
                if (CarouselLayout.this.rightView == null) {
                    CarouselLayout.this.rightView = CarouselLayout.this.addSideView(n2 + 1, CarouselLayout.this.viewPadding + CarouselLayout.this.sideImageSize + CarouselLayout.this.rightImageStart, false);
                }
                if (CarouselLayout.this.carouselIndicator != null) {
                    CarouselLayout.this.carouselIndicator.setCurrentItem(CarouselLayout.this.currentItem);
                }
                if (CarouselLayout.this.itemChangeListener != null) {
                    CarouselLayout.this.itemChangeListener.onCurrentItemChanged(CarouselLayout.this.currentItem, CarouselLayout.this.model.get(CarouselLayout.this.currentItem).id);
                }
                CarouselLayout.this.runQueuedOperation();
            }
            
            @Override
            public void onAnimationStart(final Animator animator) {
                if (animatorListener != null) {
                    animatorListener.onAnimationStart(animator);
                }
                if (CarouselLayout.this.newMiddleRightView != null) {
                    CarouselLayout.this.newMiddleRightView.setVisibility(View.VISIBLE);
                }
                if (CarouselLayout.this.itemChangeListener != null && n != n2) {
                    CarouselLayout.this.itemChangeListener.onCurrentItemChanging(n, n2, CarouselLayout.this.model.get(n2).id);
                }
            }
        };
    }
    
    private void changeSelectedItem(final boolean b, final boolean b2) {
        if (this.selectedItemView != null) {
            if (b) {
                this.selectedItemView = this.rightView;
            }
            else {
                this.selectedItemView = this.leftView;
            }
        }
    }
    
    private void doInsert(final Animator.AnimatorListener animatorListener, final int n, final Carousel.Model model, final boolean b) {
        this.model.add(n, model);
        final AnimatorSet set = new AnimatorSet();
        final ArrayList<Animator> list = new ArrayList<Animator>();
        final int currentItem = this.currentItem;
        final AnimationStrategy.Direction right = AnimationStrategy.Direction.RIGHT;
        final int viewPadding = this.viewPadding;
        final int viewPadding2 = this.viewPadding;
        final int sideImageSize = this.sideImageSize;
        final int rightImageStart = this.rightImageStart;
        final int viewPadding3 = this.viewPadding;
        final int sideImageSize2 = this.sideImageSize;
        final int mainLeftPadding = this.mainLeftPadding;
        final int viewPadding4 = this.viewPadding;
        final int sideImageSize3 = this.sideImageSize;
        final int mainLeftPadding2 = this.mainLeftPadding;
        final int mainImageSize = this.mainImageSize;
        AnimationStrategy.Direction direction;
        if (n > this.currentItem + 1) {
            direction = right;
        }
        else if (n == this.currentItem + 1) {
            this.newRightView = this.addSideView(n, viewPadding2 + sideImageSize + rightImageStart, false);
            direction = AnimationStrategy.Direction.RIGHT;
            list.add(this.animator.createViewOutAnimation(this, direction));
        }
        else if (n == this.currentItem) {
            if (b) {
                this.newLeftView = this.addSideView(n, viewPadding, false);
                direction = AnimationStrategy.Direction.LEFT;
                list.add(this.animator.createViewOutAnimation(this, direction));
                ++this.currentItem;
            }
            else {
                this.newMiddleLeftView = this.addMiddleLeftView(n, viewPadding3 + sideImageSize2 + mainLeftPadding, false);
                this.newMiddleRightView = this.addMiddleRightView(n, viewPadding4 + sideImageSize3 + mainLeftPadding2 + mainImageSize, false);
                direction = AnimationStrategy.Direction.RIGHT;
                if (this.rightView != null) {
                    list.add(this.animator.createViewOutAnimation(this, direction));
                }
                list.add((Animator)this.animator.createMiddleLeftViewAnimation(this, direction));
                list.add((Animator)this.animator.createMiddleRightViewAnimation(this, direction));
            }
        }
        else {
            direction = right;
            if (n <= this.currentItem - 1) {
                if (!b) {
                    this.move(animatorListener, true, true, false, this.animationDuration);
                    return;
                }
                ++this.currentItem;
                direction = right;
            }
        }
        set.addListener(this.buildListener(animatorListener, direction == AnimationStrategy.Direction.LEFT, currentItem, this.currentItem));
        set.playTogether((Collection)list);
        this.animationRunning = true;
        set.start();
    }
    
    private boolean handleKey(final CustomKeyEvent customKeyEvent) {
        boolean b = true;
        if (this.currentItem != -1) {
            switch (customKeyEvent) {
                default:
                    b = false;
                    break;
                case LEFT:
                    this.move(null, false, true, false, this.animationDuration);
                    break;
                case RIGHT:
                    this.move(null, true, true, false, this.animationDuration);
                    break;
                case SELECT:
                    this.selectItem();
                    break;
            }
        }
        return b;
    }

    private void initFromAttributes(Context context, AttributeSet attrs) {
        boolean z = true;
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.Carousel, 0, 0);
        if ($assertionsDisabled || a != null) {
            Resources resources = getResources();
            try {
                this.animationDuration = a.getInt(0, 250);
                this.interpolator = AnimationUtils.loadInterpolator(getContext(), a.getResourceId(1, 17432580));
                this.viewPadding = (int) a.getDimension(2, resources.getDimension(R.dimen.carousel_side_margin));
                this.sideImageSize = (int) a.getDimension(3, resources.getDimension(R.dimen.carousel_side_image_size));
                this.mainImageSize = (int) a.getDimension(4, resources.getDimension(R.dimen.carousel_main_image_size));
                this.rightImageStart = (int) a.getDimension(7, resources.getDimension(R.dimen.carousel_main_right_section_start));
                this.mainLeftPadding = (int) a.getDimension(5, resources.getDimension(R.dimen.carousel_main_left_padding));
                this.mainRightPadding = (int) a.getDimension(6, resources.getDimension(R.dimen.carousel_main_right_padding));
                this.rightSectionWidth = (int) a.getDimension(9, resources.getDimension(R.dimen.carousel_main_right_section_width));
                if (this.mainImageSize == this.sideImageSize) {
                    z = false;
                }
                this.viewsScaled = z;
                sLogger.v("view scaled=" + this.viewsScaled);
                this.rightSectionHeight = (int) getResources().getDimension(R.dimen.dashboard_box_height);
            } finally {
                a.recycle();
            }
        } else {
            throw new AssertionError();
        }
    }
    
    private void move(final Animator.AnimatorListener animatorListener, final boolean b, final boolean b2, final boolean b3, final int n) {
        Operation lastScrollAnimationOperation;
        if (b) {
            lastScrollAnimationOperation = Operation.FORWARD;
        }
        else {
            lastScrollAnimationOperation = Operation.BACK;
        }
        if (!b3 && this.animationRunning) {
            if (this.fastScrollAnimation || this.operationQueue.size() < 3) {
                int n2 = n;
                if (this.fastScrollAnimation) {
                    if (this.operationQueue.size() > 0) {
                        final OperationInfo operationInfo = this.operationQueue.peek();
                        n2 = n;
                        if (operationInfo.type == lastScrollAnimationOperation) {
                            ++operationInfo.count;
                            return;
                        }
                    }
                    else {
                        n2 = n;
                        if (this.lastScrollAnimationOperation == lastScrollAnimationOperation) {
                            n2 = 50;
                        }
                    }
                }
                final OperationInfo operationInfo2 = new OperationInfo(lastScrollAnimationOperation, null, n2);
                operationInfo2.runnable = new Runnable() {
                    @Override
                    public void run() {
                        CarouselLayout.this.move(animatorListener, b, b2, true, operationInfo2.animationDuration);
                    }
                };
                this.operationQueue.add(operationInfo2);
            }
        }
        else {
            final int currentItem = this.currentItem;
            int n3;
            if (b) {
                n3 = 1;
            }
            else {
                n3 = -1;
            }
            final int n4 = currentItem + n3;
            if (this.currentItem < 0 || n4 < 0 || n4 >= this.getCount()) {
                if (CarouselLayout.sLogger.isLoggable(2)) {
                    final Logger sLogger = CarouselLayout.sLogger;
                    final StringBuilder append = new StringBuilder().append("cannot go ");
                    String s;
                    if (b) {
                        s = "next";
                    }
                    else {
                        s = "prev";
                    }
                    sLogger.v(append.append(s).toString());
                }
                this.runQueuedOperation();
            }
            else {
                this.lastScrollAnimationOperation = lastScrollAnimationOperation;
                if (this.animator instanceof FastScrollAnimator) {
                    final AnimatorSet buildLayoutAnimation = this.animator.buildLayoutAnimation(animatorListener, this, this.currentItem, n4);
                    this.animationRunning = true;
                    if (CarouselLayout.sLogger.isLoggable(2)) {
                        CarouselLayout.sLogger.v("move:" + lastScrollAnimationOperation.name() + " duration =" + buildLayoutAnimation.getDuration());
                    }
                    buildLayoutAnimation.start();
                }
                else {
                    final AnimatorSet buildLayoutAnimation2 = this.buildLayoutAnimation(animatorListener, this.currentItem, n4);
                    buildLayoutAnimation2.setDuration((long)n);
                    this.animationRunning = true;
                    if (CarouselLayout.sLogger.isLoggable(2)) {
                        CarouselLayout.sLogger.v("move:" + lastScrollAnimationOperation.name() + " duration =" + buildLayoutAnimation2.getDuration());
                    }
                    buildLayoutAnimation2.start();
                }
                SoundUtils.playSound(SoundUtils.Sound.MENU_MOVE);
            }
        }
    }
    
    private void recycleView(final Carousel.ViewType viewType, final View view) {
        if (view != null) {
            this.removeView(view);
            view.setScaleX(1.0f);
            view.setScaleY(1.0f);
            view.setVisibility(View.VISIBLE);
            view.setTranslationX(0.0f);
            view.setTranslationY(0.0f);
            view.setAlpha(1.0f);
            if (view instanceof CrossFadeImageView) {
                final CrossFadeImageView crossFadeImageView = (CrossFadeImageView)view;
                final ImageView imageView = (ImageView)crossFadeImageView.getBig();
                imageView.setImageResource(0);
                imageView.setPivotX(0.0f);
                imageView.setPivotY(0.0f);
                if (this.viewsScaled && imageView instanceof InitialsImageView) {
                    ((InitialsImageView)imageView).setScaled(true);
                }
                final ImageView imageView2 = (ImageView)crossFadeImageView.getSmall();
                imageView2.setImageResource(0);
                imageView2.setPivotX(0.0f);
                imageView2.setPivotY(0.0f);
                if (this.viewsScaled && imageView2 instanceof InitialsImageView) {
                    ((InitialsImageView)imageView2).setScaled(true);
                }
            }
            this.viewCacheManager.putView(viewType, view);
        }
    }
    
    private void recycleViews() {
        this.recycleView(Carousel.ViewType.SIDE, this.leftView);
        this.leftView = null;
        this.recycleView(Carousel.ViewType.SIDE, this.rightView);
        this.rightView = null;
        this.recycleView(Carousel.ViewType.MIDDLE_LEFT, this.middleLeftView);
        this.middleLeftView = null;
        this.recycleView(Carousel.ViewType.MIDDLE_RIGHT, this.middleRightView);
        this.middleRightView = null;
    }
    
    private void setCurrentItem(final int currentItem, final boolean b) {
        final boolean b2 = true;
        if (currentItem >= 0 && currentItem < this.model.size()) {
            if (this.currentItem != -1) {
                this.recycleViews();
            }
            this.leftView = this.addSideView(currentItem - 1, this.viewPadding, currentItem > 0);
            this.middleLeftView = this.addMiddleLeftView(currentItem, this.viewPadding + this.sideImageSize + this.mainLeftPadding, true);
            this.middleRightView = this.addMiddleRightView(currentItem, this.viewPadding + this.sideImageSize + this.mainLeftPadding + this.mainImageSize, true);
            this.rightView = this.addSideView(currentItem + 1, this.viewPadding + this.sideImageSize + this.rightImageStart, currentItem + 1 < this.model.size() && b2);
            if (b && this.itemChangeListener != null) {
                this.itemChangeListener.onCurrentItemChanging(this.currentItem, currentItem, this.model.get(currentItem).id);
            }
            this.currentItem = currentItem;
            if (this.carouselIndicator != null) {
                this.carouselIndicator.setCurrentItem(this.currentItem);
            }
            if (this.itemChangeListener != null) {
                this.itemChangeListener.onCurrentItemChanged(this.currentItem, this.model.get(currentItem).id);
            }
            if (this.middleLeftView != null && this.middleLeftView.getVisibility() == 0) {
                this.selectedItemView = this.middleLeftView;
            }
        }
    }

    private void setModelInternal(final List<Carousel.Model> newModel, final int position, boolean animated) {
        if (this.currentItem == -1 || !animated) {
            this.model = newModel;
            setCurrentItem(position, false);
            runQueuedOperation();
            return;
        }
        AnimatorSet animatorSet = new AnimatorSet();
        Builder builder = animatorSet.play(ObjectAnimator.ofFloat(this.middleLeftView, View.ALPHA, new float[]{0.0f}));
        if (this.middleRightView != null) {
            builder.with(ObjectAnimator.ofFloat(this.middleRightView, View.ALPHA, new float[]{0.0f}));
        }
        if (this.leftView != null) {
            builder.with(ObjectAnimator.ofFloat(this.leftView, View.ALPHA, new float[]{0.0f}));
        }
        if (this.rightView != null) {
            builder.with(ObjectAnimator.ofFloat(this.rightView, View.ALPHA, new float[]{0.0f}));
        }
        animatorSet.setDuration(250);
        animatorSet.addListener(new DefaultAnimationListener() {
            public void onAnimationStart(Animator animator) {
                CarouselLayout.this.model = newModel;
                if (CarouselLayout.this.itemChangeListener != null) {
                    CarouselLayout.this.itemChangeListener.onCurrentItemChanging(CarouselLayout.this.currentItem, position, ((Carousel.Model) CarouselLayout.this.model.get(position)).id);
                }
            }

            public void onAnimationEnd(Animator animation) {
                CarouselLayout.this.setCurrentItem(position, false);
                CarouselLayout.this.runQueuedOperation();
            }
        });
        this.animationRunning = true;
        animatorSet.start();
    }
    
    View addHiddenView(final int n, final AnimationStrategy.Direction direction, final boolean b) {
        float n2;
        if (direction == AnimationStrategy.Direction.RIGHT) {
            n2 = this.rightView.getX() + this.mainImageSize + this.mainViewDividerPadding + this.rightSectionWidth + this.mainRightPadding;
        }
        else {
            n2 = -(this.leftView.getX() + this.leftView.getMeasuredWidth());
        }
        return this.addSideView(n, (int)n2, b);
    }
    
    View addMiddleLeftView(final int n, final int n2, final boolean b) {
        return this.buildView(n, n2, Carousel.ViewType.MIDDLE_LEFT, b);
    }
    
    View addMiddleRightView(final int n, final int n2, final boolean b) {
        return this.buildView(n, n2, Carousel.ViewType.MIDDLE_RIGHT, b);
    }
    
    View addSideView(final int n, final int n2, final boolean b) {
        return this.buildView(n, n2, Carousel.ViewType.SIDE, b);
    }
    
    View buildView(int visibility, final int n, final Carousel.ViewType viewType, final boolean b) {
        int n2 = 0;
        int n3 = 0;
        int n4 = 0;
        switch (viewType) {
            case SIDE:
                n3 = this.sideImageSize;
                n2 = this.sideImageSize;
                n4 = this.imageLytResourceId;
                break;
            case MIDDLE_LEFT:
                n3 = this.mainImageSize;
                n2 = this.mainImageSize;
                n4 = this.imageLytResourceId;
                break;
            case MIDDLE_RIGHT:
                n3 = this.rightSectionHeight;
                n2 = this.rightSectionWidth;
                n4 = this.infoLayoutResourceId;
                break;
        }
        final FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(n2, n3);
        layoutParams.gravity = 16;
        final View view = this.viewCacheManager.getView(viewType);
        final View view2 = this.carouselAdapter.getView(visibility, view, viewType, n4, n2);
        if (view == null && this.viewsScaled && viewType == Carousel.ViewType.SIDE && view2 instanceof CrossFadeImageView) {
            final CrossFadeImageView crossFadeImageView = (CrossFadeImageView)view2;
            final View big = crossFadeImageView.getBig();
            if (big instanceof InitialsImageView) {
                ((InitialsImageView)big).setScaled(true);
            }
            if (crossFadeImageView.getSmall() instanceof InitialsImageView) {
                ((InitialsImageView)big).setScaled(true);
            }
        }
        view2.setX((float)n);
        if (b) {
            visibility = 0;
        }
        else {
            visibility = 4;
        }
        view2.setVisibility(visibility);
        this.addView(view2, (ViewGroup.LayoutParams)layoutParams);
        return view2;
    }
    
    public void clearOperationQueue() {
        this.operationQueue.clear();
    }
    
    public int getCount() {
        int size;
        if (this.model == null) {
            size = 0;
        }
        else {
            size = this.model.size();
        }
        return size;
    }
    
    public int getCurrentItem() {
        return this.currentItem;
    }
    
    public Carousel.Model getCurrentModel() {
        return this.model.get(this.currentItem);
    }
    
    public Carousel.Listener getListener() {
        return this.itemChangeListener;
    }
    
    public Carousel.Model getModel(final int n) {
        Carousel.Model model;
        if (n < 0 || n >= this.model.size()) {
            model = null;
        }
        else {
            model = this.model.get(n);
        }
        return model;
    }
    
    public void init(final Carousel.InitParams initParams) {
        if (this.model != null) {
            throw new IllegalStateException();
        }
        if (initParams.model == null || initParams.rootContainer == null || initParams.infoLayoutResourceId == 0 || initParams.imageLytResourceId == 0) {
            throw new IllegalArgumentException();
        }
        this.model = initParams.model;
        this.infoLayoutResourceId = initParams.infoLayoutResourceId;
        this.viewProcessor = initParams.viewProcessor;
        this.rootContainer = initParams.rootContainer;
        this.carouselIndicator = initParams.carouselIndicator;
        this.carouselAdapter = new CarouselAdapter(this);
        this.imageLytResourceId = initParams.imageLytResourceId;
        this.fastScrollAnimation = initParams.fastScrollAnimation;
        this.exitOnDoubleClick = initParams.exitOnDoubleClick;
        AnimationStrategy animator;
        if (initParams.animator != null) {
            animator = initParams.animator;
        }
        else if (this.fastScrollAnimation) {
            animator = new FastScrollAnimator(this);
        }
        else {
            animator = new CarouselAnimator();
        }
        this.animator = animator;
        if (this.exitOnDoubleClick) {
            this.multipleClickGestureDetector = new MultipleClickGestureDetector(2, (MultipleClickGestureDetector.IMultipleClickKeyGesture)new MultipleClickGestureDetector.IMultipleClickKeyGesture() {
                @Override
                public IInputHandler nextHandler() {
                    return null;
                }
                
                @Override
                public boolean onGesture(final GestureEvent gestureEvent) {
                    return false;
                }
                
                @Override
                public boolean onKey(final CustomKeyEvent customKeyEvent) {
                    return CarouselLayout.this.handleKey(customKeyEvent);
                }
                
                @Override
                public void onMultipleClick(final int n) {
                    if (CarouselLayout.this.itemChangeListener != null) {
                        CarouselLayout.this.itemChangeListener.onExit();
                    }
                }
            });
        }
    }
    
    public void insert(final Animator.AnimatorListener animatorListener, final int n, final Carousel.Model model, final boolean b) {
        if (this.animationRunning) {
            this.operationQueue.add(new OperationInfo(Operation.OTHER, new Runnable() {
                @Override
                public void run() {
                    CarouselLayout.this.doInsert(animatorListener, n, model, b);
                }
            }));
        }
        else {
            this.doInsert(animatorListener, n, model, b);
        }
    }
    
    public boolean isAnimationEndPending() {
        return this.animator instanceof FastScrollAnimator && ((FastScrollAnimator)this.animator).isEndPending();
    }
    
    boolean isAnimationPending() {
        boolean b = false;
        if (this.operationQueue.size() > 0) {
            b = b;
            if (this.operationQueue.peek().type == this.lastScrollAnimationOperation) {
                b = true;
            }
        }
        return b;
    }
    
    public void moveNext(final Animator.AnimatorListener animatorListener) {
        this.move(animatorListener, true, false, false, this.animationDuration);
    }
    
    public void movePrevious(final Animator.AnimatorListener animatorListener) {
        this.move(animatorListener, false, false, false, this.animationDuration);
    }
    
    public IInputHandler nextHandler() {
        return null;
    }
    
    public void notifyDatasetChanged(final List<Carousel.Model> model, final int n) {
        this.model = model;
        this.setCurrentItem(n, true);
    }
    
    public void onClick() {
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        boolean b = false;
        if (this.currentItem == -1) {
            b = false;
        }
        else {
            this.detector.onGesture(gestureEvent);
            switch (gestureEvent.gesture) {
                default:
                    b = true;
                    break;
            }
        }
        return b;
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        boolean b;
        if (this.multipleClickGestureDetector != null) {
            b = this.multipleClickGestureDetector.onKey(customKeyEvent);
        }
        else {
            b = this.handleKey(customKeyEvent);
        }
        return b;
    }
    
    public void onTrackHand(final float n) {
    }
    
    public void reload() {
        this.setCurrentItem(this.currentItem, true);
    }
    
    void runQueuedOperation() {
        if (this.operationQueue.size() > 0) {
            final OperationInfo operationInfo = this.operationQueue.peek();
            if (operationInfo.count == 1) {
                this.operationQueue.remove();
            }
            else {
                --operationInfo.count;
            }
            this.handler.post(operationInfo.runnable);
        }
        else {
            if (this.animator instanceof FastScrollAnimator) {
                final FastScrollAnimator fastScrollAnimator = (FastScrollAnimator)this.animator;
                if (fastScrollAnimator.isEndPending()) {
                    fastScrollAnimator.endAnimation();
                    return;
                }
            }
            this.lastScrollAnimationOperation = null;
            this.animationRunning = false;
        }
    }
    
    public void selectItem() {
        if (this.animationRunning) {
            final Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    CarouselLayout.this.animationRunning = false;
                    CarouselLayout.this.selectItem();
                }
            };
            CarouselLayout.sLogger.w("queueing up select event");
            this.operationQueue.add(new OperationInfo(Operation.SELECT, runnable));
        }
        else {
            this.clearOperationQueue();
            if (this.selectedItemView == null) {
                CarouselLayout.sLogger.v("no item selected");
                this.runQueuedOperation();
            }
            else if (this.selectedItemView == this.middleLeftView) {
                final int intValue = (int)this.selectedItemView.getTag(R.id.item_id);
                SoundUtils.playSound(SoundUtils.Sound.MENU_SELECT);
                if (this.itemChangeListener != null) {
                    CarouselLayout.sLogger.v("execute item:" + intValue);
                    this.itemChangeListener.onExecuteItem(intValue, this.currentItem);
                }
                else {
                    CarouselLayout.sLogger.v("no carousel listener");
                    this.runQueuedOperation();
                }
            }
            else {
                CarouselLayout.sLogger.v("no match");
                this.runQueuedOperation();
            }
        }
    }
    
    public void setCurrentItem(final int n) {
        this.setCurrentItem(n, true);
    }
    
    public void setListener(final Carousel.Listener itemChangeListener) {
        this.itemChangeListener = itemChangeListener;
    }
    
    public void setModel(final List<Carousel.Model> list, final int n, final boolean b) {
        if (this.animationRunning) {
            this.clearOperationQueue();
            this.operationQueue.add(new OperationInfo(Operation.OTHER, new Runnable() {
                @Override
                public void run() {
                    CarouselLayout.this.setModelInternal(list, n, b);
                }
            }));
        }
        else {
            this.setModelInternal(list, n, b);
        }
    }
    
    enum Operation
    {
        BACK, 
        FORWARD, 
        OTHER, 
        SELECT;
    }
    
    static class OperationInfo
    {
        int animationDuration;
        int count;
        Runnable runnable;
        Operation type;
        
        OperationInfo(final Operation type, final Runnable runnable) {
            this.count = 1;
            this.animationDuration = -1;
            this.type = type;
            this.runnable = runnable;
        }
        
        OperationInfo(final Operation type, final Runnable runnable, final int animationDuration) {
            this.count = 1;
            this.animationDuration = -1;
            this.type = type;
            this.runnable = runnable;
            this.animationDuration = animationDuration;
        }
    }
}
