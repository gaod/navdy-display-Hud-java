package com.navdy.hud.app.ui.component.mainmenu;

import android.graphics.Shader;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import android.view.View;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import kotlin.NoWhenBranchMatchedException;
import com.navdy.hud.app.framework.toast.IToastCallback;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.hud.app.framework.voice.TTSUtils;
import android.os.Bundle;
import org.jetbrains.annotations.Nullable;
import android.content.res.Resources;
import com.navdy.hud.app.util.ReportIssueService;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import kotlin.jvm.internal.Intrinsics;
import com.navdy.hud.app.HudApplication;
//import kotlin.collections.MapsKt__MapsKt;
import kotlin.TuplesKt;
import kotlin.Pair;
//import kotlin.jvm.internal.DefaultConstructorMarker;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import java.util.List;
import java.util.ArrayList;
import com.navdy.service.library.log.Logger;
import org.jetbrains.annotations.NotNull;
import java.util.HashMap;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0000\u0018\u0000 =2\u00020\u0001:\u0002=>B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0001¢\u0006\u0002\u0010\u0007B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001\u0012\b\b\u0002\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\"\u0010\u0011\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0006\u001a\u00020\u00012\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0013H\u0016J\b\u0010\u0015\u001a\u00020\fH\u0016J\u0010\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000fH\u0016J\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0018\u001a\u00020\fH\u0016J\n\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J\b\u0010\u001b\u001a\u00020\u001cH\u0016J\b\u0010\u001d\u001a\u00020\u001eH\u0016J\b\u0010\u001f\u001a\u00020\u001eH\u0016J\u0018\u0010 \u001a\u00020\u001e2\u0006\u0010!\u001a\u00020\f2\u0006\u0010\u0018\u001a\u00020\fH\u0016J(\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u00102\u0006\u0010%\u001a\u00020&2\u0006\u0010\u0018\u001a\u00020\f2\u0006\u0010'\u001a\u00020(H\u0016J\b\u0010)\u001a\u00020#H\u0016J\b\u0010*\u001a\u00020#H\u0016J\u0010\u0010+\u001a\u00020#2\u0006\u0010,\u001a\u00020-H\u0016J\b\u0010.\u001a\u00020#H\u0016J\u0010\u0010/\u001a\u00020#2\u0006\u00100\u001a\u000201H\u0016J\u0010\u00102\u001a\u00020#2\u0006\u00103\u001a\u000204H\u0002J\u0010\u00105\u001a\u00020\u001e2\u0006\u0010,\u001a\u00020-H\u0016J\u0010\u00106\u001a\u00020#2\u0006\u0010!\u001a\u00020\fH\u0016J\u0010\u00107\u001a\u00020#2\u0006\u00108\u001a\u00020\fH\u0016J\b\u00109\u001a\u00020#H\u0016J\b\u0010:\u001a\u00020#H\u0002J\b\u0010;\u001a\u00020#H\u0016J\u0010\u0010<\u001a\u00020#2\u0006\u0010!\u001a\u00020\fH\u0002R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0001X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006?" }, d2 = { "Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;", "vscrollComponent", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;", "presenter", "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;", "parent", "(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V", "type", "Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;", "(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;)V", "backSelection", "", "backSelectionId", "currentItems", "", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "getChildMenu", "args", "", "path", "getInitialSelection", "getItems", "getModelfromPos", "pos", "getScrollIndex", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;", "getType", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;", "isBindCallsEnabled", "", "isFirstItemEmpty", "isItemClickable", "id", "onBindToView", "", "model", "view", "Landroid/view/View;", "state", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;", "onFastScrollEnd", "onFastScrollStart", "onItemSelected", "selection", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;", "onScrollIdle", "onUnload", "level", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;", "reportIssue", "issueType", "Lcom/navdy/hud/app/util/ReportIssueService$IssueType;", "selectItem", "setBackSelectionId", "setBackSelectionPos", "n", "setSelectedIcon", "showSentToast", "showToolTip", "takeSnapshot", "Companion", "ReportIssueMenuType", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class ReportIssueMenu implements IMenu
{
    public static final Companion Companion;
    private static final String NAV_ISSUE_SENT_TOAST_ID = "nav-issue-sent";
    private static final int TOAST_TIMEOUT = 1000;
    private static final VerticalList.Model back;
    private static final VerticalList.Model driveScore;
    private static final VerticalList.Model etaInaccurate;
    @NotNull
    private static final HashMap<Integer, String> idToTitleMap;
    private static final Logger logger;
    private static final VerticalList.Model maps;
    private static final VerticalList.Model navigation;
    private static final VerticalList.Model notFastestRoute;
    private static final VerticalList.Model permanentClosure;
    private static final String reportIssue;
    private static final int reportIssueColor;
    private static final ArrayList<VerticalList.Model> reportIssueItems;
    private static final VerticalList.Model roadBlocked;
    private static final VerticalList.Model smartDash;
    private static final ArrayList<VerticalList.Model> takeSnapShotItems;
    private static final String takeSnapshot;
    private static final int takeSnapshotColor;
    private static final String toastSentSuccessfully;
    private static final VerticalList.Model wrongDirection;
    private static final VerticalList.Model wrongRoadName;
    private int backSelection;
    private int backSelectionId;
    private List<? extends VerticalList.Model> currentItems;
    private final IMenu parent;
    private final MainMenuScreen2.Presenter presenter;
    private final ReportIssueMenuType type;
    private final VerticalMenuComponent vscrollComponent;
    
    static {
        Companion = new Companion();
        logger = new Logger(ReportIssueMenu.class);
        idToTitleMap = MapsKt__MapsKt.<Integer, String>hashMapOf(TuplesKt.<Integer, String>to(R.id.take_snapshot_maps, "Maps"), TuplesKt.<Integer, String>to(R.id.take_snapshot_navigation, "Navigation"), TuplesKt.<Integer, String>to(R.id.take_snapshot_smart_dash, "Smart_Dash"), TuplesKt.<Integer, String>to(R.id.take_snapshot_drive_score, "Drive_Score"));
        reportIssueItems = new ArrayList<VerticalList.Model>();
        takeSnapShotItems = new ArrayList<VerticalList.Model>();
        final Resources resources = HudApplication.getAppContext().getResources();
        final String string = resources.getString(R.string.issue_successfully_sent);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.st….issue_successfully_sent)");
        toastSentSuccessfully = string;
        final String string2 = resources.getString(R.string.report_navigation_issue);
        Intrinsics.checkExpressionValueIsNotNull(string2, "resources.getString(R.st….report_navigation_issue)");
        reportIssue = string2;
        final String string3 = resources.getString(R.string.take_snapshot);
        Intrinsics.checkExpressionValueIsNotNull(string3, "resources.getString(R.string.take_snapshot)");
        takeSnapshot = string3;
        reportIssueColor = resources.getColor(R.color.mm_options_report_issue);
        takeSnapshotColor = resources.getColor(R.color.options_dash_purple);
        final int color = resources.getColor(R.color.icon_bk_color_unselected);
        final VerticalList.Model buildModel = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, ReportIssueMenu.Companion.getReportIssueColor(), MainMenu.bkColorUnselected, ReportIssueMenu.Companion.getReportIssueColor(), resources.getString(R.string.back), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu…back), null\n            )");
        back = buildModel;
        final VerticalList.Model buildModel2 = IconBkColorViewHolder.buildModel(R.id.report_issue_menu_eta_inaccurate, R.drawable.icon_eta, ReportIssueMenu.Companion.getReportIssueColor(), color, ReportIssueMenu.Companion.getReportIssueColor(), resources.getString(ReportIssueService.IssueType.INEFFICIENT_ROUTE_ETA_TRAFFIC.getTitleStringResource()), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel2, "IconBkColorViewHolder.bu…urce), null\n            )");
        etaInaccurate = buildModel2;
        final VerticalList.Model buildModel3 = IconBkColorViewHolder.buildModel(R.id.report_issue_menu_not_fastest_route, R.drawable.icon_bad_route, ReportIssueMenu.Companion.getReportIssueColor(), color, ReportIssueMenu.Companion.getReportIssueColor(), resources.getString(ReportIssueService.IssueType.INEFFICIENT_ROUTE_SELECTED.getTitleStringResource()), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel3, "IconBkColorViewHolder.bu…urce), null\n            )");
        notFastestRoute = buildModel3;
        final VerticalList.Model buildModel4 = IconBkColorViewHolder.buildModel(R.id.report_issue_menu_road_blocked, R.drawable.icon_road_closed_temp, ReportIssueMenu.Companion.getReportIssueColor(), color, ReportIssueMenu.Companion.getReportIssueColor(), resources.getString(ReportIssueService.IssueType.ROAD_CLOSED.getTitleStringResource()), resources.getString(ReportIssueService.IssueType.ROAD_CLOSED.getMessageStringResource()));
        Intrinsics.checkExpressionValueIsNotNull(buildModel4, "IconBkColorViewHolder.bu…ngResource)\n            )");
        roadBlocked = buildModel4;
        final VerticalList.Model buildModel5 = IconBkColorViewHolder.buildModel(R.id.report_issue_menu_wrong_direction, R.drawable.icon_bad_map_info, ReportIssueMenu.Companion.getReportIssueColor(), color, ReportIssueMenu.Companion.getReportIssueColor(), resources.getString(ReportIssueService.IssueType.WRONG_DIRECTION.getTitleStringResource()), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel5, "IconBkColorViewHolder.bu…urce), null\n            )");
        wrongDirection = buildModel5;
        final VerticalList.Model buildModel6 = IconBkColorViewHolder.buildModel(R.id.report_issue_menu_wrong_road_name, R.drawable.icon_wrong_road_name, ReportIssueMenu.Companion.getReportIssueColor(), color, ReportIssueMenu.Companion.getReportIssueColor(), resources.getString(ReportIssueService.IssueType.ROAD_NAME.getTitleStringResource()), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel6, "IconBkColorViewHolder.bu…urce), null\n            )");
        wrongRoadName = buildModel6;
        final VerticalList.Model buildModel7 = IconBkColorViewHolder.buildModel(R.id.report_issue_menu_permanent_closure, R.drawable.icon_road_closed_perm, ReportIssueMenu.Companion.getReportIssueColor(), color, ReportIssueMenu.Companion.getReportIssueColor(), resources.getString(ReportIssueService.IssueType.ROAD_CLOSED_PERMANENT.getTitleStringResource()), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel7, "IconBkColorViewHolder.bu…urce), null\n            )");
        permanentClosure = buildModel7;
        ReportIssueMenu.Companion.getReportIssueItems().add(ReportIssueMenu.Companion.getBack());
        ReportIssueMenu.Companion.getReportIssueItems().add(ReportIssueMenu.Companion.getEtaInaccurate());
        ReportIssueMenu.Companion.getReportIssueItems().add(ReportIssueMenu.Companion.getNotFastestRoute());
        ReportIssueMenu.Companion.getReportIssueItems().add(ReportIssueMenu.Companion.getRoadBlocked());
        ReportIssueMenu.Companion.getReportIssueItems().add(ReportIssueMenu.Companion.getWrongDirection());
        ReportIssueMenu.Companion.getReportIssueItems().add(ReportIssueMenu.Companion.getWrongRoadName());
        ReportIssueMenu.Companion.getReportIssueItems().add(ReportIssueMenu.Companion.getPermanentClosure());
        final VerticalList.Model buildModel8 = IconBkColorViewHolder.buildModel(R.id.take_snapshot_maps, R.drawable.icon_options_report_issue_2, ReportIssueMenu.Companion.getTakeSnapshotColor(), color, ReportIssueMenu.Companion.getTakeSnapshotColor(), resources.getString(R.string.snapshot_maps), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel8, "IconBkColorViewHolder.bu…ing.snapshot_maps), null)");
        maps = buildModel8;
        final VerticalList.Model buildModel9 = IconBkColorViewHolder.buildModel(R.id.take_snapshot_navigation, R.drawable.icon_options_report_issue_2, ReportIssueMenu.Companion.getTakeSnapshotColor(), color, ReportIssueMenu.Companion.getTakeSnapshotColor(), resources.getString(R.string.snapshot_navigation), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel9, "IconBkColorViewHolder.bu…apshot_navigation), null)");
        navigation = buildModel9;
        final VerticalList.Model buildModel10 = IconBkColorViewHolder.buildModel(R.id.take_snapshot_smart_dash, R.drawable.icon_options_report_issue_2, ReportIssueMenu.Companion.getTakeSnapshotColor(), color, ReportIssueMenu.Companion.getTakeSnapshotColor(), resources.getString(R.string.snapshot_smart_dash), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel10, "IconBkColorViewHolder.bu…apshot_smart_dash), null)");
        driveScore = buildModel10;
        final VerticalList.Model buildModel11 = IconBkColorViewHolder.buildModel(R.id.take_snapshot_drive_score, R.drawable.icon_options_report_issue_2, ReportIssueMenu.Companion.getTakeSnapshotColor(), color, ReportIssueMenu.Companion.getTakeSnapshotColor(), resources.getString(R.string.snapshot_drive_score), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel11, "IconBkColorViewHolder.bu…pshot_drive_score), null)");
        smartDash = buildModel11;
        ReportIssueMenu.Companion.getTakeSnapShotItems().add(ReportIssueMenu.Companion.getMaps());
        ReportIssueMenu.Companion.getTakeSnapShotItems().add(ReportIssueMenu.Companion.getNavigation());
        ReportIssueMenu.Companion.getTakeSnapShotItems().add(ReportIssueMenu.Companion.getDriveScore());
        ReportIssueMenu.Companion.getTakeSnapShotItems().add(ReportIssueMenu.Companion.getSmartDash());
    }
    
    public ReportIssueMenu(@NotNull final VerticalMenuComponent verticalMenuComponent, @NotNull final MainMenuScreen2.Presenter presenter, @NotNull final IMenu menu) {
        Intrinsics.checkParameterIsNotNull(verticalMenuComponent, "vscrollComponent");
        Intrinsics.checkParameterIsNotNull(presenter, "presenter");
        Intrinsics.checkParameterIsNotNull(menu, "parent");
        this(verticalMenuComponent, presenter, menu, ReportIssueMenuType.NAVIGATION_ISSUES);
    }
    
    public ReportIssueMenu(@NotNull final VerticalMenuComponent vscrollComponent, @NotNull final MainMenuScreen2.Presenter presenter, @Nullable final IMenu parent, @NotNull final ReportIssueMenuType type) {
        Intrinsics.checkParameterIsNotNull(vscrollComponent, "vscrollComponent");
        Intrinsics.checkParameterIsNotNull(presenter, "presenter");
        Intrinsics.checkParameterIsNotNull(type, "type");
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
        this.type = type;
    }
    
    @NotNull
    public static final /* synthetic */ VerticalList.Model access$getBack$cp() {
        return ReportIssueMenu.back;
    }
    
    @NotNull
    public static final /* synthetic */ VerticalList.Model access$getDriveScore$cp() {
        return ReportIssueMenu.driveScore;
    }
    
    @NotNull
    public static final /* synthetic */ VerticalList.Model access$getEtaInaccurate$cp() {
        return ReportIssueMenu.etaInaccurate;
    }
    
    @NotNull
    public static final /* synthetic */ HashMap access$getIdToTitleMap$cp() {
        return ReportIssueMenu.idToTitleMap;
    }
    
    @NotNull
    public static final /* synthetic */ Logger access$getLogger$cp() {
        return ReportIssueMenu.logger;
    }
    
    @NotNull
    public static final /* synthetic */ VerticalList.Model access$getMaps$cp() {
        return ReportIssueMenu.maps;
    }
    
    @NotNull
    public static final /* synthetic */ String access$getNAV_ISSUE_SENT_TOAST_ID$cp() {
        return ReportIssueMenu.NAV_ISSUE_SENT_TOAST_ID;
    }
    
    @NotNull
    public static final /* synthetic */ VerticalList.Model access$getNavigation$cp() {
        return ReportIssueMenu.navigation;
    }
    
    @NotNull
    public static final /* synthetic */ VerticalList.Model access$getNotFastestRoute$cp() {
        return ReportIssueMenu.notFastestRoute;
    }
    
    @NotNull
    public static final /* synthetic */ VerticalList.Model access$getPermanentClosure$cp() {
        return ReportIssueMenu.permanentClosure;
    }
    
    @NotNull
    public static final /* synthetic */ String access$getReportIssue$cp() {
        return ReportIssueMenu.reportIssue;
    }
    
    public static final /* synthetic */ int access$getReportIssueColor$cp() {
        return ReportIssueMenu.reportIssueColor;
    }
    
    @NotNull
    public static final /* synthetic */ ArrayList access$getReportIssueItems$cp() {
        return ReportIssueMenu.reportIssueItems;
    }
    
    @NotNull
    public static final /* synthetic */ VerticalList.Model access$getRoadBlocked$cp() {
        return ReportIssueMenu.roadBlocked;
    }
    
    @NotNull
    public static final /* synthetic */ VerticalList.Model access$getSmartDash$cp() {
        return ReportIssueMenu.smartDash;
    }
    
    public static final /* synthetic */ int access$getTOAST_TIMEOUT$cp() {
        return ReportIssueMenu.TOAST_TIMEOUT;
    }
    
    @NotNull
    public static final /* synthetic */ ArrayList access$getTakeSnapShotItems$cp() {
        return ReportIssueMenu.takeSnapShotItems;
    }
    
    @NotNull
    public static final /* synthetic */ String access$getTakeSnapshot$cp() {
        return ReportIssueMenu.takeSnapshot;
    }
    
    public static final /* synthetic */ int access$getTakeSnapshotColor$cp() {
        return ReportIssueMenu.takeSnapshotColor;
    }
    
    @NotNull
    public static final /* synthetic */ String access$getToastSentSuccessfully$cp() {
        return ReportIssueMenu.toastSentSuccessfully;
    }
    
    @NotNull
    public static final /* synthetic */ VerticalList.Model access$getWrongDirection$cp() {
        return ReportIssueMenu.wrongDirection;
    }
    
    @NotNull
    public static final /* synthetic */ VerticalList.Model access$getWrongRoadName$cp() {
        return ReportIssueMenu.wrongRoadName;
    }
    
    private final void reportIssue(final ReportIssueService.IssueType issueType) {
        this.presenter.performSelectionAnimation((Runnable)new ReportIssueMenu$reportIssue.ReportIssueMenu$reportIssue$1(this, issueType));
    }
    
    private final void showSentToast() {
        final Bundle bundle = new Bundle();
        bundle.putInt("13", ReportIssueMenu.Companion.getTOAST_TIMEOUT());
        bundle.putInt("8", R.drawable.icon_report_issue);
        bundle.putString("4", ReportIssueMenu.Companion.getToastSentSuccessfully());
        bundle.putInt("5", R.style.Glances_1);
        bundle.putString("17", TTSUtils.TTS_NAV_STOPPED);
        bundle.putBoolean("21", true);
        bundle.putString("18", Screen.SCREEN_BACK.name());
        ToastManager.getInstance().addToast(new ToastManager.ToastParams(ReportIssueMenu.Companion.getNAV_ISSUE_SENT_TOAST_ID(), bundle, null, true, false));
    }
    
    private final void takeSnapshot(final int n) {
        this.presenter.performSelectionAnimation((Runnable)new ReportIssueMenu$takeSnapshot.ReportIssueMenu$takeSnapshot$1(this, n));
    }
    
    @Nullable
    @Override
    public IMenu getChildMenu(@NotNull final IMenu menu, @NotNull final String s, @NotNull final String s2) {
        Intrinsics.checkParameterIsNotNull(menu, "parent");
        Intrinsics.checkParameterIsNotNull(s, "args");
        Intrinsics.checkParameterIsNotNull(s2, "path");
        return null;
    }
    
    @Override
    public int getInitialSelection() {
        int n = 0;
        switch (ReportIssueMenu$WhenMappings.$EnumSwitchMapping$1[this.type.ordinal()]) {
            default:
                throw new NoWhenBranchMatchedException();
            case 1:
                n = 1;
                break;
            case 2:
                n = 0;
                break;
        }
        return n;
    }
    
    @Nullable
    @Override
    public List<VerticalList.Model> getItems() {
        List<? extends VerticalList.Model> currentItems = null;
        switch (ReportIssueMenu$WhenMappings.$EnumSwitchMapping$0[this.type.ordinal()]) {
            default:
                currentItems = null;
                break;
            case 1:
                this.currentItems = ReportIssueMenu.Companion.getReportIssueItems();
                return (List<VerticalList.Model>)this.currentItems;
            case 2:
                this.currentItems = ReportIssueMenu.Companion.getTakeSnapShotItems();
                return (List<VerticalList.Model>)this.currentItems;
        }
        return (List<VerticalList.Model>)currentItems;
        currentItems = this.currentItems;
        return (List<VerticalList.Model>)currentItems;
    }
    
    @Nullable
    @Override
    public VerticalList.Model getModelfromPos(final int n) {
        if (this.currentItems == null) {
            return null;
        }
        final List<? extends VerticalList.Model> currentItems = this.currentItems;
        if (currentItems == null) {
            Intrinsics.throwNpe();
        }
        if (currentItems.size() <= n) {
            return null;
        }
        final List<? extends VerticalList.Model> currentItems2 = this.currentItems;
        if (currentItems2 == null) {
            Intrinsics.throwNpe();
        }
        return (VerticalList.Model)currentItems2.get(n);
        model = null;
        return model;
    }
    
    @Nullable
    @Override
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    @NotNull
    @Override
    public Menu getType() {
        return Menu.REPORT_ISSUE;
    }
    
    @Override
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    @Override
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    @Override
    public boolean isItemClickable(final int n, final int n2) {
        return true;
    }
    
    @Override
    public void onBindToView(@NotNull final VerticalList.Model model, @NotNull final View view, final int n, @NotNull final VerticalList.ModelState modelState) {
        Intrinsics.checkParameterIsNotNull(model, "model");
        Intrinsics.checkParameterIsNotNull(view, "view");
        Intrinsics.checkParameterIsNotNull(modelState, "state");
    }
    
    @Override
    public void onFastScrollEnd() {
    }
    
    @Override
    public void onFastScrollStart() {
    }
    
    @Override
    public void onItemSelected(@NotNull final VerticalList.ItemSelectionState itemSelectionState) {
        Intrinsics.checkParameterIsNotNull(itemSelectionState, "selection");
    }
    
    @Override
    public void onScrollIdle() {
    }
    
    @Override
    public void onUnload(@NotNull final MenuLevel menuLevel) {
        Intrinsics.checkParameterIsNotNull(menuLevel, "level");
    }
    
    @Override
    public boolean selectItem(@NotNull final VerticalList.ItemSelectionState itemSelectionState) {
        Intrinsics.checkParameterIsNotNull(itemSelectionState, "selection");
        ReportIssueMenu.Companion.getLogger().v("select id:" + itemSelectionState.id + " pos:" + itemSelectionState.pos);
        switch (itemSelectionState.id) {
            case R.id.menu_back:
                ReportIssueMenu.Companion.getLogger().v("back");
                AnalyticsSupport.recordMenuSelection("report-issue-back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId, true);
                break;
            case R.id.report_issue_menu_eta_inaccurate:
                ReportIssueMenu.Companion.getLogger().v("ETA inaccurate");
                AnalyticsSupport.recordMenuSelection("report-issue-eta-inaccurate");
                this.reportIssue(ReportIssueService.IssueType.INEFFICIENT_ROUTE_ETA_TRAFFIC);
                break;
            case R.id.report_issue_menu_not_fastest_route:
                ReportIssueMenu.Companion.getLogger().v("Not the fastest route");
                AnalyticsSupport.recordMenuSelection("report-issue-not-fastest-route");
                this.reportIssue(ReportIssueService.IssueType.INEFFICIENT_ROUTE_SELECTED);
                break;
            case R.id.report_issue_menu_road_blocked:
                ReportIssueMenu.Companion.getLogger().v("Road blocked");
                AnalyticsSupport.recordMenuSelection("report-issue-road-blocked");
                this.reportIssue(ReportIssueService.IssueType.ROAD_CLOSED);
                break;
            case R.id.report_issue_menu_wrong_direction:
                ReportIssueMenu.Companion.getLogger().v("Wrong direction");
                AnalyticsSupport.recordMenuSelection("report-issue-wrong-direction");
                this.reportIssue(ReportIssueService.IssueType.WRONG_DIRECTION);
                break;
            case R.id.report_issue_menu_wrong_road_name:
                ReportIssueMenu.Companion.getLogger().v("Wrong road name");
                AnalyticsSupport.recordMenuSelection("report-issue-wrong-road-name");
                this.reportIssue(ReportIssueService.IssueType.ROAD_NAME);
                break;
            case R.id.report_issue_menu_permanent_closure:
                ReportIssueMenu.Companion.getLogger().v("Permanent closure");
                AnalyticsSupport.recordMenuSelection("report-issue-permanent-closure");
                this.reportIssue(ReportIssueService.IssueType.ROAD_CLOSED_PERMANENT);
                break;
            case R.id.take_snapshot_drive_score:
            case R.id.take_snapshot_maps:
            case R.id.take_snapshot_navigation:
            case R.id.take_snapshot_smart_dash:
                this.takeSnapshot(itemSelectionState.id);
                break;
        }
        return false;
    }
    
    @Override
    public void setBackSelectionId(final int backSelectionId) {
        this.backSelectionId = backSelectionId;
    }
    
    @Override
    public void setBackSelectionPos(final int backSelection) {
        this.backSelection = backSelection;
    }
    
    @Override
    public void setSelectedIcon() {
        switch (ReportIssueMenu$WhenMappings.$EnumSwitchMapping$2[this.type.ordinal()]) {
            case 1:
                this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_options_report_issue_2, ReportIssueMenu.Companion.getReportIssueColor(), null, 1.0f);
                this.vscrollComponent.selectedText.setText((CharSequence)ReportIssueMenu.Companion.getReportIssue());
                break;
            case 2:
                this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_options_report_issue_2, ReportIssueMenu.Companion.getTakeSnapshotColor(), null, 1.0f);
                this.vscrollComponent.selectedText.setText((CharSequence)ReportIssueMenu.Companion.getTakeSnapshot());
                break;
        }
    }
    
    @Override
    public void showToolTip() {
    }
    
    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0013\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082D¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0082D¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\fX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0014\u0010\u000f\u001a\u00020\fX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000eR\u0014\u0010\u0011\u001a\u00020\fX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000eR-\u0010\u0013\u001a\u001e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00040\u0014j\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u0004`\u0015¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u0019X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0014\u0010\u001c\u001a\u00020\fX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u000eR\u0014\u0010\u001e\u001a\u00020\fX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u000eR\u0014\u0010 \u001a\u00020\fX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\u000eR\u0014\u0010\"\u001a\u00020\fX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b#\u0010\u000eR\u0014\u0010$\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b%\u0010\u0006R\u0014\u0010&\u001a\u00020\bX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b'\u0010\nR\u001a\u0010(\u001a\b\u0012\u0004\u0012\u00020\f0)X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b*\u0010+R\u0014\u0010,\u001a\u00020\fX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b-\u0010\u000eR\u0014\u0010.\u001a\u00020\fX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b/\u0010\u000eR\u001a\u00100\u001a\b\u0012\u0004\u0012\u00020\f0)X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b1\u0010+R\u0014\u00102\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b3\u0010\u0006R\u0014\u00104\u001a\u00020\bX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b5\u0010\nR\u0014\u00106\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b7\u0010\u0006R\u0014\u00108\u001a\u00020\fX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b9\u0010\u000eR\u0014\u0010:\u001a\u00020\fX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b;\u0010\u000e¨\u0006<" }, d2 = { "Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;", "", "()V", "NAV_ISSUE_SENT_TOAST_ID", "", "getNAV_ISSUE_SENT_TOAST_ID", "()Ljava/lang/String;", "TOAST_TIMEOUT", "", "getTOAST_TIMEOUT", "()I", "back", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "getBack", "()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "driveScore", "getDriveScore", "etaInaccurate", "getEtaInaccurate", "idToTitleMap", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "getIdToTitleMap", "()Ljava/util/HashMap;", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "maps", "getMaps", "navigation", "getNavigation", "notFastestRoute", "getNotFastestRoute", "permanentClosure", "getPermanentClosure", "reportIssue", "getReportIssue", "reportIssueColor", "getReportIssueColor", "reportIssueItems", "Ljava/util/ArrayList;", "getReportIssueItems", "()Ljava/util/ArrayList;", "roadBlocked", "getRoadBlocked", "smartDash", "getSmartDash", "takeSnapShotItems", "getTakeSnapShotItems", "takeSnapshot", "getTakeSnapshot", "takeSnapshotColor", "getTakeSnapshotColor", "toastSentSuccessfully", "getToastSentSuccessfully", "wrongDirection", "getWrongDirection", "wrongRoadName", "getWrongRoadName", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    public static final class Companion
    {
        private final VerticalList.Model getBack() {
            return ReportIssueMenu.access$getBack$cp();
        }
        
        private final VerticalList.Model getDriveScore() {
            return ReportIssueMenu.access$getDriveScore$cp();
        }
        
        private final VerticalList.Model getEtaInaccurate() {
            return ReportIssueMenu.access$getEtaInaccurate$cp();
        }
        
        private final Logger getLogger() {
            return ReportIssueMenu.access$getLogger$cp();
        }
        
        private final VerticalList.Model getMaps() {
            return ReportIssueMenu.access$getMaps$cp();
        }
        
        private final String getNAV_ISSUE_SENT_TOAST_ID() {
            return ReportIssueMenu.access$getNAV_ISSUE_SENT_TOAST_ID$cp();
        }
        
        private final VerticalList.Model getNavigation() {
            return ReportIssueMenu.access$getNavigation$cp();
        }
        
        private final VerticalList.Model getNotFastestRoute() {
            return ReportIssueMenu.access$getNotFastestRoute$cp();
        }
        
        private final VerticalList.Model getPermanentClosure() {
            return ReportIssueMenu.access$getPermanentClosure$cp();
        }
        
        private final String getReportIssue() {
            return ReportIssueMenu.access$getReportIssue$cp();
        }
        
        private final int getReportIssueColor() {
            return ReportIssueMenu.access$getReportIssueColor$cp();
        }
        
        private final ArrayList<VerticalList.Model> getReportIssueItems() {
            return (ArrayList<VerticalList.Model>)ReportIssueMenu.access$getReportIssueItems$cp();
        }
        
        private final VerticalList.Model getRoadBlocked() {
            return ReportIssueMenu.access$getRoadBlocked$cp();
        }
        
        private final VerticalList.Model getSmartDash() {
            return ReportIssueMenu.access$getSmartDash$cp();
        }
        
        private final int getTOAST_TIMEOUT() {
            return ReportIssueMenu.access$getTOAST_TIMEOUT$cp();
        }
        
        private final ArrayList<VerticalList.Model> getTakeSnapShotItems() {
            return (ArrayList<VerticalList.Model>)ReportIssueMenu.access$getTakeSnapShotItems$cp();
        }
        
        private final String getTakeSnapshot() {
            return ReportIssueMenu.access$getTakeSnapshot$cp();
        }
        
        private final int getTakeSnapshotColor() {
            return ReportIssueMenu.access$getTakeSnapshotColor$cp();
        }
        
        private final String getToastSentSuccessfully() {
            return ReportIssueMenu.access$getToastSentSuccessfully$cp();
        }
        
        private final VerticalList.Model getWrongDirection() {
            return ReportIssueMenu.access$getWrongDirection$cp();
        }
        
        private final VerticalList.Model getWrongRoadName() {
            return ReportIssueMenu.access$getWrongRoadName$cp();
        }
        
        @NotNull
        public final HashMap<Integer, String> getIdToTitleMap() {
            return (HashMap<Integer, String>)ReportIssueMenu.access$getIdToTitleMap$cp();
        }
    }
    
    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0080\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005" }, d2 = { "Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;", "", "(Ljava/lang/String;I)V", "NAVIGATION_ISSUES", "SNAP_SHOT", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    public enum ReportIssueMenuType
    {
        NAVIGATION_ISSUES, 
        SNAP_SHOT;
    }
}
