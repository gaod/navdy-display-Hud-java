package com.navdy.hud.app.ui.framework;

import com.navdy.hud.app.framework.notifications.NotificationType;

public interface INotificationAnimationListener
{
    void onStart(final String p0, final NotificationType p1, final UIStateManager.Mode p2);
    
    void onStop(final String p0, final NotificationType p1, final UIStateManager.Mode p2);
}
