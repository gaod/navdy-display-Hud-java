package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.animation.Animator;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import kotlin.Metadata;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016¨\u0006\u0007"}, d2 = {"com/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$3", "Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;", "(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;I)V", "onAnimationEnd", "", "animation", "Landroid/animation/Animator;", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: SwitchViewHolder.kt */
public final class SwitchViewHolder$select$3 extends DefaultAnimationListener {
    final /* synthetic */ Model $model;
    final /* synthetic */ int $pos;
    final /* synthetic */ SwitchViewHolder this$0;

    SwitchViewHolder$select$3(SwitchViewHolder $outer, Model $captured_local_variable$1, int $captured_local_variable$2) {
        this.this$0 = $outer;
        this.$model = $captured_local_variable$1;
        this.$pos = $captured_local_variable$2;
    }

    public void onAnimationEnd(@Nullable Animator animation) {
        super.onAnimationEnd(animation);
        this.this$0.isOn = !this.this$0.isOn;
        this.this$0.drawSwitch(this.this$0.isOn, this.this$0.isEnabled);
        ItemSelectionState selectionState = this.this$0.vlist.getItemSelectionState();
        selectionState.set(this.$model, this.$model.id, this.$pos, -1, -1);
        this.this$0.vlist.performSelectAction(selectionState);
        this.this$0.vlist.unlock();
    }
}
