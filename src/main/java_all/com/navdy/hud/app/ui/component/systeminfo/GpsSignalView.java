package com.navdy.hud.app.ui.component.systeminfo;

import android.content.res.Resources;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.view.ViewGroup;
import com.navdy.hud.app.HudApplication;
import android.view.LayoutInflater;
import android.util.AttributeSet;
import android.content.Context;
import com.navdy.service.library.log.Logger;
import android.widget.LinearLayout;

public class GpsSignalView extends LinearLayout
{
    private Logger logger;
    
    public GpsSignalView(final Context context) {
        super(context);
        this.logger = new Logger(GpsSignalView.class);
    }
    
    public GpsSignalView(final Context context, final AttributeSet set) {
        super(context, set);
        this.logger = new Logger(GpsSignalView.class);
    }
    
    public GpsSignalView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.logger = new Logger(GpsSignalView.class);
        this.setOrientation(0);
    }
    
    public void setSatelliteData(final SatelliteData[] array) {
        this.removeAllViews();
        final LayoutInflater from = LayoutInflater.from(this.getContext());
        final Resources resources = HudApplication.getAppContext().getResources();
        final int dimensionPixelSize = resources.getDimensionPixelSize(R.dimen.satellite_width_separator);
        final int dimensionPixelSize2 = resources.getDimensionPixelSize(R.dimen.satellite_width);
        for (int i = 0; i < array.length; ++i) {
            final ViewGroup viewGroup = (ViewGroup)from.inflate(R.layout.screen_home_system_info_gps_satellite, (ViewGroup)null);
            viewGroup.setLayoutParams((ViewGroup.LayoutParams)new LinearLayout.LayoutParams(dimensionPixelSize2, -1));
            ((TextView)viewGroup.findViewById(R.id.satellite_cno)).setText((CharSequence)String.valueOf(array[i].cNo));
            ((TextView)viewGroup.findViewById(R.id.satellite_type)).setText((CharSequence)array[i].provider);
            ((TextView)viewGroup.findViewById(R.id.satellite_id)).setText((CharSequence)String.valueOf(array[i].satelliteId));
            ((RelativeLayout.LayoutParams)viewGroup.findViewById(R.id.satellite_strength).getLayoutParams()).height = array[i].cNo * 2;
            this.addView((View)viewGroup);
            if (i < array.length - 1) {
                final View view = new View(this.getContext());
                view.setLayoutParams((ViewGroup.LayoutParams)new LinearLayout.LayoutParams(dimensionPixelSize, dimensionPixelSize));
                this.addView(view);
            }
        }
        this.logger.v("set satellite =" + array.length);
    }
    
    public static class SatelliteData implements Comparable<SatelliteData>
    {
        int cNo;
        String provider;
        int satelliteId;
        
        public SatelliteData(final int satelliteId, final int cNo, final String provider) {
            this.satelliteId = satelliteId;
            this.cNo = cNo;
            this.provider = provider;
        }
        
        @Override
        public int compareTo(final SatelliteData satelliteData) {
            return satelliteData.cNo - this.cNo;
        }
    }
}
