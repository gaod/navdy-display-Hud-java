package com.navdy.hud.app.ui.component.carousel;

import android.widget.TextView;
import android.view.ViewGroup;
import com.navdy.hud.app.ui.component.image.ColorImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import android.view.View;
import com.navdy.service.library.log.Logger;

public class CarouselAdapter
{
    private static final Logger sLogger;
    CarouselLayout carousel;
    
    static {
        sLogger = new Logger(CarouselAdapter.class);
    }
    
    public CarouselAdapter(final CarouselLayout carousel) {
        this.carousel = carousel;
    }
    
    private void processImageView(final Carousel.ViewType viewType, final Carousel.Model model, final View view, final int n, final int n2) {
        while (true) {
            Label_0124: {
                if (!(view instanceof CrossFadeImageView)) {
                    break Label_0124;
                }
                final CrossFadeImageView crossFadeImageView = (CrossFadeImageView)view;
                final InitialsImageView initialsImageView = (InitialsImageView)crossFadeImageView.getBig();
                final View small = crossFadeImageView.getSmall();
                initialsImageView.setImage(model.largeImageRes, null, InitialsImageView.Style.DEFAULT);
                if (this.carousel.imageLytResourceId == R.layout.crossfade_image_lyt) {
                    ((InitialsImageView)small).setImage(model.smallImageRes, null, InitialsImageView.Style.DEFAULT);
                    break Label_0124;
                }
                Label_0109: {
                    break Label_0109;
                Block_6_Outer:
                    while (true) {
                        try {
                            if (viewType == Carousel.ViewType.MIDDLE_LEFT) {
                                this.carousel.viewProcessor.processLargeImageView(model, view, n, n2, n2);
                            }
                            else {
                                this.carousel.viewProcessor.processSmallImageView(model, view, n, n2, n2);
                            }
                            return;
                            // iftrue(Label_0160:, viewType != Carousel.ViewType.MIDDLE_LEFT)
                            // iftrue(Label_0071:, !view instanceof InitialsImageView)
                            InitialsImageView initialsImageView2 = null;
                            while (true) {
                                while (true) {
                                    initialsImageView2.setImage(model.largeImageRes, null, InitialsImageView.Style.DEFAULT);
                                    break;
                                    initialsImageView2 = (InitialsImageView)view;
                                    continue Block_6_Outer;
                                }
                                ((ColorImageView)small).setColor(model.smallImageColor);
                                break;
                                continue;
                            }
                            Label_0160: {
                                initialsImageView2.setImage(model.smallImageRes, null, InitialsImageView.Style.DEFAULT);
                            }
                            break;
                        }
                        catch (Throwable t) {
                            CarouselAdapter.sLogger.e(t);
                        }
                        return;
                    }
                }
            }
            if (this.carousel.viewProcessor != null) {
                continue;
            }
            break;
        }
    }
    
    private void processInfoView(final Carousel.Model model, final View view, final int n) {
        if (model.infoMap != null) {
            final ViewGroup viewGroup = (ViewGroup)view;
            for (int childCount = viewGroup.getChildCount(), i = 0; i < childCount; ++i) {
                final View child = viewGroup.getChildAt(i);
                if (child instanceof TextView) {
                    final String text = model.infoMap.get(child.getId());
                    if (text != null) {
                        ((TextView)child).setText((CharSequence)text);
                        child.setVisibility(View.VISIBLE);
                    }
                    else {
                        child.setVisibility(GONE);
                    }
                }
            }
        }
        if (this.carousel.viewProcessor == null) {
            return;
        }
        try {
            this.carousel.viewProcessor.processInfoView(model, view, n);
        }
        catch (Throwable t) {
            CarouselAdapter.sLogger.e(t);
        }
    }
    
    private void processView(final Carousel.ViewType viewType, final Carousel.Model model, final View view, final int n, final int n2) {
        if (viewType == Carousel.ViewType.MIDDLE_RIGHT) {
            this.processInfoView(model, view, n);
        }
        else {
            this.processImageView(viewType, model, view, n, n2);
        }
    }
    
    public View getView(final int n, View inflate, final Carousel.ViewType viewType, final int n2, final int n3) {
        if (inflate != null) {
            inflate.setTag(R.id.item_id, null);
        }
        Carousel.Model model2;
        final Carousel.Model model = model2 = null;
        if (n >= 0) {
            model2 = model;
            if (n < this.carousel.model.size()) {
                model2 = this.carousel.model.get(n);
            }
        }
        if (inflate == null) {
            CarouselAdapter.sLogger.v("create " + viewType + " view");
            final Object o = inflate = this.carousel.inflater.inflate(n2, (ViewGroup)null);
            if (o instanceof CrossFadeImageView) {
                if (viewType == Carousel.ViewType.SIDE) {
                    ((CrossFadeImageView)o).inject(CrossFadeImageView.Mode.SMALL);
                    inflate = (View)o;
                }
                else {
                    inflate = (View)o;
                    if (viewType == Carousel.ViewType.MIDDLE_LEFT) {
                        ((CrossFadeImageView)o).inject(CrossFadeImageView.Mode.BIG);
                        inflate = (View)o;
                    }
                }
            }
        }
        else {
            final Object o2 = inflate = inflate;
            if (o2 instanceof CrossFadeImageView) {
                if (viewType == Carousel.ViewType.SIDE) {
                    ((CrossFadeImageView)o2).setMode(CrossFadeImageView.Mode.SMALL);
                    inflate = (View)o2;
                }
                else {
                    inflate = (View)o2;
                    if (viewType == Carousel.ViewType.MIDDLE_LEFT) {
                        ((CrossFadeImageView)o2).setMode(CrossFadeImageView.Mode.BIG);
                        inflate = (View)o2;
                    }
                }
            }
        }
        if (model2 != null) {
            inflate.setTag(R.id.item_id, model2.id);
            inflate.setVisibility(View.VISIBLE);
            this.processView(viewType, model2, inflate, n, n3);
        }
        else {
            inflate.setVisibility(INVISIBLE);
            inflate.setTag(R.id.item_id, null);
        }
        return inflate;
    }
}
