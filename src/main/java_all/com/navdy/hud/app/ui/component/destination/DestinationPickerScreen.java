package com.navdy.hud.app.ui.component.destination;

import mortar.Presenter;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.os.Bundle;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import android.animation.Animator;
import android.animation.Animator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.TitleViewHolder;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.service.library.events.ui.ShowScreen;
import android.graphics.Shader;
import com.here.android.mpa.routing.Route;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.maps.here.HereRouteCache;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.destinations.DestinationsManager;
import com.navdy.hud.app.framework.destinations.Destination;
import com.navdy.hud.app.maps.MapsNotification;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen;
import com.navdy.hud.app.ui.component.homescreen.NavigationView;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoBoundingBox;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import javax.inject.Singleton;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.ui.activity.Main;
import dagger.Module;
import com.navdy.service.library.events.ui.Screen;
import flow.Flow;
import android.content.res.Resources;
import java.util.HashMap;
import com.navdy.hud.app.HudApplication;
import java.util.ArrayList;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.events.places.PlaceType;
import java.util.Map;
import java.util.List;
import flow.Layout;
import com.navdy.hud.app.screen.BaseScreen;

@Layout(R.layout.screen_destination_picker)
public class DestinationPickerScreen extends BaseScreen
{
    private static final int CANCEL_POSITION = 1;
    private static List<String> CONFIRMATION_CHOICES;
    private static final float MIN_DISTANCE_EXPANSION = 3000.0f;
    private static final float MIN_DISTANCE_THRESHOLD = 5000.0f;
    private static final int NAVIGATE_POSITION = 0;
    public static final String PICKER_DESTINATIONS = "PICKER_DESTINATIONS";
    public static final String PICKER_DESTINATION_ICON = "PICKER_DESTINATION_ICON";
    public static final String PICKER_HIDE_IF_NAV_STOPS = "PICKER_HIDE";
    public static final String PICKER_INITIAL_SELECTION = "PICKER_INITIAL_SELECTION";
    public static final String PICKER_LEFT_ICON = "PICKER_LEFT_ICON";
    public static final String PICKER_LEFT_ICON_BKCOLOR = "PICKER_LEFT_ICON_BKCOLOR";
    public static final String PICKER_LEFT_TITLE = "PICKER_LEFT_TITLE";
    public static final String PICKER_MAP_END_LAT = "PICKER_MAP_END_LAT";
    public static final String PICKER_MAP_END_LNG = "PICKER_MAP_END_LNG";
    public static final String PICKER_MAP_START_LAT = "PICKER_MAP_START_LAT";
    public static final String PICKER_MAP_START_LNG = "PICKER_MAP_START_LNG";
    public static final String PICKER_SHOW_DESTINATION_MAP = "PICKER_SHOW_DESTINATION_MAP";
    public static final String PICKER_SHOW_ROUTE_MAP = "PICKER_SHOW_ROUTE_MAP";
    public static final String PICKER_TITLE = "PICKER_TITLE";
    private static final int SELECTION_ANIMATION_DELAY = 1000;
    private static DestinationPickerScreen instance;
    private static final Map<PlaceType, PlaceTypeResourceHolder> placeResMapping;
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(DestinationPickerScreen.class);
        DestinationPickerScreen.CONFIRMATION_CHOICES = new ArrayList<String>();
        final Resources resources = HudApplication.getAppContext().getResources();
        DestinationPickerScreen.CONFIRMATION_CHOICES.add(resources.getString(R.string.destination_start_navigation_yes));
        DestinationPickerScreen.CONFIRMATION_CHOICES.add(resources.getString(R.string.destination_start_navigation_cancel));
        (placeResMapping = new HashMap<PlaceType, PlaceTypeResourceHolder>()).put(PlaceType.PLACE_TYPE_GAS, new PlaceTypeResourceHolder(R.drawable.icon_place_gas, R.string.gas, R.color.mm_search_gas, R.drawable.icon_pin_dot_destination_indigo));
        DestinationPickerScreen.placeResMapping.put(PlaceType.PLACE_TYPE_PARKING, new PlaceTypeResourceHolder(R.drawable.icon_place_parking, R.string.parking, R.color.mm_search_parking, R.drawable.icon_pin_dot_destination_indigo));
        DestinationPickerScreen.placeResMapping.put(PlaceType.PLACE_TYPE_RESTAURANT, new PlaceTypeResourceHolder(R.drawable.icon_place_restaurant, R.string.food, R.color.mm_search_food, R.drawable.icon_pin_dot_destination_orange));
        DestinationPickerScreen.placeResMapping.put(PlaceType.PLACE_TYPE_STORE, new PlaceTypeResourceHolder(R.drawable.icon_place_store, R.string.grocery_store, R.color.mm_search_grocery_store, R.drawable.icon_pin_dot_destination_blue));
        DestinationPickerScreen.placeResMapping.put(PlaceType.PLACE_TYPE_COFFEE, new PlaceTypeResourceHolder(R.drawable.icon_place_coffee, R.string.coffee, R.color.mm_search_coffee, R.drawable.icon_pin_dot_destination_orange));
        DestinationPickerScreen.placeResMapping.put(PlaceType.PLACE_TYPE_ATM, new PlaceTypeResourceHolder(R.drawable.icon_place_a_t_m, R.string.atm, R.color.mm_search_atm, R.drawable.icon_pin_dot_destination_blue));
        DestinationPickerScreen.placeResMapping.put(PlaceType.PLACE_TYPE_HOSPITAL, new PlaceTypeResourceHolder(R.drawable.icon_place_hospital, R.string.hospital, R.color.mm_search_hospital, R.drawable.icon_pin_dot_destination_red));
    }
    
    public DestinationPickerScreen() {
        DestinationPickerScreen.instance = this;
    }
    
    public static PlaceTypeResourceHolder getPlaceTypeHolder(final PlaceType placeType) {
        return DestinationPickerScreen.placeResMapping.get(placeType);
    }
    
    @Override
    public int getAnimationIn(final Flow.Direction direction) {
        return -1;
    }
    
    @Override
    public int getAnimationOut(final Flow.Direction direction) {
        return -1;
    }
    
    @Override
    public Object getDaggerModule() {
        return new Module();
    }
    
    @Override
    public String getMortarScopeName() {
        return this.getClass().getName();
    }
    
    @Override
    public Screen getScreen() {
        return Screen.SCREEN_DESTINATION_PICKER;
    }
    
    public static class DestinationPickerState
    {
        public boolean doNotAddOriginalRoute;
    }
    
    @dagger.Module(addsTo = Main.Module.class, injects = { DestinationPickerView.class })
    public class Module
    {
    }
    
    public static class PlaceTypeResourceHolder
    {
        public final int colorRes;
        public final int destinationIcon;
        public final int iconRes;
        public final int titleRes;
        
        private PlaceTypeResourceHolder(final int iconRes, final int titleRes, final int colorRes, final int destinationIcon) {
            this.iconRes = iconRes;
            this.titleRes = titleRes;
            this.colorRes = colorRes;
            this.destinationIcon = destinationIcon;
        }
    }
    
    @Singleton
    public static class Presenter extends BasePresenter<DestinationPickerView>
    {
        @Inject
        Bus bus;
        private boolean closed;
        private List<VerticalList.Model> data;
        private List<GeoBoundingBox> destinationGeoBBoxList;
        private List<GeoCoordinate> destinationGeoList;
        private int destinationIcon;
        DestinationParcelable[] destinations;
        boolean doNotAddOriginalRoute;
        private GeoCoordinate end;
        private boolean handledSelection;
        private boolean hideScreenOnNavStop;
        private HomeScreenView homeScreenView;
        int initialSelection;
        int itemSelection;
        private int leftBkColor;
        private int leftIcon;
        private String leftTitle;
        private NavigationView navigationView;
        private IDestinationPicker pickerCallback;
        private boolean registered;
        private boolean showDestinationMap;
        private boolean showRouteMap;
        private GeoCoordinate start;
        private HomeScreen.DisplayMode switchBackMode;
        private String title;
        
        private void buildDestinationBBox() {
            final ArrayList<GeoCoordinate> list = new ArrayList<GeoCoordinate>();
            final GeoCoordinate lastGeoCoordinate = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
            DestinationPickerScreen.sLogger.v("destinationbbox-ce:" + lastGeoCoordinate);
            this.destinationGeoList = new ArrayList<GeoCoordinate>();
            this.destinationGeoBBoxList = new ArrayList<GeoBoundingBox>();
        Label_0126:
            for (int i = 0; i < this.destinations.length; ++i) {
                if (this.destinations[i].displayLatitude == 0.0 && this.destinations[i].displayLongitude == 0.0) {
                    this.destinationGeoList.add(null);
                    this.destinationGeoBBoxList.add(null);
                }
                else {
                    list.clear();
                    if (lastGeoCoordinate != null) {
                        list.add(lastGeoCoordinate);
                    }
                    final GeoCoordinate geoCoordinate = new GeoCoordinate(this.destinations[i].displayLatitude, this.destinations[i].displayLongitude);
                    list.add(geoCoordinate);
                    final GeoBoundingBox boundingBoxContainingGeoCoordinates = GeoBoundingBox.getBoundingBoxContainingGeoCoordinates(list);
                    while (true) {
                        if (lastGeoCoordinate != null && (long)lastGeoCoordinate.distanceTo(geoCoordinate) <= 5000.0f) {
                            try {
                                boundingBoxContainingGeoCoordinates.expand(3000.0f, 3000.0f);
                                this.destinationGeoList.add(geoCoordinate);
                                this.destinationGeoBBoxList.add(boundingBoxContainingGeoCoordinates);
                                continue Label_0126;
                            }
                            catch (Throwable t) {
                                DestinationPickerScreen.sLogger.e("expand bbox", t);
                                continue;
                            }
                            break;
                        }
                        continue;
                    }
                }
            }
        }
        
        private boolean isNotADestination(final int n) {
            final DestinationParcelable destinationParcelable = (DestinationParcelable)this.data.get(n).state;
            return destinationParcelable != null && destinationParcelable.type != DestinationParcelable.DestinationType.DESTINATION;
        }
        
        private void launchDestination(final DestinationPickerView destinationPickerView, final DestinationParcelable destinationParcelable) {
            DestinationPickerScreen.sLogger.v("launchDestination {" + destinationParcelable.destinationTitle + "} display {" + destinationParcelable.displayLatitude + "," + destinationParcelable.displayLongitude + "}" + " nav {" + destinationParcelable.navLatitude + "," + destinationParcelable.navLongitude + "}");
            if (!HereMapsManager.getInstance().isInitialized()) {
                DestinationPickerScreen.sLogger.w("Here maps engine not initialized, exit");
                this.close(new Runnable() {
                    @Override
                    public void run() {
                        MapsNotification.showMapsEngineNotInitializedToast();
                    }
                });
            }
            else {
                this.close(new Runnable() {
                    @Override
                    public void run() {
                        final Destination build = new Destination.Builder().destinationTitle(destinationParcelable.destinationTitle).destinationSubtitle(destinationParcelable.destinationSubTitle).fullAddress(destinationParcelable.destinationAddress).displayPositionLatitude(destinationParcelable.displayLatitude).displayPositionLongitude(destinationParcelable.displayLongitude).navigationPositionLatitude(destinationParcelable.navLatitude).navigationPositionLongitude(destinationParcelable.navLongitude).destinationType(Destination.DestinationType.DEFAULT).identifier(destinationParcelable.identifier).favoriteDestinationType(Destination.FavoriteDestinationType.FAVORITE_NONE).destinationIcon(destinationParcelable.icon).destinationIconBkColor(destinationParcelable.iconSelectedColor).destinationPlaceId(destinationParcelable.destinationPlaceId).placeType(destinationParcelable.placeType).distanceStr(destinationParcelable.distanceStr).contacts(destinationParcelable.contacts).phoneNumbers(destinationParcelable.phoneNumbers).build();
                        DestinationPickerScreen.sLogger.v("call requestNavigation");
                        DestinationsManager.getInstance().requestNavigationWithNavLookup(build);
                    }
                });
            }
        }
        
        private void performAction(final DestinationPickerView destinationPickerView, final int n, final int n2) {
            DestinationPickerScreen.sLogger.v("performAction {" + n + "," + n2 + "}");
            NotificationManager.getInstance().enableNotifications(true);
            if (this.showRouteMap || this.showDestinationMap) {
                destinationPickerView.setBackgroundColor(-16777216);
                if (this.navigationView != null) {
                    this.navigationView.setMapMaskVisibility(0);
                }
            }
            destinationPickerView.vmenuComponent.performSelectionAnimation(new Runnable() {
                @Override
                public void run() {
                    final DestinationPickerView destinationPickerView = (DestinationPickerView)mortar.Presenter.this.getView();
                    if (destinationPickerView != null) {
                        if (Presenter.this.data == null || n2 >= Presenter.this.data.size()) {
                            DestinationPickerScreen.sLogger.w("invalid list,no selection");
                            Presenter.this.handledSelection = false;
                        }
                        else {
                            int onItemClicked = 0;
                            if (Presenter.this.pickerCallback != null) {
                                final DestinationPickerState destinationPickerState = new DestinationPickerState();
                                final DestinationParcelable destinationParcelable = (DestinationParcelable)Presenter.this.data.get(n2).state;
                                int n;
                                if (destinationParcelable != null && destinationParcelable.id != 0) {
                                    n = destinationParcelable.id;
                                }
                                else {
                                    n = n;
                                }
                                final boolean b = (onItemClicked = (Presenter.this.pickerCallback.onItemClicked(n, n2 - 1, destinationPickerState) ? 1 : 0)) != 0;
                                if (Presenter.this.showRouteMap) {
                                    DestinationPickerScreen.sLogger.v("map-route- doNotAddOriginalRoute=" + destinationPickerState.doNotAddOriginalRoute);
                                    Presenter.this.doNotAddOriginalRoute = destinationPickerState.doNotAddOriginalRoute;
                                    onItemClicked = (b ? 1 : 0);
                                }
                            }
                            if (onItemClicked == 0 && !Presenter.this.isNotADestination(n2)) {
                                final VerticalList.Model model = Presenter.this.data.get(n2);
                                DestinationPickerScreen.sLogger.v("not overriden, launching pos:" + n2 + " title=" + model.title);
                                Presenter.this.launchDestination(destinationPickerView, (DestinationParcelable)model.state);
                            }
                            else {
                                DestinationPickerScreen.sLogger.v("overridden");
                                Presenter.this.close();
                            }
                            Presenter.this.handledSelection = true;
                        }
                    }
                }
            }, 1000);
        }
        
        private void showConfirmation(final DestinationPickerView destinationPickerView, final VerticalList.ItemSelectionState itemSelectionState) {
            DestinationConfirmationHelper.configure(destinationPickerView.confirmationLayout, (DestinationParcelable)this.data.get(itemSelectionState.pos).state);
            destinationPickerView.confirmationLayout.setChoices(DestinationPickerScreen.CONFIRMATION_CHOICES, 0, new ChoiceLayout.IListener() {
                @Override
                public void executeItem(final int n, final int n2) {
                    final DestinationPickerView destinationPickerView = (DestinationPickerView)mortar.Presenter.this.getView();
                    if (destinationPickerView == null) {
                        DestinationPickerScreen.sLogger.v("not alive");
                    }
                    else {
                        switch (n) {
                            case 0: {
                                DestinationPickerScreen.sLogger.v("called presenter select");
                                final DestinationPickerView destinationPickerView2 = (DestinationPickerView)mortar.Presenter.this.getView();
                                if (destinationPickerView2 != null) {
                                    Presenter.this.performAction(destinationPickerView2, itemSelectionState.id, itemSelectionState.pos);
                                    break;
                                }
                                break;
                            }
                            case 1:
                                destinationPickerView.confirmationLayout.setVisibility(GONE);
                                Presenter.this.clearSelection();
                                destinationPickerView.vmenuComponent.verticalList.unlock();
                                break;
                        }
                    }
                }
                
                @Override
                public void itemSelected(final int n, final int n2) {
                }
            });
            destinationPickerView.confirmationLayout.setVisibility(View.VISIBLE);
        }
        
        private void updateView() {
            final DestinationPickerView destinationPickerView = this.getView();
            if (destinationPickerView == null) {
                return;
            }
            Object data = destinationPickerView.getResources();
            while (true) {
                Label_0237: {
                    if (!this.showRouteMap && !this.showDestinationMap) {
                        destinationPickerView.vmenuComponent.setLeftContainerWidth(((Resources)data).getDimensionPixelSize(R.dimen.dest_picker_left_container_w));
                        destinationPickerView.vmenuComponent.setRightContainerWidth(((Resources)data).getDimensionPixelSize(R.dimen.dest_picker_right_container_w));
                        break Label_0237;
                    }
                    destinationPickerView.vmenuComponent.leftContainer.removeAllViews();
                    destinationPickerView.vmenuComponent.leftContainer.addView(LayoutInflater.from(destinationPickerView.getContext()).inflate(R.layout.screen_destination_picker_content, (ViewGroup)destinationPickerView, false));
                    destinationPickerView.vmenuComponent.setLeftContainerWidth(((Resources)data).getDimensionPixelSize(R.dimen.dest_picker_map_left_container_w));
                    destinationPickerView.vmenuComponent.setRightContainerWidth(((Resources)data).getDimensionPixelSize(R.dimen.dest_picker_map_right_container_w));
                    destinationPickerView.vmenuComponent.rightContainer.setBackgroundColor(-16777216);
                    destinationPickerView.vmenuComponent.closeContainer.setBackgroundColor(-16777216);
                    destinationPickerView.setBackgroundColor(-16777216);
                    destinationPickerView.rightBackground.setVisibility(View.VISIBLE);
                    Block_7: {
                        if (this.showRouteMap) {
                            final String routeId = this.destinations[0].routeId;
                            data = null;
                            while (true) {
                                Label_0802: {
                                    if (routeId == null) {
                                        break Label_0802;
                                    }
                                    final HereRouteCache.RouteInfo route = HereRouteCache.getInstance().getRoute(routeId);
                                    if (route == null) {
                                        break Label_0802;
                                    }
                                    data = route.route;
                                    final Object o = route.route.getBoundingBox();
                                    if (o == null && this.start != null) {
                                        break Block_7;
                                    }
                                    break Label_0237;
                                }
                                final Object o = null;
                                continue;
                            }
                        }
                        final Object o = HereNavigationManager.getInstance().getCurrentRoute();
                        this.navigationView.switchToRouteSearchMode(this.start, this.end, this.destinationGeoBBoxList.get(0), (Route)o, false, this.destinationGeoList, this.destinationIcon);
                        break Label_0237;
                    }
                    while (true) {
                    Label_0447:
                        while (true) {
                            Label_0429: {
                                try {
                                    final Object o = new GeoBoundingBox(this.start, 5000.0f, 5000.0f);
                                    this.navigationView.switchToRouteSearchMode(this.start, this.end, (GeoBoundingBox)o, (Route)data, false, null, -1);
                                    if (this.leftTitle != null) {
                                        destinationPickerView.vmenuComponent.selectedText.setText((CharSequence)this.leftTitle);
                                    }
                                    if (this.leftIcon != 0) {
                                        if (this.leftBkColor == 0) {
                                            break Label_0429;
                                        }
                                        destinationPickerView.vmenuComponent.setSelectedIconColorImage(this.leftIcon, this.leftBkColor, null, 1.0f);
                                    }
                                    if (this.destinations == null || this.destinations.length == 0) {
                                        DestinationPickerScreen.sLogger.w("no destinations");
                                        this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_BACK).build());
                                        return;
                                    }
                                    break Label_0447;
                                }
                                catch (Throwable t) {
                                    DestinationPickerScreen.sLogger.e(t);
                                }
                                break Label_0237;
                            }
                            destinationPickerView.vmenuComponent.setSelectedIconImage(this.leftIcon, null, InitialsImageView.Style.DEFAULT);
                            continue;
                        }
                        DestinationPickerScreen.sLogger.v("title:" + this.title);
                        DestinationPickerScreen.sLogger.v("destinations:" + this.destinations.length);
                        data = new ArrayList();
                        ((List<VerticalList.Model>)data).add(TitleViewHolder.buildModel(this.title));
                        for (int i = 0; i < this.destinations.length; ++i) {
                            Object o;
                            if (this.destinations[i].iconUnselected == 0) {
                                o = IconBkColorViewHolder.buildModel(this.destinations[i].id, this.destinations[i].icon, this.destinations[i].iconSelectedColor, this.destinations[i].iconDeselectedColor, this.destinations[i].iconSelectedColor, this.destinations[i].destinationTitle, this.destinations[i].destinationSubTitle, this.destinations[i].destinationSubTitle2);
                            }
                            else {
                                o = IconsTwoViewHolder.buildModel(this.destinations[i].id, this.destinations[i].icon, this.destinations[i].iconUnselected, this.destinations[i].iconSelectedColor, this.destinations[i].iconDeselectedColor, this.destinations[i].destinationTitle, this.destinations[i].destinationSubTitle, this.destinations[i].destinationSubTitle2);
                            }
                            ((VerticalList.Model)o).subTitleFormatted = this.destinations[i].destinationSubTitleFormatted;
                            ((VerticalList.Model)o).subTitle2Formatted = this.destinations[i].destinationSubTitle2Formatted;
                            ((VerticalList.Model)o).state = this.destinations[i];
                            ((List<VerticalList.Model>)data).add((VerticalList.Model)o);
                        }
                        this.data = (List<VerticalList.Model>)data;
                        destinationPickerView.vmenuComponent.updateView(this.data, this.initialSelection, false);
                        return;
                    }
                }
                continue;
            }
        }
        
        void clearSelection() {
            this.closed = false;
            this.handledSelection = false;
        }
        
        void clearState() {
            this.initialSelection = 0;
            this.title = null;
            this.destinations = null;
            this.showRouteMap = false;
            this.showDestinationMap = false;
            this.destinationIcon = -1;
            this.switchBackMode = null;
            this.destinationGeoList = null;
            this.destinationGeoBBoxList = null;
            this.itemSelection = -1;
            this.start = null;
            this.end = null;
            this.data = null;
            this.pickerCallback = null;
            this.doNotAddOriginalRoute = false;
        }
        
        void close() {
            this.close(null);
        }
        
        void close(final Runnable runnable) {
            if (this.closed) {
                DestinationPickerScreen.sLogger.v("already closed");
            }
            else {
                if (this.showRouteMap || this.showDestinationMap) {
                    final DestinationPickerView destinationPickerView = this.getView();
                    if (destinationPickerView != null) {
                        destinationPickerView.setBackgroundColor(-16777216);
                    }
                    if (this.navigationView != null) {
                        this.navigationView.setMapMaskVisibility(0);
                    }
                }
                NotificationManager.getInstance().enableNotifications(true);
                this.closed = true;
                DestinationPickerScreen.sLogger.v("close");
                final DestinationPickerView destinationPickerView2 = this.getView();
                if (destinationPickerView2 != null) {
                    destinationPickerView2.vmenuComponent.animateOut((Animator.AnimatorListener)new DefaultAnimationListener() {
                        @Override
                        public void onAnimationEnd(final Animator animator) {
                            DestinationPickerScreen.sLogger.v("post back");
                            Presenter.this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_BACK).build());
                            if (runnable != null) {
                                runnable.run();
                            }
                            if (Presenter.this.pickerCallback != null) {
                                Presenter.this.pickerCallback.onDestinationPickerClosed();
                            }
                        }
                    });
                }
            }
        }
        
        boolean isAlive() {
            return this.registered;
        }
        
        boolean isClosed() {
            return this.closed;
        }
        
        boolean isShowDestinationMap() {
            return this.showDestinationMap;
        }
        
        boolean isShowingRouteMap() {
            return this.showRouteMap;
        }
        
        boolean itemClicked(final VerticalList.ItemSelectionState itemSelectionState) {
            final boolean b = true;
            DestinationPickerScreen.sLogger.v("itemClicked:" + itemSelectionState.id + "," + itemSelectionState.pos);
            boolean handledSelection;
            if (this.handledSelection) {
                DestinationPickerScreen.sLogger.v("already handled [" + itemSelectionState.id + "], " + itemSelectionState.pos);
                handledSelection = b;
            }
            else {
                this.handledSelection = true;
                final DestinationPickerView destinationPickerView = this.getView();
                handledSelection = b;
                if (destinationPickerView != null) {
                    if (this.showDestinationMap && !this.isNotADestination(itemSelectionState.pos)) {
                        final HereNavigationManager instance = HereNavigationManager.getInstance();
                        if (instance.isNavigationModeOn() && !instance.hasArrived()) {
                            this.showConfirmation(destinationPickerView, itemSelectionState);
                            handledSelection = b;
                            return handledSelection;
                        }
                    }
                    this.performAction(destinationPickerView, itemSelectionState.id, itemSelectionState.pos);
                    handledSelection = this.handledSelection;
                }
            }
            return handledSelection;
        }
        
        void itemSelected(final int itemSelection) {
            AnalyticsSupport.recordNearbySearchDestinationScroll(itemSelection);
            if (this.showRouteMap) {
                final String routeId = this.destinations[itemSelection].routeId;
                final HereRouteCache.RouteInfo route = HereRouteCache.getInstance().getRoute(routeId);
                if (route != null) {
                    this.navigationView.zoomToBoundBox(route.route.getBoundingBox(), route.route, true, true);
                }
                else {
                    DestinationPickerScreen.sLogger.v("itemSelected: route not found:" + routeId);
                }
            }
            else if (this.showDestinationMap) {
                this.navigationView.changeMarkerSelection(itemSelection, this.itemSelection);
                final GeoBoundingBox geoBoundingBox = this.destinationGeoBBoxList.get(itemSelection);
                if (geoBoundingBox != null) {
                    this.navigationView.zoomToBoundBox(geoBoundingBox, null, false, true);
                }
                this.itemSelection = itemSelection;
            }
        }
        
        void itemSelected(final VerticalList.ItemSelectionState itemSelectionState) {
            if (this.data != null) {
                DestinationPickerScreen.sLogger.v("itemSelected:" + itemSelectionState.id + " , " + itemSelectionState.pos);
                if (this.pickerCallback != null && this.pickerCallback.onItemSelected(itemSelectionState.id, itemSelectionState.pos - 1, new DestinationPickerState())) {
                    DestinationPickerScreen.sLogger.v("overridden onItemSelected : " + itemSelectionState.id + "," + itemSelectionState.pos);
                }
                else if (this.isNotADestination(itemSelectionState.pos)) {
                    this.navigationView.changeMarkerSelection(-1, this.itemSelection);
                    this.itemSelection = -1;
                }
                else {
                    this.itemSelected(itemSelectionState.pos - 1);
                }
            }
        }
        
        @Override
        public void onLoad(final Bundle bundle) {
            DestinationPickerScreen.sLogger.v("onLoad");
            NotificationManager.getInstance().enableNotifications(false);
            NotificationManager.getInstance().enablePhoneNotifications(true);
            this.clearState();
            this.reset();
            this.bus.register(this);
            this.registered = true;
            final UIStateManager uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
            this.navigationView = uiStateManager.getNavigationView();
            this.homeScreenView = uiStateManager.getHomescreenView();
            if (bundle != null) {
                this.leftTitle = bundle.getString("PICKER_LEFT_TITLE");
                this.leftIcon = bundle.getInt("PICKER_LEFT_ICON");
                this.leftBkColor = bundle.getInt("PICKER_LEFT_ICON_BKCOLOR");
                this.initialSelection = bundle.getInt("PICKER_INITIAL_SELECTION", 0);
                this.title = bundle.getString("PICKER_TITLE");
                this.destinations = (DestinationParcelable[])bundle.getParcelableArray("PICKER_DESTINATIONS");
                this.hideScreenOnNavStop = bundle.getBoolean("PICKER_HIDE");
                this.showRouteMap = bundle.getBoolean("PICKER_SHOW_ROUTE_MAP");
                if (this.showRouteMap) {
                    DestinationPickerScreen.sLogger.v("show route map");
                    final double double1 = bundle.getDouble("PICKER_MAP_START_LAT");
                    final double double2 = bundle.getDouble("PICKER_MAP_START_LNG");
                    if (double1 != 0.0 && double2 != 0.0) {
                        this.start = new GeoCoordinate(double1, double2);
                    }
                    else {
                        this.start = null;
                    }
                    final double double3 = bundle.getDouble("PICKER_MAP_END_LAT");
                    final double double4 = bundle.getDouble("PICKER_MAP_END_LNG");
                    if (double3 != 0.0 && double4 != 0.0) {
                        this.end = new GeoCoordinate(double3, double4);
                    }
                    else {
                        this.end = null;
                    }
                }
                else {
                    this.showDestinationMap = bundle.getBoolean("PICKER_SHOW_DESTINATION_MAP");
                    if (this.showDestinationMap) {
                        this.destinationIcon = bundle.getInt("PICKER_DESTINATION_ICON", -1);
                        DestinationPickerScreen.sLogger.v("show dest map");
                        this.buildDestinationBBox();
                    }
                    this.start = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                    final NavigationRouteRequest currentNavigationRouteRequest = HereNavigationManager.getInstance().getCurrentNavigationRouteRequest();
                    if (currentNavigationRouteRequest != null) {
                        this.end = new GeoCoordinate(currentNavigationRouteRequest.destination.latitude, currentNavigationRouteRequest.destination.longitude);
                    }
                    else {
                        this.end = null;
                    }
                }
                if (this.showRouteMap || this.showDestinationMap) {
                    this.switchBackMode = this.homeScreenView.getDisplayMode();
                    if (this.switchBackMode == HomeScreen.DisplayMode.MAP) {
                        DestinationPickerScreen.sLogger.v("switchbackmode: null");
                    }
                    else {
                        DestinationPickerScreen.sLogger.v(" onLoad switchbackmode: " + this.switchBackMode);
                        this.homeScreenView.setDisplayMode(HomeScreen.DisplayMode.MAP);
                    }
                }
            }
            if (DestinationPickerScreen.instance != null) {
                this.pickerCallback = (IDestinationPicker)DestinationPickerScreen.instance.arguments2;
                DestinationPickerScreen.instance = null;
            }
            else {
                this.pickerCallback = null;
            }
            this.updateView();
            super.onLoad(bundle);
        }
        
        @Subscribe
        public void onNavigationModeChanged(final MapEvents.NavigationModeChange navigationModeChange) {
            if (this.hideScreenOnNavStop && this.getView() != null) {
                DestinationPickerScreen.sLogger.v("onNavigationModeChanged event[" + navigationModeChange.navigationMode + "]");
                if (navigationModeChange.navigationMode == NavigationMode.MAP) {
                    this.close();
                }
            }
        }
        
        public void onUnload() {
            boolean b = !this.doNotAddOriginalRoute;
            if (!this.showRouteMap) {
                b = true;
            }
            DestinationPickerScreen.sLogger.v("onUnload addbackRoute:" + b);
            DestinationPickerScreen.instance = null;
            this.clearState();
            this.navigationView.switchBackfromRouteSearchMode(b);
            if (this.switchBackMode != null) {
                DestinationPickerScreen.sLogger.v("onUnload switchbackmode: " + this.switchBackMode);
                this.homeScreenView.setDisplayMode(this.switchBackMode);
            }
            NotificationManager.getInstance().enableNotifications(true);
            this.pickerCallback = null;
            if (this.registered) {
                this.registered = false;
                this.bus.unregister(this);
            }
            this.reset();
            super.onUnload();
        }
        
        void reset() {
            this.closed = false;
            this.handledSelection = false;
            this.data = null;
            this.hideScreenOnNavStop = false;
        }
        
        void startMapFluctuator() {
            if (this.registered && this.navigationView != null) {
                this.navigationView.cleanupFluctuator();
                this.navigationView.startFluctuator();
            }
        }
    }
}
