package com.navdy.hud.app.ui.component.carousel;

import android.graphics.RectF;
import android.animation.Animator.AnimatorListener;
import android.animation.Animator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.animation.ValueAnimator;
import android.animation.AnimatorSet;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Paint;
import android.view.View;

public class ProgressIndicator extends View implements IProgressIndicator
{
    private boolean animating;
    private int animatingColor;
    private float animatingPos;
    private int backgroundColor;
    private int barParentSize;
    private int barSize;
    private int blackColor;
    private int currentItem;
    private int currentItemColor;
    private int currentItemPaddingRadius;
    private int defaultColor;
    private boolean fullBackground;
    private int greyColor;
    private int itemCount;
    private int itemPadding;
    private int itemRadius;
    private CarouselIndicator.Orientation orientation;
    private Paint paint;
    private int roundRadius;
    private int viewPadding;
    
    public ProgressIndicator(final Context context) {
        super(context);
        this.barSize = -1;
        this.barParentSize = -1;
        this.currentItem = -1;
        this.currentItemColor = -1;
        this.init(context);
    }
    
    public ProgressIndicator(final Context context, final AttributeSet set) {
        super(context, set);
        this.barSize = -1;
        this.barParentSize = -1;
        this.currentItem = -1;
        this.currentItemColor = -1;
        this.init(context);
    }
    
    private void drawItem(final Canvas canvas, final float n, final float n2, final float n3, final int n4, final int color) {
        this.paint.setColor(this.blackColor);
        if (this.orientation == CarouselIndicator.Orientation.HORIZONTAL) {
            canvas.drawCircle(n3, n2 / 2.0f, (float)n4, this.paint);
        }
        else {
            canvas.drawCircle(n / 2.0f, n3, (float)n4, this.paint);
        }
        this.paint.setColor(color);
        if (this.orientation == CarouselIndicator.Orientation.HORIZONTAL) {
            canvas.drawCircle(n3, n2 / 2.0f, (float)this.itemRadius, this.paint);
        }
        else {
            canvas.drawCircle(n / 2.0f, n3, (float)this.itemRadius, this.paint);
        }
    }
    
    private float getItemTargetPos(float n, final int n2) {
        float n3 = n;
        if (this.fullBackground) {
            n3 = n;
            if (this.viewPadding > 0) {
                n3 = n - this.viewPadding * 2;
            }
        }
        if (this.itemCount <= 0) {
            n = 0.0f;
        }
        else if (this.itemCount == 1) {
            n = n3 - (this.itemRadius + this.currentItemPaddingRadius);
        }
        else {
            n = this.itemRadius + this.currentItemPaddingRadius;
            final float n4 = n3 - (this.itemRadius + this.currentItemPaddingRadius);
            float n5;
            if (n2 == 0) {
                n5 = this.itemRadius + this.currentItemPaddingRadius;
            }
            else if (n2 == this.itemCount - 1) {
                n5 = n3 - (this.itemRadius + this.currentItemPaddingRadius);
            }
            else {
                n5 = n2 * (n3 / (this.itemCount - 1));
            }
            if (n5 >= n) {
                n = n5;
                if (n5 > n4) {
                    n = n4;
                }
            }
        }
        return n;
    }
    
    private void init(final Context context) {
        final Resources resources = this.getResources();
        this.blackColor = resources.getColor(R.color.black);
        this.greyColor = resources.getColor(R.color.grey_4a);
        (this.paint = new Paint()).setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
    }
    
    public int getCurrentItem() {
        return this.currentItem;
    }

    public AnimatorSet getItemMoveAnimator(int toPos, int color) {
        float f;
        float width = (float) getWidth();
        float height = (float) getHeight();
        if (this.orientation == CarouselIndicator.Orientation.HORIZONTAL) {
            f = width;
        } else {
            f = height;
        }
        float currentPos = getItemTargetPos(f, this.currentItem);
        if (this.orientation == CarouselIndicator.Orientation.HORIZONTAL) {
            f = width;
        } else {
            f = height;
        }
        float targetPos = getItemTargetPos(f, toPos);
        if (this.fullBackground) {
            if (this.orientation != CarouselIndicator.Orientation.HORIZONTAL) {
                width = height;
            }
            int lastItem = (int) getItemTargetPos(width, this.itemCount - 1);
            int newPos = ((int) targetPos) + this.viewPadding;
            if (newPos < lastItem - this.viewPadding) {
                targetPos = (float) newPos;
            } else {
                targetPos = (float) (lastItem - this.viewPadding);
            }
        }
        ValueAnimator animator = ValueAnimator.ofFloat(new float[]{currentPos, targetPos});
        animator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                ProgressIndicator.this.animatingPos = ((Float) animation.getAnimatedValue()).floatValue();
                ProgressIndicator.this.invalidate();
            }
        });
        animator.addListener(new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animation) {
            }
        });
        AnimatorSet set = new AnimatorSet();
        set.play(animator);
        this.animatingPos = currentPos;
        this.animatingColor = color;
        this.animating = true;
        return set;
    }
    
    public RectF getItemPos(final int n) {
        RectF rectF;
        if (n >= 0 && n < this.itemCount) {
            float n2 = this.getWidth();
            final float n3 = this.getHeight();
            if (this.orientation != CarouselIndicator.Orientation.HORIZONTAL) {
                n2 = n3;
            }
            final float itemTargetPos = this.getItemTargetPos(n2, this.currentItem);
            float n4;
            float n5;
            if (this.orientation == CarouselIndicator.Orientation.HORIZONTAL) {
                n4 = 0.0f;
                n5 = itemTargetPos;
            }
            else {
                n5 = 0.0f;
                n4 = itemTargetPos;
            }
            rectF = new RectF(n5, n4, 0.0f, 0.0f);
        }
        else {
            rectF = null;
        }
        return rectF;
    }
    
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        final float n = this.getWidth();
        final float n2 = this.getHeight();
        final boolean b = false;
        final boolean b2 = false;
        final float n3 = n;
        float n5;
        final float n4 = n5 = n2;
        float n6 = n3;
        int n7 = b ? 1 : 0;
        int n8 = b2 ? 1 : 0;
        if (this.barSize != -1) {
            if (this.orientation == CarouselIndicator.Orientation.VERTICAL) {
                n7 = (int)(n - this.barSize) / 2;
                n6 = this.barSize;
                n8 = (b2 ? 1 : 0);
                n5 = n4;
            }
            else {
                n8 = (int)(n - this.barSize) / 2;
                n5 = this.barSize;
                n6 = n3;
                n7 = (b ? 1 : 0);
            }
        }
        this.paint.setColor(this.backgroundColor);
        if (this.itemCount == 0) {
            canvas.drawRoundRect(new RectF((float)n7, (float)n8, n7 + n6, n8 + n5), (float)this.roundRadius, (float)this.roundRadius, this.paint);
        }
        else {
            if (this.itemCount == 1) {
                canvas.drawRoundRect(new RectF((float)n7, (float)n8, n7 + n6, n8 + n5), (float)this.roundRadius, (float)this.roundRadius, this.paint);
            }
            else if (this.fullBackground) {
                canvas.drawRoundRect(new RectF((float)n7, (float)n8, n7 + n6, n8 + n5), (float)this.roundRadius, (float)this.roundRadius, this.paint);
            }
            else {
                float n9;
                float n10;
                RectF rectF;
                if (this.orientation == CarouselIndicator.Orientation.HORIZONTAL) {
                    n9 = this.itemRadius;
                    n10 = n2 / 2.0f;
                    rectF = new RectF((float)(this.itemRadius * 2 + this.itemPadding), 0.0f, n, n2);
                }
                else {
                    n9 = n / 2.0f;
                    n10 = this.itemRadius;
                    rectF = new RectF(0.0f, (float)(this.itemRadius * 2 + this.itemPadding), n, n2);
                }
                canvas.drawCircle(n9, n10, (float)this.itemRadius, this.paint);
                canvas.drawRoundRect(rectF, (float)this.roundRadius, (float)this.roundRadius, this.paint);
            }
            final int n11 = this.currentItemPaddingRadius + this.itemRadius;
            if (this.animating) {
                this.drawItem(canvas, n, n2, this.animatingPos, n11, this.animatingColor);
            }
            else if (this.currentItem != -1) {
                float n12;
                if (this.orientation == CarouselIndicator.Orientation.HORIZONTAL) {
                    n12 = n;
                }
                else {
                    n12 = n2;
                }
                float itemTargetPos;
                final float n13 = itemTargetPos = this.getItemTargetPos(n12, this.currentItem);
                if (this.fullBackground) {
                    itemTargetPos = n13;
                    if (this.viewPadding > 0) {
                        if (this.currentItem == 0) {
                            itemTargetPos = this.viewPadding;
                        }
                        else if (this.currentItem == this.itemCount - 1) {
                            itemTargetPos = this.getHeight() - this.viewPadding;
                        }
                        else {
                            final int n14 = this.getHeight() - this.viewPadding;
                            final int n15 = (int)n13 + this.viewPadding;
                            if (n15 <= n14) {
                                itemTargetPos = n15;
                            }
                            else {
                                itemTargetPos = n14;
                            }
                        }
                    }
                }
                int n16 = this.defaultColor;
                if (this.currentItemColor != -1) {
                    n16 = this.currentItemColor;
                }
                this.drawItem(canvas, n, n2, itemTargetPos, n11, n16);
            }
        }
    }
    
    protected void onMeasure(int n, int n2) {
        super.onMeasure(n, n2);
        if (this.barSize != -1) {
            n = this.getMeasuredWidth();
            n2 = this.getMeasuredHeight();
            if (this.orientation == CarouselIndicator.Orientation.VERTICAL) {
                n = this.barParentSize;
            }
            else {
                n2 = this.barParentSize;
            }
            this.setMeasuredDimension(n, n2);
        }
    }
    
    public void setBackgroundColor(final int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }
    
    public void setCurrentItem(final int n) {
        this.setCurrentItem(n, -1);
    }
    
    public void setCurrentItem(final int currentItem, final int currentItemColor) {
        if (currentItem >= 0 && currentItem <= this.itemCount - 1) {
            this.currentItem = currentItem;
            this.currentItemColor = currentItemColor;
            this.animating = false;
            this.animatingPos = -1.0f;
            this.animatingColor = -1;
            this.invalidate();
        }
    }
    
    public void setItemCount(final int itemCount) {
        if (itemCount <= 0) {
            throw new IllegalArgumentException();
        }
        this.currentItem = -1;
        this.itemCount = itemCount;
    }
    
    public void setOrientation(final CarouselIndicator.Orientation orientation) {
        this.orientation = orientation;
    }
    
    public void setProperties(final int roundRadius, final int itemRadius, final int itemPadding, final int currentItemPaddingRadius, final int defaultColor, final int backgroundColor, final boolean fullBackground, final int viewPadding, final int barSize, final int barParentSize) {
        this.roundRadius = roundRadius;
        this.itemRadius = itemRadius;
        this.itemPadding = itemPadding;
        this.viewPadding = viewPadding;
        this.currentItemPaddingRadius = currentItemPaddingRadius;
        this.defaultColor = defaultColor;
        this.barSize = barSize;
        this.barParentSize = barParentSize;
        if (backgroundColor != -1) {
            this.backgroundColor = backgroundColor;
        }
        else {
            this.backgroundColor = this.greyColor;
        }
        this.fullBackground = fullBackground;
        if (this.fullBackground && viewPadding > 0) {
            this.viewPadding += itemRadius;
        }
    }
}
