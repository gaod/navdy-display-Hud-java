package com.navdy.hud.app.ui.component.destination;

import com.navdy.hud.app.ui.component.image.InitialsImageView;
import android.graphics.Shader;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import android.widget.TextView;
import android.text.Html;
import android.text.TextUtils;
import com.navdy.hud.app.util.ViewUtil;
import com.navdy.hud.app.view.MaxWidthLinearLayout;
import com.navdy.hud.app.framework.destinations.Destination;
import com.navdy.hud.app.ui.component.ConfirmationLayout;

public class DestinationConfirmationHelper
{
    private static float[] FONT_SIZES;
    private static int[] MAX_LINES;
    
    static {
        DestinationConfirmationHelper.MAX_LINES = new int[] { 1, 1, 2, 2 };
        DestinationConfirmationHelper.FONT_SIZES = new float[] { 30.0f, 26.0f, 26.0f, 22.0f };
    }
    
    private static void configure(final ConfirmationLayout confirmationLayout, final Destination destination, final String text, final CharSequence charSequence, final boolean b, final boolean b2, int maxWidth, int paddingStart) {
        setImage(confirmationLayout, destination, b2, maxWidth, paddingStart);
        confirmationLayout.screenTitle.setText(R.string.destination_change_navigation_title);
        confirmationLayout.title1.setVisibility(GONE);
        final TextView title2 = confirmationLayout.title2;
        final TextView title3 = confirmationLayout.title3;
        final MaxWidthLinearLayout maxWidthLinearLayout = (MaxWidthLinearLayout)confirmationLayout.findViewById(R.id.infoContainer);
        paddingStart = maxWidthLinearLayout.getPaddingStart();
        final int paddingEnd = maxWidthLinearLayout.getPaddingEnd();
        maxWidth = maxWidthLinearLayout.getMaxWidth();
        title2.setText((CharSequence)text);
        final int[] array = new int[2];
        ViewUtil.autosize(title2, DestinationConfirmationHelper.MAX_LINES, maxWidth - (paddingStart + paddingEnd), DestinationConfirmationHelper.FONT_SIZES, array);
        title2.setMaxLines(array[1]);
        Object fromHtml = charSequence;
        if (!TextUtils.isEmpty(charSequence)) {
            fromHtml = charSequence;
            if (b) {
                fromHtml = Html.fromHtml((String)charSequence);
            }
        }
        ViewUtil.applyTextAndStyle(title3, (CharSequence)fromHtml, 2, R.style.destination_subtitle_two_line);
        title2.setVisibility(View.VISIBLE);
        title3.setVisibility(View.VISIBLE);
    }
    
    public static void configure(final ConfirmationLayout confirmationLayout, final DestinationParcelable destinationParcelable) {
        configure(confirmationLayout, null, destinationParcelable.destinationTitle, destinationParcelable.destinationSubTitle, destinationParcelable.destinationSubTitleFormatted, destinationParcelable.iconUnselected == 0, destinationParcelable.icon, destinationParcelable.iconSelectedColor);
    }
    
    public static void configure(final ConfirmationLayout confirmationLayout, final VerticalList.Model model, final Destination destination) {
        configure(confirmationLayout, destination, destination.destinationTitle, destination.destinationSubtitle, false, model.type == VerticalList.ModelType.ICON_BKCOLOR, model.icon, model.iconSelectedColor);
    }
    
    private static void setImage(final ConfirmationLayout confirmationLayout, final Destination destination, final boolean b, final int imageResource, final int n) {
        if (b) {
            confirmationLayout.screenImageIconBkColor.setIcon(imageResource, n, null, 1.25f);
            confirmationLayout.screenImage.setVisibility(GONE);
            confirmationLayout.screenImageIconBkColor.setVisibility(View.VISIBLE);
        }
        else {
            setInitials(destination, confirmationLayout.screenImage);
            confirmationLayout.screenImage.setImageResource(imageResource);
            confirmationLayout.screenImage.setVisibility(View.VISIBLE);
            confirmationLayout.screenImageIconBkColor.setVisibility(GONE);
        }
    }
    
    private static void setInitials(final Destination destination, final InitialsImageView initialsImageView) {
        if (destination != null && !TextUtils.isEmpty((CharSequence)destination.initials)) {
            InitialsImageView.Style style;
            if (destination.isInitialNumber) {
                if (destination.initials.length() <= 3) {
                    style = InitialsImageView.Style.LARGE;
                }
                else {
                    style = InitialsImageView.Style.MEDIUM;
                }
            }
            else if (destination.initials.length() <= 2) {
                style = InitialsImageView.Style.LARGE;
            }
            else {
                style = InitialsImageView.Style.MEDIUM;
            }
            initialsImageView.setInitials(destination.initials, style);
        }
        else {
            initialsImageView.setInitials(null, InitialsImageView.Style.DEFAULT);
        }
    }
}
