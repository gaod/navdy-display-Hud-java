package com.navdy.hud.app.ui.component.mainmenu;

import android.graphics.Shader;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import android.view.View;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.homescreen.SmartDashView;
import com.navdy.service.library.events.preferences.LocalPreferences;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.maps.here.HereMapsManager;
import java.util.ArrayList;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder;
import com.navdy.hud.app.device.gps.GpsUtils;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.debug.DriveRecorder;
import java.util.List;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import android.content.res.Resources;
import com.navdy.hud.app.ui.component.vlist.VerticalList;

public class MainOptionsMenu implements IMenu
{
    private static final VerticalList.Model autoZoom;
    private static final VerticalList.Model back;
    private static final int dashColor;
    private static final String dashTitle;
    private static final VerticalList.Model manualZoom;
    private static final int mapColor;
    private static final String mapTitle;
    private static final VerticalList.Model pauseDemo;
    private static final VerticalList.Model playDemo;
    private static final VerticalList.Model rawGps;
    private static final Resources resources;
    private static final VerticalList.Model restartDemo;
    private static final Logger sLogger;
    private static final VerticalList.Model scrollLeftGauge;
    private static final VerticalList.Model scrollRightGauge;
    private static final VerticalList.Model selectCenterGauge;
    private static final VerticalList.Model sideGauges;
    private int backSelection;
    private Bus bus;
    private List<VerticalList.Model> cachedList;
    private DashGaugeConfiguratorMenu dashGaugeConfiguratorMenu;
    private DriveRecorder driveRecorder;
    private DriverProfileManager driverProfileManager;
    private Mode mode;
    private IMenu parent;
    private MainMenuScreen2.Presenter presenter;
    private UIStateManager uiStateManager;
    private VerticalMenuComponent vscrollComponent;
    
    static {
        sLogger = new Logger(MainOptionsMenu.class);
        resources = HudApplication.getAppContext().getResources();
        final int color = MainOptionsMenu.resources.getColor(R.color.mm_back);
        dashColor = MainOptionsMenu.resources.getColor(R.color.mm_dash_options);
        mapColor = MainOptionsMenu.resources.getColor(R.color.mm_map_options);
        dashTitle = MainOptionsMenu.resources.getString(R.string.carousel_menu_smartdash_options);
        mapTitle = MainOptionsMenu.resources.getString(R.string.carousel_menu_map_options);
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, color, MainMenu.bkColorUnselected, color, MainOptionsMenu.resources.getString(R.string.back), null);
        final String string = MainOptionsMenu.resources.getString(R.string.manual_zoom);
        final int color2 = MainOptionsMenu.resources.getColor(R.color.mm_options_manual_zoom);
        manualZoom = IconBkColorViewHolder.buildModel(R.id.main_menu_options_manual_zoom, R.drawable.icon_options_map_zoom_manual_2, color2, MainMenu.bkColorUnselected, color2, string, null);
        final String string2 = MainOptionsMenu.resources.getString(R.string.show_raw_gps);
        final int color3 = MainOptionsMenu.resources.getColor(R.color.mm_options_manual_zoom);
        final boolean enabled = GpsUtils.SHOW_RAW_GPS.isEnabled();
        final SwitchViewHolder.Companion companion = SwitchViewHolder.Companion;
        final Resources resources2 = MainOptionsMenu.resources;
        int n;
        if (enabled) {
            n = R.string.si_enabled;
        }
        else {
            n = R.string.si_disabled;
        }
        rawGps = companion.buildModel(R.id.main_menu_options_raw_gps, color3, string2, resources2.getString(n), enabled, true);
        final String string3 = MainOptionsMenu.resources.getString(R.string.auto_zoom);
        final int color4 = MainOptionsMenu.resources.getColor(R.color.mm_options_auto_zoom);
        autoZoom = IconBkColorViewHolder.buildModel(R.id.main_menu_options_auto_zoom, R.drawable.icon_options_map_zoom_auto_2, color4, MainMenu.bkColorUnselected, color4, string3, null);
        final String string4 = MainOptionsMenu.resources.getString(R.string.play_demo);
        final int color5 = MainOptionsMenu.resources.getColor(R.color.mm_options_demo);
        playDemo = IconBkColorViewHolder.buildModel(R.id.main_menu_options_play_demo, R.drawable.icon_mm_demo_play_2, color5, MainMenu.bkColorUnselected, color5, string4, null);
        final String string5 = MainOptionsMenu.resources.getString(R.string.pause_demo);
        final int color6 = MainOptionsMenu.resources.getColor(R.color.mm_options_demo);
        pauseDemo = IconBkColorViewHolder.buildModel(R.id.main_menu_options_pause_demo, R.drawable.icon_mm_demo_pause_2, color6, MainMenu.bkColorUnselected, color6, string5, null);
        final String string6 = MainOptionsMenu.resources.getString(R.string.restart_demo);
        final int color7 = MainOptionsMenu.resources.getColor(R.color.mm_options_demo);
        restartDemo = IconBkColorViewHolder.buildModel(R.id.main_menu_options_restart_demo, R.drawable.icon_mm_demo_restart_2, color7, MainMenu.bkColorUnselected, color7, string6, null);
        final String string7 = MainOptionsMenu.resources.getString(R.string.scroll_left_gauge);
        final int color8 = MainOptionsMenu.resources.getColor(R.color.mm_options_scroll_gauge);
        scrollLeftGauge = IconBkColorViewHolder.buildModel(R.id.main_menu_options_scroll_left, R.drawable.icon_options_dash_scroll_left_2, color8, MainMenu.bkColorUnselected, color8, string7, null);
        final String string8 = MainOptionsMenu.resources.getString(R.string.scroll_right_gauge);
        final int color9 = MainOptionsMenu.resources.getColor(R.color.mm_options_scroll_gauge);
        scrollRightGauge = IconBkColorViewHolder.buildModel(R.id.main_menu_options_scroll_right, R.drawable.icon_options_dash_scroll_right_2, color9, MainMenu.bkColorUnselected, color9, string8, null);
        final String string9 = MainOptionsMenu.resources.getString(R.string.carousel_menu_smartdash_select_center_gauge);
        final int color10 = MainOptionsMenu.resources.getColor(R.color.mm_options_scroll_gauge);
        selectCenterGauge = IconBkColorViewHolder.buildModel(R.id.main_menu_options_select_center_gauge, R.drawable.icon_center_gauge, color10, MainMenu.bkColorUnselected, color10, string9, null);
        final String string10 = MainOptionsMenu.resources.getString(R.string.carousel_menu_smartdash_side_gauges);
        final int color11 = MainOptionsMenu.resources.getColor(R.color.mm_options_scroll_gauge);
        sideGauges = IconBkColorViewHolder.buildModel(R.id.main_menu_options_side_gauges, R.drawable.icon_side_gauges, color11, MainMenu.bkColorUnselected, color11, string10, null);
    }
    
    MainOptionsMenu(final Bus bus, final VerticalMenuComponent vscrollComponent, final MainMenuScreen2.Presenter presenter, final IMenu parent) {
        this.bus = bus;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
        final RemoteDeviceManager instance = RemoteDeviceManager.getInstance();
        this.driveRecorder = instance.getDriveRecorder();
        this.driverProfileManager = DriverProfileHelper.getInstance().getDriverProfileManager();
        this.uiStateManager = instance.getUiStateManager();
        this.dashGaugeConfiguratorMenu = new DashGaugeConfiguratorMenu(bus, instance.getSharedPreferences(), this.vscrollComponent, this.presenter, this);
    }
    
    @Override
    public IMenu getChildMenu(final IMenu menu, final String s, final String s2) {
        return null;
    }
    
    @Override
    public int getInitialSelection() {
        return 1;
    }
    
    @Override
    public List<VerticalList.Model> getItems() {
        final ArrayList<VerticalList.Model> cachedList = new ArrayList<VerticalList.Model>();
        cachedList.add(MainOptionsMenu.back);
        switch (this.mode) {
            case MAP: {
                if (!HereMapsManager.getInstance().isInitialized()) {
                    break;
                }
                final LocalPreferences localPreferences = this.driverProfileManager.getLocalPreferences();
                if (localPreferences.manualZoom != null && localPreferences.manualZoom) {
                    cachedList.add(MainOptionsMenu.autoZoom);
                }
                else {
                    cachedList.add(MainOptionsMenu.manualZoom);
                }
                if (!DeviceUtil.isUserBuild()) {
                    cachedList.add(MainOptionsMenu.rawGps);
                }
                final boolean demoAvailable = this.driveRecorder.isDemoAvailable();
                final boolean demoPlaying = this.driveRecorder.isDemoPlaying();
                if (!demoAvailable) {
                    break;
                }
                if (!demoPlaying) {
                    cachedList.add(MainOptionsMenu.playDemo);
                    break;
                }
                cachedList.add(MainOptionsMenu.pauseDemo);
                cachedList.add(MainOptionsMenu.restartDemo);
                break;
            }
            case DASH: {
                final SmartDashView smartDashView = this.uiStateManager.getSmartDashView();
                if (smartDashView != null) {
                    switch (smartDashView.getCurrentScrollableSideOption()) {
                        case 0:
                            cachedList.add(MainOptionsMenu.scrollLeftGauge);
                            break;
                        case 1:
                            cachedList.add(MainOptionsMenu.scrollRightGauge);
                            break;
                    }
                }
                cachedList.add(MainOptionsMenu.selectCenterGauge);
                cachedList.add(MainOptionsMenu.sideGauges);
                break;
            }
        }
        return this.cachedList = cachedList;
    }
    
    @Override
    public VerticalList.Model getModelfromPos(final int n) {
        VerticalList.Model model;
        if (this.cachedList != null && this.cachedList.size() > n) {
            model = this.cachedList.get(n);
        }
        else {
            model = null;
        }
        return model;
    }
    
    @Override
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    @Override
    public Menu getType() {
        return Menu.MAIN_OPTIONS;
    }
    
    @Override
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    @Override
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    @Override
    public boolean isItemClickable(final int n, final int n2) {
        return true;
    }
    
    @Override
    public void onBindToView(final VerticalList.Model model, final View view, final int n, final VerticalList.ModelState modelState) {
    }
    
    @Override
    public void onFastScrollEnd() {
    }
    
    @Override
    public void onFastScrollStart() {
    }
    
    @Override
    public void onItemSelected(final VerticalList.ItemSelectionState itemSelectionState) {
    }
    
    @Override
    public void onScrollIdle() {
    }
    
    @Override
    public void onUnload(final MenuLevel menuLevel) {
    }
    
    @Override
    public boolean selectItem(final VerticalList.ItemSelectionState itemSelectionState) {
        boolean b = true;
        final boolean b2 = false;
        MainOptionsMenu.sLogger.v("select id:" + itemSelectionState.id + " pos:" + itemSelectionState.pos);
        switch (itemSelectionState.id) {
            case R.id.menu_back:
                MainOptionsMenu.sLogger.v("back");
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                break;
            case R.id.main_menu_options_raw_gps: {
                MainOptionsMenu.sLogger.i("Toggling raw gps");
                final VerticalList.Model model = itemSelectionState.model;
                if (model.isOn) {
                    b = false;
                }
                model.isOn = b;
                final Resources resources = MainOptionsMenu.resources;
                int n;
                if (b) {
                    n = R.string.si_enabled;
                }
                else {
                    n = R.string.si_disabled;
                }
                model.subTitle = resources.getString(n);
                GpsUtils.SHOW_RAW_GPS.setEnabled(b);
                this.presenter.refreshDataforPos(itemSelectionState.pos);
                return b2;
            }
            case R.id.main_menu_options_manual_zoom:
                this.presenter.performSelectionAnimation(new Runnable() {
                    @Override
                    public void run() {
                        MainOptionsMenu.this.presenter.close();
                        final LocalPreferences localPreferences = MainOptionsMenu.this.driverProfileManager.getLocalPreferences();
                        AnalyticsSupport.recordOptionSelection("Manual_Zoom");
                        MainOptionsMenu.this.driverProfileManager.updateLocalPreferences(new LocalPreferences.Builder(localPreferences).manualZoom(true).manualZoomLevel(-1.0f).build());
                    }
                });
                break;
            case R.id.main_menu_options_auto_zoom:
                this.presenter.performSelectionAnimation(new Runnable() {
                    @Override
                    public void run() {
                        MainOptionsMenu.this.presenter.close();
                        final LocalPreferences localPreferences = MainOptionsMenu.this.driverProfileManager.getLocalPreferences();
                        AnalyticsSupport.recordOptionSelection("Auto_Zoom");
                        MainOptionsMenu.this.driverProfileManager.updateLocalPreferences(new LocalPreferences.Builder(localPreferences).manualZoom(false).manualZoomLevel(-1.0f).build());
                    }
                });
                break;
            case R.id.main_menu_options_play_demo:
                this.presenter.performSelectionAnimation(new Runnable() {
                    @Override
                    public void run() {
                        MainOptionsMenu.this.presenter.close();
                        if (MainOptionsMenu.this.driveRecorder.isDemoPaused()) {
                            MainOptionsMenu.this.driveRecorder.performDemoPlaybackAction(DriveRecorder.Action.RESUME, true);
                        }
                        else {
                            MainOptionsMenu.this.driveRecorder.performDemoPlaybackAction(DriveRecorder.Action.PLAY, true);
                        }
                    }
                });
                break;
            case R.id.main_menu_options_pause_demo:
                this.presenter.performSelectionAnimation(new Runnable() {
                    @Override
                    public void run() {
                        MainOptionsMenu.this.presenter.close();
                        MainOptionsMenu.this.driveRecorder.performDemoPlaybackAction(DriveRecorder.Action.PAUSE, true);
                    }
                });
                break;
            case R.id.main_menu_options_restart_demo:
                this.presenter.performSelectionAnimation(new Runnable() {
                    @Override
                    public void run() {
                        MainOptionsMenu.this.presenter.close();
                        MainOptionsMenu.this.driveRecorder.performDemoPlaybackAction(DriveRecorder.Action.RESTART, true);
                    }
                });
                break;
            case R.id.main_menu_options_scroll_left:
            case R.id.main_menu_options_scroll_right:
                this.presenter.performSelectionAnimation(new Runnable() {
                    @Override
                    public void run() {
                        MainOptionsMenu.this.presenter.close();
                        final SmartDashView smartDashView = MainOptionsMenu.this.uiStateManager.getSmartDashView();
                        if (smartDashView != null) {
                            smartDashView.onScrollableSideOptionSelected();
                        }
                    }
                });
                break;
            case R.id.main_menu_options_select_center_gauge:
                this.dashGaugeConfiguratorMenu.setGaugeType(DashGaugeConfiguratorMenu.GaugeType.CENTER);
                this.presenter.loadMenu(this.dashGaugeConfiguratorMenu, MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
                break;
            case R.id.main_menu_options_side_gauges:
                this.dashGaugeConfiguratorMenu.setGaugeType(DashGaugeConfiguratorMenu.GaugeType.SIDE);
                this.presenter.loadMenu(this.dashGaugeConfiguratorMenu, MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
                break;
        }
        return true;
    }
    
    @Override
    public void setBackSelectionId(final int n) {
    }
    
    @Override
    public void setBackSelectionPos(final int backSelection) {
        this.backSelection = backSelection;
    }
    
    void setMode(final Mode mode) {
        this.mode = mode;
    }
    
    @Override
    public void setSelectedIcon() {
        int n = 0;
        int n2 = 0;
        CharSequence text = null;
        switch (this.mode) {
            case MAP:
                n = R.drawable.icon_main_menu_map_options;
                n2 = MainOptionsMenu.mapColor;
                text = MainOptionsMenu.mapTitle;
                break;
            case DASH:
                n = R.drawable.icon_main_menu_dash_options;
                n2 = MainOptionsMenu.dashColor;
                text = MainOptionsMenu.dashTitle;
                break;
        }
        this.vscrollComponent.setSelectedIconColorImage(n, n2, null, 1.0f);
        this.vscrollComponent.selectedText.setText(text);
    }
    
    @Override
    public void showToolTip() {
    }
    
    enum Mode
    {
        DASH, 
        MAP;
    }
}
