package com.navdy.hud.app.ui.component.homescreen;

import com.navdy.hud.app.view.DriveScoreGaugePresenter;
import com.navdy.hud.app.view.EngineTemperaturePresenter;
import com.navdy.hud.app.view.GForcePresenter;
import com.navdy.hud.app.view.SpeedLimitSignPresenter;
import com.navdy.hud.app.view.MusicWidgetPresenter;
import com.navdy.hud.app.view.MPGGaugePresenter;
import com.navdy.hud.app.view.ETAGaugePresenter;
import com.navdy.hud.app.view.CalendarWidgetPresenter;
import com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter;
import com.navdy.hud.app.view.ClockWidgetPresenter;
import com.navdy.hud.app.view.CompassPresenter;
import com.navdy.hud.app.view.FuelGaugePresenter2;
import com.navdy.hud.app.view.EmptyGaugePresenter;
import com.navdy.hud.app.view.DashboardWidgetPresenter;
import android.content.Context;
import java.util.HashMap;

public class DashWidgetPresenterFactory
{
    private static final HashMap<String, Integer> RESOURCE_MAP;
    
    static {
        (RESOURCE_MAP = new HashMap<String, Integer>()).put("EMPTY_WIDGET", 0);
        DashWidgetPresenterFactory.RESOURCE_MAP.put("MPG_GRAPH_WIDGET", R.drawable.asset_dash20_proto_gauge_mpg);
        DashWidgetPresenterFactory.RESOURCE_MAP.put("MPG_AVG_WIDGET", R.drawable.asset_dash20_proto_gauge_clock_analog);
        DashWidgetPresenterFactory.RESOURCE_MAP.put("WEATHER_GRAPH_WIDGET", R.drawable.asset_dash20_proto_gauge_weather);
    }
    
    public static DashboardWidgetPresenter createDashWidgetPresenter(final Context context, final String s) {
        DashboardWidgetPresenter dashboardWidgetPresenter = null;
        switch (s) {
            default:
                dashboardWidgetPresenter = new ImageWidgetPresenter(context, DashWidgetPresenterFactory.RESOURCE_MAP.get(s), s);
                break;
            case "EMPTY_WIDGET":
                dashboardWidgetPresenter = new EmptyGaugePresenter(context);
                break;
            case "FUEL_GAUGE_ID":
                dashboardWidgetPresenter = new FuelGaugePresenter2(context);
                break;
            case "COMPASS_WIDGET":
                dashboardWidgetPresenter = new CompassPresenter(context);
                break;
            case "ANALOG_CLOCK_WIDGET":
                dashboardWidgetPresenter = new ClockWidgetPresenter(context, ClockWidgetPresenter.ClockType.ANALOG);
                break;
            case "DIGITAL_CLOCK_WIDGET":
                dashboardWidgetPresenter = new ClockWidgetPresenter(context, ClockWidgetPresenter.ClockType.DIGITAL1);
                break;
            case "DIGITAL_CLOCK_2_WIDGET":
                dashboardWidgetPresenter = new ClockWidgetPresenter(context, ClockWidgetPresenter.ClockType.DIGITAL2);
                break;
            case "TRAFFIC_INCIDENT_GAUGE_ID":
                dashboardWidgetPresenter = new TrafficIncidentWidgetPresenter();
                break;
            case "CALENDAR_WIDGET":
                dashboardWidgetPresenter = new CalendarWidgetPresenter(context);
                break;
            case "ETA_GAUGE_ID":
                dashboardWidgetPresenter = new ETAGaugePresenter(context);
                break;
            case "MPG_AVG_WIDGET":
                dashboardWidgetPresenter = new MPGGaugePresenter(context);
                break;
            case "MUSIC_WIDGET":
                dashboardWidgetPresenter = new MusicWidgetPresenter(context);
                break;
            case "SPEED_LIMIT_SIGN_GAUGE_ID":
                dashboardWidgetPresenter = new SpeedLimitSignPresenter(context);
                break;
            case "GFORCE_WIDGET":
                dashboardWidgetPresenter = new GForcePresenter(context);
                break;
            case "ENGINE_TEMPERATURE_GAUGE_ID":
                dashboardWidgetPresenter = new EngineTemperaturePresenter(context);
                break;
            case "DRIVE_SCORE_GAUGE_ID":
                dashboardWidgetPresenter = new DriveScoreGaugePresenter(context, R.layout.drive_score_gauge_layout, true);
                break;
        }
        return dashboardWidgetPresenter;
    }
}
