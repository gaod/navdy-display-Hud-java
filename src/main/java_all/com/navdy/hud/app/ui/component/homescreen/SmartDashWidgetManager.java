package com.navdy.hud.app.ui.component.homescreen;

import com.navdy.hud.app.view.SpeedLimitSignPresenter;
import com.navdy.hud.app.view.MPGGaugePresenter;
import com.navdy.hud.app.view.CompassPresenter;
import com.navdy.hud.app.view.EngineTemperaturePresenter;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.view.DriveScoreGaugePresenter;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.view.FuelGaugePresenter2;
import java.util.Collection;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.view.DashboardWidgetPresenter;
import android.text.TextUtils;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashMap;
import android.content.Context;
import android.content.SharedPreferences;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import java.util.HashSet;
import java.util.List;

public class SmartDashWidgetManager
{
    public static final String ANALOG_CLOCK_WIDGET_ID = "ANALOG_CLOCK_WIDGET";
    public static final String CALENDAR_WIDGET_ID = "CALENDAR_WIDGET";
    public static final String COMPASS_WIDGET_ID = "COMPASS_WIDGET";
    public static final String DIGITAL_CLOCK_2_WIDGET_ID = "DIGITAL_CLOCK_2_WIDGET";
    public static final String DIGITAL_CLOCK_WIDGET_ID = "DIGITAL_CLOCK_WIDGET";
    public static final String DRIVE_SCORE_GAUGE_ID = "DRIVE_SCORE_GAUGE_ID";
    public static final String EMPTY_WIDGET_ID = "EMPTY_WIDGET";
    public static final String ENGINE_TEMPERATURE_GAUGE_ID = "ENGINE_TEMPERATURE_GAUGE_ID";
    public static final String ETA_GAUGE_ID = "ETA_GAUGE_ID";
    public static final String FUEL_GAUGE_ID = "FUEL_GAUGE_ID";
    private static List<String> GAUGES;
    private static HashSet<String> GAUGE_NAME_LOOKUP;
    public static final String GFORCE_WIDGET_ID = "GFORCE_WIDGET";
    public static final String MPG_AVG_WIDGET_ID = "MPG_AVG_WIDGET";
    public static final String MPG_GRAPH_WIDGET_ID = "MPG_GRAPH_WIDGET";
    public static final String MUSIC_WIDGET_ID = "MUSIC_WIDGET";
    public static final String PREFERENCE_GAUGE_ENABLED = "PREF_GAUGE_ENABLED_";
    public static final String SPEED_LIMIT_SIGN_GAUGE_ID = "SPEED_LIMIT_SIGN_GAUGE_ID";
    public static final String TRAFFIC_INCIDENT_GAUGE_ID = "TRAFFIC_INCIDENT_GAUGE_ID";
    public static final String WEATHER_WIDGET_ID = "WEATHER_GRAPH_WIDGET";
    private static final Logger sLogger;
    private Bus bus;
    private SharedPreferences currentUserPreferences;
    private IWidgetFilter filter;
    private SharedPreferences globalPreferences;
    private LifecycleEvent lastLifeCycleEvent;
    private Context mContext;
    private List<String> mGaugeIds;
    private boolean mLoaded;
    private HashMap<String, Integer> mWidgetIndexMap;
    private HashMap<Integer, SmartDashWidgetCache> mWidgetPresentersCache;
    
    static {
        sLogger = new Logger(SmartDashWidgetManager.class);
        SmartDashWidgetManager.GAUGES = new ArrayList<String>();
        SmartDashWidgetManager.GAUGE_NAME_LOOKUP = new HashSet<String>();
        SmartDashWidgetManager.GAUGES.add("CALENDAR_WIDGET");
        SmartDashWidgetManager.GAUGES.add("COMPASS_WIDGET");
        SmartDashWidgetManager.GAUGES.add("ANALOG_CLOCK_WIDGET");
        SmartDashWidgetManager.GAUGES.add("DIGITAL_CLOCK_WIDGET");
        SmartDashWidgetManager.GAUGES.add("DIGITAL_CLOCK_2_WIDGET");
        SmartDashWidgetManager.GAUGES.add("DRIVE_SCORE_GAUGE_ID");
        SmartDashWidgetManager.GAUGES.add("ENGINE_TEMPERATURE_GAUGE_ID");
        SmartDashWidgetManager.GAUGES.add("FUEL_GAUGE_ID");
        SmartDashWidgetManager.GAUGES.add("GFORCE_WIDGET");
        SmartDashWidgetManager.GAUGES.add("MUSIC_WIDGET");
        SmartDashWidgetManager.GAUGES.add("MPG_AVG_WIDGET");
        SmartDashWidgetManager.GAUGES.add("SPEED_LIMIT_SIGN_GAUGE_ID");
        SmartDashWidgetManager.GAUGES.add("TRAFFIC_INCIDENT_GAUGE_ID");
        SmartDashWidgetManager.GAUGES.add("ETA_GAUGE_ID");
        SmartDashWidgetManager.GAUGES.add("EMPTY_WIDGET");
        final Iterator<String> iterator = SmartDashWidgetManager.GAUGES.iterator();
        while (iterator.hasNext()) {
            SmartDashWidgetManager.GAUGE_NAME_LOOKUP.add(iterator.next());
        }
    }
    
    public SmartDashWidgetManager(final SharedPreferences globalPreferences, final Context mContext) {
        this.mWidgetPresentersCache = new HashMap<Integer, SmartDashWidgetCache>();
        this.mLoaded = false;
        this.lastLifeCycleEvent = null;
        this.globalPreferences = globalPreferences;
        this.mContext = mContext;
        this.mGaugeIds = new ArrayList<String>();
        this.mWidgetIndexMap = new HashMap<String, Integer>();
        this.bus = new Bus();
    }
    
    public static boolean isValidGaugeId(final String s) {
        return !TextUtils.isEmpty((CharSequence)s) && SmartDashWidgetManager.GAUGE_NAME_LOOKUP.contains(s);
    }
    
    private void sendLifecycleEvent(final LifecycleEvent lifecycleEvent) {
        final Iterator<SmartDashWidgetCache> iterator = this.mWidgetPresentersCache.values().iterator();
        while (iterator.hasNext()) {
            this.sendLifecycleEvent(lifecycleEvent, iterator.next().getWidgetPresentersMap().values().iterator());
        }
    }
    
    private void sendLifecycleEvent(final LifecycleEvent lifecycleEvent, final Iterator<DashboardWidgetPresenter> iterator) {
        while (iterator.hasNext()) {
            switch (lifecycleEvent) {
                default:
                    continue;
                case PAUSE:
                    iterator.next().onPause();
                    continue;
                case RESUME:
                    iterator.next().onResume();
                    continue;
            }
        }
    }
    
    public SmartDashWidgetCache buildSmartDashWidgetCache(final int n) {
        SmartDashWidgetCache smartDashWidgetCache;
        if (this.mWidgetPresentersCache.containsKey(n)) {
            smartDashWidgetCache = this.mWidgetPresentersCache.get(n);
            smartDashWidgetCache.clear();
            SmartDashWidgetManager.sLogger.v("widget::: cache cleared for " + n);
        }
        else {
            smartDashWidgetCache = new SmartDashWidgetCache(this.mContext);
            this.mWidgetPresentersCache.put(n, smartDashWidgetCache);
            SmartDashWidgetManager.sLogger.v("widget::: cache created for " + n);
        }
        final Iterator<String> iterator = this.mGaugeIds.iterator();
        while (iterator.hasNext()) {
            smartDashWidgetCache.add(iterator.next());
        }
        if (this.lastLifeCycleEvent != null) {
            this.sendLifecycleEvent(this.lastLifeCycleEvent, smartDashWidgetCache.getWidgetPresentersMap().values().iterator());
        }
        return smartDashWidgetCache;
    }
    
    public int getIndexForWidgetIdentifier(final String s) {
        int intValue;
        if (this.mWidgetIndexMap.containsKey(s)) {
            intValue = this.mWidgetIndexMap.get(s);
        }
        else {
            intValue = -1;
        }
        return intValue;
    }
    
    public String getWidgetIdentifierForIndex(final int n) {
        String s;
        if (n >= 0 && n < this.mGaugeIds.size()) {
            s = this.mGaugeIds.get(n);
        }
        else {
            s = null;
        }
        return s;
    }
    
    public boolean isGaugeOn(final String s) {
        if (this.currentUserPreferences == null) {
            SmartDashWidgetManager.sLogger.d("isGaugeOn : Current Preferences is not set, using the global preferences");
            this.currentUserPreferences = this.globalPreferences;
        }
        return this.currentUserPreferences.getBoolean("PREF_GAUGE_ENABLED_" + s, true);
    }
    
    public void onPause() {
        this.lastLifeCycleEvent = LifecycleEvent.PAUSE;
        this.sendLifecycleEvent(LifecycleEvent.PAUSE);
    }
    
    public void onResume() {
        this.lastLifeCycleEvent = LifecycleEvent.RESUME;
        this.sendLifecycleEvent(LifecycleEvent.RESUME);
    }
    
    public void reLoadAvailableWidgets(final boolean b) {
        SmartDashWidgetManager.sLogger.v("reLoadAvailableWidgets");
        this.mGaugeIds.clear();
        this.mWidgetIndexMap.clear();
        final boolean defaultProfile = DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        SmartDashWidgetManager.sLogger.d("Default profile ? " + defaultProfile);
        SharedPreferences currentUserPreferences;
        if (defaultProfile) {
            currentUserPreferences = this.globalPreferences;
        }
        else {
            currentUserPreferences = DriverProfileHelper.getInstance().getDriverProfileManager().getLocalPreferencesForCurrentDriverProfile(HudApplication.getAppContext());
        }
        this.currentUserPreferences = currentUserPreferences;
        int n = 0;
        for (final String s : SmartDashWidgetManager.GAUGES) {
            if ((b || this.isGaugeOn(s)) && this.filter != null && !this.filter.filter(s)) {
                this.mGaugeIds.add(s);
                SmartDashWidgetManager.sLogger.d("adding Gauge : " + s + ", at : " + n);
                this.mWidgetIndexMap.put(s, n);
                ++n;
            }
            else {
                SmartDashWidgetManager.sLogger.d("Not adding Gauge, as its filtered out , ID : " + s);
            }
        }
        if (this.mGaugeIds.size() == 0) {
            this.mGaugeIds.add("EMPTY_WIDGET");
            this.mWidgetIndexMap.put("EMPTY_WIDGET", 0);
        }
        if (this.mLoaded) {
            this.bus.post(Reload.RELOAD_CACHE);
            this.bus.post(Reload.RELOADED);
        }
        else {
            this.mLoaded = true;
        }
    }
    
    public void registerForChanges(final Object o) {
        this.bus.register(o);
    }
    
    public void setFilter(final IWidgetFilter filter) {
        this.filter = filter;
    }
    
    public void setGaugeOn(final String s, final boolean b) {
        if (this.currentUserPreferences == null) {
            SmartDashWidgetManager.sLogger.d("setGaugeOn : Current Preferences is not set, using the global preferences");
            this.currentUserPreferences = this.globalPreferences;
        }
        this.currentUserPreferences.edit().putBoolean("PREF_GAUGE_ENABLED_" + s, b).apply();
    }
    
    public void updateWidget(final String s, final Object o) {
        final Collection<SmartDashWidgetCache> values = this.mWidgetPresentersCache.values();
        if (values != null) {
            for (final SmartDashWidgetCache smartDashWidgetCache : values) {
                if (smartDashWidgetCache != null) {
                    smartDashWidgetCache.updateWidget(s, o);
                }
            }
        }
    }
    
    public interface IWidgetFilter
    {
        boolean filter(final String p0);
    }
    
    public enum LifecycleEvent
    {
        PAUSE, 
        RESUME;
    }
    
    public enum Reload
    {
        RELOADED, 
        RELOAD_CACHE;
    }
    
    public class SmartDashWidgetCache
    {
        private Context mContext;
        private List<String> mGaugeIds;
        private HashMap<String, Integer> mWidgetIndexMap;
        private HashMap<String, DashboardWidgetPresenter> mWidgetPresentersMap;
        
        public SmartDashWidgetCache(final Context mContext) {
            this.mContext = mContext;
            this.mGaugeIds = new ArrayList<String>();
            this.mWidgetPresentersMap = new HashMap<String, DashboardWidgetPresenter>();
            this.mWidgetIndexMap = new HashMap<String, Integer>();
        }
        
        private void initializeWidget(final String s) {
            switch (s) {
                case "FUEL_GAUGE_ID": {
                    final FuelGaugePresenter2 fuelGaugePresenter2 = (FuelGaugePresenter2)this.getWidgetPresenter(s);
                    final int fuelLevel = ObdManager.getInstance().getFuelLevel();
                    if (fuelGaugePresenter2 != null) {
                        fuelGaugePresenter2.setFuelLevel(fuelLevel);
                        break;
                    }
                    break;
                }
                case "DRIVE_SCORE_GAUGE_ID": {
                    final DriveScoreGaugePresenter driveScoreGaugePresenter = (DriveScoreGaugePresenter)this.getWidgetPresenter(s);
                    if (driveScoreGaugePresenter != null) {
                        driveScoreGaugePresenter.setDriveScoreUpdated(RemoteDeviceManager.getInstance().getTelemetryDataManager().getDriveScoreUpdatedEvent());
                        break;
                    }
                    break;
                }
                case "ENGINE_TEMPERATURE_GAUGE_ID": {
                    final EngineTemperaturePresenter engineTemperaturePresenter = (EngineTemperaturePresenter)this.getWidgetPresenter(s);
                    double engineTemperature = ObdManager.getInstance().getPidValue(5);
                    if (engineTemperaturePresenter != null) {
                        if (engineTemperature == -1.0) {
                            engineTemperature = EngineTemperaturePresenter.Companion.getTEMPERATURE_GAUGE_MID_POINT_CELSIUS();
                        }
                        engineTemperaturePresenter.setEngineTemperature(engineTemperature);
                        break;
                    }
                    break;
                }
            }
        }
        
        public void add(final String s) {
            if (!this.mWidgetIndexMap.containsKey(s)) {
                this.mGaugeIds.add(s);
                this.mWidgetIndexMap.put(s, this.mGaugeIds.size() - 1);
                this.mWidgetPresentersMap.put(s, DashWidgetPresenterFactory.createDashWidgetPresenter(this.mContext, s));
                this.initializeWidget(s);
            }
        }
        
        public void clear() {
            this.mGaugeIds.clear();
            this.mWidgetPresentersMap.clear();
            this.mWidgetIndexMap.clear();
        }
        
        public int getIndexForWidget(final String s) {
            int intValue;
            if (this.mWidgetIndexMap.containsKey(s)) {
                intValue = this.mWidgetIndexMap.get(s);
            }
            else {
                intValue = -1;
            }
            return intValue;
        }
        
        public DashboardWidgetPresenter getWidgetPresenter(final int n) {
            DashboardWidgetPresenter dashboardWidgetPresenter;
            if (n >= 0 && n < this.mGaugeIds.size()) {
                dashboardWidgetPresenter = this.mWidgetPresentersMap.get(this.mGaugeIds.get(n));
            }
            else {
                dashboardWidgetPresenter = null;
            }
            return dashboardWidgetPresenter;
        }
        
        public DashboardWidgetPresenter getWidgetPresenter(final String s) {
            return this.getWidgetPresenter(this.getIndexForWidget(s));
        }
        
        public HashMap<String, DashboardWidgetPresenter> getWidgetPresentersMap() {
            return this.mWidgetPresentersMap;
        }
        
        public int getWidgetsCount() {
            return this.mGaugeIds.size();
        }
        
        public void updateWidget(final String s, final Object o) {
            if (!TextUtils.isEmpty((CharSequence)s)) {
                switch (s) {
                    case "FUEL_GAUGE_ID": {
                        final FuelGaugePresenter2 fuelGaugePresenter2 = (FuelGaugePresenter2)this.getWidgetPresenter(s);
                        if (fuelGaugePresenter2 != null && o instanceof Double) {
                            fuelGaugePresenter2.setFuelLevel(((Number)o).intValue());
                            break;
                        }
                        break;
                    }
                    case "COMPASS_WIDGET": {
                        final CompassPresenter compassPresenter = (CompassPresenter)this.getWidgetPresenter(s);
                        if (compassPresenter != null) {
                            compassPresenter.setHeadingAngle(((Number)o).doubleValue());
                            break;
                        }
                        break;
                    }
                    case "MPG_AVG_WIDGET": {
                        final MPGGaugePresenter mpgGaugePresenter = (MPGGaugePresenter)this.getWidgetPresenter(s);
                        if (mpgGaugePresenter != null) {
                            mpgGaugePresenter.setCurrentMPG(((Number)o).doubleValue());
                            break;
                        }
                        break;
                    }
                    case "SPEED_LIMIT_SIGN_GAUGE_ID": {
                        final SpeedLimitSignPresenter speedLimitSignPresenter = (SpeedLimitSignPresenter)this.getWidgetPresenter(s);
                        if (speedLimitSignPresenter != null) {
                            speedLimitSignPresenter.setSpeedLimit(((Number)o).intValue());
                            break;
                        }
                        break;
                    }
                    case "ENGINE_TEMPERATURE_GAUGE_ID": {
                        final EngineTemperaturePresenter engineTemperaturePresenter = (EngineTemperaturePresenter)this.getWidgetPresenter(s);
                        if (engineTemperaturePresenter != null) {
                            engineTemperaturePresenter.setEngineTemperature(((Number)o).intValue());
                            break;
                        }
                        break;
                    }
                }
            }
        }
    }
    
    public static class UserPreferenceChanged
    {
    }
}
