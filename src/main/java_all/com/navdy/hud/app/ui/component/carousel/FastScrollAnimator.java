package com.navdy.hud.app.ui.component.carousel;

import android.animation.AnimatorSet;
import android.os.SystemClock;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.animation.TimeInterpolator;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.AnimatorSet;
import android.animation.Animator;
import com.navdy.hud.app.HudApplication;
import android.view.View;
import com.navdy.service.library.log.Logger;

public class FastScrollAnimator implements AnimationStrategy
{
    public static final int ANIMATION_HERO = 50;
    public static final int ANIMATION_HERO_SINGLE = 133;
    public static final int ANIMATION_HERO_TEXT = 165;
    public static final int ANIMATION_HERO_TRANSLATE_X;
    private static final Logger sLogger;
    private CarouselLayout carouselLayout;
    private boolean endPending;
    private View hiddenView;
    private long lastScrollAnimationFinishTime;
    
    static {
        sLogger = new Logger(FastScrollAnimator.class);
        ANIMATION_HERO_TRANSLATE_X = (int)HudApplication.getAppContext().getResources().getDimension(R.dimen.carousel_fastscroll_hero_translate_x);
    }
    
    FastScrollAnimator(final CarouselLayout carouselLayout) {
        this.carouselLayout = carouselLayout;
        this.hiddenView = carouselLayout.buildView(0, 0, Carousel.ViewType.MIDDLE_LEFT, false);
    }
    
    @Override
    public AnimatorSet buildLayoutAnimation(final Animator.AnimatorListener animator$AnimatorListener, final CarouselLayout carouselLayout, int n, int n2) {
        final AnimatorSet set = new AnimatorSet();
        if (n2 > n) {
            n = 1;
        }
        else {
            n = 0;
        }
        Direction direction;
        if (n != 0) {
            direction = Direction.LEFT;
        }
        else {
            direction = Direction.RIGHT;
        }
        int n3;
        if (direction == Direction.LEFT) {
            n = (int)(carouselLayout.middleLeftView.getX() - FastScrollAnimator.ANIMATION_HERO_TRANSLATE_X);
            n3 = (int)(carouselLayout.middleLeftView.getX() + FastScrollAnimator.ANIMATION_HERO_TRANSLATE_X);
        }
        else {
            n = (int)(carouselLayout.middleLeftView.getX() + FastScrollAnimator.ANIMATION_HERO_TRANSLATE_X);
            n3 = (int)(carouselLayout.middleLeftView.getX() - FastScrollAnimator.ANIMATION_HERO_TRANSLATE_X);
        }
        this.hiddenView.setX((float)n3);
        this.hiddenView.setAlpha(0.0f);
        final AnimatorSet.Builder play = set.play((Animator)ObjectAnimator.ofFloat(carouselLayout.middleLeftView, View.X, new float[] { n }));
        play.with((Animator)ObjectAnimator.ofFloat(carouselLayout.middleLeftView, View.ALPHA, new float[] { 0.0f }));
        play.with((Animator)ObjectAnimator.ofFloat(this.hiddenView, View.X, new float[] { this.carouselLayout.middleLeftView.getX() }));
        play.with((Animator)ObjectAnimator.ofFloat(this.hiddenView, View.ALPHA, new float[] { 1.0f }));
        this.carouselLayout.carouselAdapter.getView(n2, this.hiddenView, Carousel.ViewType.MIDDLE_LEFT, this.carouselLayout.imageLytResourceId, this.carouselLayout.mainImageSize);
        if (this.carouselLayout.carouselIndicator != null) {
            final AnimatorSet itemMoveAnimator = this.carouselLayout.carouselIndicator.getItemMoveAnimator(n2, -1);
            if (itemMoveAnimator != null) {
                play.with((Animator)itemMoveAnimator);
            }
        }
        set.setInterpolator((TimeInterpolator)this.carouselLayout.interpolator);
        set.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                FastScrollAnimator.this.lastScrollAnimationFinishTime = SystemClock.elapsedRealtime();
                if (!FastScrollAnimator.this.carouselLayout.isAnimationPending()) {
                    FastScrollAnimator.this.endAnimation(animator$AnimatorListener, n2, false);
                }
                else {
                    FastScrollAnimator.this.carouselLayout.currentItem = n2;
                    final View access$100 = FastScrollAnimator.this.hiddenView;
                    FastScrollAnimator.this.hiddenView = FastScrollAnimator.this.carouselLayout.middleLeftView;
                    FastScrollAnimator.this.hiddenView.setVisibility(INVISIBLE);
                    FastScrollAnimator.this.hiddenView.setAlpha(0.0f);
                    FastScrollAnimator.this.carouselLayout.middleLeftView = access$100;
                    if (animator$AnimatorListener != null) {
                        animator$AnimatorListener.onAnimationEnd(animator);
                    }
                    FastScrollAnimator.this.carouselLayout.selectedItemView = FastScrollAnimator.this.carouselLayout.middleLeftView;
                    if (FastScrollAnimator.this.carouselLayout.carouselIndicator != null) {
                        FastScrollAnimator.this.carouselLayout.carouselIndicator.setCurrentItem(FastScrollAnimator.this.carouselLayout.currentItem);
                    }
                    if (FastScrollAnimator.this.carouselLayout.itemChangeListener != null) {
                        FastScrollAnimator.this.carouselLayout.itemChangeListener.onCurrentItemChanged(FastScrollAnimator.this.carouselLayout.currentItem, FastScrollAnimator.this.carouselLayout.model.get(FastScrollAnimator.this.carouselLayout.currentItem).id);
                    }
                    FastScrollAnimator.this.carouselLayout.runQueuedOperation();
                }
            }
            
            @Override
            public void onAnimationStart(final Animator animator) {
                FastScrollAnimator.this.endPending = true;
                if (animator$AnimatorListener != null) {
                    animator$AnimatorListener.onAnimationStart(animator);
                }
                FastScrollAnimator.this.hiddenView.setVisibility(View.VISIBLE);
                FastScrollAnimator.this.carouselLayout.leftView.setAlpha(0.0f);
                FastScrollAnimator.this.carouselLayout.rightView.setAlpha(0.0f);
                FastScrollAnimator.this.carouselLayout.middleRightView.setAlpha(0.0f);
                if (!FastScrollAnimator.this.carouselLayout.isAnimationPending()) {
                    FastScrollAnimator.this.carouselLayout.carouselAdapter.getView(n2 - 1, FastScrollAnimator.this.carouselLayout.leftView, Carousel.ViewType.SIDE, FastScrollAnimator.this.carouselLayout.imageLytResourceId, FastScrollAnimator.this.carouselLayout.sideImageSize);
                    FastScrollAnimator.this.carouselLayout.carouselAdapter.getView(n2 + 1, FastScrollAnimator.this.carouselLayout.rightView, Carousel.ViewType.SIDE, FastScrollAnimator.this.carouselLayout.imageLytResourceId, FastScrollAnimator.this.carouselLayout.sideImageSize);
                    FastScrollAnimator.this.carouselLayout.carouselAdapter.getView(n2, FastScrollAnimator.this.carouselLayout.middleRightView, Carousel.ViewType.MIDDLE_RIGHT, FastScrollAnimator.this.carouselLayout.imageLytResourceId, FastScrollAnimator.this.carouselLayout.sideImageSize);
                }
            }
        });
        n2 = (n = 50);
        if (!this.carouselLayout.isAnimationPending()) {
            n = n2;
            if (SystemClock.elapsedRealtime() - this.lastScrollAnimationFinishTime > 50L) {
                n = 133;
            }
        }
        set.setDuration((long)n);
        if (FastScrollAnimator.sLogger.isLoggable(3)) {
            FastScrollAnimator.sLogger.v("dur=" + n);
        }
        return set;
    }
    
    @Override
    public AnimatorSet createHiddenViewAnimation(final CarouselLayout carouselLayout, final Direction direction) {
        return null;
    }
    
    @Override
    public AnimatorSet createMiddleLeftViewAnimation(final CarouselLayout carouselLayout, final Direction direction) {
        return null;
    }
    
    @Override
    public AnimatorSet createMiddleRightViewAnimation(final CarouselLayout carouselLayout, final Direction direction) {
        return null;
    }
    
    @Override
    public AnimatorSet createNewMiddleRightViewAnimation(final CarouselLayout carouselLayout, final Direction direction) {
        return null;
    }
    
    @Override
    public AnimatorSet createSideViewToMiddleAnimation(final CarouselLayout carouselLayout, final Direction direction) {
        return null;
    }
    
    @Override
    public Animator createViewOutAnimation(final CarouselLayout carouselLayout, final Direction direction) {
        return null;
    }
    
    void endAnimation() {
        if (this.endPending) {
            this.endPending = false;
            this.carouselLayout.carouselAdapter.getView(this.carouselLayout.currentItem - 1, this.carouselLayout.leftView, Carousel.ViewType.SIDE, this.carouselLayout.imageLytResourceId, this.carouselLayout.sideImageSize);
            this.carouselLayout.carouselAdapter.getView(this.carouselLayout.currentItem + 1, this.carouselLayout.rightView, Carousel.ViewType.SIDE, this.carouselLayout.imageLytResourceId, this.carouselLayout.sideImageSize);
            this.carouselLayout.carouselAdapter.getView(this.carouselLayout.currentItem, this.carouselLayout.middleRightView, Carousel.ViewType.MIDDLE_RIGHT, this.carouselLayout.imageLytResourceId, this.carouselLayout.sideImageSize);
            this.endAnimation(null, this.carouselLayout.currentItem, true);
        }
    }
    
    void endAnimation(final Animator.AnimatorListener animator$AnimatorListener, final int n, final boolean b) {
        final AnimatorSet set = new AnimatorSet();
        final ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.carouselLayout.leftView, View.ALPHA, new float[] { 1.0f });
        final ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.carouselLayout.rightView, View.ALPHA, new float[] { 1.0f });
        final ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(this.carouselLayout.middleRightView, View.ALPHA, new float[] { 1.0f });
        set.setDuration(165L);
        set.playTogether(new Animator[] { ofFloat, ofFloat2, ofFloat3 });
        set.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                FastScrollAnimator.this.endPending = false;
                if (!b) {
                    FastScrollAnimator.this.carouselLayout.currentItem = n;
                    final View access$100 = FastScrollAnimator.this.hiddenView;
                    FastScrollAnimator.this.hiddenView = FastScrollAnimator.this.carouselLayout.middleLeftView;
                    FastScrollAnimator.this.hiddenView.setVisibility(INVISIBLE);
                    FastScrollAnimator.this.hiddenView.setAlpha(0.0f);
                    FastScrollAnimator.this.carouselLayout.middleLeftView = access$100;
                    if (animator$AnimatorListener != null) {
                        animator$AnimatorListener.onAnimationEnd(animator);
                    }
                    FastScrollAnimator.this.carouselLayout.selectedItemView = FastScrollAnimator.this.carouselLayout.middleLeftView;
                    if (FastScrollAnimator.this.carouselLayout.carouselIndicator != null) {
                        FastScrollAnimator.this.carouselLayout.carouselIndicator.setCurrentItem(FastScrollAnimator.this.carouselLayout.currentItem);
                    }
                    if (FastScrollAnimator.this.carouselLayout.itemChangeListener != null) {
                        FastScrollAnimator.this.carouselLayout.itemChangeListener.onCurrentItemChanged(FastScrollAnimator.this.carouselLayout.currentItem, FastScrollAnimator.this.carouselLayout.model.get(FastScrollAnimator.this.carouselLayout.currentItem).id);
                    }
                }
                FastScrollAnimator.this.carouselLayout.runQueuedOperation();
            }
        });
        set.start();
    }
    
    boolean isEndPending() {
        return this.endPending;
    }
}
