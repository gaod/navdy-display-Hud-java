package com.navdy.hud.app.ui.component.mainmenu;

public class MusicMenuConstants
{
    static final int PAGE_LOAD_DISTANCE = 20;
    static final int PAGE_SIZE = 25;
}
