package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.view.LayoutInflater;
import com.navdy.hud.app.ui.component.vlist.VerticalModelCache;
import com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.AnimatorSet;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import android.view.ViewGroup;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.LayerDrawable;
import android.widget.ImageView;
import android.text.Html;
import android.graphics.Color;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import android.os.Handler;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
//import kotlin.jvm.internal.DefaultConstructorMarker;
import android.view.ViewGroup;
import android.view.View;
import android.widget.TextView;
import com.navdy.hud.app.ui.component.SwitchHaloView;
import org.jetbrains.annotations.Nullable;
import android.animation.AnimatorSet;
import org.jetbrains.annotations.NotNull;

import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.service.library.log.Logger;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000¡\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006*\u0001\u0019\u0018\u0000 u2\u00020\u0001:\u0001uB\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0018\u0010P\u001a\u00020Q2\u0006\u0010R\u001a\u00020S2\u0006\u0010T\u001a\u00020UH\u0016J\b\u0010V\u001a\u00020QH\u0016J0\u0010W\u001a\u00020Q2\u0006\u0010X\u001a\u00020Y2\u0006\u0010Z\u001a\u00020-2\u0006\u0010[\u001a\u00020-2\u0006\u0010\\\u001a\u00020-2\u0006\u0010]\u001a\u00020\"H\u0016J\u0016\u0010^\u001a\u00020Q2\u0006\u0010+\u001a\u00020\"2\u0006\u0010*\u001a\u00020\"J\b\u0010_\u001a\u00020`H\u0016J\b\u0010a\u001a\u00020\nH\u0002J\b\u0010b\u001a\u00020\nH\u0002J\u0010\u0010c\u001a\u00020\n2\u0006\u0010d\u001a\u00020eH\u0002J\u0018\u0010f\u001a\u00020Q2\u0006\u0010R\u001a\u00020S2\u0006\u0010T\u001a\u00020UH\u0016J \u0010g\u001a\u00020Q2\u0006\u0010R\u001a\u00020S2\u0006\u0010h\u001a\u00020\n2\u0006\u0010i\u001a\u00020\nH\u0016J\u0010\u0010j\u001a\u00020Q2\u0006\u0010k\u001a\u00020\nH\u0002J(\u0010l\u001a\u00020Q2\u0006\u0010m\u001a\u00020n2\u0006\u0010o\u001a\u00020p2\u0006\u0010i\u001a\u00020\n2\u0006\u0010q\u001a\u00020\"H\u0016J\u001a\u00100\u001a\u00020Q2\b\u0010r\u001a\u0004\u0018\u00010e2\u0006\u0010s\u001a\u00020\"H\u0002J\u001a\u00104\u001a\u00020Q2\b\u0010r\u001a\u0004\u0018\u00010e2\u0006\u0010s\u001a\u00020\"H\u0002J\u0010\u0010L\u001a\u00020Q2\u0006\u0010r\u001a\u00020eH\u0002J\b\u0010q\u001a\u00020QH\u0016J\b\u0010t\u001a\u00020QH\u0002R\u0014\u0010\t\u001a\u00020\nX\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\u00020\nX\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\fR \u0010\u000f\u001a\b\u0018\u00010\u0010R\u00020\u0011X\u0084\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u00020\u0019X\u0082\u0004¢\u0006\u0004\n\u0002\u0010\u001aR\u001a\u0010\u001b\u001a\u00020\u001cX\u0084\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u000e\u0010!\u001a\u00020\"X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\"X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\"X\u0082\u000e¢\u0006\u0002\n\u0000R\u001a\u0010%\u001a\u00020\"X\u0084\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b&\u0010'\"\u0004\b(\u0010)R\u000e\u0010*\u001a\u00020\"X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020\"X\u0082\u000e¢\u0006\u0002\n\u0000R\u001a\u0010,\u001a\u00020-X\u0084\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R\u001a\u00102\u001a\u00020-X\u0084\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b3\u0010/\"\u0004\b4\u00101R\u001a\u00105\u001a\u000206X\u0084\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b7\u00108\"\u0004\b9\u0010:R\u001a\u0010;\u001a\u00020\u0003X\u0084\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b<\u0010=\"\u0004\b>\u0010?R\u001a\u0010@\u001a\u000206X\u0084\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bA\u00108\"\u0004\bB\u0010:R\u001a\u0010C\u001a\u00020\"X\u0084\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bD\u0010'\"\u0004\bE\u0010)R\u001a\u0010F\u001a\u00020\u0003X\u0084\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bG\u0010=\"\u0004\bH\u0010?R\u000e\u0010I\u001a\u00020\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u001a\u0010J\u001a\u00020-X\u0084\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bK\u0010/\"\u0004\bL\u00101R\u000e\u0010M\u001a\u00020NX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010O\u001a\u00020NX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006v" }, d2 = { "Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;", "Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;", "layout", "Landroid/view/ViewGroup;", "vlist", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList;", "handler", "Landroid/os/Handler;", "(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V", "FLUCTUATOR_OPACITY_ALPHA", "", "getFLUCTUATOR_OPACITY_ALPHA", "()I", "HALO_DELAY_START_DURATION", "getHALO_DELAY_START_DURATION", "animatorSetBuilder", "Landroid/animation/AnimatorSet.Builder;", "Landroid/animation/AnimatorSet;", "getAnimatorSetBuilder", "()Landroid/animation/AnimatorSet.Builder;", "setAnimatorSetBuilder", "(Landroid/animation/AnimatorSet.Builder;)V", "fluctuatorRunnable", "Ljava/lang/Runnable;", "fluctuatorStartListener", "com/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1", "Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1;", "haloView", "Lcom/navdy/hud/app/ui/component/SwitchHaloView;", "getHaloView", "()Lcom/navdy/hud/app/ui/component/SwitchHaloView;", "setHaloView", "(Lcom/navdy/hud/app/ui/component/SwitchHaloView;)V", "hasIconFluctuatorColor", "", "hasSubTitle", "hasSubTitle2", "iconScaleAnimationDisabled", "getIconScaleAnimationDisabled", "()Z", "setIconScaleAnimationDisabled", "(Z)V", "isEnabled", "isOn", "subTitle", "Landroid/widget/TextView;", "getSubTitle", "()Landroid/widget/TextView;", "setSubTitle", "(Landroid/widget/TextView;)V", "subTitle2", "getSubTitle2", "setSubTitle2", "switchBackground", "Landroid/view/View;", "getSwitchBackground", "()Landroid/view/View;", "setSwitchBackground", "(Landroid/view/View;)V", "switchContainer", "getSwitchContainer", "()Landroid/view/ViewGroup;", "setSwitchContainer", "(Landroid/view/ViewGroup;)V", "switchThumb", "getSwitchThumb", "setSwitchThumb", "textAnimationDisabled", "getTextAnimationDisabled", "setTextAnimationDisabled", "textContainer", "getTextContainer", "setTextContainer", "thumbMargin", "title", "getTitle", "setTitle", "titleSelectedTopMargin", "", "titleUnselectedScale", "bind", "", "model", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "modelState", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;", "clearAnimation", "copyAndPosition", "imageC", "Landroid/widget/ImageView;", "titleC", "subTitleC", "subTitle2C", "setImage", "drawSwitch", "getModelType", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;", "getSubTitle2Color", "getSubTitleColor", "getTextColor", "property", "", "preBind", "select", "pos", "duration", "setIconFluctuatorColor", "color", "setItemState", "state", "Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;", "animation", "Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;", "startFluctuator", "str", "formatted", "stopFluctuator", "Companion", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class SwitchViewHolder extends VerticalViewHolder
{
    public static final Companion Companion;
    @NotNull
    private static final Logger logger;
    private final int FLUCTUATOR_OPACITY_ALPHA;
    private final int HALO_DELAY_START_DURATION;
    @Nullable
    private AnimatorSet.Builder animatorSetBuilder;
    private final Runnable fluctuatorRunnable;
    private final FluctuatorStartListener fluctuatorStartListener;
    @NotNull
    private SwitchHaloView haloView;
    private boolean hasIconFluctuatorColor;
    private boolean hasSubTitle;
    private boolean hasSubTitle2;
    private boolean iconScaleAnimationDisabled;
    private boolean isEnabled;
    private boolean isOn;
    @NotNull
    private TextView subTitle;
    @NotNull
    private TextView subTitle2;
    @NotNull
    private View switchBackground;
    @NotNull
    private ViewGroup switchContainer;
    @NotNull
    private View switchThumb;
    private boolean textAnimationDisabled;
    @NotNull
    private ViewGroup textContainer;
    private int thumbMargin;
    @NotNull
    private TextView title;
    private float titleSelectedTopMargin;
    private float titleUnselectedScale;
    
    static {
        Companion = new Companion();
        logger = new Logger(SwitchViewHolder.Companion.getClass());
    }

    public final class fluctuatorRunnable
        implements Runnable
    {
        final /* synthetic */ SwitchViewHolder this$0;

        fluctuatorRunnable(SwitchViewHolder switchViewHolder) {
            this.this$0 = switchViewHolder;
        }

        @Override
        public final void run() {
            this.this$0.startFluctuator();
        }
    }

    public final class FluctuatorStartListener
            extends DefaultAnimationListener
    {
        final /* synthetic */ Handler $handler;
        final /* synthetic */ SwitchViewHolder this$0;

        FluctuatorStartListener(SwitchViewHolder switchViewHolder, Handler handler) {
            this.this$0 = switchViewHolder;
            this.$handler = handler;
        }

        public void onAnimationEnd(@NotNull Animator animation) {
            Intrinsics.checkParameterIsNotNull(animation, "animation");
            this.this$0.itemAnimatorSet = (AnimatorSet) null;
            this.$handler.removeCallbacks(this.this$0.fluctuatorRunnable);
            this.$handler.postDelayed(this.this$0.fluctuatorRunnable, (long) this.this$0.getHALO_DELAY_START_DURATION());
        }
    }

    public SwitchViewHolder(@NotNull final ViewGroup viewGroup, @NotNull final VerticalList list, @NotNull final Handler handler) {

        Intrinsics.checkParameterIsNotNull(viewGroup, "layout");
        Intrinsics.checkParameterIsNotNull(list, "vlist");
        Intrinsics.checkParameterIsNotNull(handler, "handler");
        super(viewGroup, list, handler);

        this.FLUCTUATOR_OPACITY_ALPHA = 153;
        this.HALO_DELAY_START_DURATION = 100;
        this.fluctuatorStartListener = new FluctuatorStartListener(this, handler);
        this.fluctuatorRunnable = (Runnable)new fluctuatorRunnable(this);
        final View viewById = viewGroup.findViewById(R.id.textContainer);
        if (viewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup");
        }
        this.textContainer = (ViewGroup)viewById;
        final View viewById2 = viewGroup.findViewById(R.id.title);
        if (viewById2 == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        this.title = (TextView)viewById2;
        final View viewById3 = viewGroup.findViewById(R.id.subTitle);
        if (viewById3 == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        this.subTitle = (TextView)viewById3;
        final View viewById4 = viewGroup.findViewById(R.id.subTitle2);
        if (viewById4 == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        this.subTitle2 = (TextView)viewById4;
        final View viewById5 = viewGroup.findViewById(R.id.toggleSwitchBackground);
        Intrinsics.checkExpressionValueIsNotNull(viewById5, "layout.findViewById(R.id.toggleSwitchBackground)");
        this.switchBackground = viewById5;
        final View viewById6 = viewGroup.findViewById(R.id.toggleSwitchThumb);
        Intrinsics.checkExpressionValueIsNotNull(viewById6, "layout.findViewById(R.id.toggleSwitchThumb)");
        this.switchThumb = viewById6;
        final View viewById7 = viewGroup.findViewById(R.id.imageContainer);
        if (viewById7 == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup");
        }
        this.switchContainer = (ViewGroup)viewById7;
        this.thumbMargin = viewGroup.getResources().getDimensionPixelSize(R.dimen.vlist_toggle_switch_thumb_margin);
        final View viewById8 = viewGroup.findViewById(R.id.halo);
        if (viewById8 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.SwitchHaloView");
        }
        (this.haloView = (SwitchHaloView)viewById8).setVisibility(GONE);
    }
    
    @NotNull
    public static final /* synthetic */ Logger access$getLogger$cp() {
        return SwitchViewHolder.logger;
    }
    
    private final int getSubTitle2Color() {
        int textColor;
        if (this.extras == null) {
            textColor = 0;
        }
        else {
            textColor = this.getTextColor("SUBTITLE_2_COLOR");
        }
        return textColor;
    }
    
    private final int getSubTitleColor() {
        int textColor;
        if (this.extras == null) {
            textColor = 0;
        }
        else {
            textColor = this.getTextColor("SUBTITLE_COLOR");
        }
        return textColor;
    }
    
    private final int getTextColor(String s) {
        final boolean b = false;
        int int1;
        if (this.extras == null) {
            int1 = (b ? 1 : 0);
        }
        else {
            s = this.extras.get(s);
            int1 = (b ? 1 : 0);
            if (s != null) {
                try {
                    int1 = Integer.parseInt(s);
                }
                catch (NumberFormatException ex) {
                    int1 = (b ? 1 : 0);
                }
            }
        }
        return int1;
    }
    
    private final void setIconFluctuatorColor(int argb) {
        if (argb != 0) {
            argb = Color.argb(this.FLUCTUATOR_OPACITY_ALPHA, Color.red(argb), Color.green(argb), Color.blue(argb));
            this.hasIconFluctuatorColor = true;
            this.haloView.setStrokeColor(argb);
        }
        else {
            this.hasIconFluctuatorColor = false;
        }
    }
    
    private final void setSubTitle(final String s, final boolean b) {
        if (s == null) {
            this.subTitle.setText((CharSequence)"");
            this.hasSubTitle = false;
        }
        else {
            final int subTitleColor = this.getSubTitleColor();
            if (subTitleColor == 0) {
                this.subTitle.setTextColor(VerticalViewHolder.subTitleColor);
            }
            else {
                this.subTitle.setTextColor(subTitleColor);
            }
            if (b) {
                this.subTitle.setText((CharSequence)Html.fromHtml(s));
            }
            else {
                this.subTitle.setText((CharSequence)s);
            }
            this.hasSubTitle = true;
        }
    }
    
    private final void setSubTitle2(final String s, final boolean b) {
        if (s == null) {
            this.subTitle2.setText((CharSequence)"");
            this.subTitle2.setVisibility(GONE);
            this.hasSubTitle2 = false;
        }
        else {
            final int subTitle2Color = this.getSubTitle2Color();
            if (subTitle2Color == 0) {
                this.subTitle2.setTextColor(VerticalViewHolder.subTitle2Color);
            }
            else {
                this.subTitle2.setTextColor(subTitle2Color);
            }
            if (b) {
                this.subTitle2.setText((CharSequence)Html.fromHtml(s));
            }
            else {
                this.subTitle2.setText((CharSequence)s);
            }
            this.subTitle2.setVisibility(View.VISIBLE);
            this.hasSubTitle2 = true;
        }
    }
    
    private final void setTitle(final String s) {
        this.title.setText((CharSequence)s);
    }
    
    private final void stopFluctuator() {
        if (this.hasIconFluctuatorColor) {
            this.handler.removeCallbacks(this.fluctuatorRunnable);
            this.haloView.setVisibility(GONE);
            this.haloView.stop();
        }
    }
    
    @Override
    public void bind(@NotNull final VerticalList.Model model, @NotNull final VerticalList.ModelState modelState) {
        Intrinsics.checkParameterIsNotNull(model, "model");
        Intrinsics.checkParameterIsNotNull(modelState, "modelState");
        this.isOn = model.isOn;
        this.isEnabled = model.isEnabled;
        this.drawSwitch(this.isOn, this.isEnabled);
        if (modelState.updateTitle) {
            final String title = model.title;
            Intrinsics.checkExpressionValueIsNotNull(title, "model.title");
            this.setTitle(title);
        }
        if (modelState.updateSubTitle) {
            if (model.subTitle != null) {
                this.setSubTitle(model.subTitle, model.subTitleFormatted);
            }
            else {
                this.setSubTitle(null, false);
            }
        }
        if (modelState.updateSubTitle2) {
            if (model.subTitle2 != null) {
                this.setSubTitle2(model.subTitle2, model.subTitle2Formatted);
            }
            else {
                this.setSubTitle2(null, false);
            }
        }
        this.textAnimationDisabled = model.noTextAnimation;
        this.iconScaleAnimationDisabled = model.noImageScaleAnimation;
        this.setIconFluctuatorColor(model.iconFluctuatorColor);
    }
    
    @Override
    public void clearAnimation() {
        this.stopFluctuator();
        this.stopAnimation();
        this.currentState = null;
        this.layout.setVisibility(View.VISIBLE);
        this.layout.setAlpha(1.0f);
    }
    
    @Override
    public void copyAndPosition(@NotNull final ImageView imageView, @NotNull final TextView textView, @NotNull final TextView textView2, @NotNull final TextView textView3, final boolean b) {
        Intrinsics.checkParameterIsNotNull(imageView, "imageC");
        Intrinsics.checkParameterIsNotNull(textView, "titleC");
        Intrinsics.checkParameterIsNotNull(textView2, "subTitleC");
        Intrinsics.checkParameterIsNotNull(textView3, "subTitle2C");
    }
    
    public final void drawSwitch(final boolean b, final boolean b2) {
        if (b2) {
            this.switchThumb.setBackgroundResource(R.drawable.switch_thumb_enabled);
        }
        else {
            this.switchThumb.setBackgroundResource(R.drawable.switch_thumb_disabled);
        }
        if (b2 && b) {
            this.switchBackground.setBackgroundResource(R.drawable.switch_button_off);
            final Drawable background = this.switchBackground.getBackground();
            if (background == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
            }
            final Drawable drawable = ((LayerDrawable)background).getDrawable(1);
            if (drawable == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.ClipDrawable");
            }
            ((ClipDrawable)drawable).setLevel(5000);
            final ViewGroup.LayoutParams layoutParams = this.switchThumb.getLayoutParams();
            if (layoutParams == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup.MarginLayoutParams)layoutParams;
            viewGroup$MarginLayoutParams.leftMargin = this.thumbMargin;
            this.switchThumb.setLayoutParams((ViewGroup.LayoutParams)viewGroup$MarginLayoutParams);
        }
        else {
            this.switchBackground.setBackgroundResource(R.drawable.switch_button_off);
            final Drawable background2 = this.switchBackground.getBackground();
            if (background2 == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
            }
            final Drawable drawable2 = ((LayerDrawable)background2).getDrawable(1);
            if (drawable2 == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.ClipDrawable");
            }
            ((ClipDrawable)drawable2).setLevel(0);
            final ViewGroup.LayoutParams layoutParams2 = this.switchThumb.getLayoutParams();
            if (layoutParams2 == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams2 = (ViewGroup.MarginLayoutParams)layoutParams2;
            viewGroup$MarginLayoutParams2.leftMargin = 0;
            this.switchThumb.setLayoutParams((ViewGroup.LayoutParams)viewGroup$MarginLayoutParams2);
        }
    }
    
    @Nullable
    protected final AnimatorSet.Builder getAnimatorSetBuilder() {
        return this.animatorSetBuilder;
    }
    
    public final int getFLUCTUATOR_OPACITY_ALPHA() {
        return this.FLUCTUATOR_OPACITY_ALPHA;
    }
    
    public final int getHALO_DELAY_START_DURATION() {
        return this.HALO_DELAY_START_DURATION;
    }
    
    @NotNull
    protected final SwitchHaloView getHaloView() {
        return this.haloView;
    }
    
    protected final boolean getIconScaleAnimationDisabled() {
        return this.iconScaleAnimationDisabled;
    }
    
    @NotNull
    @Override
    public VerticalList.ModelType getModelType() {
        return VerticalList.ModelType.SWITCH;
    }
    
    @NotNull
    protected final TextView getSubTitle() {
        return this.subTitle;
    }
    
    @NotNull
    protected final TextView getSubTitle2() {
        return this.subTitle2;
    }
    
    @NotNull
    protected final View getSwitchBackground() {
        return this.switchBackground;
    }
    
    @NotNull
    protected final ViewGroup getSwitchContainer() {
        return this.switchContainer;
    }
    
    @NotNull
    protected final View getSwitchThumb() {
        return this.switchThumb;
    }
    
    protected final boolean getTextAnimationDisabled() {
        return this.textAnimationDisabled;
    }
    
    @NotNull
    protected final ViewGroup getTextContainer() {
        return this.textContainer;
    }
    
    @NotNull
    protected final TextView getTitle() {
        return this.title;
    }
    
    @Override
    public void preBind(@NotNull final VerticalList.Model model, @NotNull final VerticalList.ModelState modelState) {
        Intrinsics.checkParameterIsNotNull(model, "model");
        Intrinsics.checkParameterIsNotNull(modelState, "modelState");
        this.isOn = model.isOn;
        this.isEnabled = model.isEnabled;
        this.drawSwitch(this.isOn, this.isEnabled);
        final ViewGroup.LayoutParams layoutParams = this.title.getLayoutParams();
        if (layoutParams == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        }
        ((ViewGroup.MarginLayoutParams)layoutParams).topMargin = (int)model.fontInfo.titleFontTopMargin;
        this.title.setTextSize(model.fontInfo.titleFontSize);
        this.titleSelectedTopMargin = (int)model.fontInfo.titleFontTopMargin;
        final ViewGroup.LayoutParams layoutParams2 = this.subTitle.getLayoutParams();
        if (layoutParams2 == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        }
        ((ViewGroup.MarginLayoutParams)layoutParams2).topMargin = (int)model.fontInfo.subTitleFontTopMargin;
        if (!model.subTitle_2Lines) {
            this.subTitle.setTextAppearance(HudApplication.getAppContext(), R.style.vlist_subtitle);
            this.subTitle.setSingleLine(true);
            this.subTitle.setEllipsize((TextUtils.TruncateAt)null);
        }
        else {
            this.subTitle.setTextAppearance(HudApplication.getAppContext(), R.style.vlist_subtitle_2_line);
            this.subTitle.setSingleLine(false);
            this.subTitle.setMaxLines(2);
            this.subTitle.setEllipsize(TextUtils.TruncateAt.END);
        }
        this.subTitle.setTextSize(model.fontInfo.subTitleFontSize);
        final ViewGroup.LayoutParams layoutParams3 = this.subTitle2.getLayoutParams();
        if (layoutParams3 == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        }
        ((ViewGroup.MarginLayoutParams)layoutParams3).topMargin = (int)model.fontInfo.subTitle2FontTopMargin;
        this.subTitle2.setTextSize(model.fontInfo.subTitle2FontSize);
        this.titleUnselectedScale = model.fontInfo.titleScale;
    }
    
    @Override
    public void select(@NotNull final VerticalList.Model model, final int n, final int n2) {
        Intrinsics.checkParameterIsNotNull(model, "model");
        if (this.hasIconFluctuatorColor) {
            this.haloView.setVisibility(INVISIBLE);
            this.haloView.stop();
        }
        if (!this.isEnabled) {
            this.vlist.unlock();
        }
        else {
            final AnimatorSet set = new AnimatorSet();
            final ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this.switchContainer, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.SCALE_X, new float[] { 1.0f, 0.8f }), PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[] { 1.0f, 0.8f }) });
            ofPropertyValuesHolder.setDuration((long)n2);
            final ObjectAnimator ofPropertyValuesHolder2 = ObjectAnimator.ofPropertyValuesHolder(this.switchContainer, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.SCALE_X, new float[] { 0.8f, 1.0f }), PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[] { 0.8f, 1.0f }) });
            ofPropertyValuesHolder2.setDuration((long)n2);
            final AnimatorSet set2 = new AnimatorSet();
            set2.playSequentially(new Animator[] { (Animator)ofPropertyValuesHolder, (Animator)ofPropertyValuesHolder2 });
            final AnimatorSet set3 = new AnimatorSet();
            final Drawable background = this.switchBackground.getBackground();
            if (background == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
            }
            final Drawable drawable = ((LayerDrawable)background).getDrawable(1);
            if (drawable == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.ClipDrawable");
            }
            final ClipDrawable clipDrawable = (ClipDrawable)drawable;
            final ViewGroup.LayoutParams layoutParams = this.switchThumb.getLayoutParams();
            if (layoutParams == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup.MarginLayoutParams)layoutParams;
            final ValueAnimator valueAnimator = null;
            final ValueAnimator valueAnimator2 = null;
            ValueAnimator valueAnimator3;
            ValueAnimator valueAnimator4;
            if (this.isOn) {
                valueAnimator3 = ValueAnimator.ofFloat(new float[] { this.thumbMargin, 0.0f });
                valueAnimator3.setDuration(100L);
                valueAnimator4 = ValueAnimator.ofFloat(new float[] { 5000.0f, 1000.0f });
                set2.setDuration(100L);
            }
            else {
                valueAnimator3 = ValueAnimator.ofFloat(new float[] { 0.0f, this.thumbMargin });
                valueAnimator3.setDuration(100L);
                valueAnimator4 = ValueAnimator.ofFloat(new float[] { 3000.0f, 7000.0f });
                set2.setDuration(100L);
            }
            if (valueAnimator4 != null) {
                valueAnimator4.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new SwitchViewHolder$select.SwitchViewHolder$select$1(this, clipDrawable));
            }
            if (valueAnimator3 != null) {
                valueAnimator3.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new SwitchViewHolder$select.SwitchViewHolder$select$2(this, viewGroup$MarginLayoutParams));
            }
            set3.playTogether(new Animator[] { (Animator)valueAnimator3, (Animator)valueAnimator4 });
            set.play((Animator)set3).after((Animator)set2);
            set.addListener((Animator.AnimatorListener)new SwitchViewHolder$select.SwitchViewHolder$select$3(this, model, n));
            set.start();
        }
    }
    
    protected final void setAnimatorSetBuilder(@Nullable final AnimatorSet.Builder animatorSetBuilder) {
        this.animatorSetBuilder = animatorSetBuilder;
    }
    
    protected final void setHaloView(@NotNull final SwitchHaloView haloView) {
        Intrinsics.checkParameterIsNotNull(haloView, "set");
        this.haloView = haloView;
    }
    
    protected final void setIconScaleAnimationDisabled(final boolean iconScaleAnimationDisabled) {
        this.iconScaleAnimationDisabled = iconScaleAnimationDisabled;
    }
    
    @Override
    public void setItemState(@NotNull State selected, @NotNull AnimationType none, final int n, final boolean b) {
        Intrinsics.checkParameterIsNotNull(selected, "state");
        Intrinsics.checkParameterIsNotNull(none, "animation");
        float n2 = 0.0f;
        float titleUnselectedScale = 0.0f;
        float titleSelectedTopMargin = 0.0f;
        float alpha = 0.0f;
        float titleUnselectedScale2 = 0.0f;
        float alpha2 = 0.0f;
        float titleUnselectedScale3 = 0.0f;
        this.animatorSetBuilder = null;
        Label_0104: {
            if (!this.textAnimationDisabled) {
                break Label_0104;
            }
            if (!Intrinsics.areEqual(none, AnimationType.MOVE)) {
                selected = State.SELECTED;
                break Label_0104;
            }
            this.itemAnimatorSet = new AnimatorSet();
            this.animatorSetBuilder = this.itemAnimatorSet.play((Animator)ValueAnimator.ofFloat(new float[] { 0.0f, 1.0f }));
            return;
        }
        switch (SwitchViewHolder$WhenMappings.$EnumSwitchMapping$0[selected.ordinal()]) {
            case 1:
                n2 = 1.0f;
                titleUnselectedScale = 1.0f;
                titleSelectedTopMargin = this.titleSelectedTopMargin;
                alpha = 1.0f;
                titleUnselectedScale2 = 1.0f;
                alpha2 = 1.0f;
                titleUnselectedScale3 = 1.0f;
                break;
            case 2:
                n2 = 0.6f;
                titleUnselectedScale = this.titleUnselectedScale;
                titleSelectedTopMargin = 0.0f;
                alpha = 0.0f;
                titleUnselectedScale2 = this.titleUnselectedScale;
                alpha2 = 0.0f;
                titleUnselectedScale3 = this.titleUnselectedScale;
                break;
        }
        if (!this.hasSubTitle) {
            titleSelectedTopMargin = 0.0f;
            alpha = 0.0f;
            alpha2 = 0.0f;
        }
        switch (SwitchViewHolder$WhenMappings.$EnumSwitchMapping$1[none.ordinal()]) {
            case 1:
            case 2:
                this.switchContainer.setScaleX(n2);
                this.switchContainer.setScaleY(n2);
                break;
            case 3: {
                this.itemAnimatorSet = new AnimatorSet();
                final PropertyValuesHolder ofFloat = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[] { n2 });
                final PropertyValuesHolder ofFloat2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[] { n2 });
                this.animatorSetBuilder = this.itemAnimatorSet.play((Animator)ObjectAnimator.ofPropertyValuesHolder(this.title, new PropertyValuesHolder[] { ofFloat, ofFloat2 }));
                final AnimatorSet.Builder animatorSetBuilder = this.animatorSetBuilder;
                if (animatorSetBuilder == null) {
                    Intrinsics.throwNpe();
                }
                animatorSetBuilder.with((Animator)ObjectAnimator.ofPropertyValuesHolder(this.switchContainer, new PropertyValuesHolder[] { ofFloat, ofFloat2 }));
                break;
            }
        }
        this.title.setPivotX(0.0f);
        this.title.setPivotY(VerticalViewHolder.titleHeight / 2);
        switch (SwitchViewHolder$WhenMappings.$EnumSwitchMapping$2[none.ordinal()]) {
            case 1:
            case 2: {
                this.title.setScaleX(titleUnselectedScale);
                this.title.setScaleY(titleUnselectedScale);
                final ViewGroup.LayoutParams layoutParams = this.title.getLayoutParams();
                if (layoutParams == null) {
                    throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
                }
                ((ViewGroup.MarginLayoutParams)layoutParams).topMargin = (int)titleSelectedTopMargin;
                this.title.requestLayout();
                break;
            }
            case 3: {
                final PropertyValuesHolder ofFloat3 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[] { titleUnselectedScale });
                final PropertyValuesHolder ofFloat4 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[] { titleUnselectedScale });
                final AnimatorSet.Builder animatorSetBuilder2 = this.animatorSetBuilder;
                if (animatorSetBuilder2 == null) {
                    Intrinsics.throwNpe();
                }
                animatorSetBuilder2.with((Animator)ObjectAnimator.ofPropertyValuesHolder(this.title, new PropertyValuesHolder[] { ofFloat3, ofFloat4 }));
                final AnimatorSet.Builder animatorSetBuilder3 = this.animatorSetBuilder;
                if (animatorSetBuilder3 == null) {
                    Intrinsics.throwNpe();
                }
                animatorSetBuilder3.with(VerticalAnimationUtils.animateMargin((View)this.title, (int)titleSelectedTopMargin));
                break;
            }
        }
        this.subTitle.setPivotX(0.0f);
        this.subTitle.setPivotY(VerticalViewHolder.subTitleHeight / 2);
        Enum<AnimationType> none2 = none;
        if (!this.hasSubTitle) {
            none2 = AnimationType.NONE;
        }
        switch (SwitchViewHolder$WhenMappings.$EnumSwitchMapping$3[none2.ordinal()]) {
            case 1:
            case 2:
                this.subTitle.setAlpha(alpha);
                this.subTitle.setScaleX(titleUnselectedScale2);
                this.subTitle.setScaleY(titleUnselectedScale2);
                break;
            case 3: {
                final PropertyValuesHolder ofFloat5 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[] { titleUnselectedScale2 });
                final PropertyValuesHolder ofFloat6 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[] { titleUnselectedScale2 });
                final PropertyValuesHolder ofFloat7 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[] { alpha });
                final AnimatorSet.Builder animatorSetBuilder4 = this.animatorSetBuilder;
                if (animatorSetBuilder4 == null) {
                    Intrinsics.throwNpe();
                }
                animatorSetBuilder4.with((Animator)ObjectAnimator.ofPropertyValuesHolder(this.subTitle, new PropertyValuesHolder[] { ofFloat5, ofFloat6, ofFloat7 }));
                break;
            }
        }
        this.subTitle2.setPivotX(0.0f);
        this.subTitle2.setPivotY(VerticalViewHolder.subTitleHeight / 2);
        if (!this.hasSubTitle2) {
            none = AnimationType.NONE;
        }
        switch (SwitchViewHolder$WhenMappings.$EnumSwitchMapping$4[none.ordinal()]) {
            case 1:
            case 2:
                this.subTitle2.setAlpha(alpha2);
                this.subTitle2.setScaleX(titleUnselectedScale3);
                this.subTitle2.setScaleY(titleUnselectedScale3);
                break;
            case 3: {
                final PropertyValuesHolder ofFloat8 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[] { titleUnselectedScale3 });
                final PropertyValuesHolder ofFloat9 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[] { titleUnselectedScale3 });
                final PropertyValuesHolder ofFloat10 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[] { alpha });
                final AnimatorSet.Builder animatorSetBuilder5 = this.animatorSetBuilder;
                if (animatorSetBuilder5 == null) {
                    Intrinsics.throwNpe();
                }
                animatorSetBuilder5.with((Animator)ObjectAnimator.ofPropertyValuesHolder(this.subTitle2, new PropertyValuesHolder[] { ofFloat8, ofFloat9, ofFloat10 }));
                break;
            }
        }
        if (!Intrinsics.areEqual(selected, State.SELECTED) || !b) {
            return;
        }
        if (this.itemAnimatorSet != null) {
            this.itemAnimatorSet.addListener((Animator.AnimatorListener)this.fluctuatorStartListener);
            return;
        }
        this.startFluctuator();
    }
    
    protected final void setSubTitle(@NotNull final TextView subTitle) {
        Intrinsics.checkParameterIsNotNull(subTitle, "set");
        this.subTitle = subTitle;
    }
    
    protected final void setSubTitle2(@NotNull final TextView subTitle2) {
        Intrinsics.checkParameterIsNotNull(subTitle2, "set");
        this.subTitle2 = subTitle2;
    }
    
    protected final void setSwitchBackground(@NotNull final View switchBackground) {
        Intrinsics.checkParameterIsNotNull(switchBackground, "set");
        this.switchBackground = switchBackground;
    }
    
    protected final void setSwitchContainer(@NotNull final ViewGroup switchContainer) {
        Intrinsics.checkParameterIsNotNull(switchContainer, "set");
        this.switchContainer = switchContainer;
    }
    
    protected final void setSwitchThumb(@NotNull final View switchThumb) {
        Intrinsics.checkParameterIsNotNull(switchThumb, "set");
        this.switchThumb = switchThumb;
    }
    
    protected final void setTextAnimationDisabled(final boolean textAnimationDisabled) {
        this.textAnimationDisabled = textAnimationDisabled;
    }
    
    protected final void setTextContainer(@NotNull final ViewGroup textContainer) {
        Intrinsics.checkParameterIsNotNull(textContainer, "set");
        this.textContainer = textContainer;
    }
    
    protected final void setTitle(@NotNull final TextView title) {
        Intrinsics.checkParameterIsNotNull(title, "set");
        this.title = title;
    }
    
    @Override
    public void startFluctuator() {
        if (this.hasIconFluctuatorColor) {
            this.haloView.setVisibility(View.VISIBLE);
            this.haloView.start();
        }
    }
    
    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J:\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\r2\b\b\u0002\u0010\u000f\u001a\u00020\u00102\b\b\u0002\u0010\u0011\u001a\u00020\u0010J\u001e\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019J\u0016\u0010\u001a\u001a\u00020\u00152\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u001b\u001a\u00020\nR\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u001c" }, d2 = { "Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;", "", "()V", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "buildModel", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "id", "", "iconFluctuatorColor", "title", "", "subTitle", "isOn", "", "isEnabled", "buildViewHolder", "Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;", "parent", "Landroid/view/ViewGroup;", "vlist", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList;", "handler", "Landroid/os/Handler;", "getLayout", "lytId", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    public static final class Companion
    {
        @NotNull
        public final VerticalList.Model buildModel(final int id, final int iconFluctuatorColor, @NotNull final String title, @NotNull final String subTitle, final boolean isOn, final boolean isEnabled) {
            Intrinsics.checkParameterIsNotNull(title, "title");
            Intrinsics.checkParameterIsNotNull(subTitle, "subTitle");
            VerticalList.Model fromCache;
            if ((fromCache = VerticalModelCache.getFromCache(VerticalList.ModelType.SWITCH)) == null) {
                fromCache = new VerticalList.Model();
            }
            fromCache.type = VerticalList.ModelType.SWITCH;
            fromCache.id = id;
            fromCache.iconFluctuatorColor = iconFluctuatorColor;
            fromCache.title = title;
            fromCache.subTitle = subTitle;
            fromCache.isOn = isOn;
            fromCache.isEnabled = isEnabled;
            return fromCache;
        }
        
        @NotNull
        public final SwitchViewHolder buildViewHolder(@NotNull final ViewGroup viewGroup, @NotNull final VerticalList list, @NotNull final Handler handler) {
            Intrinsics.checkParameterIsNotNull(viewGroup, "parent");
            Intrinsics.checkParameterIsNotNull(list, "vlist");
            Intrinsics.checkParameterIsNotNull(handler, "handler");
            return new SwitchViewHolder(this.getLayout(viewGroup, R.layout.vlist_toggle_switch), list, handler);
        }
        
        @NotNull
        public final ViewGroup getLayout(@NotNull final ViewGroup viewGroup, final int n) {
            Intrinsics.checkParameterIsNotNull(viewGroup, "parent");
            final View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(n, viewGroup, false);
            if (inflate == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup");
            }
            return (ViewGroup)inflate;
        }
        
        @NotNull
        public final Logger getLogger() {
            return SwitchViewHolder.access$getLogger$cp();
        }
    }
}
