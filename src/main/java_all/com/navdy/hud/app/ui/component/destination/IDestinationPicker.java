package com.navdy.hud.app.ui.component.destination;

public interface IDestinationPicker
{
    void onDestinationPickerClosed();
    
    boolean onItemClicked(final int p0, final int p1, final DestinationPickerScreen.DestinationPickerState p2);
    
    boolean onItemSelected(final int p0, final int p1, final DestinationPickerScreen.DestinationPickerState p2);
}
