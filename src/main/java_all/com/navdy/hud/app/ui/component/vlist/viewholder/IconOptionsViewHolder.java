package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.AnimatorSet;
import android.content.Context;
import com.navdy.hud.app.ui.component.HaloView;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import android.view.ViewGroup;
import android.view.ViewGroup;
import com.navdy.hud.app.manager.InputManager;
import android.view.View;
import com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils;
import android.widget.TextView;
import android.widget.ImageView;
import android.graphics.Color;
import android.graphics.Shader;
import android.view.LayoutInflater;
import android.animation.Animator;
import android.os.Handler;
import android.view.ViewGroup;
import com.navdy.hud.app.HudApplication;
import java.util.ArrayList;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.animation.AnimatorSet;
import com.navdy.service.library.log.Logger;

public class IconOptionsViewHolder extends VerticalViewHolder
{
    private static final int iconMargin;
    private static final Logger sLogger;
    protected AnimatorSet.Builder animatorSetBuilder;
    private int currentSelection;
    private Runnable fluctuatorRunnable;
    private DefaultAnimationListener fluctuatorStartListener;
    private boolean itemNotSelected;
    private State lastState;
    private VerticalList.Model model;
    private ArrayList<ViewContainer> viewContainers;
    
    static {
        sLogger = new Logger(IconOptionsViewHolder.class);
        iconMargin = HudApplication.getAppContext().getResources().getDimensionPixelOffset(R.dimen.vlist_icon_list_margin);
    }
    
    public IconOptionsViewHolder(final ViewGroup viewGroup, final VerticalList list, final Handler handler) {
        super(viewGroup, list, handler);
        this.viewContainers = new ArrayList<ViewContainer>();
        this.currentSelection = -1;
        this.fluctuatorStartListener = new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                IconOptionsViewHolder.this.itemAnimatorSet = null;
                IconOptionsViewHolder.this.handler.removeCallbacks(IconOptionsViewHolder.this.fluctuatorRunnable);
                IconOptionsViewHolder.this.handler.postDelayed(IconOptionsViewHolder.this.fluctuatorRunnable, 100L);
            }
        };
        this.fluctuatorRunnable = new Runnable() {
            @Override
            public void run() {
                IconOptionsViewHolder.this.startFluctuator();
            }
        };
        viewGroup.setPivotX((float)(VerticalViewHolder.listItemHeight / 2));
        viewGroup.setPivotY((float)(VerticalViewHolder.listItemHeight / 2));
    }
    
    public static VerticalList.Model buildModel(final int id, final int[] iconList, final int[] iconIds, final int[] iconSelectedColors, final int[] iconDeselectedColors, final int[] iconFluctuatorColors, final int currentIconSelection, final boolean supportsToolTip) {
        final VerticalList.Model model = new VerticalList.Model();
        model.type = VerticalList.ModelType.ICON_OPTIONS;
        model.id = id;
        model.iconList = iconList;
        model.iconIds = iconIds;
        model.iconSelectedColors = iconSelectedColors;
        model.iconDeselectedColors = iconDeselectedColors;
        model.iconFluctuatorColors = iconFluctuatorColors;
        model.currentIconSelection = currentIconSelection;
        model.supportsToolTip = supportsToolTip;
        return model;
    }
    
    public static IconOptionsViewHolder buildViewHolder(final ViewGroup viewGroup, final VerticalList list, final Handler handler) {
        return new IconOptionsViewHolder((ViewGroup)LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.vlist_icon_options, viewGroup, false), list, handler);
    }
    
    public static int getOffSetFromPos(int n) {
        if (n <= 0) {
            n = 0;
        }
        else {
            n = VerticalViewHolder.mainIconSize * n + IconOptionsViewHolder.iconMargin * n;
        }
        return n;
    }
    
    private boolean isItemInBounds(final int n) {
        return this.model != null && n >= 0 && n < this.model.iconList.length;
    }
    
    private void startFluctuator(final int n, final boolean b) {
        if (this.isItemInBounds(n)) {
            final ViewContainer viewContainer = this.viewContainers.get(n);
            if (b) {
                viewContainer.big.setIcon(this.model.iconList[n], this.model.iconSelectedColors[n], null, 0.83f);
            }
            viewContainer.haloView.setVisibility(View.VISIBLE);
            viewContainer.haloView.start();
        }
    }
    
    private void stopFluctuator(final int n, final boolean b) {
        if (this.isItemInBounds(n)) {
            this.handler.removeCallbacks(this.fluctuatorRunnable);
            final ViewContainer viewContainer = this.viewContainers.get(n);
            if (b) {
                viewContainer.big.setIcon(this.model.iconList[n], this.model.iconDeselectedColors[n], null, 0.83f);
            }
            viewContainer.haloView.setVisibility(GONE);
            viewContainer.haloView.stop();
        }
    }
    
    @Override
    public void bind(final VerticalList.Model model, final VerticalList.ModelState modelState) {
        if (this.vlist.adapter.getModel(this.vlist.getRawPosition()) != model) {
            this.itemNotSelected = true;
        }
        for (int i = 0; i < model.iconList.length; ++i) {
            final ViewContainer viewContainer = this.viewContainers.get(i);
            viewContainer.big.setIcon(model.iconList[i], model.iconSelectedColors[i], null, 0.83f);
            viewContainer.small.setIcon(model.iconList[i], model.iconDeselectedColors[i], null, 0.83f);
            final int argb = Color.argb(153, Color.red(model.iconFluctuatorColors[i]), Color.green(model.iconFluctuatorColors[i]), Color.blue(model.iconFluctuatorColors[i]));
            viewContainer.haloView.setVisibility(INVISIBLE);
            viewContainer.haloView.setStrokeColor(argb);
        }
    }
    
    @Override
    public void clearAnimation() {
        this.stopFluctuator(this.currentSelection, true);
        this.stopAnimation();
        this.currentState = null;
        this.layout.setVisibility(View.VISIBLE);
        this.layout.setAlpha(1.0f);
    }
    
    @Override
    public void copyAndPosition(final ImageView imageView, final TextView textView, final TextView textView2, final TextView textView3, final boolean b) {
        if (this.currentSelection >= 0) {
            final View big = this.viewContainers.get(this.currentSelection).crossFadeImageView.getBig();
            if (b) {
                VerticalAnimationUtils.copyImage((ImageView)big, imageView);
            }
            imageView.setX((float)(IconOptionsViewHolder.selectedImageX + getOffSetFromPos(this.currentSelection)));
            imageView.setY((float)IconOptionsViewHolder.selectedImageY);
        }
    }
    
    public int getCurrentSelection() {
        return this.currentSelection;
    }
    
    public int getCurrentSelectionId() {
        int n;
        if (this.currentSelection < 0 || this.currentSelection >= this.model.iconIds.length) {
            n = -1;
        }
        else {
            n = this.model.iconIds[this.currentSelection];
        }
        return n;
    }
    
    @Override
    public VerticalList.ModelType getModelType() {
        return VerticalList.ModelType.ICON_OPTIONS;
    }
    
    @Override
    public boolean handleKey(final InputManager.CustomKeyEvent customKeyEvent) {
        boolean b = false;
        if (this.isItemInBounds(this.currentSelection)) {
            switch (customKeyEvent) {
                case LEFT:
                    if (this.currentSelection != 0) {
                        --this.currentSelection;
                        this.stopFluctuator(this.currentSelection + 1, true);
                        this.startFluctuator(this.currentSelection, true);
                        final VerticalList.ItemSelectionState itemSelectionState = this.vlist.getItemSelectionState();
                        itemSelectionState.set(this.model, this.model.id, this.vlist.getCurrentPosition(), this.model.iconIds[this.currentSelection], this.currentSelection);
                        this.vlist.callItemSelected(itemSelectionState);
                        b = true;
                        break;
                    }
                    break;
                case RIGHT:
                    if (this.currentSelection != this.model.iconList.length - 1) {
                        ++this.currentSelection;
                        this.stopFluctuator(this.currentSelection - 1, true);
                        this.startFluctuator(this.currentSelection, true);
                        final VerticalList.ItemSelectionState itemSelectionState2 = this.vlist.getItemSelectionState();
                        itemSelectionState2.set(this.model, this.model.id, this.vlist.getCurrentPosition(), this.model.iconIds[this.currentSelection], this.currentSelection);
                        this.vlist.callItemSelected(itemSelectionState2);
                        b = true;
                        break;
                    }
                    break;
            }
        }
        return b;
    }
    
    @Override
    public boolean hasToolTip() {
        return this.model.supportsToolTip;
    }
    
    @Override
    public void preBind(final VerticalList.Model model, final VerticalList.ModelState modelState) {
        this.model = model;
        final Context context = this.layout.getContext();
        if (this.layout.getChildCount() != model.iconList.length + model.iconList.length - 1) {
            this.currentSelection = model.currentIconSelection;
            IconOptionsViewHolder.sLogger.v("preBind: createIcons:" + model.iconList.length);
            this.layout.removeAllViews();
            this.viewContainers.clear();
            final LayoutInflater from = LayoutInflater.from(this.layout.getContext());
            for (int i = 0; i < model.iconList.length; ++i) {
                if (i != 0) {
                    final View view = new View(context);
                    view.setLayoutParams((ViewGroup.LayoutParams)new ViewGroup.MarginLayoutParams(VerticalViewHolder.iconMargin, VerticalViewHolder.mainIconSize));
                    this.layout.addView(view);
                }
                final ViewContainer viewContainer = new ViewContainer();
                final ViewGroup viewGroup = (ViewGroup)from.inflate(R.layout.vlist_halo_image, this.layout, false);
                viewContainer.iconContainer = (ViewGroup)viewGroup.findViewById(R.id.iconContainer);
                (viewContainer.crossFadeImageView = (CrossFadeImageView)viewGroup.findViewById(R.id.crossFadeImageView)).inject(CrossFadeImageView.Mode.SMALL);
                viewContainer.big = (IconColorImageView)viewGroup.findViewById(R.id.big);
                viewContainer.small = (IconColorImageView)viewGroup.findViewById(R.id.small);
                viewContainer.haloView = (HaloView)viewGroup.findViewById(R.id.halo);
                this.viewContainers.add(viewContainer);
                viewGroup.setLayoutParams((ViewGroup.LayoutParams)new ViewGroup.MarginLayoutParams(VerticalViewHolder.mainIconSize, VerticalViewHolder.mainIconSize));
                this.layout.addView((View)viewGroup);
            }
        }
        else {
            IconOptionsViewHolder.sLogger.v("preBind: createIcons: already created");
        }
    }
    
    @Override
    public void select(final VerticalList.Model model, final int n, final int n2) {
        if (this.isItemInBounds(this.currentSelection)) {
            this.stopFluctuator(this.currentSelection, false);
            VerticalAnimationUtils.performClick((View)this.viewContainers.get(this.currentSelection).iconContainer, n2, new Runnable() {
                @Override
                public void run() {
                    final VerticalList.ItemSelectionState itemSelectionState = IconOptionsViewHolder.this.vlist.getItemSelectionState();
                    itemSelectionState.set(IconOptionsViewHolder.this.model, IconOptionsViewHolder.this.model.id, n, IconOptionsViewHolder.this.model.iconIds[IconOptionsViewHolder.this.currentSelection], IconOptionsViewHolder.this.currentSelection);
                    IconOptionsViewHolder.this.vlist.performSelectAction(itemSelectionState);
                }
            });
        }
    }
    
    public void selectionDown() {
        IconOptionsViewHolder.sLogger.v("selectionDown");
        if (!this.isItemInBounds(this.currentSelection)) {
            IconOptionsViewHolder.sLogger.v("selectionDown:invalid pos:" + this.currentSelection);
        }
        else {
            this.stopFluctuator(this.currentSelection, false);
            VerticalAnimationUtils.performClickDown((View)this.viewContainers.get(this.currentSelection).iconContainer, 25, null, false);
        }
    }
    
    public void selectionUp() {
        IconOptionsViewHolder.sLogger.v("selectionUp");
        if (!this.isItemInBounds(this.currentSelection)) {
            IconOptionsViewHolder.sLogger.v("selectionUp:invalid pos:" + this.currentSelection);
        }
        else {
            this.startFluctuator(this.currentSelection, false);
            VerticalAnimationUtils.performClickUp((View)this.viewContainers.get(this.currentSelection).iconContainer, 25, null);
        }
    }
    
    @Override
    public void setItemState(final State lastState, final AnimationType animationType, int i, final boolean b) {
        if (this.lastState != lastState) {
            i = 0;
            Enum<AnimationType> move = animationType;
            if (this.itemNotSelected) {
                i = 1;
                this.itemNotSelected = false;
                move = AnimationType.MOVE;
            }
            this.animatorSetBuilder = null;
            float n = 0.0f;
            CrossFadeImageView.Mode mode = null;
            switch (lastState) {
                case SELECTED:
                    n = 1.0f;
                    mode = CrossFadeImageView.Mode.BIG;
                    break;
                case UNSELECTED:
                    n = 0.6f;
                    mode = CrossFadeImageView.Mode.SMALL;
                    break;
            }
            switch (move) {
                case NONE:
                case INIT: {
                    ViewContainer viewContainer;
                    for (i = 0; i < this.model.iconList.length; ++i) {
                        viewContainer = this.viewContainers.get(i);
                        viewContainer.iconContainer.setScaleX(n);
                        viewContainer.iconContainer.setScaleY(n);
                        viewContainer.crossFadeImageView.setMode(mode);
                        if (i == this.currentSelection) {
                            viewContainer.big.setIcon(this.model.iconList[i], this.model.iconSelectedColors[i], null, 0.83f);
                        }
                        else {
                            viewContainer.big.setIcon(this.model.iconList[i], this.model.iconDeselectedColors[i], null, 0.83f);
                        }
                    }
                    break;
                }
                case MOVE: {
                    if (i != 0) {
                        ViewContainer viewContainer2;
                        for (i = 0; i < this.model.iconList.length; ++i) {
                            viewContainer2 = this.viewContainers.get(i);
                            viewContainer2.big.setIcon(this.model.iconList[i], this.model.iconDeselectedColors[i], null, 0.83f);
                            if (viewContainer2.crossFadeImageView.getMode() != mode) {
                                viewContainer2.crossFadeImageView.setMode(mode);
                            }
                        }
                        this.layout.setScaleX(n);
                        this.layout.setScaleY(n);
                        break;
                    }
                    this.itemAnimatorSet = new AnimatorSet();
                    final int n2 = i = 0;
                    if (this.currentSelection != -1) {
                        final ViewContainer viewContainer3 = this.viewContainers.get(this.currentSelection);
                        i = n2;
                        if (viewContainer3.crossFadeImageView.getMode() != mode) {
                            i = 1;
                            viewContainer3.crossFadeImageView.setMode(mode);
                        }
                    }
                    for (int j = 0; j < this.model.iconList.length; ++j) {
                        final ViewContainer viewContainer4 = this.viewContainers.get(j);
                        if ((j != this.currentSelection || i == 0) && viewContainer4.crossFadeImageView.getMode() != mode) {
                            if (this.animatorSetBuilder == null) {
                                this.animatorSetBuilder = this.itemAnimatorSet.play((Animator)viewContainer4.crossFadeImageView.getCrossFadeAnimator());
                            }
                            else {
                                this.animatorSetBuilder.with((Animator)viewContainer4.crossFadeImageView.getCrossFadeAnimator());
                            }
                        }
                    }
                    final PropertyValuesHolder ofFloat = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[] { n });
                    final PropertyValuesHolder ofFloat2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[] { n });
                    if (this.animatorSetBuilder == null) {
                        this.animatorSetBuilder = this.itemAnimatorSet.play((Animator)ObjectAnimator.ofPropertyValuesHolder(this.layout, new PropertyValuesHolder[] { ofFloat, ofFloat2 }));
                        break;
                    }
                    this.animatorSetBuilder.with((Animator)ObjectAnimator.ofPropertyValuesHolder(this.layout, new PropertyValuesHolder[] { ofFloat, ofFloat2 }));
                    break;
                }
            }
            if (lastState == State.SELECTED) {
                if (this.itemAnimatorSet != null) {
                    this.itemAnimatorSet.addListener((Animator.AnimatorListener)this.fluctuatorStartListener);
                }
                else {
                    this.startFluctuator();
                }
            }
            this.lastState = lastState;
        }
    }
    
    @Override
    public void startFluctuator() {
        this.startFluctuator(this.currentSelection, true);
    }
    
    private static class ViewContainer
    {
        IconColorImageView big;
        CrossFadeImageView crossFadeImageView;
        HaloView haloView;
        ViewGroup iconContainer;
        IconColorImageView small;
    }
}
