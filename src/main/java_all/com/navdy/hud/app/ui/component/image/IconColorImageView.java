package com.navdy.hud.app.ui.component.image;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.graphics.Paint;
import android.content.Context;
import android.graphics.Shader;
import android.widget.ImageView;

public class IconColorImageView extends ImageView
{
    private int bkColor;
    private Shader bkColorGradient;
    private Context context;
    private boolean draw;
    private int iconResource;
    private IconShape iconShape;
    private Paint paint;
    private float scaleF;
    
    public IconColorImageView(final Context context) {
        this(context, null, 0);
    }
    
    public IconColorImageView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public IconColorImageView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.iconShape = IconShape.CIRCLE;
        this.draw = true;
        this.context = context;
        this.init();
    }
    
    private void init() {
        (this.paint = new Paint()).setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
    }
    
    protected void onDraw(final Canvas canvas) {
        final int width = this.getWidth();
        final int height = this.getHeight();
        canvas.drawColor(0);
        if (this.bkColorGradient != null) {
            this.paint.setShader(this.bkColorGradient);
        }
        else {
            this.paint.setShader((Shader)null);
            this.paint.setColor(this.bkColor);
        }
        if (this.iconShape == null || this.iconShape == IconShape.CIRCLE) {
            canvas.drawCircle((float)(width / 2), (float)(height / 2), (float)(width / 2), this.paint);
        }
        else {
            canvas.drawRect(0.0f, 0.0f, (float)width, (float)height, this.paint);
        }
        if (!this.draw) {
            super.onDraw(canvas);
        }
        else if (this.iconResource != 0) {
            final Drawable drawable = this.context.getDrawable(this.iconResource);
            if (drawable != null) {
                final Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();
                if (bitmap != null) {
                    canvas.scale(this.scaleF, this.scaleF);
                    canvas.drawBitmap(bitmap, 0.0f, 0.0f, (Paint)null);
                }
            }
        }
    }
    
    public void setDraw(final boolean draw) {
        this.draw = draw;
    }
    
    public void setIcon(final int iconResource, final int bkColor, final Shader bkColorGradient, final float scaleF) {
        this.iconResource = iconResource;
        this.bkColor = bkColor;
        this.bkColorGradient = bkColorGradient;
        this.scaleF = scaleF;
        this.invalidate();
    }
    
    public void setIconShape(final IconShape iconShape) {
        this.iconShape = iconShape;
    }
    
    public enum IconShape
    {
        CIRCLE, 
        SQUARE;
    }
}
