package com.navdy.hud.app.ui.component;

import android.animation.Animator;
import android.graphics.Paint;
import android.graphics.Canvas;
import android.content.res.TypedArray;
import android.animation.TimeInterpolator;
import com.navdy.hud.app.R;
import android.animation.Animator;
import android.os.Looper;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.util.AttributeSet;
import android.content.Context;
import android.animation.ValueAnimator;
import android.graphics.Paint;
import android.view.animation.Interpolator;
import android.os.Handler;
import android.animation.ValueAnimator;
import android.animation.AnimatorSet;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.view.View;

public class HaloView extends View
{
    private int animationDelay;
    private int animationDuration;
    private DefaultAnimationListener animationListener;
    private AnimatorSet animatorSet;
    private AnimatorSet currentAnimatorSet;
    protected float currentStrokeWidth;
    protected float endRadius;
    protected float endStrokeWidth;
    private AnimatorSet firstAnimatorSet;
    private boolean firstIterationDone;
    private ValueAnimator firstRadiusAnimator;
    private Handler handler;
    private Interpolator interpolator;
    protected float middleRadius;
    protected float middleStrokeWidth;
    private Paint paint;
    private ValueAnimator radiusAnimator;
    private AnimatorSet reverseAnimatorSet;
    private ValueAnimator reverseRadiusAnimator;
    protected float startRadius;
    private Runnable startRunnable;
    private boolean started;
    private int strokeColor;
    private ValueAnimator.AnimatorUpdateListener updateListener;
    
    public HaloView(final Context context) {
        this(context, null);
    }
    
    public HaloView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public HaloView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.interpolator = (Interpolator)new AccelerateDecelerateInterpolator();
        this.handler = new Handler(Looper.getMainLooper());
        this.animationListener = new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                if (HaloView.this.started) {
                    HaloView.this.handler.removeCallbacks(HaloView.this.startRunnable);
                    HaloView.this.handler.postDelayed(HaloView.this.startRunnable, (long)HaloView.this.animationDelay);
                }
            }
        };
        this.startRunnable = new Runnable() {
            @Override
            public void run() {
                HaloView.this.toggleAnimator();
                HaloView.this.currentAnimatorSet.start();
            }
        };
        this.updateListener = (ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                HaloView.this.onAnimationUpdateInternal(valueAnimator);
            }
        };
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.HaloView, n, 0);
        if (obtainStyledAttributes != null) {
            this.strokeColor = obtainStyledAttributes.getColor(0, -1);
            this.startRadius = obtainStyledAttributes.getDimension(1, 0.0f);
            this.endRadius = obtainStyledAttributes.getDimension(2, 0.0f);
            this.middleRadius = obtainStyledAttributes.getDimension(3, 0.0f);
            this.animationDuration = obtainStyledAttributes.getInteger(4, 0);
            this.animationDelay = obtainStyledAttributes.getInteger(5, 0);
            obtainStyledAttributes.recycle();
        }
        this.endStrokeWidth = this.endRadius - this.startRadius;
        if (this.endStrokeWidth < 0.0f) {
            this.endStrokeWidth = 0.0f;
        }
        this.middleStrokeWidth = this.endRadius - this.middleRadius;
        if (this.middleStrokeWidth < 0.0f) {
            this.middleStrokeWidth = 0.0f;
        }
        (this.paint = new Paint()).setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
        this.firstRadiusAnimator = ValueAnimator.ofFloat(new float[] { this.startRadius, this.endRadius });
        this.radiusAnimator = ValueAnimator.ofFloat(new float[] { this.middleRadius, this.endRadius });
        this.reverseRadiusAnimator = ValueAnimator.ofFloat(new float[] { this.endRadius, this.middleRadius });
        this.firstRadiusAnimator.addUpdateListener(this.updateListener);
        this.radiusAnimator.addUpdateListener(this.updateListener);
        this.reverseRadiusAnimator.addUpdateListener(this.updateListener);
        (this.firstAnimatorSet = new AnimatorSet()).setDuration((long)this.animationDuration);
        this.firstAnimatorSet.setInterpolator((TimeInterpolator)this.interpolator);
        (this.animatorSet = new AnimatorSet()).setDuration((long)this.animationDuration);
        this.animatorSet.setInterpolator((TimeInterpolator)this.interpolator);
        (this.reverseAnimatorSet = new AnimatorSet()).setDuration((long)this.animationDuration);
        this.reverseAnimatorSet.setInterpolator((TimeInterpolator)this.interpolator);
        this.animatorSet.play((Animator)this.radiusAnimator);
        this.reverseAnimatorSet.play((Animator)this.reverseRadiusAnimator);
        this.currentStrokeWidth = this.endStrokeWidth;
        this.invalidate();
    }
    
    private void toggleAnimator() {
        if (this.currentAnimatorSet == this.animatorSet) {
            this.currentAnimatorSet = this.reverseAnimatorSet;
        }
        else {
            this.currentAnimatorSet = this.animatorSet;
        }
    }
    
    public void onAnimationUpdateInternal(final ValueAnimator valueAnimator) {
        this.currentStrokeWidth = (float)valueAnimator.getAnimatedValue() - this.startRadius;
        this.invalidate();
    }
    
    public void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        this.onDrawInternal(canvas);
    }
    
    public void onDrawInternal(final Canvas canvas) {
        this.paint.setStyle(Paint.Style.FILL);
        this.paint.setStrokeWidth(0.0f);
        this.paint.setColor(this.strokeColor);
        canvas.drawCircle((float)(this.getWidth() / 2), (float)(this.getHeight() / 2), this.startRadius + this.currentStrokeWidth, this.paint);
    }
    
    public void setDuration(final int animationDuration) {
        this.animationDuration = animationDuration;
    }
    
    public void setEndRadius(final int n) {
        this.endRadius = n;
    }
    
    public void setMiddleRadius(final int n) {
        this.middleRadius = n;
    }
    
    public void setStartDelay(final int animationDelay) {
        this.animationDelay = animationDelay;
    }
    
    public void setStartRadius(final int n) {
        this.startRadius = n;
    }
    
    public void setStrokeColor(final int strokeColor) {
        this.strokeColor = strokeColor;
        this.invalidate();
    }
    
    public void setStrokeWidth(final int n) {
        this.currentStrokeWidth = n;
    }
    
    public void start() {
        this.stop();
        this.started = true;
        this.firstAnimatorSet.addListener((Animator.AnimatorListener)this.animationListener);
        this.animatorSet.addListener((Animator.AnimatorListener)this.animationListener);
        this.reverseAnimatorSet.addListener((Animator.AnimatorListener)this.animationListener);
        if (!this.firstIterationDone) {
            this.firstIterationDone = true;
            this.currentAnimatorSet = this.animatorSet;
            this.firstAnimatorSet.start();
        }
        else {
            this.currentAnimatorSet = this.reverseAnimatorSet;
            this.reverseAnimatorSet.start();
        }
    }
    
    public void stop() {
        this.started = false;
        this.handler.removeCallbacks(this.startRunnable);
        if (this.firstAnimatorSet.isRunning()) {
            this.firstAnimatorSet.removeAllListeners();
            this.firstAnimatorSet.cancel();
        }
        if (this.animatorSet.isRunning()) {
            this.animatorSet.removeAllListeners();
            this.animatorSet.cancel();
        }
        if (this.reverseAnimatorSet.isRunning()) {
            this.reverseAnimatorSet.removeAllListeners();
            this.reverseAnimatorSet.cancel();
        }
        this.currentStrokeWidth = this.endStrokeWidth;
        this.requestLayout();
    }
}
