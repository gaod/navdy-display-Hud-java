package com.navdy.hud.app.screen;

import com.navdy.hud.app.event.ShowScreenWithArgs;
import android.os.Bundle;
import com.navdy.service.library.events.ui.ShowScreen;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import javax.inject.Singleton;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.view.DialUpdateConfirmationView;
import com.navdy.hud.app.ui.activity.Main;
import dagger.Module;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import flow.Layout;

@Layout(R.layout.screen_dial_update_confirmation)
public class DialUpdateConfirmationScreen extends BaseScreen
{
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(DialUpdateConfirmationScreen.class);
    }
    
    @Override
    public Object getDaggerModule() {
        return new Module();
    }
    
    @Override
    public String getMortarScopeName() {
        return DialUpdateConfirmationScreen.class.getName();
    }
    
    @Override
    public Screen getScreen() {
        return Screen.SCREEN_DIAL_UPDATE_CONFIRMATION;
    }
    
    @dagger.Module(addsTo = Main.Module.class, injects = { DialUpdateConfirmationView.class })
    public class Module
    {
    }
    
    @Singleton
    public static class Presenter extends BasePresenter<DialUpdateConfirmationView>
    {
        private boolean isReminder;
        @Inject
        Bus mBus;
        
        public void finish() {
            this.mBus.post(new ShowScreen.Builder().screen(Screen.SCREEN_BACK).build());
        }
        
        public void install() {
            final Bundle bundle = new Bundle();
            bundle.putInt("PROGRESS_CAUSE", 1);
            this.mBus.post(new ShowScreenWithArgs(Screen.SCREEN_DIAL_UPDATE_PROGRESS, bundle, false));
        }
        
        public boolean isReminder() {
            return this.isReminder;
        }
        
        @Override
        public void onLoad(final Bundle bundle) {
            super.onLoad(bundle);
            if (bundle != null) {
                this.isReminder = bundle.getBoolean("UPDATE_REMINDER", false);
            }
            else {
                DialUpdateConfirmationScreen.sLogger.e("null bundle passed to onLoad()");
                this.isReminder = false;
            }
        }
    }
}
