package com.navdy.hud.app.screen;

import com.navdy.service.library.events.ui.ShowScreen;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import javax.inject.Singleton;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.view.TemperatureWarningView;
import com.navdy.hud.app.ui.activity.Main;
import dagger.Module;
import com.navdy.service.library.events.ui.Screen;
import flow.Layout;

@Layout(R.layout.temperature_warning_lyt)
public class TemperatureWarningScreen extends BaseScreen
{
    @Override
    public Object getDaggerModule() {
        return new Module();
    }
    
    @Override
    public String getMortarScopeName() {
        return TemperatureWarningScreen.class.getName();
    }
    
    @Override
    public Screen getScreen() {
        return Screen.SCREEN_TEMPERATURE_WARNING;
    }
    
    @dagger.Module(addsTo = Main.Module.class, injects = { TemperatureWarningView.class })
    public class Module
    {
    }
    
    @Singleton
    public static class Presenter extends BasePresenter<TemperatureWarningView>
    {
        @Inject
        Bus mBus;
        
        public void finish() {
            this.mBus.post(new ShowScreen.Builder().screen(Screen.SCREEN_BACK).build());
        }
    }
}
