package com.navdy.service.library.task;

class TaskManager$PriorityRunnable implements Runnable {
    final long order;
    final int priority;
    final Runnable runnable;
    
    TaskManager$PriorityRunnable(Runnable a, com.navdy.service.library.task.TaskManager$TaskPriority a0, long j) {
        this.runnable = a;
        this.priority = a0.getValue();
        this.order = j;
    }
    
    public void run() {
        try {
            this.runnable.run();
        } catch(Throwable a) {
            com.navdy.service.library.task.TaskManager.access$000().e("TaskManager:", a);
        }
    }
}
