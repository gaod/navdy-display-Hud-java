package com.navdy.service.library.device.connection;

class ConnectionListener$3 implements com.navdy.service.library.device.connection.ConnectionListener$EventDispatcher {
    final com.navdy.service.library.device.connection.ConnectionListener this$0;
    
    ConnectionListener$3(com.navdy.service.library.device.connection.ConnectionListener a) {
        super();
        this.this$0 = a;
    }
    
    public void dispatchEvent(com.navdy.service.library.device.connection.ConnectionListener a, com.navdy.service.library.device.connection.ConnectionListener$Listener a0) {
        a0.onStopped(a);
    }
    
    public void dispatchEvent(com.navdy.service.library.util.Listenable a, com.navdy.service.library.util.Listenable$Listener a0) {
        this.dispatchEvent((com.navdy.service.library.device.connection.ConnectionListener)a, (com.navdy.service.library.device.connection.ConnectionListener$Listener)a0);
    }
}
