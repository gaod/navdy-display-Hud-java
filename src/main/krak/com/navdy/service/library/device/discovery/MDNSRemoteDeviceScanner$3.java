package com.navdy.service.library.device.discovery;

class MDNSRemoteDeviceScanner$3 implements android.net.nsd.NsdManager.ResolveListener {
    final com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner this$0;
    
    MDNSRemoteDeviceScanner$3(com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner a) {
        super();
        this.this$0 = a;
    }
    
    public void onResolveFailed(android.net.nsd.NsdServiceInfo a, int i) {
        this.this$0.mResolveListeners.remove(this);
        com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.e(new StringBuilder().append("Resolve failed").append(i).toString());
        this.this$0.resolveComplete();
    }
    
    public void onServiceResolved(android.net.nsd.NsdServiceInfo a) {
        this.this$0.mResolveListeners.remove(this);
        com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.e(new StringBuilder().append("Resolve Succeeded. ").append(a).toString());
        label1: {
            label0: {
                com.navdy.service.library.device.connection.ConnectionInfo a0 = null;
                try {
                    a0 = com.navdy.service.library.device.connection.ConnectionInfo.fromServiceInfo(a);
                } catch(IllegalArgumentException ignoredException) {
                    break label0;
                }
                this.this$0.mHandler.post((Runnable)new com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner$3$1(this, a0));
                this.this$0.resolveComplete();
                break label1;
            }
            com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.e("Unable to process serviceInfo");
        }
    }
}
