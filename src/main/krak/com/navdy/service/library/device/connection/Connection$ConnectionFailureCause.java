package com.navdy.service.library.device.connection;


public enum Connection$ConnectionFailureCause {
    UNKNOWN(0),
    CONNECTION_REFUSED(1),
    CONNECTION_TIMED_OUT(2);

    private int value;
    Connection$ConnectionFailureCause(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class Connection$ConnectionFailureCause extends Enum {
//    final private static com.navdy.service.library.device.connection.Connection$ConnectionFailureCause[] $VALUES;
//    final public static com.navdy.service.library.device.connection.Connection$ConnectionFailureCause CONNECTION_REFUSED;
//    final public static com.navdy.service.library.device.connection.Connection$ConnectionFailureCause CONNECTION_TIMED_OUT;
//    final public static com.navdy.service.library.device.connection.Connection$ConnectionFailureCause UNKNOWN;
//    
//    static {
//        UNKNOWN = new com.navdy.service.library.device.connection.Connection$ConnectionFailureCause("UNKNOWN", 0);
//        CONNECTION_REFUSED = new com.navdy.service.library.device.connection.Connection$ConnectionFailureCause("CONNECTION_REFUSED", 1);
//        CONNECTION_TIMED_OUT = new com.navdy.service.library.device.connection.Connection$ConnectionFailureCause("CONNECTION_TIMED_OUT", 2);
//        com.navdy.service.library.device.connection.Connection$ConnectionFailureCause[] a = new com.navdy.service.library.device.connection.Connection$ConnectionFailureCause[3];
//        a[0] = UNKNOWN;
//        a[1] = CONNECTION_REFUSED;
//        a[2] = CONNECTION_TIMED_OUT;
//        $VALUES = a;
//    }
//    
//    private Connection$ConnectionFailureCause(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.service.library.device.connection.Connection$ConnectionFailureCause valueOf(String s) {
//        return (com.navdy.service.library.device.connection.Connection$ConnectionFailureCause)Enum.valueOf(com.navdy.service.library.device.connection.Connection$ConnectionFailureCause.class, s);
//    }
//    
//    public static com.navdy.service.library.device.connection.Connection$ConnectionFailureCause[] values() {
//        return $VALUES.clone();
//    }
//}
//