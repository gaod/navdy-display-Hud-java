package com.navdy.service.library.device.connection;

abstract public class ConnectionService extends android.app.Service implements com.navdy.service.library.device.connection.ConnectionListener$Listener, com.navdy.service.library.device.RemoteDevice$Listener {
    final public static java.util.UUID ACCESSORY_IAP2;
    final public static String ACTION_LINK_BANDWIDTH_LEVEL_CHANGED = "LINK_BANDWIDTH_LEVEL_CHANGED";
    final public static String CATEGORY_NAVDY_LINK = "NAVDY_LINK";
    final private static int CONNECT_TIMEOUT = 10000;
    final private static int DEAD_CONNECTION_TIME = 60000;
    final private static com.navdy.service.library.events.connection.ConnectionStatus DEVICES_CHANGED_EVENT;
    final public static java.util.UUID DEVICE_IAP2;
    final private static int EVENT_DISCONNECT = 3;
    final protected static int EVENT_HEARTBEAT = 2;
    final private static int EVENT_RESTART_LISTENERS = 4;
    final private static int EVENT_STATE_CHANGE = 1;
    final public static String EXTRA_BANDWIDTH_LEVEL = "EXTRA_BANDWIDTH_MODE";
    final private static int HEARTBEAT_INTERVAL = 4000;
    final private static int IDLE_TIMEOUT = 1000;
    final public static String ILLEGAL_ARGUMENT = "ILLEGAL_ARGUMENT";
    final public static String NAVDY_IAP_NAME = "Navdy iAP";
    final public static String NAVDY_PROTO_SERVICE_NAME = "Navdy";
    final public static java.util.UUID NAVDY_PROTO_SERVICE_UUID;
    final public static java.util.UUID NAVDY_PROXY_TUNNEL_UUID;
    final private static int PAIRING_TIMEOUT = 30000;
    final public static String REASON_DEAD_CONNECTION = "DEAD_CONNECTION";
    final private static int RECONNECT_DELAY = 2000;
    final private static int RECONNECT_TIMEOUT = 10000;
    final protected static int RETRY_CONNECTING_AFTER_FAILURE_TIMEOUT = 30000;
    final private static int SLEEP_TIMEOUT = 60000;
    private android.content.BroadcastReceiver bluetoothReceiver;
    private boolean broadcasting;
    final protected Object connectionLock;
    protected com.navdy.service.library.device.RemoteDeviceRegistry deviceRegistry;
    private boolean forceReconnect;
    protected boolean inProcess;
    private long lastMessageReceivedTime;
    final protected Object listenerLock;
    private com.navdy.hud.app.IEventListener[] listenersArray;
    private boolean listening;
    final protected com.navdy.service.library.log.Logger logger;
    protected com.navdy.service.library.device.connection.ConnectionListener[] mConnectionListeners;
    private com.navdy.hud.app.IEventSource$Stub mEventSource;
    final private java.util.List mListeners;
    protected com.navdy.service.library.device.RemoteDevice mRemoteDevice;
    protected com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster[] mRemoteDeviceBroadcasters;
    protected com.squareup.wire.Wire mWire;
    private com.navdy.service.library.device.connection.ConnectionService$PendingConnectHandler pendingConnectHandler;
    private com.navdy.service.library.device.connection.ProxyService proxyService;
    private volatile boolean quitting;
    protected Runnable reconnectRunnable;
    protected boolean serverMode;
    protected volatile com.navdy.service.library.device.connection.ConnectionService$ServiceHandler serviceHandler;
    private volatile android.os.Looper serviceLooper;
    protected com.navdy.service.library.device.connection.ConnectionService$State state;
    private long stateAge;
    
    static {
        DEVICES_CHANGED_EVENT = new com.navdy.service.library.events.connection.ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus$Status.CONNECTION_PAIRED_DEVICES_CHANGED, (String)null);
        NAVDY_PROTO_SERVICE_UUID = java.util.UUID.fromString("1992B7D7-C9B9-4F4F-AA7F-9FE33D95851E");
        NAVDY_PROXY_TUNNEL_UUID = java.util.UUID.fromString("D72BC85F-F015-4F24-A72F-35924E10888F");
        DEVICE_IAP2 = java.util.UUID.fromString("00000000-deca-fade-deca-deafdecacafe");
        ACCESSORY_IAP2 = java.util.UUID.fromString("00000000-deca-fade-deca-deafdecacaff");
    }
    
    public ConnectionService() {
        this.logger = new com.navdy.service.library.log.Logger((this).getClass());
        this.connectionLock = new Object();
        this.listenerLock = new Object();
        this.mListeners = (java.util.List)new java.util.ArrayList();
        this.listenersArray = new com.navdy.hud.app.IEventListener[0];
        this.stateAge = 0L;
        this.serverMode = false;
        this.inProcess = true;
        this.listening = false;
        this.broadcasting = false;
        this.forceReconnect = false;
        this.mEventSource = new com.navdy.service.library.device.connection.ConnectionService$1(this);
        this.state = com.navdy.service.library.device.connection.ConnectionService$State.START;
        this.reconnectRunnable = (Runnable)new com.navdy.service.library.device.connection.ConnectionService$2(this);
        this.bluetoothReceiver = new com.navdy.service.library.device.connection.ConnectionService$3(this);
    }
    
    static boolean access$000(com.navdy.service.library.device.connection.ConnectionService a) {
        return a.listening;
    }
    
    static boolean access$100(com.navdy.service.library.device.connection.ConnectionService a) {
        return a.broadcasting;
    }
    
    static java.util.List access$200(com.navdy.service.library.device.connection.ConnectionService a) {
        return a.mListeners;
    }
    
    static void access$300(com.navdy.service.library.device.connection.ConnectionService a) {
        a.createListenerList();
    }
    
    static com.navdy.hud.app.IEventListener[] access$400(com.navdy.service.library.device.connection.ConnectionService a) {
        return a.listenersArray;
    }
    
    static com.navdy.service.library.device.connection.ConnectionService$PendingConnectHandler access$502(com.navdy.service.library.device.connection.ConnectionService a, com.navdy.service.library.device.connection.ConnectionService$PendingConnectHandler a0) {
        a.pendingConnectHandler = a0;
        return a0;
    }
    
    private void checkConnection() {
        com.navdy.service.library.device.RemoteDevice a = this.mRemoteDevice;
        label0: {
            label1: {
                if (a == null) {
                    break label1;
                }
                if (!this.mRemoteDevice.isConnected()) {
                    break label1;
                }
                if (this.mRemoteDevice.getLinkStatus() != com.navdy.service.library.device.RemoteDevice$LinkStatus.CONNECTED) {
                    break label0;
                }
                long j = android.os.SystemClock.elapsedRealtime() - this.lastMessageReceivedTime;
                if (j <= 60000L) {
                    break label0;
                }
                this.logger.v(new StringBuilder().append("dead connection timed out:").append(j).toString());
                if (this.reconnectAfterDeadConnection()) {
                    this.logger.d("Reconncting after dead connection timed out disconnect");
                    this.reconnect("DEAD_CONNECTION");
                    break label0;
                } else {
                    this.setState(com.navdy.service.library.device.connection.ConnectionService$State.DISCONNECTING);
                    break label0;
                }
            }
            com.navdy.service.library.log.Logger a0 = this.logger;
            StringBuilder a1 = new StringBuilder().append("dead connection remotedevice=").append((this.mRemoteDevice != null) ? "not null" : "null").append(" isConnected:");
            Object a2 = (this.mRemoteDevice != null) ? Boolean.valueOf(this.mRemoteDevice.isConnected()) : "false";
            a0.v(a1.append(a2).toString());
            this.setState(com.navdy.service.library.device.connection.ConnectionService$State.DISCONNECTING);
        }
    }
    
    private void createListenerList() {
        this.listenersArray = new com.navdy.hud.app.IEventListener[this.mListeners.size()];
        this.mListeners.toArray((Object[])this.listenersArray);
    }
    
    private void handleDeviceConnect(com.navdy.service.library.device.RemoteDevice a) {
        this.logger.v("onDeviceConnected:remembering device");
        this.rememberPairedDevice(a);
        this.logger.v("onDeviceConnected:heartBeat started");
        this.lastMessageReceivedTime = android.os.SystemClock.elapsedRealtime();
        android.content.Context a0 = this.getApplicationContext();
        this.logger.d(new StringBuilder().append("Connecting with app context: ").append(a0).toString());
        if (!this.mRemoteDevice.startLink()) {
            this.setState(com.navdy.service.library.device.connection.ConnectionService$State.DISCONNECTING);
        }
    }
    
    private void handleRemoteDisconnect(com.navdy.service.library.events.connection.DisconnectRequest a) {
        if (this.mRemoteDevice != null) {
            if (a.forget != null && a.forget.booleanValue()) {
                this.forgetPairedDevice(this.mRemoteDevice);
            }
            this.setState(com.navdy.service.library.device.connection.ConnectionService$State.DISCONNECTING);
        }
    }
    
    private boolean hasPaired() {
        return this.deviceRegistry.hasPaired();
    }
    
    private void sendPingIfNeeded() {
        try {
            if (this.mRemoteDevice != null && this.mRemoteDevice.isConnected() && android.os.SystemClock.elapsedRealtime() - this.mRemoteDevice.lastMessageSentTime > 4000L) {
                this.mRemoteDevice.postEvent((com.squareup.wire.Message)new com.navdy.service.library.events.Ping());
                if (this.serverMode && this.logger.isLoggable(2)) {
                    this.logger.v("NAVDY-PACKET [H2P-Outgoing-Event] Ping");
                }
            }
        } catch(Throwable a) {
            this.logger.e(a);
        }
    }
    
    public void connect(com.navdy.service.library.device.connection.ConnectionInfo a) {
        this.setActiveDevice(new com.navdy.service.library.device.RemoteDevice((android.content.Context)this, a, this.inProcess));
    }
    
    abstract protected com.navdy.service.library.device.connection.ProxyService createProxyService();
    
    
    protected void enterState(com.navdy.service.library.device.connection.ConnectionService$State a) {
        this.logger.d(new StringBuilder().append("Entering state:").append(a).toString());
        if (a != com.navdy.service.library.device.connection.ConnectionService$State.DESTROYED) {
            int i = 0;
            this.stateAge = 0L;
            switch(com.navdy.service.library.device.connection.ConnectionService$4.$SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State[a.ordinal()]) {
                case 7: {
                    this.stopListeners();
                    this.stopBroadcasters();
                    this.handleDeviceConnect(this.mRemoteDevice);
                    i = 1000;
                    break;
                }
                case 6: {
                    if (this.mRemoteDevice == null) {
                        i = 1000;
                        break;
                    } else {
                        this.mRemoteDevice.disconnect();
                        i = 1000;
                        break;
                    }
                }
                case 5: {
                    i = 10000;
                    break;
                }
                case 4: {
                    i = 10000;
                    break;
                }
                case 3: {
                    if (this.isPromiscuous()) {
                        this.startBroadcasters();
                    }
                    this.startListeners();
                    i = (this.serverMode) ? 30000 : 1000;
                    break;
                }
                case 2: {
                    this.startBroadcasters();
                    this.startListeners();
                    this.forwardEventLocally((com.squareup.wire.Message)new com.navdy.service.library.events.connection.ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus$Status.CONNECTION_PAIRING, ""));
                    i = 30000;
                    break;
                }
                case 1: {
                    i = 1000;
                    break;
                }
                default: {
                    i = 1000;
                }
            }
            this.serviceHandler.removeMessages(2);
            this.serviceHandler.sendEmptyMessageDelayed(2, (long)i);
        } else {
            if (this.listening) {
                this.stopListeners();
            }
            if (this.broadcasting) {
                this.stopBroadcasters();
            }
            this.serviceHandler.removeCallbacksAndMessages(null);
        }
    }
    
    protected void exitState(com.navdy.service.library.device.connection.ConnectionService$State a) {
        this.logger.d(new StringBuilder().append("Exiting state:").append(a).toString());
        switch(com.navdy.service.library.device.connection.ConnectionService$4.$SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State[a.ordinal()]) {
            case 8: {
                this.startProxyService();
                break;
            }
            case 5: {
                this.serviceHandler.removeCallbacks(this.reconnectRunnable);
                break;
            }
            case 2: {
                if (this.isPromiscuous()) {
                    break;
                }
                this.stopBroadcasters();
                break;
            }
        }
    }
    
    protected void forgetPairedDevice(android.bluetooth.BluetoothDevice a) {
        this.deviceRegistry.removePairedConnection(a);
        this.forwardEventLocally((com.squareup.wire.Message)DEVICES_CHANGED_EVENT);
    }
    
    protected void forgetPairedDevice(com.navdy.service.library.device.RemoteDevice a) {
        this.deviceRegistry.removePairedConnection(a.getActiveConnectionInfo());
        this.forwardEventLocally((com.squareup.wire.Message)DEVICES_CHANGED_EVENT);
    }
    
    protected void forwardEventLocally(com.squareup.wire.Message a) {
        this.forwardEventLocally(com.navdy.service.library.events.NavdyEventUtil.eventFromMessage(a).toByteArray());
    }
    
    protected void forwardEventLocally(byte[] a) {
        try {
            this.mEventSource.postEvent(a);
        } catch(Throwable a0) {
            this.logger.e(a0);
        }
    }
    
    abstract protected com.navdy.service.library.device.connection.ConnectionListener[] getConnectionListeners(android.content.Context arg);
    
    
    abstract protected com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster[] getRemoteDeviceBroadcasters();
    
    
    protected void handleDeviceDisconnect(com.navdy.service.library.device.RemoteDevice a, com.navdy.service.library.device.connection.Connection$DisconnectCause a0) {
        this.logger.v(new StringBuilder().append("device disconnect:").append(a).append(" cause:").append(a0).toString());
        if (a != null) {
            a.stopLink();
        }
        label2: if (this.mRemoteDevice == a) {
            com.navdy.service.library.device.connection.Connection$DisconnectCause a1 = com.navdy.service.library.device.connection.Connection$DisconnectCause.ABORTED;
            label0: {
                label1: {
                    if (a0 == a1) {
                        break label1;
                    }
                    if (!this.forceReconnect) {
                        break label0;
                    }
                }
                if (!this.serverMode) {
                    break label0;
                }
                this.forceReconnect = false;
                new com.navdy.service.library.device.connection.ConnectionService$PendingConnectHandler(this, this.mRemoteDevice, (com.navdy.service.library.device.connection.Connection)null).run();
                break label2;
            }
            this.setRemoteDevice((com.navdy.service.library.device.RemoteDevice)null);
            this.setState(com.navdy.service.library.device.connection.ConnectionService$State.IDLE);
        }
    }
    
    protected void heartBeat() {
        int i = 0;
        if (this.stateAge % 10L == 0L) {
            this.logger.d(new StringBuilder().append("Heartbeat: in state:").append(this.state).toString());
        }
        this.stateAge = this.stateAge + 1L;
        switch(com.navdy.service.library.device.connection.ConnectionService$4.$SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State[this.state.ordinal()]) {
            case 7: {
                this.sendPingIfNeeded();
                this.checkConnection();
                i = 4000;
                break;
            }
            case 4: case 5: {
                this.setState(com.navdy.service.library.device.connection.ConnectionService$State.IDLE);
                i = 1000;
                break;
            }
            case 3: {
                if (this.serverMode) {
                    if (this.mRemoteDevice == null) {
                        if (this.needAutoSearch()) {
                            this.setState(com.navdy.service.library.device.connection.ConnectionService$State.SEARCHING);
                            i = 1000;
                            break;
                        } else {
                            i = 60000;
                            break;
                        }
                    } else {
                        this.setState(com.navdy.service.library.device.connection.ConnectionService$State.RECONNECTING);
                        this.serviceHandler.post(this.reconnectRunnable);
                        i = 1000;
                        break;
                    }
                } else {
                    i = 60000;
                    break;
                }
            }
            case 2: {
                i = 30000;
                break;
            }
            case 1: {
                if (this.pendingConnectHandler == null) {
                    boolean b = this.hasPaired();
                    label0: {
                        if (b) {
                            break label0;
                        }
                        if (!this.serverMode) {
                            break label0;
                        }
                        this.setState(com.navdy.service.library.device.connection.ConnectionService$State.PAIRING);
                        i = 1000;
                        break;
                    }
                    if (this.hasPaired()) {
                        this.setState(com.navdy.service.library.device.connection.ConnectionService$State.LISTENING);
                        i = 1000;
                        break;
                    } else {
                        i = 60000;
                        break;
                    }
                } else {
                    this.pendingConnectHandler.run();
                    i = 1000;
                    break;
                }
            }
            default: {
                i = 1000;
                break;
            }
            case 6: {
                i = 1000;
            }
        }
        this.serviceHandler.removeMessages(2);
        this.serviceHandler.sendEmptyMessageDelayed(2, (long)i);
    }
    
    public boolean isConnected() {
        boolean b = false;
        com.navdy.service.library.device.RemoteDevice a = this.mRemoteDevice;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (this.mRemoteDevice.isConnected()) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public boolean isPromiscuous() {
        return false;
    }
    
    public boolean needAutoSearch() {
        return true;
    }
    
    public android.os.IBinder onBind(android.content.Intent a) {
        com.navdy.hud.app.IEventSource$Stub a0 = null;
        if (com.navdy.hud.app.IEventSource.class.getName().equals(a.getAction())) {
            this.logger.d("returning IEventSource API endpoint");
            a0 = this.mEventSource;
        } else {
            this.logger.w(new StringBuilder().append("invalid action:").append(a.getAction()).toString());
            a0 = null;
        }
        return (android.os.IBinder)a0;
    }
    
    public void onConnected(com.navdy.service.library.device.connection.ConnectionListener a, com.navdy.service.library.device.connection.Connection a0) {
        this.logger.v("listener connected");
        this.setActiveDevice(new com.navdy.service.library.device.RemoteDevice(this.getApplicationContext(), a0.getConnectionInfo(), this.inProcess), a0);
    }
    
    public void onConnectionFailed(com.navdy.service.library.device.connection.ConnectionListener a) {
        this.logger.e("onConnectionFailed:restart listeners");
        this.setState(com.navdy.service.library.device.connection.ConnectionService$State.IDLE);
    }
    
    public void onCreate() {
        long j = android.os.Looper.getMainLooper().getThread().getId();
        this.logger.e(new StringBuilder().append("ConnectionService created: mainthread:").append(j).toString());
        Class[] a = new Class[1];
        a[0] = com.navdy.service.library.events.Ext_NavdyEvent.class;
        this.mWire = new com.squareup.wire.Wire(a);
        android.os.HandlerThread a0 = new android.os.HandlerThread("ConnectionService");
        a0.start();
        this.serviceLooper = a0.getLooper();
        this.serviceHandler = new com.navdy.service.library.device.connection.ConnectionService$ServiceHandler(this, this.serviceLooper);
        this.registerReceiver(this.bluetoothReceiver, new android.content.IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
        this.deviceRegistry = com.navdy.service.library.device.RemoteDeviceRegistry.getInstance((android.content.Context)this);
    }
    
    public void onDestroy() {
        this.logger.v("ConnectionService destroyed.  mwhahahaha.");
        com.navdy.service.library.device.connection.ConnectionService$State a = com.navdy.service.library.device.connection.ConnectionService$State.DESTROYED;
        try {
            this.setState(a);
            this.unregisterReceiver(this.bluetoothReceiver);
            this.serviceLooper.quitSafely();
            this.stopProxyService();
        } catch(Throwable a0) {
            this.logger.e(a0);
        }
    }
    
    public void onDeviceConnectFailure(com.navdy.service.library.device.RemoteDevice a, com.navdy.service.library.device.connection.Connection$ConnectionFailureCause a0) {
        if (this.state != com.navdy.service.library.device.connection.ConnectionService$State.RECONNECTING) {
            this.logger.i("onDeviceConnectFailure - switching to idle state");
            com.navdy.service.library.device.RemoteDevice a1 = this.mRemoteDevice;
            label1: {
                label0: {
                    if (a != a1) {
                        break label0;
                    }
                    if (this.serverMode) {
                        break label0;
                    }
                    this.setRemoteDevice((com.navdy.service.library.device.RemoteDevice)null);
                    break label1;
                }
                this.logger.d("Not clearing the remote device, to attempt reconnect");
            }
            this.setState(com.navdy.service.library.device.connection.ConnectionService$State.IDLE);
        } else {
            this.logger.i("onDeviceConnectFailure - retrying");
            this.serviceHandler.postDelayed(this.reconnectRunnable, 2000L);
        }
    }
    
    public void onDeviceConnected(com.navdy.service.library.device.RemoteDevice a) {
        this.setState(com.navdy.service.library.device.connection.ConnectionService$State.CONNECTED);
    }
    
    public void onDeviceConnecting(com.navdy.service.library.device.RemoteDevice a) {
    }
    
    public void onDeviceDisconnected(com.navdy.service.library.device.RemoteDevice a, com.navdy.service.library.device.connection.Connection$DisconnectCause a0) {
        android.os.Message a1 = this.serviceHandler.obtainMessage(3, a0.ordinal(), -1, a);
        this.serviceHandler.sendMessage(a1);
    }
    
    public void onNavdyEventReceived(com.navdy.service.library.device.RemoteDevice a, com.navdy.service.library.events.NavdyEvent a0) {
        this.lastMessageReceivedTime = android.os.SystemClock.elapsedRealtime();
        if (a0.type == com.navdy.service.library.events.NavdyEvent$MessageType.DisconnectRequest) {
            this.handleRemoteDisconnect((com.navdy.service.library.events.connection.DisconnectRequest)a0.getExtension(com.navdy.service.library.events.Ext_NavdyEvent.disconnectRequest));
        }
    }
    
    public void onNavdyEventReceived(com.navdy.service.library.device.RemoteDevice a, byte[] a0) {
        this.lastMessageReceivedTime = android.os.SystemClock.elapsedRealtime();
        com.navdy.service.library.events.NavdyEvent$MessageType a1 = com.navdy.service.library.events.WireUtil.getEventType(a0);
        if (this.logger.isLoggable(2)) {
            String s = (a1 == null) ? "UNKNOWN" : a1.name();
            this.logger.v(new StringBuilder().append((this.serverMode) ? "NAVDY-PACKET [P2H" : "NAVDY-PACKET [H2P").append("-Event] ").append(s).append(" size:").append(a0.length).toString());
        }
        label1: {
            Throwable a2 = null;
            if (a1 == null) {
                break label1;
            }
            if (a1 == com.navdy.service.library.events.NavdyEvent$MessageType.Ping) {
                break label1;
            }
            com.navdy.service.library.events.NavdyEvent$MessageType a3 = com.navdy.service.library.events.NavdyEvent$MessageType.DisconnectRequest;
            label0: {
                Throwable a4 = null;
                if (a1 != a3) {
                    if (this.processEvent(a0, a1)) {
                        break label1;
                    }
                    if (a1 != com.navdy.service.library.events.NavdyEvent$MessageType.DeviceInfo) {
                        this.forwardEventLocally(a0);
                        break label1;
                    } else {
                        try {
                            if (this.mRemoteDevice == null) {
                                break label1;
                            }
                            if (!this.mRemoteDevice.isConnected()) {
                                break label1;
                            }
                            com.navdy.service.library.events.DeviceInfo a5 = new com.navdy.service.library.events.DeviceInfo$Builder((com.navdy.service.library.events.DeviceInfo)com.navdy.service.library.events.NavdyEventUtil.messageFromEvent((com.navdy.service.library.events.NavdyEvent)this.mWire.parseFrom(a0, com.navdy.service.library.events.NavdyEvent.class))).deviceId(this.mRemoteDevice.getDeviceId().toString()).build();
                            this.mRemoteDevice.setDeviceInfo(a5);
                            this.logger.v("set remote device info");
                            this.forwardEventLocally(com.navdy.service.library.events.NavdyEventUtil.eventFromMessage((com.squareup.wire.Message)a5).toByteArray());
                            break label1;
                        } catch(Throwable a6) {
                            a4 = a6;
                        }
                    }
                } else {
                    try {
                        this.handleRemoteDisconnect((com.navdy.service.library.events.connection.DisconnectRequest)((com.navdy.service.library.events.NavdyEvent)this.mWire.parseFrom(a0, com.navdy.service.library.events.NavdyEvent.class)).getExtension(com.navdy.service.library.events.Ext_NavdyEvent.disconnectRequest));
                    } catch(Throwable a7) {
                        a2 = a7;
                        break label0;
                    }
                    break label1;
                }
                this.logger.e("Failed to parse deviceinfo", a4);
                break label1;
            }
            this.logger.e("Failed to parse event", a2);
        }
    }
    
    public int onStartCommand(android.content.Intent a, int i, int i0) {
        if (a != null && android.bluetooth.BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            if (this.state != com.navdy.service.library.device.connection.ConnectionService$State.CONNECTED) {
                this.logger.i("BT enabled, switching to IDLE state");
                this.setState(com.navdy.service.library.device.connection.ConnectionService$State.IDLE);
            } else {
                this.logger.i("BT enabled, disconnecting since app likely crashed");
                this.setState(com.navdy.service.library.device.connection.ConnectionService$State.DISCONNECTING);
            }
        }
        return 1;
    }
    
    public void onStartFailure(com.navdy.service.library.device.connection.ConnectionListener a) {
        this.logger.e(new StringBuilder().append("failed to start listening:").append(a).toString());
    }
    
    public void onStarted(com.navdy.service.library.device.connection.ConnectionListener a) {
        this.logger.v("started listening");
    }
    
    public void onStopped(com.navdy.service.library.device.connection.ConnectionListener a) {
        this.logger.v(new StringBuilder().append("stopped listening:").append(a).toString());
    }
    
    protected boolean processEvent(byte[] a, com.navdy.service.library.events.NavdyEvent$MessageType a0) {
        return false;
    }
    
    protected boolean processLocalEvent(byte[] a, com.navdy.service.library.events.NavdyEvent$MessageType a0) {
        return false;
    }
    
    public void reconnect(String s) {
        synchronized(this) {
            if (this.mRemoteDevice != null) {
                this.forceReconnect = true;
                this.setState(com.navdy.service.library.device.connection.ConnectionService$State.DISCONNECTING);
            }
        }
        /*monexit(this)*/;
    }
    
    public boolean reconnectAfterDeadConnection() {
        return false;
    }
    
    protected void rememberPairedDevice(com.navdy.service.library.device.RemoteDevice a) {
        this.deviceRegistry.addPairedConnection(a.getActiveConnectionInfo());
        this.forwardEventLocally((com.squareup.wire.Message)DEVICES_CHANGED_EVENT);
    }
    
    protected void sendEventsOnLocalConnect() {
    }
    
    public void sendMessage(com.squareup.wire.Message a) {
        if (this.isConnected()) {
            this.mRemoteDevice.postEvent(a);
        }
    }
    
    public boolean setActiveDevice(com.navdy.service.library.device.RemoteDevice a) {
        return this.setActiveDevice(a, (com.navdy.service.library.device.connection.Connection)null);
    }
    
    public boolean setActiveDevice(com.navdy.service.library.device.RemoteDevice a, com.navdy.service.library.device.connection.Connection a0) {
        synchronized(this) {
            label2: if (this.mRemoteDevice != null) {
                boolean b = this.mRemoteDevice.isConnected();
                label0: {
                    label1: {
                        if (b) {
                            break label1;
                        }
                        if (!this.mRemoteDevice.isConnecting()) {
                            break label0;
                        }
                    }
                    this.setState(com.navdy.service.library.device.connection.ConnectionService$State.DISCONNECTING);
                    break label2;
                }
                this.setRemoteDevice((com.navdy.service.library.device.RemoteDevice)null);
            }
            if (a != null) {
                com.navdy.service.library.device.connection.ConnectionService$PendingConnectHandler a1 = new com.navdy.service.library.device.connection.ConnectionService$PendingConnectHandler(this, a, a0);
                if (this.mRemoteDevice != null) {
                    this.pendingConnectHandler = a1;
                } else {
                    a1.run();
                }
            }
        }
        /*monexit(this)*/;
        return true;
    }
    
    protected void setBandwidthLevel(int i) {
        label0: if (this.state == com.navdy.service.library.device.connection.ConnectionService$State.CONNECTED && this.mRemoteDevice != null) {
            label1: {
                if (i == 0) {
                    break label1;
                }
                if (i != 1) {
                    break label0;
                }
            }
            this.mRemoteDevice.setLinkBandwidthLevel(i);
            com.navdy.service.library.events.connection.LinkPropertiesChanged$Builder a = new com.navdy.service.library.events.connection.LinkPropertiesChanged$Builder();
            a.bandwidthLevel(Integer.valueOf(i));
            this.forwardEventLocally((com.squareup.wire.Message)a.build());
        }
    }
    
    protected void setRemoteDevice(com.navdy.service.library.device.RemoteDevice a) {
        synchronized(this) {
            if (a != this.mRemoteDevice) {
                if (this.mRemoteDevice != null) {
                    this.mRemoteDevice.removeListener((com.navdy.service.library.util.Listenable$Listener)this);
                }
                this.mRemoteDevice = a;
                if (this.mRemoteDevice != null) {
                    this.mRemoteDevice.addListener((com.navdy.service.library.util.Listenable$Listener)this);
                }
            }
        }
        /*monexit(this)*/;
    }
    
    public void setState(com.navdy.service.library.device.connection.ConnectionService$State a) {
        if (!this.quitting && this.state != a) {
            if (a == com.navdy.service.library.device.connection.ConnectionService$State.DESTROYED) {
                this.quitting = true;
            }
            android.os.Message a0 = this.serviceHandler.obtainMessage(1, a);
            this.serviceHandler.sendMessage(a0);
        }
    }
    
    protected void startBroadcasters() {
        synchronized(this.connectionLock) {
            this.broadcasting = true;
            if (this.mRemoteDeviceBroadcasters == null) {
                this.mRemoteDeviceBroadcasters = this.getRemoteDeviceBroadcasters();
            }
            int i = 0;
            while(i < this.mRemoteDeviceBroadcasters.length) {
                com.navdy.service.library.log.Logger a0 = this.logger;
                StringBuilder a1 = new StringBuilder().append("starting connection broadcaster:");
                Object a2 = this.mRemoteDeviceBroadcasters[i];
                a0.v(a1.append(a2.getClass().getName()).toString());
                try {
                    Object a3 = this.mRemoteDeviceBroadcasters[i];
                    ((com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster)a3).start();
                } catch(Throwable a4) {
                    this.logger.e(a4);
                }
                i = i + 1;
            }
            /*monexit(a)*/;
        }
    }
    
    protected void startListeners() {
        synchronized(this.connectionLock) {
            this.listening = true;
            if (this.mConnectionListeners == null) {
                this.logger.v("initializing connector listener");
                this.mConnectionListeners = this.getConnectionListeners(this.getApplicationContext());
                int i = 0;
                while(i < this.mConnectionListeners.length) {
                    this.mConnectionListeners[i].addListener((com.navdy.service.library.util.Listenable$Listener)this);
                    i = i + 1;
                }
            }
            int i0 = 0;
            while(i0 < this.mConnectionListeners.length) {
                this.logger.v(new StringBuilder().append("starting connection listener:").append(this.mConnectionListeners[i0]).toString());
                try {
                    this.mConnectionListeners[i0].start();
                } catch(Throwable a0) {
                    this.logger.e(a0);
                }
                i0 = i0 + 1;
            }
            /*monexit(a)*/;
        }
    }
    
    protected void startProxyService() {
        com.navdy.service.library.device.connection.ProxyService a = this.proxyService;
        label0: {
            label1: {
                if (a == null) {
                    break label1;
                }
                if (this.proxyService.isAlive()) {
                    break label0;
                }
            }
            String s = com.navdy.service.library.util.SystemUtils.getProcessName(this.getApplicationContext(), android.os.Process.myPid());
            this.logger.v(new StringBuilder().append(s).append(": start service for proxy").toString());
            try {
                this.proxyService = this.createProxyService();
                this.proxyService.start();
            } catch(Exception a0) {
                this.logger.e("Failed to start proxy service", (Throwable)a0);
            }
        }
    }
    
    protected void stopBroadcasters() {
        this.logger.v("stopping all broadcasters");
        synchronized(this.connectionLock) {
            this.broadcasting = false;
            if (this.mRemoteDeviceBroadcasters != null) {
                int i = 0;
                while(i < this.mRemoteDeviceBroadcasters.length) {
                    com.navdy.service.library.log.Logger a0 = this.logger;
                    StringBuilder a1 = new StringBuilder().append("stopping connection broadcaster:");
                    Object a2 = this.mRemoteDeviceBroadcasters[i];
                    a0.v(a1.append(a2.getClass().getName()).toString());
                    try {
                        Object a3 = this.mRemoteDeviceBroadcasters[i];
                        ((com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster)a3).stop();
                    } catch(Throwable a4) {
                        this.logger.e(a4);
                    }
                    i = i + 1;
                }
            }
            /*monexit(a)*/;
        }
    }
    
    protected void stopListeners() {
        synchronized(this.connectionLock) {
            this.listening = false;
            if (this.mConnectionListeners != null) {
                int i = 0;
                while(i < this.mConnectionListeners.length) {
                    this.logger.v(new StringBuilder().append("stopping:").append(this.mConnectionListeners[i]).toString());
                    try {
                        this.mConnectionListeners[i].stop();
                    } catch(Throwable a0) {
                        this.logger.e(a0);
                    }
                    i = i + 1;
                }
            }
            /*monexit(a)*/;
        }
    }
    
    protected void stopProxyService() {
        if (this.proxyService != null) {
            this.proxyService.cancel();
            this.proxyService = null;
        }
    }
}
