package com.navdy.service.library.device;

abstract public interface RemoteDeviceRegistry$DeviceListUpdatedListener extends com.navdy.service.library.util.Listenable$Listener {
    abstract public void onDeviceListChanged(java.util.Set arg);
}
