package com.navdy.service.library.device.connection;

class Connection$4 implements com.navdy.service.library.device.connection.Connection$EventDispatcher {
    final com.navdy.service.library.device.connection.Connection this$0;
    final com.navdy.service.library.device.connection.Connection$DisconnectCause val$cause;
    
    Connection$4(com.navdy.service.library.device.connection.Connection a, com.navdy.service.library.device.connection.Connection$DisconnectCause a0) {
        super();
        this.this$0 = a;
        this.val$cause = a0;
    }
    
    public void dispatchEvent(com.navdy.service.library.device.connection.Connection a, com.navdy.service.library.device.connection.Connection$Listener a0) {
        a0.onDisconnected(a, this.val$cause);
    }
    
    public void dispatchEvent(com.navdy.service.library.util.Listenable a, com.navdy.service.library.util.Listenable$Listener a0) {
        this.dispatchEvent((com.navdy.service.library.device.connection.Connection)a, (com.navdy.service.library.device.connection.Connection$Listener)a0);
    }
}
