package com.navdy.service.library.device.connection;

abstract public class Connection extends com.navdy.service.library.util.Listenable {
    private static java.util.Map factoryMap;
    private static com.navdy.service.library.device.connection.Connection$ConnectionFactory sDefaultFactory;
    protected com.navdy.service.library.log.Logger logger;
    protected com.navdy.service.library.device.connection.ConnectionInfo mConnectionInfo;
    protected com.navdy.service.library.device.connection.Connection$Status mStatus;
    
    static {
        factoryMap = (java.util.Map)new java.util.HashMap();
        sDefaultFactory = (com.navdy.service.library.device.connection.Connection$ConnectionFactory)new com.navdy.service.library.device.connection.Connection$1();
        com.navdy.service.library.device.connection.Connection.registerConnectionType(com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF, sDefaultFactory);
        com.navdy.service.library.device.connection.Connection.registerConnectionType(com.navdy.service.library.device.connection.ConnectionType.BT_TUNNEL, sDefaultFactory);
        com.navdy.service.library.device.connection.Connection.registerConnectionType(com.navdy.service.library.device.connection.ConnectionType.TCP_PROTOBUF, sDefaultFactory);
        com.navdy.service.library.device.connection.Connection.registerConnectionType(com.navdy.service.library.device.connection.ConnectionType.BT_IAP2_LINK, sDefaultFactory);
        com.navdy.service.library.device.connection.Connection.registerConnectionType(com.navdy.service.library.device.connection.ConnectionType.EA_PROTOBUF, sDefaultFactory);
    }
    
    public Connection(com.navdy.service.library.device.connection.ConnectionInfo a) {
        this.logger = new com.navdy.service.library.log.Logger((this).getClass());
        this.mConnectionInfo = a;
        this.mStatus = com.navdy.service.library.device.connection.Connection$Status.DISCONNECTED;
    }
    
    public static com.navdy.service.library.device.connection.Connection instantiateFromConnectionInfo(android.content.Context a, com.navdy.service.library.device.connection.ConnectionInfo a0) {
        Object a1 = factoryMap.get(a0.getType());
        if (a1 == null) {
            throw new IllegalArgumentException(new StringBuilder().append("Unknown connection class for type: ").append(a0.getType()).toString());
        }
        return ((com.navdy.service.library.device.connection.Connection$ConnectionFactory)a1).build(a, a0);
    }
    
    public static void registerConnectionType(com.navdy.service.library.device.connection.ConnectionType a, com.navdy.service.library.device.connection.Connection$ConnectionFactory a0) {
        factoryMap.put(a, a0);
    }
    
    abstract public boolean connect();
    
    
    abstract public boolean disconnect();
    
    
    protected void dispatchConnectEvent() {
        this.dispatchToListeners((com.navdy.service.library.util.Listenable$EventDispatcher)new com.navdy.service.library.device.connection.Connection$2(this));
    }
    
    protected void dispatchConnectionFailedEvent(com.navdy.service.library.device.connection.Connection$ConnectionFailureCause a) {
        this.dispatchToListeners((com.navdy.service.library.util.Listenable$EventDispatcher)new com.navdy.service.library.device.connection.Connection$3(this, a));
    }
    
    protected void dispatchDisconnectEvent(com.navdy.service.library.device.connection.Connection$DisconnectCause a) {
        this.dispatchToListeners((com.navdy.service.library.util.Listenable$EventDispatcher)new com.navdy.service.library.device.connection.Connection$4(this, a));
    }
    
    public com.navdy.service.library.device.connection.ConnectionInfo getConnectionInfo() {
        return this.mConnectionInfo;
    }
    
    abstract public com.navdy.service.library.network.SocketAdapter getSocket();
    
    
    public com.navdy.service.library.device.connection.Connection$Status getStatus() {
        return this.mStatus;
    }
    
    public com.navdy.service.library.device.connection.ConnectionType getType() {
        return this.mConnectionInfo.getType();
    }
}
