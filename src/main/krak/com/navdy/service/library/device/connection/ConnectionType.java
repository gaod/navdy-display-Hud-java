package com.navdy.service.library.device.connection;


public enum ConnectionType {
    HTTP(0),
    TCP_PROTOBUF(1),
    BT_PROTOBUF(2),
    EA_PROTOBUF(3),
    BT_TUNNEL(4),
    EA_TUNNEL(5),
    BT_IAP2_LINK(6);

    private int value;
    ConnectionType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class ConnectionType extends Enum {
//    final private static com.navdy.service.library.device.connection.ConnectionType[] $VALUES;
//    final public static com.navdy.service.library.device.connection.ConnectionType BT_IAP2_LINK;
//    final public static com.navdy.service.library.device.connection.ConnectionType BT_PROTOBUF;
//    final public static com.navdy.service.library.device.connection.ConnectionType BT_TUNNEL;
//    final public static com.navdy.service.library.device.connection.ConnectionType EA_PROTOBUF;
//    final public static com.navdy.service.library.device.connection.ConnectionType EA_TUNNEL;
//    final public static com.navdy.service.library.device.connection.ConnectionType HTTP;
//    final public static com.navdy.service.library.device.connection.ConnectionType TCP_PROTOBUF;
//    final private String mServiceType;
//    
//    static {
//        HTTP = new com.navdy.service.library.device.connection.ConnectionType("HTTP", 0, "_http._tcp.");
//        TCP_PROTOBUF = new com.navdy.service.library.device.connection.ConnectionType("TCP_PROTOBUF", 1, "_navdybus._tcp.");
//        BT_PROTOBUF = new com.navdy.service.library.device.connection.ConnectionType("BT_PROTOBUF", 2);
//        EA_PROTOBUF = new com.navdy.service.library.device.connection.ConnectionType("EA_PROTOBUF", 3);
//        BT_TUNNEL = new com.navdy.service.library.device.connection.ConnectionType("BT_TUNNEL", 4);
//        EA_TUNNEL = new com.navdy.service.library.device.connection.ConnectionType("EA_TUNNEL", 5);
//        BT_IAP2_LINK = new com.navdy.service.library.device.connection.ConnectionType("BT_IAP2_LINK", 6);
//        com.navdy.service.library.device.connection.ConnectionType[] a = new com.navdy.service.library.device.connection.ConnectionType[7];
//        a[0] = HTTP;
//        a[1] = TCP_PROTOBUF;
//        a[2] = BT_PROTOBUF;
//        a[3] = EA_PROTOBUF;
//        a[4] = BT_TUNNEL;
//        a[5] = EA_TUNNEL;
//        a[6] = BT_IAP2_LINK;
//        $VALUES = a;
//    }
//    
//    private ConnectionType(String s, int i) {
//        super(s, i);
//        this.mServiceType = null;
//    }
//    
//    private ConnectionType(String s, int i, String s0) {
//        super(s, i);
//        this.mServiceType = s0;
//    }
//    
//    public static com.navdy.service.library.device.connection.ConnectionType fromServiceType(String s) {
//        com.navdy.service.library.device.connection.ConnectionType[] a = com.navdy.service.library.device.connection.ConnectionType.values();
//        int i = a.length;
//        int i0 = 0;
//        while(true) {
//            com.navdy.service.library.device.connection.ConnectionType a0 = null;
//            if (i0 >= i) {
//                a0 = null;
//            } else {
//                a0 = a[i0];
//                if (!s.equals(a0.getServiceType())) {
//                    i0 = i0 + 1;
//                    continue;
//                }
//            }
//            return a0;
//        }
//    }
//    
//    public static java.util.Set getServiceTypes() {
//        java.util.HashSet a = new java.util.HashSet(com.navdy.service.library.device.connection.ConnectionType.values().length);
//        com.navdy.service.library.device.connection.ConnectionType[] a0 = com.navdy.service.library.device.connection.ConnectionType.values();
//        int i = a0.length;
//        int i0 = 0;
//        while(i0 < i) {
//            com.navdy.service.library.device.connection.ConnectionType a1 = a0[i0];
//            if (a1.getServiceType() != null) {
//                a.add(a1.getServiceType());
//            }
//            i0 = i0 + 1;
//        }
//        return (java.util.Set)a;
//    }
//    
//    public static com.navdy.service.library.device.connection.ConnectionType valueOf(String s) {
//        return (com.navdy.service.library.device.connection.ConnectionType)Enum.valueOf(com.navdy.service.library.device.connection.ConnectionType.class, s);
//    }
//    
//    public static com.navdy.service.library.device.connection.ConnectionType[] values() {
//        return $VALUES.clone();
//    }
//    
//    public String getServiceType() {
//        return this.mServiceType;
//    }
//}
//