package com.navdy.service.library.device.discovery;

class RemoteDeviceScanner$3 implements com.navdy.service.library.device.discovery.RemoteDeviceScanner$ScanEventDispatcher {
    final com.navdy.service.library.device.discovery.RemoteDeviceScanner this$0;
    final java.util.List val$devices;
    
    RemoteDeviceScanner$3(com.navdy.service.library.device.discovery.RemoteDeviceScanner a, java.util.List a0) {
        super();
        this.this$0 = a;
        this.val$devices = a0;
    }
    
    public void dispatchEvent(com.navdy.service.library.device.discovery.RemoteDeviceScanner a, com.navdy.service.library.device.discovery.RemoteDeviceScanner$Listener a0) {
        a0.onDiscovered(a, this.val$devices);
    }
    
    public void dispatchEvent(com.navdy.service.library.util.Listenable a, com.navdy.service.library.util.Listenable$Listener a0) {
        this.dispatchEvent((com.navdy.service.library.device.discovery.RemoteDeviceScanner)a, (com.navdy.service.library.device.discovery.RemoteDeviceScanner$Listener)a0);
    }
}
