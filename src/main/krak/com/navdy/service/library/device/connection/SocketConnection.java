package com.navdy.service.library.device.connection;

public class SocketConnection extends com.navdy.service.library.device.connection.Connection {
    private com.navdy.service.library.network.SocketFactory connector;
    protected boolean mBlockReconnect;
    protected com.navdy.service.library.device.connection.SocketConnection$ConnectThread mConnectThread;
    private com.navdy.service.library.network.SocketAdapter socket;
    
    public SocketConnection(com.navdy.service.library.device.connection.ConnectionInfo a, com.navdy.service.library.network.SocketAdapter a0) {
        super(a);
        this.setExistingSocketConnection(a0);
    }
    
    public SocketConnection(com.navdy.service.library.device.connection.ConnectionInfo a, com.navdy.service.library.network.SocketFactory a0) {
        super(a);
        this.connector = a0;
    }
    
    static com.navdy.service.library.network.SocketFactory access$000(com.navdy.service.library.device.connection.SocketConnection a) {
        return a.connector;
    }
    
    static void access$100(com.navdy.service.library.device.connection.SocketConnection a, com.navdy.service.library.network.SocketAdapter a0) {
        a.closeQuietly(a0);
    }
    
    static void access$200(com.navdy.service.library.device.connection.SocketConnection a, com.navdy.service.library.device.connection.Connection$ConnectionFailureCause a0) {
        a.connectionFailed(a0);
    }
    
    private void closeQuietly(com.navdy.service.library.network.SocketAdapter a) {
        label0: {
            Throwable a0 = null;
            if (a == null) {
                break label0;
            }
            try {
                a.close();
                break label0;
            } catch(Throwable a1) {
                a0 = a1;
            }
            this.logger.e("[SOCK] Exception while closing", a0);
        }
    }
    
    private void connectionFailed(com.navdy.service.library.device.connection.Connection$ConnectionFailureCause a) {
        this.dispatchConnectionFailedEvent(a);
    }
    
    public boolean connect() {
        boolean b = false;
        synchronized(this) {
            if (this.mBlockReconnect) {
                b = false;
            } else {
                this.logger.d(new StringBuilder().append("connect to: ").append(this.mConnectionInfo).toString());
                if (this.mStatus == com.navdy.service.library.device.connection.Connection$Status.DISCONNECTED) {
                    this.mStatus = com.navdy.service.library.device.connection.Connection$Status.CONNECTING;
                    this.mConnectThread = new com.navdy.service.library.device.connection.SocketConnection$ConnectThread(this);
                    this.mConnectThread.start();
                    b = true;
                } else {
                    this.logger.d(new StringBuilder().append("can't connect: state: ").append(this.mStatus).toString());
                    b = false;
                }
            }
        }
        /*monexit(this)*/;
        return b;
    }
    
    public void connected(com.navdy.service.library.network.SocketAdapter a) {
        label2: synchronized(this) {
            this.logger.i("connected");
            if (this.mConnectThread != null) {
                this.mConnectThread.cancel();
                this.mConnectThread = null;
            }
            com.navdy.service.library.device.connection.Connection$Status a0 = this.mStatus;
            com.navdy.service.library.device.connection.Connection$Status a1 = com.navdy.service.library.device.connection.Connection$Status.CONNECTING;
            label0: {
                label1: {
                    if (a0 != a1) {
                        break label1;
                    }
                    if (a != null) {
                        break label0;
                    }
                }
                this.logger.e("No longer connecting - disconnecting.");
                this.closeQuietly(a);
                this.mStatus = com.navdy.service.library.device.connection.Connection$Status.DISCONNECTED;
                this.dispatchDisconnectEvent(com.navdy.service.library.device.connection.Connection$DisconnectCause.NORMAL);
                break label2;
            }
            this.socket = a;
            this.mStatus = com.navdy.service.library.device.connection.Connection$Status.CONNECTED;
            this.dispatchConnectEvent();
        }
        /*monexit(this)*/;
    }
    
    public boolean disconnect() {
        label2: synchronized(this) {
            com.navdy.service.library.device.connection.Connection$Status a = this.mStatus;
            com.navdy.service.library.device.connection.Connection$Status a0 = com.navdy.service.library.device.connection.Connection$Status.DISCONNECTED;
            label0: {
                label1: {
                    if (a == a0) {
                        break label1;
                    }
                    if (this.mStatus != com.navdy.service.library.device.connection.Connection$Status.DISCONNECTING) {
                        break label0;
                    }
                }
                this.logger.d(new StringBuilder().append("can't disconnect: state: ").append(this.mStatus).toString());
                break label2;
            }
            this.logger.d(new StringBuilder().append("disconnect - connectThread:").append(this.mConnectThread).append(" socket:").append(this.socket).toString());
            this.mStatus = com.navdy.service.library.device.connection.Connection$Status.DISCONNECTING;
            if (this.mConnectThread == null) {
                if (this.socket == null) {
                    this.mStatus = com.navdy.service.library.device.connection.Connection$Status.DISCONNECTED;
                    this.dispatchDisconnectEvent(com.navdy.service.library.device.connection.Connection$DisconnectCause.NORMAL);
                } else {
                    try {
                        this.socket.close();
                    } catch(java.io.IOException a1) {
                        this.logger.e(new StringBuilder().append("Exception closing socket").append(a1.getMessage()).toString());
                    }
                    this.socket = null;
                }
            } else {
                this.mConnectThread.cancel();
                this.mConnectThread = null;
            }
        }
        /*monexit(this)*/;
        return true;
    }
    
    public com.navdy.service.library.network.SocketAdapter getSocket() {
        return this.socket;
    }
    
    protected void setExistingSocketConnection(com.navdy.service.library.network.SocketAdapter a) {
        synchronized(this) {
            this.mBlockReconnect = true;
            this.mStatus = com.navdy.service.library.device.connection.Connection$Status.CONNECTING;
            this.connected(a);
        }
        /*monexit(this)*/;
    }
}
