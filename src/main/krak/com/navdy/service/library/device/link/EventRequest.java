package com.navdy.service.library.device.link;

public class EventRequest {
    final public com.navdy.service.library.device.RemoteDevice$PostEventHandler eventCompleteHandler;
    final public byte[] eventData;
    final private android.os.Handler handler;
    
    public EventRequest(android.os.Handler a, byte[] a0, com.navdy.service.library.device.RemoteDevice$PostEventHandler a1) {
        this.eventData = (byte[])a0.clone();
        this.eventCompleteHandler = a1;
        this.handler = a;
    }
    
    public void callCompletionHandlers(com.navdy.service.library.device.RemoteDevice$PostEventStatus a) {
        this.handler.post((Runnable)new com.navdy.service.library.device.link.EventRequest$1(this, a));
    }
}
