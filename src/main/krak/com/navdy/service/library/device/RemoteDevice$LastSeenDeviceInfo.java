package com.navdy.service.library.device;

public class RemoteDevice$LastSeenDeviceInfo {
    private com.navdy.service.library.events.DeviceInfo mDeviceInfo;
    
    public RemoteDevice$LastSeenDeviceInfo() {
    }
    
    static com.navdy.service.library.events.DeviceInfo access$002(com.navdy.service.library.device.RemoteDevice$LastSeenDeviceInfo a, com.navdy.service.library.events.DeviceInfo a0) {
        a.mDeviceInfo = a0;
        return a0;
    }
    
    public com.navdy.service.library.events.DeviceInfo getDeviceInfo() {
        return this.mDeviceInfo;
    }
}
