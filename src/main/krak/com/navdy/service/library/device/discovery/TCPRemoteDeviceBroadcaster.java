package com.navdy.service.library.device.discovery;

public class TCPRemoteDeviceBroadcaster extends com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster {
    public TCPRemoteDeviceBroadcaster(android.content.Context a) {
        super(a, com.navdy.service.library.device.connection.TCPConnectionInfo.nsdServiceWithDeviceId(com.navdy.service.library.device.NavdyDeviceId.getThisDevice(a)));
    }
}
