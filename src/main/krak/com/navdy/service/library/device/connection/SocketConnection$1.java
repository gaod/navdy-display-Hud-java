package com.navdy.service.library.device.connection;

class SocketConnection$1 {
    final static int[] $SwitchMap$com$navdy$service$library$device$connection$Connection$Status;
    
    static {
        $SwitchMap$com$navdy$service$library$device$connection$Connection$Status = new int[com.navdy.service.library.device.connection.Connection$Status.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$device$connection$Connection$Status;
        com.navdy.service.library.device.connection.Connection$Status a0 = com.navdy.service.library.device.connection.Connection$Status.CONNECTING;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$connection$Connection$Status[com.navdy.service.library.device.connection.Connection$Status.DISCONNECTING.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
    }
}
