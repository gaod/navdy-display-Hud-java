package com.navdy.service.library.network.http.services;

class JiraClient$4 implements okhttp3.Callback {
    final com.navdy.service.library.network.http.services.JiraClient this$0;
    final com.navdy.service.library.network.http.services.JiraClient$ResultCallback val$callback;
    final String val$ticketId;
    
    JiraClient$4(com.navdy.service.library.network.http.services.JiraClient a, com.navdy.service.library.network.http.services.JiraClient$ResultCallback a0, String s) {
        super();
        this.this$0 = a;
        this.val$callback = a0;
        this.val$ticketId = s;
    }
    
    public void onFailure(okhttp3.Call a, java.io.IOException a0) {
        if (this.val$callback != null) {
            this.val$callback.onError((Throwable)a0);
        }
    }
    
    public void onResponse(okhttp3.Call a, okhttp3.Response a0) {
        label0: try {
            int i = a0.code();
            label1: {
                if (i == 200) {
                    break label1;
                }
                if (i != 201) {
                    break label0;
                }
            }
            if (this.val$callback != null) {
                this.val$callback.onSuccess(this.val$ticketId);
            }
        } catch(Throwable a1) {
            if (this.val$callback != null) {
                this.val$callback.onError(a1);
            }
        }
    }
}
