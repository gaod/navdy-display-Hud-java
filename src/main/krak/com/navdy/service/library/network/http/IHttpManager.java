package com.navdy.service.library.network.http;

abstract public interface IHttpManager {
    abstract public okhttp3.OkHttpClient getClient();
    
    
    abstract public okhttp3.OkHttpClient$Builder getClientCopy();
}
