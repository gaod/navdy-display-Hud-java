package com.navdy.service.library.network;

abstract public interface SocketAcceptor extends java.io.Closeable {
    abstract public com.navdy.service.library.network.SocketAdapter accept();
    
    
    abstract public void close();
    
    
    abstract public com.navdy.service.library.device.connection.ConnectionInfo getRemoteConnectionInfo(com.navdy.service.library.network.SocketAdapter arg, com.navdy.service.library.device.connection.ConnectionType arg0);
}
