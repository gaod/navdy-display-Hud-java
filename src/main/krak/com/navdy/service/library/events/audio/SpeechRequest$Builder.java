package com.navdy.service.library.events.audio;

final public class SpeechRequest$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.audio.SpeechRequest$Category category;
    public String id;
    public String language;
    public Boolean sendStatus;
    public String words;
    
    public SpeechRequest$Builder() {
    }
    
    public SpeechRequest$Builder(com.navdy.service.library.events.audio.SpeechRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.words = a.words;
            this.category = a.category;
            this.id = a.id;
            this.sendStatus = a.sendStatus;
            this.language = a.language;
        }
    }
    
    public com.navdy.service.library.events.audio.SpeechRequest build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.audio.SpeechRequest(this, (com.navdy.service.library.events.audio.SpeechRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.SpeechRequest$Builder category(com.navdy.service.library.events.audio.SpeechRequest$Category a) {
        this.category = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.SpeechRequest$Builder id(String s) {
        this.id = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.SpeechRequest$Builder language(String s) {
        this.language = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.SpeechRequest$Builder sendStatus(Boolean a) {
        this.sendStatus = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.SpeechRequest$Builder words(String s) {
        this.words = s;
        return this;
    }
}
