package com.navdy.service.library.events.debug;

final public class StartDriveRecordingEvent$Builder extends com.squareup.wire.Message.Builder {
    public String label;
    
    public StartDriveRecordingEvent$Builder() {
    }
    
    public StartDriveRecordingEvent$Builder(com.navdy.service.library.events.debug.StartDriveRecordingEvent a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.label = a.label;
        }
    }
    
    public com.navdy.service.library.events.debug.StartDriveRecordingEvent build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.debug.StartDriveRecordingEvent(this, (com.navdy.service.library.events.debug.StartDriveRecordingEvent$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.debug.StartDriveRecordingEvent$Builder label(String s) {
        this.label = s;
        return this;
    }
}
