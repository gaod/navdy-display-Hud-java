package com.navdy.service.library.events.preferences;

final public class NotificationPreferencesRequest$Builder extends com.squareup.wire.Message.Builder {
    public Long serial_number;
    
    public NotificationPreferencesRequest$Builder() {
    }
    
    public NotificationPreferencesRequest$Builder(com.navdy.service.library.events.preferences.NotificationPreferencesRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.serial_number = a.serial_number;
        }
    }
    
    public com.navdy.service.library.events.preferences.NotificationPreferencesRequest build() {
        return new com.navdy.service.library.events.preferences.NotificationPreferencesRequest(this, (com.navdy.service.library.events.preferences.NotificationPreferencesRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.preferences.NotificationPreferencesRequest$Builder serial_number(Long a) {
        this.serial_number = a;
        return this;
    }
}
