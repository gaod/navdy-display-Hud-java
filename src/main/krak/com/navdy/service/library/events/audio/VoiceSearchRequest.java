package com.navdy.service.library.events.audio;

final public class VoiceSearchRequest extends com.squareup.wire.Message {
    final public static Boolean DEFAULT_END;
    final private static long serialVersionUID = 0L;
    final public Boolean end;
    
    static {
        DEFAULT_END = Boolean.valueOf(false);
    }
    
    private VoiceSearchRequest(com.navdy.service.library.events.audio.VoiceSearchRequest$Builder a) {
        this(a.end);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    VoiceSearchRequest(com.navdy.service.library.events.audio.VoiceSearchRequest$Builder a, com.navdy.service.library.events.audio.VoiceSearchRequest$1 a0) {
        this(a);
    }
    
    public VoiceSearchRequest(Boolean a) {
        this.end = a;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.audio.VoiceSearchRequest && this.equals(this.end, ((com.navdy.service.library.events.audio.VoiceSearchRequest)a).end);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.end == null) ? 0 : this.end.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
