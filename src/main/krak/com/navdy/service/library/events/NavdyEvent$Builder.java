package com.navdy.service.library.events;

final public class NavdyEvent$Builder extends com.squareup.wire.ExtendableMessage$ExtendableBuilder {
    public com.navdy.service.library.events.NavdyEvent$MessageType type;
    
    public NavdyEvent$Builder() {
    }
    
    public NavdyEvent$Builder(com.navdy.service.library.events.NavdyEvent a) {
        super((com.squareup.wire.ExtendableMessage)a);
        if (a != null) {
            this.type = a.type;
        }
    }
    
    public com.navdy.service.library.events.NavdyEvent build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.NavdyEvent(this, (com.navdy.service.library.events.NavdyEvent$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.NavdyEvent$Builder setExtension(com.squareup.wire.Extension a, Object a0) {
        ((com.squareup.wire.ExtendableMessage$ExtendableBuilder)this).setExtension(a, a0);
        return this;
    }
    
    public com.squareup.wire.ExtendableMessage$ExtendableBuilder setExtension(com.squareup.wire.Extension a, Object a0) {
        return this.setExtension(a, a0);
    }
    
    public com.navdy.service.library.events.NavdyEvent$Builder type(com.navdy.service.library.events.NavdyEvent$MessageType a) {
        this.type = a;
        return this;
    }
}
