package com.navdy.service.library.events.destination;

final public class Destination$Builder extends com.squareup.wire.Message.Builder {
    public java.util.List contacts;
    public String destinationDistance;
    public Integer destinationIcon;
    public Integer destinationIconBkColor;
    public String destination_subtitle;
    public String destination_title;
    public com.navdy.service.library.events.location.LatLong display_position;
    public com.navdy.service.library.events.destination.Destination$FavoriteType favorite_type;
    public String full_address;
    public String identifier;
    public Boolean is_recommendation;
    public Long last_navigated_to;
    public com.navdy.service.library.events.location.LatLong navigation_position;
    public java.util.List phoneNumbers;
    public String place_id;
    public com.navdy.service.library.events.places.PlaceType place_type;
    public com.navdy.service.library.events.destination.Destination$SuggestionType suggestion_type;
    
    public Destination$Builder() {
    }
    
    public Destination$Builder(com.navdy.service.library.events.destination.Destination a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.navigation_position = a.navigation_position;
            this.display_position = a.display_position;
            this.full_address = a.full_address;
            this.destination_title = a.destination_title;
            this.destination_subtitle = a.destination_subtitle;
            this.favorite_type = a.favorite_type;
            this.identifier = a.identifier;
            this.suggestion_type = a.suggestion_type;
            this.is_recommendation = a.is_recommendation;
            this.last_navigated_to = a.last_navigated_to;
            this.place_type = a.place_type;
            this.destinationIcon = a.destinationIcon;
            this.destinationIconBkColor = a.destinationIconBkColor;
            this.place_id = a.place_id;
            this.destinationDistance = a.destinationDistance;
            this.phoneNumbers = com.navdy.service.library.events.destination.Destination.access$000(a.phoneNumbers);
            this.contacts = com.navdy.service.library.events.destination.Destination.access$100(a.contacts);
        }
    }
    
    public com.navdy.service.library.events.destination.Destination build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.destination.Destination(this, (com.navdy.service.library.events.destination.Destination$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.destination.Destination$Builder contacts(java.util.List a) {
        this.contacts = com.navdy.service.library.events.destination.Destination$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.destination.Destination$Builder destinationDistance(String s) {
        this.destinationDistance = s;
        return this;
    }
    
    public com.navdy.service.library.events.destination.Destination$Builder destinationIcon(Integer a) {
        this.destinationIcon = a;
        return this;
    }
    
    public com.navdy.service.library.events.destination.Destination$Builder destinationIconBkColor(Integer a) {
        this.destinationIconBkColor = a;
        return this;
    }
    
    public com.navdy.service.library.events.destination.Destination$Builder destination_subtitle(String s) {
        this.destination_subtitle = s;
        return this;
    }
    
    public com.navdy.service.library.events.destination.Destination$Builder destination_title(String s) {
        this.destination_title = s;
        return this;
    }
    
    public com.navdy.service.library.events.destination.Destination$Builder display_position(com.navdy.service.library.events.location.LatLong a) {
        this.display_position = a;
        return this;
    }
    
    public com.navdy.service.library.events.destination.Destination$Builder favorite_type(com.navdy.service.library.events.destination.Destination$FavoriteType a) {
        this.favorite_type = a;
        return this;
    }
    
    public com.navdy.service.library.events.destination.Destination$Builder full_address(String s) {
        this.full_address = s;
        return this;
    }
    
    public com.navdy.service.library.events.destination.Destination$Builder identifier(String s) {
        this.identifier = s;
        return this;
    }
    
    public com.navdy.service.library.events.destination.Destination$Builder is_recommendation(Boolean a) {
        this.is_recommendation = a;
        return this;
    }
    
    public com.navdy.service.library.events.destination.Destination$Builder last_navigated_to(Long a) {
        this.last_navigated_to = a;
        return this;
    }
    
    public com.navdy.service.library.events.destination.Destination$Builder navigation_position(com.navdy.service.library.events.location.LatLong a) {
        this.navigation_position = a;
        return this;
    }
    
    public com.navdy.service.library.events.destination.Destination$Builder phoneNumbers(java.util.List a) {
        this.phoneNumbers = com.navdy.service.library.events.destination.Destination$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.destination.Destination$Builder place_id(String s) {
        this.place_id = s;
        return this;
    }
    
    public com.navdy.service.library.events.destination.Destination$Builder place_type(com.navdy.service.library.events.places.PlaceType a) {
        this.place_type = a;
        return this;
    }
    
    public com.navdy.service.library.events.destination.Destination$Builder suggestion_type(com.navdy.service.library.events.destination.Destination$SuggestionType a) {
        this.suggestion_type = a;
        return this;
    }
}
