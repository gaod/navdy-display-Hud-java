package com.navdy.service.library.events.debug;

final public class StopDriveRecordingResponse$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.RequestStatus status;
    
    public StopDriveRecordingResponse$Builder() {
    }
    
    public StopDriveRecordingResponse$Builder(com.navdy.service.library.events.debug.StopDriveRecordingResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
        }
    }
    
    public com.navdy.service.library.events.debug.StopDriveRecordingResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.debug.StopDriveRecordingResponse(this, (com.navdy.service.library.events.debug.StopDriveRecordingResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.debug.StopDriveRecordingResponse$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
}
