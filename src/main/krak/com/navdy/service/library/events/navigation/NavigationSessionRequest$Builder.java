package com.navdy.service.library.events.navigation;

final public class NavigationSessionRequest$Builder extends com.squareup.wire.Message.Builder {
    public String label;
    public com.navdy.service.library.events.navigation.NavigationSessionState newState;
    public Boolean originDisplay;
    public String routeId;
    public Integer simulationSpeed;
    
    public NavigationSessionRequest$Builder() {
    }
    
    public NavigationSessionRequest$Builder(com.navdy.service.library.events.navigation.NavigationSessionRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.newState = a.newState;
            this.label = a.label;
            this.routeId = a.routeId;
            this.simulationSpeed = a.simulationSpeed;
            this.originDisplay = a.originDisplay;
        }
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionRequest build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.navigation.NavigationSessionRequest(this, (com.navdy.service.library.events.navigation.NavigationSessionRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionRequest$Builder label(String s) {
        this.label = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionRequest$Builder newState(com.navdy.service.library.events.navigation.NavigationSessionState a) {
        this.newState = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionRequest$Builder originDisplay(Boolean a) {
        this.originDisplay = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionRequest$Builder routeId(String s) {
        this.routeId = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionRequest$Builder simulationSpeed(Integer a) {
        this.simulationSpeed = a;
        return this;
    }
}
