package com.navdy.service.library.events.photo;

final public class PhotoUpdateQuery extends com.squareup.wire.Message {
    final public static String DEFAULT_ALBUM = "";
    final public static String DEFAULT_AUTHOR = "";
    final public static String DEFAULT_CHECKSUM = "";
    final public static String DEFAULT_IDENTIFIER = "";
    final public static com.navdy.service.library.events.photo.PhotoType DEFAULT_PHOTOTYPE;
    final public static Long DEFAULT_SIZE;
    final public static String DEFAULT_TRACK = "";
    final private static long serialVersionUID = 0L;
    final public String album;
    final public String author;
    final public String checksum;
    final public String identifier;
    final public com.navdy.service.library.events.photo.PhotoType photoType;
    final public Long size;
    final public String track;
    
    static {
        DEFAULT_PHOTOTYPE = com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT;
        DEFAULT_SIZE = Long.valueOf(0L);
    }
    
    private PhotoUpdateQuery(com.navdy.service.library.events.photo.PhotoUpdateQuery$Builder a) {
        this(a.identifier, a.photoType, a.size, a.checksum, a.album, a.track, a.author);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    PhotoUpdateQuery(com.navdy.service.library.events.photo.PhotoUpdateQuery$Builder a, com.navdy.service.library.events.photo.PhotoUpdateQuery$1 a0) {
        this(a);
    }
    
    public PhotoUpdateQuery(String s, com.navdy.service.library.events.photo.PhotoType a, Long a0, String s0, String s1, String s2, String s3) {
        this.identifier = s;
        this.photoType = a;
        this.size = a0;
        this.checksum = s0;
        this.album = s1;
        this.track = s2;
        this.author = s3;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.photo.PhotoUpdateQuery) {
                com.navdy.service.library.events.photo.PhotoUpdateQuery a0 = (com.navdy.service.library.events.photo.PhotoUpdateQuery)a;
                boolean b0 = this.equals(this.identifier, a0.identifier);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.photoType, a0.photoType)) {
                        break label1;
                    }
                    if (!this.equals(this.size, a0.size)) {
                        break label1;
                    }
                    if (!this.equals(this.checksum, a0.checksum)) {
                        break label1;
                    }
                    if (!this.equals(this.album, a0.album)) {
                        break label1;
                    }
                    if (!this.equals(this.track, a0.track)) {
                        break label1;
                    }
                    if (this.equals(this.author, a0.author)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((((this.identifier == null) ? 0 : this.identifier.hashCode()) * 37 + ((this.photoType == null) ? 0 : this.photoType.hashCode())) * 37 + ((this.size == null) ? 0 : this.size.hashCode())) * 37 + ((this.checksum == null) ? 0 : this.checksum.hashCode())) * 37 + ((this.album == null) ? 0 : this.album.hashCode())) * 37 + ((this.track == null) ? 0 : this.track.hashCode())) * 37 + ((this.author == null) ? 0 : this.author.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
