package com.navdy.service.library.events.callcontrol;

final public class TelephonyRequest$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.callcontrol.CallAction action;
    public String callUUID;
    public String number;
    
    public TelephonyRequest$Builder() {
    }
    
    public TelephonyRequest$Builder(com.navdy.service.library.events.callcontrol.TelephonyRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.action = a.action;
            this.number = a.number;
            this.callUUID = a.callUUID;
        }
    }
    
    public com.navdy.service.library.events.callcontrol.TelephonyRequest$Builder action(com.navdy.service.library.events.callcontrol.CallAction a) {
        this.action = a;
        return this;
    }
    
    public com.navdy.service.library.events.callcontrol.TelephonyRequest build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.callcontrol.TelephonyRequest(this, (com.navdy.service.library.events.callcontrol.TelephonyRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.callcontrol.TelephonyRequest$Builder callUUID(String s) {
        this.callUUID = s;
        return this;
    }
    
    public com.navdy.service.library.events.callcontrol.TelephonyRequest$Builder number(String s) {
        this.number = s;
        return this;
    }
}
