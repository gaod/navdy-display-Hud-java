package com.navdy.service.library.events.audio;

final public class MusicCollectionResponse$Builder extends com.squareup.wire.Message.Builder {
    public java.util.List characterMap;
    public com.navdy.service.library.events.audio.MusicCollectionInfo collectionInfo;
    public java.util.List musicCollections;
    public java.util.List musicTracks;
    
    public MusicCollectionResponse$Builder() {
    }
    
    public MusicCollectionResponse$Builder(com.navdy.service.library.events.audio.MusicCollectionResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.collectionInfo = a.collectionInfo;
            this.musicCollections = com.navdy.service.library.events.audio.MusicCollectionResponse.access$000(a.musicCollections);
            this.musicTracks = com.navdy.service.library.events.audio.MusicCollectionResponse.access$100(a.musicTracks);
            this.characterMap = com.navdy.service.library.events.audio.MusicCollectionResponse.access$200(a.characterMap);
        }
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionResponse build() {
        return new com.navdy.service.library.events.audio.MusicCollectionResponse(this, (com.navdy.service.library.events.audio.MusicCollectionResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionResponse$Builder characterMap(java.util.List a) {
        this.characterMap = com.navdy.service.library.events.audio.MusicCollectionResponse$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionResponse$Builder collectionInfo(com.navdy.service.library.events.audio.MusicCollectionInfo a) {
        this.collectionInfo = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionResponse$Builder musicCollections(java.util.List a) {
        this.musicCollections = com.navdy.service.library.events.audio.MusicCollectionResponse$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionResponse$Builder musicTracks(java.util.List a) {
        this.musicTracks = com.navdy.service.library.events.audio.MusicCollectionResponse$Builder.checkForNulls(a);
        return this;
    }
}
