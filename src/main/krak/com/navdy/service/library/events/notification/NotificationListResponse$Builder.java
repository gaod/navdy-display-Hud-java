package com.navdy.service.library.events.notification;

final public class NotificationListResponse$Builder extends com.squareup.wire.Message.Builder {
    public java.util.List ids;
    public com.navdy.service.library.events.RequestStatus status;
    public String status_detail;
    
    public NotificationListResponse$Builder() {
    }
    
    public NotificationListResponse$Builder(com.navdy.service.library.events.notification.NotificationListResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
            this.status_detail = a.status_detail;
            this.ids = com.navdy.service.library.events.notification.NotificationListResponse.access$000(a.ids);
        }
    }
    
    public com.navdy.service.library.events.notification.NotificationListResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.notification.NotificationListResponse(this, (com.navdy.service.library.events.notification.NotificationListResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.notification.NotificationListResponse$Builder ids(java.util.List a) {
        this.ids = com.navdy.service.library.events.notification.NotificationListResponse$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationListResponse$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationListResponse$Builder status_detail(String s) {
        this.status_detail = s;
        return this;
    }
}
