package com.navdy.service.library.events.ui;

final public class Screen extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.ui.Screen[] $VALUES;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_AUTO_BRIGHTNESS;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_BACK;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_BLUETOOTH_PAIRING;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_BRIGHTNESS;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_CALL_CONTROL;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_CAROUSEL_MENU;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_CONTEXT_MENU;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_DASHBOARD;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_DESTINATION_PICKER;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_DIAL_PAIRING;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_DIAL_UPDATE_CONFIRMATION;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_DIAL_UPDATE_PROGRESS;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_FACTORY_RESET;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_FAVORITE_CONTACTS;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_FAVORITE_PLACES;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_FIRST_LAUNCH;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_FORCE_UPDATE;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_GESTURE_LEARNING;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_HOME;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_HYBRID_MAP;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_MAIN_MENU;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_MAP_ANIMATION_MODE;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_MEDIA_BROWSER;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_MENU;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_MUSIC;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_MUSIC_DETAILS;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_NOTIFICATION;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_OPTIONS;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_OTA_CONFIRMATION;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_PLACES;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_READ_MESSAGE;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_RECENT_CALLS;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_RECOMMENDED_PLACES;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_REPLY_MESSAGE;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_REPORT_ISSUE;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_SETTINGS;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_SHUTDOWN_CONFIRMATION;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_SYSTEM_INFO;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_TEMPERATURE_WARNING;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_TOAST;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_TURN_BY_TURN;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_VOICE_CONTROL;
    final public static com.navdy.service.library.events.ui.Screen SCREEN_WELCOME;
    final private int value;
    
    static {
        SCREEN_DASHBOARD = new com.navdy.service.library.events.ui.Screen("SCREEN_DASHBOARD", 0, 1);
        SCREEN_TURN_BY_TURN = new com.navdy.service.library.events.ui.Screen("SCREEN_TURN_BY_TURN", 1, 2);
        SCREEN_HYBRID_MAP = new com.navdy.service.library.events.ui.Screen("SCREEN_HYBRID_MAP", 2, 3);
        SCREEN_MEDIA_BROWSER = new com.navdy.service.library.events.ui.Screen("SCREEN_MEDIA_BROWSER", 3, 4);
        SCREEN_MENU = new com.navdy.service.library.events.ui.Screen("SCREEN_MENU", 4, 5);
        SCREEN_NOTIFICATION = new com.navdy.service.library.events.ui.Screen("SCREEN_NOTIFICATION", 5, 6);
        SCREEN_CALL_CONTROL = new com.navdy.service.library.events.ui.Screen("SCREEN_CALL_CONTROL", 6, 7);
        SCREEN_MUSIC = new com.navdy.service.library.events.ui.Screen("SCREEN_MUSIC", 7, 8);
        SCREEN_VOICE_CONTROL = new com.navdy.service.library.events.ui.Screen("SCREEN_VOICE_CONTROL", 8, 9);
        SCREEN_RECENT_CALLS = new com.navdy.service.library.events.ui.Screen("SCREEN_RECENT_CALLS", 9, 10);
        SCREEN_BACK = new com.navdy.service.library.events.ui.Screen("SCREEN_BACK", 10, 11);
        SCREEN_CONTEXT_MENU = new com.navdy.service.library.events.ui.Screen("SCREEN_CONTEXT_MENU", 11, 12);
        SCREEN_CAROUSEL_MENU = new com.navdy.service.library.events.ui.Screen("SCREEN_CAROUSEL_MENU", 12, 13);
        SCREEN_OPTIONS = new com.navdy.service.library.events.ui.Screen("SCREEN_OPTIONS", 13, 14);
        SCREEN_PLACES = new com.navdy.service.library.events.ui.Screen("SCREEN_PLACES", 14, 15);
        SCREEN_OTA_CONFIRMATION = new com.navdy.service.library.events.ui.Screen("SCREEN_OTA_CONFIRMATION", 15, 16);
        SCREEN_WELCOME = new com.navdy.service.library.events.ui.Screen("SCREEN_WELCOME", 16, 17);
        SCREEN_FAVORITE_CONTACTS = new com.navdy.service.library.events.ui.Screen("SCREEN_FAVORITE_CONTACTS", 17, 18);
        SCREEN_FAVORITE_PLACES = new com.navdy.service.library.events.ui.Screen("SCREEN_FAVORITE_PLACES", 18, 19);
        SCREEN_BRIGHTNESS = new com.navdy.service.library.events.ui.Screen("SCREEN_BRIGHTNESS", 19, 20);
        SCREEN_RECOMMENDED_PLACES = new com.navdy.service.library.events.ui.Screen("SCREEN_RECOMMENDED_PLACES", 20, 21);
        SCREEN_REPLY_MESSAGE = new com.navdy.service.library.events.ui.Screen("SCREEN_REPLY_MESSAGE", 21, 22);
        SCREEN_READ_MESSAGE = new com.navdy.service.library.events.ui.Screen("SCREEN_READ_MESSAGE", 22, 23);
        SCREEN_MAP_ANIMATION_MODE = new com.navdy.service.library.events.ui.Screen("SCREEN_MAP_ANIMATION_MODE", 23, 24);
        SCREEN_SHUTDOWN_CONFIRMATION = new com.navdy.service.library.events.ui.Screen("SCREEN_SHUTDOWN_CONFIRMATION", 24, 25);
        SCREEN_DIAL_PAIRING = new com.navdy.service.library.events.ui.Screen("SCREEN_DIAL_PAIRING", 25, 26);
        SCREEN_SETTINGS = new com.navdy.service.library.events.ui.Screen("SCREEN_SETTINGS", 26, 27);
        SCREEN_AUTO_BRIGHTNESS = new com.navdy.service.library.events.ui.Screen("SCREEN_AUTO_BRIGHTNESS", 27, 28);
        SCREEN_FACTORY_RESET = new com.navdy.service.library.events.ui.Screen("SCREEN_FACTORY_RESET", 28, 29);
        SCREEN_BLUETOOTH_PAIRING = new com.navdy.service.library.events.ui.Screen("SCREEN_BLUETOOTH_PAIRING", 29, 30);
        SCREEN_REPORT_ISSUE = new com.navdy.service.library.events.ui.Screen("SCREEN_REPORT_ISSUE", 30, 31);
        SCREEN_HOME = new com.navdy.service.library.events.ui.Screen("SCREEN_HOME", 31, 32);
        SCREEN_TOAST = new com.navdy.service.library.events.ui.Screen("SCREEN_TOAST", 32, 33);
        SCREEN_DIAL_UPDATE_PROGRESS = new com.navdy.service.library.events.ui.Screen("SCREEN_DIAL_UPDATE_PROGRESS", 33, 34);
        SCREEN_FIRST_LAUNCH = new com.navdy.service.library.events.ui.Screen("SCREEN_FIRST_LAUNCH", 34, 35);
        SCREEN_FORCE_UPDATE = new com.navdy.service.library.events.ui.Screen("SCREEN_FORCE_UPDATE", 35, 36);
        SCREEN_TEMPERATURE_WARNING = new com.navdy.service.library.events.ui.Screen("SCREEN_TEMPERATURE_WARNING", 36, 37);
        SCREEN_DIAL_UPDATE_CONFIRMATION = new com.navdy.service.library.events.ui.Screen("SCREEN_DIAL_UPDATE_CONFIRMATION", 37, 38);
        SCREEN_GESTURE_LEARNING = new com.navdy.service.library.events.ui.Screen("SCREEN_GESTURE_LEARNING", 38, 39);
        SCREEN_SYSTEM_INFO = new com.navdy.service.library.events.ui.Screen("SCREEN_SYSTEM_INFO", 39, 40);
        SCREEN_MAIN_MENU = new com.navdy.service.library.events.ui.Screen("SCREEN_MAIN_MENU", 40, 41);
        SCREEN_DESTINATION_PICKER = new com.navdy.service.library.events.ui.Screen("SCREEN_DESTINATION_PICKER", 41, 42);
        SCREEN_MUSIC_DETAILS = new com.navdy.service.library.events.ui.Screen("SCREEN_MUSIC_DETAILS", 42, 43);
        com.navdy.service.library.events.ui.Screen[] a = new com.navdy.service.library.events.ui.Screen[43];
        a[0] = SCREEN_DASHBOARD;
        a[1] = SCREEN_TURN_BY_TURN;
        a[2] = SCREEN_HYBRID_MAP;
        a[3] = SCREEN_MEDIA_BROWSER;
        a[4] = SCREEN_MENU;
        a[5] = SCREEN_NOTIFICATION;
        a[6] = SCREEN_CALL_CONTROL;
        a[7] = SCREEN_MUSIC;
        a[8] = SCREEN_VOICE_CONTROL;
        a[9] = SCREEN_RECENT_CALLS;
        a[10] = SCREEN_BACK;
        a[11] = SCREEN_CONTEXT_MENU;
        a[12] = SCREEN_CAROUSEL_MENU;
        a[13] = SCREEN_OPTIONS;
        a[14] = SCREEN_PLACES;
        a[15] = SCREEN_OTA_CONFIRMATION;
        a[16] = SCREEN_WELCOME;
        a[17] = SCREEN_FAVORITE_CONTACTS;
        a[18] = SCREEN_FAVORITE_PLACES;
        a[19] = SCREEN_BRIGHTNESS;
        a[20] = SCREEN_RECOMMENDED_PLACES;
        a[21] = SCREEN_REPLY_MESSAGE;
        a[22] = SCREEN_READ_MESSAGE;
        a[23] = SCREEN_MAP_ANIMATION_MODE;
        a[24] = SCREEN_SHUTDOWN_CONFIRMATION;
        a[25] = SCREEN_DIAL_PAIRING;
        a[26] = SCREEN_SETTINGS;
        a[27] = SCREEN_AUTO_BRIGHTNESS;
        a[28] = SCREEN_FACTORY_RESET;
        a[29] = SCREEN_BLUETOOTH_PAIRING;
        a[30] = SCREEN_REPORT_ISSUE;
        a[31] = SCREEN_HOME;
        a[32] = SCREEN_TOAST;
        a[33] = SCREEN_DIAL_UPDATE_PROGRESS;
        a[34] = SCREEN_FIRST_LAUNCH;
        a[35] = SCREEN_FORCE_UPDATE;
        a[36] = SCREEN_TEMPERATURE_WARNING;
        a[37] = SCREEN_DIAL_UPDATE_CONFIRMATION;
        a[38] = SCREEN_GESTURE_LEARNING;
        a[39] = SCREEN_SYSTEM_INFO;
        a[40] = SCREEN_MAIN_MENU;
        a[41] = SCREEN_DESTINATION_PICKER;
        a[42] = SCREEN_MUSIC_DETAILS;
        $VALUES = a;
    }
    
    private Screen(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.ui.Screen valueOf(String s) {
        return (com.navdy.service.library.events.ui.Screen)Enum.valueOf(com.navdy.service.library.events.ui.Screen.class, s);
    }
    
    public static com.navdy.service.library.events.ui.Screen[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
