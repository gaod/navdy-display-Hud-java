package com.navdy.service.library.events.debug;

final public class StartDrivePlaybackEvent extends com.squareup.wire.Message {
    final public static String DEFAULT_LABEL = "";
    final public static Boolean DEFAULT_PLAYSECONDARYLOCATION;
    final private static long serialVersionUID = 0L;
    final public String label;
    final public Boolean playSecondaryLocation;
    
    static {
        DEFAULT_PLAYSECONDARYLOCATION = Boolean.valueOf(false);
    }
    
    private StartDrivePlaybackEvent(com.navdy.service.library.events.debug.StartDrivePlaybackEvent$Builder a) {
        this(a.label, a.playSecondaryLocation);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    StartDrivePlaybackEvent(com.navdy.service.library.events.debug.StartDrivePlaybackEvent$Builder a, com.navdy.service.library.events.debug.StartDrivePlaybackEvent$1 a0) {
        this(a);
    }
    
    public StartDrivePlaybackEvent(String s, Boolean a) {
        this.label = s;
        this.playSecondaryLocation = a;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.debug.StartDrivePlaybackEvent) {
                com.navdy.service.library.events.debug.StartDrivePlaybackEvent a0 = (com.navdy.service.library.events.debug.StartDrivePlaybackEvent)a;
                boolean b0 = this.equals(this.label, a0.label);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.playSecondaryLocation, a0.playSecondaryLocation)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.label == null) ? 0 : this.label.hashCode()) * 37 + ((this.playSecondaryLocation == null) ? 0 : this.playSecondaryLocation.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
