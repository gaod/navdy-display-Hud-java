package com.navdy.service.library.events.photo;

final public class PhotoUpdateQueryResponse$Builder extends com.squareup.wire.Message.Builder {
    public String identifier;
    public Boolean updateRequired;
    
    public PhotoUpdateQueryResponse$Builder() {
    }
    
    public PhotoUpdateQueryResponse$Builder(com.navdy.service.library.events.photo.PhotoUpdateQueryResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.identifier = a.identifier;
            this.updateRequired = a.updateRequired;
        }
    }
    
    public com.navdy.service.library.events.photo.PhotoUpdateQueryResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.photo.PhotoUpdateQueryResponse(this, (com.navdy.service.library.events.photo.PhotoUpdateQueryResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.photo.PhotoUpdateQueryResponse$Builder identifier(String s) {
        this.identifier = s;
        return this;
    }
    
    public com.navdy.service.library.events.photo.PhotoUpdateQueryResponse$Builder updateRequired(Boolean a) {
        this.updateRequired = a;
        return this;
    }
}
