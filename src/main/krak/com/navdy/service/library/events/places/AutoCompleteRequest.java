package com.navdy.service.library.events.places;

final public class AutoCompleteRequest extends com.squareup.wire.Message {
    final public static Integer DEFAULT_MAXRESULTS;
    final public static String DEFAULT_PARTIALSEARCH = "";
    final public static Integer DEFAULT_SEARCHAREA;
    final private static long serialVersionUID = 0L;
    final public Integer maxResults;
    final public String partialSearch;
    final public Integer searchArea;
    
    static {
        DEFAULT_SEARCHAREA = Integer.valueOf(0);
        DEFAULT_MAXRESULTS = Integer.valueOf(0);
    }
    
    private AutoCompleteRequest(com.navdy.service.library.events.places.AutoCompleteRequest$Builder a) {
        this(a.partialSearch, a.searchArea, a.maxResults);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    AutoCompleteRequest(com.navdy.service.library.events.places.AutoCompleteRequest$Builder a, com.navdy.service.library.events.places.AutoCompleteRequest$1 a0) {
        this(a);
    }
    
    public AutoCompleteRequest(String s, Integer a, Integer a0) {
        this.partialSearch = s;
        this.searchArea = a;
        this.maxResults = a0;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.places.AutoCompleteRequest) {
                com.navdy.service.library.events.places.AutoCompleteRequest a0 = (com.navdy.service.library.events.places.AutoCompleteRequest)a;
                boolean b0 = this.equals(this.partialSearch, a0.partialSearch);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.searchArea, a0.searchArea)) {
                        break label1;
                    }
                    if (this.equals(this.maxResults, a0.maxResults)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((this.partialSearch == null) ? 0 : this.partialSearch.hashCode()) * 37 + ((this.searchArea == null) ? 0 : this.searchArea.hashCode())) * 37 + ((this.maxResults == null) ? 0 : this.maxResults.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
