package com.navdy.service.library.events.photo;

final public class PhotoResponse extends com.squareup.wire.Message {
    final public static String DEFAULT_IDENTIFIER = "";
    final public static okio.ByteString DEFAULT_PHOTO;
    final public static String DEFAULT_PHOTOCHECKSUM = "";
    final public static com.navdy.service.library.events.photo.PhotoType DEFAULT_PHOTOTYPE;
    final public static com.navdy.service.library.events.RequestStatus DEFAULT_STATUS;
    final public static String DEFAULT_STATUSDETAIL = "";
    final private static long serialVersionUID = 0L;
    final public String identifier;
    final public okio.ByteString photo;
    final public String photoChecksum;
    final public com.navdy.service.library.events.photo.PhotoType photoType;
    final public com.navdy.service.library.events.RequestStatus status;
    final public String statusDetail;
    
    static {
        DEFAULT_PHOTO = okio.ByteString.EMPTY;
        DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
        DEFAULT_PHOTOTYPE = com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT;
    }
    
    private PhotoResponse(com.navdy.service.library.events.photo.PhotoResponse$Builder a) {
        this(a.photo, a.status, a.statusDetail, a.identifier, a.photoChecksum, a.photoType);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    PhotoResponse(com.navdy.service.library.events.photo.PhotoResponse$Builder a, com.navdy.service.library.events.photo.PhotoResponse$1 a0) {
        this(a);
    }
    
    public PhotoResponse(okio.ByteString a, com.navdy.service.library.events.RequestStatus a0, String s, String s0, String s1, com.navdy.service.library.events.photo.PhotoType a1) {
        this.photo = a;
        this.status = a0;
        this.statusDetail = s;
        this.identifier = s0;
        this.photoChecksum = s1;
        this.photoType = a1;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.photo.PhotoResponse) {
                com.navdy.service.library.events.photo.PhotoResponse a0 = (com.navdy.service.library.events.photo.PhotoResponse)a;
                boolean b0 = this.equals(this.photo, a0.photo);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.status, a0.status)) {
                        break label1;
                    }
                    if (!this.equals(this.statusDetail, a0.statusDetail)) {
                        break label1;
                    }
                    if (!this.equals(this.identifier, a0.identifier)) {
                        break label1;
                    }
                    if (!this.equals(this.photoChecksum, a0.photoChecksum)) {
                        break label1;
                    }
                    if (this.equals(this.photoType, a0.photoType)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((((this.photo == null) ? 0 : this.photo.hashCode()) * 37 + ((this.status == null) ? 0 : this.status.hashCode())) * 37 + ((this.statusDetail == null) ? 0 : this.statusDetail.hashCode())) * 37 + ((this.identifier == null) ? 0 : this.identifier.hashCode())) * 37 + ((this.photoChecksum == null) ? 0 : this.photoChecksum.hashCode())) * 37 + ((this.photoType == null) ? 0 : this.photoType.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
