package com.navdy.service.library.events.preferences;

final public class DriverProfilePreferences extends com.squareup.wire.Message {
    final public static java.util.List DEFAULT_ADDITIONALLOCALES;
    final public static Boolean DEFAULT_AUTO_ON_ENABLED;
    final public static String DEFAULT_CAR_MAKE = "";
    final public static String DEFAULT_CAR_MODEL = "";
    final public static String DEFAULT_CAR_YEAR = "";
    final public static String DEFAULT_DEVICE_NAME = "";
    final public static com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction DEFAULT_DIAL_LONG_PRESS_ACTION;
    final public static com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat DEFAULT_DISPLAY_FORMAT;
    final public static String DEFAULT_DRIVER_EMAIL = "";
    final public static String DEFAULT_DRIVER_NAME = "";
    final public static com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode DEFAULT_FEATURE_MODE;
    final public static Boolean DEFAULT_LIMIT_BANDWIDTH;
    final public static String DEFAULT_LOCALE = "en_US";
    final public static Long DEFAULT_OBDBLACKLISTLASTMODIFIED;
    final public static com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting DEFAULT_OBDSCANSETTING;
    final public static String DEFAULT_PHOTO_CHECKSUM = "";
    final public static Boolean DEFAULT_PROFILE_IS_PUBLIC;
    final public static Long DEFAULT_SERIAL_NUMBER;
    final public static com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem DEFAULT_UNIT_SYSTEM;
    final private static long serialVersionUID = 0L;
    final public java.util.List additionalLocales;
    final public Boolean auto_on_enabled;
    final public String car_make;
    final public String car_model;
    final public String car_year;
    final public String device_name;
    final public com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction dial_long_press_action;
    final public com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat display_format;
    final public String driver_email;
    final public String driver_name;
    final public com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode feature_mode;
    final public Boolean limit_bandwidth;
    final public String locale;
    final public Long obdBlacklistLastModified;
    final public com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting obdScanSetting;
    final public String photo_checksum;
    final public Boolean profile_is_public;
    final public Long serial_number;
    final public com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem unit_system;
    
    static {
        DEFAULT_SERIAL_NUMBER = Long.valueOf(0L);
        DEFAULT_PROFILE_IS_PUBLIC = Boolean.valueOf(false);
        DEFAULT_AUTO_ON_ENABLED = Boolean.valueOf(true);
        DEFAULT_DISPLAY_FORMAT = com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat.DISPLAY_FORMAT_NORMAL;
        DEFAULT_UNIT_SYSTEM = com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem.UNIT_SYSTEM_IMPERIAL;
        DEFAULT_FEATURE_MODE = com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode.FEATURE_MODE_RELEASE;
        DEFAULT_OBDSCANSETTING = com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting.SCAN_DEFAULT_ON;
        DEFAULT_LIMIT_BANDWIDTH = Boolean.valueOf(false);
        DEFAULT_OBDBLACKLISTLASTMODIFIED = Long.valueOf(0L);
        DEFAULT_DIAL_LONG_PRESS_ACTION = com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction.DIAL_LONG_PRESS_VOICE_ASSISTANT;
        DEFAULT_ADDITIONALLOCALES = java.util.Collections.emptyList();
    }
    
    private DriverProfilePreferences(com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder a) {
        this(a.serial_number, a.driver_name, a.device_name, a.profile_is_public, a.photo_checksum, a.driver_email, a.car_make, a.car_model, a.car_year, a.auto_on_enabled, a.display_format, a.locale, a.unit_system, a.feature_mode, a.obdScanSetting, a.limit_bandwidth, a.obdBlacklistLastModified, a.dial_long_press_action, a.additionalLocales);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    DriverProfilePreferences(com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder a, com.navdy.service.library.events.preferences.DriverProfilePreferences$1 a0) {
        this(a);
    }
    
    public DriverProfilePreferences(Long a, String s, String s0, Boolean a0, String s1, String s2, String s3, String s4, String s5, Boolean a1, com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat a2, String s6, com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem a3, com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode a4, com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting a5, Boolean a6, Long a7, com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction a8, java.util.List a9) {
        this.serial_number = a;
        this.driver_name = s;
        this.device_name = s0;
        this.profile_is_public = a0;
        this.photo_checksum = s1;
        this.driver_email = s2;
        this.car_make = s3;
        this.car_model = s4;
        this.car_year = s5;
        this.auto_on_enabled = a1;
        this.display_format = a2;
        this.locale = s6;
        this.unit_system = a3;
        this.feature_mode = a4;
        this.obdScanSetting = a5;
        this.limit_bandwidth = a6;
        this.obdBlacklistLastModified = a7;
        this.dial_long_press_action = a8;
        this.additionalLocales = com.navdy.service.library.events.preferences.DriverProfilePreferences.immutableCopyOf(a9);
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.preferences.DriverProfilePreferences.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.preferences.DriverProfilePreferences) {
                com.navdy.service.library.events.preferences.DriverProfilePreferences a0 = (com.navdy.service.library.events.preferences.DriverProfilePreferences)a;
                boolean b0 = this.equals(this.serial_number, a0.serial_number);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.driver_name, a0.driver_name)) {
                        break label1;
                    }
                    if (!this.equals(this.device_name, a0.device_name)) {
                        break label1;
                    }
                    if (!this.equals(this.profile_is_public, a0.profile_is_public)) {
                        break label1;
                    }
                    if (!this.equals(this.photo_checksum, a0.photo_checksum)) {
                        break label1;
                    }
                    if (!this.equals(this.driver_email, a0.driver_email)) {
                        break label1;
                    }
                    if (!this.equals(this.car_make, a0.car_make)) {
                        break label1;
                    }
                    if (!this.equals(this.car_model, a0.car_model)) {
                        break label1;
                    }
                    if (!this.equals(this.car_year, a0.car_year)) {
                        break label1;
                    }
                    if (!this.equals(this.auto_on_enabled, a0.auto_on_enabled)) {
                        break label1;
                    }
                    if (!this.equals(this.display_format, a0.display_format)) {
                        break label1;
                    }
                    if (!this.equals(this.locale, a0.locale)) {
                        break label1;
                    }
                    if (!this.equals(this.unit_system, a0.unit_system)) {
                        break label1;
                    }
                    if (!this.equals(this.feature_mode, a0.feature_mode)) {
                        break label1;
                    }
                    if (!this.equals(this.obdScanSetting, a0.obdScanSetting)) {
                        break label1;
                    }
                    if (!this.equals(this.limit_bandwidth, a0.limit_bandwidth)) {
                        break label1;
                    }
                    if (!this.equals(this.obdBlacklistLastModified, a0.obdBlacklistLastModified)) {
                        break label1;
                    }
                    if (!this.equals(this.dial_long_press_action, a0.dial_long_press_action)) {
                        break label1;
                    }
                    if (this.equals(this.additionalLocales, a0.additionalLocales)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            int i0 = (this.serial_number == null) ? 0 : this.serial_number.hashCode();
            int i1 = (this.driver_name == null) ? 0 : this.driver_name.hashCode();
            int i2 = (this.device_name == null) ? 0 : this.device_name.hashCode();
            int i3 = (this.profile_is_public == null) ? 0 : this.profile_is_public.hashCode();
            int i4 = (this.photo_checksum == null) ? 0 : this.photo_checksum.hashCode();
            int i5 = (this.driver_email == null) ? 0 : this.driver_email.hashCode();
            int i6 = (this.car_make == null) ? 0 : this.car_make.hashCode();
            int i7 = (this.car_model == null) ? 0 : this.car_model.hashCode();
            int i8 = (this.car_year == null) ? 0 : this.car_year.hashCode();
            int i9 = (this.auto_on_enabled == null) ? 0 : this.auto_on_enabled.hashCode();
            int i10 = (this.display_format == null) ? 0 : this.display_format.hashCode();
            int i11 = (this.locale == null) ? 0 : this.locale.hashCode();
            int i12 = (this.unit_system == null) ? 0 : this.unit_system.hashCode();
            int i13 = (this.feature_mode == null) ? 0 : this.feature_mode.hashCode();
            int i14 = (this.obdScanSetting == null) ? 0 : this.obdScanSetting.hashCode();
            int i15 = (this.limit_bandwidth == null) ? 0 : this.limit_bandwidth.hashCode();
            int i16 = (this.obdBlacklistLastModified == null) ? 0 : this.obdBlacklistLastModified.hashCode();
            int i17 = (this.dial_long_press_action == null) ? 0 : this.dial_long_press_action.hashCode();
            int i18 = (this.additionalLocales == null) ? 1 : this.additionalLocales.hashCode();
            int i19 = ((i0 * 37 + i1) * 37 + i2) * 37 + i3;
            i = ((((((((((((((i19 * 37 + i4) * 37 + i5) * 37 + i6) * 37 + i7) * 37 + i8) * 37 + i9) * 37 + i10) * 37 + i11) * 37 + i12) * 37 + i13) * 37 + i14) * 37 + i15) * 37 + i16) * 37 + i17) * 37 + i18;
            this.hashCode = i;
        }
        return i;
    }
}
