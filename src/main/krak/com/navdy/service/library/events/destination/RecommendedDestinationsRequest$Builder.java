package com.navdy.service.library.events.destination;

final public class RecommendedDestinationsRequest$Builder extends com.squareup.wire.Message.Builder {
    public Long serial_number;
    
    public RecommendedDestinationsRequest$Builder() {
    }
    
    public RecommendedDestinationsRequest$Builder(com.navdy.service.library.events.destination.RecommendedDestinationsRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.serial_number = a.serial_number;
        }
    }
    
    public com.navdy.service.library.events.destination.RecommendedDestinationsRequest build() {
        return new com.navdy.service.library.events.destination.RecommendedDestinationsRequest(this, (com.navdy.service.library.events.destination.RecommendedDestinationsRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.destination.RecommendedDestinationsRequest$Builder serial_number(Long a) {
        this.serial_number = a;
        return this;
    }
}
