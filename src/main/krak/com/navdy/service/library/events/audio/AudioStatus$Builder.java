package com.navdy.service.library.events.audio;

final public class AudioStatus$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.audio.AudioStatus$ConnectionType connectionType;
    public Boolean hasSCOConnection;
    public Boolean isConnected;
    public Boolean isPlaying;
    public com.navdy.service.library.events.audio.AudioStatus$ProfileType profileType;
    
    public AudioStatus$Builder() {
    }
    
    public AudioStatus$Builder(com.navdy.service.library.events.audio.AudioStatus a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.isConnected = a.isConnected;
            this.connectionType = a.connectionType;
            this.profileType = a.profileType;
            this.isPlaying = a.isPlaying;
            this.hasSCOConnection = a.hasSCOConnection;
        }
    }
    
    public com.navdy.service.library.events.audio.AudioStatus build() {
        return new com.navdy.service.library.events.audio.AudioStatus(this, (com.navdy.service.library.events.audio.AudioStatus$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.AudioStatus$Builder connectionType(com.navdy.service.library.events.audio.AudioStatus$ConnectionType a) {
        this.connectionType = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.AudioStatus$Builder hasSCOConnection(Boolean a) {
        this.hasSCOConnection = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.AudioStatus$Builder isConnected(Boolean a) {
        this.isConnected = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.AudioStatus$Builder isPlaying(Boolean a) {
        this.isPlaying = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.AudioStatus$Builder profileType(com.navdy.service.library.events.audio.AudioStatus$ProfileType a) {
        this.profileType = a;
        return this;
    }
}
