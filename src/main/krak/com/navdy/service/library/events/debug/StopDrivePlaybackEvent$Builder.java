package com.navdy.service.library.events.debug;

final public class StopDrivePlaybackEvent$Builder extends com.squareup.wire.Message.Builder {
    public StopDrivePlaybackEvent$Builder() {
    }
    
    public StopDrivePlaybackEvent$Builder(com.navdy.service.library.events.debug.StopDrivePlaybackEvent a) {
        super((com.squareup.wire.Message)a);
    }
    
    public com.navdy.service.library.events.debug.StopDrivePlaybackEvent build() {
        return new com.navdy.service.library.events.debug.StopDrivePlaybackEvent(this, (com.navdy.service.library.events.debug.StopDrivePlaybackEvent$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
}
