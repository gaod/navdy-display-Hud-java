package com.navdy.service.library.events.places;

final public class AutoCompleteResponse$Builder extends com.squareup.wire.Message.Builder {
    public String partialSearch;
    public java.util.List results;
    public com.navdy.service.library.events.RequestStatus status;
    public String statusDetail;
    
    public AutoCompleteResponse$Builder() {
    }
    
    public AutoCompleteResponse$Builder(com.navdy.service.library.events.places.AutoCompleteResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.partialSearch = a.partialSearch;
            this.status = a.status;
            this.statusDetail = a.statusDetail;
            this.results = com.navdy.service.library.events.places.AutoCompleteResponse.access$000(a.results);
        }
    }
    
    public com.navdy.service.library.events.places.AutoCompleteResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.places.AutoCompleteResponse(this, (com.navdy.service.library.events.places.AutoCompleteResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.places.AutoCompleteResponse$Builder partialSearch(String s) {
        this.partialSearch = s;
        return this;
    }
    
    public com.navdy.service.library.events.places.AutoCompleteResponse$Builder results(java.util.List a) {
        this.results = com.navdy.service.library.events.places.AutoCompleteResponse$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.places.AutoCompleteResponse$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
    
    public com.navdy.service.library.events.places.AutoCompleteResponse$Builder statusDetail(String s) {
        this.statusDetail = s;
        return this;
    }
}
