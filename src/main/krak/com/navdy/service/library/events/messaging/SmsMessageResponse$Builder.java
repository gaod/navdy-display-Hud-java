package com.navdy.service.library.events.messaging;

final public class SmsMessageResponse$Builder extends com.squareup.wire.Message.Builder {
    public String id;
    public String name;
    public String number;
    public com.navdy.service.library.events.RequestStatus status;
    
    public SmsMessageResponse$Builder() {
    }
    
    public SmsMessageResponse$Builder(com.navdy.service.library.events.messaging.SmsMessageResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
            this.number = a.number;
            this.name = a.name;
            this.id = a.id;
        }
    }
    
    public com.navdy.service.library.events.messaging.SmsMessageResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.messaging.SmsMessageResponse(this, (com.navdy.service.library.events.messaging.SmsMessageResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.messaging.SmsMessageResponse$Builder id(String s) {
        this.id = s;
        return this;
    }
    
    public com.navdy.service.library.events.messaging.SmsMessageResponse$Builder name(String s) {
        this.name = s;
        return this;
    }
    
    public com.navdy.service.library.events.messaging.SmsMessageResponse$Builder number(String s) {
        this.number = s;
        return this;
    }
    
    public com.navdy.service.library.events.messaging.SmsMessageResponse$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
}
