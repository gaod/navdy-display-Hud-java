package com.navdy.service.library.events.input;

final public class GestureEvent extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.input.Gesture DEFAULT_GESTURE;
    final public static Integer DEFAULT_X;
    final public static Integer DEFAULT_Y;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.input.Gesture gesture;
    final public Integer x;
    final public Integer y;
    
    static {
        DEFAULT_GESTURE = com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_LEFT;
        DEFAULT_X = Integer.valueOf(0);
        DEFAULT_Y = Integer.valueOf(0);
    }
    
    public GestureEvent(com.navdy.service.library.events.input.Gesture a, Integer a0, Integer a1) {
        this.gesture = a;
        this.x = a0;
        this.y = a1;
    }
    
    private GestureEvent(com.navdy.service.library.events.input.GestureEvent$Builder a) {
        this(a.gesture, a.x, a.y);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    GestureEvent(com.navdy.service.library.events.input.GestureEvent$Builder a, com.navdy.service.library.events.input.GestureEvent$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.input.GestureEvent) {
                com.navdy.service.library.events.input.GestureEvent a0 = (com.navdy.service.library.events.input.GestureEvent)a;
                boolean b0 = this.equals(this.gesture, a0.gesture);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.x, a0.x)) {
                        break label1;
                    }
                    if (this.equals(this.y, a0.y)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((this.gesture == null) ? 0 : this.gesture.hashCode()) * 37 + ((this.x == null) ? 0 : this.x.hashCode())) * 37 + ((this.y == null) ? 0 : this.y.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
