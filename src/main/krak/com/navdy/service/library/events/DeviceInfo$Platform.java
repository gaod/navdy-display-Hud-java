package com.navdy.service.library.events;

final public class DeviceInfo$Platform extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.DeviceInfo$Platform[] $VALUES;
    final public static com.navdy.service.library.events.DeviceInfo$Platform PLATFORM_Android;
    final public static com.navdy.service.library.events.DeviceInfo$Platform PLATFORM_iOS;
    final private int value;
    
    static {
        PLATFORM_iOS = new com.navdy.service.library.events.DeviceInfo$Platform("PLATFORM_iOS", 0, 1);
        PLATFORM_Android = new com.navdy.service.library.events.DeviceInfo$Platform("PLATFORM_Android", 1, 2);
        com.navdy.service.library.events.DeviceInfo$Platform[] a = new com.navdy.service.library.events.DeviceInfo$Platform[2];
        a[0] = PLATFORM_iOS;
        a[1] = PLATFORM_Android;
        $VALUES = a;
    }
    
    private DeviceInfo$Platform(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.DeviceInfo$Platform valueOf(String s) {
        return (com.navdy.service.library.events.DeviceInfo$Platform)Enum.valueOf(com.navdy.service.library.events.DeviceInfo$Platform.class, s);
    }
    
    public static com.navdy.service.library.events.DeviceInfo$Platform[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
