package com.navdy.service.library.events.navigation;

final public class NavigationSessionDeferRequest extends com.squareup.wire.Message {
    final public static Integer DEFAULT_DELAY;
    final public static Long DEFAULT_EXPIRETIMESTAMP;
    final public static Boolean DEFAULT_ORIGINDISPLAY;
    final public static String DEFAULT_REQUESTID = "";
    final public static String DEFAULT_ROUTEID = "";
    final private static long serialVersionUID = 0L;
    final public Integer delay;
    final public Long expireTimestamp;
    final public Boolean originDisplay;
    final public String requestId;
    final public String routeId;
    
    static {
        DEFAULT_DELAY = Integer.valueOf(0);
        DEFAULT_ORIGINDISPLAY = Boolean.valueOf(false);
        DEFAULT_EXPIRETIMESTAMP = Long.valueOf(0L);
    }
    
    private NavigationSessionDeferRequest(com.navdy.service.library.events.navigation.NavigationSessionDeferRequest$Builder a) {
        this(a.requestId, a.routeId, a.delay, a.originDisplay, a.expireTimestamp);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NavigationSessionDeferRequest(com.navdy.service.library.events.navigation.NavigationSessionDeferRequest$Builder a, com.navdy.service.library.events.navigation.NavigationSessionDeferRequest$1 a0) {
        this(a);
    }
    
    public NavigationSessionDeferRequest(String s, String s0, Integer a, Boolean a0, Long a1) {
        this.requestId = s;
        this.routeId = s0;
        this.delay = a;
        this.originDisplay = a0;
        this.expireTimestamp = a1;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.navigation.NavigationSessionDeferRequest) {
                com.navdy.service.library.events.navigation.NavigationSessionDeferRequest a0 = (com.navdy.service.library.events.navigation.NavigationSessionDeferRequest)a;
                boolean b0 = this.equals(this.requestId, a0.requestId);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.routeId, a0.routeId)) {
                        break label1;
                    }
                    if (!this.equals(this.delay, a0.delay)) {
                        break label1;
                    }
                    if (!this.equals(this.originDisplay, a0.originDisplay)) {
                        break label1;
                    }
                    if (this.equals(this.expireTimestamp, a0.expireTimestamp)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((this.requestId == null) ? 0 : this.requestId.hashCode()) * 37 + ((this.routeId == null) ? 0 : this.routeId.hashCode())) * 37 + ((this.delay == null) ? 0 : this.delay.hashCode())) * 37 + ((this.originDisplay == null) ? 0 : this.originDisplay.hashCode())) * 37 + ((this.expireTimestamp == null) ? 0 : this.expireTimestamp.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
