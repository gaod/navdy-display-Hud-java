package com.navdy.service.library.events.file;

final public class FileListRequest$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.file.FileType file_type;
    
    public FileListRequest$Builder() {
    }
    
    public FileListRequest$Builder(com.navdy.service.library.events.file.FileListRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.file_type = a.file_type;
        }
    }
    
    public com.navdy.service.library.events.file.FileListRequest build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.file.FileListRequest(this, (com.navdy.service.library.events.file.FileListRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.file.FileListRequest$Builder file_type(com.navdy.service.library.events.file.FileType a) {
        this.file_type = a;
        return this;
    }
}
