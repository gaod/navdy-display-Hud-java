package com.navdy.service.library.events.places;

final public class PlacesSearchRequest extends com.squareup.wire.Message {
    final public static Integer DEFAULT_MAXRESULTS;
    final public static String DEFAULT_REQUESTID = "";
    final public static Integer DEFAULT_SEARCHAREA;
    final public static String DEFAULT_SEARCHQUERY = "";
    final private static long serialVersionUID = 0L;
    final public Integer maxResults;
    final public String requestId;
    final public Integer searchArea;
    final public String searchQuery;
    
    static {
        DEFAULT_SEARCHAREA = Integer.valueOf(0);
        DEFAULT_MAXRESULTS = Integer.valueOf(0);
    }
    
    private PlacesSearchRequest(com.navdy.service.library.events.places.PlacesSearchRequest$Builder a) {
        this(a.searchQuery, a.searchArea, a.maxResults, a.requestId);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    PlacesSearchRequest(com.navdy.service.library.events.places.PlacesSearchRequest$Builder a, com.navdy.service.library.events.places.PlacesSearchRequest$1 a0) {
        this(a);
    }
    
    public PlacesSearchRequest(String s, Integer a, Integer a0, String s0) {
        this.searchQuery = s;
        this.searchArea = a;
        this.maxResults = a0;
        this.requestId = s0;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.places.PlacesSearchRequest) {
                com.navdy.service.library.events.places.PlacesSearchRequest a0 = (com.navdy.service.library.events.places.PlacesSearchRequest)a;
                boolean b0 = this.equals(this.searchQuery, a0.searchQuery);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.searchArea, a0.searchArea)) {
                        break label1;
                    }
                    if (!this.equals(this.maxResults, a0.maxResults)) {
                        break label1;
                    }
                    if (this.equals(this.requestId, a0.requestId)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((this.searchQuery == null) ? 0 : this.searchQuery.hashCode()) * 37 + ((this.searchArea == null) ? 0 : this.searchArea.hashCode())) * 37 + ((this.maxResults == null) ? 0 : this.maxResults.hashCode())) * 37 + ((this.requestId == null) ? 0 : this.requestId.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
