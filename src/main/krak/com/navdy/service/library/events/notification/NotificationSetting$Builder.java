package com.navdy.service.library.events.notification;

final public class NotificationSetting$Builder extends com.squareup.wire.Message.Builder {
    public String app;
    public Boolean enabled;
    
    public NotificationSetting$Builder() {
    }
    
    public NotificationSetting$Builder(com.navdy.service.library.events.notification.NotificationSetting a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.app = a.app;
            this.enabled = a.enabled;
        }
    }
    
    public com.navdy.service.library.events.notification.NotificationSetting$Builder app(String s) {
        this.app = s;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationSetting build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.notification.NotificationSetting(this, (com.navdy.service.library.events.notification.NotificationSetting$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.notification.NotificationSetting$Builder enabled(Boolean a) {
        this.enabled = a;
        return this;
    }
}
