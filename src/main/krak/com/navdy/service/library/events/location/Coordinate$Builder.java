package com.navdy.service.library.events.location;

final public class Coordinate$Builder extends com.squareup.wire.Message.Builder {
    public Float accuracy;
    public Double altitude;
    public Float bearing;
    public Double latitude;
    public Double longitude;
    public String provider;
    public Float speed;
    public Long timestamp;
    
    public Coordinate$Builder() {
    }
    
    public Coordinate$Builder(com.navdy.service.library.events.location.Coordinate a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.latitude = a.latitude;
            this.longitude = a.longitude;
            this.accuracy = a.accuracy;
            this.altitude = a.altitude;
            this.bearing = a.bearing;
            this.speed = a.speed;
            this.timestamp = a.timestamp;
            this.provider = a.provider;
        }
    }
    
    public com.navdy.service.library.events.location.Coordinate$Builder accuracy(Float a) {
        this.accuracy = a;
        return this;
    }
    
    public com.navdy.service.library.events.location.Coordinate$Builder altitude(Double a) {
        this.altitude = a;
        return this;
    }
    
    public com.navdy.service.library.events.location.Coordinate$Builder bearing(Float a) {
        this.bearing = a;
        return this;
    }
    
    public com.navdy.service.library.events.location.Coordinate build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.location.Coordinate(this, (com.navdy.service.library.events.location.Coordinate$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.location.Coordinate$Builder latitude(Double a) {
        this.latitude = a;
        return this;
    }
    
    public com.navdy.service.library.events.location.Coordinate$Builder longitude(Double a) {
        this.longitude = a;
        return this;
    }
    
    public com.navdy.service.library.events.location.Coordinate$Builder provider(String s) {
        this.provider = s;
        return this;
    }
    
    public com.navdy.service.library.events.location.Coordinate$Builder speed(Float a) {
        this.speed = a;
        return this;
    }
    
    public com.navdy.service.library.events.location.Coordinate$Builder timestamp(Long a) {
        this.timestamp = a;
        return this;
    }
}
