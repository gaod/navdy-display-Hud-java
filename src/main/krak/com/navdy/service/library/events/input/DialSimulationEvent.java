package com.navdy.service.library.events.input;

final public class DialSimulationEvent extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction DEFAULT_DIALACTION;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction dialAction;
    
    static {
        DEFAULT_DIALACTION = com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction.DIAL_CLICK;
    }
    
    private DialSimulationEvent(com.navdy.service.library.events.input.DialSimulationEvent$Builder a) {
        this(a.dialAction);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    DialSimulationEvent(com.navdy.service.library.events.input.DialSimulationEvent$Builder a, com.navdy.service.library.events.input.DialSimulationEvent$1 a0) {
        this(a);
    }
    
    public DialSimulationEvent(com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction a) {
        this.dialAction = a;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.input.DialSimulationEvent && this.equals(this.dialAction, ((com.navdy.service.library.events.input.DialSimulationEvent)a).dialAction);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.dialAction == null) ? 0 : this.dialAction.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
