package com.navdy.service.library.events;

final public class Frame extends com.squareup.wire.Message {
    final public static Integer DEFAULT_SIZE;
    final private static long serialVersionUID = 0L;
    final public Integer size;
    
    static {
        DEFAULT_SIZE = Integer.valueOf(0);
    }
    
    private Frame(com.navdy.service.library.events.Frame$Builder a) {
        this(a.size);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    Frame(com.navdy.service.library.events.Frame$Builder a, com.navdy.service.library.events.Frame$1 a0) {
        this(a);
    }
    
    public Frame(Integer a) {
        this.size = a;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.Frame && this.equals(this.size, ((com.navdy.service.library.events.Frame)a).size);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.size == null) ? 0 : this.size.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
