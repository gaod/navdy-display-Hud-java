package com.navdy.service.library.events.navigation;

final public class RouteManeuverRequest$Builder extends com.squareup.wire.Message.Builder {
    public String routeId;
    
    public RouteManeuverRequest$Builder() {
    }
    
    public RouteManeuverRequest$Builder(com.navdy.service.library.events.navigation.RouteManeuverRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.routeId = a.routeId;
        }
    }
    
    public com.navdy.service.library.events.navigation.RouteManeuverRequest build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.navigation.RouteManeuverRequest(this, (com.navdy.service.library.events.navigation.RouteManeuverRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.navigation.RouteManeuverRequest$Builder routeId(String s) {
        this.routeId = s;
        return this;
    }
}
