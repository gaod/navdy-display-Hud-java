package com.navdy.service.library.events;

final public class DeviceInfo extends com.squareup.wire.Message {
    final public static String DEFAULT_BUILDTYPE = "user";
    final public static String DEFAULT_CLIENTVERSION = "";
    final public static String DEFAULT_DEVICEID = "";
    final public static String DEFAULT_DEVICEMAKE = "unknown";
    final public static String DEFAULT_DEVICENAME = "";
    final public static String DEFAULT_DEVICEUUID = "";
    final public static Boolean DEFAULT_FORCEFULLUPDATE;
    final public static String DEFAULT_KERNELVERSION = "";
    final public static java.util.List DEFAULT_LEGACYCAPABILITIES;
    final public static String DEFAULT_MODEL = "";
    final public static java.util.List DEFAULT_MUSICPLAYERS_OBSOLETE;
    final public static com.navdy.service.library.events.DeviceInfo$Platform DEFAULT_PLATFORM;
    final public static String DEFAULT_PROTOCOLVERSION = "";
    final public static Integer DEFAULT_SYSTEMAPILEVEL;
    final public static String DEFAULT_SYSTEMVERSION = "";
    final private static long serialVersionUID = 0L;
    final public String buildType;
    final public com.navdy.service.library.events.Capabilities capabilities;
    final public String clientVersion;
    final public String deviceId;
    final public String deviceMake;
    final public String deviceName;
    final public String deviceUuid;
    final public Boolean forceFullUpdate;
    final public String kernelVersion;
    final public java.util.List legacyCapabilities;
    final public String model;
    final public java.util.List musicPlayers_OBSOLETE;
    final public com.navdy.service.library.events.DeviceInfo$Platform platform;
    final public String protocolVersion;
    final public Integer systemApiLevel;
    final public String systemVersion;
    
    static {
        DEFAULT_SYSTEMAPILEVEL = Integer.valueOf(0);
        DEFAULT_PLATFORM = com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS;
        DEFAULT_FORCEFULLUPDATE = Boolean.valueOf(false);
        DEFAULT_LEGACYCAPABILITIES = java.util.Collections.emptyList();
        DEFAULT_MUSICPLAYERS_OBSOLETE = java.util.Collections.emptyList();
    }
    
    private DeviceInfo(com.navdy.service.library.events.DeviceInfo$Builder a) {
        this(a.deviceId, a.clientVersion, a.protocolVersion, a.deviceName, a.systemVersion, a.model, a.deviceUuid, a.systemApiLevel, a.kernelVersion, a.platform, a.buildType, a.deviceMake, a.forceFullUpdate, a.legacyCapabilities, a.musicPlayers_OBSOLETE, a.capabilities);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    DeviceInfo(com.navdy.service.library.events.DeviceInfo$Builder a, com.navdy.service.library.events.DeviceInfo$1 a0) {
        this(a);
    }
    
    public DeviceInfo(String s, String s0, String s1, String s2, String s3, String s4, String s5, Integer a, String s6, com.navdy.service.library.events.DeviceInfo$Platform a0, String s7, String s8, Boolean a1, java.util.List a2, java.util.List a3, com.navdy.service.library.events.Capabilities a4) {
        this.deviceId = s;
        this.clientVersion = s0;
        this.protocolVersion = s1;
        this.deviceName = s2;
        this.systemVersion = s3;
        this.model = s4;
        this.deviceUuid = s5;
        this.systemApiLevel = a;
        this.kernelVersion = s6;
        this.platform = a0;
        this.buildType = s7;
        this.deviceMake = s8;
        this.forceFullUpdate = a1;
        this.legacyCapabilities = com.navdy.service.library.events.DeviceInfo.immutableCopyOf(a2);
        this.musicPlayers_OBSOLETE = com.navdy.service.library.events.DeviceInfo.immutableCopyOf(a3);
        this.capabilities = a4;
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.DeviceInfo.copyOf(a);
    }
    
    static java.util.List access$100(java.util.List a) {
        return com.navdy.service.library.events.DeviceInfo.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.DeviceInfo) {
                com.navdy.service.library.events.DeviceInfo a0 = (com.navdy.service.library.events.DeviceInfo)a;
                boolean b0 = this.equals(this.deviceId, a0.deviceId);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.clientVersion, a0.clientVersion)) {
                        break label1;
                    }
                    if (!this.equals(this.protocolVersion, a0.protocolVersion)) {
                        break label1;
                    }
                    if (!this.equals(this.deviceName, a0.deviceName)) {
                        break label1;
                    }
                    if (!this.equals(this.systemVersion, a0.systemVersion)) {
                        break label1;
                    }
                    if (!this.equals(this.model, a0.model)) {
                        break label1;
                    }
                    if (!this.equals(this.deviceUuid, a0.deviceUuid)) {
                        break label1;
                    }
                    if (!this.equals(this.systemApiLevel, a0.systemApiLevel)) {
                        break label1;
                    }
                    if (!this.equals(this.kernelVersion, a0.kernelVersion)) {
                        break label1;
                    }
                    if (!this.equals(this.platform, a0.platform)) {
                        break label1;
                    }
                    if (!this.equals(this.buildType, a0.buildType)) {
                        break label1;
                    }
                    if (!this.equals(this.deviceMake, a0.deviceMake)) {
                        break label1;
                    }
                    if (!this.equals(this.forceFullUpdate, a0.forceFullUpdate)) {
                        break label1;
                    }
                    if (!this.equals(this.legacyCapabilities, a0.legacyCapabilities)) {
                        break label1;
                    }
                    if (!this.equals(this.musicPlayers_OBSOLETE, a0.musicPlayers_OBSOLETE)) {
                        break label1;
                    }
                    if (this.equals(this.capabilities, a0.capabilities)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            int i0 = (this.deviceId == null) ? 0 : this.deviceId.hashCode();
            int i1 = (this.clientVersion == null) ? 0 : this.clientVersion.hashCode();
            int i2 = (this.protocolVersion == null) ? 0 : this.protocolVersion.hashCode();
            int i3 = (this.deviceName == null) ? 0 : this.deviceName.hashCode();
            int i4 = (this.systemVersion == null) ? 0 : this.systemVersion.hashCode();
            int i5 = (this.model == null) ? 0 : this.model.hashCode();
            int i6 = (this.deviceUuid == null) ? 0 : this.deviceUuid.hashCode();
            int i7 = (this.systemApiLevel == null) ? 0 : this.systemApiLevel.hashCode();
            int i8 = (this.kernelVersion == null) ? 0 : this.kernelVersion.hashCode();
            int i9 = (this.platform == null) ? 0 : this.platform.hashCode();
            int i10 = (this.buildType == null) ? 0 : this.buildType.hashCode();
            int i11 = (this.deviceMake == null) ? 0 : this.deviceMake.hashCode();
            int i12 = (this.forceFullUpdate == null) ? 0 : this.forceFullUpdate.hashCode();
            int i13 = (this.legacyCapabilities == null) ? 1 : this.legacyCapabilities.hashCode();
            int i14 = (this.musicPlayers_OBSOLETE == null) ? 1 : this.musicPlayers_OBSOLETE.hashCode();
            int i15 = (this.capabilities == null) ? 0 : this.capabilities.hashCode();
            i = ((((((((((((((i0 * 37 + i1) * 37 + i2) * 37 + i3) * 37 + i4) * 37 + i5) * 37 + i6) * 37 + i7) * 37 + i8) * 37 + i9) * 37 + i10) * 37 + i11) * 37 + i12) * 37 + i13) * 37 + i14) * 37 + i15;
            this.hashCode = i;
        }
        return i;
    }
}
