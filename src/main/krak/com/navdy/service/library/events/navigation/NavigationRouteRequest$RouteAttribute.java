package com.navdy.service.library.events.navigation;

final public class NavigationRouteRequest$RouteAttribute extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.navigation.NavigationRouteRequest$RouteAttribute[] $VALUES;
    final public static com.navdy.service.library.events.navigation.NavigationRouteRequest$RouteAttribute ROUTE_ATTRIBUTE_GAS;
    final public static com.navdy.service.library.events.navigation.NavigationRouteRequest$RouteAttribute ROUTE_ATTRIBUTE_SAVED_ROUTE;
    final private int value;
    
    static {
        ROUTE_ATTRIBUTE_GAS = new com.navdy.service.library.events.navigation.NavigationRouteRequest$RouteAttribute("ROUTE_ATTRIBUTE_GAS", 0, 0);
        ROUTE_ATTRIBUTE_SAVED_ROUTE = new com.navdy.service.library.events.navigation.NavigationRouteRequest$RouteAttribute("ROUTE_ATTRIBUTE_SAVED_ROUTE", 1, 1);
        com.navdy.service.library.events.navigation.NavigationRouteRequest$RouteAttribute[] a = new com.navdy.service.library.events.navigation.NavigationRouteRequest$RouteAttribute[2];
        a[0] = ROUTE_ATTRIBUTE_GAS;
        a[1] = ROUTE_ATTRIBUTE_SAVED_ROUTE;
        $VALUES = a;
    }
    
    private NavigationRouteRequest$RouteAttribute(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.navigation.NavigationRouteRequest$RouteAttribute valueOf(String s) {
        return (com.navdy.service.library.events.navigation.NavigationRouteRequest$RouteAttribute)Enum.valueOf(com.navdy.service.library.events.navigation.NavigationRouteRequest$RouteAttribute.class, s);
    }
    
    public static com.navdy.service.library.events.navigation.NavigationRouteRequest$RouteAttribute[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
