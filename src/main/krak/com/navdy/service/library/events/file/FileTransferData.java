package com.navdy.service.library.events.file;

final public class FileTransferData extends com.squareup.wire.Message {
    final public static Integer DEFAULT_CHUNKINDEX;
    final public static okio.ByteString DEFAULT_DATABYTES;
    final public static String DEFAULT_FILECHECKSUM = "";
    final public static Boolean DEFAULT_LASTCHUNK;
    final public static Integer DEFAULT_TRANSFERID;
    final private static long serialVersionUID = 0L;
    final public Integer chunkIndex;
    final public okio.ByteString dataBytes;
    final public String fileCheckSum;
    final public Boolean lastChunk;
    final public Integer transferId;
    
    static {
        DEFAULT_TRANSFERID = Integer.valueOf(0);
        DEFAULT_CHUNKINDEX = Integer.valueOf(0);
        DEFAULT_DATABYTES = okio.ByteString.EMPTY;
        DEFAULT_LASTCHUNK = Boolean.valueOf(false);
    }
    
    private FileTransferData(com.navdy.service.library.events.file.FileTransferData$Builder a) {
        this(a.transferId, a.chunkIndex, a.dataBytes, a.lastChunk, a.fileCheckSum);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    FileTransferData(com.navdy.service.library.events.file.FileTransferData$Builder a, com.navdy.service.library.events.file.FileTransferData$1 a0) {
        this(a);
    }
    
    public FileTransferData(Integer a, Integer a0, okio.ByteString a1, Boolean a2, String s) {
        this.transferId = a;
        this.chunkIndex = a0;
        this.dataBytes = a1;
        this.lastChunk = a2;
        this.fileCheckSum = s;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.file.FileTransferData) {
                com.navdy.service.library.events.file.FileTransferData a0 = (com.navdy.service.library.events.file.FileTransferData)a;
                boolean b0 = this.equals(this.transferId, a0.transferId);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.chunkIndex, a0.chunkIndex)) {
                        break label1;
                    }
                    if (!this.equals(this.dataBytes, a0.dataBytes)) {
                        break label1;
                    }
                    if (!this.equals(this.lastChunk, a0.lastChunk)) {
                        break label1;
                    }
                    if (this.equals(this.fileCheckSum, a0.fileCheckSum)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((this.transferId == null) ? 0 : this.transferId.hashCode()) * 37 + ((this.chunkIndex == null) ? 0 : this.chunkIndex.hashCode())) * 37 + ((this.dataBytes == null) ? 0 : this.dataBytes.hashCode())) * 37 + ((this.lastChunk == null) ? 0 : this.lastChunk.hashCode())) * 37 + ((this.fileCheckSum == null) ? 0 : this.fileCheckSum.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
