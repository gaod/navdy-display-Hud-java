package com.navdy.service.library.events.preferences;

final public class ScrollableSide extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.preferences.ScrollableSide[] $VALUES;
    final public static com.navdy.service.library.events.preferences.ScrollableSide LEFT;
    final public static com.navdy.service.library.events.preferences.ScrollableSide RIGHT;
    final private int value;
    
    static {
        LEFT = new com.navdy.service.library.events.preferences.ScrollableSide("LEFT", 0, 1);
        RIGHT = new com.navdy.service.library.events.preferences.ScrollableSide("RIGHT", 1, 2);
        com.navdy.service.library.events.preferences.ScrollableSide[] a = new com.navdy.service.library.events.preferences.ScrollableSide[2];
        a[0] = LEFT;
        a[1] = RIGHT;
        $VALUES = a;
    }
    
    private ScrollableSide(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.preferences.ScrollableSide valueOf(String s) {
        return (com.navdy.service.library.events.preferences.ScrollableSide)Enum.valueOf(com.navdy.service.library.events.preferences.ScrollableSide.class, s);
    }
    
    public static com.navdy.service.library.events.preferences.ScrollableSide[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
