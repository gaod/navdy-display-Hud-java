package com.navdy.service.library.events.places;

final public class DestinationSelectedRequest extends com.squareup.wire.Message {
    final public static String DEFAULT_REQUEST_ID = "";
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.destination.Destination destination;
    final public String request_id;
    
    private DestinationSelectedRequest(com.navdy.service.library.events.places.DestinationSelectedRequest$Builder a) {
        this(a.request_id, a.destination);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    DestinationSelectedRequest(com.navdy.service.library.events.places.DestinationSelectedRequest$Builder a, com.navdy.service.library.events.places.DestinationSelectedRequest$1 a0) {
        this(a);
    }
    
    public DestinationSelectedRequest(String s, com.navdy.service.library.events.destination.Destination a) {
        this.request_id = s;
        this.destination = a;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.places.DestinationSelectedRequest) {
                com.navdy.service.library.events.places.DestinationSelectedRequest a0 = (com.navdy.service.library.events.places.DestinationSelectedRequest)a;
                boolean b0 = this.equals(this.request_id, a0.request_id);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.destination, a0.destination)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.request_id == null) ? 0 : this.request_id.hashCode()) * 37 + ((this.destination == null) ? 0 : this.destination.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
