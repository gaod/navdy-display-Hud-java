package com.navdy.service.library.events.glances;

final public class SocialConstants extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.glances.SocialConstants[] $VALUES;
    final public static com.navdy.service.library.events.glances.SocialConstants SOCIAL_FROM;
    final public static com.navdy.service.library.events.glances.SocialConstants SOCIAL_MESSAGE;
    final public static com.navdy.service.library.events.glances.SocialConstants SOCIAL_TO;
    final private int value;
    
    static {
        SOCIAL_FROM = new com.navdy.service.library.events.glances.SocialConstants("SOCIAL_FROM", 0, 0);
        SOCIAL_TO = new com.navdy.service.library.events.glances.SocialConstants("SOCIAL_TO", 1, 1);
        SOCIAL_MESSAGE = new com.navdy.service.library.events.glances.SocialConstants("SOCIAL_MESSAGE", 2, 2);
        com.navdy.service.library.events.glances.SocialConstants[] a = new com.navdy.service.library.events.glances.SocialConstants[3];
        a[0] = SOCIAL_FROM;
        a[1] = SOCIAL_TO;
        a[2] = SOCIAL_MESSAGE;
        $VALUES = a;
    }
    
    private SocialConstants(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.glances.SocialConstants valueOf(String s) {
        return (com.navdy.service.library.events.glances.SocialConstants)Enum.valueOf(com.navdy.service.library.events.glances.SocialConstants.class, s);
    }
    
    public static com.navdy.service.library.events.glances.SocialConstants[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
