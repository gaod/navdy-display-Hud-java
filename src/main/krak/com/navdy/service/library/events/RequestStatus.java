package com.navdy.service.library.events;

final public class RequestStatus extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.RequestStatus[] $VALUES;
    final public static com.navdy.service.library.events.RequestStatus REQUEST_ALREADY_IN_PROGRESS;
    final public static com.navdy.service.library.events.RequestStatus REQUEST_CANCELLED;
    final public static com.navdy.service.library.events.RequestStatus REQUEST_INVALID_REQUEST;
    final public static com.navdy.service.library.events.RequestStatus REQUEST_INVALID_STATE;
    final public static com.navdy.service.library.events.RequestStatus REQUEST_NOT_AVAILABLE;
    final public static com.navdy.service.library.events.RequestStatus REQUEST_NOT_READY;
    final public static com.navdy.service.library.events.RequestStatus REQUEST_NO_LOCATION_SERVICE;
    final public static com.navdy.service.library.events.RequestStatus REQUEST_SERVICE_ERROR;
    final public static com.navdy.service.library.events.RequestStatus REQUEST_SUCCESS;
    final public static com.navdy.service.library.events.RequestStatus REQUEST_UNKNOWN_ERROR;
    final public static com.navdy.service.library.events.RequestStatus REQUEST_VERSION_IS_CURRENT;
    final private int value;
    
    static {
        REQUEST_SUCCESS = new com.navdy.service.library.events.RequestStatus("REQUEST_SUCCESS", 0, 1);
        REQUEST_NOT_READY = new com.navdy.service.library.events.RequestStatus("REQUEST_NOT_READY", 1, 2);
        REQUEST_NO_LOCATION_SERVICE = new com.navdy.service.library.events.RequestStatus("REQUEST_NO_LOCATION_SERVICE", 2, 3);
        REQUEST_SERVICE_ERROR = new com.navdy.service.library.events.RequestStatus("REQUEST_SERVICE_ERROR", 3, 4);
        REQUEST_INVALID_REQUEST = new com.navdy.service.library.events.RequestStatus("REQUEST_INVALID_REQUEST", 4, 5);
        REQUEST_INVALID_STATE = new com.navdy.service.library.events.RequestStatus("REQUEST_INVALID_STATE", 5, 6);
        REQUEST_UNKNOWN_ERROR = new com.navdy.service.library.events.RequestStatus("REQUEST_UNKNOWN_ERROR", 6, 7);
        REQUEST_VERSION_IS_CURRENT = new com.navdy.service.library.events.RequestStatus("REQUEST_VERSION_IS_CURRENT", 7, 8);
        REQUEST_NOT_AVAILABLE = new com.navdy.service.library.events.RequestStatus("REQUEST_NOT_AVAILABLE", 8, 9);
        REQUEST_ALREADY_IN_PROGRESS = new com.navdy.service.library.events.RequestStatus("REQUEST_ALREADY_IN_PROGRESS", 9, 10);
        REQUEST_CANCELLED = new com.navdy.service.library.events.RequestStatus("REQUEST_CANCELLED", 10, 11);
        com.navdy.service.library.events.RequestStatus[] a = new com.navdy.service.library.events.RequestStatus[11];
        a[0] = REQUEST_SUCCESS;
        a[1] = REQUEST_NOT_READY;
        a[2] = REQUEST_NO_LOCATION_SERVICE;
        a[3] = REQUEST_SERVICE_ERROR;
        a[4] = REQUEST_INVALID_REQUEST;
        a[5] = REQUEST_INVALID_STATE;
        a[6] = REQUEST_UNKNOWN_ERROR;
        a[7] = REQUEST_VERSION_IS_CURRENT;
        a[8] = REQUEST_NOT_AVAILABLE;
        a[9] = REQUEST_ALREADY_IN_PROGRESS;
        a[10] = REQUEST_CANCELLED;
        $VALUES = a;
    }
    
    private RequestStatus(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.RequestStatus valueOf(String s) {
        return (com.navdy.service.library.events.RequestStatus)Enum.valueOf(com.navdy.service.library.events.RequestStatus.class, s);
    }
    
    public static com.navdy.service.library.events.RequestStatus[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
