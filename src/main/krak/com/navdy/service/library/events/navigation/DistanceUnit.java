package com.navdy.service.library.events.navigation;

final public class DistanceUnit extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.navigation.DistanceUnit[] $VALUES;
    final public static com.navdy.service.library.events.navigation.DistanceUnit DISTANCE_FEET;
    final public static com.navdy.service.library.events.navigation.DistanceUnit DISTANCE_KMS;
    final public static com.navdy.service.library.events.navigation.DistanceUnit DISTANCE_METERS;
    final public static com.navdy.service.library.events.navigation.DistanceUnit DISTANCE_MILES;
    final private int value;
    
    static {
        DISTANCE_MILES = new com.navdy.service.library.events.navigation.DistanceUnit("DISTANCE_MILES", 0, 1);
        DISTANCE_KMS = new com.navdy.service.library.events.navigation.DistanceUnit("DISTANCE_KMS", 1, 2);
        DISTANCE_FEET = new com.navdy.service.library.events.navigation.DistanceUnit("DISTANCE_FEET", 2, 3);
        DISTANCE_METERS = new com.navdy.service.library.events.navigation.DistanceUnit("DISTANCE_METERS", 3, 4);
        com.navdy.service.library.events.navigation.DistanceUnit[] a = new com.navdy.service.library.events.navigation.DistanceUnit[4];
        a[0] = DISTANCE_MILES;
        a[1] = DISTANCE_KMS;
        a[2] = DISTANCE_FEET;
        a[3] = DISTANCE_METERS;
        $VALUES = a;
    }
    
    private DistanceUnit(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.navigation.DistanceUnit valueOf(String s) {
        return (com.navdy.service.library.events.navigation.DistanceUnit)Enum.valueOf(com.navdy.service.library.events.navigation.DistanceUnit.class, s);
    }
    
    public static com.navdy.service.library.events.navigation.DistanceUnit[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
