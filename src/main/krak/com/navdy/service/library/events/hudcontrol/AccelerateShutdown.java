package com.navdy.service.library.events.hudcontrol;

final public class AccelerateShutdown extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.hudcontrol.AccelerateShutdown$AccelerateReason DEFAULT_REASON;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.hudcontrol.AccelerateShutdown$AccelerateReason reason;
    
    static {
        DEFAULT_REASON = com.navdy.service.library.events.hudcontrol.AccelerateShutdown$AccelerateReason.ACCELERATE_REASON_UNKNOWN;
    }
    
    public AccelerateShutdown(com.navdy.service.library.events.hudcontrol.AccelerateShutdown$AccelerateReason a) {
        this.reason = a;
    }
    
    private AccelerateShutdown(com.navdy.service.library.events.hudcontrol.AccelerateShutdown$Builder a) {
        this(a.reason);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    AccelerateShutdown(com.navdy.service.library.events.hudcontrol.AccelerateShutdown$Builder a, com.navdy.service.library.events.hudcontrol.AccelerateShutdown$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.hudcontrol.AccelerateShutdown && this.equals(this.reason, ((com.navdy.service.library.events.hudcontrol.AccelerateShutdown)a).reason);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.reason == null) ? 0 : this.reason.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
