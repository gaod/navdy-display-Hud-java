package com.navdy.service.library.events.settings;

final public class Setting$Builder extends com.squareup.wire.Message.Builder {
    public String key;
    public String value;
    
    public Setting$Builder() {
    }
    
    public Setting$Builder(com.navdy.service.library.events.settings.Setting a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.key = a.key;
            this.value = a.value;
        }
    }
    
    public com.navdy.service.library.events.settings.Setting build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.settings.Setting(this, (com.navdy.service.library.events.settings.Setting$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.settings.Setting$Builder key(String s) {
        this.key = s;
        return this;
    }
    
    public com.navdy.service.library.events.settings.Setting$Builder value(String s) {
        this.value = s;
        return this;
    }
}
