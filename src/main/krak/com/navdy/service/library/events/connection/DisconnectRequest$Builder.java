package com.navdy.service.library.events.connection;

final public class DisconnectRequest$Builder extends com.squareup.wire.Message.Builder {
    public Boolean forget;
    
    public DisconnectRequest$Builder() {
    }
    
    public DisconnectRequest$Builder(com.navdy.service.library.events.connection.DisconnectRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.forget = a.forget;
        }
    }
    
    public com.navdy.service.library.events.connection.DisconnectRequest build() {
        return new com.navdy.service.library.events.connection.DisconnectRequest(this, (com.navdy.service.library.events.connection.DisconnectRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.connection.DisconnectRequest$Builder forget(Boolean a) {
        this.forget = a;
        return this;
    }
}
