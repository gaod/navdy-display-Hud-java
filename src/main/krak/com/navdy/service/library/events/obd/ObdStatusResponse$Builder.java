package com.navdy.service.library.events.obd;

final public class ObdStatusResponse$Builder extends com.squareup.wire.Message.Builder {
    public Boolean isConnected;
    public Integer speed;
    public com.navdy.service.library.events.RequestStatus status;
    public String vin;
    
    public ObdStatusResponse$Builder() {
    }
    
    public ObdStatusResponse$Builder(com.navdy.service.library.events.obd.ObdStatusResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
            this.isConnected = a.isConnected;
            this.vin = a.vin;
            this.speed = a.speed;
        }
    }
    
    public com.navdy.service.library.events.obd.ObdStatusResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.obd.ObdStatusResponse(this, (com.navdy.service.library.events.obd.ObdStatusResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.obd.ObdStatusResponse$Builder isConnected(Boolean a) {
        this.isConnected = a;
        return this;
    }
    
    public com.navdy.service.library.events.obd.ObdStatusResponse$Builder speed(Integer a) {
        this.speed = a;
        return this;
    }
    
    public com.navdy.service.library.events.obd.ObdStatusResponse$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
    
    public com.navdy.service.library.events.obd.ObdStatusResponse$Builder vin(String s) {
        this.vin = s;
        return this;
    }
}
