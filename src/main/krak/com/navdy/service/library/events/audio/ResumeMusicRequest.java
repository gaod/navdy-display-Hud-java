package com.navdy.service.library.events.audio;

final public class ResumeMusicRequest extends com.squareup.wire.Message {
    final private static long serialVersionUID = 0L;
    
    public ResumeMusicRequest() {
    }
    
    private ResumeMusicRequest(com.navdy.service.library.events.audio.ResumeMusicRequest$Builder a) {
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    ResumeMusicRequest(com.navdy.service.library.events.audio.ResumeMusicRequest$Builder a, com.navdy.service.library.events.audio.ResumeMusicRequest$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        return a instanceof com.navdy.service.library.events.audio.ResumeMusicRequest;
    }
    
    public int hashCode() {
        return 0;
    }
}
