package com.navdy.service.library.events.settings;

final public class NetworkStateChange extends com.squareup.wire.Message {
    final public static Boolean DEFAULT_CELLNETWORK;
    final public static Boolean DEFAULT_NETWORKAVAILABLE;
    final public static Boolean DEFAULT_REACHABILITY;
    final public static Boolean DEFAULT_WIFINETWORK;
    final private static long serialVersionUID = 0L;
    final public Boolean cellNetwork;
    final public Boolean networkAvailable;
    final public Boolean reachability;
    final public Boolean wifiNetwork;
    
    static {
        DEFAULT_NETWORKAVAILABLE = Boolean.valueOf(false);
        DEFAULT_CELLNETWORK = Boolean.valueOf(false);
        DEFAULT_WIFINETWORK = Boolean.valueOf(false);
        DEFAULT_REACHABILITY = Boolean.valueOf(false);
    }
    
    private NetworkStateChange(com.navdy.service.library.events.settings.NetworkStateChange$Builder a) {
        this(a.networkAvailable, a.cellNetwork, a.wifiNetwork, a.reachability);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NetworkStateChange(com.navdy.service.library.events.settings.NetworkStateChange$Builder a, com.navdy.service.library.events.settings.NetworkStateChange$1 a0) {
        this(a);
    }
    
    public NetworkStateChange(Boolean a, Boolean a0, Boolean a1, Boolean a2) {
        this.networkAvailable = a;
        this.cellNetwork = a0;
        this.wifiNetwork = a1;
        this.reachability = a2;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.settings.NetworkStateChange) {
                com.navdy.service.library.events.settings.NetworkStateChange a0 = (com.navdy.service.library.events.settings.NetworkStateChange)a;
                boolean b0 = this.equals(this.networkAvailable, a0.networkAvailable);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.cellNetwork, a0.cellNetwork)) {
                        break label1;
                    }
                    if (!this.equals(this.wifiNetwork, a0.wifiNetwork)) {
                        break label1;
                    }
                    if (this.equals(this.reachability, a0.reachability)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((this.networkAvailable == null) ? 0 : this.networkAvailable.hashCode()) * 37 + ((this.cellNetwork == null) ? 0 : this.cellNetwork.hashCode())) * 37 + ((this.wifiNetwork == null) ? 0 : this.wifiNetwork.hashCode())) * 37 + ((this.reachability == null) ? 0 : this.reachability.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
