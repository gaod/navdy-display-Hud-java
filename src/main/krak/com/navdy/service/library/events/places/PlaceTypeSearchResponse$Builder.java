package com.navdy.service.library.events.places;

final public class PlaceTypeSearchResponse$Builder extends com.squareup.wire.Message.Builder {
    public java.util.List destinations;
    public String request_id;
    public com.navdy.service.library.events.RequestStatus request_status;
    
    public PlaceTypeSearchResponse$Builder() {
    }
    
    public PlaceTypeSearchResponse$Builder(com.navdy.service.library.events.places.PlaceTypeSearchResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.request_id = a.request_id;
            this.request_status = a.request_status;
            this.destinations = com.navdy.service.library.events.places.PlaceTypeSearchResponse.access$000(a.destinations);
        }
    }
    
    public com.navdy.service.library.events.places.PlaceTypeSearchResponse build() {
        return new com.navdy.service.library.events.places.PlaceTypeSearchResponse(this, (com.navdy.service.library.events.places.PlaceTypeSearchResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.places.PlaceTypeSearchResponse$Builder destinations(java.util.List a) {
        this.destinations = com.navdy.service.library.events.places.PlaceTypeSearchResponse$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.places.PlaceTypeSearchResponse$Builder request_id(String s) {
        this.request_id = s;
        return this;
    }
    
    public com.navdy.service.library.events.places.PlaceTypeSearchResponse$Builder request_status(com.navdy.service.library.events.RequestStatus a) {
        this.request_status = a;
        return this;
    }
}
