package com.navdy.service.library.events;

final public class Capabilities extends com.squareup.wire.Message {
    final public static Boolean DEFAULT_CANNEDRESPONSETOSMS;
    final public static Boolean DEFAULT_COMPACTUI;
    final public static Boolean DEFAULT_CUSTOMDIALLONGPRESS;
    final public static Boolean DEFAULT_LOCALMUSICBROWSER;
    final public static Boolean DEFAULT_MUSICARTWORKCACHE;
    final public static Boolean DEFAULT_NAVCOORDSLOOKUP;
    final public static Boolean DEFAULT_PLACETYPESEARCH;
    final public static Boolean DEFAULT_SEARCHRESULTLIST;
    final public static Boolean DEFAULT_SUPPORTSPHONETICTTS;
    final public static Boolean DEFAULT_VOICESEARCH;
    final public static Boolean DEFAULT_VOICESEARCHNEWIOSPAUSEBEHAVIORS;
    final private static long serialVersionUID = 0L;
    final public Boolean cannedResponseToSms;
    final public Boolean compactUi;
    final public Boolean customDialLongPress;
    final public Boolean localMusicBrowser;
    final public Boolean musicArtworkCache;
    final public Boolean navCoordsLookup;
    final public Boolean placeTypeSearch;
    final public Boolean searchResultList;
    final public Boolean supportsPhoneticTts;
    final public Boolean voiceSearch;
    final public Boolean voiceSearchNewIOSPauseBehaviors;
    
    static {
        DEFAULT_COMPACTUI = Boolean.valueOf(false);
        DEFAULT_PLACETYPESEARCH = Boolean.valueOf(false);
        DEFAULT_VOICESEARCH = Boolean.valueOf(false);
        DEFAULT_LOCALMUSICBROWSER = Boolean.valueOf(false);
        DEFAULT_NAVCOORDSLOOKUP = Boolean.valueOf(false);
        DEFAULT_SEARCHRESULTLIST = Boolean.valueOf(false);
        DEFAULT_MUSICARTWORKCACHE = Boolean.valueOf(false);
        DEFAULT_VOICESEARCHNEWIOSPAUSEBEHAVIORS = Boolean.valueOf(false);
        DEFAULT_CANNEDRESPONSETOSMS = Boolean.valueOf(false);
        DEFAULT_CUSTOMDIALLONGPRESS = Boolean.valueOf(false);
        DEFAULT_SUPPORTSPHONETICTTS = Boolean.valueOf(false);
    }
    
    private Capabilities(com.navdy.service.library.events.Capabilities$Builder a) {
        this(a.compactUi, a.placeTypeSearch, a.voiceSearch, a.localMusicBrowser, a.navCoordsLookup, a.searchResultList, a.musicArtworkCache, a.voiceSearchNewIOSPauseBehaviors, a.cannedResponseToSms, a.customDialLongPress, a.supportsPhoneticTts);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    Capabilities(com.navdy.service.library.events.Capabilities$Builder a, com.navdy.service.library.events.Capabilities$1 a0) {
        this(a);
    }
    
    public Capabilities(Boolean a, Boolean a0, Boolean a1, Boolean a2, Boolean a3, Boolean a4, Boolean a5, Boolean a6, Boolean a7, Boolean a8, Boolean a9) {
        this.compactUi = a;
        this.placeTypeSearch = a0;
        this.voiceSearch = a1;
        this.localMusicBrowser = a2;
        this.navCoordsLookup = a3;
        this.searchResultList = a4;
        this.musicArtworkCache = a5;
        this.voiceSearchNewIOSPauseBehaviors = a6;
        this.cannedResponseToSms = a7;
        this.customDialLongPress = a8;
        this.supportsPhoneticTts = a9;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.Capabilities) {
                com.navdy.service.library.events.Capabilities a0 = (com.navdy.service.library.events.Capabilities)a;
                boolean b0 = this.equals(this.compactUi, a0.compactUi);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.placeTypeSearch, a0.placeTypeSearch)) {
                        break label1;
                    }
                    if (!this.equals(this.voiceSearch, a0.voiceSearch)) {
                        break label1;
                    }
                    if (!this.equals(this.localMusicBrowser, a0.localMusicBrowser)) {
                        break label1;
                    }
                    if (!this.equals(this.navCoordsLookup, a0.navCoordsLookup)) {
                        break label1;
                    }
                    if (!this.equals(this.searchResultList, a0.searchResultList)) {
                        break label1;
                    }
                    if (!this.equals(this.musicArtworkCache, a0.musicArtworkCache)) {
                        break label1;
                    }
                    if (!this.equals(this.voiceSearchNewIOSPauseBehaviors, a0.voiceSearchNewIOSPauseBehaviors)) {
                        break label1;
                    }
                    if (!this.equals(this.cannedResponseToSms, a0.cannedResponseToSms)) {
                        break label1;
                    }
                    if (!this.equals(this.customDialLongPress, a0.customDialLongPress)) {
                        break label1;
                    }
                    if (this.equals(this.supportsPhoneticTts, a0.supportsPhoneticTts)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((((((((this.compactUi == null) ? 0 : this.compactUi.hashCode()) * 37 + ((this.placeTypeSearch == null) ? 0 : this.placeTypeSearch.hashCode())) * 37 + ((this.voiceSearch == null) ? 0 : this.voiceSearch.hashCode())) * 37 + ((this.localMusicBrowser == null) ? 0 : this.localMusicBrowser.hashCode())) * 37 + ((this.navCoordsLookup == null) ? 0 : this.navCoordsLookup.hashCode())) * 37 + ((this.searchResultList == null) ? 0 : this.searchResultList.hashCode())) * 37 + ((this.musicArtworkCache == null) ? 0 : this.musicArtworkCache.hashCode())) * 37 + ((this.voiceSearchNewIOSPauseBehaviors == null) ? 0 : this.voiceSearchNewIOSPauseBehaviors.hashCode())) * 37 + ((this.cannedResponseToSms == null) ? 0 : this.cannedResponseToSms.hashCode())) * 37 + ((this.customDialLongPress == null) ? 0 : this.customDialLongPress.hashCode())) * 37 + ((this.supportsPhoneticTts == null) ? 0 : this.supportsPhoneticTts.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
