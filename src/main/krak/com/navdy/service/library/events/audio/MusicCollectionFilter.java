package com.navdy.service.library.events.audio;

final public class MusicCollectionFilter extends com.squareup.wire.Message {
    final public static String DEFAULT_FIELD = "";
    final public static String DEFAULT_VALUE = "";
    final private static long serialVersionUID = 0L;
    final public String field;
    final public String value;
    
    private MusicCollectionFilter(com.navdy.service.library.events.audio.MusicCollectionFilter$Builder a) {
        this(a.field, a.value);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    MusicCollectionFilter(com.navdy.service.library.events.audio.MusicCollectionFilter$Builder a, com.navdy.service.library.events.audio.MusicCollectionFilter$1 a0) {
        this(a);
    }
    
    public MusicCollectionFilter(String s, String s0) {
        this.field = s;
        this.value = s0;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.audio.MusicCollectionFilter) {
                com.navdy.service.library.events.audio.MusicCollectionFilter a0 = (com.navdy.service.library.events.audio.MusicCollectionFilter)a;
                boolean b0 = this.equals(this.field, a0.field);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.value, a0.value)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.field == null) ? 0 : this.field.hashCode()) * 37 + ((this.value == null) ? 0 : this.value.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
