package com.navdy.service.library.events.input;

final public class KeyEvent extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.input.KeyEvent[] $VALUES;
    final public static com.navdy.service.library.events.input.KeyEvent KEY_DOWN;
    final public static com.navdy.service.library.events.input.KeyEvent KEY_UP;
    final private int value;
    
    static {
        KEY_DOWN = new com.navdy.service.library.events.input.KeyEvent("KEY_DOWN", 0, 1);
        KEY_UP = new com.navdy.service.library.events.input.KeyEvent("KEY_UP", 1, 2);
        com.navdy.service.library.events.input.KeyEvent[] a = new com.navdy.service.library.events.input.KeyEvent[2];
        a[0] = KEY_DOWN;
        a[1] = KEY_UP;
        $VALUES = a;
    }
    
    private KeyEvent(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.input.KeyEvent valueOf(String s) {
        return (com.navdy.service.library.events.input.KeyEvent)Enum.valueOf(com.navdy.service.library.events.input.KeyEvent.class, s);
    }
    
    public static com.navdy.service.library.events.input.KeyEvent[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
