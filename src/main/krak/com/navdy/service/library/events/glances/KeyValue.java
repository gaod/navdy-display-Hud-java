package com.navdy.service.library.events.glances;

final public class KeyValue extends com.squareup.wire.Message {
    final public static String DEFAULT_KEY = "";
    final public static String DEFAULT_VALUE = "";
    final private static long serialVersionUID = 0L;
    final public String key;
    final public String value;
    
    private KeyValue(com.navdy.service.library.events.glances.KeyValue$Builder a) {
        this(a.key, a.value);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    KeyValue(com.navdy.service.library.events.glances.KeyValue$Builder a, com.navdy.service.library.events.glances.KeyValue$1 a0) {
        this(a);
    }
    
    public KeyValue(String s, String s0) {
        this.key = s;
        this.value = s0;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.glances.KeyValue) {
                com.navdy.service.library.events.glances.KeyValue a0 = (com.navdy.service.library.events.glances.KeyValue)a;
                boolean b0 = this.equals(this.key, a0.key);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.value, a0.value)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.key == null) ? 0 : this.key.hashCode()) * 37 + ((this.value == null) ? 0 : this.value.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
