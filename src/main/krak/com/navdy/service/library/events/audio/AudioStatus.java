package com.navdy.service.library.events.audio;

final public class AudioStatus extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.audio.AudioStatus$ConnectionType DEFAULT_CONNECTIONTYPE;
    final public static Boolean DEFAULT_HASSCOCONNECTION;
    final public static Boolean DEFAULT_ISCONNECTED;
    final public static Boolean DEFAULT_ISPLAYING;
    final public static com.navdy.service.library.events.audio.AudioStatus$ProfileType DEFAULT_PROFILETYPE;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.audio.AudioStatus$ConnectionType connectionType;
    final public Boolean hasSCOConnection;
    final public Boolean isConnected;
    final public Boolean isPlaying;
    final public com.navdy.service.library.events.audio.AudioStatus$ProfileType profileType;
    
    static {
        DEFAULT_ISCONNECTED = Boolean.valueOf(false);
        DEFAULT_CONNECTIONTYPE = com.navdy.service.library.events.audio.AudioStatus$ConnectionType.AUDIO_CONNECTION_NONE;
        DEFAULT_PROFILETYPE = com.navdy.service.library.events.audio.AudioStatus$ProfileType.AUDIO_PROFILE_NONE;
        DEFAULT_ISPLAYING = Boolean.valueOf(false);
        DEFAULT_HASSCOCONNECTION = Boolean.valueOf(false);
    }
    
    private AudioStatus(com.navdy.service.library.events.audio.AudioStatus$Builder a) {
        this(a.isConnected, a.connectionType, a.profileType, a.isPlaying, a.hasSCOConnection);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    AudioStatus(com.navdy.service.library.events.audio.AudioStatus$Builder a, com.navdy.service.library.events.audio.AudioStatus$1 a0) {
        this(a);
    }
    
    public AudioStatus(Boolean a, com.navdy.service.library.events.audio.AudioStatus$ConnectionType a0, com.navdy.service.library.events.audio.AudioStatus$ProfileType a1, Boolean a2, Boolean a3) {
        this.isConnected = a;
        this.connectionType = a0;
        this.profileType = a1;
        this.isPlaying = a2;
        this.hasSCOConnection = a3;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.audio.AudioStatus) {
                com.navdy.service.library.events.audio.AudioStatus a0 = (com.navdy.service.library.events.audio.AudioStatus)a;
                boolean b0 = this.equals(this.isConnected, a0.isConnected);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.connectionType, a0.connectionType)) {
                        break label1;
                    }
                    if (!this.equals(this.profileType, a0.profileType)) {
                        break label1;
                    }
                    if (!this.equals(this.isPlaying, a0.isPlaying)) {
                        break label1;
                    }
                    if (this.equals(this.hasSCOConnection, a0.hasSCOConnection)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((this.isConnected == null) ? 0 : this.isConnected.hashCode()) * 37 + ((this.connectionType == null) ? 0 : this.connectionType.hashCode())) * 37 + ((this.profileType == null) ? 0 : this.profileType.hashCode())) * 37 + ((this.isPlaying == null) ? 0 : this.isPlaying.hashCode())) * 37 + ((this.hasSCOConnection == null) ? 0 : this.hasSCOConnection.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
