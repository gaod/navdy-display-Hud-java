package com.navdy.service.library.events.places;

final public class PlacesSearchRequest$Builder extends com.squareup.wire.Message.Builder {
    public Integer maxResults;
    public String requestId;
    public Integer searchArea;
    public String searchQuery;
    
    public PlacesSearchRequest$Builder() {
    }
    
    public PlacesSearchRequest$Builder(com.navdy.service.library.events.places.PlacesSearchRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.searchQuery = a.searchQuery;
            this.searchArea = a.searchArea;
            this.maxResults = a.maxResults;
            this.requestId = a.requestId;
        }
    }
    
    public com.navdy.service.library.events.places.PlacesSearchRequest build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.places.PlacesSearchRequest(this, (com.navdy.service.library.events.places.PlacesSearchRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.places.PlacesSearchRequest$Builder maxResults(Integer a) {
        this.maxResults = a;
        return this;
    }
    
    public com.navdy.service.library.events.places.PlacesSearchRequest$Builder requestId(String s) {
        this.requestId = s;
        return this;
    }
    
    public com.navdy.service.library.events.places.PlacesSearchRequest$Builder searchArea(Integer a) {
        this.searchArea = a;
        return this;
    }
    
    public com.navdy.service.library.events.places.PlacesSearchRequest$Builder searchQuery(String s) {
        this.searchQuery = s;
        return this;
    }
}
