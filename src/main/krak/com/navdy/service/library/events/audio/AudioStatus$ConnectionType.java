package com.navdy.service.library.events.audio;

final public class AudioStatus$ConnectionType extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.audio.AudioStatus$ConnectionType[] $VALUES;
    final public static com.navdy.service.library.events.audio.AudioStatus$ConnectionType AUDIO_CONNECTION_AUX;
    final public static com.navdy.service.library.events.audio.AudioStatus$ConnectionType AUDIO_CONNECTION_BLUETOOTH;
    final public static com.navdy.service.library.events.audio.AudioStatus$ConnectionType AUDIO_CONNECTION_NONE;
    final public static com.navdy.service.library.events.audio.AudioStatus$ConnectionType AUDIO_CONNECTION_SPEAKER;
    final private int value;
    
    static {
        AUDIO_CONNECTION_NONE = new com.navdy.service.library.events.audio.AudioStatus$ConnectionType("AUDIO_CONNECTION_NONE", 0, 1);
        AUDIO_CONNECTION_BLUETOOTH = new com.navdy.service.library.events.audio.AudioStatus$ConnectionType("AUDIO_CONNECTION_BLUETOOTH", 1, 2);
        AUDIO_CONNECTION_SPEAKER = new com.navdy.service.library.events.audio.AudioStatus$ConnectionType("AUDIO_CONNECTION_SPEAKER", 2, 3);
        AUDIO_CONNECTION_AUX = new com.navdy.service.library.events.audio.AudioStatus$ConnectionType("AUDIO_CONNECTION_AUX", 3, 4);
        com.navdy.service.library.events.audio.AudioStatus$ConnectionType[] a = new com.navdy.service.library.events.audio.AudioStatus$ConnectionType[4];
        a[0] = AUDIO_CONNECTION_NONE;
        a[1] = AUDIO_CONNECTION_BLUETOOTH;
        a[2] = AUDIO_CONNECTION_SPEAKER;
        a[3] = AUDIO_CONNECTION_AUX;
        $VALUES = a;
    }
    
    private AudioStatus$ConnectionType(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.audio.AudioStatus$ConnectionType valueOf(String s) {
        return (com.navdy.service.library.events.audio.AudioStatus$ConnectionType)Enum.valueOf(com.navdy.service.library.events.audio.AudioStatus$ConnectionType.class, s);
    }
    
    public static com.navdy.service.library.events.audio.AudioStatus$ConnectionType[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
