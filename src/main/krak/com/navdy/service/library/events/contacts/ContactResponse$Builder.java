package com.navdy.service.library.events.contacts;

final public class ContactResponse$Builder extends com.squareup.wire.Message.Builder {
    public java.util.List contacts;
    public String identifier;
    public com.navdy.service.library.events.RequestStatus status;
    public String statusDetail;
    
    public ContactResponse$Builder() {
    }
    
    public ContactResponse$Builder(com.navdy.service.library.events.contacts.ContactResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
            this.statusDetail = a.statusDetail;
            this.identifier = a.identifier;
            this.contacts = com.navdy.service.library.events.contacts.ContactResponse.access$000(a.contacts);
        }
    }
    
    public com.navdy.service.library.events.contacts.ContactResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.contacts.ContactResponse(this, (com.navdy.service.library.events.contacts.ContactResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.contacts.ContactResponse$Builder contacts(java.util.List a) {
        this.contacts = com.navdy.service.library.events.contacts.ContactResponse$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.contacts.ContactResponse$Builder identifier(String s) {
        this.identifier = s;
        return this;
    }
    
    public com.navdy.service.library.events.contacts.ContactResponse$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
    
    public com.navdy.service.library.events.contacts.ContactResponse$Builder statusDetail(String s) {
        this.statusDetail = s;
        return this;
    }
}
