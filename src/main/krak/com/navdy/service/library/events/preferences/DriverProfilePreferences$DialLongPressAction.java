package com.navdy.service.library.events.preferences;

final public class DriverProfilePreferences$DialLongPressAction extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction[] $VALUES;
    final public static com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction DIAL_LONG_PRESS_PLACE_SEARCH;
    final public static com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction DIAL_LONG_PRESS_VOICE_ASSISTANT;
    final private int value;
    
    static {
        DIAL_LONG_PRESS_VOICE_ASSISTANT = new com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction("DIAL_LONG_PRESS_VOICE_ASSISTANT", 0, 0);
        DIAL_LONG_PRESS_PLACE_SEARCH = new com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction("DIAL_LONG_PRESS_PLACE_SEARCH", 1, 1);
        com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction[] a = new com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction[2];
        a[0] = DIAL_LONG_PRESS_VOICE_ASSISTANT;
        a[1] = DIAL_LONG_PRESS_PLACE_SEARCH;
        $VALUES = a;
    }
    
    private DriverProfilePreferences$DialLongPressAction(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction valueOf(String s) {
        return (com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction)Enum.valueOf(com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction.class, s);
    }
    
    public static com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
