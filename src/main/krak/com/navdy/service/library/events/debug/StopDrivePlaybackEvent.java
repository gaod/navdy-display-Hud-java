package com.navdy.service.library.events.debug;

final public class StopDrivePlaybackEvent extends com.squareup.wire.Message {
    final private static long serialVersionUID = 0L;
    
    public StopDrivePlaybackEvent() {
    }
    
    private StopDrivePlaybackEvent(com.navdy.service.library.events.debug.StopDrivePlaybackEvent$Builder a) {
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    StopDrivePlaybackEvent(com.navdy.service.library.events.debug.StopDrivePlaybackEvent$Builder a, com.navdy.service.library.events.debug.StopDrivePlaybackEvent$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        return a instanceof com.navdy.service.library.events.debug.StopDrivePlaybackEvent;
    }
    
    public int hashCode() {
        return 0;
    }
}
