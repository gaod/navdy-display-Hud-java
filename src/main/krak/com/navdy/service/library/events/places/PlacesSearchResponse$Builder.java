package com.navdy.service.library.events.places;

final public class PlacesSearchResponse$Builder extends com.squareup.wire.Message.Builder {
    public String requestId;
    public java.util.List results;
    public String searchQuery;
    public com.navdy.service.library.events.RequestStatus status;
    public String statusDetail;
    
    public PlacesSearchResponse$Builder() {
    }
    
    public PlacesSearchResponse$Builder(com.navdy.service.library.events.places.PlacesSearchResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.searchQuery = a.searchQuery;
            this.status = a.status;
            this.statusDetail = a.statusDetail;
            this.results = com.navdy.service.library.events.places.PlacesSearchResponse.access$000(a.results);
            this.requestId = a.requestId;
        }
    }
    
    public com.navdy.service.library.events.places.PlacesSearchResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.places.PlacesSearchResponse(this, (com.navdy.service.library.events.places.PlacesSearchResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.places.PlacesSearchResponse$Builder requestId(String s) {
        this.requestId = s;
        return this;
    }
    
    public com.navdy.service.library.events.places.PlacesSearchResponse$Builder results(java.util.List a) {
        this.results = com.navdy.service.library.events.places.PlacesSearchResponse$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.places.PlacesSearchResponse$Builder searchQuery(String s) {
        this.searchQuery = s;
        return this;
    }
    
    public com.navdy.service.library.events.places.PlacesSearchResponse$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
    
    public com.navdy.service.library.events.places.PlacesSearchResponse$Builder statusDetail(String s) {
        this.statusDetail = s;
        return this;
    }
}
