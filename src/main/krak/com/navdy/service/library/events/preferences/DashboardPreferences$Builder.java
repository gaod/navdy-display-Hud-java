package com.navdy.service.library.events.preferences;

final public class DashboardPreferences$Builder extends com.squareup.wire.Message.Builder {
    public String leftGaugeId;
    public com.navdy.service.library.events.preferences.MiddleGauge middleGauge;
    public String rightGaugeId;
    public com.navdy.service.library.events.preferences.ScrollableSide scrollableSide;
    
    public DashboardPreferences$Builder() {
    }
    
    public DashboardPreferences$Builder(com.navdy.service.library.events.preferences.DashboardPreferences a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.middleGauge = a.middleGauge;
            this.scrollableSide = a.scrollableSide;
            this.rightGaugeId = a.rightGaugeId;
            this.leftGaugeId = a.leftGaugeId;
        }
    }
    
    public com.navdy.service.library.events.preferences.DashboardPreferences build() {
        return new com.navdy.service.library.events.preferences.DashboardPreferences(this, (com.navdy.service.library.events.preferences.DashboardPreferences$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.preferences.DashboardPreferences$Builder leftGaugeId(String s) {
        this.leftGaugeId = s;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DashboardPreferences$Builder middleGauge(com.navdy.service.library.events.preferences.MiddleGauge a) {
        this.middleGauge = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DashboardPreferences$Builder rightGaugeId(String s) {
        this.rightGaugeId = s;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DashboardPreferences$Builder scrollableSide(com.navdy.service.library.events.preferences.ScrollableSide a) {
        this.scrollableSide = a;
        return this;
    }
}
