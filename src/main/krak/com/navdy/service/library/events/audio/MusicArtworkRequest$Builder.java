package com.navdy.service.library.events.audio;

final public class MusicArtworkRequest$Builder extends com.squareup.wire.Message.Builder {
    public String album;
    public String author;
    public String collectionId;
    public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    public com.navdy.service.library.events.audio.MusicCollectionType collectionType;
    public String name;
    public Integer size;
    
    public MusicArtworkRequest$Builder() {
    }
    
    public MusicArtworkRequest$Builder(com.navdy.service.library.events.audio.MusicArtworkRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.collectionSource = a.collectionSource;
            this.name = a.name;
            this.album = a.album;
            this.author = a.author;
            this.size = a.size;
            this.collectionType = a.collectionType;
            this.collectionId = a.collectionId;
        }
    }
    
    public com.navdy.service.library.events.audio.MusicArtworkRequest$Builder album(String s) {
        this.album = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicArtworkRequest$Builder author(String s) {
        this.author = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicArtworkRequest build() {
        return new com.navdy.service.library.events.audio.MusicArtworkRequest(this, (com.navdy.service.library.events.audio.MusicArtworkRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.MusicArtworkRequest$Builder collectionId(String s) {
        this.collectionId = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicArtworkRequest$Builder collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource a) {
        this.collectionSource = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicArtworkRequest$Builder collectionType(com.navdy.service.library.events.audio.MusicCollectionType a) {
        this.collectionType = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicArtworkRequest$Builder name(String s) {
        this.name = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicArtworkRequest$Builder size(Integer a) {
        this.size = a;
        return this;
    }
}
