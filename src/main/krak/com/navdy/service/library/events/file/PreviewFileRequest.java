package com.navdy.service.library.events.file;

final public class PreviewFileRequest extends com.squareup.wire.Message {
    final public static String DEFAULT_FILENAME = "";
    final private static long serialVersionUID = 0L;
    final public String filename;
    
    private PreviewFileRequest(com.navdy.service.library.events.file.PreviewFileRequest$Builder a) {
        this(a.filename);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    PreviewFileRequest(com.navdy.service.library.events.file.PreviewFileRequest$Builder a, com.navdy.service.library.events.file.PreviewFileRequest$1 a0) {
        this(a);
    }
    
    public PreviewFileRequest(String s) {
        this.filename = s;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.file.PreviewFileRequest && this.equals(this.filename, ((com.navdy.service.library.events.file.PreviewFileRequest)a).filename);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.filename == null) ? 0 : this.filename.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
