package com.navdy.service.library.events.dial;

final public class DialStatusResponse$Builder extends com.squareup.wire.Message.Builder {
    public Integer batteryLevel;
    public Boolean isConnected;
    public Boolean isPaired;
    public String macAddress;
    public String name;
    
    public DialStatusResponse$Builder() {
    }
    
    public DialStatusResponse$Builder(com.navdy.service.library.events.dial.DialStatusResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.isPaired = a.isPaired;
            this.isConnected = a.isConnected;
            this.name = a.name;
            this.macAddress = a.macAddress;
            this.batteryLevel = a.batteryLevel;
        }
    }
    
    public com.navdy.service.library.events.dial.DialStatusResponse$Builder batteryLevel(Integer a) {
        this.batteryLevel = a;
        return this;
    }
    
    public com.navdy.service.library.events.dial.DialStatusResponse build() {
        return new com.navdy.service.library.events.dial.DialStatusResponse(this, (com.navdy.service.library.events.dial.DialStatusResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.dial.DialStatusResponse$Builder isConnected(Boolean a) {
        this.isConnected = a;
        return this;
    }
    
    public com.navdy.service.library.events.dial.DialStatusResponse$Builder isPaired(Boolean a) {
        this.isPaired = a;
        return this;
    }
    
    public com.navdy.service.library.events.dial.DialStatusResponse$Builder macAddress(String s) {
        this.macAddress = s;
        return this;
    }
    
    public com.navdy.service.library.events.dial.DialStatusResponse$Builder name(String s) {
        this.name = s;
        return this;
    }
}
