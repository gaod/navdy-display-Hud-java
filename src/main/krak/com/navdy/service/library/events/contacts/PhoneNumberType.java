package com.navdy.service.library.events.contacts;

final public class PhoneNumberType extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.contacts.PhoneNumberType[] $VALUES;
    final public static com.navdy.service.library.events.contacts.PhoneNumberType PHONE_NUMBER_HOME;
    final public static com.navdy.service.library.events.contacts.PhoneNumberType PHONE_NUMBER_MOBILE;
    final public static com.navdy.service.library.events.contacts.PhoneNumberType PHONE_NUMBER_OTHER;
    final public static com.navdy.service.library.events.contacts.PhoneNumberType PHONE_NUMBER_WORK;
    final private int value;
    
    static {
        PHONE_NUMBER_HOME = new com.navdy.service.library.events.contacts.PhoneNumberType("PHONE_NUMBER_HOME", 0, 1);
        PHONE_NUMBER_WORK = new com.navdy.service.library.events.contacts.PhoneNumberType("PHONE_NUMBER_WORK", 1, 2);
        PHONE_NUMBER_MOBILE = new com.navdy.service.library.events.contacts.PhoneNumberType("PHONE_NUMBER_MOBILE", 2, 3);
        PHONE_NUMBER_OTHER = new com.navdy.service.library.events.contacts.PhoneNumberType("PHONE_NUMBER_OTHER", 3, 4);
        com.navdy.service.library.events.contacts.PhoneNumberType[] a = new com.navdy.service.library.events.contacts.PhoneNumberType[4];
        a[0] = PHONE_NUMBER_HOME;
        a[1] = PHONE_NUMBER_WORK;
        a[2] = PHONE_NUMBER_MOBILE;
        a[3] = PHONE_NUMBER_OTHER;
        $VALUES = a;
    }
    
    private PhoneNumberType(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.contacts.PhoneNumberType valueOf(String s) {
        return (com.navdy.service.library.events.contacts.PhoneNumberType)Enum.valueOf(com.navdy.service.library.events.contacts.PhoneNumberType.class, s);
    }
    
    public static com.navdy.service.library.events.contacts.PhoneNumberType[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
