package com.navdy.service.library.events.preferences;

final public class DriverProfilePreferences$DisplayFormat extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat[] $VALUES;
    final public static com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat DISPLAY_FORMAT_COMPACT;
    final public static com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat DISPLAY_FORMAT_NORMAL;
    final private int value;
    
    static {
        DISPLAY_FORMAT_NORMAL = new com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat("DISPLAY_FORMAT_NORMAL", 0, 0);
        DISPLAY_FORMAT_COMPACT = new com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat("DISPLAY_FORMAT_COMPACT", 1, 1);
        com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat[] a = new com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat[2];
        a[0] = DISPLAY_FORMAT_NORMAL;
        a[1] = DISPLAY_FORMAT_COMPACT;
        $VALUES = a;
    }
    
    private DriverProfilePreferences$DisplayFormat(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat valueOf(String s) {
        return (com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat)Enum.valueOf(com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat.class, s);
    }
    
    public static com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
