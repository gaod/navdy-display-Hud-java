package com.navdy.service.library.events.input;

final public class LaunchAppEvent$Builder extends com.squareup.wire.Message.Builder {
    public String appBundleID;
    
    public LaunchAppEvent$Builder() {
    }
    
    public LaunchAppEvent$Builder(com.navdy.service.library.events.input.LaunchAppEvent a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.appBundleID = a.appBundleID;
        }
    }
    
    public com.navdy.service.library.events.input.LaunchAppEvent$Builder appBundleID(String s) {
        this.appBundleID = s;
        return this;
    }
    
    public com.navdy.service.library.events.input.LaunchAppEvent build() {
        return new com.navdy.service.library.events.input.LaunchAppEvent(this, (com.navdy.service.library.events.input.LaunchAppEvent$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
}
