package com.navdy.service.library.events.photo;

final public class PhotoUpdate$Builder extends com.squareup.wire.Message.Builder {
    public String identifier;
    public okio.ByteString photo;
    public com.navdy.service.library.events.photo.PhotoType photoType;
    
    public PhotoUpdate$Builder() {
    }
    
    public PhotoUpdate$Builder(com.navdy.service.library.events.photo.PhotoUpdate a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.identifier = a.identifier;
            this.photo = a.photo;
            this.photoType = a.photoType;
        }
    }
    
    public com.navdy.service.library.events.photo.PhotoUpdate build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.photo.PhotoUpdate(this, (com.navdy.service.library.events.photo.PhotoUpdate$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.photo.PhotoUpdate$Builder identifier(String s) {
        this.identifier = s;
        return this;
    }
    
    public com.navdy.service.library.events.photo.PhotoUpdate$Builder photo(okio.ByteString a) {
        this.photo = a;
        return this;
    }
    
    public com.navdy.service.library.events.photo.PhotoUpdate$Builder photoType(com.navdy.service.library.events.photo.PhotoType a) {
        this.photoType = a;
        return this;
    }
}
