package com.navdy.service.library.events.debug;

final public class DriveRecordingsResponse extends com.squareup.wire.Message {
    final public static java.util.List DEFAULT_RECORDINGS;
    final private static long serialVersionUID = 0L;
    final public java.util.List recordings;
    
    static {
        DEFAULT_RECORDINGS = java.util.Collections.emptyList();
    }
    
    private DriveRecordingsResponse(com.navdy.service.library.events.debug.DriveRecordingsResponse$Builder a) {
        this(a.recordings);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    DriveRecordingsResponse(com.navdy.service.library.events.debug.DriveRecordingsResponse$Builder a, com.navdy.service.library.events.debug.DriveRecordingsResponse$1 a0) {
        this(a);
    }
    
    public DriveRecordingsResponse(java.util.List a) {
        this.recordings = com.navdy.service.library.events.debug.DriveRecordingsResponse.immutableCopyOf(a);
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.debug.DriveRecordingsResponse.copyOf(a);
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.debug.DriveRecordingsResponse && this.equals(this.recordings, ((com.navdy.service.library.events.debug.DriveRecordingsResponse)a).recordings);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.recordings == null) ? 1 : this.recordings.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
