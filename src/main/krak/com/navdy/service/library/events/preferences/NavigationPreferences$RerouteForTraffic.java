package com.navdy.service.library.events.preferences;

final public class NavigationPreferences$RerouteForTraffic extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic[] $VALUES;
    final public static com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic REROUTE_AUTOMATIC;
    final public static com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic REROUTE_CONFIRM;
    final public static com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic REROUTE_NEVER;
    final private int value;
    
    static {
        REROUTE_CONFIRM = new com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic("REROUTE_CONFIRM", 0, 0);
        REROUTE_AUTOMATIC = new com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic("REROUTE_AUTOMATIC", 1, 1);
        REROUTE_NEVER = new com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic("REROUTE_NEVER", 2, 2);
        com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic[] a = new com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic[3];
        a[0] = REROUTE_CONFIRM;
        a[1] = REROUTE_AUTOMATIC;
        a[2] = REROUTE_NEVER;
        $VALUES = a;
    }
    
    private NavigationPreferences$RerouteForTraffic(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic valueOf(String s) {
        return (com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic)Enum.valueOf(com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic.class, s);
    }
    
    public static com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
