package com.navdy.service.library.events;

final public class Ping$Builder extends com.squareup.wire.Message.Builder {
    public Ping$Builder() {
    }
    
    public Ping$Builder(com.navdy.service.library.events.Ping a) {
        super((com.squareup.wire.Message)a);
    }
    
    public com.navdy.service.library.events.Ping build() {
        return new com.navdy.service.library.events.Ping(this, (com.navdy.service.library.events.Ping$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
}
