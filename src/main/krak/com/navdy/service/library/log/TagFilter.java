package com.navdy.service.library.log;

public class TagFilter implements com.navdy.service.library.log.Filter {
    private boolean invert;
    private java.util.regex.Pattern pattern;
    
    public TagFilter(String s) {
        this(s, 0, false);
    }
    
    public TagFilter(String s, int i, boolean b) {
        this.invert = false;
        this.pattern = java.util.regex.Pattern.compile(s, i);
        this.invert = b;
    }
    
    public TagFilter(java.util.regex.Pattern a, boolean b) {
        this.invert = false;
        this.pattern = a;
        this.invert = b;
    }
    
    public static com.navdy.service.library.log.TagFilter block(String[] a) {
        return new com.navdy.service.library.log.TagFilter(com.navdy.service.library.log.TagFilter.startsWithAny(a), 0, true);
    }
    
    public static com.navdy.service.library.log.TagFilter pass(String[] a) {
        return new com.navdy.service.library.log.TagFilter(com.navdy.service.library.log.TagFilter.startsWithAny(a));
    }
    
    public static String startsWithAny(String[] a) {
        StringBuilder a0 = new StringBuilder("^(?:");
        a0.append(android.text.TextUtils.join((CharSequence)"|", (Object[])a));
        a0.append(")");
        return a0.toString();
    }
    
    public boolean matches(int i, String s, String s0) {
        boolean b = this.pattern.matcher((CharSequence)s).find();
        if (this.invert) {
            b = !b;
        }
        return b;
    }
}
