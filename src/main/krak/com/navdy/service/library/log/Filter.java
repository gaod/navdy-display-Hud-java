package com.navdy.service.library.log;

abstract public interface Filter {
    abstract public boolean matches(int arg, String arg0, String arg1);
}
