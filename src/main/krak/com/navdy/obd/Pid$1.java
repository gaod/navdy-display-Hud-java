package com.navdy.obd;

final class Pid$1 implements android.os.Parcelable$Creator {
    Pid$1() {
    }
    
    public com.navdy.obd.Pid createFromParcel(android.os.Parcel a) {
        return new com.navdy.obd.Pid(a);
    }
    
    public Object createFromParcel(android.os.Parcel a) {
        return this.createFromParcel(a);
    }
    
    public com.navdy.obd.Pid[] newArray(int i) {
        return new com.navdy.obd.Pid[i];
    }
    
    public Object[] newArray(int i) {
        return this.newArray(i);
    }
}
