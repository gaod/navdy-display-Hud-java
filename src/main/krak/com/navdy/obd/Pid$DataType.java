package com.navdy.obd;


    public enum Pid$DataType {
        INT(0),
        FLOAT(1),
        PERCENTAGE(2),
        BOOL(3);

        private int value;
        Pid$DataType(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class Pid$DataType extends Enum {
//    final private static com.navdy.obd.Pid$DataType[] $VALUES;
//    final public static com.navdy.obd.Pid$DataType BOOL;
//    final public static com.navdy.obd.Pid$DataType FLOAT;
//    final public static com.navdy.obd.Pid$DataType INT;
//    final public static com.navdy.obd.Pid$DataType PERCENTAGE;
//    
//    static {
//        INT = new com.navdy.obd.Pid$DataType("INT", 0);
//        FLOAT = new com.navdy.obd.Pid$DataType("FLOAT", 1);
//        PERCENTAGE = new com.navdy.obd.Pid$DataType("PERCENTAGE", 2);
//        BOOL = new com.navdy.obd.Pid$DataType("BOOL", 3);
//        com.navdy.obd.Pid$DataType[] a = new com.navdy.obd.Pid$DataType[4];
//        a[0] = INT;
//        a[1] = FLOAT;
//        a[2] = PERCENTAGE;
//        a[3] = BOOL;
//        $VALUES = a;
//    }
//    
//    private Pid$DataType(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.obd.Pid$DataType valueOf(String s) {
//        return (com.navdy.obd.Pid$DataType)Enum.valueOf(com.navdy.obd.Pid$DataType.class, s);
//    }
//    
//    public static com.navdy.obd.Pid$DataType[] values() {
//        return $VALUES.clone();
//    }
//}
//