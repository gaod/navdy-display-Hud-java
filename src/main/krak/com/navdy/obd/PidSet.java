package com.navdy.obd;

public class PidSet implements android.os.Parcelable {
    final public static android.os.Parcelable$Creator CREATOR;
    final public static int MAX_PID = 320;
    private java.util.BitSet pids;
    
    static {
        CREATOR = (android.os.Parcelable$Creator)new com.navdy.obd.PidSet$1();
    }
    
    public PidSet() {
        this.pids = new java.util.BitSet(320);
    }
    
    public PidSet(long j, int i) {
        this();
        int i0 = 0;
        while(i0 < 32) {
            if ((j & (long)(1 << 31 - i0)) != 0L) {
                this.pids.set(i + i0 + 1);
            }
            i0 = i0 + 1;
        }
    }
    
    public PidSet(android.os.Parcel a) {
        int i = a.readByte();
        long[] a0 = new long[i];
        a.readLongArray(a0);
        this.pids = java.util.BitSet.valueOf(a0);
    }
    
    public PidSet(com.navdy.obd.ScanSchedule a) {
        this();
        Object a0 = a.schedule.entrySet().iterator();
        while(((java.util.Iterator)a0).hasNext()) {
            Object a1 = ((java.util.Iterator)a0).next();
            this.pids.set(((com.navdy.obd.ScanSchedule$Scan)((java.util.Map.Entry)a1).getValue()).pid);
        }
    }
    
    public PidSet(java.util.List a) {
        this();
        Object a0 = a.iterator();
        while(((java.util.Iterator)a0).hasNext()) {
            com.navdy.obd.Pid a1 = (com.navdy.obd.Pid)((java.util.Iterator)a0).next();
            this.pids.set(a1.getId());
        }
    }
    
    public void add(int i) {
        this.pids.set(i);
    }
    
    public void add(com.navdy.obd.Pid a) {
        this.add(a.getId());
    }
    
    public java.util.List asList() {
        java.util.ArrayList a = new java.util.ArrayList(this.pids.cardinality());
        int i = 0;
        while(true) {
            if (i < 320 && i != -1) {
                i = this.pids.nextSetBit(i);
                if (i == -1) {
                    continue;
                }
                ((java.util.List)a).add(new com.navdy.obd.Pid(i));
                i = i + 1;
                continue;
            }
            return (java.util.List)a;
        }
    }
    
    public void clear() {
        this.pids.clear();
    }
    
    public boolean contains(int i) {
        return this.pids.get(i);
    }
    
    public boolean contains(com.navdy.obd.Pid a) {
        return this.pids.get(a.getId());
    }
    
    public int describeContents() {
        return 0;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        boolean b0 = a instanceof com.navdy.obd.PidSet;
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.pids.equals(((com.navdy.obd.PidSet)a).pids)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public boolean isEmpty() {
        return this.pids.isEmpty();
    }
    
    public void merge(com.navdy.obd.PidSet a) {
        this.pids.or(a.pids);
    }
    
    public int nextPid(int i) {
        return this.pids.nextSetBit(i);
    }
    
    public void remove(int i) {
        this.pids.clear(i);
    }
    
    public int size() {
        return this.pids.cardinality();
    }
    
    public void writeToParcel(android.os.Parcel a, int i) {
        long[] a0 = this.pids.toLongArray();
        int i0 = (byte)a0.length;
        a.writeByte((byte)i0);
        a.writeLongArray(a0);
    }
}
