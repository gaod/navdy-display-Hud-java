package com.navdy.obd;

public class VehicleInfo implements android.os.Parcelable {
    final public static android.os.Parcelable$Creator CREATOR;
    final public java.util.List ecus;
    final public boolean isCheckEngineLightOn;
    private com.navdy.obd.ECU primaryEcu;
    final public String protocol;
    final public java.util.List troubleCodes;
    final public String vin;
    
    static {
        CREATOR = (android.os.Parcelable$Creator)new com.navdy.obd.VehicleInfo$1();
    }
    
    public VehicleInfo(android.os.Parcel a) {
        java.util.ArrayList a0 = new java.util.ArrayList();
        this.protocol = a.readString();
        this.vin = a.readString();
        a.readList((java.util.List)a0, (ClassLoader)null);
        this.ecus = java.util.Collections.unmodifiableList((java.util.List)a0);
        int i = a.readByte();
        this.isCheckEngineLightOn = i != 0;
        java.util.ArrayList a1 = new java.util.ArrayList();
        a.readList((java.util.List)a1, (ClassLoader)null);
        this.troubleCodes = java.util.Collections.unmodifiableList((java.util.List)a1);
    }
    
    public VehicleInfo(String s) {
        this((String)null, (java.util.List)null, s, false, (java.util.List)null);
    }
    
    public VehicleInfo(String s, java.util.List a, String s0, boolean b, java.util.List a0) {
        this.protocol = s;
        java.util.List a1 = (a == null) ? null : java.util.Collections.unmodifiableList(a);
        this.ecus = a1;
        this.vin = s0;
        this.isCheckEngineLightOn = b;
        this.troubleCodes = a0;
    }
    
    public int describeContents() {
        return 0;
    }
    
    public java.util.List getEcus() {
        return this.ecus;
    }
    
    public com.navdy.obd.ECU getPrimaryEcu() {
        if (this.primaryEcu == null) {
            java.util.Iterator a = this.ecus.iterator();
            int i = 0;
            com.navdy.obd.ECU a0 = null;
            Object a1 = a;
            while(true) {
                boolean b = ((java.util.Iterator)a1).hasNext();
                com.navdy.obd.ECU a2 = a0;
                if (!b) {
                    break;
                }
                a0 = (com.navdy.obd.ECU)((java.util.Iterator)a1).next();
                int i0 = a0.supportedPids.size();
                if (i0 <= i) {
                    a0 = a2;
                } else if (a0.supportedPids.contains(13)) {
                    i = i0;
                } else {
                    a0 = a2;
                }
            }
            this.primaryEcu = a0;
        }
        return this.primaryEcu;
    }
    
    public void writeToParcel(android.os.Parcel a, int i) {
        a.writeString(this.protocol);
        a.writeString(this.vin);
        a.writeList(this.ecus);
        int i0 = (byte)this.isCheckEngineLightOn;
        a.writeByte((byte)i0);
        a.writeList(this.troubleCodes);
    }
}
