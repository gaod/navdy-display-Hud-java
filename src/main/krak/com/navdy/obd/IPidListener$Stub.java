package com.navdy.obd;

abstract public class IPidListener$Stub extends android.os.Binder implements com.navdy.obd.IPidListener {
    final private static String DESCRIPTOR = "com.navdy.obd.IPidListener";
    final static int TRANSACTION_onConnectionStateChange = 2;
    final static int TRANSACTION_pidsChanged = 1;
    final static int TRANSACTION_pidsRead = 3;
    
    public IPidListener$Stub() {
        this.attachInterface((android.os.IInterface)this, "com.navdy.obd.IPidListener");
    }
    
    public static com.navdy.obd.IPidListener asInterface(android.os.IBinder a) {
        Object a0 = null;
        label1: if (a != null) {
            android.os.IInterface a1 = a.queryLocalInterface("com.navdy.obd.IPidListener");
            label0: {
                if (a1 == null) {
                    break label0;
                }
                if (!(a1 instanceof com.navdy.obd.IPidListener)) {
                    break label0;
                }
                a0 = a1;
                break label1;
            }
            a0 = new com.navdy.obd.IPidListener$Stub$Proxy(a);
        } else {
            a0 = null;
        }
        return (com.navdy.obd.IPidListener)a0;
    }
    
    public android.os.IBinder asBinder() {
        return (android.os.IBinder)this;
    }
    
    public boolean onTransact(int i, android.os.Parcel a, android.os.Parcel a0, int i0) {
        boolean b = false;
        switch(i) {
            case 1598968902: {
                a0.writeString("com.navdy.obd.IPidListener");
                b = true;
                break;
            }
            case 3: {
                a.enforceInterface("com.navdy.obd.IPidListener");
                this.pidsRead((java.util.List)a.createTypedArrayList(com.navdy.obd.Pid.CREATOR), (java.util.List)a.createTypedArrayList(com.navdy.obd.Pid.CREATOR));
                a0.writeNoException();
                b = true;
                break;
            }
            case 2: {
                a.enforceInterface("com.navdy.obd.IPidListener");
                this.onConnectionStateChange(a.readInt());
                a0.writeNoException();
                b = true;
                break;
            }
            case 1: {
                a.enforceInterface("com.navdy.obd.IPidListener");
                this.pidsChanged((java.util.List)a.createTypedArrayList(com.navdy.obd.Pid.CREATOR));
                a0.writeNoException();
                b = true;
                break;
            }
            default: {
                b = super.onTransact(i, a, a0, i0);
            }
        }
        return b;
    }
}
