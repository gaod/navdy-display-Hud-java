package com.navdy.obd;

final public class ScanSchedule implements android.os.Parcelable {
    final public static android.os.Parcelable$Creator CREATOR;
    java.util.Map schedule;
    
    static {
        CREATOR = (android.os.Parcelable$Creator)new com.navdy.obd.ScanSchedule$1();
    }
    
    public ScanSchedule() {
        this.schedule = (java.util.Map)new java.util.HashMap();
    }
    
    private ScanSchedule(android.os.Parcel a) {
        this.readFromParcel(a);
    }
    
    ScanSchedule(android.os.Parcel a, com.navdy.obd.ScanSchedule$1 a0) {
        this(a);
    }
    
    public ScanSchedule(com.navdy.obd.ScanSchedule a) {
        this.schedule = (java.util.Map)new java.util.HashMap(a.schedule);
    }
    
    public void addPid(int i, int i0) {
        com.navdy.obd.ScanSchedule$Scan a = (com.navdy.obd.ScanSchedule$Scan)this.schedule.get(Integer.valueOf(i));
        if (a == null) {
            this.schedule.put(Integer.valueOf(i), new com.navdy.obd.ScanSchedule$Scan(i, i0));
        } else if (i0 < a.scanInterval) {
            a.scanInterval = i0;
        }
    }
    
    public void addPids(java.util.List a, int i) {
        Object a0 = a.iterator();
        while(((java.util.Iterator)a0).hasNext()) {
            this.addPid(((com.navdy.obd.Pid)((java.util.Iterator)a0).next()).getId(), i);
        }
    }
    
    public int describeContents() {
        return 0;
    }
    
    public java.util.List getScanList() {
        return (java.util.List)new java.util.ArrayList(this.schedule.values());
    }
    
    public boolean isEmpty() {
        return this.schedule.isEmpty();
    }
    
    public void merge(com.navdy.obd.ScanSchedule a) {
        Object a0 = a.schedule.entrySet().iterator();
        while(((java.util.Iterator)a0).hasNext()) {
            com.navdy.obd.ScanSchedule$Scan a1 = (com.navdy.obd.ScanSchedule$Scan)((java.util.Map.Entry)((java.util.Iterator)a0).next()).getValue();
            this.addPid(a1.pid, a1.scanInterval);
        }
    }
    
    public void readFromParcel(android.os.Parcel a) {
        this.schedule = (java.util.Map)new java.util.HashMap();
        int i = a.readInt();
        int i0 = 0;
        while(i0 < i) {
            this.addPid(a.readInt(), a.readInt());
            i0 = i0 + 1;
        }
    }
    
    public com.navdy.obd.ScanSchedule$Scan remove(int i) {
        return (com.navdy.obd.ScanSchedule$Scan)this.schedule.remove(Integer.valueOf(i));
    }
    
    public int size() {
        return this.schedule.size();
    }
    
    public String toString() {
        StringBuilder a = new StringBuilder("ScanSchedule{ schedule=");
        if (this.schedule == null) {
            a.append("null");
        } else {
            java.util.Iterator a0 = this.schedule.entrySet().iterator();
            boolean b = true;
            Object a1 = a0;
            while(((java.util.Iterator)a1).hasNext()) {
                com.navdy.obd.ScanSchedule$Scan a2 = (com.navdy.obd.ScanSchedule$Scan)((java.util.Map.Entry)((java.util.Iterator)a1).next()).getValue();
                if (b) {
                    b = false;
                } else {
                    a.append(",");
                }
                a.append("[").append(a2.pid).append(", ").append(a2.scanInterval).append("ms]");
            }
        }
        a.append(" }");
        return a.toString();
    }
    
    public void writeToParcel(android.os.Parcel a, int i) {
        a.writeInt(this.schedule.size());
        Object a0 = this.schedule.entrySet().iterator();
        while(((java.util.Iterator)a0).hasNext()) {
            com.navdy.obd.ScanSchedule$Scan a1 = (com.navdy.obd.ScanSchedule$Scan)((java.util.Map.Entry)((java.util.Iterator)a0).next()).getValue();
            a.writeInt(a1.pid);
            a.writeInt(a1.scanInterval);
        }
    }
}
