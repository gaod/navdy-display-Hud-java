package com.navdy.hud.device.connection;

public class EASessionSocketAdapter implements com.navdy.service.library.network.SocketAdapter {
    com.navdy.hud.mfi.EASession eaSession;
    private com.navdy.service.library.log.Logger sLogger;
    
    public EASessionSocketAdapter(com.navdy.hud.mfi.EASession a) {
        this.sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.device.connection.EASessionSocketAdapter.class);
        this.eaSession = a;
    }
    
    public void close() {
        this.sLogger.d("closing connection");
        if (this.eaSession != null) {
            this.eaSession.close();
            this.eaSession = null;
        }
    }
    
    public void connect() {
        if (!this.isConnected()) {
            throw new java.io.IOException("Can't create outbound EASession connections");
        }
        this.sLogger.d("Connecting to existing eaSession");
    }
    
    public java.io.InputStream getInputStream() {
        java.io.InputStream a = (this.eaSession == null) ? null : this.eaSession.getInputStream();
        return a;
    }
    
    public java.io.OutputStream getOutputStream() {
        java.io.OutputStream a = (this.eaSession == null) ? null : this.eaSession.getOutputStream();
        return a;
    }
    
    public com.navdy.service.library.device.NavdyDeviceId getRemoteDevice() {
        return (this.isConnected()) ? new com.navdy.service.library.device.NavdyDeviceId(com.navdy.service.library.device.NavdyDeviceId$Type.EA, this.eaSession.getMacAddress(), this.eaSession.getName()) : null;
    }
    
    public boolean isConnected() {
        return this.eaSession != null;
    }
}
