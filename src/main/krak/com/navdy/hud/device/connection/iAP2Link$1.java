package com.navdy.hud.device.connection;

class iAP2Link$1 implements com.navdy.service.library.device.link.WriterThread$EventProcessor {
    final com.navdy.hud.device.connection.iAP2Link this$0;
    
    iAP2Link$1(com.navdy.hud.device.connection.iAP2Link a) {
        super();
        this.this$0 = a;
    }
    
    public byte[] processEvent(byte[] a) {
        com.navdy.service.library.events.NavdyEvent$MessageType a0 = com.navdy.service.library.events.WireUtil.getEventType(a);
        if (a0 != null && com.navdy.hud.device.connection.iAP2Link.access$000(this.this$0).containsKey(a0)) {
            boolean b = false;
            Object a1 = com.navdy.hud.device.connection.iAP2Link.access$000(this.this$0).get(a0);
            try {
                b = ((com.navdy.hud.device.connection.iAP2Link$NavdyEventProcessor)a1).processNavdyEvent((com.navdy.service.library.events.NavdyEvent)com.navdy.hud.device.connection.iAP2Link.access$100(this.this$0).parseFrom(a, com.navdy.service.library.events.NavdyEvent.class));
            } catch(java.io.IOException a2) {
                com.navdy.hud.device.connection.iAP2Link.access$200().e("Error while parsing request", (Throwable)a2);
                b = false;
            }
            if (b) {
                a = null;
            }
        }
        return a;
    }
}
