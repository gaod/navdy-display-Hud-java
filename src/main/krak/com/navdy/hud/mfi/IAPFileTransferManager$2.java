package com.navdy.hud.mfi;

class IAPFileTransferManager$2 implements Runnable {
    final com.navdy.hud.mfi.IAPFileTransferManager this$0;
    
    IAPFileTransferManager$2(com.navdy.hud.mfi.IAPFileTransferManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        Object a = com.navdy.hud.mfi.IAPFileTransferManager.access$000(this.this$0).values().iterator();
        while(((java.util.Iterator)a).hasNext()) {
            com.navdy.hud.mfi.FileTransferSession a0 = (com.navdy.hud.mfi.FileTransferSession)((java.util.Iterator)a).next();
            if (a0 != null) {
                if (a0.isActive()) {
                    android.util.Log.d(com.navdy.hud.mfi.IAPFileTransferManager.access$100(), new StringBuilder().append("Canceling, File transfer session , ID : ").append(a0.mFileTransferIdentifier).toString());
                    a0.cancel();
                    if (com.navdy.hud.mfi.IAPFileTransferManager.access$200(this.this$0) != null) {
                        com.navdy.hud.mfi.IAPFileTransferManager.access$200(this.this$0).onFileTransferCancel(a0.mFileTransferIdentifier);
                    }
                } else {
                    android.util.Log.d(com.navdy.hud.mfi.IAPFileTransferManager.access$100(), new StringBuilder().append("File transfer session not canceled , ID : ").append(a0.mFileTransferIdentifier).append(", Not active").toString());
                }
            }
        }
    }
}
