package com.navdy.hud.mfi;


    public enum iAPProcessor$AppLaunchMethod {
        LaunchWithUserAlert(0),
    LaunchWithoutAlert(1);

        private int value;
        iAPProcessor$AppLaunchMethod(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class iAPProcessor$AppLaunchMethod extends Enum {
//    final private static com.navdy.hud.mfi.iAPProcessor$AppLaunchMethod[] $VALUES;
//    final public static com.navdy.hud.mfi.iAPProcessor$AppLaunchMethod LaunchWithUserAlert;
//    final public static com.navdy.hud.mfi.iAPProcessor$AppLaunchMethod LaunchWithoutAlert;
//    
//    static {
//        LaunchWithUserAlert = new com.navdy.hud.mfi.iAPProcessor$AppLaunchMethod("LaunchWithUserAlert", 0);
//        LaunchWithoutAlert = new com.navdy.hud.mfi.iAPProcessor$AppLaunchMethod("LaunchWithoutAlert", 1);
//        com.navdy.hud.mfi.iAPProcessor$AppLaunchMethod[] a = new com.navdy.hud.mfi.iAPProcessor$AppLaunchMethod[2];
//        a[0] = LaunchWithUserAlert;
//        a[1] = LaunchWithoutAlert;
//        $VALUES = a;
//    }
//    
//    private iAPProcessor$AppLaunchMethod(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$AppLaunchMethod valueOf(String s) {
//        return (com.navdy.hud.mfi.iAPProcessor$AppLaunchMethod)Enum.valueOf(com.navdy.hud.mfi.iAPProcessor$AppLaunchMethod.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$AppLaunchMethod[] values() {
//        return $VALUES.clone();
//    }
//}
//