package com.navdy.hud.mfi;

class iAPProcessor$1 extends android.os.Handler {
    final com.navdy.hud.mfi.iAPProcessor this$0;
    
    iAPProcessor$1(com.navdy.hud.mfi.iAPProcessor a, android.os.Looper a0) {
        super(a0);
        this.this$0 = a;
    }
    
    public void handleMessage(android.os.Message a) {
        switch(a.what) {
            case 4: {
                if (com.navdy.hud.mfi.iAPProcessor.access$300(this.this$0) == null) {
                    break;
                }
                android.util.Log.d("MFi", "Skipping Device Authentication as we did not get the certificate");
                com.navdy.hud.mfi.iAPProcessor.access$300(this.this$0).onReady();
                break;
            }
            case 3: {
                com.navdy.hud.mfi.iAPProcessor.access$200(this.this$0);
                break;
            }
            case 2: {
                com.navdy.hud.mfi.iAPProcessor.access$100(this.this$0);
                break;
            }
            case 1: {
                com.navdy.hud.mfi.SessionPacket a0 = (com.navdy.hud.mfi.SessionPacket)a.obj;
                com.navdy.hud.mfi.iAPProcessor.access$000(this.this$0, a0);
                break;
            }
        }
    }
}
