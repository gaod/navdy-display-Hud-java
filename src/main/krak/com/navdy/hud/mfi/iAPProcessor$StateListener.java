package com.navdy.hud.mfi;

abstract public interface iAPProcessor$StateListener {
    abstract public void onError();
    
    
    abstract public void onReady();
    
    
    abstract public void onSessionStart(com.navdy.hud.mfi.EASession arg);
    
    
    abstract public void onSessionStop(com.navdy.hud.mfi.EASession arg);
}
