package com.navdy.hud.mfi;


public enum IAPMusicManager$NowPlayingUpdatesParameters {
    MediaItemAttributes(0),
    PlaybackAttributes(1);

    private int value;
    IAPMusicManager$NowPlayingUpdatesParameters(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class IAPMusicManager$NowPlayingUpdatesParameters extends Enum {
//    final private static com.navdy.hud.mfi.IAPMusicManager$NowPlayingUpdatesParameters[] $VALUES;
//    final public static com.navdy.hud.mfi.IAPMusicManager$NowPlayingUpdatesParameters MediaItemAttributes;
//    final public static com.navdy.hud.mfi.IAPMusicManager$NowPlayingUpdatesParameters PlaybackAttributes;
//    
//    static {
//        MediaItemAttributes = new com.navdy.hud.mfi.IAPMusicManager$NowPlayingUpdatesParameters("MediaItemAttributes", 0);
//        PlaybackAttributes = new com.navdy.hud.mfi.IAPMusicManager$NowPlayingUpdatesParameters("PlaybackAttributes", 1);
//        com.navdy.hud.mfi.IAPMusicManager$NowPlayingUpdatesParameters[] a = new com.navdy.hud.mfi.IAPMusicManager$NowPlayingUpdatesParameters[2];
//        a[0] = MediaItemAttributes;
//        a[1] = PlaybackAttributes;
//        $VALUES = a;
//    }
//    
//    private IAPMusicManager$NowPlayingUpdatesParameters(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.IAPMusicManager$NowPlayingUpdatesParameters valueOf(String s) {
//        return (com.navdy.hud.mfi.IAPMusicManager$NowPlayingUpdatesParameters)Enum.valueOf(com.navdy.hud.mfi.IAPMusicManager$NowPlayingUpdatesParameters.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.IAPMusicManager$NowPlayingUpdatesParameters[] values() {
//        return $VALUES.clone();
//    }
//}
//