package com.navdy.hud.mfi;


    public enum iAPProcessor$BluetoothTransportComponent {
        TransportComponentIdentifier(0),
    TransportComponentName(1),
    TransportSupportsiAP2Connection(2),
    BluetoothTransportMediaAccessControlAddress(3);

        private int value;
        iAPProcessor$BluetoothTransportComponent(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class iAPProcessor$BluetoothTransportComponent extends Enum {
//    final private static com.navdy.hud.mfi.iAPProcessor$BluetoothTransportComponent[] $VALUES;
//    final public static com.navdy.hud.mfi.iAPProcessor$BluetoothTransportComponent BluetoothTransportMediaAccessControlAddress;
//    final public static com.navdy.hud.mfi.iAPProcessor$BluetoothTransportComponent TransportComponentIdentifier;
//    final public static com.navdy.hud.mfi.iAPProcessor$BluetoothTransportComponent TransportComponentName;
//    final public static com.navdy.hud.mfi.iAPProcessor$BluetoothTransportComponent TransportSupportsiAP2Connection;
//    
//    static {
//        TransportComponentIdentifier = new com.navdy.hud.mfi.iAPProcessor$BluetoothTransportComponent("TransportComponentIdentifier", 0);
//        TransportComponentName = new com.navdy.hud.mfi.iAPProcessor$BluetoothTransportComponent("TransportComponentName", 1);
//        TransportSupportsiAP2Connection = new com.navdy.hud.mfi.iAPProcessor$BluetoothTransportComponent("TransportSupportsiAP2Connection", 2);
//        BluetoothTransportMediaAccessControlAddress = new com.navdy.hud.mfi.iAPProcessor$BluetoothTransportComponent("BluetoothTransportMediaAccessControlAddress", 3);
//        com.navdy.hud.mfi.iAPProcessor$BluetoothTransportComponent[] a = new com.navdy.hud.mfi.iAPProcessor$BluetoothTransportComponent[4];
//        a[0] = TransportComponentIdentifier;
//        a[1] = TransportComponentName;
//        a[2] = TransportSupportsiAP2Connection;
//        a[3] = BluetoothTransportMediaAccessControlAddress;
//        $VALUES = a;
//    }
//    
//    private iAPProcessor$BluetoothTransportComponent(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$BluetoothTransportComponent valueOf(String s) {
//        return (com.navdy.hud.mfi.iAPProcessor$BluetoothTransportComponent)Enum.valueOf(com.navdy.hud.mfi.iAPProcessor$BluetoothTransportComponent.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$BluetoothTransportComponent[] values() {
//        return $VALUES.clone();
//    }
//}
//