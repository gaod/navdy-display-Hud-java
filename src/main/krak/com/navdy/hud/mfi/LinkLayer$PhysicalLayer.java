package com.navdy.hud.mfi;

abstract public interface LinkLayer$PhysicalLayer extends com.navdy.hud.mfi.LinkPacketReceiver {
    abstract public byte[] getLocalAddress();
}
