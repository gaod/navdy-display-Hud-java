package com.navdy.hud.mfi;

public class I2C {
    final public static int MAX_BLOCK_SIZE = 32;
    private int mBus;
    private int mFD;
    
    static {
        System.loadLibrary("iap2");
        com.navdy.hud.mfi.I2C.classInitNative();
    }
    
    public I2C(int i) {
        this.mFD = -1;
        this.mBus = i;
    }
    
    native private static void classInitNative();
    
    
    native private void native_close();
    
    
    native private int native_open(int arg);
    
    
    native private int native_read(int arg, int arg0, byte[] arg1, int arg2, int arg3);
    
    
    native private int native_write(int arg, int arg0, byte[] arg1, int arg2, int arg3);
    
    
    public void close() {
        if (this.mFD >= 0) {
            this.native_close();
        }
    }
    
    public void open() {
        this.native_open(this.mBus);
        if (this.mFD < 0) {
            throw new java.io.IOException("Failed to open i2c device");
        }
    }
    
    public int read(int i, int i0, byte[] a) {
        return this.read(i, i0, a, 0, a.length);
    }
    
    public int read(int i, int i0, byte[] a, int i1, int i2) {
        return this.native_read(i, i0, a, i1, i2);
    }
    
    public int readByte(int i, int i0) {
        byte[] a = new byte[1];
        if (this.read(i, i0, a) != 0) {
            throw new java.io.IOException("Unable to read byte");
        }
        int i1 = a[0];
        return i1 & 255;
    }
    
    public int readShort(int i, int i0) {
        byte[] a = new byte[2];
        if (this.read(i, i0, a) != 0) {
            throw new java.io.IOException("Unable to read short");
        }
        int i1 = a[0];
        int i2 = (i1 & 255) << 8;
        int i3 = a[1];
        return i2 | i3 & 255;
    }
    
    public int write(int i, int i0, byte[] a, int i1) {
        return this.native_write(i, i0, a, 0, i1);
    }
    
    public int write(int i, int i0, byte[] a, int i1, int i2) {
        return this.native_write(i, i0, a, 0, i2);
    }
    
    public int writeByte(int i, int i0, int i1) {
        byte[] a = new byte[1];
        int i2 = (byte)i1;
        int i3 = (byte)i2;
        a[0] = (byte)i3;
        return this.write(i, i0, a, a.length);
    }
    
    public int writeShort(int i, int i0, int i1) {
        byte[] a = new byte[2];
        int i2 = (byte)(i1 >> 8 & 255);
        int i3 = (byte)i2;
        a[0] = (byte)i3;
        int i4 = (byte)(i1 & 255);
        int i5 = (byte)i4;
        a[1] = (byte)i5;
        return this.write(i, i0, a, a.length);
    }
}
