package com.navdy.hud.mfi;

class LinkLayer$1 extends android.os.Handler {
    final com.navdy.hud.mfi.LinkLayer this$0;
    
    LinkLayer$1(com.navdy.hud.mfi.LinkLayer a, android.os.Looper a0) {
        super(a0);
        this.this$0 = a;
    }
    
    private void doNativeRunLoop(int i, byte[] a, int i0) {
        if (com.navdy.hud.mfi.LinkLayer.access$300(this.this$0, i, a, i0) >= 0) {
            this.transferPackets(1);
            this.transferPackets(2);
        } else {
            android.util.Log.i("LinkLayer", "runloop detected a disconnect");
            this.this$0.connectionEnded();
        }
    }
    
    private void transferPackets(int i) {
        while(true) {
            int i0 = com.navdy.hud.mfi.LinkLayer.access$400(this.this$0);
            byte[] a = com.navdy.hud.mfi.LinkLayer.access$500(this.this$0, i);
            if (a == null) {
                return;
            }
            switch(i) {
                case 2: {
                    com.navdy.hud.mfi.SessionPacket a0 = new com.navdy.hud.mfi.SessionPacket(i0, a);
                    com.navdy.hud.mfi.LinkLayer.access$800(this.this$0, new StringBuilder().append("LL->iAP[").append(a0.session).append("]").toString(), (com.navdy.hud.mfi.Packet)a0);
                    com.navdy.hud.mfi.LinkLayer.access$200(this.this$0).queue(a0);
                    break;
                }
                case 1: {
                    com.navdy.hud.mfi.LinkLayer.access$600(this.this$0, a);
                    com.navdy.hud.mfi.LinkPacket a1 = new com.navdy.hud.mfi.LinkPacket(a);
                    com.navdy.hud.mfi.LinkLayer.access$700(this.this$0).queue(a1);
                    break;
                }
                default: {
                    android.util.Log.e("LinkLayer", new StringBuilder().append("transferPackets: unsupported message type (").append(i).append(")").toString());
                }
            }
        }
    }
    
    public void handleMessage(android.os.Message a) {
        int i = 0;
        int i0 = a.what;
        int i1 = a.what;
        byte[] a0 = null;
        switch(i1) {
            case 4: {
                com.navdy.hud.mfi.LinkLayer.access$200(this.this$0).linkLost();
                a0 = null;
                i = 0;
                break;
            }
            case 1: {
                a0 = ((com.navdy.hud.mfi.Packet)a.obj).data;
                i = 0;
                break;
            }
            case 0: case 2: {
                com.navdy.hud.mfi.SessionPacket a1 = null;
                if (com.navdy.hud.mfi.LinkLayer.access$000(this.this$0).peek() == null) {
                    Object a2 = com.navdy.hud.mfi.LinkLayer.access$100(this.this$0).peek();
                    a1 = null;
                    if (a2 != null) {
                        a1 = (com.navdy.hud.mfi.SessionPacket)com.navdy.hud.mfi.LinkLayer.access$100(this.this$0).poll();
                    }
                } else {
                    a1 = (com.navdy.hud.mfi.SessionPacket)com.navdy.hud.mfi.LinkLayer.access$000(this.this$0).poll();
                }
                a0 = null;
                if (a1 == null) {
                    i = 0;
                    break;
                } else {
                    a0 = a1.data;
                    i = a1.session;
                    i0 = 2;
                    break;
                }
            }
            default: {
                a0 = null;
                i = 0;
                break;
            }
            case 3: {
                i = 0;
            }
        }
        if (a.what == 0) {
            this.sendEmptyMessageDelayed(0, 20L);
        }
        this.doNativeRunLoop(i0, a0, i);
    }
}
