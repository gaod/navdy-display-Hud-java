package com.navdy.hud.mfi;

public class iAPProcessor implements com.navdy.hud.mfi.SessionPacketReceiver, com.navdy.hud.mfi.EASessionPacketReceiver {
    final public static String CHARSET_NAME = "UTF-8";
    final public static int DEVICE_AUTHENTICATION_CERTIFICATE_WAIT_TIME = 3000;
    final public static int DEVICE_AUTHENTICATION_RETRY_INTERVAL = 5000;
    final private static int EASESSION_SESSION = 7;
    final public static int HEADER_BUFFER = 100;
    final public static String HID_COMPONENT_NAME = "navdy-media-remote";
    final public static int MAX_DEVICE_AUTHENTICATION_RETRIES = 5;
    final private static int MESSAGE_LINK_LOST = 2;
    final private static int MESSAGE_RETRY_DEVICE_AUTHENTICATION = 3;
    final private static int MESSAGE_SESSION_MESSAGE = 1;
    final private static int MESSAGE_SKIP_DEVICE_AUTHENTICATION = 4;
    final public static String NAVDY_CLIENT_BUNDLE_ID = "com.navdy.NavdyClient";
    final public static int PRODUCT_IDENTIFIER = 1;
    final public static String PROTOCOL_V1 = "com.navdy.hud.api.v1";
    final public static String PROXY_V1 = "com.navdy.hud.proxy.v1";
    final public static int STARTING_OFFSET = 6;
    final private static String TAG;
    final public static int VENDOR_IDENTIFIER = 39046;
    final static android.util.SparseArray iAPMsgById;
    final static boolean isCommunicationUpdateSupported = false;
    final static boolean isCommunicationsSupported = true;
    final static boolean isHIDKeyboardSupported = false;
    final static boolean isHIDMediaRemoteSupported = true;
    final private static boolean isHIDSupported = true;
    final static boolean isMusicSupported = true;
    final public static com.navdy.hud.mfi.iAPProcessor$MediaRemoteComponent mediaRemoteComponent;
    final private static byte[] msgStart;
    private static String[] protocols;
    com.navdy.hud.mfi.iAPProcessor$iAPMessage __someiAPMessageToForceEnumLoading__;
    String accessoryName;
    private com.navdy.hud.mfi.AuthCoprocessor authCoprocessor;
    private java.util.HashMap controlProcessors;
    private int controlSession;
    private int deviceAuthenticationRetries;
    private android.util.SparseArray eaSessionById;
    int[] hidDescriptorKeyboard;
    int[] hidDescriptorMediaRemote;
    private com.navdy.hud.mfi.IAPListener iapListener;
    private boolean isDeviceAuthenticationSupported;
    private boolean linkEstablished;
    private com.navdy.hud.mfi.LinkLayer linkLayer;
    private com.navdy.hud.mfi.IIAPFileTransferManager mFileTransferManager;
    final private android.os.Handler myHandler;
    private android.util.SparseArray reassambleMsgs;
    private com.navdy.hud.mfi.iAPProcessor$StateListener stateListener;
    
    static {
        TAG = com.navdy.hud.mfi.iAPProcessor.class.getSimpleName();
        String[] a = new String[2];
        a[0] = "com.navdy.hud.api.v1";
        a[1] = "com.navdy.hud.proxy.v1";
        protocols = a;
        mediaRemoteComponent = com.navdy.hud.mfi.iAPProcessor$MediaRemoteComponent.HIDMediaRemote;
        iAPMsgById = new android.util.SparseArray();
        byte[] a0 = new byte[2];
        a0[0] = (byte)64;
        a0[1] = (byte)64;
        msgStart = a0;
    }
    
    public iAPProcessor(com.navdy.hud.mfi.IAPListener a) {
        this(a, (android.content.Context)null);
    }
    
    public iAPProcessor(com.navdy.hud.mfi.IAPListener a, android.content.Context a0) {
        this.eaSessionById = new android.util.SparseArray();
        this.deviceAuthenticationRetries = 0;
        this.controlSession = -1;
        this.isDeviceAuthenticationSupported = true;
        int[] a1 = new int[31];
        a1[0] = 5;
        a1[1] = 12;
        a1[2] = 9;
        a1[3] = 1;
        a1[4] = 161;
        a1[5] = 1;
        a1[6] = 21;
        a1[7] = 0;
        a1[8] = 37;
        a1[9] = 1;
        a1[10] = 9;
        a1[11] = 207;
        a1[12] = 9;
        a1[13] = 205;
        a1[14] = 9;
        a1[15] = 181;
        a1[16] = 9;
        a1[17] = 182;
        a1[18] = 117;
        a1[19] = 1;
        a1[20] = 149;
        a1[21] = 4;
        a1[22] = 129;
        a1[23] = 2;
        a1[24] = 117;
        a1[25] = 4;
        a1[26] = 149;
        a1[27] = 1;
        a1[28] = 129;
        a1[29] = 3;
        a1[30] = 192;
        this.hidDescriptorMediaRemote = a1;
        int[] a2 = new int[45];
        a2[0] = 5;
        a2[1] = 12;
        a2[2] = 9;
        a2[3] = 1;
        a2[4] = 161;
        a2[5] = 1;
        a2[6] = 5;
        a2[7] = 7;
        a2[8] = 21;
        a2[9] = 0;
        a2[10] = 37;
        a2[11] = 1;
        a2[12] = 117;
        a2[13] = 8;
        a2[14] = 149;
        a2[15] = 1;
        a2[16] = 129;
        a2[17] = 3;
        a2[18] = 5;
        a2[19] = 12;
        a2[20] = 21;
        a2[21] = 0;
        a2[22] = 37;
        a2[23] = 1;
        a2[24] = 9;
        a2[25] = 64;
        a2[26] = 9;
        a2[27] = 205;
        a2[28] = 9;
        a2[29] = 181;
        a2[30] = 9;
        a2[31] = 182;
        a2[32] = 117;
        a2[33] = 1;
        a2[34] = 149;
        a2[35] = 4;
        a2[36] = 129;
        a2[37] = 2;
        a2[38] = 117;
        a2[39] = 4;
        a2[40] = 149;
        a2[41] = 1;
        a2[42] = 129;
        a2[43] = 3;
        a2[44] = 192;
        this.hidDescriptorKeyboard = a2;
        this.accessoryName = "Navdy HUD";
        this.myHandler = new com.navdy.hud.mfi.iAPProcessor$1(this, android.os.Looper.getMainLooper());
        this.__someiAPMessageToForceEnumLoading__ = com.navdy.hud.mfi.iAPProcessor$iAPMessage.RequestAuthenticationRequest;
        this.reassambleMsgs = new android.util.SparseArray();
        this.iapListener = a;
        this.authCoprocessor = new com.navdy.hud.mfi.AuthCoprocessor(a, a0);
        this.controlProcessors = new java.util.HashMap();
    }
    
    static void access$000(com.navdy.hud.mfi.iAPProcessor a, com.navdy.hud.mfi.SessionPacket a0) {
        a.handleSessionPacket(a0);
    }
    
    static void access$100(com.navdy.hud.mfi.iAPProcessor a) {
        a.handleLinkLoss();
    }
    
    static void access$200(com.navdy.hud.mfi.iAPProcessor a) {
        a.retryDeviceAuthentication();
    }
    
    static com.navdy.hud.mfi.iAPProcessor$StateListener access$300(com.navdy.hud.mfi.iAPProcessor a) {
        return a.stateListener;
    }
    
    static String access$400() {
        return TAG;
    }
    
    static byte[] access$500() {
        return msgStart;
    }
    
    static void copy(byte[] a, int i, byte[] a0) {
        System.arraycopy(a0, 0, a, i, a0.length);
    }
    
    private void handleLinkLoss() {
        this.myHandler.removeMessages(3);
        this.myHandler.removeMessages(4);
        this.linkEstablished = false;
        this.deviceAuthenticationRetries = 0;
        if (this.mFileTransferManager != null) {
            this.mFileTransferManager.clear();
        }
        int i = 0;
        while(i < this.eaSessionById.size()) {
            this.stopEASession((com.navdy.hud.mfi.EASession)this.eaSessionById.valueAt(i));
            i = i + 1;
        }
        this.eaSessionById.clear();
    }
    
    private void handleSessionMessage(int i, int i0, byte[] a) {
        com.navdy.hud.mfi.iAPProcessor$iAPMessage a0 = (com.navdy.hud.mfi.iAPProcessor$iAPMessage)iAPMsgById.get(i0);
        label0: {
            Throwable a1 = null;
            label1: {
                Throwable a2 = null;
                label2: {
                    Throwable a3 = null;
                    label3: {
                        Throwable a4 = null;
                        if (a0 != null) {
                            android.util.Log.d("MFi", new StringBuilder().append("received message ").append(a0.toString()).append(" (").append(a.length).append(" bytes) ").toString());
                            switch(com.navdy.hud.mfi.iAPProcessor$2.$SwitchMap$com$navdy$hud$mfi$iAPProcessor$iAPMessage[a0.ordinal()]) {
                                case 10: {
                                    int i1 = com.navdy.hud.mfi.iAPProcessor.parse(a).getUInt16(0);
                                    this.stopEASession((com.navdy.hud.mfi.EASession)this.eaSessionById.get(i1));
                                    this.eaSessionById.remove(i1);
                                    break label0;
                                }
                                case 9: {
                                    com.navdy.hud.mfi.iAPProcessor$IAP2Params a5 = com.navdy.hud.mfi.iAPProcessor.parse(a);
                                    int i2 = a5.getUInt8(0);
                                    int i3 = a5.getUInt16(1);
                                    android.util.Log.d("MFi", new StringBuilder().append("StartExternalAccessoryProtocolSession: ").append(i3).append(" (protoId: ").append(i2).append(")").toString());
                                    com.navdy.hud.mfi.EASession a6 = new com.navdy.hud.mfi.EASession(this, this.linkLayer.getRemoteAddress(), this.linkLayer.getRemoteName(), i3, protocols[i2]);
                                    this.eaSessionById.append(i3, a6);
                                    this.startEASession(a6);
                                    break label0;
                                }
                                case 8: {
                                    byte[] a7 = com.navdy.hud.mfi.iAPProcessor.parse(a).getBlob(0);
                                    this.authCoprocessor.open();
                                    try {
                                        boolean b = false;
                                        if (a7.length <= 128) {
                                            b = this.authCoprocessor.validateDeviceSignature(a7);
                                        } else {
                                            android.util.Log.w("MFi", "DeviceAuthenticationResponse response too big - working around iOS 10 bug");
                                            b = true;
                                        }
                                        if (!b) {
                                            android.util.Log.e("MFi", "DeviceAuthenticationResponse validation failed");
                                        }
                                        if (this.deviceAuthenticationRetries >= 5) {
                                            android.util.Log.d("MFi", "DeviceAuthenticationRetries reached the limit so sending success");
                                            b = true;
                                        }
                                        this.sendToDevice(i, (com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator)new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(b ? com.navdy.hud.mfi.iAPProcessor$iAPMessage.DeviceAuthenticationSucceeded : com.navdy.hud.mfi.iAPProcessor$iAPMessage.DeviceAuthenticationFailed));
                                        if (b) {
                                            if (this.iapListener != null) {
                                                this.iapListener.onDeviceAuthenticationSuccess(this.deviceAuthenticationRetries);
                                            }
                                        } else {
                                            this.myHandler.sendEmptyMessageDelayed(3, 5000L);
                                        }
                                        if (this.stateListener != null) {
                                            if (b) {
                                                this.stateListener.onReady();
                                            } else {
                                                this.stateListener.onError();
                                            }
                                        }
                                    } catch(Throwable a8) {
                                        a4 = a8;
                                        break;
                                    }
                                    this.authCoprocessor.close();
                                    break label0;
                                }
                                case 7: {
                                    this.myHandler.removeMessages(4);
                                    byte[] a9 = com.navdy.hud.mfi.iAPProcessor.parse(a).getBlob(0);
                                    this.authCoprocessor.open();
                                    try {
                                        byte[] a10 = this.authCoprocessor.validateDeviceCertificateAndGenerateDeviceChallenge(a9);
                                        this.sendToDevice(i, new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.RequestDeviceAuthenticationChallengeResponse).addBlob(0, a10));
                                    } catch(Throwable a11) {
                                        a3 = a11;
                                        break label3;
                                    }
                                    this.authCoprocessor.close();
                                    break label0;
                                }
                                case 6: {
                                    String s = TAG;
                                    Object[] a12 = new Object[1];
                                    a12[0] = com.navdy.hud.mfi.Utils.bytesToHex(a, true);
                                    android.util.Log.d(s, String.format("msg data: %s", a12));
                                    if (this.isDeviceAuthenticationSupported) {
                                        android.util.Log.d("MFi", "Identification rejected, trying without DeviceAuthentication");
                                        this.isDeviceAuthenticationSupported = false;
                                        this.sendIdentification(i);
                                        break label0;
                                    } else {
                                        android.util.Log.d("MFi", "Identification rejected, even with DeviceAuthentication turned off");
                                        if (this.stateListener == null) {
                                            break label0;
                                        }
                                        this.stateListener.onError();
                                        break label0;
                                    }
                                }
                                case 5: {
                                    this.deviceAuthenticationRetries = 0;
                                    if (this.isDeviceAuthenticationSupported) {
                                        android.util.Log.d("MFi", "DeviceAuthentication supported, attempting");
                                        this.sendToDevice(i, (com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator)new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.RequestDeviceAuthenticationCertificate));
                                        this.myHandler.sendEmptyMessageDelayed(4, 3000L);
                                        break label0;
                                    } else {
                                        if (this.stateListener == null) {
                                            break label0;
                                        }
                                        this.stateListener.onReady();
                                        break label0;
                                    }
                                }
                                case 4: {
                                    this.isDeviceAuthenticationSupported = true;
                                    this.sendIdentification(i);
                                    break label0;
                                }
                                case 2: {
                                    byte[] a13 = com.navdy.hud.mfi.iAPProcessor.parse(a).getBlob(0);
                                    this.authCoprocessor.open();
                                    try {
                                        byte[] a14 = this.authCoprocessor.createAccessorySignature(a13);
                                        this.sendToDevice(i, new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.AuthenticationResponse).addBlob(0, a14));
                                    } catch(Throwable a15) {
                                        a2 = a15;
                                        break label2;
                                    }
                                    this.authCoprocessor.close();
                                    break label0;
                                }
                                case 1: {
                                    this.controlSession = i;
                                    this.deviceAuthenticationRetries = 0;
                                    this.authCoprocessor.open();
                                    try {
                                        byte[] a16 = this.authCoprocessor.readAccessoryCertificate();
                                        this.sendToDevice(i, new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.AuthenticationCertificate).addBlob(0, a16));
                                    } catch(Throwable a17) {
                                        a1 = a17;
                                        break label1;
                                    }
                                    this.authCoprocessor.close();
                                    break label0;
                                }
                                default: {
                                    com.navdy.hud.mfi.IAPControlMessageProcessor a18 = (com.navdy.hud.mfi.IAPControlMessageProcessor)this.controlProcessors.get(a0);
                                    if (a18 == null) {
                                        break label0;
                                    }
                                    a18.processControlMessage(a0, i, a);
                                    break label0;
                                }
                                case 3: {
                                    break label0;
                                }
                            }
                        } else {
                            String s0 = TAG;
                            Object[] a19 = new Object[1];
                            a19[0] = Integer.valueOf(a.length);
                            android.util.Log.e(s0, String.format("received unknown message (%d bytes)", a19));
                            break label0;
                        }
                        this.authCoprocessor.close();
                        throw a4;
                    }
                    this.authCoprocessor.close();
                    throw a3;
                }
                this.authCoprocessor.close();
                throw a2;
            }
            this.authCoprocessor.close();
            throw a1;
        }
    }
    
    private void handleSessionPacket(com.navdy.hud.mfi.SessionPacket a) {
        try {
            this.linkEstablished = true;
            if (a.session != 2) {
                com.navdy.hud.mfi.iAPProcessor$SessionMessage a0 = (com.navdy.hud.mfi.iAPProcessor$SessionMessage)this.reassambleMsgs.get(a.session);
                if (a0 == null) {
                    if (this.startsWith(a.data, msgStart)) {
                        a0 = new com.navdy.hud.mfi.iAPProcessor$SessionMessage(this, com.navdy.hud.mfi.Utils.unpackUInt16(a.data, 2), com.navdy.hud.mfi.Utils.unpackUInt16(a.data, 4));
                        this.reassambleMsgs.put(a.session, a0);
                    } else {
                        int i = com.navdy.hud.mfi.Utils.unpackUInt16(a.data, 0);
                        android.util.Log.d("MFi", new StringBuilder().append("Apple Device(E):").append(i).append(" Size:").append(a.data.length).toString());
                        if (this.eaSessionById.get(i) != null) {
                            byte[] a1 = java.util.Arrays.copyOfRange(a.data, 2, a.data.length);
                            ((com.navdy.hud.mfi.EASession)this.eaSessionById.get(i)).queue(a1);
                        }
                    }
                }
                if (a0 != null) {
                    a0.append(a.data);
                    if (a0.builder.size() >= a0.msgLen) {
                        if (a0.builder.size() != a0.msgLen) {
                            this.reassambleMsgs.remove(a.session);
                            throw new RuntimeException("bad message size");
                        }
                        this.handleSessionMessage(a.session, a0.msgId, a0.builder.build());
                        this.reassambleMsgs.remove(a.session);
                    }
                }
            } else if (this.mFileTransferManager != null) {
                this.mFileTransferManager.queue(a.data);
            }
        } catch(Exception a2) {
            android.util.Log.e(TAG, "", (Throwable)a2);
            this.reassambleMsgs.remove(a.session);
            if (this.stateListener != null) {
                this.stateListener.onError();
            }
        }
    }
    
    public static com.navdy.hud.mfi.iAPProcessor$IAP2Params parse(byte[] a) {
        if (a == null) {
            throw new IllegalArgumentException();
        }
        return new com.navdy.hud.mfi.iAPProcessor$IAP2Params(a, 6);
    }
    
    private void retryDeviceAuthentication() {
        if (this.linkEstablished) {
            this.deviceAuthenticationRetries = this.deviceAuthenticationRetries + 1;
            this.sendToDevice(this.controlSession, (com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator)new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.RequestDeviceAuthenticationCertificate));
        }
    }
    
    private void sendHIDReport(int i) {
        int i0 = mediaRemoteComponent.ordinal();
        int i1 = com.navdy.hud.mfi.iAPProcessor$2.$SwitchMap$com$navdy$hud$mfi$iAPProcessor$MediaRemoteComponent[mediaRemoteComponent.ordinal()];
        int[] a = null;
        switch(i1) {
            case 2: {
                a = new int[1];
                a[0] = i;
                break;
            }
            case 1: {
                a = new int[2];
                a[0] = 0;
                a[1] = i;
                break;
            }
        }
        this.sendToDevice(this.controlSession, new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.AccessoryHIDReport).addUInt16((Enum)com.navdy.hud.mfi.iAPProcessor$AccessoryHIDReport.HIDComponentIdentifier, i0).addBlob((Enum)com.navdy.hud.mfi.iAPProcessor$AccessoryHIDReport.HIDReport, com.navdy.hud.mfi.Utils.intsToBytes(a)));
    }
    
    private void sendIdentification(int i) {
        com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage a = new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.IdentificationInformation);
        a.addString((Enum)com.navdy.hud.mfi.iAPProcessor$IdentificationInformation.Name, this.accessoryName).addString((Enum)com.navdy.hud.mfi.iAPProcessor$IdentificationInformation.ModelIdentifier, android.os.Build.MODEL).addString((Enum)com.navdy.hud.mfi.iAPProcessor$IdentificationInformation.Manufacturer, android.os.Build.MANUFACTURER).addString((Enum)com.navdy.hud.mfi.iAPProcessor$IdentificationInformation.SerialNumber, android.os.Build.SERIAL).addString((Enum)com.navdy.hud.mfi.iAPProcessor$IdentificationInformation.FirmwareVersion, android.os.Build.VERSION.RELEASE).addString((Enum)com.navdy.hud.mfi.iAPProcessor$IdentificationInformation.HardwareVersion, android.os.Build.HARDWARE);
        com.navdy.hud.mfi.ByteArrayBuilder a0 = new com.navdy.hud.mfi.ByteArrayBuilder();
        if (this.isDeviceAuthenticationSupported) {
            a0.addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.RequestDeviceAuthenticationCertificate.id).addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.RequestDeviceAuthenticationChallengeResponse.id).addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.DeviceAuthenticationSucceeded.id).addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.DeviceAuthenticationFailed.id).build();
        }
        a0.addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.StartHID.id).addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.StopHID.id).addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.AccessoryHIDReport.id);
        a0.addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.StartCallStateUpdates.id).addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.StopCallStateUpdates.id).addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.InitiateCall.id).addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.AcceptCall.id).addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.EndCall.id).addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.MuteStatusUpdate.id);
        a0.addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.StartNowPlayingUpdates.id).addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.StopNowPlayingUpdates.id);
        a0.addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.RequestAppLaunch.id);
        com.navdy.hud.mfi.ByteArrayBuilder a1 = new com.navdy.hud.mfi.ByteArrayBuilder();
        a1.addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.StartExternalAccessoryProtocolSession.id).addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.StopExternalAccessoryProtocolSession.id);
        a1.addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.DeviceHIDReport.id);
        a1.addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.CallStateUpdate.id);
        a1.addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.NowPlayingUpdate.id);
        if (this.isDeviceAuthenticationSupported) {
            a1.addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.DeviceAuthenticationCertificate.id).addInt16(com.navdy.hud.mfi.iAPProcessor$iAPMessage.DeviceAuthenticationResponse.id);
        }
        a.addBlob((Enum)com.navdy.hud.mfi.iAPProcessor$IdentificationInformation.MessagesSentByAccessory, a0.build()).addBlob((Enum)com.navdy.hud.mfi.iAPProcessor$IdentificationInformation.MessagesReceivedFromDevice, a1.build()).addEnum((Enum)com.navdy.hud.mfi.iAPProcessor$IdentificationInformation.PowerProvidingCapability, 0).addUInt16((Enum)com.navdy.hud.mfi.iAPProcessor$IdentificationInformation.MaximumCurrentDrawnFromDevice, 0).addBlob((Enum)com.navdy.hud.mfi.iAPProcessor$IdentificationInformation.SupportedExternalAccessoryProtocol, new com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator().addUInt8((Enum)com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol.ExternalAccessoryProtocolIdentifier, 0).addString((Enum)com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol.ExternalAccessoryProtocolName, protocols[0]).addEnum((Enum)com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol.ExternalAccessoryProtocolMatchAction, 1).toBytes()).addBlob((Enum)com.navdy.hud.mfi.iAPProcessor$IdentificationInformation.SupportedExternalAccessoryProtocol, new com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator().addUInt8((Enum)com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol.ExternalAccessoryProtocolIdentifier, 1).addString((Enum)com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol.ExternalAccessoryProtocolName, protocols[1]).addEnum((Enum)com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol.ExternalAccessoryProtocolMatchAction, 1).toBytes()).addString((Enum)com.navdy.hud.mfi.iAPProcessor$IdentificationInformation.CurrentLanguage, "en").addString((Enum)com.navdy.hud.mfi.iAPProcessor$IdentificationInformation.SupportedLanguage, "en").addBlob((Enum)com.navdy.hud.mfi.iAPProcessor$IdentificationInformation.BluetoothTransportComponent, new com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator().addUInt16((Enum)com.navdy.hud.mfi.iAPProcessor$BluetoothTransportComponent.TransportComponentIdentifier, 1234).addString((Enum)com.navdy.hud.mfi.iAPProcessor$BluetoothTransportComponent.TransportComponentName, "navdy-transport").addNone((Enum)com.navdy.hud.mfi.iAPProcessor$BluetoothTransportComponent.TransportSupportsiAP2Connection).addBlob((Enum)com.navdy.hud.mfi.iAPProcessor$BluetoothTransportComponent.BluetoothTransportMediaAccessControlAddress, this.linkLayer.getLocalAddress()).toBytes());
        a.addBlob((Enum)com.navdy.hud.mfi.iAPProcessor$IdentificationInformation.iAP2HIDComponent, new com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator().addUInt16((Enum)com.navdy.hud.mfi.iAPProcessor$iAP2HIDComponent.HIDComponentIdentifier, com.navdy.hud.mfi.iAPProcessor$MediaRemoteComponent.HIDMediaRemote.ordinal()).addString((Enum)com.navdy.hud.mfi.iAPProcessor$iAP2HIDComponent.HIDComponentName, "navdy-media-remote").addEnum((Enum)com.navdy.hud.mfi.iAPProcessor$iAP2HIDComponent.HIDComponentFunction, (Enum)com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction.MediaPlaybackRemote).toBytes());
        this.sendToDevice(i, (com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator)a);
    }
    
    private void startEASession(com.navdy.hud.mfi.EASession a) {
        android.util.Log.d(TAG, new StringBuilder().append("startEASession: session: ").append(a.getSessionIdentifier()).toString());
        if (this.stateListener != null) {
            this.stateListener.onSessionStart(a);
        }
    }
    
    private boolean startsWith(byte[] a, byte[] a0) {
        int i = 0;
        while(true) {
            boolean b = false;
            if (i < a.length) {
                if (i < a0.length) {
                    int i0 = a[i];
                    int i1 = a0[i];
                    if (i0 != i1) {
                        b = false;
                    } else {
                        i = i + 1;
                        continue;
                    }
                } else {
                    b = true;
                }
            } else {
                b = false;
            }
            return b;
        }
    }
    
    private void stopEASession(com.navdy.hud.mfi.EASession a) {
        android.util.Log.d(TAG, new StringBuilder().append("stopEASession: session: ").append(a.getSessionIdentifier()).toString());
        if (this.stateListener != null) {
            this.stateListener.onSessionStop(a);
        }
    }
    
    public void connect(com.navdy.hud.mfi.IIAPFileTransferManager a) {
        this.mFileTransferManager = a;
    }
    
    public void connect(com.navdy.hud.mfi.LinkLayer a) {
        this.linkLayer = a;
    }
    
    public void connect(com.navdy.hud.mfi.iAPProcessor$StateListener a) {
        this.stateListener = a;
    }
    
    public int getControlSessionId() {
        return this.controlSession;
    }
    
    public void launchApp(String s, boolean b) {
        com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage a = new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.RequestAppLaunch.id);
        a.addString((Enum)com.navdy.hud.mfi.iAPProcessor$RequestAppLaunch.AppBundleID, s);
        a.addEnum((Enum)com.navdy.hud.mfi.iAPProcessor$RequestAppLaunch.LaunchAlert, (Enum)(b ? com.navdy.hud.mfi.iAPProcessor$AppLaunchMethod.LaunchWithUserAlert : com.navdy.hud.mfi.iAPProcessor$AppLaunchMethod.LaunchWithoutAlert));
        this.sendControlMessage((com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator)a);
    }
    
    public void launchApp(boolean b) {
        this.launchApp("com.navdy.NavdyClient", b);
    }
    
    public void linkLost() {
        this.myHandler.sendEmptyMessage(2);
    }
    
    public void onKeyDown(int i) {
        this.sendHIDReport(1 << i);
    }
    
    public void onKeyDown(Enum a) {
        this.onKeyDown(a.ordinal());
    }
    
    public void onKeyUp(int i) {
        this.sendHIDReport(0);
    }
    
    public void onKeyUp(Enum a) {
        this.sendHIDReport(0);
    }
    
    public void queue(com.navdy.hud.mfi.EASessionPacket a) {
        int i = ((com.navdy.hud.mfi.EASession)this.eaSessionById.get(a.session)).getSessionIdentifier();
        int i0 = this.linkLayer.native_get_maximum_payload_size() - 100;
        if (i0 > 0) {
            com.navdy.hud.mfi.Utils.logTransfer(TAG, new StringBuilder().append("EA->iAP[").append(i).append("/").append(7).append("]").toString(), new Object[0]);
            int i1 = a.data.length;
            int i2 = 0;
            while(true) {
                int i3 = Math.min(i1, i0);
                i1 = i1 - i3;
                try {
                    android.util.Log.d("MFi", new StringBuilder().append("Accessory(E)   :").append(i).append(" Size:").append(i3).toString());
                    this.sendToDevice(7, new com.navdy.hud.mfi.ByteArrayBuilder(i3 + 2).addInt16(i).addBlob(a.data, i2, i3).build());
                    i2 = i2 + i3;
                } catch(java.io.IOException a0) {
                    android.util.Log.e(TAG, new StringBuilder().append("Error while sending to the device").append(a0).toString());
                }
                if (i1 <= 0) {
                    break;
                }
            }
        }
    }
    
    public void queue(com.navdy.hud.mfi.SessionPacket a) {
        android.os.Message a0 = this.myHandler.obtainMessage(1, a);
        this.myHandler.sendMessage(a0);
    }
    
    public void registerControlMessageProcessor(com.navdy.hud.mfi.iAPProcessor$iAPMessage a, com.navdy.hud.mfi.IAPControlMessageProcessor a0) {
        if (a0 != null) {
            this.controlProcessors.put(a, a0);
        }
    }
    
    public void sendControlMessage(com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator a) {
        this.sendToDevice(this.controlSession, a);
    }
    
    public void sendToDevice(int i, com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator a) {
        byte[] a0 = a.toBytes();
        if (a instanceof com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage) {
            com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage a1 = (com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage)a;
            com.navdy.hud.mfi.iAPProcessor$iAPMessage a2 = (com.navdy.hud.mfi.iAPProcessor$iAPMessage)iAPMsgById.get(a1.msgId);
            android.util.Log.d("MFi", new StringBuilder().append("sending message ").append(a2.toString()).toString());
            String s = TAG;
            Object[] a3 = new Object[2];
            a3[0] = a2.toString();
            a3[1] = Integer.valueOf(a0.length);
            com.navdy.hud.mfi.Utils.logTransfer(s, "sending message %s (%d bytes)", a3);
        }
        this.sendToDevice(i, a0);
    }
    
    public void sendToDevice(int i, byte[] a) {
        this.linkLayer.queue(new com.navdy.hud.mfi.SessionPacket(i, a));
    }
    
    public void setAccessoryName(String s) {
        this.accessoryName = s;
    }
    
    public void startHIDSession() {
        int i = mediaRemoteComponent.ordinal();
        int i0 = com.navdy.hud.mfi.iAPProcessor$2.$SwitchMap$com$navdy$hud$mfi$iAPProcessor$MediaRemoteComponent[mediaRemoteComponent.ordinal()];
        int[] a = null;
        switch(i0) {
            case 2: {
                a = this.hidDescriptorMediaRemote;
                break;
            }
            case 1: {
                a = this.hidDescriptorKeyboard;
                break;
            }
        }
        this.sendToDevice(this.controlSession, new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.StartHID).addUInt16((Enum)com.navdy.hud.mfi.iAPProcessor$StartHID.HIDComponentIdentifier, i).addUInt16((Enum)com.navdy.hud.mfi.iAPProcessor$StartHID.VendorIdentifier, 39046).addUInt16((Enum)com.navdy.hud.mfi.iAPProcessor$StartHID.ProductIdentifier, 1).addBlob((Enum)com.navdy.hud.mfi.iAPProcessor$StartHID.HIDReportDescriptor, com.navdy.hud.mfi.Utils.intsToBytes(a)));
    }
    
    public void stopHIDSession() {
        int i = mediaRemoteComponent.ordinal();
        this.sendToDevice(this.controlSession, new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.StopHID).addUInt16((Enum)com.navdy.hud.mfi.iAPProcessor$AccessoryHIDReport.HIDComponentIdentifier, i));
    }
}
