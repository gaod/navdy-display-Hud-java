package com.navdy.hud.mfi;


    public enum IAPCommunicationsManager$MuteStatusUpdate {
        MuteStatus(0);

        private int value;
        IAPCommunicationsManager$MuteStatusUpdate(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class IAPCommunicationsManager$MuteStatusUpdate extends Enum {
//    final private static com.navdy.hud.mfi.IAPCommunicationsManager$MuteStatusUpdate[] $VALUES;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$MuteStatusUpdate MuteStatus;
//    
//    static {
//        MuteStatus = new com.navdy.hud.mfi.IAPCommunicationsManager$MuteStatusUpdate("MuteStatus", 0);
//        com.navdy.hud.mfi.IAPCommunicationsManager$MuteStatusUpdate[] a = new com.navdy.hud.mfi.IAPCommunicationsManager$MuteStatusUpdate[1];
//        a[0] = MuteStatus;
//        $VALUES = a;
//    }
//    
//    private IAPCommunicationsManager$MuteStatusUpdate(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.IAPCommunicationsManager$MuteStatusUpdate valueOf(String s) {
//        return (com.navdy.hud.mfi.IAPCommunicationsManager$MuteStatusUpdate)Enum.valueOf(com.navdy.hud.mfi.IAPCommunicationsManager$MuteStatusUpdate.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.IAPCommunicationsManager$MuteStatusUpdate[] values() {
//        return $VALUES.clone();
//    }
//}
//