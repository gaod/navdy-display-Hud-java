package com.navdy.hud.mfi;


public enum CallStateUpdate$DisconnectReason {
    Ended(0),
    Declined(1),
    Failed(2);

    private int value;
    CallStateUpdate$DisconnectReason(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class CallStateUpdate$DisconnectReason extends Enum {
//    final private static com.navdy.hud.mfi.CallStateUpdate$DisconnectReason[] $VALUES;
//    final public static com.navdy.hud.mfi.CallStateUpdate$DisconnectReason Declined;
//    final public static com.navdy.hud.mfi.CallStateUpdate$DisconnectReason Ended;
//    final public static com.navdy.hud.mfi.CallStateUpdate$DisconnectReason Failed;
//    
//    static {
//        Ended = new com.navdy.hud.mfi.CallStateUpdate$DisconnectReason("Ended", 0);
//        Declined = new com.navdy.hud.mfi.CallStateUpdate$DisconnectReason("Declined", 1);
//        Failed = new com.navdy.hud.mfi.CallStateUpdate$DisconnectReason("Failed", 2);
//        com.navdy.hud.mfi.CallStateUpdate$DisconnectReason[] a = new com.navdy.hud.mfi.CallStateUpdate$DisconnectReason[3];
//        a[0] = Ended;
//        a[1] = Declined;
//        a[2] = Failed;
//        $VALUES = a;
//    }
//    
//    private CallStateUpdate$DisconnectReason(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.CallStateUpdate$DisconnectReason valueOf(String s) {
//        return (com.navdy.hud.mfi.CallStateUpdate$DisconnectReason)Enum.valueOf(com.navdy.hud.mfi.CallStateUpdate$DisconnectReason.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.CallStateUpdate$DisconnectReason[] values() {
//        return $VALUES.clone();
//    }
//}
//