package com.navdy.hud.mfi;


public enum IAPMusicManager$MediaItemAttributes {
    MediaItemPersistentIdentifier(0),
    MediaItemTitle(1),
    MediaItemPlaybackDurationInMilliSeconds(2),
    MediaItemAlbumTitle(3),
    MediaItemAlbumTrackNumber(4),
    MediaItemAlbumTrackCount(5),
    MediaItemArtist(6),
    MediaItemGenre(7),
    MediaItemArtworkFileTransferIdentifier(8);

    private int value;
    IAPMusicManager$MediaItemAttributes(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class IAPMusicManager$MediaItemAttributes extends Enum {
//    final private static com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes[] $VALUES;
//    final public static com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes MediaItemAlbumTitle;
//    final public static com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes MediaItemAlbumTrackCount;
//    final public static com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes MediaItemAlbumTrackNumber;
//    final public static com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes MediaItemArtist;
//    final public static com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes MediaItemArtworkFileTransferIdentifier;
//    final public static com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes MediaItemGenre;
//    final public static com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes MediaItemPersistentIdentifier;
//    final public static com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes MediaItemPlaybackDurationInMilliSeconds;
//    final public static com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes MediaItemTitle;
//    int paramIndex;
//    
//    static {
//        MediaItemPersistentIdentifier = new com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes("MediaItemPersistentIdentifier", 0, 0);
//        MediaItemTitle = new com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes("MediaItemTitle", 1, 1);
//        MediaItemPlaybackDurationInMilliSeconds = new com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes("MediaItemPlaybackDurationInMilliSeconds", 2, 4);
//        MediaItemAlbumTitle = new com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes("MediaItemAlbumTitle", 3, 6);
//        MediaItemAlbumTrackNumber = new com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes("MediaItemAlbumTrackNumber", 4, 7);
//        MediaItemAlbumTrackCount = new com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes("MediaItemAlbumTrackCount", 5, 8);
//        MediaItemArtist = new com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes("MediaItemArtist", 6, 12);
//        MediaItemGenre = new com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes("MediaItemGenre", 7, 16);
//        MediaItemArtworkFileTransferIdentifier = new com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes("MediaItemArtworkFileTransferIdentifier", 8, 26);
//        com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes[] a = new com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes[9];
//        a[0] = MediaItemPersistentIdentifier;
//        a[1] = MediaItemTitle;
//        a[2] = MediaItemPlaybackDurationInMilliSeconds;
//        a[3] = MediaItemAlbumTitle;
//        a[4] = MediaItemAlbumTrackNumber;
//        a[5] = MediaItemAlbumTrackCount;
//        a[6] = MediaItemArtist;
//        a[7] = MediaItemGenre;
//        a[8] = MediaItemArtworkFileTransferIdentifier;
//        $VALUES = a;
//    }
//    
//    private IAPMusicManager$MediaItemAttributes(String s, int i, int i0) {
//        super(s, i);
//        this.paramIndex = i0;
//    }
//    
//    public static com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes valueOf(String s) {
//        return (com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes)Enum.valueOf(com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes[] values() {
//        return $VALUES.clone();
//    }
//    
//    public int getParam() {
//        return this.paramIndex;
//    }
//}
//