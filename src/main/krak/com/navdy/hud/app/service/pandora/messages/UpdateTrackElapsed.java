package com.navdy.hud.app.service.pandora.messages;

public class UpdateTrackElapsed extends com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage {
    public short elapsed;
    public int trackToken;
    
    public UpdateTrackElapsed() {
    }
    
    protected static com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage innerBuildFromPayload(byte[] a) {
        java.nio.ByteBuffer a0 = java.nio.ByteBuffer.wrap(a);
        a0.get();
        com.navdy.hud.app.service.pandora.messages.UpdateTrackElapsed a1 = new com.navdy.hud.app.service.pandora.messages.UpdateTrackElapsed();
        a1.trackToken = a0.getInt();
        int i = a0.getShort();
        a1.elapsed = (short)i;
        return a1;
    }
    
    public String toString() {
        StringBuilder a = new StringBuilder().append("Got track's elapsed time update - seconds: ");
        int i = this.elapsed;
        return a.append(i).toString();
    }
}
