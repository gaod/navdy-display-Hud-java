package com.navdy.hud.app.service;

abstract public class S3FileUploadService extends android.app.IntentService {
    final public static String ACTION_SYNC = "SYNC";
    final private static String AWS_ACCOUNT_ID = "AWS_ACCESS_KEY_ID";
    final private static String AWS_SECRET = "AWS_SECRET_ACCESS_KEY";
    final public static int RETRY_DELAY = 10000;
    final public static int S3_CONNECTION_TIMEOUT = 15000;
    final public static int S3_UPLOAD_TIMEOUT = 300000;
    protected com.navdy.service.library.log.Logger logger;
    private com.amazonaws.services.s3.AmazonS3Client s3Client;
    private com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility transferUtility;
    
    public S3FileUploadService(String s) {
        super(s);
        this.logger = this.getLogger();
    }
    
    static com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility access$000(com.navdy.hud.app.service.S3FileUploadService a) {
        return a.transferUtility;
    }
    
    public static void populateFilesQueue(java.util.ArrayList a, com.navdy.hud.app.service.S3FileUploadService$UploadQueue a0, int i) {
        if (a != null) {
            Object a1 = a.iterator();
            while(((java.util.Iterator)a1).hasNext()) {
                java.io.File a2 = (java.io.File)((java.util.Iterator)a1).next();
                if (a2.isFile()) {
                    a0.add(new com.navdy.hud.app.service.S3FileUploadService$Request(a2, (String)null));
                    if (a0.size() == i) {
                        a0.pop();
                    }
                }
            }
        }
    }
    
    abstract public boolean canCompleteRequest(com.navdy.hud.app.service.S3FileUploadService$Request arg);
    
    
    public com.amazonaws.services.s3.AmazonS3Client createS3Client() {
        com.amazonaws.auth.BasicAWSCredentials a = new com.amazonaws.auth.BasicAWSCredentials(com.navdy.service.library.util.CredentialUtil.getCredentials(com.navdy.hud.app.HudApplication.getAppContext(), "AWS_ACCESS_KEY_ID"), com.navdy.service.library.util.CredentialUtil.getCredentials(com.navdy.hud.app.HudApplication.getAppContext(), "AWS_SECRET_ACCESS_KEY"));
        com.amazonaws.ClientConfiguration a0 = new com.amazonaws.ClientConfiguration();
        a0.setConnectionTimeout(15000);
        a0.setSocketTimeout(300000);
        return new com.amazonaws.services.s3.AmazonS3Client((com.amazonaws.auth.AWSCredentials)a, a0);
    }
    
    abstract protected String getAWSBucket();
    
    
    abstract protected com.navdy.hud.app.service.S3FileUploadService$Request getCurrentRequest();
    
    
    abstract protected java.util.concurrent.atomic.AtomicBoolean getIsUploading();
    
    
    abstract protected String getKeyPrefix(java.io.File arg);
    
    
    abstract protected com.navdy.service.library.log.Logger getLogger();
    
    
    abstract protected com.navdy.hud.app.service.S3FileUploadService$UploadQueue getUploadQueue();
    
    
    abstract protected void initialize();
    
    
    public void onCreate() {
        super.onCreate();
        if (this.s3Client == null) {
            this.s3Client = this.createS3Client();
            this.transferUtility = new com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility((com.amazonaws.services.s3.AmazonS3)this.s3Client, com.navdy.hud.app.HudApplication.getAppContext());
        }
    }
    
    public void onDestroy() {
        super.onDestroy();
    }
    
    protected void onHandleIntent(android.content.Intent a) {
        com.navdy.hud.app.service.S3FileUploadService$UploadQueue a0 = null;
        Throwable a1 = null;
        boolean b = "SYNC".equals(((a == null) ? "" : a.getAction()));
        label0: {
            label2: if (b) {
                boolean b0 = com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext());
                this.logger.d(new StringBuilder().append("Performing sync , connected to network ? : ").append(b0).toString());
                if (b0) {
                    this.initialize();
                    synchronized(this.getUploadQueue()) {
                        java.util.concurrent.atomic.AtomicBoolean a2 = null;
                        int i = a0.size();
                        label3: {
                            label4: {
                                com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver a3 = null;
                                label5: {
                                    if (i > 0) {
                                        break label5;
                                    }
                                    this.logger.d("Nothing to upload");
                                    break label4;
                                }
                                a2 = this.getIsUploading();
                                if (!a2.compareAndSet(false, true)) {
                                    break label4;
                                }
                                com.navdy.hud.app.service.S3FileUploadService$Request a4 = null;
                                while(true) {
                                    if (a4 != null) {
                                        this.logger.e(new StringBuilder().append("File to upload ").append(a4.file).append(", Does not exist anymore").toString());
                                    }
                                    if (a0.size() <= 0) {
                                        break label3;
                                    }
                                    a4 = a0.pop();
                                    if (a4.file.exists()) {
                                        break;
                                    }
                                }
                                this.setCurrentRequest(a4);
                                java.io.File a5 = a4.file;
                                String s = this.getKeyPrefix(a5);
                                String s0 = (android.text.TextUtils.isEmpty((CharSequence)s)) ? a5.getName() : new StringBuilder().append(s).append(java.io.File.separator).append(a5.getName()).toString();
                                this.logger.d(new StringBuilder().append("Trying to upload : ").append(a5.getName()).append(", Under :").append(s).append(", Key :").append(s0).toString());
                                label1: {
                                    IllegalArgumentException a6 = null;
                                    try {
                                        a3 = this.transferUtility.upload(this.getAWSBucket(), s0, a5);
                                        break label1;
                                    } catch(IllegalArgumentException a7) {
                                        a6 = a7;
                                    }
                                    a6.printStackTrace();
                                    a2.set(false);
                                    this.uploadFinished(false, a5.getPath(), a4.userTag);
                                    this.sync();
                                    /*monexit(a0)*/;
                                    break label2;
                                }
                                int i0 = a3.getId();
                                this.logger.d(new StringBuilder().append("Transfer id ").append(i0).toString());
                                a3.setTransferListener((com.amazonaws.mobileconnectors.s3.transferutility.TransferListener)new com.navdy.hud.app.service.S3FileUploadService$1(this, a2, a5, a3));
                            }
                            /*monexit(a0)*/;
                            break label2;
                        }
                        a2.set(false);
                        /*monexit(a0)*/;
                    }
                }
            }
            return;
        }
        while(true) {
            try {
                /*monexit(a0)*/;
            } catch(IllegalMonitorStateException | NullPointerException a9) {
                Throwable a10 = a9;
                a1 = a10;
                continue;
            }
            throw a1;
        }
    }
    
    abstract public void reSchedule();
    
    
    abstract protected void setCurrentRequest(com.navdy.hud.app.service.S3FileUploadService$Request arg);
    
    
    abstract public void sync();
    
    
    abstract protected void uploadFinished(boolean arg, String arg0, String arg1);
}
