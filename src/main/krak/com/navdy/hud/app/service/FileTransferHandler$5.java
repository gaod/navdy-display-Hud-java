package com.navdy.hud.app.service;

class FileTransferHandler$5 implements Runnable {
    final com.navdy.hud.app.service.FileTransferHandler this$0;
    final String val$absolutePath;
    
    FileTransferHandler$5(com.navdy.hud.app.service.FileTransferHandler a, String s) {
        super();
        this.this$0 = a;
        this.val$absolutePath = s;
    }
    
    public void run() {
        com.navdy.hud.app.service.FileTransferHandler.access$100(this.this$0).v("onFileTransferDone");
        java.io.File a = new java.io.File(this.val$absolutePath);
        boolean b = a.exists();
        label2: {
            label0: {
                label1: {
                    if (!b) {
                        break label1;
                    }
                    if (!a.isFile()) {
                        break label1;
                    }
                    if (a.canRead()) {
                        break label0;
                    }
                }
                com.navdy.hud.app.service.FileTransferHandler.access$100(this.this$0).e(new StringBuilder().append("Cannot read from downloaded OTA file ").append(a.getAbsolutePath()).toString());
                break label2;
            }
            android.content.Intent a0 = new android.content.Intent("com.navdy.hud.app.service.OTA_DOWNLOAD");
            a0.putExtra("path", a.getAbsolutePath());
            com.navdy.hud.app.HudApplication.getAppContext().sendBroadcastAsUser(a0, android.os.Process.myUserHandle());
        }
    }
}
