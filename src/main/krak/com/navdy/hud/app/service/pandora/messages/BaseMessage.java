package com.navdy.hud.app.service.pandora.messages;

abstract public class BaseMessage {
    final public static String ACCESSORY_ID = "CFDA92FC";
    final public static short API_VERSION = (short)3;
    final protected static java.nio.charset.Charset FIXED_LENGTH_STRING_ENCODING;
    final protected static int INT_BYTES_SIZE = 4;
    final public static int MAX_ARTWORK_PAYLOAD_SIZE = 9025;
    final protected static int MAX_STRING_BYTE_LENGTH = 247;
    final private static short PNDR_API_VERSION_1 = (short)1;
    final private static short PNDR_API_VERSION_3 = (short)3;
    final public static byte PNDR_EVENT_TRACK_PLAY = (byte)48;
    final protected static byte PNDR_FALSE = (byte)0;
    final public static byte PNDR_GET_TRACK_ALBUM_ART = (byte)20;
    final public static byte PNDR_GET_TRACK_INFO_EXTENDED = (byte)22;
    final private static byte PNDR_IMAGE_JPEG = (byte)1;
    final private static byte PNDR_IMAGE_NONE = (byte)0;
    final private static byte PNDR_IMAGE_PNG = (byte)2;
    final private static byte PNDR_IMAGE_RGB565 = (byte)3;
    final public static byte PNDR_RETURN_TRACK_ALBUM_ART_SEGMENT = (byte)-107;
    final public static byte PNDR_RETURN_TRACK_INFO_EXTENDED = (byte)-99;
    final private static byte PNDR_SESSION_FLAG_PAUSE_ON_START = (byte)2;
    final public static byte PNDR_SESSION_START = (byte)0;
    final public static byte PNDR_SESSION_TERMINATE = (byte)5;
    final public static byte PNDR_SET_TRACK_ELAPSED_POLLING = (byte)21;
    final public static byte PNDR_STATUS_INCOMPATIBLE_API_VERSION = (byte)3;
    final public static byte PNDR_STATUS_INSUFFICIENT_CONNECTIVITY = (byte)7;
    final public static byte PNDR_STATUS_INVALID_LOGIN = (byte)9;
    final public static byte PNDR_STATUS_LICENSING_RESTRICTIONS = (byte)8;
    final public static byte PNDR_STATUS_NO_STATIONS = (byte)5;
    final public static byte PNDR_STATUS_NO_STATION_ACTIVE = (byte)6;
    final public static byte PNDR_STATUS_PAUSED = (byte)2;
    final public static byte PNDR_STATUS_PLAYING = (byte)1;
    final public static byte PNDR_STATUS_UNKNOWN_ERROR = (byte)4;
    final public static byte PNDR_TRACK_FLAG_ALLOW_SKIP = (byte)2;
    final public static int PNDR_TRACK_NONE = 0;
    final protected static byte PNDR_TRUE = (byte)1;
    final public static byte PNDR_UPDATE_STATION_ACTIVE = (byte)-70;
    final public static byte PNDR_UPDATE_STATUS = (byte)-127;
    final public static byte PNDR_UPDATE_TRACK = (byte)-112;
    final public static byte PNDR_UPDATE_TRACK_ALBUM_ART = (byte)-106;
    final public static byte PNDR_UPDATE_TRACK_COMPLETED = (byte)-98;
    final public static byte PNDR_UPDATE_TRACK_ELAPSED = (byte)-105;
    final public static byte REQUIRED_ARTWORK_IMAGE_TYPE = (byte)1;
    final public static short REQUIRED_ARTWORK_SIZE = (short)100;
    final public static short REQUIRED_STATION_ARTWORK_SIZE = (short)0;
    final public static byte SESSION_START_FLAGS = (byte)2;
    final protected static int SHORT_BYTES_SIZE = 2;
    final protected static byte STRING_BORDER_BYTE = (byte)0;
    final protected static java.nio.charset.Charset STRING_ENCODING;
    
    static {
        STRING_ENCODING = java.nio.charset.StandardCharsets.UTF_8;
        FIXED_LENGTH_STRING_ENCODING = java.nio.charset.StandardCharsets.US_ASCII;
    }
    
    public BaseMessage() {
    }
    
    abstract public String toString();
}
