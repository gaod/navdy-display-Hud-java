package com.navdy.hud.app.service;

class ConnectionServiceProxy$2 implements android.content.ServiceConnection {
    final com.navdy.hud.app.service.ConnectionServiceProxy this$0;
    
    ConnectionServiceProxy$2(com.navdy.hud.app.service.ConnectionServiceProxy a) {
        super();
        this.this$0 = a;
    }
    
    public void onServiceConnected(android.content.ComponentName a, android.os.IBinder a0) {
        com.navdy.hud.app.service.ConnectionServiceProxy.access$000().i("Connected to IEventSource service");
        this.this$0.mEventSource = com.navdy.hud.app.IEventSource$Stub.asInterface(a0);
        try {
            this.this$0.mEventSource.addEventListener((com.navdy.hud.app.IEventListener)com.navdy.hud.app.service.ConnectionServiceProxy.access$100(this.this$0));
        } catch(android.os.RemoteException a1) {
            com.navdy.hud.app.service.ConnectionServiceProxy.access$000().e("Failed to connect event listener", (Throwable)a1);
        }
        com.navdy.hud.app.service.ConnectionServiceProxy.access$000().d("Starting connection service state machine");
        android.content.Intent a2 = new android.content.Intent(this.this$0.mContext, com.navdy.hud.app.service.HudConnectionService.class);
        this.this$0.mContext.startService(a2);
        this.this$0.bus.post(new com.navdy.hud.app.event.InitEvents$ConnectionServiceStarted());
    }
    
    public void onServiceDisconnected(android.content.ComponentName a) {
        com.navdy.hud.app.service.ConnectionServiceProxy.access$000().i("Disconnected from IEventSource service");
        com.navdy.service.library.events.connection.ConnectionStateChange a0 = new com.navdy.service.library.events.connection.ConnectionStateChange("", com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_DISCONNECTED);
        this.this$0.bus.post(a0);
        this.this$0.mEventSource = null;
    }
}
