package com.navdy.hud.app.service.pandora.messages;

public class UpdateStationActive extends com.navdy.hud.app.service.pandora.messages.BaseIncomingOneIntMessage {
    public UpdateStationActive(int i) {
        super(i);
    }
    
    protected static com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage innerBuildFromPayload(byte[] a) {
        return new com.navdy.hud.app.service.pandora.messages.UpdateStationActive(com.navdy.hud.app.service.pandora.messages.UpdateStationActive.parseIntValue(a));
    }
    
    public String toString() {
        return new StringBuilder().append("New active station with token: ").append(this.value).toString();
    }
}
