package com.navdy.hud.app.service.pandora.messages;

public class SetTrackElapsedPolling extends com.navdy.hud.app.service.pandora.messages.BaseOutgoingConstantMessage {
    final public static com.navdy.hud.app.service.pandora.messages.SetTrackElapsedPolling DISABLED;
    final public static com.navdy.hud.app.service.pandora.messages.SetTrackElapsedPolling ENABLED;
    public boolean isOn;
    
    static {
        ENABLED = new com.navdy.hud.app.service.pandora.messages.SetTrackElapsedPolling(true);
        DISABLED = new com.navdy.hud.app.service.pandora.messages.SetTrackElapsedPolling(false);
    }
    
    private SetTrackElapsedPolling(boolean b) {
        this.isOn = b;
    }
    
    public byte[] buildPayload() {
        return super.buildPayload();
    }
    
    protected java.io.ByteArrayOutputStream putThis(java.io.ByteArrayOutputStream a) {
        com.navdy.hud.app.service.pandora.messages.SetTrackElapsedPolling.putByte(a, (byte)21);
        com.navdy.hud.app.service.pandora.messages.SetTrackElapsedPolling.putBoolean(a, this.isOn);
        return a;
    }
    
    public String toString() {
        return new StringBuilder().append("Setting track elapsed polling to: ").append(this.isOn).toString();
    }
}
