package com.navdy.hud.app.service;

class HudConnectionService$6 implements com.navdy.service.library.network.SocketFactory {
    final com.navdy.hud.app.service.HudConnectionService this$0;
    
    HudConnectionService$6(com.navdy.hud.app.service.HudConnectionService a) {
        super();
        this.this$0 = a;
    }
    
    public com.navdy.service.library.network.SocketAdapter build() {
        Object a = null;
        if (com.navdy.hud.device.connection.iAP2Link.proxyEASession == null) {
            if (com.navdy.hud.app.service.HudConnectionService.access$1200(this.this$0) == null) {
                throw new java.io.IOException("can't create proxy tunnel because HUD is not connected to remote device");
            }
            a = new com.navdy.service.library.network.BTSocketFactory(com.navdy.hud.app.service.HudConnectionService.access$1300(this.this$0).getDeviceId().getBluetoothAddress(), com.navdy.service.library.device.connection.ConnectionService.NAVDY_PROXY_TUNNEL_UUID).build();
        } else {
            a = new com.navdy.hud.device.connection.EASessionSocketAdapter(com.navdy.hud.device.connection.iAP2Link.proxyEASession);
        }
        return (com.navdy.service.library.network.SocketAdapter)a;
    }
}
