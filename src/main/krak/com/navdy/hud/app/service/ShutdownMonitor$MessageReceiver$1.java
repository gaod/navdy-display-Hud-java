package com.navdy.hud.app.service;

class ShutdownMonitor$MessageReceiver$1 implements Runnable {
    final com.navdy.hud.app.service.ShutdownMonitor$MessageReceiver this$1;
    
    ShutdownMonitor$MessageReceiver$1(com.navdy.hud.app.service.ShutdownMonitor$MessageReceiver a) {
        super();
        this.this$1 = a;
    }
    
    public void run() {
        if (!com.navdy.hud.app.service.ShutdownMonitor.access$300(this.this$1.this$0) && !com.navdy.hud.app.service.ShutdownMonitor.access$400(this.this$1.this$0)) {
            this.this$1.this$0.bus.post(new com.navdy.hud.app.event.Shutdown(com.navdy.hud.app.event.Shutdown$Reason.POWER_LOSS));
        }
    }
}
