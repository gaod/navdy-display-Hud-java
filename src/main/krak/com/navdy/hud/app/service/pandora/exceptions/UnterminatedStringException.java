package com.navdy.hud.app.service.pandora.exceptions;

public class UnterminatedStringException extends Exception {
    public UnterminatedStringException() {
        super("String field wit variable length not terminated with null byte");
    }
}
