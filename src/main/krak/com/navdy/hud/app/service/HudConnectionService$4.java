package com.navdy.hud.app.service;

class HudConnectionService$4 implements com.navdy.hud.app.service.DeviceSearch$EventSink {
    final com.navdy.hud.app.service.HudConnectionService this$0;
    
    HudConnectionService$4(com.navdy.hud.app.service.HudConnectionService a) {
        super();
        this.this$0 = a;
    }
    
    public void onEvent(com.squareup.wire.Message a) {
        if (a instanceof com.navdy.service.library.events.connection.ConnectionStatus) {
            com.navdy.service.library.events.connection.ConnectionStatus a0 = (com.navdy.service.library.events.connection.ConnectionStatus)a;
            com.squareup.wire.Message a1 = a0;
            if (a0.status != com.navdy.service.library.events.connection.ConnectionStatus$Status.CONNECTION_SEARCH_FINISHED) {
                a = a1;
            } else if (com.navdy.hud.app.service.HudConnectionService.access$900(this.this$0) != com.navdy.service.library.device.connection.ConnectionService$State.SEARCHING) {
                a = a1;
            } else {
                if (com.navdy.hud.app.service.HudConnectionService.access$200(this.this$0) != null) {
                    com.navdy.hud.app.service.HudConnectionService.access$200(this.this$0).close();
                    com.navdy.hud.app.service.HudConnectionService.access$202(this.this$0, (com.navdy.hud.app.service.DeviceSearch)null);
                }
                this.this$0.setState(com.navdy.service.library.device.connection.ConnectionService$State.IDLE);
                a = a1;
            }
        }
        com.navdy.hud.app.service.HudConnectionService.access$1000(this.this$0, a);
    }
}
