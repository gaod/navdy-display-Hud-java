package com.navdy.hud.app.service.pandora.messages;

abstract public class BaseOutgoingMessage extends com.navdy.hud.app.service.pandora.messages.BaseMessage {
    public BaseOutgoingMessage() {
    }
    
    protected static java.io.ByteArrayOutputStream putBoolean(java.io.ByteArrayOutputStream a, boolean b) {
        a.write(b ? 1 : 0);
        return a;
    }
    
    protected static java.io.ByteArrayOutputStream putByte(java.io.ByteArrayOutputStream a, byte a0) {
        a.write((int)a0);
        return a;
    }
    
    protected static java.io.ByteArrayOutputStream putFixedLengthASCIIString(java.io.ByteArrayOutputStream a, int i, String s) {
        byte[] a0 = s.getBytes(FIXED_LENGTH_STRING_ENCODING);
        if (a0.length > i) {
            throw new com.navdy.hud.app.service.pandora.exceptions.StringOverflowException();
        }
        a.write(a0);
        int i0 = a0.length;
        while(i0 < i) {
            a.write(0);
            i0 = i0 + 1;
        }
        return a;
    }
    
    protected static java.io.ByteArrayOutputStream putInt(java.io.ByteArrayOutputStream a, int i) {
        a.write(java.nio.ByteBuffer.allocate(4).putInt(i).array());
        return a;
    }
    
    protected static java.io.ByteArrayOutputStream putShort(java.io.ByteArrayOutputStream a, short a0) {
        a.write(java.nio.ByteBuffer.allocate(2).putShort(a0).array());
        return a;
    }
    
    protected static java.io.ByteArrayOutputStream putString(java.io.ByteArrayOutputStream a, String s) {
        if (s.length() > 247) {
            throw new com.navdy.hud.app.service.pandora.exceptions.StringOverflowException();
        }
        if (s.indexOf(0) >= 0) {
            throw new com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException();
        }
        a.write(s.getBytes(STRING_ENCODING));
        a.write(0);
        return a;
    }
    
    public byte[] buildPayload() {
        byte[] a = null;
        java.io.ByteArrayOutputStream a0 = new java.io.ByteArrayOutputStream();
        try {
            this.putThis(a0);
            a = a0.toByteArray();
        } catch(Throwable a1) {
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
            throw a1;
        }
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
        return a;
    }
    
    protected java.io.ByteArrayOutputStream putThis(java.io.ByteArrayOutputStream a) {
        throw new com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException();
    }
}
