package com.navdy.hud.app.service.pandora.messages;

public class SessionStart extends com.navdy.hud.app.service.pandora.messages.BaseOutgoingConstantMessage {
    final private static int ACCESSORY_ID_FIELD_LENGTH = 8;
    final public static com.navdy.hud.app.service.pandora.messages.SessionStart INSTANCE;
    
    static {
        INSTANCE = new com.navdy.hud.app.service.pandora.messages.SessionStart();
    }
    
    private SessionStart() {
    }
    
    public byte[] buildPayload() {
        return super.buildPayload();
    }
    
    public java.io.ByteArrayOutputStream putThis(java.io.ByteArrayOutputStream a) {
        com.navdy.hud.app.service.pandora.messages.SessionStart.putByte(a, (byte)0);
        com.navdy.hud.app.service.pandora.messages.SessionStart.putShort(a, (short)3);
        com.navdy.hud.app.service.pandora.messages.SessionStart.putFixedLengthASCIIString(a, 8, "CFDA92FC");
        com.navdy.hud.app.service.pandora.messages.SessionStart.putShort(a, (short)100);
        com.navdy.hud.app.service.pandora.messages.SessionStart.putByte(a, (byte)1);
        com.navdy.hud.app.service.pandora.messages.SessionStart.putByte(a, (byte)2);
        com.navdy.hud.app.service.pandora.messages.SessionStart.putShort(a, (short)0);
        return a;
    }
    
    public String toString() {
        return "Session Start";
    }
}
