package com.navdy.hud.app.service;

class CustomNotificationServiceHandler$1 implements com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener {
    final com.navdy.hud.app.service.CustomNotificationServiceHandler this$0;
    final long val$t1;
    
    CustomNotificationServiceHandler$1(com.navdy.hud.app.service.CustomNotificationServiceHandler a, long j) {
        super();
        this.this$0 = a;
        this.val$t1 = j;
    }
    
    public void onCompleted(java.util.List a) {
        try {
            long j = android.os.SystemClock.elapsedRealtime();
            com.navdy.hud.app.service.CustomNotificationServiceHandler.access$000().v(new StringBuilder().append("FIND_GAS_STATION: found gas station (").append(a.size()).append(") time to find [").append(j - this.val$t1).append("]").toString());
            com.here.android.mpa.common.GeoCoordinate a0 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getRouteStartPoint();
            if (a0 != null) {
                com.navdy.hud.app.service.CustomNotificationServiceHandler.access$000().v(new StringBuilder().append("using debug start point:").append(a0).toString());
            } else {
                a0 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
            }
            if (a0 != null) {
                java.util.Iterator a1 = a.iterator();
                int i = 1;
                Object a2 = a1;
                while(((java.util.Iterator)a2).hasNext()) {
                    com.here.android.mpa.search.Place a3 = (com.here.android.mpa.search.Place)((java.util.Iterator)a2).next();
                    com.here.android.mpa.search.Location a4 = a3.getLocation();
                    com.here.android.mpa.common.GeoCoordinate a5 = com.navdy.hud.app.maps.here.HerePlacesManager.getPlaceEntry(a3);
                    String s = a4.getAddress().toString().replace((CharSequence)"<br/>", (CharSequence)", ").replace((CharSequence)"\n", (CharSequence)"");
                    com.here.android.mpa.common.GeoCoordinate a6 = a4.getCoordinate();
                    com.navdy.hud.app.service.CustomNotificationServiceHandler.access$000().v(new StringBuilder().append("FIND_GAS_STATION [").append(i).append("]").append(" name [").append(a3.getName()).append("]").append(" address [").append(s).append("]").append(" distance [").append((int)a0.distanceTo(a5)).append("] meters").append(" displayPos [").append(a6).append("]").append(" navPos [").append(a5).append("]").toString());
                    i = i + 1;
                }
            } else {
                com.navdy.hud.app.service.CustomNotificationServiceHandler.access$000().e("FIND_GAS_STATION no current position");
            }
        } catch(Throwable a7) {
            com.navdy.hud.app.service.CustomNotificationServiceHandler.access$000().e("FIND_GAS_STATION", a7);
        }
    }
    
    public void onError(com.navdy.hud.app.maps.here.HerePlacesManager$Error a) {
        long j = android.os.SystemClock.elapsedRealtime();
        com.navdy.hud.app.service.CustomNotificationServiceHandler.access$000().v(new StringBuilder().append("FIND_GAS_STATION: could not find gas station:").append(a).append(" time to error [").append(j - this.val$t1).append("]").toString());
    }
}
