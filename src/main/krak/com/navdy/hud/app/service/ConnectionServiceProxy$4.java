package com.navdy.hud.app.service;

class ConnectionServiceProxy$4 {
    final static int[] $SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType;
    
    static {
        $SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType = new int[com.navdy.service.library.events.NavdyEvent$MessageType.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType;
        com.navdy.service.library.events.NavdyEvent$MessageType a0 = com.navdy.service.library.events.NavdyEvent$MessageType.StartDriveRecordingEvent;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType[com.navdy.service.library.events.NavdyEvent$MessageType.StopDriveRecordingEvent.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
    }
}
