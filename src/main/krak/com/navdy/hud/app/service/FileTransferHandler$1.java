package com.navdy.hud.app.service;

class FileTransferHandler$1 implements Runnable {
    final com.navdy.hud.app.service.FileTransferHandler this$0;
    final com.navdy.service.library.events.file.FileTransferRequest val$request;
    
    FileTransferHandler$1(com.navdy.hud.app.service.FileTransferHandler a, com.navdy.service.library.events.file.FileTransferRequest a0) {
        super();
        this.this$0 = a;
        this.val$request = a0;
    }
    
    public void run() {
        int i = this.this$0.remoteDevice.getLinkBandwidthLevel();
        label0: {
            Throwable a = null;
            if (i > 0) {
                com.navdy.service.library.events.file.FileTransferResponse a0 = this.this$0.fileTransferManager.handleFileTransferRequest(this.val$request);
                this.this$0.remoteDevice.postEvent((com.squareup.wire.Message)a0);
                if (!a0.success.booleanValue()) {
                    break label0;
                }
                int i0 = a0.transferId.intValue();
                try {
                    if (a0.supportsAcks.booleanValue()) {
                        if (com.navdy.hud.app.service.FileTransferHandler.access$000(this.this$0, i0) == null) {
                            com.navdy.hud.app.service.FileTransferHandler.sLogger.e("Error sending the first chunk because data was null");
                        } else {
                            com.navdy.hud.app.service.FileTransferHandler.sLogger.d("Send the first chunk");
                        }
                        com.navdy.hud.app.service.FileTransferHandler.sLogger.d("Flow control enabled, waiting for the FileTransferStatus");
                        break label0;
                    } else {
                        com.navdy.service.library.events.file.FileTransferData a1 = null;
                        while(true) {
                            a1 = com.navdy.hud.app.service.FileTransferHandler.access$000(this.this$0, i0);
                            if (a1 == null) {
                                com.navdy.hud.app.service.FileTransferHandler.sLogger.e("Error sending the chunk because data was null");
                                break;
                            } else {
                                Thread.sleep(1000L);
                                if (a1 == null) {
                                    break;
                                }
                                if (a1.lastChunk.booleanValue()) {
                                    break;
                                }
                            }
                        }
                        if (a1 == null) {
                            break label0;
                        }
                        if (!a1.lastChunk.booleanValue()) {
                            break label0;
                        }
                        this.this$0.fileTransferAuthority.onFileSent(this.val$request.fileType);
                        break label0;
                    }
                } catch(Throwable a2) {
                    a = a2;
                }
            } else {
                com.navdy.hud.app.service.FileTransferHandler.sLogger.d("Link bandwidth is low");
                if (this.val$request == null) {
                    break label0;
                }
                com.navdy.hud.app.service.FileTransferHandler.sLogger.d("FileTransferRequest (pull) : rejecting file transfer request to avoid link traffic");
                com.navdy.service.library.events.file.FileTransferResponse a3 = new com.navdy.service.library.events.file.FileTransferResponse$Builder().success(Boolean.valueOf(false)).destinationFileName(this.val$request.destinationFileName).fileType(this.val$request.fileType).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_HOST_BUSY).build();
                this.this$0.remoteDevice.postEvent((com.squareup.wire.Message)a3);
                break label0;
            }
            com.navdy.hud.app.service.FileTransferHandler.sLogger.d("Exception while reading the data and sending it across", a);
        }
    }
}
