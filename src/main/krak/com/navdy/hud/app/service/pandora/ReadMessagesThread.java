package com.navdy.hud.app.service.pandora;

public class ReadMessagesThread implements Runnable {
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.navdy.hud.app.service.pandora.PandoraManager parentManager;
    
    static {
        sLogger = com.navdy.hud.app.service.pandora.PandoraManager.sLogger;
    }
    
    public ReadMessagesThread(com.navdy.hud.app.service.pandora.PandoraManager a) {
        this.parentManager = a;
    }
    
    public void run() {
        label7: {
            java.io.ByteArrayOutputStream a = null;
            java.io.InputStream a0 = null;
            Throwable a1 = null;
            label1: {
                label6: {
                    java.io.ByteArrayOutputStream a2 = null;
                    label0: {
                        {
                            android.bluetooth.BluetoothSocket a3 = null;
                            int i = 0;
                            try {
                                a = null;
                                a0 = null;
                                a3 = this.parentManager.getSocket();
                                a0 = a3.getInputStream();
                                a = null;
                                i = a0.read();
                                a = null;
                            } catch(Throwable a4) {
                                a1 = a4;
                                break label1;
                            }
                            try {
                                while(i >= 0 && a3 != null) {
                                    label5: {
                                        label3: {
                                            label2: {
                                                {
                                                    label4: {
                                                        if (a != null) {
                                                            break label4;
                                                        }
                                                        if (i == 126) {
                                                            break label3;
                                                        }
                                                        com.navdy.service.library.log.Logger a5 = sLogger;
                                                        a2 = a;
                                                        StringBuilder a6 = new StringBuilder().append("Unexpected byte received - skipping: ");
                                                        Object[] a7 = new Object[1];
                                                        a7[0] = Integer.valueOf(i);
                                                        a5.e(a6.append(String.format("%02X", a7)).toString());
                                                        break label5;
                                                    }
                                                    a2 = a;
                                                    a.write(i);
                                                    if (i == 124) {
                                                        break label2;
                                                    }
                                                }
                                                break label5;
                                            }
                                            a2 = a;
                                            this.parentManager.onFrameReceived(a.toByteArray());
                                            a = null;
                                            break label5;
                                        }
                                        a2 = a;
                                        a = new java.io.ByteArrayOutputStream();
                                        try {
                                            a.write(i);
                                        } catch(Throwable a8) {
                                            a1 = a8;
                                            break label1;
                                        }
                                    }
                                    try {
                                        i = a0.read();
                                    } catch(Throwable a9) {
                                        a1 = a9;
                                        break label1;
                                    }
                                }
                                com.navdy.service.library.log.Logger a10 = sLogger;
                                a2 = a;
                                a10.w("End of input read from the stream");
                                break label6;
                            } catch(Throwable a11) {
                                a1 = a11;
                                break label0;
                            }
                        }
                    }
                    a = a2;
                    break label1;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                this.parentManager.terminateAndClose();
                break label7;
            }
            try {
                sLogger.e(Thread.currentThread().getName(), a1);
            } catch(Throwable a12) {
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                this.parentManager.terminateAndClose();
                throw a12;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
            this.parentManager.terminateAndClose();
        }
        sLogger.v(new StringBuilder().append("exiting thread:").append(Thread.currentThread().getName()).toString());
    }
}
