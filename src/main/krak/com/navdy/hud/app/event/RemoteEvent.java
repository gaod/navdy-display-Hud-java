package com.navdy.hud.app.event;

public class RemoteEvent {
    protected com.squareup.wire.Message message;
    
    public RemoteEvent() {
    }
    
    public RemoteEvent(com.squareup.wire.Message a) {
        this.message = a;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (this != a) {
            label4: {
                label5: {
                    if (a == null) {
                        break label5;
                    }
                    if ((this).getClass() == a.getClass()) {
                        break label4;
                    }
                }
                b = false;
                break label0;
            }
            com.navdy.hud.app.event.RemoteEvent a0 = (com.navdy.hud.app.event.RemoteEvent)a;
            com.squareup.wire.Message a1 = this.message;
            label3: {
                label1: {
                    label2: {
                        if (a1 != null) {
                            break label2;
                        }
                        if (a0.message != null) {
                            break label1;
                        }
                        break label3;
                    }
                    if ((this.message).equals(a0.message)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
                break label0;
            }
            b = true;
        } else {
            b = true;
        }
        return b;
    }
    
    public com.squareup.wire.Message getMessage() {
        return this.message;
    }
    
    public int hashCode() {
        return (this.message == null) ? 0 : (this.message).hashCode();
    }
}
