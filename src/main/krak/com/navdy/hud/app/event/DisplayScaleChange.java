package com.navdy.hud.app.event;

public class DisplayScaleChange {
    final public com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat format;
    
    public DisplayScaleChange(com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat a) {
        this.format = a;
    }
}
