package com.navdy.hud.app.event;


public enum Shutdown$Reason {
    UNKNOWN(0),
    CRITICAL_VOLTAGE(1),
    LOW_VOLTAGE(2),
    HIGH_TEMPERATURE(3),
    TIMEOUT(4),
    OTA(5),
    DIAL_OTA(6),
    FORCED_UPDATE(7),
    FACTORY_RESET(8),
    POWER_LOSS(9),
    MENU(10),
    POWER_BUTTON(11),
    ENGINE_OFF(12),
    ACCELERATE_SHUTDOWN(13),
    INACTIVITY(14);

    private int value;
    Shutdown$Reason(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class Shutdown$Reason extends Enum {
//    final private static com.navdy.hud.app.event.Shutdown$Reason[] $VALUES;
//    final public static com.navdy.hud.app.event.Shutdown$Reason ACCELERATE_SHUTDOWN;
//    final public static com.navdy.hud.app.event.Shutdown$Reason CRITICAL_VOLTAGE;
//    final public static com.navdy.hud.app.event.Shutdown$Reason DIAL_OTA;
//    final public static com.navdy.hud.app.event.Shutdown$Reason ENGINE_OFF;
//    final public static com.navdy.hud.app.event.Shutdown$Reason FACTORY_RESET;
//    final public static com.navdy.hud.app.event.Shutdown$Reason FORCED_UPDATE;
//    final public static com.navdy.hud.app.event.Shutdown$Reason HIGH_TEMPERATURE;
//    final public static com.navdy.hud.app.event.Shutdown$Reason INACTIVITY;
//    final public static com.navdy.hud.app.event.Shutdown$Reason LOW_VOLTAGE;
//    final public static com.navdy.hud.app.event.Shutdown$Reason MENU;
//    final public static com.navdy.hud.app.event.Shutdown$Reason OTA;
//    final public static com.navdy.hud.app.event.Shutdown$Reason POWER_BUTTON;
//    final public static com.navdy.hud.app.event.Shutdown$Reason POWER_LOSS;
//    final public static com.navdy.hud.app.event.Shutdown$Reason TIMEOUT;
//    final public static com.navdy.hud.app.event.Shutdown$Reason UNKNOWN;
//    final public String attr;
//    final public boolean requireConfirmation;
//    
//    static {
//        UNKNOWN = new com.navdy.hud.app.event.Shutdown$Reason("UNKNOWN", 0, "Unknown", false);
//        CRITICAL_VOLTAGE = new com.navdy.hud.app.event.Shutdown$Reason("CRITICAL_VOLTAGE", 1, "Critical_Voltage", false);
//        LOW_VOLTAGE = new com.navdy.hud.app.event.Shutdown$Reason("LOW_VOLTAGE", 2, "Low_Voltage", false);
//        HIGH_TEMPERATURE = new com.navdy.hud.app.event.Shutdown$Reason("HIGH_TEMPERATURE", 3, "High_Temperature", false);
//        TIMEOUT = new com.navdy.hud.app.event.Shutdown$Reason("TIMEOUT", 4, "Timeout", false);
//        OTA = new com.navdy.hud.app.event.Shutdown$Reason("OTA", 5, "OTA", false);
//        DIAL_OTA = new com.navdy.hud.app.event.Shutdown$Reason("DIAL_OTA", 6, "Dial_OTA", false);
//        FORCED_UPDATE = new com.navdy.hud.app.event.Shutdown$Reason("FORCED_UPDATE", 7, "Forced_Update", false);
//        FACTORY_RESET = new com.navdy.hud.app.event.Shutdown$Reason("FACTORY_RESET", 8, "Factory_Reset", false);
//        POWER_LOSS = new com.navdy.hud.app.event.Shutdown$Reason("POWER_LOSS", 9, "Power_Loss", false);
//        MENU = new com.navdy.hud.app.event.Shutdown$Reason("MENU", 10, "Menu", true);
//        POWER_BUTTON = new com.navdy.hud.app.event.Shutdown$Reason("POWER_BUTTON", 11, "Power_Button", true);
//        ENGINE_OFF = new com.navdy.hud.app.event.Shutdown$Reason("ENGINE_OFF", 12, "Engine_Off", true);
//        ACCELERATE_SHUTDOWN = new com.navdy.hud.app.event.Shutdown$Reason("ACCELERATE_SHUTDOWN", 13, "Accelerate_Shutdown", true);
//        INACTIVITY = new com.navdy.hud.app.event.Shutdown$Reason("INACTIVITY", 14, "Inactivity", true);
//        com.navdy.hud.app.event.Shutdown$Reason[] a = new com.navdy.hud.app.event.Shutdown$Reason[15];
//        a[0] = UNKNOWN;
//        a[1] = CRITICAL_VOLTAGE;
//        a[2] = LOW_VOLTAGE;
//        a[3] = HIGH_TEMPERATURE;
//        a[4] = TIMEOUT;
//        a[5] = OTA;
//        a[6] = DIAL_OTA;
//        a[7] = FORCED_UPDATE;
//        a[8] = FACTORY_RESET;
//        a[9] = POWER_LOSS;
//        a[10] = MENU;
//        a[11] = POWER_BUTTON;
//        a[12] = ENGINE_OFF;
//        a[13] = ACCELERATE_SHUTDOWN;
//        a[14] = INACTIVITY;
//        $VALUES = a;
//    }
//    
//    private Shutdown$Reason(String s, int i, String s0, boolean b) {
//        super(s, i);
//        this.attr = s0;
//        this.requireConfirmation = b;
//    }
//    
//    public static com.navdy.hud.app.event.Shutdown$Reason valueOf(String s) {
//        return (com.navdy.hud.app.event.Shutdown$Reason)Enum.valueOf(com.navdy.hud.app.event.Shutdown$Reason.class, s);
//    }
//    
//    public static com.navdy.hud.app.event.Shutdown$Reason[] values() {
//        return $VALUES.clone();
//    }
//    
//    public android.os.Bundle asBundle() {
//        android.os.Bundle a = new android.os.Bundle();
//        a.putString("SHUTDOWN_CAUSE", this.toString());
//        return a;
//    }
//}
//