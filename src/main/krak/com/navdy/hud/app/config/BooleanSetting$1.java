package com.navdy.hud.app.config;

class BooleanSetting$1 {
    final static int[] $SwitchMap$com$navdy$hud$app$config$BooleanSetting$Scope;
    
    static {
        $SwitchMap$com$navdy$hud$app$config$BooleanSetting$Scope = new int[com.navdy.hud.app.config.BooleanSetting$Scope.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$config$BooleanSetting$Scope;
        com.navdy.hud.app.config.BooleanSetting$Scope a0 = com.navdy.hud.app.config.BooleanSetting$Scope.CUSTOM;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$config$BooleanSetting$Scope[com.navdy.hud.app.config.BooleanSetting$Scope.NEVER.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$config$BooleanSetting$Scope[com.navdy.hud.app.config.BooleanSetting$Scope.ENG.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$config$BooleanSetting$Scope[com.navdy.hud.app.config.BooleanSetting$Scope.BETA.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$config$BooleanSetting$Scope[com.navdy.hud.app.config.BooleanSetting$Scope.ALWAYS.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
    }
}
