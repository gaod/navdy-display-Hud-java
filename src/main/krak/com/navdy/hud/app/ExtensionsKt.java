package com.navdy.hud.app;

final public class ExtensionsKt {
    final public static String cacheKey(com.navdy.service.library.events.audio.MusicCollectionRequest a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "$receiver");
        com.navdy.service.library.events.audio.MusicCollectionRequest a0 = (com.navdy.service.library.events.audio.MusicCollectionRequest)com.navdy.service.library.events.MessageStore.removeNulls((com.squareup.wire.Message)a);
        return new StringBuilder().append(a0.collectionSource).append("#").append(a0.collectionType).append("#").append(a0.collectionId).append("#").append(a0.offset).toString();
    }
    
    final public static double celsiusToFahrenheit(double d) {
        return (double)9 * d / (double)5 + (double)32;
    }
    
    final public static double clamp(double d, double d0, double d1) {
        if (d < d0) {
            d = d0;
        } else if (d > d1) {
            d = d1;
        }
        return d;
    }
    
    final public static float clamp(float f, float f0, float f1) {
        if (f < f0) {
            f = f0;
        } else if (f > f1) {
            f = f1;
        }
        return f;
    }
}
