package com.navdy.hud.app.common;

final public class ProdModule$$ModuleAdapter$ProvideBusProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    final private com.navdy.hud.app.common.ProdModule module;
    
    public ProdModule$$ModuleAdapter$ProvideBusProvidesAdapter(com.navdy.hud.app.common.ProdModule a) {
        super("com.squareup.otto.Bus", true, "com.navdy.hud.app.common.ProdModule", "provideBus");
        this.module = a;
        this.setLibrary(true);
    }
    
    public com.squareup.otto.Bus get() {
        return this.module.provideBus();
    }
    
    public Object get() {
        return this.get();
    }
}
