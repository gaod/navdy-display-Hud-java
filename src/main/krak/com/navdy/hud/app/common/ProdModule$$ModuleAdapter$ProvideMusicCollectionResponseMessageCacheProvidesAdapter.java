package com.navdy.hud.app.common;

final public class ProdModule$$ModuleAdapter$ProvideMusicCollectionResponseMessageCacheProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    final private com.navdy.hud.app.common.ProdModule module;
    private dagger.internal.Binding pathManager;
    
    public ProdModule$$ModuleAdapter$ProvideMusicCollectionResponseMessageCacheProvidesAdapter(com.navdy.hud.app.common.ProdModule a) {
        super("com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>", true, "com.navdy.hud.app.common.ProdModule", "provideMusicCollectionResponseMessageCache");
        this.module = a;
        this.setLibrary(true);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.pathManager = a.requestBinding("com.navdy.hud.app.storage.PathManager", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.storage.cache.MessageCache get() {
        return this.module.provideMusicCollectionResponseMessageCache((com.navdy.hud.app.storage.PathManager)this.pathManager.get());
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a.add(this.pathManager);
    }
}
