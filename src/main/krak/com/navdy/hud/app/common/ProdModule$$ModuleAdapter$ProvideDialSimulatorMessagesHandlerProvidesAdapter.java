package com.navdy.hud.app.common;

final public class ProdModule$$ModuleAdapter$ProvideDialSimulatorMessagesHandlerProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    final private com.navdy.hud.app.common.ProdModule module;
    
    public ProdModule$$ModuleAdapter$ProvideDialSimulatorMessagesHandlerProvidesAdapter(com.navdy.hud.app.common.ProdModule a) {
        super("com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler", true, "com.navdy.hud.app.common.ProdModule", "provideDialSimulatorMessagesHandler");
        this.module = a;
        this.setLibrary(true);
    }
    
    public com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler get() {
        return this.module.provideDialSimulatorMessagesHandler();
    }
    
    public Object get() {
        return this.get();
    }
}
