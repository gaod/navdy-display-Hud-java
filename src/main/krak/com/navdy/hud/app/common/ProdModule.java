package com.navdy.hud.app.common;
import com.navdy.hud.app.R;

public class ProdModule {
    final public static int MUSIC_DATA_CACHE_SIZE = 2000000;
    final public static String MUSIC_RESPONSE_CACHE = "MUSIC_RESPONSE_CACHE";
    final public static String PREF_NAME = "App";
    private android.content.Context context;
    
    public ProdModule(android.content.Context a) {
        this.context = a;
    }
    
    com.navdy.hud.app.ancs.AncsServiceConnector provideAncsServiceConnector(com.squareup.otto.Bus a) {
        return new com.navdy.hud.app.ancs.AncsServiceConnector(this.context, a);
    }
    
    com.navdy.hud.app.util.MusicArtworkCache provideArtworkCache() {
        return new com.navdy.hud.app.util.MusicArtworkCache();
    }
    
    com.squareup.otto.Bus provideBus() {
        return new com.navdy.hud.app.common.MainThreadBus();
    }
    
    com.navdy.hud.app.framework.phonecall.CallManager provideCallManager(com.squareup.otto.Bus a) {
        return new com.navdy.hud.app.framework.phonecall.CallManager(a, this.context);
    }
    
    com.navdy.hud.app.service.ConnectionHandler provideConnectionHandler(com.navdy.hud.app.service.ConnectionServiceProxy a, com.navdy.hud.app.device.PowerManager a0, com.navdy.hud.app.profile.DriverProfileManager a1, com.navdy.hud.app.ui.framework.UIStateManager a2, com.navdy.hud.app.common.TimeHelper a3) {
        return new com.navdy.hud.app.service.ConnectionHandler(this.context, a, a0, a1, a2, a3);
    }
    
    com.navdy.hud.app.service.ConnectionServiceProxy provideConnectionServiceProxy(com.squareup.otto.Bus a) {
        return new com.navdy.hud.app.service.ConnectionServiceProxy(this.context, a);
    }
    
    com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler provideDialSimulatorMessagesHandler() {
        return new com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler(this.context);
    }
    
    com.navdy.hud.app.profile.DriverProfileManager provideDriverProfileManager(com.squareup.otto.Bus a, com.navdy.hud.app.storage.PathManager a0, com.navdy.hud.app.common.TimeHelper a1) {
        return new com.navdy.hud.app.profile.DriverProfileManager(a, a0, a1);
    }
    
    com.navdy.hud.app.util.FeatureUtil provideFeatureUtil(android.content.SharedPreferences a) {
        return new com.navdy.hud.app.util.FeatureUtil(a);
    }
    
    com.navdy.hud.app.gesture.GestureServiceConnector provideGestureServiceConnector(com.squareup.otto.Bus a, com.navdy.hud.app.device.PowerManager a0) {
        return new com.navdy.hud.app.gesture.GestureServiceConnector(a, a0);
    }
    
    com.navdy.service.library.network.http.IHttpManager provideHttpManager() {
        return (com.navdy.service.library.network.http.IHttpManager)new com.navdy.service.library.network.http.HttpManager();
    }
    
    com.navdy.hud.app.manager.InputManager provideInputManager(com.squareup.otto.Bus a, com.navdy.hud.app.device.PowerManager a0, com.navdy.hud.app.ui.framework.UIStateManager a1) {
        return new com.navdy.hud.app.manager.InputManager(a, a0, a1);
    }
    
    com.navdy.hud.app.storage.cache.MessageCache provideMusicCollectionResponseMessageCache(com.navdy.hud.app.storage.PathManager a) {
        return new com.navdy.hud.app.storage.cache.MessageCache((com.navdy.hud.app.storage.cache.Cache)new com.navdy.hud.app.storage.cache.DiskLruCacheAdapter("MUSIC_RESPONSE_CACHE", a.getMusicDiskCacheFolder(), 2000000), com.navdy.service.library.events.audio.MusicCollectionResponse.class);
    }
    
    com.navdy.hud.app.manager.MusicManager provideMusicManager(com.navdy.hud.app.storage.cache.MessageCache a, com.squareup.otto.Bus a0, com.navdy.hud.app.ui.framework.UIStateManager a1, com.navdy.hud.app.service.pandora.PandoraManager a2, com.navdy.hud.app.util.MusicArtworkCache a3) {
        return new com.navdy.hud.app.manager.MusicManager(a, a0, this.context.getResources(), a1, a2, a3);
    }
    
    com.navdy.hud.app.debug.DriveRecorder provideObdDataReceorder(com.squareup.otto.Bus a, com.navdy.hud.app.service.ConnectionHandler a0) {
        return new com.navdy.hud.app.debug.DriveRecorder(a, a0);
    }
    
    com.navdy.hud.app.manager.PairingManager providePairingManager() {
        return new com.navdy.hud.app.manager.PairingManager();
    }
    
    com.navdy.hud.app.service.pandora.PandoraManager providePandoraManager(com.squareup.otto.Bus a) {
        return new com.navdy.hud.app.service.pandora.PandoraManager(a, this.context.getResources());
    }
    
    com.navdy.hud.app.storage.PathManager providePathManager() {
        return com.navdy.hud.app.storage.PathManager.getInstance();
    }
    
    com.navdy.hud.app.device.PowerManager providePowerManager(com.squareup.otto.Bus a, android.content.SharedPreferences a0) {
        return new com.navdy.hud.app.device.PowerManager(a, this.context, a0);
    }
    
    com.navdy.hud.app.config.SettingsManager provideSettingsManager(com.squareup.otto.Bus a, android.content.SharedPreferences a0) {
        return new com.navdy.hud.app.config.SettingsManager(a, a0);
    }
    
    android.content.SharedPreferences provideSharedPreferences() {
        android.preference.PreferenceManager.setDefaultValues(this.context, "App", 0, R.xml.developer_preferences, true);
        return this.context.getSharedPreferences("App", 0);
    }
    
    com.navdy.hud.app.analytics.TelemetryDataManager provideTelemetryDataManager(com.navdy.hud.app.ui.framework.UIStateManager a, com.navdy.hud.app.device.PowerManager a0, com.squareup.otto.Bus a1, android.content.SharedPreferences a2, com.navdy.hud.app.framework.trips.TripManager a3) {
        return new com.navdy.hud.app.analytics.TelemetryDataManager(a, a0, a1, a2, a3);
    }
    
    com.navdy.hud.app.common.TimeHelper provideTimeHelper(com.squareup.otto.Bus a) {
        return new com.navdy.hud.app.common.TimeHelper(this.context, a);
    }
    
    com.navdy.hud.app.framework.trips.TripManager provideTripManager(com.squareup.otto.Bus a, android.content.SharedPreferences a0) {
        return new com.navdy.hud.app.framework.trips.TripManager(a, a0);
    }
    
    com.navdy.hud.app.ui.framework.UIStateManager provideUIStateManager() {
        return new com.navdy.hud.app.ui.framework.UIStateManager();
    }
    
    com.navdy.hud.app.framework.voice.VoiceSearchHandler provideVoiceSearchHandler(com.squareup.otto.Bus a, com.navdy.hud.app.util.FeatureUtil a0) {
        return new com.navdy.hud.app.framework.voice.VoiceSearchHandler(this.context, a, a0);
    }
}
