package com.navdy.hud.app.audio;


public enum SoundUtils$Sound {
    MENU_MOVE(0),
    MENU_SELECT(1),
    STARTUP(2),
    SHUTDOWN(3),
    ALERT_POSITIVE(4),
    ALERT_NEGATIVE(5);

    private int value;
    SoundUtils$Sound(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class SoundUtils$Sound extends Enum {
//    final private static com.navdy.hud.app.audio.SoundUtils$Sound[] $VALUES;
//    final public static com.navdy.hud.app.audio.SoundUtils$Sound ALERT_NEGATIVE;
//    final public static com.navdy.hud.app.audio.SoundUtils$Sound ALERT_POSITIVE;
//    final public static com.navdy.hud.app.audio.SoundUtils$Sound MENU_MOVE;
//    final public static com.navdy.hud.app.audio.SoundUtils$Sound MENU_SELECT;
//    final public static com.navdy.hud.app.audio.SoundUtils$Sound SHUTDOWN;
//    final public static com.navdy.hud.app.audio.SoundUtils$Sound STARTUP;
//    
//    static {
//        MENU_MOVE = new com.navdy.hud.app.audio.SoundUtils$Sound("MENU_MOVE", 0);
//        MENU_SELECT = new com.navdy.hud.app.audio.SoundUtils$Sound("MENU_SELECT", 1);
//        STARTUP = new com.navdy.hud.app.audio.SoundUtils$Sound("STARTUP", 2);
//        SHUTDOWN = new com.navdy.hud.app.audio.SoundUtils$Sound("SHUTDOWN", 3);
//        ALERT_POSITIVE = new com.navdy.hud.app.audio.SoundUtils$Sound("ALERT_POSITIVE", 4);
//        ALERT_NEGATIVE = new com.navdy.hud.app.audio.SoundUtils$Sound("ALERT_NEGATIVE", 5);
//        com.navdy.hud.app.audio.SoundUtils$Sound[] a = new com.navdy.hud.app.audio.SoundUtils$Sound[6];
//        a[0] = MENU_MOVE;
//        a[1] = MENU_SELECT;
//        a[2] = STARTUP;
//        a[3] = SHUTDOWN;
//        a[4] = ALERT_POSITIVE;
//        a[5] = ALERT_NEGATIVE;
//        $VALUES = a;
//    }
//    
//    private SoundUtils$Sound(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.audio.SoundUtils$Sound valueOf(String s) {
//        return (com.navdy.hud.app.audio.SoundUtils$Sound)Enum.valueOf(com.navdy.hud.app.audio.SoundUtils$Sound.class, s);
//    }
//    
//    public static com.navdy.hud.app.audio.SoundUtils$Sound[] values() {
//        return $VALUES.clone();
//    }
//}
//