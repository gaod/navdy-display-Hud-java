package com.navdy.hud.app;
import com.navdy.hud.app.R;

final public class R$raw {
    final public static int dial_pairing_flow = R.raw.dial_pairing_flow;
    final public static int dial_repairing_flow = R.raw.dial_repairing_flow;
    final public static int here_mwconfig_integrity = R.raw.here_mwconfig_integrity;
    final public static int here_voices = R.raw.here_voices;
    final public static int here_voices_integrity = R.raw.here_voices_integrity;
    final public static int here_voices_md5 = R.raw.here_voices_md5;
    final public static int mwconfig_hud = R.raw.mwconfig_hud;
    final public static int mwconfig_hud_md5 = R.raw.mwconfig_hud_md5;
    final public static int sound_alert_negative = R.raw.sound_alert_negative;
    final public static int sound_alert_positive = R.raw.sound_alert_positive;
    final public static int sound_menu_move = R.raw.sound_menu_move;
    final public static int sound_menu_select = R.raw.sound_menu_select;
    final public static int sound_shutdown = R.raw.sound_shutdown;
    final public static int sound_startup = R.raw.sound_startup;
    final public static int update3_2_0 = R.raw.update3_2_0;
    final public static int update4_2_1 = R.raw.update4_2_1;
    
    public R$raw() {
    }
}
