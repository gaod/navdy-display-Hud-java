package com.navdy.hud.app;
import com.navdy.hud.app.R;

final public class R$style {
    final public static int AppTheme = R.style.AppTheme;
    final public static int GaugeText = R.style.GaugeText;
    final public static int Glances_1 = R.style.Glances_1;
    final public static int Glances_1_bold = R.style.Glances_1_bold;
    final public static int Glances_2 = R.style.Glances_2;
    final public static int Glances_Disabled_1 = R.style.Glances_Disabled_1;
    final public static int Glances_Disabled_2 = R.style.Glances_Disabled_2;
    final public static int HockeyApp_ButtonStyle = R.style.HockeyApp_ButtonStyle;
    final public static int HockeyApp_EditTextStyle = R.style.HockeyApp_EditTextStyle;
    final public static int HockeyApp_SingleLineInputStyle = R.style.HockeyApp_SingleLineInputStyle;
    final public static int HudTheme = R.style.HudTheme;
    final public static int NotificationText = R.style.NotificationText;
    final public static int NotificationTitle = R.style.NotificationTitle;
    final public static int Roboto = R.style.Roboto;
    final public static int RobotoMedium = R.style.RobotoMedium;
    final public static int Roboto_Bold = R.style.Roboto_Bold;
    final public static int ToastMainTitle = R.style.ToastMainTitle;
    final public static int Toast_1 = R.style.Toast_1;
    final public static int Toast_2 = R.style.Toast_2;
    final public static int Toast_3 = R.style.Toast_3;
    final public static int destination_subtitle_two_line = R.style.destination_subtitle_two_line;
    final public static int destination_title_large = R.style.destination_title_large;
    final public static int destination_title_short = R.style.destination_title_short;
    final public static int glance_message_body = R.style.glance_message_body;
    final public static int glance_message_title = R.style.glance_message_title;
    final public static int glance_title_1 = R.style.glance_title_1;
    final public static int glance_title_2 = R.style.glance_title_2;
    final public static int glance_title_3 = R.style.glance_title_3;
    final public static int glance_title_4 = R.style.glance_title_4;
    final public static int incident_1 = R.style.incident_1;
    final public static int incident_2 = R.style.incident_2;
    final public static int installing_update_title_1 = R.style.installing_update_title_1;
    final public static int installing_update_title_2 = R.style.installing_update_title_2;
    final public static int mainTitle = R.style.mainTitle;
    final public static int route_duration_20_b = R.style.route_duration_20_b;
    final public static int route_duration_24 = R.style.route_duration_24;
    final public static int route_duration_26 = R.style.route_duration_26;
    final public static int small_glance_sign_text_1 = R.style.small_glance_sign_text_1;
    final public static int small_glance_sign_text_2 = R.style.small_glance_sign_text_2;
    final public static int small_glance_sign_text_3 = R.style.small_glance_sign_text_3;
    final public static int system_info_section = R.style.system_info_section;
    final public static int system_info_section_heading = R.style.system_info_section_heading;
    final public static int system_info_section_title = R.style.system_info_section_title;
    final public static int title1 = R.style.title1;
    final public static int title2 = R.style.title2;
    final public static int title3 = R.style.title3;
    final public static int title3_single = R.style.title3_single;
    final public static int tool_tip = R.style.tool_tip;
    final public static int vlist_picker_subtitle = R.style.vlist_picker_subtitle;
    final public static int vlist_picker_title = R.style.vlist_picker_title;
    final public static int vlist_subtitle = R.style.vlist_subtitle;
    final public static int vlist_subtitle_2_line = R.style.vlist_subtitle_2_line;
    final public static int vlist_title = R.style.vlist_title;
    
    public R$style() {
    }
}
