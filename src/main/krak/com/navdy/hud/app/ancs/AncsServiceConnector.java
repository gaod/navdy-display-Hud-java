package com.navdy.hud.app.ancs;

public class AncsServiceConnector extends com.navdy.hud.app.common.ServiceReconnector {
    final private static int NOTIFICATION_EVENT_SEND_THRESHOLD = 15000;
    final private static int ONE_HOUR_IN_MS = 3600000;
    private static java.util.HashMap blackList;
    private static java.util.HashMap enableNotificationTimeMap;
    final static com.navdy.service.library.log.Logger sLogger;
    private com.navdy.service.library.events.notification.NotificationCategory[] categoryMap;
    private Runnable connectRunnable;
    private Runnable disconnectRunnable;
    private com.navdy.ancs.IAncsServiceListener listener;
    private boolean localeUpToDate;
    protected com.navdy.hud.app.util.NotificationActionCache mActionCache;
    protected com.squareup.otto.Bus mBus;
    protected android.content.Context mContext;
    private com.navdy.service.library.events.DeviceInfo mDeviceInfo;
    protected com.navdy.hud.app.profile.NotificationSettings mNotificationSettings;
    protected com.navdy.ancs.IAncsService mService;
    private com.navdy.service.library.events.notification.NotificationsState notificationsState;
    private boolean reconnect;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ancs.AncsServiceConnector.class);
        blackList = new java.util.HashMap();
        enableNotificationTimeMap = new java.util.HashMap();
        blackList.put("com.apple.mobilephone", Boolean.valueOf(true));
    }
    
    public AncsServiceConnector(android.content.Context a, com.squareup.otto.Bus a0) {
        super(a, com.navdy.hud.app.ancs.AncsServiceConnector.getServiceIntent(), (String)null);
        this.notificationsState = com.navdy.service.library.events.notification.NotificationsState.NOTIFICATIONS_STOPPED;
        this.listener = (com.navdy.ancs.IAncsServiceListener)new com.navdy.hud.app.ancs.AncsServiceConnector$1(this);
        com.navdy.service.library.events.notification.NotificationCategory[] a1 = new com.navdy.service.library.events.notification.NotificationCategory[12];
        a1[0] = com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_OTHER;
        a1[1] = com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_INCOMING_CALL;
        a1[2] = com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_MISSED_CALL;
        a1[3] = com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_VOICE_MAIL;
        a1[4] = com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_SOCIAL;
        a1[5] = com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_SCHEDULE;
        a1[6] = com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_EMAIL;
        a1[7] = com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_NEWS;
        a1[8] = com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_HEALTH_AND_FITNESS;
        a1[9] = com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_BUSINESS_AND_FINANCE;
        a1[10] = com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_LOCATION;
        a1[11] = com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_ENTERTAINMENT;
        this.categoryMap = a1;
        this.connectRunnable = (Runnable)new com.navdy.hud.app.ancs.AncsServiceConnector$2(this);
        this.disconnectRunnable = (Runnable)new com.navdy.hud.app.ancs.AncsServiceConnector$3(this);
        this.mContext = a;
        this.mBus = a0;
        this.mNotificationSettings = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getNotificationSettings();
        this.mActionCache = new com.navdy.hud.app.util.NotificationActionCache((this).getClass());
        a0.register(this);
    }
    
    static void access$000(com.navdy.hud.app.ancs.AncsServiceConnector a, com.navdy.service.library.events.notification.NotificationsState a0, com.navdy.service.library.events.notification.NotificationsError a1) {
        a.setNotificationsState(a0, a1);
    }
    
    static java.util.HashMap access$100() {
        return blackList;
    }
    
    static com.navdy.service.library.events.glances.GlanceEvent access$200(com.navdy.hud.app.ancs.AncsServiceConnector a, com.navdy.ancs.AppleNotification a0) {
        return a.convertToGlanceEvent(a0);
    }
    
    static java.util.HashMap access$300() {
        return enableNotificationTimeMap;
    }
    
    static com.navdy.service.library.events.notification.NotificationEvent access$400(com.navdy.hud.app.ancs.AncsServiceConnector a, com.navdy.ancs.AppleNotification a0) {
        return a.convertNotification(a0);
    }
    
    static com.navdy.service.library.events.DeviceInfo access$500(com.navdy.hud.app.ancs.AncsServiceConnector a) {
        return a.mDeviceInfo;
    }
    
    static boolean access$600(com.navdy.hud.app.ancs.AncsServiceConnector a) {
        return a.localeUpToDate;
    }
    
    static void access$700(com.navdy.hud.app.ancs.AncsServiceConnector a) {
        a.stop();
    }
    
    private java.util.List buildActions(com.navdy.ancs.AppleNotification a) {
        java.util.List a0 = (a.getAppId().equals("com.apple.MobileSMS")) ? null : this.getDefaultActions(a);
        return a0;
    }
    
    private void connect() {
        this.reconnect = true;
        if (this.isIOS(this.mDeviceInfo)) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(this.connectRunnable, 1);
        }
    }
    
    private com.navdy.service.library.events.notification.NotificationEvent convertNotification(com.navdy.ancs.AppleNotification a) {
        String s = a.getTitle();
        String s0 = a.getSubTitle();
        return new com.navdy.service.library.events.notification.NotificationEvent(Integer.valueOf(a.getNotificationUid()), this.mapCategory(a.getCategoryId()), s, s0, a.getMessage(), a.getAppId(), this.buildActions(a), (okio.ByteString)null, (String)null, (String)null, s, Boolean.valueOf(true), a.getAppName());
    }
    
    private com.navdy.service.library.events.glances.GlanceEvent convertToGlanceEvent(com.navdy.ancs.AppleNotification a) {
        com.navdy.service.library.events.glances.GlanceEvent a0 = null;
        String s = a.getAppId();
        String s0 = String.valueOf(a.getNotificationUid());
        String s1 = a.getTitle();
        String s2 = a.getSubTitle();
        String s3 = a.getMessage();
        int i = a.getEventId();
        java.util.Date a1 = a.getDate();
        label1: {
            boolean b = false;
            label0: {
                if (s1 != null) {
                    break label0;
                }
                if (s3 != null) {
                    break label0;
                }
                if (s2 != null) {
                    break label0;
                }
                a0 = null;
                break label1;
            }
            com.navdy.hud.app.framework.glance.GlanceApp a2 = com.navdy.hud.app.framework.glance.GlanceHelper.getGlancesApp(s);
            if (a2 != null) {
                b = true;
            } else {
                a2 = com.navdy.hud.app.framework.glance.GlanceApp.GENERIC;
                b = false;
            }
            sLogger.v(new StringBuilder().append("[ancs-notif] appId[").append(s).append("]").append(" id[").append(s0).append("]").append(" title[").append(s1).append("]").append(" subTitle[").append(s2).append("]").append(" message[").append(s3).append("]").append(" eventId[").append(String.valueOf(i)).append("]").append(" time[").append(a1).append("]").append(" whitelisted[").append(b).append("]").append(" app[").append(a2).append("]").toString());
            switch(com.navdy.hud.app.ancs.AncsServiceConnector$4.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[a2.ordinal()]) {
                case 13: {
                    a0 = com.navdy.hud.app.ancs.AncsGlanceHelper.buildGenericEvent(s, s0, s1, s2, s3, a1);
                    break;
                }
                case 12: {
                    a0 = com.navdy.hud.app.ancs.AncsGlanceHelper.buildAppleCalendarEvent(s, s0, s1, s2, s3, a1);
                    break;
                }
                case 11: {
                    a0 = com.navdy.hud.app.ancs.AncsGlanceHelper.buildAppleMailEvent(s, s0, s1, s2, s3, a1);
                    break;
                }
                case 10: {
                    a0 = com.navdy.hud.app.ancs.AncsGlanceHelper.buildiMessageEvent(s, s0, s1, s2, s3, a1);
                    break;
                }
                case 9: {
                    a0 = com.navdy.hud.app.ancs.AncsGlanceHelper.buildTwitterEvent(s, s0, s1, s2, s3, a1);
                    break;
                }
                case 8: {
                    a0 = com.navdy.hud.app.ancs.AncsGlanceHelper.buildFacebookEvent(s, s0, s1, s2, s3, a1);
                    break;
                }
                case 7: {
                    a0 = com.navdy.hud.app.ancs.AncsGlanceHelper.buildFacebookMessengerEvent(s, s0, s1, s2, s3, a1);
                    break;
                }
                case 6: {
                    a0 = com.navdy.hud.app.ancs.AncsGlanceHelper.buildWhatsappEvent(s, s0, s1, s2, s3, a1);
                    break;
                }
                case 5: {
                    a0 = com.navdy.hud.app.ancs.AncsGlanceHelper.buildSlackEvent(s, s0, s1, s2, s3, a1);
                    break;
                }
                case 4: {
                    a0 = com.navdy.hud.app.ancs.AncsGlanceHelper.buildGoogleHangoutEvent(s, s0, s1, s2, s3, a1);
                    break;
                }
                case 3: {
                    a0 = com.navdy.hud.app.ancs.AncsGlanceHelper.buildGenericMailEvent(s, s0, s1, s2, s3, a1);
                    break;
                }
                case 2: {
                    a0 = com.navdy.hud.app.ancs.AncsGlanceHelper.buildGoogleMailEvent(s, s0, s1, s2, s3, a1);
                    break;
                }
                case 1: {
                    a0 = com.navdy.hud.app.ancs.AncsGlanceHelper.buildGoogleCalendarEvent(s, s0, s1, s2, s3, a1);
                    break;
                }
                default: {
                    sLogger.w(new StringBuilder().append("[ancs-notif] app of type [").append(a2).append("] not handled").toString());
                    a0 = null;
                }
            }
        }
        return a0;
    }
    
    private void disconnect() {
        this.reconnect = false;
        com.navdy.service.library.task.TaskManager.getInstance().execute(this.disconnectRunnable, 1);
    }
    
    private java.util.List getDefaultActions(com.navdy.ancs.AppleNotification a) {
        java.util.ArrayList a0 = new java.util.ArrayList();
        int i = a.getNotificationUid();
        if (a.hasPositiveAction()) {
            ((java.util.List)a0).add(this.mActionCache.buildAction(i, 0, 0, a.getPositiveActionLabel()));
        }
        if (a.hasNegativeAction()) {
            ((java.util.List)a0).add(this.mActionCache.buildAction(i, 1, 0, a.getNegativeActionLabel()));
        }
        return (java.util.List)a0;
    }
    
    private static android.content.Intent getServiceIntent() {
        android.content.Intent a = new android.content.Intent();
        a.setComponent(new android.content.ComponentName("com.navdy.ancs.app", "com.navdy.ancs.ANCSService"));
        return a;
    }
    
    private boolean isIOS(com.navdy.service.library.events.DeviceInfo a) {
        boolean b = false;
        label3: {
            label0: {
                label2: {
                    if (a == null) {
                        break label2;
                    }
                    com.navdy.service.library.events.DeviceInfo$Platform a0 = a.platform;
                    label1: {
                        if (a0 == null) {
                            break label1;
                        }
                        if (a.platform == com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS) {
                            break label0;
                        }
                    }
                    if (a.model.contains((CharSequence)"iPhone")) {
                        break label0;
                    }
                }
                b = false;
                break label3;
            }
            b = true;
        }
        return b;
    }
    
    private com.navdy.service.library.events.notification.NotificationCategory mapCategory(int i) {
        com.navdy.service.library.events.notification.NotificationCategory a = null;
        label2: {
            label0: {
                label1: {
                    if (i < 0) {
                        break label1;
                    }
                    if (i < this.categoryMap.length) {
                        break label0;
                    }
                }
                a = com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_OTHER;
                break label2;
            }
            a = this.categoryMap[i];
        }
        return a;
    }
    
    private void setNotificationsState(com.navdy.service.library.events.notification.NotificationsState a, com.navdy.service.library.events.notification.NotificationsError a0) {
        if (this.notificationsState != a) {
            this.notificationsState = a;
            this.mBus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.notification.NotificationsStatusUpdate$Builder().state(this.notificationsState).service(com.navdy.service.library.events.notification.ServiceType.SERVICE_ANCS).errorDetails(a0).build()));
        }
    }
    
    private void stop() {
        sLogger.d("Disconnecting ANCS connection");
        com.navdy.ancs.IAncsService a = this.mService;
        label0: {
            android.os.RemoteException a0 = null;
            if (a == null) {
                break label0;
            }
            try {
                this.mService.disconnect();
                break label0;
            } catch(android.os.RemoteException a1) {
                a0 = a1;
            }
            sLogger.e("Failed to disconnect ANCS", (Throwable)a0);
        }
    }
    
    private void updateNotificationFilters() {
        this.mNotificationSettings = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getNotificationSettings();
        com.navdy.ancs.IAncsService a = this.mService;
        label0: {
            if (a == null) {
                break label0;
            }
            try {
                long j = System.currentTimeMillis();
                java.util.List a0 = (this.mNotificationSettings == null) ? null : this.mNotificationSettings.enabledApps();
                sLogger.d(new StringBuilder().append("Setting ANCS to filter apps: ").append((a0 == null) ? "null" : java.util.Arrays.toString(a0.toArray())).toString());
                this.mService.setNotificationFilter(a0, j - 3600000L);
                break label0;
            } catch(android.os.RemoteException ignoredException) {
            }
            sLogger.w("Failed to update notification filter");
        }
    }
    
    protected void onConnected(android.content.ComponentName a, android.os.IBinder a0) {
        this.mService = com.navdy.ancs.IAncsService$Stub.asInterface(a0);
        try {
            this.updateNotificationFilters();
            this.mService.addListener(this.listener);
        } catch(android.os.RemoteException a1) {
            sLogger.e("Failed to add notification listener", (Throwable)a1);
        }
        if (this.reconnect) {
            this.connect();
        }
    }
    
    public void onConnectionChange(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        if (com.navdy.hud.app.ancs.AncsServiceConnector$4.$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[a.state.ordinal()] != 0) {
            this.mDeviceInfo = null;
            this.localeUpToDate = false;
            this.disconnect();
        }
    }
    
    public void onDeviceInfoAvailable(com.navdy.hud.app.event.DeviceInfoAvailable a) {
        sLogger.i("device info updated");
        this.mDeviceInfo = a.deviceInfo;
    }
    
    protected void onDisconnected(android.content.ComponentName a) {
        this.mService = null;
    }
    
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged a) {
        this.updateNotificationFilters();
    }
    
    public void onInitPhase(com.navdy.hud.app.event.InitEvents$InitPhase a) {
        switch(com.navdy.hud.app.ancs.AncsServiceConnector$4.$SwitchMap$com$navdy$hud$app$event$InitEvents$Phase[a.phase.ordinal()]) {
            case 2: {
                this.shutdown();
                break;
            }
            case 1: {
                this.localeUpToDate = true;
                if (!this.reconnect) {
                    break;
                }
                this.connect();
                break;
            }
        }
    }
    
    public void onNotificationAction(com.navdy.service.library.events.notification.NotificationAction a) {
        if (this.mActionCache.validAction(a)) {
            this.performAction(a.notificationId.intValue(), a.actionId.intValue());
            this.mBus.post(new com.navdy.service.library.events.ui.DismissScreen(com.navdy.service.library.events.ui.Screen.SCREEN_NOTIFICATION));
            this.mActionCache.markComplete(a);
        }
    }
    
    public void onNotificationPreferences(com.navdy.service.library.events.preferences.NotificationPreferences a) {
        this.updateNotificationFilters();
    }
    
    public void onNotificationsRequest(com.navdy.service.library.events.notification.NotificationsStatusRequest a) {
        com.navdy.service.library.events.notification.ServiceType a0 = a.service;
        label0: {
            label1: {
                if (a0 == null) {
                    break label1;
                }
                if (!com.navdy.service.library.events.notification.ServiceType.SERVICE_ANCS.equals(a.service)) {
                    break label0;
                }
            }
            sLogger.i(new StringBuilder().append("Received NotificationsStatusRequest:").append(a).toString());
            if (a.newState != null) {
                switch(com.navdy.hud.app.ancs.AncsServiceConnector$4.$SwitchMap$com$navdy$service$library$events$notification$NotificationsState[a.newState.ordinal()]) {
                    case 2: {
                        this.disconnect();
                        break;
                    }
                    case 1: {
                        this.connect();
                        break;
                    }
                }
            }
            this.mBus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.notification.NotificationsStatusUpdate$Builder().state(this.notificationsState).service(com.navdy.service.library.events.notification.ServiceType.SERVICE_ANCS).build()));
        }
    }
    
    public void onShutdown(com.navdy.hud.app.event.Shutdown a) {
        if (a.state == com.navdy.hud.app.event.Shutdown$State.CONFIRMED) {
            this.shutdown();
        }
    }
    
    public void performAction(int i, int i0) {
        com.navdy.ancs.IAncsService a = this.mService;
        label0: {
            android.os.RemoteException a0 = null;
            if (a == null) {
                break label0;
            }
            try {
                sLogger.d(new StringBuilder().append("Performing action (").append(i0).append(") for notification - ").append(i).toString());
                this.mService.performNotificationAction(i, i0);
                break label0;
            } catch(android.os.RemoteException a1) {
                a0 = a1;
            }
            sLogger.e("Exception when performing notification action", (Throwable)a0);
        }
    }
    
    public void shutdown() {
        this.reconnect = false;
        this.stop();
        super.shutdown();
    }
}
