package com.navdy.hud.app.maps.here;

class HereLocationFixManager$7 implements Runnable {
    final com.navdy.hud.app.maps.here.HereLocationFixManager this$0;
    final android.location.Location val$location;
    
    HereLocationFixManager$7(com.navdy.hud.app.maps.here.HereLocationFixManager a, android.location.Location a0) {
        super();
        this.this$0 = a;
        this.val$location = a0;
    }
    
    public void run() {
        try {
            String s = this.val$location.getProvider();
            if ("NAVDY_GPS_PROVIDER".equals(s)) {
                s = "gps";
            }
            String s0 = new StringBuilder().append(this.val$location.getLatitude()).append(",").append(this.val$location.getLongitude()).append(",").append(this.val$location.getBearing()).append(",").append(this.val$location.getSpeed()).append(",").append(this.val$location.getAccuracy()).append(",").append(this.val$location.getAltitude()).append(",").append(this.val$location.getTime()).append(",").append("raw").append(",").append(s).append("\n").toString();
            com.navdy.hud.app.maps.here.HereLocationFixManager.access$1500(this.this$0).write(s0.getBytes());
        } catch(Throwable a) {
            com.navdy.hud.app.maps.here.HereLocationFixManager.access$200().e(a);
        }
    }
}
