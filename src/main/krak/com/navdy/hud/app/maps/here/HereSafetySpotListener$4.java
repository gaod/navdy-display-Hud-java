package com.navdy.hud.app.maps.here;

class HereSafetySpotListener$4 {
    final static int[] $SwitchMap$com$here$android$mpa$mapping$SafetySpotInfo$Type;
    
    static {
        $SwitchMap$com$here$android$mpa$mapping$SafetySpotInfo$Type = new int[com.here.android.mpa.mapping.SafetySpotInfo$Type.values().length];
        int[] a = $SwitchMap$com$here$android$mpa$mapping$SafetySpotInfo$Type;
        com.here.android.mpa.mapping.SafetySpotInfo$Type a0 = com.here.android.mpa.mapping.SafetySpotInfo$Type.SPEED_CAMERA;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$here$android$mpa$mapping$SafetySpotInfo$Type[com.here.android.mpa.mapping.SafetySpotInfo$Type.SPEED_REDLIGHT_CAMERA.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
    }
}
