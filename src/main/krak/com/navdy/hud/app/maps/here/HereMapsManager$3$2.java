package com.navdy.hud.app.maps.here;

class HereMapsManager$3$2 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapsManager$3 this$1;
    final com.here.android.mpa.common.PositioningManager$LocationMethod val$locationMethod;
    final com.here.android.mpa.common.PositioningManager$LocationStatus val$locationStatus;
    
    HereMapsManager$3$2(com.navdy.hud.app.maps.here.HereMapsManager$3 a, com.here.android.mpa.common.PositioningManager$LocationMethod a0, com.here.android.mpa.common.PositioningManager$LocationStatus a1) {
        super();
        this.this$1 = a;
        this.val$locationMethod = a0;
        this.val$locationStatus = a1;
    }
    
    public void run() {
        try {
            if (com.navdy.hud.app.maps.here.HereMapsManager.access$100().isLoggable(2)) {
                com.navdy.hud.app.maps.here.HereMapsManager.access$100().d(new StringBuilder().append("onPositionFixChanged: method:").append(this.val$locationMethod).append(" status: ").append(this.val$locationStatus).toString());
            }
            if (this.val$locationMethod == com.here.android.mpa.common.PositioningManager$LocationMethod.GPS && com.navdy.hud.app.maps.here.HereMapUtil.isInTunnel(com.navdy.hud.app.maps.here.HereMapsManager.access$900(this.this$1.this$0).getRoadElement()) && !com.navdy.hud.app.maps.here.HereMapsManager.access$2800(this.this$1.this$0)) {
                com.navdy.hud.app.maps.here.HereMapsManager.access$100().d("TUNNEL extrapolation on");
                com.navdy.hud.app.maps.here.HereMapsManager.access$2802(this.this$1.this$0, true);
                com.navdy.hud.app.maps.here.HereMapsManager.access$2900(this.this$1.this$0);
            }
        } catch(Throwable a) {
            com.navdy.hud.app.maps.here.HereMapsManager.access$100().e(a);
        }
    }
}
