package com.navdy.hud.app.maps;


public enum MapEvents$DialMapZoom$Type {
    IN(0),
    OUT(1);

    private int value;
    MapEvents$DialMapZoom$Type(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class MapEvents$DialMapZoom$Type extends Enum {
//    final private static com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type[] $VALUES;
//    final public static com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type IN;
//    final public static com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type OUT;
//    
//    static {
//        IN = new com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type("IN", 0);
//        OUT = new com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type("OUT", 1);
//        com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type[] a = new com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type[2];
//        a[0] = IN;
//        a[1] = OUT;
//        $VALUES = a;
//    }
//    
//    private MapEvents$DialMapZoom$Type(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type valueOf(String s) {
//        return (com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type)Enum.valueOf(com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type.class, s);
//    }
//    
//    public static com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type[] values() {
//        return $VALUES.clone();
//    }
//}
//