package com.navdy.hud.app.maps.here;

class HereRouteManager$8 {
    final static int[] $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type;
    
    static {
        $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type = new int[com.here.android.mpa.routing.RouteOptions$Type.values().length];
        int[] a = $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type;
        com.here.android.mpa.routing.RouteOptions$Type a0 = com.here.android.mpa.routing.RouteOptions$Type.SHORTEST;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type[com.here.android.mpa.routing.RouteOptions$Type.FASTEST.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
    }
}
