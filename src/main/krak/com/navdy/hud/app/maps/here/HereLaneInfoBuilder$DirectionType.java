package com.navdy.hud.app.maps.here;


public enum HereLaneInfoBuilder$DirectionType {
    LEFT(0),
    RIGHT(1),
    LEFT_RIGHT(2);

    private int value;
    HereLaneInfoBuilder$DirectionType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class HereLaneInfoBuilder$DirectionType extends Enum {
//    final private static com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType[] $VALUES;
//    final public static com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType LEFT;
//    final public static com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType LEFT_RIGHT;
//    final public static com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType RIGHT;
//    
//    static {
//        LEFT = new com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType("LEFT", 0);
//        RIGHT = new com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType("RIGHT", 1);
//        LEFT_RIGHT = new com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType("LEFT_RIGHT", 2);
//        com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType[] a = new com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType[3];
//        a[0] = LEFT;
//        a[1] = RIGHT;
//        a[2] = LEFT_RIGHT;
//        $VALUES = a;
//    }
//    
//    private HereLaneInfoBuilder$DirectionType(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType valueOf(String s) {
//        return (com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType)Enum.valueOf(com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.class, s);
//    }
//    
//    public static com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType[] values() {
//        return $VALUES.clone();
//    }
//}
//