package com.navdy.hud.app.maps.here;

class HereNavigationManager$4 implements Runnable {
    final com.navdy.hud.app.maps.here.HereNavigationManager this$0;
    final boolean val$sendRouteResult;
    final com.navdy.service.library.events.navigation.NavigationSessionState val$sessionState;
    
    HereNavigationManager$4(com.navdy.hud.app.maps.here.HereNavigationManager a, boolean b, com.navdy.service.library.events.navigation.NavigationSessionState a0) {
        super();
        this.this$0 = a;
        this.val$sendRouteResult = b;
        this.val$sessionState = a0;
    }
    
    public void run() {
        com.navdy.service.library.events.navigation.NavigationRouteRequest a = com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).navigationRouteRequest;
        String s = null;
        String s0 = null;
        com.navdy.service.library.events.navigation.NavigationRouteResult a0 = null;
        if (a != null) {
            s = a.label;
            s0 = com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).routeId;
            boolean b = this.val$sendRouteResult;
            a0 = null;
            if (b) {
                com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a1 = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(s0);
                a0 = null;
                if (a1 != null) {
                    com.navdy.service.library.events.navigation.NavigationRouteResult a2 = a1.routeResult;
                    int i = a2.duration.intValue();
                    int i0 = a2.duration_traffic.intValue();
                    java.util.Date a3 = com.navdy.hud.app.maps.here.HereNavigationManager.access$700(this.this$0).getEta(true, com.here.android.mpa.routing.Route$TrafficPenaltyMode.DISABLED);
                    java.util.Date a4 = com.navdy.hud.app.maps.here.HereNavigationManager.access$700(this.this$0).getEta(true, com.here.android.mpa.routing.Route$TrafficPenaltyMode.OPTIMAL);
                    long j = System.currentTimeMillis();
                    if (com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(a3)) {
                        i = (int)(a3.getTime() - j) / 1000;
                    }
                    if (com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(a4)) {
                        i0 = (int)(a4.getTime() - j) / 1000;
                    }
                    int i1 = (int)com.navdy.hud.app.maps.here.HereNavigationManager.access$700(this.this$0).getDestinationDistance();
                    a0 = new com.navdy.service.library.events.navigation.NavigationRouteResult$Builder(a2).duration(Integer.valueOf(i)).duration_traffic(Integer.valueOf(i0)).length(Integer.valueOf(i1)).build();
                    com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v(new StringBuilder().append("NavigationSessionStatusEvent new data duration[").append(i).append("] traffic[").append(i0).append("] length[").append(i1).append("]").toString());
                }
            }
        }
        if (s0 == null && this.val$sessionState == com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED) {
            com.navdy.service.library.events.navigation.NavigationRouteRequest a5 = com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).getLastNavigationRequest();
            if (a5 != null) {
                s = a5.label;
                s0 = com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).getLastRouteId();
            }
        }
        com.navdy.service.library.events.navigation.NavigationSessionStatusEvent a6 = new com.navdy.service.library.events.navigation.NavigationSessionStatusEvent(this.val$sessionState, s, s0, a0, com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).destinationIdentifier);
        com.navdy.hud.app.maps.here.HereNavigationManager.access$800().sendEventToClient((com.squareup.wire.Message)a6);
        com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v(new StringBuilder().append("posted NavigationSessionStatusEvent state[").append(a6.sessionState).append("] label[").append(a6.label).append("] routeId[").append(a6.routeId).append("]").append("] result[").append((a0 != null) ? a0.via : "null").append("]").toString());
    }
}
