package com.navdy.hud.app.maps;

final public class MapsEventHandler$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding connectionHandler;
    private dagger.internal.Binding mDriverProfileManager;
    private dagger.internal.Binding sharedPreferences;
    
    public MapsEventHandler$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.maps.MapsEventHandler", false, com.navdy.hud.app.maps.MapsEventHandler.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.connectionHandler = a.requestBinding("com.navdy.hud.app.service.ConnectionHandler", com.navdy.hud.app.maps.MapsEventHandler.class, (this).getClass().getClassLoader());
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.maps.MapsEventHandler.class, (this).getClass().getClassLoader());
        this.sharedPreferences = a.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.maps.MapsEventHandler.class, (this).getClass().getClassLoader());
        this.mDriverProfileManager = a.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", com.navdy.hud.app.maps.MapsEventHandler.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.connectionHandler);
        a0.add(this.bus);
        a0.add(this.sharedPreferences);
        a0.add(this.mDriverProfileManager);
    }
    
    public void injectMembers(com.navdy.hud.app.maps.MapsEventHandler a) {
        a.connectionHandler = (com.navdy.hud.app.service.ConnectionHandler)this.connectionHandler.get();
        a.bus = (com.squareup.otto.Bus)this.bus.get();
        a.sharedPreferences = (android.content.SharedPreferences)this.sharedPreferences.get();
        a.mDriverProfileManager = (com.navdy.hud.app.profile.DriverProfileManager)this.mDriverProfileManager.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.maps.MapsEventHandler)a);
    }
}
