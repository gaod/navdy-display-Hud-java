package com.navdy.hud.app.maps.here;

class HereTrafficUpdater2$1 implements com.here.android.mpa.guidance.TrafficUpdater$Listener {
    final com.navdy.hud.app.maps.here.HereTrafficUpdater2 this$0;
    
    HereTrafficUpdater2$1(com.navdy.hud.app.maps.here.HereTrafficUpdater2 a) {
        super();
        this.this$0 = a;
    }
    
    public void onStatusChanged(com.here.android.mpa.guidance.TrafficUpdater$RequestState a) {
        int i = this.this$0.getRefereshInterval();
        label0: {
            Throwable a0 = null;
            label1: {
                label2: {
                    label4: {
                        boolean b = false;
                        label6: try {
                            com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$000().i(new StringBuilder().append("onRequestListener::").append(a).toString());
                            com.here.android.mpa.guidance.TrafficUpdater$RequestState a1 = com.here.android.mpa.guidance.TrafficUpdater$RequestState.DONE;
                            label5: {
                                if (a == a1) {
                                    break label5;
                                }
                                com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$000().i("onRequestListener:: traffic data download failed");
                                i = this.this$0.getRefereshInterval() / 2;
                                com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$800(this.this$0).post(com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$700());
                                b = true;
                                break label6;
                            }
                            com.here.android.mpa.routing.Route a2 = com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$100(this.this$0);
                            label3: {
                                if (a2 != null) {
                                    break label3;
                                }
                                com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$000().i("onRequestListener:traffic downloaded for open map");
                                break label4;
                            }
                            com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$000().i("onRequestListener:traffic downloaded for route");
                            if (!com.navdy.hud.app.maps.MapSettings.isTrafficDashWidgetsEnabled()) {
                                break label2;
                            }
                            com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$000().i("onRequestListener:call getEvents");
                            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereTrafficUpdater2$1$1(this), 2);
                            b = false;
                        } catch(Throwable a3) {
                            a0 = a3;
                            break label1;
                        }
                        if (!com.navdy.hud.app.maps.MapSettings.isTrafficDashWidgetsEnabled()) {
                            com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$202(this.this$0, (com.here.android.mpa.guidance.TrafficUpdater$RequestInfo)null);
                            com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$302(this.this$0, 0L);
                        }
                        if (!b) {
                            break label0;
                        }
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$400(this.this$0, i);
                        break label0;
                    }
                    if (!com.navdy.hud.app.maps.MapSettings.isTrafficDashWidgetsEnabled()) {
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$202(this.this$0, (com.here.android.mpa.guidance.TrafficUpdater$RequestInfo)null);
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$302(this.this$0, 0L);
                    }
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$400(this.this$0, i);
                    break label0;
                }
                if (!com.navdy.hud.app.maps.MapSettings.isTrafficDashWidgetsEnabled()) {
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$202(this.this$0, (com.here.android.mpa.guidance.TrafficUpdater$RequestInfo)null);
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$302(this.this$0, 0L);
                }
                com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$400(this.this$0, i);
                break label0;
            }
            try {
                com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$800(this.this$0).post(com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$700());
                com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$000().e("onRequestListener", a0);
            } catch(Throwable a4) {
                if (!com.navdy.hud.app.maps.MapSettings.isTrafficDashWidgetsEnabled()) {
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$202(this.this$0, (com.here.android.mpa.guidance.TrafficUpdater$RequestInfo)null);
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$302(this.this$0, 0L);
                }
                throw a4;
            }
            if (!com.navdy.hud.app.maps.MapSettings.isTrafficDashWidgetsEnabled()) {
                com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$202(this.this$0, (com.here.android.mpa.guidance.TrafficUpdater$RequestInfo)null);
                com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$302(this.this$0, 0L);
            }
            com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$400(this.this$0, i);
        }
    }
}
