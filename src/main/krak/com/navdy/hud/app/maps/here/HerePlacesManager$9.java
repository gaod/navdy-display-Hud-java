package com.navdy.hud.app.maps.here;
import com.navdy.hud.app.R;

final class HerePlacesManager$9 implements Runnable {
    final com.here.android.mpa.common.GeoCoordinate val$currentLocation;
    final java.util.List val$destinationLinks;
    final int val$maxResults;
    final String val$queryText;
    final int val$searchArea;
    
    HerePlacesManager$9(int i, java.util.List a, com.here.android.mpa.common.GeoCoordinate a0, int i0, String s) {
        super();
        this.val$maxResults = i;
        this.val$destinationLinks = a;
        this.val$currentLocation = a0;
        this.val$searchArea = i0;
        this.val$queryText = s;
    }
    
    public void run() {
        try {
            java.util.ArrayList a = new java.util.ArrayList(this.val$maxResults);
            java.util.Iterator a0 = this.val$destinationLinks.iterator();
            int i = 0;
            Object a1 = a0;
            while(((java.util.Iterator)a1).hasNext()) {
                com.here.android.mpa.search.PlaceLink a2 = (com.here.android.mpa.search.PlaceLink)((java.util.Iterator)a1).next();
                com.here.android.mpa.common.GeoCoordinate a3 = a2.getPosition();
                {
                    if (a3 == null) {
                        continue;
                    }
                    if (this.val$currentLocation != null && this.val$searchArea > 0 && this.val$currentLocation.distanceTo(a3) > (double)this.val$searchArea) {
                        continue;
                    }
                    com.navdy.service.library.events.location.Coordinate a4 = new com.navdy.service.library.events.location.Coordinate$Builder().latitude(Double.valueOf(a3.getLatitude())).longitude(Double.valueOf(a3.getLongitude())).build();
                    String s = a2.getVicinity();
                    String s0 = a2.getTitle();
                    double d = a2.getDistance();
                    if (s != null) {
                        s = s.replace((CharSequence)"<br/>", (CharSequence)", ");
                    }
                    com.here.android.mpa.search.Category a5 = a2.getCategory();
                    String s1 = null;
                    if (a5 != null) {
                        s1 = a5.getName();
                    }
                    a.add(new com.navdy.service.library.events.places.PlacesSearchResult(s0, (com.navdy.service.library.events.location.Coordinate)null, a4, s, s1, Double.valueOf(d)));
                    i = i + 1;
                    if (i != this.val$maxResults) {
                        continue;
                    }
                    com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("[search] max results reached:").append(this.val$maxResults).toString());
                    break;
                }
            }
            com.navdy.service.library.events.places.PlacesSearchResponse a6 = new com.navdy.service.library.events.places.PlacesSearchResponse$Builder().searchQuery(this.val$queryText).status(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS).statusDetail((String)null).results((java.util.List)a).build();
            com.navdy.hud.app.maps.MapsEventHandler.getInstance().sendEventToClient((com.squareup.wire.Message)a6);
            com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("[search] returned:").append(a.size()).toString());
        } catch(Throwable a7) {
            com.navdy.hud.app.maps.here.HerePlacesManager.access$000().e(a7);
            com.navdy.hud.app.maps.here.HerePlacesManager.access$300(this.val$queryText, com.navdy.service.library.events.RequestStatus.REQUEST_UNKNOWN_ERROR, com.navdy.hud.app.maps.here.HerePlacesManager.access$200().getString(R.string.autoCompleteSearch));
        }
    }
}
