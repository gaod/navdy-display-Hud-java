package com.navdy.hud.app.maps.here;

final class HerePlacesManager$6 implements com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener {
    final com.here.android.mpa.search.CategoryFilter val$filter;
    final com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener val$listener;
    final int val$nResults;
    
    HerePlacesManager$6(com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener a, com.here.android.mpa.search.CategoryFilter a0, int i) {
        super();
        this.val$listener = a;
        this.val$filter = a0;
        this.val$nResults = i;
    }
    
    public void onCompleted(java.util.List a) {
        this.val$listener.onCompleted(a);
    }
    
    public void onError(com.navdy.hud.app.maps.here.HerePlacesManager$Error a) {
        com.navdy.hud.app.maps.here.HerePlacesManager.handleCategoriesRequest(this.val$filter, this.val$nResults, (com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener)new com.navdy.hud.app.maps.here.HerePlacesManager$6$1(this), true);
    }
}
