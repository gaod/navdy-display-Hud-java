package com.navdy.hud.app.maps.here;

class HereMapsManager$4 implements android.content.SharedPreferences$OnSharedPreferenceChangeListener {
    final com.navdy.hud.app.maps.here.HereMapsManager this$0;
    
    HereMapsManager$4(com.navdy.hud.app.maps.here.HereMapsManager a) {
        super();
        this.this$0 = a;
    }
    
    public void onSharedPreferenceChanged(android.content.SharedPreferences a, String s) {
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().d(new StringBuilder().append("onSharedPreferenceChanged: ").append(s).toString());
        boolean b = this.this$0.isInitialized();
        label0: {
            Throwable a0 = null;
            label1: {
                Throwable a1 = null;
                if (b) {
                    int i = 0;
                    switch(s.hashCode()) {
                        case 134000933: {
                            i = (s.equals("map.zoom")) ? 1 : -1;
                            break;
                        }
                        case 133816335: {
                            i = (s.equals("map.tilt")) ? 0 : -1;
                            break;
                        }
                        case -285821321: {
                            i = (s.equals("map.scheme")) ? 2 : -1;
                            break;
                        }
                        case -1094616929: {
                            i = (s.equals("map.animation.mode")) ? 3 : -1;
                            break;
                        }
                        default: {
                            i = -1;
                        }
                    }
                    switch(i) {
                        case 2: {
                            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereMapsManager$4$1(this), 2);
                            break label0;
                        }
                        case 1: {
                            String s0 = a.getString(s, (String)null);
                            if (s0 != null) {
                                float f = 0.0f;
                                try {
                                    f = Float.parseFloat(s0);
                                } catch(Throwable a2) {
                                    a1 = a2;
                                    break;
                                }
                                com.navdy.hud.app.maps.here.HereMapsManager.access$100().w(new StringBuilder().append("onSharedPreferenceChanged:zoom:").append(f).toString());
                                com.navdy.hud.app.maps.here.HereMapsManager.access$500(this.this$0).setZoomLevel((double)f);
                                break label0;
                            } else {
                                com.navdy.hud.app.maps.here.HereMapsManager.access$100().v("onSharedPreferenceChanged:no zoom");
                                break label0;
                            }
                        }
                        case 0: {
                            String s1 = a.getString(s, (String)null);
                            if (s1 != null) {
                                float f0 = 0.0f;
                                try {
                                    f0 = Float.parseFloat(s1);
                                } catch(Throwable a3) {
                                    a0 = a3;
                                    break label1;
                                }
                                com.navdy.hud.app.maps.here.HereMapsManager.access$100().w(new StringBuilder().append("onSharedPreferenceChanged:tilt:").append(f0).toString());
                                com.navdy.hud.app.maps.here.HereMapsManager.access$500(this.this$0).setTilt(f0);
                                break label0;
                            } else {
                                com.navdy.hud.app.maps.here.HereMapsManager.access$100().v("onSharedPreferenceChanged:no tilt");
                                break label0;
                            }
                        }
                        default: {
                            break label0;
                        }
                    }
                } else {
                    com.navdy.hud.app.maps.here.HereMapsManager.access$100().w(new StringBuilder().append("onSharedPreferenceChanged: map engine not intialized:").append(s).toString());
                    break label0;
                }
                com.navdy.hud.app.maps.here.HereMapsManager.access$100().e("onSharedPreferenceChanged:zoom", a1);
                break label0;
            }
            com.navdy.hud.app.maps.here.HereMapsManager.access$100().e("onSharedPreferenceChanged:tilt", a0);
        }
    }
}
