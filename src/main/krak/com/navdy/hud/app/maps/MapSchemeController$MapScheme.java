package com.navdy.hud.app.maps;


    public enum MapSchemeController$MapScheme {
        DAY(0),
    NIGHT(1);

        private int value;
        MapSchemeController$MapScheme(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class MapSchemeController$MapScheme extends Enum {
//    final private static com.navdy.hud.app.maps.MapSchemeController$MapScheme[] $VALUES;
//    final public static com.navdy.hud.app.maps.MapSchemeController$MapScheme DAY;
//    final public static com.navdy.hud.app.maps.MapSchemeController$MapScheme NIGHT;
//    
//    static {
//        DAY = new com.navdy.hud.app.maps.MapSchemeController$MapScheme("DAY", 0);
//        NIGHT = new com.navdy.hud.app.maps.MapSchemeController$MapScheme("NIGHT", 1);
//        com.navdy.hud.app.maps.MapSchemeController$MapScheme[] a = new com.navdy.hud.app.maps.MapSchemeController$MapScheme[2];
//        a[0] = DAY;
//        a[1] = NIGHT;
//        $VALUES = a;
//    }
//    
//    private MapSchemeController$MapScheme(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.maps.MapSchemeController$MapScheme valueOf(String s) {
//        return (com.navdy.hud.app.maps.MapSchemeController$MapScheme)Enum.valueOf(com.navdy.hud.app.maps.MapSchemeController$MapScheme.class, s);
//    }
//    
//    public static com.navdy.hud.app.maps.MapSchemeController$MapScheme[] values() {
//        return $VALUES.clone();
//    }
//}
//