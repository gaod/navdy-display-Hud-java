package com.navdy.hud.app.maps.here;


    public enum HereMapAnimator$AnimationMode {
        NONE(0),
    ANIMATION(1),
    ZOOM_TILT_ANIMATION(2);

        private int value;
        HereMapAnimator$AnimationMode(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class HereMapAnimator$AnimationMode extends Enum {
//    final private static com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode[] $VALUES;
//    final public static com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode ANIMATION;
//    final public static com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode NONE;
//    final public static com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode ZOOM_TILT_ANIMATION;
//    
//    static {
//        NONE = new com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode("NONE", 0);
//        ANIMATION = new com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode("ANIMATION", 1);
//        ZOOM_TILT_ANIMATION = new com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode("ZOOM_TILT_ANIMATION", 2);
//        com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode[] a = new com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode[3];
//        a[0] = NONE;
//        a[1] = ANIMATION;
//        a[2] = ZOOM_TILT_ANIMATION;
//        $VALUES = a;
//    }
//    
//    private HereMapAnimator$AnimationMode(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode valueOf(String s) {
//        return (com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode)Enum.valueOf(com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode.class, s);
//    }
//    
//    public static com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode[] values() {
//        return $VALUES.clone();
//    }
//}
//