package com.navdy.hud.app.maps.here;

final public class HereMapCameraManager {
    final public static float DEFAULT_TILT = 60f;
    final public static double DEFAULT_ZOOM_LEVEL = 16.5;
    final private static int EASE_OUT_DYNAMIC_ZOOM_TILT_INTERVAL = 10000;
    final private static double HIGH_SPEED_THRESHOLD = 29.0576;
    final private static float HIGH_SPEED_TILT = 70f;
    final private static double HIGH_SPEED_ZOOM = 15.25;
    final private static double LOW_SPEED_THRESHOLD = 8.9408;
    final private static float LOW_SPEED_TILT = 60f;
    final private static double LOW_SPEED_ZOOM = 16.5;
    final private static double MAX_ZOOM_LEVEL_DIAL = 16.5;
    final private static double MEDIUM_SPEED_THRESHOLD = 20.1168;
    final private static double MEDIUM_SPEED_ZOOM = 16.0;
    final private static double MIN_SPEED_DIFFERENCE = 1.5;
    final private static float MIN_TILT_DIFFERENCE_TRIGGER = 5f;
    final private static double MIN_ZOOM_DIFFERENCE_TRIGGER = 0.25;
    final public static double MIN_ZOOM_LEVEL_DIAL = 12.0;
    final private static double MPH_TO_MS = 0.44704;
    final private static int N_ZOOM_TILT_ANIMATION_STEPS = 5;
    final private static float OVERVIEW_MAP_ZOOM_LEVEL = 10000f;
    final private static double VERY_HIGH_SPEED_THRESHOLD = 33.528;
    final private static double VERY_HIGH_SPEED_ZOOM = 14.75;
    final private static long ZOOM_LOCK_DURATION = 10000L;
    final private static double ZOOM_STEP = 0.25;
    final private static int ZOOM_TILT_ANIMATION_INTERVAL = 200;
    private static com.navdy.hud.app.maps.here.HereMapCameraManager sInstance;
    final private static com.navdy.service.library.log.Logger sLogger;
    private volatile boolean animationOn;
    private float currentDynamicTilt;
    private double currentDynamicZoom;
    private com.here.android.mpa.common.GeoCoordinate currentGeoCoordinate;
    private com.here.android.mpa.common.GeoPosition currentGeoPosition;
    private Runnable easeOutDynamicZoomTilt;
    private Runnable easeOutDynamicZoomTiltBk;
    private volatile boolean goBackfromOverviewOn;
    private volatile boolean goToOverviewOn;
    final private android.os.Handler handler;
    private com.navdy.hud.app.maps.here.HereMapAnimator hereMapAnimator;
    final private com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private boolean init;
    private boolean initialZoom;
    private boolean isFullAnimationSettingEnabled;
    private long lastDynamicZoomTime;
    private double lastRecordedSpeed;
    private com.navdy.service.library.events.preferences.LocalPreferences localPreferences;
    private com.navdy.hud.app.maps.here.HereMapController mapController;
    private boolean mapUIReady;
    private volatile int nTimesAnimated;
    private com.navdy.hud.app.ui.component.homescreen.NavigationView navigationView;
    private com.navdy.hud.app.manager.SpeedManager speedManager;
    private Runnable unlockZoom;
    private volatile boolean zoomActionOn;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereMapCameraManager.class);
        sInstance = new com.navdy.hud.app.maps.here.HereMapCameraManager();
    }
    
    private HereMapCameraManager() {
        this.unlockZoom = (Runnable)new com.navdy.hud.app.maps.here.HereMapCameraManager$1(this);
        this.easeOutDynamicZoomTilt = (Runnable)new com.navdy.hud.app.maps.here.HereMapCameraManager$2(this);
        this.easeOutDynamicZoomTiltBk = (Runnable)new com.navdy.hud.app.maps.here.HereMapCameraManager$3(this);
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
        this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
    }
    
    static Runnable access$000(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        return a.easeOutDynamicZoomTilt;
    }
    
    static android.os.Handler access$100(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        return a.handler;
    }
    
    static float access$1000(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        return a.currentDynamicTilt;
    }
    
    static long access$1102(com.navdy.hud.app.maps.here.HereMapCameraManager a, long j) {
        a.lastDynamicZoomTime = j;
        return j;
    }
    
    static void access$1200(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        a.animateCamera();
    }
    
    static com.here.android.mpa.common.GeoPosition access$1300(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        return a.currentGeoPosition;
    }
    
    static boolean access$1400(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        return a.animationOn;
    }
    
    static boolean access$1402(com.navdy.hud.app.maps.here.HereMapCameraManager a, boolean b) {
        a.animationOn = b;
        return b;
    }
    
    static boolean access$1500(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        return a.goBackfromOverviewOn;
    }
    
    static boolean access$1502(com.navdy.hud.app.maps.here.HereMapCameraManager a, boolean b) {
        a.goBackfromOverviewOn = b;
        return b;
    }
    
    static boolean access$1600(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        return a.goToOverviewOn;
    }
    
    static boolean access$1602(com.navdy.hud.app.maps.here.HereMapCameraManager a, boolean b) {
        a.goToOverviewOn = b;
        return b;
    }
    
    static Runnable access$1700(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        return a.unlockZoom;
    }
    
    static void access$1800(com.navdy.hud.app.maps.here.HereMapCameraManager a, Runnable a0) {
        a.goBackFromOverview(a0);
    }
    
    static boolean access$1900(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        return a.isFullAnimationSettingEnabled;
    }
    
    static boolean access$200(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        return a.zoomActionOn;
    }
    
    static com.navdy.hud.app.maps.here.HereMapAnimator access$2000(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        return a.hereMapAnimator;
    }
    
    static boolean access$202(com.navdy.hud.app.maps.here.HereMapCameraManager a, boolean b) {
        a.zoomActionOn = b;
        return b;
    }
    
    static void access$2100(com.navdy.hud.app.maps.here.HereMapCameraManager a, com.here.android.mpa.common.GeoCoordinate a0, Runnable a1) {
        a.goToOverview(a0, a1);
    }
    
    static int access$2200(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        return a.nTimesAnimated;
    }
    
    static int access$2202(com.navdy.hud.app.maps.here.HereMapCameraManager a, int i) {
        a.nTimesAnimated = i;
        return i;
    }
    
    static int access$2208(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        int i = a.nTimesAnimated;
        a.nTimesAnimated = i + 1;
        return i;
    }
    
    static Runnable access$2300(com.navdy.hud.app.maps.here.HereMapCameraManager a, double d, float f) {
        return a.getAnimateCameraRunnable(d, f);
    }
    
    static com.navdy.hud.app.maps.here.HereMapController access$300(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        return a.mapController;
    }
    
    static com.navdy.service.library.log.Logger access$400() {
        return sLogger;
    }
    
    static void access$500(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        a.zoomToDefault();
    }
    
    static boolean access$600(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        return a.isOverViewMapVisible();
    }
    
    static com.navdy.service.library.events.preferences.LocalPreferences access$700(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        return a.localPreferences;
    }
    
    static com.navdy.service.library.events.preferences.LocalPreferences access$702(com.navdy.hud.app.maps.here.HereMapCameraManager a, com.navdy.service.library.events.preferences.LocalPreferences a0) {
        a.localPreferences = a0;
        return a0;
    }
    
    static Runnable access$800(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        return a.easeOutDynamicZoomTiltBk;
    }
    
    static double access$900(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        return a.currentDynamicZoom;
    }
    
    private void animateCamera() {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereMapCameraManager$5(this), 2);
    }
    
    private Runnable getAnimateCameraRunnable(double d, float f) {
        return (Runnable)new com.navdy.hud.app.maps.here.HereMapCameraManager$8(this, d, f);
    }
    
    private float getDynamicTilt(double d) {
        float f = 0.0f;
        label2: if (d <= 8.9408) {
            f = 60f;
        } else {
            int i = (d > 8.9408) ? 1 : (d == 8.9408) ? 0 : -1;
            label0: {
                label1: {
                    if (i <= 0) {
                        break label1;
                    }
                    if (d <= 29.0576) {
                        break label0;
                    }
                }
                f = 70f;
                break label2;
            }
            f = (float)this.getLinearMapSection(d, 8.9408, 29.0576, 60.0, 70.0);
        }
        return f;
    }
    
    private double getDynamicZoom(double d) {
        double d0 = 0.0;
        label2: if (d <= 8.9408) {
            d0 = 16.5;
        } else {
            int i = (d > 8.9408) ? 1 : (d == 8.9408) ? 0 : -1;
            label4: {
                if (i <= 0) {
                    break label4;
                }
                if (!(d <= 20.1168)) {
                    break label4;
                }
                d0 = this.getLinearMapSection(d, 8.9408, 20.1168, 16.5, 16.0);
                break label2;
            }
            int i0 = (d > 20.1168) ? 1 : (d == 20.1168) ? 0 : -1;
            label3: {
                if (i0 <= 0) {
                    break label3;
                }
                if (!(d <= 29.0576)) {
                    break label3;
                }
                d0 = this.getLinearMapSection(d, 20.1168, 29.0576, 16.0, 15.25);
                break label2;
            }
            int i1 = (d > 29.0576) ? 1 : (d == 29.0576) ? 0 : -1;
            label0: {
                label1: {
                    if (i1 <= 0) {
                        break label1;
                    }
                    if (d <= 33.528) {
                        break label0;
                    }
                }
                d0 = 14.75;
                break label2;
            }
            d0 = this.getLinearMapSection(d, 29.0576, 33.528, 15.25, 14.75);
        }
        return d0;
    }
    
    public static com.navdy.hud.app.maps.here.HereMapCameraManager getInstance() {
        return sInstance;
    }
    
    private double getLinearMapSection(double d, double d0, double d1, double d2, double d3) {
        return (d - d0) * (d3 - d2) / (d1 - d0) + d2;
    }
    
    private boolean getNavigationView() {
        boolean b = false;
        if (this.navigationView == null) {
            com.navdy.hud.app.ui.framework.UIStateManager a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
            this.navigationView = a.getNavigationView();
            this.homeScreenView = a.getHomescreenView();
            b = this.navigationView != null;
        } else {
            b = true;
        }
        return b;
    }
    
    private void goBackFromOverview(Runnable a) {
        if (this.getNavigationView()) {
            this.navigationView.animateBackfromOverview((Runnable)new com.navdy.hud.app.maps.here.HereMapCameraManager$6(this, a));
        }
    }
    
    private void goToOverview(com.here.android.mpa.common.GeoCoordinate a, Runnable a0) {
        if (this.getNavigationView()) {
            if (this.hereNavigationManager.getCurrentRoute() == null) {
                this.navigationView.animateToOverview(a, (com.here.android.mpa.routing.Route)null, (com.here.android.mpa.mapping.MapRoute)null, a0, false);
            } else {
                this.navigationView.animateToOverview(a, this.hereNavigationManager.getCurrentRoute(), this.hereNavigationManager.getCurrentMapRoute(), a0, this.hereNavigationManager.hasArrived());
            }
        }
    }
    
    private boolean isOverViewMapVisible() {
        boolean b = false;
        boolean b0 = this.getNavigationView();
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.navigationView.isOverviewMapMode()) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private void setInitialZoom() {
        label0: if (!this.initialZoom) {
            sLogger.v(new StringBuilder().append("setInitialZoom:").append(this.currentGeoPosition).toString());
            com.here.android.mpa.common.GeoPosition a = this.currentGeoPosition;
            label1: {
                if (a != null) {
                    break label1;
                }
                com.here.android.mpa.common.GeoCoordinate a0 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                if (a0 == null) {
                    break label0;
                }
                this.currentGeoPosition = new com.here.android.mpa.common.GeoPosition(a0);
                this.currentGeoCoordinate = a0;
            }
            this.initialZoom = this.setZoom();
            sLogger.v(new StringBuilder().append("setInitialZoom:").append(this.initialZoom).toString());
        }
    }
    
    private void zoomToDefault() {
        if (this.init) {
            if (this.isManualZoom()) {
                sLogger.v("zoomToDefault: manual zoom enabled");
            } else if (this.getNavigationView()) {
                if (this.navigationView.isOverviewMapMode()) {
                    this.goBackFromOverview((Runnable)new com.navdy.hud.app.maps.here.HereMapCameraManager$7(this));
                } else {
                    this.animateCamera();
                }
            } else {
                sLogger.v("zoomToDefault: no view");
            }
        }
    }
    
    public com.here.android.mpa.common.GeoCoordinate getLastGeoCoordinate() {
        return this.currentGeoCoordinate;
    }
    
    public com.here.android.mpa.common.GeoPosition getLastGeoPosition() {
        return this.currentGeoPosition;
    }
    
    public float getLastTilt() {
        return this.currentDynamicTilt;
    }
    
    public double getLastZoom() {
        double d = 0.0;
        if (this.isManualZoom()) {
            d = (this.localPreferences.manualZoomLevel.floatValue() != 10000f) ? (double)this.localPreferences.manualZoomLevel.floatValue() : 12.0;
        } else {
            d = this.currentDynamicZoom;
        }
        return d;
    }
    
    public void initialize(com.navdy.hud.app.maps.here.HereMapController a, com.squareup.otto.Bus a0, com.navdy.hud.app.maps.here.HereMapAnimator a1) {
        synchronized(this) {
            if (!this.init) {
                this.mapController = a;
                this.hereMapAnimator = a1;
                this.isFullAnimationSettingEnabled = a1.getAnimationMode() == com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode.ZOOM_TILT_ANIMATION;
                com.navdy.hud.app.profile.DriverProfile a2 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
                this.localPreferences = a2.getLocalPreferences();
                sLogger.v(new StringBuilder().append("localPreferences [").append(a2.getProfileName()).append("] ").append(this.localPreferences).toString());
                this.setZoom();
                this.currentDynamicZoom = 16.5;
                this.currentDynamicTilt = 60f;
                a0.register(this);
                this.init = true;
            }
        }
        /*monexit(this)*/;
    }
    
    public boolean isManualZoom() {
        boolean b = false;
        com.navdy.service.library.events.preferences.LocalPreferences a = this.localPreferences;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (Boolean.TRUE.equals(this.localPreferences.manualZoom)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public boolean isOverviewZoomLevel() {
        boolean b = false;
        boolean b0 = this.isManualZoom();
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.localPreferences.manualZoomLevel.floatValue() == 10000f) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public void onDialZoom(com.navdy.hud.app.maps.MapEvents$DialMapZoom a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereMapCameraManager$4(this, a), 16);
    }
    
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged a) {
        com.navdy.hud.app.profile.DriverProfile a0 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
        this.localPreferences = a0.getLocalPreferences();
        sLogger.v(new StringBuilder().append("driver changed[").append(a0.getProfileName()).append("] ").append(this.localPreferences).toString());
        this.setZoom();
    }
    
    public void onGeoPositionChange(com.here.android.mpa.common.GeoPosition a) {
        this.currentGeoPosition = a;
        this.currentGeoCoordinate = a.getCoordinate();
        Boolean a0 = this.localPreferences.manualZoom;
        label0: {
            label2: {
                if (a0 == null) {
                    break label2;
                }
                if (this.localPreferences.manualZoom.booleanValue()) {
                    break label0;
                }
            }
            double d = (double)this.speedManager.getObdSpeed();
            double d0 = (d == -1.0) ? this.currentGeoPosition.getSpeed() : (double)com.navdy.hud.app.manager.SpeedManager.convert(d, this.speedManager.getSpeedUnit(), com.navdy.hud.app.manager.SpeedManager$SpeedUnit.METERS_PER_SECOND);
            if (d0 != this.lastRecordedSpeed) {
                double d1 = this.lastRecordedSpeed;
                boolean b = Math.abs(d0 - this.lastRecordedSpeed) >= 1.5;
                this.lastRecordedSpeed = d0;
                if (b) {
                    this.handler.removeCallbacks(this.easeOutDynamicZoomTilt);
                    this.currentDynamicZoom = this.getDynamicZoom(d0);
                    this.currentDynamicTilt = this.getDynamicTilt(d0);
                    if (!this.zoomActionOn) {
                        boolean b0 = Math.abs(this.mapController.getZoomLevel() - this.currentDynamicZoom) >= 0.25;
                        boolean b1 = Math.abs(this.mapController.getTilt() - this.currentDynamicTilt) > 5f;
                        label1: {
                            if (b0) {
                                break label1;
                            }
                            if (!b1) {
                                break label0;
                            }
                        }
                        long j = android.os.SystemClock.elapsedRealtime() - this.lastDynamicZoomTime;
                        if (j >= 10000L) {
                            sLogger.v(new StringBuilder().append("significant zoom=").append(b0).append(" tilt=").append(b1).append(" current-zoom=").append(this.currentDynamicZoom).append(" current-tilt=").append(this.currentDynamicTilt).append(" speed = ").append(d0).append(" lastSpeed=").append(d1).toString());
                            this.lastDynamicZoomTime = android.os.SystemClock.elapsedRealtime();
                            this.animateCamera();
                        } else {
                            long j0 = 10000L - j;
                            this.handler.postDelayed(this.easeOutDynamicZoomTilt, j0);
                            if (sLogger.isLoggable(2)) {
                                sLogger.v(new StringBuilder().append("significant too early =").append(j).append(" scheduled =").append(j0).toString());
                            }
                        }
                    }
                }
            } else if (sLogger.isLoggable(2)) {
                sLogger.v(new StringBuilder().append("speed not changed :").append(d0).toString());
            }
        }
    }
    
    public void onLocalPreferencesChanged(com.navdy.service.library.events.preferences.LocalPreferences a) {
        if (a.equals(this.localPreferences)) {
            sLogger.v(new StringBuilder().append("localPref not changed:").append(a).toString());
        } else {
            sLogger.v(new StringBuilder().append("localPref changed:").append(a).toString());
            this.localPreferences = a;
            this.setZoom();
        }
    }
    
    public void onLocationFixChangeEvent(com.navdy.hud.app.maps.MapEvents$LocationFix a) {
        if (this.init && this.mapUIReady) {
            this.setInitialZoom();
        }
    }
    
    public void onMapUIReady(com.navdy.hud.app.maps.MapEvents$MapUIReady a) {
        this.mapUIReady = true;
        this.setInitialZoom();
    }
    
    public boolean setZoom() {
        boolean b = false;
        label9: if (this.init) {
            com.here.android.mpa.common.GeoPosition a = this.currentGeoPosition;
            label7: {
                label8: {
                    if (a == null) {
                        break label8;
                    }
                    if (this.localPreferences != null) {
                        break label7;
                    }
                }
                b = false;
                break label9;
            }
            if (this.getNavigationView()) {
                com.navdy.hud.app.maps.here.HereMapController$State a0 = this.mapController.getState();
                label2: if (this.isManualZoom()) {
                    sLogger.v(new StringBuilder().append("setZoom: manual: ").append(a0).toString());
                    double d = (double)this.localPreferences.manualZoomLevel.floatValue();
                    if (d != 10000.0) {
                        sLogger.v(new StringBuilder().append("setZoom: normal zoom level: ").append(d).toString());
                        int i = (d < 12.0) ? -1 : (d == 12.0) ? 0 : 1;
                        label5: {
                            label6: {
                                if (i < 0) {
                                    break label6;
                                }
                                if (!(d > 16.5)) {
                                    break label5;
                                }
                            }
                            sLogger.w(new StringBuilder().append("setZoom: normal zoom level changed from [").append(d).append("] to [").append(16.5).append("]").toString());
                            d = 16.5;
                        }
                        com.navdy.hud.app.maps.here.HereMapController$State a1 = this.mapController.getState();
                        com.navdy.hud.app.maps.here.HereMapController$State a2 = com.navdy.hud.app.maps.here.HereMapController$State.OVERVIEW;
                        label3: {
                            label4: {
                                if (a1 == a2) {
                                    break label4;
                                }
                                if (this.mapController.getState() != com.navdy.hud.app.maps.here.HereMapController$State.TRANSITION) {
                                    break label3;
                                }
                            }
                            sLogger.v("setZoom: normal zoom level, showRouteMap");
                            this.navigationView.showRouteMap(this.currentGeoPosition, d, this.currentDynamicTilt);
                            break label2;
                        }
                        if (this.mapController.getState() != com.navdy.hud.app.maps.here.HereMapController$State.AR_MODE) {
                            sLogger.v(new StringBuilder().append("setZoom: normal zoom level, incorrect state:").append(a0).toString());
                        } else {
                            this.mapController.setCenter(this.currentGeoPosition.getCoordinate(), com.here.android.mpa.mapping.Map$Animation.NONE, d, (float)this.currentGeoPosition.getHeading(), this.currentDynamicTilt);
                            sLogger.v("setZoom: normal zoom level, already in route map, setcenter");
                        }
                    } else if (a0 != com.navdy.hud.app.maps.here.HereMapController$State.AR_MODE) {
                        sLogger.v(new StringBuilder().append("setZoom: overview map zoom level, not in ar mode:").append(a0).append(", set to OVERVIEW").toString());
                        this.mapController.setState(com.navdy.hud.app.maps.here.HereMapController$State.OVERVIEW);
                        this.navigationView.showStartMarker();
                    } else {
                        sLogger.v("setZoom: overview map zoom level, showOverviewMap");
                        this.navigationView.showOverviewMap(this.hereNavigationManager.getCurrentRoute(), this.hereNavigationManager.getCurrentMapRoute(), this.currentGeoPosition.getCoordinate(), this.hereNavigationManager.hasArrived());
                    }
                } else {
                    sLogger.v(new StringBuilder().append("setZoom: automatic: ").append(a0).toString());
                    com.navdy.hud.app.maps.here.HereMapController$State a3 = com.navdy.hud.app.maps.here.HereMapController$State.OVERVIEW;
                    label0: {
                        label1: {
                            if (a0 == a3) {
                                break label1;
                            }
                            if (a0 != com.navdy.hud.app.maps.here.HereMapController$State.TRANSITION) {
                                break label0;
                            }
                        }
                        sLogger.v("setZoom:automatic showRouteMap");
                        this.navigationView.showRouteMap(this.currentGeoPosition, this.currentDynamicZoom, this.currentDynamicTilt);
                        break label2;
                    }
                    if (this.mapController.getState() != com.navdy.hud.app.maps.here.HereMapController$State.AR_MODE) {
                        sLogger.v(new StringBuilder().append("setZoom:automatic incorrect state:").append(a0).toString());
                    } else {
                        sLogger.v("setZoom:automatic already in route map, setCenter");
                        this.mapController.setCenter(this.currentGeoPosition.getCoordinate(), com.here.android.mpa.mapping.Map$Animation.NONE, this.currentDynamicZoom, (float)this.currentGeoPosition.getHeading(), this.currentDynamicTilt);
                    }
                }
                b = true;
            } else {
                b = false;
            }
        } else {
            b = false;
        }
        return b;
    }
}
