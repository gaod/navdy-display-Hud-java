package com.navdy.hud.app.maps.here;

class HereMapController$12 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapController this$0;
    final com.here.android.mpa.mapping.Map$Animation val$animation;
    final com.here.android.mpa.common.GeoBoundingBox val$boundingBox;
    final float val$orientation;
    final com.here.android.mpa.common.ViewRect val$viewRect;
    
    HereMapController$12(com.navdy.hud.app.maps.here.HereMapController a, com.here.android.mpa.common.GeoBoundingBox a0, com.here.android.mpa.common.ViewRect a1, com.here.android.mpa.mapping.Map$Animation a2, float f) {
        super();
        this.this$0 = a;
        this.val$boundingBox = a0;
        this.val$viewRect = a1;
        this.val$animation = a2;
        this.val$orientation = f;
    }
    
    public void run() {
        try {
            com.navdy.hud.app.maps.here.HereMapController.access$000(this.this$0).zoomTo(this.val$boundingBox, this.val$viewRect, this.val$animation, this.val$orientation);
        } catch(Throwable a) {
            com.navdy.hud.app.maps.here.HereMapController.access$100().e(a);
        }
    }
}
