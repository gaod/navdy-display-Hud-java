package com.navdy.hud.app.maps.here;

class HereTrafficUpdater2$1$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereTrafficUpdater2$1 this$1;
    
    HereTrafficUpdater2$1$1(com.navdy.hud.app.maps.here.HereTrafficUpdater2$1 a) {
        super();
        this.this$1 = a;
    }
    
    public void run() {
        long j = android.os.SystemClock.elapsedRealtime();
        label4: {
            label2: try {
                com.navdy.hud.app.maps.here.HereNavigationManager a = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
                boolean b = a.isNavigationModeOn();
                label3: {
                    if (b) {
                        break label3;
                    }
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$000().v("nav is off");
                    break label4;
                }
                com.here.android.mpa.routing.Maneuver a0 = a.getNextManeuver();
                com.here.android.mpa.routing.Maneuver a1 = a.getPrevManeuver();
                com.here.android.mpa.common.RoadElement a2 = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadElement();
                java.util.List a3 = com.navdy.hud.app.maps.here.HereMapUtil.getAheadRouteElements(com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$100(this.this$1.this$0), a2, a1, a0);
                label0: {
                    label1: {
                        if (a3 == null) {
                            break label1;
                        }
                        if (a3.size() != 0) {
                            break label0;
                        }
                    }
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$000().i("onRequestListener: no ahead road elements");
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$202(this.this$1.this$0, (com.here.android.mpa.guidance.TrafficUpdater$RequestInfo)null);
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$302(this.this$1.this$0, 0L);
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$400(this.this$1.this$0, this.this$1.this$0.getRefereshInterval());
                    break label2;
                }
                com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$000().i(new StringBuilder().append("onRequestListener: calling getevent size:").append(a3.size()).toString());
                com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$600(this.this$1.this$0).getEvents(a3, com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$500(this.this$1.this$0));
            } catch(Throwable a4) {
                com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$202(this.this$1.this$0, (com.here.android.mpa.guidance.TrafficUpdater$RequestInfo)null);
                com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$302(this.this$1.this$0, 0L);
                com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$400(this.this$1.this$0, this.this$1.this$0.getRefereshInterval() / 2);
                com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$000().e("onRequestListener", a4);
            }
            long j0 = android.os.SystemClock.elapsedRealtime();
            com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$000().v(new StringBuilder().append("ahead route build time [").append(j0 - j).append("]").toString());
        }
    }
}
