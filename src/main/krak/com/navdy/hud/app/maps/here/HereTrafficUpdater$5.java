package com.navdy.hud.app.maps.here;

class HereTrafficUpdater$5 {
    final static int[] $SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error;
    
    static {
        $SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error = new int[com.here.android.mpa.guidance.TrafficUpdater$Error.values().length];
        int[] a = $SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error;
        com.here.android.mpa.guidance.TrafficUpdater$Error a0 = com.here.android.mpa.guidance.TrafficUpdater$Error.NONE;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error[com.here.android.mpa.guidance.TrafficUpdater$Error.UNKNOWN.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error[com.here.android.mpa.guidance.TrafficUpdater$Error.OUT_OF_MEMORY.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error[com.here.android.mpa.guidance.TrafficUpdater$Error.REQUEST_FAILED.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
    }
}
