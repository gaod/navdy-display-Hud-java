package com.navdy.hud.app.maps;


public enum MapEvents$ManeuverEvent$Type {
    FIRST(0),
    LAST(1),
    INTERMEDIATE(2);

    private int value;
    MapEvents$ManeuverEvent$Type(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class MapEvents$ManeuverEvent$Type extends Enum {
//    final private static com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type[] $VALUES;
//    final public static com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type FIRST;
//    final public static com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type INTERMEDIATE;
//    final public static com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type LAST;
//    
//    static {
//        FIRST = new com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type("FIRST", 0);
//        LAST = new com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type("LAST", 1);
//        INTERMEDIATE = new com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type("INTERMEDIATE", 2);
//        com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type[] a = new com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type[3];
//        a[0] = FIRST;
//        a[1] = LAST;
//        a[2] = INTERMEDIATE;
//        $VALUES = a;
//    }
//    
//    private MapEvents$ManeuverEvent$Type(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type valueOf(String s) {
//        return (com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type)Enum.valueOf(com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type.class, s);
//    }
//    
//    public static com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type[] values() {
//        return $VALUES.clone();
//    }
//}
//