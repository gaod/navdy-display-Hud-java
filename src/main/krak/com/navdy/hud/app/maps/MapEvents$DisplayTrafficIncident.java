package com.navdy.hud.app.maps;

public class MapEvents$DisplayTrafficIncident {
    final public static String ACCIDENT = "ACCIDENT";
    final public static String CLOSURE = "CLOSURE";
    final public static String CONGESTION = "CONGESTION";
    final public static String FLOW = "FLOW";
    final public static String OTHER = "OTHER";
    final public static String ROADWORKS = "ROADWORKS";
    final public static String UNDEFINED = "UNDEFINED";
    public String affectedStreet;
    public com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category category;
    public String description;
    public long distanceToIncident;
    public com.here.android.mpa.common.Image icon;
    public java.util.Date reported;
    public String title;
    public com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type type;
    public java.util.Date updated;
    
    public MapEvents$DisplayTrafficIncident(com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type a, com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category a0) {
        this.type = a;
        this.category = a0;
    }
    
    public MapEvents$DisplayTrafficIncident(com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type a, com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category a0, String s, String s0, String s1, long j, java.util.Date a1, java.util.Date a2, com.here.android.mpa.common.Image a3) {
        this.type = a;
        this.category = a0;
        this.title = s;
        this.description = s0;
        this.affectedStreet = s1;
        this.distanceToIncident = j;
        this.reported = a1;
        this.updated = a2;
        this.icon = a3;
    }
}
