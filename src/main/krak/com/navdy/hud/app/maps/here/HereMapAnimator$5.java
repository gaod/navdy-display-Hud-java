package com.navdy.hud.app.maps.here;

class HereMapAnimator$5 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapAnimator this$0;
    
    HereMapAnimator$5(com.navdy.hud.app.maps.here.HereMapAnimator a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.here.android.mpa.common.GeoPosition a = com.navdy.hud.app.maps.here.HereMapAnimator.access$700(this.this$0);
        if (a != null) {
            if (com.navdy.hud.app.maps.here.HereMapAnimator.access$2000(this.this$0) != com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode.NONE) {
                com.navdy.hud.app.maps.here.HereMapAnimator.access$400().v("rendering: set center");
                com.navdy.hud.app.maps.here.HereMapAnimator.access$900(this.this$0).setCenter(a.getCoordinate(), com.here.android.mpa.mapping.Map$Animation.NONE, -1.0, (float)a.getHeading(), -1f);
            } else {
                this.this$0.setGeoPosition(a);
                com.navdy.hud.app.maps.here.HereMapAnimator.access$400().v("rendering: set last pos");
            }
        }
    }
}
