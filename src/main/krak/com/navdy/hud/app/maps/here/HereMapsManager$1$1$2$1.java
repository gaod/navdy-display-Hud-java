package com.navdy.hud.app.maps.here;

class HereMapsManager$1$1$2$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapsManager$1$1$2 this$3;
    final long val$t3;
    
    HereMapsManager$1$1$2$1(com.navdy.hud.app.maps.here.HereMapsManager$1$1$2 a, long j) {
        super();
        this.this$3 = a;
        this.val$t3 = j;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereNavigationManager a = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
        com.navdy.hud.app.maps.here.HereMapsManager.access$1000(this.this$3.this$2.this$1.this$0);
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v("map created");
        com.navdy.hud.app.maps.notification.TrafficNotificationManager.getInstance();
        com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode a0 = com.navdy.hud.app.maps.MapSettings.isCustomAnimationEnabled() ? com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode.ANIMATION : com.navdy.hud.app.maps.MapSettings.isFullCustomAnimatonEnabled() ? com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode.ZOOM_TILT_ANIMATION : com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode.NONE;
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v(new StringBuilder().append("HereMapAnimator mode is [").append(a0).append("]").toString());
        com.navdy.hud.app.maps.here.HereMapsManager.access$1102(this.this$3.this$2.this$1.this$0, new com.navdy.hud.app.maps.here.HereMapAnimator(a0, com.navdy.hud.app.maps.here.HereMapsManager.access$500(this.this$3.this$2.this$1.this$0), a.getNavController()));
        if (!com.navdy.hud.app.maps.here.HereMapsManager.access$1200(this.this$3.this$2.this$1.this$0)) {
            com.navdy.hud.app.maps.here.HereMapsManager.access$100().v("HereMapAnimator MapRendering initial disabled");
            com.navdy.hud.app.maps.here.HereMapsManager.access$1100(this.this$3.this$2.this$1.this$0).stopMapRendering();
        }
        com.navdy.hud.app.maps.here.HereMapCameraManager.getInstance().initialize(com.navdy.hud.app.maps.here.HereMapsManager.access$500(this.this$3.this$2.this$1.this$0), this.this$3.this$2.this$1.this$0.bus, com.navdy.hud.app.maps.here.HereMapsManager.access$1100(this.this$3.this$2.this$1.this$0));
        com.navdy.hud.app.maps.here.HereMapsManager.access$1302(this.this$3.this$2.this$1.this$0, new com.navdy.hud.app.maps.here.HereLocationFixManager(this.this$3.this$2.this$1.this$0.bus));
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v(new StringBuilder().append("MAP-ENGINE-INIT-4 took [").append(android.os.SystemClock.elapsedRealtime() - this.val$t3).append("]").toString());
        com.navdy.hud.app.maps.here.HereMapsManager.access$1402(this.this$3.this$2.this$1.this$0, new com.navdy.hud.app.maps.notification.RouteCalculationEventHandler(this.this$3.this$2.this$1.this$0.bus));
        com.navdy.hud.app.maps.here.HereMapsManager.access$1500(this.this$3.this$2.this$1.this$0);
        com.navdy.hud.app.maps.here.HereMapsManager.access$1602(this.this$3.this$2.this$1.this$0, new com.navdy.hud.app.maps.here.HereOfflineMapsVersion(this.this$3.this$2.this$1.this$0.getOfflineMapsData()));
        synchronized(com.navdy.hud.app.maps.here.HereMapsManager.access$1700(this.this$3.this$2.this$1.this$0)) {
            com.navdy.hud.app.maps.here.HereMapsManager.access$1802(this.this$3.this$2.this$1.this$0, false);
            com.navdy.hud.app.maps.here.HereMapsManager.access$1902(this.this$3.this$2.this$1.this$0, true);
            this.this$3.this$2.this$1.this$0.bus.post(new com.navdy.hud.app.maps.MapEvents$MapEngineInitialize(com.navdy.hud.app.maps.here.HereMapsManager.access$1900(this.this$3.this$2.this$1.this$0)));
            com.navdy.hud.app.maps.here.HereMapsManager.access$100().v("MAP-ENGINE-INIT event sent");
            /*monexit(a1)*/;
        }
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v("setEngineOnlineState initial");
        com.navdy.hud.app.maps.here.HereMapsManager.access$2000(this.this$3.this$2.this$1.this$0);
        if (!this.this$3.this$2.this$1.this$0.powerManager.inQuietMode()) {
            com.navdy.hud.app.maps.here.HereMapsManager.access$2100(this.this$3.this$2.this$1.this$0);
        }
    }
}
