package com.navdy.hud.app.maps.here;

class HereNewManeuverListener$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereNewManeuverListener this$0;
    
    HereNewManeuverListener$1(com.navdy.hud.app.maps.here.HereNewManeuverListener a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        try {
            com.here.android.mpa.routing.Maneuver a = com.navdy.hud.app.maps.here.HereNewManeuverListener.access$000(this.this$0).getNextManeuver();
            com.navdy.hud.app.maps.here.HereNewManeuverListener.access$102(this.this$0, a);
            if (a != null) {
                com.here.android.mpa.routing.Maneuver a0 = com.navdy.hud.app.maps.here.HereNewManeuverListener.access$000(this.this$0).getAfterNextManeuver();
                if (com.navdy.hud.app.maps.here.HereNewManeuverListener.access$400(this.this$0)) {
                    String s = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadName();
                    com.navdy.hud.app.maps.here.HereNewManeuverListener.access$300(this.this$0).v(new StringBuilder().append(com.navdy.hud.app.maps.here.HereNewManeuverListener.access$200(this.this$0)).append(" ").append(com.navdy.hud.app.maps.here.HereNewManeuverListener.access$000(this.this$0).getState().name()).append(" current road[").append(s).append("] maneuver road name[").append(a.getRoadName()).append("] maneuver next roadname[").append(a.getNextRoadName()).append("] turn[").append(a.getTurn().name()).append("] action[").append(a.getAction().name()).append("] icon[").append(a.getIcon().name()).append("]").toString());
                }
                com.here.android.mpa.routing.Maneuver$Action a1 = a.getAction();
                com.here.android.mpa.routing.Maneuver$Action a2 = com.here.android.mpa.routing.Maneuver$Action.END;
                label0: {
                    label1: {
                        if (a1 == a2) {
                            break label1;
                        }
                        if (a.getIcon() != com.here.android.mpa.routing.Maneuver$Icon.END) {
                            break label0;
                        }
                    }
                    com.navdy.hud.app.maps.here.HereNewManeuverListener.access$300(this.this$0).v(new StringBuilder().append(com.navdy.hud.app.maps.here.HereNewManeuverListener.access$200(this.this$0)).append(" ").append(com.navdy.hud.app.maps.here.HereNewManeuverListener.access$000(this.this$0).getState()).append(" LAST MANEUVER RECEIVED").toString());
                    a0 = null;
                }
                boolean b = com.navdy.hud.app.maps.here.HereNewManeuverListener.access$500(this.this$0);
                com.navdy.hud.app.maps.here.HereNewManeuverListener.access$502(this.this$0, false);
                com.navdy.hud.app.maps.here.HereNewManeuverListener.access$600(this.this$0).updateNavigationInfo(a, a0, (com.here.android.mpa.routing.Maneuver)null, b);
                if (com.navdy.hud.app.maps.here.HereNewManeuverListener.access$600(this.this$0).isShownFirstManeuver()) {
                    com.navdy.hud.app.maps.here.HereNewManeuverListener.access$700(this.this$0).post(new com.navdy.hud.app.maps.MapEvents$ManeuverEvent(com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type.INTERMEDIATE, a));
                } else {
                    com.navdy.hud.app.maps.here.HereNewManeuverListener.access$600(this.this$0).setShownFirstManeuver(true);
                    if (com.navdy.hud.app.maps.here.HereNewManeuverListener.access$600(this.this$0).getNavigationSessionPreference().spokenTurnByTurn) {
                        String s0 = com.navdy.hud.app.maps.here.HereRouteManager.buildStartRouteTTS();
                        com.navdy.hud.app.maps.here.HereNewManeuverListener.access$300(this.this$0).v(new StringBuilder().append("tts[").append(s0).append("]").toString());
                        com.navdy.hud.app.maps.here.HereNewManeuverListener.access$700(this.this$0).post(com.navdy.hud.app.maps.notification.RouteCalculationNotification.CANCEL_CALC_TTS);
                        com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(s0, com.navdy.service.library.events.audio.SpeechRequest$Category.SPEECH_TURN_BY_TURN, com.navdy.hud.app.maps.notification.RouteCalculationNotification.ROUTE_CALC_TTS_ID);
                    }
                    com.navdy.hud.app.maps.here.HereNewManeuverListener.access$700(this.this$0).post(new com.navdy.hud.app.maps.MapEvents$ManeuverEvent(com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type.FIRST, a));
                }
            } else {
                com.navdy.hud.app.maps.here.HereNewManeuverListener.access$300(this.this$0).w(new StringBuilder().append(com.navdy.hud.app.maps.here.HereNewManeuverListener.access$200(this.this$0)).append("There is no next maneuver").toString());
            }
        } catch(Throwable a3) {
            com.navdy.hud.app.maps.here.HereNewManeuverListener.access$300(this.this$0).e(a3);
        }
    }
}
