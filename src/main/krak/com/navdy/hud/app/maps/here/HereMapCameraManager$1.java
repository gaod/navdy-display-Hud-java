package com.navdy.hud.app.maps.here;

class HereMapCameraManager$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapCameraManager this$0;
    
    HereMapCameraManager$1(com.navdy.hud.app.maps.here.HereMapCameraManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereMapCameraManager.access$100(this.this$0).removeCallbacks(com.navdy.hud.app.maps.here.HereMapCameraManager.access$000(this.this$0));
        com.navdy.hud.app.maps.here.HereMapCameraManager.access$202(this.this$0, false);
        if (com.navdy.hud.app.maps.here.HereMapCameraManager.access$300(this.this$0).getState() != com.navdy.hud.app.maps.here.HereMapController$State.ROUTE_PICKER) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereMapCameraManager$1$1(this), 2);
        } else {
            com.navdy.hud.app.maps.here.HereMapCameraManager.access$400().v("unlocking: in route search, no-op");
        }
    }
}
