package com.navdy.hud.app.maps.here;

public class HereGpsSignalListener extends com.here.android.mpa.guidance.NavigationManager$GpsSignalListener {
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    private com.navdy.service.library.log.Logger logger;
    private com.navdy.hud.app.maps.MapsEventHandler mapsEventHandler;
    private String tag;
    
    HereGpsSignalListener(com.navdy.service.library.log.Logger a, String s, com.squareup.otto.Bus a0, com.navdy.hud.app.maps.MapsEventHandler a1, com.navdy.hud.app.maps.here.HereNavigationManager a2) {
        this.logger = a;
        this.tag = s;
        this.bus = a0;
        this.mapsEventHandler = a1;
        this.hereNavigationManager = a2;
    }
    
    public void onGpsLost() {
        this.logger.w(new StringBuilder().append(this.tag).append(" Gps signal lost").toString());
        this.bus.post(this.hereNavigationManager.BUS_GPS_SIGNAL_LOST);
    }
    
    public void onGpsRestored() {
        this.logger.i(new StringBuilder().append(this.tag).append(" Gps signal restored").toString());
        this.bus.post(this.hereNavigationManager.BUS_GPS_SIGNAL_RESTORED);
    }
}
