package com.navdy.hud.app.maps.here;
import com.navdy.hud.app.R;

public class HereSpeedWarningListener extends com.here.android.mpa.guidance.NavigationManager$SpeedWarningListener {
    final private static int BUFFER_SPEED = 7;
    private com.squareup.otto.Bus bus;
    private android.content.Context context;
    private com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    private com.navdy.service.library.log.Logger logger;
    private com.navdy.hud.app.maps.MapsEventHandler mapsEventHandler;
    private com.navdy.hud.app.manager.SpeedManager speedManager;
    private boolean speedWarningOn;
    private String tag;
    private boolean verbose;
    private int warningSpeed;
    
    HereSpeedWarningListener(com.navdy.service.library.log.Logger a, String s, boolean b, com.squareup.otto.Bus a0, com.navdy.hud.app.maps.MapsEventHandler a1, com.navdy.hud.app.maps.here.HereNavigationManager a2) {
        this.context = com.navdy.hud.app.HudApplication.getAppContext();
        this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
        this.logger = a;
        this.tag = s;
        this.verbose = b;
        this.bus = a0;
        this.mapsEventHandler = a1;
        this.hereNavigationManager = a2;
    }
    
    public void onSpeedExceeded(String s, float f) {
        this.bus.post(com.navdy.hud.app.maps.here.HereNavigationManager.SPEED_EXCEEDED);
        this.logger.w(new StringBuilder().append(this.tag).append(" speed exceeded:").append(s).append(",").append(f).toString());
        if (!this.speedWarningOn) {
            this.warningSpeed = -1;
        }
        this.speedWarningOn = true;
        com.navdy.service.library.events.preferences.NavigationPreferences a = com.navdy.hud.app.maps.MapsEventHandler.getInstance().getNavigationPreferences();
        if (Boolean.TRUE.equals(a.spokenSpeedLimitWarnings) && !com.navdy.hud.app.framework.phonecall.CallUtils.isPhoneCallInProgress()) {
            int i = this.speedManager.getCurrentSpeed();
            if (i > 0) {
                com.navdy.hud.app.manager.SpeedManager$SpeedUnit a0 = this.speedManager.getSpeedUnit();
                int i0 = com.navdy.hud.app.manager.SpeedManager.convert((double)f, com.navdy.hud.app.manager.SpeedManager$SpeedUnit.METERS_PER_SECOND, a0);
                int i1 = (this.warningSpeed != -1) ? this.warningSpeed + this.warningSpeed / 10 : i0 + 7;
                if (i < i1) {
                    this.logger.w(new StringBuilder().append(this.tag).append(" speed exceeded current[").append(i).append("] threshold[").append(i1).append("] allowed[").append(i0).append("] ").append(a0.name()).toString());
                } else {
                    this.logger.w(new StringBuilder().append(this.tag).append(" speed exceeded current[").append(i).append("] threshold[").append(i1).append("] allowed[").append(i0).append("] ").append(a0.name()).toString());
                    this.warningSpeed = i;
                    switch(com.navdy.hud.app.maps.here.HereSpeedWarningListener$1.$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[a0.ordinal()]) {
                        case 3: {
                            String dummy = this.hereNavigationManager.TTS_METERS;
                            break;
                        }
                        case 2: {
                            String dummy0 = this.hereNavigationManager.TTS_KMS;
                            break;
                        }
                        case 1: {
                            String dummy1 = this.hereNavigationManager.TTS_MILES;
                            break;
                        }
                    }
                    android.content.Context a1 = this.context;
                    Object[] a2 = new Object[1];
                    a2[0] = Integer.valueOf(i0);
                    com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(a1.getString(R.string.tts_speed_warning, a2), com.navdy.service.library.events.audio.SpeechRequest$Category.SPEECH_SPEED_WARNING, (String)null);
                }
            } else {
                this.logger.w(new StringBuilder().append(this.tag).append("no obd or gps speed available").toString());
            }
        }
    }
    
    public void onSpeedExceededEnd(String s, float f) {
        this.speedWarningOn = false;
        this.warningSpeed = -1;
        this.bus.post(com.navdy.hud.app.maps.here.HereNavigationManager.SPEED_NORMAL);
        this.logger.w(new StringBuilder().append(this.tag).append("speed normal:").append(s).append(",").append(f).toString());
    }
}
