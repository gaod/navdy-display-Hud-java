package com.navdy.hud.app.maps;

public class GpsEventsReceiver extends android.content.BroadcastReceiver {
    final private static com.navdy.hud.app.event.DrivingStateChange DRIVING_STARTED;
    final private static com.navdy.hud.app.event.DrivingStateChange DRIVING_STOPPED;
    private com.navdy.service.library.log.Logger sLogger;
    
    static {
        DRIVING_STARTED = new com.navdy.hud.app.event.DrivingStateChange(true);
        DRIVING_STOPPED = new com.navdy.hud.app.event.DrivingStateChange(false);
    }
    
    public GpsEventsReceiver() {
        this.sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.GpsEventsReceiver.class);
    }
    
    private void reportGpsEvent(android.content.Intent a) {
        try {
            int i = 0;
            String s = a.getAction();
            android.os.Bundle a0 = a.getExtras();
            switch(s.hashCode()) {
                case 1315284313: {
                    i = (s.equals("GPS_COLLECT_LOGS")) ? 8 : -1;
                    break;
                }
                case 897686227: {
                    i = (s.equals("GPS_WARM_RESET_UBLOX")) ? 5 : -1;
                    break;
                }
                case -107791191: {
                    i = (s.equals("GPS_Switch")) ? 2 : -1;
                    break;
                }
                case -192082374: {
                    i = (s.equals("GPS_ENABLE_ESF_RAW")) ? 6 : -1;
                    break;
                }
                case -616759979: {
                    i = (s.equals("driving_stopped")) ? 4 : -1;
                    break;
                }
                case -629625847: {
                    i = (s.equals("driving_started")) ? 3 : -1;
                    break;
                }
                case -1788590639: {
                    i = (s.equals("GPS_DR_STOPPED")) ? 1 : -1;
                    break;
                }
                case -1801456507: {
                    i = (s.equals("GPS_DR_STARTED")) ? 0 : -1;
                    break;
                }
                case -2045233493: {
                    i = (s.equals("GPS_SATELLITE_STATUS")) ? 7 : -1;
                    break;
                }
                default: {
                    i = -1;
                }
            }
            switch(i) {
                case 8: {
                    String s0 = a.getStringExtra("logPath");
                    if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                        this.sLogger.w(new StringBuilder().append("Invalid gps log path:").append(s0).toString());
                        break;
                    } else {
                        com.navdy.hud.app.device.gps.GpsDeadReckoningManager.getInstance().dumpGpsInfo(s0);
                        break;
                    }
                }
                case 7: {
                    android.os.Bundle a1 = a.getBundleExtra("satellite_data");
                    if (a1 == null) {
                        break;
                    }
                    com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus().post(new com.navdy.hud.app.device.gps.GpsUtils$GpsSatelliteData(a1));
                    break;
                }
                case 6: {
                    this.sLogger.v("esf-raw");
                    com.navdy.hud.app.device.gps.GpsDeadReckoningManager.getInstance().enableEsfRaw();
                    break;
                }
                case 5: {
                    this.sLogger.v("warm reset ublox");
                    com.navdy.hud.app.device.gps.GpsDeadReckoningManager.getInstance().sendWarmReset();
                    com.navdy.hud.app.framework.voice.TTSUtils.debugShowGpsReset("Warm Reset Ublox");
                    break;
                }
                case 4: {
                    this.sLogger.i("Driving stopped");
                    com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus().post(DRIVING_STOPPED);
                    break;
                }
                case 3: {
                    this.sLogger.i("Driving started");
                    com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus().post(DRIVING_STARTED);
                    break;
                }
                case 2: {
                    com.navdy.hud.app.framework.voice.TTSUtils.debugShowGpsSwitch(a0.getString("title"), a0.getString("info"));
                    boolean b = a0.getBoolean("phone");
                    boolean b0 = a0.getBoolean("ublox");
                    com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus().post(new com.navdy.hud.app.device.gps.GpsUtils$GpsSwitch(b, b0));
                    break;
                }
                case 1: {
                    com.navdy.hud.app.framework.voice.TTSUtils.debugShowDREnded();
                    break;
                }
                case 0: {
                    com.navdy.hud.app.framework.voice.TTSUtils.debugShowDRStarted("");
                    break;
                }
            }
        } catch(Throwable a2) {
            this.sLogger.e(a2);
        }
    }
    
    public void onReceive(android.content.Context a, android.content.Intent a0) {
        this.reportGpsEvent(a0);
    }
}
