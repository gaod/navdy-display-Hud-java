package com.navdy.hud.app.maps.here;

class NavdyTrafficRerouteManager$5 implements com.navdy.hud.app.maps.here.HereRouteCalculator$RouteCalculatorListener {
    final com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager this$0;
    final String val$additionalVia;
    final String val$currentVia;
    final long val$diff;
    final long val$distanceDiff;
    final long val$etaUtc;
    final com.here.android.mpa.routing.Route val$fasterRoute;
    final com.here.android.mpa.routing.RouteTta val$fasterRouteTta;
    final boolean val$notifFromHereTraffic;
    final String val$via;
    
    NavdyTrafficRerouteManager$5(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager a, boolean b, long j, String s, String s0, com.here.android.mpa.routing.RouteTta a0, long j0, com.here.android.mpa.routing.Route a1, String s1, long j1) {
        super();
        this.this$0 = a;
        this.val$notifFromHereTraffic = b;
        this.val$diff = j;
        this.val$via = s;
        this.val$currentVia = s0;
        this.val$fasterRouteTta = a0;
        this.val$etaUtc = j0;
        this.val$fasterRoute = a1;
        this.val$additionalVia = s1;
        this.val$distanceDiff = j1;
    }
    
    public void error(com.here.android.mpa.routing.RoutingError a, Throwable a0) {
        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$500(this.this$0).removeCallbacks(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$400(this.this$0));
        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$600(this.this$0);
        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$000(this.this$0).fasterRouteNotFound(this.val$notifFromHereTraffic, this.val$diff, this.val$via, this.val$currentVia);
        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().e(new StringBuilder().append("error occured:").append(a).toString(), a0);
    }
    
    public void postSuccess(java.util.ArrayList a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager$5$1(this, a), 2);
    }
    
    public void preSuccess() {
        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$500(this.this$0).removeCallbacks(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$400(this.this$0));
    }
    
    public void progress(int i) {
    }
}
