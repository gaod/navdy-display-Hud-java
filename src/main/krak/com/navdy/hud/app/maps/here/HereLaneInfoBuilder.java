package com.navdy.hud.app.maps.here;
import com.navdy.hud.app.R;

public class HereLaneInfoBuilder {
    private static boolean initialized;
    private static java.util.HashMap sLaneDrawableCache;
    private static java.util.HashMap sLeftOnlyNotRecommendedDirection;
    private static java.util.HashMap sLeftOnlyRecommendedDirection;
    private static java.util.HashMap sLeftRightNotRecommendedDirection;
    private static java.util.HashMap sLeftRightRecommendedDirection;
    final private static com.navdy.service.library.log.Logger sLogger;
    private static java.util.HashMap sRightOnlyNotRecommendedDirection;
    private static java.util.HashMap sRightOnlyRecommendedDirection;
    private static java.util.HashMap sSingleNotRecommendedDirection;
    private static java.util.HashMap sSingleRecommendedDirection;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereLaneInfoBuilder.class);
    }
    
    public HereLaneInfoBuilder() {
    }
    
    public static boolean compareLaneData(java.util.ArrayList a, java.util.ArrayList a0) {
        boolean b = false;
        label0: if (a == null) {
            b = false;
        } else if (a0 == null) {
            b = false;
        } else {
            int i = a.size();
            if (i == a0.size()) {
                int i0 = 0;
                while(true) {
                    if (i0 >= i) {
                        b = true;
                        break;
                    } else {
                        com.navdy.hud.app.maps.MapEvents$LaneData a1 = (com.navdy.hud.app.maps.MapEvents$LaneData)a.get(i0);
                        com.navdy.hud.app.maps.MapEvents$LaneData a2 = (com.navdy.hud.app.maps.MapEvents$LaneData)a0.get(i0);
                        if (a1 == null) {
                            b = false;
                            break;
                        } else if (a2 == null) {
                            b = false;
                            break;
                        } else if (a1.position != a2.position) {
                            b = false;
                            break;
                        } else if (a1.icons == null) {
                            b = false;
                            break;
                        } else if (a2.icons == null) {
                            b = false;
                            break;
                        } else if (a1.icons.length != a2.icons.length) {
                            b = false;
                            break;
                        } else {
                            int i1 = 0;
                            while(i1 < a1.icons.length) {
                                if (a1.icons[i1] != a2.icons[i1]) {
                                    b = false;
                                    break label0;
                                } else {
                                    i1 = i1 + 1;
                                }
                            }
                            i0 = i0 + 1;
                        }
                    }
                }
            } else {
                b = false;
            }
        }
        return b;
    }
    
    public static com.here.android.mpa.guidance.LaneInformation$Direction getDirection(java.util.EnumSet a, com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType a0) {
        Object a1 = a.iterator();
        while(true) {
            com.here.android.mpa.guidance.LaneInformation$Direction a2 = null;
            if (((java.util.Iterator)a1).hasNext()) {
                a2 = (com.here.android.mpa.guidance.LaneInformation$Direction)((java.util.Iterator)a1).next();
                switch(com.navdy.hud.app.maps.here.HereLaneInfoBuilder$1.$SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[a2.ordinal()]) {
                    case 6: case 7: case 8: {
                        if (a0 != com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.LEFT) {
                            continue;
                        }
                        break;
                    }
                    case 2: case 3: case 4: {
                        if (a0 != com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.RIGHT) {
                            continue;
                        }
                        break;
                    }
                    default: {
                        continue;
                    }
                }
            } else {
                a2 = null;
            }
            return a2;
        }
    }
    
    private static com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType getDirectionType(java.util.List a) {
        com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType a0 = null;
        Object a1 = a.iterator();
        int i = 0;
        int i0 = 0;
        while(((java.util.Iterator)a1).hasNext()) {
            com.here.android.mpa.guidance.LaneInformation$Direction a2 = (com.here.android.mpa.guidance.LaneInformation$Direction)((java.util.Iterator)a1).next();
            switch(com.navdy.hud.app.maps.here.HereLaneInfoBuilder$1.$SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[a2.ordinal()]) {
                case 9: {
                    i0 = i0 + 1;
                    break;
                }
                case 8: {
                    i = i + 1;
                    break;
                }
                case 7: {
                    i = i + 1;
                    break;
                }
                case 6: {
                    i = i + 1;
                    break;
                }
                case 5: {
                    i = i + 1;
                    break;
                }
                case 4: {
                    i0 = i0 + 1;
                    break;
                }
                case 3: {
                    i0 = i0 + 1;
                    break;
                }
                case 2: {
                    i0 = i0 + 1;
                    break;
                }
                default: {
                    break;
                }
                case 1: {
                }
            }
        }
        label1: {
            label0: {
                if (i <= 0) {
                    break label0;
                }
                if (i0 <= 0) {
                    break label0;
                }
                a0 = com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.LEFT_RIGHT;
                break label1;
            }
            a0 = (i <= 0) ? com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.RIGHT : com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.LEFT;
        }
        return a0;
    }
    
    public static android.graphics.drawable.Drawable[] getDrawable(java.util.List a, com.navdy.hud.app.maps.MapEvents$LaneData$Position a0, com.here.android.mpa.guidance.LaneInformation$Direction a1) {
        android.graphics.drawable.Drawable[] a2 = null;
        if (a != null) {
            android.graphics.drawable.Drawable[] a3 = null;
            if (a.size() != 1) {
                com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType a4 = com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDirectionType(a);
                a3 = new android.graphics.drawable.Drawable[a.size()];
                java.util.Iterator a5 = a.iterator();
                int i = 0;
                Object a6 = a5;
                while(((java.util.Iterator)a6).hasNext()) {
                    com.here.android.mpa.guidance.LaneInformation$Direction a7 = (com.here.android.mpa.guidance.LaneInformation$Direction)((java.util.Iterator)a6).next();
                    com.navdy.hud.app.maps.MapEvents$LaneData$Position a8 = (a0 != com.navdy.hud.app.maps.MapEvents$LaneData$Position.ON_ROUTE) ? a0 : (a1 == null) ? a0 : (a7 != a1) ? com.navdy.hud.app.maps.MapEvents$LaneData$Position.OFF_ROUTE : com.navdy.hud.app.maps.MapEvents$LaneData$Position.ON_ROUTE;
                    switch(com.navdy.hud.app.maps.here.HereLaneInfoBuilder$1.$SwitchMap$com$navdy$hud$app$maps$here$HereLaneInfoBuilder$DirectionType[a4.ordinal()]) {
                        case 3: {
                            a3[i] = com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDrawableForDirection(a7, a8, sLeftRightRecommendedDirection, sLeftRightNotRecommendedDirection);
                            i = i + 1;
                            break;
                        }
                        case 2: {
                            a3[i] = com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDrawableForDirection(a7, a8, sRightOnlyRecommendedDirection, sRightOnlyNotRecommendedDirection);
                            i = i + 1;
                            break;
                        }
                        case 1: {
                            a3[i] = com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDrawableForDirection(a7, a8, sLeftOnlyRecommendedDirection, sLeftOnlyNotRecommendedDirection);
                            i = i + 1;
                            break;
                        }
                    }
                }
            } else {
                a3 = new android.graphics.drawable.Drawable[1];
                a3[0] = com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDrawableForDirection((com.here.android.mpa.guidance.LaneInformation$Direction)a.get(0), a0, sSingleRecommendedDirection, sSingleNotRecommendedDirection);
            }
            if (a3 == null) {
                a2 = a3;
            } else {
                int i0 = 0;
                int i1 = 0;
                int i2 = 0;
                while(i2 < a3.length) {
                    if (a3[i2] != null) {
                        i0 = i0 + 1;
                    } else {
                        i1 = i1 + 1;
                    }
                    i2 = i2 + 1;
                }
                if (i0 != 0) {
                    if (i1 <= 0) {
                        a2 = a3;
                    } else {
                        a2 = new android.graphics.drawable.Drawable[i0];
                        int i3 = 0;
                        int i4 = 0;
                        while(i4 < a3.length) {
                            if (a3[i4] != null) {
                                a2[i3] = a3[i4];
                                i3 = i3 + 1;
                            }
                            i4 = i4 + 1;
                        }
                    }
                } else {
                    a2 = null;
                }
            }
        } else {
            a2 = null;
        }
        return a2;
    }
    
    private static android.graphics.drawable.Drawable getDrawableForDirection(com.here.android.mpa.guidance.LaneInformation$Direction a, com.navdy.hud.app.maps.MapEvents$LaneData$Position a0, java.util.HashMap a1, java.util.HashMap a2) {
        Integer a3 = (a0 != com.navdy.hud.app.maps.MapEvents$LaneData$Position.ON_ROUTE) ? (Integer)a2.get(a) : (Integer)a1.get(a);
        return (a3 != null) ? (android.graphics.drawable.Drawable)sLaneDrawableCache.get(a3) : null;
    }
    
    public static int getNumDirections(java.util.EnumSet a, com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType a0) {
        Object a1 = a.iterator();
        int i = 0;
        while(((java.util.Iterator)a1).hasNext()) {
            com.here.android.mpa.guidance.LaneInformation$Direction a2 = (com.here.android.mpa.guidance.LaneInformation$Direction)((java.util.Iterator)a1).next();
            switch(com.navdy.hud.app.maps.here.HereLaneInfoBuilder$1.$SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[a2.ordinal()]) {
                case 6: case 7: case 8: {
                    if (a0 != com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.LEFT) {
                        break;
                    }
                    i = i + 1;
                    break;
                }
                case 2: case 3: case 4: {
                    if (a0 != com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.RIGHT) {
                        break;
                    }
                    i = i + 1;
                    break;
                }
            }
        }
        return i;
    }
    
    public static void init() {
        label1: synchronized(com.navdy.hud.app.maps.here.HereLaneInfoBuilder.class) {
        boolean b = initialized;
            //Throwable a = null;
            if (b) {
                break label1;
            }
            label0: {
                //try {
                    com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
                    initialized = true;
                    android.content.res.Resources a0 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
                    sLaneDrawableCache = new java.util.HashMap();
                    sSingleRecommendedDirection = new java.util.HashMap();
                    sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.STRAIGHT, Integer.valueOf(R.drawable.icon_lg_rec_straight_only));
                    sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SLIGHTLY_RIGHT, Integer.valueOf(R.drawable.icon_lg_rec_slight_right_only));
                    sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.RIGHT, Integer.valueOf(R.drawable.icon_lg_rec_right_only));
                    sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SHARP_RIGHT, Integer.valueOf(R.drawable.icon_lg_rec_heavy_right_only));
                    sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.U_TURN_RIGHT, Integer.valueOf(R.drawable.icon_lg_rec_uturn_right_only));
                    sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SECOND_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.MERGE_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.U_TURN_LEFT, Integer.valueOf(R.drawable.icon_lg_rec_uturn_left_only));
                    sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SHARP_LEFT, Integer.valueOf(R.drawable.icon_lg_rec_heavy_left_only));
                    sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.LEFT, Integer.valueOf(R.drawable.icon_lg_rec_left_only));
                    sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SLIGHTLY_LEFT, Integer.valueOf(R.drawable.icon_lg_rec_slight_left_only));
                    sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SECOND_LEFT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.MERGE_LEFT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sSingleRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.MERGE_LANES, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_rec_straight_only), a0.getDrawable(R.drawable.icon_lg_rec_straight_only));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_rec_slight_right_only), a0.getDrawable(R.drawable.icon_lg_rec_slight_right_only));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_rec_right_only), a0.getDrawable(R.drawable.icon_lg_rec_right_only));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_rec_heavy_right_only), a0.getDrawable(R.drawable.icon_lg_rec_heavy_right_only));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_rec_uturn_right_only), a0.getDrawable(R.drawable.icon_lg_rec_uturn_right_only));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_rec_uturn_left_only), a0.getDrawable(R.drawable.icon_lg_rec_uturn_left_only));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_rec_heavy_left_only), a0.getDrawable(R.drawable.icon_lg_rec_heavy_left_only));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_rec_left_only), a0.getDrawable(R.drawable.icon_lg_rec_left_only));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_rec_slight_left_only), a0.getDrawable(R.drawable.icon_lg_rec_slight_left_only));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_glance_whatsapp), a0.getDrawable(R.drawable.icon_glance_whatsapp));
                    sSingleNotRecommendedDirection = new java.util.HashMap();
                    sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.STRAIGHT, Integer.valueOf(R.drawable.icon_lg_straight_only));
                    sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SLIGHTLY_RIGHT, Integer.valueOf(R.drawable.icon_lg_slight_right_only));
                    sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.RIGHT, Integer.valueOf(R.drawable.icon_lg_right_only));
                    sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SHARP_RIGHT, Integer.valueOf(R.drawable.icon_lg_heavy_right_only));
                    sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.U_TURN_RIGHT, Integer.valueOf(R.drawable.icon_lg_uturn_right_only));
                    sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SECOND_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.MERGE_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.U_TURN_LEFT, Integer.valueOf(R.drawable.icon_lg_uturn_left_only));
                    sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SHARP_LEFT, Integer.valueOf(R.drawable.icon_lg_heavy_left_only));
                    sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.LEFT, Integer.valueOf(R.drawable.icon_lg_left_only));
                    sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SLIGHTLY_LEFT, Integer.valueOf(R.drawable.icon_lg_slight_left_only));
                    sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SECOND_LEFT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.MERGE_LEFT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sSingleNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.MERGE_LANES, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_straight_only), a0.getDrawable(R.drawable.icon_lg_straight_only));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_slight_right_only), a0.getDrawable(R.drawable.icon_lg_slight_right_only));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_right_only), a0.getDrawable(R.drawable.icon_lg_right_only));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_heavy_right_only), a0.getDrawable(R.drawable.icon_lg_heavy_right_only));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_uturn_right_only), a0.getDrawable(R.drawable.icon_lg_uturn_right_only));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_uturn_left_only), a0.getDrawable(R.drawable.icon_lg_uturn_left_only));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_heavy_left_only), a0.getDrawable(R.drawable.icon_lg_heavy_left_only));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_left_only), a0.getDrawable(R.drawable.icon_lg_left_only));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_slight_left_only), a0.getDrawable(R.drawable.icon_lg_slight_left_only));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_glance_whatsapp), a0.getDrawable(R.drawable.icon_glance_whatsapp));
                    sRightOnlyRecommendedDirection = new java.util.HashMap();
                    sRightOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.STRAIGHT, Integer.valueOf(R.drawable.icon_lg_compright_rec_straight));
                    sRightOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SLIGHTLY_RIGHT, Integer.valueOf(R.drawable.icon_lg_compright_rec_slight_right));
                    sRightOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.RIGHT, Integer.valueOf(R.drawable.icon_lg_compright_rec_right));
                    sRightOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SHARP_RIGHT, Integer.valueOf(R.drawable.icon_lg_compright_rec_heavy_right));
                    sRightOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.U_TURN_RIGHT, Integer.valueOf(R.drawable.icon_lg_compright_rec_uturn_right));
                    sRightOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SECOND_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sRightOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.MERGE_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compright_rec_straight), a0.getDrawable(R.drawable.icon_lg_compright_rec_straight));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compright_rec_slight_right), a0.getDrawable(R.drawable.icon_lg_compright_rec_slight_right));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compright_rec_right), a0.getDrawable(R.drawable.icon_lg_compright_rec_right));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compright_rec_heavy_right), a0.getDrawable(R.drawable.icon_lg_compright_rec_heavy_right));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compright_rec_uturn_right), a0.getDrawable(R.drawable.icon_lg_compright_rec_uturn_right));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_glance_whatsapp), a0.getDrawable(R.drawable.icon_glance_whatsapp));
                    sRightOnlyNotRecommendedDirection = new java.util.HashMap();
                    sRightOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.STRAIGHT, Integer.valueOf(R.drawable.icon_lg_compright_straight));
                    sRightOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SLIGHTLY_RIGHT, Integer.valueOf(R.drawable.icon_lg_compright_slight_right));
                    sRightOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.RIGHT, Integer.valueOf(R.drawable.icon_lg_compright_right));
                    sRightOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SHARP_RIGHT, Integer.valueOf(R.drawable.icon_lg_compright_heavy_right));
                    sRightOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.U_TURN_RIGHT, Integer.valueOf(R.drawable.icon_lg_compright_uturn_right));
                    sRightOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SECOND_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sRightOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.MERGE_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compright_straight), a0.getDrawable(R.drawable.icon_lg_compright_straight));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compright_slight_right), a0.getDrawable(R.drawable.icon_lg_compright_slight_right));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compright_right), a0.getDrawable(R.drawable.icon_lg_compright_right));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compright_heavy_right), a0.getDrawable(R.drawable.icon_lg_compright_heavy_right));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compright_uturn_right), a0.getDrawable(R.drawable.icon_lg_compright_uturn_right));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_glance_whatsapp), a0.getDrawable(R.drawable.icon_glance_whatsapp));
                    sLeftOnlyRecommendedDirection = new java.util.HashMap();
                    sLeftOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.STRAIGHT, Integer.valueOf(R.drawable.icon_lg_compleft_rec_straight));
                    sLeftOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SLIGHTLY_LEFT, Integer.valueOf(R.drawable.icon_lg_compleft_rec_slight_left));
                    sLeftOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.LEFT, Integer.valueOf(R.drawable.icon_lg_compleft_rec_left));
                    sLeftOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SHARP_LEFT, Integer.valueOf(R.drawable.icon_lg_compleft_rec_heavy_left));
                    sLeftOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.U_TURN_LEFT, Integer.valueOf(R.drawable.icon_lg_compleft_rec_uturn_left));
                    sLeftOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SECOND_LEFT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sLeftOnlyRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.MERGE_LEFT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compleft_rec_straight), a0.getDrawable(R.drawable.icon_lg_compleft_rec_straight));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compleft_rec_slight_left), a0.getDrawable(R.drawable.icon_lg_compleft_rec_slight_left));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compleft_rec_left), a0.getDrawable(R.drawable.icon_lg_compleft_rec_left));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compleft_rec_heavy_left), a0.getDrawable(R.drawable.icon_lg_compleft_rec_heavy_left));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compleft_rec_uturn_left), a0.getDrawable(R.drawable.icon_lg_compleft_rec_uturn_left));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_glance_whatsapp), a0.getDrawable(R.drawable.icon_glance_whatsapp));
                    sLeftOnlyNotRecommendedDirection = new java.util.HashMap();
                    sLeftOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.STRAIGHT, Integer.valueOf(R.drawable.icon_lg_compleft_straight));
                    sLeftOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SLIGHTLY_LEFT, Integer.valueOf(R.drawable.icon_lg_compleft_slight_left));
                    sLeftOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.LEFT, Integer.valueOf(R.drawable.icon_lg_compleft_left));
                    sLeftOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SHARP_LEFT, Integer.valueOf(R.drawable.icon_lg_compleft_heavy_left));
                    sLeftOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.U_TURN_LEFT, Integer.valueOf(R.drawable.icon_lg_compleft_uturn_left));
                    sLeftOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SECOND_LEFT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sLeftOnlyNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.MERGE_LEFT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compleft_straight), a0.getDrawable(R.drawable.icon_lg_compleft_straight));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compleft_slight_left), a0.getDrawable(R.drawable.icon_lg_compleft_slight_left));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compleft_left), a0.getDrawable(R.drawable.icon_lg_compleft_left));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compleft_heavy_left), a0.getDrawable(R.drawable.icon_lg_compleft_heavy_left));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compleft_uturn_left), a0.getDrawable(R.drawable.icon_lg_compleft_uturn_left));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_glance_whatsapp), a0.getDrawable(R.drawable.icon_glance_whatsapp));
                    sLeftRightRecommendedDirection = new java.util.HashMap();
                    sLeftRightRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.STRAIGHT, Integer.valueOf(R.drawable.icon_lg_compboth_rec_straight));
                    sLeftRightRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SLIGHTLY_RIGHT, Integer.valueOf(R.drawable.icon_lg_compboth_rec_slight_right));
                    sLeftRightRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.RIGHT, Integer.valueOf(R.drawable.icon_lg_compboth_rec_right));
                    sLeftRightRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SHARP_RIGHT, Integer.valueOf(R.drawable.icon_lg_compboth_rec_heavy_right));
                    sLeftRightRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SECOND_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sLeftRightRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.MERGE_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sLeftRightRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SLIGHTLY_LEFT, Integer.valueOf(R.drawable.icon_lg_compboth_rec_slight_left));
                    sLeftRightRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.LEFT, Integer.valueOf(R.drawable.icon_lg_compboth_rec_left));
                    sLeftRightRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SHARP_LEFT, Integer.valueOf(R.drawable.icon_lg_compboth_rec_heavy_left));
                    sLeftRightRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.MERGE_LEFT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_rec_straight), a0.getDrawable(R.drawable.icon_lg_compboth_rec_straight));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_rec_slight_right), a0.getDrawable(R.drawable.icon_lg_compboth_rec_slight_right));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_rec_right), a0.getDrawable(R.drawable.icon_lg_compboth_rec_right));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_rec_heavy_right), a0.getDrawable(R.drawable.icon_lg_compboth_rec_heavy_right));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_rec_slight_left), a0.getDrawable(R.drawable.icon_lg_compboth_rec_slight_left));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_rec_left), a0.getDrawable(R.drawable.icon_lg_compboth_rec_left));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_rec_heavy_left), a0.getDrawable(R.drawable.icon_lg_compboth_rec_heavy_left));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_glance_whatsapp), a0.getDrawable(R.drawable.icon_glance_whatsapp));
                    sLeftRightNotRecommendedDirection = new java.util.HashMap();
                    sLeftRightNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.STRAIGHT, Integer.valueOf(R.drawable.icon_lg_compboth_straight));
                    sLeftRightNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SLIGHTLY_RIGHT, Integer.valueOf(R.drawable.icon_lg_compboth_slight_right));
                    sLeftRightNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.RIGHT, Integer.valueOf(R.drawable.icon_lg_compboth_right));
                    sLeftRightNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SHARP_RIGHT, Integer.valueOf(R.drawable.icon_lg_compboth_heavy_right));
                    sLeftRightNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.MERGE_RIGHT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sLeftRightNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SLIGHTLY_LEFT, Integer.valueOf(R.drawable.icon_lg_compboth_slight_left));
                    sLeftRightNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.LEFT, Integer.valueOf(R.drawable.icon_lg_compboth_left));
                    sLeftRightNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.SHARP_LEFT, Integer.valueOf(R.drawable.icon_lg_compboth_heavy_left));
                    sLeftRightNotRecommendedDirection.put(com.here.android.mpa.guidance.LaneInformation$Direction.MERGE_LEFT, Integer.valueOf(R.drawable.icon_glance_whatsapp));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_straight), a0.getDrawable(R.drawable.icon_lg_compboth_straight));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_slight_right), a0.getDrawable(R.drawable.icon_lg_compboth_slight_right));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_right), a0.getDrawable(R.drawable.icon_lg_compboth_right));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_heavy_right), a0.getDrawable(R.drawable.icon_lg_compboth_heavy_right));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_slight_left), a0.getDrawable(R.drawable.icon_lg_compboth_slight_left));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_left), a0.getDrawable(R.drawable.icon_lg_compboth_left));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_lg_compboth_heavy_left), a0.getDrawable(R.drawable.icon_lg_compboth_heavy_left));
                    sLaneDrawableCache.put(Integer.valueOf(R.drawable.icon_glance_whatsapp), a0.getDrawable(R.drawable.icon_glance_whatsapp));
                //} catch(Throwable a1) {
                //    a = a1;
                //    break label0;
                //}
                //break label1;
            }
            /*monexit(com.navdy.hud.app.maps.here.HereLaneInfoBuilder.class)*/;
            //throw a;
        }
        /*monexit(com.navdy.hud.app.maps.here.HereLaneInfoBuilder.class)*/;
    }
}
