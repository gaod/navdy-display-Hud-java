package com.navdy.hud.app.maps.here;

public class HerePlacesManager {
    final private static java.util.ArrayList EMPTY_AUTO_COMPLETE_RESULT;
    final private static java.util.ArrayList EMPTY_RESULT;
    final public static String HERE_PLACE_TYPE_ATM = "atm-bank-exchange";
    final public static String HERE_PLACE_TYPE_COFFEE = "coffee-tea";
    final public static String HERE_PLACE_TYPE_GAS = "petrol-station";
    final public static String HERE_PLACE_TYPE_HOSPITAL = "hospital-health-care-facility";
    final public static String HERE_PLACE_TYPE_PARKING = "parking-facility";
    final public static String[] HERE_PLACE_TYPE_RESTAURANT;
    final public static String HERE_PLACE_TYPE_STORE = "shopping";
    final private static int MAX_RESULTS_AUTOCOMPLETE = 10;
    final private static int MAX_RESULTS_SEARCH = 15;
    final private static android.content.Context context;
    final private static com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager;
    final private static com.navdy.service.library.log.Logger sLogger;
    private static volatile boolean willRestablishOnline;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HerePlacesManager.class);
        EMPTY_RESULT = new java.util.ArrayList(0);
        EMPTY_AUTO_COMPLETE_RESULT = new java.util.ArrayList(0);
        hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
        context = com.navdy.hud.app.HudApplication.getAppContext();
        String[] a = new String[2];
        a[0] = "restaurant";
        a[1] = "snacks-fast-food";
        HERE_PLACE_TYPE_RESTAURANT = a;
    }
    
    public HerePlacesManager() {
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static com.navdy.hud.app.maps.here.HereMapsManager access$100() {
        return hereMapsManager;
    }
    
    static void access$1000() {
        com.navdy.hud.app.maps.here.HerePlacesManager.tryRestablishOnline();
    }
    
    static void access$1100(java.util.List a, com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener a0) {
        com.navdy.hud.app.maps.here.HerePlacesManager.handlePlaceLinksRequest(a, a0);
    }
    
    static void access$1200(java.util.List a, com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener a0) {
        com.navdy.hud.app.maps.here.HerePlacesManager.invokeCategorySearchListener(a, a0);
    }
    
    static android.content.Context access$200() {
        return context;
    }
    
    static void access$300(String s, com.navdy.service.library.events.RequestStatus a, String s0) {
        com.navdy.hud.app.maps.here.HerePlacesManager.returnSearchErrorResponse(s, a, s0);
    }
    
    static java.util.ArrayList access$400() {
        return EMPTY_RESULT;
    }
    
    static void access$500(com.here.android.mpa.common.GeoCoordinate a, String s, java.util.List a0, int i, int i0) {
        com.navdy.hud.app.maps.here.HerePlacesManager.returnSearchResults(a, s, a0, i, i0);
    }
    
    static void access$600(String s, com.navdy.service.library.events.RequestStatus a, String s0) {
        com.navdy.hud.app.maps.here.HerePlacesManager.returnAutoCompleteErrorResponse(s, a, s0);
    }
    
    static java.util.ArrayList access$700() {
        return EMPTY_AUTO_COMPLETE_RESULT;
    }
    
    static void access$800(String s, int i, java.util.List a) {
        com.navdy.hud.app.maps.here.HerePlacesManager.returnAutoCompleteResults(s, i, a);
    }
    
    static void access$900() {
        com.navdy.hud.app.maps.here.HerePlacesManager.establishOffline();
    }
    
    public static void autoComplete(com.here.android.mpa.common.GeoCoordinate a, String s, int i, int i0, com.navdy.hud.app.maps.here.HerePlacesManager$AutoCompleteCallback a0) {
        com.here.android.mpa.search.TextSuggestionRequest a1 = new com.here.android.mpa.search.TextSuggestionRequest(s);
        a1.setCollectionSize(i0);
        a1.setSearchCenter(a);
        com.here.android.mpa.search.ErrorCode a2 = a1.execute((com.here.android.mpa.search.ResultListener)new com.navdy.hud.app.maps.here.HerePlacesManager$3(a0));
        com.here.android.mpa.search.ErrorCode a3 = com.here.android.mpa.search.ErrorCode.NONE;
        label0: {
            Throwable a4 = null;
            if (a2 == a3) {
                break label0;
            }
            try {
                a0.result(a2, (java.util.List)null);
                break label0;
            } catch(Throwable a5) {
                a4 = a5;
            }
            sLogger.e(a4);
        }
    }
    
    private static void establishOffline() {
        willRestablishOnline = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isEngineOnline();
        if (willRestablishOnline) {
            sLogger.v("turn engine offline");
            com.navdy.hud.app.maps.here.HereMapsManager.getInstance().turnOffline();
        }
    }
    
    public static com.here.android.mpa.search.GeocodeRequest geoCodeStreetAddress(com.here.android.mpa.common.GeoCoordinate a, String s, int i, com.navdy.hud.app.maps.here.HerePlacesManager$GeoCodeCallback a0) {
        com.here.android.mpa.search.GeocodeRequest a1 = new com.here.android.mpa.search.GeocodeRequest(s);
        if (a != null) {
            a1.setSearchArea(a, i);
        }
        com.here.android.mpa.search.ErrorCode a2 = a1.execute((com.here.android.mpa.search.ResultListener)new com.navdy.hud.app.maps.here.HerePlacesManager$2(a0));
        if (a2 != com.here.android.mpa.search.ErrorCode.NONE) {
            try {
                a0.result(a2, (java.util.List)null);
            } catch(Throwable a3) {
                sLogger.e(a3);
            }
            a1 = null;
        }
        return a1;
    }
    
    public static com.here.android.mpa.common.GeoCoordinate getPlaceEntry(com.here.android.mpa.search.Place a) {
        return (a.getLocation().getAccessPoints().size() <= 0) ? a.getLocation().getCoordinate() : ((com.here.android.mpa.search.NavigationPosition)a.getLocation().getAccessPoints().get(0)).getCoordinate();
    }
    
    public static void handleAutoCompleteRequest(com.navdy.service.library.events.places.AutoCompleteRequest a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HerePlacesManager$5(a), 19);
    }
    
    public static void handleCategoriesRequest(com.here.android.mpa.search.CategoryFilter a, int i, com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener a0) {
        com.navdy.hud.app.maps.here.HerePlacesManager.handleCategoriesRequest(a, i, (com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener)new com.navdy.hud.app.maps.here.HerePlacesManager$6(a0, a, i), false);
    }
    
    public static void handleCategoriesRequest(com.here.android.mpa.search.CategoryFilter a, int i, com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener a0, boolean b) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HerePlacesManager$7(a, i, a0, b), 2);
    }
    
    private static void handlePlaceLinksRequest(java.util.List a, com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener a0) {
        java.util.ArrayList a1 = new java.util.ArrayList();
        java.util.concurrent.atomic.AtomicInteger a2 = new java.util.concurrent.atomic.AtomicInteger(a.size());
        Object a3 = a.iterator();
        Object a4 = a0;
        while(((java.util.Iterator)a3).hasNext()) {
            com.here.android.mpa.search.ErrorCode a5 = ((com.here.android.mpa.search.PlaceLink)((java.util.Iterator)a3).next()).getDetailsRequest().execute((com.here.android.mpa.search.ResultListener)new com.navdy.hud.app.maps.here.HerePlacesManager$8(a2, (java.util.List)a1, (com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener)a4));
            if (a5 != com.here.android.mpa.search.ErrorCode.NONE) {
                sLogger.e(new StringBuilder().append("Error in place detail request: ").append(a5.name()).toString());
                if (a2.decrementAndGet() == 0 && ((java.util.List)a1).size() == 0) {
                    com.navdy.hud.app.maps.here.HerePlacesManager.tryRestablishOnline();
                    ((com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener)a4).onError(com.navdy.hud.app.maps.here.HerePlacesManager$Error.BAD_REQUEST);
                }
            }
        }
    }
    
    public static void handlePlacesSearchRequest(com.navdy.service.library.events.places.PlacesSearchRequest a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HerePlacesManager$4(a), 19);
    }
    
    private static void invokeCategorySearchListener(java.util.List a, com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener a0) {
        int i = a.size();
        if (i != 0) {
            sLogger.v(new StringBuilder().append("places query complete:").append(i).toString());
            com.navdy.hud.app.maps.here.HerePlacesManager.tryRestablishOnline();
            a0.onCompleted(a);
        } else {
            sLogger.v("places query complete, no result");
            com.navdy.hud.app.maps.here.HerePlacesManager.tryRestablishOnline();
            a0.onError(com.navdy.hud.app.maps.here.HerePlacesManager$Error.RESPONSE_ERROR);
        }
    }
    
    public static void printAutoCompleteRequest(com.navdy.service.library.events.places.AutoCompleteRequest a) {
        try {
            sLogger.v(new StringBuilder().append("AutoCompleteRequest query[").append(a.partialSearch).append("] maxResults[").append(a.maxResults).append("]").toString());
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
    }
    
    public static void printSearchRequest(com.navdy.service.library.events.places.PlacesSearchRequest a) {
        try {
            sLogger.v(new StringBuilder().append("PlacesSearchRequest query[").append(a.searchQuery).append("] area[").append(a.searchArea).append("] maxResults[").append(a.maxResults).append("]").toString());
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
    }
    
    private static void returnAutoCompleteErrorResponse(String s, com.navdy.service.library.events.RequestStatus a, String s0) {
        try {
            sLogger.e(new StringBuilder().append("[autocomplete]").append(a).append(": ").append(s0).toString());
            com.navdy.hud.app.maps.MapsEventHandler.getInstance().sendEventToClient((com.squareup.wire.Message)new com.navdy.service.library.events.places.AutoCompleteResponse(s, a, s0, (java.util.List)null));
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
    }
    
    private static void returnAutoCompleteResults(String s, int i, java.util.List a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HerePlacesManager$10(a, i, s), 2);
    }
    
    private static void returnSearchErrorResponse(String s, com.navdy.service.library.events.RequestStatus a, String s0) {
        try {
            sLogger.e(new StringBuilder().append("[search]").append(a).append(": ").append(s0).toString());
            com.navdy.service.library.events.places.PlacesSearchResponse a0 = new com.navdy.service.library.events.places.PlacesSearchResponse$Builder().searchQuery(s).status(a).statusDetail(s0).results((java.util.List)null).build();
            com.navdy.hud.app.maps.MapsEventHandler.getInstance().sendEventToClient((com.squareup.wire.Message)a0);
        } catch(Throwable a1) {
            sLogger.e(a1);
        }
    }
    
    private static void returnSearchResults(com.here.android.mpa.common.GeoCoordinate a, String s, java.util.List a0, int i, int i0) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HerePlacesManager$9(i, a0, a, i0, s), 2);
    }
    
    public static void searchPlaces(com.here.android.mpa.common.GeoCoordinate a, String s, int i, int i0, com.navdy.hud.app.maps.here.HerePlacesManager$PlacesSearchCallback a0) {
        com.here.android.mpa.search.SearchRequest a1 = new com.here.android.mpa.search.SearchRequest(s);
        a1.setSearchCenter(a);
        a1.setCollectionSize(i0);
        com.here.android.mpa.search.ErrorCode a2 = a1.execute((com.here.android.mpa.search.ResultListener)new com.navdy.hud.app.maps.here.HerePlacesManager$1(a0));
        com.here.android.mpa.search.ErrorCode a3 = com.here.android.mpa.search.ErrorCode.NONE;
        label0: {
            Throwable a4 = null;
            if (a2 == a3) {
                break label0;
            }
            try {
                a0.result(a2, (com.here.android.mpa.search.DiscoveryResultPage)null);
                break label0;
            } catch(Throwable a5) {
                a4 = a5;
            }
            sLogger.e(a4);
        }
    }
    
    private static void tryRestablishOnline() {
        if (willRestablishOnline && !com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isEngineOnline() && com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected() && com.navdy.hud.app.framework.network.NetworkStateManager.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext())) {
            sLogger.v("turn engine online");
            com.navdy.hud.app.maps.here.HereMapsManager.getInstance().turnOnline();
        }
        willRestablishOnline = false;
    }
}
