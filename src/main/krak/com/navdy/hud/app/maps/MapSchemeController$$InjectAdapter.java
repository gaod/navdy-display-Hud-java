package com.navdy.hud.app.maps;

final public class MapSchemeController$$InjectAdapter extends dagger.internal.Binding implements javax.inject.Provider, dagger.MembersInjector {
    private dagger.internal.Binding sharedPreferences;
    
    public MapSchemeController$$InjectAdapter() {
        super("com.navdy.hud.app.maps.MapSchemeController", "members/com.navdy.hud.app.maps.MapSchemeController", false, com.navdy.hud.app.maps.MapSchemeController.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.sharedPreferences = a.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.maps.MapSchemeController.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.maps.MapSchemeController get() {
        com.navdy.hud.app.maps.MapSchemeController a = new com.navdy.hud.app.maps.MapSchemeController();
        this.injectMembers(a);
        return a;
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.sharedPreferences);
    }
    
    public void injectMembers(com.navdy.hud.app.maps.MapSchemeController a) {
        a.sharedPreferences = (android.content.SharedPreferences)this.sharedPreferences.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.maps.MapSchemeController)a);
    }
}
