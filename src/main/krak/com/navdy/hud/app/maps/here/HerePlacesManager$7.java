package com.navdy.hud.app.maps.here;

final class HerePlacesManager$7 implements Runnable {
    final com.here.android.mpa.search.CategoryFilter val$filter;
    final com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener val$listener;
    final int val$nResults;
    final boolean val$offline;
    
    HerePlacesManager$7(com.here.android.mpa.search.CategoryFilter a, int i, com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener a0, boolean b) {
        super();
        this.val$filter = a;
        this.val$nResults = i;
        this.val$listener = a0;
        this.val$offline = b;
    }
    
    public void run() {
        Throwable a = null;
        long j = android.os.SystemClock.elapsedRealtime();
        com.here.android.mpa.search.CategoryFilter a0 = this.val$filter;
        label0: {
            if (a0 != null && this.val$nResults > 0 && this.val$listener != null) {
                label12: {
                    Throwable a1 = null;
                    label1: {
                        label9: {
                            label7: {
                                label5: {
                                    label2: {
                                        label11: try {
                                            boolean b = com.navdy.hud.app.maps.here.HerePlacesManager.access$100().isInitialized();
                                            label10: {
                                                if (b) {
                                                    break label10;
                                                }
                                                com.navdy.hud.app.maps.here.HerePlacesManager.access$000().i("engine not ready");
                                                this.val$listener.onError(com.navdy.hud.app.maps.here.HerePlacesManager$Error.NO_MAP_ENGINE);
                                                break label11;
                                            }
                                            com.navdy.hud.app.maps.here.HereLocationFixManager a2 = com.navdy.hud.app.maps.here.HerePlacesManager.access$100().getLocationFixManager();
                                            label8: {
                                                if (a2 != null) {
                                                    break label8;
                                                }
                                                com.navdy.hud.app.maps.here.HerePlacesManager.access$000().i("engine not ready, location fix manager n/a");
                                                this.val$listener.onError(com.navdy.hud.app.maps.here.HerePlacesManager$Error.NO_USER_LOCATION);
                                                break label9;
                                            }
                                            com.here.android.mpa.common.GeoCoordinate a3 = com.navdy.hud.app.maps.here.HerePlacesManager.access$100().getRouteStartPoint();
                                            if (a3 != null) {
                                                com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("using debug start point:").append(a3).toString());
                                            } else {
                                                a3 = a2.getLastGeoCoordinate();
                                            }
                                            label6: {
                                                if (a3 != null) {
                                                    break label6;
                                                }
                                                com.navdy.hud.app.maps.here.HerePlacesManager.access$000().i("start location n/a");
                                                this.val$listener.onError(com.navdy.hud.app.maps.here.HerePlacesManager$Error.NO_USER_LOCATION);
                                                break label7;
                                            }
                                            boolean b0 = this.val$offline;
                                            label3: {
                                                label4: {
                                                    if (!b0) {
                                                        break label4;
                                                    }
                                                    com.navdy.hud.app.maps.here.HerePlacesManager.access$900();
                                                    break label3;
                                                }
                                                if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isEngineOnline()) {
                                                    break label3;
                                                }
                                                this.val$listener.onError(com.navdy.hud.app.maps.here.HerePlacesManager$Error.BAD_REQUEST);
                                                break label5;
                                            }
                                            com.here.android.mpa.search.ErrorCode a4 = new com.here.android.mpa.search.ExploreRequest().setCategoryFilter(this.val$filter).setSearchCenter(a3).setCollectionSize(this.val$nResults).execute((com.here.android.mpa.search.ResultListener)new com.navdy.hud.app.maps.here.HerePlacesManager$7$1(this, j));
                                            if (a4 == com.here.android.mpa.search.ErrorCode.NONE) {
                                                break label2;
                                            }
                                            com.navdy.hud.app.maps.here.HerePlacesManager.access$000().e(new StringBuilder().append("Error while requesting nearby gas stations: ").append(a4.name()).toString());
                                            com.navdy.hud.app.maps.here.HerePlacesManager.access$1000();
                                            this.val$listener.onError(com.navdy.hud.app.maps.here.HerePlacesManager$Error.BAD_REQUEST);
                                            break label2;
                                        } catch(Throwable a5) {
                                            a1 = a5;
                                            break label1;
                                        }
                                        long j0 = android.os.SystemClock.elapsedRealtime();
                                        com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("handleCategoriesRequest time-1=").append(j0 - j).toString());
                                        break label12;
                                    }
                                    long j1 = android.os.SystemClock.elapsedRealtime();
                                    com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("handleCategoriesRequest time-1=").append(j1 - j).toString());
                                    break label12;
                                }
                                long j2 = android.os.SystemClock.elapsedRealtime();
                                com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("handleCategoriesRequest time-1=").append(j2 - j).toString());
                                break label12;
                            }
                            long j3 = android.os.SystemClock.elapsedRealtime();
                            com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("handleCategoriesRequest time-1=").append(j3 - j).toString());
                            break label12;
                        }
                        long j4 = android.os.SystemClock.elapsedRealtime();
                        com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("handleCategoriesRequest time-1=").append(j4 - j).toString());
                        break label12;
                    }
                    try {
                        com.navdy.hud.app.maps.here.HerePlacesManager.access$000().e("HERE internal DiscoveryRequest.execute exception: ", a1);
                        com.navdy.hud.app.maps.here.HerePlacesManager.access$1000();
                        this.val$listener.onError(com.navdy.hud.app.maps.here.HerePlacesManager$Error.UNKNOWN_ERROR);
                    } catch(Throwable a6) {
                        a = a6;
                        break label0;
                    }
                    long j5 = android.os.SystemClock.elapsedRealtime();
                    com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("handleCategoriesRequest time-1=").append(j5 - j).toString());
                }
                return;
            }
            throw new IllegalArgumentException();
        }
        long j6 = android.os.SystemClock.elapsedRealtime();
        com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("handleCategoriesRequest time-1=").append(j6 - j).toString());
        throw a;
    }
}
