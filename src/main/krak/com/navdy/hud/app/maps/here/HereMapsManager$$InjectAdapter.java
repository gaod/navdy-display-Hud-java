package com.navdy.hud.app.maps.here;

final public class HereMapsManager$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding mDriverProfileManager;
    private dagger.internal.Binding powerManager;
    private dagger.internal.Binding sharedPreferences;
    
    public HereMapsManager$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.maps.here.HereMapsManager", false, com.navdy.hud.app.maps.here.HereMapsManager.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.maps.here.HereMapsManager.class, (this).getClass().getClassLoader());
        this.powerManager = a.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.maps.here.HereMapsManager.class, (this).getClass().getClassLoader());
        this.sharedPreferences = a.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.maps.here.HereMapsManager.class, (this).getClass().getClassLoader());
        this.mDriverProfileManager = a.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", com.navdy.hud.app.maps.here.HereMapsManager.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.bus);
        a0.add(this.powerManager);
        a0.add(this.sharedPreferences);
        a0.add(this.mDriverProfileManager);
    }
    
    public void injectMembers(com.navdy.hud.app.maps.here.HereMapsManager a) {
        a.bus = (com.squareup.otto.Bus)this.bus.get();
        a.powerManager = (com.navdy.hud.app.device.PowerManager)this.powerManager.get();
        a.sharedPreferences = (android.content.SharedPreferences)this.sharedPreferences.get();
        a.mDriverProfileManager = (com.navdy.hud.app.profile.DriverProfileManager)this.mDriverProfileManager.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.maps.here.HereMapsManager)a);
    }
}
