package com.navdy.hud.app;
import com.navdy.hud.app.R;

final public class R$plurals {
    final public static int common_days_1d = R.plurals.common_days_1d;
    final public static int episodes_count = R.plurals.episodes_count;
    final public static int songs_count = R.plurals.songs_count;
    
    public R$plurals() {
    }
}
