package com.navdy.hud.app.debug;

class DriveRecorder$3 implements Runnable {
    final com.navdy.hud.app.debug.DriveRecorder this$0;
    final String val$fileName;
    
    DriveRecorder$3(com.navdy.hud.app.debug.DriveRecorder a, String s) {
        super();
        this.this$0 = a;
        this.val$fileName = s;
    }
    
    public void run() {
        boolean b = this.this$0.bPrepare(this.val$fileName);
        label0: {
            Throwable a = null;
            label1: if (b) {
                com.navdy.hud.app.debug.DriveRecorder.sLogger.d("Prepare succeeded");
                try {
                    com.navdy.hud.app.debug.DriveRecorder.access$202(this.this$0, 0);
                    com.navdy.hud.app.debug.DriveRecorder.access$302(this.this$0, new android.os.HandlerThread("ObdPlayback"));
                    com.navdy.hud.app.debug.DriveRecorder.access$300(this.this$0).start();
                    com.navdy.hud.app.debug.DriveRecorder.access$402(this.this$0, new android.os.Handler(com.navdy.hud.app.debug.DriveRecorder.access$300(this.this$0).getLooper()));
                    com.navdy.hud.app.debug.DriveRecorder.access$400(this.this$0).post(com.navdy.hud.app.debug.DriveRecorder.access$500(this.this$0));
                } catch(Throwable a0) {
                    a = a0;
                    break label1;
                }
                break label0;
            } else {
                com.navdy.hud.app.debug.DriveRecorder.sLogger.e(new StringBuilder().append("Failed to prepare for playback , file : ").append(this.val$fileName).toString());
                this.this$0.stopPlayback();
                break label0;
            }
            this.this$0.stopPlayback();
            com.navdy.hud.app.debug.DriveRecorder.sLogger.e(a);
        }
    }
}
