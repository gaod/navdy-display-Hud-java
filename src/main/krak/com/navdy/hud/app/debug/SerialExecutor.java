package com.navdy.hud.app.debug;

abstract public interface SerialExecutor {
    abstract public void execute(Runnable arg);
}
