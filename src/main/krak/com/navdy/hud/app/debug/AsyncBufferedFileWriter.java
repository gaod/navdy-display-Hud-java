package com.navdy.hud.app.debug;

final public class AsyncBufferedFileWriter {
    final private java.io.BufferedWriter bufferedWriter;
    final private com.navdy.hud.app.debug.SerialExecutor executor;
    
    public AsyncBufferedFileWriter(String s, com.navdy.hud.app.debug.SerialExecutor a, int i) {
        super();
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s, "filePath");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "executor");
        this.executor = a;
        this.bufferedWriter = new java.io.BufferedWriter((java.io.Writer)new java.io.OutputStreamWriter((java.io.OutputStream)new java.io.FileOutputStream(new java.io.File(s)), "utf-8"), i);
    }
    
    public static void write$default(com.navdy.hud.app.debug.AsyncBufferedFileWriter a, String s, boolean b, int i, Object a0) {
        if ((i & 2) != 0) {
            b = false;
        }
        a.write(s, b);
    }
    
    final public void flush() {
        this.executor.execute((Runnable)new com.navdy.hud.app.debug.AsyncBufferedFileWriter$flush$1(this));
    }
    
    final public void flushAndClose() {
        this.executor.execute((Runnable)new com.navdy.hud.app.debug.AsyncBufferedFileWriter$flushAndClose$1(this));
    }
    
    final public java.io.BufferedWriter getBufferedWriter() {
        return this.bufferedWriter;
    }
    
    final public void write(String s) {
        com.navdy.hud.app.debug.AsyncBufferedFileWriter.write$default(this, s, false, 2, null);
    }
    
    final public void write(String s, boolean b) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s, "data");
        this.executor.execute((Runnable)new com.navdy.hud.app.debug.AsyncBufferedFileWriter$write$1(this, s, b));
    }
}
