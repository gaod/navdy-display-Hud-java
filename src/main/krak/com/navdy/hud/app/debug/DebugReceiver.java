package com.navdy.hud.app.debug;

public class DebugReceiver extends android.content.BroadcastReceiver {
    final public static String ACTION_ANR = "com.navdy.app.debug.ANR";
    final public static String ACTION_BROADCAST_ANR = "com.navdy.app.debug.BROADCAST_ANR";
    final public static String ACTION_CHANGE_HEADING = "com.navdy.app.debug.CHANGE_HEADING";
    final public static String ACTION_CHECK_FOR_MAP_UPDATES = "com.navdy.app.debug.CHECK_FOR_MAP_DATA_UPDATE";
    final public static String ACTION_CLEAR_MANEUVER = "com.navdy.app.debug.CLEAR_MANEUVER";
    final public static String ACTION_CRASH = "com.navdy.app.debug.CRASH";
    final public static String ACTION_DNS_LOOKUP = "com.navdy.app.debug.DNS_LOOKUP";
    final public static String ACTION_DNS_LOOKUP_TEST = "com.navdy.app.debug.DNS_LOOKUP_TEST";
    final public static String ACTION_DUMP_THREAD_STACK = "com.navdy.hud.debug.DUMP_THREAD_STACK";
    final public static String ACTION_EJECT_JUNCTION_VIEW = "com.navdy.app.debug.EJECT_JUNC_VIEW";
    final public static String ACTION_GET_GOOGLE = "com.navdy.app.debug.GET_GOOGLE";
    final public static String ACTION_HIDE_LANE_INFO = "com.navdy.app.debug.HIDE_LANE_INFO";
    final public static String ACTION_HIDE_RECALC = "com.navdy.app.debug.HIDE_RECALC";
    final public static String ACTION_INJECT_JUNCTION_VIEW = "com.navdy.app.debug.INJECT_JUNC_VIEW";
    final public static String ACTION_LAUNCH_MUSIC_DETAILS = "com.navdy.app.debug.MUSIC_DETAILS";
    final public static String ACTION_LAUNCH_PICKER = "com.navdy.app.debug.LAUNCH_PICKER";
    final public static String ACTION_NATIVE_CRASH = "com.navdy.app.debug.NATIVE_CRASH";
    final public static String ACTION_NATIVE_CRASH_SEPARATE_THREAD = "com.navdy.app.debug.NATIVE_CRASH_SEPARATE_THREAD";
    final public static String ACTION_NETSTAT = "com.navdy.app.debug.NETSTAT";
    final public static String ACTION_PLAYBACK_ROUTE = "com.navdy.app.debug.PLAYBACK_ROUTE";
    final public static String ACTION_PLAYBACK_ROUTE_SECONDARY = "com.navdy.app.debug.PLAYBACK_ROUTE_SECONDARY";
    final public static String ACTION_PRINT_BUS_REGISTRATION = "com.navdy.app.debug.PRINT_BUS_REGISTRATION";
    final public static String ACTION_PRINT_MAP_NAV_MODE = "com.navdy.app.debug.PRINT_MAP_NAV_MODE";
    final public static String ACTION_PRINT_MAP_ZOOM = "com.navdy.app.debug.PRINT_MAP_ZOOM";
    final public static String ACTION_SCALE_DISPLAY = "com.navdy.app.debug.SCALE_DISPLAY";
    final public static String ACTION_SEND_NOW_MANEUVER = "com.navdy.app.debug.NOW_MANEUVER";
    final public static String ACTION_SEND_SOON_MANEUVER = "com.navdy.app.debug.SOON_MANEUVER";
    final public static String ACTION_SEND_STAY_MANEUVER = "com.navdy.app.debug.STAY_MANEUVER";
    final public static String ACTION_SET_DISTANCE_MANEUVER = "com.navdy.app.debug.DISTANCE_MANEUVER";
    final public static String ACTION_SET_ICON_MANEUVER = "com.navdy.app.debug.ICON_MANEUVER";
    final public static String ACTION_SET_INSTRUCTION_MANEUVER = "com.navdy.app.debug.INSTRUCTION_MANEUVER";
    final public static String ACTION_SET_SIMULATION_SPEED = "com.navdy.app.debug.SET_SIM_SPEED";
    final public static String ACTION_SET_START_ROUTE_CALC_POINT = "com.navdy.app.debug.START_ROUTE_POINT";
    final public static String ACTION_SET_THEN_MANEUVER = "com.navdy.app.debug.THEN_MANEUVER";
    final public static String ACTION_SHOW_LANE_INFO = "com.navdy.app.debug.SHOW_LANE_INFO";
    final public static String ACTION_SHOW_RECALC = "com.navdy.app.debug.SHOW_RECALC";
    final public static String ACTION_START_BUS_PROFILING = "com.navdy.app.debug.START_BUS_PROFILING";
    final public static String ACTION_START_CPU_HOG = "com.navdy.app.debug.START_CPU_HOG";
    final public static String ACTION_START_MAIN_THREAD_PROFILING = "com.navdy.app.debug.START_MAIN_THREAD_PROFILING";
    final public static String ACTION_START_MAP_RENDERING = "com.navdy.app.debug.START_MAP_RENDERING";
    final public static String ACTION_STOP_BUS_PROFILING = "com.navdy.app.debug.STOP_BUS_PROFILING";
    final public static String ACTION_STOP_CPU_HOG = "com.navdy.app.debug.STOP_CPU_HOG";
    final public static String ACTION_STOP_MAIN_THREAD_PROFILING = "com.navdy.app.debug.STOP_MAIN_THREAD_PROFILING";
    final public static String ACTION_STOP_MAP_RENDERING = "com.navdy.app.debug.STOP_MAP_RENDERING";
    final public static String ACTION_STOP_PLAYBACK_ROUTE = "com.navdy.app.debug.STOP_PLAYBACK_ROUTE";
    final public static String ACTION_TEST = "com.navdy.hud.debug.TEST";
    final public static String ACTION_TEST_TBT_TTS = "com.navdy.app.debug.TEST_TBT_TTS";
    final public static String ACTION_TEST_TBT_TTS_CANCEL = "com.navdy.app.debug.TEST_TBT_TTS_CANCEL";
    final public static String ACTION_UPDATE_MAP_DATA = "com.navdy.app.debug.UPDATE_MAP_DATA";
    final public static String ACTION_WAKEUP = "com.navdy.app.debug.WAKEUP";
    final public static int CONNECTION_TIMEOUT_MILLIS = 30000;
    final public static int DESTINATION_SUGGESTION_TYPE_FAVORITE = 2;
    final public static int DESTINATION_SUGGESTION_TYPE_HOME = 0;
    final public static int DESTINATION_SUGGESTION_TYPE_NEW_PLACE = 3;
    final public static int DESTINATION_SUGGESTION_TYPE_WORK = 1;
    final public static String DISABLE_HERE_LOC_DBG = "com.navdy.app.debug.DISABLE_HERE_LOC_DBG";
    final public static String DNS_LOOKUP_HOST_NAME = "DNS_LOOKUP_HOST_NAME";
    final private static boolean ENABLED = true;
    final public static String ENABLE_HERE_LOC_DBG = "com.navdy.app.debug.ENABLE_HERE_LOC_DBG";
    final public static String EXTRA_BANDWIDTH_LEVEL = "BANDWIDTH_LEVEL";
    final public static String EXTRA_COMMAND = "COMMAND";
    final public static String EXTRA_DEEP = "DEEP";
    final public static String EXTRA_DESTINATION_SUGGESTION_TYPE = "DESTINATION_SUGGESTION_TYPE";
    final public static String EXTRA_KEY = "KEY";
    final public static String EXTRA_LIGHT = "LIGHT";
    final public static String EXTRA_MANEUVER_DISTANCE = "EXTRA_MANEUVER_DISTANCE";
    final public static String EXTRA_MANEUVER_ICON = "EXTRA_MANEUVER_ICON";
    final public static String EXTRA_MANEUVER_INSTRUCTION = "EXTRA_MANEUVER_INSTRUCTION";
    final public static String EXTRA_MANEUVER_THEN_ICON = "EXTRA_MANEUVER_THEN_ICON";
    final public static String EXTRA_MODE = "MODE";
    final public static String EXTRA_NOTIFICATION_EVENT_APP_ID = "appId";
    final public static String EXTRA_NOTIFICATION_EVENT_APP_NAME = "appName";
    final public static String EXTRA_NOTIFICATION_EVENT_ID = "id";
    final public static String EXTRA_NOTIFICATION_EVENT_MESSAGE = "message";
    final public static String EXTRA_NOTIFICATION_EVENT_SUB_TITLE = "subtitle";
    final public static String EXTRA_NOTIFICATION_EVENT_TITLE = "title";
    final public static String EXTRA_OBD_CONFIG = "CONFIG";
    final public static String EXTRA_SIZE = "SIZE";
    final public static String EXTRA_TEST = "TEST";
    final public static String EXTRA_URL = "URL";
    final public static String HEADING = "HEADING";
    final private static int MAIN_THREAD_DEFAULT_PROFILING_THRESHOLD = 0;
    final public static String MAIN_THREAD_EXTRA_PROFILING_THRESHOLD = "PROFILING_THRESHOLD";
    final public static int READ_TIMEOUT_MILLIS = 30000;
    final public static String SPEED = "SPEED";
    final public static String TEST_DESTINATION_SUGGESTION = "TEST_DESTINATION_SUGGESTION";
    final public static String TEST_DIAL = "TEST_DIAL";
    final public static String TEST_DISABLE_OBD_PIDS_SCANNING = "TEST_DISABLE_OBD";
    final public static String TEST_ENABLE_OBD_PIDS_SCANNING = "TEST_ENABLE_OBD";
    final public static String TEST_HID = "HID";
    final public static String TEST_LAUNCH_APP = "LAUNCH_APP";
    final public static String TEST_LIGHT = "TEST_LIGHT";
    final public static String TEST_OBD_CONFIG = "TEST_OBD";
    final public static String TEST_OBD_FLASH_NEW_FIRMWARE = "TEST_OBD_FLASH_NEW_FIRMWARE";
    final public static String TEST_OBD_FLASH_OLD_FIRMWARE = "TEST_OBD_FLASH_OLD_FIRMWARE";
    final public static String TEST_OBD_RESET = "TEST_OBD_RESET";
    final public static String TEST_OBD_SET_MODE_J1939 = "TEST_OBD_SET_MODE_J1939";
    final public static String TEST_OBD_SET_MODE_OBD2 = "TEST_OBD_SET_MODE_OBD2";
    final public static String TEST_OBD_SLEEP = "TEST_OBD_SLEEP";
    final public static String TEST_PROXY_FILE_DOWNLOAD = "TEST_PROXY_FILE_DOWNLOAD";
    final public static String TEST_PROXY_FILE_UPLOAD = "TEST_PROXY_FILE_UPLOAD";
    final public static String TEST_SEND_NOTIFICATION_EVENT = "TEST_SEND_NOTIFICATION_EVENT";
    final public static String TEST_SHUT_DOWN = "SHUTDOWN";
    final public static String TEST_SWITCH_BANDWIDTH_LEVEL = "TEST_SWITCH_BANDWIDTH";
    final public static String TEST_TTS = "TEST_TTS";
    final private static String[] TTS_TEXTS;
    final static com.navdy.service.library.log.Logger sLogger;
    @Inject
    com.squareup.otto.Bus mBus;
    private com.navdy.hud.app.debug.DebugReceiver$PrinterImpl mainThreadProfilingPrinter;
    
    static {
        String[] a = new String[5];
        a[0] = "Turn left after 100 feet";
        a[1] = "Turn right after 500 feet. 1, 2, 3, 4, 5, 6, 7, 8,9,10";
        a[2] = "There is a slight traffic ahead, be on the right lane.";
        a[3] = "Hi how are you doing today? Testing a long long long long text";
        a[4] = "Testing testing still testing.";
        TTS_TEXTS = a;
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.debug.DebugReceiver.class);
    }
    
    public DebugReceiver() {
        this.mainThreadProfilingPrinter = new com.navdy.hud.app.debug.DebugReceiver$PrinterImpl((com.navdy.hud.app.debug.DebugReceiver$1)null);
    }
    
    static void access$100(com.navdy.hud.app.debug.DebugReceiver a, int i) {
        a.busywait(i);
    }
    
    private void busywait(int i) {
        long j = android.os.SystemClock.elapsedRealtime();
        long j0 = android.os.SystemClock.elapsedRealtime();
        while(j0 - j < (long)i) {
            j0 = android.os.SystemClock.elapsedRealtime();
        }
    }
    
    private void dnsLookup(String s) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.debug.DebugReceiver$7(this, s), 1);
    }
    
    private void dnsLookupTest() {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.debug.DebugReceiver$8(this), 1);
    }
    
    private void getGoogleHomePage() {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.debug.DebugReceiver$6(this), 1);
    }
    
    public static void setHereDebugLocation(boolean b) {
        try {
            android.os.Looper.class.getDeclaredField("enableHereLocationDebugging").setBoolean(null, b);
            sLogger.v(new StringBuilder().append("setHereDebugLocation set to ").append(b).toString());
        } catch(Throwable a) {
            sLogger.e("setHereDebugLocation", a);
        }
    }
    
    private void testAppLaunch(android.content.Intent a) {
        this.mBus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.input.LaunchAppEvent((String)null)));
    }
    
    private void testDestinationSuggestion(android.content.Intent a) {
        int i = a.getIntExtra("DESTINATION_SUGGESTION_TYPE", 0);
        com.navdy.service.library.events.places.SuggestedDestination a0 = null;
        switch(i) {
            case 3: {
                a0 = new com.navdy.service.library.events.places.SuggestedDestination(new com.navdy.service.library.events.destination.Destination$Builder().navigation_position(new com.navdy.service.library.events.location.LatLong(Double.valueOf(37.7766916), Double.valueOf(-122.3970457))).display_position(new com.navdy.service.library.events.location.LatLong(Double.valueOf(37.7766916), Double.valueOf(-122.3970457))).full_address("700 4th St, San Francisco, CA 94107").destination_title("").destination_subtitle("").favorite_type(com.navdy.service.library.events.destination.Destination$FavoriteType.FAVORITE_NONE).identifier("4").suggestion_type(com.navdy.service.library.events.destination.Destination.DEFAULT_SUGGESTION_TYPE).is_recommendation(Boolean.valueOf(true)).last_navigated_to(Long.valueOf(0L)).build(), Integer.valueOf(-1), com.navdy.service.library.events.places.SuggestedDestination$SuggestionType.SUGGESTION_RECOMMENDATION);
                break;
            }
            case 2: {
                a0 = new com.navdy.service.library.events.places.SuggestedDestination(new com.navdy.service.library.events.destination.Destination$Builder().navigation_position(new com.navdy.service.library.events.location.LatLong(Double.valueOf(37.7879373), Double.valueOf(-122.4096868))).display_position(new com.navdy.service.library.events.location.LatLong(Double.valueOf(37.7879373), Double.valueOf(-122.4096868))).full_address("Union Square, San Francisco, CA 94108").destination_title("Union Square").destination_subtitle("").favorite_type(com.navdy.service.library.events.destination.Destination$FavoriteType.FAVORITE_CUSTOM).identifier("3").suggestion_type(com.navdy.service.library.events.destination.Destination.DEFAULT_SUGGESTION_TYPE).is_recommendation(Boolean.valueOf(true)).last_navigated_to(Long.valueOf(0L)).build(), Integer.valueOf(400), com.navdy.service.library.events.places.SuggestedDestination$SuggestionType.SUGGESTION_RECOMMENDATION);
                break;
            }
            case 1: {
                a0 = new com.navdy.service.library.events.places.SuggestedDestination(new com.navdy.service.library.events.destination.Destination$Builder().navigation_position(new com.navdy.service.library.events.location.LatLong(Double.valueOf(37.7735078), Double.valueOf(-122.4055828))).display_position(new com.navdy.service.library.events.location.LatLong(Double.valueOf(37.7735078), Double.valueOf(-122.4055828))).full_address("575 7th St, San Francisco, CA 94103").destination_title("Work").destination_subtitle("").favorite_type(com.navdy.service.library.events.destination.Destination$FavoriteType.FAVORITE_WORK).identifier("2").suggestion_type(com.navdy.service.library.events.destination.Destination.DEFAULT_SUGGESTION_TYPE).is_recommendation(Boolean.valueOf(true)).last_navigated_to(Long.valueOf(0L)).build(), Integer.valueOf(3600), com.navdy.service.library.events.places.SuggestedDestination$SuggestionType.SUGGESTION_RECOMMENDATION);
                break;
            }
            case 0: {
                a0 = new com.navdy.service.library.events.places.SuggestedDestination(new com.navdy.service.library.events.destination.Destination$Builder().navigation_position(new com.navdy.service.library.events.location.LatLong(Double.valueOf(37.7786444), Double.valueOf(-122.4462313))).display_position(new com.navdy.service.library.events.location.LatLong(Double.valueOf(37.7786444), Double.valueOf(-122.4462313))).full_address("201 8th St, San Francisco, CA 94103").destination_title("Home").destination_subtitle("").favorite_type(com.navdy.service.library.events.destination.Destination$FavoriteType.FAVORITE_HOME).identifier("1").suggestion_type(com.navdy.service.library.events.destination.Destination.DEFAULT_SUGGESTION_TYPE).is_recommendation(Boolean.valueOf(true)).last_navigated_to(Long.valueOf(0L)).build(), Integer.valueOf(600), com.navdy.service.library.events.places.SuggestedDestination$SuggestionType.SUGGESTION_RECOMMENDATION);
                break;
            }
        }
        this.mBus.post(a0);
    }
    
    private void testDial(android.content.Intent a) {
        String s = a.getStringExtra("COMMAND");
        if (s.equals("PowerButtonDoubleClick")) {
            com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getInputManager().injectKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent.POWER_BUTTON_DOUBLE_CLICK);
        } else if (s.equals("rebond")) {
            this.mBus.post(new com.navdy.service.library.events.dial.DialBondRequest(com.navdy.service.library.events.dial.DialBondRequest$DialAction.DIAL_CLEAR_BOND));
            this.mBus.post(new com.navdy.service.library.events.dial.DialBondRequest(com.navdy.service.library.events.dial.DialBondRequest$DialAction.DIAL_BOND));
        }
    }
    
    private void testHID(android.content.Intent a) {
        try {
            int i = a.getIntExtra("KEY", 0);
            this.mBus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.input.MediaRemoteKeyEvent(com.navdy.service.library.events.input.MediaRemoteKey.values()[i], com.navdy.service.library.events.input.KeyEvent.KEY_DOWN)));
            new java.util.Timer().schedule((java.util.TimerTask)new com.navdy.hud.app.debug.DebugReceiver$2(this, i), 100L);
        } catch(Exception ignoredException) {
            sLogger.e("Exception getting the extra");
        }
    }
    
    private void testLight(android.content.Intent a) {
        int i = a.getIntExtra("LIGHT", 0);
        com.navdy.hud.app.device.light.LightManager a0 = com.navdy.hud.app.device.light.LightManager.getInstance();
        switch(i) {
            case 8: {
                com.navdy.hud.app.device.light.HUDLightUtils.showError(com.navdy.hud.app.HudApplication.getAppContext(), a0);
                break;
            }
            case 7: {
                com.navdy.hud.app.device.light.HUDLightUtils.showUSBTransfer(com.navdy.hud.app.HudApplication.getAppContext(), a0);
                break;
            }
            case 6: {
                com.navdy.hud.app.device.light.HUDLightUtils.showUSBPowerShutDown(com.navdy.hud.app.HudApplication.getAppContext(), a0);
                break;
            }
            case 5: {
                com.navdy.hud.app.device.light.HUDLightUtils.showUSBPowerOn(com.navdy.hud.app.HudApplication.getAppContext(), a0);
                break;
            }
            case 4: {
                com.navdy.hud.app.device.light.HUDLightUtils.showGestureDetected(com.navdy.hud.app.HudApplication.getAppContext(), a0);
                break;
            }
            case 3: {
                com.navdy.hud.app.device.light.HUDLightUtils.showGestureNotRecognized(com.navdy.hud.app.HudApplication.getAppContext(), a0);
                break;
            }
            case 2: {
                com.navdy.hud.app.device.light.HUDLightUtils.showGestureDetectionEnabled(com.navdy.hud.app.HudApplication.getAppContext(), a0, "Debug");
                break;
            }
            case 1: {
                com.navdy.hud.app.device.light.HUDLightUtils.showShutDown(a0);
                break;
            }
            case 0: {
                com.navdy.hud.app.device.light.HUDLightUtils.showPairing(com.navdy.hud.app.HudApplication.getAppContext(), a0, true);
                break;
            }
        }
    }
    
    private void testObdConfig(android.content.Intent a) {
        switch(a.getIntExtra("CONFIG", 1)) {
            case 1: {
                com.navdy.hud.app.obd.ObdManager.getInstance().getObdDeviceConfigurationManager().setConfigurationFile("default_on");
                break;
            }
            case 0: {
                com.navdy.hud.app.obd.ObdManager.getInstance().getObdDeviceConfigurationManager().setConfigurationFile("debug");
                break;
            }
        }
    }
    
    private void testProxyFileDownload(android.content.Intent a) {
        int i = a.getIntExtra("SIZE", 100);
        String s = new StringBuilder().append("http://test.mjpmz4phvm.us-west-2.elasticbeanstalk.com/file?size=").append(i).toString();
        String s0 = a.getStringExtra("URL");
        if (s0 == null) {
            s0 = s;
        }
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.debug.DebugReceiver$4(this, i, s0), 1);
    }
    
    private void testProxyFileUpload(android.content.Intent a) {
        int i = a.getIntExtra("SIZE", 100);
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.debug.DebugReceiver$3(this, i), 1);
    }
    
    private void testSendNotificationEvent(android.content.Intent a) {
        try {
            com.navdy.service.library.events.notification.NotificationEvent$Builder a0 = new com.navdy.service.library.events.notification.NotificationEvent$Builder();
            if (a != null) {
                a0.id(Integer.valueOf(a.getIntExtra("id", 1)));
                a0.category(com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_OTHER);
                a0.title(a.getStringExtra("title"));
                a0.subtitle(a.getStringExtra("subtitle"));
                a0.message(a.getStringExtra("message"));
                a0.appId(a.getStringExtra("appId"));
                a0.appName(a.getStringExtra("appName"));
                this.mBus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a0.build()));
            }
        } catch(Exception a1) {
            sLogger.e("Error while sending test Notification event ", (Throwable)a1);
        }
    }
    
    private void testShutDown() {
        android.os.Bundle a = com.navdy.hud.app.event.Shutdown$Reason.POWER_BUTTON.asBundle();
        this.mBus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_SHUTDOWN_CONFIRMATION, a, false));
    }
    
    private void testSwitchBandwidthLevel(android.content.Intent a) {
        int i = a.getIntExtra("BANDWIDTH_LEVEL", 1);
        android.content.Intent a0 = new android.content.Intent();
        a0.setAction("LINK_BANDWIDTH_LEVEL_CHANGED");
        a0.addCategory("NAVDY_LINK");
        a0.putExtra("EXTRA_BANDWIDTH_MODE", i);
        com.navdy.hud.app.HudApplication.getAppContext().sendBroadcast(a0);
    }
    
    private void testTTS(android.content.Intent a) {
        java.util.Random a0 = new java.util.Random();
        com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(TTS_TEXTS[a0.nextInt(TTS_TEXTS.length)], com.navdy.service.library.events.audio.SpeechRequest$Category.SPEECH_TURN_BY_TURN, (String)null);
    }
    
    private void triggerANR() {
        new android.os.Handler(android.os.Looper.getMainLooper()).post((Runnable)new com.navdy.hud.app.debug.DebugReceiver$5(this));
    }
    
    private void triggerBroadcastANR() {
        this.busywait(12000);
    }
    
    private void triggerCrash() {
        throw new IllegalStateException("Oops");
    }
    
    public void onReceive(android.content.Context a, android.content.Intent a0) {
    }
}
