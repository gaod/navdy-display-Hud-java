package com.navdy.hud.app.debug;

final public class DebugReceiver$$InjectAdapter extends dagger.internal.Binding implements javax.inject.Provider, dagger.MembersInjector {
    private dagger.internal.Binding mBus;
    
    public DebugReceiver$$InjectAdapter() {
        super("com.navdy.hud.app.debug.DebugReceiver", "members/com.navdy.hud.app.debug.DebugReceiver", false, com.navdy.hud.app.debug.DebugReceiver.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.mBus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.debug.DebugReceiver.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.debug.DebugReceiver get() {
        com.navdy.hud.app.debug.DebugReceiver a = new com.navdy.hud.app.debug.DebugReceiver();
        this.injectMembers(a);
        return a;
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.mBus);
    }
    
    public void injectMembers(com.navdy.hud.app.debug.DebugReceiver a) {
        a.mBus = (com.squareup.otto.Bus)this.mBus.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.debug.DebugReceiver)a);
    }
}
