package com.navdy.hud.app.debug;

class RouteRecorder$5 implements Runnable {
    final com.navdy.hud.app.debug.RouteRecorder this$0;
    
    RouteRecorder$5(com.navdy.hud.app.debug.RouteRecorder a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        java.util.ArrayList a = new java.util.ArrayList();
        java.io.File[] a0 = new java.io.File(new StringBuilder().append(com.navdy.hud.app.debug.RouteRecorder.access$1000(this.this$0).getMapsPartitionPath()).append(java.io.File.separator).append("drive_logs").toString()).listFiles();
        if (a0 != null) {
            int i = a0.length;
            int i0 = 0;
            while(i0 < i) {
                java.io.File a1 = a0[i0];
                if (!a1.getName().endsWith(".obd")) {
                    ((java.util.List)a).add(a1.getName());
                }
                i0 = i0 + 1;
            }
        }
        com.navdy.hud.app.debug.RouteRecorder.access$1100(this.this$0).sendMessage((com.squareup.wire.Message)new com.navdy.service.library.events.debug.DriveRecordingsResponse((java.util.List)a));
    }
}
