package com.navdy.hud.app.debug;

class RouteRecorder$9 implements Runnable {
    final com.navdy.hud.app.debug.RouteRecorder this$0;
    final String val$locationString;
    
    RouteRecorder$9(com.navdy.hud.app.debug.RouteRecorder a, String s) {
        super();
        this.this$0 = a;
        this.val$locationString = s;
    }
    
    public void run() {
        boolean b = com.navdy.hud.app.debug.RouteRecorder.access$1400(this.this$0);
        label1: {
            java.io.IOException a = null;
            if (!b) {
                break label1;
            }
            label0: {
                try {
                    com.navdy.hud.app.debug.RouteRecorder.access$1200(this.this$0).write(new StringBuilder().append(this.val$locationString).append("\n").toString());
                    com.navdy.hud.app.debug.RouteRecorder.access$1200(this.this$0).flush();
                } catch(java.io.IOException a0) {
                    a = a0;
                    break label0;
                }
                break label1;
            }
            com.navdy.hud.app.debug.RouteRecorder.sLogger.e((Throwable)a);
        }
    }
}
