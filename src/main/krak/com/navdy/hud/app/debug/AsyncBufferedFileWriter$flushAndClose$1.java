package com.navdy.hud.app.debug;

final class AsyncBufferedFileWriter$flushAndClose$1 implements Runnable {
    final com.navdy.hud.app.debug.AsyncBufferedFileWriter this$0;
    
    AsyncBufferedFileWriter$flushAndClose$1(com.navdy.hud.app.debug.AsyncBufferedFileWriter a) {
        super();
        this.this$0 = a;
    }
    
    final public void run() {
        this.this$0.getBufferedWriter().flush();
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.this$0.getBufferedWriter());
    }
}
