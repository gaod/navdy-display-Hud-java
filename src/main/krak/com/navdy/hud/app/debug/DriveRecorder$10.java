package com.navdy.hud.app.debug;

class DriveRecorder$10 {
    final static int[] $SwitchMap$com$navdy$hud$app$debug$DriveRecorder$Action;
    
    static {
        $SwitchMap$com$navdy$hud$app$debug$DriveRecorder$Action = new int[com.navdy.hud.app.debug.DriveRecorder$Action.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$debug$DriveRecorder$Action;
        com.navdy.hud.app.debug.DriveRecorder$Action a0 = com.navdy.hud.app.debug.DriveRecorder$Action.PLAY;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$debug$DriveRecorder$Action[com.navdy.hud.app.debug.DriveRecorder$Action.PAUSE.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$debug$DriveRecorder$Action[com.navdy.hud.app.debug.DriveRecorder$Action.RESUME.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$debug$DriveRecorder$Action[com.navdy.hud.app.debug.DriveRecorder$Action.RESTART.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$debug$DriveRecorder$Action[com.navdy.hud.app.debug.DriveRecorder$Action.STOP.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$debug$DriveRecorder$Action[com.navdy.hud.app.debug.DriveRecorder$Action.PRELOAD.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException4) {
        }
    }
}
