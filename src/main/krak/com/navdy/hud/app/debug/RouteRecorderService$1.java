package com.navdy.hud.app.debug;

class RouteRecorderService$1 extends com.navdy.hud.app.debug.IRouteRecorder$Stub {
    final com.navdy.hud.app.debug.RouteRecorderService this$0;
    
    RouteRecorderService$1(com.navdy.hud.app.debug.RouteRecorderService a) {
        super();
        this.this$0 = a;
    }
    
    public boolean isPaused() {
        return com.navdy.hud.app.debug.RouteRecorderService.access$000(this.this$0).isPaused();
    }
    
    public boolean isPlaying() {
        return com.navdy.hud.app.debug.RouteRecorderService.access$000(this.this$0).isPlaying();
    }
    
    public boolean isRecording() {
        return com.navdy.hud.app.debug.RouteRecorderService.access$000(this.this$0).isRecording();
    }
    
    public boolean isStopped() {
        return com.navdy.hud.app.debug.RouteRecorderService.access$000(this.this$0).isStopped();
    }
    
    public void pausePlayback() {
        com.navdy.hud.app.debug.RouteRecorderService.access$000(this.this$0).pausePlayback();
    }
    
    public void prepare(String s, boolean b) {
        com.navdy.hud.app.debug.RouteRecorderService.access$000(this.this$0).prepare(s, b);
    }
    
    public boolean restartPlayback() {
        return com.navdy.hud.app.debug.RouteRecorderService.access$000(this.this$0).restartPlayback();
    }
    
    public void resumePlayback() {
        com.navdy.hud.app.debug.RouteRecorderService.access$000(this.this$0).resumePlayback();
    }
    
    public void startPlayback(String s, boolean b, boolean b0) {
        com.navdy.hud.app.debug.RouteRecorderService.access$000(this.this$0).startPlayback(s, b, b0, true);
    }
    
    public String startRecording(String s) {
        return com.navdy.hud.app.debug.RouteRecorderService.access$000(this.this$0).startRecording(s, true);
    }
    
    public void stopPlayback() {
        com.navdy.hud.app.debug.RouteRecorderService.access$000(this.this$0).stopPlayback();
    }
    
    public void stopRecording() {
        com.navdy.hud.app.debug.RouteRecorderService.access$000(this.this$0).stopRecording(true);
    }
}
