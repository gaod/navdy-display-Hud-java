package com.navdy.hud.app.device.dial;

class DialFirmwareUpdater$2 implements Runnable {
    final com.navdy.hud.app.device.dial.DialFirmwareUpdater this$0;
    final android.bluetooth.BluetoothDevice val$device;
    
    DialFirmwareUpdater$2(com.navdy.hud.app.device.dial.DialFirmwareUpdater a, android.bluetooth.BluetoothDevice a0) {
        super();
        this.this$0 = a;
        this.val$device = a0;
    }
    
    public void run() {
        com.navdy.hud.app.device.dial.DialManager.getInstance().forgetDial(this.val$device);
        com.navdy.hud.app.device.dial.DialFirmwareUpdater.access$200(this.this$0, com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error.NONE, (String)null);
    }
}
