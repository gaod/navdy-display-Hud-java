package com.navdy.hud.app.device.gps;

class GpsDeadReckoningManager$7 implements android.os.Handler$Callback {
    final com.navdy.hud.app.device.gps.GpsDeadReckoningManager this$0;
    
    GpsDeadReckoningManager$7(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a) {
        super();
        this.this$0 = a;
    }
    
    public boolean handleMessage(android.os.Message a) {
        int i = a.what;
        label1: {
            Throwable a0 = null;
            if (i != 1) {
                break label1;
            }
            com.navdy.hud.app.device.gps.GpsDeadReckoningManager$Alignment a1 = (com.navdy.hud.app.device.gps.GpsDeadReckoningManager$Alignment)a.obj;
            label0: {
                try {
                    com.navdy.hud.app.device.gps.GpsDeadReckoningManager.access$1600(this.this$0, a1);
                } catch(Throwable a2) {
                    a0 = a2;
                    break label0;
                }
                break label1;
            }
            com.navdy.hud.app.device.gps.GpsDeadReckoningManager.access$1000().e(a0);
            if (com.navdy.hud.app.device.gps.GpsDeadReckoningManager.access$1700(this.this$0)) {
                if (com.navdy.hud.app.device.gps.GpsDeadReckoningManager.access$1800(this.this$0)) {
                    com.navdy.hud.app.device.gps.GpsDeadReckoningManager.access$900(this.this$0, true);
                }
            } else {
                com.navdy.hud.app.device.gps.GpsDeadReckoningManager.access$1400(this.this$0, true);
            }
        }
        return false;
    }
}
