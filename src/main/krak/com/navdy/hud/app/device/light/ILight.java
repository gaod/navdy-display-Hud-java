package com.navdy.hud.app.device.light;

abstract public interface ILight {
    abstract public void popSetting();
    
    
    abstract public void pushSetting(com.navdy.hud.app.device.light.LightSettings arg);
    
    
    abstract public void removeSetting(com.navdy.hud.app.device.light.LightSettings arg);
    
    
    abstract public void reset();
    
    
    abstract public void turnOff();
    
    
    abstract public void turnOn();
}
