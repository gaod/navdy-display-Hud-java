package com.navdy.hud.app.device.gps;

public class GpsUtils {
    public static com.navdy.hud.app.config.BooleanSetting SHOW_RAW_GPS;
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.gps.GpsUtils.class);
        SHOW_RAW_GPS = new com.navdy.hud.app.config.BooleanSetting("Show Raw GPS", com.navdy.hud.app.config.BooleanSetting$Scope.NEVER, "map.raw_gps", "Add map indicators showing raw and map matched GPS");
    }
    
    public GpsUtils() {
    }
    
    public static com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection getHeadingDirection(double d) {
        com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection a = com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection.N;
        if (!(d < 0.0) && !(d > 360.0)) {
            int i = (int)((float)((22.5 + d) % 360.0) / 45f);
            a = com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection.values()[i];
        }
        return a;
    }
    
    public static boolean isDebugRawGpsPosEnabled() {
        return SHOW_RAW_GPS.isEnabled();
    }
    
    public static void sendEventBroadcast(String s, android.os.Bundle a) {
        try {
            android.content.Intent a0 = new android.content.Intent(s);
            if (a != null) {
                a0.putExtras(a);
            }
            com.navdy.hud.app.HudApplication.getAppContext().sendBroadcastAsUser(a0, android.os.Process.myUserHandle());
        } catch(Throwable a1) {
            sLogger.e(a1);
        }
    }
}
