package com.navdy.hud.app.device.dial;

public class DialFirmwareUpdater$Versions {
    public com.navdy.hud.app.device.dial.DialFirmwareUpdater$Versions$Version dial;
    public com.navdy.hud.app.device.dial.DialFirmwareUpdater$Versions$Version local;
    final com.navdy.hud.app.device.dial.DialFirmwareUpdater this$0;
    
    public DialFirmwareUpdater$Versions(com.navdy.hud.app.device.dial.DialFirmwareUpdater a) {
        super();
        this.this$0 = a;
        this.dial = new com.navdy.hud.app.device.dial.DialFirmwareUpdater$Versions$Version(this);
        this.local = new com.navdy.hud.app.device.dial.DialFirmwareUpdater$Versions$Version(this);
    }
    
    private boolean isUpdateOK() {
        boolean b = false;
        if (this.dial.incrementalVersion >= 29) {
            b = true;
        } else {
            com.navdy.hud.app.device.dial.DialFirmwareUpdater.access$000().i("New dial firmware update logic is disabled (Current version < 29)");
            b = false;
        }
        return b;
    }
    
    public boolean isUpdateAvailable() {
        return this.local.incrementalVersion != -1 && this.dial.incrementalVersion != -1 && this.dial.incrementalVersion < this.local.incrementalVersion && this.isUpdateOK();
    }
}
