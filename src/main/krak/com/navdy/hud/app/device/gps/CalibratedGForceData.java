package com.navdy.hud.app.device.gps;

final public class CalibratedGForceData {
    final private float xAccel;
    final private float yAccel;
    final private float zAccel;
    
    public CalibratedGForceData(float f, float f0, float f1) {
        this.xAccel = f;
        this.yAccel = f0;
        this.zAccel = f1;
    }
    
    public static com.navdy.hud.app.device.gps.CalibratedGForceData copy$default(com.navdy.hud.app.device.gps.CalibratedGForceData a, float f, float f0, float f1, int i, Object a0) {
        if ((i & 1) != 0) {
            f = a.xAccel;
        }
        if ((i & 2) != 0) {
            f0 = a.yAccel;
        }
        if ((i & 4) != 0) {
            f1 = a.zAccel;
        }
        return a.copy(f, f0, f1);
    }
    
    final public float component1() {
        return this.xAccel;
    }
    
    final public float component2() {
        return this.yAccel;
    }
    
    final public float component3() {
        return this.zAccel;
    }
    
    final public com.navdy.hud.app.device.gps.CalibratedGForceData copy(float f, float f0, float f1) {
        return new com.navdy.hud.app.device.gps.CalibratedGForceData(f, f0, f1);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label2: {
            label0: {
                label1: {
                    if (this == a) {
                        break label1;
                    }
                    if (!(a instanceof com.navdy.hud.app.device.gps.CalibratedGForceData)) {
                        break label0;
                    }
                    com.navdy.hud.app.device.gps.CalibratedGForceData a0 = (com.navdy.hud.app.device.gps.CalibratedGForceData)a;
                    if (Float.compare(this.xAccel, a0.xAccel) != 0) {
                        break label0;
                    }
                    if (Float.compare(this.yAccel, a0.yAccel) != 0) {
                        break label0;
                    }
                    if (Float.compare(this.zAccel, a0.zAccel) != 0) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    final public float getXAccel() {
        return this.xAccel;
    }
    
    final public float getYAccel() {
        return this.yAccel;
    }
    
    final public float getZAccel() {
        return this.zAccel;
    }
    
    public int hashCode() {
        return (Float.floatToIntBits(this.xAccel) * 31 + Float.floatToIntBits(this.yAccel)) * 31 + Float.floatToIntBits(this.zAccel);
    }
    
    public String toString() {
        return new StringBuilder().append("CalibratedGForceData(xAccel=").append(this.xAccel).append(", yAccel=").append(this.yAccel).append(", zAccel=").append(this.zAccel).append(")").toString();
    }
}
