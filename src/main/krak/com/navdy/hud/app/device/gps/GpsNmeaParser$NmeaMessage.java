package com.navdy.hud.app.device.gps;


    public enum GpsNmeaParser$NmeaMessage {
        NOT_SUPPORTED(0),
    GGA(1),
    GLL(2),
    GNS(3),
    RMC(4),
    VTG(5),
    GSV(6);

        private int value;
        GpsNmeaParser$NmeaMessage(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class GpsNmeaParser$NmeaMessage extends Enum {
//    final private static com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage[] $VALUES;
//    final public static com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage GGA;
//    final public static com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage GLL;
//    final public static com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage GNS;
//    final public static com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage GSV;
//    final public static com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage NOT_SUPPORTED;
//    final public static com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage RMC;
//    final public static com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage VTG;
//    
//    static {
//        NOT_SUPPORTED = new com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage("NOT_SUPPORTED", 0);
//        GGA = new com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage("GGA", 1);
//        GLL = new com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage("GLL", 2);
//        GNS = new com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage("GNS", 3);
//        RMC = new com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage("RMC", 4);
//        VTG = new com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage("VTG", 5);
//        GSV = new com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage("GSV", 6);
//        com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage[] a = new com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage[7];
//        a[0] = NOT_SUPPORTED;
//        a[1] = GGA;
//        a[2] = GLL;
//        a[3] = GNS;
//        a[4] = RMC;
//        a[5] = VTG;
//        a[6] = GSV;
//        $VALUES = a;
//    }
//    
//    private GpsNmeaParser$NmeaMessage(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage valueOf(String s) {
//        return (com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage)Enum.valueOf(com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage.class, s);
//    }
//    
//    public static com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage[] values() {
//        return $VALUES.clone();
//    }
//}
//