package com.navdy.hud.app.device.dial;

class DialSimulatorMessagesHandler$SimulatedKeyEventRunnable implements Runnable {
    private com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction eventAction;
    final com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler this$0;
    
    public DialSimulatorMessagesHandler$SimulatedKeyEventRunnable(com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler a, com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction a0) {
        super();
        this.this$0 = a;
        this.eventAction = a0;
    }
    
    public void run() {
        android.app.Instrumentation a = new android.app.Instrumentation();
        if (this.eventAction != com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction.DIAL_LONG_CLICK) {
            a.sendKeyDownUpSync(((Integer)com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler.access$100().get(this.eventAction)).intValue());
        } else {
            android.view.KeyEvent a0 = new android.view.KeyEvent(0, 66);
            int i = a0.getFlags();
            android.view.KeyEvent a1 = android.view.KeyEvent.changeTimeRepeat(a0, a0.getEventTime(), 1, i | 128);
            a.sendKeySync(a1);
            a.sendKeySync(android.view.KeyEvent.changeAction(android.view.KeyEvent.changeTimeRepeat(a1, a1.getEventTime() + (long)com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler.access$000(this.this$0), 0, i | 640), 1));
        }
    }
}
