package com.navdy.hud.app.device.dial;

class DialManager$6 extends android.content.BroadcastReceiver {
    final com.navdy.hud.app.device.dial.DialManager this$0;
    
    DialManager$6(com.navdy.hud.app.device.dial.DialManager a) {
        super();
        this.this$0 = a;
    }
    
    public void onReceive(android.content.Context a, android.content.Intent a0) {
        label2: try {
            String s = a0.getAction();
            android.bluetooth.BluetoothDevice a1 = (android.bluetooth.BluetoothDevice)a0.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            if (a1 != null && com.navdy.hud.app.device.dial.DialManager.access$000(this.this$0) != null) {
                if (this.this$0.isDialDevice(a1)) {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("[Dial]btBondRecvr [").append(a1.getName()).append("] action[").append(s).append("]").toString());
                    if ("android.bluetooth.device.action.BOND_STATE_CHANGED".equals(s)) {
                        int i = a0.getIntExtra("android.bluetooth.device.extra.BOND_STATE", -2147483648);
                        int i0 = a0.getIntExtra("android.bluetooth.device.extra.PREVIOUS_BOND_STATE", -2147483648);
                        com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("[Dial]btBondRecvr: prev state:").append(i0).append(" new state:").append(i).toString());
                        if (i != 12) {
                            if (i == 10) {
                                com.navdy.hud.app.device.dial.DialManager.sLogger.e(new StringBuilder().append("[Dial]btBondRecvr: not paired tries[").append(com.navdy.hud.app.device.dial.DialManager.access$900(this.this$0)).append("]").toString());
                                if (com.navdy.hud.app.device.dial.DialManager.access$900(this.this$0) != 3) {
                                    com.navdy.hud.app.device.dial.DialManager.sLogger.e("[Dial]btBondRecvr: trying to bond again");
                                    com.navdy.hud.app.device.dial.DialManager.access$908(this.this$0);
                                    com.navdy.hud.app.device.dial.DialManager.access$400(this.this$0).postDelayed((Runnable)new com.navdy.hud.app.device.dial.DialManager$6$2(this, a1), 4000L);
                                } else {
                                    com.navdy.hud.app.device.dial.DialManager.sLogger.v("btBondRecvr removed runnable");
                                    com.navdy.hud.app.device.dial.DialManager.access$400(this.this$0).removeCallbacks(com.navdy.hud.app.device.dial.DialManager.access$600(this.this$0));
                                    android.bluetooth.BluetoothDevice a2 = com.navdy.hud.app.device.dial.DialManager.access$000(this.this$0);
                                    String s0 = null;
                                    if (a2 != null) {
                                        s0 = com.navdy.hud.app.device.dial.DialManager.access$000(this.this$0).getName();
                                    }
                                    com.navdy.hud.app.device.dial.DialManager.access$002(this.this$0, (android.bluetooth.BluetoothDevice)null);
                                    com.navdy.hud.app.device.dial.DialManager.sLogger.e("[Dial]btBondRecvr: giving up");
                                    com.navdy.hud.app.device.dial.DialManager.access$100().dialName = s0;
                                    com.navdy.hud.app.device.dial.DialManager.access$200(this.this$0).post(com.navdy.hud.app.device.dial.DialManager.access$100());
                                }
                            }
                        } else {
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v("btBondRecvr removed hang runnable");
                            com.navdy.hud.app.device.dial.DialManager.access$400(this.this$0).removeCallbacks(com.navdy.hud.app.device.dial.DialManager.access$600(this.this$0));
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]btBondRecvr: got pairing event");
                            android.bluetooth.BluetoothDevice a3 = com.navdy.hud.app.device.dial.DialManager.access$000(this.this$0);
                            label0: {
                                label1: {
                                    if (a3 == null) {
                                        break label1;
                                    }
                                    if (com.navdy.hud.app.device.dial.DialManager.access$000(this.this$0).equals(a1)) {
                                        break label0;
                                    }
                                }
                                com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("[Dial]no bonding active bondDial[").append(com.navdy.hud.app.device.dial.DialManager.access$000(this.this$0)).append("] device[").append(a1).append("]").toString());
                                break label2;
                            }
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("[Dial]btBondRecvr: bonding active bondDial[").append(com.navdy.hud.app.device.dial.DialManager.access$000(this.this$0)).append("] device[").append(a1).append("]").toString());
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("btBondRecvr:").append(a1).append("state is:").append(a1.getBondState()).toString());
                            if (com.navdy.hud.app.device.dial.DialManager.access$700(this.this$0, a1)) {
                                com.navdy.hud.app.device.dial.DialManager.sLogger.v("btBondRecvr: added to bond list");
                            } else {
                                com.navdy.hud.app.device.dial.DialManager.sLogger.v("btConnRecvr: already in bond list");
                            }
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v("btConnRecvr: attempt connection reqd");
                            com.navdy.hud.app.device.dial.DialManager.access$002(this.this$0, (android.bluetooth.BluetoothDevice)null);
                            com.navdy.hud.app.device.dial.DialManagerHelper.connectToDial(com.navdy.hud.app.device.dial.DialManager.access$800(this.this$0), a1, (com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnection)new com.navdy.hud.app.device.dial.DialManager$6$1(this, a1));
                        }
                    }
                } else {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.i(new StringBuilder().append("[Dial]btBondRecvr: notification not for dial:").append(a1.getName()).append(" addr:").append(a1.getAddress()).toString());
                }
            }
        } catch(Throwable a4) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.e("[Dial]", a4);
        }
    }
}
