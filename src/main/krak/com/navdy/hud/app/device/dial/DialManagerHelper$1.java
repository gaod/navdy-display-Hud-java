package com.navdy.hud.app.device.dial;

final class DialManagerHelper$1 implements android.bluetooth.BluetoothProfile$ServiceListener {
    final com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnectionStatus val$callBack;
    final android.bluetooth.BluetoothDevice val$device;
    
    DialManagerHelper$1(android.bluetooth.BluetoothDevice a, com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnectionStatus a0) {
        super();
        this.val$device = a;
        this.val$callBack = a0;
    }
    
    public void onServiceConnected(int i, android.bluetooth.BluetoothProfile a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.device.dial.DialManagerHelper$1$1(this, a), 1);
    }
    
    public void onServiceDisconnected(int i) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.device.dial.DialManagerHelper$1$2(this), 1);
    }
}
