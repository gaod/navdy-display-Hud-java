package com.navdy.hud.app.device.light;

public class LED implements com.navdy.hud.app.device.light.ILight {
    final private static String BLINK_ATTRIBUTE = "blink";
    final public static int CLEAR_COLOR = 0;
    final private static String COLOR_ATTRIBUTE = "color";
    final public static int DEFAULT_COLOR;
    final private static com.navdy.hud.app.device.light.LED$Settings DEFAULT_SETTINGS;
    final private static String SLOPE_DOWN_ATTRUTE = "slope_down";
    final private static String SLOPE_UP_ATTRIBUTE = "slope_up";
    final private static com.navdy.service.library.log.Logger sLogger;
    private java.util.concurrent.atomic.AtomicBoolean activityBlinkRunning;
    private String blinkPath;
    private String colorPath;
    private java.util.LinkedList mLightSettingsStack;
    private java.util.concurrent.ExecutorService mSerialExecutor;
    private String slopeDownPath;
    private String slopeUpPath;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.light.LED.class);
        DEFAULT_COLOR = android.graphics.Color.parseColor("#ff000000");
        DEFAULT_SETTINGS = new com.navdy.hud.app.device.light.LED$Settings$Builder().setName("DEFAULT").setColor(DEFAULT_COLOR).setIsBlinking(false).build();
    }
    
    public LED(String s) {
        this.activityBlinkRunning = new java.util.concurrent.atomic.AtomicBoolean(false);
        if (new java.io.File(s).exists()) {
            this.colorPath = new StringBuilder().append(s).append(java.io.File.separator).append("color").toString();
            this.slopeUpPath = new StringBuilder().append(s).append(java.io.File.separator).append("slope_up").toString();
            this.slopeDownPath = new StringBuilder().append(s).append(java.io.File.separator).append("slope_down").toString();
            this.blinkPath = new StringBuilder().append(s).append(java.io.File.separator).append("blink").toString();
            this.mLightSettingsStack = new java.util.LinkedList();
            this.mSerialExecutor = java.util.concurrent.Executors.newSingleThreadExecutor();
            this.mLightSettingsStack.push(DEFAULT_SETTINGS);
        } else {
            sLogger.w(new StringBuilder().append("Unable to open led at ").append(s).toString());
        }
    }
    
    static void access$100(com.navdy.hud.app.device.light.LED a) {
        a.activityBlink();
    }
    
    static void access$200(com.navdy.hud.app.device.light.LED a, int i) {
        a.setColor(i);
    }
    
    static void access$300(com.navdy.hud.app.device.light.LED a, com.navdy.hud.app.device.light.LED$BlinkFrequency a0) {
        a.setBlinkFrequency(a0);
    }
    
    static void access$400(com.navdy.hud.app.device.light.LED a) {
        a.startBlinking();
    }
    
    static void access$500(com.navdy.hud.app.device.light.LED a, int i, int i0, com.navdy.hud.app.device.light.LED$BlinkFrequency a0) {
        a.startBlinking(i, i0, a0);
    }
    
    static void access$600(com.navdy.hud.app.device.light.LED a) {
        a.stopBlinking();
    }
    
    private void activityBlink() {
        try {
            this.stopBlinking();
            this.setBlinkFrequency(com.navdy.hud.app.device.light.LED$BlinkFrequency.HIGH);
            this.startBlinking();
            com.navdy.hud.app.device.light.LED$BlinkFrequency a = com.navdy.hud.app.device.light.LED$BlinkFrequency.HIGH;
            try {
                Thread.sleep((long)(com.navdy.hud.app.device.light.LED$BlinkFrequency.access$000(a) / 2));
            } catch(InterruptedException ignoredException) {
                sLogger.e("Interrupted exception while activity blinking");
            }
            this.stopBlinking();
        } catch(Throwable a0) {
            this.activityBlinkRunning.set(false);
            throw a0;
        }
        this.activityBlinkRunning.set(false);
    }
    
    private void applySettings(com.navdy.hud.app.device.light.LightSettings a) {
        this.mSerialExecutor.execute((Runnable)new com.navdy.hud.app.device.light.LED$1(this, a));
    }
    
    private void setBlinkFrequency(com.navdy.hud.app.device.light.LED$BlinkFrequency a) {
        if (a != null) {
            com.navdy.hud.app.device.light.LED.writeToKernelDevice(String.valueOf(a.getBlinkDelay()), this.slopeUpPath);
            com.navdy.hud.app.device.light.LED.writeToKernelDevice(String.valueOf(a.getBlinkDelay()), this.slopeDownPath);
        }
    }
    
    private void setColor(int i) {
        Object[] a = new Object[1];
        a[0] = Integer.valueOf(i);
        com.navdy.hud.app.device.light.LED.writeToKernelDevice(String.format("%08x", a), this.colorPath);
    }
    
    private void startBlinking() {
        com.navdy.hud.app.device.light.LED.writeToKernelDevice("1", this.blinkPath);
    }
    
    private void startBlinking(int i, int i0, com.navdy.hud.app.device.light.LED$BlinkFrequency a) {
        this.stopBlinking();
        int i1 = com.navdy.hud.app.device.light.LED$BlinkFrequency.access$000(a);
        if (i > 0) {
            int i2 = 0;
            while(i2 < i) {
                try {
                    this.setColor(i0);
                    Thread.sleep((long)(i1 / 2));
                    this.setColor(0);
                    Thread.sleep((long)(i1 / 2));
                } catch(InterruptedException ignoredException) {
                    sLogger.e("Interrupted exception while blinking");
                }
                i2 = i2 + 1;
            }
        }
    }
    
    private void stopBlinking() {
        if (sLogger.isLoggable(3)) {
            sLogger.d("Stop blinking");
        }
        com.navdy.hud.app.device.light.LED.writeToKernelDevice("0", this.blinkPath);
    }
    
    private static void writeToKernelDevice(String s, String s0) {
        try {
            java.io.File a = new java.io.File(s0);
            try {
                java.io.FileWriter a0 = new java.io.FileWriter(a);
                a0.write(s);
                a0.flush();
                a0.close();
            } catch(java.io.IOException a1) {
                sLogger.e("Error writing to the sysfsfile ", (Throwable)a1);
            }
        } catch(Exception ignoredException) {
            sLogger.e("Exception while trying to write to the sysfs ");
        }
    }
    
    public static void writeToSysfs(String s, String s0) {
        sLogger.d(new StringBuilder().append("writing '").append(s).append("' to sysfs file '").append(s0).append("'").toString());
        com.navdy.hud.app.device.light.LED.writeToKernelDevice(s, s0);
    }
    
    public boolean isAvailable() {
        return this.mLightSettingsStack != null;
    }
    
    public void popSetting() {
        synchronized(this) {
            if (this.mLightSettingsStack.size() > 1) {
                com.navdy.hud.app.device.light.LightSettings a = (com.navdy.hud.app.device.light.LightSettings)this.mLightSettingsStack.pop();
                if (a != null) {
                    com.navdy.hud.app.device.light.LED$Settings a0 = (com.navdy.hud.app.device.light.LED$Settings)a;
                }
            }
            this.applySettings((com.navdy.hud.app.device.light.LightSettings)this.mLightSettingsStack.peek());
        }
        /*monexit(this)*/;
    }
    
    public void pushSetting(com.navdy.hud.app.device.light.LightSettings a) {
        synchronized(this) {
            this.mLightSettingsStack.push(a);
            this.applySettings(a);
        }
        /*monexit(this)*/;
    }
    
    public void removeSetting(com.navdy.hud.app.device.light.LightSettings a) {
        synchronized(this) {
            if (this.mLightSettingsStack.peek() != a) {
                this.mLightSettingsStack.remove(a);
            } else {
                this.popSetting();
            }
        }
        /*monexit(this)*/;
    }
    
    public void reset() {
        synchronized(this) {
            this.mLightSettingsStack.clear();
            this.pushSetting((com.navdy.hud.app.device.light.LightSettings)DEFAULT_SETTINGS);
        }
        /*monexit(this)*/;
    }
    
    public void startActivityBlink() {
        if (this.activityBlinkRunning.compareAndSet(false, true)) {
            com.navdy.hud.app.device.light.LED$Settings a = new com.navdy.hud.app.device.light.LED$Settings$Builder().setName("ActivityBlink").setActivityBlink(true).build();
            this.pushSetting((com.navdy.hud.app.device.light.LightSettings)a);
            this.removeSetting((com.navdy.hud.app.device.light.LightSettings)a);
        }
    }
    
    public void turnOff() {
        synchronized(this) {
            this.stopBlinking();
            this.setColor(0);
        }
        /*monexit(this)*/;
    }
    
    public void turnOn() {
        synchronized(this) {
            this.stopBlinking();
            this.setColor(DEFAULT_COLOR);
        }
        /*monexit(this)*/;
    }
}
