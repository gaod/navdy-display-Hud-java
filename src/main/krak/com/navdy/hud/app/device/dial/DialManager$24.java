package com.navdy.hud.app.device.dial;

class DialManager$24 {
    final static int[] $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason;
    
    static {
        $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason = new int[com.navdy.hud.app.device.dial.DialConstants$NotificationReason.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason;
        com.navdy.hud.app.device.dial.DialConstants$NotificationReason a0 = com.navdy.hud.app.device.dial.DialConstants$NotificationReason.EXTREMELY_LOW_BATTERY;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason[com.navdy.hud.app.device.dial.DialConstants$NotificationReason.LOW_BATTERY.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason[com.navdy.hud.app.device.dial.DialConstants$NotificationReason.VERY_LOW_BATTERY.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
    }
}
