package com.navdy.hud.app.device.gps;

class GpsManager$7 implements Runnable {
    final com.navdy.hud.app.device.gps.GpsManager this$0;
    
    GpsManager$7(com.navdy.hud.app.device.gps.GpsManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        try {
            com.navdy.hud.app.device.gps.GpsManager.access$000().v(new StringBuilder().append("issuing warm reset to ublox, no location report in last ").append(com.navdy.hud.app.device.gps.GpsManager.access$2200()).toString());
            com.navdy.hud.app.device.gps.GpsManager.access$2300(this.this$0, "GPS_WARM_RESET_UBLOX", (android.os.Bundle)null);
        } catch(Throwable a) {
            com.navdy.hud.app.device.gps.GpsManager.access$000().e(a);
        }
    }
}
