package com.navdy.hud.app.device.dial;

final class DialManagerHelper$11 implements Runnable {
    final android.bluetooth.BluetoothDevice val$device;
    final boolean val$firstTime;
    final boolean val$foundDial;
    final boolean val$pair;
    final String val$result;
    
    DialManagerHelper$11(boolean b, boolean b0, boolean b1, String s, android.bluetooth.BluetoothDevice a) {
        super();
        this.val$firstTime = b;
        this.val$foundDial = b0;
        this.val$pair = b1;
        this.val$result = s;
        this.val$device = a;
    }
    
    public void run() {
        try {
            java.util.HashMap a = new java.util.HashMap();
            ((java.util.Map)a).put("First_Time", ((this.val$firstTime) ? "true" : "false"));
            ((java.util.Map)a).put("Success", ((this.val$foundDial) ? "true" : "false"));
            String s = (this.val$pair) ? "Pair_Attempt" : "Connect";
            ((java.util.Map)a).put("Type", s);
            ((java.util.Map)a).put("Result", this.val$result);
            android.bluetooth.BluetoothDevice a0 = this.val$device;
            String s0 = null;
            if (a0 != null) {
                s0 = this.val$device.getAddress();
            }
            ((java.util.Map)a).put("Dial_Address", s0);
            if (this.val$foundDial) {
                com.navdy.hud.app.device.dial.DialManager a1 = com.navdy.hud.app.device.dial.DialManager.getInstance();
                String s1 = String.valueOf(a1.getLastKnownBatteryLevel());
                String s2 = String.valueOf(a1.getLastKnownRawBatteryLevel());
                String s3 = String.valueOf(a1.getLastKnownSystemTemperature());
                String s4 = a1.getFirmWareVersion();
                String s5 = a1.getHardwareVersion();
                int i = a1.getDialFirmwareUpdater().getVersions().dial.incrementalVersion;
                ((java.util.Map)a).put("Battery_Level", s1);
                ((java.util.Map)a).put("Raw_Battery_Level", s2);
                ((java.util.Map)a).put("System_Temperature", s3);
                ((java.util.Map)a).put("FW_Version", s4);
                ((java.util.Map)a).put("HW_Version", s5);
                ((java.util.Map)a).put("Incremental_Version", Integer.toString(i));
                com.navdy.hud.app.device.dial.DialManagerHelper.access$000().v(new StringBuilder().append("sending firstTime [").append(this.val$firstTime).append("] foundDial[").append(this.val$foundDial).append("] battery[").append(s1).append("] rawBattery[").append(s2).append("] temperature[").append(s3).append("]").append(" fw[").append(s4).append("] hw[").append(s5).append("] incremental[").append(i).append("] type[").append(s).append("] result [").append(this.val$result).append("] address [").append(s0).append("]").toString());
            } else {
                com.navdy.hud.app.device.dial.DialManagerHelper.access$000().v(new StringBuilder().append("sending firstTime [").append(this.val$firstTime).append("] foundDial[").append(this.val$foundDial).append("] type[").append(s).append("] result [").append(this.val$result).append("] address [").append(s0).append("]").toString());
            }
            com.localytics.android.Localytics.tagEvent("Dial_Pairing", (java.util.Map)a);
        } catch(Throwable a2) {
            com.navdy.hud.app.device.dial.DialManagerHelper.access$000().e(a2);
        }
    }
}
