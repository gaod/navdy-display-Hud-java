package com.navdy.hud.app.device.dial;
import com.navdy.hud.app.R;

public class DialNotification {
    final private static String[] BATTERY_TOASTS;
    final private static int DIAL_BATTERY_TIMEOUT = 2000;
    final private static int DIAL_BATTERY_TIMEOUT_EXTREMELY_LOW = 20000;
    final public static int DIAL_CONNECTED_TIMEOUT = 5000;
    final public static String DIAL_CONNECT_ID = "dial-connect";
    final private static int DIAL_DISCONNECTED_TIMEOUT = 1000;
    final public static String DIAL_DISCONNECT_ID = "dial-disconnect";
    final public static String DIAL_EXTREMELY_LOW_BATTERY_ID = "dial-exlow-battery";
    final public static String DIAL_FORGOTTEN_ID = "dial-forgotten";
    final private static int DIAL_FORGOTTEN_TIMEOUT = 2000;
    final public static String DIAL_LOW_BATTERY_ID = "dial-low-battery";
    final private static String[] DIAL_TOASTS;
    final public static String DIAL_VERY_LOW_BATTERY_ID = "dial-vlow-battery";
    final private static String EMPTY = "";
    final private static String battery_unknown;
    final private static String connected;
    final private static String disconnected;
    final private static String forgotten;
    final private static String forgotten_plural;
    private static android.content.res.Resources resources;
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.dial.DialNotification.class);
        String[] a = new String[3];
        a[0] = "dial-low-battery";
        a[1] = "dial-vlow-battery";
        a[2] = "dial-exlow-battery";
        BATTERY_TOASTS = a;
        String[] a0 = new String[6];
        a0[0] = "dial-connect";
        a0[1] = "dial-disconnect";
        a0[2] = "dial-forgotten";
        a0[3] = "dial-low-battery";
        a0[4] = "dial-vlow-battery";
        a0[5] = "dial-exlow-battery";
        DIAL_TOASTS = a0;
        resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        disconnected = resources.getString(R.string.connection_status_disconnected);
        connected = resources.getString(R.string.dial_paired_text);
        forgotten = resources.getString(R.string.dial_forgotten);
        forgotten_plural = resources.getString(R.string.dial_forgotten_multiple);
        battery_unknown = resources.getString(R.string.question_mark);
    }
    
    public DialNotification() {
    }
    
    public static void dismissAllBatteryToasts() {
        com.navdy.hud.app.framework.toast.ToastManager a = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        a.dismissCurrentToast(BATTERY_TOASTS);
        a.clearPendingToast(BATTERY_TOASTS);
    }
    
    public static String getDialAddressPart(String s) {
        if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
            int i = s.indexOf("(");
            if (i >= 0) {
                String s0 = s.substring(i + 1);
                int i0 = s0.indexOf(")");
                s = (i0 < 0) ? s0 : s0.substring(0, i0);
            }
        }
        return s;
    }
    
    public static String getDialBatteryLevel() {
        int i = com.navdy.hud.app.device.dial.DialManager.getInstance().getLastKnownBatteryLevel();
        return (i != -1) ? String.valueOf(com.navdy.hud.app.device.dial.DialManager.getDisplayBatteryLevel(i)) : battery_unknown;
    }
    
    public static String getDialName() {
        android.bluetooth.BluetoothDevice a = com.navdy.hud.app.device.dial.DialManager.getInstance().getDialDevice();
        String s = null;
        if (a != null) {
            s = a.getName();
            if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                String s0 = a.getAddress();
                int i = s0.length();
                if (i > 4) {
                    s0 = s0.substring(i - 4);
                }
                android.content.Context a0 = com.navdy.hud.app.HudApplication.getAppContext();
                Object[] a1 = new Object[1];
                a1[0] = s0;
                s = a0.getString(R.string.dial_str, a1);
            }
        }
        return s;
    }
    
    public static void showConnectedToast() {
        android.os.Bundle a = new android.os.Bundle();
        a.putInt("13", 5000);
        a.putInt("8", R.drawable.icon_dial_2);
        a.putInt("11", R.drawable.icon_sm_success);
        a.putString("4", resources.getString(R.string.navdy_dial));
        a.putInt("5", R.style.Glances_1_bold);
        String s = com.navdy.hud.app.device.dial.DialNotification.getDialAddressPart(com.navdy.hud.app.device.dial.DialNotification.getDialName());
        if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
            android.content.res.Resources a0 = resources;
            Object[] a1 = new Object[1];
            a1[0] = s;
            a.putString("6", a0.getString(R.string.navdy_dial_name, a1));
        }
        a.putString("17", com.navdy.hud.app.framework.voice.TTSUtils.TTS_DIAL_CONNECTED);
        com.navdy.hud.app.device.dial.DialNotification.showDialToast("dial-connect", a, true);
    }
    
    private static void showDialToast(String s, android.os.Bundle a, boolean b) {
        com.navdy.hud.app.framework.toast.ToastManager a0 = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        String s0 = a0.getCurrentToastId();
        a0.clearPendingToast(DIAL_TOASTS);
        String[] a1 = DIAL_TOASTS;
        int i = a1.length;
        int i0 = 0;
        while(i0 < i) {
            String s1 = a1[i0];
            if (!android.text.TextUtils.equals((CharSequence)s1, (CharSequence)s)) {
                a0.dismissCurrentToast(s1);
            }
            i0 = i0 + 1;
        }
        if (!android.text.TextUtils.equals((CharSequence)s, (CharSequence)s0)) {
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams(s, a, (com.navdy.hud.app.framework.toast.IToastCallback)null, b, false));
        }
    }
    
    public static void showDisconnectedToast(String s) {
        com.navdy.hud.app.screen.BaseScreen a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getCurrentScreen();
        label1: {
            label0: {
                if (a == null) {
                    break label0;
                }
                if (a.getScreen() != com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING) {
                    break label0;
                }
                sLogger.v("not showing dial disconnected toast");
                break label1;
            }
            android.os.Bundle a0 = new android.os.Bundle();
            a0.putInt("13", 1000);
            a0.putInt("8", R.drawable.icon_dial_forgotten);
            a0.putInt("11", R.drawable.icon_sm_forgotten);
            a0.putString("4", disconnected);
            a0.putInt("5", R.style.Glances_1);
            if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
                a0.putString("6", s);
            }
            a0.putString("17", com.navdy.hud.app.framework.voice.TTSUtils.TTS_DIAL_DISCONNECTED);
            com.navdy.hud.app.device.dial.DialNotification.showDialToast("dial-disconnect", a0, true);
        }
    }
    
    public static void showExtremelyLowBatteryToast() {
        android.os.Bundle a = new android.os.Bundle();
        a.putInt("13", 20000);
        a.putInt("8", R.drawable.icon_toast_dial_battery_very_low);
        String s = com.navdy.hud.app.device.dial.DialNotification.getDialBatteryLevel();
        android.content.res.Resources a0 = resources;
        Object[] a1 = new Object[1];
        a1[0] = s;
        a.putString("4", a0.getString(R.string.dial_ex_low_battery, a1));
        a.putInt("5", R.style.Glances_1);
        a.putBoolean("12", true);
        a.putString("17", com.navdy.hud.app.framework.voice.TTSUtils.TTS_DIAL_BATTERY_EXTREMELY_LOW);
        com.navdy.hud.app.device.dial.DialNotification.showDialToast("dial-exlow-battery", a, false);
    }
    
    public static void showForgottenToast(boolean b, String s) {
        android.os.Bundle a = new android.os.Bundle();
        a.putInt("13", 2000);
        a.putInt("8", R.drawable.icon_dial_forgotten);
        a.putInt("11", R.drawable.icon_sm_forgotten);
        if (b) {
            a.putString("4", forgotten_plural);
        } else {
            a.putString("4", forgotten);
        }
        if (!b && !android.text.TextUtils.isEmpty((CharSequence)s)) {
            a.putString("6", s);
            a.putInt("7", R.style.title3_single);
        }
        a.putInt("5", R.style.Glances_1);
        a.putString("17", com.navdy.hud.app.framework.voice.TTSUtils.TTS_DIAL_FORGOTTEN);
        com.navdy.hud.app.device.dial.DialNotification.showDialToast("dial-forgotten", a, true);
    }
    
    public static void showLowBatteryToast() {
        android.os.Bundle a = new android.os.Bundle();
        a.putInt("13", 2000);
        a.putInt("8", R.drawable.icon_toast_dial_battery_low);
        String s = com.navdy.hud.app.device.dial.DialNotification.getDialBatteryLevel();
        android.content.res.Resources a0 = resources;
        Object[] a1 = new Object[1];
        a1[0] = s;
        a.putString("4", a0.getString(R.string.dial_low_battery, a1));
        a.putInt("5", R.style.Glances_1);
        a.putString("17", com.navdy.hud.app.framework.voice.TTSUtils.TTS_DIAL_BATTERY_LOW);
        com.navdy.hud.app.device.dial.DialNotification.showDialToast("dial-low-battery", a, true);
    }
    
    public static void showVeryLowBatteryToast() {
        android.os.Bundle a = new android.os.Bundle();
        a.putInt("13", 2000);
        a.putInt("8", R.drawable.icon_toast_dial_battery_low);
        String s = com.navdy.hud.app.device.dial.DialNotification.getDialBatteryLevel();
        android.content.res.Resources a0 = resources;
        Object[] a1 = new Object[1];
        a1[0] = s;
        a.putString("4", a0.getString(R.string.dial_low_battery, a1));
        a.putInt("5", R.style.Glances_1);
        a.putString("17", com.navdy.hud.app.framework.voice.TTSUtils.TTS_DIAL_BATTERY_VERY_LOW);
        com.navdy.hud.app.device.dial.DialNotification.showDialToast("dial-vlow-battery", a, false);
    }
}
