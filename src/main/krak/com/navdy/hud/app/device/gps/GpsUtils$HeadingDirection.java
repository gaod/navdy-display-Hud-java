package com.navdy.hud.app.device.gps;


public enum GpsUtils$HeadingDirection {
    N(0),
    NE(1),
    E(2),
    SE(3),
    S(4),
    SW(5),
    W(6),
    NW(7);

    private int value;
    GpsUtils$HeadingDirection(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class GpsUtils$HeadingDirection extends Enum {
//    final private static com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection[] $VALUES;
//    final public static com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection E;
//    final public static com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection N;
//    final public static com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection NE;
//    final public static com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection NW;
//    final public static com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection S;
//    final public static com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection SE;
//    final public static com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection SW;
//    final public static com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection W;
//    
//    static {
//        N = new com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection("N", 0);
//        NE = new com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection("NE", 1);
//        E = new com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection("E", 2);
//        SE = new com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection("SE", 3);
//        S = new com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection("S", 4);
//        SW = new com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection("SW", 5);
//        W = new com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection("W", 6);
//        NW = new com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection("NW", 7);
//        com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection[] a = new com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection[8];
//        a[0] = N;
//        a[1] = NE;
//        a[2] = E;
//        a[3] = SE;
//        a[4] = S;
//        a[5] = SW;
//        a[6] = W;
//        a[7] = NW;
//        $VALUES = a;
//    }
//    
//    private GpsUtils$HeadingDirection(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection valueOf(String s) {
//        return (com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection)Enum.valueOf(com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection.class, s);
//    }
//    
//    public static com.navdy.hud.app.device.gps.GpsUtils$HeadingDirection[] values() {
//        return $VALUES.clone();
//    }
//}
//