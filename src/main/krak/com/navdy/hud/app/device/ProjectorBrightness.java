package com.navdy.hud.app.device;

public class ProjectorBrightness {
    final private static java.util.List AUTO_BRIGHTNESS_MAPPING;
    final private static java.util.List AUTO_BRIGHTNESS_MAPPING_FILES;
    final private static int BRIGHTNESS_SCALE_VALUES_COUNT = 256;
    final private static String DATA_FILE = "/sys/dlpc/RGB_Brightness";
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.ProjectorBrightness.class);
        AUTO_BRIGHTNESS_MAPPING_FILES = (java.util.List)new com.navdy.hud.app.device.ProjectorBrightness$1();
        AUTO_BRIGHTNESS_MAPPING = com.navdy.hud.app.device.ProjectorBrightness.readAutoBrightnessMapping();
    }
    
    public ProjectorBrightness() {
    }
    
    public static int getValue() {
        try {
            return com.navdy.hud.app.device.ProjectorBrightness.getValue(com.navdy.service.library.util.IOUtils.convertFileToString("/sys/dlpc/RGB_Brightness").trim());
        } catch(NumberFormatException a) {
            sLogger.e("Exception while reading auto-brightness value", (Throwable)a);
            throw new java.io.IOException("Corrupted data in file with auto-brightness value");
        }
    }
    
    protected static int getValue(String s) {
        int i = AUTO_BRIGHTNESS_MAPPING.indexOf(s);
        if (i < 0) {
            sLogger.e(new StringBuilder().append("Cannot get brightness value for auto-brightness string '").append(s).append("'").toString());
            i = 0;
        }
        return i;
    }
    
    public static void init() {
    }
    
    private static java.util.List readAutoBrightnessMapping() {
        java.util.List a = java.util.Collections.emptyList();
        String s = (com.navdy.hud.app.util.SerialNumber.instance.revisionCode.equals("3")) ? "_v3" : "_v4";
        java.util.Iterator a0 = AUTO_BRIGHTNESS_MAPPING_FILES.iterator();
        Object a1 = a;
        Object a2 = a0;
        while(true) {
            boolean b = ((java.util.Iterator)a2).hasNext();
            label14: {
                java.io.FileInputStream a3 = null;
                java.io.BufferedReader a4 = null;
                if (!b) {
                    break label14;
                }
                String s0 = (String)((java.util.Iterator)a2).next();
                String s1 = new StringBuilder().append(s0).append(s).toString();
                java.util.ArrayList a5 = new java.util.ArrayList(256);
                label13: {
                    label12: {
                        java.io.FileInputStream a6 = null;
                        java.io.BufferedReader a7 = null;
                        Throwable a8 = null;
                        label0: {
                            label1: {
                                label6: {
                                    {
                                        label5: {
                                            label4: try {
                                                java.io.IOException a9 = null;
                                                a6 = null;
                                                a7 = null;
                                                label2: {
                                                    label7: {
                                                        label10: {
                                                            label9: {
                                                                label11: {
                                                                    try {
                                                                        try {
                                                                            a6 = null;
                                                                            a7 = null;
                                                                            a3 = new java.io.FileInputStream(s1);
                                                                            break label11;
                                                                        } catch(java.io.FileNotFoundException ignoredException) {
                                                                        }
                                                                    } catch(java.io.IOException a10) {
                                                                        a9 = a10;
                                                                        break label10;
                                                                    }
                                                                    a3 = null;
                                                                    a4 = null;
                                                                    break label9;
                                                                }
                                                                label8: {
                                                                    try {
                                                                        try {
                                                                            try {
                                                                                a4 = new java.io.BufferedReader((java.io.Reader)new java.io.InputStreamReader((java.io.InputStream)a3));
                                                                                break label8;
                                                                            } catch(java.io.FileNotFoundException ignoredException0) {
                                                                            }
                                                                        } catch(java.io.IOException a11) {
                                                                            a9 = a11;
                                                                            break label7;
                                                                        }
                                                                    } catch(Throwable a12) {
                                                                        a8 = a12;
                                                                        break label6;
                                                                    }
                                                                    a4 = null;
                                                                    break label9;
                                                                }
                                                                try {
                                                                    try {
                                                                        try {
                                                                            String s2 = a4.readLine();
                                                                            while(s2 != null && ((java.util.List)a5).size() < 256) {
                                                                                ((java.util.List)a5).add(s2.trim());
                                                                                s2 = a4.readLine();
                                                                            }
                                                                            int i = ((java.util.List)a5).size();
                                                                            label3: {
                                                                                if (i < 256) {
                                                                                    break label3;
                                                                                }
                                                                                sLogger.i(new StringBuilder().append("Successfully read mapping from file ").append(s1).toString());
                                                                                break label4;
                                                                            }
                                                                            sLogger.w(new StringBuilder().append("File ").append(s1).append(" not complete - skipping it").toString());
                                                                            break label5;
                                                                        } catch(java.io.FileNotFoundException ignoredException1) {
                                                                        }
                                                                    } catch(java.io.IOException a13) {
                                                                        a9 = a13;
                                                                        break label2;
                                                                    }
                                                                } catch(Throwable a14) {
                                                                    a8 = a14;
                                                                    break label1;
                                                                }
                                                            }
                                                            com.navdy.service.library.log.Logger a15 = sLogger;
                                                            a6 = a3;
                                                            a7 = a4;
                                                            a15.w(new StringBuilder().append(s1).append(" not found - checking next file").toString());
                                                            break label12;
                                                        }
                                                        a3 = null;
                                                        a4 = null;
                                                        break label2;
                                                    }
                                                    a4 = null;
                                                }
                                                com.navdy.service.library.log.Logger a16 = sLogger;
                                                a6 = a3;
                                                a7 = a4;
                                                a16.e(new StringBuilder().append("Cannot read auto-brightness mapping file ").append(s1).append(" : ").append(a9.getMessage()).toString());
                                                break label13;
                                            } catch(Throwable a17) {
                                                a8 = a17;
                                                break label0;
                                            }
                                            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a4);
                                            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a3);
                                            a1 = a5;
                                            break label14;
                                        }
                                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a4);
                                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a3);
                                        a1 = a5;
                                        continue;
                                    }
                                }
                                a6 = a3;
                                a7 = null;
                                break label0;
                            }
                            a6 = a3;
                            a7 = a4;
                        }
                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a7);
                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a6);
                        throw a8;
                    }
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a4);
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a3);
                    a1 = a5;
                    continue;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a4);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a3);
                a1 = a5;
                continue;
            }
            if (((java.util.List)a1).size() < 256) {
                sLogger.e("No auto-brightness mapping file found");
                a1 = java.util.Collections.emptyList();
            }
            return (java.util.List)a1;
        }
    }
}
