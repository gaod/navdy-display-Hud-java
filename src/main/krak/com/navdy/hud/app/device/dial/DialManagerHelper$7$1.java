package com.navdy.hud.app.device.dial;

class DialManagerHelper$7$1 implements Runnable {
    final com.navdy.hud.app.device.dial.DialManagerHelper$7 this$0;
    final android.bluetooth.BluetoothProfile val$proxy;
    
    DialManagerHelper$7$1(com.navdy.hud.app.device.dial.DialManagerHelper$7 a, android.bluetooth.BluetoothProfile a0) {
        super();
        this.this$0 = a;
        this.val$proxy = a0;
    }
    
    public void run() {
        try {
            int i = this.val$proxy.getConnectionState(this.this$0.val$device);
            com.navdy.hud.app.device.dial.DialManagerHelper.access$000().v(new StringBuilder().append("[Dial]disconnectFromDial Device ").append(this.this$0.val$device.getName()).append(" state is ").append(com.navdy.hud.app.bluetooth.utils.BluetoothUtils.getConnectedState(i)).toString());
            if (i != 2) {
                com.navdy.hud.app.device.dial.DialManagerHelper.access$000().v(new StringBuilder().append("[Dial]disconnectFromDial already disconnected:").append(this.this$0.val$device).toString());
                if (!this.this$0.eventSent) {
                    this.this$0.eventSent = true;
                    this.this$0.val$callBack.onAttemptDisconnection(true);
                }
            } else if (com.navdy.hud.app.device.dial.DialManagerHelper.access$200(this.val$proxy, this.this$0.val$device)) {
                com.navdy.hud.app.device.dial.DialManagerHelper.access$000().v("[Dial]disconnectFromDial proxy-disconnect from hid device successful");
                if (!this.this$0.eventSent) {
                    this.this$0.eventSent = true;
                    this.this$0.val$callBack.onAttemptDisconnection(true);
                }
            } else {
                com.navdy.hud.app.device.dial.DialManagerHelper.access$000().v("[Dial]disconnectFromDial proxy-disconnect from hid device failed");
                if (!this.this$0.eventSent) {
                    this.this$0.eventSent = true;
                    this.this$0.val$callBack.onAttemptDisconnection(false);
                }
            }
        } catch(Throwable a) {
            com.navdy.hud.app.device.dial.DialManagerHelper.access$000().e("[Dial] disconnectFromDial", a);
            if (!this.this$0.eventSent) {
                this.this$0.eventSent = true;
                this.this$0.val$callBack.onAttemptDisconnection(false);
            }
        }
    }
}
