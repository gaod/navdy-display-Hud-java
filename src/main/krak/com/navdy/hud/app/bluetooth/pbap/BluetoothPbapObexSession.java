package com.navdy.hud.app.bluetooth.pbap;

final class BluetoothPbapObexSession {
    final static int OBEX_SESSION_AUTHENTICATION_REQUEST = 105;
    final static int OBEX_SESSION_AUTHENTICATION_TIMEOUT = 106;
    final static int OBEX_SESSION_CONNECTED = 100;
    final static int OBEX_SESSION_DISCONNECTED = 102;
    final static int OBEX_SESSION_FAILED = 101;
    final static int OBEX_SESSION_REQUEST_COMPLETED = 103;
    final static int OBEX_SESSION_REQUEST_FAILED = 104;
    final private static byte[] PBAP_TARGET;
    final private static String TAG = "BTPbapObexSession";
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexAuthenticator mAuth;
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession$ObexClientThread mObexClientThread;
    private android.os.Handler mSessionHandler;
    final private com.navdy.hud.app.bluetooth.obex.ObexTransport mTransport;
    
    static {
        byte[] a = new byte[16];
        a[0] = (byte)121;
        a[1] = (byte)97;
        a[2] = (byte)53;
        a[3] = (byte)(-16);
        a[4] = (byte)(-16);
        a[5] = (byte)(-59);
        a[6] = (byte)17;
        a[7] = (byte)(-40);
        a[8] = (byte)9;
        a[9] = (byte)102;
        a[10] = (byte)8;
        a[11] = (byte)0;
        a[12] = (byte)32;
        a[13] = (byte)12;
        a[14] = (byte)(-102);
        a[15] = (byte)102;
        PBAP_TARGET = a;
    }
    
    public BluetoothPbapObexSession(com.navdy.hud.app.bluetooth.obex.ObexTransport a) {
        this.mAuth = null;
        this.mTransport = a;
    }
    
    static com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession$ObexClientThread access$100(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession a) {
        return a.mObexClientThread;
    }
    
    static android.os.Handler access$200(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession a) {
        return a.mSessionHandler;
    }
    
    static com.navdy.hud.app.bluetooth.obex.ObexTransport access$300(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession a) {
        return a.mTransport;
    }
    
    static com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexAuthenticator access$400(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession a) {
        return a.mAuth;
    }
    
    static byte[] access$500() {
        return PBAP_TARGET;
    }
    
    public void abort() {
        android.util.Log.d("BTPbapObexSession", "abort");
        if (this.mObexClientThread != null && com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession$ObexClientThread.access$000(this.mObexClientThread) != null) {
            new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession$1(this).run();
        }
    }
    
    public boolean schedule(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest a) {
        boolean b = false;
        android.util.Log.d("BTPbapObexSession", new StringBuilder().append("schedule: ").append((a).getClass().getSimpleName()).toString());
        if (this.mObexClientThread != null) {
            b = this.mObexClientThread.schedule(a);
        } else {
            android.util.Log.e("BTPbapObexSession", "OBEX session not started");
            b = false;
        }
        return b;
    }
    
    public boolean setAuthReply(String s) {
        boolean b = false;
        android.util.Log.d("BTPbapObexSession", new StringBuilder().append("setAuthReply key=").append(s).toString());
        if (this.mAuth != null) {
            this.mAuth.setReply(s);
            b = true;
        } else {
            b = false;
        }
        return b;
    }
    
    public void start(android.os.Handler a) {
        android.util.Log.d("BTPbapObexSession", "start");
        this.mSessionHandler = a;
        this.mAuth = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexAuthenticator(this.mSessionHandler);
        this.mObexClientThread = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession$ObexClientThread(this);
        this.mObexClientThread.start();
    }
    
    public void stop() {
        android.util.Log.d("BTPbapObexSession", "stop");
        if (this.mObexClientThread != null) {
            boolean b = false;
            label2: try {
                boolean b0 = this.mObexClientThread.isAlive();
                label0: {
                    label1: {
                        if (!b0) {
                            break label1;
                        }
                        if (this.mObexClientThread.isRunning()) {
                            break label0;
                        }
                    }
                    b = false;
                    break label2;
                }
                b = true;
            } catch(Throwable ignoredException) {
                b = false;
            }
            if (b) {
                try {
                    this.mObexClientThread.interrupt();
                } catch(Throwable ignoredException0) {
                }
                try {
                    this.mObexClientThread.join();
                } catch(Throwable ignoredException1) {
                }
            }
            this.mObexClientThread = null;
        }
    }
}
