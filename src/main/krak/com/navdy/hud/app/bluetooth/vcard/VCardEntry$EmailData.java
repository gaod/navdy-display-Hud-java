package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntry$EmailData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement {
    final private String mAddress;
    final private boolean mIsPrimary;
    final private String mLabel;
    final private int mType;
    
    public VCardEntry$EmailData(String s, int i, String s0, boolean b) {
        this.mType = i;
        this.mAddress = s;
        this.mLabel = s0;
        this.mIsPrimary = b;
    }
    
    static String access$1500(com.navdy.hud.app.bluetooth.vcard.VCardEntry$EmailData a) {
        return a.mAddress;
    }
    
    public void constructInsertOperation(java.util.List a, int i) {
        android.content.ContentProviderOperation$Builder a0 = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract$Data.CONTENT_URI);
        a0.withValueBackReference("raw_contact_id", i);
        a0.withValue("mimetype", "vnd.android.cursor.item/email_v2");
        a0.withValue("data2", Integer.valueOf(this.mType));
        if (this.mType == 0) {
            a0.withValue("data3", this.mLabel);
        }
        a0.withValue("data1", this.mAddress);
        if (this.mIsPrimary) {
            a0.withValue("is_primary", Integer.valueOf(1));
        }
        a.add(a0.build());
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (this != a) {
            if (a instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry$EmailData) {
                com.navdy.hud.app.bluetooth.vcard.VCardEntry$EmailData a0 = (com.navdy.hud.app.bluetooth.vcard.VCardEntry$EmailData)a;
                int i = this.mType;
                int i0 = a0.mType;
                label1: {
                    if (i != i0) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mAddress, (CharSequence)a0.mAddress)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mLabel, (CharSequence)a0.mLabel)) {
                        break label1;
                    }
                    if (this.mIsPrimary == a0.mIsPrimary) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public String getAddress() {
        return this.mAddress;
    }
    
    final public com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel getEntryLabel() {
        return com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel.EMAIL;
    }
    
    public String getLabel() {
        return this.mLabel;
    }
    
    public int getType() {
        return this.mType;
    }
    
    public int hashCode() {
        return ((this.mType * 31 + ((this.mAddress == null) ? 0 : this.mAddress.hashCode())) * 31 + ((this.mLabel == null) ? 0 : this.mLabel.hashCode())) * 31 + ((this.mIsPrimary) ? 1231 : 1237);
    }
    
    public boolean isEmpty() {
        return android.text.TextUtils.isEmpty((CharSequence)this.mAddress);
    }
    
    public boolean isPrimary() {
        return this.mIsPrimary;
    }
    
    public String toString() {
        Object[] a = new Object[4];
        a[0] = Integer.valueOf(this.mType);
        a[1] = this.mAddress;
        a[2] = this.mLabel;
        a[3] = Boolean.valueOf(this.mIsPrimary);
        return String.format("type: %d, data: %s, label: %s, isPrimary: %s", a);
    }
}
