package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardNestedException extends com.navdy.hud.app.bluetooth.vcard.exception.VCardNotSupportedException {
    public VCardNestedException() {
    }
    
    public VCardNestedException(String s) {
        super(s);
    }
}
