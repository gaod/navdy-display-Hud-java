package com.navdy.hud.app.bluetooth.pbap;

class BluetoothPbapObexTransport implements com.navdy.hud.app.bluetooth.obex.ObexTransport {
    private android.bluetooth.BluetoothSocket mSocket;
    
    public BluetoothPbapObexTransport(android.bluetooth.BluetoothSocket a) {
        this.mSocket = null;
        this.mSocket = a;
    }
    
    public void close() {
        this.mSocket.close();
    }
    
    public void connect() {
    }
    
    public void create() {
    }
    
    public void disconnect() {
    }
    
    public int getMaxReceivePacketSize() {
        return -1;
    }
    
    public int getMaxTransmitPacketSize() {
        return -1;
    }
    
    public boolean isConnected() {
        return this.mSocket.isConnected();
    }
    
    public boolean isSrmSupported() {
        return false;
    }
    
    public void listen() {
    }
    
    public java.io.DataInputStream openDataInputStream() {
        return new java.io.DataInputStream(this.openInputStream());
    }
    
    public java.io.DataOutputStream openDataOutputStream() {
        return new java.io.DataOutputStream(this.openOutputStream());
    }
    
    public java.io.InputStream openInputStream() {
        return this.mSocket.getInputStream();
    }
    
    public java.io.OutputStream openOutputStream() {
        return this.mSocket.getOutputStream();
    }
}
