package com.navdy.hud.app.bluetooth.pbap;

class PBAPClientManager$1 implements android.os.Handler$Callback {
    final com.navdy.hud.app.bluetooth.pbap.PBAPClientManager this$0;
    
    PBAPClientManager$1(com.navdy.hud.app.bluetooth.pbap.PBAPClientManager a) {
        super();
        this.this$0 = a;
    }
    
    public boolean handleMessage(android.os.Message a) {
        try {
            switch(a.what) {
                case 204: {
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$000().w("pbap client auth-timeout");
                    break;
                }
                case 203: {
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$000().w("pbap client auth-requested");
                    break;
                }
                case 202: {
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$000().i("pbap client dis-connected");
                    break;
                }
                case 201: {
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$000().i("pbap client connected, pulling phonebook [telecom/cch.vcf]");
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$200(this.this$0).removeCallbacks(com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$100(this.this$0));
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$300(this.this$0).pullPhoneBook("telecom/cch.vcf");
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$200(this.this$0).postDelayed(com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$100(this.this$0), 45000L);
                    break;
                }
                case 106: {
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$000().e("pbap client vcard listing size error");
                    break;
                }
                case 105: {
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$000().e("pbap client phone book size error");
                    break;
                }
                case 104: {
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$000().e("pbap client vcard entry error");
                    break;
                }
                case 103: {
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$000().e("pbap client vcard listing error");
                    break;
                }
                case 102: {
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$000().e("pbap client pull phone book error");
                    break;
                }
                case 101: {
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$000().e("pbap client set phone book error");
                    break;
                }
                case 6: {
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$000().v(new StringBuilder().append("pbap client vcard listing size done:").append(a.arg1).toString());
                    break;
                }
                case 5: {
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$000().v(new StringBuilder().append("pbap client phone book size done:").append(a.arg1).toString());
                    break;
                }
                case 4: {
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$000().v("pbap client pull vcard entry done");
                    break;
                }
                case 3: {
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$000().v("pbap client vcard listing done");
                    break;
                }
                case 2: {
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$000().v("pbap client pull phone book done");
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$200(this.this$0).removeCallbacks(com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$100(this.this$0));
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$400(this.this$0, (java.util.ArrayList)a.obj);
                    break;
                }
                case 1: {
                    com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$000().v("pbap client set phone book done");
                    break;
                }
            }
        } catch(Throwable a0) {
            com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$000().e(a0);
        }
        return false;
    }
}
