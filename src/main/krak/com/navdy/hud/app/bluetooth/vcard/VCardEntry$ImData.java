package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntry$ImData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement {
    final private String mAddress;
    final private String mCustomProtocol;
    final private boolean mIsPrimary;
    final private int mProtocol;
    final private int mType;
    
    public VCardEntry$ImData(int i, String s, String s0, int i0, boolean b) {
        this.mProtocol = i;
        this.mCustomProtocol = s;
        this.mType = i0;
        this.mAddress = s0;
        this.mIsPrimary = b;
    }
    
    public void constructInsertOperation(java.util.List a, int i) {
        android.content.ContentProviderOperation$Builder a0 = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract$Data.CONTENT_URI);
        a0.withValueBackReference("raw_contact_id", i);
        a0.withValue("mimetype", "vnd.android.cursor.item/im");
        a0.withValue("data2", Integer.valueOf(this.mType));
        a0.withValue("data5", Integer.valueOf(this.mProtocol));
        a0.withValue("data1", this.mAddress);
        if (this.mProtocol == -1) {
            a0.withValue("data6", this.mCustomProtocol);
        }
        if (this.mIsPrimary) {
            a0.withValue("is_primary", Integer.valueOf(1));
        }
        a.add(a0.build());
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (this != a) {
            if (a instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry$ImData) {
                com.navdy.hud.app.bluetooth.vcard.VCardEntry$ImData a0 = (com.navdy.hud.app.bluetooth.vcard.VCardEntry$ImData)a;
                int i = this.mType;
                int i0 = a0.mType;
                label1: {
                    if (i != i0) {
                        break label1;
                    }
                    if (this.mProtocol != a0.mProtocol) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mCustomProtocol, (CharSequence)a0.mCustomProtocol)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mAddress, (CharSequence)a0.mAddress)) {
                        break label1;
                    }
                    if (this.mIsPrimary == a0.mIsPrimary) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public String getAddress() {
        return this.mAddress;
    }
    
    public String getCustomProtocol() {
        return this.mCustomProtocol;
    }
    
    final public com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel getEntryLabel() {
        return com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel.IM;
    }
    
    public int getProtocol() {
        return this.mProtocol;
    }
    
    public int getType() {
        return this.mType;
    }
    
    public int hashCode() {
        return (((this.mType * 31 + this.mProtocol) * 31 + ((this.mCustomProtocol == null) ? 0 : this.mCustomProtocol.hashCode())) * 31 + ((this.mAddress == null) ? 0 : this.mAddress.hashCode())) * 31 + ((this.mIsPrimary) ? 1231 : 1237);
    }
    
    public boolean isEmpty() {
        return android.text.TextUtils.isEmpty((CharSequence)this.mAddress);
    }
    
    public boolean isPrimary() {
        return this.mIsPrimary;
    }
    
    public String toString() {
        Object[] a = new Object[5];
        a[0] = Integer.valueOf(this.mType);
        a[1] = Integer.valueOf(this.mProtocol);
        a[2] = this.mCustomProtocol;
        a[3] = this.mAddress;
        a[4] = Boolean.valueOf(this.mIsPrimary);
        return String.format("type: %d, protocol: %d, custom_protcol: %s, data: %s, isPrimary: %s", a);
    }
}
