package com.navdy.hud.app.bluetooth.vcard;

abstract public interface VCardConstants$ImportOnly {
    final public static String PROPERTY_X_GOOGLE_TALK_WITH_SPACE = "X-GOOGLE TALK";
    final public static String PROPERTY_X_NICKNAME = "X-NICKNAME";
}
