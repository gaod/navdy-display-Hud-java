package com.navdy.hud.app.bluetooth.utils;

final public class BluetoothUtils {
    public BluetoothUtils() {
    }
    
    public static String getBondState(int i) {
        String s = null;
        switch(i) {
            case 12: {
                s = "Bonded";
                break;
            }
            case 11: {
                s = "Bonding";
                break;
            }
            case 10: {
                s = "Not Bonded";
                break;
            }
            default: {
                s = new StringBuilder().append("Unknown:").append(i).toString();
            }
        }
        return s;
    }
    
    public static String getConnectedState(int i) {
        String s = null;
        switch(i) {
            case 3: {
                s = "Disconnecting";
                break;
            }
            case 2: {
                s = "Connected";
                break;
            }
            case 1: {
                s = "Connecting";
                break;
            }
            case 0: {
                s = "Disconnected";
                break;
            }
            default: {
                s = new StringBuilder().append("Unknown:").append(i).toString();
            }
        }
        return s;
    }
    
    public static String getDeviceType(int i) {
        String s = null;
        switch(i) {
            case 3: {
                s = "Dual";
                break;
            }
            case 2: {
                s = "LE";
                break;
            }
            case 1: {
                s = "Classic";
                break;
            }
            default: {
                s = new StringBuilder().append("Unknown:").append(i).toString();
            }
        }
        return s;
    }
}
