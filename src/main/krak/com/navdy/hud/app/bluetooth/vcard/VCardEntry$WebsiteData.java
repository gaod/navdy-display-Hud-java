package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntry$WebsiteData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement {
    final private String mWebsite;
    
    public VCardEntry$WebsiteData(String s) {
        this.mWebsite = s;
    }
    
    public void constructInsertOperation(java.util.List a, int i) {
        android.content.ContentProviderOperation$Builder a0 = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract$Data.CONTENT_URI);
        a0.withValueBackReference("raw_contact_id", i);
        a0.withValue("mimetype", "vnd.android.cursor.item/website");
        a0.withValue("data1", this.mWebsite);
        a0.withValue("data2", Integer.valueOf(1));
        a.add(a0.build());
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        if (this != a) {
            if (a instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry$WebsiteData) {
                com.navdy.hud.app.bluetooth.vcard.VCardEntry$WebsiteData a0 = (com.navdy.hud.app.bluetooth.vcard.VCardEntry$WebsiteData)a;
                b = android.text.TextUtils.equals((CharSequence)this.mWebsite, (CharSequence)a0.mWebsite);
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel getEntryLabel() {
        return com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel.WEBSITE;
    }
    
    public String getWebsite() {
        return this.mWebsite;
    }
    
    public int hashCode() {
        return (this.mWebsite == null) ? 0 : this.mWebsite.hashCode();
    }
    
    public boolean isEmpty() {
        return android.text.TextUtils.isEmpty((CharSequence)this.mWebsite);
    }
    
    public String toString() {
        return new StringBuilder().append("website: ").append(this.mWebsite).toString();
    }
}
