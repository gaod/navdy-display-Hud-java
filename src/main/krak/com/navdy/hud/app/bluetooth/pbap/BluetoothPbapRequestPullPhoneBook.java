package com.navdy.hud.app.bluetooth.pbap;

final class BluetoothPbapRequestPullPhoneBook extends com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest {
    final private static String TAG = "BTPbapReqPullPBook";
    final private static String TYPE = "x-bt/phonebook";
    final private byte mFormat;
    private int mNewMissedCalls;
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapVcardList mResponse;
    
    public BluetoothPbapRequestPullPhoneBook(String s, long j, byte a, int i, int i0) {
        this.mNewMissedCalls = -1;
        int i1 = a;
        if (i >= 0 && i <= 65535) {
            if (i0 >= 0 && i0 <= 65535) {
                this.mHeaderSet.setHeader(1, s);
                this.mHeaderSet.setHeader(66, "x-bt/phonebook");
                com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters a0 = new com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters();
                if (i1 != 0 && i1 != 1) {
                    i1 = 0;
                }
                if (j != 0L) {
                    a0.add((byte)6, j);
                }
                a0.add((byte)7, (byte)i1);
                if (i <= 0) {
                    a0.add((byte)4, (short)(-1));
                } else {
                    int i2 = (short)i;
                    a0.add((byte)4, (short)i2);
                }
                if (i0 > 0) {
                    int i3 = (short)i0;
                    a0.add((byte)5, (short)i3);
                }
                a0.addToHeaderSet(this.mHeaderSet);
                this.mFormat = (byte)i1;
                return;
            }
            throw new IllegalArgumentException("listStartOffset should be [0..65535]");
        }
        throw new IllegalArgumentException("maxListCount should be [0..65535]");
    }
    
    public java.util.ArrayList getList() {
        return this.mResponse.getList();
    }
    
    public int getNewMissedCalls() {
        return this.mNewMissedCalls;
    }
    
    protected void readResponse(java.io.InputStream a) {
        android.util.Log.v("BTPbapReqPullPBook", "readResponse");
        int i = this.mFormat;
        this.mResponse = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapVcardList(a, (byte)i);
    }
    
    protected void readResponseHeaders(com.navdy.hud.app.bluetooth.obex.HeaderSet a) {
        android.util.Log.v("BTPbapReqPullPBook", "readResponse");
        com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters a0 = com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters.fromHeaderSet(a);
        if (a0.exists((byte)9)) {
            int i = a0.getByte((byte)9);
            this.mNewMissedCalls = i;
        }
    }
}
