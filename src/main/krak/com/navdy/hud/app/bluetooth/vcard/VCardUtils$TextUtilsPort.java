package com.navdy.hud.app.bluetooth.vcard;

public class VCardUtils$TextUtilsPort {
    public VCardUtils$TextUtilsPort() {
    }
    
    public static boolean isPrintableAscii(char a) {
        boolean b = false;
        int i = a;
        i = a;
        label3: {
            label0: {
                label1: {
                    label2: {
                        if (32 > a) {
                            break label2;
                        }
                        if (i <= 126) {
                            break label1;
                        }
                    }
                    if (i == 13) {
                        break label1;
                    }
                    if (i != 10) {
                        break label0;
                    }
                }
                b = true;
                break label3;
            }
            b = false;
        }
        return b;
    }
    
    public static boolean isPrintableAsciiOnly(CharSequence a) {
        int i = a.length();
        Object a0 = a;
        int i0 = 0;
        while(true) {
            boolean b = false;
            if (i0 >= i) {
                b = true;
            } else {
                int i1 = ((CharSequence)a0).charAt(i0);
                if (com.navdy.hud.app.bluetooth.vcard.VCardUtils$TextUtilsPort.isPrintableAscii((char)i1)) {
                    i0 = i0 + 1;
                    continue;
                }
                b = false;
            }
            return b;
        }
    }
}
