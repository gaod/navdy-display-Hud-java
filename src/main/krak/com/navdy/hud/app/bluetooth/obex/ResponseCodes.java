package com.navdy.hud.app.bluetooth.obex;

final public class ResponseCodes {
    final public static int OBEX_DATABASE_FULL = 224;
    final public static int OBEX_DATABASE_LOCKED = 225;
    final public static int OBEX_HTTP_ACCEPTED = 162;
    final public static int OBEX_HTTP_BAD_GATEWAY = 210;
    final public static int OBEX_HTTP_BAD_METHOD = 197;
    final public static int OBEX_HTTP_BAD_REQUEST = 192;
    final public static int OBEX_HTTP_CONFLICT = 201;
    final public static int OBEX_HTTP_CONTINUE = 144;
    final public static int OBEX_HTTP_CREATED = 161;
    final public static int OBEX_HTTP_ENTITY_TOO_LARGE = 205;
    final public static int OBEX_HTTP_FORBIDDEN = 195;
    final public static int OBEX_HTTP_GATEWAY_TIMEOUT = 212;
    final public static int OBEX_HTTP_GONE = 202;
    final public static int OBEX_HTTP_INTERNAL_ERROR = 208;
    final public static int OBEX_HTTP_LENGTH_REQUIRED = 203;
    final public static int OBEX_HTTP_MOVED_PERM = 177;
    final public static int OBEX_HTTP_MOVED_TEMP = 178;
    final public static int OBEX_HTTP_MULT_CHOICE = 176;
    final public static int OBEX_HTTP_NOT_ACCEPTABLE = 198;
    final public static int OBEX_HTTP_NOT_AUTHORITATIVE = 163;
    final public static int OBEX_HTTP_NOT_FOUND = 196;
    final public static int OBEX_HTTP_NOT_IMPLEMENTED = 209;
    final public static int OBEX_HTTP_NOT_MODIFIED = 180;
    final public static int OBEX_HTTP_NO_CONTENT = 164;
    final public static int OBEX_HTTP_OK = 160;
    final public static int OBEX_HTTP_PARTIAL = 166;
    final public static int OBEX_HTTP_PAYMENT_REQUIRED = 194;
    final public static int OBEX_HTTP_PRECON_FAILED = 204;
    final public static int OBEX_HTTP_PROXY_AUTH = 199;
    final public static int OBEX_HTTP_REQ_TOO_LARGE = 206;
    final public static int OBEX_HTTP_RESET = 165;
    final public static int OBEX_HTTP_SEE_OTHER = 179;
    final public static int OBEX_HTTP_TIMEOUT = 200;
    final public static int OBEX_HTTP_UNAUTHORIZED = 193;
    final public static int OBEX_HTTP_UNAVAILABLE = 211;
    final public static int OBEX_HTTP_UNSUPPORTED_TYPE = 207;
    final public static int OBEX_HTTP_USE_PROXY = 181;
    final public static int OBEX_HTTP_VERSION = 213;
    
    private ResponseCodes() {
    }
}
