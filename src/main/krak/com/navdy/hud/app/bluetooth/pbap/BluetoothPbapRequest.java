package com.navdy.hud.app.bluetooth.pbap;

abstract class BluetoothPbapRequest {
    final protected static byte OAP_TAGID_FILTER = (byte)6;
    final protected static byte OAP_TAGID_FORMAT = (byte)7;
    final protected static byte OAP_TAGID_LIST_START_OFFSET = (byte)5;
    final protected static byte OAP_TAGID_MAX_LIST_COUNT = (byte)4;
    final protected static byte OAP_TAGID_NEW_MISSED_CALLS = (byte)9;
    final protected static byte OAP_TAGID_ORDER = (byte)1;
    final protected static byte OAP_TAGID_PHONEBOOK_SIZE = (byte)8;
    final protected static byte OAP_TAGID_SEARCH_ATTRIBUTE = (byte)3;
    final protected static byte OAP_TAGID_SEARCH_VALUE = (byte)2;
    final private static String TAG = "BTPbapRequest";
    private boolean mAborted;
    protected com.navdy.hud.app.bluetooth.obex.HeaderSet mHeaderSet;
    private com.navdy.hud.app.bluetooth.obex.ClientOperation mOp;
    protected int mResponseCode;
    
    public BluetoothPbapRequest() {
        this.mAborted = false;
        this.mOp = null;
        this.mHeaderSet = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
    }
    
    public void abort() {
        this.mAborted = true;
        com.navdy.hud.app.bluetooth.obex.ClientOperation a = this.mOp;
        label0: {
            java.io.IOException a0 = null;
            if (a == null) {
                break label0;
            }
            try {
                this.mOp.abort();
                break label0;
            } catch(java.io.IOException a1) {
                a0 = a1;
            }
            android.util.Log.e("BTPbapRequest", "Exception occured when trying to abort", (Throwable)a0);
        }
    }
    
    protected void checkResponseCode(int i) {
        android.util.Log.v("BTPbapRequest", "checkResponseCode");
    }
    
    public void execute(com.navdy.hud.app.bluetooth.obex.ClientSession a) {
        android.util.Log.v("BTPbapRequest", "execute");
        boolean b = this.mAborted;
        label1: {
            java.io.IOException a0 = null;
            label0: if (b) {
                this.mResponseCode = 208;
                break label1;
            } else {
                try {
                    this.mOp = (com.navdy.hud.app.bluetooth.obex.ClientOperation)a.get(this.mHeaderSet);
                    this.mOp.setGetFinalFlag(true);
                    this.mOp.continueOperation(true, false);
                    this.readResponseHeaders(this.mOp.getReceivedHeader());
                    java.io.InputStream a1 = this.mOp.openInputStream();
                    this.readResponse(a1);
                    a1.close();
                    this.mOp.close();
                    this.mResponseCode = this.mOp.getResponseCode();
                    android.util.Log.d("BTPbapRequest", new StringBuilder().append("mResponseCode=").append(this.mResponseCode).toString());
                    this.checkResponseCode(this.mResponseCode);
                } catch(java.io.IOException a2) {
                    a0 = a2;
                    break label0;
                }
                break label1;
            }
            android.util.Log.e("BTPbapRequest", "IOException occured when processing request", (Throwable)a0);
            this.mResponseCode = 208;
            throw a0;
        }
    }
    
    final public boolean isSuccess() {
        return this.mResponseCode == 160;
    }
    
    protected void readResponse(java.io.InputStream a) {
        android.util.Log.v("BTPbapRequest", "readResponse");
    }
    
    protected void readResponseHeaders(com.navdy.hud.app.bluetooth.obex.HeaderSet a) {
        android.util.Log.v("BTPbapRequest", "readResponseHeaders");
    }
}
