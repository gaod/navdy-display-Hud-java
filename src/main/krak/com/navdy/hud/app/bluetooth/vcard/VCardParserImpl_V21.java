package com.navdy.hud.app.bluetooth.vcard;

class VCardParserImpl_V21 {
    final private static String DEFAULT_CHARSET = "UTF-8";
    final private static String DEFAULT_ENCODING = "8BIT";
    final private static String LOG_TAG = "vCard";
    final private static int STATE_GROUP_OR_PROPERTY_NAME = 0;
    final private static int STATE_PARAMS = 1;
    final private static int STATE_PARAMS_IN_DQUOTE = 2;
    private boolean mCanceled;
    protected String mCurrentCharset;
    protected String mCurrentEncoding;
    final protected String mIntermediateCharset;
    final private java.util.List mInterpreterList;
    protected com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V21$CustomBufferedReader mReader;
    final protected java.util.Set mUnknownTypeSet;
    final protected java.util.Set mUnknownValueSet;
    
    public VCardParserImpl_V21() {
        this(com.navdy.hud.app.bluetooth.vcard.VCardConfig.VCARD_TYPE_DEFAULT);
    }
    
    public VCardParserImpl_V21(int i) {
        this.mInterpreterList = (java.util.List)new java.util.ArrayList();
        this.mUnknownTypeSet = (java.util.Set)new java.util.HashSet();
        this.mUnknownValueSet = (java.util.Set)new java.util.HashSet();
        this.mIntermediateCharset = "ISO-8859-1";
    }
    
    private String getPotentialMultiline(String s) {
        StringBuilder a = new StringBuilder();
        a.append(s);
        while(true) {
            String s0 = this.peekLine();
            if (s0 != null && s0.length() != 0 && this.getPropertyNameUpperCase(s0) == null) {
                this.getLine();
                a.append(" ").append(s0);
                continue;
            }
            return a.toString();
        }
    }
    
    private String getPropertyNameUpperCase(String s) {
        String s0 = null;
        int i = s.indexOf(":");
        if (i <= -1) {
            s0 = null;
        } else {
            int i0 = s.indexOf(";");
            if (i != -1) {
                i0 = (i0 != -1) ? Math.min(i, i0) : i;
            }
            s0 = s.substring(0, i0).toUpperCase();
        }
        return s0;
    }
    
    private String getQuotedPrintablePart(String s) {
        if (s.trim().endsWith("=")) {
            String s0 = null;
            int i = s.length() - 1;
            while(true) {
                int i0 = s.charAt(i);
                if (i0 == 61) {
                    break;
                }
            }
            StringBuilder a = new StringBuilder();
            a.append(s.substring(0, i + 1));
            a.append("\r\n");
            label0: while(true) {
                s0 = this.getLine();
                if (s0 == null) {
                    throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("File ended during parsing a Quoted-Printable String");
                }
                if (!s0.trim().endsWith("=")) {
                    break label0;
                }
                int i1 = s0.length() - 1;
                while(true) {
                    int i2 = s0.charAt(i1);
                    if (i2 == 61) {
                        a.append(s0.substring(0, i1 + 1));
                        a.append("\r\n");
                        break;
                    }
                }
            }
            a.append(s0);
            s = a.toString();
        }
        return s;
    }
    
    private void handleAdrOrgN(com.navdy.hud.app.bluetooth.vcard.VCardProperty a, String s, String s0, String s1) {
        java.util.ArrayList a0 = new java.util.ArrayList();
        if (this.mCurrentEncoding.equals("QUOTED-PRINTABLE")) {
            String s2 = this.getQuotedPrintablePart(s);
            a.setRawValue(s2);
            Object a1 = com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructListFromValue(s2, this.getVersion()).iterator();
            while(((java.util.Iterator)a1).hasNext()) {
                ((java.util.List)a0).add(com.navdy.hud.app.bluetooth.vcard.VCardUtils.parseQuotedPrintable((String)((java.util.Iterator)a1).next(), false, s0, s1));
            }
        } else {
            Object a2 = com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructListFromValue(this.getPotentialMultiline(s), this.getVersion()).iterator();
            while(((java.util.Iterator)a2).hasNext()) {
                ((java.util.List)a0).add(com.navdy.hud.app.bluetooth.vcard.VCardUtils.convertStringCharset((String)((java.util.Iterator)a2).next(), s0, s1));
            }
        }
        a.setValues((java.util.List)a0);
        Object a3 = this.mInterpreterList.iterator();
        while(((java.util.Iterator)a3).hasNext()) {
            ((com.navdy.hud.app.bluetooth.vcard.VCardInterpreter)((java.util.Iterator)a3).next()).onPropertyCreated(a);
        }
    }
    
    private void handleNest() {
        Object a = this.mInterpreterList.iterator();
        while(((java.util.Iterator)a).hasNext()) {
            ((com.navdy.hud.app.bluetooth.vcard.VCardInterpreter)((java.util.Iterator)a).next()).onEntryStarted();
        }
        this.parseItems();
        Object a0 = this.mInterpreterList.iterator();
        while(((java.util.Iterator)a0).hasNext()) {
            ((com.navdy.hud.app.bluetooth.vcard.VCardInterpreter)((java.util.Iterator)a0).next()).onEntryEnded();
        }
    }
    
    private boolean isAsciiLetter(char a) {
        boolean b = false;
        int i = a;
        i = a;
        label3: {
            label0: {
                label2: {
                    if (a < 97) {
                        break label2;
                    }
                    if (i <= 122) {
                        break label0;
                    }
                }
                label1: {
                    if (i < 65) {
                        break label1;
                    }
                    if (i <= 90) {
                        break label0;
                    }
                }
                b = false;
                break label3;
            }
            b = true;
        }
        return b;
    }
    
    private void parseItemInter(com.navdy.hud.app.bluetooth.vcard.VCardProperty a, String s) {
        String s0 = a.getRawValue();
        if (s.equals("AGENT")) {
            this.handleAgent(a);
        } else {
            if (!this.isValidPropertyName(s)) {
                throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException(new StringBuilder().append("Unknown property name: \"").append(s).append("\"").toString());
            }
            if (s.equals("VERSION") && !s0.equals(this.getVersionString())) {
                throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardVersionException(new StringBuilder().append("Incompatible version: ").append(s0).append(" != ").append(this.getVersionString()).toString());
            }
            this.handlePropertyValue(a, s);
        }
    }
    
    private boolean parseOneVCard() {
        boolean b = false;
        this.mCurrentEncoding = "8BIT";
        this.mCurrentCharset = "UTF-8";
        if (this.readBeginVCard(false)) {
            Object a = this.mInterpreterList.iterator();
            while(((java.util.Iterator)a).hasNext()) {
                ((com.navdy.hud.app.bluetooth.vcard.VCardInterpreter)((java.util.Iterator)a).next()).onEntryStarted();
            }
            this.parseItems();
            Object a0 = this.mInterpreterList.iterator();
            while(((java.util.Iterator)a0).hasNext()) {
                ((com.navdy.hud.app.bluetooth.vcard.VCardInterpreter)((java.util.Iterator)a0).next()).onEntryEnded();
            }
            b = true;
        } else {
            b = false;
        }
        return b;
    }
    
    static String unescapeCharacter(char a) {
        String s = null;
        int i = a;
        i = a;
        label2: {
            label0: {
                label1: {
                    if (a == 92) {
                        break label1;
                    }
                    if (i == 59) {
                        break label1;
                    }
                    if (i == 58) {
                        break label1;
                    }
                    if (i != 44) {
                        break label0;
                    }
                }
                s = String.valueOf((char)i);
                break label2;
            }
            s = null;
        }
        return s;
    }
    
    public void addInterpreter(com.navdy.hud.app.bluetooth.vcard.VCardInterpreter a) {
        this.mInterpreterList.add(a);
    }
    
    final public void cancel() {
        synchronized(this) {
            android.util.Log.i("vCard", "ParserImpl received cancel operation.");
            this.mCanceled = true;
        }
        /*monexit(this)*/;
    }
    
    protected com.navdy.hud.app.bluetooth.vcard.VCardProperty constructPropertyData(String s) {
        com.navdy.hud.app.bluetooth.vcard.VCardProperty a = new com.navdy.hud.app.bluetooth.vcard.VCardProperty();
        int i = s.length();
        if (i > 0) {
            int i0 = s.charAt(0);
            if (i0 == 35) {
                throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardInvalidCommentLineException();
            }
        }
        int i1 = 0;
        int i2 = 0;
        int i3 = 0;
        while(i3 < i) {
            int i4 = s.charAt(i3);
            {
                label1: {
                    switch(i1) {
                        case 2: {
                            if (i4 != 34) {
                                break;
                            }
                            if ("2.1".equalsIgnoreCase(this.getVersionString())) {
                                android.util.Log.w("vCard", "Double-quoted params found in vCard 2.1. Silently allow it");
                            }
                            i1 = 1;
                            break;
                        }
                        case 1: {
                            label3: {
                                if (i4 != 34) {
                                    break label3;
                                }
                                if ("2.1".equalsIgnoreCase(this.getVersionString())) {
                                    android.util.Log.w("vCard", "Double-quoted params found in vCard 2.1. Silently allow it");
                                }
                                i1 = 2;
                                break;
                            }
                            label2: {
                                if (i4 != 59) {
                                    break label2;
                                }
                                this.handleParams(a, s.substring(i2, i3));
                                i2 = i3 + 1;
                                break;
                            }
                            if (i4 != 58) {
                                break;
                            }
                            this.handleParams(a, s.substring(i2, i3));
                            a.setRawValue((i3 >= i - 1) ? "" : s.substring(i3 + 1));
                            break label1;
                        }
                        case 0: {
                            label0: {
                                if (i4 == 58) {
                                    break label0;
                                }
                                if (i4 != 46) {
                                    if (i4 != 59) {
                                        break;
                                    }
                                    a.setName(s.substring(i2, i3));
                                    i2 = i3 + 1;
                                    i1 = 1;
                                    break;
                                } else {
                                    String s0 = s.substring(i2, i3);
                                    if (s0.length() != 0) {
                                        a.addGroup(s0);
                                    } else {
                                        android.util.Log.w("vCard", "Empty group found. Ignoring.");
                                    }
                                    i2 = i3 + 1;
                                    break;
                                }
                            }
                            a.setName(s.substring(i2, i3));
                            a.setRawValue((i3 >= i - 1) ? "" : s.substring(i3 + 1));
                            break label1;
                        }
                    }
                    i3 = i3 + 1;
                    continue;
                }
                return a;
            }
        }
        throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardInvalidLineException(new StringBuilder().append("Invalid line: \"").append(s).append("\"").toString());
    }
    
    protected java.util.Set getAvailableEncodingSet() {
        return com.navdy.hud.app.bluetooth.vcard.VCardParser_V21.sAvailableEncoding;
    }
    
    protected String getBase64(String s) {
        StringBuilder a = new StringBuilder();
        a.append(s);
        while(true) {
            String s0 = this.peekLine();
            if (s0 == null) {
                throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("File ended during parsing BASE64 binary");
            }
            String s1 = this.getPropertyNameUpperCase(s0);
            boolean b = this.getKnownPropertyNameSet().contains(s1);
            {
                label2: {
                    label0: {
                        label1: {
                            if (b) {
                                break label1;
                            }
                            if (!"X-ANDROID-CUSTOM".equals(s1)) {
                                break label0;
                            }
                        }
                        android.util.Log.w("vCard", "Found a next property during parsing a BASE64 string, which must not contain semi-colon or colon. Treat the line as next property.");
                        android.util.Log.w("vCard", new StringBuilder().append("Problematic line: ").append(s0.trim()).toString());
                        break label2;
                    }
                    this.getLine();
                    if (s0.length() != 0) {
                        a.append(s0.trim());
                        continue;
                    }
                }
                return a.toString();
            }
        }
    }
    
    protected String getCurrentCharset() {
        return this.mCurrentCharset;
    }
    
    protected String getDefaultCharset() {
        return "UTF-8";
    }
    
    protected String getDefaultEncoding() {
        return "8BIT";
    }
    
    protected java.util.Set getKnownPropertyNameSet() {
        return com.navdy.hud.app.bluetooth.vcard.VCardParser_V21.sKnownPropertyNameSet;
    }
    
    protected java.util.Set getKnownTypeSet() {
        return com.navdy.hud.app.bluetooth.vcard.VCardParser_V21.sKnownTypeSet;
    }
    
    protected java.util.Set getKnownValueSet() {
        return com.navdy.hud.app.bluetooth.vcard.VCardParser_V21.sKnownValueSet;
    }
    
    protected String getLine() {
        return this.mReader.readLine();
    }
    
    protected String getNonEmptyLine() {
        while(true) {
            String s = this.getLine();
            if (s == null) {
                throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("Reached end of buffer.");
            }
            if (s.trim().length() > 0) {
                return s;
            }
        }
    }
    
    protected int getVersion() {
        return 0;
    }
    
    protected String getVersionString() {
        return "2.1";
    }
    
    protected void handleAgent(com.navdy.hud.app.bluetooth.vcard.VCardProperty a) {
        if (a.getRawValue().toUpperCase().contains((CharSequence)"BEGIN:VCARD")) {
            throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardAgentNotSupportedException("AGENT Property is not supported now.");
        }
        Object a0 = this.mInterpreterList.iterator();
        while(((java.util.Iterator)a0).hasNext()) {
            ((com.navdy.hud.app.bluetooth.vcard.VCardInterpreter)((java.util.Iterator)a0).next()).onPropertyCreated(a);
        }
    }
    
    protected void handleAnyParam(com.navdy.hud.app.bluetooth.vcard.VCardProperty a, String s, String s0) {
        a.addParameter(s, s0);
    }
    
    protected void handleCharset(com.navdy.hud.app.bluetooth.vcard.VCardProperty a, String s) {
        this.mCurrentCharset = s;
        a.addParameter("CHARSET", s);
    }
    
    protected void handleEncoding(com.navdy.hud.app.bluetooth.vcard.VCardProperty a, String s) {
        if (!this.getAvailableEncodingSet().contains(s) && !s.startsWith("X-")) {
            throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException(new StringBuilder().append("Unknown encoding \"").append(s).append("\"").toString());
        }
        a.addParameter("ENCODING", s);
        this.mCurrentEncoding = s.toUpperCase();
    }
    
    protected void handleLanguage(com.navdy.hud.app.bluetooth.vcard.VCardProperty a, String s) {
        String[] a0 = s.split("-");
        if (a0.length != 2) {
            throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException(new StringBuilder().append("Invalid Language: \"").append(s).append("\"").toString());
        }
        String s0 = a0[0];
        int i = s0.length();
        int i0 = 0;
        while(i0 < i) {
            int i1 = s0.charAt(i0);
            if (!this.isAsciiLetter((char)i1)) {
                throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException(new StringBuilder().append("Invalid Language: \"").append(s).append("\"").toString());
            }
            i0 = i0 + 1;
        }
        String s1 = a0[1];
        int i2 = s1.length();
        int i3 = 0;
        while(i3 < i2) {
            int i4 = s1.charAt(i3);
            if (!this.isAsciiLetter((char)i4)) {
                throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException(new StringBuilder().append("Invalid Language: \"").append(s).append("\"").toString());
            }
            i3 = i3 + 1;
        }
        a.addParameter("LANGUAGE", s);
    }
    
    protected void handleParamWithoutName(com.navdy.hud.app.bluetooth.vcard.VCardProperty a, String s) {
        this.handleType(a, s);
    }
    
    protected void handleParams(com.navdy.hud.app.bluetooth.vcard.VCardProperty a, String s) {
        String[] a0 = s.split("=", 2);
        if (a0.length != 2) {
            this.handleParamWithoutName(a, a0[0]);
        } else {
            String s0 = a0[0].trim().toUpperCase();
            String s1 = a0[1].trim();
            if (s0.equals("TYPE")) {
                this.handleType(a, s1);
            } else if (s0.equals("VALUE")) {
                this.handleValue(a, s1);
            } else if (s0.equals("ENCODING")) {
                this.handleEncoding(a, s1.toUpperCase());
            } else if (s0.equals("CHARSET")) {
                this.handleCharset(a, s1);
            } else if (s0.equals("LANGUAGE")) {
                this.handleLanguage(a, s1);
            } else {
                if (!s0.startsWith("X-")) {
                    throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException(new StringBuilder().append("Unknown type \"").append(s0).append("\"").toString());
                }
                this.handleAnyParam(a, s0, s1);
            }
        }
    }
    
    protected void handlePropertyValue(com.navdy.hud.app.bluetooth.vcard.VCardProperty a, String s) {
        String s0 = a.getName().toUpperCase();
        String s1 = a.getRawValue();
        java.util.Collection a0 = a.getParameters("CHARSET");
        String s2 = (a0 == null) ? null : (String)a0.iterator().next();
        if (android.text.TextUtils.isEmpty((CharSequence)s2)) {
            s2 = "UTF-8";
        }
        boolean b = s0.equals("ADR");
        label0: {
            label5: {
                label6: {
                    if (b) {
                        break label6;
                    }
                    if (s0.equals("ORG")) {
                        break label6;
                    }
                    if (!s0.equals("N")) {
                        break label5;
                    }
                }
                this.handleAdrOrgN(a, s1, "ISO-8859-1", s2);
                break label0;
            }
            boolean b0 = this.mCurrentEncoding.equals("QUOTED-PRINTABLE");
            label3: {
                label4: {
                    if (b0) {
                        break label4;
                    }
                    if (!s0.equals("FN")) {
                        break label3;
                    }
                    if (a.getParameters("ENCODING") != null) {
                        break label3;
                    }
                    if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.appearsLikeAndroidVCardQuotedPrintable(s1)) {
                        break label3;
                    }
                }
                String s3 = this.getQuotedPrintablePart(s1);
                String s4 = com.navdy.hud.app.bluetooth.vcard.VCardUtils.parseQuotedPrintable(s3, false, "ISO-8859-1", s2);
                a.setRawValue(s3);
                String[] a1 = new String[1];
                a1[0] = s4;
                a.setValues(a1);
                Object a2 = this.mInterpreterList.iterator();
                while(((java.util.Iterator)a2).hasNext()) {
                    ((com.navdy.hud.app.bluetooth.vcard.VCardInterpreter)((java.util.Iterator)a2).next()).onPropertyCreated(a);
                }
                break label0;
            }
            boolean b1 = this.mCurrentEncoding.equals("BASE64");
            label1: {
                label2: {
                    if (b1) {
                        break label2;
                    }
                    if (!this.mCurrentEncoding.equals("B")) {
                        break label1;
                    }
                }
                try {
                    String s5 = this.getBase64(s1);
                    try {
                        a.setByteValue(android.util.Base64.decode(s5, 0));
                    } catch(IllegalArgumentException ignoredException) {
                        throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException(new StringBuilder().append("Decode error on base64 photo: ").append(s1).toString());
                    }
                    Object a3 = this.mInterpreterList.iterator();
                    while(((java.util.Iterator)a3).hasNext()) {
                        ((com.navdy.hud.app.bluetooth.vcard.VCardInterpreter)((java.util.Iterator)a3).next()).onPropertyCreated(a);
                    }
                    break label0;
                } catch(OutOfMemoryError ignoredException0) {
                    android.util.Log.e("vCard", "OutOfMemoryError happened during parsing BASE64 data!");
                    Object a4 = this.mInterpreterList.iterator();
                    while(((java.util.Iterator)a4).hasNext()) {
                        ((com.navdy.hud.app.bluetooth.vcard.VCardInterpreter)((java.util.Iterator)a4).next()).onPropertyCreated(a);
                    }
                    break label0;
                }
            }
            if (!this.mCurrentEncoding.equals("7BIT") && !this.mCurrentEncoding.equals("8BIT") && !this.mCurrentEncoding.startsWith("X-")) {
                Object[] a5 = new Object[2];
                a5[0] = this.mCurrentEncoding;
                a5[1] = this.getVersionString();
                android.util.Log.w("vCard", String.format("The encoding \"%s\" is unsupported by vCard %s", a5));
            }
            if (this.getVersion() == 0) {
                StringBuilder a6 = null;
                while(true) {
                    String s6 = this.peekLine();
                    if (android.text.TextUtils.isEmpty((CharSequence)s6)) {
                        break;
                    }
                    int i = s6.charAt(0);
                    if (i != 32) {
                        break;
                    }
                    if ("END:VCARD".contains((CharSequence)s6.toUpperCase())) {
                        break;
                    }
                    this.getLine();
                    if (a6 == null) {
                        a6 = new StringBuilder();
                        a6.append(s1);
                    }
                    a6.append(s6.substring(1));
                }
                if (a6 != null) {
                    s1 = a6.toString();
                }
            }
            java.util.ArrayList a7 = new java.util.ArrayList();
            a7.add(com.navdy.hud.app.bluetooth.vcard.VCardUtils.convertStringCharset(this.maybeUnescapeText(s1), "ISO-8859-1", s2));
            a.setValues((java.util.List)a7);
            Object a8 = this.mInterpreterList.iterator();
            while(((java.util.Iterator)a8).hasNext()) {
                ((com.navdy.hud.app.bluetooth.vcard.VCardInterpreter)((java.util.Iterator)a8).next()).onPropertyCreated(a);
            }
        }
    }
    
    protected void handleType(com.navdy.hud.app.bluetooth.vcard.VCardProperty a, String s) {
        if (!this.getKnownTypeSet().contains(s.toUpperCase()) && !s.startsWith("X-") && !this.mUnknownTypeSet.contains(s)) {
            this.mUnknownTypeSet.add(s);
            Object[] a0 = new Object[2];
            a0[0] = Integer.valueOf(this.getVersion());
            a0[1] = s;
            android.util.Log.w("vCard", String.format("TYPE unsupported by %s: ", a0));
        }
        a.addParameter("TYPE", s);
    }
    
    protected void handleValue(com.navdy.hud.app.bluetooth.vcard.VCardProperty a, String s) {
        if (!this.getKnownValueSet().contains(s.toUpperCase()) && !s.startsWith("X-") && !this.mUnknownValueSet.contains(s)) {
            this.mUnknownValueSet.add(s);
            Object[] a0 = new Object[2];
            a0[0] = Integer.valueOf(this.getVersion());
            a0[1] = s;
            android.util.Log.w("vCard", String.format("The value unsupported by TYPE of %s: ", a0));
        }
        a.addParameter("VALUE", s);
    }
    
    protected boolean isValidPropertyName(String s) {
        if (!this.getKnownPropertyNameSet().contains(s.toUpperCase()) && !s.startsWith("X-") && !this.mUnknownTypeSet.contains(s)) {
            this.mUnknownTypeSet.add(s);
            android.util.Log.w("vCard", new StringBuilder().append("Property name unsupported by vCard 2.1: ").append(s).toString());
        }
        return true;
    }
    
    protected String maybeUnescapeCharacter(char a) {
        return com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V21.unescapeCharacter(a);
    }
    
    protected String maybeUnescapeText(String s) {
        return s;
    }
    
    public void parse(java.io.InputStream a) {
        Throwable a0 = null;
        if (a == null) {
            throw new NullPointerException("InputStream must not be null.");
        }
        this.mReader = new com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V21$CustomBufferedReader((java.io.Reader)new java.io.InputStreamReader(a, this.mIntermediateCharset));
        System.currentTimeMillis();
        Object a1 = this.mInterpreterList.iterator();
        while(((java.util.Iterator)a1).hasNext()) {
            ((com.navdy.hud.app.bluetooth.vcard.VCardInterpreter)((java.util.Iterator)a1).next()).onVCardStarted();
        }
        label0: while(true) {
            label1: synchronized(this) {
                label3: {
                    label2: try {
                        if (this.mCanceled) {
                            break label2;
                        }
                        /*monexit(this)*/;
                        break label3;
                    } catch(IllegalMonitorStateException | NullPointerException a2) {
                        a0 = a2;
                        break label0;
                    }
                    try {
                        android.util.Log.i("vCard", "Cancel request has come. exitting parse operation.");
                        /*monexit(this)*/;
                        break label1;
                    } catch(Throwable a3) {
                        a0 = a3;
                        break label0;
                    }
                }
                if (this.parseOneVCard()) {
                    continue label0;
                }
            }
            Object a4 = this.mInterpreterList.iterator();
            while(((java.util.Iterator)a4).hasNext()) {
                ((com.navdy.hud.app.bluetooth.vcard.VCardInterpreter)((java.util.Iterator)a4).next()).onVCardEnded();
            }
            return;
        }
        
    }
    
    protected boolean parseItem() {
        boolean b = false;
        this.mCurrentEncoding = "8BIT";
        com.navdy.hud.app.bluetooth.vcard.VCardProperty a = this.constructPropertyData(this.getNonEmptyLine());
        String s = a.getName().toUpperCase();
        String s0 = a.getRawValue();
        boolean b0 = s.equals("BEGIN");
        label3: {
            label0: {
                label2: {
                    label1: {
                        if (!b0) {
                            break label1;
                        }
                        if (!s0.equalsIgnoreCase("VCARD")) {
                            throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException(new StringBuilder().append("Unknown BEGIN type: ").append(s0).toString());
                        }
                        this.handleNest();
                        break label2;
                    }
                    if (s.equals("END")) {
                        break label0;
                    }
                    this.parseItemInter(a, s);
                }
                b = false;
                break label3;
            }
            if (!s0.equalsIgnoreCase("VCARD")) {
                throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException(new StringBuilder().append("Unknown END type: ").append(s0).toString());
            }
            b = true;
        }
        return b;
    }
    
    protected void parseItems() {
        boolean b = false;
        try {
            b = this.parseItem();
        } catch(com.navdy.hud.app.bluetooth.vcard.exception.VCardInvalidCommentLineException ignoredException) {
            android.util.Log.e("vCard", "Invalid line which looks like some comment was found. Ignored.");
            b = false;
        }
        while(!b) {
            try {
                b = this.parseItem();
                continue;
            } catch(com.navdy.hud.app.bluetooth.vcard.exception.VCardInvalidCommentLineException ignoredException0) {
            }
            android.util.Log.e("vCard", "Invalid line which looks like some comment was found. Ignored.");
        }
    }
    
    public void parseOne(java.io.InputStream a) {
        if (a == null) {
            throw new NullPointerException("InputStream must not be null.");
        }
        this.mReader = new com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V21$CustomBufferedReader((java.io.Reader)new java.io.InputStreamReader(a, this.mIntermediateCharset));
        System.currentTimeMillis();
        Object a0 = this.mInterpreterList.iterator();
        while(((java.util.Iterator)a0).hasNext()) {
            ((com.navdy.hud.app.bluetooth.vcard.VCardInterpreter)((java.util.Iterator)a0).next()).onVCardStarted();
        }
        this.parseOneVCard();
        Object a1 = this.mInterpreterList.iterator();
        while(((java.util.Iterator)a1).hasNext()) {
            ((com.navdy.hud.app.bluetooth.vcard.VCardInterpreter)((java.util.Iterator)a1).next()).onVCardEnded();
        }
    }
    
    protected String peekLine() {
        return this.mReader.peekLine();
    }
    
    protected boolean readBeginVCard(boolean b) {
        while(true) {
            boolean b0 = false;
            String s = this.getLine();
            label1: if (s != null) {
                if (s.trim().length() <= 0) {
                    continue;
                }
                String[] a = s.split(":", 2);
                int i = a.length;
                label0: {
                    if (i != 2) {
                        break label0;
                    }
                    if (!a[0].trim().equalsIgnoreCase("BEGIN")) {
                        break label0;
                    }
                    if (!a[1].trim().equalsIgnoreCase("VCARD")) {
                        break label0;
                    }
                    b0 = true;
                    break label1;
                }
                if (!b) {
                    throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException(new StringBuilder().append("Expected String \"BEGIN:VCARD\" did not come (Instead, \"").append(s).append("\" came)").toString());
                }
                if (!b) {
                    throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("Reached where must not be reached.");
                }
                continue;
            } else {
                b0 = false;
            }
            return b0;
        }
    }
}
