package com.navdy.hud.app.bluetooth.vcard;

class VCardEntry$InsertOperationConstrutor implements com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElementIterator {
    final private int mBackReferenceIndex;
    final private java.util.List mOperationList;
    final com.navdy.hud.app.bluetooth.vcard.VCardEntry this$0;
    
    public VCardEntry$InsertOperationConstrutor(com.navdy.hud.app.bluetooth.vcard.VCardEntry a, java.util.List a0, int i) {
        super();
        this.this$0 = a;
        this.mOperationList = a0;
        this.mBackReferenceIndex = i;
    }
    
    public boolean onElement(com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement a) {
        if (!a.isEmpty()) {
            a.constructInsertOperation(this.mOperationList, this.mBackReferenceIndex);
        }
        return true;
    }
    
    public void onElementGroupEnded() {
    }
    
    public void onElementGroupStarted(com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel a) {
    }
    
    public void onIterationEnded() {
    }
    
    public void onIterationStarted() {
    }
}
