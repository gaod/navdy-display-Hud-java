package com.navdy.hud.app.bluetooth.obex;

final public class PrivateOutputStream extends java.io.OutputStream {
    private java.io.ByteArrayOutputStream mArray;
    private int mMaxPacketSize;
    private boolean mOpen;
    private com.navdy.hud.app.bluetooth.obex.BaseStream mParent;
    
    public PrivateOutputStream(com.navdy.hud.app.bluetooth.obex.BaseStream a, int i) {
        this.mParent = a;
        this.mArray = new java.io.ByteArrayOutputStream();
        this.mMaxPacketSize = i;
        this.mOpen = true;
    }
    
    private void ensureOpen() {
        this.mParent.ensureOpen();
        if (!this.mOpen) {
            throw new java.io.IOException("Output stream is closed");
        }
    }
    
    public void close() {
        this.mOpen = false;
        this.mParent.streamClosed(false);
    }
    
    public boolean isClosed() {
        return !this.mOpen;
    }
    
    public byte[] readBytes(int i) {
        byte[] a = null;
        synchronized(this) {
            if (this.mArray.size() <= 0) {
                a = null;
            } else {
                byte[] a0 = this.mArray.toByteArray();
                this.mArray.reset();
                a = new byte[i];
                System.arraycopy(a0, 0, a, 0, i);
                if (a0.length != i) {
                    this.mArray.write(a0, i, a0.length - i);
                }
            }
        }
        /*monexit(this)*/;
        return a;
    }
    
    public int size() {
        return this.mArray.size();
    }
    
    public void write(int i) {
        synchronized(this) {
            this.ensureOpen();
            this.mParent.ensureNotDone();
            this.mArray.write(i);
            if (this.mArray.size() == this.mMaxPacketSize) {
                this.mParent.continueOperation(true, false);
            }
        }
        /*monexit(this)*/;
    }
    
    public void write(byte[] a) {
        this.write(a, 0, a.length);
    }
    
    public void write(byte[] a, int i, int i0) {
        synchronized(this) {
            if (a == null) {
                throw new java.io.IOException("buffer is null");
            }
            int i1 = i | i0;
            label0: {
                label1: {
                    if (i1 < 0) {
                        break label1;
                    }
                    if (i0 <= a.length - i) {
                        break label0;
                    }
                }
                throw new IndexOutOfBoundsException("index outof bound");
            }
            this.ensureOpen();
            this.mParent.ensureNotDone();
            while(this.mArray.size() + i0 >= this.mMaxPacketSize) {
                int i2 = this.mMaxPacketSize - this.mArray.size();
                this.mArray.write(a, i, i2);
                int i3 = i + i2;
                int i4 = i0 - i2;
                this.mParent.continueOperation(true, false);
                i0 = i4;
                i = i3;
            }
            if (i0 > 0) {
                this.mArray.write(a, i, i0);
            }
        }
        /*monexit(this)*/;
    }
}
