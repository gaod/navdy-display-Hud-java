package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardVersionException extends com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
    public VCardVersionException() {
    }
    
    public VCardVersionException(String s) {
        super(s);
    }
}
