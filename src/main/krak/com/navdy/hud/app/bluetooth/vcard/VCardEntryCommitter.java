package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntryCommitter implements com.navdy.hud.app.bluetooth.vcard.VCardEntryHandler {
    public static String LOG_TAG;
    final private android.content.ContentResolver mContentResolver;
    private int mCounter;
    final private java.util.ArrayList mCreatedUris;
    private java.util.ArrayList mOperationList;
    private long mTimeToCommit;
    
    static {
        LOG_TAG = "vCard";
    }
    
    public VCardEntryCommitter(android.content.ContentResolver a) {
        this.mCreatedUris = new java.util.ArrayList();
        this.mContentResolver = a;
    }
    
    private android.net.Uri pushIntoContentResolver(java.util.ArrayList a) {
        android.net.Uri a0 = null;
        label0: {
            android.os.RemoteException a1 = null;
            label1: try {
                android.content.ContentProviderResult[] a2 = null;
                try {
                    a2 = this.mContentResolver.applyBatch("com.android.contacts", a);
                } catch(android.os.RemoteException a3) {
                    a1 = a3;
                    break label1;
                }
                a0 = null;
                if (a2 == null) {
                    break label0;
                }
                int i = a2.length;
                a0 = null;
                if (i == 0) {
                    break label0;
                }
                a0 = (a2[0] != null) ? a2[0].uri : null;
                break label0;
            } catch(android.content.OperationApplicationException a4) {
                String s = LOG_TAG;
                Object[] a5 = new Object[2];
                a5[0] = a4.toString();
                a5[1] = a4.getMessage();
                android.util.Log.e(s, String.format("%s: %s", a5));
                a0 = null;
                break label0;
            }
            String s0 = LOG_TAG;
            Object[] a6 = new Object[2];
            a6[0] = a1.toString();
            a6[1] = a1.getMessage();
            android.util.Log.e(s0, String.format("%s: %s", a6));
            a0 = null;
        }
        return a0;
    }
    
    public java.util.ArrayList getCreatedUris() {
        return this.mCreatedUris;
    }
    
    public void onEnd() {
        if (this.mOperationList != null) {
            this.mCreatedUris.add(this.pushIntoContentResolver(this.mOperationList));
        }
        if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.showPerformanceLog()) {
            String s = LOG_TAG;
            Object[] a = new Object[1];
            a[0] = Long.valueOf(this.mTimeToCommit);
            android.util.Log.d(s, String.format("time to commit entries: %d ms", a));
        }
    }
    
    public void onEntryCreated(com.navdy.hud.app.bluetooth.vcard.VCardEntry a) {
        long j = System.currentTimeMillis();
        this.mOperationList = a.constructInsertOperations(this.mContentResolver, this.mOperationList);
        this.mCounter = this.mCounter + 1;
        if (this.mCounter >= 20) {
            this.mCreatedUris.add(this.pushIntoContentResolver(this.mOperationList));
            this.mCounter = 0;
            this.mOperationList = null;
        }
        this.mTimeToCommit = this.mTimeToCommit + (System.currentTimeMillis() - j);
    }
    
    public void onStart() {
    }
}
