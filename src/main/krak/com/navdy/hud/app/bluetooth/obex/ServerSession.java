package com.navdy.hud.app.bluetooth.obex;

final public class ServerSession extends com.navdy.hud.app.bluetooth.obex.ObexSession implements Runnable {
    final private static String TAG = "Obex ServerSession";
    final private static boolean V = false;
    private boolean mClosed;
    private java.io.InputStream mInput;
    private com.navdy.hud.app.bluetooth.obex.ServerRequestHandler mListener;
    private int mMaxPacketLength;
    private java.io.OutputStream mOutput;
    private Thread mProcessThread;
    private com.navdy.hud.app.bluetooth.obex.ObexTransport mTransport;
    
    public ServerSession(com.navdy.hud.app.bluetooth.obex.ObexTransport a, com.navdy.hud.app.bluetooth.obex.ServerRequestHandler a0, com.navdy.hud.app.bluetooth.obex.Authenticator a1) {
        this.mAuthenticator = a1;
        this.mTransport = a;
        this.mInput = this.mTransport.openInputStream();
        this.mOutput = this.mTransport.openOutputStream();
        this.mListener = a0;
        this.mMaxPacketLength = 256;
        this.mClosed = false;
        this.mProcessThread = new Thread((Runnable)this);
        this.mProcessThread.start();
    }
    
    private void handleAbortRequest() {
        int i = 0;
        com.navdy.hud.app.bluetooth.obex.HeaderSet a = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        com.navdy.hud.app.bluetooth.obex.HeaderSet a0 = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        int i0 = (this.mInput.read() << 8) + this.mInput.read();
        if (i0 <= com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxRxPacketSize(this.mTransport)) {
            int i1 = 3;
            while(i1 < i0) {
                this.mInput.read();
                i1 = i1 + 1;
            }
            int i2 = this.mListener.onAbort(a, a0);
            android.util.Log.v("Obex ServerSession", new StringBuilder().append("onAbort request handler return value- ").append(i2).toString());
            i = this.validateResponseCode(i2);
        } else {
            i = 206;
        }
        this.sendResponse(i, (byte[])null);
    }
    
    private void handleConnectRequest() {
        int i = 0;
        byte[] a = null;
        int i0 = 0;
        com.navdy.hud.app.bluetooth.obex.HeaderSet a0 = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        com.navdy.hud.app.bluetooth.obex.HeaderSet a1 = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        int i1 = (this.mInput.read() << 8) + this.mInput.read();
        this.mInput.read();
        this.mInput.read();
        this.mMaxPacketLength = this.mInput.read();
        this.mMaxPacketLength = (this.mMaxPacketLength << 8) + this.mInput.read();
        if (this.mMaxPacketLength > 65534) {
            this.mMaxPacketLength = 65534;
        }
        if (this.mMaxPacketLength > com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxTxPacketSize(this.mTransport)) {
            android.util.Log.w("Obex ServerSession", new StringBuilder().append("Requested MaxObexPacketSize ").append(this.mMaxPacketLength).append(" is larger than the max size supported by the transport: ").append(com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxTxPacketSize(this.mTransport)).append(" Reducing to this size.").toString());
            this.mMaxPacketLength = com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxTxPacketSize(this.mTransport);
        }
        int i2 = com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxRxPacketSize(this.mTransport);
        label0: {
            label1: if (i1 <= i2) {
                if (i1 > 7) {
                    byte[] a2 = new byte[i1 - 7];
                    int i3 = this.mInput.read(a2);
                    while(i3 != a2.length) {
                        i3 = i3 + this.mInput.read(a2, i3, a2.length - i3);
                    }
                    com.navdy.hud.app.bluetooth.obex.ObexHelper.updateHeaderSet(a0, a2);
                }
                long j = this.mListener.getConnectionId();
                int i4 = (j < -1L) ? -1 : (j == -1L) ? 0 : 1;
                label3: {
                    label2: {
                        if (i4 == 0) {
                            break label2;
                        }
                        if (a0.mConnectionID == null) {
                            break label2;
                        }
                        this.mListener.setConnectionId(com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToLong(a0.mConnectionID));
                        break label3;
                    }
                    this.mListener.setConnectionId(1L);
                }
                if (a0.mAuthResp == null) {
                    i = -1;
                } else {
                    if (this.handleAuthResp(a0.mAuthResp)) {
                        i = -1;
                    } else {
                        this.mListener.onAuthenticationFailure(com.navdy.hud.app.bluetooth.obex.ObexHelper.getTagValue((byte)1, a0.mAuthResp));
                        i = 193;
                    }
                    a0.mAuthResp = null;
                }
                a = null;
                if (i == 193) {
                    i0 = 7;
                    break label0;
                } else {
                    int i5 = 0;
                    if (a0.mAuthChall != null) {
                        this.handleAuthChall(a0);
                        a1.mAuthResp = new byte[a0.mAuthResp.length];
                        System.arraycopy(a0.mAuthResp, 0, a1.mAuthResp, 0, a1.mAuthResp.length);
                        a0.mAuthChall = null;
                        a0.mAuthResp = null;
                    }
                    try {
                        i = this.validateResponseCode(this.mListener.onConnect(a0, a1));
                        if (a1.nonce == null) {
                            this.mChallengeDigest = null;
                        } else {
                            this.mChallengeDigest = new byte[16];
                            System.arraycopy(a1.nonce, 0, this.mChallengeDigest, 0, 16);
                        }
                        long j0 = this.mListener.getConnectionId();
                        if (j0 != -1L) {
                            a1.mConnectionID = com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToByteArray(j0);
                        } else {
                            a1.mConnectionID = null;
                        }
                        a = com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(a1, false);
                        i0 = 7 + a.length;
                        i5 = this.mMaxPacketLength;
                    } catch(Exception ignoredException) {
                        break label1;
                    }
                    if (i0 <= i5) {
                        break label0;
                    }
                    a = null;
                    i = 208;
                    i0 = 7;
                    break label0;
                }
            } else {
                a = null;
                i = 206;
                i0 = 7;
                break label0;
            }
            a = null;
            i = 208;
            i0 = 7;
        }
        byte[] a3 = com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToByteArray((long)i0);
        byte[] a4 = new byte[i0];
        int i6 = com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxRxPacketSize(this.mTransport);
        int i7 = (byte)i;
        int i8 = (byte)i7;
        a4[0] = (byte)i8;
        int i9 = a3[2];
        int i10 = (byte)i9;
        a4[1] = (byte)i10;
        int i11 = a3[3];
        int i12 = (byte)i11;
        a4[2] = (byte)i12;
        a4[3] = (byte)16;
        a4[4] = (byte)0;
        int i13 = (byte)(i6 >> 8);
        int i14 = (byte)i13;
        a4[5] = (byte)i14;
        int i15 = (byte)(i6 & 255);
        int i16 = (byte)i15;
        a4[6] = (byte)i16;
        if (a != null) {
            System.arraycopy(a, 0, a4, 7, a.length);
        }
        this.mOutput.write(a4);
        this.mOutput.flush();
    }
    
    private void handleDisconnectRequest() {
        com.navdy.hud.app.bluetooth.obex.HeaderSet a = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        com.navdy.hud.app.bluetooth.obex.HeaderSet a0 = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        int i = (this.mInput.read() << 8) + this.mInput.read();
        int i0 = com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxRxPacketSize(this.mTransport);
        label4: {
            int i1 = 0;
            byte[] a1 = null;
            int i2 = 0;
            label0: {
                label1: if (i <= i0) {
                    if (i > 3) {
                        byte[] a2 = new byte[i - 3];
                        int i3 = this.mInput.read(a2);
                        while(i3 != a2.length) {
                            i3 = i3 + this.mInput.read(a2, i3, a2.length - i3);
                        }
                        com.navdy.hud.app.bluetooth.obex.ObexHelper.updateHeaderSet(a, a2);
                    }
                    long j = this.mListener.getConnectionId();
                    int i4 = (j < -1L) ? -1 : (j == -1L) ? 0 : 1;
                    label3: {
                        label2: {
                            if (i4 == 0) {
                                break label2;
                            }
                            if (a.mConnectionID == null) {
                                break label2;
                            }
                            this.mListener.setConnectionId(com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToLong(a.mConnectionID));
                            break label3;
                        }
                        this.mListener.setConnectionId(1L);
                    }
                    if (a.mAuthResp == null) {
                        i1 = 160;
                    } else {
                        if (this.handleAuthResp(a.mAuthResp)) {
                            i1 = 160;
                        } else {
                            this.mListener.onAuthenticationFailure(com.navdy.hud.app.bluetooth.obex.ObexHelper.getTagValue((byte)1, a.mAuthResp));
                            i1 = 193;
                        }
                        a.mAuthResp = null;
                    }
                    a1 = null;
                    if (i1 == 193) {
                        i2 = 3;
                        break label0;
                    } else {
                        if (a.mAuthChall != null) {
                            this.handleAuthChall(a);
                            a.mAuthChall = null;
                        }
                        try {
                            this.mListener.onDisconnect(a, a0);
                        } catch(Exception ignoredException) {
                            break label1;
                        }
                        long j0 = this.mListener.getConnectionId();
                        if (j0 != -1L) {
                            a0.mConnectionID = com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToByteArray(j0);
                        } else {
                            a0.mConnectionID = null;
                        }
                        a1 = com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(a0, false);
                        i2 = 3 + a1.length;
                        if (i2 <= this.mMaxPacketLength) {
                            break label0;
                        }
                        i2 = 3;
                        a1 = null;
                        i1 = 208;
                        break label0;
                    }
                } else {
                    i2 = 3;
                    a1 = null;
                    i1 = 206;
                    break label0;
                }
                this.sendResponse(208, (byte[])null);
                break label4;
            }
            byte[] a3 = (a1 == null) ? new byte[3] : new byte[a1.length + 3];
            int i5 = (byte)i1;
            int i6 = (byte)i5;
            a3[0] = (byte)i6;
            int i7 = (byte)(i2 >> 8);
            int i8 = (byte)i7;
            a3[1] = (byte)i8;
            int i9 = (byte)i2;
            int i10 = (byte)i9;
            a3[2] = (byte)i10;
            if (a1 != null) {
                System.arraycopy(a1, 0, a3, 3, a1.length);
            }
            this.mOutput.write(a3);
            this.mOutput.flush();
        }
    }
    
    private void handleGetRequest(int i) {
        com.navdy.hud.app.bluetooth.obex.ServerOperation a = new com.navdy.hud.app.bluetooth.obex.ServerOperation(this, this.mInput, i, this.mMaxPacketLength, this.mListener);
        try {
            int i0 = this.validateResponseCode(this.mListener.onGet((com.navdy.hud.app.bluetooth.obex.Operation)a));
            if (!a.isAborted) {
                a.sendReply(i0);
            }
        } catch(Exception ignoredException) {
            this.sendResponse(208, (byte[])null);
        }
    }
    
    private void handlePutRequest(int i) {
        com.navdy.hud.app.bluetooth.obex.ServerOperation a = new com.navdy.hud.app.bluetooth.obex.ServerOperation(this, this.mInput, i, this.mMaxPacketLength, this.mListener);
        label1: try {
            int i0 = 0;
            boolean b = a.finalBitSet;
            label3: {
                label2: {
                    if (!b) {
                        break label2;
                    }
                    if (a.isValidBody()) {
                        break label2;
                    }
                    i0 = this.validateResponseCode(this.mListener.onDelete(a.requestHeader, a.replyHeader));
                    break label3;
                }
                i0 = this.validateResponseCode(this.mListener.onPut((com.navdy.hud.app.bluetooth.obex.Operation)a));
            }
            label0: {
                if (i0 == 160) {
                    break label0;
                }
                if (a.isAborted) {
                    break label0;
                }
                a.sendReply(i0);
                break label1;
            }
            if (!a.isAborted) {
                while(!a.finalBitSet) {
                    a.sendReply(144);
                }
                a.sendReply(i0);
            }
        } catch(Exception ignoredException) {
            if (!a.isAborted) {
                this.sendResponse(208, (byte[])null);
            }
        }
    }
    
    private void handleSetPathRequest() {
        com.navdy.hud.app.bluetooth.obex.HeaderSet a = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        com.navdy.hud.app.bluetooth.obex.HeaderSet a0 = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        int i = (this.mInput.read() << 8) + this.mInput.read();
        int i0 = this.mInput.read();
        this.mInput.read();
        int i1 = com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxRxPacketSize(this.mTransport);
        label4: {
            int i2 = 0;
            byte[] a1 = null;
            int i3 = 0;
            label0: {
                label1: if (i <= i1) {
                    if (i <= 5) {
                        i2 = -1;
                    } else {
                        byte[] a2 = new byte[i - 5];
                        int i4 = this.mInput.read(a2);
                        while(i4 != a2.length) {
                            i4 = i4 + this.mInput.read(a2, i4, a2.length - i4);
                        }
                        com.navdy.hud.app.bluetooth.obex.ObexHelper.updateHeaderSet(a, a2);
                        long j = this.mListener.getConnectionId();
                        int i5 = (j < -1L) ? -1 : (j == -1L) ? 0 : 1;
                        label3: {
                            label2: {
                                if (i5 == 0) {
                                    break label2;
                                }
                                if (a.mConnectionID == null) {
                                    break label2;
                                }
                                this.mListener.setConnectionId(com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToLong(a.mConnectionID));
                                break label3;
                            }
                            this.mListener.setConnectionId(1L);
                        }
                        if (a.mAuthResp == null) {
                            i2 = -1;
                        } else {
                            if (this.handleAuthResp(a.mAuthResp)) {
                                i2 = -1;
                            } else {
                                this.mListener.onAuthenticationFailure(com.navdy.hud.app.bluetooth.obex.ObexHelper.getTagValue((byte)1, a.mAuthResp));
                                i2 = 193;
                            }
                            a.mAuthResp = null;
                        }
                    }
                    a1 = null;
                    if (i2 == 193) {
                        i3 = 3;
                        break label0;
                    } else {
                        int i6 = 0;
                        if (a.mAuthChall != null) {
                            this.handleAuthChall(a);
                            a0.mAuthResp = new byte[a.mAuthResp.length];
                            System.arraycopy(a.mAuthResp, 0, a0.mAuthResp, 0, a0.mAuthResp.length);
                            a.mAuthChall = null;
                            a.mAuthResp = null;
                        }
                        boolean b = (i0 & 1) != 0;
                        boolean b0 = (i0 & 2) == 0;
                        try {
                            i6 = this.mListener.onSetPath(a, a0, b, b0);
                        } catch(Exception ignoredException) {
                            break label1;
                        }
                        i2 = this.validateResponseCode(i6);
                        if (a0.nonce == null) {
                            this.mChallengeDigest = null;
                        } else {
                            this.mChallengeDigest = new byte[16];
                            System.arraycopy(a0.nonce, 0, this.mChallengeDigest, 0, 16);
                        }
                        long j0 = this.mListener.getConnectionId();
                        if (j0 != -1L) {
                            a0.mConnectionID = com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToByteArray(j0);
                        } else {
                            a0.mConnectionID = null;
                        }
                        a1 = com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(a0, false);
                        i3 = 3 + a1.length;
                        if (i3 <= this.mMaxPacketLength) {
                            break label0;
                        }
                        a1 = null;
                        i2 = 208;
                        i3 = 3;
                        break label0;
                    }
                } else {
                    a1 = null;
                    i2 = 206;
                    i3 = 3;
                    break label0;
                }
                this.sendResponse(208, (byte[])null);
                break label4;
            }
            byte[] a3 = new byte[i3];
            int i7 = (byte)i2;
            int i8 = (byte)i7;
            a3[0] = (byte)i8;
            int i9 = (byte)(i3 >> 8);
            int i10 = (byte)i9;
            a3[1] = (byte)i10;
            int i11 = (byte)i3;
            int i12 = (byte)i11;
            a3[2] = (byte)i12;
            if (a1 != null) {
                System.arraycopy(a1, 0, a3, 3, a1.length);
            }
            this.mOutput.write(a3);
            this.mOutput.flush();
        }
    }
    
    private int validateResponseCode(int i) {
        label0: {
            label5: {
                if (i < 160) {
                    break label5;
                }
                if (i > 166) {
                    break label5;
                }
                break label0;
            }
            label4: {
                if (i < 176) {
                    break label4;
                }
                if (i <= 181) {
                    break label0;
                }
            }
            label3: {
                if (i < 192) {
                    break label3;
                }
                if (i <= 207) {
                    break label0;
                }
            }
            label2: {
                if (i < 208) {
                    break label2;
                }
                if (i <= 213) {
                    break label0;
                }
            }
            label1: {
                if (i < 224) {
                    break label1;
                }
                if (i <= 225) {
                    break label0;
                }
            }
            i = 208;
        }
        return i;
    }
    
    public void close() {
        synchronized(this) {
            if (this.mListener != null) {
                this.mListener.onClose();
            }
            try {
                this.mClosed = true;
                if (this.mInput != null) {
                    this.mInput.close();
                }
                if (this.mOutput != null) {
                    this.mOutput.close();
                }
                if (this.mTransport != null) {
                    this.mTransport.close();
                }
            } catch(Exception ignoredException) {
            }
            this.mTransport = null;
            this.mInput = null;
            this.mOutput = null;
            this.mListener = null;
        }
        /*monexit(this)*/;
    }
    
    public com.navdy.hud.app.bluetooth.obex.ObexTransport getTransport() {
        return this.mTransport;
    }
    
    public void run() {
        boolean b = false;
        while(true) {
            label1: {
                Exception a = null;
                if (b) {
                    break label1;
                }
                label0: {
                    NullPointerException a0 = null;
                    try {
                        try {
                            if (this.mClosed) {
                                break label1;
                            }
                            int i = this.mInput.read();
                            switch(i) {
                                case 255: {
                                    this.handleAbortRequest();
                                    continue;
                                }
                                case 133: {
                                    this.handleSetPathRequest();
                                    continue;
                                }
                                case 129: {
                                    this.handleDisconnectRequest();
                                    b = true;
                                    continue;
                                }
                                case 128: {
                                    this.handleConnectRequest();
                                    continue;
                                }
                                case 3: case 131: {
                                    this.handleGetRequest(i);
                                    continue;
                                }
                                case 2: case 130: {
                                    this.handlePutRequest(i);
                                    continue;
                                }
                                case -1: {
                                    b = true;
                                    continue;
                                }
                                default: {
                                    int i0 = this.mInput.read();
                                    int i1 = this.mInput.read();
                                    int i2 = 3;
                                    while(i2 < (i0 << 8) + i1) {
                                        this.mInput.read();
                                        i2 = i2 + 1;
                                    }
                                    this.sendResponse(209, (byte[])null);
                                    continue;
                                }
                            }
                        } catch(NullPointerException a1) {
                            a0 = a1;
                        }
                    } catch(Exception a2) {
                        a = a2;
                        break label0;
                    }
                    android.util.Log.d("Obex ServerSession", "Exception occured - ignoring", (Throwable)a0);
                    break label1;
                }
                android.util.Log.d("Obex ServerSession", "Exception occured - ignoring", (Throwable)a);
            }
            this.close();
            return;
        }
    }
    
    public void sendResponse(int i, byte[] a) {
        java.io.OutputStream a0 = this.mOutput;
        if (a0 != null) {
            byte[] a1 = null;
            if (a == null) {
                a1 = new byte[3];
                int i0 = (byte)i;
                int i1 = (byte)i0;
                a1[0] = (byte)i1;
                a1[1] = (byte)0;
                a1[2] = (byte)3;
            } else {
                int i2 = 3 + a.length;
                a1 = new byte[i2];
                int i3 = (byte)i;
                int i4 = (byte)i3;
                a1[0] = (byte)i4;
                int i5 = (byte)(i2 >> 8);
                int i6 = (byte)i5;
                a1[1] = (byte)i6;
                int i7 = (byte)i2;
                int i8 = (byte)i7;
                a1[2] = (byte)i8;
                System.arraycopy(a, 0, a1, 3, a.length);
            }
            a0.write(a1);
            a0.flush();
        }
    }
}
