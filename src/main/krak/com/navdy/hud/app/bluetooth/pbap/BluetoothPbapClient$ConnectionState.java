package com.navdy.hud.app.bluetooth.pbap;


public enum BluetoothPbapClient$ConnectionState {
    DISCONNECTED(0),
    CONNECTING(1),
    CONNECTED(2),
    DISCONNECTING(3);

    private int value;
    BluetoothPbapClient$ConnectionState(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class BluetoothPbapClient$ConnectionState extends Enum {
//    final private static com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState[] $VALUES;
//    final public static com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState CONNECTED;
//    final public static com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState CONNECTING;
//    final public static com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState DISCONNECTED;
//    final public static com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState DISCONNECTING;
//    
//    static {
//        DISCONNECTED = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState("DISCONNECTED", 0);
//        CONNECTING = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState("CONNECTING", 1);
//        CONNECTED = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState("CONNECTED", 2);
//        DISCONNECTING = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState("DISCONNECTING", 3);
//        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState[] a = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState[4];
//        a[0] = DISCONNECTED;
//        a[1] = CONNECTING;
//        a[2] = CONNECTED;
//        a[3] = DISCONNECTING;
//        $VALUES = a;
//    }
//    
//    private BluetoothPbapClient$ConnectionState(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState valueOf(String s) {
//        return (com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState)Enum.valueOf(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState.class, s);
//    }
//    
//    public static com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient$ConnectionState[] values() {
//        return $VALUES.clone();
//    }
//}
//