package com.navdy.hud.app.bluetooth.obex;

public class ObexPacket {
    public int mHeaderId;
    public int mLength;
    public byte[] mPayload;
    
    private ObexPacket(int i, int i0) {
        this.mPayload = null;
        this.mHeaderId = i;
        this.mLength = i0;
    }
    
    public static com.navdy.hud.app.bluetooth.obex.ObexPacket read(int i, java.io.InputStream a) {
        int i0 = (a.read() << 8) + a.read();
        com.navdy.hud.app.bluetooth.obex.ObexPacket a0 = new com.navdy.hud.app.bluetooth.obex.ObexPacket(i, i0);
        byte[] a1 = null;
        if (i0 > 3) {
            a1 = new byte[i0 - 3];
            int i1 = a.read(a1);
            while(i1 != a1.length) {
                i1 = i1 + a.read(a1, i1, a1.length - i1);
            }
        }
        a0.mPayload = a1;
        return a0;
    }
    
    public static com.navdy.hud.app.bluetooth.obex.ObexPacket read(java.io.InputStream a) {
        return com.navdy.hud.app.bluetooth.obex.ObexPacket.read(a.read(), a);
    }
}
