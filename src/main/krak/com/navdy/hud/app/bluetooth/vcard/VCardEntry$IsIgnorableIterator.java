package com.navdy.hud.app.bluetooth.vcard;

class VCardEntry$IsIgnorableIterator implements com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElementIterator {
    private boolean mEmpty;
    final com.navdy.hud.app.bluetooth.vcard.VCardEntry this$0;
    
    private VCardEntry$IsIgnorableIterator(com.navdy.hud.app.bluetooth.vcard.VCardEntry a) {
        super();
        this.this$0 = a;
        this.mEmpty = true;
    }
    
    VCardEntry$IsIgnorableIterator(com.navdy.hud.app.bluetooth.vcard.VCardEntry a, com.navdy.hud.app.bluetooth.vcard.VCardEntry$1 a0) {
        this(a);
    }
    
    public boolean getResult() {
        return this.mEmpty;
    }
    
    public boolean onElement(com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement a) {
        boolean b = false;
        if (a.isEmpty()) {
            b = true;
        } else {
            this.mEmpty = false;
            b = false;
        }
        return b;
    }
    
    public void onElementGroupEnded() {
    }
    
    public void onElementGroupStarted(com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel a) {
    }
    
    public void onIterationEnded() {
    }
    
    public void onIterationStarted() {
    }
}
