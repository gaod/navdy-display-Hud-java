package com.navdy.hud.app.bluetooth.vcard;

abstract public interface VCardEntry$EntryElement {
    abstract public void constructInsertOperation(java.util.List arg, int arg0);
    
    
    abstract public com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel getEntryLabel();
    
    
    abstract public boolean isEmpty();
}
