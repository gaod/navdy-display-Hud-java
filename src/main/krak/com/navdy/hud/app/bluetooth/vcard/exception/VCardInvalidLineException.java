package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardInvalidLineException extends com.navdy.hud.app.bluetooth.vcard.exception.VCardException {
    public VCardInvalidLineException() {
    }
    
    public VCardInvalidLineException(String s) {
        super(s);
    }
}
