package com.navdy.hud.app.bluetooth.vcard;

public class VCardSourceDetector implements com.navdy.hud.app.bluetooth.vcard.VCardInterpreter {
    private static java.util.Set APPLE_SIGNS;
    private static java.util.Set FOMA_SIGNS;
    private static java.util.Set JAPANESE_MOBILE_PHONE_SIGNS;
    final private static String LOG_TAG = "vCard";
    final private static int PARSE_TYPE_APPLE = 1;
    final private static int PARSE_TYPE_DOCOMO_FOMA = 3;
    final private static int PARSE_TYPE_MOBILE_PHONE_JP = 2;
    final public static int PARSE_TYPE_UNKNOWN = 0;
    final private static int PARSE_TYPE_WINDOWS_MOBILE_V65_JP = 4;
    private static String TYPE_FOMA_CHARSET_SIGN;
    private static java.util.Set WINDOWS_MOBILE_PHONE_SIGNS;
    private int mParseType;
    private String mSpecifiedCharset;
    private int mVersion;
    
    static {
        String[] a = new String[5];
        a[0] = "X-PHONETIC-FIRST-NAME";
        a[1] = "X-PHONETIC-MIDDLE-NAME";
        a[2] = "X-PHONETIC-LAST-NAME";
        a[3] = "X-ABADR";
        a[4] = "X-ABUID";
        APPLE_SIGNS = (java.util.Set)new java.util.HashSet((java.util.Collection)java.util.Arrays.asList((Object[])a));
        String[] a0 = new String[3];
        a0[0] = "X-GNO";
        a0[1] = "X-GN";
        a0[2] = "X-REDUCTION";
        JAPANESE_MOBILE_PHONE_SIGNS = (java.util.Set)new java.util.HashSet((java.util.Collection)java.util.Arrays.asList((Object[])a0));
        String[] a1 = new String[3];
        a1[0] = "X-MICROSOFT-ASST_TEL";
        a1[1] = "X-MICROSOFT-ASSISTANT";
        a1[2] = "X-MICROSOFT-OFFICELOC";
        WINDOWS_MOBILE_PHONE_SIGNS = (java.util.Set)new java.util.HashSet((java.util.Collection)java.util.Arrays.asList((Object[])a1));
        String[] a2 = new String[6];
        a2[0] = "X-SD-VERN";
        a2[1] = "X-SD-FORMAT_VER";
        a2[2] = "X-SD-CATEGORIES";
        a2[3] = "X-SD-CLASS";
        a2[4] = "X-SD-DCREATED";
        a2[5] = "X-SD-DESCRIPTION";
        FOMA_SIGNS = (java.util.Set)new java.util.HashSet((java.util.Collection)java.util.Arrays.asList((Object[])a2));
        TYPE_FOMA_CHARSET_SIGN = "X-SD-CHAR_CODE";
    }
    
    public VCardSourceDetector() {
        this.mParseType = 0;
        this.mVersion = -1;
    }
    
    public String getEstimatedCharset() {
        String s = null;
        if (android.text.TextUtils.isEmpty((CharSequence)this.mSpecifiedCharset)) {
            s = this.mSpecifiedCharset;
        } else {
            switch(this.mParseType) {
                case 2: case 3: case 4: {
                    s = "SHIFT_JIS";
                    break;
                }
                case 1: {
                    s = "UTF-8";
                    break;
                }
                default: {
                    s = null;
                }
            }
        }
        return s;
    }
    
    public int getEstimatedType() {
        int i = 0;
        switch(this.mParseType) {
            case 3: {
                i = 939524104;
                break;
            }
            case 2: {
                i = 402653192;
                break;
            }
            default: {
                i = (this.mVersion != 0) ? (this.mVersion != 1) ? (this.mVersion != 2) ? 0 : -1073741822 : -1073741823 : -1073741824;
            }
        }
        return i;
    }
    
    public void onEntryEnded() {
    }
    
    public void onEntryStarted() {
    }
    
    public void onPropertyCreated(com.navdy.hud.app.bluetooth.vcard.VCardProperty a) {
        String s = a.getName();
        java.util.List a0 = a.getValueList();
        boolean b = s.equalsIgnoreCase("VERSION");
        label0: {
            label1: {
                label2: {
                    if (!b) {
                        break label2;
                    }
                    if (a0.size() > 0) {
                        break label1;
                    }
                }
                if (!s.equalsIgnoreCase(TYPE_FOMA_CHARSET_SIGN)) {
                    break label0;
                }
                this.mParseType = 3;
                if (a0.size() <= 0) {
                    break label0;
                }
                this.mSpecifiedCharset = (String)a0.get(0);
                break label0;
            }
            String s0 = (String)a0.get(0);
            if (s0.equals("2.1")) {
                this.mVersion = 0;
            } else if (s0.equals("3.0")) {
                this.mVersion = 1;
            } else if (s0.equals("4.0")) {
                this.mVersion = 2;
            } else {
                android.util.Log.w("vCard", new StringBuilder().append("Invalid version string: ").append(s0).toString());
            }
        }
        if (this.mParseType == 0) {
            if (WINDOWS_MOBILE_PHONE_SIGNS.contains(s)) {
                this.mParseType = 4;
            } else if (FOMA_SIGNS.contains(s)) {
                this.mParseType = 3;
            } else if (JAPANESE_MOBILE_PHONE_SIGNS.contains(s)) {
                this.mParseType = 2;
            } else if (APPLE_SIGNS.contains(s)) {
                this.mParseType = 1;
            }
        }
    }
    
    public void onVCardEnded() {
    }
    
    public void onVCardStarted() {
    }
}
