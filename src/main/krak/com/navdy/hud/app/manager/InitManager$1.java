package com.navdy.hud.app.manager;

class InitManager$1 implements Runnable {
    public boolean notified;
    final com.navdy.hud.app.manager.InitManager this$0;
    
    InitManager$1(com.navdy.hud.app.manager.InitManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        if (!this.notified && com.navdy.hud.app.manager.InitManager.access$000(this.this$0).serviceConnected() && android.bluetooth.BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            com.navdy.hud.app.manager.InitManager.access$100().i("bt and connection service ready");
            this.notified = true;
            com.navdy.hud.app.manager.InitManager.access$200(this.this$0).post(new com.navdy.hud.app.event.InitEvents$InitPhase(com.navdy.hud.app.event.InitEvents$Phase.CONNECTION_SERVICE_STARTED));
            com.navdy.hud.app.manager.InitManager.access$300(this.this$0).post((Runnable)new com.navdy.hud.app.manager.InitManager$PhaseEmitter(this.this$0, com.navdy.hud.app.event.InitEvents$Phase.POST_START));
            com.navdy.hud.app.manager.InitManager.access$300(this.this$0).post((Runnable)new com.navdy.hud.app.manager.InitManager$PhaseEmitter(this.this$0, com.navdy.hud.app.event.InitEvents$Phase.LATE));
        }
    }
}
