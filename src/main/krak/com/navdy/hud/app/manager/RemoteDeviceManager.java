package com.navdy.hud.app.manager;

final public class RemoteDeviceManager {
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.hud.app.manager.RemoteDeviceManager singleton;
    @Inject
    com.squareup.otto.Bus bus;
    @Inject
    com.navdy.hud.app.framework.calendar.CalendarManager calendarManager;
    @Inject
    com.navdy.hud.app.framework.phonecall.CallManager callManager;
    @Inject
    com.navdy.hud.app.service.ConnectionHandler connectionHandler;
    @Inject
    com.navdy.hud.app.debug.DriveRecorder driveRecorder;
    @Inject
    com.navdy.hud.app.util.FeatureUtil featureUtil;
    @Inject
    com.navdy.hud.app.gesture.GestureServiceConnector gestureServiceConnector;
    @Inject
    com.navdy.service.library.network.http.IHttpManager httpManager;
    @Inject
    com.navdy.hud.app.manager.InputManager inputManager;
    @Inject
    com.navdy.hud.app.manager.MusicManager musicManager;
    @Inject
    android.content.SharedPreferences preferences;
    @Inject
    com.navdy.hud.app.analytics.TelemetryDataManager telemetryDataManager;
    @Inject
    com.navdy.hud.app.common.TimeHelper timeHelper;
    @Inject
    com.navdy.hud.app.framework.trips.TripManager tripManager;
    @Inject
    com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    @Inject
    com.navdy.hud.app.framework.voice.VoiceSearchHandler voiceSearchHandler;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.manager.RemoteDeviceManager.class);
        singleton = new com.navdy.hud.app.manager.RemoteDeviceManager();
    }
    
    private RemoteDeviceManager() {
        android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
        if (a != null) {
            mortar.Mortar.inject(a, this);
        }
    }
    
    public static com.navdy.hud.app.manager.RemoteDeviceManager getInstance() {
        return singleton;
    }
    
    public boolean doesRemoteDeviceHasCapability(com.navdy.service.library.events.LegacyCapability a) {
        boolean b = false;
        com.navdy.service.library.device.RemoteDevice a0 = this.connectionHandler.getRemoteDevice();
        if (a0 == null) {
            b = false;
        } else {
            com.navdy.service.library.events.DeviceInfo a1 = a0.getDeviceInfo();
            b = a1 != null && a1.legacyCapabilities != null && a1.legacyCapabilities.contains(a);
        }
        return b;
    }
    
    public com.squareup.otto.Bus getBus() {
        return this.bus;
    }
    
    public com.navdy.hud.app.framework.calendar.CalendarManager getCalendarManager() {
        return this.calendarManager;
    }
    
    public com.navdy.hud.app.framework.phonecall.CallManager getCallManager() {
        return this.callManager;
    }
    
    public com.navdy.hud.app.service.ConnectionHandler getConnectionHandler() {
        return this.connectionHandler;
    }
    
    public com.navdy.service.library.device.NavdyDeviceId getDeviceId() {
        com.navdy.service.library.device.RemoteDevice a = this.connectionHandler.getRemoteDevice();
        com.navdy.service.library.device.NavdyDeviceId a0 = (a == null) ? null : a.getDeviceId();
        return a0;
    }
    
    public com.navdy.hud.app.debug.DriveRecorder getDriveRecorder() {
        return this.driveRecorder;
    }
    
    public com.navdy.hud.app.util.FeatureUtil getFeatureUtil() {
        return this.featureUtil;
    }
    
    public com.navdy.hud.app.gesture.GestureServiceConnector getGestureServiceConnector() {
        return this.gestureServiceConnector;
    }
    
    public com.navdy.service.library.network.http.IHttpManager getHttpManager() {
        return this.httpManager;
    }
    
    public com.navdy.hud.app.manager.InputManager getInputManager() {
        return this.inputManager;
    }
    
    public com.navdy.hud.app.manager.MusicManager getMusicManager() {
        return this.musicManager;
    }
    
    public com.navdy.service.library.events.DeviceInfo getRemoteDeviceInfo() {
        com.navdy.service.library.device.RemoteDevice a = this.connectionHandler.getRemoteDevice();
        com.navdy.service.library.events.DeviceInfo a0 = (a == null) ? null : a.getDeviceInfo();
        return a0;
    }
    
    public com.navdy.service.library.events.DeviceInfo$Platform getRemoteDevicePlatform() {
        com.navdy.service.library.events.DeviceInfo$Platform a = null;
        com.navdy.service.library.device.RemoteDevice a0 = this.connectionHandler.getRemoteDevice();
        if (a0 == null) {
            a = null;
        } else {
            com.navdy.service.library.events.DeviceInfo a1 = a0.getDeviceInfo();
            a = (a1 == null) ? com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS : a1.platform;
        }
        return a;
    }
    
    public android.content.SharedPreferences getSharedPreferences() {
        return this.preferences;
    }
    
    public com.navdy.hud.app.analytics.TelemetryDataManager getTelemetryDataManager() {
        return this.telemetryDataManager;
    }
    
    public com.navdy.hud.app.common.TimeHelper getTimeHelper() {
        return this.timeHelper;
    }
    
    public com.navdy.hud.app.framework.trips.TripManager getTripManager() {
        return this.tripManager;
    }
    
    public com.navdy.hud.app.ui.framework.UIStateManager getUiStateManager() {
        return this.uiStateManager;
    }
    
    public com.navdy.hud.app.framework.voice.VoiceSearchHandler getVoiceSearchHandler() {
        return this.voiceSearchHandler;
    }
    
    public boolean isAppConnected() {
        return this.isRemoteDeviceConnected() && !this.connectionHandler.isAppClosed();
    }
    
    public boolean isNetworkLinkReady() {
        return this.connectionHandler.isNetworkLinkReady();
    }
    
    public boolean isRemoteDeviceConnected() {
        return this.getRemoteDeviceInfo() != null;
    }
    
    public boolean isRemoteDeviceIos() {
        com.navdy.service.library.events.DeviceInfo$Platform a = this.getRemoteDevicePlatform();
        return com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS.equals(a);
    }
}
