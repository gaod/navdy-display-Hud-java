package com.navdy.hud.app.manager;

class UpdateReminderManager$ReminderRunnable implements Runnable {
    boolean isDialUpdate;
    final com.navdy.hud.app.manager.UpdateReminderManager this$0;
    
    UpdateReminderManager$ReminderRunnable(com.navdy.hud.app.manager.UpdateReminderManager a, boolean b) {
        super();
        this.this$0 = a;
        this.isDialUpdate = b;
    }
    
    public void run() {
        android.os.Bundle a = new android.os.Bundle();
        if (this.isDialUpdate) {
            com.navdy.hud.app.device.dial.DialManager a0 = com.navdy.hud.app.device.dial.DialManager.getInstance();
            if (a0.isDialConnected()) {
                if (a0.getDialFirmwareUpdater().getVersions().isUpdateAvailable()) {
                    a.putBoolean("UPDATE_REMINDER", true);
                    if (!com.navdy.hud.app.screen.DialUpdateProgressScreen.updateStarted) {
                        this.this$0.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_UPDATE_CONFIRMATION, a, false));
                    }
                    this.this$0.sharedPreferences.edit().putLong("dial_reminder_time", System.currentTimeMillis() + com.navdy.hud.app.manager.UpdateReminderManager.access$000()).apply();
                } else {
                    this.this$0.sharedPreferences.edit().remove("dial_reminder_time").apply();
                }
            } else {
                this.this$0.sharedPreferences.edit().putLong("dial_reminder_time", System.currentTimeMillis() + com.navdy.hud.app.manager.UpdateReminderManager.access$100()).apply();
            }
        } else if (com.navdy.hud.app.manager.UpdateReminderManager.access$200(this.this$0)) {
            a.putBoolean("UPDATE_REMINDER", true);
            this.this$0.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_OTA_CONFIRMATION, a, false));
            this.this$0.sharedPreferences.edit().putLong("ota_reminder_time", System.currentTimeMillis() + com.navdy.hud.app.manager.UpdateReminderManager.access$000()).apply();
        } else {
            this.this$0.sharedPreferences.edit().remove("ota_reminder_time").apply();
        }
        com.navdy.hud.app.manager.UpdateReminderManager.access$300(this.this$0);
    }
}
