package com.navdy.hud.app.manager;

final public class UpdateReminderManager$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding sharedPreferences;
    
    public UpdateReminderManager$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.manager.UpdateReminderManager", false, com.navdy.hud.app.manager.UpdateReminderManager.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.manager.UpdateReminderManager.class, (this).getClass().getClassLoader());
        this.sharedPreferences = a.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.manager.UpdateReminderManager.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.bus);
        a0.add(this.sharedPreferences);
    }
    
    public void injectMembers(com.navdy.hud.app.manager.UpdateReminderManager a) {
        a.bus = (com.squareup.otto.Bus)this.bus.get();
        a.sharedPreferences = (android.content.SharedPreferences)this.sharedPreferences.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.manager.UpdateReminderManager)a);
    }
}
