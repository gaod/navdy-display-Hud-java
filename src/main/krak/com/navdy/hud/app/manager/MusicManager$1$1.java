package com.navdy.hud.app.manager;

class MusicManager$1$1 implements com.navdy.hud.app.util.MusicArtworkCache$Callback {
    final com.navdy.hud.app.manager.MusicManager$1 this$1;
    
    MusicManager$1$1(com.navdy.hud.app.manager.MusicManager$1 a) {
        super();
        this.this$1 = a;
    }
    
    public void onHit(byte[] a) {
        com.navdy.hud.app.manager.MusicManager.access$000().d("CACHE HIT");
        com.navdy.hud.app.manager.MusicManager.access$102(this.this$1.this$0, okio.ByteString.of(a));
        com.navdy.hud.app.manager.MusicManager.access$200(this.this$1.this$0);
    }
    
    public void onMiss() {
        com.navdy.hud.app.manager.MusicManager.access$000().d("CACHE MISS");
        com.navdy.service.library.events.audio.MusicArtworkRequest a = new com.navdy.service.library.events.audio.MusicArtworkRequest$Builder().name(this.this$1.val$trackInfo.name).album(this.this$1.val$trackInfo.album).author(this.this$1.val$trackInfo.author).size(Integer.valueOf(200)).build();
        com.navdy.hud.app.manager.MusicManager.access$000().d(new StringBuilder().append("requesting artwork: ").append(a).toString());
        com.navdy.hud.app.manager.MusicManager.access$300(this.this$1.this$0).post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a));
    }
}
