package com.navdy.hud.app.manager;

public class PairingManager {
    private boolean autoPairing;
    
    public PairingManager() {
    }
    
    public boolean isAutoPairing() {
        return this.autoPairing;
    }
    
    public void setAutoPairing(boolean b) {
        this.autoPairing = b;
    }
}
