package com.navdy.hud.app.manager;

abstract public interface MusicManager$MusicUpdateListener {
    abstract public void onAlbumArtUpdate(okio.ByteString arg, boolean arg0);
    
    
    abstract public void onTrackUpdated(com.navdy.service.library.events.audio.MusicTrackInfo arg, java.util.Set arg0, boolean arg1);
}
