package com.navdy.hud.app.analytics;

final public class TelemetrySessionKt {
    final public static float KMPHToMetersPerSecond(float f) {
        return 0.277778f * f;
    }
    
    final public static float MPHToMetersPerSecond(float f) {
        return 0.44704f * f;
    }
    
    final public static float MetersPerSecondToKMPH(float f) {
        return 3.6f * f;
    }
    
    final public static float MetersPerSecondToMPH(float f) {
        return 2.23694f * f;
    }
    
    final public static long milliSecondsToSeconds(long j) {
        return j / 1000L;
    }
    
    final public static long timeSince(long j, long j0) {
        return j - j0;
    }
}
