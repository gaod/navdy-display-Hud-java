package com.navdy.hud.app.analytics;

final public class AnalyticsSupport$NotificationReceiver$$InjectAdapter extends dagger.internal.Binding implements javax.inject.Provider, dagger.MembersInjector {
    private dagger.internal.Binding appPreferences;
    private dagger.internal.Binding powerManager;
    
    public AnalyticsSupport$NotificationReceiver$$InjectAdapter() {
        super("com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver", "members/com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver", false, com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.powerManager = a.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver.class, (this).getClass().getClassLoader());
        this.appPreferences = a.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver get() {
        com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver a = new com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver();
        this.injectMembers(a);
        return a;
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.powerManager);
        a0.add(this.appPreferences);
    }
    
    public void injectMembers(com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver a) {
        a.powerManager = (com.navdy.hud.app.device.PowerManager)this.powerManager.get();
        a.appPreferences = (android.content.SharedPreferences)this.appPreferences.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver)a);
    }
}
