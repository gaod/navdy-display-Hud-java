package com.navdy.hud.app.analytics;


    public enum NavigationQualityTracker$TrafficLevel {
        NO_DATA(0),
    LIGHT(1),
    MODERATE(2),
    HEAVY(3);

        private int value;
        NavigationQualityTracker$TrafficLevel(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class NavigationQualityTracker$TrafficLevel extends Enum {
//    final private static com.navdy.hud.app.analytics.NavigationQualityTracker$TrafficLevel[] $VALUES;
//    final public static com.navdy.hud.app.analytics.NavigationQualityTracker$TrafficLevel HEAVY;
//    final public static com.navdy.hud.app.analytics.NavigationQualityTracker$TrafficLevel LIGHT;
//    final public static com.navdy.hud.app.analytics.NavigationQualityTracker$TrafficLevel MODERATE;
//    final public static com.navdy.hud.app.analytics.NavigationQualityTracker$TrafficLevel NO_DATA;
//    
//    static {
//        NO_DATA = new com.navdy.hud.app.analytics.NavigationQualityTracker$TrafficLevel("NO_DATA", 0);
//        LIGHT = new com.navdy.hud.app.analytics.NavigationQualityTracker$TrafficLevel("LIGHT", 1);
//        MODERATE = new com.navdy.hud.app.analytics.NavigationQualityTracker$TrafficLevel("MODERATE", 2);
//        HEAVY = new com.navdy.hud.app.analytics.NavigationQualityTracker$TrafficLevel("HEAVY", 3);
//        com.navdy.hud.app.analytics.NavigationQualityTracker$TrafficLevel[] a = new com.navdy.hud.app.analytics.NavigationQualityTracker$TrafficLevel[4];
//        a[0] = NO_DATA;
//        a[1] = LIGHT;
//        a[2] = MODERATE;
//        a[3] = HEAVY;
//        $VALUES = a;
//    }
//    
//    private NavigationQualityTracker$TrafficLevel(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.analytics.NavigationQualityTracker$TrafficLevel valueOf(String s) {
//        return (com.navdy.hud.app.analytics.NavigationQualityTracker$TrafficLevel)Enum.valueOf(com.navdy.hud.app.analytics.NavigationQualityTracker$TrafficLevel.class, s);
//    }
//    
//    public static com.navdy.hud.app.analytics.NavigationQualityTracker$TrafficLevel[] values() {
//        return $VALUES.clone();
//    }
//}
//