package com.navdy.hud.app.analytics;

class AnalyticsSupport$3 {
    final static int[] $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState;
    final static int[] $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$FeatureMode;
    final static int[] $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RerouteForTraffic;
    
    static {
        $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState = new int[com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState;
        com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState a0 = com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState.VOICE_SEARCH_STARTING;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState[com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState.VOICE_SEARCH_LISTENING.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState[com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState.VOICE_SEARCH_SUCCESS.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState[com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState.VOICE_SEARCH_SEARCHING.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState[com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState.VOICE_SEARCH_ERROR.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RerouteForTraffic = new int[com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic.values().length];
        int[] a1 = $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RerouteForTraffic;
        com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic a2 = com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic.REROUTE_AUTOMATIC;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RerouteForTraffic[com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic.REROUTE_CONFIRM.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RerouteForTraffic[com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic.REROUTE_NEVER.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException6) {
        }
        $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$FeatureMode = new int[com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode.values().length];
        int[] a3 = $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$FeatureMode;
        com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode a4 = com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode.FEATURE_MODE_BETA;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$FeatureMode[com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode.FEATURE_MODE_RELEASE.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$FeatureMode[com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode.FEATURE_MODE_EXPERIMENTAL.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException9) {
        }
    }
}
