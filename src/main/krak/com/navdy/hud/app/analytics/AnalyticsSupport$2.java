package com.navdy.hud.app.analytics;

final class AnalyticsSupport$2 extends android.content.BroadcastReceiver {
    AnalyticsSupport$2() {
    }
    
    public void onReceive(android.content.Context a, android.content.Intent a0) {
        String s = a0.getAction();
        label2: if ("android.bluetooth.device.action.ACL_DISCONNECTED".equals(s)) {
            android.bluetooth.BluetoothDevice a1 = (android.bluetooth.BluetoothDevice)a0.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            label0: {
                if (a1 == null) {
                    break label0;
                }
                int i = a1.getType();
                label1: {
                    if (i == 1) {
                        break label1;
                    }
                    if (a1.getType() != 3) {
                        break label0;
                    }
                }
                com.navdy.hud.app.analytics.AnalyticsSupport.access$3102(a0.getIntExtra("android.bluetooth.device.extra.STATUS", 0));
                com.navdy.hud.app.analytics.AnalyticsSupport.access$000().d(new StringBuilder().append("ACTION_ACL_DISCONNECTED, Disconnect reason ").append(com.navdy.hud.app.analytics.AnalyticsSupport.access$3100()).append(" ").append(a0).toString());
                break label2;
            }
            int i0 = a0.getIntExtra("android.bluetooth.device.extra.STATUS", 0);
            com.navdy.hud.app.analytics.AnalyticsSupport.access$000().d(new StringBuilder().append("ACTION_ACL_DISCONNECTED, Non classic device , Disconnect reason ").append(i0).toString());
        } else if ("com.navdy.hud.app.force_reconnect".equals(s)) {
            com.navdy.hud.app.analytics.AnalyticsSupport.access$3202(true);
            com.navdy.hud.app.analytics.AnalyticsSupport.access$3302(a0.getStringExtra("force_reconnect_reason"));
            com.navdy.hud.app.analytics.AnalyticsSupport.access$000().d(new StringBuilder().append("ACTION_DEVICE_FORCE_RECONNECT, Reason : ").append(com.navdy.hud.app.analytics.AnalyticsSupport.access$3300()).append(" ").append(a0).toString());
        }
    }
}
