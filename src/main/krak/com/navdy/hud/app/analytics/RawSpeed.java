package com.navdy.hud.app.analytics;

final public class RawSpeed {
    final private float speed;
    final private long timeStamp;
    
    public RawSpeed(float f, long j) {
        this.speed = f;
        this.timeStamp = j;
    }
    
    public RawSpeed(float f, long j, int i, kotlin.jvm.internal.DefaultConstructorMarker a) {
        if ((i & 2) != 0) {
            j = 0L;
        }
        this(f, j);
    }
    
    public static com.navdy.hud.app.analytics.RawSpeed copy$default(com.navdy.hud.app.analytics.RawSpeed a, float f, long j, int i, Object a0) {
        if ((i & 1) != 0) {
            f = a.speed;
        }
        if ((i & 2) != 0) {
            j = a.timeStamp;
        }
        return a.copy(f, j);
    }
    
    final public float component1() {
        return this.speed;
    }
    
    final public long component2() {
        return this.timeStamp;
    }
    
    final public com.navdy.hud.app.analytics.RawSpeed copy(float f, long j) {
        return new com.navdy.hud.app.analytics.RawSpeed(f, j);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: {
            label1: {
                if (this == a) {
                    break label1;
                }
                if (!(a instanceof com.navdy.hud.app.analytics.RawSpeed)) {
                    b = false;
                    break label0;
                }
                com.navdy.hud.app.analytics.RawSpeed a0 = (com.navdy.hud.app.analytics.RawSpeed)a;
                if (Float.compare(this.speed, a0.speed) != 0) {
                    b = false;
                    break label0;
                }
                if (!(this.timeStamp == a0.timeStamp)) {
                    b = false;
                    break label0;
                }
            }
            b = true;
        }
        return b;
    }
    
    final public float getSpeed() {
        return this.speed;
    }
    
    final public long getTimeStamp() {
        return this.timeStamp;
    }
    
    public int hashCode() {
        int i = Float.floatToIntBits(this.speed);
        long j = this.timeStamp;
        return i * 31 + (int)(j ^ j >>> 32);
    }
    
    public String toString() {
        return new StringBuilder().append("RawSpeed(speed=").append(this.speed).append(", timeStamp=").append(this.timeStamp).append(")").toString();
    }
}
