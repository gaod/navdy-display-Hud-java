package com.navdy.hud.app.analytics;

final public class TelemetryDataManager$WhenMappings {
    final public static int[] $EnumSwitchMapping$0;
    
    static {
        $EnumSwitchMapping$0 = new int[com.navdy.hud.app.event.InitEvents$Phase.values().length];
        $EnumSwitchMapping$0[com.navdy.hud.app.event.InitEvents$Phase.POST_START.ordinal()] = 1;
    }
}
