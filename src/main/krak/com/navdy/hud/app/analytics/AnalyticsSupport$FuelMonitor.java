package com.navdy.hud.app.analytics;

class AnalyticsSupport$FuelMonitor implements Runnable {
    private AnalyticsSupport$FuelMonitor() {
    }
    
    AnalyticsSupport$FuelMonitor(com.navdy.hud.app.analytics.AnalyticsSupport$1 a) {
        this();
    }
    
    public void run() {
        try {
            int i = com.navdy.hud.app.analytics.AnalyticsSupport.access$3400().getDistanceTravelled();
            int i0 = com.navdy.hud.app.analytics.AnalyticsSupport.access$3400().getFuelLevel();
            double d = com.navdy.hud.app.analytics.AnalyticsSupport.access$3400().getInstantFuelConsumption();
            String s = com.navdy.hud.app.analytics.AnalyticsSupport.access$3400().getVin();
            if (i0 != -1) {
                String[] a = new String[8];
                a[0] = "VIN";
                a[1] = s;
                a[2] = "Fuel_Level";
                a[3] = Integer.toString(i0);
                a[4] = "Fuel_Consumption";
                a[5] = Double.toString(d);
                a[6] = "Distance";
                a[7] = Integer.toString(i);
                com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Fuel_Level", a);
            }
        } catch(Throwable a0) {
            com.navdy.hud.app.analytics.AnalyticsSupport.access$200().postDelayed((Runnable)this, 180000L);
            throw a0;
        }
        com.navdy.hud.app.analytics.AnalyticsSupport.access$200().postDelayed((Runnable)this, 180000L);
    }
}
