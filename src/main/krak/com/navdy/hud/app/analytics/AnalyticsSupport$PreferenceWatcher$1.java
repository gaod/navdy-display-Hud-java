package com.navdy.hud.app.analytics;

class AnalyticsSupport$PreferenceWatcher$1 implements Runnable {
    final com.navdy.hud.app.analytics.AnalyticsSupport$PreferenceWatcher this$0;
    final com.navdy.hud.app.profile.DriverProfile val$profile;
    
    AnalyticsSupport$PreferenceWatcher$1(com.navdy.hud.app.analytics.AnalyticsSupport$PreferenceWatcher a, com.navdy.hud.app.profile.DriverProfile a0) {
        super();
        this.this$0 = a;
        this.val$profile = a0;
    }
    
    public void run() {
        com.navdy.hud.app.analytics.Event a = this.this$0.adapter.calculateState(this.val$profile);
        String s = a.getChangedFields(this.this$0.oldState);
        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
            com.navdy.hud.app.analytics.AnalyticsSupport.access$000().d(new StringBuilder().append("No change in ").append(a.tag).toString());
        } else {
            a.argMap.put("Preferences_Updated", s);
            this.this$0.oldState = a;
            com.navdy.hud.app.analytics.AnalyticsSupport.access$000().d(new StringBuilder().append("Recording preference change: ").append(a).toString());
            com.navdy.hud.app.analytics.AnalyticsSupport.access$2200(a, false);
        }
    }
}
