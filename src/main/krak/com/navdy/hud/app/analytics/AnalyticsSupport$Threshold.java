package com.navdy.hud.app.analytics;

class AnalyticsSupport$Threshold {
    final private int max;
    final private long minimumTime;
    private long triggerTime;
    
    AnalyticsSupport$Threshold(int i, long j) {
        this.triggerTime = -1L;
        this.max = i;
        this.minimumTime = j;
    }
    
    boolean isHit() {
        boolean b = false;
        long j = this.triggerTime;
        int i = (j < -1L) ? -1 : (j == -1L) ? 0 : 1;
        label2: {
            label0: {
                label1: {
                    if (i == 0) {
                        break label1;
                    }
                    if (android.os.SystemClock.elapsedRealtime() - this.triggerTime >= this.minimumTime) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public void update(int i) {
        if (i < this.max) {
            this.triggerTime = -1L;
        } else if (this.triggerTime == -1L) {
            this.triggerTime = android.os.SystemClock.elapsedRealtime();
        }
    }
}
