package com.navdy.hud.app.analytics;

final public class AnalyticsSupport$TemperatureReporter$$InjectAdapter extends dagger.internal.Binding implements javax.inject.Provider, dagger.MembersInjector {
    private dagger.internal.Binding powerManager;
    
    public AnalyticsSupport$TemperatureReporter$$InjectAdapter() {
        super("com.navdy.hud.app.analytics.AnalyticsSupport$TemperatureReporter", "members/com.navdy.hud.app.analytics.AnalyticsSupport$TemperatureReporter", false, com.navdy.hud.app.analytics.AnalyticsSupport$TemperatureReporter.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.powerManager = a.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.analytics.AnalyticsSupport$TemperatureReporter.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.analytics.AnalyticsSupport$TemperatureReporter get() {
        com.navdy.hud.app.analytics.AnalyticsSupport$TemperatureReporter a = new com.navdy.hud.app.analytics.AnalyticsSupport$TemperatureReporter();
        this.injectMembers(a);
        return a;
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.powerManager);
    }
    
    public void injectMembers(com.navdy.hud.app.analytics.AnalyticsSupport$TemperatureReporter a) {
        a.powerManager = (com.navdy.hud.app.device.PowerManager)this.powerManager.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.analytics.AnalyticsSupport$TemperatureReporter)a);
    }
}
