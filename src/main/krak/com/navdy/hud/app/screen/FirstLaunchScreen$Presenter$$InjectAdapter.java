package com.navdy.hud.app.screen;

final public class FirstLaunchScreen$Presenter$$InjectAdapter extends dagger.internal.Binding implements javax.inject.Provider, dagger.MembersInjector {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding powerManager;
    private dagger.internal.Binding supertype;
    private dagger.internal.Binding uiStateManager;
    
    public FirstLaunchScreen$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.screen.FirstLaunchScreen$Presenter", "members/com.navdy.hud.app.screen.FirstLaunchScreen$Presenter", true, com.navdy.hud.app.screen.FirstLaunchScreen$Presenter.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.screen.FirstLaunchScreen$Presenter.class, (this).getClass().getClassLoader());
        this.uiStateManager = a.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.screen.FirstLaunchScreen$Presenter.class, (this).getClass().getClassLoader());
        this.powerManager = a.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.screen.FirstLaunchScreen$Presenter.class, (this).getClass().getClassLoader());
        this.supertype = a.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", com.navdy.hud.app.screen.FirstLaunchScreen$Presenter.class, (this).getClass().getClassLoader(), false, true);
    }
    
    public com.navdy.hud.app.screen.FirstLaunchScreen$Presenter get() {
        com.navdy.hud.app.screen.FirstLaunchScreen$Presenter a = new com.navdy.hud.app.screen.FirstLaunchScreen$Presenter();
        this.injectMembers(a);
        return a;
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.bus);
        a0.add(this.uiStateManager);
        a0.add(this.powerManager);
        a0.add(this.supertype);
    }
    
    public void injectMembers(com.navdy.hud.app.screen.FirstLaunchScreen$Presenter a) {
        a.bus = (com.squareup.otto.Bus)this.bus.get();
        a.uiStateManager = (com.navdy.hud.app.ui.framework.UIStateManager)this.uiStateManager.get();
        a.powerManager = (com.navdy.hud.app.device.PowerManager)this.powerManager.get();
        this.supertype.injectMembers(a);
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.screen.FirstLaunchScreen$Presenter)a);
    }
}
