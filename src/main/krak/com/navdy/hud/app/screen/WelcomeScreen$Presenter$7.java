package com.navdy.hud.app.screen;

class WelcomeScreen$Presenter$7 implements Runnable {
    final com.navdy.hud.app.screen.WelcomeScreen$Presenter this$0;
    final com.navdy.service.library.device.NavdyDeviceId val$deviceId;
    
    WelcomeScreen$Presenter$7(com.navdy.hud.app.screen.WelcomeScreen$Presenter a, com.navdy.service.library.device.NavdyDeviceId a0) {
        super();
        this.this$0 = a;
        this.val$deviceId = a0;
    }
    
    public void run() {
        this.this$0.connectionHandler.connectToDevice(this.val$deviceId);
    }
}
