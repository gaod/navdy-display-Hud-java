package com.navdy.hud.app.screen;

final public class DialManagerScreen$Presenter$$InjectAdapter extends dagger.internal.Binding implements javax.inject.Provider, dagger.MembersInjector {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding gestureServiceConnector;
    private dagger.internal.Binding supertype;
    private dagger.internal.Binding uiStateManager;
    
    public DialManagerScreen$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.screen.DialManagerScreen$Presenter", "members/com.navdy.hud.app.screen.DialManagerScreen$Presenter", true, com.navdy.hud.app.screen.DialManagerScreen$Presenter.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.screen.DialManagerScreen$Presenter.class, (this).getClass().getClassLoader());
        this.uiStateManager = a.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.screen.DialManagerScreen$Presenter.class, (this).getClass().getClassLoader());
        this.gestureServiceConnector = a.requestBinding("com.navdy.hud.app.gesture.GestureServiceConnector", com.navdy.hud.app.screen.DialManagerScreen$Presenter.class, (this).getClass().getClassLoader());
        this.supertype = a.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", com.navdy.hud.app.screen.DialManagerScreen$Presenter.class, (this).getClass().getClassLoader(), false, true);
    }
    
    public com.navdy.hud.app.screen.DialManagerScreen$Presenter get() {
        com.navdy.hud.app.screen.DialManagerScreen$Presenter a = new com.navdy.hud.app.screen.DialManagerScreen$Presenter();
        this.injectMembers(a);
        return a;
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.bus);
        a0.add(this.uiStateManager);
        a0.add(this.gestureServiceConnector);
        a0.add(this.supertype);
    }
    
    public void injectMembers(com.navdy.hud.app.screen.DialManagerScreen$Presenter a) {
        a.bus = (com.squareup.otto.Bus)this.bus.get();
        a.uiStateManager = (com.navdy.hud.app.ui.framework.UIStateManager)this.uiStateManager.get();
        a.gestureServiceConnector = (com.navdy.hud.app.gesture.GestureServiceConnector)this.gestureServiceConnector.get();
        this.supertype.injectMembers(a);
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.screen.DialManagerScreen$Presenter)a);
    }
}
