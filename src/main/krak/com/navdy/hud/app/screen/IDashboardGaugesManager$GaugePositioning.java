package com.navdy.hud.app.screen;


public enum IDashboardGaugesManager$GaugePositioning {
    LEFT(0),
    RIGHT(1);

    private int value;
    IDashboardGaugesManager$GaugePositioning(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class IDashboardGaugesManager$GaugePositioning extends Enum {
//    final private static com.navdy.hud.app.screen.IDashboardGaugesManager$GaugePositioning[] $VALUES;
//    final public static com.navdy.hud.app.screen.IDashboardGaugesManager$GaugePositioning LEFT;
//    final public static com.navdy.hud.app.screen.IDashboardGaugesManager$GaugePositioning RIGHT;
//    
//    static {
//        LEFT = new com.navdy.hud.app.screen.IDashboardGaugesManager$GaugePositioning("LEFT", 0);
//        RIGHT = new com.navdy.hud.app.screen.IDashboardGaugesManager$GaugePositioning("RIGHT", 1);
//        com.navdy.hud.app.screen.IDashboardGaugesManager$GaugePositioning[] a = new com.navdy.hud.app.screen.IDashboardGaugesManager$GaugePositioning[2];
//        a[0] = LEFT;
//        a[1] = RIGHT;
//        $VALUES = a;
//    }
//    
//    private IDashboardGaugesManager$GaugePositioning(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.screen.IDashboardGaugesManager$GaugePositioning valueOf(String s) {
//        return (com.navdy.hud.app.screen.IDashboardGaugesManager$GaugePositioning)Enum.valueOf(com.navdy.hud.app.screen.IDashboardGaugesManager$GaugePositioning.class, s);
//    }
//    
//    public static com.navdy.hud.app.screen.IDashboardGaugesManager$GaugePositioning[] values() {
//        return $VALUES.clone();
//    }
//}
//