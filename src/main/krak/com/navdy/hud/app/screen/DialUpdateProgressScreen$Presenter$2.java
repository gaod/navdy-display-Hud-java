package com.navdy.hud.app.screen;

class DialUpdateProgressScreen$Presenter$2 implements Runnable {
    final com.navdy.hud.app.screen.DialUpdateProgressScreen$Presenter this$0;
    
    DialUpdateProgressScreen$Presenter$2(com.navdy.hud.app.screen.DialUpdateProgressScreen$Presenter a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        android.os.Bundle a = new android.os.Bundle();
        a.putString("OtaDialNameKey", com.navdy.hud.app.device.dial.DialManager.getInstance().getDialFirmwareUpdater().getDialName());
        this.this$0.mBus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING, a, false));
    }
}
