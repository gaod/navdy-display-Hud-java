package com.navdy.hud.app.screen;
import com.navdy.hud.app.R;

abstract public class BaseScreen implements mortar.Blueprint {
    final public static int ANIMATION_NONE = -1;
    final private static com.navdy.service.library.log.Logger sLogger;
    protected android.os.Bundle arguments;
    protected Object arguments2;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.BaseScreen.class);
    }
    
    public BaseScreen() {
    }
    
    private android.os.Bundle getInstanceState(mortar.MortarScope a) {
        android.os.Bundle a0 = null;
        try {
            java.lang.reflect.Field a1 = (a).getClass().getDeclaredField("latestSavedInstanceState");
            a1.setAccessible(true);
            a0 = (android.os.Bundle)a1.get(a);
        } catch(Throwable a2) {
            sLogger.e("failed to get screen instance state", a2);
            a0 = null;
        }
        return a0;
    }
    
    private void setInstanceState(mortar.MortarScope a, android.os.Bundle a0) {
        try {
            java.lang.reflect.Field a1 = (a).getClass().getDeclaredField("latestSavedInstanceState");
            a1.setAccessible(true);
            a1.set(a, a0);
        } catch(Throwable a2) {
            sLogger.e("failed to set screen instance state", a2);
        }
    }
    
    private void updateInstanceState() {
        mortar.MortarScope a = mortar.Mortar.getScope(com.navdy.hud.app.HudApplication.getAppContext()).findChild(com.navdy.hud.app.ui.activity.Main.class.getName());
        if (a != null) {
            mortar.MortarScope a0 = a.requireChild((mortar.Blueprint)this);
            android.os.Bundle a1 = this.getInstanceState(a0);
            if (a1 == null) {
                a1 = new android.os.Bundle();
                this.setInstanceState(a0, a1);
            }
            String s = this.getMortarScopeName();
            a1.putBundle(new StringBuilder().append(s).append("$Presenter").toString(), this.arguments);
        }
    }
    
    public int getAnimationIn(flow.Flow$Direction a) {
        return (a != flow.Flow$Direction.FORWARD) ? R.anim.slide_in_up : R.anim.slide_in_down;
    }
    
    public int getAnimationOut(flow.Flow$Direction a) {
        return (a != flow.Flow$Direction.FORWARD) ? R.anim.slide_out_up : R.anim.slide_out_down;
    }
    
    public android.os.Bundle getArguments() {
        return this.arguments;
    }
    
    public Object getArguments2() {
        return this.arguments2;
    }
    
    abstract public com.navdy.service.library.events.ui.Screen getScreen();
    
    
    public void onAnimationInEnd() {
    }
    
    public void onAnimationInStart() {
    }
    
    public void onAnimationOutEnd() {
    }
    
    public void onAnimationOutStart() {
    }
    
    public void setArguments(android.os.Bundle a, Object a0) {
        this.arguments = a;
        this.arguments2 = a0;
        this.updateInstanceState();
    }
}
