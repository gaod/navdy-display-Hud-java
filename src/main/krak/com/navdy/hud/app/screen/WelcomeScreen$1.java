package com.navdy.hud.app.screen;

class WelcomeScreen$1 {
    final static int[] $SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State;
    final static int[] $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState;
    final static int[] $SwitchMap$com$navdy$service$library$events$connection$ConnectionStatus$Status;
    
    static {
        $SwitchMap$com$navdy$service$library$events$connection$ConnectionStatus$Status = new int[com.navdy.service.library.events.connection.ConnectionStatus$Status.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$connection$ConnectionStatus$Status;
        com.navdy.service.library.events.connection.ConnectionStatus$Status a0 = com.navdy.service.library.events.connection.ConnectionStatus$Status.CONNECTION_SEARCH_STARTED;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$connection$ConnectionStatus$Status[com.navdy.service.library.events.connection.ConnectionStatus$Status.CONNECTION_SEARCH_FINISHED.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$connection$ConnectionStatus$Status[com.navdy.service.library.events.connection.ConnectionStatus$Status.CONNECTION_FOUND.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$connection$ConnectionStatus$Status[com.navdy.service.library.events.connection.ConnectionStatus$Status.CONNECTION_LOST.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$connection$ConnectionStatus$Status[com.navdy.service.library.events.connection.ConnectionStatus$Status.CONNECTION_PAIRED_DEVICES_CHANGED.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState = new int[com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.values().length];
        int[] a1 = $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState;
        com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState a2 = com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_LINK_LOST;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_CONNECTED.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_VERIFIED.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_DISCONNECTED.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_LINK_ESTABLISHED.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException8) {
        }
        $SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State = new int[com.navdy.hud.app.screen.WelcomeScreen$State.values().length];
        int[] a3 = $SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State;
        com.navdy.hud.app.screen.WelcomeScreen$State a4 = com.navdy.hud.app.screen.WelcomeScreen$State.WELCOME;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException9) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State[com.navdy.hud.app.screen.WelcomeScreen$State.CONNECTING.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException10) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State[com.navdy.hud.app.screen.WelcomeScreen$State.LAUNCHING_APP.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException11) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State[com.navdy.hud.app.screen.WelcomeScreen$State.CONNECTION_FAILED.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException12) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State[com.navdy.hud.app.screen.WelcomeScreen$State.SEARCHING.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException13) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State[com.navdy.hud.app.screen.WelcomeScreen$State.DOWNLOAD_APP.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException14) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State[com.navdy.hud.app.screen.WelcomeScreen$State.PICKING.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException15) {
        }
    }
}
