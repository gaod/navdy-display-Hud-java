package com.navdy.hud.app.screen;
import com.navdy.hud.app.R;

public class AutoBrightnessScreen$Presenter extends com.navdy.hud.app.ui.framework.BasePresenter {
    final private static int POS_ACTION = 0;
    private com.navdy.hud.app.ui.component.ChoiceLayout$IListener autoBrightnessOffListener;
    private com.navdy.hud.app.ui.component.ChoiceLayout$IListener autoBrightnessOnListener;
    @Inject
    com.squareup.otto.Bus bus;
    private android.content.res.Resources resources;
    @Inject
    android.content.SharedPreferences sharedPreferences;
    private com.navdy.hud.app.view.AutoBrightnessView view;
    
    public AutoBrightnessScreen$Presenter() {
        this.autoBrightnessOnListener = (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)new com.navdy.hud.app.screen.AutoBrightnessScreen$Presenter$1(this);
        this.autoBrightnessOffListener = (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)new com.navdy.hud.app.screen.AutoBrightnessScreen$Presenter$2(this);
    }
    
    static void access$000(com.navdy.hud.app.screen.AutoBrightnessScreen$Presenter a, boolean b) {
        a.setAutoBrightness(b);
    }
    
    private java.util.List getChoicesOff() {
        java.util.ArrayList a = new java.util.ArrayList();
        String s = this.resources.getString(R.string.auto_brightness_choice_enable);
        String s0 = this.resources.getString(R.string.auto_brightness_choice_cancel);
        ((java.util.List)a).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(s, 0));
        ((java.util.List)a).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(s0, 0));
        return (java.util.List)a;
    }
    
    private java.util.List getChoicesOn() {
        java.util.ArrayList a = new java.util.ArrayList();
        String s = this.resources.getString(R.string.auto_brightness_choice_disable);
        String s0 = this.resources.getString(R.string.auto_brightness_choice_cancel);
        ((java.util.List)a).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(s, 0));
        ((java.util.List)a).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(s0, 0));
        return (java.util.List)a;
    }
    
    private void init(com.navdy.hud.app.view.AutoBrightnessView a) {
        if ("true".equals(this.sharedPreferences.getString("screen.auto_brightness", ""))) {
            java.util.List a0 = this.getChoicesOn();
            a.setTitle(this.resources.getString(R.string.auto_brightness_on_title));
            a.setStatusLabel(this.resources.getString(R.string.auto_brightness_on_status_label));
            a.setIcon(this.resources.getDrawable(R.drawable.icon_mm_brightness_copy));
            a.setChoices(a0, this.autoBrightnessOnListener);
        } else {
            java.util.List a1 = this.getChoicesOff();
            a.setTitle(this.resources.getString(R.string.auto_brightness_off_title));
            a.setStatusLabel(this.resources.getString(R.string.auto_brightness_off_status_label));
            a.setIcon(this.resources.getDrawable(R.drawable.icon_mm_brightness_disabled));
            a.setChoices(a1, this.autoBrightnessOffListener);
        }
        this.view = a;
    }
    
    private void setAutoBrightness(boolean b) {
        android.content.SharedPreferences$Editor a = this.sharedPreferences.edit();
        a.putString("screen.auto_brightness", b ? "true" : "false");
        a.apply();
        com.navdy.hud.app.util.os.SystemProperties.set("persist.sys.autobrightness", b ? "enabled" : "disabled");
    }
    
    public boolean handleKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        switch(com.navdy.hud.app.screen.AutoBrightnessScreen$1.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
            case 3: {
                this.view.executeSelectedItem();
                break;
            }
            case 2: {
                this.view.moveSelectionRight();
                break;
            }
            case 1: {
                this.view.moveSelectionLeft();
                break;
            }
        }
        return true;
    }
    
    public void onLoad(android.os.Bundle a) {
        this.resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        this.bus.register(this);
        this.updateView();
        super.onLoad(a);
    }
    
    protected void updateView() {
        com.navdy.hud.app.view.AutoBrightnessView a = (com.navdy.hud.app.view.AutoBrightnessView)this.getView();
        if (a != null) {
            this.init(a);
        }
    }
}
