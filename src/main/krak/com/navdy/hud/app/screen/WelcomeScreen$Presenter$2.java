package com.navdy.hud.app.screen;

class WelcomeScreen$Presenter$2 implements Runnable {
    final com.navdy.hud.app.screen.WelcomeScreen$Presenter this$0;
    
    WelcomeScreen$Presenter$2(com.navdy.hud.app.screen.WelcomeScreen$Presenter a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.screen.WelcomeScreen.access$000().v(new StringBuilder().append("appLaunch: ").append(com.navdy.hud.app.screen.WelcomeScreen$Presenter.access$300(this.this$0)).toString());
        if (!com.navdy.hud.app.screen.WelcomeScreen$Presenter.access$300(this.this$0) && com.navdy.hud.app.ui.activity.Main.mProtocolStatus == com.navdy.hud.app.ui.activity.Main$ProtocolStatus.PROTOCOL_VALID) {
            com.navdy.hud.app.screen.WelcomeScreen.access$000().v("showing app dis-connected toast");
            com.navdy.hud.app.framework.connection.ConnectionNotification.showDisconnectedToast(true);
            com.navdy.hud.app.screen.WelcomeScreen$Presenter.access$402(this.this$0, true);
        }
    }
}
