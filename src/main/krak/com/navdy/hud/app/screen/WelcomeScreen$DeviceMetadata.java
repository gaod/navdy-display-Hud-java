package com.navdy.hud.app.screen;

public class WelcomeScreen$DeviceMetadata {
    final public com.navdy.service.library.device.NavdyDeviceId deviceId;
    final public com.navdy.hud.app.profile.DriverProfile driverProfile;
    
    public WelcomeScreen$DeviceMetadata(com.navdy.service.library.device.NavdyDeviceId a, com.navdy.hud.app.profile.DriverProfile a0) {
        this.deviceId = a;
        this.driverProfile = a0;
    }
    
    public String toString() {
        return new StringBuilder().append("DeviceMetadata{deviceId=").append(this.deviceId).append(", driverProfile=").append(this.driverProfile).append((char)125).toString();
    }
}
