package com.navdy.hud.app.gesture;

abstract public interface FlickTurnGestureDetector$IFlickTurnKeyGesture extends com.navdy.hud.app.gesture.MultipleClickGestureDetector$IMultipleClickKeyGesture {
    abstract public void onFlickLeft();
    
    
    abstract public void onFlickRight();
}
