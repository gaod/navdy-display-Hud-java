package com.navdy.hud.app.gesture;

public class GestureServiceConnector$GestureProgress {
    public com.navdy.hud.app.gesture.GestureServiceConnector$GestureDirection direction;
    public float progress;
    
    public GestureServiceConnector$GestureProgress(com.navdy.hud.app.gesture.GestureServiceConnector$GestureDirection a, float f) {
        this.direction = a;
        this.progress = f;
    }
}
