package com.navdy.hud.app.obd;

class ObdDeviceConfigurationManager$2 implements Runnable {
    final com.navdy.hud.app.obd.ObdDeviceConfigurationManager this$0;
    final String val$make;
    final String val$model;
    final String val$year;
    
    ObdDeviceConfigurationManager$2(com.navdy.hud.app.obd.ObdDeviceConfigurationManager a, String s, String s0, String s1) {
        super();
        this.this$0 = a;
        this.val$make = s;
        this.val$model = s0;
        this.val$year = s1;
    }
    
    public void run() {
        if (com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$100(this.this$0)) {
            if (com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$300(this.this$0)) {
                com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$200().d("setCarDetails: Vin is recognized and it is used as the preferred source");
            } else {
                String s = new StringBuilder().append((android.text.TextUtils.isEmpty((CharSequence)this.val$make)) ? "*" : this.val$make).append("_").append((android.text.TextUtils.isEmpty((CharSequence)this.val$model)) ? "*" : this.val$model).append("_").append((android.text.TextUtils.isEmpty((CharSequence)this.val$year)) ? "*" : this.val$year).toString();
                com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$200().d(new StringBuilder().append("Vehicle name :").append(s).toString());
                if (com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$400() == null) {
                    com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$402(com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$500(this.this$0, "configuration_mapping.csv"));
                }
                com.navdy.hud.app.obd.ObdDeviceConfigurationManager$Configuration a = com.navdy.hud.app.obd.ObdDeviceConfigurationManager.pickConfiguration(com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$400(), s);
                if (a != null) {
                    com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$000(this.this$0, a.configurationName);
                }
            }
        } else {
            com.navdy.hud.app.obd.ObdDeviceConfigurationManager.access$200().d("setCarDetails: Auto on is de selected by the user , so skipping the overwriting of configuration");
        }
    }
}
