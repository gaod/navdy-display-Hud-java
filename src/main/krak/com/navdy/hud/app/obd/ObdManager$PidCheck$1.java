package com.navdy.hud.app.obd;

class ObdManager$PidCheck$1 implements Runnable {
    final com.navdy.hud.app.obd.ObdManager$PidCheck this$1;
    final int val$pid;
    final com.navdy.hud.app.obd.ObdManager val$this$0;
    
    ObdManager$PidCheck$1(com.navdy.hud.app.obd.ObdManager$PidCheck a, com.navdy.hud.app.obd.ObdManager a0, int i) {
        super();
        this.this$1 = a;
        this.val$this$0 = a0;
        this.val$pid = i;
    }
    
    public void run() {
        if (!this.this$1.hasIncorrectData) {
            this.this$1.waitingForValidData = false;
            this.this$1.hasIncorrectData = true;
            this.this$1.invalidatePid(this.val$pid);
        }
    }
}
