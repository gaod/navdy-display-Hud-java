package com.navdy.hud.app.obd;

public class ObdDeviceConfigurationManager {
    final public static String ASSETS_FOLDER_PREFIX = "stn_obd";
    final public static String CAR_CONFIGURATION_MAPPING_FILE = "configuration_mapping.csv";
    private static com.navdy.hud.app.obd.ObdDeviceConfigurationManager$Configuration[] CAR_DETAILS_CONFIGURATION_MAPPING;
    final public static String CONFIGURATION_FILE_EXTENSION = "conf";
    final public static String CONFIGURATION_NAME_PREFIX = "Navdy OBD-II ";
    final public static String DEBUG_CONFIGURATION = "debug";
    final public static String DEFAULT_OFF_CONFIGURATION = "no_trigger";
    final public static String DEFAULT_ON_CONFIGURATION = "default_on";
    final public static String STSATI = "STSATI";
    private static com.navdy.hud.app.obd.ObdDeviceConfigurationManager$Configuration[] VIN_CONFIGURATION_MAPPING;
    final public static String VIN_CONFIGURATION_MAPPING_FILE = "vin_configuration_mapping.csv";
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.navdy.hud.app.obd.CarServiceConnector mCarServiceConnector;
    private volatile boolean mConnected;
    private String mExpectedConfigurationFileName;
    private volatile boolean mIsAutoOnEnabled;
    private volatile boolean mIsVinRecognized;
    private volatile String mLastKnowVinNumber;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.obd.ObdDeviceConfigurationManager.class);
    }
    
    public ObdDeviceConfigurationManager(com.navdy.hud.app.obd.CarServiceConnector a) {
        this.mIsAutoOnEnabled = true;
        this.mExpectedConfigurationFileName = null;
        this.mCarServiceConnector = a;
    }
    
    static void access$000(com.navdy.hud.app.obd.ObdDeviceConfigurationManager a, String s) {
        a.bSetConfigurationFile(s);
    }
    
    static boolean access$100(com.navdy.hud.app.obd.ObdDeviceConfigurationManager a) {
        return a.mIsAutoOnEnabled;
    }
    
    static com.navdy.service.library.log.Logger access$200() {
        return sLogger;
    }
    
    static boolean access$300(com.navdy.hud.app.obd.ObdDeviceConfigurationManager a) {
        return a.mIsVinRecognized;
    }
    
    static boolean access$302(com.navdy.hud.app.obd.ObdDeviceConfigurationManager a, boolean b) {
        a.mIsVinRecognized = b;
        return b;
    }
    
    static com.navdy.hud.app.obd.ObdDeviceConfigurationManager$Configuration[] access$400() {
        return CAR_DETAILS_CONFIGURATION_MAPPING;
    }
    
    static com.navdy.hud.app.obd.ObdDeviceConfigurationManager$Configuration[] access$402(com.navdy.hud.app.obd.ObdDeviceConfigurationManager$Configuration[] a) {
        CAR_DETAILS_CONFIGURATION_MAPPING = a;
        return a;
    }
    
    static com.navdy.hud.app.obd.ObdDeviceConfigurationManager$Configuration[] access$500(com.navdy.hud.app.obd.ObdDeviceConfigurationManager a, String s) {
        return a.loadConfigurationMappingList(s);
    }
    
    static String access$600(com.navdy.hud.app.obd.ObdDeviceConfigurationManager a) {
        return a.mLastKnowVinNumber;
    }
    
    static com.navdy.hud.app.obd.ObdDeviceConfigurationManager$Configuration[] access$700() {
        return VIN_CONFIGURATION_MAPPING;
    }
    
    static com.navdy.hud.app.obd.ObdDeviceConfigurationManager$Configuration[] access$702(com.navdy.hud.app.obd.ObdDeviceConfigurationManager$Configuration[] a) {
        VIN_CONFIGURATION_MAPPING = a;
        return a;
    }
    
    private void applyConfiguration(com.navdy.hud.app.obd.ObdDeviceConfigurationManager$OBDConfiguration a) {
        label0: {
            android.os.RemoteException a0 = null;
            if (a == null) {
                break label0;
            }
            if (!this.mConnected) {
                break label0;
            }
            com.navdy.obd.ICarService a1 = this.mCarServiceConnector.getCarApi();
            if (android.text.TextUtils.isEmpty((CharSequence)a.configurationData)) {
                break label0;
            }
            if (android.text.TextUtils.isEmpty((CharSequence)a.configurationIdentifier)) {
                break label0;
            }
            sLogger.d(new StringBuilder().append("Writing new configuration :").append(a).toString());
            try {
                boolean b = a1.applyConfiguration(a.configurationData);
                String s = a1.getCurrentConfigurationName();
                sLogger.d(new StringBuilder().append("New Configuration applied : ").append(b).append(", configuration : ").append(s).toString());
                break label0;
            } catch(android.os.RemoteException a2) {
                a0 = a2;
            }
            sLogger.e("Failed to apply the configuration :", a0.getCause());
        }
    }
    
    private void bSetConfigurationFile(String s) {
        if (!this.validateConfigurationName(s)) {
            sLogger.d(new StringBuilder().append("Received an invalid configuration name : ").append(s).toString());
            throw new IllegalArgumentException("Configuration does not exists");
        }
        this.mExpectedConfigurationFileName = s;
        this.syncConfiguration();
    }
    
    private void decodeVin() {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.obd.ObdDeviceConfigurationManager$3(this), 13);
    }
    
    static com.navdy.hud.app.obd.ObdDeviceConfigurationManager$OBDConfiguration extractOBDConfiguration(String s) {
        StringBuilder a = new StringBuilder();
        String[] a0 = s.split("\n");
        int i = a0.length;
        String s0 = null;
        int i0 = 0;
        while(i0 < i) {
            String s1 = a0[i0];
            boolean b = s1.startsWith("ST");
            label0: {
                label1: {
                    if (b) {
                        break label1;
                    }
                    if (!s1.startsWith("AT")) {
                        break label0;
                    }
                }
                a.append(s1).append("\n");
                if (s1.startsWith("STSATI")) {
                    s0 = s1.substring("STSATI".length()).trim();
                }
            }
            i0 = i0 + 1;
        }
        return new com.navdy.hud.app.obd.ObdDeviceConfigurationManager$OBDConfiguration(a.toString().trim(), s0);
    }
    
    public static boolean isValidVin(String s) {
        boolean b = false;
        boolean b0 = android.text.TextUtils.isEmpty((CharSequence)s);
        label2: {
            label0: {
                label1: {
                    if (b0) {
                        break label1;
                    }
                    if (s.length() == 17) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private com.navdy.hud.app.obd.ObdDeviceConfigurationManager$Configuration[] loadConfigurationMappingList(String s) {
        com.navdy.hud.app.obd.ObdDeviceConfigurationManager$Configuration[] a = null;
        try {
            a = null;
            String[] a0 = this.readObdAssetFile(s).split("\n");
            com.navdy.hud.app.obd.ObdDeviceConfigurationManager$Configuration[] a1 = new com.navdy.hud.app.obd.ObdDeviceConfigurationManager$Configuration[a0.length];
            int i = a0.length;
            int i0 = 0;
            int i1 = 0;
            while(i0 < i) {
                a = a1;
                String[] a2 = a0[i0].split(",");
                com.navdy.hud.app.obd.ObdDeviceConfigurationManager$Configuration a3 = new com.navdy.hud.app.obd.ObdDeviceConfigurationManager$Configuration();
                a3.pattern = java.util.regex.Pattern.compile(a2[0], 2);
                a3.configurationName = a2[1];
                a1[i1] = a3;
                i0 = i0 + 1;
                i1 = i1 + 1;
            }
            a = a1;
        } catch(java.io.IOException ignoredException) {
            sLogger.d("Failed to load the configuration mapping file");
        }
        return a;
    }
    
    public static com.navdy.hud.app.obd.ObdDeviceConfigurationManager$Configuration pickConfiguration(com.navdy.hud.app.obd.ObdDeviceConfigurationManager$Configuration[] a, String s) {
        int i = a.length;
        int i0 = 0;
        while(true) {
            com.navdy.hud.app.obd.ObdDeviceConfigurationManager$Configuration a0 = null;
            if (i0 < i) {
                a0 = a[i0];
                if (!a0.pattern.matcher((CharSequence)s).matches()) {
                    i0 = i0 + 1;
                    continue;
                }
            }
            return a0;
        }
    }
    
    private com.navdy.hud.app.obd.ObdDeviceConfigurationManager$OBDConfiguration readConfiguration(String s) {
        com.navdy.hud.app.obd.ObdDeviceConfigurationManager$OBDConfiguration a = null;
        try {
            a = com.navdy.hud.app.obd.ObdDeviceConfigurationManager.extractOBDConfiguration(this.readObdAssetFile(new StringBuilder().append(s).append(".").append("conf").toString()));
        } catch(java.io.IOException ignoredException) {
            sLogger.e(new StringBuilder().append("Error reading the configuration file :").append(s).toString());
            a = null;
        }
        return a;
    }
    
    private String readObdAssetFile(String s) {
        java.io.InputStream a = com.navdy.hud.app.HudApplication.getAppContext().getAssets().open(new StringBuilder().append("stn_obd/").append(s).toString());
        String s0 = com.navdy.service.library.util.IOUtils.convertInputStreamToString(a, "UTF-8");
        try {
            a.close();
        } catch(java.io.IOException ignoredException) {
            sLogger.d("Error closing the stream after reading the asset file");
        }
        return s0;
    }
    
    private void syncConfiguration() {
        boolean b = this.mConnected;
        label0: {
            android.os.RemoteException a = null;
            if (!b) {
                break label0;
            }
            if (android.text.TextUtils.isEmpty((CharSequence)this.mExpectedConfigurationFileName)) {
                break label0;
            }
            com.navdy.obd.ICarService a0 = this.mCarServiceConnector.getCarApi();
            com.navdy.hud.app.obd.ObdDeviceConfigurationManager$OBDConfiguration a1 = this.readConfiguration(this.mExpectedConfigurationFileName);
            try {
                String s = a0.getCurrentConfigurationName();
                String s0 = a1.configurationIdentifier;
                if (s0.equals(s)) {
                    sLogger.d("The configuration on the obd chip is as expected");
                    break label0;
                } else {
                    sLogger.d("The configuration on the obd chip is different");
                    sLogger.d(new StringBuilder().append("Expected : ").append(s0).toString());
                    sLogger.d(new StringBuilder().append("Actual : ").append(s).toString());
                    sLogger.d("Syncing configuration...");
                    this.applyConfiguration(a1);
                    break label0;
                }
            } catch(android.os.RemoteException a2) {
                a = a2;
            }
            sLogger.e("Error while applying configuration", (Throwable)a);
        }
    }
    
    public static void turnOffOBDSleep() {
        com.navdy.hud.app.obd.ObdManager.getInstance().getObdDeviceConfigurationManager().setConfigurationFile("debug");
    }
    
    private boolean validateConfigurationName(String s) {
        boolean b = false;
        boolean b0 = android.text.TextUtils.isEmpty((CharSequence)s);
        label0: {
            label1: if (b0) {
                b = false;
                break label0;
            } else {
                java.io.InputStream a = null;
                try {
                    a = com.navdy.hud.app.HudApplication.getAppContext().getAssets().open(new StringBuilder().append("stn_obd/").append(s).append(".").append("conf").toString());
                } catch(java.io.IOException ignoredException) {
                    break label1;
                }
                if (a == null) {
                    b = false;
                    break label0;
                } else {
                    try {
                        a.close();
                    } catch(java.io.IOException ignoredException0) {
                    }
                    b = true;
                    break label0;
                }
            }
            b = false;
        }
        return b;
    }
    
    public boolean isAutoOnEnabled() {
        return this.mIsAutoOnEnabled;
    }
    
    public void setAutoOnEnabled(boolean b) {
        this.mIsAutoOnEnabled = b;
        if (!this.mIsAutoOnEnabled) {
            this.setConfigurationFile("no_trigger");
        }
    }
    
    public void setCarDetails(String s, String s0, String s1) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.obd.ObdDeviceConfigurationManager$2(this, s, s0, s1), 13);
    }
    
    public void setConfigurationFile(String s) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.obd.ObdDeviceConfigurationManager$1(this, s), 13);
    }
    
    public void setConnectionState(boolean b) {
        boolean b0 = this.mConnected;
        label0: {
            if (b == b0) {
                this.mLastKnowVinNumber = "";
                this.mIsAutoOnEnabled = true;
                this.mIsVinRecognized = false;
                break label0;
            } else {
                this.mConnected = b;
                sLogger.d("OBD Connection state changed");
                if (!this.mConnected) {
                    break label0;
                }
                com.navdy.obd.ICarService a = this.mCarServiceConnector.getCarApi();
                try {
                    this.mLastKnowVinNumber = a.getVIN();
                    sLogger.d(new StringBuilder().append("Vin number read :").append(this.mLastKnowVinNumber).toString());
                    this.decodeVin();
                    break label0;
                } catch(android.os.RemoteException ignoredException) {
                }
            }
            sLogger.e("Remote exception while getting the VIN number ");
        }
    }
    
    public void setDecodedCarDetails(com.navdy.hud.app.obd.CarDetails a) {
    }
}
