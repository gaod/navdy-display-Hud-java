package com.navdy.hud.app.obd;

final public class CarMDVinDecoder$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding mHttpManager;
    
    public CarMDVinDecoder$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.obd.CarMDVinDecoder", false, com.navdy.hud.app.obd.CarMDVinDecoder.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.mHttpManager = a.requestBinding("com.navdy.service.library.network.http.IHttpManager", com.navdy.hud.app.obd.CarMDVinDecoder.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.mHttpManager);
    }
    
    public void injectMembers(com.navdy.hud.app.obd.CarMDVinDecoder a) {
        a.mHttpManager = (com.navdy.service.library.network.http.IHttpManager)this.mHttpManager.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.obd.CarMDVinDecoder)a);
    }
}
