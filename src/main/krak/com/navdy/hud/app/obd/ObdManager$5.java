package com.navdy.hud.app.obd;

class ObdManager$5 implements Runnable {
    final com.navdy.hud.app.obd.ObdManager this$0;
    
    ObdManager$5(com.navdy.hud.app.obd.ObdManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        double d = com.navdy.hud.app.obd.ObdManager.access$400(this.this$0);
        com.navdy.hud.app.obd.ObdManager.access$402(this.this$0, this.this$0.getBatteryVoltage());
        if (com.navdy.hud.app.obd.ObdManager.access$400(this.this$0) != -1.0 && d == -1.0) {
            com.navdy.hud.app.obd.ObdManager.access$502(this.this$0, com.navdy.hud.app.obd.ObdManager.access$400(this.this$0));
            com.navdy.hud.app.analytics.AnalyticsSupport.recordStartingBatteryVoltage(com.navdy.hud.app.obd.ObdManager.access$400(this.this$0));
        }
        double d0 = com.navdy.hud.app.obd.ObdManager.access$400(this.this$0);
        int i = (d0 > 0.0) ? 1 : (d0 == 0.0) ? 0 : -1;
        label0: {
            Exception a = null;
            label3: {
                label4: {
                    if (i == 0) {
                        break label4;
                    }
                    if (com.navdy.hud.app.obd.ObdManager.access$400(this.this$0) != -1.0) {
                        break label3;
                    }
                }
                if (com.navdy.hud.app.obd.ObdManager.access$600(this.this$0) != 0) {
                    com.navdy.hud.app.obd.ObdManager.access$700().d("Battery level data is not available");
                }
                com.navdy.hud.app.obd.ObdManager.access$602(this.this$0, 0);
                com.navdy.hud.app.obd.ObdManager.access$802(this.this$0, 0);
                break label0;
            }
            if (com.navdy.hud.app.obd.ObdManager.access$400(this.this$0) < com.navdy.hud.app.obd.ObdManager.access$500(this.this$0)) {
                com.navdy.hud.app.obd.ObdManager.access$502(this.this$0, com.navdy.hud.app.obd.ObdManager.access$400(this.this$0));
            }
            if (com.navdy.hud.app.obd.ObdManager.access$400(this.this$0) > com.navdy.hud.app.obd.ObdManager.access$900(this.this$0)) {
                com.navdy.hud.app.obd.ObdManager.access$902(this.this$0, com.navdy.hud.app.obd.ObdManager.access$400(this.this$0));
            }
            double d1 = (this.this$0.powerManager.inQuietMode()) ? com.navdy.hud.app.obd.ObdManager.LOW_BATTERY_VOLTAGE : com.navdy.hud.app.obd.ObdManager.CRITICAL_BATTERY_VOLTAGE;
            double d2 = com.navdy.hud.app.obd.ObdManager.access$400(this.this$0);
            int i0 = (d2 > 13.100000381469727) ? 1 : (d2 == 13.100000381469727) ? 0 : -1;
            label2: {
                if (i0 < 0) {
                    break label2;
                }
                if (!this.this$0.powerManager.inQuietMode()) {
                    break label2;
                }
                com.navdy.hud.app.obd.ObdManager.access$700().d("Battery seems to be charging, waking up");
                this.this$0.powerManager.wakeUp(com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason.VOLTAGE_SPIKE);
                com.navdy.hud.app.obd.ObdManager.access$802(this.this$0, 0);
                break label0;
            }
            label1: if (com.navdy.hud.app.obd.ObdManager.access$400(this.this$0) < d1) {
                com.navdy.hud.app.obd.ObdManager.access$802(this.this$0, 0);
                com.navdy.hud.app.obd.ObdManager.access$700().d(new StringBuilder().append("Battery voltage (LOW) : ").append(com.navdy.hud.app.obd.ObdManager.access$400(this.this$0)).toString());
                com.navdy.hud.app.obd.ObdManager.access$608(this.this$0);
                if (com.navdy.hud.app.obd.ObdManager.access$600(this.this$0) <= com.navdy.hud.app.obd.ObdManager.access$1000()) {
                    break label0;
                }
                com.navdy.hud.app.obd.ObdManager.access$700().d("Battery Voltage is too low, shutting down");
                {
                    try {
                        com.navdy.hud.app.event.Shutdown$Reason a0 = (this.this$0.powerManager.inQuietMode()) ? com.navdy.hud.app.event.Shutdown$Reason.LOW_VOLTAGE : com.navdy.hud.app.event.Shutdown$Reason.CRITICAL_VOLTAGE;
                        this.this$0.sleep(true);
                        this.this$0.bus.post(new com.navdy.hud.app.event.Shutdown(a0));
                    } catch(Exception a1) {
                        a = a1;
                        break label1;
                    }
                    break label0;
                }
            } else {
                com.navdy.hud.app.obd.ObdManager.access$602(this.this$0, 0);
                if (!this.this$0.powerManager.inQuietMode()) {
                    break label0;
                }
                com.navdy.hud.app.obd.ObdManager.access$700().d(new StringBuilder().append("In Quiet mode , Battery voltage : ").append(com.navdy.hud.app.obd.ObdManager.access$400(this.this$0)).toString());
                com.navdy.hud.app.obd.ObdManager.access$808(this.this$0);
                if (com.navdy.hud.app.obd.ObdManager.access$800(this.this$0) < 5) {
                    break label0;
                }
                com.navdy.hud.app.obd.ObdManager.access$700().d("Safe to put obd chip to sleep, invoking sleep");
                com.navdy.hud.app.obd.ObdManager.access$802(this.this$0, 0);
                com.navdy.hud.app.obd.ObdManager.access$200(this.this$0).removeCallbacks(com.navdy.hud.app.obd.ObdManager.access$1100(this.this$0));
                this.this$0.sleep(false);
                break label0;
            }
            com.navdy.hud.app.obd.ObdManager.access$700().e(new StringBuilder().append("error invoking ShutdownMonitor.androidShutdown(): ").append(a).toString());
        }
    }
}
