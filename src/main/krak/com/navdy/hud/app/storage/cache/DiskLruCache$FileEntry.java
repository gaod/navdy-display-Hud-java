package com.navdy.hud.app.storage.cache;

class DiskLruCache$FileEntry {
    String name;
    boolean removed;
    int size;
    
    DiskLruCache$FileEntry(String s, int i) {
        this.name = s;
        this.size = i;
    }
}
