package com.navdy.hud.app.storage.cache;

final public class MessageCache implements com.navdy.hud.app.storage.cache.Cache {
    final private com.navdy.hud.app.storage.cache.Cache dataCache;
    final private Class type;
    final private com.squareup.wire.Wire wire;
    
    public MessageCache(com.navdy.hud.app.storage.cache.Cache a, Class a0) {
        super();
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "dataCache");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "type");
        this.dataCache = a;
        this.type = a0;
        Class[] a1 = new Class[1];
        a1[0] = com.navdy.service.library.events.Ext_NavdyEvent.class;
        this.wire = new com.squareup.wire.Wire(a1);
    }
    
    public void clear() {
        this.dataCache.clear();
    }
    
    public boolean contains(Object a) {
        return this.contains((String)a);
    }
    
    public boolean contains(String s) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s, "key");
        return this.dataCache.contains(s);
    }
    
    public com.squareup.wire.Message get(String s) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s, "key");
        byte[] a = (byte[])this.dataCache.get(s);
        com.squareup.wire.Message a0 = null;
        if (a != null) {
            a0 = com.navdy.service.library.events.MessageStore.removeNulls(this.wire.parseFrom(a, this.type));
        }
        return a0;
    }
    
    public Object get(Object a) {
        return this.get((String)a);
    }
    
    final public com.navdy.hud.app.storage.cache.Cache getDataCache() {
        return this.dataCache;
    }
    
    final public Class getType() {
        return this.type;
    }
    
    public void put(Object a, Object a0) {
        this.put((String)a, (com.squareup.wire.Message)a0);
    }
    
    public void put(String s, com.squareup.wire.Message a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s, "key");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "message");
        byte[] a0 = a.toByteArray();
        com.navdy.hud.app.storage.cache.Cache a1 = this.dataCache;
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a0, "bytes");
        a1.put(s, a0);
    }
    
    public void remove(Object a) {
        this.remove((String)a);
    }
    
    public void remove(String s) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s, "key");
        this.dataCache.remove(s);
    }
}
