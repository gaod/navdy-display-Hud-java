package com.navdy.hud.app.framework.places;
import com.navdy.hud.app.R;

class NearbyPlaceSearchNotification$2 implements com.navdy.hud.app.ui.component.destination.IDestinationPicker {
    private boolean itemSelected;
    private boolean retrySelected;
    final com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification this$0;
    
    NearbyPlaceSearchNotification$2(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification a) {
        super();
        this.this$0 = a;
        this.itemSelected = false;
        this.retrySelected = false;
    }
    
    public void onDestinationPickerClosed() {
        if (!this.itemSelected && !this.retrySelected) {
            com.navdy.hud.app.analytics.AnalyticsSupport.recordNearbySearchResultsClose();
        }
        if (this.retrySelected) {
            com.navdy.hud.app.framework.notifications.NotificationManager a = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
            com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification a0 = (com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification)a.getNotification("navdy#place#type#search#notif");
            if (a0 == null) {
                a0 = new com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$300(this.this$0));
            }
            com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$400().v(new StringBuilder().append("launching notif search again:").append(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$300(this.this$0)).toString());
            a.addNotification((com.navdy.hud.app.framework.notifications.INotification)a0);
        }
    }
    
    public boolean onItemClicked(int i, int i0, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$DestinationPickerState a) {
        boolean b = false;
        com.navdy.hud.app.analytics.AnalyticsSupport.recordNearbySearchSelection(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$300(this.this$0), i0);
        this.itemSelected = true;
        if (i != R.id.search_again) {
            b = false;
        } else {
            com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$400().v("search again");
            this.retrySelected = true;
            b = true;
        }
        return b;
    }
    
    public boolean onItemSelected(int i, int i0, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$DestinationPickerState a) {
        return false;
    }
}
