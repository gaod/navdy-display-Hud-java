package com.navdy.hud.app.framework.glance;

class GlanceHandler$2 implements com.navdy.hud.app.framework.twilio.TwilioSmsManager$Callback {
    final com.navdy.hud.app.framework.glance.GlanceHandler this$0;
    final String val$message;
    final String val$name;
    final String val$number;
    
    GlanceHandler$2(com.navdy.hud.app.framework.glance.GlanceHandler a, String s, String s0, String s1) {
        super();
        this.this$0 = a;
        this.val$number = s;
        this.val$message = s0;
        this.val$name = s1;
    }
    
    public void result(com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode a) {
        if (a != com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode.SUCCESS) {
            this.this$0.sendSmsFailedNotification(this.val$number, this.val$message, this.val$name);
        }
    }
}
