package com.navdy.hud.app.framework.voice;

class VoiceSearchTipsView$2 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.framework.voice.VoiceSearchTipsView this$0;
    
    VoiceSearchTipsView$2(com.navdy.hud.app.framework.voice.VoiceSearchTipsView a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        super.onAnimationEnd(a);
        com.navdy.hud.app.framework.voice.VoiceSearchTipsView.access$202(this.this$0, (com.navdy.hud.app.framework.voice.VoiceSearchTipsView.access$200(this.this$0) + 1) % com.navdy.hud.app.framework.voice.VoiceSearchTipsView.access$300());
        String s = com.navdy.hud.app.framework.voice.VoiceSearchTipsView.access$400()[com.navdy.hud.app.framework.voice.VoiceSearchTipsView.access$200(this.this$0)];
        int i = com.navdy.hud.app.framework.voice.VoiceSearchTipsView.access$500(this.this$0, com.navdy.hud.app.framework.voice.VoiceSearchTipsView.access$200(this.this$0));
        this.this$0.tipsText1.setText((CharSequence)s);
        this.this$0.tipsIcon1.setImageResource(i);
        this.this$0.holder1.setAlpha(1f);
        this.this$0.holder2.setAlpha(0.0f);
    }
}
