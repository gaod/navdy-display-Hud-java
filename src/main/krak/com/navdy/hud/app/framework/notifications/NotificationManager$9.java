package com.navdy.hud.app.framework.notifications;

class NotificationManager$9 implements Runnable {
    final com.navdy.hud.app.framework.notifications.NotificationManager this$0;
    final int val$color;
    final android.view.View val$expandViewOut;
    final int val$item;
    final com.navdy.hud.app.framework.notifications.NotificationManager$Info val$notifInfo;
    
    NotificationManager$9(com.navdy.hud.app.framework.notifications.NotificationManager a, int i, int i0, com.navdy.hud.app.framework.notifications.NotificationManager$Info a0, android.view.View a1) {
        super();
        this.this$0 = a;
        this.val$item = i;
        this.val$color = i0;
        this.val$notifInfo = a0;
        this.val$expandViewOut = a1;
    }
    
    public void run() {
        synchronized(com.navdy.hud.app.framework.notifications.NotificationManager.access$2300(this.this$0)) {
            com.navdy.hud.app.framework.notifications.NotificationManager.access$3900(this.this$0).setCurrentItem(this.val$item, this.val$color);
            if (com.navdy.hud.app.framework.notifications.NotificationManager.access$1100(this.this$0)) {
                if (com.navdy.hud.app.framework.notifications.NotificationManager.access$1200(this.this$0) != null && com.navdy.hud.app.framework.notifications.NotificationManager.access$1200(this.this$0).startCalled) {
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$1200(this.this$0).startCalled = false;
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$1200(this.this$0).notification.onStop();
                }
                if (this.val$notifInfo == null) {
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$1302(this.this$0, com.navdy.hud.app.framework.notifications.NotificationManager.access$1200(this.this$0));
                }
                com.navdy.hud.app.framework.notifications.NotificationManager.access$1202(this.this$0, this.val$notifInfo);
                if (this.val$expandViewOut != null && com.navdy.hud.app.framework.glance.GlanceHelper.isDeleteAllView(this.val$expandViewOut)) {
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$4000(this.this$0).removeView(this.val$expandViewOut);
                    com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("delete all big view removed");
                    this.val$expandViewOut.setTag(null);
                    com.navdy.hud.app.framework.glance.GlanceViewCache.putView(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_TEXT, this.val$expandViewOut);
                }
                if (com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.isLoggable(2)) {
                    this.this$0.printPriorityMap();
                }
                if (com.navdy.hud.app.framework.notifications.NotificationManager.access$1200(this.this$0) != null) {
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$1200(this.this$0).notification.onExpandedNotificationSwitched();
                }
                com.navdy.hud.app.framework.notifications.NotificationManager.access$1600(this.this$0, this.val$item);
            }
            com.navdy.hud.app.framework.notifications.NotificationManager.access$4100(this.this$0);
            /*monexit(a)*/;
        }
        com.navdy.hud.app.framework.notifications.INotification a4 = (com.navdy.hud.app.framework.notifications.NotificationManager.access$1200(this.this$0) == null) ? null : com.navdy.hud.app.framework.notifications.NotificationManager.access$1200(this.this$0).notification;
        com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction("Glance_Advance", a4, (String)null);
    }
}
