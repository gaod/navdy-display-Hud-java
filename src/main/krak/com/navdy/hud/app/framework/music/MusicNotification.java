package com.navdy.hud.app.framework.music;
import com.navdy.hud.app.R;

public class MusicNotification implements com.navdy.hud.app.framework.notifications.INotification, com.navdy.hud.app.manager.MusicManager$MusicUpdateListener {
    final private static float ARTWORK_ALPHA_NOT_PLAYING = 0.5f;
    final private static float ARTWORK_ALPHA_PLAYING = 1f;
    final private static String EMPTY = "";
    final private static int MUSIC_TIMEOUT = 5000;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private com.navdy.service.library.util.ScalingUtilities$ScalingLogic FIT;
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.framework.music.MediaControllerLayout choiceLayout;
    private android.view.ViewGroup container;
    private com.navdy.hud.app.framework.notifications.INotificationController controller;
    private android.os.Handler handler;
    private com.navdy.hud.app.framework.music.AlbumArtImageView image;
    private java.util.concurrent.atomic.AtomicBoolean isKeyPressedDown;
    private String lastEventIdentifier;
    private com.navdy.hud.app.manager.MusicManager musicManager;
    private android.widget.ProgressBar progressBar;
    private android.widget.TextView subTitle;
    private android.widget.TextView title;
    private com.navdy.service.library.events.audio.MusicTrackInfo trackInfo;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.music.MusicNotification.class);
    }
    
    public MusicNotification(com.navdy.hud.app.manager.MusicManager a, com.squareup.otto.Bus a0) {
        this.FIT = com.navdy.service.library.util.ScalingUtilities$ScalingLogic.FIT;
        this.isKeyPressedDown = new java.util.concurrent.atomic.AtomicBoolean(false);
        this.musicManager = a;
        this.bus = a0;
        this.handler = new android.os.Handler();
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static void access$100(com.navdy.hud.app.framework.music.MusicNotification a, boolean b) {
        a.setDefaultImage(b);
    }
    
    static com.navdy.service.library.util.ScalingUtilities$ScalingLogic access$200(com.navdy.hud.app.framework.music.MusicNotification a) {
        return a.FIT;
    }
    
    static void access$300(com.navdy.hud.app.framework.music.MusicNotification a, android.graphics.Bitmap a0, boolean b) {
        a.setImage(a0, b);
    }
    
    static com.navdy.hud.app.framework.music.AlbumArtImageView access$400(com.navdy.hud.app.framework.music.MusicNotification a) {
        return a.image;
    }
    
    private void setDefaultImage(boolean b) {
        this.handler.post((Runnable)new com.navdy.hud.app.framework.music.MusicNotification$2(this, b));
    }
    
    private void setImage(android.graphics.Bitmap a, boolean b) {
        this.handler.post((Runnable)new com.navdy.hud.app.framework.music.MusicNotification$3(this, a, b));
    }
    
    public boolean canAddToStackIfCurrentExists() {
        return false;
    }
    
    public boolean expandNotification() {
        return false;
    }
    
    public int getColor() {
        return 0;
    }
    
    public android.view.View getExpandedView(android.content.Context a, Object a0) {
        return null;
    }
    
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    public String getId() {
        return "navdy#music#notif";
    }
    
    public int getTimeout() {
        return 5000;
    }
    
    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.MUSIC;
    }
    
    public android.view.View getView(android.content.Context a) {
        if (this.container == null) {
            this.container = (android.view.ViewGroup)android.view.LayoutInflater.from(a).inflate(R.layout.notification_music, (android.view.ViewGroup)null);
            this.title = (android.widget.TextView)this.container.findViewById(R.id.title);
            this.subTitle = (android.widget.TextView)this.container.findViewById(R.id.subTitle);
            this.image = (com.navdy.hud.app.framework.music.AlbumArtImageView)this.container.findViewById(R.id.image);
            this.progressBar = (android.widget.ProgressBar)this.container.findViewById(R.id.music_progress);
            this.choiceLayout = (com.navdy.hud.app.framework.music.MediaControllerLayout)this.container.findViewById(R.id.choiceLayout);
        }
        return this.container;
    }
    
    public android.animation.AnimatorSet getViewSwitchAnimation(boolean b) {
        return null;
    }
    
    public boolean isAlive() {
        return false;
    }
    
    public boolean isPurgeable() {
        return false;
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    public void onAlbumArtUpdate(okio.ByteString a, boolean b) {
        if (a != null) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.music.MusicNotification$1(this, a, b), 1);
        } else {
            sLogger.v("ByteString image to set in notification is null");
            this.setDefaultImage(b);
        }
    }
    
    public void onClick() {
    }
    
    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode a) {
    }
    
    public void onExpandedNotificationSwitched() {
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        label0: if (this.controller != null) {
            com.navdy.hud.app.framework.music.MediaControllerLayout a0 = this.choiceLayout;
            label1: {
                if (a0 == null) {
                    break label1;
                }
                switch(com.navdy.hud.app.framework.music.MusicNotification$4.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                    case 4: {
                        this.controller.resetTimeout();
                        b = true;
                        break label0;
                    }
                    case 3: {
                        this.controller.resetTimeout();
                        com.navdy.hud.app.ui.component.ChoiceLayout$Choice a1 = this.choiceLayout.getSelectedItem();
                        if (a1 != null) {
                            com.navdy.hud.app.manager.MusicManager$MediaControl a2 = com.navdy.hud.app.manager.MusicManager.CONTROLS[a1.id];
                            switch(com.navdy.hud.app.framework.music.MusicNotification$4.$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl[a2.ordinal()]) {
                                case 3: {
                                    this.choiceLayout.executeSelectedItem(true);
                                    android.os.Bundle a3 = new android.os.Bundle();
                                    String s = this.musicManager.getMusicMenuPath();
                                    if (s == null) {
                                        s = new StringBuilder().append("/").append(com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MUSIC.name()).toString();
                                    }
                                    a3.putString("MENU_PATH", s);
                                    com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.getId(), true, com.navdy.service.library.events.ui.Screen.SCREEN_MAIN_MENU, a3, null);
                                    b = true;
                                    break label0;
                                }
                                case 1: case 2: {
                                    if (this.trackInfo != null) {
                                        this.musicManager.executeMediaControl(a2, this.isKeyPressedDown.get());
                                    }
                                    this.choiceLayout.executeSelectedItem(true);
                                    b = true;
                                    break label0;
                                }
                                default: {
                                    b = false;
                                    break label0;
                                }
                            }
                        } else {
                            sLogger.w("Choice layout selected item is null, returning");
                            b = false;
                            break label0;
                        }
                    }
                    case 2: {
                        this.controller.resetTimeout();
                        this.choiceLayout.moveSelectionRight();
                        b = true;
                        break label0;
                    }
                    case 1: {
                        this.controller.resetTimeout();
                        this.choiceLayout.moveSelectionLeft();
                        b = true;
                        break label0;
                    }
                }
            }
            b = false;
        } else {
            b = false;
        }
        return b;
    }
    
    public void onKeyEvent(android.view.KeyEvent a) {
        label1: if (this.controller != null && this.choiceLayout != null && com.navdy.hud.app.manager.InputManager.isCenterKey(a.getKeyCode())) {
            com.navdy.hud.app.ui.component.ChoiceLayout$Choice a0 = this.choiceLayout.getSelectedItem();
            if (a0 != null) {
                com.navdy.hud.app.manager.MusicManager$MediaControl a1 = com.navdy.hud.app.manager.MusicManager.CONTROLS[a0.id];
                com.navdy.hud.app.manager.MusicManager$MediaControl a2 = com.navdy.hud.app.manager.MusicManager$MediaControl.NEXT;
                label2: {
                    if (a1 == a2) {
                        break label2;
                    }
                    if (a1 != com.navdy.hud.app.manager.MusicManager$MediaControl.PREVIOUS) {
                        break label1;
                    }
                }
                this.controller.resetTimeout();
                if (this.trackInfo != null) {
                    this.musicManager.handleKeyEvent(a, a1, this.isKeyPressedDown.get());
                }
                int i = a.getAction();
                label0: {
                    if (i != 0) {
                        break label0;
                    }
                    if (!this.isKeyPressedDown.compareAndSet(false, true)) {
                        break label0;
                    }
                    this.choiceLayout.keyDownSelectedItem();
                    break label1;
                }
                if (a.getAction() == 1 && this.isKeyPressedDown.compareAndSet(true, false)) {
                    this.choiceLayout.keyUpSelectedItem();
                }
            } else {
                sLogger.w("Choice layout selected item is null, returning");
            }
        }
    }
    
    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode a) {
    }
    
    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController a) {
        this.controller = a;
        this.bus.register(this);
        this.musicManager.addMusicUpdateListener((com.navdy.hud.app.manager.MusicManager$MusicUpdateListener)this);
        this.updateProgressBar(this.musicManager.getCurrentPosition());
    }
    
    public void onStop() {
        this.musicManager.removeMusicUpdateListener((com.navdy.hud.app.manager.MusicManager$MusicUpdateListener)this);
        this.bus.unregister(this);
        this.controller = null;
    }
    
    public void onTrackHand(float f) {
    }
    
    public void onTrackUpdated(com.navdy.service.library.events.audio.MusicTrackInfo a, java.util.Set a0, boolean b) {
        this.trackInfo = a;
        if (this.controller != null) {
            String s = com.navdy.service.library.util.MusicDataUtils.songIdentifierFromTrackInfo(a);
            if (this.lastEventIdentifier != null && !this.lastEventIdentifier.equals(s)) {
                this.controller.resetTimeout();
            }
            this.lastEventIdentifier = s;
            if (a.name == null) {
                this.subTitle.setText((CharSequence)"");
            } else {
                this.subTitle.setText((CharSequence)a.name);
            }
            if (a.author == null) {
                this.title.setText((CharSequence)"");
            } else {
                this.title.setText((CharSequence)a.author);
            }
            if (a.duration != null && a.currentPosition != null) {
                this.progressBar.setMax(a.duration.intValue());
                this.progressBar.setSecondaryProgress(a.duration.intValue());
            }
            if (com.navdy.hud.app.manager.MusicManager.tryingToPlay(a.playbackState)) {
                this.image.setAlpha(1f);
            } else {
                this.image.setAlpha(0.5f);
            }
            this.choiceLayout.updateControls(a0);
        }
    }
    
    public void onUpdate() {
        this.onTrackUpdated(this.musicManager.getCurrentTrack(), this.musicManager.getCurrentControls(), false);
        this.updateProgressBar(this.musicManager.getCurrentPosition());
    }
    
    public boolean supportScroll() {
        return false;
    }
    
    public void updateProgressBar(int i) {
        if (this.controller != null) {
            this.progressBar.setProgress(i);
        }
    }
}
