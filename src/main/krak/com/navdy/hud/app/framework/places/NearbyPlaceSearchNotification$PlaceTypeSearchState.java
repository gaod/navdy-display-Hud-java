package com.navdy.hud.app.framework.places;


    public enum NearbyPlaceSearchNotification$PlaceTypeSearchState {
        SEARCHING(0),
    ERROR(1),
    NO_RESULTS(2),
    SEARCH_COMPLETE(3);

        private int value;
        NearbyPlaceSearchNotification$PlaceTypeSearchState(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class NearbyPlaceSearchNotification$PlaceTypeSearchState extends Enum {
//    final private static com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState[] $VALUES;
//    final public static com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState ERROR;
//    final public static com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState NO_RESULTS;
//    final public static com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState SEARCHING;
//    final public static com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState SEARCH_COMPLETE;
//    
//    static {
//        SEARCHING = new com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState("SEARCHING", 0);
//        ERROR = new com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState("ERROR", 1);
//        NO_RESULTS = new com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState("NO_RESULTS", 2);
//        SEARCH_COMPLETE = new com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState("SEARCH_COMPLETE", 3);
//        com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState[] a = new com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState[4];
//        a[0] = SEARCHING;
//        a[1] = ERROR;
//        a[2] = NO_RESULTS;
//        a[3] = SEARCH_COMPLETE;
//        $VALUES = a;
//    }
//    
//    private NearbyPlaceSearchNotification$PlaceTypeSearchState(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState valueOf(String s) {
//        return (com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState)Enum.valueOf(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState[] values() {
//        return $VALUES.clone();
//    }
//}
//