package com.navdy.hud.app.framework.recentcall;

public class RecentCall implements Comparable {
    final private static com.navdy.service.library.log.Logger sLogger;
    public java.util.Date callTime;
    public com.navdy.hud.app.framework.recentcall.RecentCall$CallType callType;
    public com.navdy.hud.app.framework.recentcall.RecentCall$Category category;
    public int defaultImageIndex;
    public String firstName;
    public String formattedNumber;
    public String initials;
    public String name;
    public String number;
    public com.navdy.hud.app.framework.contacts.NumberType numberType;
    public String numberTypeStr;
    public long numericNumber;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.recentcall.RecentCall.class);
    }
    
    public RecentCall(com.navdy.hud.app.framework.recentcall.RecentCall a) {
        this.name = a.name;
        this.number = a.number;
        this.category = a.category;
        this.numberType = a.numberType;
        this.callTime = a.callTime;
        this.callType = a.callType;
        this.defaultImageIndex = a.defaultImageIndex;
    }
    
    public RecentCall(String s, com.navdy.hud.app.framework.recentcall.RecentCall$Category a, String s0, com.navdy.hud.app.framework.contacts.NumberType a0, java.util.Date a1, com.navdy.hud.app.framework.recentcall.RecentCall$CallType a2, int i, long j) {
        this.name = s;
        this.number = s0;
        this.category = a;
        this.numberType = a0;
        this.callTime = a1;
        this.callType = a2;
        this.defaultImageIndex = i;
        this.validateArguments();
    }
    
    public int compareTo(com.navdy.hud.app.framework.recentcall.RecentCall a) {
        return (a != null) ? this.callTime.compareTo(a.callTime) : 0;
    }
    
    public int compareTo(Object a) {
        return this.compareTo((com.navdy.hud.app.framework.recentcall.RecentCall)a);
    }
    
    public String toString() {
        return new StringBuilder().append("RecentCall{name='").append(this.name).append((char)39).append(", category=").append(this.category).append(", number='").append(this.number).append((char)39).append(", numberType=").append(this.numberType).append(", callTime=").append(this.callTime).append(", callType=").append(this.callType).append(", defaultImageIndex=").append(this.defaultImageIndex).append((char)125).toString();
    }
    
    public void validateArguments() {
        if (!android.text.TextUtils.isEmpty((CharSequence)this.name)) {
            this.firstName = com.navdy.hud.app.framework.contacts.ContactUtil.getFirstName(this.name);
            this.initials = com.navdy.hud.app.framework.contacts.ContactUtil.getInitials(this.name);
        }
        this.numberTypeStr = com.navdy.hud.app.framework.contacts.ContactUtil.getPhoneType(this.numberType);
        this.formattedNumber = com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(this.number);
        boolean b = android.text.TextUtils.isEmpty((CharSequence)this.number);
        label0: {
            Throwable a = null;
            if (b) {
                break label0;
            }
            label1: if (this.numericNumber <= 0L) {
                try {
                    java.util.Locale a0 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentLocale();
                    this.numericNumber = com.google.i18n.phonenumbers.PhoneNumberUtil.getInstance().parse(this.number, a0.getCountry()).getNationalNumber();
                } catch(Throwable a1) {
                    a = a1;
                    break label1;
                }
                break label0;
            } else {
                this.numericNumber = this.numericNumber;
                break label0;
            }
            if (sLogger.isLoggable(2)) {
                sLogger.e(a);
            }
        }
    }
}
