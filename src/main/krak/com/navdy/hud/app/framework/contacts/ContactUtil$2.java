package com.navdy.hud.app.framework.contacts;

final class ContactUtil$2 implements com.squareup.picasso.Callback {
    final com.navdy.hud.app.ui.component.image.InitialsImageView val$imageView;
    final java.io.File val$path;
    
    ContactUtil$2(com.navdy.hud.app.ui.component.image.InitialsImageView a, java.io.File a0) {
        super();
        this.val$imageView = a;
        this.val$path = a0;
    }
    
    public void onError() {
    }
    
    public void onSuccess() {
        this.val$imageView.setTag(this.val$path.getAbsolutePath());
    }
}
