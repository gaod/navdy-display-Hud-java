package com.navdy.hud.app.framework.recentcall;


public enum RecentCall$Category {
    UNNKNOWN(0),
    PHONE_CALL(1),
    MESSAGE(2);

    private int value;
    RecentCall$Category(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class RecentCall$Category extends Enum {
//    final private static com.navdy.hud.app.framework.recentcall.RecentCall$Category[] $VALUES;
//    final public static com.navdy.hud.app.framework.recentcall.RecentCall$Category MESSAGE;
//    final public static com.navdy.hud.app.framework.recentcall.RecentCall$Category PHONE_CALL;
//    final public static com.navdy.hud.app.framework.recentcall.RecentCall$Category UNNKNOWN;
//    int value;
//    
//    static {
//        UNNKNOWN = new com.navdy.hud.app.framework.recentcall.RecentCall$Category("UNNKNOWN", 0, 0);
//        PHONE_CALL = new com.navdy.hud.app.framework.recentcall.RecentCall$Category("PHONE_CALL", 1, 1);
//        MESSAGE = new com.navdy.hud.app.framework.recentcall.RecentCall$Category("MESSAGE", 2, 2);
//        com.navdy.hud.app.framework.recentcall.RecentCall$Category[] a = new com.navdy.hud.app.framework.recentcall.RecentCall$Category[3];
//        a[0] = UNNKNOWN;
//        a[1] = PHONE_CALL;
//        a[2] = MESSAGE;
//        $VALUES = a;
//    }
//    
//    private RecentCall$Category(String s, int i, int i0) {
//        super(s, i);
//        this.value = i0;
//    }
//    
//    public static com.navdy.hud.app.framework.recentcall.RecentCall$Category buildFromValue(int i) {
//        com.navdy.hud.app.framework.recentcall.RecentCall$Category a = null;
//        switch(i) {
//            case 2: {
//                a = MESSAGE;
//                break;
//            }
//            case 1: {
//                a = PHONE_CALL;
//                break;
//            }
//            default: {
//                a = UNNKNOWN;
//            }
//        }
//        return a;
//    }
//    
//    public static com.navdy.hud.app.framework.recentcall.RecentCall$Category valueOf(String s) {
//        return (com.navdy.hud.app.framework.recentcall.RecentCall$Category)Enum.valueOf(com.navdy.hud.app.framework.recentcall.RecentCall$Category.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.recentcall.RecentCall$Category[] values() {
//        return $VALUES.clone();
//    }
//    
//    public int getValue() {
//        return this.value;
//    }
//}
//