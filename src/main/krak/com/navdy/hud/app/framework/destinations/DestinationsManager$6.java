package com.navdy.hud.app.framework.destinations;

class DestinationsManager$6 {
    final static int[] $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$DestinationType;
    final static int[] $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType;
    final static int[] $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$PlaceCategory;
    
    static {
        $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$DestinationType = new int[com.navdy.hud.app.framework.destinations.Destination$DestinationType.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$DestinationType;
        com.navdy.hud.app.framework.destinations.Destination$DestinationType a0 = com.navdy.hud.app.framework.destinations.Destination$DestinationType.DEFAULT;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$DestinationType[com.navdy.hud.app.framework.destinations.Destination$DestinationType.FIND_GAS.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$PlaceCategory = new int[com.navdy.hud.app.framework.destinations.Destination$PlaceCategory.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$PlaceCategory;
        com.navdy.hud.app.framework.destinations.Destination$PlaceCategory a2 = com.navdy.hud.app.framework.destinations.Destination$PlaceCategory.SUGGESTED;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$PlaceCategory[com.navdy.hud.app.framework.destinations.Destination$PlaceCategory.SUGGESTED_RECENT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$PlaceCategory[com.navdy.hud.app.framework.destinations.Destination$PlaceCategory.RECENT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException3) {
        }
        $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType = new int[com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.values().length];
        int[] a3 = $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType;
        com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType a4 = com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.FAVORITE_NONE;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType[com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.FAVORITE_HOME.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType[com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.FAVORITE_WORK.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType[com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.FAVORITE_CONTACT.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType[com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.FAVORITE_CALENDAR.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType[com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.FAVORITE_CUSTOM.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException9) {
        }
    }
}
