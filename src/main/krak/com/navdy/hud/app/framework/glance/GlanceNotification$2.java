package com.navdy.hud.app.framework.glance;
import com.navdy.hud.app.R;

class GlanceNotification$2 implements com.navdy.hud.app.ui.component.ChoiceLayout2$IListener {
    final com.navdy.hud.app.framework.glance.GlanceNotification this$0;
    
    GlanceNotification$2(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        super();
        this.this$0 = a;
    }
    
    public void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2$Selection a) {
        if (com.navdy.hud.app.framework.glance.GlanceNotification.access$000(this.this$0) != null) {
            switch(a.id) {
                case 8: {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager a0 = com.navdy.hud.app.framework.fuel.FuelRoutingManager.getInstance();
                    if (a0 != null) {
                        a0.dismissGasRoute();
                    }
                    com.navdy.hud.app.framework.glance.GlanceNotification.access$1500(this.this$0);
                    break;
                }
                case 7: {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager a1 = com.navdy.hud.app.framework.fuel.FuelRoutingManager.getInstance();
                    if (a1 != null) {
                        a1.routeToGasStation();
                    }
                    com.navdy.hud.app.framework.glance.GlanceNotification.access$1500(this.this$0);
                    break;
                }
                case 6: {
                    com.navdy.hud.app.framework.glance.GlanceNotification.access$1500(this.this$0);
                    break;
                }
                case 5: {
                    if (com.navdy.hud.app.framework.glance.GlanceNotification.access$1800(this.this$0).getTag(R.id.message_secondary_screen) == null) {
                        com.navdy.hud.app.framework.glance.GlanceNotification.access$1900(this.this$0);
                        com.navdy.hud.app.framework.glance.GlanceNotification.access$000(this.this$0).startTimeout(this.this$0.getTimeout());
                        break;
                    } else {
                        com.navdy.hud.app.framework.glance.GlanceNotification.access$000(this.this$0).collapseNotification(false, false);
                        break;
                    }
                }
                case 4: {
                    com.navdy.hud.app.framework.glance.GlanceNotification.access$1400(this.this$0, com.navdy.hud.app.framework.glance.GlanceNotification$Mode.REPLY, com.navdy.hud.app.framework.glance.GlanceNotification.access$1700(this.this$0));
                    break;
                }
                case 3: {
                    com.navdy.hud.app.framework.glance.GlanceNotification.access$1600(this.this$0);
                    break;
                }
                case 2: {
                    com.navdy.hud.app.framework.glance.GlanceNotification.access$1400(this.this$0, com.navdy.hud.app.framework.glance.GlanceNotification$Mode.READ, (String)null);
                    break;
                }
                case 1: {
                    com.navdy.hud.app.framework.glance.GlanceNotification.access$1300(this.this$0);
                    break;
                }
            }
        }
    }
    
    public void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2$Selection a) {
        if (com.navdy.hud.app.framework.glance.GlanceNotification.access$000(this.this$0) != null) {
            com.navdy.hud.app.framework.glance.GlanceNotification.access$000(this.this$0).resetTimeout();
        }
    }
}
