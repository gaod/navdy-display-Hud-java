package com.navdy.hud.app.framework.twilio;
import com.navdy.hud.app.R;

class TwilioSmsManager$1 implements Runnable {
    final com.navdy.hud.app.framework.twilio.TwilioSmsManager this$0;
    final com.navdy.hud.app.framework.twilio.TwilioSmsManager$Callback val$cb;
    final String val$message;
    final String val$number;
    final String val$profileId;
    
    TwilioSmsManager$1(com.navdy.hud.app.framework.twilio.TwilioSmsManager a, String s, String s0, String s1, com.navdy.hud.app.framework.twilio.TwilioSmsManager$Callback a0) {
        super();
        this.this$0 = a;
        this.val$message = s;
        this.val$number = s0;
        this.val$profileId = s1;
        this.val$cb = a0;
    }
    
    public void run() {
        try {
            String s = null;
            com.navdy.hud.app.framework.twilio.TwilioSmsManager.access$000().v("sendSms-start");
            String s0 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getFirstName();
            if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                s = this.val$message;
            } else {
                android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
                Object[] a0 = new Object[2];
                a0[0] = s0;
                a0[1] = this.val$message;
                s = a.getString(R.string.ios_sms_format, a0);
            }
            String s1 = com.navdy.hud.app.util.PhoneUtil.convertToE164Format(this.val$number);
            okhttp3.FormBody a1 = new okhttp3.FormBody$Builder().add("To", s1).add("MessagingServiceSid", com.navdy.hud.app.framework.twilio.TwilioSmsManager.access$100()).add("Body", s).build();
            okhttp3.Request a2 = new okhttp3.Request$Builder().url(com.navdy.hud.app.framework.twilio.TwilioSmsManager.access$300()).header("Authorization", com.navdy.hud.app.framework.twilio.TwilioSmsManager.access$200()).post((okhttp3.RequestBody)a1).build();
            okhttp3.Response a3 = com.navdy.hud.app.framework.twilio.TwilioSmsManager.access$400(this.this$0).newCall(a2).execute();
            if (a3.isSuccessful()) {
                com.navdy.hud.app.framework.twilio.TwilioSmsManager.access$000().v(new StringBuilder().append("sendSms-end-suc:").append(a3.code()).toString());
                com.navdy.hud.app.framework.twilio.TwilioSmsManager.access$500(this.this$0, com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode.SUCCESS, this.val$profileId, this.val$cb);
            } else {
                com.navdy.hud.app.framework.twilio.TwilioSmsManager.access$000().e(new StringBuilder().append("sendSms-end-err:").append(a3.code()).append(",").append(a3.message()).toString());
                com.navdy.hud.app.framework.twilio.TwilioSmsManager.access$500(this.this$0, com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode.TWILIO_SERVER_ERROR, this.val$profileId, this.val$cb);
            }
        } catch(Throwable a4) {
            com.navdy.hud.app.framework.twilio.TwilioSmsManager.access$000().e("sendSms-end-err", a4);
            if (a4 instanceof java.io.IOException) {
                com.navdy.hud.app.framework.twilio.TwilioSmsManager.access$500(this.this$0, com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode.NETWORK_ERROR, this.val$profileId, this.val$cb);
            } else {
                com.navdy.hud.app.framework.twilio.TwilioSmsManager.access$500(this.this$0, com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode.INTERNAL_ERROR, this.val$profileId, this.val$cb);
            }
        }
    }
}
