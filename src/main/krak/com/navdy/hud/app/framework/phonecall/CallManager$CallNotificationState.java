package com.navdy.hud.app.framework.phonecall;


    public enum CallManager$CallNotificationState {
        IDLE(0),
    RINGING(1),
    MISSED(2),
    ENDED(3),
    FAILED(4),
    DIALING(5),
    IN_PROGRESS(6),
    REJECTED(7),
    CANCELLED(8);

        private int value;
        CallManager$CallNotificationState(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class CallManager$CallNotificationState extends Enum {
//    final private static com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState[] $VALUES;
//    final public static com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState CANCELLED;
//    final public static com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState DIALING;
//    final public static com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState ENDED;
//    final public static com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState FAILED;
//    final public static com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState IDLE;
//    final public static com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState IN_PROGRESS;
//    final public static com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState MISSED;
//    final public static com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState REJECTED;
//    final public static com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState RINGING;
//    
//    static {
//        IDLE = new com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState("IDLE", 0);
//        RINGING = new com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState("RINGING", 1);
//        MISSED = new com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState("MISSED", 2);
//        ENDED = new com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState("ENDED", 3);
//        FAILED = new com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState("FAILED", 4);
//        DIALING = new com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState("DIALING", 5);
//        IN_PROGRESS = new com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState("IN_PROGRESS", 6);
//        REJECTED = new com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState("REJECTED", 7);
//        CANCELLED = new com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState("CANCELLED", 8);
//        com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState[] a = new com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState[9];
//        a[0] = IDLE;
//        a[1] = RINGING;
//        a[2] = MISSED;
//        a[3] = ENDED;
//        a[4] = FAILED;
//        a[5] = DIALING;
//        a[6] = IN_PROGRESS;
//        a[7] = REJECTED;
//        a[8] = CANCELLED;
//        $VALUES = a;
//    }
//    
//    private CallManager$CallNotificationState(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState valueOf(String s) {
//        return (com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState)Enum.valueOf(com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState[] values() {
//        return $VALUES.clone();
//    }
//}
//