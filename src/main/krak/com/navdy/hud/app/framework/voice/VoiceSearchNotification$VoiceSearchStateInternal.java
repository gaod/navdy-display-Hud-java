package com.navdy.hud.app.framework.voice;
import com.navdy.hud.app.R;


    public enum VoiceSearchNotification$VoiceSearchStateInternal {
        IDLE(0),
    STARTING(1),
    LISTENING(2),
    SEARCHING(3),
    FAILED(4),
    RESULTS(5);

        private int value;
        VoiceSearchNotification$VoiceSearchStateInternal(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class VoiceSearchNotification$VoiceSearchStateInternal extends Enum {
//    final private static com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal[] $VALUES;
//    final public static com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal FAILED;
//    final public static com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal IDLE;
//    final public static com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal LISTENING;
//    final public static com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal RESULTS;
//    final public static com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal SEARCHING;
//    final public static com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal STARTING;
//    private int statusTextResId;
//    
//    static {
//        IDLE = new com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal("IDLE", 0, -1);
//        STARTING = new com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal("STARTING", 1, R.string.voice_search_starting);
//        LISTENING = new com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal("LISTENING", 2, R.string.voice_search_listening);
//        SEARCHING = new com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal("SEARCHING", 3, R.string.voice_search_searching);
//        FAILED = new com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal("FAILED", 4, R.string.voice_search_failed);
//        RESULTS = new com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal("RESULTS", 5, R.string.voice_search_more_results);
//        com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal[] a = new com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal[6];
//        a[0] = IDLE;
//        a[1] = STARTING;
//        a[2] = LISTENING;
//        a[3] = SEARCHING;
//        a[4] = FAILED;
//        a[5] = RESULTS;
//        $VALUES = a;
//    }
//    
//    private VoiceSearchNotification$VoiceSearchStateInternal(String s, int i, int i0) {
//        super(s, i);
//        this.statusTextResId = i0;
//    }
//    
//    public static com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal valueOf(String s) {
//        return (com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal)Enum.valueOf(com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal[] values() {
//        return $VALUES.clone();
//    }
//    
//    public int getStatusTextResId() {
//        return this.statusTextResId;
//    }
//}
//