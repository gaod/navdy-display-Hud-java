package com.navdy.hud.app.framework.glympse;

class GlympseNotification$1 implements Runnable {
    final com.navdy.hud.app.framework.glympse.GlympseNotification this$0;
    
    GlympseNotification$1(com.navdy.hud.app.framework.glympse.GlympseNotification a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.framework.glympse.GlympseNotification.access$000().v("Glympse:retry sending message");
        StringBuilder a = new StringBuilder();
        boolean b = com.navdy.hud.app.framework.glympse.GlympseManager.getInstance().addMessage(com.navdy.hud.app.framework.glympse.GlympseNotification.access$100(this.this$0), com.navdy.hud.app.framework.glympse.GlympseNotification.access$200(this.this$0), com.navdy.hud.app.framework.glympse.GlympseNotification.access$300(this.this$0), com.navdy.hud.app.framework.glympse.GlympseNotification.access$400(this.this$0), com.navdy.hud.app.framework.glympse.GlympseNotification.access$500(this.this$0), a) != com.navdy.hud.app.framework.glympse.GlympseManager$Error.NONE;
        String s = (com.navdy.hud.app.framework.glympse.GlympseNotification.access$300(this.this$0) != null) ? "Trip" : "Location";
        if (b) {
            com.navdy.hud.app.framework.glympse.GlympseNotification.access$600(com.navdy.hud.app.framework.glympse.GlympseNotification.access$200(this.this$0), com.navdy.hud.app.framework.glympse.GlympseNotification.access$100(this.this$0), com.navdy.hud.app.framework.glympse.GlympseNotification.access$300(this.this$0), com.navdy.hud.app.framework.glympse.GlympseNotification.access$400(this.this$0), com.navdy.hud.app.framework.glympse.GlympseNotification.access$500(this.this$0));
        }
        com.navdy.hud.app.framework.glympse.GlympseNotification.access$000().v(new StringBuilder().append("Glympse:retry sucess:").append(!b).toString());
        com.navdy.hud.app.analytics.AnalyticsSupport.recordGlympseSent(!b, s, com.navdy.hud.app.framework.glympse.GlympseNotification.access$200(this.this$0), a.toString());
    }
}
