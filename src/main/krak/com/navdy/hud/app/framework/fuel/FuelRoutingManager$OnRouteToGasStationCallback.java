package com.navdy.hud.app.framework.fuel;

abstract public interface FuelRoutingManager$OnRouteToGasStationCallback {
    abstract public void onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error arg);
    
    
    abstract public void onOptimalRouteCalculationComplete(com.navdy.hud.app.framework.fuel.FuelRoutingManager$RouteCacheItem arg, java.util.ArrayList arg0, com.navdy.service.library.events.navigation.NavigationRouteRequest arg1);
}
