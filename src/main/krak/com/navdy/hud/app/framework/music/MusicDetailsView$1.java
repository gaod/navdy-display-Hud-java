package com.navdy.hud.app.framework.music;

class MusicDetailsView$1 implements com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$Callback {
    final com.navdy.hud.app.framework.music.MusicDetailsView this$0;
    
    MusicDetailsView$1(com.navdy.hud.app.framework.music.MusicDetailsView a) {
        super();
        this.this$0 = a;
    }
    
    public void close() {
        this.this$0.presenter.close();
    }
    
    public boolean isClosed() {
        return this.this$0.presenter.isClosed();
    }
    
    public boolean isItemClickable(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        return this.this$0.presenter.isItemClickable(a);
    }
    
    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, android.view.View a0, int i, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a1) {
    }
    
    public void onFastScrollEnd() {
    }
    
    public void onFastScrollStart() {
    }
    
    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
    }
    
    public void onLoad() {
        com.navdy.hud.app.framework.music.MusicDetailsView.access$000().v("onLoad");
        this.this$0.presenter.resetSelectedItem();
    }
    
    public void onScrollIdle() {
    }
    
    public void select(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        this.this$0.presenter.selectItem(a);
    }
    
    public void showToolTip() {
    }
}
