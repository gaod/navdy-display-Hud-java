package com.navdy.hud.app.framework.fuel;

class FuelRoutingManager$6$1$2 implements Runnable {
    final com.navdy.hud.app.framework.fuel.FuelRoutingManager$6$1 this$2;
    final com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error val$error;
    
    FuelRoutingManager$6$1$2(com.navdy.hud.app.framework.fuel.FuelRoutingManager$6$1 a, com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error a0) {
        super();
        this.this$2 = a;
        this.val$error = a0;
    }
    
    public void run() {
        com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().w(new StringBuilder().append("received an error on calculateGasStationRoutes ").append(this.val$error.name()).toString());
        com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$602(this.this$2.this$1.this$0, false);
        if (this.this$2.this$1.val$postNotificationOnError) {
            java.util.ArrayList a = new java.util.ArrayList(1);
            ((java.util.List)a).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.FuelConstants.FUEL_LEVEL.name(), String.valueOf(this.this$2.this$1.val$fuelLevel)));
            ((java.util.List)a).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.FuelConstants.NO_ROUTE.name(), ""));
            com.navdy.service.library.events.glances.GlanceEvent a0 = new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_FUEL).id("low#fuel#level").postTime(Long.valueOf(System.currentTimeMillis())).provider("com.navdy.fuel").glanceData((java.util.List)a).build();
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$2400(this.this$2.this$1.this$0).post(a0);
            if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().isLoggable(2)) {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().v("posting low fuel glance with no routes");
            }
            this.this$2.this$1.this$0.reset();
        }
    }
}
