package com.navdy.hud.app.framework.glance;
import com.navdy.hud.app.R;


public enum GlanceViewCache$ViewType {
    SMALL_GLANCE_MESSAGE(0),
    BIG_GLANCE_MESSAGE(1),
    BIG_GLANCE_MESSAGE_SINGLE(2),
    SMALL_IMAGE(3),
    BIG_TEXT(4),
    BIG_MULTI_TEXT(5),
    SMALL_SIGN(6),
    BIG_CALENDAR(7);

    private int value;
    GlanceViewCache$ViewType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class GlanceViewCache$ViewType extends Enum {
//    final private static com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType[] $VALUES;
//    final public static com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType BIG_CALENDAR;
//    final public static com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType BIG_GLANCE_MESSAGE;
//    final public static com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType BIG_GLANCE_MESSAGE_SINGLE;
//    final public static com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType BIG_MULTI_TEXT;
//    final public static com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType BIG_TEXT;
//    final public static com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType SMALL_GLANCE_MESSAGE;
//    final public static com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType SMALL_IMAGE;
//    final public static com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType SMALL_SIGN;
//    final int cacheSize;
//    final int layoutId;
//    
//    static {
//        SMALL_GLANCE_MESSAGE = new com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType("SMALL_GLANCE_MESSAGE", 0, R.layout.glance_small_message, 2);
//        BIG_GLANCE_MESSAGE = new com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType("BIG_GLANCE_MESSAGE", 1, R.layout.glance_large_message, 2);
//        BIG_GLANCE_MESSAGE_SINGLE = new com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType("BIG_GLANCE_MESSAGE_SINGLE", 2, R.layout.glance_large_message_single, 2);
//        SMALL_IMAGE = new com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType("SMALL_IMAGE", 3, R.layout.glance_small_image, 2);
//        BIG_TEXT = new com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType("BIG_TEXT", 4, R.layout.glance_large_text, 2);
//        BIG_MULTI_TEXT = new com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType("BIG_MULTI_TEXT", 5, R.layout.glance_large_multi_text, 2);
//        SMALL_SIGN = new com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType("SMALL_SIGN", 6, R.layout.glance_small_sign, 2);
//        BIG_CALENDAR = new com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType("BIG_CALENDAR", 7, R.layout.glance_large_calendar, 2);
//        com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType[] a = new com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType[8];
//        a[0] = SMALL_GLANCE_MESSAGE;
//        a[1] = BIG_GLANCE_MESSAGE;
//        a[2] = BIG_GLANCE_MESSAGE_SINGLE;
//        a[3] = SMALL_IMAGE;
//        a[4] = BIG_TEXT;
//        a[5] = BIG_MULTI_TEXT;
//        a[6] = SMALL_SIGN;
//        a[7] = BIG_CALENDAR;
//        $VALUES = a;
//    }
//    
//    private GlanceViewCache$ViewType(String s, int i, int i0, int i1) {
//        super(s, i);
//        this.layoutId = i0;
//        this.cacheSize = i1;
//    }
//    
//    public static com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType valueOf(String s) {
//        return (com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType)Enum.valueOf(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType[] values() {
//        return $VALUES.clone();
//    }
//}
//