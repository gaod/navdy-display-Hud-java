package com.navdy.hud.app.framework.glympse;
import com.navdy.hud.app.R;

final public class GlympseManager {
    final private static String GLYMPSE_API_KEY;
    final private static String GLYMPSE_BASE_URL = "api.glympse.com";
    final public static int GLYMPSE_DURATION;
    final public static String GLYMPSE_TYPE_SHARE_LOCATION = "Location";
    final public static String GLYMPSE_TYPE_SHARE_TRIP = "Trip";
    final private static String NAVDY_CONTACT_UUID = "navdy_contact_uuid";
    final private static long NAVDY_GLYMPSE_PARTNER_ID = 1L;
    final private static int UPDATE_ETA_INTERVAL;
    final private static android.os.Handler handler;
    final private static com.navdy.service.library.log.Logger logger;
    final private static String[] messages;
    private String currentGlympseUserId;
    private com.glympse.android.api.GGlympse glympse;
    private boolean glympseInitFailed;
    private boolean isTrackingETA;
    final private com.navdy.hud.app.framework.notifications.NotificationManager notificationManager;
    final private java.util.Map ticketsAwaitingReadReceipt;
    private Runnable updateETA;
    
    static {
        logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.glympse.GlympseManager.class);
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        String[] a0 = new String[5];
        a0[0] = a.getString(R.string.send_without_message);
        a0[1] = a.getString(R.string.on_my_way);
        a0[2] = a.getString(R.string.i_am_driving);
        a0[3] = a.getString(R.string.running_late);
        a0[4] = a.getString(R.string.i_am_here);
        messages = a0;
        GLYMPSE_API_KEY = com.navdy.service.library.util.CredentialUtil.getCredentials(com.navdy.hud.app.HudApplication.getAppContext(), "GLYMPSE_API_KEY");
        GLYMPSE_DURATION = (int)java.util.concurrent.TimeUnit.HOURS.toMillis(1L);
        UPDATE_ETA_INTERVAL = (int)java.util.concurrent.TimeUnit.MINUTES.toMillis(1L);
        handler = new android.os.Handler(android.os.Looper.getMainLooper());
    }
    
    private GlympseManager() {
        this.updateETA = (Runnable)new com.navdy.hud.app.framework.glympse.GlympseManager$1(this);
        this.notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        this.ticketsAwaitingReadReceipt = (java.util.Map)new java.util.HashMap();
        com.navdy.service.library.log.Logger a = logger;
        try {
            a.v("creating glympse");
            this.glympse = com.glympse.android.api.GlympseFactory.createGlympse(com.navdy.hud.app.HudApplication.getAppContext(), "api.glympse.com", GLYMPSE_API_KEY);
            this.setUpEventListener();
            this.glympse.setEtaMode(1);
            this.glympse.start();
            logger.v(new StringBuilder().append("calling glympse start duration=").append((long)com.navdy.hud.app.maps.MapSettings.getGlympseDuration() / java.util.concurrent.TimeUnit.MINUTES.toMillis(1L)).append(" minutes").toString());
        } catch(Throwable a0) {
            logger.e(a0);
            this.glympseInitFailed = true;
        }
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus().register(this);
    }
    
    GlympseManager(com.navdy.hud.app.framework.glympse.GlympseManager$1 a) {
        this();
    }
    
    static void access$1000(com.navdy.hud.app.framework.glympse.GlympseManager a, com.navdy.hud.app.framework.contacts.Contact a0) {
        a.addGlympseSentGlance(a0);
    }
    
    static void access$1100(com.navdy.hud.app.framework.glympse.GlympseManager a, com.navdy.hud.app.framework.contacts.Contact a0) {
        a.addGlympseReadGlance(a0);
    }
    
    static com.navdy.service.library.log.Logger access$200() {
        return logger;
    }
    
    static com.glympse.android.api.GGlympse access$300(com.navdy.hud.app.framework.glympse.GlympseManager a) {
        return a.glympse;
    }
    
    static android.os.Handler access$400() {
        return handler;
    }
    
    static int access$500() {
        return UPDATE_ETA_INTERVAL;
    }
    
    static boolean access$600(com.navdy.hud.app.framework.glympse.GlympseManager a, int i, int i0) {
        return a.hasGlympseEvent(i, i0);
    }
    
    static void access$700(com.navdy.hud.app.framework.glympse.GlympseManager a) {
        a.setUserProfile();
    }
    
    static void access$800(com.navdy.hud.app.framework.glympse.GlympseManager a, com.glympse.android.api.GTicket a0) {
        a.addTicketListener(a0);
    }
    
    static java.util.Map access$900(com.navdy.hud.app.framework.glympse.GlympseManager a) {
        return a.ticketsAwaitingReadReceipt;
    }
    
    private void addGlympseReadGlance(com.navdy.hud.app.framework.contacts.Contact a) {
        com.navdy.hud.app.framework.glympse.GlympseNotification a0 = new com.navdy.hud.app.framework.glympse.GlympseNotification(a, com.navdy.hud.app.framework.glympse.GlympseNotification$Type.READ);
        this.notificationManager.addNotification((com.navdy.hud.app.framework.notifications.INotification)a0);
    }
    
    private void addGlympseSentGlance(com.navdy.hud.app.framework.contacts.Contact a) {
        com.navdy.hud.app.framework.glympse.GlympseNotification a0 = new com.navdy.hud.app.framework.glympse.GlympseNotification(a, com.navdy.hud.app.framework.glympse.GlympseNotification$Type.SENT);
        this.notificationManager.addNotification((com.navdy.hud.app.framework.notifications.INotification)a0);
    }
    
    private void addTicketListener(com.glympse.android.api.GTicket a) {
        a.addListener((com.glympse.android.api.GEventListener)new com.navdy.hud.app.framework.glympse.GlympseManager$3(this, a));
    }
    
    private void deleteActiveTickets() {
        logger.v("deleteActiveTickets");
        this.stopTrackingETA();
        com.glympse.android.core.GArray a = this.glympse.getHistoryManager().getTickets();
        int i = a.length();
        Object a0 = a;
        int i0 = 0;
        while(i0 < i) {
            Object a1 = ((com.glympse.android.core.GArray)a0).at(i0);
            logger.v(new StringBuilder().append("deleteActiveTickets deleting ticket with id ").append(((com.glympse.android.api.GTicket)a1).getId()).toString());
            ((com.glympse.android.api.GTicket)a1).deleteTicket();
            i0 = i0 + 1;
        }
    }
    
    public static com.navdy.hud.app.framework.glympse.GlympseManager getInstance() {
        return com.navdy.hud.app.framework.glympse.GlympseManager$InternalSingleton.access$100();
    }
    
    private boolean hasGlympseEvent(int i, int i0) {
        return (i & i0) != 0;
    }
    
    private void setUpEventListener() {
        com.navdy.hud.app.framework.glympse.GlympseManager$2 a = new com.navdy.hud.app.framework.glympse.GlympseManager$2(this);
        this.glympse.addListener((com.glympse.android.api.GEventListener)a);
    }
    
    private void setUserProfile() {
        com.navdy.hud.app.profile.DriverProfile a = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
        if (android.text.TextUtils.equals((CharSequence)this.currentGlympseUserId, (CharSequence)a.getProfileName())) {
            logger.v("called setUserProfile with same profile as current, no-op");
        } else {
            com.glympse.android.api.GUser a0 = this.glympse.getUserManager().getSelf();
            if (a.isDefaultProfile()) {
                logger.v("driverProfile is default, not setting it for Glympse");
            } else {
                this.deleteActiveTickets();
                this.ticketsAwaitingReadReceipt.clear();
                logger.v(new StringBuilder().append("setting new user profile for Glympse: ").append(a.getProfileName()).toString());
                if (!android.text.TextUtils.isEmpty((CharSequence)a.getFirstName())) {
                    a0.setNickname(a.getFirstName());
                }
                if (a.getDriverImage() == null) {
                    logger.v("not setting photo");
                    a0.setAvatar((com.glympse.android.core.GDrawable)null);
                } else {
                    logger.v("setting photo");
                    a0.setAvatar(com.glympse.android.core.CoreFactory.createDrawable(a.getDriverImage()));
                }
                this.currentGlympseUserId = a.getProfileName();
            }
        }
    }
    
    private void startTrackingETA() {
        if (!this.isTrackingETA) {
            this.isTrackingETA = true;
            handler.post(this.updateETA);
        }
    }
    
    private void stopTrackingETA() {
        handler.removeCallbacks(this.updateETA);
        this.isTrackingETA = false;
    }
    
    public com.navdy.hud.app.framework.glympse.GlympseManager$Error addMessage(com.navdy.hud.app.framework.contacts.Contact a, String s, String s0, double d, double d0, StringBuilder a0) {
        com.navdy.hud.app.framework.glympse.GlympseManager$Error a1 = null;
        if (this.glympseInitFailed) {
            logger.e("glympse init failed, cannot add message, no-op");
            a1 = com.navdy.hud.app.framework.glympse.GlympseManager$Error.INTERNAL_ERROR;
        } else if (com.navdy.hud.app.framework.network.NetworkStateManager.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext())) {
            boolean b = false;
            com.glympse.android.api.GPlace a2 = null;
            boolean b0 = android.text.TextUtils.isEmpty((CharSequence)s0);
            label2: {
                label0: {
                    label1: {
                        if (b0) {
                            break label1;
                        }
                        if (d != 0.0) {
                            break label0;
                        }
                        if (d0 != 0.0) {
                            break label0;
                        }
                    }
                    b = false;
                    break label2;
                }
                b = true;
            }
            if (b) {
                a2 = com.glympse.android.api.GlympseFactory.createPlace(d, d0, com.glympse.android.core.CoreFactory.createString(s0));
                this.startTrackingETA();
            } else {
                this.stopTrackingETA();
                a2 = null;
            }
            com.glympse.android.api.GTicket a3 = com.glympse.android.api.GlympseFactory.createTicket(com.navdy.hud.app.maps.MapSettings.getGlympseDuration(), s, a2);
            String s1 = com.navdy.hud.app.util.PhoneUtil.convertToE164Format(a.number);
            a3.addInvite(com.glympse.android.api.GlympseFactory.createInvite(3, a.name, s1));
            String s2 = java.util.UUID.randomUUID().toString();
            a0.setLength(0);
            a0.append(s2);
            this.ticketsAwaitingReadReceipt.put(s2, a);
            a3.appendData(1L, "navdy_contact_uuid", com.glympse.android.core.CoreFactory.createPrimitive(s2));
            this.glympse.sendTicket(a3);
            logger.v(new StringBuilder().append("addMessage, added ticket name[").append(a.name).append("] e164Number[").append(s1).append("] number[").append(a.number).append("] uuid[").append(s2).append("]").toString());
            a1 = com.navdy.hud.app.framework.glympse.GlympseManager$Error.NONE;
        } else {
            logger.v("addMessage, no connection, no-op");
            a1 = com.navdy.hud.app.framework.glympse.GlympseManager$Error.NO_INTERNET;
        }
        return a1;
    }
    
    public void expireActiveTickets() {
        if (this.isSynced()) {
            logger.v("expireActiveTickets");
            this.stopTrackingETA();
            com.glympse.android.core.GArray a = this.glympse.getHistoryManager().getTickets();
            int i = a.length();
            Object a0 = a;
            int i0 = 0;
            while(i0 < i) {
                Object a1 = ((com.glympse.android.core.GArray)a0).at(i0);
                boolean b = ((com.glympse.android.api.GTicket)a1).isActive();
                {
                    if (b) {
                        logger.v(new StringBuilder().append("expireActiveTickets ticket[ ").append(((com.glympse.android.api.GTicket)a1).getId()).append("] expiryTime[").append(new java.util.Date(((com.glympse.android.api.GTicket)a1).getExpireTime())).append("]").toString());
                        ((com.glympse.android.api.GTicket)a1).expire();
                        i0 = i0 + 1;
                        continue;
                    }
                    logger.v(new StringBuilder().append("expireActiveTickets ticket[").append(((com.glympse.android.api.GTicket)a1).getId()).append("] not active").toString());
                    break;
                }
            }
        }
    }
    
    public String[] getMessages() {
        return messages;
    }
    
    public boolean isSynced() {
        boolean b = false;
        boolean b0 = this.glympseInitFailed;
        label2: {
            label0: {
                label1: {
                    if (b0) {
                        break label1;
                    }
                    if (this.glympse.getHistoryManager().isSynced()) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged a) {
        if (this.isSynced()) {
            this.setUserProfile();
        }
    }
}
