package com.navdy.hud.app.framework.notifications;

public class NotificationAnimator$OperationInfo {
    boolean collapse;
    boolean gesture;
    boolean hideNotification;
    String id;
    com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation operation;
    boolean quick;
    
    NotificationAnimator$OperationInfo(com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation a, boolean b, boolean b0, boolean b1, String s, boolean b2) {
        this.operation = a;
        this.hideNotification = b;
        this.quick = b0;
        this.gesture = b1;
        this.id = s;
        this.collapse = b2;
    }
}
