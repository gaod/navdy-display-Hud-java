package com.navdy.hud.app.framework.glance;

class GlanceTracker$1 {
    final static int[] $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp;
    
    static {
        $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp = new int[com.navdy.hud.app.framework.glance.GlanceApp.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp;
        com.navdy.hud.app.framework.glance.GlanceApp a0 = com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_CALENDAR;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.APPLE_CALENDAR.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_CALENDAR.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_MAIL.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_INBOX.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.APPLE_MAIL.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_MAIL.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException5) {
        }
    }
}
