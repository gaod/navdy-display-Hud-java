package com.navdy.hud.app.framework.notifications;

class NotificationManager$16 {
    final static int[] $SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType;
    final static int[] $SwitchMap$com$navdy$hud$app$framework$notifications$NotificationAnimator$Operation;
    final static int[] $SwitchMap$com$navdy$hud$app$framework$notifications$NotificationManager$ScrollState;
    final static int[] $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
    final static int[] $SwitchMap$com$navdy$service$library$events$ui$Screen;
    
    static {
        $SwitchMap$com$navdy$service$library$events$ui$Screen = new int[com.navdy.service.library.events.ui.Screen.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$ui$Screen;
        com.navdy.service.library.events.ui.Screen a0 = com.navdy.service.library.events.ui.Screen.SCREEN_DESTINATION_PICKER;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        $SwitchMap$com$navdy$hud$app$framework$notifications$NotificationAnimator$Operation = new int[com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$framework$notifications$NotificationAnimator$Operation;
        com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation a2 = com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation.MOVE_NEXT;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$notifications$NotificationAnimator$Operation[com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation.MOVE_PREV.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$notifications$NotificationAnimator$Operation[com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation.SELECT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$notifications$NotificationAnimator$Operation[com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation.COLLAPSE_VIEW.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$notifications$NotificationAnimator$Operation[com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation.UPDATE_INDICATOR.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$notifications$NotificationAnimator$Operation[com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation.REMOVE_NOTIFICATION.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException5) {
        }
        $SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType = new int[com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.values().length];
        int[] a3 = $SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType;
        com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType a4 = com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.SMALL_IMAGE;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType[com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.SMALL_GLANCE_MESSAGE.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException7) {
        }
        $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.values().length];
        int[] a5 = $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
        com.navdy.hud.app.manager.InputManager$CustomKeyEvent a6 = com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LEFT;
        try {
            a5[a6.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException9) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.SELECT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException10) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.POWER_BUTTON_LONG_PRESS.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException11) {
        }
        $SwitchMap$com$navdy$hud$app$framework$notifications$NotificationManager$ScrollState = new int[com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState.values().length];
        int[] a7 = $SwitchMap$com$navdy$hud$app$framework$notifications$NotificationManager$ScrollState;
        com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState a8 = com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState.LIMIT_REACHED;
        try {
            a7[a8.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException12) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$notifications$NotificationManager$ScrollState[com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState.NONE.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException13) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$notifications$NotificationManager$ScrollState[com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState.NOT_HANDLED.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException14) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$notifications$NotificationManager$ScrollState[com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState.HANDLED.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException15) {
        }
    }
}
