package com.navdy.hud.app.framework.glympse;
import com.navdy.hud.app.R;

public class GlympseNotification$$ViewInjector {
    public GlympseNotification$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.framework.glympse.GlympseNotification a0, Object a1) {
        a0.title = (android.widget.TextView)a.findRequiredView(a1, R.id.title, "field 'title'");
        a0.subtitle = (android.widget.TextView)a.findRequiredView(a1, R.id.subtitle, "field 'subtitle'");
        a0.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2)a.findRequiredView(a1, R.id.choice_layout, "field 'choiceLayout'");
        a0.notificationIcon = (com.navdy.hud.app.ui.component.image.IconColorImageView)a.findRequiredView(a1, R.id.notification_icon, "field 'notificationIcon'");
        a0.notificationUserImage = (android.widget.ImageView)a.findRequiredView(a1, R.id.notification_user_image, "field 'notificationUserImage'");
        a0.badge = (android.widget.ImageView)a.findRequiredView(a1, R.id.badge, "field 'badge'");
        a0.badgeIcon = (com.navdy.hud.app.ui.component.image.IconColorImageView)a.findRequiredView(a1, R.id.badge_icon, "field 'badgeIcon'");
    }
    
    public static void reset(com.navdy.hud.app.framework.glympse.GlympseNotification a) {
        a.title = null;
        a.subtitle = null;
        a.choiceLayout = null;
        a.notificationIcon = null;
        a.notificationUserImage = null;
        a.badge = null;
        a.badgeIcon = null;
    }
}
