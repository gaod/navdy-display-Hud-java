package com.navdy.hud.app.framework.contacts;

public class PhoneImageDownloader {
    final private static String MD5_EXTENSION = ".md5";
    final private static boolean VERBOSE = false;
    final private static com.navdy.hud.app.framework.contacts.PhoneImageDownloader sInstance;
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.squareup.otto.Bus bus;
    private java.util.HashSet contactPhotoChecked;
    private java.util.concurrent.PriorityBlockingQueue contactQueue;
    private android.content.Context context;
    private volatile String currentDriverProfileId;
    private boolean downloading;
    private Object lockObj;
    private java.util.Comparator priorityComparator;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.contacts.PhoneImageDownloader.class);
        sInstance = new com.navdy.hud.app.framework.contacts.PhoneImageDownloader();
    }
    
    private PhoneImageDownloader() {
        this.priorityComparator = (java.util.Comparator)new com.navdy.hud.app.framework.contacts.PhoneImageDownloader$1(this);
        this.context = com.navdy.hud.app.HudApplication.getAppContext();
        this.contactQueue = new java.util.concurrent.PriorityBlockingQueue(10, this.priorityComparator);
        this.lockObj = new Object();
        this.contactPhotoChecked = new java.util.HashSet();
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        this.bus.register(this);
    }
    
    static void access$000(com.navdy.hud.app.framework.contacts.PhoneImageDownloader a) {
        a.download();
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static String access$200(com.navdy.hud.app.framework.contacts.PhoneImageDownloader a, String s, com.navdy.service.library.events.photo.PhotoType a0) {
        return a.normalizedFilename(s, a0);
    }
    
    static String access$300(com.navdy.hud.app.framework.contacts.PhoneImageDownloader a) {
        return a.currentDriverProfileId;
    }
    
    static java.io.File access$400(com.navdy.hud.app.framework.contacts.PhoneImageDownloader a, com.navdy.hud.app.profile.DriverProfile a0, com.navdy.service.library.events.photo.PhotoType a1) {
        return a.getPhotoPath(a0, a1);
    }
    
    static com.squareup.otto.Bus access$500(com.navdy.hud.app.framework.contacts.PhoneImageDownloader a) {
        return a.bus;
    }
    
    static void access$600(com.navdy.hud.app.framework.contacts.PhoneImageDownloader a) {
        a.invokeDownload();
    }
    
    static android.content.Context access$700(com.navdy.hud.app.framework.contacts.PhoneImageDownloader a) {
        return a.context;
    }
    
    private void download() {
        try {
            synchronized(this.lockObj) {
                if (this.contactQueue.size() != 0) {
                    com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Info a0 = (com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Info)this.contactQueue.remove();
                    if (a0.priority == com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Priority.NORMAL.getValue()) {
                        com.navdy.hud.app.util.GenericUtil.sleep(500);
                    }
                    this.currentDriverProfileId = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getProfileName();
                    String s = this.hashFor(a0.sourceIdentifier, a0.photoType);
                    if (s != null) {
                        sLogger.v(new StringBuilder().append("checking for update file [").append(a0.fileName).append("] id[").append(a0.sourceIdentifier).append("]").toString());
                    } else {
                        sLogger.v(new StringBuilder().append("sending fresh download request file [").append(a0.fileName).append("] id[").append(a0.sourceIdentifier).append("]").toString());
                    }
                    com.navdy.service.library.events.photo.PhotoRequest a1 = new com.navdy.service.library.events.photo.PhotoRequest(a0.sourceIdentifier, s, a0.photoType, a0.displayName);
                    this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a1));
                    /*monexit(a)*/;
                } else {
                    sLogger.v("no request pending");
                    this.downloading = false;
                    /*monexit(a)*/;
                }
            }
        } catch(Throwable a6) {
            sLogger.e(a6);
            this.invokeDownload();
        }
    }
    
    final public static com.navdy.hud.app.framework.contacts.PhoneImageDownloader getInstance() {
        return sInstance;
    }
    
    private java.io.File getPhotoPath(com.navdy.hud.app.profile.DriverProfile a, com.navdy.service.library.events.photo.PhotoType a0) {
        java.io.File a1 = null;
        switch(com.navdy.hud.app.framework.contacts.PhoneImageDownloader$5.$SwitchMap$com$navdy$service$library$events$photo$PhotoType[a0.ordinal()]) {
            case 3: {
                a1 = a.getPreferencesDirectory();
                break;
            }
            case 2: {
                a1 = a.getContactsImageDir();
                break;
            }
            case 1: {
                a1 = a.getMusicImageDir();
                break;
            }
            default: {
                a1 = null;
            }
        }
        return a1;
    }
    
    private void invokeDownload() {
        this.currentDriverProfileId = null;
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.contacts.PhoneImageDownloader$2(this), 7);
    }
    
    private String normalizedFilename(String s, com.navdy.service.library.events.photo.PhotoType a) {
        return (a != com.navdy.service.library.events.photo.PhotoType.PHOTO_DRIVER_PROFILE) ? com.navdy.hud.app.util.GenericUtil.normalizeToFilename(s) : "DriverImage";
    }
    
    private void purgeQueue() {
        sLogger.i("purging queue");
        this.currentDriverProfileId = null;
        synchronized(this.lockObj) {
            this.contactQueue.clear();
            this.downloading = false;
            /*monexit(a)*/;
        }
    }
    
    private void removeImage(String s, com.navdy.service.library.events.photo.PhotoType a) {
        label2: {
            label0: {
                label1: {
                    if (s == null) {
                        break label1;
                    }
                    if (a != null) {
                        break label0;
                    }
                }
                sLogger.i("null param");
                break label2;
            }
            java.io.File a0 = this.getImagePath(s, a);
            com.navdy.hud.app.util.picasso.PicassoUtil.getInstance().invalidate(a0);
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.contacts.PhoneImageDownloader$4(this, a0, s, a), 7);
        }
    }
    
    private void storePhoto(com.navdy.service.library.events.photo.PhotoResponse a) {
        if (a.photo != null) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.contacts.PhoneImageDownloader$3(this, a), 7);
        } else {
            sLogger.v(new StringBuilder().append("photo is upto-date type[").append(a.photoType).append("] id[").append(a.identifier).append("]").toString());
            this.bus.post(new com.navdy.hud.app.framework.contacts.PhoneImageDownloader$PhotoDownloadStatus(a.identifier, a.photoType, true, true));
            this.invokeDownload();
        }
    }
    
    public void clearAllPhotoCheckEntries() {
        synchronized(this.lockObj) {
            this.contactPhotoChecked.clear();
            sLogger.v("cleared all photo check entries");
            /*monexit(a)*/;
        }
    }
    
    public void clearPhotoCheckEntry(String s, com.navdy.service.library.events.photo.PhotoType a) {
        String s0 = this.normalizedFilename(s, a);
        synchronized(this.lockObj) {
            if (this.contactPhotoChecked.remove(s0)) {
                sLogger.v(new StringBuilder().append("photo check entry removed [").append(s0).append("]").toString());
            }
            /*monexit(a0)*/;
        }
    }
    
    public java.io.File getImagePath(String s, com.navdy.service.library.events.photo.PhotoType a) {
        return this.getImagePath(s, "", a);
    }
    
    public java.io.File getImagePath(String s, String s0, com.navdy.service.library.events.photo.PhotoType a) {
        String s1 = this.normalizedFilename(s, a);
        java.io.File a0 = this.getPhotoPath(com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile(), a);
        return (a0 != null) ? new java.io.File(a0, new StringBuilder().append(s1).append(s0).toString()) : null;
    }
    
    public String hashFor(String s, com.navdy.service.library.events.photo.PhotoType a) {
        String s0 = this.normalizedFilename(s, a);
        java.io.File a0 = this.getPhotoPath(com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile(), a);
        java.io.File a1 = new java.io.File(a0, s0);
        boolean b = a1.exists();
        String s1 = null;
        label0: {
            Throwable a2 = null;
            if (!b) {
                break label0;
            }
            boolean b0 = a1.isDirectory();
            s1 = null;
            if (b0) {
                break label0;
            }
            java.io.File a3 = new java.io.File(a0, new StringBuilder().append(s0).append(".md5").toString());
            label1: if (a3.exists()) {
                try {
                    s1 = com.navdy.service.library.util.IOUtils.convertFileToString(a3.getAbsolutePath());
                } catch(Throwable a4) {
                    a2 = a4;
                    break label1;
                }
                break label0;
            } else {
                sLogger.w(new StringBuilder().append("md5 does not exist, delete file, ").append(s0).toString());
                com.navdy.service.library.util.IOUtils.deleteFile(this.context, a1.getAbsolutePath());
                s1 = null;
                break label0;
            }
            sLogger.e(new StringBuilder().append("error reading md5[").append(s0).append("]").toString(), a2);
            com.navdy.service.library.util.IOUtils.deleteFile(this.context, a1.getAbsolutePath());
            com.navdy.service.library.util.IOUtils.deleteFile(this.context, a3.getAbsolutePath());
            s1 = null;
        }
        return s1;
    }
    
    public void onConnectionStatusChange(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        if (com.navdy.hud.app.framework.contacts.PhoneImageDownloader$5.$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[a.state.ordinal()] != 0) {
            this.purgeQueue();
        }
    }
    
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged a) {
        this.purgeQueue();
    }
    
    public void onPhotoResponse(com.navdy.service.library.events.photo.PhotoResponse a) {
        sLogger.v(new StringBuilder().append("got photoResponse:").append(a.status).append(" type[").append(a.photoType).append("] id[").append(a.identifier).append("]").toString());
        if (this.currentDriverProfileId != null) {
            String s = this.normalizedFilename(a.identifier, a.photoType);
            if (a.status != com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
                if (a.status != com.navdy.service.library.events.RequestStatus.REQUEST_NOT_AVAILABLE) {
                    sLogger.v(new StringBuilder().append("photo not available type[").append(a.photoType).append("] id[").append(a.identifier).append("] error[").append(a.status).append("]").toString());
                } else {
                    this.contactPhotoChecked.add(s);
                    this.removeImage(a.identifier, a.photoType);
                    sLogger.v(new StringBuilder().append("photo not available type[").append(a.photoType).append("] id[").append(a.identifier).append("]").toString());
                }
                this.invokeDownload();
            } else {
                this.contactPhotoChecked.add(s);
                this.storePhoto(a);
            }
        } else {
            sLogger.w("currentDriverProfile is null");
            this.invokeDownload();
        }
    }
    
    public void submitDownload(String s, com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Priority a, com.navdy.service.library.events.photo.PhotoType a0, String s0) {
        Object a1 = null;
        Throwable a2 = null;
        boolean b = android.text.TextUtils.isEmpty((CharSequence)s);
        label0: {
            label2: if (b) {
                sLogger.w("invalid id passed");
            } else {
                String s1 = this.normalizedFilename(s, a0);
                synchronized(this.lockObj) {
                    com.navdy.service.library.events.photo.PhotoType a3 = com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT;
                    label1: {
                        if (a0 != a3) {
                            break label1;
                        }
                        if (!this.contactPhotoChecked.contains(s1)) {
                            break label1;
                        }
                        sLogger.v(new StringBuilder().append("contact photo request has already been processed name[").append(s1).append("]").toString());
                        /*monexit(a1)*/;
                        break label2;
                    }
                    sLogger.v(new StringBuilder().append("submitting photo request name[").append(s1).append("] type [").append(a0).append("] displayName[").append(s0).append("]").toString());
                    this.contactQueue.add(new com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Info(s1, a, a0, s, s0));
                    if (!this.downloading) {
                        this.downloading = true;
                        this.invokeDownload();
                    }
                    /*monexit(a1)*/;
                }
            }
            return;
        }
        while(true) {
            try {
                /*monexit(a1)*/;
            } catch(IllegalMonitorStateException | NullPointerException a5) {
                Throwable a6 = a5;
                a2 = a6;
                continue;
            }
            throw a2;
        }
    }
}
