package com.navdy.hud.app.framework.contacts;

class PhoneImageDownloader$1 implements java.util.Comparator {
    final com.navdy.hud.app.framework.contacts.PhoneImageDownloader this$0;
    
    PhoneImageDownloader$1(com.navdy.hud.app.framework.contacts.PhoneImageDownloader a) {
        super();
        this.this$0 = a;
    }
    
    public int compare(com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Info a, com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Info a0) {
        int i = 0;
        boolean b = a instanceof com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Info;
        label2: {
            label0: {
                label1: {
                    if (!b) {
                        break label1;
                    }
                    if (a0 instanceof com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Info) {
                        break label0;
                    }
                }
                i = 0;
                break label2;
            }
            i = a.priority - a0.priority;
        }
        return i;
    }
    
    public int compare(Object a, Object a0) {
        return this.compare((com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Info)a, (com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Info)a0);
    }
}
