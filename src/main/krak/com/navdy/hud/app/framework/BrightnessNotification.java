package com.navdy.hud.app.framework;
import com.navdy.hud.app.R;

public class BrightnessNotification extends android.database.ContentObserver implements com.navdy.hud.app.framework.notifications.INotification, android.content.SharedPreferences$OnSharedPreferenceChangeListener {
    final private static int DEFAULT_AUTO_BRIGHTNESS_ADJUSTMENT;
    final private static int DEFAULT_BRIGHTNESS_INT;
    final private static long DETENT_TIMEOUT = 250L;
    final private static android.os.Handler HANDLER;
    final private static int INITIAL_CHANGING_STEP = 1;
    final private static int MAX_BRIGHTNESS = 255;
    final private static int MAX_STEP = 16;
    final private static int NOTIFICATION_TIMEOUT = 10000;
    final private static android.net.Uri SCREEN_BRIGHTNESS_SETTING_URI;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private boolean AUTO_BRIGHTNESS_ADJ_ENABLED;
    private String autoSuffix;
    private int changingStep;
    private com.navdy.hud.app.ui.component.ChoiceLayout2 choiceLayout;
    private android.view.ViewGroup container;
    private com.navdy.hud.app.framework.notifications.INotificationController controller;
    private int currentAdjustmentValue;
    private int currentValue;
    private boolean isAutoBrightnessOn;
    private boolean isLastChangeIncreasing;
    private long lastChangeActionTime;
    private int previousAdjustmentValue;
    private int previousBrightnessValue;
    private com.navdy.hud.app.view.Gauge progressBar;
    private android.content.res.Resources resources;
    private android.content.SharedPreferences sharedPreferences;
    private android.content.SharedPreferences$Editor sharedPreferencesEditor;
    private boolean showAutoBrightnessAdjustmentUI;
    private android.widget.TextView textIndicator;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.BrightnessNotification.class);
        DEFAULT_BRIGHTNESS_INT = Integer.parseInt("128");
        DEFAULT_AUTO_BRIGHTNESS_ADJUSTMENT = Integer.parseInt("0");
        HANDLER = new android.os.Handler();
        SCREEN_BRIGHTNESS_SETTING_URI = android.provider.Settings$System.getUriFor("screen_brightness");
    }
    
    public BrightnessNotification() {
        super(new android.os.Handler());
        this.autoSuffix = "";
        this.changingStep = 1;
        this.lastChangeActionTime = -251L;
        this.isLastChangeIncreasing = false;
        this.showAutoBrightnessAdjustmentUI = false;
        this.AUTO_BRIGHTNESS_ADJ_ENABLED = true;
        this.sharedPreferences = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences();
        this.sharedPreferencesEditor = this.sharedPreferences.edit();
        this.isAutoBrightnessOn = this.getCurrentIsAutoBrightnessOn();
        this.showAutoBrightnessAdjustmentUI = this.isAutoBrightnessOn;
        this.resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        this.autoSuffix = this.resources.getString(R.string.brightness_auto_suffix);
    }
    
    static void access$000(com.navdy.hud.app.framework.BrightnessNotification a) {
        a.close();
    }
    
    private void close() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.getId());
        if (this.isAutoBrightnessOn) {
            com.navdy.hud.app.analytics.AnalyticsSupport.recordAutobrightnessAdjustmentChanged(this.previousBrightnessValue, this.previousAdjustmentValue, this.getCurrentAutoBrigthnessAdjustment());
        } else {
            com.navdy.hud.app.analytics.AnalyticsSupport.recordBrightnessChanged(this.previousBrightnessValue, this.getCurrentBrightness());
        }
    }
    
    private void detentChangingStep(boolean b) {
        long j = android.os.SystemClock.elapsedRealtime();
        boolean b0 = this.isLastChangeIncreasing;
        label2: {
            label0: {
                label1: {
                    if (b != b0) {
                        break label1;
                    }
                    if (this.lastChangeActionTime + 250L >= j) {
                        break label0;
                    }
                }
                this.changingStep = 1;
                break label2;
            }
            this.changingStep = this.changingStep << 1;
            if (this.changingStep > 16) {
                this.changingStep = 16;
            }
        }
        this.isLastChangeIncreasing = b;
        this.lastChangeActionTime = j;
    }
    
    private int getCurrentAutoBrigthnessAdjustment() {
        int i = 0;
        try {
            i = Integer.parseInt(this.sharedPreferences.getString("screen.auto_brightness_adj", "0"));
        } catch(NumberFormatException a) {
            sLogger.e("Cannot parse auto brightness adjustment from the shared preferences", (Throwable)a);
            i = DEFAULT_AUTO_BRIGHTNESS_ADJUSTMENT;
        }
        return i;
    }
    
    private int getCurrentBrightness() {
        int i = 0;
        boolean b = this.isAutoBrightnessOn;
        label1: {
            android.provider.Settings$SettingNotFoundException a = null;
            label2: {
                NumberFormatException a0 = null;
                label0: if (b) {
                    try {
                        i = android.provider.Settings$System.getInt(com.navdy.hud.app.HudApplication.getAppContext().getContentResolver(), "screen_brightness");
                        break label1;
                    } catch(android.provider.Settings$SettingNotFoundException a1) {
                        a = a1;
                        break label2;
                    }
                } else {
                    try {
                        i = Integer.parseInt(this.sharedPreferences.getString("screen.brightness", "128"));
                    } catch(NumberFormatException a2) {
                        a0 = a2;
                        break label0;
                    }
                    break label1;
                }
                sLogger.e("Cannot parse brightness from the shared preferences", (Throwable)a0);
                i = DEFAULT_BRIGHTNESS_INT;
                break label1;
            }
            sLogger.e("Settings not found exception ", (Throwable)a);
            i = DEFAULT_BRIGHTNESS_INT;
        }
        return i;
    }
    
    private boolean getCurrentIsAutoBrightnessOn() {
        return "true".equals(this.sharedPreferences.getString("screen.auto_brightness", ""));
    }
    
    private void showFixedChoices(android.view.ViewGroup a) {
        this.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2)a.findViewById(R.id.choiceLayout);
        java.util.ArrayList a0 = new java.util.ArrayList(3);
        android.content.res.Resources a1 = a.getResources();
        String s = a1.getString(R.string.call_dismiss);
        int i = a1.getColor(R.color.glance_ok_blue);
        ((java.util.List)a0).add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(2, R.drawable.icon_glances_ok, i, R.drawable.icon_glances_ok, -16777216, s, i));
        this.choiceLayout.setChoices((java.util.List)a0, 1, (com.navdy.hud.app.ui.component.ChoiceLayout2$IListener)new com.navdy.hud.app.framework.BrightnessNotification$1(this));
        this.choiceLayout.setVisibility(0);
    }
    
    public static void showNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().addNotification((com.navdy.hud.app.framework.notifications.INotification)new com.navdy.hud.app.framework.BrightnessNotification());
    }
    
    private void updateBrightness(int i) {
        if (i != 0) {
            if (this.showAutoBrightnessAdjustmentUI) {
                int i0 = this.currentAdjustmentValue + i;
                if (i0 < -64) {
                    i0 = -64;
                }
                if (i0 > 64) {
                    i0 = 64;
                }
                this.sharedPreferencesEditor.putString("screen.auto_brightness_adj", String.valueOf(i0));
                this.sharedPreferencesEditor.apply();
            } else {
                int i1 = this.currentValue + i;
                if (i1 < 0) {
                    i1 = 0;
                }
                if (i1 > 255) {
                    i1 = 255;
                }
                this.sharedPreferencesEditor.putString("screen.brightness", String.valueOf(i1));
                this.sharedPreferencesEditor.apply();
            }
        }
    }
    
    public boolean canAddToStackIfCurrentExists() {
        return false;
    }
    
    public boolean expandNotification() {
        return false;
    }
    
    public int getColor() {
        return 0;
    }
    
    public android.view.View getExpandedView(android.content.Context a, Object a0) {
        return null;
    }
    
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    public String getId() {
        return "navdy#brightness#notif";
    }
    
    public int getTimeout() {
        return 10000;
    }
    
    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.BRIGHTNESS;
    }
    
    public android.view.View getView(android.content.Context a) {
        if (this.container == null) {
            if (this.showAutoBrightnessAdjustmentUI) {
                this.container = (android.view.ViewGroup)android.view.LayoutInflater.from(a).inflate(R.layout.notification_auto_brightness, (android.view.ViewGroup)null);
            } else {
                this.container = (android.view.ViewGroup)android.view.LayoutInflater.from(a).inflate(R.layout.notification_brightness, (android.view.ViewGroup)null);
            }
            this.textIndicator = (android.widget.TextView)this.container.findViewById(R.id.subTitle);
            this.progressBar = (com.navdy.hud.app.view.Gauge)this.container.findViewById(R.id.circle_progress);
            this.progressBar.setAnimated(false);
            this.showFixedChoices(this.container);
        }
        return this.container;
    }
    
    public android.animation.AnimatorSet getViewSwitchAnimation(boolean b) {
        return null;
    }
    
    public boolean isAlive() {
        return false;
    }
    
    public boolean isPurgeable() {
        return false;
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    public void onChange(boolean b, android.net.Uri a) {
        if (!b && SCREEN_BRIGHTNESS_SETTING_URI.equals(a)) {
            this.updateState();
        }
    }
    
    public void onClick() {
        this.close();
    }
    
    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode a) {
    }
    
    public void onExpandedNotificationSwitched() {
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        if (this.controller != null) {
            if (a != com.navdy.hud.app.manager.InputManager$CustomKeyEvent.SELECT) {
                this.controller.resetTimeout();
            }
            switch(com.navdy.hud.app.framework.BrightnessNotification$2.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                case 3: {
                    sLogger.v("Increasing brightness");
                    this.detentChangingStep(true);
                    this.updateBrightness(this.changingStep);
                    b = true;
                    break;
                }
                case 2: {
                    sLogger.v("Decreasing brightness");
                    this.detentChangingStep(false);
                    this.updateBrightness(-this.changingStep);
                    b = true;
                    break;
                }
                case 1: {
                    if (this.choiceLayout == null) {
                        this.close();
                    } else {
                        this.choiceLayout.executeSelectedItem();
                    }
                    b = true;
                    break;
                }
                default: {
                    b = false;
                }
            }
        } else {
            b = false;
        }
        return b;
    }
    
    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode a) {
    }
    
    public void onSharedPreferenceChanged(android.content.SharedPreferences a, String s) {
        boolean b = s.equals("screen.brightness");
        label2: {
            label0: {
                label1: {
                    if (b) {
                        break label1;
                    }
                    if (!s.equals("screen.auto_brightness_adj")) {
                        break label0;
                    }
                }
                this.updateState();
                break label2;
            }
            if (s.equals("screen.auto_brightness")) {
                boolean b0 = "true".equals(s);
                if (this.isAutoBrightnessOn != b0) {
                    this.isAutoBrightnessOn = b0;
                    this.updateState();
                }
            }
        }
    }
    
    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController a) {
        com.navdy.hud.app.HudApplication.getAppContext().getContentResolver().registerContentObserver(SCREEN_BRIGHTNESS_SETTING_URI, false, (android.database.ContentObserver)this);
        this.controller = a;
        this.sharedPreferences.registerOnSharedPreferenceChangeListener((android.content.SharedPreferences$OnSharedPreferenceChangeListener)this);
        this.previousBrightnessValue = this.getCurrentBrightness();
        this.previousAdjustmentValue = this.getCurrentAutoBrigthnessAdjustment();
        this.updateState();
    }
    
    public void onStop() {
        com.navdy.hud.app.HudApplication.getAppContext().getContentResolver().unregisterContentObserver((android.database.ContentObserver)this);
        this.sharedPreferences.unregisterOnSharedPreferenceChangeListener((android.content.SharedPreferences$OnSharedPreferenceChangeListener)this);
        this.controller = null;
    }
    
    public void onTrackHand(float f) {
    }
    
    public void onUpdate() {
        if (this.controller != null) {
            this.controller.resetTimeout();
        }
        this.updateState();
    }
    
    public boolean supportScroll() {
        return false;
    }
    
    public void updateState() {
        if (this.controller != null) {
            this.currentValue = this.getCurrentBrightness();
            sLogger.v(new StringBuilder().append("Current brightness :").append(this.currentValue).toString());
            if (this.showAutoBrightnessAdjustmentUI) {
                this.currentAdjustmentValue = this.getCurrentAutoBrigthnessAdjustment();
                this.progressBar.setValue(this.currentAdjustmentValue + 64);
                if (this.currentValue < 255) {
                    if (this.currentValue > 0) {
                        this.textIndicator.setText((CharSequence)null);
                    } else {
                        this.textIndicator.setText(R.string.minimum);
                    }
                } else {
                    this.textIndicator.setText(R.string.max);
                }
            } else {
                android.widget.TextView a = this.textIndicator;
                android.content.res.Resources a0 = this.resources;
                int i = this.currentValue;
                String s = (this.isAutoBrightnessOn) ? this.autoSuffix : "";
                Object[] a1 = new Object[3];
                a1[0] = Integer.valueOf(i);
                a1[1] = Integer.valueOf(255);
                a1[2] = s;
                a.setText((CharSequence)a0.getString(R.string.brightness_notification_subtitle, a1));
                this.progressBar.setValue(this.currentValue);
            }
        } else {
            sLogger.v("brightness notif offscreen");
        }
    }
}
