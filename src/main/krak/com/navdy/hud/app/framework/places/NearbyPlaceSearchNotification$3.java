package com.navdy.hud.app.framework.places;

class NearbyPlaceSearchNotification$3 implements com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener {
    final com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification this$0;
    
    NearbyPlaceSearchNotification$3(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification a) {
        super();
        this.this$0 = a;
    }
    
    public void onCompleted(java.util.List a) {
        if (com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$000(this.this$0) != null) {
            if (com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$500(this.this$0) != com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState.ERROR) {
                com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$400().v("performing offline search, returned places: ");
                java.util.Iterator a0 = a.iterator();
                Object a1 = a;
                Object a2 = a0;
                while(((java.util.Iterator)a2).hasNext()) {
                    com.here.android.mpa.search.Place a3 = (com.here.android.mpa.search.Place)((java.util.Iterator)a2).next();
                    com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$400().v(a3.getName());
                }
                java.util.ArrayList a4 = new java.util.ArrayList();
                Object a5 = ((java.util.List)a1).iterator();
                while(((java.util.Iterator)a5).hasNext()) {
                    com.navdy.service.library.events.location.LatLong a6 = null;
                    String s = null;
                    com.here.android.mpa.search.Place a7 = (com.here.android.mpa.search.Place)((java.util.Iterator)a5).next();
                    com.here.android.mpa.common.GeoCoordinate a8 = a7.getLocation().getCoordinate();
                    com.navdy.service.library.events.location.LatLong a9 = new com.navdy.service.library.events.location.LatLong(Double.valueOf(a8.getLatitude()), Double.valueOf(a8.getLongitude()));
                    java.util.List a10 = a7.getLocation().getAccessPoints();
                    if (a10.size() <= 0) {
                        a6 = a9;
                    } else {
                        com.here.android.mpa.common.GeoCoordinate a11 = ((com.here.android.mpa.search.NavigationPosition)a10.get(0)).getCoordinate();
                        a6 = new com.navdy.service.library.events.location.LatLong(Double.valueOf(a11.getLatitude()), Double.valueOf(a11.getLongitude()));
                    }
                    com.here.android.mpa.search.Address a12 = a7.getLocation().getAddress();
                    String s0 = a12.getHouseNumber();
                    label1: {
                        label0: {
                            if (s0 == null) {
                                break label0;
                            }
                            if (a12.getStreet() == null) {
                                break label0;
                            }
                            s = new StringBuilder().append(a12.getHouseNumber()).append(" ").append(a12.getStreet()).toString();
                            break label1;
                        }
                        s = a12.toString();
                    }
                    ((java.util.List)a4).add(new com.navdy.service.library.events.destination.Destination$Builder().navigation_position(a6).display_position(a9).full_address(a7.getLocation().getAddress().toString()).destination_title(a7.getName()).destination_subtitle(s).favorite_type(com.navdy.service.library.events.destination.Destination$FavoriteType.FAVORITE_NONE).identifier(java.util.UUID.randomUUID().toString()).suggestion_type(com.navdy.service.library.events.destination.Destination$SuggestionType.SUGGESTION_NONE).is_recommendation(Boolean.valueOf(false)).last_navigated_to(Long.valueOf(0L)).place_type(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$300(this.this$0)).build());
                }
                com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$700(this.this$0).post((Runnable)new com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$3$1(this, (java.util.List)a4));
            } else {
                com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$400().w("received response after place type search has already failed, no-op");
            }
        }
    }
    
    public void onError(com.navdy.hud.app.maps.here.HerePlacesManager$Error a) {
        if (com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$000(this.this$0) != null) {
            com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$400().w(new StringBuilder().append("error while performing offline search: ").append(a.name()).toString());
            com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$700(this.this$0).post((Runnable)new com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$3$2(this));
        }
    }
}
