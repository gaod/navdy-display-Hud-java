package com.navdy.hud.app.framework.voice;
import com.navdy.hud.app.R;

class VoiceAssistNotification$1 implements Runnable {
    final com.navdy.hud.app.framework.voice.VoiceAssistNotification this$0;
    
    VoiceAssistNotification$1(com.navdy.hud.app.framework.voice.VoiceAssistNotification a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        if (com.navdy.hud.app.framework.voice.VoiceAssistNotification.access$000(this.this$0) != null) {
            com.navdy.hud.app.framework.voice.VoiceAssistNotification.access$100().d("Siri request did not get any response and timed out");
            com.navdy.hud.app.framework.voice.VoiceAssistNotification.access$200(this.this$0).setVisibility(4);
            com.navdy.hud.app.framework.voice.VoiceAssistNotification.access$200(this.this$0).stop();
            com.navdy.hud.app.framework.voice.VoiceAssistNotification.access$400(this.this$0).setText((CharSequence)com.navdy.hud.app.framework.voice.VoiceAssistNotification.access$300());
            com.navdy.hud.app.framework.voice.VoiceAssistNotification.access$500(this.this$0).setImageResource(R.drawable.icon_siri);
        }
    }
}
