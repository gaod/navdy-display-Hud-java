package com.navdy.hud.app.framework.notifications;

final class NotificationAnimator$3 implements Runnable {
    final android.view.View val$indicator;
    
    NotificationAnimator$3(android.view.View a) {
        super();
        this.val$indicator = a;
    }
    
    public void run() {
        this.val$indicator.setVisibility(8);
    }
}
