package com.navdy.hud.app.framework.contacts;

class FavoriteContactsManager$2 implements Runnable {
    final com.navdy.hud.app.framework.contacts.FavoriteContactsManager this$0;
    final com.navdy.service.library.events.contacts.FavoriteContactsResponse val$response;
    
    FavoriteContactsManager$2(com.navdy.hud.app.framework.contacts.FavoriteContactsManager a, com.navdy.service.library.events.contacts.FavoriteContactsResponse a0) {
        super();
        this.this$0 = a;
        this.val$response = a0;
    }
    
    public void run() {
        try {
            if (this.val$response.status != com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
                if (this.val$response.status != com.navdy.service.library.events.RequestStatus.REQUEST_NOT_AVAILABLE) {
                    com.navdy.hud.app.framework.contacts.FavoriteContactsManager.access$300().e(new StringBuilder().append("sent fav-contact response error:").append(this.val$response.status).toString());
                } else {
                    com.navdy.hud.app.framework.contacts.FavoriteContactsManager.access$202(this.this$0, false);
                }
            } else {
                com.navdy.hud.app.framework.contacts.FavoriteContactsManager.access$202(this.this$0, true);
                if (this.val$response.contacts == null) {
                    com.navdy.hud.app.framework.contacts.FavoriteContactsManager.access$300().w("fav-contact list returned is null");
                } else {
                    com.navdy.hud.app.framework.contacts.FavoriteContactsManager.access$300().v(new StringBuilder().append("onFavoriteContactResponse size[").append(this.val$response.contacts.size()).append("]").toString());
                    java.util.HashSet a = new java.util.HashSet();
                    java.util.ArrayList a0 = new java.util.ArrayList();
                    Object a1 = this.val$response.contacts.iterator();
                    while(((java.util.Iterator)a1).hasNext()) {
                        com.navdy.service.library.events.contacts.Contact a2 = (com.navdy.service.library.events.contacts.Contact)((java.util.Iterator)a1).next();
                        {
                            if (android.text.TextUtils.isEmpty((CharSequence)a2.number)) {
                                com.navdy.hud.app.framework.contacts.FavoriteContactsManager.access$300().v("null number");
                                continue;
                            }
                            if (a.contains(a2.number)) {
                                com.navdy.hud.app.framework.contacts.FavoriteContactsManager.access$300().v(new StringBuilder().append("number already seen:").append(a2.number).toString());
                                continue;
                            }
                            com.navdy.hud.app.framework.contacts.NumberType a3 = com.navdy.hud.app.framework.contacts.ContactUtil.getNumberType(a2.numberType);
                            String s = a2.name;
                            if (!android.text.TextUtils.isEmpty((CharSequence)a2.name) && !com.navdy.hud.app.framework.contacts.ContactUtil.isDisplayNameValid(a2.name, a2.number, com.navdy.hud.app.framework.contacts.ContactUtil.getPhoneNumber(a2.number))) {
                                s = null;
                            }
                            int i = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance().getContactImageIndex(a2.number);
                            ((java.util.List)a0).add(new com.navdy.hud.app.framework.contacts.Contact(s, a2.number, a3, i, 0L));
                            a.add(a2.number);
                            if (((java.util.List)a0).size() != 30) {
                                continue;
                            }
                            com.navdy.hud.app.framework.contacts.FavoriteContactsManager.access$300().v("exceeded max size");
                            break;
                        }
                    }
                    com.navdy.hud.app.framework.contacts.FavoriteContactsManager.access$300().v(new StringBuilder().append("favorite contact size[").append(((java.util.List)a0).size()).append("]").toString());
                    com.navdy.hud.app.storage.db.helper.FavoriteContactsPersistenceHelper.storeFavoriteContacts(com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getProfileName(), (java.util.List)a0, com.navdy.hud.app.framework.contacts.FavoriteContactsManager.access$400(this.this$0));
                }
            }
        } catch(Throwable a4) {
            com.navdy.hud.app.framework.contacts.FavoriteContactsManager.access$300().e(a4);
        }
    }
}
