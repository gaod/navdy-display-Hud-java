package com.navdy.hud.app.framework.places;

class NearbyPlaceSearchNotification$4 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification this$0;
    
    NearbyPlaceSearchNotification$4(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        if (com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$1000(this.this$0) == null) {
            com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$400().v("abandon loading animation");
        } else {
            com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$1000(this.this$0).setStartDelay(33L);
            com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.access$1000(this.this$0).start();
        }
    }
}
