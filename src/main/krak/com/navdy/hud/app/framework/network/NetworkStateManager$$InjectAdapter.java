package com.navdy.hud.app.framework.network;

final public class NetworkStateManager$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding bus;
    
    public NetworkStateManager$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.framework.network.NetworkStateManager", false, com.navdy.hud.app.framework.network.NetworkStateManager.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.framework.network.NetworkStateManager.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.bus);
    }
    
    public void injectMembers(com.navdy.hud.app.framework.network.NetworkStateManager a) {
        a.bus = (com.squareup.otto.Bus)this.bus.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.framework.network.NetworkStateManager)a);
    }
}
