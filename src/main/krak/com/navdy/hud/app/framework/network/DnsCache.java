package com.navdy.hud.app.framework.network;

class DnsCache {
    private java.util.HashMap IPtoHostnameMap;
    
    DnsCache() {
        this.IPtoHostnameMap = new java.util.HashMap();
    }
    
    public void addEntry(String s, String s0) {
        synchronized(this) {
            this.IPtoHostnameMap.put(s, s0);
        }
        /*monexit(this)*/;
    }
    
    public void clear(String s, String s0) {
        synchronized(this) {
            this.IPtoHostnameMap.clear();
        }
        /*monexit(this)*/;
    }
    
    public String getHostnamefromIP(String s) {
        String s0 = null;
        synchronized(this) {
            s0 = (String)this.IPtoHostnameMap.get(s);
        }
        /*monexit(this)*/;
        return s0;
    }
}
