package com.navdy.hud.app.settings;

class AdaptiveBrightnessControl$3 implements Runnable {
    final com.navdy.hud.app.settings.AdaptiveBrightnessControl this$0;
    final int val$val;
    
    AdaptiveBrightnessControl$3(com.navdy.hud.app.settings.AdaptiveBrightnessControl a, int i) {
        super();
        this.this$0 = a;
        this.val$val = i;
    }
    
    public void run() {
        float f = (float)this.val$val / 255f;
        android.util.Log.d("TEST", new StringBuilder().append("Normalized : ").append(f).toString());
        android.provider.Settings$System.putFloat(com.navdy.hud.app.settings.AdaptiveBrightnessControl.access$000(this.this$0).getContentResolver(), "screen_auto_brightness_adj", f);
    }
}
