package com.navdy.hud.app.settings;

class AdaptiveBrightnessControl$1 implements Runnable {
    final com.navdy.hud.app.settings.AdaptiveBrightnessControl this$0;
    final int val$val;
    
    AdaptiveBrightnessControl$1(com.navdy.hud.app.settings.AdaptiveBrightnessControl a, int i) {
        super();
        this.this$0 = a;
        this.val$val = i;
    }
    
    public void run() {
        android.provider.Settings$System.putInt(com.navdy.hud.app.settings.AdaptiveBrightnessControl.access$000(this.this$0).getContentResolver(), "screen_brightness", this.val$val);
    }
}
