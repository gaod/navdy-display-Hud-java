package com.navdy.hud.app.settings;

class BrightnessControl$2 implements Runnable {
    final com.navdy.hud.app.settings.BrightnessControl this$0;
    final boolean val$autoBrightness;
    
    BrightnessControl$2(com.navdy.hud.app.settings.BrightnessControl a, boolean b) {
        super();
        this.this$0 = a;
        this.val$autoBrightness = b;
    }
    
    public void run() {
        android.provider.Settings$System.putInt(com.navdy.hud.app.settings.BrightnessControl.access$000(this.this$0).getContentResolver(), "screen_brightness_mode", (this.val$autoBrightness) ? 1 : 0);
        com.navdy.hud.app.settings.BrightnessControl.access$300(this.this$0).post((Runnable)new com.navdy.hud.app.settings.BrightnessControl$2$1(this));
    }
}
