package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class MPGGaugePresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    final private static int GAUGE_UPDATE_INTERVAL = 1000;
    final private static long MPG_AVERAGE_TIME_WINDOW;
    private android.content.Context mContext;
    @InjectView(R.id.txt_unit)
    android.widget.TextView mFuelConsumptionUnit;
    @InjectView(R.id.txt_value)
    android.widget.TextView mFuelConsumptionValue;
    private android.os.Handler mHandler;
    private String mKmplLabel;
    private double mMpg;
    private String mMpgLabel;
    private Runnable mRunnable;
    
    static {
        MPG_AVERAGE_TIME_WINDOW = java.util.concurrent.TimeUnit.MINUTES.toMillis(5L);
    }
    
    public MPGGaugePresenter(android.content.Context a) {
        this.mContext = a;
        android.content.res.Resources a0 = a.getResources();
        this.mMpgLabel = a0.getString(R.string.mpg);
        this.mKmplLabel = a0.getString(R.string.kmpl);
        this.mHandler = new android.os.Handler();
        this.mRunnable = (Runnable)new com.navdy.hud.app.view.MPGGaugePresenter$1(this);
    }
    
    static Runnable access$000(com.navdy.hud.app.view.MPGGaugePresenter a) {
        return a.mRunnable;
    }
    
    static android.os.Handler access$100(com.navdy.hud.app.view.MPGGaugePresenter a) {
        return a.mHandler;
    }
    
    public android.graphics.drawable.Drawable getDrawable() {
        return null;
    }
    
    public String getWidgetIdentifier() {
        return "MPG_AVG_WIDGET";
    }
    
    public String getWidgetName() {
        String s = null;
        com.navdy.hud.app.manager.SpeedManager$SpeedUnit a = com.navdy.hud.app.manager.SpeedManager.getInstance().getSpeedUnit();
        switch(com.navdy.hud.app.view.MPGGaugePresenter$2.$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[a.ordinal()]) {
            case 2: {
                s = this.mMpgLabel;
                break;
            }
            case 1: {
                s = this.mKmplLabel;
                break;
            }
            default: {
                s = null;
            }
        }
        return s;
    }
    
    public void setCurrentMPG(double d) {
        this.mMpg = d;
        this.reDraw();
    }
    
    public void setView(com.navdy.hud.app.view.DashboardWidgetView a, android.os.Bundle a0) {
        if (a == null) {
            this.mHandler.removeCallbacks(this.mRunnable);
        } else {
            a.setContentView(R.layout.fuel_consumption_gauge);
            butterknife.ButterKnife.inject(this, (android.view.View)a);
            this.mHandler.removeCallbacks(this.mRunnable);
            this.mHandler.post(this.mRunnable);
        }
        super.setView(a, a0);
    }
    
    protected void updateGauge() {
        if (this.mWidgetView != null) {
            long j = 0L;
            com.navdy.hud.app.manager.SpeedManager$SpeedUnit a = com.navdy.hud.app.manager.SpeedManager.getInstance().getSpeedUnit();
            switch(com.navdy.hud.app.view.MPGGaugePresenter$2.$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[a.ordinal()]) {
                case 2: {
                    j = Math.round(com.navdy.hud.app.util.ConversionUtil.convertLpHundredKmToMPG(this.mMpg));
                    this.mFuelConsumptionUnit.setText((CharSequence)this.mMpgLabel);
                    break;
                }
                case 1: {
                    j = Math.round(com.navdy.hud.app.util.ConversionUtil.convertLpHundredKmToKMPL(this.mMpg));
                    this.mFuelConsumptionUnit.setText((CharSequence)this.mKmplLabel);
                    break;
                }
                default: {
                    j = 0L;
                }
            }
            if (j <= 0L) {
                this.mFuelConsumptionValue.setText((CharSequence)"- -");
            } else {
                this.mFuelConsumptionValue.setText((CharSequence)Long.toString(j));
            }
        }
    }
}
