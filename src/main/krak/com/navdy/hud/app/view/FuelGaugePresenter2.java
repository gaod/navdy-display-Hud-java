package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class FuelGaugePresenter2 extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    final public static int LOW_FUEL_WARNING_LIMIT = 25;
    final public static int VERY_LOW_FUEL_WARNING_LIMIT = 13;
    com.navdy.hud.app.view.drawable.FuelGaugeDrawable2 fuelGaugeDrawable2;
    private String fuelGaugeName;
    @InjectView(R.id.fuel_tank_side_indicator_left)
    android.widget.ImageView fuelTankIndicatorLeft;
    @InjectView(R.id.fuel_tank_side_indicator_right)
    android.widget.ImageView fuelTankIndicatorRight;
    @InjectView(R.id.fuel_type_icon)
    android.widget.ImageView fuelTypeIndicator;
    @InjectView(R.id.low_fuel_indicator_left)
    android.widget.ImageView lowFuelIndicatorLeft;
    @InjectView(R.id.low_fuel_indicator_right)
    android.widget.ImageView lowFuelIndicatorRight;
    private android.content.Context mContext;
    private int mFuelLevel;
    private int mFuelRange;
    private String mFuelRangeUnit;
    private boolean mLeftOriented;
    private com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide mTanksSide;
    @InjectView(R.id.txt_value)
    android.widget.TextView rangeText;
    @InjectView(R.id.txt_unit)
    android.widget.TextView rangeUnitText;
    
    public FuelGaugePresenter2(android.content.Context a) {
        this.mTanksSide = com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide.UNKNOWN;
        this.mFuelRangeUnit = "";
        this.mLeftOriented = true;
        this.mContext = a;
        this.fuelGaugeDrawable2 = new com.navdy.hud.app.view.drawable.FuelGaugeDrawable2(a, R.array.smart_dash_fuel_gauge2_state_colors);
        this.fuelGaugeDrawable2.setMinValue(0.0f);
        this.fuelGaugeDrawable2.setMaxGaugeValue(100f);
        this.fuelGaugeName = a.getResources().getString(R.string.widget_fuel);
    }
    
    public android.graphics.drawable.Drawable getDrawable() {
        return this.fuelGaugeDrawable2;
    }
    
    public String getWidgetIdentifier() {
        return "FUEL_GAUGE_ID";
    }
    
    public String getWidgetName() {
        return this.fuelGaugeName;
    }
    
    public void setFuelLevel(int i) {
        this.mFuelLevel = i;
        this.reDraw();
    }
    
    public void setFuelRange(int i) {
        this.mFuelRange = i;
    }
    
    public void setFuelRangeUnit(String s) {
        this.mFuelRangeUnit = s;
    }
    
    public void setFuelTankSide(com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide a) {
        this.mTanksSide = a;
    }
    
    public void setView(com.navdy.hud.app.view.DashboardWidgetView a, android.os.Bundle a0) {
        int i = 0;
        if (a0 == null) {
            i = R.layout.fuel_gauge_left;
        } else {
            switch(a0.getInt("EXTRA_GRAVITY")) {
                case 2: {
                    this.mLeftOriented = false;
                    i = R.layout.fuel_gauge_right;
                    break;
                }
                case 0: {
                    this.mLeftOriented = true;
                    i = R.layout.fuel_gauge_left;
                    break;
                }
                default: {
                    i = R.layout.fuel_gauge_left;
                    break;
                }
                case 1: {
                    i = R.layout.fuel_gauge_left;
                }
            }
        }
        if (a != null) {
            a.setContentView(i);
            butterknife.ButterKnife.inject(this, (android.view.View)a);
        }
        super.setView(a, a0);
        this.reDraw();
    }
    
    protected void updateGauge() {
        if (this.mWidgetView != null) {
            boolean b = this.mFuelLevel <= 25;
            boolean b0 = this.mFuelLevel <= 13;
            this.fuelGaugeDrawable2.setGaugeValue((float)this.mFuelLevel);
            this.fuelGaugeDrawable2.setLeftOriented(this.mLeftOriented);
            this.fuelGaugeDrawable2.setState(b ? b0 ? 2 : 1 : 0);
            this.lowFuelIndicatorLeft.setVisibility(8);
            this.lowFuelIndicatorRight.setVisibility(8);
            switch(com.navdy.hud.app.view.FuelGaugePresenter2$1.$SwitchMap$com$navdy$hud$app$view$FuelGaugePresenter2$FuelTankSide[this.mTanksSide.ordinal()]) {
                case 3: {
                    this.fuelTankIndicatorLeft.setVisibility(8);
                    this.fuelTankIndicatorRight.setVisibility(0);
                    if (!b0) {
                        break;
                    }
                    this.lowFuelIndicatorLeft.setVisibility(0);
                    break;
                }
                case 2: {
                    this.fuelTankIndicatorLeft.setVisibility(0);
                    this.fuelTankIndicatorRight.setVisibility(8);
                    if (!b0) {
                        break;
                    }
                    this.lowFuelIndicatorRight.setVisibility(0);
                    break;
                }
                case 1: {
                    this.fuelTankIndicatorLeft.setVisibility(8);
                    this.fuelTankIndicatorRight.setVisibility(8);
                    if (!b0) {
                        break;
                    }
                    this.lowFuelIndicatorLeft.setVisibility(0);
                    break;
                }
            }
            if (this.mFuelRange <= 0) {
                this.rangeText.setText((CharSequence)"");
            } else {
                this.rangeText.setText((CharSequence)Integer.toString(this.mFuelRange));
            }
            this.rangeUnitText.setText((CharSequence)this.mFuelRangeUnit);
        }
    }
}
