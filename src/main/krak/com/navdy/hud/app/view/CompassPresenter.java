package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class CompassPresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter implements com.navdy.hud.app.view.SerialValueAnimator$SerialValueAnimatorAdapter {
    private com.navdy.hud.app.view.SerialValueAnimator animator;
    private String compassGaugeName;
    com.navdy.hud.app.view.CompassDrawable mCompassDrawable;
    float mHeadingAngle;
    
    public CompassPresenter(android.content.Context a) {
        this.mCompassDrawable = new com.navdy.hud.app.view.CompassDrawable(a);
        this.mCompassDrawable.setMaxGaugeValue(360f);
        this.mCompassDrawable.setMinValue(0.0f);
        this.animator = new com.navdy.hud.app.view.SerialValueAnimator((com.navdy.hud.app.view.SerialValueAnimator$SerialValueAnimatorAdapter)this, 50);
        this.compassGaugeName = a.getResources().getString(R.string.widget_compass);
    }
    
    public void animationComplete(float f) {
    }
    
    public android.graphics.drawable.Drawable getDrawable() {
        return this.mCompassDrawable;
    }
    
    public float getValue() {
        return this.mHeadingAngle;
    }
    
    public String getWidgetIdentifier() {
        return "COMPASS_WIDGET";
    }
    
    public String getWidgetName() {
        return this.compassGaugeName;
    }
    
    public void setHeadingAngle(double d) {
        boolean b = this.isDashActive;
        label2: {
            label0: {
                label1: {
                    if (!b) {
                        break label1;
                    }
                    if (!this.isWidgetVisibleToUser) {
                        break label1;
                    }
                    if (this.mWidgetView != null) {
                        break label0;
                    }
                }
                this.mHeadingAngle = (float)d;
                break label2;
            }
            if (d != 0.0) {
                this.animator.setValue((float)(d % 360.0));
            } else {
                this.mHeadingAngle = 0.0f;
                this.reDraw();
            }
        }
    }
    
    public void setValue(float f) {
        this.mHeadingAngle = f;
        this.reDraw();
    }
    
    public void setView(com.navdy.hud.app.view.DashboardWidgetView a) {
        if (a != null) {
            a.setContentView(R.layout.smart_dash_widget_circular_content_layout);
        }
        super.setView(a);
    }
    
    protected void updateGauge() {
        this.mCompassDrawable.setGaugeValue(this.mHeadingAngle);
    }
}
