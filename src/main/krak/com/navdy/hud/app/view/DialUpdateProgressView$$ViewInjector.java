package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class DialUpdateProgressView$$ViewInjector {
    public DialUpdateProgressView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.DialUpdateProgressView a0, Object a1) {
        a0.mProgress = (android.widget.ProgressBar)a.findRequiredView(a1, R.id.progress, "field 'mProgress'");
    }
    
    public static void reset(com.navdy.hud.app.view.DialUpdateProgressView a) {
        a.mProgress = null;
    }
}
