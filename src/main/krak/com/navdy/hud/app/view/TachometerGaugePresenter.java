package com.navdy.hud.app.view;

public class TachometerGaugePresenter extends com.navdy.hud.app.view.GaugeViewPresenter implements com.navdy.hud.app.view.SerialValueAnimator$SerialValueAnimatorAdapter {
    final public static String ID = "TACHOMETER_GAUGE_IDENTIFIER";
    final private static int MAX_MARKER_ANIMATION_DELAY = 500;
    final private static int MAX_MARKER_ANIMATION_DURATION = 1000;
    final private static int MAX_RPM_VALUE = 8000;
    final private static int RPM_DIFFERENCE = 100;
    final private static int RPM_WARNING_THRESHOLD = 7000;
    final public static int VALUE_ANIMATION_DURATION = 200;
    int animatedRpm;
    private Runnable animationRunnable;
    private boolean fullMode;
    private android.os.Handler handler;
    final private String kmhString;
    private int lastRPM;
    com.navdy.hud.app.view.drawable.TachometerGaugeDrawable mTachometerGaugeDrawable;
    private int maxMarkerAlpha;
    private int maxRpmValue;
    final private String mphString;
    int rpm;
    private com.navdy.hud.app.view.SerialValueAnimator serialValueAnimator;
    private boolean showingMaxMarkerAnimation;
    int speed;
    private int speedFastWarningColor;
    int speedLimit;
    private com.navdy.hud.app.manager.SpeedManager speedManager;
    private int speedSafeColor;
    private int speedVeryFastWarningColor;
    private android.animation.ValueAnimator valueAnimator;
    private String widgetName;
    
    public TachometerGaugePresenter(android.content.Context a) {
        this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
        this.lastRPM = -2147483648;
        this.handler = new android.os.Handler();
        this.serialValueAnimator = new com.navdy.hud.app.view.SerialValueAnimator((com.navdy.hud.app.view.SerialValueAnimator$SerialValueAnimatorAdapter)this, 200);
        this.mTachometerGaugeDrawable = new com.navdy.hud.app.view.drawable.TachometerGaugeDrawable(a, 0);
        this.mTachometerGaugeDrawable.setMaxGaugeValue(8000f);
        this.mTachometerGaugeDrawable.setRPMWarningLevel(7000);
        this.speedSafeColor = a.getResources().getColor(17170443);
        this.speedFastWarningColor = a.getResources().getColor(R.color.cyan);
        this.speedVeryFastWarningColor = a.getResources().getColor(R.color.speed_very_fast_warning_color);
        this.widgetName = a.getResources().getString(R.string.gauge_tacho_meter);
        android.content.res.Resources a0 = a.getResources();
        this.mphString = a0.getString(R.string.mph);
        this.kmhString = a0.getString(R.string.kilometers_per_hour);
        this.animationRunnable = (Runnable)new com.navdy.hud.app.view.TachometerGaugePresenter$1(this);
    }
    
    static android.animation.ValueAnimator access$000(com.navdy.hud.app.view.TachometerGaugePresenter a) {
        return a.valueAnimator;
    }
    
    static android.animation.ValueAnimator access$002(com.navdy.hud.app.view.TachometerGaugePresenter a, android.animation.ValueAnimator a0) {
        a.valueAnimator = a0;
        return a0;
    }
    
    static int access$102(com.navdy.hud.app.view.TachometerGaugePresenter a, int i) {
        a.maxMarkerAlpha = i;
        return i;
    }
    
    static boolean access$202(com.navdy.hud.app.view.TachometerGaugePresenter a, boolean b) {
        a.showingMaxMarkerAnimation = b;
        return b;
    }
    
    static int access$302(com.navdy.hud.app.view.TachometerGaugePresenter a, int i) {
        a.maxRpmValue = i;
        return i;
    }
    
    public void animationComplete(float f) {
    }
    
    public android.graphics.drawable.Drawable getDrawable() {
        return this.mTachometerGaugeDrawable;
    }
    
    public float getValue() {
        return (float)this.rpm;
    }
    
    public String getWidgetIdentifier() {
        return "TACHOMETER_GAUGE_IDENTIFIER";
    }
    
    public String getWidgetName() {
        return null;
    }
    
    public void setRPM(int i) {
        if (this.isDashActive && this.lastRPM != i && Math.abs(this.lastRPM - i) >= 100) {
            this.lastRPM = i;
            this.serialValueAnimator.setValue((float)i);
        }
    }
    
    public void setSpeed(int i) {
        if (i != -1 && this.speed != i) {
            this.speed = i;
            if (this.isDashActive) {
                this.reDraw();
            }
        }
    }
    
    public void setSpeedLimit(int i) {
        if (this.speedLimit != i) {
            this.speedLimit = i;
            if (this.isDashActive) {
                this.reDraw();
            }
        }
    }
    
    public void setValue(float f) {
        this.rpm = (int)f;
        if (this.rpm > this.maxRpmValue) {
            this.maxRpmValue = Math.min(8000, this.rpm);
            this.handler.removeCallbacks(this.animationRunnable);
            this.showingMaxMarkerAnimation = false;
            this.maxMarkerAlpha = 0;
            if (this.valueAnimator != null && this.valueAnimator.isRunning()) {
                this.valueAnimator.cancel();
            }
        }
        if (this.rpm < this.maxRpmValue && !this.showingMaxMarkerAnimation) {
            this.maxMarkerAlpha = 255;
            this.showingMaxMarkerAnimation = true;
            this.handler.removeCallbacks(this.animationRunnable);
            this.handler.postDelayed(this.animationRunnable, 500L);
        }
        this.reDraw();
    }
    
    public void setView(com.navdy.hud.app.view.DashboardWidgetView a) {
        if (a != null) {
            a.setContentView(R.layout.tachometer_gauge);
        }
        super.setView(a);
        this.mGaugeView = (com.navdy.hud.app.view.GaugeView)a;
        if (this.mGaugeView != null && android.os.Build.VERSION.SDK_INT >= 21) {
            android.util.TypedValue a0 = new android.util.TypedValue();
            this.mGaugeView.getResources().getValue(R.dimen.speedometer_text_letter_spacing, a0, true);
            float f = a0.getFloat();
            this.mGaugeView.getValueTextView().setLetterSpacing(f);
        }
    }
    
    protected void updateGauge() {
        if (this.mGaugeView != null) {
            String s = null;
            int i = 0;
            this.mTachometerGaugeDrawable.setGaugeValue((float)this.rpm);
            this.mTachometerGaugeDrawable.setMaxMarkerValue(this.maxRpmValue);
            this.mTachometerGaugeDrawable.setMaxMarkerAlpha(this.maxMarkerAlpha);
            this.mGaugeView.setValueText((CharSequence)Integer.toString(this.speed));
            com.navdy.hud.app.manager.SpeedManager$SpeedUnit a = this.speedManager.getSpeedUnit();
            if (com.navdy.hud.app.view.TachometerGaugePresenter$2.$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[a.ordinal()] == 2) {
                s = this.kmhString;
                i = 13;
            } else {
                s = this.mphString;
                i = 8;
            }
            if (this.speedLimit <= 0) {
                this.mGaugeView.setUnitText((CharSequence)s);
                this.mGaugeView.setValueText((CharSequence)Integer.toString(this.speed));
                this.mGaugeView.getUnitTextView().setTextColor(this.speedSafeColor);
            } else if (this.speed < this.speedLimit + i) {
                if (this.speed < this.speedLimit) {
                    this.mGaugeView.getUnitTextView().setTextColor(this.speedSafeColor);
                    this.mGaugeView.setUnitText((CharSequence)s);
                } else {
                    com.navdy.hud.app.view.GaugeView a0 = this.mGaugeView;
                    android.content.res.Resources a1 = this.mGaugeView.getResources();
                    Object[] a2 = new Object[2];
                    a2[0] = Integer.valueOf(this.speedLimit);
                    a2[1] = s;
                    a0.setUnitText((CharSequence)a1.getString(R.string.speed_limit_with_unit, a2));
                    this.mGaugeView.getUnitTextView().setTextColor(this.speedFastWarningColor);
                }
            } else {
                com.navdy.hud.app.view.GaugeView a3 = this.mGaugeView;
                android.content.res.Resources a4 = this.mGaugeView.getResources();
                Object[] a5 = new Object[2];
                a5[0] = Integer.valueOf(this.speedLimit);
                a5[1] = s;
                a3.setUnitText((CharSequence)a4.getString(R.string.speed_limit_with_unit, a5));
                this.mGaugeView.getUnitTextView().setTextColor(this.speedVeryFastWarningColor);
            }
        }
    }
}
