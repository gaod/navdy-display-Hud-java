package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class ForceUpdateView$$ViewInjector {
    public ForceUpdateView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.ForceUpdateView a0, Object a1) {
        a0.mScreenTitleText = (android.widget.TextView)a.findRequiredView(a1, R.id.mainTitle, "field 'mScreenTitleText'");
        a0.mTextView1 = (android.widget.TextView)a.findRequiredView(a1, R.id.title1, "field 'mTextView1'");
        a0.mTextView2 = (android.widget.TextView)a.findRequiredView(a1, R.id.title2, "field 'mTextView2'");
        a0.mChoiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout)a.findRequiredView(a1, R.id.choiceLayout, "field 'mChoiceLayout'");
        a0.mTextView3 = (android.widget.TextView)a.findRequiredView(a1, R.id.title3, "field 'mTextView3'");
        a0.mIcon = (android.widget.ImageView)a.findRequiredView(a1, R.id.image, "field 'mIcon'");
    }
    
    public static void reset(com.navdy.hud.app.view.ForceUpdateView a) {
        a.mScreenTitleText = null;
        a.mTextView1 = null;
        a.mTextView2 = null;
        a.mChoiceLayout = null;
        a.mTextView3 = null;
        a.mIcon = null;
    }
}
