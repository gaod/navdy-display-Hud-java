package com.navdy.hud.app.view;

final public class WelcomeView$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding presenter;
    
    public WelcomeView$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.view.WelcomeView", false, com.navdy.hud.app.view.WelcomeView.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.presenter = a.requestBinding("com.navdy.hud.app.screen.WelcomeScreen$Presenter", com.navdy.hud.app.view.WelcomeView.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.presenter);
    }
    
    public void injectMembers(com.navdy.hud.app.view.WelcomeView a) {
        a.presenter = (com.navdy.hud.app.screen.WelcomeScreen$Presenter)this.presenter.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.view.WelcomeView)a);
    }
}
