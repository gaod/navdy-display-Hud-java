package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class EmptyGaugePresenter$$ViewInjector {
    public EmptyGaugePresenter$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.EmptyGaugePresenter a0, Object a1) {
        a0.mEmptyGaugeText = (android.widget.TextView)a.findRequiredView(a1, R.id.txt_empty_gauge, "field 'mEmptyGaugeText'");
    }
    
    public static void reset(com.navdy.hud.app.view.EmptyGaugePresenter a) {
        a.mEmptyGaugeText = null;
    }
}
