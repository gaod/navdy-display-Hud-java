package com.navdy.hud.app.view;

class GestureLearningView$3 implements android.animation.Animator$AnimatorListener {
    final com.navdy.hud.app.view.GestureLearningView this$0;
    
    GestureLearningView$3(com.navdy.hud.app.view.GestureLearningView a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationCancel(android.animation.Animator a) {
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        com.navdy.hud.app.view.GestureLearningView.access$402(this.this$0, (com.navdy.hud.app.view.GestureLearningView.access$400(this.this$0) + 1) % com.navdy.hud.app.view.GestureLearningView.access$500(this.this$0).length);
        String s = com.navdy.hud.app.view.GestureLearningView.access$500(this.this$0)[com.navdy.hud.app.view.GestureLearningView.access$400(this.this$0)];
        String s0 = com.navdy.hud.app.view.GestureLearningView.access$500(this.this$0)[(com.navdy.hud.app.view.GestureLearningView.access$400(this.this$0) + 1) % com.navdy.hud.app.view.GestureLearningView.access$500(this.this$0).length];
        this.this$0.mTipsTextView1.setText((CharSequence)s);
        android.view.ViewGroup$MarginLayoutParams a0 = (android.view.ViewGroup$MarginLayoutParams)this.this$0.mTipsScroller.getLayoutParams();
        a0.topMargin = 0;
        this.this$0.mTipsScroller.setLayoutParams((android.view.ViewGroup$LayoutParams)a0);
        this.this$0.mTipsTextView2.setText((CharSequence)s0);
        if (!com.navdy.hud.app.view.GestureLearningView.access$600(this.this$0)) {
            com.navdy.hud.app.view.GestureLearningView.access$800(this.this$0).postDelayed(com.navdy.hud.app.view.GestureLearningView.access$700(this.this$0), 4000L);
        }
    }
    
    public void onAnimationRepeat(android.animation.Animator a) {
    }
    
    public void onAnimationStart(android.animation.Animator a) {
    }
}
