package com.navdy.hud.app.view;

class EmptyGaugePresenter$1 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final com.navdy.hud.app.view.EmptyGaugePresenter this$0;
    
    EmptyGaugePresenter$1(com.navdy.hud.app.view.EmptyGaugePresenter a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        float f = ((Float)a.getAnimatedValue()).floatValue();
        if (this.this$0.mEmptyGaugeText != null) {
            this.this$0.mEmptyGaugeText.setAlpha(f);
        }
    }
}
