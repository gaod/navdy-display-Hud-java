package com.navdy.hud.app.view;

final public class FactoryResetView$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding presenter;
    
    public FactoryResetView$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.view.FactoryResetView", false, com.navdy.hud.app.view.FactoryResetView.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.presenter = a.requestBinding("com.navdy.hud.app.screen.FactoryResetScreen$Presenter", com.navdy.hud.app.view.FactoryResetView.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.presenter);
    }
    
    public void injectMembers(com.navdy.hud.app.view.FactoryResetView a) {
        a.presenter = (com.navdy.hud.app.screen.FactoryResetScreen$Presenter)this.presenter.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.view.FactoryResetView)a);
    }
}
