package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class FuelGaugePresenter2$$ViewInjector {
    public FuelGaugePresenter2$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.FuelGaugePresenter2 a0, Object a1) {
        a0.lowFuelIndicatorLeft = (android.widget.ImageView)a.findRequiredView(a1, R.id.low_fuel_indicator_left, "field 'lowFuelIndicatorLeft'");
        a0.lowFuelIndicatorRight = (android.widget.ImageView)a.findRequiredView(a1, R.id.low_fuel_indicator_right, "field 'lowFuelIndicatorRight'");
        a0.fuelTankIndicatorLeft = (android.widget.ImageView)a.findRequiredView(a1, R.id.fuel_tank_side_indicator_left, "field 'fuelTankIndicatorLeft'");
        a0.fuelTankIndicatorRight = (android.widget.ImageView)a.findRequiredView(a1, R.id.fuel_tank_side_indicator_right, "field 'fuelTankIndicatorRight'");
        a0.fuelTypeIndicator = (android.widget.ImageView)a.findRequiredView(a1, R.id.fuel_type_icon, "field 'fuelTypeIndicator'");
        a0.rangeText = (android.widget.TextView)a.findRequiredView(a1, R.id.txt_value, "field 'rangeText'");
        a0.rangeUnitText = (android.widget.TextView)a.findRequiredView(a1, R.id.txt_unit, "field 'rangeUnitText'");
    }
    
    public static void reset(com.navdy.hud.app.view.FuelGaugePresenter2 a) {
        a.lowFuelIndicatorLeft = null;
        a.lowFuelIndicatorRight = null;
        a.fuelTankIndicatorLeft = null;
        a.fuelTankIndicatorRight = null;
        a.fuelTypeIndicator = null;
        a.rangeText = null;
        a.rangeUnitText = null;
    }
}
