package com.navdy.hud.app.view.drawable;

public class GaugeDrawable extends com.navdy.hud.app.view.drawable.CustomDrawable {
    private android.graphics.drawable.Drawable mBackgroundDrawable;
    protected int[] mColorTable;
    protected int mDefaultColor;
    protected float mMaxValue;
    protected float mMinValue;
    protected float mValue;
    
    public GaugeDrawable(android.content.Context a, int i) {
        if (i != 0) {
            this.mBackgroundDrawable = a.getResources().getDrawable(i);
        }
        this.mPaint.setAntiAlias(true);
    }
    
    public GaugeDrawable(android.content.Context a, int i, int i0) {
        this(a, i);
        this.mColorTable = a.getResources().getIntArray(i0);
        if (this.mColorTable != null && this.mColorTable.length > 0) {
            this.mDefaultColor = this.mColorTable[0];
        }
    }
    
    public void draw(android.graphics.Canvas a) {
        super.draw(a);
        if (this.mBackgroundDrawable != null) {
            this.mBackgroundDrawable.draw(a);
        }
    }
    
    public int getHeight() {
        android.graphics.Rect a = this.getBounds();
        return (a == null) ? 0 : a.height();
    }
    
    public int getWidth() {
        android.graphics.Rect a = this.getBounds();
        return (a == null) ? 0 : a.width();
    }
    
    public void setBounds(int i, int i0, int i1, int i2) {
        super.setBounds(i, i0, i1, i2);
        if (this.mBackgroundDrawable != null) {
            this.mBackgroundDrawable.setBounds(i, i0, i1, i2);
        }
    }
    
    public void setGaugeValue(float f) {
        this.mValue = f;
        if (f > this.mMaxValue) {
            this.mValue = this.mMaxValue;
        } else if (f < this.mMinValue) {
            this.mValue = this.mMinValue;
        }
    }
    
    public void setMaxGaugeValue(float f) {
        this.mMaxValue = f;
    }
    
    public void setMinValue(float f) {
        this.mMinValue = f;
    }
    
    public void setState(int i) {
        if (this.mColorTable != null && i < this.mColorTable.length && i >= 0) {
            this.mDefaultColor = this.mColorTable[i];
            return;
        }
        throw new IllegalArgumentException();
    }
}
