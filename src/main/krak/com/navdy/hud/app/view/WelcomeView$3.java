package com.navdy.hud.app.view;

class WelcomeView$3 implements com.navdy.hud.app.ui.component.carousel.Carousel$ViewProcessor {
    final com.navdy.hud.app.view.WelcomeView this$0;
    
    WelcomeView$3(com.navdy.hud.app.view.WelcomeView a) {
        super();
        this.this$0 = a;
    }
    
    private void updateInitials(boolean b, com.navdy.hud.app.ui.component.carousel.Carousel$Model a, android.widget.ImageView a0) {
        com.navdy.hud.app.ui.component.image.InitialsImageView a1 = (com.navdy.hud.app.ui.component.image.InitialsImageView)a0;
        android.widget.ImageView a2 = a1;
        int i = a.id;
        label5: {
            float f = 0.0f;
            label3: {
                label4: {
                    if (i == R.id.welcome_menu_add_driver) {
                        break label4;
                    }
                    if (a.id != R.id.welcome_menu_searching) {
                        break label3;
                    }
                }
                a1.setInitials((String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.LARGE);
                break label5;
            }
            com.navdy.hud.app.screen.WelcomeScreen$DeviceMetadata a3 = (com.navdy.hud.app.screen.WelcomeScreen$DeviceMetadata)a.extras;
            boolean b0 = a.id >= 100;
            label2: {
                label0: {
                    label1: {
                        if (!b0) {
                            break label1;
                        }
                        if (!b) {
                            break label0;
                        }
                    }
                    f = 1f;
                    break label2;
                }
                f = 0.5f;
            }
            a2.setAlpha(f);
            com.navdy.service.library.device.NavdyDeviceId a4 = a3.deviceId;
            a2.setTag(a4);
            java.util.HashMap a5 = a.infoMap;
            String s = com.navdy.hud.app.framework.contacts.ContactUtil.getInitials((a5 == null) ? "" : (String)((java.util.Map)a5).get(Integer.valueOf(R.id.subTitle)));
            a1.setInitials((String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
            java.io.File a6 = a3.driverProfile.getDriverImageFile();
            android.graphics.Bitmap a7 = com.navdy.hud.app.util.picasso.PicassoUtil.getBitmapfromCache(a6);
            if (a7 == null) {
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.view.WelcomeView$3$1(this, a2, a4, a6, a1, s), 1);
            } else {
                a1.setImageBitmap(a7);
            }
        }
    }
    
    public void processInfoView(com.navdy.hud.app.ui.component.carousel.Carousel$Model a, android.view.View a0, int i) {
    }
    
    public void processLargeImageView(com.navdy.hud.app.ui.component.carousel.Carousel$Model a, android.view.View a0, int i, int i0, int i1) {
        com.navdy.hud.app.ui.component.image.CrossFadeImageView a1 = (com.navdy.hud.app.ui.component.image.CrossFadeImageView)a0;
        this.updateInitials(false, a, (android.widget.ImageView)(com.navdy.hud.app.ui.component.image.InitialsImageView)a1.getSmall());
        this.updateInitials(true, a, (android.widget.ImageView)(com.navdy.hud.app.ui.component.image.InitialsImageView)a1.getBig());
    }
    
    public void processSmallImageView(com.navdy.hud.app.ui.component.carousel.Carousel$Model a, android.view.View a0, int i, int i0, int i1) {
        com.navdy.hud.app.ui.component.image.CrossFadeImageView a1 = (com.navdy.hud.app.ui.component.image.CrossFadeImageView)a0;
        this.updateInitials(false, a, (android.widget.ImageView)(com.navdy.hud.app.ui.component.image.InitialsImageView)a1.getSmall());
        this.updateInitials(true, a, (android.widget.ImageView)(com.navdy.hud.app.ui.component.image.InitialsImageView)a1.getBig());
    }
}
