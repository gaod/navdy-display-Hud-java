package com.navdy.hud.app.view;

public class FontTextView extends android.widget.TextView {
    private static java.util.HashMap mCachedTypeFaces;
    
    static {
        mCachedTypeFaces = new java.util.HashMap();
    }
    
    public FontTextView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public FontTextView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public FontTextView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        android.content.res.TypedArray a1 = a.obtainStyledAttributes(a0, com.navdy.hud.app.R$styleable.FontTextView, i, 0);
        if (a1 != null) {
            String s = a1.getString(0);
            if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
                this.setTypeface(com.navdy.hud.app.view.FontTextView.createFromAsset(a.getAssets(), s));
            }
            a1.recycle();
        }
    }
    
    public static android.graphics.Typeface createFromAsset(android.content.res.AssetManager a, String s) {
        android.graphics.Typeface a0 = null;
        if (mCachedTypeFaces.containsKey(s)) {
            a0 = (android.graphics.Typeface)mCachedTypeFaces.get(s);
        } else {
            a0 = android.graphics.Typeface.createFromAsset(a, s);
            if (a0 != null) {
                mCachedTypeFaces.put(s, a0);
            }
        }
        return a0;
    }
}
