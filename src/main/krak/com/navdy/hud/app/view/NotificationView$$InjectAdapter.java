package com.navdy.hud.app.view;

final public class NotificationView$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding presenter;
    
    public NotificationView$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.view.NotificationView", false, com.navdy.hud.app.view.NotificationView.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.presenter = a.requestBinding("com.navdy.hud.app.presenter.NotificationPresenter", com.navdy.hud.app.view.NotificationView.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.presenter);
    }
    
    public void injectMembers(com.navdy.hud.app.view.NotificationView a) {
        a.presenter = (com.navdy.hud.app.presenter.NotificationPresenter)this.presenter.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.view.NotificationView)a);
    }
}
