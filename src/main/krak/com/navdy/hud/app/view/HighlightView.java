package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class HighlightView extends android.view.View {
    private android.graphics.Paint paint;
    private int radius;
    private float strokeWidth;
    
    public HighlightView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public HighlightView(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
        this.initFromAttributes(a, a0);
        this.paint = new android.graphics.Paint();
        this.paint.setStrokeWidth(this.strokeWidth);
        this.paint.setAntiAlias(true);
        this.paint.setColor(this.getResources().getColor(R.color.hud_cyan));
    }
    
    private void initFromAttributes(android.content.Context a, android.util.AttributeSet a0) {
        android.content.res.TypedArray a1 = a.getTheme().obtainStyledAttributes(a0, com.navdy.hud.app.R$styleable.Indicator, 0, 0);
        this.strokeWidth = a1.getDimension(3, (float)(int)this.getResources().getDimension(R.dimen.default_highlight_stroke_width));
        a1.recycle();
    }
    
    protected void onDraw(android.graphics.Canvas a) {
        super.onDraw(a);
        int i = this.getWidth();
        int i0 = this.getHeight();
        a.drawCircle((float)(i / 2), (float)(i0 / 2), (float)this.radius, this.paint);
    }
    
    public void setPaintStyle(android.graphics.Paint$Style a) {
        if (a == android.graphics.Paint$Style.FILL_AND_STROKE) {
            throw new IllegalArgumentException();
        }
        this.paint.setStyle(a);
    }
    
    public void setRadius(int i) {
        this.radius = i;
    }
    
    public void setStrokeWidth(float f) {
        this.strokeWidth = f;
    }
}
