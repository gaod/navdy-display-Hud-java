package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class MusicWidgetPresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter implements com.navdy.hud.app.manager.MusicManager$MusicUpdateListener {
    final private static float ARTWORK_ALPHA_NOT_PLAYING = 0.5f;
    final private static float ARTWORK_ALPHA_PLAYING = 1f;
    private static com.navdy.service.library.log.Logger logger;
    private android.graphics.Bitmap albumArt;
    private boolean animateNextArtworkTransition;
    private com.navdy.hud.app.framework.music.OutlineTextView artistText;
    private android.os.Handler handler;
    private com.navdy.hud.app.framework.music.AlbumArtImageView image;
    private CharSequence lastAlbumArtHash;
    private String musicGaugeName;
    @Inject
    com.navdy.hud.app.manager.MusicManager musicManager;
    private com.navdy.hud.app.framework.music.OutlineTextView stateText;
    private com.navdy.hud.app.framework.music.OutlineTextView titleText;
    private com.navdy.service.library.events.audio.MusicTrackInfo trackInfo;
    
    static {
        logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.MusicWidgetPresenter.class);
    }
    
    public MusicWidgetPresenter(android.content.Context a) {
        this.trackInfo = null;
        this.handler = new android.os.Handler();
        this.musicGaugeName = a.getResources().getString(R.string.widget_music);
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return logger;
    }
    
    static void access$100(com.navdy.hud.app.view.MusicWidgetPresenter a) {
        a.resetArtwork();
    }
    
    static void access$200(com.navdy.hud.app.view.MusicWidgetPresenter a) {
        a.redrawOnMainThread();
    }
    
    static android.graphics.Bitmap access$300(com.navdy.hud.app.view.MusicWidgetPresenter a) {
        return a.albumArt;
    }
    
    static android.graphics.Bitmap access$302(com.navdy.hud.app.view.MusicWidgetPresenter a, android.graphics.Bitmap a0) {
        a.albumArt = a0;
        return a0;
    }
    
    private void redrawOnMainThread() {
        this.handler.post((Runnable)new com.navdy.hud.app.view.MusicWidgetPresenter$3(this));
    }
    
    private void resetArtwork() {
        this.albumArt = null;
        this.lastAlbumArtHash = null;
    }
    
    private void updateMetadata() {
        CharSequence a = this.titleText.getText();
        String s = this.trackInfo.name;
        label2: {
            label3: {
                if (s == null) {
                    break label3;
                }
                if (this.trackInfo.name.equals(a)) {
                    break label2;
                }
            }
            this.titleText.setText((CharSequence)this.trackInfo.name);
        }
        CharSequence a0 = this.artistText.getText();
        String s0 = this.trackInfo.author;
        label0: {
            label1: {
                if (s0 == null) {
                    break label1;
                }
                if (this.trackInfo.author.equals(a0)) {
                    break label0;
                }
            }
            this.artistText.setText((CharSequence)this.trackInfo.author);
        }
    }
    
    public android.graphics.drawable.Drawable getDrawable() {
        return null;
    }
    
    public String getWidgetIdentifier() {
        logger.d("getWidgetIdentifier");
        return "MUSIC_WIDGET";
    }
    
    public String getWidgetName() {
        return this.musicGaugeName;
    }
    
    public void onAlbumArtUpdate(okio.ByteString a, boolean b) {
        logger.d(new StringBuilder().append("onAlbumArtUpdate ").append(a).append(", ").append(b).toString());
        this.animateNextArtworkTransition = b;
        label2: if (a != null) {
            String s = a.md5().hex();
            boolean b0 = android.text.TextUtils.equals(this.lastAlbumArtHash, (CharSequence)s);
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.albumArt != null) {
                        break label0;
                    }
                }
                this.lastAlbumArtHash = (CharSequence)s;
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.view.MusicWidgetPresenter$2(this, a), 1);
                break label2;
            }
            logger.d("Already have this artwork, ignoring");
        } else {
            logger.v("ByteString image to set in notification is null");
            this.resetArtwork();
            this.reDraw();
        }
    }
    
    public void onTrackUpdated(com.navdy.service.library.events.audio.MusicTrackInfo a, java.util.Set a0, boolean b) {
        this.trackInfo = a;
        if (b) {
            logger.d("onTrackUpdated, delayed!");
            this.handler.postDelayed((Runnable)new com.navdy.hud.app.view.MusicWidgetPresenter$1(this), 250L);
        } else {
            logger.d("onTrackUpdated (immediate)");
            this.reDraw();
        }
    }
    
    public void setView(com.navdy.hud.app.view.DashboardWidgetView a, android.os.Bundle a0) {
        logger.d(new StringBuilder().append("setView ").append(a).toString());
        if (a != null) {
            a.setContentView(R.layout.music_widget);
            this.titleText = (com.navdy.hud.app.framework.music.OutlineTextView)a.findViewById(R.id.txt_name);
            this.artistText = (com.navdy.hud.app.framework.music.OutlineTextView)a.findViewById(R.id.txt_author);
            this.stateText = (com.navdy.hud.app.framework.music.OutlineTextView)a.findViewById(R.id.txt_player_state);
            this.image = (com.navdy.hud.app.framework.music.AlbumArtImageView)a.findViewById(R.id.img_album_art);
        }
        super.setView(a);
    }
    
    public void setWidgetVisibleToUser(boolean b) {
        logger.d(new StringBuilder().append("setWidgetVisibleToUser ").append(b).toString());
        if (b) {
            logger.v("setWidgetVisibleToUser: add music update listener");
            this.musicManager.addMusicUpdateListener((com.navdy.hud.app.manager.MusicManager$MusicUpdateListener)this);
            this.trackInfo = this.musicManager.getCurrentTrack();
        } else {
            logger.v("setWidgetVisibleToUser: remove music update listener");
            this.musicManager.removeMusicUpdateListener((com.navdy.hud.app.manager.MusicManager$MusicUpdateListener)this);
        }
        super.setWidgetVisibleToUser(b);
    }
    
    protected void updateGauge() {
        this.image.setArtworkBitmap(this.albumArt, this.animateNextArtworkTransition);
        com.navdy.service.library.events.audio.MusicTrackInfo a = this.trackInfo;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (this.trackInfo != com.navdy.hud.app.manager.MusicManager.EMPTY_TRACK) {
                        break label0;
                    }
                }
                this.titleText.setText((CharSequence)"");
                this.artistText.setText((CharSequence)"");
                this.stateText.setVisibility(4);
                break label2;
            }
            if (com.navdy.hud.app.manager.MusicManager.tryingToPlay(this.trackInfo.playbackState)) {
                this.image.setAlpha(1f);
                this.stateText.setVisibility(4);
            } else {
                this.image.setAlpha(0.5f);
                this.stateText.setVisibility(0);
            }
            this.updateMetadata();
        }
    }
}
