package com.navdy.hud.app.view;

class MainView$7 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.view.MainView this$0;
    final android.view.ViewGroup val$notificationExtension;
    
    MainView$7(com.navdy.hud.app.view.MainView a, android.view.ViewGroup a0) {
        super();
        this.this$0 = a;
        this.val$notificationExtension = a0;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        super.onAnimationEnd(a);
        android.view.View a0 = this.val$notificationExtension.getChildAt(0);
        if (a0 instanceof com.navdy.hud.app.ui.activity.Main$INotificationExtensionView) {
            ((com.navdy.hud.app.ui.activity.Main$INotificationExtensionView)a0).onStop();
        }
        this.val$notificationExtension.setVisibility(8);
        this.val$notificationExtension.removeAllViews();
    }
}
