package com.navdy.hud.app.view;

abstract public class DashboardWidgetPresenter {
    final public static String EXTRA_GRAVITY = "EXTRA_GRAVITY";
    final public static String EXTRA_IS_ACTIVE = "EXTRA_IS_ACTIVE";
    final public static int GRAVITY_CENTER = 1;
    final public static int GRAVITY_LEFT = 0;
    final public static int GRAVITY_RIGHT = 2;
    protected boolean isDashActive;
    protected boolean isRegistered;
    protected boolean isWidgetVisibleToUser;
    protected com.navdy.service.library.log.Logger logger;
    protected android.view.View mCustomView;
    protected com.navdy.hud.app.view.DashboardWidgetView mWidgetView;
    
    public DashboardWidgetPresenter() {
        this.logger = new com.navdy.service.library.log.Logger((this).getClass());
        this.isRegistered = false;
    }
    
    public boolean canDraw() {
        boolean b = false;
        boolean b0 = this.isDashActive;
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.isWidgetVisibleToUser) {
                        break label1;
                    }
                    if (this.mWidgetView != null) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    abstract public android.graphics.drawable.Drawable getDrawable();
    
    
    abstract public String getWidgetIdentifier();
    
    
    abstract public String getWidgetName();
    
    
    public com.navdy.hud.app.view.DashboardWidgetView getWidgetView() {
        return this.mWidgetView;
    }
    
    protected boolean isRegisteringToBusRequired() {
        return false;
    }
    
    public boolean isWidgetActive() {
        return this.mWidgetView != null;
    }
    
    public void onPause() {
        this.isDashActive = false;
        if (this.canDraw()) {
            this.logger.v("widget::onPause:");
        }
    }
    
    public void onResume() {
        this.isDashActive = true;
        if (this.canDraw()) {
            this.logger.v("widget::onResume:");
            this.reDraw();
        }
    }
    
    public void reDraw() {
        if (this.canDraw()) {
            this.updateGauge();
            if (this.mCustomView != null) {
                this.mCustomView.invalidate();
            }
        }
    }
    
    protected void registerToBus() {
        com.navdy.hud.app.HudApplication.getApplication().getBus().register(this);
    }
    
    public void setView(com.navdy.hud.app.view.DashboardWidgetView a) {
        this.mWidgetView = a;
        com.navdy.hud.app.view.DashboardWidgetView a0 = this.mWidgetView;
        label1: {
            RuntimeException a1 = null;
            label0: {
                RuntimeException a2 = null;
                label2: if (a0 == null) {
                    if (!this.isRegistered) {
                        break label1;
                    }
                    {
                        try {
                            this.unregisterToBus();
                            this.isRegistered = false;
                        } catch(RuntimeException a3) {
                            a2 = a3;
                            break label2;
                        }
                        break label1;
                    }
                } else {
                    this.mCustomView = a.getCustomView();
                    if (this.mCustomView != null) {
                        this.mCustomView.setBackground(this.getDrawable());
                    }
                    if (!this.isRegisteringToBusRequired()) {
                        break label1;
                    }
                    if (this.isRegistered) {
                        break label1;
                    }
                    try {
                        this.registerToBus();
                        this.isRegistered = true;
                        break label1;
                    } catch(RuntimeException a4) {
                        a1 = a4;
                        break label0;
                    }
                }
                this.logger.e("Runtime exception unregistering from the bus ", (Throwable)a2);
                break label1;
            }
            this.logger.e("Runtime exception registering to the bus ", (Throwable)a1);
        }
        this.reDraw();
    }
    
    public void setView(com.navdy.hud.app.view.DashboardWidgetView a, android.os.Bundle a0) {
        this.setView(a);
    }
    
    public void setWidgetVisibleToUser(boolean b) {
        this.isWidgetVisibleToUser = b;
        if (this.canDraw()) {
            this.logger.v("widget::visible:reDraw");
            this.reDraw();
        }
    }
    
    protected void unregisterToBus() {
        com.navdy.hud.app.HudApplication.getApplication().getBus().unregister(this);
    }
    
    abstract protected void updateGauge();
}
