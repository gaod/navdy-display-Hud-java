package com.navdy.hud.app.view;

public class SpeedLimitSignPresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    private int speedLimit;
    @InjectView(R.id.speed_limit_sign)
    protected com.navdy.hud.app.view.SpeedLimitSignView speedLimitSignView;
    private String speedLimitSignWidgetName;
    @InjectView(R.id.txt_speed_limit_unavailable)
    protected android.widget.TextView speedLimitUnavailableText;
    private com.navdy.hud.app.manager.SpeedManager speedManager;
    
    public SpeedLimitSignPresenter(android.content.Context a) {
        this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
        this.speedLimitSignWidgetName = a.getResources().getString(R.string.widget_speed_limit);
    }
    
    public android.graphics.drawable.Drawable getDrawable() {
        return null;
    }
    
    public String getWidgetIdentifier() {
        return "SPEED_LIMIT_SIGN_GAUGE_ID";
    }
    
    public String getWidgetName() {
        return this.speedLimitSignWidgetName;
    }
    
    protected boolean isRegisteringToBusRequired() {
        return true;
    }
    
    public void onSpeedUnitChanged(com.navdy.hud.app.manager.SpeedManager$SpeedUnitChanged a) {
        this.reDraw();
    }
    
    public void setSpeedLimit(int i) {
        this.speedLimit = i;
        this.reDraw();
    }
    
    public void setView(com.navdy.hud.app.view.DashboardWidgetView a, android.os.Bundle a0) {
        if (a != null) {
            a.setContentView(R.layout.speed_limit_sign_gauge);
            butterknife.ButterKnife.inject(this, (android.view.View)a);
        }
        super.setView(a, a0);
    }
    
    protected void updateGauge() {
        if (this.speedLimit <= 0) {
            this.speedLimitUnavailableText.setVisibility(0);
            this.speedLimitSignView.setVisibility(8);
        } else {
            this.speedLimitUnavailableText.setVisibility(8);
            this.speedLimitSignView.setVisibility(0);
            com.navdy.hud.app.manager.SpeedManager$SpeedUnit a = this.speedManager.getSpeedUnit();
            this.speedLimitSignView.setSpeedLimitUnit(a);
            this.speedLimitSignView.setSpeedLimit(this.speedLimit);
        }
    }
}
