package com.navdy.hud.app.view;


    public enum LearnGestureScreenLayout$Mode {
        GESTURE(0),
    SENSOR_BLOCKED(1),
    TIPS(2),
    CAPTURE(3);

        private int value;
        LearnGestureScreenLayout$Mode(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class LearnGestureScreenLayout$Mode extends Enum {
//    final private static com.navdy.hud.app.view.LearnGestureScreenLayout$Mode[] $VALUES;
//    final public static com.navdy.hud.app.view.LearnGestureScreenLayout$Mode CAPTURE;
//    final public static com.navdy.hud.app.view.LearnGestureScreenLayout$Mode GESTURE;
//    final public static com.navdy.hud.app.view.LearnGestureScreenLayout$Mode SENSOR_BLOCKED;
//    final public static com.navdy.hud.app.view.LearnGestureScreenLayout$Mode TIPS;
//    
//    static {
//        GESTURE = new com.navdy.hud.app.view.LearnGestureScreenLayout$Mode("GESTURE", 0);
//        SENSOR_BLOCKED = new com.navdy.hud.app.view.LearnGestureScreenLayout$Mode("SENSOR_BLOCKED", 1);
//        TIPS = new com.navdy.hud.app.view.LearnGestureScreenLayout$Mode("TIPS", 2);
//        CAPTURE = new com.navdy.hud.app.view.LearnGestureScreenLayout$Mode("CAPTURE", 3);
//        com.navdy.hud.app.view.LearnGestureScreenLayout$Mode[] a = new com.navdy.hud.app.view.LearnGestureScreenLayout$Mode[4];
//        a[0] = GESTURE;
//        a[1] = SENSOR_BLOCKED;
//        a[2] = TIPS;
//        a[3] = CAPTURE;
//        $VALUES = a;
//    }
//    
//    private LearnGestureScreenLayout$Mode(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.view.LearnGestureScreenLayout$Mode valueOf(String s) {
//        return (com.navdy.hud.app.view.LearnGestureScreenLayout$Mode)Enum.valueOf(com.navdy.hud.app.view.LearnGestureScreenLayout$Mode.class, s);
//    }
//    
//    public static com.navdy.hud.app.view.LearnGestureScreenLayout$Mode[] values() {
//        return $VALUES.clone();
//    }
//}
//