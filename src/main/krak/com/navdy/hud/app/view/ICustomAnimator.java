package com.navdy.hud.app.view;

abstract public interface ICustomAnimator {
    abstract public android.animation.Animator getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode arg);
}
