package com.navdy.hud.app.view;


public enum ClockWidgetPresenter$ClockType {
    ANALOG(0),
    DIGITAL1(1),
    DIGITAL2(2);

    private int value;
    ClockWidgetPresenter$ClockType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class ClockWidgetPresenter$ClockType extends Enum {
//    final private static com.navdy.hud.app.view.ClockWidgetPresenter$ClockType[] $VALUES;
//    final public static com.navdy.hud.app.view.ClockWidgetPresenter$ClockType ANALOG;
//    final public static com.navdy.hud.app.view.ClockWidgetPresenter$ClockType DIGITAL1;
//    final public static com.navdy.hud.app.view.ClockWidgetPresenter$ClockType DIGITAL2;
//    
//    static {
//        ANALOG = new com.navdy.hud.app.view.ClockWidgetPresenter$ClockType("ANALOG", 0);
//        DIGITAL1 = new com.navdy.hud.app.view.ClockWidgetPresenter$ClockType("DIGITAL1", 1);
//        DIGITAL2 = new com.navdy.hud.app.view.ClockWidgetPresenter$ClockType("DIGITAL2", 2);
//        com.navdy.hud.app.view.ClockWidgetPresenter$ClockType[] a = new com.navdy.hud.app.view.ClockWidgetPresenter$ClockType[3];
//        a[0] = ANALOG;
//        a[1] = DIGITAL1;
//        a[2] = DIGITAL2;
//        $VALUES = a;
//    }
//    
//    private ClockWidgetPresenter$ClockType(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.view.ClockWidgetPresenter$ClockType valueOf(String s) {
//        return (com.navdy.hud.app.view.ClockWidgetPresenter$ClockType)Enum.valueOf(com.navdy.hud.app.view.ClockWidgetPresenter$ClockType.class, s);
//    }
//    
//    public static com.navdy.hud.app.view.ClockWidgetPresenter$ClockType[] values() {
//        return $VALUES.clone();
//    }
//}
//