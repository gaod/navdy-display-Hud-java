package com.navdy.hud.app.view;

class MainView$1 implements com.navdy.hud.app.ui.framework.INotificationAnimationListener {
    final com.navdy.hud.app.view.MainView this$0;
    
    MainView$1(com.navdy.hud.app.view.MainView a) {
        super();
        this.this$0 = a;
    }
    
    public void onStart(String s, com.navdy.hud.app.framework.notifications.NotificationType a, com.navdy.hud.app.ui.framework.UIStateManager$Mode a0) {
        if (a0 != com.navdy.hud.app.ui.framework.UIStateManager$Mode.EXPAND) {
            com.navdy.hud.app.view.MainView.access$102(this.this$0, true);
        } else {
            com.navdy.hud.app.view.MainView.access$002(this.this$0, true);
        }
    }
    
    public void onStop(String s, com.navdy.hud.app.framework.notifications.NotificationType a, com.navdy.hud.app.ui.framework.UIStateManager$Mode a0) {
        if (a0 != com.navdy.hud.app.ui.framework.UIStateManager$Mode.EXPAND) {
            com.navdy.hud.app.view.MainView.access$102(this.this$0, false);
        } else {
            com.navdy.hud.app.view.MainView.access$002(this.this$0, false);
        }
    }
}
