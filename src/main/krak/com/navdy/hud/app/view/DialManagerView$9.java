package com.navdy.hud.app.view;

class DialManagerView$9 {
    final static int[] $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$DialConnectionStatus$Status;
    final static int[] $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason;
    final static int[] $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
    
    static {
        $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$DialConnectionStatus$Status = new int[com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$DialConnectionStatus$Status;
        com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status a0 = com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status.CONNECTED;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$DialConnectionStatus$Status[com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status.CONNECTING.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$DialConnectionStatus$Status[com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status.DISCONNECTED.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$DialConnectionStatus$Status[com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status.CONNECTION_FAILED.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$DialConnectionStatus$Status[com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status.NO_DIAL_FOUND.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason = new int[com.navdy.hud.app.device.dial.DialConstants$NotificationReason.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason;
        com.navdy.hud.app.device.dial.DialConstants$NotificationReason a2 = com.navdy.hud.app.device.dial.DialConstants$NotificationReason.OK_BATTERY;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason[com.navdy.hud.app.device.dial.DialConstants$NotificationReason.LOW_BATTERY.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason[com.navdy.hud.app.device.dial.DialConstants$NotificationReason.VERY_LOW_BATTERY.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason[com.navdy.hud.app.device.dial.DialConstants$NotificationReason.EXTREMELY_LOW_BATTERY.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException7) {
        }
        $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.values().length];
        int[] a3 = $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
        com.navdy.hud.app.manager.InputManager$CustomKeyEvent a4 = com.navdy.hud.app.manager.InputManager$CustomKeyEvent.POWER_BUTTON_CLICK;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.POWER_BUTTON_DOUBLE_CLICK.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException9) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LEFT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException10) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.RIGHT.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException11) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.SELECT.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException12) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.POWER_BUTTON_LONG_PRESS.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException13) {
        }
    }
}
