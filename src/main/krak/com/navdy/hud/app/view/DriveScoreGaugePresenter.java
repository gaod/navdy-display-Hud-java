package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

final public class DriveScoreGaugePresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    final private long CLEAR_WARNING_TIME_MS;
    private Runnable clearWarning;
    final private android.content.Context context;
    private com.navdy.hud.app.view.drawable.DriveScoreGaugeDrawable driveScoreGaugeDrawable;
    final private String driveScoreGaugeName;
    public android.widget.TextView driveScoreText;
    private com.navdy.hud.app.analytics.TelemetryDataManager$DriveScoreUpdated driveScoreUpdated;
    final private android.os.Handler handler;
    final private int layoutId;
    private boolean newEvent;
    final private boolean showScore;
    public android.widget.TextView summaryText;
    final private int summaryTextColorNormal;
    final private int summaryTextColorWarning;
    final private String summaryTextDriveScore;
    final private String summaryTextExcessiveSpeeding;
    final private String summaryTextHardAcceleration;
    final private String summaryTextHardBraking;
    final private String summaryTextHighG;
    final private String summaryTextSpeeding;
    public android.widget.ImageView warningImage;
    
    public DriveScoreGaugePresenter(android.content.Context a, int i, boolean b) {
        super();
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        this.context = a;
        this.layoutId = i;
        this.showScore = b;
        this.CLEAR_WARNING_TIME_MS = 2000L;
        this.driveScoreGaugeDrawable = new com.navdy.hud.app.view.drawable.DriveScoreGaugeDrawable(this.context, R.array.drive_state_colors);
        com.navdy.hud.app.view.drawable.DriveScoreGaugeDrawable a0 = this.driveScoreGaugeDrawable;
        if (a0 != null) {
            a0.setMinValue(0.0f);
        }
        com.navdy.hud.app.view.drawable.DriveScoreGaugeDrawable a1 = this.driveScoreGaugeDrawable;
        if (a1 != null) {
            a1.setMaxGaugeValue(100f);
        }
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        android.content.res.Resources a2 = this.context.getResources();
        String s = a2.getString(R.string.drive_score);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(s, "resources.getString(R.string.drive_score)");
        this.summaryTextDriveScore = s;
        String s0 = a2.getString(R.string.hard_accel);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(s0, "resources.getString(R.string.hard_accel)");
        this.summaryTextHardAcceleration = s0;
        String s1 = a2.getString(R.string.hard_braking);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(s1, "resources.getString(R.string.hard_braking)");
        this.summaryTextHardBraking = s1;
        String s2 = a2.getString(R.string.speeding);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(s2, "resources.getString(R.string.speeding)");
        this.summaryTextSpeeding = s2;
        String s3 = a2.getString(R.string.widget_drive_score);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(s3, "resources.getString(R.string.widget_drive_score)");
        this.driveScoreGaugeName = s3;
        String s4 = a2.getString(R.string.excessive_speeding);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(s4, "resources.getString(R.string.excessive_speeding)");
        this.summaryTextExcessiveSpeeding = s4;
        String s5 = a2.getString(R.string.high_g);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(s5, "resources.getString(R.string.high_g)");
        this.summaryTextHighG = s5;
        this.summaryTextColorNormal = a2.getColor(17170443);
        this.summaryTextColorWarning = a2.getColor(R.color.driving_score_warning_color);
        this.clearWarning = (Runnable)new com.navdy.hud.app.view.DriveScoreGaugePresenter$clearWarning$1(this);
        this.driveScoreUpdated = new com.navdy.hud.app.analytics.TelemetryDataManager$DriveScoreUpdated(com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent.NONE, false, false, false, 100);
    }
    
    final public static boolean access$getNewEvent$p(com.navdy.hud.app.view.DriveScoreGaugePresenter a) {
        return a.newEvent;
    }
    
    final public static void access$setNewEvent$p(com.navdy.hud.app.view.DriveScoreGaugePresenter a, boolean b) {
        a.setNewEvent(b);
    }
    
    final private void setNewEvent(boolean b) {
        this.newEvent = b;
    }
    
    final public long getCLEAR_WARNING_TIME_MS() {
        return this.CLEAR_WARNING_TIME_MS;
    }
    
    final public Runnable getClearWarning() {
        return this.clearWarning;
    }
    
    final public android.content.Context getContext() {
        return this.context;
    }
    
    public android.graphics.drawable.Drawable getDrawable() {
        return (this.showScore) ? this.driveScoreGaugeDrawable : null;
    }
    
    final public com.navdy.hud.app.view.drawable.DriveScoreGaugeDrawable getDriveScoreGaugeDrawable() {
        return this.driveScoreGaugeDrawable;
    }
    
    final public String getDriveScoreGaugeName() {
        return this.driveScoreGaugeName;
    }
    
    final public android.widget.TextView getDriveScoreText() {
        android.widget.TextView a = this.driveScoreText;
        if (a == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
        }
        return a;
    }
    
    final public com.navdy.hud.app.analytics.TelemetryDataManager$DriveScoreUpdated getDriveScoreUpdated() {
        return this.driveScoreUpdated;
    }
    
    final public android.os.Handler getHandler() {
        return this.handler;
    }
    
    final public int getLayoutId() {
        return this.layoutId;
    }
    
    final public boolean getShowScore() {
        return this.showScore;
    }
    
    final public android.widget.TextView getSummaryText() {
        android.widget.TextView a = this.summaryText;
        if (a == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
        }
        return a;
    }
    
    final public int getSummaryTextColorNormal() {
        return this.summaryTextColorNormal;
    }
    
    final public int getSummaryTextColorWarning() {
        return this.summaryTextColorWarning;
    }
    
    final public String getSummaryTextDriveScore() {
        return this.summaryTextDriveScore;
    }
    
    final public String getSummaryTextExcessiveSpeeding() {
        return this.summaryTextExcessiveSpeeding;
    }
    
    final public String getSummaryTextHardAcceleration() {
        return this.summaryTextHardAcceleration;
    }
    
    final public String getSummaryTextHardBraking() {
        return this.summaryTextHardBraking;
    }
    
    final public String getSummaryTextHighG() {
        return this.summaryTextHighG;
    }
    
    final public String getSummaryTextSpeeding() {
        return this.summaryTextSpeeding;
    }
    
    final public android.widget.ImageView getWarningImage() {
        android.widget.ImageView a = this.warningImage;
        if (a == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("warningImage");
        }
        return a;
    }
    
    public String getWidgetIdentifier() {
        return "DRIVE_SCORE_GAUGE_ID";
    }
    
    public String getWidgetName() {
        return this.driveScoreGaugeName;
    }
    
    protected boolean isRegisteringToBusRequired() {
        return true;
    }
    
    final public void onDriveScoreUpdated(com.navdy.hud.app.analytics.TelemetryDataManager$DriveScoreUpdated a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "driveScoreUpdated");
        this.setDriveScoreUpdated(a);
    }
    
    public void onResume() {
        this.setNewEvent(false);
        super.onResume();
    }
    
    final public void setClearWarning(Runnable a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.clearWarning = a;
    }
    
    final public void setDriveScoreGaugeDrawable(com.navdy.hud.app.view.drawable.DriveScoreGaugeDrawable a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.driveScoreGaugeDrawable = a;
    }
    
    final public void setDriveScoreText(android.widget.TextView a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.driveScoreText = a;
    }
    
    final public void setDriveScoreUpdated(com.navdy.hud.app.analytics.TelemetryDataManager$DriveScoreUpdated a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "value");
        this.driveScoreUpdated = a;
        if (kotlin.jvm.internal.Intrinsics.areEqual(a.getInterestingEvent(), com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent.NONE) ^ true) {
            this.setNewEvent(true);
        }
        this.reDraw();
    }
    
    final public void setSummaryText(android.widget.TextView a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.summaryText = a;
    }
    
    public void setView(com.navdy.hud.app.view.DashboardWidgetView a, android.os.Bundle a0) {
        int i = this.layoutId;
        if (a != null) {
            this.setDriveScoreUpdated(com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTelemetryDataManager().getDriveScoreUpdatedEvent());
            this.setNewEvent(false);
            a.setContentView(i);
            if (this.showScore) {
                android.view.View a1 = a.findViewById(R.id.txt_value);
                if (a1 == null) {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.TextView");
                }
                this.driveScoreText = (android.widget.TextView)a1;
            }
            android.view.View a2 = a.findViewById(R.id.txt_summary);
            if (a2 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.TextView");
            }
            this.summaryText = (android.widget.TextView)a2;
            android.view.View a3 = a.findViewById(R.id.img_warning);
            if (a3 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.ImageView");
            }
            this.warningImage = (android.widget.ImageView)a3;
        }
        super.setView(a, a0);
    }
    
    final public void setWarningImage(android.widget.ImageView a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.warningImage = a;
    }
    
    public void setWidgetVisibleToUser(boolean b) {
        this.setNewEvent(false);
        super.setWidgetVisibleToUser(b);
    }
    
    final public void showHighG() {
        if (this.showScore) {
            this.driveScoreGaugeDrawable.setState(1);
            android.widget.TextView a = this.driveScoreText;
            if (a == null) {
                kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
            }
            a.setVisibility(4);
        }
        android.widget.ImageView a0 = this.warningImage;
        if (a0 == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("warningImage");
        }
        a0.setVisibility(0);
        android.widget.TextView a1 = this.summaryText;
        if (a1 == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
        }
        a1.setTextColor(this.summaryTextColorWarning);
        android.widget.ImageView a2 = this.warningImage;
        if (a2 == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("warningImage");
        }
        a2.setImageResource(R.drawable.high_g);
        android.widget.TextView a3 = this.summaryText;
        if (a3 == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
        }
        a3.setText((CharSequence)this.summaryTextHighG);
    }
    
    final public void showSpeeding() {
        if (this.showScore) {
            this.driveScoreGaugeDrawable.setState(1);
            android.widget.TextView a = this.driveScoreText;
            if (a == null) {
                kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
            }
            a.setVisibility(4);
        }
        android.widget.ImageView a0 = this.warningImage;
        if (a0 == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("warningImage");
        }
        a0.setVisibility(0);
        android.widget.TextView a1 = this.summaryText;
        if (a1 == null) {
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
        }
        a1.setTextColor(this.summaryTextColorWarning);
        if (this.driveScoreUpdated.isExcessiveSpeeding()) {
            android.widget.ImageView a2 = this.warningImage;
            if (a2 == null) {
                kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("warningImage");
            }
            a2.setImageResource(R.drawable.excessive_speed);
            android.widget.TextView a3 = this.summaryText;
            if (a3 == null) {
                kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
            }
            a3.setText((CharSequence)this.summaryTextExcessiveSpeeding);
        } else {
            android.widget.ImageView a4 = this.warningImage;
            if (a4 == null) {
                kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("warningImage");
            }
            a4.setImageResource(R.drawable.speed);
            android.widget.TextView a5 = this.summaryText;
            if (a5 == null) {
                kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
            }
            a5.setText((CharSequence)this.summaryTextSpeeding);
        }
    }
    
    protected void updateGauge() {
        this.logger.d(new StringBuilder().append("Update Drive score ").append(this.driveScoreUpdated).toString());
        if (this.mWidgetView != null) {
            if (this.showScore) {
                this.driveScoreGaugeDrawable.setGaugeValue((float)this.driveScoreUpdated.getDriveScore());
            }
            if (this.newEvent) {
                if (this.showScore) {
                    this.driveScoreGaugeDrawable.setState(1);
                    android.widget.TextView a = this.driveScoreText;
                    if (a == null) {
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
                    }
                    a.setVisibility(4);
                }
                android.widget.ImageView a0 = this.warningImage;
                if (a0 == null) {
                    kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("warningImage");
                }
                a0.setVisibility(0);
                android.widget.TextView a1 = this.summaryText;
                if (a1 == null) {
                    kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                }
                a1.setTextColor(this.summaryTextColorWarning);
                if (kotlin.jvm.internal.Intrinsics.areEqual(this.driveScoreUpdated.getInterestingEvent(), com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent.HARD_ACCELERATION)) {
                    android.widget.ImageView a2 = this.warningImage;
                    if (a2 == null) {
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("warningImage");
                    }
                    a2.setImageResource(R.drawable.accel);
                    android.widget.TextView a3 = this.summaryText;
                    if (a3 == null) {
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                    }
                    a3.setText((CharSequence)this.summaryTextHardAcceleration);
                } else if (kotlin.jvm.internal.Intrinsics.areEqual(this.driveScoreUpdated.getInterestingEvent(), com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent.HARD_BRAKING)) {
                    android.widget.ImageView a4 = this.warningImage;
                    if (a4 == null) {
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("warningImage");
                    }
                    a4.setImageResource(R.drawable.brake);
                    android.widget.TextView a5 = this.summaryText;
                    if (a5 == null) {
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                    }
                    a5.setText((CharSequence)this.summaryTextHardBraking);
                } else if (this.driveScoreUpdated.isDoingHighGManeuver()) {
                    this.showHighG();
                } else if (this.driveScoreUpdated.isSpeeding()) {
                    this.showSpeeding();
                }
                this.handler.removeCallbacks(this.clearWarning);
                this.handler.postDelayed(this.clearWarning, this.CLEAR_WARNING_TIME_MS);
            } else if (this.driveScoreUpdated.isDoingHighGManeuver()) {
                this.showHighG();
            } else if (this.driveScoreUpdated.isSpeeding()) {
                this.showSpeeding();
            } else {
                this.handler.removeCallbacks(this.clearWarning);
                if (this.showScore) {
                    this.driveScoreGaugeDrawable.setState(0);
                    android.widget.TextView a6 = this.driveScoreText;
                    if (a6 == null) {
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
                    }
                    a6.setVisibility(0);
                    android.widget.TextView a7 = this.driveScoreText;
                    if (a7 == null) {
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
                    }
                    a7.setText((CharSequence)String.valueOf(this.driveScoreUpdated.getDriveScore()));
                    android.widget.TextView a8 = this.summaryText;
                    if (a8 == null) {
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                    }
                    a8.setTextColor(this.summaryTextColorNormal);
                    android.widget.TextView a9 = this.summaryText;
                    if (a9 == null) {
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                    }
                    a9.setText((CharSequence)this.summaryTextDriveScore);
                } else {
                    android.widget.TextView a10 = this.summaryText;
                    if (a10 == null) {
                        kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                    }
                    a10.setText((CharSequence)null);
                }
                android.widget.ImageView a11 = this.warningImage;
                if (a11 == null) {
                    kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException("warningImage");
                }
                a11.setVisibility(4);
            }
        }
    }
}
