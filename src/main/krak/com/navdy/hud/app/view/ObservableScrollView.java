package com.navdy.hud.app.view;

public class ObservableScrollView extends android.widget.ScrollView {
    private android.view.View child;
    private com.navdy.hud.app.view.ObservableScrollView$IScrollListener listener;
    
    public ObservableScrollView(android.content.Context a) {
        super(a);
    }
    
    public ObservableScrollView(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
    }
    
    public ObservableScrollView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
    }
    
    protected void onScrollChanged(int i, int i0, int i1, int i2) {
        if (this.child == null) {
            this.child = this.getChildAt(0);
        }
        if (this.child != null) {
            if (i0 > 0) {
                if (this.child.getBottom() - (this.getHeight() + this.getScrollY()) != 0) {
                    if (this.listener != null) {
                        this.listener.onScroll(i, i0, i1, i2);
                    }
                    super.onScrollChanged(i, i0, i1, i2);
                } else {
                    if (this.listener != null) {
                        this.listener.onBottom();
                    }
                    super.onScrollChanged(i, i0, i1, i2);
                }
            } else {
                if (this.listener != null) {
                    this.listener.onTop();
                }
                super.onScrollChanged(i, i0, i1, i2);
            }
        } else {
            super.onScrollChanged(i, i0, i1, i2);
        }
    }
    
    public void setScrollListener(com.navdy.hud.app.view.ObservableScrollView$IScrollListener a) {
        this.listener = a;
    }
}
