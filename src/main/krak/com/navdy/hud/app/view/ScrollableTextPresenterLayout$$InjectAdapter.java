package com.navdy.hud.app.view;

final public class ScrollableTextPresenterLayout$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding mPresenter;
    
    public ScrollableTextPresenterLayout$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.view.ScrollableTextPresenterLayout", false, com.navdy.hud.app.view.ScrollableTextPresenterLayout.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.mPresenter = a.requestBinding("com.navdy.hud.app.screen.GestureLearningScreen$Presenter", com.navdy.hud.app.view.ScrollableTextPresenterLayout.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.mPresenter);
    }
    
    public void injectMembers(com.navdy.hud.app.view.ScrollableTextPresenterLayout a) {
        a.mPresenter = (com.navdy.hud.app.screen.GestureLearningScreen$Presenter)this.mPresenter.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.view.ScrollableTextPresenterLayout)a);
    }
}
