package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class CompassDrawable extends com.navdy.hud.app.view.drawable.GaugeDrawable {
    final private static double SEGMENT_ANGLE = 22.5;
    final private static int TEXT_PADDING = 6;
    final private static int TOTAL_SEGMENTS = 16;
    private static String[] headings;
    private static int majorTickHeight;
    private static int minorTickHeight;
    private static int[] widths;
    private android.graphics.drawable.Drawable mIndicatorDrawable;
    private int mIndicatorHeight;
    private int mIndicatorTopMargin;
    private int mIndicatorWidth;
    private android.graphics.Path mPath;
    private int mStripHeight;
    private int mStripTopMargin;
    private int mStripWidth;
    private android.graphics.Paint mTextPaint;
    
    static {
        majorTickHeight = 10;
        minorTickHeight = 5;
    }
    
    public CompassDrawable(android.content.Context a) {
        super(a, 0);
        android.content.res.Resources a0 = a.getResources();
        this.mIndicatorDrawable = a0.getDrawable(R.drawable.icon_gauge_compass_heading_indicator);
        this.mIndicatorWidth = a0.getDimensionPixelSize(R.dimen.compass_indicator_width);
        this.mIndicatorHeight = a0.getDimensionPixelSize(R.dimen.compass_indicator_height);
        this.mIndicatorTopMargin = a0.getDimensionPixelSize(R.dimen.compass_indicator_top_margin);
        this.mStripWidth = a0.getDimensionPixelSize(R.dimen.compass_strip_width);
        this.mStripTopMargin = a0.getDimensionPixelSize(R.dimen.compass_strip_margin_top);
        this.mStripHeight = a0.getDimensionPixelSize(R.dimen.compass_strip_height);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStyle(android.graphics.Paint$Style.FILL);
        this.mPaint.setColor(a0.getColor(R.color.compass_frame_color));
        this.mTextPaint = new android.graphics.Paint();
        this.mTextPaint.setStrokeWidth(0.0f);
        this.mTextPaint.setColor(-1);
        this.mTextPaint.setAntiAlias(false);
        this.mTextPaint.setTextAlign(android.graphics.Paint$Align.LEFT);
        this.mTextPaint.setTypeface(android.graphics.Typeface.create("sans-serif", 1));
        this.mTextPaint.setTextSize((float)a0.getDimensionPixelSize(R.dimen.compass_text_height));
        this.mPath = new android.graphics.Path();
        android.graphics.Rect a1 = new android.graphics.Rect();
        if (headings == null) {
            majorTickHeight = a0.getDimensionPixelSize(R.dimen.compass_major_tick_height);
            minorTickHeight = a0.getDimensionPixelSize(R.dimen.compass_minor_tick_height);
            headings = a0.getStringArray(R.array.compass_points);
            widths = new int[headings.length];
            int i = 0;
            while(i < widths.length) {
                this.mTextPaint.getTextBounds(headings[i], 0, headings[i].length(), a1);
                widths[i] = a1.width();
                i = i + 1;
            }
        }
    }
    
    private void drawCompassPoints(android.graphics.Canvas a, float f, int i, int i0, int i1, int i2, boolean b) {
        int i3 = i1 - i;
        float f0 = (float)(i3 / 2 + i);
        int i4 = this.mStripWidth / 16;
        int i5 = (int)Math.ceil((double)(i3 / i4)) + 1;
        float f1 = f % 360f / 22.5f;
        int i6 = (int)f1;
        float f2 = (float)i6;
        int i7 = -i5 / 2;
        int i8 = i5 / 2;
        while(i7 < i8) {
            int i9 = (int)((double)(((float)i7 + (f1 - f2)) * (float)i4 + f0) + 0.5);
            int i10 = (i6 - i7 + 16) % 16;
            boolean b0 = (i10 & 1) == 0;
            int i11 = b0 ? majorTickHeight : minorTickHeight;
            this.mTextPaint.setAntiAlias(false);
            a.drawLine((float)i9, (float)(i2 - i11), (float)i9, (float)i2, this.mTextPaint);
            if (b0 && b) {
                int i12 = i10 / 2;
                String s = headings[i12];
                this.mTextPaint.setAntiAlias(true);
                a.drawText(s, (float)(i9 - widths[i12] / 2), (float)(i2 - i11 - 6), this.mTextPaint);
            }
            i7 = i7 + 1;
        }
    }
    
    public void draw(android.graphics.Canvas a) {
        super.draw(a);
        android.graphics.Rect a0 = this.getBounds();
        int i = a0.width();
        int i0 = a0.height();
        a.clipRect(a0);
        a.drawArc(0.0f, 0.0f, (float)i, (float)i0, 0.0f, 360f, true, this.mPaint);
        int i1 = a0.left;
        int i2 = i / 2;
        int i3 = this.mIndicatorWidth / 2;
        int i4 = a0.top + this.mIndicatorTopMargin;
        int i5 = a0.left;
        int i6 = i / 2;
        int i7 = this.mIndicatorWidth / 2;
        int i8 = this.mIndicatorHeight;
        this.mIndicatorDrawable.setBounds(i1 + i2 - i3, i4, i5 + i6 + i7, i4 + i8);
        this.mIndicatorDrawable.draw(a);
        a.clipPath(this.mPath);
        int i9 = this.mStripTopMargin;
        int i10 = this.mStripTopMargin;
        int i11 = this.mStripHeight;
        this.drawCompassPoints(a, this.mValue, 0, i9, i, i10 + i11, this.mValue != 0.0f);
    }
    
    protected void onBoundsChange(android.graphics.Rect a) {
        super.onBoundsChange(a);
        this.mPath.reset();
        this.mPath.addOval(0.0f, 0.0f, (float)a.width(), (float)a.height(), android.graphics.Path$Direction.CW);
    }
}
