package com.navdy.hud.app.view;
import com.navdy.hud.app.R;


    public enum GestureVideoCaptureView$State {
        NOT_PAIRED(0),
    SWIPE_LEFT(1),
    SWIPE_RIGHT(2),
    DOUBLE_TAP_1(3),
    DOUBLE_TAP_2(4),
    SAVING(5),
    UPLOADING(6),
    ALL_DONE(7),
    UPLOADING_FAILURE(8);

        private int value;
        GestureVideoCaptureView$State(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class GestureVideoCaptureView$State extends Enum {
//    final private static com.navdy.hud.app.view.GestureVideoCaptureView$State[] $VALUES;
//    final public static com.navdy.hud.app.view.GestureVideoCaptureView$State ALL_DONE;
//    final public static com.navdy.hud.app.view.GestureVideoCaptureView$State DOUBLE_TAP_1;
//    final public static com.navdy.hud.app.view.GestureVideoCaptureView$State DOUBLE_TAP_2;
//    final public static com.navdy.hud.app.view.GestureVideoCaptureView$State NOT_PAIRED;
//    final public static com.navdy.hud.app.view.GestureVideoCaptureView$State SAVING;
//    final public static com.navdy.hud.app.view.GestureVideoCaptureView$State SWIPE_LEFT;
//    final public static com.navdy.hud.app.view.GestureVideoCaptureView$State SWIPE_RIGHT;
//    final public static com.navdy.hud.app.view.GestureVideoCaptureView$State UPLOADING;
//    final public static com.navdy.hud.app.view.GestureVideoCaptureView$State UPLOADING_FAILURE;
//    int detailText;
//    String filename;
//    int mainText;
//    
//    static {
//        NOT_PAIRED = new com.navdy.hud.app.view.GestureVideoCaptureView$State("NOT_PAIRED", 0, R.string.please_pair_phone, R.string.please_pair_phone_detail, (String)null);
//        SWIPE_LEFT = new com.navdy.hud.app.view.GestureVideoCaptureView$State("SWIPE_LEFT", 1, R.string.swipe_left, 0, "swipe-left");
//        SWIPE_RIGHT = new com.navdy.hud.app.view.GestureVideoCaptureView$State("SWIPE_RIGHT", 2, R.string.swipe_right, 0, "swipe-right");
//        DOUBLE_TAP_1 = new com.navdy.hud.app.view.GestureVideoCaptureView$State("DOUBLE_TAP_1", 3, R.string.double_tap_1, R.string.double_tap_detail_1, "tap");
//        DOUBLE_TAP_2 = new com.navdy.hud.app.view.GestureVideoCaptureView$State("DOUBLE_TAP_2", 4, R.string.double_tap_2, R.string.double_tap_detail_2, "tap2");
//        SAVING = new com.navdy.hud.app.view.GestureVideoCaptureView$State("SAVING", 5, 0, R.string.saving_recording, (String)null);
//        UPLOADING = new com.navdy.hud.app.view.GestureVideoCaptureView$State("UPLOADING", 6, R.string.uploading, R.string.uploading_detail, (String)null);
//        ALL_DONE = new com.navdy.hud.app.view.GestureVideoCaptureView$State("ALL_DONE", 7, R.string.uploading_success, 0, (String)null);
//        UPLOADING_FAILURE = new com.navdy.hud.app.view.GestureVideoCaptureView$State("UPLOADING_FAILURE", 8, R.string.upload_failure, R.string.upload_failure_detail, (String)null);
//        com.navdy.hud.app.view.GestureVideoCaptureView$State[] a = new com.navdy.hud.app.view.GestureVideoCaptureView$State[9];
//        a[0] = NOT_PAIRED;
//        a[1] = SWIPE_LEFT;
//        a[2] = SWIPE_RIGHT;
//        a[3] = DOUBLE_TAP_1;
//        a[4] = DOUBLE_TAP_2;
//        a[5] = SAVING;
//        a[6] = UPLOADING;
//        a[7] = ALL_DONE;
//        a[8] = UPLOADING_FAILURE;
//        $VALUES = a;
//    }
//    
//    private GestureVideoCaptureView$State(String s, int i, int i0, int i1, String s0) {
//        super(s, i);
//        this.mainText = i0;
//        this.detailText = i1;
//        this.filename = s0;
//    }
//    
//    public static com.navdy.hud.app.view.GestureVideoCaptureView$State valueOf(String s) {
//        return (com.navdy.hud.app.view.GestureVideoCaptureView$State)Enum.valueOf(com.navdy.hud.app.view.GestureVideoCaptureView$State.class, s);
//    }
//    
//    public static com.navdy.hud.app.view.GestureVideoCaptureView$State[] values() {
//        return $VALUES.clone();
//    }
//}
//