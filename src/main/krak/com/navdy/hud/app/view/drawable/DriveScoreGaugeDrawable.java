package com.navdy.hud.app.view.drawable;
import com.navdy.hud.app.R;

final public class DriveScoreGaugeDrawable extends com.navdy.hud.app.view.drawable.GaugeDrawable {
    final private float START_ANGLE;
    final private float SWEEP_ANGLE;
    final private int backgroundColor;
    final private android.content.Context context;
    final private int driveScoreGauge_Width;
    final private int warningColor;
    
    public DriveScoreGaugeDrawable(android.content.Context a, int i) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        super(a, 0, i);
        this.context = a;
        this.START_ANGLE = 157.5f;
        this.SWEEP_ANGLE = 225f;
        this.driveScoreGauge_Width = this.context.getResources().getDimensionPixelSize(R.dimen.drive_score_gauge_width);
        this.mDefaultColor = this.mColorTable[0];
        this.warningColor = this.mColorTable[1];
        this.backgroundColor = this.mColorTable[2];
    }
    
    public void draw(android.graphics.Canvas a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "canvas");
        super.draw(a);
        android.graphics.Rect a0 = this.getBounds();
        android.graphics.RectF a1 = new android.graphics.RectF((float)a0.left, (float)a0.top, (float)a0.right, (float)a0.bottom);
        a1.inset((float)this.driveScoreGauge_Width / (float)2, (float)this.driveScoreGauge_Width / (float)2);
        this.mPaint.setStyle(android.graphics.Paint$Style.STROKE);
        this.mPaint.setStrokeWidth((float)this.driveScoreGauge_Width);
        this.mPaint.setColor(this.backgroundColor);
        a.drawArc(a1, this.START_ANGLE, this.SWEEP_ANGLE, false, this.mPaint);
        int i = (int)(this.mValue / this.mMaxValue * this.SWEEP_ANGLE);
        float f = this.mValue;
        if (kotlin.ranges.RangesKt.intRangeContains((kotlin.ranges.ClosedRange)new kotlin.ranges.IntRange(1, 99), f)) {
            this.mPaint.setColor(this.mDefaultColor);
            a.drawArc(a1, this.START_ANGLE, (float)i - 5f, false, this.mPaint);
            this.mPaint.setColor(-16777216);
            a.drawArc(a1, this.START_ANGLE + (float)i - 5f, 5f, false, this.mPaint);
        } else if (f == 100f) {
            this.mPaint.setColor(this.mDefaultColor);
            a.drawArc(a1, this.START_ANGLE, (float)i, false, this.mPaint);
        }
    }
}
