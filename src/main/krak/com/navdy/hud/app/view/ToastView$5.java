package com.navdy.hud.app.view;

class ToastView$5 {
    final static int[] $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
    final static int[] $SwitchMap$com$navdy$service$library$events$input$Gesture;
    
    static {
        $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
        com.navdy.hud.app.manager.InputManager$CustomKeyEvent a0 = com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LEFT;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.SELECT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.POWER_BUTTON_LONG_PRESS.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LONG_PRESS.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        $SwitchMap$com$navdy$service$library$events$input$Gesture = new int[com.navdy.service.library.events.input.Gesture.values().length];
        int[] a1 = $SwitchMap$com$navdy$service$library$events$input$Gesture;
        com.navdy.service.library.events.input.Gesture a2 = com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_LEFT;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$input$Gesture[com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException5) {
        }
    }
}
