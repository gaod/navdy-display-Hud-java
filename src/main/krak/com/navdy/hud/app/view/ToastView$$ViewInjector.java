package com.navdy.hud.app.view;

public class ToastView$$ViewInjector {
    public ToastView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.ToastView a0, Object a1) {
        a0.mainView = (com.navdy.hud.app.ui.component.ConfirmationLayout)a.findRequiredView(a1, R.id.mainView, "field 'mainView'");
    }
    
    public static void reset(com.navdy.hud.app.view.ToastView a) {
        a.mainView = null;
    }
}
