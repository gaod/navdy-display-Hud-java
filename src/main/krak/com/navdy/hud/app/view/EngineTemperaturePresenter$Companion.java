package com.navdy.hud.app.view;

final public class EngineTemperaturePresenter$Companion {
    private EngineTemperaturePresenter$Companion() {
    }
    
    public EngineTemperaturePresenter$Companion(kotlin.jvm.internal.DefaultConstructorMarker a) {
        this();
    }
    
    final public double getTEMPERATURE_GAUGE_COLD_THRESHOLD() {
        return com.navdy.hud.app.view.EngineTemperaturePresenter.access$getTEMPERATURE_GAUGE_COLD_THRESHOLD$cp();
    }
    
    final public double getTEMPERATURE_GAUGE_HOT_THRESHOLD() {
        return com.navdy.hud.app.view.EngineTemperaturePresenter.access$getTEMPERATURE_GAUGE_HOT_THRESHOLD$cp();
    }
    
    final public double getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS() {
        return com.navdy.hud.app.view.EngineTemperaturePresenter.access$getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS$cp();
    }
    
    final public double getTEMPERATURE_GAUGE_MID_POINT_CELSIUS() {
        return com.navdy.hud.app.view.EngineTemperaturePresenter.access$getTEMPERATURE_GAUGE_MID_POINT_CELSIUS$cp();
    }
    
    final public double getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS() {
        return com.navdy.hud.app.view.EngineTemperaturePresenter.access$getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS$cp();
    }
}
