package com.navdy.hud.app.ui.component.carousel;

abstract public interface AnimationStrategy {
    abstract public android.animation.AnimatorSet buildLayoutAnimation(android.animation.Animator$AnimatorListener arg, com.navdy.hud.app.ui.component.carousel.CarouselLayout arg0, int arg1, int arg2);
    
    
    abstract public android.animation.AnimatorSet createHiddenViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout arg, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction arg0);
    
    
    abstract public android.animation.AnimatorSet createMiddleLeftViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout arg, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction arg0);
    
    
    abstract public android.animation.AnimatorSet createMiddleRightViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout arg, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction arg0);
    
    
    abstract public android.animation.AnimatorSet createNewMiddleRightViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout arg, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction arg0);
    
    
    abstract public android.animation.AnimatorSet createSideViewToMiddleAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout arg, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction arg0);
    
    
    abstract public android.animation.Animator createViewOutAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout arg, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction arg0);
}
