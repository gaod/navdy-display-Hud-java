package com.navdy.hud.app.ui.component.carousel;

abstract public interface IProgressIndicator {
    abstract public int getCurrentItem();
    
    
    abstract public android.animation.AnimatorSet getItemMoveAnimator(int arg, int arg0);
    
    
    abstract public android.graphics.RectF getItemPos(int arg);
    
    
    abstract public void setCurrentItem(int arg);
    
    
    abstract public void setCurrentItem(int arg, int arg0);
    
    
    abstract public void setItemCount(int arg);
    
    
    abstract public void setOrientation(com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation arg);
}
