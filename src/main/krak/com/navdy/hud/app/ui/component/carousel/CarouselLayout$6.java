package com.navdy.hud.app.ui.component.carousel;

class CarouselLayout$6 implements Runnable {
    final com.navdy.hud.app.ui.component.carousel.CarouselLayout this$0;
    final boolean val$keyPress;
    final android.animation.Animator$AnimatorListener val$listener;
    final boolean val$next;
    final com.navdy.hud.app.ui.component.carousel.CarouselLayout$OperationInfo val$operationInfo;
    
    CarouselLayout$6(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, android.animation.Animator$AnimatorListener a0, boolean b, boolean b0, com.navdy.hud.app.ui.component.carousel.CarouselLayout$OperationInfo a1) {
        super();
        this.this$0 = a;
        this.val$listener = a0;
        this.val$next = b;
        this.val$keyPress = b0;
        this.val$operationInfo = a1;
    }
    
    public void run() {
        com.navdy.hud.app.ui.component.carousel.CarouselLayout.access$300(this.this$0, this.val$listener, this.val$next, this.val$keyPress, true, this.val$operationInfo.animationDuration);
    }
}
