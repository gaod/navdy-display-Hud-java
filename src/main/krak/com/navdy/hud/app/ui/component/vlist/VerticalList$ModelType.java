package com.navdy.hud.app.ui.component.vlist;


public enum VerticalList$ModelType {
    BLANK(0),
    TITLE(1),
    TITLE_SUBTITLE(2),
    ICON_BKCOLOR(3),
    TWO_ICONS(4),
    ICON(5),
    LOADING(6),
    ICON_OPTIONS(7),
    SCROLL_CONTENT(8),
    LOADING_CONTENT(9),
    SWITCH(10);

    private int value;
    VerticalList$ModelType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class VerticalList$ModelType extends Enum {
//    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType BLANK;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType ICON;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType ICON_BKCOLOR;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType ICON_OPTIONS;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType LOADING;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType LOADING_CONTENT;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType SCROLL_CONTENT;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType SWITCH;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType TITLE;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType TITLE_SUBTITLE;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType TWO_ICONS;
//    
//    static {
//        BLANK = new com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType("BLANK", 0);
//        TITLE = new com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType("TITLE", 1);
//        TITLE_SUBTITLE = new com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType("TITLE_SUBTITLE", 2);
//        ICON_BKCOLOR = new com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType("ICON_BKCOLOR", 3);
//        TWO_ICONS = new com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType("TWO_ICONS", 4);
//        ICON = new com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType("ICON", 5);
//        LOADING = new com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType("LOADING", 6);
//        ICON_OPTIONS = new com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType("ICON_OPTIONS", 7);
//        SCROLL_CONTENT = new com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType("SCROLL_CONTENT", 8);
//        LOADING_CONTENT = new com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType("LOADING_CONTENT", 9);
//        SWITCH = new com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType("SWITCH", 10);
//        com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType[] a = new com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType[11];
//        a[0] = BLANK;
//        a[1] = TITLE;
//        a[2] = TITLE_SUBTITLE;
//        a[3] = ICON_BKCOLOR;
//        a[4] = TWO_ICONS;
//        a[5] = ICON;
//        a[6] = LOADING;
//        a[7] = ICON_OPTIONS;
//        a[8] = SCROLL_CONTENT;
//        a[9] = LOADING_CONTENT;
//        a[10] = SWITCH;
//        $VALUES = a;
//    }
//    
//    private VerticalList$ModelType(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType)Enum.valueOf(com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType[] values() {
//        return $VALUES.clone();
//    }
//}
//