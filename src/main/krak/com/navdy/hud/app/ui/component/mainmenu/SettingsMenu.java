package com.navdy.hud.app.ui.component.mainmenu;
import com.navdy.hud.app.R;

public class SettingsMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model back;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model brightness;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model connectPhone;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model dialFwUpdate;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model displayFwUpdate;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model factoryReset;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model learningGestures;
    final private static android.content.res.Resources resources;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static String setting;
    final private static int settingColor;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model shutdown;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model systemInfo;
    private int backSelection;
    private int backSelectionId;
    private com.squareup.otto.Bus bus;
    private java.util.List cachedList;
    private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter presenter;
    private com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu systemInfoMenu;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.SettingsMenu.class);
        resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        settingColor = resources.getColor(R.color.mm_settings);
        setting = resources.getString(R.string.carousel_menu_settings_title);
        String s = resources.getString(R.string.back);
        int i = resources.getColor(R.color.mm_back);
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, i, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i, s, (String)null);
        String s0 = resources.getString(R.string.carousel_settings_brightness);
        int i0 = resources.getColor(R.color.mm_settings_brightness);
        brightness = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.settings_menu_brightness, R.drawable.icon_settings_brightness_2, i0, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i0, s0, (String)null);
        String s1 = resources.getString(R.string.carousel_settings_connect_phone_title);
        int i1 = resources.getColor(R.color.mm_connnect_phone);
        connectPhone = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.settings_menu_connect_phone, R.drawable.icon_settings_connect_phone_2, i1, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i1, s1, (String)null);
        String s2 = resources.getString(R.string.carousel_settings_learning_gesture_title);
        int i2 = resources.getColor(R.color.mm_settings_learn_gestures);
        learningGestures = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.settings_menu_learning_gestures, R.drawable.icon_settings_learn_gestures_2, i2, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i2, s2, (String)null);
        String s3 = resources.getString(R.string.carousel_settings_software_update_title);
        int i3 = resources.getColor(R.color.mm_settings_update_display);
        displayFwUpdate = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.settings_menu_software_update, R.drawable.icon_software_update_2, i3, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i3, s3, (String)null);
        String s4 = resources.getString(R.string.carousel_settings_dial_update_title);
        int i4 = resources.getColor(R.color.mm_settings_update_dial);
        dialFwUpdate = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.settings_menu_dial_update, R.drawable.icon_dial_update_2, i4, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i4, s4, (String)null);
        String s5 = resources.getString(R.string.carousel_settings_shutdown_title);
        int i5 = resources.getColor(R.color.mm_settings_shutdown);
        shutdown = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.settings_menu_shutdown, R.drawable.icon_settings_shutdown_2, i5, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i5, s5, (String)null);
        String s6 = resources.getString(R.string.carousel_menu_system_info_title);
        systemInfo = com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(R.id.main_menu_system_info, R.drawable.icon_settings_navdy_data, R.drawable.icon_settings_navdy_data_sm, resources.getColor(R.color.mm_settings_sys_info), -1, s6, (String)null);
        String s7 = resources.getString(R.string.carousel_settings_factory_reset_title);
        int i6 = resources.getColor(R.color.mm_settings_factory_reset);
        factoryReset = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.settings_menu_factory_reset, R.drawable.icon_settings_factory_reset_2, i6, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i6, s7, (String)null);
    }
    
    public SettingsMenu(com.squareup.otto.Bus a, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a0, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a1, com.navdy.hud.app.ui.component.mainmenu.IMenu a2) {
        this.bus = a;
        this.vscrollComponent = a0;
        this.presenter = a1;
        this.parent = a2;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu a, String s, String s0) {
        return null;
    }
    
    public int getInitialSelection() {
        return 1;
    }
    
    public java.util.List getItems() {
        Object a = null;
        if (this.cachedList == null) {
            java.util.ArrayList a0 = new java.util.ArrayList();
            ((java.util.List)a0).add(back);
            ((java.util.List)a0).add(brightness);
            ((java.util.List)a0).add(connectPhone);
            ((java.util.List)a0).add(learningGestures);
            if (com.navdy.hud.app.util.OTAUpdateService.isUpdateAvailable()) {
                ((java.util.List)a0).add(displayFwUpdate);
            }
            com.navdy.hud.app.device.dial.DialManager a1 = com.navdy.hud.app.device.dial.DialManager.getInstance();
            if (a1.isDialConnected() && a1.getDialFirmwareUpdater().getVersions().isUpdateAvailable()) {
                ((java.util.List)a0).add(dialFwUpdate);
            }
            ((java.util.List)a0).add(shutdown);
            ((java.util.List)a0).add(factoryReset);
            ((java.util.List)a0).add(systemInfo);
            this.cachedList = (java.util.List)a0;
            a = a0;
        } else {
            a = this.cachedList;
        }
        return (java.util.List)a;
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$Model getModelfromPos(int i) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = null;
        java.util.List a0 = this.cachedList;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (this.cachedList.size() > i) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.cachedList.get(i);
        }
        return a;
    }
    
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.SETTINGS;
    }
    
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    public boolean isItemClickable(int i, int i0) {
        return true;
    }
    
    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, android.view.View a0, int i, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a1) {
    }
    
    public void onFastScrollEnd() {
    }
    
    public void onFastScrollStart() {
    }
    
    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
    }
    
    public void onScrollIdle() {
    }
    
    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel a) {
    }
    
    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        sLogger.v(new StringBuilder().append("select id:").append(a.id).append(" pos:").append(a.pos).toString());
        switch(a.id) {
            case R.id.settings_menu_software_update: {
                sLogger.v("software update");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("software_update");
                android.os.Bundle a0 = new android.os.Bundle();
                a0.putBoolean("UPDATE_REMINDER", false);
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_OTA_CONFIRMATION, a0, false));
                break;
            }
            case R.id.settings_menu_shutdown: {
                sLogger.v("shutdown");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("shutdown");
                android.os.Bundle a1 = com.navdy.hud.app.event.Shutdown$Reason.MENU.asBundle();
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_SHUTDOWN_CONFIRMATION, a1, false));
                break;
            }
            case R.id.settings_menu_learning_gestures: {
                sLogger.v("Learning gestures");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("learning_gestures");
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_GESTURE_LEARNING));
                break;
            }
            case R.id.settings_menu_factory_reset: {
                sLogger.v("factory reset");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("factory_reset");
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_FACTORY_RESET));
                break;
            }
            case R.id.settings_menu_dial_update: {
                sLogger.v("dial update");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("dial_update");
                android.os.Bundle a2 = new android.os.Bundle();
                a2.putBoolean("UPDATE_REMINDER", false);
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_UPDATE_CONFIRMATION, a2, false));
                break;
            }
            case R.id.settings_menu_dial_pairing: {
                sLogger.v("Dial pairing");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("dial_pairing");
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING));
                break;
            }
            case R.id.settings_menu_connect_phone: {
                sLogger.v("connect phone");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("connect_phone");
                android.os.Bundle a3 = new android.os.Bundle();
                a3.putString(com.navdy.hud.app.screen.WelcomeScreen.ARG_ACTION, com.navdy.hud.app.screen.WelcomeScreen.ACTION_SWITCH_PHONE);
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME, a3, false));
                break;
            }
            case R.id.settings_menu_brightness: {
                sLogger.v("brightness");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("brightness");
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_BRIGHTNESS));
                break;
            }
            case R.id.settings_menu_auto_brightness: {
                sLogger.v("auto brightness");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("auto_brightness");
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_AUTO_BRIGHTNESS));
                break;
            }
            case R.id.menu_back: {
                sLogger.v("back");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                this.backSelectionId = 0;
                break;
            }
            case R.id.main_menu_system_info: {
                sLogger.v("system info");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("system_info");
                if (this.systemInfoMenu == null) {
                    this.systemInfoMenu = new com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu(this.bus, this.vscrollComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this);
                }
                this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.systemInfoMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 1, true);
                break;
            }
        }
        return true;
    }
    
    public void setBackSelectionId(int i) {
        this.backSelectionId = i;
    }
    
    public void setBackSelectionPos(int i) {
        this.backSelection = i;
    }
    
    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_mm_settings_2, settingColor, (android.graphics.Shader)null, 1f);
        this.vscrollComponent.selectedText.setText((CharSequence)setting);
    }
    
    public void showToolTip() {
    }
}
