package com.navdy.hud.app.ui.component.homescreen;

class TbtShrunkDistanceView$1 {
    final static int[] $SwitchMap$com$navdy$hud$app$ui$component$homescreen$TbtShrunkDistanceView$Mode;
    final static int[] $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode;
    
    static {
        $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode = new int[com.navdy.hud.app.view.MainView$CustomAnimationMode.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode;
        com.navdy.hud.app.view.MainView$CustomAnimationMode a0 = com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        $SwitchMap$com$navdy$hud$app$ui$component$homescreen$TbtShrunkDistanceView$Mode = new int[com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$ui$component$homescreen$TbtShrunkDistanceView$Mode;
        com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode a2 = com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode.DISTANCE_NUMBER;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$homescreen$TbtShrunkDistanceView$Mode[com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode.PROGRESS_BAR.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException2) {
        }
    }
}
