package com.navdy.hud.app.ui.component.dashboard;


    public enum GaugeViewPager$Operation {
        NEXT(0),
        PREVIOUS(1);

        private int value;
        GaugeViewPager$Operation(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class GaugeViewPager$Operation extends Enum {
//    final private static com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation NEXT;
//    final public static com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation PREVIOUS;
//    
//    static {
//        NEXT = new com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation("NEXT", 0);
//        PREVIOUS = new com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation("PREVIOUS", 1);
//        com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation[] a = new com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation[2];
//        a[0] = NEXT;
//        a[1] = PREVIOUS;
//        $VALUES = a;
//    }
//    
//    private GaugeViewPager$Operation(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation)Enum.valueOf(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation[] values() {
//        return $VALUES.clone();
//    }
//}
//