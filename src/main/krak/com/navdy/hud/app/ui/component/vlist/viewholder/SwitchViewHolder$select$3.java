package com.navdy.hud.app.ui.component.vlist.viewholder;

final public class SwitchViewHolder$select$3 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.vlist.VerticalList$Model $model;
    final int $pos;
    final com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder this$0;
    
    SwitchViewHolder$select$3(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder a, com.navdy.hud.app.ui.component.vlist.VerticalList$Model a0, int i) {
        super();
        this.this$0 = a;
        this.$model = a0;
        this.$pos = i;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        super.onAnimationEnd(a);
        com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder.access$setOn$p(this.this$0, !com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder.access$isOn$p(this.this$0));
        this.this$0.drawSwitch(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder.access$isOn$p(this.this$0), com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder.access$isEnabled$p(this.this$0));
        com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a0 = this.this$0.vlist.getItemSelectionState();
        a0.set(this.$model, this.$model.id, this.$pos, -1, -1);
        this.this$0.vlist.performSelectAction(a0);
        this.this$0.vlist.unlock();
    }
}
