package com.navdy.hud.app.ui.component.destination;

class DestinationPickerView$1 implements com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$Callback {
    final com.navdy.hud.app.ui.component.destination.DestinationPickerView this$0;
    
    DestinationPickerView$1(com.navdy.hud.app.ui.component.destination.DestinationPickerView a) {
        super();
        this.this$0 = a;
    }
    
    public void close() {
        com.navdy.hud.app.analytics.AnalyticsSupport.recordRouteSelectionCancelled();
        this.this$0.presenter.close();
    }
    
    public boolean isClosed() {
        return this.this$0.presenter.isClosed();
    }
    
    public boolean isItemClickable(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        return true;
    }
    
    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, android.view.View a0, int i, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a1) {
    }
    
    public void onFastScrollEnd() {
    }
    
    public void onFastScrollStart() {
    }
    
    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        a.pos = a.pos + 1;
        this.this$0.presenter.itemSelected(a);
    }
    
    public void onLoad() {
        com.navdy.hud.app.ui.component.destination.DestinationPickerView.access$000().v("onLoad");
        label0: if (this.this$0.presenter != null) {
            boolean b = this.this$0.presenter.isShowingRouteMap();
            label1: {
                if (b) {
                    break label1;
                }
                if (!this.this$0.presenter.isShowDestinationMap()) {
                    break label0;
                }
            }
            this.this$0.setBackgroundColor(0);
            this.this$0.getHandler().postDelayed((Runnable)new com.navdy.hud.app.ui.component.destination.DestinationPickerView$1$1(this), 1000L);
        }
        this.this$0.vmenuComponent.animateIn((android.animation.Animator$AnimatorListener)null);
    }
    
    public void onScrollIdle() {
    }
    
    public void select(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        a.pos = a.pos + 1;
        this.this$0.presenter.itemClicked(a);
    }
    
    public void showToolTip() {
    }
}
