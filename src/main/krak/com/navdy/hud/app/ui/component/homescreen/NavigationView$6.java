package com.navdy.hud.app.ui.component.homescreen;

class NavigationView$6 implements Runnable {
    final com.navdy.hud.app.ui.component.homescreen.NavigationView this$0;
    
    NavigationView$6(com.navdy.hud.app.ui.component.homescreen.NavigationView a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).v("cleanupMapOverview");
        if (com.navdy.hud.app.ui.component.homescreen.NavigationView.access$500(this.this$0) != null) {
            com.navdy.hud.app.ui.component.homescreen.NavigationView.access$000(this.this$0).removeMapObject((com.here.android.mpa.mapping.MapObject)com.navdy.hud.app.ui.component.homescreen.NavigationView.access$500(this.this$0));
        }
        if (com.navdy.hud.app.ui.component.homescreen.NavigationView.access$700(this.this$0) != null) {
            com.navdy.hud.app.ui.component.homescreen.NavigationView.access$000(this.this$0).removeMapObject((com.here.android.mpa.mapping.MapObject)com.navdy.hud.app.ui.component.homescreen.NavigationView.access$700(this.this$0));
        }
        this.this$0.cleanupMapOverviewRoutes();
        this.this$0.cleanupFluctuator();
    }
}
