package com.navdy.hud.app.ui.component.vmenu;

class VerticalMenuComponent$10 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent this$0;
    final Runnable val$endAction;
    final int val$startDelay;
    
    VerticalMenuComponent$10(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a, Runnable a0, int i) {
        super();
        this.this$0 = a;
        this.val$endAction = a0;
        this.val$startDelay = i;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        android.view.ViewPropertyAnimator a0 = this.this$0.rightContainer.animate().alpha(0.0f).setDuration(150L).withEndAction(this.val$endAction);
        if (this.val$startDelay > 0) {
            a0.setStartDelay((long)this.val$startDelay);
        }
        a0.start();
    }
}
