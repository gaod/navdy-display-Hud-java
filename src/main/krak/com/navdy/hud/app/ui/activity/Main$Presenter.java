package com.navdy.hud.app.ui.activity;
import com.navdy.hud.app.R;
import javax.inject.Inject;
import com.squareup.otto.Subscribe;

public class Main$Presenter extends com.navdy.hud.app.ui.framework.BasePresenter implements flow.Flow$Listener, com.navdy.hud.app.manager.InputManager$IInputHandler {
    final public static String MAIN_SETTINGS = "main.settings";
    private boolean animationRunning;
    @Inject
    com.squareup.otto.Bus bus;
    @Inject
    com.navdy.hud.app.framework.phonecall.CallManager callManager;
    @Inject
    com.navdy.hud.app.service.ConnectionHandler connectionHandler;
    private boolean createdScreens;
    private com.navdy.service.library.events.ui.Screen currentScreen;
    private int defaultNotifColor;
    @Inject
    com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler dialSimulatorMessagesHandler;
    @Inject
    com.navdy.hud.app.profile.DriverProfileManager driverProfileManager;
    flow.Flow flow;
    @Inject
    android.content.SharedPreferences globalPreferences;
    private android.os.Handler handler;
    private com.navdy.hud.app.settings.HUDSettings hudSettings;
    private com.navdy.hud.app.manager.InitManager initManager;
    private com.navdy.hud.app.gesture.MultipleClickGestureDetector innerEventsDetector;
    @Inject
    com.navdy.hud.app.manager.InputManager inputManager;
    @Inject
    com.navdy.hud.app.manager.MusicManager musicManager;
    private boolean notifAnimationRunning;
    private com.navdy.hud.app.ui.framework.INotificationAnimationListener notificationAnimationListener;
    private boolean notificationColorEnabled;
    private com.navdy.hud.app.framework.notifications.NotificationManager notificationManager;
    private java.util.Queue operationQueue;
    @Inject
    com.navdy.hud.app.service.pandora.PandoraManager pandoraManager;
    @Inject
    com.navdy.hud.app.device.PowerManager powerManager;
    @Inject
    android.content.SharedPreferences preferences;
    private com.navdy.hud.app.ui.framework.IScreenAnimationListener screenAnimationListener;
    private com.navdy.hud.app.ui.activity.Main$Presenter$DeferredServices services;
    private com.navdy.hud.app.settings.MainScreenSettings settings;
    @Inject
    com.navdy.hud.app.config.SettingsManager settingsManager;
    private boolean showingPortrait;
    private boolean systemTrayEnabled;
    @Inject
    com.navdy.hud.app.framework.trips.TripManager tripManager;
    @Inject
    com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    private com.navdy.hud.app.framework.voice.VoiceAssistNotification voiceAssistNotification;
    private com.navdy.hud.app.framework.voice.VoiceSearchNotification voiceSearchNotification;
    
    public Main$Presenter() {
        this.systemTrayEnabled = true;
        this.notificationColorEnabled = true;
        this.innerEventsDetector = new com.navdy.hud.app.gesture.MultipleClickGestureDetector(4, (com.navdy.hud.app.gesture.MultipleClickGestureDetector$IMultipleClickKeyGesture)new com.navdy.hud.app.ui.activity.Main$Presenter$1(this));
        this.operationQueue = (java.util.Queue)new java.util.LinkedList();
        this.handler = new android.os.Handler();
        this.screenAnimationListener = (com.navdy.hud.app.ui.framework.IScreenAnimationListener)new com.navdy.hud.app.ui.activity.Main$Presenter$3(this);
        this.notificationAnimationListener = (com.navdy.hud.app.ui.framework.INotificationAnimationListener)new com.navdy.hud.app.ui.activity.Main$Presenter$4(this);
    }
    
    static void access$000(com.navdy.hud.app.ui.activity.Main$Presenter a, int i) {
        a.executeMultipleKeyEvent(i);
    }
    
    static boolean access$100(com.navdy.hud.app.ui.activity.Main$Presenter a, com.navdy.hud.app.manager.InputManager$CustomKeyEvent a0) {
        return a.executeKeyEvent(a0);
    }
    
    static void access$1000(com.navdy.hud.app.ui.activity.Main$Presenter a, com.navdy.service.library.events.ui.Screen a0, android.os.Bundle a1, Object a2, boolean b, boolean b0) {
        a.updateScreen(a0, a1, a2, b, b0);
    }
    
    static void access$400(com.navdy.hud.app.ui.activity.Main$Presenter a, com.navdy.hud.app.ui.activity.Main$ScreenCategory a0, com.navdy.service.library.events.ui.Screen a1) {
        a.updateSystemTrayVisibility(a0, a1);
    }
    
    static boolean access$502(com.navdy.hud.app.ui.activity.Main$Presenter a, boolean b) {
        a.notifAnimationRunning = b;
        return b;
    }
    
    static void access$600(com.navdy.hud.app.ui.activity.Main$Presenter a) {
        a.runQueuedOperation();
    }
    
    static com.navdy.hud.app.ui.activity.Main$Presenter$DeferredServices access$900(com.navdy.hud.app.ui.activity.Main$Presenter a) {
        return a.services;
    }
    
    private void clearViews() {
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        if (a != null && a.isNotificationViewShowing() && !this.notifAnimationRunning) {
            com.navdy.hud.app.ui.activity.Main.access$200().v("NotificationManager:dismissNotification");
            a.dismissNotification();
        }
    }
    
    private boolean executeKeyEvent(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        switch(com.navdy.hud.app.ui.activity.Main$1.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
            case 5: {
                com.navdy.hud.app.ui.activity.Main.access$200().v("power button click: no-op");
                b = false;
                break;
            }
            case 4: {
                com.navdy.hud.app.ui.activity.Main.access$200().v("power button double click: show dial pairing");
                this.bus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING).build());
                b = true;
                break;
            }
            case 3: {
                com.navdy.hud.app.ui.activity.Main.access$200().v("power button click: show shutdown");
                android.os.Bundle a0 = com.navdy.hud.app.event.Shutdown$Reason.POWER_BUTTON.asBundle();
                this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_SHUTDOWN_CONFIRMATION, a0, false));
                b = true;
                break;
            }
            case 2: {
                com.navdy.hud.app.manager.RemoteDeviceManager a1 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
                boolean b0 = a1.isRemoteDeviceConnected();
                label2: {
                    label0: {
                        label1: {
                            if (!b0) {
                                break label1;
                            }
                            if (a1.isAppConnected()) {
                                break label0;
                            }
                        }
                        this.connectionHandler.sendConnectionNotification();
                        break label2;
                    }
                    if (com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getLongPressAction() != com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH) {
                        this.startVoiceAssistance(false);
                    } else {
                        com.navdy.hud.app.ui.activity.Main.access$200().v("longpress, Place Search ");
                        this.launchVoiceSearch();
                    }
                }
                b = true;
                break;
            }
            case 1: {
                this.bus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_MAIN_MENU).build());
                b = true;
                break;
            }
            default: {
                b = false;
            }
        }
        return b;
    }
    
    private void executeMultipleKeyEvent(int i) {
        if (i != 2) {
            if (i != 3) {
                if (i >= 4) {
                    com.navdy.hud.app.ui.activity.Main.access$200().d("4 clicks, dumping a snapshot");
                    com.navdy.hud.app.util.ReportIssueService.dumpSnapshotAsync();
                    if (!com.navdy.hud.app.util.DeviceUtil.isUserBuild()) {
                        this.handler.postDelayed((Runnable)new com.navdy.hud.app.ui.activity.Main$Presenter$2(this), 2000L);
                    }
                }
            } else {
                com.navdy.hud.app.ui.activity.Main.access$200().v("Triple click");
                com.navdy.hud.app.ui.component.homescreen.HomeScreenView a = this.uiStateManager.getHomescreenView();
                if (a.getDisplayMode() != com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP) {
                    a.setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP);
                } else {
                    a.setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.SMART_DASH);
                }
            }
        } else {
            com.navdy.hud.app.ui.activity.Main.access$200().v("Double click");
            this.takeNotificationAction();
        }
    }
    
    private void hideNotification() {
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        if (a != null && this.notificationManager.getCurrentNotification() != null) {
            com.navdy.hud.app.ui.activity.Main.access$200().v("NotificationManager:calling shownotification");
            a.dismissNotification();
        }
    }
    
    private boolean isPortrait() {
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        if (a != null) {
            this.showingPortrait = com.navdy.hud.app.util.ScreenOrientation.isPortrait((android.app.Activity)a.getContext());
        }
        return this.showingPortrait;
    }
    
    private void launchVoiceAssistance(boolean b) {
        com.navdy.hud.app.framework.toast.ToastManager a = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        boolean b0 = a.isToastDisplayed();
        label1: {
            label0: {
                if (!b0) {
                    break label0;
                }
                if (!android.text.TextUtils.equals((CharSequence)a.getCurrentToastId(), (CharSequence)"incomingcall#toast")) {
                    break label0;
                }
                com.navdy.hud.app.ui.activity.Main.access$200().v("startVoiceAssistance:cannot launch, phone toast");
                break label1;
            }
            a.disableToasts(true);
            com.navdy.hud.app.ui.activity.Main.access$200().v(new StringBuilder().append("startVoiceAssistance: disable toast, current notif:").append(this.notificationManager.getCurrentNotification()).toString());
            com.navdy.hud.app.framework.notifications.NotificationManager a0 = this.notificationManager;
            Object a1 = b ? this.voiceSearchNotification : this.voiceAssistNotification;
            a0.addNotification((com.navdy.hud.app.framework.notifications.INotification)a1);
        }
    }
    
    private void launchVoiceSearch() {
        com.navdy.hud.app.framework.notifications.NotificationManager a = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        com.navdy.hud.app.framework.voice.VoiceSearchNotification a0 = (com.navdy.hud.app.framework.voice.VoiceSearchNotification)a.getNotification("navdy#voicesearch#notif");
        if (a0 == null) {
            a0 = new com.navdy.hud.app.framework.voice.VoiceSearchNotification();
        }
        a.addNotification((com.navdy.hud.app.framework.notifications.INotification)a0);
    }
    
    private static mortar.Blueprint lookupScreen(com.navdy.service.library.events.ui.Screen a) {
        Object a0 = null;
        label1: {
            label0: {
                InstantiationException a1 = null;
                try {
                    try {
                        Class a2 = null;
                        switch(com.navdy.hud.app.ui.activity.Main$1.$SwitchMap$com$navdy$service$library$events$ui$Screen[a.ordinal()]) {
                            case 1: case 2: {
                                a = com.navdy.service.library.events.ui.Screen.SCREEN_HOME;
                            }
                            default: {
                                a2 = (Class)com.navdy.hud.app.ui.activity.Main.access$1100().get(a.ordinal());
                            }
                        }
                        if (a2 == null) {
                            break label0;
                        }
                        a0 = a2.newInstance();
                        break label1;
                    } catch(InstantiationException a3) {
                        a1 = a3;
                    }
                } catch(IllegalAccessException a4) {
                    com.navdy.hud.app.ui.activity.Main.access$200().e("Failed to instantiate screen", (Throwable)a4);
                    break label0;
                }
                com.navdy.hud.app.ui.activity.Main.access$200().e("Failed to instantiate screen", (Throwable)a1);
            }
            a0 = null;
        }
        return (mortar.Blueprint)a0;
    }
    
    private void persistSettings() {
        com.navdy.hud.app.ui.activity.Main.access$200().d("Starting to persist settings");
        String s = new com.google.gson.Gson().toJson(this.settings);
        this.preferences.edit().putString("main.settings", s).apply();
        com.navdy.hud.app.ui.activity.Main.access$200().d(new StringBuilder().append("Persisted settings:").append(s).toString());
    }
    
    private void runQueuedOperation() {
        int i = this.operationQueue.size();
        if (i <= 0) {
            this.setInputFocus();
            this.animationRunning = false;
            com.navdy.hud.app.ui.activity.Main.access$200().v("animationRunning:false");
        } else {
            com.navdy.hud.app.ui.activity.Main$ScreenInfo a = (com.navdy.hud.app.ui.activity.Main$ScreenInfo)this.operationQueue.remove();
            com.navdy.hud.app.ui.activity.Main.access$200().v(new StringBuilder().append("runQueuedOperation size:").append(i).append(" posting:").append(a.screen).toString());
            this.handler.post((Runnable)new com.navdy.hud.app.ui.activity.Main$Presenter$7(this, a));
        }
    }
    
    private void setDisplayFormat(com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat a) {
        com.navdy.hud.app.ui.activity.Main.access$200().i(new StringBuilder().append("Switching display format to ").append(a).toString());
        com.navdy.hud.app.view.MainView a0 = (com.navdy.hud.app.view.MainView)this.getView();
        if (a0 != null) {
            a0.setDisplayFormat(a);
        }
    }
    
    private void setupFlow() {
        flow.Backstack a = flow.Backstack.single(new com.navdy.hud.app.screen.FirstLaunchScreen());
        this.currentScreen = com.navdy.service.library.events.ui.Screen.SCREEN_FIRST_LAUNCH;
        this.uiStateManager.setCurrentViewMode(this.currentScreen);
        this.flow = new flow.Flow(a, (flow.Flow$Listener)this);
        this.flow.resetTo(this.flow.getBackstack().current().getScreen());
    }
    
    private void showInstallingUpdateToast() {
        com.navdy.hud.app.framework.toast.ToastManager a = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        a.clearAllPendingToast();
        a.disableToasts(false);
        android.content.res.Resources a0 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        android.os.Bundle a1 = new android.os.Bundle();
        a.dismissCurrentToast("#ota#toast");
        a1.putString("1", a0.getString(R.string.warning));
        a1.putInt("8", R.drawable.icon_warning);
        a1.putInt("11", R.drawable.icon_sm_spinner);
        int i = (com.navdy.hud.app.obd.ObdManager.getInstance().getConnectionType() != com.navdy.hud.app.obd.ObdManager$ConnectionType.POWER_ONLY) ? R.string.do_not_remove_power : R.string.do_not_remove_power_or_turn_off_vehicle;
        a1.putString("2", a0.getString(R.string.preparing_to_install_update));
        a1.putInt("3", R.style.installing_update_title_1);
        a1.putString("4", a0.getString(i));
        a1.putInt("5", R.style.installing_update_title_2);
        a1.putInt("16", (int)a0.getDimension(R.dimen.toast_installing_update_width));
        a1.putInt("16_2", (int)a0.getDimension(R.dimen.toast_installing_update_padding));
        a.addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("#ota#toast", a1, (com.navdy.hud.app.framework.toast.IToastCallback)new com.navdy.hud.app.ui.activity.Main$Presenter$8(this), true, true));
    }
    
    private void showNotification() {
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        if (a != null) {
            com.navdy.hud.app.framework.notifications.INotification a0 = this.notificationManager.getCurrentNotification();
            if (a0 != null) {
                com.navdy.hud.app.ui.activity.Main.access$200().v("NotificationManager:calling shownotification");
                com.navdy.hud.app.ui.component.homescreen.HomeScreenView a1 = this.uiStateManager.getHomescreenView();
                if (a1.hasModeView() && a1.isModeVisible()) {
                    com.navdy.hud.app.ui.activity.Main.access$200().v("mode-view: reset mode view show notif");
                    a1.resetModeView();
                }
                a.showNotification(a0.getId(), a0.getType());
            } else {
                com.navdy.hud.app.ui.activity.Main.access$200().v("no current notification to be displayed");
                this.runQueuedOperation();
            }
        }
    }
    
    private void signalBootComplete() {
        com.navdy.hud.app.util.os.SystemProperties.set("dev.app_started", "1");
        com.navdy.hud.app.util.os.SystemProperties.set("dev.late_service", "1");
    }
    
    private void startLateServices() {
        com.navdy.hud.app.ui.activity.Main.access$200().v("starting update service");
        android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
        android.content.Intent a0 = new android.content.Intent(a, com.navdy.hud.app.util.OTAUpdateService.class);
        a0.putExtra("COMMAND", "VERIFY_UPDATE");
        a.startService(a0);
        com.navdy.hud.app.util.ReportIssueService.scheduleSync();
        com.navdy.hud.app.ui.activity.Main.access$200().v("initializing additional services");
        this.services = new com.navdy.hud.app.ui.activity.Main$Presenter$DeferredServices(a);
        label1: if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getFeatureUtil().isFeatureEnabled(com.navdy.hud.app.util.FeatureUtil$Feature.GESTURE_ENGINE)) {
            com.navdy.hud.app.ui.activity.Main.access$200().v("gesture is supported");
            com.navdy.service.library.events.preferences.InputPreferences a1 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getInputPreferences();
            label0: {
                if (a1 == null) {
                    break label0;
                }
                if (!a1.use_gestures.booleanValue()) {
                    break label0;
                }
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.ui.activity.Main$Presenter$5(this), 1);
                break label1;
            }
            com.navdy.hud.app.ui.activity.Main.access$200().v("gesture is not enabled");
        }
    }
    
    private void takeNotificationAction() {
        com.navdy.hud.app.manager.RemoteDeviceManager a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        boolean b = a.isRemoteDeviceConnected();
        label2: {
            label0: {
                label1: {
                    if (!b) {
                        break label1;
                    }
                    if (a.isAppConnected()) {
                        break label0;
                    }
                }
                this.connectionHandler.sendConnectionNotification();
                break label2;
            }
            if (this.notificationManager.isNotificationPresent("navdy#phone#call#notif")) {
                com.navdy.hud.app.ui.activity.Main.access$200().v("show phone call");
                this.notificationManager.expandNotification();
            } else {
                com.navdy.hud.app.ui.activity.Main.access$200().v("show music");
                this.musicManager.showMusicNotification();
            }
        }
    }
    
    private void updateDisplayFormat() {
        this.setDisplayFormat(this.driverProfileManager.getCurrentProfile().getDisplayFormat());
    }
    
    private void updateScreen(com.navdy.service.library.events.ui.Screen a, android.os.Bundle a0, Object a1, boolean b, boolean b0) {
        com.navdy.hud.app.ui.activity.Main.access$200().v(new StringBuilder().append("updateScreen screen[").append(a).append("] animRunning[").append(this.animationRunning).append("] ignoreAnim [").append(b).append("] dismiss[").append(b0).append("]").toString());
        label6: {
            boolean b1 = false;
            label5: {
                if (b) {
                    break label5;
                }
                if (!this.animationRunning) {
                    break label5;
                }
                this.operationQueue.add(new com.navdy.hud.app.ui.activity.Main$ScreenInfo(a, a0, a1, b0));
                break label6;
            }
            this.animationRunning = true;
            com.navdy.hud.app.ui.activity.Main.access$200().v("animationRunning:true");
            switch(com.navdy.hud.app.ui.activity.Main$1.$SwitchMap$com$navdy$service$library$events$ui$Screen[a.ordinal()]) {
                case 5: {
                    com.navdy.hud.app.ui.activity.Main.access$300();
                    b1 = true;
                    break;
                }
                case 4: {
                    if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
                        boolean b2 = false;
                        label4: {
                            label2: {
                                label3: {
                                    if (a0 == null) {
                                        break label3;
                                    }
                                    if (a0.getBoolean("extra_voice_search_notification", false)) {
                                        break label2;
                                    }
                                }
                                b2 = false;
                                break label4;
                            }
                            b2 = true;
                        }
                        this.startVoiceAssistance(b2);
                    } else {
                        this.connectionHandler.sendConnectionNotification();
                    }
                    b1 = true;
                    break;
                }
                case 3: {
                    if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
                        this.musicManager.showMusicNotification();
                    } else {
                        this.connectionHandler.sendConnectionNotification();
                    }
                    b1 = true;
                    break;
                }
                default: {
                    b1 = false;
                }
            }
            if (b1) {
                com.navdy.hud.app.ui.activity.Main.access$200().v("notif screen");
                this.runQueuedOperation();
            } else {
                if (a == com.navdy.service.library.events.ui.Screen.SCREEN_BACK) {
                    a = this.uiStateManager.getCurrentViewMode();
                }
                if (com.navdy.hud.app.ui.framework.UIStateManager.isSidePanelMode(a)) {
                    if (com.navdy.hud.app.ui.framework.UIStateManager.isOverlayMode(this.currentScreen)) {
                        this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_BACK, (android.os.Bundle)null, true));
                        this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(a, a0, a1, true));
                    } else if (com.navdy.hud.app.ui.activity.Main$1.$SwitchMap$com$navdy$service$library$events$ui$Screen[a.ordinal()] == 6) {
                        if (b0) {
                            com.navdy.hud.app.view.MainView a2 = (com.navdy.hud.app.view.MainView)this.getView();
                            if (a2 != null && a2.isNotificationViewShowing() && !this.notifAnimationRunning) {
                                com.navdy.hud.app.ui.activity.Main.access$200().v("NotificationManager:calling dismissNotification");
                                a2.dismissNotification();
                            }
                        } else {
                            this.showNotification();
                        }
                    }
                } else {
                    Object a3 = null;
                    this.clearViews();
                    mortar.Blueprint a4 = com.navdy.hud.app.ui.activity.Main$Presenter.lookupScreen(a);
                    if (a4 instanceof com.navdy.hud.app.screen.BaseScreen) {
                        com.navdy.hud.app.screen.BaseScreen a5 = (com.navdy.hud.app.screen.BaseScreen)a4;
                        a5.setArguments(a0, a1);
                        a3 = a5;
                    } else {
                        a3 = a4;
                    }
                    if (a3 instanceof com.navdy.hud.app.ui.component.homescreen.HomeScreen) {
                        switch(com.navdy.hud.app.ui.activity.Main$1.$SwitchMap$com$navdy$service$library$events$ui$Screen[a.ordinal()]) {
                            case 7: {
                                com.navdy.hud.app.ui.component.homescreen.HomeScreen a6 = (com.navdy.hud.app.ui.component.homescreen.HomeScreen)a3;
                                com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode a7 = a6.getDisplayMode();
                                int i = a7.ordinal();
                                boolean b3 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
                                android.content.SharedPreferences a8 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getLocalPreferencesForCurrentDriverProfile(com.navdy.hud.app.HudApplication.getAppContext());
                                label0: {
                                    label1: {
                                        if (b3) {
                                            break label1;
                                        }
                                        if (a8 != null) {
                                            break label0;
                                        }
                                    }
                                    a8 = this.globalPreferences;
                                }
                                if (a8 != null) {
                                    i = a8.getInt("PREFERENCE_LAST_HOME_SCREEN_MODE", a7.ordinal());
                                }
                                if (i == a7.ordinal()) {
                                    a3 = a6;
                                    break;
                                } else {
                                    if (i != com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP.ordinal()) {
                                        if (i == com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.SMART_DASH.ordinal()) {
                                            a6 = (com.navdy.hud.app.ui.component.homescreen.HomeScreen)a6;
                                            a6.setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.SMART_DASH);
                                        }
                                    } else {
                                        a6 = (com.navdy.hud.app.ui.component.homescreen.HomeScreen)a6;
                                        a6.setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP);
                                    }
                                    com.navdy.hud.app.ui.activity.Main.saveHomeScreenPreference(this.globalPreferences, i);
                                    a3 = a6;
                                    break;
                                }
                            }
                            case 2: {
                                com.navdy.hud.app.ui.activity.Main.saveHomeScreenPreference(this.globalPreferences, com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.SMART_DASH.ordinal());
                                com.navdy.hud.app.ui.component.homescreen.HomeScreen a9 = (com.navdy.hud.app.ui.component.homescreen.HomeScreen)a3;
                                a9.setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.SMART_DASH);
                                a = com.navdy.service.library.events.ui.Screen.SCREEN_HOME;
                                a3 = a9;
                                break;
                            }
                            case 1: {
                                com.navdy.service.library.events.ui.Screen a10 = com.navdy.service.library.events.ui.Screen.SCREEN_HOME;
                                com.navdy.hud.app.ui.activity.Main.saveHomeScreenPreference(this.globalPreferences, com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP.ordinal());
                                com.navdy.hud.app.ui.component.homescreen.HomeScreen a11 = (com.navdy.hud.app.ui.component.homescreen.HomeScreen)a3;
                                a11.setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP);
                                a = a10;
                                a3 = a11;
                                break;
                            }
                        }
                    }
                    if (com.navdy.hud.app.ui.framework.UIStateManager.isFullscreenMode(a)) {
                        this.uiStateManager.setCurrentViewMode(a);
                    }
                    if (a3 != null) {
                        this.currentScreen = a;
                        this.flow.replaceTo(a3);
                    }
                }
            }
        }
    }
    
    private void updateSystemTrayVisibility(com.navdy.hud.app.ui.activity.Main$ScreenCategory a, com.navdy.service.library.events.ui.Screen a0) {
        switch(com.navdy.hud.app.ui.activity.Main$1.$SwitchMap$com$navdy$hud$app$ui$activity$Main$ScreenCategory[a.ordinal()]) {
            case 2: {
                if (!this.notifAnimationRunning) {
                    this.runQueuedOperation();
                }
                if (this.notifAnimationRunning) {
                    break;
                }
                if (this.isNotificationViewShowing()) {
                    break;
                }
                com.navdy.hud.app.ui.framework.UIStateManager dummy = this.uiStateManager;
                if (!com.navdy.hud.app.ui.framework.UIStateManager.isFullscreenMode(a0)) {
                    break;
                }
                this.setNotificationColorVisibility(0);
                this.setSystemTrayVisibility(0);
                break;
            }
            case 1: {
                com.navdy.hud.app.screen.BaseScreen a1 = this.uiStateManager.getCurrentScreen();
                if (a1 != null) {
                    a0 = a1.getScreen();
                }
                com.navdy.hud.app.ui.framework.UIStateManager dummy0 = this.uiStateManager;
                if (!com.navdy.hud.app.ui.framework.UIStateManager.isFullscreenMode(a0)) {
                    break;
                }
                this.setNotificationColorVisibility(0);
                this.setSystemTrayVisibility(0);
                break;
            }
        }
    }
    
    private boolean verifyProtocolVersion(com.navdy.service.library.events.DeviceInfo a) {
        int i = 0;
        boolean b = false;
        try {
            i = Integer.parseInt(a.protocolVersion.split("\\.", 2)[0]);
        } catch(NumberFormatException ignoredException) {
            com.navdy.hud.app.ui.activity.Main.access$200().e(new StringBuilder().append("invalid remote device protocol version: ").append(a.protocolVersion).toString());
            i = -1;
        }
        int i0 = com.navdy.service.library.Version.PROTOCOL_VERSION.majorVersion;
        label2: {
            label0: {
                label1: {
                    if (i0 == i) {
                        break label1;
                    }
                    if (com.navdy.service.library.Version.PROTOCOL_VERSION.majorVersion != 1) {
                        break label0;
                    }
                    if (i != 0) {
                        break label0;
                    }
                }
                com.navdy.hud.app.ui.activity.Main.mProtocolStatus = com.navdy.hud.app.ui.activity.Main$ProtocolStatus.PROTOCOL_VALID;
                b = true;
                break label2;
            }
            if (com.navdy.service.library.Version.PROTOCOL_VERSION.majorVersion >= i) {
                com.navdy.hud.app.ui.activity.Main.mProtocolStatus = com.navdy.hud.app.ui.activity.Main$ProtocolStatus.PROTOCOL_REMOTE_NEEDS_UPDATE;
            } else {
                com.navdy.hud.app.ui.activity.Main.mProtocolStatus = com.navdy.hud.app.ui.activity.Main$ProtocolStatus.PROTOCOL_LOCAL_NEEDS_UPDATE;
            }
            com.navdy.service.library.log.Logger a0 = com.navdy.hud.app.ui.activity.Main.access$200();
            Object[] a1 = new Object[2];
            a1[0] = a.protocolVersion;
            a1[1] = com.navdy.service.library.Version.PROTOCOL_VERSION.toString();
            a0.e(String.format("protocol version mismatch remote = %s, local = %s", a1));
            b = false;
        }
        return b;
    }
    
    public void addNotificationExtensionView(android.view.View a) {
        com.navdy.hud.app.view.MainView a0 = (com.navdy.hud.app.view.MainView)this.getView();
        if (a0 != null) {
            a0.addNotificationExtensionView(a);
        }
    }
    
    public void clearInputFocus() {
        if (!(this.inputManager.getFocus() instanceof com.navdy.hud.app.view.ToastView)) {
            this.inputManager.setFocus((com.navdy.hud.app.manager.InputManager$IInputHandler)null);
            com.navdy.hud.app.ui.activity.Main.access$200().v("[focus] cleared");
        }
    }
    
    public void createScreens() {
        if (!this.createdScreens) {
            com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
            if (a != null) {
                com.navdy.hud.app.ui.activity.Main.access$200().v("createScreens");
                com.navdy.hud.app.view.ContainerView a0 = a.getContainerView();
                this.createdScreens = true;
                a0.createScreens();
            }
        }
    }
    
    public void ejectMainLowerView() {
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        if (a != null) {
            a.ejectMainLowerView();
        }
    }
    
    void enableNotificationColor(boolean b) {
        this.notificationColorEnabled = b;
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        label2: if (a != null) {
            if (this.notificationColorEnabled) {
                com.navdy.hud.app.screen.BaseScreen a0 = this.uiStateManager.getCurrentScreen();
                label0: {
                    label1: {
                        if (a0 == null) {
                            break label1;
                        }
                        com.navdy.hud.app.ui.framework.UIStateManager dummy = this.uiStateManager;
                        if (com.navdy.hud.app.ui.framework.UIStateManager.isFullscreenMode(a0.getScreen())) {
                            break label0;
                        }
                    }
                    a.setNotificationColorVisibility(4);
                    com.navdy.hud.app.ui.activity.Main.access$200().v("notif invisible");
                    break label2;
                }
                this.setNotificationColorVisibility(0);
                com.navdy.hud.app.ui.activity.Main.access$200().v("notif visible");
            } else {
                a.setNotificationColorVisibility(4);
                com.navdy.hud.app.ui.activity.Main.access$200().v("notif invisible");
            }
        }
    }
    
    void enableSystemTray(boolean b) {
        this.systemTrayEnabled = b;
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        if (a != null) {
            com.navdy.hud.app.ui.component.SystemTrayView a0 = a.getSystemTray();
            if (this.systemTrayEnabled) {
                this.setSystemTrayVisibility(0);
            } else {
                a0.setVisibility(4);
            }
        }
    }
    
    public com.navdy.hud.app.service.ConnectionHandler getConnectionHandler() {
        return this.connectionHandler;
    }
    
    android.view.View getExpandedNotificationCoverView() {
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        android.view.View a0 = (a == null) ? null : a.getExpandedNotificationCoverView();
        return a0;
    }
    
    android.widget.FrameLayout getExpandedNotificationView() {
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        android.widget.FrameLayout a0 = (a == null) ? null : a.getExpandedNotificationView();
        return a0;
    }
    
    android.view.ViewGroup getNotificationExtensionView() {
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        android.widget.FrameLayout a0 = (a == null) ? null : a.getNotificationExtensionView();
        return a0;
    }
    
    com.navdy.hud.app.ui.component.carousel.CarouselIndicator getNotificationIndicator() {
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        com.navdy.hud.app.ui.component.carousel.CarouselIndicator a0 = (a == null) ? null : a.getNotificationIndicator();
        return a0;
    }
    
    com.navdy.hud.app.ui.component.carousel.ProgressIndicator getNotificationScrollIndicator() {
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        com.navdy.hud.app.ui.component.carousel.ProgressIndicator a0 = (a == null) ? null : a.getNotificationScrollIndicator();
        return a0;
    }
    
    com.navdy.hud.app.view.NotificationView getNotificationView() {
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        com.navdy.hud.app.view.NotificationView a0 = (a == null) ? null : a.getNotificationView();
        return a0;
    }
    
    public com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration getScreenConfiguration() {
        return (this.isPortrait()) ? this.settings.getPortraitConfiguration() : this.settings.getLandscapeConfiguration();
    }
    
    com.navdy.hud.app.ui.component.SystemTrayView getSystemTray() {
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        return (a != null) ? a.getSystemTray() : null;
    }
    
    public void go(flow.Backstack a, flow.Flow$Direction a0, flow.Flow$Callback a1) {
        com.navdy.hud.app.view.MainView a2 = (com.navdy.hud.app.view.MainView)this.getView();
        if (a2 != null) {
            com.navdy.hud.app.view.ContainerView a3 = a2.getContainerView();
            com.navdy.hud.app.screen.BaseScreen a4 = (com.navdy.hud.app.screen.BaseScreen)a.current().getScreen();
            flow.Backstack a5 = this.flow.getBackstack();
            int i = (a5.size() <= 0) ? -1 : ((com.navdy.hud.app.screen.BaseScreen)a5.current().getScreen()).getAnimationOut(flow.Flow$Direction.FORWARD);
            a3.showScreen((mortar.Blueprint)a4, flow.Flow$Direction.FORWARD, a4.getAnimationIn(flow.Flow$Direction.FORWARD), i);
        }
        a1.onComplete();
    }
    
    public void injectMainLowerView(android.view.View a) {
        com.navdy.hud.app.view.MainView a0 = (com.navdy.hud.app.view.MainView)this.getView();
        if (a0 != null) {
            a0.injectMainLowerView(a);
        }
    }
    
    public boolean isMainLowerViewVisible() {
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        return a != null && a.isMainLowerViewVisible();
    }
    
    boolean isMainUIShrunk() {
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        return a != null && a.isMainUIShrunk();
    }
    
    boolean isNotificationCollapsing() {
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        return a != null && a.isNotificationCollapsing();
    }
    
    boolean isNotificationExpanding() {
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        return a != null && a.isNotificationExpanding();
    }
    
    boolean isNotificationViewShowing() {
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        return a != null && a.isNotificationViewShowing();
    }
    
    boolean isScreenAnimating() {
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        return a != null && a.isScreenAnimating();
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    @Subscribe
    public void onDeviceConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        com.navdy.hud.app.ui.activity.Main.access$200().v(new StringBuilder().append("status:").append(a.remoteDeviceId).append(" : ").append(a.state.name()).toString());
        com.navdy.hud.app.ui.component.SystemTrayView a0 = this.getSystemTray();
        switch(com.navdy.hud.app.ui.activity.Main$1.$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[a.state.ordinal()]) {
            case 2: {
                if (a0 != null) {
                    a0.setState(com.navdy.hud.app.ui.component.SystemTrayView$Device.Phone, com.navdy.hud.app.ui.component.SystemTrayView$State.DISCONNECTED);
                }
                if (com.navdy.hud.app.ui.activity.Main.mProtocolStatus == com.navdy.hud.app.ui.activity.Main$ProtocolStatus.PROTOCOL_LOCAL_NEEDS_UPDATE && this.currentScreen == com.navdy.service.library.events.ui.Screen.SCREEN_FORCE_UPDATE) {
                    this.bus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_HOME).build());
                }
                com.navdy.hud.app.ui.activity.Main.mProtocolStatus = com.navdy.hud.app.ui.activity.Main$ProtocolStatus.PROTOCOL_VALID;
                break;
            }
            case 1: {
                this.musicManager.initialize();
                if (a0 == null) {
                    break;
                }
                a0.setState(com.navdy.hud.app.ui.component.SystemTrayView$Device.Phone, com.navdy.hud.app.ui.component.SystemTrayView$State.CONNECTED);
                break;
            }
        }
    }
    
    @Subscribe
    public void onDeviceInfo(com.navdy.service.library.events.DeviceInfo a) {
        if (this.verifyProtocolVersion(a)) {
            com.navdy.service.library.events.connection.ConnectionStateChange$Builder a0 = new com.navdy.service.library.events.connection.ConnectionStateChange$Builder();
            a0.state(com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_VERIFIED);
            a0.remoteDeviceId(a.deviceId);
            this.bus.post(a0.build());
        } else {
            this.bus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_FORCE_UPDATE).build());
        }
    }
    
    @Subscribe
    public void onDialBatteryEvent(com.navdy.hud.app.device.dial.DialConstants$BatteryChangeEvent a) {
        com.navdy.hud.app.ui.component.SystemTrayView a0 = this.getSystemTray();
        if (a0 != null) {
            com.navdy.hud.app.ui.component.SystemTrayView$State a1 = null;
            com.navdy.hud.app.device.dial.DialConstants$NotificationReason a2 = com.navdy.hud.app.device.dial.DialManager.getInstance().getBatteryNotificationReason();
            if (a2 != null) {
                switch(com.navdy.hud.app.ui.activity.Main$1.$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason[a2.ordinal()]) {
                    case 4: {
                        a1 = com.navdy.hud.app.ui.component.SystemTrayView$State.OK_BATTERY;
                        break;
                    }
                    case 2: case 3: {
                        a1 = com.navdy.hud.app.ui.component.SystemTrayView$State.LOW_BATTERY;
                        break;
                    }
                    case 1: {
                        a1 = com.navdy.hud.app.ui.component.SystemTrayView$State.EXTREMELY_LOW_BATTERY;
                        break;
                    }
                    default: {
                        a1 = null;
                    }
                }
            } else {
                a1 = com.navdy.hud.app.ui.component.SystemTrayView$State.OK_BATTERY;
            }
            a0.setState(com.navdy.hud.app.ui.component.SystemTrayView$Device.Dial, a1);
        }
    }
    
    @Subscribe
    public void onDialConnectionEvent(com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus a) {
        com.navdy.hud.app.ui.component.SystemTrayView a0 = this.getSystemTray();
        if (a0 != null) {
            com.navdy.hud.app.ui.component.SystemTrayView$State a1 = (a.status != com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status.CONNECTED) ? com.navdy.hud.app.ui.component.SystemTrayView$State.DISCONNECTED : com.navdy.hud.app.ui.component.SystemTrayView$State.CONNECTED;
            a0.setState(com.navdy.hud.app.ui.component.SystemTrayView$Device.Dial, a1);
        }
    }
    
    @Subscribe
    public void onDismissScreen(com.navdy.service.library.events.ui.DismissScreen a) {
        if (a.screen == com.navdy.service.library.events.ui.Screen.SCREEN_NOTIFICATION) {
            this.updateScreen(com.navdy.service.library.events.ui.Screen.SCREEN_NOTIFICATION, (android.os.Bundle)null, null, false, true);
        }
    }
    
    @Subscribe
    public void onDisplayScaleChange(com.navdy.hud.app.event.DisplayScaleChange a) {
        this.setDisplayFormat(a.format);
    }
    
    @Subscribe
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged a) {
        this.updateDisplayFormat();
    }
    
    @Subscribe
    public void onDriverProfileUpdate(com.navdy.hud.app.event.DriverProfileUpdated a) {
        if (a.state == com.navdy.hud.app.event.DriverProfileUpdated$State.UPDATED) {
            this.updateDisplayFormat();
        }
    }
    
    protected void onExitScope() {
        super.onExitScope();
        com.navdy.hud.app.ui.activity.Main.access$200().i("Shutting down main service");
        this.connectionHandler.disconnect();
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        boolean b = false;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.gesture != null) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            if (com.navdy.hud.app.ui.activity.Main$1.$SwitchMap$com$navdy$service$library$events$input$Gesture[a.gesture.ordinal()] != 0) {
                com.navdy.hud.app.framework.notifications.NotificationManager a0 = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
                if (a0.getNotificationCount() > 0) {
                    com.navdy.hud.app.analytics.AnalyticsSupport.setGlanceNavigationSource("swipe");
                }
                a0.expandNotification();
            }
            b = true;
        }
        return b;
    }
    
    public void onGpsSatelliteData(com.navdy.hud.app.device.gps.GpsUtils$GpsSatelliteData a) {
        if (com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled()) {
            int i = a.data.getInt("SAT_FIX_TYPE", 0);
            com.navdy.hud.app.ui.component.SystemTrayView a0 = this.getSystemTray();
            if (a0 != null) {
                String s = null;
                switch(i) {
                    case 6: {
                        s = "DR";
                        break;
                    }
                    case 2: {
                        s = "Diff";
                        break;
                    }
                    case 1: {
                        s = "2D";
                        break;
                    }
                    default: {
                        s = null;
                        break;
                    }
                    case 3: case 4: case 5: {
                    }
                }
                a0.setDebugGpsMarker(s);
            }
        }
    }
    
    public void onGpsSwitchEvent(com.navdy.hud.app.device.gps.GpsUtils$GpsSwitch a) {
        if (com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled()) {
            com.navdy.hud.app.ui.activity.Main.access$200().v(new StringBuilder().append("gps_switch_event:").append(a.usingPhone).toString());
            com.navdy.hud.app.ui.component.SystemTrayView a0 = this.getSystemTray();
            if (a0 != null) {
                a0.setDebugGpsMarker((a.usingPhone) ? "P" : "U");
            }
        }
    }
    
    public void onInitEvent(com.navdy.hud.app.event.InitEvents$InitPhase a) {
        switch(com.navdy.hud.app.ui.activity.Main$1.$SwitchMap$com$navdy$hud$app$event$InitEvents$Phase[a.phase.ordinal()]) {
            case 2: {
                com.navdy.hud.app.ui.activity.Main.access$200().v("initializing obd manager");
                com.navdy.hud.app.obd.ObdManager.getInstance();
                if (!this.powerManager.inQuietMode()) {
                    this.startLateServices();
                }
                this.signalBootComplete();
                break;
            }
            case 1: {
                if (this.powerManager.inQuietMode()) {
                    this.powerManager.enterSleepMode();
                } else {
                    com.navdy.hud.app.device.light.HUDLightUtils.resetFrontLED(com.navdy.hud.app.device.light.LightManager.getInstance());
                }
                String s = com.navdy.hud.app.util.os.SystemProperties.get("persist.sys.demo.video");
                boolean b = !android.text.TextUtils.isEmpty((CharSequence)s);
                boolean b0 = this.preferences.getBoolean("start_video_on_boot_preference", false);
                label0: {
                    if (b0) {
                        break label0;
                    }
                    if (!b) {
                        break;
                    }
                }
                java.io.File a0 = android.os.Environment.getExternalStoragePublicDirectory(android.os.Environment.DIRECTORY_MOVIES);
                this.signalBootComplete();
                if (!b) {
                    s = "navdy-demo.mov";
                }
                java.io.File a1 = new java.io.File(a0, s);
                com.navdy.hud.app.ui.activity.Main.access$200().d(new StringBuilder().append("Trying to play video: ").append(a1).toString());
                if (a1.exists()) {
                    android.net.Uri a2 = android.net.Uri.fromFile(a1);
                    this.playMedia(com.navdy.hud.app.HudApplication.getAppContext(), a2);
                    break;
                } else {
                    com.navdy.hud.app.ui.activity.Main.access$200().d("Missing demo video file...");
                    break;
                }
            }
        }
    }
    
    public void onInstallingUpdate(com.navdy.hud.app.util.OTAUpdateService$InstallingUpdate a) {
        this.showInstallingUpdateToast();
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return this.innerEventsDetector.onKey(a);
    }
    
    public void onLoad(android.os.Bundle a) {
        com.navdy.hud.app.ui.activity.Main.access$702(this);
        this.notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        this.voiceAssistNotification = new com.navdy.hud.app.framework.voice.VoiceAssistNotification();
        this.voiceSearchNotification = new com.navdy.hud.app.framework.voice.VoiceSearchNotification();
        this.defaultNotifColor = 0;
        this.uiStateManager.setRootScreen(com.navdy.hud.app.ui.activity.Main.access$800());
        this.uiStateManager.addScreenAnimationListener(this.screenAnimationListener);
        this.uiStateManager.addNotificationAnimationListener(this.notificationAnimationListener);
        super.onLoad(a);
        com.navdy.hud.app.settings.MainScreenSettings a0 = null;
        if (a != null) {
            a0 = (com.navdy.hud.app.settings.MainScreenSettings)a.getParcelable("main.settings");
        }
        label1: {
            if (a0 != null) {
                break label1;
            }
            String s = this.preferences.getString("main.settings", (String)null);
            com.google.gson.Gson a1 = new com.google.gson.Gson();
            label0: {
                try {
                    a0 = (com.navdy.hud.app.settings.MainScreenSettings)a1.fromJson(s, com.navdy.hud.app.settings.MainScreenSettings.class);
                } catch(Throwable ignoredException) {
                    break label0;
                }
                break label1;
            }
            com.navdy.hud.app.ui.activity.Main.access$200().e(new StringBuilder().append("Failed to parse json ").append(s).toString());
        }
        if (a0 == null) {
            a0 = new com.navdy.hud.app.settings.MainScreenSettings();
        }
        this.settings = a0;
        this.settingsManager.addSetting((com.navdy.hud.app.config.Setting)com.navdy.hud.app.device.gps.GpsUtils.SHOW_RAW_GPS);
        this.hudSettings = new com.navdy.hud.app.settings.HUDSettings(this.preferences);
        this.updateDisplayFormat();
        this.bus.register(this);
        this.connectionHandler.connect();
        this.setupFlow();
        this.updateView();
        this.bus.register(this.dialSimulatorMessagesHandler);
        this.bus.register(this.musicManager);
        this.bus.register(this.pandoraManager);
        this.bus.register(this.tripManager);
        this.initManager = new com.navdy.hud.app.manager.InitManager(this.bus, this.connectionHandler);
        this.initManager.start();
    }
    
    public void onLocationFixChangeEvent(com.navdy.hud.app.maps.MapEvents$LocationFix a) {
        com.navdy.hud.app.ui.component.SystemTrayView a0 = this.getSystemTray();
        if (a0 != null) {
            if (a.locationAvailable) {
                a0.setState(com.navdy.hud.app.ui.component.SystemTrayView$Device.Gps, com.navdy.hud.app.ui.component.SystemTrayView$State.LOCATION_AVAILABLE);
                if (com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled()) {
                    this.onGpsSwitchEvent(new com.navdy.hud.app.device.gps.GpsUtils$GpsSwitch(a.usingPhoneLocation, a.usingLocalGpsLocation));
                }
            } else {
                a0.setState(com.navdy.hud.app.ui.component.SystemTrayView$Device.Gps, com.navdy.hud.app.ui.component.SystemTrayView$State.LOCATION_LOST);
            }
        }
    }
    
    public void onNetworkStateChange(com.navdy.service.library.events.settings.NetworkStateChange a) {
        com.navdy.hud.app.ui.component.SystemTrayView a0 = this.getSystemTray();
        if (a0 != null) {
            if (Boolean.TRUE.equals(a.networkAvailable)) {
                a0.setState(com.navdy.hud.app.ui.component.SystemTrayView$Device.Phone, com.navdy.hud.app.ui.component.SystemTrayView$State.PHONE_NETWORK_AVAILABLE);
            } else {
                a0.setState(com.navdy.hud.app.ui.component.SystemTrayView$Device.Phone, com.navdy.hud.app.ui.component.SystemTrayView$State.NO_PHONE_NETWORK);
            }
        }
    }
    
    public void onNotificationChange(com.navdy.hud.app.framework.notifications.NotificationManager$NotificationChange a) {
        com.navdy.hud.app.view.MainView a0 = (com.navdy.hud.app.view.MainView)this.getView();
        if (a0 != null) {
            this.setNotificationColorVisibility(a0.getNotificationColorVisibility());
        }
    }
    
    public void onPhoneBatteryStatusEvent(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus a) {
        com.navdy.hud.app.ui.activity.Main.access$200().v(new StringBuilder().append("PhoneBatteryStatus status[").append(a.status).append("] level[").append(a.status).append("] charging[").append(a.charging).append("]").toString());
        com.navdy.hud.app.ui.component.SystemTrayView a0 = this.getSystemTray();
        if (a0 != null) {
            if (Boolean.TRUE.equals(a.charging)) {
                a0.setState(com.navdy.hud.app.ui.component.SystemTrayView$Device.Phone, com.navdy.hud.app.ui.component.SystemTrayView$State.OK_BATTERY);
            } else {
                switch(com.navdy.hud.app.ui.activity.Main$1.$SwitchMap$com$navdy$service$library$events$callcontrol$PhoneBatteryStatus$BatteryStatus[a.status.ordinal()]) {
                    case 3: {
                        a0.setState(com.navdy.hud.app.ui.component.SystemTrayView$Device.Phone, com.navdy.hud.app.ui.component.SystemTrayView$State.LOW_BATTERY);
                        break;
                    }
                    case 2: {
                        a0.setState(com.navdy.hud.app.ui.component.SystemTrayView$Device.Phone, com.navdy.hud.app.ui.component.SystemTrayView$State.EXTREMELY_LOW_BATTERY);
                        break;
                    }
                    case 1: {
                        a0.setState(com.navdy.hud.app.ui.component.SystemTrayView$Device.Phone, com.navdy.hud.app.ui.component.SystemTrayView$State.OK_BATTERY);
                        break;
                    }
                }
            }
        }
    }
    
    public void onReadSettings(com.navdy.service.library.events.settings.ReadSettingsRequest a) {
        com.navdy.service.library.device.RemoteDevice a0 = this.connectionHandler.getRemoteDevice();
        if (a0 != null) {
            com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration a1 = this.getScreenConfiguration();
            android.graphics.Rect a2 = a1.getMargins();
            a0.postEvent((com.squareup.wire.Message)new com.navdy.service.library.events.settings.ReadSettingsResponse(new com.navdy.service.library.events.settings.ScreenConfiguration(Integer.valueOf(a2.left), Integer.valueOf(a2.top), Integer.valueOf(a2.right), Integer.valueOf(a2.bottom), Float.valueOf(a1.getScaleX()), Float.valueOf(a1.getScaleY())), this.hudSettings.readSettings(a.settings)));
        }
    }
    
    public void onSave(android.os.Bundle a) {
        a.putParcelable("main.settings", (android.os.Parcelable)this.settings);
        this.persistSettings();
        if (this.services != null) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.ui.activity.Main$Presenter$6(this), 1);
        }
        super.onSave(a);
    }
    
    public void onShowScreen(com.navdy.hud.app.event.ShowScreenWithArgs a) {
        if (this.uiStateManager.getRootScreen() != null) {
            this.updateScreen(a.screen, a.args, a.args2, a.ignoreAnimation, false);
        } else {
            com.navdy.hud.app.ui.activity.Main.access$200().w(new StringBuilder().append("Main screen not up, cannot show screen-args:").append(a).toString());
        }
    }
    
    public void onShowScreen(com.navdy.service.library.events.ui.ShowScreen a) {
        label2: if (this.uiStateManager.getRootScreen() != null) {
            java.util.List a0 = a.arguments;
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (a.arguments.size() != 0) {
                        break label0;
                    }
                }
                this.updateScreen(a.screen, (android.os.Bundle)null, null, false, false);
                break label2;
            }
            java.util.List a1 = a.arguments;
            android.os.Bundle a2 = new android.os.Bundle();
            Object a3 = a1.iterator();
            while(((java.util.Iterator)a3).hasNext()) {
                com.navdy.service.library.events.glances.KeyValue a4 = (com.navdy.service.library.events.glances.KeyValue)((java.util.Iterator)a3).next();
                if (a4.key != null && a4.value != null) {
                    a2.putString(a4.key, a4.value);
                }
            }
            this.updateScreen(a.screen, a2, null, false, false);
        } else {
            com.navdy.hud.app.ui.activity.Main.access$200().w(new StringBuilder().append("Main screen not up, cannot show screen:").append(a).toString());
        }
    }
    
    public void onShutdownEvent(com.navdy.hud.app.event.Shutdown a) {
        switch(com.navdy.hud.app.ui.activity.Main$1.$SwitchMap$com$navdy$hud$app$event$Shutdown$State[a.state.ordinal()]) {
            case 2: {
                android.content.Context a0 = com.navdy.hud.app.HudApplication.getAppContext();
                com.navdy.hud.app.ui.activity.Main.access$200().i("Stopping update service");
                a0.stopService(new android.content.Intent(a0, com.navdy.hud.app.util.OTAUpdateService.class));
                break;
            }
            case 1: {
                android.os.Bundle a1 = a.reason.asBundle();
                this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_SHUTDOWN_CONFIRMATION, a1, false));
                break;
            }
        }
    }
    
    public void onUpdateSettings(com.navdy.service.library.events.settings.UpdateSettings a) {
        com.navdy.service.library.events.settings.ScreenConfiguration a0 = a.screenConfiguration;
        if (a0 != null) {
            com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration a1 = this.getScreenConfiguration();
            a1.setMargins(new android.graphics.Rect(a0.marginLeft.intValue(), a0.marginTop.intValue(), a0.marginRight.intValue(), a0.marginBottom.intValue()));
            a1.setScaleX(a0.scaleX.floatValue());
            a1.setScaleY(a0.scaleY.floatValue());
        }
        this.hudSettings.updateSettings(a.settings);
        this.persistSettings();
        this.updateView();
    }
    
    public void onWakeUp(com.navdy.hud.app.event.Wakeup a) {
        this.startLateServices();
    }
    
    public void playMedia(android.content.Context a, android.net.Uri a0) {
        com.navdy.hud.app.ui.activity.Main.access$200().d(new StringBuilder().append("playMedia:").append(a0).toString());
        try {
            android.content.Intent a1 = new android.content.Intent("android.intent.action.VIEW");
            a1.setDataAndType(a0, "video/mp4");
            a1.addFlags(268435456);
            a.startActivity(a1);
        } catch(Exception a2) {
            com.navdy.hud.app.ui.activity.Main.access$200().e("Unable to start video loop", (Throwable)a2);
        }
    }
    
    public void removeNotificationExtensionView() {
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        if (a != null) {
            a.removeNotificationExtensionView();
        }
    }
    
    public void setInputFocus() {
        if (this.inputManager.getFocus() instanceof com.navdy.hud.app.view.ToastView) {
            com.navdy.hud.app.ui.activity.Main.access$200().v("[focus] not setting focus[toast]");
        } else {
            com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
            if (a != null) {
                if (this.isNotificationViewShowing()) {
                    this.inputManager.setFocus((com.navdy.hud.app.manager.InputManager$IInputHandler)a.getNotificationView());
                    com.navdy.hud.app.ui.activity.Main.access$200().v("[focus] notification");
                } else {
                    com.navdy.hud.app.view.ContainerView a0 = a.getContainerView();
                    if (a0 != null) {
                        android.view.View a1 = a0.getCurrentView();
                        if (a1 instanceof com.navdy.hud.app.manager.InputManager$IInputHandler) {
                            this.inputManager.setFocus((com.navdy.hud.app.manager.InputManager$IInputHandler)a1);
                            com.navdy.hud.app.ui.activity.Main.access$200().v(new StringBuilder().append("[focus] screen:").append((a1).getClass().getSimpleName()).toString());
                        } else {
                            this.inputManager.setFocus((com.navdy.hud.app.manager.InputManager$IInputHandler)a);
                            com.navdy.hud.app.ui.activity.Main.access$200().v("[focus] mainView");
                        }
                    } else {
                        this.inputManager.setFocus((com.navdy.hud.app.manager.InputManager$IInputHandler)a);
                        com.navdy.hud.app.ui.activity.Main.access$200().v("[focus] mainView");
                    }
                }
            } else {
                com.navdy.hud.app.ui.activity.Main.access$200().v("[focus] mainview is null");
            }
        }
    }
    
    public void setMargins(int i, int i0, int i1, int i2) {
        this.getScreenConfiguration().setMargins(new android.graphics.Rect(0, i0, 0, 0));
        this.persistSettings();
        this.updateView();
    }
    
    void setNotificationColorVisibility(int i) {
        label2: if (this.notificationColorEnabled) {
            com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
            if (a != null) {
                label4: {
                    label3: {
                        if (i == 0) {
                            break label3;
                        }
                        a.setNotificationColor(this.defaultNotifColor);
                        a.setNotificationColorVisibility(i);
                        break label4;
                    }
                    com.navdy.hud.app.framework.notifications.INotification a0 = this.notificationManager.getCurrentNotification();
                    label0: {
                        if (a0 == null) {
                            break label0;
                        }
                        boolean b = this.isNotificationExpanding();
                        label1: {
                            if (b) {
                                break label1;
                            }
                            if (!this.isNotificationViewShowing()) {
                                break label0;
                            }
                        }
                        com.navdy.hud.app.ui.activity.Main.access$200().v("not showing notifcolor, pending notif");
                        break label2;
                    }
                    int i0 = this.notificationManager.getNotificationColor();
                    if (i0 == -1) {
                        i0 = this.defaultNotifColor;
                    }
                    a.setNotificationColor(i0);
                    a.setNotificationColorVisibility(i);
                }
                com.navdy.hud.app.ui.activity.Main.access$200().v(new StringBuilder().append("notifColor: ").append((i != 0) ? "invisible" : "visible").toString());
            }
        } else {
            com.navdy.hud.app.ui.activity.Main.access$200().v("notifColor is disabled");
        }
    }
    
    public void setSystemTrayVisibility(int i) {
        label2: if (this.systemTrayEnabled) {
            com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
            if (a != null) {
                label0: {
                    if (i != 0) {
                        break label0;
                    }
                    if (this.notificationManager.getCurrentNotification() == null) {
                        break label0;
                    }
                    boolean b = this.isNotificationExpanding();
                    label1: {
                        if (b) {
                            break label1;
                        }
                        if (!this.isNotificationViewShowing()) {
                            break label0;
                        }
                    }
                    com.navdy.hud.app.ui.activity.Main.access$200().v("not showing systemtray, pending notif");
                    break label2;
                }
                a.getSystemTray().setVisibility(i);
                com.navdy.hud.app.ui.activity.Main.access$200().v(new StringBuilder().append("systemtray is ").append((i != 0) ? "invisible" : "visible").toString());
            }
        } else {
            com.navdy.hud.app.ui.activity.Main.access$200().v("systemtray is disabled");
        }
    }
    
    public void startVoiceAssistance(boolean b) {
        com.navdy.hud.app.ui.activity.Main.access$200().v(new StringBuilder().append("startVoiceAssistance , Voice search : ").append(b).toString());
        label2: if (this.notificationManager.isNotificationViewShowing()) {
            com.navdy.hud.app.ui.activity.Main.access$200().v("startVoiceAssistance:add pending");
            com.navdy.hud.app.framework.notifications.NotificationManager a = this.notificationManager;
            Object a0 = b ? this.voiceSearchNotification : this.voiceAssistNotification;
            a.addPendingNotification((com.navdy.hud.app.framework.notifications.INotification)a0);
            boolean b0 = this.notificationManager.isExpanded();
            label0: {
                label1: {
                    if (b0) {
                        break label1;
                    }
                    if (!this.notificationManager.isExpandedNotificationVisible()) {
                        break label0;
                    }
                }
                this.notificationManager.collapseExpandedNotification(true, true);
                break label2;
            }
            this.notificationManager.collapseNotification();
        } else {
            this.launchVoiceAssistance(b);
        }
    }
    
    public void updateView() {
        com.navdy.hud.app.view.MainView a = (com.navdy.hud.app.view.MainView)this.getView();
        if (a != null) {
            a.setVerticalOffset(this.getScreenConfiguration().getMargins().top);
            this.setNotificationColorVisibility(0);
            this.setSystemTrayVisibility(0);
            com.navdy.hud.app.ui.component.SystemTrayView a0 = this.getSystemTray();
            if (a0 != null) {
                a0.setState(com.navdy.hud.app.ui.component.SystemTrayView$Device.Dial, (com.navdy.hud.app.device.dial.DialManager.getInstance().isDialConnected()) ? com.navdy.hud.app.ui.component.SystemTrayView$State.CONNECTED : com.navdy.hud.app.ui.component.SystemTrayView$State.DISCONNECTED);
                a0.setState(com.navdy.hud.app.ui.component.SystemTrayView$Device.Phone, (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) ? com.navdy.hud.app.ui.component.SystemTrayView$State.CONNECTED : com.navdy.hud.app.ui.component.SystemTrayView$State.DISCONNECTED);
                com.navdy.hud.app.maps.here.HereMapsManager a1 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
                if (a1.isInitialized()) {
                    com.navdy.hud.app.maps.here.HereLocationFixManager a2 = a1.getLocationFixManager();
                    if (a2 != null && !a2.hasLocationFix()) {
                        a0.setState(com.navdy.hud.app.ui.component.SystemTrayView$Device.Gps, com.navdy.hud.app.ui.component.SystemTrayView$State.LOCATION_LOST);
                    }
                }
            }
        }
    }
}
