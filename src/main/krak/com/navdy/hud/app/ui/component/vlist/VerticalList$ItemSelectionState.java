package com.navdy.hud.app.ui.component.vlist;

public class VerticalList$ItemSelectionState {
    public int id;
    public com.navdy.hud.app.ui.component.vlist.VerticalList$Model model;
    public int pos;
    public int subId;
    public int subPosition;
    
    public VerticalList$ItemSelectionState() {
        this.id = -1;
        this.pos = -1;
        this.subId = -1;
        this.subPosition = -1;
    }
    
    public void set(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, int i, int i0, int i1, int i2) {
        this.model = a;
        this.id = i;
        this.pos = i0;
        this.subId = i1;
        this.subPosition = i2;
    }
}
