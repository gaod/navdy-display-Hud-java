package com.navdy.hud.app.ui.component.mainmenu;
import com.navdy.hud.app.R;

public class SystemInfoMenu$$ViewInjector {
    public SystemInfoMenu$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu a0, Object a1) {
        a0.displayFw = (android.widget.TextView)a.findRequiredView(a1, R.id.si_display_fw, "field 'displayFw'");
        a0.dialFw = (android.widget.TextView)a.findRequiredView(a1, R.id.si_dial_fw, "field 'dialFw'");
        a0.obdFw = (android.widget.TextView)a.findRequiredView(a1, R.id.si_obd_fw, "field 'obdFw'");
        a0.dialBt = (android.widget.TextView)a.findRequiredView(a1, R.id.si_dial_bt, "field 'dialBt'");
        a0.displayBt = (android.widget.TextView)a.findRequiredView(a1, R.id.si_display_bt, "field 'displayBt'");
        a0.gps = (android.widget.TextView)a.findRequiredView(a1, R.id.si_gps, "field 'gps'");
        a0.satellites = (android.widget.TextView)a.findRequiredView(a1, R.id.si_satellites, "field 'satellites'");
        a0.satelliteView = (com.navdy.hud.app.ui.component.systeminfo.GpsSignalView)a.findRequiredView(a1, R.id.gps_signal, "field 'satelliteView'");
        a0.fix = (android.widget.TextView)a.findRequiredView(a1, R.id.si_fix, "field 'fix'");
        a0.temperature = (android.widget.TextView)a.findRequiredView(a1, R.id.si_temperature, "field 'temperature'");
        a0.deviceMileage = (android.widget.TextView)a.findRequiredView(a1, R.id.si_device_mileage, "field 'deviceMileage'");
        a0.gpsSpeed = (android.widget.TextView)a.findRequiredView(a1, R.id.si_gps_speed, "field 'gpsSpeed'");
        a0.hereSdk = (android.widget.TextView)a.findRequiredView(a1, R.id.si_here_sdk_version, "field 'hereSdk'");
        a0.hereUpdated = (android.widget.TextView)a.findRequiredView(a1, R.id.si_updated, "field 'hereUpdated'");
        a0.hereMapsVersion = (android.widget.TextView)a.findRequiredView(a1, R.id.si_map_version, "field 'hereMapsVersion'");
        a0.hereOfflineMaps = (android.widget.TextView)a.findRequiredView(a1, R.id.si_offline_maps, "field 'hereOfflineMaps'");
        a0.hereMapVerified = (android.widget.TextView)a.findRequiredView(a1, R.id.si_maps_data_verified, "field 'hereMapVerified'");
        a0.hereVoiceVerified = (android.widget.TextView)a.findRequiredView(a1, R.id.si_maps_voices_verified, "field 'hereVoiceVerified'");
        a0.make = (android.widget.TextView)a.findRequiredView(a1, R.id.si_make, "field 'make'");
        a0.carModel = (android.widget.TextView)a.findRequiredView(a1, R.id.si_model, "field 'carModel'");
        a0.year = (android.widget.TextView)a.findRequiredView(a1, R.id.si_year, "field 'year'");
        a0.fuel = (android.widget.TextView)a.findRequiredView(a1, R.id.si_fuel, "field 'fuel'");
        a0.rpm = (android.widget.TextView)a.findRequiredView(a1, R.id.si_rpm, "field 'rpm'");
        a0.speed = (android.widget.TextView)a.findRequiredView(a1, R.id.si_speed, "field 'speed'");
        a0.odo = (android.widget.TextView)a.findRequiredView(a1, R.id.si_odometer, "field 'odo'");
        a0.maf = (android.widget.TextView)a.findRequiredView(a1, R.id.si_maf, "field 'maf'");
        a0.engineTemp = (android.widget.TextView)a.findRequiredView(a1, R.id.si_eng_temp, "field 'engineTemp'");
        a0.voltage = (android.widget.TextView)a.findRequiredView(a1, R.id.si_voltage, "field 'voltage'");
        a0.gestures = (android.widget.TextView)a.findRequiredView(a1, R.id.si_gestures, "field 'gestures'");
        a0.autoOn = (android.widget.TextView)a.findRequiredView(a1, R.id.si_auto_on, "field 'autoOn'");
        a0.obdData = (android.widget.TextView)a.findRequiredView(a1, R.id.si_obd_data, "field 'obdData'");
        a0.uiScaling = (android.widget.TextView)a.findRequiredView(a1, R.id.si_ui_scaling, "field 'uiScaling'");
        a0.routeCalc = (android.widget.TextView)a.findRequiredView(a1, R.id.si_route_calc, "field 'routeCalc'");
        a0.routePref = (android.widget.TextView)a.findRequiredView(a1, R.id.si_route_pref, "field 'routePref'");
        a0.engineOilPressureLayout = (android.view.ViewGroup)a.findRequiredView(a1, R.id.si_engine_oil_pressure_lyt, "field 'engineOilPressureLayout'");
        a0.engineOilPressure = (android.widget.TextView)a.findRequiredView(a1, R.id.si_eng_oil_pressure, "field 'engineOilPressure'");
        a0.specialPidsLayout = (android.view.ViewGroup)a.findRequiredView(a1, R.id.si_special_pids_lyt, "field 'specialPidsLayout'");
        a0.tripFuel = (android.widget.TextView)a.findRequiredView(a1, R.id.si_eng_trip_fuel, "field 'tripFuel'");
        a0.tripFuelLayout = (android.view.ViewGroup)a.findRequiredView(a1, R.id.si_trip_fuel_layout, "field 'tripFuelLayout'");
        a0.checkEngineLight = (android.widget.TextView)a.findRequiredView(a1, R.id.si_check_engine_light, "field 'checkEngineLight'");
        a0.engineTroubleCodes = (android.widget.TextView)a.findRequiredView(a1, R.id.si_engine_trouble_codes, "field 'engineTroubleCodes'");
        a0.odometerLayout = (android.view.ViewGroup)a.findRequiredView(a1, R.id.si_odometer_lyt, "field 'odometerLayout'");
        a0.obdDataLayout = (android.view.ViewGroup)a.findRequiredView(a1, R.id.obd_data_lyt, "field 'obdDataLayout'");
    }
    
    public static void reset(com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu a) {
        a.displayFw = null;
        a.dialFw = null;
        a.obdFw = null;
        a.dialBt = null;
        a.displayBt = null;
        a.gps = null;
        a.satellites = null;
        a.satelliteView = null;
        a.fix = null;
        a.temperature = null;
        a.deviceMileage = null;
        a.gpsSpeed = null;
        a.hereSdk = null;
        a.hereUpdated = null;
        a.hereMapsVersion = null;
        a.hereOfflineMaps = null;
        a.hereMapVerified = null;
        a.hereVoiceVerified = null;
        a.make = null;
        a.carModel = null;
        a.year = null;
        a.fuel = null;
        a.rpm = null;
        a.speed = null;
        a.odo = null;
        a.maf = null;
        a.engineTemp = null;
        a.voltage = null;
        a.gestures = null;
        a.autoOn = null;
        a.obdData = null;
        a.uiScaling = null;
        a.routeCalc = null;
        a.routePref = null;
        a.engineOilPressureLayout = null;
        a.engineOilPressure = null;
        a.specialPidsLayout = null;
        a.tripFuel = null;
        a.tripFuelLayout = null;
        a.checkEngineLight = null;
        a.engineTroubleCodes = null;
        a.odometerLayout = null;
        a.obdDataLayout = null;
    }
}
