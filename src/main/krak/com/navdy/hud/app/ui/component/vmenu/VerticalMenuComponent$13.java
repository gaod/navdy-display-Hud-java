package com.navdy.hud.app.ui.component.vmenu;

class VerticalMenuComponent$13 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent this$0;
    final Runnable val$endAction;
    final Runnable val$startAction;
    
    VerticalMenuComponent$13(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a, Runnable a0, Runnable a1) {
        super();
        this.this$0 = a;
        this.val$startAction = a0;
        this.val$endAction = a1;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        this.this$0.selectedImage.setAlpha(1f);
        this.this$0.selectedText.setAlpha(1f);
        com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.access$1000(this.this$0).setVisibility(4);
        com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.access$1100(this.this$0).setVisibility(4);
        com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.access$1200(this.this$0).setVisibility(4);
        if (this.val$endAction != null) {
            this.val$endAction.run();
        }
    }
    
    public void onAnimationStart(android.animation.Animator a) {
        if (this.val$startAction != null) {
            this.val$startAction.run();
        }
        this.this$0.rightContainer.setPivotX(0.0f);
        this.this$0.rightContainer.setPivotY((float)(this.this$0.recyclerView.getMeasuredHeight() / 2));
        this.this$0.rightContainer.setScaleX(0.5f);
        this.this$0.rightContainer.setScaleY(0.5f);
    }
}
