package com.navdy.hud.app.ui.component.vlist.viewholder;
import com.navdy.hud.app.R;

public class IconOptionsViewHolder extends com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder {
    final private static int iconMargin;
    final private static com.navdy.service.library.log.Logger sLogger;
    protected android.animation.AnimatorSet$Builder animatorSetBuilder;
    private int currentSelection;
    private Runnable fluctuatorRunnable;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener fluctuatorStartListener;
    private boolean itemNotSelected;
    private com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State lastState;
    private com.navdy.hud.app.ui.component.vlist.VerticalList$Model model;
    private java.util.ArrayList viewContainers;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.class);
        iconMargin = com.navdy.hud.app.HudApplication.getAppContext().getResources().getDimensionPixelOffset(R.dimen.vlist_icon_list_margin);
    }
    
    public IconOptionsViewHolder(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, android.os.Handler a1) {
        super(a, a0, a1);
        this.viewContainers = new java.util.ArrayList();
        this.currentSelection = -1;
        this.fluctuatorStartListener = new com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$1(this);
        this.fluctuatorRunnable = (Runnable)new com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$2(this);
        a.setPivotX((float)(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight / 2));
        a.setPivotY((float)(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.listItemHeight / 2));
    }
    
    static Runnable access$000(com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder a) {
        return a.fluctuatorRunnable;
    }
    
    static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$200(com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder a) {
        return a.model;
    }
    
    static int access$300(com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder a) {
        return a.currentSelection;
    }
    
    public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model buildModel(int i, int[] a, int[] a0, int[] a1, int[] a2, int[] a3, int i0, boolean b) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a4 = new com.navdy.hud.app.ui.component.vlist.VerticalList$Model();
        a4.type = com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.ICON_OPTIONS;
        a4.id = i;
        a4.iconList = a;
        a4.iconIds = a0;
        a4.iconSelectedColors = a1;
        a4.iconDeselectedColors = a2;
        a4.iconFluctuatorColors = a3;
        a4.currentIconSelection = i0;
        a4.supportsToolTip = b;
        return a4;
    }
    
    public static com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder buildViewHolder(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, android.os.Handler a1) {
        return new com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder((android.view.ViewGroup)android.view.LayoutInflater.from(a.getContext()).inflate(R.layout.vlist_icon_options, a, false), a0, a1);
    }
    
    public static int getOffSetFromPos(int i) {
        return (i > 0) ? com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.mainIconSize * i + iconMargin * i : 0;
    }
    
    private boolean isItemInBounds(int i) {
        boolean b = false;
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = this.model;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (i < 0) {
                        break label1;
                    }
                    if (i < this.model.iconList.length) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private void startFluctuator(int i, boolean b) {
        if (this.isItemInBounds(i)) {
            com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$ViewContainer a = (com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$ViewContainer)this.viewContainers.get(i);
            if (b) {
                a.big.setIcon((this.model.iconList[i] != 0) ? 1 : 0, (this.model.iconSelectedColors[i] != 0) ? 1 : 0, (android.graphics.Shader)null, 0.83f);
            }
            a.haloView.setVisibility(0);
            a.haloView.start();
        }
    }
    
    private void stopFluctuator(int i, boolean b) {
        if (this.isItemInBounds(i)) {
            this.handler.removeCallbacks(this.fluctuatorRunnable);
            com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$ViewContainer a = (com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$ViewContainer)this.viewContainers.get(i);
            if (b) {
                a.big.setIcon((this.model.iconList[i] != 0) ? 1 : 0, (this.model.iconDeselectedColors[i] != 0) ? 1 : 0, (android.graphics.Shader)null, 0.83f);
            }
            a.haloView.setVisibility(8);
            a.haloView.stop();
        }
    }
    
    public void bind(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a0) {
        if (this.vlist.adapter.getModel(this.vlist.getRawPosition()) != a) {
            this.itemNotSelected = true;
        }
        int i = 0;
        while(i < a.iconList.length) {
            com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$ViewContainer a1 = (com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$ViewContainer)this.viewContainers.get(i);
            a1.big.setIcon((a.iconList[i] != 0) ? 1 : 0, (a.iconSelectedColors[i] != 0) ? 1 : 0, (android.graphics.Shader)null, 0.83f);
            a1.small.setIcon((a.iconList[i] != 0) ? 1 : 0, (a.iconDeselectedColors[i] != 0) ? 1 : 0, (android.graphics.Shader)null, 0.83f);
            int i0 = android.graphics.Color.argb(153, android.graphics.Color.red((a.iconFluctuatorColors[i] != 0) ? 1 : 0), android.graphics.Color.green((a.iconFluctuatorColors[i] != 0) ? 1 : 0), android.graphics.Color.blue((a.iconFluctuatorColors[i] != 0) ? 1 : 0));
            a1.haloView.setVisibility(4);
            a1.haloView.setStrokeColor(i0);
            i = i + 1;
        }
    }
    
    public void clearAnimation() {
        this.stopFluctuator(this.currentSelection, true);
        this.stopAnimation();
        this.currentState = null;
        this.layout.setVisibility(0);
        this.layout.setAlpha(1f);
    }
    
    public void copyAndPosition(android.widget.ImageView a, android.widget.TextView a0, android.widget.TextView a1, android.widget.TextView a2, boolean b) {
        if (this.currentSelection >= 0) {
            android.view.View a3 = ((com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$ViewContainer)this.viewContainers.get(this.currentSelection)).crossFadeImageView.getBig();
            if (b) {
                com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.copyImage((android.widget.ImageView)a3, a);
            }
            a.setX((float)(selectedImageX + com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.getOffSetFromPos(this.currentSelection)));
            a.setY((float)selectedImageY);
        }
    }
    
    public int getCurrentSelection() {
        return this.currentSelection;
    }
    
    public int getCurrentSelectionId() {
        int i = 0;
        int i0 = this.currentSelection;
        label2: {
            label0: {
                label1: {
                    if (i0 < 0) {
                        break label1;
                    }
                    if (this.currentSelection < this.model.iconIds.length) {
                        break label0;
                    }
                }
                i = -1;
                break label2;
            }
            i = this.model.iconIds[this.currentSelection];
        }
        return i;
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType getModelType() {
        return com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.ICON_OPTIONS;
    }
    
    public boolean handleKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        if (this.isItemInBounds(this.currentSelection)) {
            switch(com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$4.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                case 2: {
                    if (this.currentSelection == this.model.iconList.length - 1) {
                        b = false;
                        break;
                    } else {
                        this.currentSelection = this.currentSelection + 1;
                        this.stopFluctuator(this.currentSelection - 1, true);
                        this.startFluctuator(this.currentSelection, true);
                        com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a0 = this.vlist.getItemSelectionState();
                        a0.set(this.model, this.model.id, this.vlist.getCurrentPosition(), (this.model.iconIds[this.currentSelection] != 0) ? 1 : 0, this.currentSelection);
                        this.vlist.callItemSelected(a0);
                        b = true;
                        break;
                    }
                }
                case 1: {
                    if (this.currentSelection == 0) {
                        b = false;
                        break;
                    } else {
                        this.currentSelection = this.currentSelection - 1;
                        this.stopFluctuator(this.currentSelection + 1, true);
                        this.startFluctuator(this.currentSelection, true);
                        com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a1 = this.vlist.getItemSelectionState();
                        a1.set(this.model, this.model.id, this.vlist.getCurrentPosition(), (this.model.iconIds[this.currentSelection] != 0) ? 1 : 0, this.currentSelection);
                        this.vlist.callItemSelected(a1);
                        b = true;
                        break;
                    }
                }
                default: {
                    b = false;
                }
            }
        } else {
            b = false;
        }
        return b;
    }
    
    public boolean hasToolTip() {
        return this.model.supportsToolTip;
    }
    
    public void preBind(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a0) {
        this.model = a;
        android.content.Context a1 = this.layout.getContext();
        if (this.layout.getChildCount() == a.iconList.length + a.iconList.length - 1) {
            sLogger.v("preBind: createIcons: already created");
        } else {
            this.currentSelection = a.currentIconSelection;
            sLogger.v(new StringBuilder().append("preBind: createIcons:").append(a.iconList.length).toString());
            this.layout.removeAllViews();
            this.viewContainers.clear();
            android.view.LayoutInflater a2 = android.view.LayoutInflater.from(this.layout.getContext());
            int i = 0;
            while(i < a.iconList.length) {
                if (i != 0) {
                    android.view.View a3 = new android.view.View(a1);
                    a3.setLayoutParams((android.view.ViewGroup$LayoutParams)new android.view.ViewGroup$MarginLayoutParams(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.iconMargin, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.mainIconSize));
                    this.layout.addView(a3);
                }
                com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$ViewContainer a4 = new com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$ViewContainer((com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$1)null);
                android.view.ViewGroup a5 = (android.view.ViewGroup)a2.inflate(R.layout.vlist_halo_image, this.layout, false);
                a4.iconContainer = (android.view.ViewGroup)a5.findViewById(R.id.iconContainer);
                a4.crossFadeImageView = (com.navdy.hud.app.ui.component.image.CrossFadeImageView)a5.findViewById(R.id.crossFadeImageView);
                a4.crossFadeImageView.inject(com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.SMALL);
                a4.big = (com.navdy.hud.app.ui.component.image.IconColorImageView)a5.findViewById(R.id.big);
                a4.small = (com.navdy.hud.app.ui.component.image.IconColorImageView)a5.findViewById(R.id.small);
                a4.haloView = (com.navdy.hud.app.ui.component.HaloView)a5.findViewById(R.id.halo);
                this.viewContainers.add(a4);
                a5.setLayoutParams((android.view.ViewGroup$LayoutParams)new android.view.ViewGroup$MarginLayoutParams(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.mainIconSize, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.mainIconSize));
                this.layout.addView((android.view.View)a5);
                i = i + 1;
            }
        }
    }
    
    public void select(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, int i, int i0) {
        if (this.isItemInBounds(this.currentSelection)) {
            this.stopFluctuator(this.currentSelection, false);
            com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.performClick((android.view.View)((com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$ViewContainer)this.viewContainers.get(this.currentSelection)).iconContainer, i0, (Runnable)new com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$3(this, i));
        }
    }
    
    public void selectionDown() {
        sLogger.v("selectionDown");
        if (this.isItemInBounds(this.currentSelection)) {
            this.stopFluctuator(this.currentSelection, false);
            com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.performClickDown((android.view.View)((com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$ViewContainer)this.viewContainers.get(this.currentSelection)).iconContainer, 25, (Runnable)null, false);
        } else {
            sLogger.v(new StringBuilder().append("selectionDown:invalid pos:").append(this.currentSelection).toString());
        }
    }
    
    public void selectionUp() {
        sLogger.v("selectionUp");
        if (this.isItemInBounds(this.currentSelection)) {
            this.startFluctuator(this.currentSelection, false);
            com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.performClickUp((android.view.View)((com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$ViewContainer)this.viewContainers.get(this.currentSelection)).iconContainer, 25, (Runnable)null);
        } else {
            sLogger.v(new StringBuilder().append("selectionUp:invalid pos:").append(this.currentSelection).toString());
        }
    }
    
    public void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State a, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType a0, int i, boolean b) {
        if (this.lastState != a) {
            boolean b0 = false;
            float f = 0.0f;
            if (this.itemNotSelected) {
                this.itemNotSelected = false;
                com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType a1 = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.MOVE;
                b0 = true;
                a0 = a1;
            } else {
                b0 = false;
            }
            this.animatorSetBuilder = null;
            int i0 = com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$4.$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$State[a.ordinal()];
            com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode a2 = null;
            switch(i0) {
                case 2: {
                    a2 = com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.SMALL;
                    f = 0.6f;
                    break;
                }
                case 1: {
                    a2 = com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.BIG;
                    f = 1f;
                    break;
                }
                default: {
                    f = 0.0f;
                }
            }
            switch(com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$4.$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType[a0.ordinal()]) {
                case 3: {
                    if (b0) {
                        int i1 = 0;
                        while(i1 < this.model.iconList.length) {
                            com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$ViewContainer a3 = (com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$ViewContainer)this.viewContainers.get(i1);
                            a3.big.setIcon((this.model.iconList[i1] != 0) ? 1 : 0, (this.model.iconDeselectedColors[i1] != 0) ? 1 : 0, (android.graphics.Shader)null, 0.83f);
                            if (a3.crossFadeImageView.getMode() != a2) {
                                a3.crossFadeImageView.setMode(a2);
                            }
                            i1 = i1 + 1;
                        }
                        this.layout.setScaleX(f);
                        this.layout.setScaleY(f);
                        break;
                    } else {
                        boolean b1 = false;
                        this.itemAnimatorSet = new android.animation.AnimatorSet();
                        if (this.currentSelection == -1) {
                            b1 = false;
                        } else {
                            com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$ViewContainer a4 = (com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$ViewContainer)this.viewContainers.get(this.currentSelection);
                            if (a4.crossFadeImageView.getMode() == a2) {
                                b1 = false;
                            } else {
                                a4.crossFadeImageView.setMode(a2);
                                b1 = true;
                            }
                        }
                        int i2 = 0;
                        while(i2 < this.model.iconList.length) {
                            com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$ViewContainer a5 = (com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$ViewContainer)this.viewContainers.get(i2);
                            int i3 = this.currentSelection;
                            label0: {
                                label1: {
                                    if (i2 != i3) {
                                        break label1;
                                    }
                                    if (b1) {
                                        break label0;
                                    }
                                }
                                if (a5.crossFadeImageView.getMode() != a2) {
                                    if (this.animatorSetBuilder != null) {
                                        this.animatorSetBuilder.with((android.animation.Animator)a5.crossFadeImageView.getCrossFadeAnimator());
                                    } else {
                                        this.animatorSetBuilder = this.itemAnimatorSet.play((android.animation.Animator)a5.crossFadeImageView.getCrossFadeAnimator());
                                    }
                                }
                            }
                            i2 = i2 + 1;
                        }
                        android.util.Property a6 = android.view.View.SCALE_X;
                        float[] a7 = new float[1];
                        a7[0] = f;
                        android.animation.PropertyValuesHolder a8 = android.animation.PropertyValuesHolder.ofFloat(a6, a7);
                        android.util.Property a9 = android.view.View.SCALE_Y;
                        float[] a10 = new float[1];
                        a10[0] = f;
                        android.animation.PropertyValuesHolder a11 = android.animation.PropertyValuesHolder.ofFloat(a9, a10);
                        if (this.animatorSetBuilder != null) {
                            android.animation.AnimatorSet$Builder a12 = this.animatorSetBuilder;
                            android.view.ViewGroup a13 = this.layout;
                            android.animation.PropertyValuesHolder[] a14 = new android.animation.PropertyValuesHolder[2];
                            a14[0] = a8;
                            a14[1] = a11;
                            a12.with((android.animation.Animator)android.animation.ObjectAnimator.ofPropertyValuesHolder(a13, a14));
                            break;
                        } else {
                            android.animation.AnimatorSet a15 = this.itemAnimatorSet;
                            android.view.ViewGroup a16 = this.layout;
                            android.animation.PropertyValuesHolder[] a17 = new android.animation.PropertyValuesHolder[2];
                            a17[0] = a8;
                            a17[1] = a11;
                            this.animatorSetBuilder = a15.play((android.animation.Animator)android.animation.ObjectAnimator.ofPropertyValuesHolder(a16, a17));
                            break;
                        }
                    }
                }
                case 1: case 2: {
                    int i4 = 0;
                    while(i4 < this.model.iconList.length) {
                        com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$ViewContainer a18 = (com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$ViewContainer)this.viewContainers.get(i4);
                        a18.iconContainer.setScaleX(f);
                        a18.iconContainer.setScaleY(f);
                        a18.crossFadeImageView.setMode(a2);
                        if (i4 != this.currentSelection) {
                            a18.big.setIcon((this.model.iconList[i4] != 0) ? 1 : 0, (this.model.iconDeselectedColors[i4] != 0) ? 1 : 0, (android.graphics.Shader)null, 0.83f);
                        } else {
                            a18.big.setIcon((this.model.iconList[i4] != 0) ? 1 : 0, (this.model.iconSelectedColors[i4] != 0) ? 1 : 0, (android.graphics.Shader)null, 0.83f);
                        }
                        i4 = i4 + 1;
                    }
                    break;
                }
            }
            if (a == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED) {
                if (this.itemAnimatorSet == null) {
                    this.startFluctuator();
                } else {
                    this.itemAnimatorSet.addListener((android.animation.Animator$AnimatorListener)this.fluctuatorStartListener);
                }
            }
            this.lastState = a;
        }
    }
    
    public void startFluctuator() {
        this.startFluctuator(this.currentSelection, true);
    }
}
