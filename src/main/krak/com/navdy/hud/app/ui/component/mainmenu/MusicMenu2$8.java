package com.navdy.hud.app.ui.component.mainmenu;

class MusicMenu2$8 {
    final static int[] $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel;
    final static int[] $SwitchMap$com$navdy$service$library$events$audio$MusicCollectionType;
    
    static {
        $SwitchMap$com$navdy$service$library$events$audio$MusicCollectionType = new int[com.navdy.service.library.events.audio.MusicCollectionType.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$audio$MusicCollectionType;
        com.navdy.service.library.events.audio.MusicCollectionType a0 = com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PLAYLISTS;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$MusicCollectionType[com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$MusicCollectionType[com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel = new int[com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel;
        com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel a2 = com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.BACK_TO_PARENT;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel[com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.CLOSE.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException3) {
        }
    }
}
