package com.navdy.hud.app.ui.component;

public class HaloView extends android.view.View {
    private int animationDelay;
    private int animationDuration;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener animationListener;
    private android.animation.AnimatorSet animatorSet;
    private android.animation.AnimatorSet currentAnimatorSet;
    protected float currentStrokeWidth;
    protected float endRadius;
    protected float endStrokeWidth;
    private android.animation.AnimatorSet firstAnimatorSet;
    private boolean firstIterationDone;
    private android.animation.ValueAnimator firstRadiusAnimator;
    private android.os.Handler handler;
    private android.view.animation.Interpolator interpolator;
    protected float middleRadius;
    protected float middleStrokeWidth;
    private android.graphics.Paint paint;
    private android.animation.ValueAnimator radiusAnimator;
    private android.animation.AnimatorSet reverseAnimatorSet;
    private android.animation.ValueAnimator reverseRadiusAnimator;
    protected float startRadius;
    private Runnable startRunnable;
    private boolean started;
    private int strokeColor;
    private android.animation.ValueAnimator$AnimatorUpdateListener updateListener;
    
    public HaloView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public HaloView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public HaloView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.interpolator = (android.view.animation.Interpolator)new android.view.animation.AccelerateDecelerateInterpolator();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.animationListener = new com.navdy.hud.app.ui.component.HaloView$1(this);
        this.startRunnable = (Runnable)new com.navdy.hud.app.ui.component.HaloView$2(this);
        this.updateListener = (android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.HaloView$3(this);
        android.content.res.TypedArray a1 = a.obtainStyledAttributes(a0, com.navdy.hud.app.R$styleable.HaloView, i, 0);
        if (a1 != null) {
            this.strokeColor = a1.getColor(0, -1);
            this.startRadius = a1.getDimension(1, 0.0f);
            this.endRadius = a1.getDimension(2, 0.0f);
            this.middleRadius = a1.getDimension(3, 0.0f);
            this.animationDuration = a1.getInteger(4, 0);
            this.animationDelay = a1.getInteger(5, 0);
            a1.recycle();
        }
        this.endStrokeWidth = this.endRadius - this.startRadius;
        if (this.endStrokeWidth < 0.0f) {
            this.endStrokeWidth = 0.0f;
        }
        this.middleStrokeWidth = this.endRadius - this.middleRadius;
        if (this.middleStrokeWidth < 0.0f) {
            this.middleStrokeWidth = 0.0f;
        }
        this.paint = new android.graphics.Paint();
        this.paint.setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
        float[] a2 = new float[2];
        a2[0] = this.startRadius;
        a2[1] = this.endRadius;
        this.firstRadiusAnimator = android.animation.ValueAnimator.ofFloat(a2);
        float[] a3 = new float[2];
        a3[0] = this.middleRadius;
        a3[1] = this.endRadius;
        this.radiusAnimator = android.animation.ValueAnimator.ofFloat(a3);
        float[] a4 = new float[2];
        a4[0] = this.endRadius;
        a4[1] = this.middleRadius;
        this.reverseRadiusAnimator = android.animation.ValueAnimator.ofFloat(a4);
        this.firstRadiusAnimator.addUpdateListener(this.updateListener);
        this.radiusAnimator.addUpdateListener(this.updateListener);
        this.reverseRadiusAnimator.addUpdateListener(this.updateListener);
        this.firstAnimatorSet = new android.animation.AnimatorSet();
        this.firstAnimatorSet.setDuration((long)this.animationDuration);
        this.firstAnimatorSet.setInterpolator((android.animation.TimeInterpolator)this.interpolator);
        this.animatorSet = new android.animation.AnimatorSet();
        this.animatorSet.setDuration((long)this.animationDuration);
        this.animatorSet.setInterpolator((android.animation.TimeInterpolator)this.interpolator);
        this.reverseAnimatorSet = new android.animation.AnimatorSet();
        this.reverseAnimatorSet.setDuration((long)this.animationDuration);
        this.reverseAnimatorSet.setInterpolator((android.animation.TimeInterpolator)this.interpolator);
        this.animatorSet.play((android.animation.Animator)this.radiusAnimator);
        this.reverseAnimatorSet.play((android.animation.Animator)this.reverseRadiusAnimator);
        this.currentStrokeWidth = this.endStrokeWidth;
        this.invalidate();
    }
    
    static boolean access$000(com.navdy.hud.app.ui.component.HaloView a) {
        return a.started;
    }
    
    static Runnable access$100(com.navdy.hud.app.ui.component.HaloView a) {
        return a.startRunnable;
    }
    
    static android.os.Handler access$200(com.navdy.hud.app.ui.component.HaloView a) {
        return a.handler;
    }
    
    static int access$300(com.navdy.hud.app.ui.component.HaloView a) {
        return a.animationDelay;
    }
    
    static void access$400(com.navdy.hud.app.ui.component.HaloView a) {
        a.toggleAnimator();
    }
    
    static android.animation.AnimatorSet access$500(com.navdy.hud.app.ui.component.HaloView a) {
        return a.currentAnimatorSet;
    }
    
    private void toggleAnimator() {
        if (this.currentAnimatorSet != this.animatorSet) {
            this.currentAnimatorSet = this.animatorSet;
        } else {
            this.currentAnimatorSet = this.reverseAnimatorSet;
        }
    }
    
    public void onAnimationUpdateInternal(android.animation.ValueAnimator a) {
        this.currentStrokeWidth = ((Float)a.getAnimatedValue()).floatValue() - this.startRadius;
        this.invalidate();
    }
    
    public void onDraw(android.graphics.Canvas a) {
        super.onDraw(a);
        this.onDrawInternal(a);
    }
    
    public void onDrawInternal(android.graphics.Canvas a) {
        this.paint.setStyle(android.graphics.Paint$Style.FILL);
        this.paint.setStrokeWidth(0.0f);
        this.paint.setColor(this.strokeColor);
        a.drawCircle((float)(this.getWidth() / 2), (float)(this.getHeight() / 2), this.startRadius + this.currentStrokeWidth, this.paint);
    }
    
    public void setDuration(int i) {
        this.animationDuration = i;
    }
    
    public void setEndRadius(int i) {
        this.endRadius = (float)i;
    }
    
    public void setMiddleRadius(int i) {
        this.middleRadius = (float)i;
    }
    
    public void setStartDelay(int i) {
        this.animationDelay = i;
    }
    
    public void setStartRadius(int i) {
        this.startRadius = (float)i;
    }
    
    public void setStrokeColor(int i) {
        this.strokeColor = i;
        this.invalidate();
    }
    
    public void setStrokeWidth(int i) {
        this.currentStrokeWidth = (float)i;
    }
    
    public void start() {
        this.stop();
        this.started = true;
        this.firstAnimatorSet.addListener((android.animation.Animator$AnimatorListener)this.animationListener);
        this.animatorSet.addListener((android.animation.Animator$AnimatorListener)this.animationListener);
        this.reverseAnimatorSet.addListener((android.animation.Animator$AnimatorListener)this.animationListener);
        if (this.firstIterationDone) {
            this.currentAnimatorSet = this.reverseAnimatorSet;
            this.reverseAnimatorSet.start();
        } else {
            this.firstIterationDone = true;
            this.currentAnimatorSet = this.animatorSet;
            this.firstAnimatorSet.start();
        }
    }
    
    public void stop() {
        this.started = false;
        this.handler.removeCallbacks(this.startRunnable);
        if (this.firstAnimatorSet.isRunning()) {
            this.firstAnimatorSet.removeAllListeners();
            this.firstAnimatorSet.cancel();
        }
        if (this.animatorSet.isRunning()) {
            this.animatorSet.removeAllListeners();
            this.animatorSet.cancel();
        }
        if (this.reverseAnimatorSet.isRunning()) {
            this.reverseAnimatorSet.removeAllListeners();
            this.reverseAnimatorSet.cancel();
        }
        this.currentStrokeWidth = this.endStrokeWidth;
        this.requestLayout();
    }
}
