package com.navdy.hud.app.ui.component.homescreen;

class SmartDashView$4 implements com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$ChangeListener {
    final com.navdy.hud.app.ui.component.homescreen.SmartDashView this$0;
    
    SmartDashView$4(com.navdy.hud.app.ui.component.homescreen.SmartDashView a) {
        super();
        this.this$0 = a;
    }
    
    public void onGaugeChanged(int i) {
        String s = com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$200(this.this$0).getWidgetIdentifierForIndex(i);
        com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$100().d(new StringBuilder().append("onGaugeChanged, (Right) , Widget ID : ").append(s).append(" , Position : ").append(i).toString());
        com.navdy.hud.app.ui.component.homescreen.SmartDashView.access$500(this.this$0, "PREFERENCE_RIGHT_GAUGE", s);
        this.this$0.mLeftAdapter.setExcludedPosition(i);
        this.this$0.mLeftGaugeViewPager.updateState();
    }
}
