package com.navdy.hud.app.ui.component.mainmenu;

class MainMenuView2$1 implements com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$Callback {
    final com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 this$0;
    
    MainMenuView2$1(com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a) {
        super();
        this.this$0 = a;
    }
    
    public void close() {
        this.this$0.presenter.close();
    }
    
    public boolean isClosed() {
        return this.this$0.presenter.isClosed();
    }
    
    public boolean isItemClickable(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        return this.this$0.presenter.isItemClickable(a);
    }
    
    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, android.view.View a0, int i, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a1) {
        com.navdy.hud.app.ui.component.mainmenu.IMenu a2 = this.this$0.presenter.getCurrentMenu();
        if (a2 != null) {
            a2.onBindToView(a, a0, i, a1);
        }
    }
    
    public void onFastScrollEnd() {
        com.navdy.hud.app.ui.component.mainmenu.IMenu a = this.this$0.presenter.getCurrentMenu();
        if (a != null) {
            a.onFastScrollEnd();
        }
    }
    
    public void onFastScrollStart() {
        com.navdy.hud.app.ui.component.mainmenu.IMenu a = this.this$0.presenter.getCurrentMenu();
        if (a != null) {
            a.onFastScrollStart();
        }
    }
    
    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        com.navdy.hud.app.ui.component.mainmenu.IMenu a0 = this.this$0.presenter.getCurrentMenu();
        if (a0 != null) {
            a0.onItemSelected(a);
        }
    }
    
    public void onLoad() {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.access$000().v("onLoad");
        this.this$0.presenter.resetSelectedItem();
    }
    
    public void onScrollIdle() {
        com.navdy.hud.app.ui.component.mainmenu.IMenu a = this.this$0.presenter.getCurrentMenu();
        if (a != null) {
            a.onScrollIdle();
        }
    }
    
    public void select(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        this.this$0.presenter.selectItem(a);
    }
    
    public void showToolTip() {
        com.navdy.hud.app.ui.component.mainmenu.IMenu a = this.this$0.presenter.getCurrentMenu();
        if (a != null) {
            a.showToolTip();
        }
    }
}
