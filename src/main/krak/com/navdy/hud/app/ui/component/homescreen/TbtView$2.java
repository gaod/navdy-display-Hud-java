package com.navdy.hud.app.ui.component.homescreen;

class TbtView$2 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.homescreen.TbtView this$0;
    
    TbtView$2(com.navdy.hud.app.ui.component.homescreen.TbtView a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        if (com.navdy.hud.app.ui.component.homescreen.TbtView.access$000(this.this$0).isNavigationActive() && !com.navdy.hud.app.ui.component.homescreen.TbtView.access$000(this.this$0).isRecalculating() && com.navdy.hud.app.ui.component.homescreen.TbtView.access$100(this.this$0) != com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.NOW) {
            this.this$0.progressBar.setVisibility(0);
            this.this$0.activeMapDistance.setVisibility(0);
        }
    }
    
    public void onAnimationStart(android.animation.Animator a) {
    }
}
