package com.navdy.hud.app.ui.component.mainmenu;
import com.navdy.hud.app.R;

final public class DashGaugeConfiguratorMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    final public static com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion Companion;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model back;
    final private static int backColor;
    final private static int bkColorUnselected;
    final private static int centerGaugeBackgroundColor;
    final private static java.util.ArrayList centerGaugeOptionsList;
    final private static String centerGaugeTitle;
    final private static String offLabel;
    final private static String onLabel;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static java.util.ArrayList sideGaugesOptionsList;
    final private static String sideGaugesTitle;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model speedoMeter;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model tachoMeter;
    final private static com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    final private static String unavailableLabel;
    private int backSelection;
    private int backSelectionId;
    final private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType gaugeType;
    private com.navdy.hud.app.view.DashboardWidgetView gaugeView;
    final private android.widget.FrameLayout gaugeViewContainer;
    private com.navdy.hud.app.util.HeadingDataUtil headingDataUtil;
    final private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    final private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter presenter;
    private boolean registered;
    final private android.content.SharedPreferences sharedPreferences;
    private com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$SmartDashWidgetCache smartDashWidgetCache;
    final private com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager smartDashWidgetManager;
    private boolean userPreferenceChanged;
    final private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;
    
    static {
        Companion = new com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion((kotlin.jvm.internal.DefaultConstructorMarker)null);
        centerGaugeOptionsList = new java.util.ArrayList();
        sideGaugesOptionsList = new java.util.ArrayList();
        sLogger = new com.navdy.service.library.log.Logger((Companion).getClass());
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        backColor = a.getColor(R.color.mm_back);
        bkColorUnselected = a.getColor(R.color.icon_bk_color_unselected);
        CharSequence a0 = a.getText(R.string.carousel_menu_smartdash_select_center_gauge);
        if (a0 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.String");
        }
        centerGaugeTitle = (String)a0;
        CharSequence a1 = a.getText(R.string.carousel_menu_smartdash_side_gauges);
        if (a1 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.String");
        }
        sideGaugesTitle = (String)a1;
        CharSequence a2 = a.getText(R.string.on);
        if (a2 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.String");
        }
        onLabel = (String)a2;
        CharSequence a3 = a.getText(R.string.off);
        if (a3 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.String");
        }
        offLabel = (String)a3;
        CharSequence a4 = a.getText(R.string.unavailable);
        if (a4 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.String");
        }
        unavailableLabel = (String)a4;
        com.navdy.hud.app.ui.framework.UIStateManager a5 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a5, "remoteDeviceManager.uiStateManager");
        uiStateManager = a5;
        centerGaugeBackgroundColor = a.getColor(R.color.mm_options_scroll_gauge);
        int i = com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getBackColor$p(Companion);
        String s = a.getString(R.string.back);
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a6 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, i, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i, s, (String)null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a6, "IconBkColorViewHolder.bu\u2026tuatorColor, title, null)");
        back = a6;
        String s0 = a.getString(R.string.tachometer);
        int i0 = a.getColor(R.color.mm_options_tachometer);
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a7 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_options_tachometer, R.drawable.icon_options_dash_tachometer_2, -16777216, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i0, s0, (String)null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a7, "IconBkColorViewHolder.bu\u2026tuatorColor, title, null)");
        tachoMeter = a7;
        String s1 = a.getString(R.string.speedometer);
        int i1 = a.getColor(R.color.mm_options_speedometer);
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a8 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_options_speedometer, R.drawable.icon_options_dash_speedometer_2, -16777216, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i1, s1, (String)null);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a8, "IconBkColorViewHolder.bu\u2026tuatorColor, title, null)");
        speedoMeter = a8;
        com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getCenterGaugeOptionsList$p(Companion).add(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getBack$p(Companion));
        com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getCenterGaugeOptionsList$p(Companion).add(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getTachoMeter$p(Companion));
        com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getCenterGaugeOptionsList$p(Companion).add(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getSpeedoMeter$p(Companion));
    }
    
    public DashGaugeConfiguratorMenu(com.squareup.otto.Bus a, android.content.SharedPreferences a0, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a1, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a2, com.navdy.hud.app.ui.component.mainmenu.IMenu a3) {
        super();
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "bus");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "sharedPreferences");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a1, "vscrollComponent");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a2, "presenter");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a3, "parent");
        this.bus = a;
        this.sharedPreferences = a0;
        this.vscrollComponent = a1;
        this.presenter = a2;
        this.parent = a3;
        this.headingDataUtil = new com.navdy.hud.app.util.HeadingDataUtil();
        android.widget.FrameLayout a4 = this.vscrollComponent.selectedCustomView;
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a4, "vscrollComponent.selectedCustomView");
        this.gaugeViewContainer = a4;
        this.gaugeView = new com.navdy.hud.app.view.DashboardWidgetView(this.gaugeViewContainer.getContext());
        this.gaugeView.setLayoutParams(new android.view.ViewGroup$LayoutParams(-1, -1));
        this.gaugeViewContainer.removeAllViews();
        this.gaugeViewContainer.addView((android.view.View)this.gaugeView);
        this.smartDashWidgetManager = new com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager(this.sharedPreferences, com.navdy.hud.app.HudApplication.getAppContext());
        this.smartDashWidgetManager.onResume();
        this.smartDashWidgetManager.setFilter((com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$IWidgetFilter)com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$1.INSTANCE);
        this.smartDashWidgetManager.reLoadAvailableWidgets(true);
        this.smartDashWidgetManager.registerForChanges(new com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$2(this));
        this.gaugeType = com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType.CENTER;
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getBack$cp() {
        return back;
    }
    
    final public static int access$getBackColor$cp() {
        return backColor;
    }
    
    final public static int access$getBkColorUnselected$cp() {
        return bkColorUnselected;
    }
    
    final public static int access$getCenterGaugeBackgroundColor$cp() {
        return centerGaugeBackgroundColor;
    }
    
    final public static java.util.ArrayList access$getCenterGaugeOptionsList$cp() {
        return centerGaugeOptionsList;
    }
    
    final public static String access$getCenterGaugeTitle$cp() {
        return centerGaugeTitle;
    }
    
    final public static String access$getOffLabel$cp() {
        return offLabel;
    }
    
    final public static String access$getOnLabel$cp() {
        return onLabel;
    }
    
    final public static com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter access$getPresenter$p(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu a) {
        return a.presenter;
    }
    
    final public static com.navdy.service.library.log.Logger access$getSLogger$cp() {
        return sLogger;
    }
    
    final public static java.util.ArrayList access$getSideGaugesOptionsList$cp() {
        return sideGaugesOptionsList;
    }
    
    final public static String access$getSideGaugesTitle$cp() {
        return sideGaugesTitle;
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getSpeedoMeter$cp() {
        return speedoMeter;
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getTachoMeter$cp() {
        return tachoMeter;
    }
    
    final public static com.navdy.hud.app.ui.framework.UIStateManager access$getUiStateManager$cp() {
        return uiStateManager;
    }
    
    final public static String access$getUnavailableLabel$cp() {
        return unavailableLabel;
    }
    
    final public void ObdPidChangeEvent(com.navdy.hud.app.obd.ObdManager$ObdPidChangeEvent a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "event");
        switch(a.pid.getId()) {
            case 256: {
                this.smartDashWidgetManager.updateWidget("MPG_AVG_WIDGET", Double.valueOf(a.pid.getValue()));
                break;
            }
            case 47: {
                this.smartDashWidgetManager.updateWidget("FUEL_GAUGE_ID", Double.valueOf(a.pid.getValue()));
                break;
            }
            case 5: {
                this.smartDashWidgetManager.updateWidget("ENGINE_TEMPERATURE_GAUGE_ID", Double.valueOf(a.pid.getValue()));
                break;
            }
        }
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu a, String s, String s0) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "parent");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s, "args");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s0, "path");
        return null;
    }
    
    final public com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType getGaugeType() {
        return this.gaugeType;
    }
    
    final public com.navdy.hud.app.view.DashboardWidgetView getGaugeView() {
        return this.gaugeView;
    }
    
    final public android.widget.FrameLayout getGaugeViewContainer() {
        return this.gaugeViewContainer;
    }
    
    final public com.navdy.hud.app.util.HeadingDataUtil getHeadingDataUtil() {
        return this.headingDataUtil;
    }
    
    public int getInitialSelection() {
        return 1;
    }
    
    public java.util.List getItems() {
        java.util.ArrayList a = null;
        com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType a0 = this.gaugeType;
        switch(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$WhenMappings.$EnumSwitchMapping$1[a0.ordinal()]) {
            case 2: {
                this.smartDashWidgetCache = this.smartDashWidgetManager.buildSmartDashWidgetCache(0);
                com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getSideGaugesOptionsList$p(Companion).clear();
                com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getSideGaugesOptionsList$p(Companion).add(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getBack$p(Companion));
                if (this.smartDashWidgetCache != null) {
                    com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$SmartDashWidgetCache a1 = this.smartDashWidgetCache;
                    if (a1 == null) {
                        kotlin.jvm.internal.Intrinsics.throwNpe();
                    }
                    int i = com.navdy.hud.app.HudApplication.getAppContext().getResources().getColor(R.color.mm_options_side_gauges_halo);
                    int i0 = a1.getWidgetsCount() - 1;
                    if (i0 <= 0) {
                        int i1 = 0;
                        while(true) {
                            String s = null;
                            boolean b = false;
                            com.navdy.hud.app.view.DashboardWidgetPresenter a2 = a1.getWidgetPresenter(i1);
                            label8: {
                                label9: {
                                    if (a2 == null) {
                                        break label9;
                                    }
                                    s = a2.getWidgetName();
                                    if (s != null) {
                                        break label8;
                                    }
                                }
                                s = "";
                            }
                            com.navdy.hud.app.view.DashboardWidgetPresenter a3 = a1.getWidgetPresenter(i1);
                            String s0 = (a3 == null) ? null : a3.getWidgetIdentifier();
                            boolean b0 = this.smartDashWidgetManager.isGaugeOn(s0);
                            com.navdy.obd.PidSet a4 = com.navdy.hud.app.obd.ObdManager.getInstance().getSupportedPids();
                            label2: {
                                label7: {
                                    if (s0 == null) {
                                        break label7;
                                    }
                                    switch(s0.hashCode()) {
                                        case 1168400042: {
                                            if (!s0.equals("FUEL_GAUGE_ID")) {
                                                break;
                                            }
                                            {
                                                label5: {
                                                    label6: {
                                                        if (a4 == null) {
                                                            break label6;
                                                        }
                                                        if (a4.contains(47)) {
                                                            break label5;
                                                        }
                                                    }
                                                    b = false;
                                                    break label2;
                                                }
                                                b = true;
                                            }
                                            break label2;
                                        }
                                        case -131933527: {
                                            if (!s0.equals("ENGINE_TEMPERATURE_GAUGE_ID")) {
                                                break;
                                            }
                                            {
                                                label3: {
                                                    label4: {
                                                        if (a4 == null) {
                                                            break label4;
                                                        }
                                                        if (a4.contains(5)) {
                                                            break label3;
                                                        }
                                                    }
                                                    b = false;
                                                    break label2;
                                                }
                                                b = true;
                                            }
                                            break label2;
                                        }
                                        case -1158963060: {
                                            if (!s0.equals("MPG_AVG_WIDGET")) {
                                                break;
                                            }
                                            {
                                                label0: {
                                                    label1: {
                                                        if (a4 == null) {
                                                            break label1;
                                                        }
                                                        if (a4.contains(256)) {
                                                            break label0;
                                                        }
                                                    }
                                                    b = false;
                                                    break label2;
                                                }
                                                b = true;
                                            }
                                            break label2;
                                        }
                                    }
                                }
                                b = true;
                            }
                            String s1 = b ? b0 ? com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getOnLabel$p(Companion) : com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getOffLabel$p(Companion) : com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getUnavailableLabel$p(Companion);
                            com.navdy.hud.app.ui.component.vlist.VerticalList$Model a5 = com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder.Companion.buildModel(i1, i, s, s1, b0, b);
                            com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getSideGaugesOptionsList$p(Companion).add(a5);
                            if (i1 == i0) {
                                break;
                            }
                            i1 = i1 + 1;
                        }
                    }
                }
                a = com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getSideGaugesOptionsList$p(Companion);
                break;
            }
            case 1: {
                a = com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getCenterGaugeOptionsList$p(Companion);
                break;
            }
            default: {
                throw new kotlin.NoWhenBranchMatchedException();
            }
        }
        return (java.util.List)a;
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$Model getModelfromPos(int i) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = null;
        com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType a0 = this.gaugeType;
        switch(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$WhenMappings.$EnumSwitchMapping$4[a0.ordinal()]) {
            case 2: {
                a = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getSideGaugesOptionsList$p(Companion).get(i);
                break;
            }
            case 1: {
                a = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getCenterGaugeOptionsList$p(Companion).get(i);
                break;
            }
            default: {
                throw new kotlin.NoWhenBranchMatchedException();
            }
        }
        return a;
    }
    
    final public boolean getRegistered() {
        return this.registered;
    }
    
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    final public com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$SmartDashWidgetCache getSmartDashWidgetCache() {
        return this.smartDashWidgetCache;
    }
    
    final public com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager getSmartDashWidgetManager() {
        return this.smartDashWidgetManager;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MAIN_OPTIONS;
    }
    
    final public boolean getUserPreferenceChanged() {
        return this.userPreferenceChanged;
    }
    
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    public boolean isFirstItemEmpty() {
        return false;
    }
    
    public boolean isItemClickable(int i, int i0) {
        return true;
    }
    
    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, android.view.View a0, int i, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a1) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "model");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "view");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a1, "state");
    }
    
    final public void onDriveScoreUpdated(com.navdy.hud.app.analytics.TelemetryDataManager$DriveScoreUpdated a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "driveScoreUpdated");
        this.smartDashWidgetManager.updateWidget("DRIVE_SCORE_GAUGE_ID", a);
    }
    
    final public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "profileChanged");
        com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getSLogger$p(Companion).d("onDriverProfileChanged, reloading the widgets");
        com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager a0 = this.smartDashWidgetManager;
        if (a0 != null) {
            a0.reLoadAvailableWidgets(false);
        }
    }
    
    public void onFastScrollEnd() {
    }
    
    public void onFastScrollStart() {
    }
    
    final public void onGpsLocationChanged(android.location.Location a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "location");
        double d = (double)a.getBearing();
        this.headingDataUtil.setHeading(d);
        this.smartDashWidgetManager.updateWidget("COMPASS_WIDGET", Double.valueOf(this.headingDataUtil.getHeading()));
    }
    
    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "selection");
        if (kotlin.jvm.internal.Intrinsics.areEqual(this.gaugeType, com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType.SIDE)) {
            this.showGauge(a.pos);
        }
    }
    
    final public void onLocationFixChangeEvent(com.navdy.hud.app.maps.MapEvents$LocationFix a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "event");
        if (!a.locationAvailable) {
            this.headingDataUtil.reset();
            this.smartDashWidgetManager.updateWidget("COMPASS_WIDGET", Integer.valueOf(0));
        }
    }
    
    final public void onMapEvent(com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "event");
        com.navdy.hud.app.manager.SpeedManager$SpeedUnit a0 = com.navdy.hud.app.manager.SpeedManager.getInstance().getSpeedUnit();
        float f = (float)com.navdy.hud.app.manager.SpeedManager.convert((double)a.currentSpeedLimit, com.navdy.hud.app.manager.SpeedManager$SpeedUnit.METERS_PER_SECOND, a0);
        this.smartDashWidgetManager.updateWidget("SPEED_LIMIT_SIGN_GAUGE_ID", Integer.valueOf(Math.round(f)));
    }
    
    public void onScrollIdle() {
    }
    
    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "level");
        com.navdy.hud.app.view.DashboardWidgetView a0 = this.gaugeView;
        Object a1 = (a0 == null) ? null : a0.getTag();
        if (!(a1 instanceof com.navdy.hud.app.view.DashboardWidgetPresenter)) {
            a1 = null;
        }
        com.navdy.hud.app.view.DashboardWidgetPresenter a2 = (com.navdy.hud.app.view.DashboardWidgetPresenter)a1;
        if (a2 != null) {
            a2.setView((com.navdy.hud.app.view.DashboardWidgetView)null, (android.os.Bundle)null);
        }
        if (this.userPreferenceChanged) {
            com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getSLogger$p(Companion).d(new StringBuilder().append("User preference has changed ").append(this.userPreferenceChanged).toString());
            this.bus.post(new com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$UserPreferenceChanged());
        } else {
            com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getSLogger$p(Companion).d(new StringBuilder().append("User preference has not been changed ").append(this.userPreferenceChanged).toString());
        }
        if (this.registered) {
            this.bus.unregister(this);
            this.registered = false;
        }
    }
    
    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "selection");
        com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getSLogger$p(Companion).v(new StringBuilder().append("select id:").append(a.id).append(" pos:").append(a.pos).toString());
        com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType a0 = this.gaugeType;
        label0: switch(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$WhenMappings.$EnumSwitchMapping$3[a0.ordinal()]) {
            case 2: {
                if (a.id == R.id.menu_back) {
                    this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                    break;
                } else {
                    com.navdy.hud.app.ui.component.vlist.VerticalList$Model a1 = a.model;
                    int i = a.id;
                    com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$SmartDashWidgetCache a2 = this.smartDashWidgetCache;
                    com.navdy.hud.app.view.DashboardWidgetPresenter a3 = (a2 == null) ? null : a2.getWidgetPresenter(i);
                    if (a1.isOn) {
                        String s = null;
                        a1.isOn = false;
                        a1.subTitle = com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getOffLabel$p(Companion);
                        com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager a4 = this.smartDashWidgetManager;
                        label3: {
                            label4: {
                                if (a3 == null) {
                                    break label4;
                                }
                                s = a3.getWidgetIdentifier();
                                if (s != null) {
                                    break label3;
                                }
                            }
                            s = "";
                        }
                        a4.setGaugeOn(s, false);
                    } else {
                        String s0 = null;
                        a1.isOn = true;
                        a1.subTitle = com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getOnLabel$p(Companion);
                        com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager a5 = this.smartDashWidgetManager;
                        label1: {
                            label2: {
                                if (a3 == null) {
                                    break label2;
                                }
                                s0 = a3.getWidgetIdentifier();
                                if (s0 != null) {
                                    break label1;
                                }
                            }
                            s0 = "";
                        }
                        a5.setGaugeOn(s0, true);
                    }
                    this.userPreferenceChanged = true;
                    this.presenter.refreshDataforPos(a.pos);
                    break;
                }
            }
            case 1: {
                switch(a.id) {
                    case R.id.menu_back: {
                        this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                        break label0;
                    }
                    case R.id.main_menu_options_tachometer: {
                        this.presenter.close();
                        com.navdy.hud.app.ui.component.homescreen.SmartDashView a6 = com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getUiStateManager$p(Companion).getSmartDashView();
                        if (a6 == null) {
                            break label0;
                        }
                        a6.onTachoMeterSelected();
                        break label0;
                    }
                    case R.id.main_menu_options_speedometer: {
                        this.presenter.close();
                        com.navdy.hud.app.ui.component.homescreen.SmartDashView a7 = com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getUiStateManager$p(Companion).getSmartDashView();
                        if (a7 == null) {
                            break label0;
                        }
                        a7.onSpeedoMeterSelected();
                        break label0;
                    }
                    default: {
                        break label0;
                    }
                }
            }
        }
        return false;
    }
    
    public void setBackSelectionId(int i) {
        this.backSelectionId = i;
    }
    
    public void setBackSelectionPos(int i) {
        this.backSelection = i;
    }
    
    final public void setGaugeType(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "value");
        this.gaugeType = a;
        if (kotlin.jvm.internal.Intrinsics.areEqual(a, com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType.SIDE)) {
            if (!this.registered) {
                this.bus.register(this);
                this.registered = true;
            }
        } else if (this.registered) {
            this.bus.unregister(this);
            this.registered = false;
        }
    }
    
    final public void setGaugeView(com.navdy.hud.app.view.DashboardWidgetView a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.gaugeView = a;
    }
    
    final public void setHeadingDataUtil(com.navdy.hud.app.util.HeadingDataUtil a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.headingDataUtil = a;
    }
    
    final public void setRegistered(boolean b) {
        this.registered = b;
    }
    
    public void setSelectedIcon() {
        com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType a = this.gaugeType;
        switch(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$WhenMappings.$EnumSwitchMapping$2[a.ordinal()]) {
            case 2: {
                this.vscrollComponent.showSelectedCustomView();
                this.vscrollComponent.selectedText.setText((CharSequence)null);
                if (this.smartDashWidgetCache == null) {
                    this.getItems();
                }
                this.showGauge(1);
                break;
            }
            case 1: {
                this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_center_gauge, com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getCenterGaugeBackgroundColor$p(Companion), (android.graphics.Shader)null, 1f);
                this.vscrollComponent.selectedText.setText((CharSequence)com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getCenterGaugeTitle$p(Companion));
                break;
            }
        }
    }
    
    final public void setSmartDashWidgetCache(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$SmartDashWidgetCache a) {
        this.smartDashWidgetCache = a;
    }
    
    final public void setUserPreferenceChanged(boolean b) {
        this.userPreferenceChanged = b;
    }
    
    final public void showGauge(int i) {
        if (i <= 0) {
            this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_side_gauges, com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getCenterGaugeBackgroundColor$p(Companion), (android.graphics.Shader)null, 1f);
            this.vscrollComponent.selectedText.setText((CharSequence)com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion.access$getSideGaugesTitle$p(Companion));
        } else {
            this.vscrollComponent.showSelectedCustomView();
            this.vscrollComponent.selectedText.setText((CharSequence)null);
            Object a = this.gaugeView.getTag();
            if (!(a instanceof com.navdy.hud.app.view.DashboardWidgetPresenter)) {
                a = null;
            }
            com.navdy.hud.app.view.DashboardWidgetPresenter a0 = (com.navdy.hud.app.view.DashboardWidgetPresenter)a;
            com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$SmartDashWidgetCache a1 = this.smartDashWidgetCache;
            com.navdy.hud.app.view.DashboardWidgetPresenter a2 = (a1 == null) ? null : a1.getWidgetPresenter(i - 1);
            if (a0 != null && a0 != this.presenter && a0.getWidgetView() == this.gaugeView) {
                a0.setView((com.navdy.hud.app.view.DashboardWidgetView)null, (android.os.Bundle)null);
            }
            if (a2 != null) {
                android.os.Bundle a3 = new android.os.Bundle();
                a3.putInt("EXTRA_GRAVITY", 0);
                a3.putBoolean("EXTRA_IS_ACTIVE", true);
                a2.setView(this.gaugeView, a3);
                a2.setWidgetVisibleToUser(true);
            }
            this.gaugeView.setTag(a2);
        }
    }
    
    public void showToolTip() {
    }
}
