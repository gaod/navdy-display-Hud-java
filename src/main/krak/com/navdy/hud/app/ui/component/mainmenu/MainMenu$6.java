package com.navdy.hud.app.ui.component.mainmenu;

class MainMenu$6 {
    final static int[] $SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode;
    final static int[] $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$Menu;
    
    static {
        $SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode = new int[com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode;
        com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode a0 = com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.SMART_DASH;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode[com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$Menu = new int[com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$Menu;
        com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu a2 = com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.CONTACTS;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$Menu[com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.PLACES.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$Menu[com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.SETTINGS.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$Menu[com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MUSIC.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException4) {
        }
    }
}
