package com.navdy.hud.app.ui.component.mainmenu;

final public class ReportIssueMenu$WhenMappings {
    final public static int[] $EnumSwitchMapping$0;
    final public static int[] $EnumSwitchMapping$1;
    final public static int[] $EnumSwitchMapping$2;
    
    static {
        $EnumSwitchMapping$0 = new int[com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType.values().length];
        $EnumSwitchMapping$0[com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType.NAVIGATION_ISSUES.ordinal()] = 1;
        $EnumSwitchMapping$0[com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType.SNAP_SHOT.ordinal()] = 2;
        $EnumSwitchMapping$1 = new int[com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType.values().length];
        $EnumSwitchMapping$1[com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType.NAVIGATION_ISSUES.ordinal()] = 1;
        $EnumSwitchMapping$1[com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType.SNAP_SHOT.ordinal()] = 2;
        $EnumSwitchMapping$2 = new int[com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType.values().length];
        $EnumSwitchMapping$2[com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType.NAVIGATION_ISSUES.ordinal()] = 1;
        $EnumSwitchMapping$2[com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType.SNAP_SHOT.ordinal()] = 2;
    }
}
