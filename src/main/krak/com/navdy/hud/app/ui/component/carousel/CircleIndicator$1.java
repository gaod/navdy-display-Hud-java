package com.navdy.hud.app.ui.component.carousel;

class CircleIndicator$1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.carousel.CircleIndicator this$0;
    final int val$toPos;
    
    CircleIndicator$1(com.navdy.hud.app.ui.component.carousel.CircleIndicator a, int i) {
        super();
        this.this$0 = a;
        this.val$toPos = i;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        com.navdy.hud.app.ui.component.carousel.CircleIndicator.access$002(this.this$0, this.val$toPos);
    }
}
