package com.navdy.hud.app.ui.component.homescreen;

public class HomeScreenRightSectionView extends android.widget.FrameLayout {
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.homescreen.HomeScreenRightSectionView.class);
    }
    
    public HomeScreenRightSectionView(android.content.Context a) {
        super(a);
    }
    
    public HomeScreenRightSectionView(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
    }
    
    public HomeScreenRightSectionView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
    }
    
    public void ejectRightSection() {
        sLogger.v("ejectRightSection");
        this.removeAllViews();
    }
    
    public void getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode a, android.animation.AnimatorSet$Builder a0) {
        switch(com.navdy.hud.app.ui.component.homescreen.HomeScreenRightSectionView$1.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: {
                a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mainScreenRightSectionX));
                break;
            }
            case 1: {
                a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mainScreenRightSectionShrinkX));
                break;
            }
        }
    }
    
    public boolean hasView() {
        return this.getChildCount() > 0;
    }
    
    public void injectRightSection(android.view.View a) {
        sLogger.v("injectRightSection");
        this.removeAllViews();
        this.addView(a);
    }
    
    public boolean isViewVisible() {
        return this.getX() < (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mainScreenRightSectionX;
    }
    
    public void setView(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        sLogger.v(new StringBuilder().append("setview: ").append(a).toString());
        this.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mainScreenRightSectionX);
    }
    
    public void updateLayoutForMode(com.navdy.hud.app.maps.NavigationMode a, com.navdy.hud.app.ui.component.homescreen.HomeScreenView a0) {
        int i = (a0.isNavigationActive()) ? com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeMapHeight : com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapHeight;
        ((android.view.ViewGroup$MarginLayoutParams)this.getLayoutParams()).height = i;
        this.requestLayout();
    }
}
