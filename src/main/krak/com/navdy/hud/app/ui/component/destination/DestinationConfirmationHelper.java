package com.navdy.hud.app.ui.component.destination;
import com.navdy.hud.app.R;

public class DestinationConfirmationHelper {
    private static float[] FONT_SIZES;
    private static int[] MAX_LINES;
    
    static {
        int[] a = new int[4];
        a[0] = 1;
        a[1] = 1;
        a[2] = 2;
        a[3] = 2;
        MAX_LINES = a;
        float[] a0 = new float[4];
        a0[0] = 30f;
        a0[1] = 26f;
        a0[2] = 26f;
        a0[3] = 22f;
        FONT_SIZES = a0;
    }
    
    public DestinationConfirmationHelper() {
    }
    
    private static void configure(com.navdy.hud.app.ui.component.ConfirmationLayout a, com.navdy.hud.app.framework.destinations.Destination a0, String s, CharSequence a1, boolean b, boolean b0, int i, int i0) {
        com.navdy.hud.app.ui.component.destination.DestinationConfirmationHelper.setImage(a, a0, b0, i, i0);
        a.screenTitle.setText(R.string.destination_change_navigation_title);
        a.title1.setVisibility(8);
        android.widget.TextView a2 = a.title2;
        android.widget.TextView a3 = a.title3;
        com.navdy.hud.app.view.MaxWidthLinearLayout a4 = (com.navdy.hud.app.view.MaxWidthLinearLayout)a.findViewById(R.id.infoContainer);
        int i1 = a4.getPaddingStart();
        int i2 = a4.getPaddingEnd();
        int i3 = a4.getMaxWidth();
        a2.setText((CharSequence)s);
        int[] a5 = new int[2];
        com.navdy.hud.app.util.ViewUtil.autosize(a2, MAX_LINES, i3 - (i1 + i2), FONT_SIZES, a5);
        a2.setMaxLines((a5[1] != 0) ? 1 : 0);
        Object a6 = (android.text.TextUtils.isEmpty(a1)) ? a1 : b ? android.text.Html.fromHtml((String)a1) : a1;
        com.navdy.hud.app.util.ViewUtil.applyTextAndStyle(a3, (CharSequence)a6, 2, R.style.destination_subtitle_two_line);
        a2.setVisibility(0);
        a3.setVisibility(0);
    }
    
    public static void configure(com.navdy.hud.app.ui.component.ConfirmationLayout a, com.navdy.hud.app.ui.component.destination.DestinationParcelable a0) {
        com.navdy.hud.app.ui.component.destination.DestinationConfirmationHelper.configure(a, (com.navdy.hud.app.framework.destinations.Destination)null, a0.destinationTitle, (CharSequence)a0.destinationSubTitle, a0.destinationSubTitleFormatted, a0.iconUnselected == 0, a0.icon, a0.iconSelectedColor);
    }
    
    public static void configure(com.navdy.hud.app.ui.component.ConfirmationLayout a, com.navdy.hud.app.ui.component.vlist.VerticalList$Model a0, com.navdy.hud.app.framework.destinations.Destination a1) {
        com.navdy.hud.app.ui.component.destination.DestinationConfirmationHelper.configure(a, a1, a1.destinationTitle, (CharSequence)a1.destinationSubtitle, false, a0.type == com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.ICON_BKCOLOR, a0.icon, a0.iconSelectedColor);
    }
    
    private static void setImage(com.navdy.hud.app.ui.component.ConfirmationLayout a, com.navdy.hud.app.framework.destinations.Destination a0, boolean b, int i, int i0) {
        if (b) {
            a.screenImageIconBkColor.setIcon(i, i0, (android.graphics.Shader)null, 1.25f);
            a.screenImage.setVisibility(8);
            a.screenImageIconBkColor.setVisibility(0);
        } else {
            com.navdy.hud.app.ui.component.destination.DestinationConfirmationHelper.setInitials(a0, a.screenImage);
            a.screenImage.setImageResource(i);
            a.screenImage.setVisibility(0);
            a.screenImageIconBkColor.setVisibility(8);
        }
    }
    
    private static void setInitials(com.navdy.hud.app.framework.destinations.Destination a, com.navdy.hud.app.ui.component.image.InitialsImageView a0) {
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)a.initials)) {
                        break label0;
                    }
                }
                a0.setInitials((String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
                break label2;
            }
            com.navdy.hud.app.ui.component.image.InitialsImageView$Style a1 = (a.isInitialNumber) ? (a.initials.length() > 3) ? com.navdy.hud.app.ui.component.image.InitialsImageView$Style.MEDIUM : com.navdy.hud.app.ui.component.image.InitialsImageView$Style.LARGE : (a.initials.length() > 2) ? com.navdy.hud.app.ui.component.image.InitialsImageView$Style.MEDIUM : com.navdy.hud.app.ui.component.image.InitialsImageView$Style.LARGE;
            a0.setInitials(a.initials, a1);
        }
    }
}
