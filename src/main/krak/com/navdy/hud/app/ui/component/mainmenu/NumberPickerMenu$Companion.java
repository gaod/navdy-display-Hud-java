package com.navdy.hud.app.ui.component.mainmenu;

final public class NumberPickerMenu$Companion {
    private NumberPickerMenu$Companion() {
    }
    
    public NumberPickerMenu$Companion(kotlin.jvm.internal.DefaultConstructorMarker a) {
        this();
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getBack$p(com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Companion a) {
        return a.getBack();
    }
    
    final public static int access$getBaseMessageId$p(com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Companion a) {
        return a.getBaseMessageId();
    }
    
    final public static android.content.Context access$getContext$p(com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Companion a) {
        return a.getContext();
    }
    
    final public static int access$getFluctuatorColor$p(com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Companion a) {
        return a.getFluctuatorColor();
    }
    
    final public static com.navdy.service.library.log.Logger access$getLogger$p(com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Companion a) {
        return a.getLogger();
    }
    
    final public static android.content.res.Resources access$getResources$p(com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Companion a) {
        return a.getResources();
    }
    
    final public static int access$getSELECTION_ANIMATION_DELAY$p(com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Companion a) {
        return a.getSELECTION_ANIMATION_DELAY();
    }
    
    final private com.navdy.hud.app.ui.component.vlist.VerticalList$Model getBack() {
        return com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu.access$getBack$cp();
    }
    
    final private int getBaseMessageId() {
        return com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu.access$getBaseMessageId$cp();
    }
    
    final private android.content.Context getContext() {
        return com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu.access$getContext$cp();
    }
    
    final private int getFluctuatorColor() {
        return com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu.access$getFluctuatorColor$cp();
    }
    
    final private com.navdy.service.library.log.Logger getLogger() {
        return com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu.access$getLogger$cp();
    }
    
    final private android.content.res.Resources getResources() {
        return com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu.access$getResources$cp();
    }
    
    final private int getSELECTION_ANIMATION_DELAY() {
        return com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu.access$getSELECTION_ANIMATION_DELAY$cp();
    }
}
