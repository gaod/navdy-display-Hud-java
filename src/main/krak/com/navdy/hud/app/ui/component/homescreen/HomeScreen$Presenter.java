package com.navdy.hud.app.ui.component.homescreen;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class HomeScreen$Presenter extends com.navdy.hud.app.ui.framework.BasePresenter {
    final private static com.navdy.service.library.log.Logger logger;
    @Inject
    com.squareup.otto.Bus bus;
    
    static {
        logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter.class);
    }
    
    public HomeScreen$Presenter() {
    }
    
    com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode getDisplayMode() {
        com.navdy.hud.app.ui.component.homescreen.HomeScreenView a = this.getHomeScreenView();
        com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode a0 = (a == null) ? null : a.getDisplayMode();
        return a0;
    }
    
    com.navdy.hud.app.ui.component.homescreen.HomeScreenView getHomeScreenView() {
        return (com.navdy.hud.app.ui.component.homescreen.HomeScreenView)this.getView();
    }
    
    public void onLoad(android.os.Bundle a) {
        com.navdy.hud.app.ui.component.homescreen.HomeScreen.access$002(this);
    }
    
    protected void onUnload() {
        com.navdy.hud.app.ui.component.homescreen.HomeScreen.access$002((com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter)null);
    }
    
    void setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode a) {
        com.navdy.hud.app.ui.component.homescreen.HomeScreenView a0 = this.getHomeScreenView();
        if (a0 != null) {
            a0.setDisplayMode(a);
        }
    }
}
