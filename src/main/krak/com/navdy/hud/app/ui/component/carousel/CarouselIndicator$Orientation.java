package com.navdy.hud.app.ui.component.carousel;


public enum CarouselIndicator$Orientation {
    HORIZONTAL(0),
    VERTICAL(1);

    private int value;
    CarouselIndicator$Orientation(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class CarouselIndicator$Orientation extends Enum {
//    final private static com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation HORIZONTAL;
//    final public static com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation VERTICAL;
//    
//    static {
//        HORIZONTAL = new com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation("HORIZONTAL", 0);
//        VERTICAL = new com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation("VERTICAL", 1);
//        com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation[] a = new com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation[2];
//        a[0] = HORIZONTAL;
//        a[1] = VERTICAL;
//        $VALUES = a;
//    }
//    
//    private CarouselIndicator$Orientation(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation)Enum.valueOf(com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation[] values() {
//        return $VALUES.clone();
//    }
//}
//