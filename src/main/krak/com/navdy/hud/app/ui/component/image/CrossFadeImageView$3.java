package com.navdy.hud.app.ui.component.image;

class CrossFadeImageView$3 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.image.CrossFadeImageView this$0;
    
    CrossFadeImageView$3(com.navdy.hud.app.ui.component.image.CrossFadeImageView a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        if (this.this$0.mode != com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.BIG) {
            this.this$0.mode = com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.BIG;
        } else {
            this.this$0.mode = com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.SMALL;
        }
    }
}
