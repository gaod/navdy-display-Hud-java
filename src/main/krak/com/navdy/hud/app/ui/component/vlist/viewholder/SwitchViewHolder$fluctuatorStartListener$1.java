package com.navdy.hud.app.ui.component.vlist.viewholder;

final public class SwitchViewHolder$fluctuatorStartListener$1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final android.os.Handler $handler;
    final com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder this$0;
    
    SwitchViewHolder$fluctuatorStartListener$1(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder a, android.os.Handler a0) {
        super();
        this.this$0 = a;
        this.$handler = a0;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "animation");
        this.this$0.itemAnimatorSet = null;
        this.$handler.removeCallbacks(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder.access$getFluctuatorRunnable$p(this.this$0));
        this.$handler.postDelayed(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder.access$getFluctuatorRunnable$p(this.this$0), (long)this.this$0.getHALO_DELAY_START_DURATION());
    }
}
