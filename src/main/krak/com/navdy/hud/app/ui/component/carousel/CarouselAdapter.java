package com.navdy.hud.app.ui.component.carousel;
import com.navdy.hud.app.R;

public class CarouselAdapter {
    final private static com.navdy.service.library.log.Logger sLogger;
    com.navdy.hud.app.ui.component.carousel.CarouselLayout carousel;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.carousel.CarouselAdapter.class);
    }
    
    public CarouselAdapter(com.navdy.hud.app.ui.component.carousel.CarouselLayout a) {
        this.carousel = a;
    }
    
    private void processImageView(com.navdy.hud.app.ui.component.carousel.Carousel$ViewType a, com.navdy.hud.app.ui.component.carousel.Carousel$Model a0, android.view.View a1, int i, int i0) {
        if (a1 instanceof com.navdy.hud.app.ui.component.image.CrossFadeImageView) {
            com.navdy.hud.app.ui.component.image.CrossFadeImageView a2 = (com.navdy.hud.app.ui.component.image.CrossFadeImageView)a1;
            android.view.View a3 = a2;
            com.navdy.hud.app.ui.component.image.InitialsImageView a4 = (com.navdy.hud.app.ui.component.image.InitialsImageView)a2.getBig();
            android.view.View a5 = a2.getSmall();
            a4.setImage(a0.largeImageRes, (String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
            if (this.carousel.imageLytResourceId != R.layout.crossfade_image_lyt) {
                ((com.navdy.hud.app.ui.component.image.ColorImageView)a5).setColor(a0.smallImageColor);
                a1 = a3;
            } else {
                ((com.navdy.hud.app.ui.component.image.InitialsImageView)a5).setImage(a0.smallImageRes, (String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
                a1 = a3;
            }
        } else if (a1 instanceof com.navdy.hud.app.ui.component.image.InitialsImageView) {
            com.navdy.hud.app.ui.component.image.InitialsImageView a6 = (com.navdy.hud.app.ui.component.image.InitialsImageView)a1;
            android.view.View a7 = a6;
            if (a != com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.MIDDLE_LEFT) {
                a6.setImage(a0.smallImageRes, (String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
                a1 = a7;
            } else {
                a6.setImage(a0.largeImageRes, (String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
                a1 = a7;
            }
        }
        com.navdy.hud.app.ui.component.carousel.Carousel$ViewProcessor a8 = this.carousel.viewProcessor;
        label0: {
            Throwable a9 = null;
            if (a8 == null) {
                break label0;
            }
            try {
                if (a != com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.MIDDLE_LEFT) {
                    this.carousel.viewProcessor.processSmallImageView(a0, a1, i, i0, i0);
                    break label0;
                } else {
                    this.carousel.viewProcessor.processLargeImageView(a0, a1, i, i0, i0);
                    break label0;
                }
            } catch(Throwable a10) {
                a9 = a10;
            }
            sLogger.e(a9);
        }
    }
    
    private void processInfoView(com.navdy.hud.app.ui.component.carousel.Carousel$Model a, android.view.View a0, int i) {
        if (a.infoMap != null) {
            android.view.ViewGroup a1 = (android.view.ViewGroup)a0;
            android.view.View a2 = a1;
            int i0 = a1.getChildCount();
            int i1 = 0;
            while(i1 < i0) {
                android.view.View a3 = a1.getChildAt(i1);
                if (a3 instanceof android.widget.TextView) {
                    String s = (String)a.infoMap.get(Integer.valueOf(a3.getId()));
                    if (s == null) {
                        a3.setVisibility(8);
                    } else {
                        android.widget.TextView a4 = (android.widget.TextView)a3;
                        android.view.View a5 = a4;
                        a4.setText((CharSequence)s);
                        a5.setVisibility(0);
                    }
                }
                i1 = i1 + 1;
            }
            a0 = a2;
        }
        com.navdy.hud.app.ui.component.carousel.Carousel$ViewProcessor a6 = this.carousel.viewProcessor;
        label0: {
            Throwable a7 = null;
            if (a6 == null) {
                break label0;
            }
            try {
                this.carousel.viewProcessor.processInfoView(a, a0, i);
                break label0;
            } catch(Throwable a8) {
                a7 = a8;
            }
            sLogger.e(a7);
        }
    }
    
    private void processView(com.navdy.hud.app.ui.component.carousel.Carousel$ViewType a, com.navdy.hud.app.ui.component.carousel.Carousel$Model a0, android.view.View a1, int i, int i0) {
        if (a != com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.MIDDLE_RIGHT) {
            this.processImageView(a, a0, a1, i, i0);
        } else {
            this.processInfoView(a0, a1, i);
        }
    }
    
    public android.view.View getView(int i, android.view.View a, com.navdy.hud.app.ui.component.carousel.Carousel$ViewType a0, int i0, int i1) {
        if (a != null) {
            a.setTag(R.id.item_id, null);
        }
        com.navdy.hud.app.ui.component.carousel.Carousel$Model a1 = null;
        if (i >= 0) {
            int i2 = this.carousel.model.size();
            a1 = null;
            if (i < i2) {
                a1 = (com.navdy.hud.app.ui.component.carousel.Carousel$Model)this.carousel.model.get(i);
            }
        }
        if (a != null) {
            if (a instanceof com.navdy.hud.app.ui.component.image.CrossFadeImageView) {
                if (a0 != com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.SIDE) {
                    if (a0 == com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.MIDDLE_LEFT) {
                        com.navdy.hud.app.ui.component.image.CrossFadeImageView a2 = (com.navdy.hud.app.ui.component.image.CrossFadeImageView)a;
                        android.view.View a3 = a2;
                        a2.setMode(com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.BIG);
                        a = a3;
                    }
                } else {
                    com.navdy.hud.app.ui.component.image.CrossFadeImageView a4 = (com.navdy.hud.app.ui.component.image.CrossFadeImageView)a;
                    android.view.View a5 = a4;
                    a4.setMode(com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.SMALL);
                    a = a5;
                }
            }
        } else {
            sLogger.v(new StringBuilder().append("create ").append(a0).append(" view").toString());
            android.view.View a6 = this.carousel.inflater.inflate(i0, (android.view.ViewGroup)null);
            if (a6 instanceof com.navdy.hud.app.ui.component.image.CrossFadeImageView) {
                if (a0 != com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.SIDE) {
                    if (a0 != com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.MIDDLE_LEFT) {
                        a = a6;
                    } else {
                        com.navdy.hud.app.ui.component.image.CrossFadeImageView a7 = (com.navdy.hud.app.ui.component.image.CrossFadeImageView)a6;
                        android.view.View a8 = a7;
                        a7.inject(com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.BIG);
                        a = a8;
                    }
                } else {
                    com.navdy.hud.app.ui.component.image.CrossFadeImageView a9 = (com.navdy.hud.app.ui.component.image.CrossFadeImageView)a6;
                    android.view.View a10 = a9;
                    a9.inject(com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.SMALL);
                    a = a10;
                }
            } else {
                a = a6;
            }
        }
        if (a1 == null) {
            a.setVisibility(4);
            a.setTag(R.id.item_id, null);
        } else {
            a.setTag(R.id.item_id, Integer.valueOf(a1.id));
            a.setVisibility(0);
            this.processView(a0, a1, a, i, i1);
        }
        return a;
    }
}
