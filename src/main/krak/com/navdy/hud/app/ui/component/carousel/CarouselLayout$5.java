package com.navdy.hud.app.ui.component.carousel;

class CarouselLayout$5 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.carousel.CarouselLayout this$0;
    final java.util.List val$newModel;
    final int val$position;
    
    CarouselLayout$5(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, java.util.List a0, int i) {
        super();
        this.this$0 = a;
        this.val$newModel = a0;
        this.val$position = i;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        com.navdy.hud.app.ui.component.carousel.CarouselLayout.access$600(this.this$0, this.val$position, false);
        this.this$0.runQueuedOperation();
    }
    
    public void onAnimationStart(android.animation.Animator a) {
        this.this$0.model = this.val$newModel;
        if (this.this$0.itemChangeListener != null) {
            this.this$0.itemChangeListener.onCurrentItemChanging(this.this$0.currentItem, this.val$position, ((com.navdy.hud.app.ui.component.carousel.Carousel$Model)this.this$0.model.get(this.val$position)).id);
        }
    }
}
