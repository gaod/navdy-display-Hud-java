package com.navdy.hud.app.ui.component.vlist.viewholder;

class IconBaseViewHolder$4 {
    final static int[] $SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType;
    final static int[] $SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$State;
    
    static {
        $SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType = new int[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType;
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType a0 = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.INIT;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.NONE.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.MOVE.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        $SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$State = new int[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$State;
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State a2 = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$State[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.UNSELECTED.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException3) {
        }
    }
}
