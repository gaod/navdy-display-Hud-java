package com.navdy.hud.app.ui.component.vmenu;

final class VerticalAnimationUtils$3 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final android.view.ViewGroup$MarginLayoutParams val$layoutParams;
    final android.view.View val$view;
    
    VerticalAnimationUtils$3(android.view.ViewGroup$MarginLayoutParams a, android.view.View a0) {
        super();
        this.val$layoutParams = a;
        this.val$view = a0;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        int i = ((Integer)a.getAnimatedValue()).intValue();
        this.val$layoutParams.topMargin = i;
        this.val$view.requestLayout();
    }
}
