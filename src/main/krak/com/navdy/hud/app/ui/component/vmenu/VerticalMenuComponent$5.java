package com.navdy.hud.app.ui.component.vmenu;

class VerticalMenuComponent$5 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent this$0;
    
    VerticalMenuComponent$5(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.access$000().v("closeMenuHideListener");
        com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.access$102(this.this$0, false);
        this.this$0.closeContainer.setVisibility(8);
        this.this$0.closeContainerScrim.setVisibility(8);
    }
}
