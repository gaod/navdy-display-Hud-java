package com.navdy.hud.app.ui.component;


public enum SystemTrayView$Device {
    Phone(0),
    Dial(1),
    Gps(2);

    private int value;
    SystemTrayView$Device(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class SystemTrayView$Device extends Enum {
//    final private static com.navdy.hud.app.ui.component.SystemTrayView$Device[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.SystemTrayView$Device Dial;
//    final public static com.navdy.hud.app.ui.component.SystemTrayView$Device Gps;
//    final public static com.navdy.hud.app.ui.component.SystemTrayView$Device Phone;
//    
//    static {
//        Phone = new com.navdy.hud.app.ui.component.SystemTrayView$Device("Phone", 0);
//        Dial = new com.navdy.hud.app.ui.component.SystemTrayView$Device("Dial", 1);
//        Gps = new com.navdy.hud.app.ui.component.SystemTrayView$Device("Gps", 2);
//        com.navdy.hud.app.ui.component.SystemTrayView$Device[] a = new com.navdy.hud.app.ui.component.SystemTrayView$Device[3];
//        a[0] = Phone;
//        a[1] = Dial;
//        a[2] = Gps;
//        $VALUES = a;
//    }
//    
//    private SystemTrayView$Device(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.SystemTrayView$Device valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.SystemTrayView$Device)Enum.valueOf(com.navdy.hud.app.ui.component.SystemTrayView$Device.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.SystemTrayView$Device[] values() {
//        return $VALUES.clone();
//    }
//}
//