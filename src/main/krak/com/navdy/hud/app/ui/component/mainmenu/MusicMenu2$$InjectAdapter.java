package com.navdy.hud.app.ui.component.mainmenu;

final public class MusicMenu2$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding musicArtworkCache;
    private dagger.internal.Binding musicCollectionResponseMessageCache;
    
    public MusicMenu2$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.ui.component.mainmenu.MusicMenu2", false, com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.musicArtworkCache = a.requestBinding("com.navdy.hud.app.util.MusicArtworkCache", com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.class, (this).getClass().getClassLoader());
        this.musicCollectionResponseMessageCache = a.requestBinding("com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>", com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.musicArtworkCache);
        a0.add(this.musicCollectionResponseMessageCache);
    }
    
    public void injectMembers(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a) {
        a.musicArtworkCache = (com.navdy.hud.app.util.MusicArtworkCache)this.musicArtworkCache.get();
        a.musicCollectionResponseMessageCache = (com.navdy.hud.app.storage.cache.MessageCache)this.musicCollectionResponseMessageCache.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.ui.component.mainmenu.MusicMenu2)a);
    }
}
