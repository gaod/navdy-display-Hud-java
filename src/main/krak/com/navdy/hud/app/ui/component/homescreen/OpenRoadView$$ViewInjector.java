package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class OpenRoadView$$ViewInjector {
    public OpenRoadView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.ui.component.homescreen.OpenRoadView a0, Object a1) {
        a0.openMapRoadInfo = (android.widget.TextView)a.findRequiredView(a1, R.id.openMapRoadInfo, "field 'openMapRoadInfo'");
    }
    
    public static void reset(com.navdy.hud.app.ui.component.homescreen.OpenRoadView a) {
        a.openMapRoadInfo = null;
    }
}
