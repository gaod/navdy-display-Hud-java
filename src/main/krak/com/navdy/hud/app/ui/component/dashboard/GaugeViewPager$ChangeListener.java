package com.navdy.hud.app.ui.component.dashboard;

abstract public interface GaugeViewPager$ChangeListener {
    abstract public void onGaugeChanged(int arg);
}
