package com.navdy.hud.app.ui.component.vmenu;

class VerticalMenuComponent$7 implements com.navdy.hud.app.ui.component.vlist.VerticalList$ContainerCallback {
    final com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent this$0;
    
    VerticalMenuComponent$7(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a) {
        super();
        this.this$0 = a;
    }
    
    public void hideToolTips() {
        this.this$0.hideToolTip();
    }
    
    public boolean isCloseMenuVisible() {
        return this.this$0.isCloseMenuVisible();
    }
    
    public boolean isFastScrolling() {
        return com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.access$900(this.this$0);
    }
    
    public void showToolTips() {
        if (com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.access$700(this.this$0) != null) {
            com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.access$700(this.this$0).showToolTip();
        }
    }
}
