package com.navdy.hud.app.ui.component.destination;

final public class DestinationPickerView$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding presenter;
    
    public DestinationPickerView$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.ui.component.destination.DestinationPickerView", false, com.navdy.hud.app.ui.component.destination.DestinationPickerView.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.presenter = a.requestBinding("com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter", com.navdy.hud.app.ui.component.destination.DestinationPickerView.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.presenter);
    }
    
    public void injectMembers(com.navdy.hud.app.ui.component.destination.DestinationPickerView a) {
        a.presenter = (com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter)this.presenter.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.ui.component.destination.DestinationPickerView)a);
    }
}
