package com.navdy.hud.app.ui.component.carousel;

class CarouselLayout$7 implements Runnable {
    final com.navdy.hud.app.ui.component.carousel.CarouselLayout this$0;
    final com.navdy.hud.app.ui.component.carousel.Carousel$Model val$item;
    final android.animation.Animator$AnimatorListener val$listener;
    final int val$pos;
    final boolean val$preserveCurrentItem;
    
    CarouselLayout$7(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, android.animation.Animator$AnimatorListener a0, int i, com.navdy.hud.app.ui.component.carousel.Carousel$Model a1, boolean b) {
        super();
        this.this$0 = a;
        this.val$listener = a0;
        this.val$pos = i;
        this.val$item = a1;
        this.val$preserveCurrentItem = b;
    }
    
    public void run() {
        com.navdy.hud.app.ui.component.carousel.CarouselLayout.access$700(this.this$0, this.val$listener, this.val$pos, this.val$item, this.val$preserveCurrentItem);
    }
}
