package com.navdy.hud.app.ui.framework;

public class UIStateManager {
    final private static java.util.EnumSet FULLSCREEN_MODES;
    final private static java.util.EnumSet MAIN_SCREENS_SET;
    final private static java.util.EnumSet OVERLAY_MODES;
    final private static java.util.EnumSet PAUSE_HOME_SCREEN_SET;
    final private static java.util.EnumSet SIDE_PANEL_MODES;
    private volatile com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants$Type currentDashboardType;
    private com.navdy.hud.app.screen.BaseScreen currentScreen;
    private volatile com.navdy.service.library.events.ui.Screen currentViewMode;
    private com.navdy.service.library.events.ui.Screen defaultMainActiveScreen;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private com.navdy.hud.app.screen.BaseScreen mainActiveScreen;
    private int mainPanelWidth;
    private java.util.HashSet notificationListeners;
    private com.navdy.hud.app.ui.activity.Main rootScreen;
    private java.util.HashSet screenListeners;
    private int sidePanelWidth;
    private com.navdy.hud.app.view.ToastView toastView;
    
    static {
        SIDE_PANEL_MODES = java.util.EnumSet.of((Enum)com.navdy.service.library.events.ui.Screen.SCREEN_NOTIFICATION);
        com.navdy.service.library.events.ui.Screen a = com.navdy.service.library.events.ui.Screen.SCREEN_MENU;
        com.navdy.service.library.events.ui.Screen[] a0 = new com.navdy.service.library.events.ui.Screen[17];
        a0[0] = com.navdy.service.library.events.ui.Screen.SCREEN_MAIN_MENU;
        a0[1] = com.navdy.service.library.events.ui.Screen.SCREEN_RECENT_CALLS;
        a0[2] = com.navdy.service.library.events.ui.Screen.SCREEN_RECOMMENDED_PLACES;
        a0[3] = com.navdy.service.library.events.ui.Screen.SCREEN_FAVORITE_PLACES;
        a0[4] = com.navdy.service.library.events.ui.Screen.SCREEN_OTA_CONFIRMATION;
        a0[5] = com.navdy.service.library.events.ui.Screen.SCREEN_BRIGHTNESS;
        a0[6] = com.navdy.service.library.events.ui.Screen.SCREEN_FAVORITE_CONTACTS;
        a0[7] = com.navdy.service.library.events.ui.Screen.SCREEN_SHUTDOWN_CONFIRMATION;
        a0[8] = com.navdy.service.library.events.ui.Screen.SCREEN_SETTINGS;
        a0[9] = com.navdy.service.library.events.ui.Screen.SCREEN_AUTO_BRIGHTNESS;
        a0[10] = com.navdy.service.library.events.ui.Screen.SCREEN_FACTORY_RESET;
        a0[11] = com.navdy.service.library.events.ui.Screen.SCREEN_REPORT_ISSUE;
        a0[12] = com.navdy.service.library.events.ui.Screen.SCREEN_TOAST;
        a0[13] = com.navdy.service.library.events.ui.Screen.SCREEN_FORCE_UPDATE;
        a0[14] = com.navdy.service.library.events.ui.Screen.SCREEN_GESTURE_LEARNING;
        a0[15] = com.navdy.service.library.events.ui.Screen.SCREEN_DESTINATION_PICKER;
        a0[16] = com.navdy.service.library.events.ui.Screen.SCREEN_MUSIC_DETAILS;
        OVERLAY_MODES = java.util.EnumSet.of((Enum)a, (Enum[])a0);
        FULLSCREEN_MODES = java.util.EnumSet.of((Enum)com.navdy.service.library.events.ui.Screen.SCREEN_FIRST_LAUNCH, (Enum)com.navdy.service.library.events.ui.Screen.SCREEN_HOME, (Enum)com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME, (Enum)com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING);
        com.navdy.service.library.events.ui.Screen a1 = com.navdy.service.library.events.ui.Screen.SCREEN_MENU;
        com.navdy.service.library.events.ui.Screen[] a2 = new com.navdy.service.library.events.ui.Screen[13];
        a2[0] = com.navdy.service.library.events.ui.Screen.SCREEN_RECENT_CALLS;
        a2[1] = com.navdy.service.library.events.ui.Screen.SCREEN_RECOMMENDED_PLACES;
        a2[2] = com.navdy.service.library.events.ui.Screen.SCREEN_FAVORITE_PLACES;
        a2[3] = com.navdy.service.library.events.ui.Screen.SCREEN_OTA_CONFIRMATION;
        a2[4] = com.navdy.service.library.events.ui.Screen.SCREEN_FAVORITE_CONTACTS;
        a2[5] = com.navdy.service.library.events.ui.Screen.SCREEN_SHUTDOWN_CONFIRMATION;
        a2[6] = com.navdy.service.library.events.ui.Screen.SCREEN_SETTINGS;
        a2[7] = com.navdy.service.library.events.ui.Screen.SCREEN_AUTO_BRIGHTNESS;
        a2[8] = com.navdy.service.library.events.ui.Screen.SCREEN_FACTORY_RESET;
        a2[9] = com.navdy.service.library.events.ui.Screen.SCREEN_REPORT_ISSUE;
        a2[10] = com.navdy.service.library.events.ui.Screen.SCREEN_FORCE_UPDATE;
        a2[11] = com.navdy.service.library.events.ui.Screen.SCREEN_GESTURE_LEARNING;
        a2[12] = com.navdy.service.library.events.ui.Screen.SCREEN_MUSIC_DETAILS;
        PAUSE_HOME_SCREEN_SET = java.util.EnumSet.of((Enum)a1, (Enum[])a2);
        MAIN_SCREENS_SET = java.util.EnumSet.of((Enum)com.navdy.service.library.events.ui.Screen.SCREEN_HOME);
    }
    
    public UIStateManager() {
        this.defaultMainActiveScreen = com.navdy.service.library.events.ui.Screen.SCREEN_HOME;
        this.screenListeners = new java.util.HashSet();
        this.notificationListeners = new java.util.HashSet();
    }
    
    private void checkListener(Object a) {
        if (a == null) {
            throw new IllegalArgumentException();
        }
    }
    
    public static boolean isFullscreenMode(com.navdy.service.library.events.ui.Screen a) {
        return FULLSCREEN_MODES.contains(a);
    }
    
    public static boolean isOverlayMode(com.navdy.service.library.events.ui.Screen a) {
        return OVERLAY_MODES.contains(a);
    }
    
    public static boolean isPauseHomescreen(com.navdy.service.library.events.ui.Screen a) {
        return PAUSE_HOME_SCREEN_SET.contains(a);
    }
    
    public static boolean isSidePanelMode(com.navdy.service.library.events.ui.Screen a) {
        return SIDE_PANEL_MODES.contains(a);
    }
    
    public void addNotificationAnimationListener(com.navdy.hud.app.ui.framework.INotificationAnimationListener a) {
        synchronized(this.notificationListeners) {
            this.checkListener(a);
            this.notificationListeners.add(a);
            /*monexit(a0)*/;
        }
    }
    
    public void addScreenAnimationListener(com.navdy.hud.app.ui.framework.IScreenAnimationListener a) {
        this.checkListener(a);
        this.screenListeners.add(a);
    }
    
    public void enableNotificationColor(boolean b) {
        if (this.rootScreen != null) {
            this.rootScreen.enableNotificationColor(b);
        }
    }
    
    public void enableSystemTray(boolean b) {
        if (this.rootScreen != null) {
            this.rootScreen.enableSystemTray(b);
        }
    }
    
    public com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants$Type getCurrentDashboardType() {
        return this.currentDashboardType;
    }
    
    public com.navdy.hud.app.screen.BaseScreen getCurrentScreen() {
        return this.currentScreen;
    }
    
    public com.navdy.service.library.events.ui.Screen getCurrentViewMode() {
        return this.currentViewMode;
    }
    
    public com.navdy.hud.app.view.MainView$CustomAnimationMode getCustomAnimateMode() {
        com.navdy.hud.app.view.MainView$CustomAnimationMode a = com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND;
        label0: if (this.isMainUIShrunk()) {
            boolean b = this.rootScreen.isNotificationViewShowing();
            label1: {
                if (b) {
                    break label1;
                }
                if (!this.rootScreen.isNotificationExpanding()) {
                    break label0;
                }
            }
            a = com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT;
        }
        if (a != null && this.homeScreenView != null && a == com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND && this.homeScreenView.isModeVisible()) {
            a = com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_MODE;
        }
        return a;
    }
    
    public com.navdy.service.library.events.ui.Screen getDefaultMainActiveScreen() {
        return this.defaultMainActiveScreen;
    }
    
    public com.navdy.hud.app.ui.component.homescreen.HomeScreenView getHomescreenView() {
        return this.homeScreenView;
    }
    
    public com.navdy.hud.app.screen.BaseScreen getMainActiveScreen() {
        return this.mainActiveScreen;
    }
    
    public int getMainPanelWidth() {
        return this.mainPanelWidth;
    }
    
    public java.util.EnumSet getMainScreensSet() {
        return MAIN_SCREENS_SET;
    }
    
    public com.navdy.hud.app.ui.component.homescreen.NavigationView getNavigationView() {
        com.navdy.hud.app.ui.component.homescreen.NavigationView a = (this.homeScreenView == null) ? null : this.homeScreenView.getNavigationView();
        return a;
    }
    
    public com.navdy.hud.app.ui.activity.Main getRootScreen() {
        return this.rootScreen;
    }
    
    public int getSidePanelWidth() {
        return this.sidePanelWidth;
    }
    
    public com.navdy.hud.app.ui.component.homescreen.SmartDashView getSmartDashView() {
        com.navdy.hud.app.ui.component.homescreen.SmartDashView a = (this.homeScreenView == null) ? null : (com.navdy.hud.app.ui.component.homescreen.SmartDashView)this.homeScreenView.getSmartDashView();
        return a;
    }
    
    public com.navdy.hud.app.view.ToastView getToastView() {
        return this.toastView;
    }
    
    public boolean isDialPairingScreenOn() {
        boolean b = false;
        com.navdy.hud.app.screen.BaseScreen a = this.getCurrentScreen();
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.getScreen() == com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public boolean isDialPairingScreenScanning() {
        boolean b = false;
        com.navdy.hud.app.screen.BaseScreen a = this.getCurrentScreen();
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.getScreen() == com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = ((com.navdy.hud.app.screen.DialManagerScreen)a).isScanningMode();
        }
        return b;
    }
    
    public boolean isMainUIShrunk() {
        return this.rootScreen != null && this.rootScreen.isMainUIShrunk();
    }
    
    public boolean isNavigationActive() {
        com.navdy.hud.app.ui.component.homescreen.HomeScreenView a = this.getHomescreenView();
        return a != null && a.isNavigationActive();
    }
    
    public boolean isWelcomeScreenOn() {
        boolean b = false;
        com.navdy.hud.app.screen.BaseScreen a = this.getCurrentScreen();
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.getScreen() == com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public void postNotificationAnimationEvent(boolean b, String s, com.navdy.hud.app.framework.notifications.NotificationType a, com.navdy.hud.app.ui.framework.UIStateManager$Mode a0) {
        synchronized(this.notificationListeners) {
            Object a2 = this.notificationListeners.iterator();
            while(((java.util.Iterator)a2).hasNext()) {
                Object a3 = ((java.util.Iterator)a2).next();
                if (b) {
                    ((com.navdy.hud.app.ui.framework.INotificationAnimationListener)a3).onStart(s, a, a0);
                } else {
                    ((com.navdy.hud.app.ui.framework.INotificationAnimationListener)a3).onStop(s, a, a0);
                }
            }
            /*monexit(a1)*/;
        }
    }
    
    public void postScreenAnimationEvent(boolean b, com.navdy.hud.app.screen.BaseScreen a, com.navdy.hud.app.screen.BaseScreen a0) {
        Object a1 = this.screenListeners.iterator();
        while(((java.util.Iterator)a1).hasNext()) {
            Object a2 = ((java.util.Iterator)a1).next();
            if (b) {
                ((com.navdy.hud.app.ui.framework.IScreenAnimationListener)a2).onStart(a, a0);
            } else {
                ((com.navdy.hud.app.ui.framework.IScreenAnimationListener)a2).onStop(a, a0);
            }
        }
    }
    
    public void removeNotificationAnimationListener(com.navdy.hud.app.ui.framework.INotificationAnimationListener a) {
        synchronized(this.notificationListeners) {
            if (a != null) {
                this.notificationListeners.remove(a);
            }
            /*monexit(a0)*/;
        }
    }
    
    public void removeScreenAnimationListener(com.navdy.hud.app.ui.framework.IScreenAnimationListener a) {
        if (a != null) {
            this.screenListeners.remove(a);
        }
    }
    
    public void setCurrentDashboardType(com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants$Type a) {
        this.currentDashboardType = a;
    }
    
    public void setCurrentViewMode(com.navdy.service.library.events.ui.Screen a) {
        this.currentViewMode = a;
    }
    
    public void setHomescreenView(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        this.homeScreenView = a;
    }
    
    public void setInputFocus() {
        if (this.rootScreen != null) {
            this.rootScreen.setInputFocus();
        }
    }
    
    public void setMainActiveScreen(com.navdy.hud.app.screen.BaseScreen a) {
        if (com.navdy.hud.app.ui.framework.UIStateManager.isFullscreenMode(a.getScreen())) {
            this.mainActiveScreen = a;
            com.navdy.service.library.events.ui.Screen a0 = a.getScreen();
            if (a0 != com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME && a0 != com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING && a0 != com.navdy.service.library.events.ui.Screen.SCREEN_FIRST_LAUNCH) {
                this.defaultMainActiveScreen = a0;
            }
        } else {
            this.mainActiveScreen = null;
        }
        this.currentScreen = a;
    }
    
    public void setMainPanelWidth(int i) {
        this.mainPanelWidth = i;
    }
    
    public void setRootScreen(com.navdy.hud.app.ui.activity.Main a) {
        this.rootScreen = a;
    }
    
    public void setSidePanelWidth(int i) {
        this.sidePanelWidth = i;
    }
    
    public void setToastView(com.navdy.hud.app.view.ToastView a) {
        this.toastView = a;
    }
    
    public void showSystemTray(int i) {
        if (this.rootScreen != null) {
            this.rootScreen.setSystemTrayVisibility(i);
        }
    }
}
