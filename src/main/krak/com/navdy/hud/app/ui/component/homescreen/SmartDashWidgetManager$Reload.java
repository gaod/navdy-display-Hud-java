package com.navdy.hud.app.ui.component.homescreen;


public enum SmartDashWidgetManager$Reload {
    RELOAD_CACHE(0),
    RELOADED(1);

    private int value;
    SmartDashWidgetManager$Reload(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class SmartDashWidgetManager$Reload extends Enum {
//    final private static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$Reload[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$Reload RELOADED;
//    final public static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$Reload RELOAD_CACHE;
//    
//    static {
//        RELOAD_CACHE = new com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$Reload("RELOAD_CACHE", 0);
//        RELOADED = new com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$Reload("RELOADED", 1);
//        com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$Reload[] a = new com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$Reload[2];
//        a[0] = RELOAD_CACHE;
//        a[1] = RELOADED;
//        $VALUES = a;
//    }
//    
//    private SmartDashWidgetManager$Reload(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$Reload valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$Reload)Enum.valueOf(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$Reload.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$Reload[] values() {
//        return $VALUES.clone();
//    }
//}
//