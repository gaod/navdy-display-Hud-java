package com.navdy.hud.app.ui.component.mainmenu;

final public class MainMenuView2$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding presenter;
    
    public MainMenuView2$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.ui.component.mainmenu.MainMenuView2", false, com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.presenter = a.requestBinding("com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter", com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.presenter);
    }
    
    public void injectMembers(com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a) {
        a.presenter = (com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter)this.presenter.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)a);
    }
}
