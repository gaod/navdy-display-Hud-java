package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class DashWidgetPresenterFactory {
    final private static java.util.HashMap RESOURCE_MAP;
    
    static {
        RESOURCE_MAP = new java.util.HashMap();
        RESOURCE_MAP.put("EMPTY_WIDGET", Integer.valueOf(0));
        RESOURCE_MAP.put("MPG_GRAPH_WIDGET", Integer.valueOf(R.drawable.asset_dash20_proto_gauge_mpg));
        RESOURCE_MAP.put("MPG_AVG_WIDGET", Integer.valueOf(R.drawable.asset_dash20_proto_gauge_clock_analog));
        RESOURCE_MAP.put("WEATHER_GRAPH_WIDGET", Integer.valueOf(R.drawable.asset_dash20_proto_gauge_weather));
    }
    
    public DashWidgetPresenterFactory() {
    }
    
    public static com.navdy.hud.app.view.DashboardWidgetPresenter createDashWidgetPresenter(android.content.Context a, String s) {
        int i = 0;
        com.navdy.hud.app.view.DashboardWidgetPresenter a0 = null;
        switch(s.hashCode()) {
            case 2057821183: {
                i = (s.equals("GFORCE_WIDGET")) ? 12 : -1;
                break;
            }
            case 1872543020: {
                i = (s.equals("TRAFFIC_INCIDENT_GAUGE_ID")) ? 6 : -1;
                break;
            }
            case 1168400042: {
                i = (s.equals("FUEL_GAUGE_ID")) ? 1 : -1;
                break;
            }
            case 1119776967: {
                i = (s.equals("SPEED_LIMIT_SIGN_GAUGE_ID")) ? 11 : -1;
                break;
            }
            case 811991446: {
                i = (s.equals("EMPTY_WIDGET")) ? 0 : -1;
                break;
            }
            case 662413980: {
                i = (s.equals("DIGITAL_CLOCK_WIDGET")) ? 4 : -1;
                break;
            }
            case 641719909: {
                i = (s.equals("CALENDAR_WIDGET")) ? 7 : -1;
                break;
            }
            case 460083427: {
                i = (s.equals("DRIVE_SCORE_GAUGE_ID")) ? 14 : -1;
                break;
            }
            case -131933527: {
                i = (s.equals("ENGINE_TEMPERATURE_GAUGE_ID")) ? 13 : -1;
                break;
            }
            case -538792631: {
                i = (s.equals("DIGITAL_CLOCK_2_WIDGET")) ? 5 : -1;
                break;
            }
            case -856367506: {
                i = (s.equals("ETA_GAUGE_ID")) ? 8 : -1;
                break;
            }
            case -1158963060: {
                i = (s.equals("MPG_AVG_WIDGET")) ? 9 : -1;
                break;
            }
            case -1515555394: {
                i = (s.equals("MUSIC_WIDGET")) ? 10 : -1;
                break;
            }
            case -1874661839: {
                i = (s.equals("COMPASS_WIDGET")) ? 2 : -1;
                break;
            }
            case -2114596188: {
                i = (s.equals("ANALOG_CLOCK_WIDGET")) ? 3 : -1;
                break;
            }
            default: {
                i = -1;
            }
        }
        switch(i) {
            case 14: {
                a0 = new com.navdy.hud.app.view.DriveScoreGaugePresenter(a, R.layout.drive_score_gauge_layout, true);
                break;
            }
            case 13: {
                a0 = new com.navdy.hud.app.view.EngineTemperaturePresenter(a);
                break;
            }
            case 12: {
                a0 = new com.navdy.hud.app.view.GForcePresenter(a);
                break;
            }
            case 11: {
                a0 = new com.navdy.hud.app.view.SpeedLimitSignPresenter(a);
                break;
            }
            case 10: {
                a0 = new com.navdy.hud.app.view.MusicWidgetPresenter(a);
                break;
            }
            case 9: {
                a0 = new com.navdy.hud.app.view.MPGGaugePresenter(a);
                break;
            }
            case 8: {
                a0 = new com.navdy.hud.app.view.ETAGaugePresenter(a);
                break;
            }
            case 7: {
                a0 = new com.navdy.hud.app.view.CalendarWidgetPresenter(a);
                break;
            }
            case 6: {
                a0 = new com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter();
                break;
            }
            case 5: {
                a0 = new com.navdy.hud.app.view.ClockWidgetPresenter(a, com.navdy.hud.app.view.ClockWidgetPresenter$ClockType.DIGITAL2);
                break;
            }
            case 4: {
                a0 = new com.navdy.hud.app.view.ClockWidgetPresenter(a, com.navdy.hud.app.view.ClockWidgetPresenter$ClockType.DIGITAL1);
                break;
            }
            case 3: {
                a0 = new com.navdy.hud.app.view.ClockWidgetPresenter(a, com.navdy.hud.app.view.ClockWidgetPresenter$ClockType.ANALOG);
                break;
            }
            case 2: {
                a0 = new com.navdy.hud.app.view.CompassPresenter(a);
                break;
            }
            case 1: {
                a0 = new com.navdy.hud.app.view.FuelGaugePresenter2(a);
                break;
            }
            case 0: {
                a0 = new com.navdy.hud.app.view.EmptyGaugePresenter(a);
                break;
            }
            default: {
                a0 = new com.navdy.hud.app.ui.component.homescreen.ImageWidgetPresenter(a, ((Integer)RESOURCE_MAP.get(s)).intValue(), s);
            }
        }
        return a0;
    }
}
