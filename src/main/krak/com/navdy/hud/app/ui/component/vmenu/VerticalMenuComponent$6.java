package com.navdy.hud.app.ui.component.vmenu;

class VerticalMenuComponent$6 implements com.navdy.hud.app.ui.component.vlist.VerticalList$Callback {
    final com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent this$0;
    
    VerticalMenuComponent$6(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a) {
        super();
        this.this$0 = a;
    }
    
    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, android.view.View a0, int i, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a1) {
        com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.access$700(this.this$0).onBindToView(a, a0, i, a1);
    }
    
    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.access$700(this.this$0).onItemSelected(a);
    }
    
    public void onLoad() {
        com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.access$802(this.this$0, true);
        com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.access$700(this.this$0).onLoad();
    }
    
    public void onScrollIdle() {
        com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.access$700(this.this$0).onScrollIdle();
    }
    
    public void select(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.access$700(this.this$0).select(a);
    }
}
