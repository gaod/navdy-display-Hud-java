package com.navdy.hud.app.ui.component.homescreen;

class SmartDashView$8 {
    final static int[] $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
    final static int[] $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode;
    final static int[] $SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge;
    final static int[] $SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide;
    
    static {
        $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode = new int[com.navdy.hud.app.view.MainView$CustomAnimationMode.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode;
        com.navdy.hud.app.view.MainView$CustomAnimationMode a0 = com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_MODE.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
        com.navdy.hud.app.manager.InputManager$CustomKeyEvent a2 = com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LEFT;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException3) {
        }
        $SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide = new int[com.navdy.service.library.events.preferences.ScrollableSide.values().length];
        int[] a3 = $SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide;
        com.navdy.service.library.events.preferences.ScrollableSide a4 = com.navdy.service.library.events.preferences.ScrollableSide.LEFT;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide[com.navdy.service.library.events.preferences.ScrollableSide.RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException5) {
        }
        $SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge = new int[com.navdy.service.library.events.preferences.MiddleGauge.values().length];
        int[] a5 = $SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge;
        com.navdy.service.library.events.preferences.MiddleGauge a6 = com.navdy.service.library.events.preferences.MiddleGauge.SPEEDOMETER;
        try {
            a5[a6.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge[com.navdy.service.library.events.preferences.MiddleGauge.TACHOMETER.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException7) {
        }
    }
}
