package com.navdy.hud.app.ui.component.homescreen;

public class TbtEarlyView extends android.widget.LinearLayout implements com.navdy.hud.app.ui.component.homescreen.IHomeScreenLifecycle {
    final private static String TBT_EARLY_SHRUNK_MODE_VISIBLE = "persist.sys.tbtearlyshrunk";
    private com.navdy.hud.app.view.MainView$CustomAnimationMode currentMode;
    @InjectView(R.id.earlyManeuverImage)
    android.widget.ImageView earlyManeuverImage;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    final private boolean isShrunkVisible;
    private int lastNextTurnIconId;
    private com.navdy.service.library.log.Logger logger;
    private boolean paused;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    
    public TbtEarlyView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public TbtEarlyView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public TbtEarlyView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.lastNextTurnIconId = -1;
        this.isShrunkVisible = com.navdy.hud.app.util.os.SystemProperties.getBoolean("persist.sys.tbtearlyshrunk", true);
    }
    
    private void setEarlyManeuver(int i) {
        if (this.lastNextTurnIconId != i) {
            this.lastNextTurnIconId = i;
            if (!this.paused) {
                if (this.lastNextTurnIconId != -1) {
                    this.earlyManeuverImage.setImageResource(this.lastNextTurnIconId);
                }
                this.showHideEarlyManeuver();
            }
        }
    }
    
    public void clearState() {
        this.lastNextTurnIconId = -1;
        this.showHideEarlyManeuver();
    }
    
    public void getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode a, android.animation.AnimatorSet$Builder a0) {
        this.currentMode = a;
        switch(com.navdy.hud.app.ui.component.homescreen.TbtEarlyView$1.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: {
                if (!this.isShrunkVisible) {
                    a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getAlphaAnimator((android.view.View)this, 0));
                }
                a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getWidthAnimator((android.view.View)this, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadEarlyManeuverWidthShrunk));
                a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadEarlyManeuverShrinkLeftX));
                a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getYPositionAnimator((android.view.View)this, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadEarlyManeuverYShrunk));
                break;
            }
            case 1: {
                if (!this.isShrunkVisible) {
                    a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getAlphaAnimator((android.view.View)this, 1));
                }
                a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getWidthAnimator((android.view.View)this, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadEarlyManeuverWidth));
                a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadEarlyManeuverX));
                a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getYPositionAnimator((android.view.View)this, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadEarlyManeuverY));
                break;
            }
        }
    }
    
    protected void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        com.navdy.hud.app.manager.RemoteDeviceManager a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        this.uiStateManager = a.getUiStateManager();
        a.getBus().register(this);
    }
    
    public void onManeuverDisplay(com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a) {
        this.setEarlyManeuver(a.nextTurnIconId);
    }
    
    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.logger.v("::onPause:tbtearly");
        }
    }
    
    public void onResume() {
        if (this.paused) {
            this.paused = false;
            this.logger.v("::onResume:tbtearly");
            int i = this.lastNextTurnIconId;
            if (i != -1) {
                this.logger.v("::onResume:tbtearly set last turn icon");
                this.lastNextTurnIconId = -1;
                this.setEarlyManeuver(i);
            }
        }
    }
    
    public void setView(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        this.currentMode = a;
        switch(com.navdy.hud.app.ui.component.homescreen.TbtEarlyView$1.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: {
                this.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadEarlyManeuverShrinkLeftX);
                this.setY((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadEarlyManeuverYShrunk);
                break;
            }
            case 1: {
                this.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadEarlyManeuverX);
                this.setY((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadEarlyManeuverY);
                break;
            }
        }
    }
    
    public void setViewXY() {
        this.setView(this.uiStateManager.getCustomAnimateMode());
    }
    
    public void showHideEarlyManeuver() {
        if (this.lastNextTurnIconId == -1) {
            this.setVisibility(4);
            this.earlyManeuverImage.setImageResource(0);
        } else {
            boolean b = false;
            if (this.homeScreenView == null) {
                this.homeScreenView = this.uiStateManager.getHomescreenView();
            }
            com.navdy.hud.app.view.MainView$CustomAnimationMode a = this.currentMode;
            com.navdy.hud.app.view.MainView$CustomAnimationMode a0 = com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND;
            label2: {
                label0: {
                    label1: {
                        if (a == a0) {
                            break label1;
                        }
                        if (!this.isShrunkVisible) {
                            break label0;
                        }
                    }
                    b = false;
                    break label2;
                }
                b = true;
            }
            if (b) {
                this.setVisibility(4);
            } else {
                this.setVisibility(0);
            }
        }
    }
}
