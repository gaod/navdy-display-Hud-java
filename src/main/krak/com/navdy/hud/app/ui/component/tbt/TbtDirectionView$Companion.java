package com.navdy.hud.app.ui.component.tbt;

final public class TbtDirectionView$Companion {
    private TbtDirectionView$Companion() {
    }
    
    public TbtDirectionView$Companion(kotlin.jvm.internal.DefaultConstructorMarker a) {
        this();
    }
    
    final public static int access$getFullWidth$p(com.navdy.hud.app.ui.component.tbt.TbtDirectionView$Companion a) {
        return a.getFullWidth();
    }
    
    final public static int access$getMediumWidth$p(com.navdy.hud.app.ui.component.tbt.TbtDirectionView$Companion a) {
        return a.getMediumWidth();
    }
    
    final public static android.content.res.Resources access$getResources$p(com.navdy.hud.app.ui.component.tbt.TbtDirectionView$Companion a) {
        return a.getResources();
    }
    
    final private int getFullWidth() {
        return com.navdy.hud.app.ui.component.tbt.TbtDirectionView.access$getFullWidth$cp();
    }
    
    final private com.navdy.service.library.log.Logger getLogger() {
        return com.navdy.hud.app.ui.component.tbt.TbtDirectionView.access$getLogger$cp();
    }
    
    final private int getMediumWidth() {
        return com.navdy.hud.app.ui.component.tbt.TbtDirectionView.access$getMediumWidth$cp();
    }
    
    final private android.content.res.Resources getResources() {
        return com.navdy.hud.app.ui.component.tbt.TbtDirectionView.access$getResources$cp();
    }
}
