package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class SpeedView$$ViewInjector {
    public SpeedView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.ui.component.homescreen.SpeedView a0, Object a1) {
        a0.speedView = (android.widget.TextView)a.findRequiredView(a1, R.id.speedView, "field 'speedView'");
        a0.speedUnitView = (android.widget.TextView)a.findRequiredView(a1, R.id.speedUnitView, "field 'speedUnitView'");
    }
    
    public static void reset(com.navdy.hud.app.ui.component.homescreen.SpeedView a) {
        a.speedView = null;
        a.speedUnitView = null;
    }
}
