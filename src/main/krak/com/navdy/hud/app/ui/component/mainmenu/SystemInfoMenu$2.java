package com.navdy.hud.app.ui.component.mainmenu;

class SystemInfoMenu$2 {
    final static int[] $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$DisplayFormat;
    final static int[] $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$ObdScanSetting;
    final static int[] $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$UnitSystem;
    final static int[] $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RoutingType;
    
    static {
        $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RoutingType = new int[com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RoutingType;
        com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType a0 = com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType.ROUTING_FASTEST;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RoutingType[com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType.ROUTING_SHORTEST.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$DisplayFormat = new int[com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat.values().length];
        int[] a1 = $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$DisplayFormat;
        com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat a2 = com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat.DISPLAY_FORMAT_COMPACT;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$DisplayFormat[com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat.DISPLAY_FORMAT_NORMAL.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException2) {
        }
        $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$ObdScanSetting = new int[com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting.values().length];
        int[] a3 = $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$ObdScanSetting;
        com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting a4 = com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting.SCAN_ON;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$ObdScanSetting[com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting.SCAN_DEFAULT_ON.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException4) {
        }
        $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$UnitSystem = new int[com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem.values().length];
        int[] a5 = $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$UnitSystem;
        com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem a6 = com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem.UNIT_SYSTEM_METRIC;
        try {
            a5[a6.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$UnitSystem[com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem.UNIT_SYSTEM_IMPERIAL.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException6) {
        }
    }
}
