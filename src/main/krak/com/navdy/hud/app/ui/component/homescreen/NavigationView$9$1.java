package com.navdy.hud.app.ui.component.homescreen;

class NavigationView$9$1 implements Runnable {
    final com.navdy.hud.app.ui.component.homescreen.NavigationView$9 this$1;
    
    NavigationView$9$1(com.navdy.hud.app.ui.component.homescreen.NavigationView$9 a) {
        super();
        this.this$1 = a;
    }
    
    public void run() {
        com.navdy.hud.app.ui.component.homescreen.NavigationView.access$400(this.this$1.this$0).post(com.navdy.hud.app.ui.component.homescreen.NavigationView.access$900(this.this$1.this$0));
        if (com.navdy.hud.app.ui.component.homescreen.NavigationView.access$500(this.this$1.this$0) != null) {
            com.navdy.hud.app.ui.component.homescreen.NavigationView.access$000(this.this$1.this$0).removeMapObject((com.here.android.mpa.mapping.MapObject)com.navdy.hud.app.ui.component.homescreen.NavigationView.access$500(this.this$1.this$0));
        }
        com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentMapRoute();
        boolean b = com.navdy.hud.app.ui.component.homescreen.NavigationView.access$1000(this.this$1.this$0).isTrafficEnabled();
        com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$1.this$0).v(new StringBuilder().append("animateBackfromOverview traffic=").append(b).toString());
        com.navdy.hud.app.ui.component.homescreen.NavigationView.access$000(this.this$1.this$0).setState(com.navdy.hud.app.maps.here.HereMapController$State.AR_MODE);
        if (this.this$1.val$endAction != null) {
            this.this$1.val$endAction.run();
        }
    }
}
