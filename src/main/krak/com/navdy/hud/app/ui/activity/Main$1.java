package com.navdy.hud.app.ui.activity;

class Main$1 {
    final static int[] $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason;
    final static int[] $SwitchMap$com$navdy$hud$app$event$InitEvents$Phase;
    final static int[] $SwitchMap$com$navdy$hud$app$event$Shutdown$State;
    final static int[] $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
    final static int[] $SwitchMap$com$navdy$hud$app$ui$activity$Main$ScreenCategory;
    final static int[] $SwitchMap$com$navdy$service$library$events$callcontrol$PhoneBatteryStatus$BatteryStatus;
    final static int[] $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState;
    final static int[] $SwitchMap$com$navdy$service$library$events$input$Gesture;
    final static int[] $SwitchMap$com$navdy$service$library$events$ui$Screen;
    
    static {
        $SwitchMap$com$navdy$hud$app$ui$activity$Main$ScreenCategory = new int[com.navdy.hud.app.ui.activity.Main$ScreenCategory.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$ui$activity$Main$ScreenCategory;
        com.navdy.hud.app.ui.activity.Main$ScreenCategory a0 = com.navdy.hud.app.ui.activity.Main$ScreenCategory.NOTIFICATION;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$activity$Main$ScreenCategory[com.navdy.hud.app.ui.activity.Main$ScreenCategory.SCREEN.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason = new int[com.navdy.hud.app.device.dial.DialConstants$NotificationReason.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason;
        com.navdy.hud.app.device.dial.DialConstants$NotificationReason a2 = com.navdy.hud.app.device.dial.DialConstants$NotificationReason.EXTREMELY_LOW_BATTERY;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason[com.navdy.hud.app.device.dial.DialConstants$NotificationReason.LOW_BATTERY.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason[com.navdy.hud.app.device.dial.DialConstants$NotificationReason.VERY_LOW_BATTERY.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason[com.navdy.hud.app.device.dial.DialConstants$NotificationReason.OK_BATTERY.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException4) {
        }
        $SwitchMap$com$navdy$service$library$events$callcontrol$PhoneBatteryStatus$BatteryStatus = new int[com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus.values().length];
        int[] a3 = $SwitchMap$com$navdy$service$library$events$callcontrol$PhoneBatteryStatus$BatteryStatus;
        com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus a4 = com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus.BATTERY_OK;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$callcontrol$PhoneBatteryStatus$BatteryStatus[com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus.BATTERY_EXTREMELY_LOW.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$callcontrol$PhoneBatteryStatus$BatteryStatus[com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus.BATTERY_LOW.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException7) {
        }
        $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState = new int[com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.values().length];
        int[] a5 = $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState;
        com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState a6 = com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_VERIFIED;
        try {
            a5[a6.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_DISCONNECTED.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException9) {
        }
        $SwitchMap$com$navdy$service$library$events$ui$Screen = new int[com.navdy.service.library.events.ui.Screen.values().length];
        int[] a7 = $SwitchMap$com$navdy$service$library$events$ui$Screen;
        com.navdy.service.library.events.ui.Screen a8 = com.navdy.service.library.events.ui.Screen.SCREEN_HYBRID_MAP;
        try {
            a7[a8.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException10) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$ui$Screen[com.navdy.service.library.events.ui.Screen.SCREEN_DASHBOARD.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException11) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$ui$Screen[com.navdy.service.library.events.ui.Screen.SCREEN_MUSIC.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException12) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$ui$Screen[com.navdy.service.library.events.ui.Screen.SCREEN_VOICE_CONTROL.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException13) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$ui$Screen[com.navdy.service.library.events.ui.Screen.SCREEN_BRIGHTNESS.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException14) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$ui$Screen[com.navdy.service.library.events.ui.Screen.SCREEN_NOTIFICATION.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException15) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$ui$Screen[com.navdy.service.library.events.ui.Screen.SCREEN_HOME.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException16) {
        }
        $SwitchMap$com$navdy$service$library$events$input$Gesture = new int[com.navdy.service.library.events.input.Gesture.values().length];
        int[] a9 = $SwitchMap$com$navdy$service$library$events$input$Gesture;
        com.navdy.service.library.events.input.Gesture a10 = com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_LEFT;
        try {
            a9[a10.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException17) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$input$Gesture[com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException18) {
        }
        $SwitchMap$com$navdy$hud$app$event$Shutdown$State = new int[com.navdy.hud.app.event.Shutdown$State.values().length];
        int[] a11 = $SwitchMap$com$navdy$hud$app$event$Shutdown$State;
        com.navdy.hud.app.event.Shutdown$State a12 = com.navdy.hud.app.event.Shutdown$State.CONFIRMATION;
        try {
            a11[a12.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException19) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$event$Shutdown$State[com.navdy.hud.app.event.Shutdown$State.SHUTTING_DOWN.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException20) {
        }
        $SwitchMap$com$navdy$hud$app$event$InitEvents$Phase = new int[com.navdy.hud.app.event.InitEvents$Phase.values().length];
        int[] a13 = $SwitchMap$com$navdy$hud$app$event$InitEvents$Phase;
        com.navdy.hud.app.event.InitEvents$Phase a14 = com.navdy.hud.app.event.InitEvents$Phase.PRE_USER_INTERACTION;
        try {
            a13[a14.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException21) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$event$InitEvents$Phase[com.navdy.hud.app.event.InitEvents$Phase.POST_START.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException22) {
        }
        $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.values().length];
        int[] a15 = $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
        com.navdy.hud.app.manager.InputManager$CustomKeyEvent a16 = com.navdy.hud.app.manager.InputManager$CustomKeyEvent.SELECT;
        try {
            a15[a16.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException23) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LONG_PRESS.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException24) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.POWER_BUTTON_LONG_PRESS.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException25) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.POWER_BUTTON_DOUBLE_CLICK.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException26) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.POWER_BUTTON_CLICK.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException27) {
        }
    }
}
