package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class TbtView$$ViewInjector {
    public TbtView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.ui.component.homescreen.TbtView a0, Object a1) {
        a0.activeMapDistance = (android.widget.TextView)a.findRequiredView(a1, R.id.activeMapDistance, "field 'activeMapDistance'");
        a0.activeMapIcon = (android.widget.ImageView)a.findRequiredView(a1, R.id.activeMapIcon, "field 'activeMapIcon'");
        a0.nextMapIconContainer = a.findRequiredView(a1, R.id.nextMapIconContainer, "field 'nextMapIconContainer'");
        a0.nextMapIcon = (android.widget.ImageView)a.findRequiredView(a1, R.id.nextMapIcon, "field 'nextMapIcon'");
        a0.activeMapInstruction = (android.widget.TextView)a.findRequiredView(a1, R.id.activeMapInstruction, "field 'activeMapInstruction'");
        a0.nextMapInstructionContainer = a.findRequiredView(a1, R.id.nextMapInstructionContainer, "field 'nextMapInstructionContainer'");
        a0.nextMapInstruction = (android.widget.TextView)a.findRequiredView(a1, R.id.nextMapInstruction, "field 'nextMapInstruction'");
        a0.progressBar = (android.widget.ProgressBar)a.findRequiredView(a1, R.id.progress_bar, "field 'progressBar'");
        a0.nowIcon = a.findRequiredView(a1, R.id.now_icon, "field 'nowIcon'");
    }
    
    public static void reset(com.navdy.hud.app.ui.component.homescreen.TbtView a) {
        a.activeMapDistance = null;
        a.activeMapIcon = null;
        a.nextMapIconContainer = null;
        a.nextMapIcon = null;
        a.activeMapInstruction = null;
        a.nextMapInstructionContainer = null;
        a.nextMapInstruction = null;
        a.progressBar = null;
        a.nowIcon = null;
    }
}
