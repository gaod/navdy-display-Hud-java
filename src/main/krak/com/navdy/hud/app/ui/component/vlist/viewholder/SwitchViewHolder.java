package com.navdy.hud.app.ui.component.vlist.viewholder;
import com.navdy.hud.app.R;

final public class SwitchViewHolder extends com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder {
    final public static com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$Companion Companion;
    final private static com.navdy.service.library.log.Logger logger;
    final private int FLUCTUATOR_OPACITY_ALPHA;
    final private int HALO_DELAY_START_DURATION;
    private android.animation.AnimatorSet$Builder animatorSetBuilder;
    final private Runnable fluctuatorRunnable;
    final private com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$fluctuatorStartListener$1 fluctuatorStartListener;
    private com.navdy.hud.app.ui.component.SwitchHaloView haloView;
    private boolean hasIconFluctuatorColor;
    private boolean hasSubTitle;
    private boolean hasSubTitle2;
    private boolean iconScaleAnimationDisabled;
    private boolean isEnabled;
    private boolean isOn;
    private android.widget.TextView subTitle;
    private android.widget.TextView subTitle2;
    private android.view.View switchBackground;
    private android.view.ViewGroup switchContainer;
    private android.view.View switchThumb;
    private boolean textAnimationDisabled;
    private android.view.ViewGroup textContainer;
    private int thumbMargin;
    private android.widget.TextView title;
    private float titleSelectedTopMargin;
    private float titleUnselectedScale;
    
    static {
        Companion = new com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$Companion((kotlin.jvm.internal.DefaultConstructorMarker)null);
        logger = new com.navdy.service.library.log.Logger((Companion).getClass());
    }
    
    public SwitchViewHolder(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, android.os.Handler a1) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "layout");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "vlist");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a1, "handler");
        super(a, a0, a1);
        this.FLUCTUATOR_OPACITY_ALPHA = 153;
        this.HALO_DELAY_START_DURATION = 100;
        this.fluctuatorStartListener = new com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$fluctuatorStartListener$1(this, a1);
        this.fluctuatorRunnable = (Runnable)new com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$fluctuatorRunnable$1(this);
        android.view.View a2 = a.findViewById(R.id.textContainer);
        if (a2 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup");
        }
        this.textContainer = (android.view.ViewGroup)a2;
        android.view.View a3 = a.findViewById(R.id.title);
        if (a3 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        this.title = (android.widget.TextView)a3;
        android.view.View a4 = a.findViewById(R.id.subTitle);
        if (a4 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        this.subTitle = (android.widget.TextView)a4;
        android.view.View a5 = a.findViewById(R.id.subTitle2);
        if (a5 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        this.subTitle2 = (android.widget.TextView)a5;
        android.view.View a6 = a.findViewById(R.id.toggleSwitchBackground);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a6, "layout.findViewById(R.id.toggleSwitchBackground)");
        this.switchBackground = a6;
        android.view.View a7 = a.findViewById(R.id.toggleSwitchThumb);
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a7, "layout.findViewById(R.id.toggleSwitchThumb)");
        this.switchThumb = a7;
        android.view.View a8 = a.findViewById(R.id.imageContainer);
        if (a8 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup");
        }
        this.switchContainer = (android.view.ViewGroup)a8;
        this.thumbMargin = a.getResources().getDimensionPixelSize(R.dimen.vlist_toggle_switch_thumb_margin);
        android.view.View a9 = a.findViewById(R.id.halo);
        if (a9 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.SwitchHaloView");
        }
        this.haloView = (com.navdy.hud.app.ui.component.SwitchHaloView)a9;
        this.haloView.setVisibility(8);
    }
    
    final public static Runnable access$getFluctuatorRunnable$p(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder a) {
        return a.fluctuatorRunnable;
    }
    
    final public static com.navdy.service.library.log.Logger access$getLogger$cp() {
        return logger;
    }
    
    final public static boolean access$isEnabled$p(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder a) {
        return a.isEnabled;
    }
    
    final public static boolean access$isOn$p(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder a) {
        return a.isOn;
    }
    
    final public static void access$setEnabled$p(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder a, boolean b) {
        a.isEnabled = b;
    }
    
    final public static void access$setOn$p(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder a, boolean b) {
        a.isOn = b;
    }
    
    final private int getSubTitle2Color() {
        return (this.extras != null) ? this.getTextColor("SUBTITLE_2_COLOR") : 0;
    }
    
    final private int getSubTitleColor() {
        return (this.extras != null) ? this.getTextColor("SUBTITLE_COLOR") : 0;
    }
    
    final private int getTextColor(String s) {
        int i = 0;
        java.util.HashMap a = this.extras;
        label0: {
            label1: if (a != null) {
                String s0 = (String)this.extras.get(s);
                if (s0 == null) {
                    i = 0;
                    break label0;
                } else {
                    try {
                        i = Integer.parseInt(s0);
                    } catch(NumberFormatException ignoredException) {
                        break label1;
                    }
                    break label0;
                }
            } else {
                i = 0;
                break label0;
            }
            i = 0;
        }
        return i;
    }
    
    final private void setIconFluctuatorColor(int i) {
        if (i == 0) {
            this.hasIconFluctuatorColor = false;
        } else {
            int i0 = android.graphics.Color.argb(this.FLUCTUATOR_OPACITY_ALPHA, android.graphics.Color.red(i), android.graphics.Color.green(i), android.graphics.Color.blue(i));
            this.hasIconFluctuatorColor = true;
            this.haloView.setStrokeColor(i0);
        }
    }
    
    final private void setSubTitle(String s, boolean b) {
        if (s != null) {
            int i = this.getSubTitleColor();
            if (i != 0) {
                this.subTitle.setTextColor(i);
            } else {
                this.subTitle.setTextColor(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.subTitleColor);
            }
            if (b) {
                this.subTitle.setText((CharSequence)android.text.Html.fromHtml(s));
            } else {
                this.subTitle.setText((CharSequence)s);
            }
            this.hasSubTitle = true;
        } else {
            this.subTitle.setText((CharSequence)"");
            this.hasSubTitle = false;
        }
    }
    
    final private void setSubTitle2(String s, boolean b) {
        if (s != null) {
            int i = this.getSubTitle2Color();
            if (i != 0) {
                this.subTitle2.setTextColor(i);
            } else {
                this.subTitle2.setTextColor(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.subTitle2Color);
            }
            if (b) {
                this.subTitle2.setText((CharSequence)android.text.Html.fromHtml(s));
            } else {
                this.subTitle2.setText((CharSequence)s);
            }
            this.subTitle2.setVisibility(0);
            this.hasSubTitle2 = true;
        } else {
            this.subTitle2.setText((CharSequence)"");
            this.subTitle2.setVisibility(8);
            this.hasSubTitle2 = false;
        }
    }
    
    final private void setTitle(String s) {
        this.title.setText((CharSequence)s);
    }
    
    final private void stopFluctuator() {
        if (this.hasIconFluctuatorColor) {
            this.handler.removeCallbacks(this.fluctuatorRunnable);
            this.haloView.setVisibility(8);
            this.haloView.stop();
        }
    }
    
    public void bind(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a0) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "model");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "modelState");
        this.isOn = a.isOn;
        this.isEnabled = a.isEnabled;
        this.drawSwitch(this.isOn, this.isEnabled);
        if (a0.updateTitle) {
            String s = a.title;
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(s, "model.title");
            this.setTitle(s);
        }
        if (a0.updateSubTitle) {
            if (a.subTitle == null) {
                this.setSubTitle((String)null, false);
            } else {
                this.setSubTitle(a.subTitle, a.subTitleFormatted);
            }
        }
        if (a0.updateSubTitle2) {
            if (a.subTitle2 == null) {
                this.setSubTitle2((String)null, false);
            } else {
                this.setSubTitle2(a.subTitle2, a.subTitle2Formatted);
            }
        }
        this.textAnimationDisabled = a.noTextAnimation;
        this.iconScaleAnimationDisabled = a.noImageScaleAnimation;
        this.setIconFluctuatorColor(a.iconFluctuatorColor);
    }
    
    public void clearAnimation() {
        this.stopFluctuator();
        this.stopAnimation();
        this.currentState = null;
        this.layout.setVisibility(0);
        this.layout.setAlpha(1f);
    }
    
    public void copyAndPosition(android.widget.ImageView a, android.widget.TextView a0, android.widget.TextView a1, android.widget.TextView a2, boolean b) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "imageC");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "titleC");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a1, "subTitleC");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a2, "subTitle2C");
    }
    
    final public void drawSwitch(boolean b, boolean b0) {
        if (b0) {
            this.switchThumb.setBackgroundResource(R.drawable.switch_thumb_enabled);
        } else {
            this.switchThumb.setBackgroundResource(R.drawable.switch_thumb_disabled);
        }
        label1: {
            label0: {
                if (!b0) {
                    break label0;
                }
                if (!b) {
                    break label0;
                }
                this.switchBackground.setBackgroundResource(R.drawable.switch_button_off);
                android.graphics.drawable.Drawable a = this.switchBackground.getBackground();
                if (a == null) {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
                }
                android.graphics.drawable.Drawable a0 = ((android.graphics.drawable.LayerDrawable)a).getDrawable(1);
                if (a0 == null) {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type android.graphics.drawable.ClipDrawable");
                }
                ((android.graphics.drawable.ClipDrawable)a0).setLevel(5000);
                android.view.ViewGroup$LayoutParams a1 = this.switchThumb.getLayoutParams();
                if (a1 == null) {
                    throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
                }
                android.view.ViewGroup$MarginLayoutParams a2 = (android.view.ViewGroup$MarginLayoutParams)a1;
                a2.leftMargin = this.thumbMargin;
                this.switchThumb.setLayoutParams((android.view.ViewGroup$LayoutParams)a2);
                break label1;
            }
            this.switchBackground.setBackgroundResource(R.drawable.switch_button_off);
            android.graphics.drawable.Drawable a3 = this.switchBackground.getBackground();
            if (a3 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
            }
            android.graphics.drawable.Drawable a4 = ((android.graphics.drawable.LayerDrawable)a3).getDrawable(1);
            if (a4 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.graphics.drawable.ClipDrawable");
            }
            ((android.graphics.drawable.ClipDrawable)a4).setLevel(0);
            android.view.ViewGroup$LayoutParams a5 = this.switchThumb.getLayoutParams();
            if (a5 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            android.view.ViewGroup$MarginLayoutParams a6 = (android.view.ViewGroup$MarginLayoutParams)a5;
            a6.leftMargin = 0;
            this.switchThumb.setLayoutParams((android.view.ViewGroup$LayoutParams)a6);
        }
    }
    
    final protected android.animation.AnimatorSet$Builder getAnimatorSetBuilder() {
        return this.animatorSetBuilder;
    }
    
    final public int getFLUCTUATOR_OPACITY_ALPHA() {
        return this.FLUCTUATOR_OPACITY_ALPHA;
    }
    
    final public int getHALO_DELAY_START_DURATION() {
        return this.HALO_DELAY_START_DURATION;
    }
    
    final protected com.navdy.hud.app.ui.component.SwitchHaloView getHaloView() {
        return this.haloView;
    }
    
    final protected boolean getIconScaleAnimationDisabled() {
        return this.iconScaleAnimationDisabled;
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType getModelType() {
        return com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.SWITCH;
    }
    
    final protected android.widget.TextView getSubTitle() {
        return this.subTitle;
    }
    
    final protected android.widget.TextView getSubTitle2() {
        return this.subTitle2;
    }
    
    final protected android.view.View getSwitchBackground() {
        return this.switchBackground;
    }
    
    final protected android.view.ViewGroup getSwitchContainer() {
        return this.switchContainer;
    }
    
    final protected android.view.View getSwitchThumb() {
        return this.switchThumb;
    }
    
    final protected boolean getTextAnimationDisabled() {
        return this.textAnimationDisabled;
    }
    
    final protected android.view.ViewGroup getTextContainer() {
        return this.textContainer;
    }
    
    final protected android.widget.TextView getTitle() {
        return this.title;
    }
    
    public void preBind(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a0) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "model");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "modelState");
        this.isOn = a.isOn;
        this.isEnabled = a.isEnabled;
        this.drawSwitch(this.isOn, this.isEnabled);
        android.view.ViewGroup$LayoutParams a1 = this.title.getLayoutParams();
        if (a1 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        }
        ((android.view.ViewGroup$MarginLayoutParams)a1).topMargin = (int)a.fontInfo.titleFontTopMargin;
        this.title.setTextSize(a.fontInfo.titleFontSize);
        this.titleSelectedTopMargin = (float)(int)a.fontInfo.titleFontTopMargin;
        android.view.ViewGroup$LayoutParams a2 = this.subTitle.getLayoutParams();
        if (a2 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        }
        ((android.view.ViewGroup$MarginLayoutParams)a2).topMargin = (int)a.fontInfo.subTitleFontTopMargin;
        if (a.subTitle_2Lines) {
            this.subTitle.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.vlist_subtitle_2_line);
            this.subTitle.setSingleLine(false);
            this.subTitle.setMaxLines(2);
            this.subTitle.setEllipsize(android.text.TextUtils$TruncateAt.END);
        } else {
            this.subTitle.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.vlist_subtitle);
            this.subTitle.setSingleLine(true);
            this.subTitle.setEllipsize((android.text.TextUtils$TruncateAt)null);
        }
        this.subTitle.setTextSize(a.fontInfo.subTitleFontSize);
        android.view.ViewGroup$LayoutParams a3 = this.subTitle2.getLayoutParams();
        if (a3 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        }
        ((android.view.ViewGroup$MarginLayoutParams)a3).topMargin = (int)a.fontInfo.subTitle2FontTopMargin;
        this.subTitle2.setTextSize(a.fontInfo.subTitle2FontSize);
        this.titleUnselectedScale = a.fontInfo.titleScale;
    }
    
    public void select(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, int i, int i0) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "model");
        if (this.hasIconFluctuatorColor) {
            this.haloView.setVisibility(4);
            this.haloView.stop();
        }
        if (this.isEnabled) {
            android.animation.ValueAnimator a0 = null;
            android.animation.ValueAnimator a1 = null;
            android.animation.AnimatorSet a2 = new android.animation.AnimatorSet();
            android.util.Property a3 = android.view.View.SCALE_X;
            float[] a4 = new float[2];
            a4[0] = 1f;
            a4[1] = 0.8f;
            android.animation.PropertyValuesHolder a5 = android.animation.PropertyValuesHolder.ofFloat(a3, a4);
            android.util.Property a6 = android.view.View.SCALE_Y;
            float[] a7 = new float[2];
            a7[0] = 1f;
            a7[1] = 0.8f;
            android.animation.PropertyValuesHolder a8 = android.animation.PropertyValuesHolder.ofFloat(a6, a7);
            android.view.ViewGroup a9 = this.switchContainer;
            android.animation.PropertyValuesHolder[] a10 = new android.animation.PropertyValuesHolder[2];
            a10[0] = a5;
            a10[1] = a8;
            android.animation.ObjectAnimator a11 = android.animation.ObjectAnimator.ofPropertyValuesHolder(a9, a10);
            a11.setDuration((long)i0);
            android.util.Property a12 = android.view.View.SCALE_X;
            float[] a13 = new float[2];
            a13[0] = 0.8f;
            a13[1] = 1f;
            android.animation.PropertyValuesHolder a14 = android.animation.PropertyValuesHolder.ofFloat(a12, a13);
            android.util.Property a15 = android.view.View.SCALE_Y;
            float[] a16 = new float[2];
            a16[0] = 0.8f;
            a16[1] = 1f;
            android.animation.PropertyValuesHolder a17 = android.animation.PropertyValuesHolder.ofFloat(a15, a16);
            android.view.ViewGroup a18 = this.switchContainer;
            android.animation.PropertyValuesHolder[] a19 = new android.animation.PropertyValuesHolder[2];
            a19[0] = a14;
            a19[1] = a17;
            android.animation.ObjectAnimator a20 = android.animation.ObjectAnimator.ofPropertyValuesHolder(a18, a19);
            a20.setDuration((long)i0);
            android.animation.AnimatorSet a21 = new android.animation.AnimatorSet();
            android.animation.Animator[] a22 = new android.animation.Animator[2];
            a22[0] = a11;
            a22[1] = a20;
            a21.playSequentially(a22);
            android.animation.AnimatorSet a23 = new android.animation.AnimatorSet();
            android.graphics.drawable.Drawable a24 = this.switchBackground.getBackground();
            if (a24 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
            }
            android.graphics.drawable.Drawable a25 = ((android.graphics.drawable.LayerDrawable)a24).getDrawable(1);
            if (a25 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.graphics.drawable.ClipDrawable");
            }
            android.graphics.drawable.ClipDrawable a26 = (android.graphics.drawable.ClipDrawable)a25;
            android.view.ViewGroup$LayoutParams a27 = this.switchThumb.getLayoutParams();
            if (a27 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            android.view.ViewGroup$MarginLayoutParams a28 = (android.view.ViewGroup$MarginLayoutParams)a27;
            if (this.isOn) {
                float[] a29 = new float[2];
                a29[0] = (float)this.thumbMargin;
                a29[1] = 0.0f;
                a0 = android.animation.ValueAnimator.ofFloat(a29);
                a0.setDuration(100L);
                float[] a30 = new float[2];
                a30[0] = 5000f;
                a30[1] = 1000f;
                a1 = android.animation.ValueAnimator.ofFloat(a30);
                a21.setDuration(100L);
            } else {
                float[] a31 = new float[2];
                a31[0] = 0.0f;
                a31[1] = (float)this.thumbMargin;
                a0 = android.animation.ValueAnimator.ofFloat(a31);
                a0.setDuration(100L);
                float[] a32 = new float[2];
                a32[0] = 3000f;
                a32[1] = 7000f;
                a1 = android.animation.ValueAnimator.ofFloat(a32);
                a21.setDuration(100L);
            }
            if (a1 != null) {
                a1.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$select$1(this, a26));
            }
            if (a0 != null) {
                a0.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$select$2(this, a28));
            }
            android.animation.Animator[] a33 = new android.animation.Animator[2];
            a33[0] = a0;
            a33[1] = a1;
            a23.playTogether(a33);
            a2.play((android.animation.Animator)a23).after((android.animation.Animator)a21);
            a2.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$select$3(this, a, i));
            a2.start();
        } else {
            this.vlist.unlock();
        }
    }
    
    final protected void setAnimatorSetBuilder(android.animation.AnimatorSet$Builder a) {
        this.animatorSetBuilder = a;
    }
    
    final protected void setHaloView(com.navdy.hud.app.ui.component.SwitchHaloView a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.haloView = a;
    }
    
    final protected void setIconScaleAnimationDisabled(boolean b) {
        this.iconScaleAnimationDisabled = b;
    }
    
    public void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State a, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType a0, int i, boolean b) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "state");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "animation");
        this.animatorSetBuilder = null;
        boolean b0 = this.textAnimationDisabled;
        label2: {
            float f = 0.0f;
            float f0 = 0.0f;
            float f1 = 0.0f;
            float f2 = 0.0f;
            float f3 = 0.0f;
            float f4 = 0.0f;
            float f5 = 0.0f;
            label1: {
                if (!b0) {
                    break label1;
                }
                boolean b1 = kotlin.jvm.internal.Intrinsics.areEqual(a0, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.MOVE);
                label0: {
                    if (b1) {
                        break label0;
                    }
                    a = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED;
                    break label1;
                }
                this.itemAnimatorSet = new android.animation.AnimatorSet();
                android.animation.AnimatorSet a1 = this.itemAnimatorSet;
                float[] a2 = new float[2];
                a2[0] = 0.0f;
                a2[1] = 1f;
                this.animatorSetBuilder = a1.play((android.animation.Animator)android.animation.ValueAnimator.ofFloat(a2));
                break label2;
            }
            switch(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$WhenMappings.$EnumSwitchMapping$0[a.ordinal()]) {
                case 2: {
                    f = this.titleUnselectedScale;
                    f0 = this.titleUnselectedScale;
                    f1 = this.titleUnselectedScale;
                    f2 = 0.6f;
                    f3 = 0.0f;
                    f4 = 0.0f;
                    f5 = 0.0f;
                    break;
                }
                case 1: {
                    f3 = this.titleSelectedTopMargin;
                    f2 = 1f;
                    f = 1f;
                    f4 = 1f;
                    f0 = 1f;
                    f5 = 1f;
                    f1 = 1f;
                    break;
                }
                default: {
                    f2 = 0.0f;
                    f = 0.0f;
                    f3 = 0.0f;
                    f4 = 0.0f;
                    f0 = 0.0f;
                    f5 = 0.0f;
                    f1 = 0.0f;
                }
            }
            if (!this.hasSubTitle) {
                f3 = 0.0f;
                f4 = 0.0f;
                f5 = 0.0f;
            }
            switch(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$WhenMappings.$EnumSwitchMapping$1[a0.ordinal()]) {
                case 3: {
                    this.itemAnimatorSet = new android.animation.AnimatorSet();
                    android.util.Property a3 = android.view.View.SCALE_X;
                    float[] a4 = new float[1];
                    a4[0] = f2;
                    android.animation.PropertyValuesHolder a5 = android.animation.PropertyValuesHolder.ofFloat(a3, a4);
                    android.util.Property a6 = android.view.View.SCALE_Y;
                    float[] a7 = new float[1];
                    a7[0] = f2;
                    android.animation.PropertyValuesHolder a8 = android.animation.PropertyValuesHolder.ofFloat(a6, a7);
                    android.animation.AnimatorSet a9 = this.itemAnimatorSet;
                    android.widget.TextView a10 = this.title;
                    android.animation.PropertyValuesHolder[] a11 = new android.animation.PropertyValuesHolder[2];
                    a11[0] = a5;
                    a11[1] = a8;
                    this.animatorSetBuilder = a9.play((android.animation.Animator)android.animation.ObjectAnimator.ofPropertyValuesHolder(a10, a11));
                    android.animation.AnimatorSet$Builder a12 = this.animatorSetBuilder;
                    if (a12 == null) {
                        kotlin.jvm.internal.Intrinsics.throwNpe();
                    }
                    android.view.ViewGroup a13 = this.switchContainer;
                    android.animation.PropertyValuesHolder[] a14 = new android.animation.PropertyValuesHolder[2];
                    a14[0] = a5;
                    a14[1] = a8;
                    a12.with((android.animation.Animator)android.animation.ObjectAnimator.ofPropertyValuesHolder(a13, a14));
                    break;
                }
                case 1: case 2: {
                    this.switchContainer.setScaleX(f2);
                    this.switchContainer.setScaleY(f2);
                    break;
                }
            }
            this.title.setPivotX(0.0f);
            this.title.setPivotY(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.titleHeight / (float)2);
            switch(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$WhenMappings.$EnumSwitchMapping$2[a0.ordinal()]) {
                case 3: {
                    android.util.Property a15 = android.view.View.SCALE_X;
                    float[] a16 = new float[1];
                    a16[0] = f;
                    android.animation.PropertyValuesHolder a17 = android.animation.PropertyValuesHolder.ofFloat(a15, a16);
                    android.util.Property a18 = android.view.View.SCALE_Y;
                    float[] a19 = new float[1];
                    a19[0] = f;
                    android.animation.PropertyValuesHolder a20 = android.animation.PropertyValuesHolder.ofFloat(a18, a19);
                    android.animation.AnimatorSet$Builder a21 = this.animatorSetBuilder;
                    if (a21 == null) {
                        kotlin.jvm.internal.Intrinsics.throwNpe();
                    }
                    android.widget.TextView a22 = this.title;
                    android.animation.PropertyValuesHolder[] a23 = new android.animation.PropertyValuesHolder[2];
                    a23[0] = a17;
                    a23[1] = a20;
                    a21.with((android.animation.Animator)android.animation.ObjectAnimator.ofPropertyValuesHolder(a22, a23));
                    android.animation.AnimatorSet$Builder a24 = this.animatorSetBuilder;
                    if (a24 == null) {
                        kotlin.jvm.internal.Intrinsics.throwNpe();
                    }
                    a24.with(com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.animateMargin((android.view.View)this.title, (int)f3));
                    break;
                }
                case 1: case 2: {
                    this.title.setScaleX(f);
                    this.title.setScaleY(f);
                    android.view.ViewGroup$LayoutParams a25 = this.title.getLayoutParams();
                    if (a25 == null) {
                        throw new kotlin.TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
                    }
                    ((android.view.ViewGroup$MarginLayoutParams)a25).topMargin = (int)f3;
                    this.title.requestLayout();
                    break;
                }
            }
            this.subTitle.setPivotX(0.0f);
            this.subTitle.setPivotY(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.subTitleHeight / (float)2);
            com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType a26 = (this.hasSubTitle) ? a0 : com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.NONE;
            switch(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$WhenMappings.$EnumSwitchMapping$3[a26.ordinal()]) {
                case 3: {
                    android.util.Property a27 = android.view.View.SCALE_X;
                    float[] a28 = new float[1];
                    a28[0] = f0;
                    android.animation.PropertyValuesHolder a29 = android.animation.PropertyValuesHolder.ofFloat(a27, a28);
                    android.util.Property a30 = android.view.View.SCALE_Y;
                    float[] a31 = new float[1];
                    a31[0] = f0;
                    android.animation.PropertyValuesHolder a32 = android.animation.PropertyValuesHolder.ofFloat(a30, a31);
                    android.util.Property a33 = android.view.View.ALPHA;
                    float[] a34 = new float[1];
                    a34[0] = f4;
                    android.animation.PropertyValuesHolder a35 = android.animation.PropertyValuesHolder.ofFloat(a33, a34);
                    android.animation.AnimatorSet$Builder a36 = this.animatorSetBuilder;
                    if (a36 == null) {
                        kotlin.jvm.internal.Intrinsics.throwNpe();
                    }
                    android.widget.TextView a37 = this.subTitle;
                    android.animation.PropertyValuesHolder[] a38 = new android.animation.PropertyValuesHolder[3];
                    a38[0] = a29;
                    a38[1] = a32;
                    a38[2] = a35;
                    a36.with((android.animation.Animator)android.animation.ObjectAnimator.ofPropertyValuesHolder(a37, a38));
                    break;
                }
                case 1: case 2: {
                    this.subTitle.setAlpha(f4);
                    this.subTitle.setScaleX(f0);
                    this.subTitle.setScaleY(f0);
                    break;
                }
            }
            this.subTitle2.setPivotX(0.0f);
            this.subTitle2.setPivotY(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.subTitleHeight / (float)2);
            if (!this.hasSubTitle2) {
                a0 = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.NONE;
            }
            switch(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder$WhenMappings.$EnumSwitchMapping$4[a0.ordinal()]) {
                case 3: {
                    android.util.Property a39 = android.view.View.SCALE_X;
                    float[] a40 = new float[1];
                    a40[0] = f1;
                    android.animation.PropertyValuesHolder a41 = android.animation.PropertyValuesHolder.ofFloat(a39, a40);
                    android.util.Property a42 = android.view.View.SCALE_Y;
                    float[] a43 = new float[1];
                    a43[0] = f1;
                    android.animation.PropertyValuesHolder a44 = android.animation.PropertyValuesHolder.ofFloat(a42, a43);
                    android.util.Property a45 = android.view.View.ALPHA;
                    float[] a46 = new float[1];
                    a46[0] = f4;
                    android.animation.PropertyValuesHolder a47 = android.animation.PropertyValuesHolder.ofFloat(a45, a46);
                    android.animation.AnimatorSet$Builder a48 = this.animatorSetBuilder;
                    if (a48 == null) {
                        kotlin.jvm.internal.Intrinsics.throwNpe();
                    }
                    android.widget.TextView a49 = this.subTitle2;
                    android.animation.PropertyValuesHolder[] a50 = new android.animation.PropertyValuesHolder[3];
                    a50[0] = a41;
                    a50[1] = a44;
                    a50[2] = a47;
                    a48.with((android.animation.Animator)android.animation.ObjectAnimator.ofPropertyValuesHolder(a49, a50));
                    break;
                }
                case 1: case 2: {
                    this.subTitle2.setAlpha(f5);
                    this.subTitle2.setScaleX(f1);
                    this.subTitle2.setScaleY(f1);
                    break;
                }
            }
            if (kotlin.jvm.internal.Intrinsics.areEqual(a, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED) && b) {
                if (this.itemAnimatorSet == null) {
                    this.startFluctuator();
                } else {
                    this.itemAnimatorSet.addListener((android.animation.Animator$AnimatorListener)this.fluctuatorStartListener);
                }
            }
        }
    }
    
    final protected void setSubTitle(android.widget.TextView a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.subTitle = a;
    }
    
    final protected void setSubTitle2(android.widget.TextView a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.subTitle2 = a;
    }
    
    final protected void setSwitchBackground(android.view.View a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.switchBackground = a;
    }
    
    final protected void setSwitchContainer(android.view.ViewGroup a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.switchContainer = a;
    }
    
    final protected void setSwitchThumb(android.view.View a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.switchThumb = a;
    }
    
    final protected void setTextAnimationDisabled(boolean b) {
        this.textAnimationDisabled = b;
    }
    
    final protected void setTextContainer(android.view.ViewGroup a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.textContainer = a;
    }
    
    final protected void setTitle(android.widget.TextView a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.title = a;
    }
    
    public void startFluctuator() {
        if (this.hasIconFluctuatorColor) {
            this.haloView.setVisibility(0);
            this.haloView.start();
        }
    }
}
