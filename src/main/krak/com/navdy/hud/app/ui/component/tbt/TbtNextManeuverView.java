package com.navdy.hud.app.ui.component.tbt;
import com.navdy.hud.app.R;

final public class TbtNextManeuverView extends android.widget.LinearLayout {
    final public static com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView$Companion Companion;
    final private static com.navdy.service.library.log.Logger logger;
    private java.util.HashMap _$_findViewCache;
    private com.navdy.hud.app.view.MainView$CustomAnimationMode currentMode;
    private com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState lastManeuverState;
    private int lastNextTurnIconId;
    private android.widget.ImageView nextManeuverIcon;
    private android.widget.TextView nextManeuverText;
    
    static {
        Companion = new com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView$Companion((kotlin.jvm.internal.DefaultConstructorMarker)null);
        logger = new com.navdy.service.library.log.Logger("TbtNextManeuverView");
    }
    
    public TbtNextManeuverView(android.content.Context a) {
        super(a);
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        this.lastNextTurnIconId = -1;
    }
    
    public TbtNextManeuverView(android.content.Context a, android.util.AttributeSet a0) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "attrs");
        super(a, a0);
        this.lastNextTurnIconId = -1;
    }
    
    public TbtNextManeuverView(android.content.Context a, android.util.AttributeSet a0, int i) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "attrs");
        super(a, a0, i);
        this.lastNextTurnIconId = -1;
    }
    
    final public static com.navdy.service.library.log.Logger access$getLogger$cp() {
        return logger;
    }
    
    final private void updateIcon() {
        if (this.lastNextTurnIconId == -1) {
            android.widget.ImageView a = this.nextManeuverIcon;
            if (a != null) {
                a.setImageDrawable((android.graphics.drawable.Drawable)null);
            }
            this.setVisibility(8);
        } else {
            android.widget.ImageView a0 = this.nextManeuverIcon;
            if (a0 != null) {
                a0.setImageResource(this.lastNextTurnIconId);
            }
            if (kotlin.jvm.internal.Intrinsics.areEqual(this.currentMode, com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT)) {
                this.setVisibility(8);
            } else {
                this.setVisibility(0);
            }
        }
    }
    
    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }
    
    public android.view.View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new java.util.HashMap();
        }
        android.view.View a = (android.view.View)this._$_findViewCache.get(Integer.valueOf(i));
        if (a == null) {
            a = this.findViewById(i);
            this._$_findViewCache.put(Integer.valueOf(i), a);
        }
        return a;
    }
    
    final public void clear() {
        this.lastNextTurnIconId = -1;
        android.widget.ImageView a = this.nextManeuverIcon;
        if (a != null) {
            a.setImageDrawable((android.graphics.drawable.Drawable)null);
        }
        this.setVisibility(8);
    }
    
    final public void getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode a, android.animation.AnimatorSet$Builder a0) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "mode");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "mainBuilder");
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        android.view.View a = this.findViewById(R.id.nextManeuverText);
        if (a == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        this.nextManeuverText = (android.widget.TextView)a;
        android.view.View a0 = this.findViewById(R.id.nextManeuverIcon);
        if (a0 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type android.widget.ImageView");
        }
        this.nextManeuverIcon = (android.widget.ImageView)a0;
    }
    
    final public void setMode(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "mode");
        if (!kotlin.jvm.internal.Intrinsics.areEqual(this.currentMode, a)) {
            this.currentMode = a;
            if (kotlin.jvm.internal.Intrinsics.areEqual(a, com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND)) {
                this.updateIcon();
            } else {
                this.setVisibility(8);
            }
        }
    }
    
    final public void updateDisplay(com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "event");
        if (this.lastNextTurnIconId != a.nextTurnIconId) {
            this.lastNextTurnIconId = a.nextTurnIconId;
            this.lastManeuverState = a.maneuverState;
            this.updateIcon();
        }
    }
}
