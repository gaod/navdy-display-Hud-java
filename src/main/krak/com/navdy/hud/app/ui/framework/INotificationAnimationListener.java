package com.navdy.hud.app.ui.framework;

abstract public interface INotificationAnimationListener {
    abstract public void onStart(String arg, com.navdy.hud.app.framework.notifications.NotificationType arg0, com.navdy.hud.app.ui.framework.UIStateManager$Mode arg1);
    
    
    abstract public void onStop(String arg, com.navdy.hud.app.framework.notifications.NotificationType arg0, com.navdy.hud.app.ui.framework.UIStateManager$Mode arg1);
}
