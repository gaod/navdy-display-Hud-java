package com.navdy.hud.app.ui.component.mainmenu;

final public class MainMenuScreen2$Presenter$$InjectAdapter extends dagger.internal.Binding implements javax.inject.Provider, dagger.MembersInjector {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding sharedPreferences;
    private dagger.internal.Binding supertype;
    private dagger.internal.Binding tripManager;
    
    public MainMenuScreen2$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter", "members/com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter", true, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter.class, (this).getClass().getClassLoader());
        this.tripManager = a.requestBinding("com.navdy.hud.app.framework.trips.TripManager", com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter.class, (this).getClass().getClassLoader());
        this.sharedPreferences = a.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter.class, (this).getClass().getClassLoader());
        this.supertype = a.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter.class, (this).getClass().getClassLoader(), false, true);
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter get() {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a = new com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter();
        this.injectMembers(a);
        return a;
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.bus);
        a0.add(this.tripManager);
        a0.add(this.sharedPreferences);
        a0.add(this.supertype);
    }
    
    public void injectMembers(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a) {
        a.bus = (com.squareup.otto.Bus)this.bus.get();
        a.tripManager = (com.navdy.hud.app.framework.trips.TripManager)this.tripManager.get();
        a.sharedPreferences = (android.content.SharedPreferences)this.sharedPreferences.get();
        this.supertype.injectMembers(a);
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter)a);
    }
}
