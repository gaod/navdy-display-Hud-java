package com.navdy.hud.app.ui.component.dashboard;

abstract public interface IDashboardOptionsAdapter {
    final public static int LEFT = 0;
    final public static String PREFERENCE_LEFT_GAUGE_ID = "PREFERENCE_LEFT_GAUGE";
    final public static String PREFERENCE_MIDDLE_GAUGE = "PREFERENCE_MIDDLE_GAUGE";
    final public static String PREFERENCE_RIGHT_GAUGE_ID = "PREFERENCE_RIGHT_GAUGE";
    final public static String PREFERENCE_SCROLLABLE_SIDE = "PREFERENCE_SCROLLABLE_SIDE";
    final public static int RIGHT = 1;
    final public static int SPEEDOMETER_GAUGE = 1;
    final public static int TACHOMETER_GAUGE = 0;
    
    abstract public int getCurrentScrollableSideOption();
    
    
    abstract public int getMiddleGaugeOption();
    
    
    abstract public void onScrollableSideOptionSelected();
    
    
    abstract public void onSpeedoMeterSelected();
    
    
    abstract public void onTachoMeterSelected();
}
