package com.navdy.hud.app.ui.component.homescreen;

final class HomeScreenUtils$2 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final android.widget.ProgressBar val$progressBar;
    
    HomeScreenUtils$2(android.widget.ProgressBar a) {
        super();
        this.val$progressBar = a;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        int i = ((Integer)a.getAnimatedValue()).intValue();
        this.val$progressBar.setProgress(i);
    }
}
