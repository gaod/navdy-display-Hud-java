package com.navdy.hud.app.ui.component.vlist.viewholder;

class IconOptionsViewHolder$3 implements Runnable {
    final com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder this$0;
    final int val$pos;
    
    IconOptionsViewHolder$3(com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder a, int i) {
        super();
        this.this$0 = a;
        this.val$pos = i;
    }
    
    public void run() {
        com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a = this.this$0.vlist.getItemSelectionState();
        a.set(com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.access$200(this.this$0), com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.access$200(this.this$0).id, this.val$pos, (com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.access$200(this.this$0).iconIds[com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.access$300(this.this$0)] != 0) ? 1 : 0, com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.access$300(this.this$0));
        this.this$0.vlist.performSelectAction(a);
    }
}
