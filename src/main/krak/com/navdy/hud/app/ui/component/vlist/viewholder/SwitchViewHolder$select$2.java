package com.navdy.hud.app.ui.component.vlist.viewholder;

final public class SwitchViewHolder$select$2 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final android.view.ViewGroup$MarginLayoutParams $marginLayoutParamas;
    final com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder this$0;
    
    SwitchViewHolder$select$2(com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder a, android.view.ViewGroup$MarginLayoutParams a0) {
        super();
        this.this$0 = a;
        this.$marginLayoutParamas = a0;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        android.view.ViewGroup$MarginLayoutParams a0 = this.$marginLayoutParamas;
        Object a1 = (a == null) ? null : a.getAnimatedValue();
        if (a1 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Float");
        }
        a0.leftMargin = (int)((Float)a1).floatValue();
        this.this$0.getSwitchThumb().setLayoutParams((android.view.ViewGroup$LayoutParams)this.$marginLayoutParamas);
    }
}
