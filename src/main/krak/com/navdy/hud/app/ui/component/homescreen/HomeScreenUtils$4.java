package com.navdy.hud.app.ui.component.homescreen;

final class HomeScreenUtils$4 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final android.view.View val$view;
    
    HomeScreenUtils$4(android.view.View a) {
        super();
        this.val$view = a;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        int i = ((Integer)a.getAnimatedValue()).intValue();
        ((android.view.ViewGroup$MarginLayoutParams)this.val$view.getLayoutParams()).leftMargin = i;
        this.val$view.invalidate();
        this.val$view.requestLayout();
    }
}
