package com.navdy.hud.app.ui.component.mainmenu;

final public class DashGaugeConfiguratorMenu$2 {
    final com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu this$0;
    
    DashGaugeConfiguratorMenu$2(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu a) {
        super();
        this.this$0 = a;
    }
    
    final public void onReload(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$Reload a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "reload");
        switch(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$WhenMappings.$EnumSwitchMapping$0[a.ordinal()]) {
            case 2: {
                this.this$0.setSmartDashWidgetCache(this.this$0.getSmartDashWidgetManager().buildSmartDashWidgetCache(0));
                com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.access$getPresenter$p(this.this$0).updateCurrentMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.this$0);
                break;
            }
            case 1: {
                com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.access$getPresenter$p(this.this$0).updateCurrentMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.this$0);
                break;
            }
        }
    }
}
