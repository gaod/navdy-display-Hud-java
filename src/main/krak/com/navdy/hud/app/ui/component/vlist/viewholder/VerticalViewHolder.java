package com.navdy.hud.app.ui.component.vlist.viewholder;
import com.navdy.hud.app.R;

abstract public class VerticalViewHolder extends android.support.v7.widget.RecyclerView$ViewHolder {
    final public static String EMPTY = "";
    final public static int NO_COLOR = 0;
    final public static int iconMargin;
    final public static int listItemHeight;
    final public static int mainIconSize;
    final public static int rootTopOffset;
    final static com.navdy.service.library.log.Logger sLogger;
    final public static int scrollDistance;
    final public static int selectedIconSize;
    final public static int selectedImageX;
    final public static int selectedImageY;
    final public static int selectedTextX;
    final public static int subTitle2Color;
    final public static int subTitleColor;
    final public static float subTitleHeight;
    final public static int textLeftMargin;
    final public static float titleHeight;
    final public static int unselectedIconSize;
    protected com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State currentState;
    protected java.util.HashMap extras;
    protected android.os.Handler handler;
    protected android.view.animation.Interpolator interpolator;
    protected android.animation.AnimatorSet itemAnimatorSet;
    public android.view.ViewGroup layout;
    protected int pos;
    protected com.navdy.hud.app.ui.component.vlist.VerticalList vlist;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.class);
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        selectedIconSize = a.getDimensionPixelSize(R.dimen.vlist_image);
        unselectedIconSize = a.getDimensionPixelSize(R.dimen.vlist_small_image);
        selectedImageX = a.getDimensionPixelSize(R.dimen.vmenu_sel_image_x);
        selectedImageY = a.getDimensionPixelSize(R.dimen.vmenu_sel_image_y);
        mainIconSize = a.getDimensionPixelSize(R.dimen.vlist_image_frame);
        textLeftMargin = a.getDimensionPixelSize(R.dimen.vlist_text_left_margin);
        listItemHeight = a.getDimensionPixelSize(R.dimen.vlist_item_height);
        titleHeight = a.getDimension(R.dimen.vlist_title);
        subTitleHeight = a.getDimension(R.dimen.vlist_subtitle);
        selectedTextX = a.getDimensionPixelSize(R.dimen.vmenu_sel_text_x);
        rootTopOffset = a.getDimensionPixelSize(R.dimen.vmenu_root_top_offset);
        iconMargin = a.getDimensionPixelOffset(R.dimen.vlist_icon_list_margin);
        subTitleColor = a.getColor(R.color.vlist_subtitle);
        subTitle2Color = a.getColor(R.color.vlist_subtitle);
        scrollDistance = listItemHeight;
    }
    
    public VerticalViewHolder(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, android.os.Handler a1) {
        super((android.view.View)a);
        this.pos = -1;
        this.interpolator = com.navdy.hud.app.ui.component.vlist.VerticalList.getInterpolator();
        this.layout = a;
        this.vlist = a0;
        this.handler = a1;
    }
    
    abstract public void bind(com.navdy.hud.app.ui.component.vlist.VerticalList$Model arg, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState arg0);
    
    
    abstract public void clearAnimation();
    
    
    abstract public void copyAndPosition(android.widget.ImageView arg, android.widget.TextView arg0, android.widget.TextView arg1, android.widget.TextView arg2, boolean arg3);
    
    
    abstract public com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType getModelType();
    
    
    public int getPos() {
        return this.pos;
    }
    
    public com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State getState() {
        return this.currentState;
    }
    
    public boolean handleKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return false;
    }
    
    public boolean hasToolTip() {
        return false;
    }
    
    public void preBind(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a0) {
    }
    
    abstract public void select(com.navdy.hud.app.ui.component.vlist.VerticalList$Model arg, int arg0, int arg1);
    
    
    public void setExtras(java.util.HashMap a) {
        this.extras = a;
    }
    
    abstract public void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State arg, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType arg0, int arg1, boolean arg2);
    
    
    public void setPos(int i) {
        this.pos = i;
        this.currentState = null;
    }
    
    final public void setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State a, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType a0, int i) {
        this.setState(a, a0, i, true);
    }
    
    final public void setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State a, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType a0, int i, boolean b) {
        if (!(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$1.$SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$ModelType[this.getModelType().ordinal()] != 0)) {
            if (sLogger.isLoggable(2)) {
                sLogger.v(new StringBuilder().append("setState {").append(this.pos).append(", ").append(a).append("} animate=").append(a0).append(" dur=").append(i).append(" current-raw {").append(this.vlist.getRawPosition()).append("} vh-id{").append(System.identityHashCode(this)).append("}").toString());
            }
            if (this.currentState != a) {
                this.clearAnimation();
                this.currentState = a;
                if (a != com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED) {
                    this.vlist.selectedList.remove(Integer.valueOf(this.pos));
                } else {
                    this.vlist.selectedList.add(Integer.valueOf(this.pos));
                }
                this.setItemState(a, a0, i, b);
                if (this.itemAnimatorSet != null) {
                    this.itemAnimatorSet.setDuration((long)i);
                    this.itemAnimatorSet.setInterpolator((android.animation.TimeInterpolator)this.interpolator);
                    this.itemAnimatorSet.start();
                }
            } else if (a != com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED) {
                this.vlist.selectedList.remove(Integer.valueOf(this.pos));
            } else {
                this.vlist.selectedList.add(Integer.valueOf(this.pos));
            }
        }
    }
    
    public void startFluctuator() {
    }
    
    public void stopAnimation() {
        if (this.itemAnimatorSet != null && this.itemAnimatorSet.isRunning()) {
            this.itemAnimatorSet.removeAllListeners();
            this.itemAnimatorSet.cancel();
        }
        this.itemAnimatorSet = null;
    }
}
