package com.navdy.hud.app.ui.component.homescreen;

class HomeScreenView$5 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.homescreen.HomeScreenView this$0;
    
    HomeScreenView$5(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        if (this.this$0.isNavigationActive()) {
            if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().hasArrived()) {
                if (!com.navdy.hud.app.ui.component.homescreen.HomeScreenView.access$200(this.this$0)) {
                    if (this.this$0.getDisplayMode() == com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP) {
                        this.this$0.timeContainer.setVisibility(0);
                    }
                    this.this$0.timeContainer.setAlpha(1f);
                }
                this.this$0.activeEtaContainer.setVisibility(8);
            } else if (com.navdy.hud.app.ui.component.homescreen.HomeScreenView.access$300(this.this$0) == com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP) {
                this.this$0.activeEtaContainer.setVisibility(0);
                this.this$0.activeEtaContainer.setAlpha(1f);
                this.this$0.timeContainer.setVisibility(8);
            }
        }
    }
}
