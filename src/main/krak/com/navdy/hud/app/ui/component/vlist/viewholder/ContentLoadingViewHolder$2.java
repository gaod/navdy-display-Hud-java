package com.navdy.hud.app.ui.component.vlist.viewholder;

class ContentLoadingViewHolder$2 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder this$0;
    
    ContentLoadingViewHolder$2(com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.access$000(this.this$0).setVisibility(8);
    }
    
    public void onAnimationStart(android.animation.Animator a) {
        com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.access$100(this.this$0).setVisibility(0);
        if (com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.access$200(this.this$0) != null && com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.access$200(this.this$0).isRunning()) {
            com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.access$200(this.this$0).stop();
        }
    }
}
