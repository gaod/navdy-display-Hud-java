package com.navdy.hud.app.ui.component.mainmenu;

final class DashGaugeConfiguratorMenu$1 implements com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$IWidgetFilter {
    final public static com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$1 INSTANCE;
    
    static {
        INSTANCE = new com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$1();
    }
    
    DashGaugeConfiguratorMenu$1() {
    }
    
    final public boolean filter(String s) {
        boolean b = false;
        label0: {
            label1: {
                if (s == null) {
                    break label1;
                }
                switch(s.hashCode()) {
                    case 1872543020: {
                        if (!s.equals("TRAFFIC_INCIDENT_GAUGE_ID")) {
                            break;
                        }
                        b = !com.navdy.hud.app.maps.MapSettings.isTrafficDashWidgetsEnabled();
                        break label0;
                    }
                    case 811991446: {
                        if (!s.equals("EMPTY_WIDGET")) {
                            break;
                        }
                        b = true;
                        break label0;
                    }
                }
            }
            b = false;
        }
        return b;
    }
}
