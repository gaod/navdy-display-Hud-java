package com.navdy.hud.app.ui.component.carousel;

class CircleIndicator$2 implements android.view.ViewTreeObserver$OnGlobalLayoutListener {
    final com.navdy.hud.app.ui.component.carousel.CircleIndicator this$0;
    final android.view.View val$view;
    
    CircleIndicator$2(com.navdy.hud.app.ui.component.carousel.CircleIndicator a, android.view.View a0) {
        super();
        this.this$0 = a;
        this.val$view = a0;
    }
    
    public void onGlobalLayout() {
        if (((com.navdy.hud.app.ui.component.carousel.CircleIndicator.access$100(this.this$0) != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) ? this.val$view.getTop() : this.val$view.getLeft()) != 0) {
            this.val$view.getViewTreeObserver().removeOnGlobalLayoutListener((android.view.ViewTreeObserver$OnGlobalLayoutListener)this);
            com.navdy.hud.app.ui.component.carousel.CircleIndicator.access$200(this.this$0, this.val$view);
        }
    }
}
