package com.navdy.hud.app.util;


public enum FeatureUtil$Feature {
    GESTURE_ENGINE(0),
    GESTURE_COLLECTOR(1),
    FUEL_ROUTING(2),
    GESTURE_PROGRESS(3);

    private int value;
    FeatureUtil$Feature(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class FeatureUtil$Feature extends Enum {
//    final private static com.navdy.hud.app.util.FeatureUtil$Feature[] $VALUES;
//    final public static com.navdy.hud.app.util.FeatureUtil$Feature FUEL_ROUTING;
//    final public static com.navdy.hud.app.util.FeatureUtil$Feature GESTURE_COLLECTOR;
//    final public static com.navdy.hud.app.util.FeatureUtil$Feature GESTURE_ENGINE;
//    final public static com.navdy.hud.app.util.FeatureUtil$Feature GESTURE_PROGRESS;
//    
//    static {
//        GESTURE_ENGINE = new com.navdy.hud.app.util.FeatureUtil$Feature("GESTURE_ENGINE", 0);
//        GESTURE_COLLECTOR = new com.navdy.hud.app.util.FeatureUtil$Feature("GESTURE_COLLECTOR", 1);
//        FUEL_ROUTING = new com.navdy.hud.app.util.FeatureUtil$Feature("FUEL_ROUTING", 2);
//        GESTURE_PROGRESS = new com.navdy.hud.app.util.FeatureUtil$Feature("GESTURE_PROGRESS", 3);
//        com.navdy.hud.app.util.FeatureUtil$Feature[] a = new com.navdy.hud.app.util.FeatureUtil$Feature[4];
//        a[0] = GESTURE_ENGINE;
//        a[1] = GESTURE_COLLECTOR;
//        a[2] = FUEL_ROUTING;
//        a[3] = GESTURE_PROGRESS;
//        $VALUES = a;
//    }
//    
//    private FeatureUtil$Feature(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.util.FeatureUtil$Feature valueOf(String s) {
//        return (com.navdy.hud.app.util.FeatureUtil$Feature)Enum.valueOf(com.navdy.hud.app.util.FeatureUtil$Feature.class, s);
//    }
//    
//    public static com.navdy.hud.app.util.FeatureUtil$Feature[] values() {
//        return $VALUES.clone();
//    }
//}
//