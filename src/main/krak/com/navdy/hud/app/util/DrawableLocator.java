package com.navdy.hud.app.util;

public class DrawableLocator {
    private android.graphics.drawable.Drawable mDrawable;
    private android.view.View mParent;
    private int mResourceId;
    
    public DrawableLocator(android.view.View a, android.content.res.TypedArray a0, int i, int i0) {
        this.mResourceId = 0;
        this.mParent = a;
        if (a.isInEditMode()) {
            this.mDrawable = a0.getDrawable(i);
        } else {
            this.mResourceId = a0.getResourceId(i, -1);
            if (this.mResourceId == -1) {
                this.mResourceId = i0;
            }
        }
    }
    
    public static android.graphics.Bitmap drawableToBitmap(android.graphics.drawable.Drawable a) {
        android.graphics.Bitmap a0 = null;
        if (a instanceof android.graphics.drawable.BitmapDrawable) {
            a0 = ((android.graphics.drawable.BitmapDrawable)a).getBitmap();
        } else {
            a0 = android.graphics.Bitmap.createBitmap(a.getIntrinsicWidth(), a.getIntrinsicHeight(), android.graphics.Bitmap.Config.ARGB_8888);
            android.graphics.Canvas a1 = new android.graphics.Canvas(a0);
            a.setBounds(0, 0, a1.getWidth(), a1.getHeight());
            a.draw(a1);
        }
        return a0;
    }
    
    public android.graphics.Bitmap getBitmap() {
        return (this.mDrawable == null) ? (this.mResourceId != 0) ? android.graphics.BitmapFactory.decodeResource(this.mParent.getResources(), this.mResourceId) : null : com.navdy.hud.app.util.DrawableLocator.drawableToBitmap(this.mDrawable);
    }
    
    public boolean isSet() {
        boolean b = false;
        android.graphics.drawable.Drawable a = this.mDrawable;
        label2: {
            label0: {
                label1: {
                    if (a != null) {
                        break label1;
                    }
                    if (this.mResourceId == 0) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
}
