package com.navdy.hud.app.util;

public class OTAUpdateService$OTAFailedException extends Exception {
    public String last_install;
    public String last_log;
    
    OTAUpdateService$OTAFailedException(String s, String s0) {
        super("OTA installation failed");
        this.last_install = s;
        this.last_log = s0;
    }
}
