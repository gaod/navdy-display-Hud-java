package com.navdy.hud.app.util;

class ReportIssueService$1 implements com.navdy.service.library.network.http.services.JiraClient$ResultCallback {
    final com.navdy.hud.app.util.ReportIssueService this$0;
    final java.io.File val$fileToSync;
    
    ReportIssueService$1(com.navdy.hud.app.util.ReportIssueService a, java.io.File a0) {
        super();
        this.this$0 = a;
        this.val$fileToSync = a0;
    }
    
    public void onError(Throwable a) {
        com.navdy.hud.app.util.ReportIssueService.access$000().e("onError, error uploading the files ", a);
        com.navdy.hud.app.util.ReportIssueService.access$200(this.this$0);
    }
    
    public void onSuccess(Object a) {
        com.navdy.hud.app.util.ReportIssueService.access$000().d("Files attached successfully");
        com.navdy.hud.app.util.ReportIssueService.access$100(this.this$0, this.val$fileToSync);
    }
}
