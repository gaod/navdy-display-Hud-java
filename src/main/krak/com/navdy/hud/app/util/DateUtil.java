package com.navdy.hud.app.util;
import com.navdy.hud.app.R;

public class DateUtil {
    final private static int HOUR_HAND_ANGLE_PER_HOUR = 30;
    final private static int MINUTE_HAND_ANGLE_PER_MINUTE = 6;
    private static java.text.SimpleDateFormat dateLabelFormat;
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.DateUtil.class);
        dateLabelFormat = new java.text.SimpleDateFormat("d MMMM", java.util.Locale.US);
    }
    
    public DateUtil() {
    }
    
    public static float getClockAngleForHour(int i, int i0) {
        return (float)(((i % 12 - 3) * 30 + 360) % 360) + (float)i0 / 60f * 30f;
    }
    
    public static float getClockAngleForMinutes(int i) {
        return (float)(((i - 15) * 6 + 360) % 360);
    }
    
    public static String getDateLabel(java.util.Date a) {
        String s = null;
        label2: try {
            java.text.SimpleDateFormat a0 = null;
            Throwable a1 = null;
            label0: if (android.text.format.DateUtils.isToday(a.getTime())) {
                s = com.navdy.hud.app.HudApplication.getAppContext().getResources().getString(R.string.today);
                break label2;
            } else {
                java.util.Date a2 = new java.util.Date();
                java.util.Calendar a3 = java.util.Calendar.getInstance();
                a3.setTime(a2);
                java.util.Calendar a4 = java.util.Calendar.getInstance();
                a4.setTime(a);
                int i = a3.get(1);
                int i0 = a4.get(1);
                int i1 = a3.get(6);
                int i2 = a4.get(6);
                label1: {
                    if (i != i0) {
                        break label1;
                    }
                    if (i1 - i2 != 1) {
                        break label1;
                    }
                    s = com.navdy.hud.app.HudApplication.getAppContext().getResources().getString(R.string.yesterday);
                    break label2;
                }
                synchronized(dateLabelFormat) {
                    java.text.SimpleDateFormat a5 = dateLabelFormat;
                    s = a5.format(a);
                    /*monexit(a0)*/;
                }
                break label2;
            }
            while(true) {
                try {
                    /*monexit(a0)*/;
                } catch(IllegalMonitorStateException | NullPointerException a7) {
                    Throwable a8 = a7;
                    a1 = a8;
                    continue;
                }
                throw a1;
            }
        } catch(Throwable a9) {
            sLogger.e(a9);
            s = null;
        }
        return s;
    }
    
    public static java.util.Date parseIrmcDateStr(String s) {
        java.util.Date a = null;
        label0: {
            Throwable a0 = null;
            if (s != null) {
                try {
                    int i = s.length();
                    a = null;
                    if (i != 15) {
                        break label0;
                    }
                    java.util.Calendar a1 = java.util.Calendar.getInstance();
                    a1.setTimeInMillis(0L);
                    a1.set(1, Integer.parseInt(s.substring(0, 4)));
                    a1.set(2, Integer.parseInt(s.substring(4, 6)) - 1);
                    a1.set(5, Integer.parseInt(s.substring(6, 8)));
                    a1.set(11, Integer.parseInt(s.substring(9, 11)));
                    a1.set(12, Integer.parseInt(s.substring(11, 13)));
                    a1.set(13, Integer.parseInt(s.substring(13, 15)));
                    a = a1.getTime();
                    break label0;
                } catch(Throwable a2) {
                    a0 = a2;
                }
            } else {
                a = null;
                break label0;
            }
            sLogger.e(a0);
            a = null;
        }
        return a;
    }
}
