package com.navdy.hud.app.util;

public class ReportIssueService extends android.app.IntentService {
    final private static String ACTION_DUMP = "Dump";
    final private static String ACTION_SNAPSHOT = "Snapshot";
    final private static String ACTION_SUBMIT_JIRA = "Jira";
    final private static String ACTION_SYNC = "Sync";
    final public static String ASSIGNEE = "assignee";
    final public static String ASSIGNEE_EMAIL = "assigneeEmail";
    final public static String ATTACHMENT = "attachment";
    final private static java.text.SimpleDateFormat DATE_FORMAT;
    final public static String ENVIRONMENT = "Environment";
    final public static String EXTRA_ISSUE_TYPE = "EXTRA_ISSUE_TYPE";
    final public static String EXTRA_SNAPSHOT_TITLE = "EXTRA_SNAPSHOT_TITLE";
    final private static String HUD_ISSUE_TYPE = "Issue";
    final private static String HUD_PROJECT = "HUD";
    final private static String ISSUE_TYPE = "Task";
    final private static String JIRA_CREDENTIALS_META = "JIRA_CREDENTIALS";
    final private static int MAX_FILES_OUT_STANDING = 10;
    final private static String NAME = "REPORT_ISSUE";
    final private static String PROJECT_NAME = "NP";
    final public static long RETRY_INTERVAL;
    final private static java.text.SimpleDateFormat SNAPSHOT_DATE_FORMAT;
    final private static java.text.SimpleDateFormat SNAPSHOT_JIRA_TICKET_DATE_FORMAT;
    final public static String SUMMARY = "Summary";
    final public static int SYNC_REQ = 128;
    final public static String TICKET_ID = "ticketId";
    private static java.io.File lastSnapShotFile;
    private static boolean mIsInitialized;
    private static java.util.concurrent.PriorityBlockingQueue mIssuesToSync;
    private static long mLastIssueSubmittedAt;
    private static java.util.concurrent.atomic.AtomicBoolean mSyncing;
    private static String navigationIssuesFolder;
    private static com.navdy.service.library.log.Logger sLogger;
    @Inject
    com.squareup.otto.Bus bus;
    @Inject
    com.navdy.hud.app.debug.DriveRecorder driveRecorder;
    @Inject
    com.navdy.hud.app.gesture.GestureServiceConnector gestureService;
    @Inject
    com.navdy.service.library.network.http.IHttpManager mHttpManager;
    com.navdy.service.library.network.http.services.JiraClient mJiraClient;
    
    static {
        DATE_FORMAT = new java.text.SimpleDateFormat("yyyy-MM-dd'_'HH:mm:ss.SSS", java.util.Locale.US);
        mIssuesToSync = new java.util.concurrent.PriorityBlockingQueue(5, (java.util.Comparator)new com.navdy.hud.app.util.ReportIssueService$FilesModifiedTimeComparator());
        RETRY_INTERVAL = java.util.concurrent.TimeUnit.MINUTES.toMillis(3L);
        mLastIssueSubmittedAt = 0L;
        mIsInitialized = false;
        mSyncing = new java.util.concurrent.atomic.AtomicBoolean(false);
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.ReportIssueService.class);
        SNAPSHOT_DATE_FORMAT = new java.text.SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", java.util.Locale.US);
        SNAPSHOT_JIRA_TICKET_DATE_FORMAT = new java.text.SimpleDateFormat("MMM,dd hh:mm a", java.util.Locale.US);
    }
    
    public ReportIssueService() {
        super("REPORT_ISSUE");
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static void access$100(com.navdy.hud.app.util.ReportIssueService a, java.io.File a0) {
        a.onSyncComplete(a0);
    }
    
    static void access$200(com.navdy.hud.app.util.ReportIssueService a) {
        a.syncLater();
    }
    
    static void access$300(com.navdy.hud.app.util.ReportIssueService a, String s, java.io.File a0, com.navdy.service.library.network.http.services.JiraClient$ResultCallback a1) {
        a.attachFilesToTheJiraTicket(s, a0, a1);
    }
    
    private void attachFilesToTheJiraTicket(String s, java.io.File a, com.navdy.service.library.network.http.services.JiraClient$ResultCallback a0) {
        boolean b = a.exists();
        label1: {
            java.io.IOException a1 = null;
            if (!b) {
                break label1;
            }
            if (!a.getName().endsWith(".zip")) {
                break label1;
            }
            java.io.File a2 = new java.io.File(a.getParentFile(), "Staging");
            if (a2.exists()) {
                if (a2.isDirectory()) {
                    com.navdy.service.library.util.IOUtils.deleteDirectory(com.navdy.hud.app.HudApplication.getAppContext(), a2);
                } else {
                    com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), a2.getAbsolutePath());
                }
            }
            a2.mkdir();
            label0: {
                try {
                    com.navdy.service.library.util.IOUtils.deCompressZipToDirectory(com.navdy.hud.app.HudApplication.getAppContext(), a, a2);
                    java.io.File[] a3 = a2.listFiles();
                    java.util.ArrayList a4 = new java.util.ArrayList();
                    java.util.ArrayList a5 = new java.util.ArrayList();
                    int i = a3.length;
                    Object a6 = a0;
                    int i0 = 0;
                    while(i0 < i) {
                        java.io.File a7 = a3[i0];
                        if (a7.getName().endsWith(".png")) {
                            a5.add(a7);
                        } else {
                            a4.add(a7);
                        }
                        i0 = i0 + 1;
                    }
                    java.io.File[] a8 = (java.io.File[])a4.toArray((Object[])new java.io.File[a4.size()]);
                    java.io.File a9 = new java.io.File(a2, "logs.zip");
                    com.navdy.service.library.util.IOUtils.compressFilesToZip(com.navdy.hud.app.HudApplication.getAppContext(), a8, a9.getAbsolutePath());
                    java.util.ArrayList a10 = new java.util.ArrayList();
                    com.navdy.service.library.network.http.services.JiraClient$Attachment a11 = new com.navdy.service.library.network.http.services.JiraClient$Attachment();
                    a11.filePath = a9.getAbsolutePath();
                    a11.mimeType = "application/zip";
                    a10.add(a11);
                    Object a12 = a5.iterator();
                    while(((java.util.Iterator)a12).hasNext()) {
                        java.io.File a13 = (java.io.File)((java.util.Iterator)a12).next();
                        com.navdy.service.library.network.http.services.JiraClient$Attachment a14 = new com.navdy.service.library.network.http.services.JiraClient$Attachment();
                        a14.filePath = a13.getAbsolutePath();
                        a14.mimeType = "image/png";
                        a10.add(a14);
                    }
                    this.mJiraClient.attachFilesToTicket(s, (java.util.List)a10, (com.navdy.service.library.network.http.services.JiraClient$ResultCallback)a6);
                } catch(java.io.IOException a15) {
                    a1 = a15;
                    break label0;
                }
                break label1;
            }
            sLogger.e("Error decompressing the zip file to get the attachments", (Throwable)a1);
        }
    }
    
    private String buildDescription() {
        String s = null;
        String s0 = null;
        String s1 = null;
        com.navdy.service.library.device.NavdyDeviceId a = com.navdy.service.library.device.NavdyDeviceId.getThisDevice((android.content.Context)this);
        StringBuilder a0 = new StringBuilder();
        a0.append("\n#############\n");
        a0.append("HERE metadata: ");
        a0.append("\n#############\n\n");
        String s2 = com.navdy.hud.app.util.DeviceUtil.getCurrentHereSdkVersion();
        String s3 = com.navdy.hud.app.util.DeviceUtil.getHEREMapsDataInfo();
        label2: {
            Throwable a1 = null;
            if (s3 == null) {
                s = "";
                break label2;
            } else {
                try {
                    s = new com.google.gson.GsonBuilder().setPrettyPrinting().create().toJson(new com.google.gson.JsonParser().parse(s3));
                    break label2;
                } catch(Throwable a2) {
                    a1 = a2;
                }
            }
            sLogger.e(a1);
            s = "";
        }
        if (!android.text.TextUtils.isEmpty((CharSequence)s2)) {
            a0.append("HERE mSDK for Android v").append(s2).append("\n");
        }
        if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
            a0.append("HERE Map Data:\n").append(s3).append("\n");
        }
        a0.append("\n#############\n");
        a0.append("Navigation details:");
        a0.append("\n#############\n\n");
        com.here.android.mpa.common.GeoPosition a3 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLastGeoPosition();
        label1: {
            label0: {
                if (a3 == null) {
                    break label0;
                }
                if (a3.getCoordinate() == null) {
                    break label0;
                }
                a0.append("Current Location: ").append(a3.getCoordinate().getLatitude()).append(",").append(a3.getCoordinate().getLongitude()).append("\n");
                break label1;
            }
            a0.append("Current Location: N/A\n");
        }
        a0.append("Current Road: ").append(com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadName()).append("\n");
        com.here.android.mpa.common.RoadElement a4 = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadElement();
        if (a4 != null) {
            a0.append("Current road element ID: ").append(a4.getIdentifier()).append("\n");
        }
        com.navdy.hud.app.framework.DriverProfileHelper a5 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance();
        com.navdy.hud.app.profile.DriverProfile a6 = a5.getCurrentProfile();
        if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isNavigationModeOn()) {
            com.navdy.hud.app.maps.here.HereNavigationManager a7 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
            com.here.android.mpa.routing.Route a8 = a7.getCurrentRoute();
            com.navdy.hud.app.maps.here.HereNavController a9 = a7.getNavController();
            if (a8 != null) {
                com.here.android.mpa.routing.RouteTta a10 = a9.getTta(com.here.android.mpa.routing.Route$TrafficPenaltyMode.OPTIMAL, true);
                com.here.android.mpa.common.GeoCoordinate a11 = a7.getStartLocation();
                if (a11 == null) {
                    a0.append("Route Starting point: N/A\n");
                } else {
                    a0.append("Route Starting point: ").append(a11.getLatitude()).append(",").append(a11.getLongitude()).append("\n");
                }
                com.here.android.mpa.common.GeoCoordinate a12 = a8.getDestination();
                if (a12 == null) {
                    a0.append("Route Ending point: N/A\n");
                } else {
                    a0.append("Route Ending point: ").append(a12.getLatitude()).append(",").append(a12.getLongitude()).append("\n");
                }
                boolean b = a7.isTrafficConsidered(a7.getCurrentRouteId());
                a0.append("Traffic Used: ").append(b).append("\n");
                long j = com.navdy.hud.app.analytics.NavigationQualityTracker.getInstance().getTripStartUtc();
                if (j > 0L) {
                    java.text.SimpleDateFormat a13 = new java.text.SimpleDateFormat("MMMM dd HH:mm:ss zzzz yyyy Z", java.util.Locale.US);
                    a0.append("Trip started: ").append(a13.format(new java.util.Date(j))).append("\n");
                }
                com.navdy.hud.app.analytics.NavigationQualityTracker a14 = com.navdy.hud.app.analytics.NavigationQualityTracker.getInstance();
                int i = a14.getActualDurationSoFar();
                int i0 = a14.getExpectedDuration();
                int i1 = a14.getExpectedDistance();
                long j0 = a9.getDestinationDistance();
                if (i0 > 0) {
                    a0.append("Expected initial ETA according to HERE: ").append(i0).append(" seconds\n");
                }
                if (i1 > 0) {
                    a0.append("Expected initial distance according to HERE: ").append(i1).append(" meters\n");
                }
                if (a10 != null) {
                    int i2 = a10.getDuration();
                    a0.append("Time To Arrival remaining according to HERE: ").append(i2).append(" seconds\n");
                }
                if (j0 > 0L) {
                    a0.append("Distance remaining according to HERE: ").append(j0).append(" meters\n");
                }
                if (i > 0) {
                    a0.append("Duration of the trip so far: ").append(i).append(" seconds\n");
                }
            }
            String s4 = a7.getDestinationLabel();
            a0.append("Destination: ").append(s4).append(", ").append(a7.getDestinationStreetAddress()).append("\n");
            a0.append("\n#############\n");
            a0.append("User route preferences:");
            a0.append("\n#############\n\n");
            com.navdy.service.library.events.preferences.NavigationPreferences a15 = a6.getNavigationPreferences();
            String s5 = (a15.routingType != com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType.ROUTING_FASTEST) ? "shortest" : "fastest";
            a0.append("Route calculation: ").append(s5).append("\n");
            a0.append("Highways: ").append(a15.allowHighways).append("\n");
            a0.append("Toll roads: ").append(a15.allowTollRoads).append("\n");
            a0.append("Ferries: ").append(a15.allowFerries).append("\n");
            a0.append("Tunnels: ").append(a15.allowTunnels).append("\n");
            a0.append("Unpaved Roads: ").append(a15.allowUnpavedRoads).append("\n");
            a0.append("Auto Trains: ").append(a15.allowAutoTrains).append("\n");
            a0.append("\n#############\n");
            a0.append("Maneuvers (entire route):");
            a0.append("\n#############\n\n");
            com.navdy.hud.app.maps.here.HereMapUtil.printRouteDetails(a8, s4, (com.navdy.service.library.events.navigation.NavigationRouteRequest)null, a0);
            a0.append("\n#############\n");
            a0.append("Waypoints (entire route):");
            a0.append("\n#############\n\n");
            Object a16 = a8.getManeuvers().iterator();
            int i3 = 0;
            while(((java.util.Iterator)a16).hasNext()) {
                com.here.android.mpa.common.GeoCoordinate a17 = ((com.here.android.mpa.routing.Maneuver)((java.util.Iterator)a16).next()).getCoordinate();
                if (a17 != null) {
                    double d = a17.getLatitude();
                    double d0 = a17.getLongitude();
                    a0.append("waypoint").append(i3).append(": ").append(d).append(",").append(d0).append("\n");
                    i3 = i3 + 1;
                }
            }
            a0.append("\n#############\n");
            a0.append("Maneuvers (remaining route):");
            a0.append("\n#############\n\n");
            com.navdy.hud.app.maps.here.HereMapUtil.printRouteDetails(a8, s4, (com.navdy.service.library.events.navigation.NavigationRouteRequest)null, a0, a7.getNextManeuver());
        }
        a0.append("\n#############\n");
        a0.append("Navdy info:");
        a0.append("\n#############\n\n");
        a0.append("Device ID: ").append(a.toString()).append("\n");
        a0.append("HUD app version: 1.3.3051-corona\n");
        a0.append("Serial number: ").append(android.os.Build.SERIAL).append("\n");
        a0.append("Model: ").append(android.os.Build.MODEL).append("\n");
        a0.append("API Level: ").append(android.os.Build.VERSION.SDK_INT).append("\n");
        a0.append("Build type: ").append(android.os.Build.TYPE).append("\n");
        if (a6.isDefaultProfile()) {
            s0 = a5.getDriverProfileManager().getLastUserName();
            s1 = a5.getDriverProfileManager().getLastUserEmail();
        } else {
            s0 = a6.getDriverName();
            s1 = a6.getDriverEmail();
        }
        if (!android.text.TextUtils.isEmpty((CharSequence)s0) && !android.text.TextUtils.isEmpty((CharSequence)s1)) {
            a0.append("Username: ").append(s0).append("\n");
            a0.append("Email: ").append(s1).append("\n");
        }
        return a0.toString();
    }
    
    public static boolean canReportIssue() {
        return mIssuesToSync.size() <= 10;
    }
    
    public static void dispatchReportNewIssue(com.navdy.hud.app.util.ReportIssueService$IssueType a) {
        android.content.Intent a0 = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.ReportIssueService.class);
        a0.setAction("Dump");
        a0.putExtra("EXTRA_ISSUE_TYPE", (java.io.Serializable)a);
        com.navdy.hud.app.HudApplication.getAppContext().startService(a0);
    }
    
    public static void dispatchSync() {
        android.content.Intent a = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.ReportIssueService.class);
        a.setAction("Sync");
        com.navdy.hud.app.HudApplication.getAppContext().startService(a);
    }
    
    private void dumpIssue(com.navdy.hud.app.util.ReportIssueService$IssueType a) {
        if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isNavigationModeOn()) {
            StringBuilder a0 = new StringBuilder();
            a0.append(new StringBuilder().append(a.ordinal()).append("\n").toString());
            a0.append(this.buildDescription());
            this.saveJiraTicketDescriptionToFile(a0.toString(), false);
            mLastIssueSubmittedAt = System.currentTimeMillis();
        }
    }
    
    private void dumpSnapshot() {
        sLogger.d("Creating the snapshot of the HUD for support ticket");
        com.navdy.hud.app.device.light.HUDLightUtils.showSnapshotCollection(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), false);
        label6: {
            Throwable a = null;
            label0: {
                label2: {
                    label5: {
                        label4: try {
                            String s = com.navdy.hud.app.storage.PathManager.getInstance().getNonFatalCrashReportDir();
                            com.navdy.hud.app.util.DeviceUtil.takeDeviceScreenShot(new StringBuilder().append(s).append(java.io.File.separator).append("screen.png").toString());
                            java.util.ArrayList a0 = new java.util.ArrayList();
                            if (android.os.Build.TYPE.equals("eng")) {
                                this.gestureService.dumpRecording();
                            }
                            this.driveRecorder.flushRecordings();
                            this.gestureService.takeSnapShot("/data/misc/swiped/camera.png");
                            java.io.File a1 = new java.io.File(new StringBuilder().append(s).append(java.io.File.separator).append("deviceInfo.txt").toString());
                            if (a1.exists()) {
                                com.navdy.service.library.util.IOUtils.deleteFile((android.content.Context)this, a1.getAbsolutePath());
                            }
                            java.io.FileWriter a2 = new java.io.FileWriter(a1);
                            com.navdy.hud.app.service.ConnectionHandler a3 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getConnectionHandler();
                            if (a3 != null) {
                                com.navdy.service.library.events.DeviceInfo a4 = a3.getLastConnectedDeviceInfo();
                                if (a4 != null) {
                                    a2.write(com.navdy.hud.app.util.CrashReportService.printDeviceInfo(a4));
                                }
                            }
                            a2.flush();
                            try {
                                a2.close();
                            } catch(java.io.IOException a5) {
                                sLogger.e("Error closing the file writer for log file", (Throwable)a5);
                            }
                            java.io.File a6 = new java.io.File(new StringBuilder().append(s).append(java.io.File.separator).append("temp").toString());
                            if (a6.exists()) {
                                com.navdy.service.library.util.IOUtils.deleteDirectory((android.content.Context)this, a6);
                            }
                            boolean b = a6.mkdirs();
                            label3: {
                                if (b) {
                                    break label3;
                                }
                                sLogger.e("could not create tempDirectory");
                                break label4;
                            }
                            String s0 = a6.getAbsolutePath();
                            com.navdy.service.library.util.LogUtils.copySnapshotSystemLogs(s0);
                            com.navdy.hud.app.device.gps.GpsDeadReckoningManager.getInstance().dumpGpsInfo(s0);
                            com.navdy.hud.app.storage.PathManager.getInstance().collectEnvironmentInfo(s0);
                            java.io.File[] a7 = a6.listFiles();
                            java.io.File a8 = new java.io.File(new StringBuilder().append(s).append(java.io.File.separator).append("route.log").toString());
                            if (a8.exists()) {
                                com.navdy.service.library.util.IOUtils.deleteFile((android.content.Context)this, a8.getAbsolutePath());
                            }
                            String s1 = this.buildDescription();
                            java.io.FileWriter a9 = new java.io.FileWriter(a8);
                            a9.write(s1);
                            a9.flush();
                            try {
                                a9.close();
                            } catch(java.io.IOException a10) {
                                sLogger.e("Error closing the file writer for route log file", (Throwable)a10);
                            }
                            ((java.util.List)a0).add(new java.io.File("/data/misc/swiped/camera.png"));
                            ((java.util.List)a0).add(new java.io.File(new StringBuilder().append(s).append(java.io.File.separator).append("screen.png").toString()));
                            ((java.util.List)a0).add(a8);
                            ((java.util.List)a0).add(a1);
                            if (a7 != null) {
                                java.util.Collections.addAll((java.util.Collection)a0, (Object[])a7);
                            }
                            java.util.HashSet a11 = new java.util.HashSet();
                            java.util.List a12 = com.navdy.hud.app.util.ReportIssueService.getLatestDriveLogFiles(1);
                            if (a12 != null && a12.size() > 0) {
                                ((java.util.List)a0).addAll((java.util.Collection)a12);
                                Object a13 = a12.iterator();
                                while(((java.util.Iterator)a13).hasNext()) {
                                    a11.add((java.io.File)((java.util.Iterator)a13).next());
                                }
                            }
                            java.util.Date a14 = new java.util.Date(System.currentTimeMillis());
                            String s2 = SNAPSHOT_DATE_FORMAT.format(a14);
                            String s3 = new StringBuilder().append("snapshot-").append(android.os.Build.SERIAL).append("-").append(android.os.Build.VERSION.INCREMENTAL).append("_").append(s2).append(".zip").toString();
                            java.io.File a15 = new java.io.File(new StringBuilder().append(s).append(java.io.File.separator).append(s3).toString());
                            if (a15.exists()) {
                                com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), a15.getAbsolutePath());
                            }
                            java.io.File[] a16 = new java.io.File[((java.util.List)a0).size()];
                            ((java.util.List)a0).toArray((Object[])a16);
                            boolean b0 = a15.createNewFile();
                            label1: {
                                if (b0) {
                                    break label1;
                                }
                                sLogger.e("snapshot file could not be created");
                                break label2;
                            }
                            com.navdy.service.library.util.IOUtils.compressFilesToZip((android.content.Context)this, a16, a15.getAbsolutePath());
                            Object a17 = ((java.util.List)a0).iterator();
                            while(((java.util.Iterator)a17).hasNext()) {
                                java.io.File a18 = (java.io.File)((java.util.Iterator)a17).next();
                                if (!a11.contains(a18)) {
                                    com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), a18.getAbsolutePath());
                                }
                            }
                            com.navdy.service.library.util.IOUtils.deleteDirectory((android.content.Context)this, a6);
                            lastSnapShotFile = a15;
                            sLogger.d(new StringBuilder().append("Snapshot File created ").append(lastSnapShotFile).toString());
                            com.navdy.hud.app.util.CrashReportService.addSnapshotAsync(a15.getAbsolutePath());
                            break label5;
                        } catch(Throwable a19) {
                            a = a19;
                            break label0;
                        }
                        com.navdy.hud.app.device.light.HUDLightUtils.showSnapshotCollection(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), true);
                        break label6;
                    }
                    com.navdy.hud.app.device.light.HUDLightUtils.showSnapshotCollection(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), true);
                    break label6;
                }
                com.navdy.hud.app.device.light.HUDLightUtils.showSnapshotCollection(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), true);
                break label6;
            }
            try {
                sLogger.e("Error creating a snapshot ", a);
            } catch(Throwable a20) {
                com.navdy.hud.app.device.light.HUDLightUtils.showSnapshotCollection(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), true);
                throw a20;
            }
            com.navdy.hud.app.device.light.HUDLightUtils.showSnapshotCollection(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), true);
        }
    }
    
    public static void dumpSnapshotAsync() {
        android.content.Intent a = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.ReportIssueService.class);
        a.setAction("Snapshot");
        com.navdy.hud.app.HudApplication.getAppContext().startService(a);
    }
    
    public static com.navdy.hud.app.util.ReportIssueService$IssueType getIssueTypeForId(int i) {
        com.navdy.hud.app.util.ReportIssueService$IssueType a = null;
        com.navdy.hud.app.util.ReportIssueService$IssueType[] a0 = com.navdy.hud.app.util.ReportIssueService$IssueType.values();
        label2: {
            label0: {
                label1: {
                    if (i < 0) {
                        break label1;
                    }
                    if (i < a0.length) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = a0[i];
        }
        return a;
    }
    
    public static java.util.List getLatestDriveLogFiles(int i) {
        java.util.ArrayList a = null;
        if (i > 0) {
            java.io.File[] a0 = com.navdy.hud.app.debug.DriveRecorder.getDriveLogsDir("").listFiles();
            a = null;
            if (a0 != null) {
                int i0 = a0.length;
                a = null;
                if (i0 != 0) {
                    a = new java.util.ArrayList();
                    java.util.Arrays.sort((Object[])a0, (java.util.Comparator)new com.navdy.hud.app.util.ReportIssueService$4());
                    int i1 = a0.length - 1;
                    while(i1 >= 0 && a.size() < i * 4) {
                        a.add(a0[i1]);
                        i1 = i1 + -1;
                    }
                }
            }
        } else {
            a = null;
        }
        return (java.util.List)a;
    }
    
    private String getSummary(com.navdy.hud.app.util.ReportIssueService$IssueType a) {
        return (a.getMessageStringResource() == 0) ? new StringBuilder().append("Navigation issue: [").append(a.getIssueTypeCode()).append("] ").append(this.getString(a.getTitleStringResource())).toString() : new StringBuilder().append("Navigation issue: [").append(a.getIssueTypeCode()).append("] ").append(this.getString(a.getTitleStringResource())).append(": ").append(this.getString(a.getMessageStringResource())).toString();
    }
    
    public static void initialize() {
        if (!mIsInitialized) {
            sLogger.d("Initializing the service statically");
            navigationIssuesFolder = com.navdy.hud.app.storage.PathManager.getInstance().getNavigationIssuesDir();
            java.io.File[] a = new java.io.File(navigationIssuesFolder).listFiles();
            sLogger.d(new StringBuilder().append("Number of Files waiting for sync :").append((a == null) ? 0 : a.length).toString());
            if (a != null) {
                int i = a.length;
                int i0 = 0;
                while(i0 < i) {
                    java.io.File a0 = a[i0];
                    sLogger.d(new StringBuilder().append("File ").append(a0.getName()).toString());
                    mIssuesToSync.add(a0);
                    if (mIssuesToSync.size() == 10) {
                        java.io.File a1 = (java.io.File)mIssuesToSync.poll();
                        sLogger.d(new StringBuilder().append("Deleting the old file ").append(a1.getName()).toString());
                        com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), a1.getAbsolutePath());
                    }
                    i0 = i0 + 1;
                }
            }
            mIsInitialized = true;
        }
    }
    
    private void onSyncComplete(java.io.File a) {
        if (a != null) {
            sLogger.d(new StringBuilder().append("Removed the file from list :").append(mIssuesToSync.remove(a)).toString());
            com.navdy.service.library.util.IOUtils.deleteFile((android.content.Context)this, a.getAbsolutePath());
            com.navdy.hud.app.util.ReportIssueService.dispatchSync();
        }
        mSyncing.set(false);
    }
    
    private void saveJiraTicketDescriptionToFile(String s, boolean b) {
        long j = System.currentTimeMillis();
        java.io.File a = new java.io.File(new StringBuilder().append(navigationIssuesFolder).append(java.io.File.separator).append(DATE_FORMAT.format(new java.util.Date(j))).append(b ? ".json" : "").toString());
        if (a.exists()) {
            com.navdy.service.library.util.IOUtils.deleteFile((android.content.Context)this, a.getAbsolutePath());
        }
        label5: {
            java.io.FileWriter a0 = null;
            label4: {
                label1: {
                    java.io.FileWriter a1 = null;
                    Throwable a2 = null;
                    try {
                        label0: {
                            java.io.IOException a3 = null;
                            label3: {
                                label2: {
                                    try {
                                        a1 = null;
                                        boolean b0 = a.createNewFile();
                                        a0 = null;
                                        if (!b0) {
                                            break label1;
                                        }
                                        a1 = null;
                                        a0 = new java.io.FileWriter(a);
                                        break label2;
                                    } catch(java.io.IOException a4) {
                                        a3 = a4;
                                    }
                                    a0 = null;
                                    break label3;
                                }
                                try {
                                    try {
                                        a0.write(s);
                                        a0.close();
                                        mIssuesToSync.add(a);
                                        com.navdy.hud.app.util.ReportIssueService.dispatchSync();
                                        break label1;
                                    } catch(java.io.IOException a5) {
                                        a3 = a5;
                                    }
                                } catch(Throwable a6) {
                                    a2 = a6;
                                    break label0;
                                }
                            }
                            com.navdy.service.library.log.Logger a7 = sLogger;
                            a1 = a0;
                            a7.e(new StringBuilder().append("Error while dumping the issue ").append(a3.getMessage()).toString(), (Throwable)a3);
                            break label4;
                        }
                        a1 = a0;
                    } catch(Throwable a8) {
                        a2 = a8;
                    }
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
                    throw a2;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                break label5;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
        }
    }
    
    public static void scheduleSync() {
        android.content.Intent a = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.ReportIssueService.class);
        a.setAction("Sync");
        android.app.PendingIntent a0 = android.app.PendingIntent.getService(com.navdy.hud.app.HudApplication.getAppContext(), 128, a, 268435456);
        long j = RETRY_INTERVAL;
        ((android.app.AlarmManager)com.navdy.hud.app.HudApplication.getAppContext().getSystemService("alarm")).set(3, android.os.SystemClock.elapsedRealtime() + j, a0);
    }
    
    public static void showSnapshotMenu() {
        android.os.Bundle a = new android.os.Bundle();
        a.putInt("MENU_MODE", com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode.SNAPSHOT_TITLE_PICKER.ordinal());
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus().post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_MAIN_MENU, a, null, false));
    }
    
    private void submitJiraTicket(String s) {
        sLogger.d(new StringBuilder().append("Submitting jira ticket with title ").append(s).toString());
        try {
            java.util.Date a = new java.util.Date(System.currentTimeMillis());
            com.navdy.hud.app.service.ConnectionHandler a0 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getConnectionHandler();
            org.json.JSONObject a1 = new org.json.JSONObject();
            a1.put("attachment", lastSnapShotFile.getAbsolutePath());
            StringBuilder a2 = new StringBuilder();
            com.navdy.hud.app.ui.framework.UIStateManager a3 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
            String s0 = SNAPSHOT_JIRA_TICKET_DATE_FORMAT.format(a);
            com.navdy.hud.app.profile.DriverProfile a4 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
            a2.append(new StringBuilder().append("[Snapshot ").append(s).append(" ").append(s0).append("] ").append((com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP.equals(a3.getHomescreenView().getDisplayMode())) ? "Map" : "Dash").toString());
            String s1 = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadName();
            if (!android.text.TextUtils.isEmpty((CharSequence)s1)) {
                a2.append(" ").append(s1);
            }
            if (!a4.isDefaultProfile()) {
                a2.append(" , From ").append(a4.getDriverEmail());
            }
            a1.put("Summary", a2.toString());
            if (!a4.isDefaultProfile()) {
                String s2 = a4.getDriverName();
                if (!android.text.TextUtils.isEmpty((CharSequence)s2)) {
                    String s3 = s2.split(" ")[0].trim();
                    sLogger.d(new StringBuilder().append("Assignee name for the snapshot ").append(s3).toString());
                    a1.put("assignee", s3);
                }
                String s4 = a4.getDriverEmail();
                if (!android.text.TextUtils.isEmpty((CharSequence)s4)) {
                    a1.put("assigneeEmail", s4);
                }
            }
            StringBuilder a5 = new StringBuilder();
            a5.append(new StringBuilder().append("HUD ").append(android.os.Build.VERSION.INCREMENTAL).append("\n").toString());
            if (a0 != null) {
                com.navdy.service.library.events.DeviceInfo a6 = a0.getLastConnectedDeviceInfo();
                if (a6 != null) {
                    a5.append((com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS.equals(a6.platform)) ? "iPhone" : "Android");
                    a5.append(new StringBuilder().append(" V").append(a6.clientVersion).append("\n").toString());
                }
            }
            a5.append(new StringBuilder().append("Language : ").append(com.navdy.hud.app.profile.HudLocale.getCurrentLocale(com.navdy.hud.app.HudApplication.getAppContext()).getLanguage()).toString());
            a1.put("Environment", a5.toString());
            this.saveJiraTicketDescriptionToFile(a1.toString(), true);
        } catch(org.json.JSONException a7) {
            a7.printStackTrace();
        }
    }
    
    public static void submitJiraTicketAsync(String s) {
        android.content.Intent a = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.ReportIssueService.class);
        a.setAction("Jira");
        a.putExtra("EXTRA_SNAPSHOT_TITLE", s);
        com.navdy.hud.app.HudApplication.getAppContext().startService(a);
    }
    
    private void sync() {
        boolean b = com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance().isNetworkAccessAllowed(com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.JIRA);
        label0: {
            org.json.JSONException a = null;
            label3: {
                java.io.IOException a0 = null;
                label4: {
                    java.io.File a1 = null;
                    java.io.FileReader a2 = null;
                    label1: {
                        label2: if (b) {
                            boolean b0 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected();
                            boolean b1 = com.navdy.hud.app.framework.network.NetworkStateManager.isConnectedToNetwork((android.content.Context)this);
                            sLogger.d(new StringBuilder().append("Trying to sync , already syncing:").append(mSyncing.get()).append(", Navigation mode on:").append(com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isNavigationModeOn()).append(", Device connected:").append(b0).append(", Internet connected :").append(b1).toString());
                            label7: {
                                label8: {
                                    if (!b0) {
                                        break label8;
                                    }
                                    if (b1) {
                                        break label7;
                                    }
                                }
                                sLogger.d("Already sync is running, scheduling for a later time");
                                com.navdy.hud.app.util.ReportIssueService.scheduleSync();
                                break label0;
                            }
                            if (!mSyncing.compareAndSet(false, true)) {
                                break label0;
                            }
                            a1 = (java.io.File)mIssuesToSync.peek();
                            label5: {
                                label6: {
                                    if (a1 == null) {
                                        break label6;
                                    }
                                    if (a1.exists()) {
                                        break label5;
                                    }
                                }
                                sLogger.d("Ticket file does not exist anymore");
                                this.onSyncComplete(a1);
                                break label0;
                            }
                            if (a1.getName().endsWith(".json")) {
                                try {
                                    try {
                                        sLogger.d("Submitting ticket from JSON file");
                                        org.json.JSONObject a3 = new org.json.JSONObject(com.navdy.service.library.util.IOUtils.convertFileToString(a1.getAbsolutePath()));
                                        String s = a3.getString("Summary");
                                        String s0 = (a3.has("Environment")) ? a3.getString("Environment") : "";
                                        String s1 = (a3.has("assignee")) ? a3.getString("assignee") : "";
                                        String s2 = (a3.has("assigneeEmail")) ? a3.getString("assigneeEmail") : "";
                                        if (android.text.TextUtils.isEmpty((CharSequence)s1)) {
                                            com.navdy.hud.app.profile.DriverProfile a4 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
                                            String s3 = a4.getDriverName();
                                            if (!android.text.TextUtils.isEmpty((CharSequence)s3)) {
                                                s1 = s3.trim().split(" ")[0];
                                            }
                                            s2 = a4.getDriverEmail();
                                        }
                                        sLogger.d(new StringBuilder().append("Assignee name ").append(s1).toString());
                                        String s4 = (a3.has("attachment")) ? a3.getString("attachment") : "";
                                        sLogger.d(new StringBuilder().append("Attachment ").append(s4).toString());
                                        java.io.File a5 = new java.io.File(s4);
                                        if (a5.exists()) {
                                            String s5 = (a3.has("ticketId")) ? a3.getString("ticketId") : "";
                                            if (android.text.TextUtils.isEmpty((CharSequence)s5)) {
                                                sLogger.d("Submitting a new ticket from JSON file");
                                                this.mJiraClient.submitTicket("HUD", "Task", s, "Snapshot, fill in the details", s0, (com.navdy.service.library.network.http.services.JiraClient$ResultCallback)new com.navdy.hud.app.util.ReportIssueService$2(this, a3, a1, a5, s1, s2));
                                                break label0;
                                            } else {
                                                sLogger.d(new StringBuilder().append("Ticket already exists ").append(s5).append(" Attaching the files to the ticket").toString());
                                                this.attachFilesToTheJiraTicket(s5, a5, (com.navdy.service.library.network.http.services.JiraClient$ResultCallback)new com.navdy.hud.app.util.ReportIssueService$1(this, a1));
                                                break label0;
                                            }
                                        } else {
                                            sLogger.e("Snapshot file has been deleted, not creating a ticket ");
                                            this.onSyncComplete(a1);
                                            break label0;
                                        }
                                    } catch(java.io.IOException a6) {
                                        a0 = a6;
                                        break label4;
                                    }
                                } catch(org.json.JSONException a7) {
                                    a = a7;
                                    break label3;
                                }
                            } else {
                                try {
                                    a2 = new java.io.FileReader(a1);
                                } catch(Exception ignoredException) {
                                    break label2;
                                }
                                try {
                                    java.io.BufferedReader a8 = new java.io.BufferedReader((java.io.Reader)a2);
                                    com.navdy.hud.app.util.ReportIssueService$IssueType a9 = com.navdy.hud.app.util.ReportIssueService.getIssueTypeForId(Integer.parseInt(a8.readLine().trim()));
                                    if (a9 == null) {
                                        throw new java.io.IOException("Bad file format");
                                    }
                                    String s6 = this.getSummary(a9);
                                    StringBuilder a10 = new StringBuilder();
                                    while(true) {
                                        String s7 = a8.readLine();
                                        if (s7 == null) {
                                            break;
                                        }
                                        a10.append(s7).append("\n");
                                    }
                                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                                    String s8 = a10.toString();
                                    this.mJiraClient.submitTicket("NP", "Task", s6, s8, (com.navdy.service.library.network.http.services.JiraClient$ResultCallback)new com.navdy.hud.app.util.ReportIssueService$3(this, a1));
                                } catch(Exception ignoredException0) {
                                    break label1;
                                }
                                break label0;
                            }
                        } else {
                            com.navdy.hud.app.util.ReportIssueService.scheduleSync();
                            break label0;
                        }
                        a2 = null;
                    }
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                    this.onSyncComplete(a1);
                    break label0;
                }
                sLogger.e("Not able to read json file ", (Throwable)a0);
                this.syncLater();
                break label0;
            }
            sLogger.e("Not able to read json file ", (Throwable)a);
            this.syncLater();
        }
    }
    
    private void syncLater() {
        mSyncing.set(false);
        com.navdy.hud.app.util.ReportIssueService.scheduleSync();
    }
    
    public void onCreate() {
        super.onCreate();
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        sLogger.d("ReportIssue service started");
        this.mJiraClient = new com.navdy.service.library.network.http.services.JiraClient(this.mHttpManager.getClientCopy().readTimeout(120L, java.util.concurrent.TimeUnit.SECONDS).connectTimeout(120L, java.util.concurrent.TimeUnit.SECONDS).build());
        String s = com.navdy.service.library.util.CredentialUtil.getCredentials((android.content.Context)this, "JIRA_CREDENTIALS");
        this.mJiraClient.setEncodedCredentials(s);
    }
    
    protected void onHandleIntent(android.content.Intent a) {
        try {
            sLogger.d(new StringBuilder().append("onHandleIntent ").append(a.toString()).toString());
            if ("Dump".equals(a.getAction())) {
                com.navdy.hud.app.util.ReportIssueService$IssueType a0 = (com.navdy.hud.app.util.ReportIssueService$IssueType)a.getSerializableExtra("EXTRA_ISSUE_TYPE");
                sLogger.d(new StringBuilder().append("Dumping an issue ").append(a0).toString());
                if (a0 != null) {
                    this.dumpIssue(a0);
                }
            } else if ("Snapshot".equals(a.getAction())) {
                this.dumpSnapshot();
            } else if ("Jira".equals(a.getAction())) {
                String s = a.getStringExtra("EXTRA_SNAPSHOT_TITLE");
                if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                    s = "HUD";
                }
                this.submitJiraTicket(s);
            } else {
                sLogger.d("Handling Sync request");
                this.sync();
            }
        } catch(Throwable a1) {
            sLogger.e("Exception while handling intent ", a1);
        }
    }
}
