package com.navdy.hud.app.util;

public class ViewUtil {
    final private static int LINE_SPACING_INDEX = 2;
    final private static int MAX_LINE_INDEX = 0;
    final private static int[] MULTI_LINE_ATTRS;
    final private static int SINGLE_LINE_INDEX = 1;
    static android.util.SparseArray TEXT_SIZES;
    
    static {
        int[] a = new int[3];
        a[0] = 16843091;
        a[1] = 16843101;
        a[2] = 16843288;
        MULTI_LINE_ATTRS = a;
        TEXT_SIZES = new android.util.SparseArray();
    }
    
    public ViewUtil() {
    }
    
    public static void adjustPadding(android.view.View a, int i, int i0, int i1, int i2) {
        a.setPadding(a.getPaddingLeft() + i, a.getPaddingTop() + i0, a.getPaddingRight() + i1, a.getPaddingBottom() + i2);
    }
    
    public static int applyTextAndStyle(android.widget.TextView a, CharSequence a0, int i, int i0) {
        android.content.Context a1 = a.getContext();
        if (android.text.TextUtils.isEmpty(a0)) {
            a.setVisibility(8);
            i = 0;
        } else {
            float f = 0.0f;
            a.setVisibility(0);
            boolean b = i == 1;
            if (i0 == -1) {
                f = 1f;
            } else {
                android.content.res.TypedArray a2 = a1.obtainStyledAttributes(i0, MULTI_LINE_ATTRS);
                int i1 = a2.getInt(0, i);
                b = (a2.hasValue(1)) ? a2.getBoolean(1, true) : i1 == 1;
                f = a2.getFloat(2, 1f);
                a2.recycle();
                a.setTextAppearance(a1, i0);
                i = i1;
            }
            a.setText(a0);
            a.setLineSpacing(0.0f, f);
            a.setSingleLine(b);
            a.setMaxLines(i);
        }
        return i;
    }
    
    public static float autosize(android.widget.TextView a, int i, int i0, float[] a0) {
        int[] a1 = new int[a0.length];
        int i1 = 0;
        while(i1 < a0.length) {
            a1[i1] = i;
            i1 = i1 + 1;
        }
        return com.navdy.hud.app.util.ViewUtil.autosize(a, a1, i0, a0, (int[])null);
    }
    
    public static float autosize(android.widget.TextView a, int[] a0, int i, float[] a1, int[] a2) {
        if (a0.length != a1.length) {
            throw new IllegalArgumentException("maxLinesArray length must match textSizes length");
        }
        CharSequence a3 = a.getText();
        android.text.TextPaint a4 = a.getPaint();
        int i0 = a1.length - 1;
        float f = a1[i0];
        Object a5 = a3;
        int i1 = -1;
        int i2 = 0;
        while(true) {
            float f0 = 0.0f;
            if (i2 >= a1.length) {
                f0 = f;
            } else {
                int i3 = a0[i2];
                a.setSingleLine(i3 == 1);
                f0 = a1[i2];
                a4.setTextSize(f0);
                int i4 = new android.text.StaticLayout((CharSequence)a5, a4, i, android.text.Layout$Alignment.ALIGN_NORMAL, 1f, 0.0f, false).getLineCount();
                label0: if (i4 > i3) {
                    f0 = f;
                } else {
                    label1: {
                        if (i1 == -1) {
                            break label1;
                        }
                        if (i4 >= i1) {
                            f0 = f;
                            break label0;
                        }
                    }
                    i0 = i2;
                    i1 = i4;
                }
                f = f0;
                if (i4 != 1) {
                    i2 = i2 + 1;
                    continue;
                }
            }
            if (a2 != null) {
                a2[0] = i0;
                if (i1 == -1) {
                    i1 = a0[i0];
                }
                a2[1] = i1;
            }
            a.setTextSize(f0);
            return f0;
        }
    }
    
    public static void autosize(android.widget.TextView a, int i, int i0, int i1) {
        com.navdy.hud.app.util.ViewUtil.autosize(a, i, i0, com.navdy.hud.app.util.ViewUtil.lookupSizes(a.getResources(), i1));
    }
    
    private static float[] lookupSizes(android.content.res.Resources a, int i) {
        float[] a0 = (float[])TEXT_SIZES.get(i);
        if (a0 == null) {
            int[] a1 = a.getIntArray(i);
            a0 = new float[a1.length];
            int i0 = 0;
            while(i0 < a1.length) {
                a0[i0] = (float)(a1[i0] != 0);
                i0 = i0 + 1;
            }
            TEXT_SIZES.put(i, a0);
        }
        return a0;
    }
    
    public static void setBottomPadding(android.view.View a, int i) {
        a.setPadding(a.getPaddingLeft(), a.getPaddingTop(), a.getPaddingRight(), i);
    }
}
