package com.navdy.hud.app.util;

public class HudTaskQueue extends com.navdy.service.library.util.TaskQueue {
    final public static int DRIVE_LOGGING = 9;
    final public static int FILE_DOWNLOAD = 5;
    final public static int GENERAL_SERIAL = 10;
    final public static int GLANCE_SERIAL = 14;
    final public static int HERE_BACKGROUND = 2;
    final public static int HERE_BACKGROUND_SERIAL = 3;
    final public static int HERE_FUEL_MANAGER_SERIAL = 21;
    final public static int HERE_LANE_INFO_SERIAL = 15;
    final public static int HERE_MAP_ANIMATION_SERIAL = 17;
    final public static int HERE_NAVIGATION_MANAGER_SERIAL = 20;
    final public static int HERE_POSITION_SERIAL = 18;
    final public static int HERE_ROUTE_MANAGER_SERIAL = 19;
    final public static int HERE_TBT_SERIAL = 4;
    final public static int HERE_ZOOM_SERIAL = 16;
    final public static int IMAGE_CACHE_PROCESSING = 22;
    final public static int IMAGE_DOWNLOAD = 7;
    final public static int NETWORK_SERIAL = 23;
    final public static int OBD_CONFIGURATION_SERIAL = 13;
    final public static int OBD_STATUS = 6;
    final public static int REMOTE_SEND_SERIAL = 11;
    final public static int SPEED_WARNING_SERIAL = 12;
    final public static int TRIP_TRACKING = 8;
    
    public HudTaskQueue() {
    }
}
