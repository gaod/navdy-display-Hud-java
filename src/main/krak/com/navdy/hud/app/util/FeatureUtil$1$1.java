package com.navdy.hud.app.util;

class FeatureUtil$1$1 implements Runnable {
    final com.navdy.hud.app.util.FeatureUtil$1 this$1;
    final android.os.Bundle val$bundle;
    
    FeatureUtil$1$1(com.navdy.hud.app.util.FeatureUtil$1 a, android.os.Bundle a0) {
        super();
        this.this$1 = a;
        this.val$bundle = a0;
    }
    
    public void run() {
        label0: try {
            String s = (String)this.val$bundle.keySet().iterator().next();
            boolean b = this.val$bundle.getBoolean(s);
            com.navdy.hud.app.util.FeatureUtil$Feature a = com.navdy.hud.app.util.FeatureUtil.access$000(this.this$1.this$0, s);
            if (a != null) {
                com.navdy.hud.app.util.FeatureUtil$Feature a0 = com.navdy.hud.app.util.FeatureUtil$Feature.GESTURE_ENGINE;
                label3: {
                    label1: {
                        label2: {
                            label4: {
                                if (a != a0) {
                                    break label4;
                                }
                                com.navdy.hud.app.util.FeatureUtil.access$200(this.this$1.this$0).edit().putBoolean("gesture.engine", b).apply();
                                com.navdy.hud.app.util.FeatureUtil.access$100().v(new StringBuilder().append("gesture engine setting set to ").append(b).toString());
                                if (!com.navdy.hud.app.util.DeviceUtil.supportsCamera()) {
                                    break label3;
                                }
                                break label2;
                            }
                            if (a != com.navdy.hud.app.util.FeatureUtil$Feature.GESTURE_COLLECTOR) {
                                break label2;
                            }
                            com.navdy.hud.app.util.FeatureUtil.access$100().v(new StringBuilder().append("gesture collector setting set to ").append(b).toString());
                            if (!com.navdy.hud.app.util.DeviceUtil.supportsCamera()) {
                                break label1;
                            }
                            if (!com.navdy.hud.app.util.FeatureUtil.access$300(this.this$1.this$0)) {
                                break label0;
                            }
                        }
                        com.navdy.hud.app.util.FeatureUtil.access$400(this.this$1.this$0, a, b);
                        break label0;
                    }
                    com.navdy.hud.app.util.FeatureUtil.access$100().v("gesture collector cannot be enabled/disabled, no camera");
                    break label0;
                }
                com.navdy.hud.app.util.FeatureUtil.access$100().v("gesture engine cannot be enabled/disabled, no camera");
            } else {
                com.navdy.hud.app.util.FeatureUtil.access$100().i(new StringBuilder().append("invalid feature:").append(s).toString());
            }
        } catch(Throwable a1) {
            com.navdy.hud.app.util.FeatureUtil.access$100().e(a1);
        }
    }
}
