package com.navdy.hud.app.util;

public class ScreenOrientation {
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.ScreenOrientation.class);
    }
    
    public ScreenOrientation() {
    }
    
    public static int getCurrentOrientation(android.app.Activity a) {
        int i = 0;
        int i0 = a.getWindowManager().getDefaultDisplay().getRotation();
        android.util.DisplayMetrics a0 = new android.util.DisplayMetrics();
        a.getWindowManager().getDefaultDisplay().getMetrics(a0);
        int i1 = a0.widthPixels;
        int i2 = a0.heightPixels;
        label0: {
            label1: {
                label3: {
                    label4: {
                        label5: {
                            if (i0 == 0) {
                                break label5;
                            }
                            if (i0 != 2) {
                                break label4;
                            }
                        }
                        if (i2 > i1) {
                            break label3;
                        }
                    }
                    label2: {
                        if (i0 == 1) {
                            break label2;
                        }
                        if (i0 != 3) {
                            break label1;
                        }
                    }
                    if (i1 <= i2) {
                        break label1;
                    }
                }
                switch(i0) {
                    case 3: {
                        i = 8;
                        break label0;
                    }
                    case 2: {
                        i = 9;
                        break label0;
                    }
                    case 1: {
                        i = 0;
                        break label0;
                    }
                    case 0: {
                        i = 1;
                        break label0;
                    }
                    default: {
                        sLogger.e("Unknown screen orientation. Defaulting to portrait.");
                        i = 1;
                        break label0;
                    }
                }
            }
            switch(i0) {
                case 3: {
                    i = 9;
                    break;
                }
                case 2: {
                    i = 8;
                    break;
                }
                case 1: {
                    i = 1;
                    break;
                }
                case 0: {
                    i = 0;
                    break;
                }
                default: {
                    sLogger.e("Unknown screen orientation. Defaulting to landscape.");
                    i = 0;
                }
            }
        }
        return i;
    }
    
    public static boolean isPortrait(int i) {
        return i == 1 || i == 9;
    }
    
    public static boolean isPortrait(android.app.Activity a) {
        return com.navdy.hud.app.util.ScreenOrientation.isPortrait(com.navdy.hud.app.util.ScreenOrientation.getCurrentOrientation(a));
    }
}
