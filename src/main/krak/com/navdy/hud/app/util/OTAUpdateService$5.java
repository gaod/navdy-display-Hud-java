package com.navdy.hud.app.util;

class OTAUpdateService$5 implements android.os.RecoverySystem$ProgressListener {
    final com.navdy.hud.app.util.OTAUpdateService this$0;
    
    OTAUpdateService$5(com.navdy.hud.app.util.OTAUpdateService a) {
        super();
        this.this$0 = a;
    }
    
    public void onProgress(int i) {
        com.navdy.service.library.log.Logger a = com.navdy.hud.app.util.OTAUpdateService.access$000();
        Object[] a0 = new Object[1];
        a0[0] = Integer.valueOf(i);
        a.d(String.format("Verify progress: %d%% completed", a0));
    }
}
