package com.navdy.hud.app.util;

public class DeviceUtil {
    final public static String BUILD_TYPE_USER = "user";
    final private static com.navdy.hud.app.util.DeviceUtil$HereSdkVersion CURRENT_HERE_SDK;
    final public static String TELEMETRY_FILE_NAME = "telemetry";
    private static Boolean hasCamera;
    private static boolean hudDevice;
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.DeviceUtil.class);
        CURRENT_HERE_SDK = com.navdy.hud.app.util.DeviceUtil$HereSdkVersion.SDK;
        boolean b = android.os.Build.MODEL.equalsIgnoreCase("NAVDY_HUD-MX6DL");
        label2: {
            label0: {
                label1: {
                    if (b) {
                        break label1;
                    }
                    if (!android.os.Build.MODEL.equalsIgnoreCase("Display")) {
                        break label0;
                    }
                }
                hudDevice = true;
                sLogger.i(new StringBuilder().append("Running on Navdy Device:").append(android.os.Build.MODEL).toString());
                break label2;
            }
            sLogger.i(new StringBuilder().append("Not running on Navdy Device:").append(android.os.Build.MODEL).toString());
        }
    }
    
    public DeviceUtil() {
    }
    
    public static void copyHEREMapsDataInfo(String s) {
        java.io.File a = new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getHereMapsDataDirectory());
        boolean b = a.exists();
        label0: {
            label1: if (b) {
                java.io.File a0 = new java.io.File(a, "meta.json");
                if (a0.exists()) {
                    String s0 = new StringBuilder().append(s).append(java.io.File.separator).append("HERE_meta.json").toString();
                    try {
                        com.navdy.service.library.util.IOUtils.copyFile(a0.getAbsolutePath(), s0);
                    } catch(java.io.IOException ignoredException) {
                        break label1;
                    }
                    break label0;
                } else {
                    sLogger.d("Meta json file not found");
                    break label0;
                }
            } else {
                sLogger.d("Here maps data directory on the maps partition does not exists");
                break label0;
            }
            sLogger.e("Error copying the HERE maps data meta json file");
        }
    }
    
    public static String getCurrentHereSdkTimestamp() {
        return com.navdy.hud.app.util.DeviceUtil$HereSdkVersion.access$100(CURRENT_HERE_SDK);
    }
    
    public static String getCurrentHereSdkVersion() {
        return com.navdy.hud.app.util.DeviceUtil$HereSdkVersion.access$000(CURRENT_HERE_SDK);
    }
    
    public static String getHEREMapsDataInfo() {
        java.io.FileInputStream a = null;
        Throwable a0 = null;
        java.io.File a1 = new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getHereMapsDataDirectory());
        boolean b = a1.exists();
        label4: {
            String s = null;
            label2: {
                java.io.FileInputStream a2 = null;
                java.io.IOException a3 = null;
                label9: {
                    java.io.IOException a4 = null;
                    label3: {
                        label5: {
                            if (b) {
                                java.io.File a5 = new java.io.File(a1, "meta.json");
                                if (a5.exists()) {
                                    label6: try {
                                        Exception a6 = null;
                                        label8: {
                                            label7: {
                                                try {
                                                    a = null;
                                                    a2 = new java.io.FileInputStream(a5);
                                                    break label7;
                                                } catch(Exception a7) {
                                                    a6 = a7;
                                                }
                                                a2 = null;
                                                break label8;
                                            }
                                            try {
                                                try {
                                                    s = com.navdy.service.library.util.IOUtils.convertInputStreamToString((java.io.InputStream)a2, "UTF-8");
                                                    break label6;
                                                } catch(Exception a8) {
                                                    a6 = a8;
                                                }
                                            } catch(Throwable a9) {
                                                a0 = a9;
                                                break label5;
                                            }
                                        }
                                        com.navdy.service.library.log.Logger a10 = sLogger;
                                        a = a2;
                                        a10.e("Error reading the Meta data,", (Throwable)a6);
                                        break label9;
                                    } catch(Throwable a11) {
                                        a0 = a11;
                                        break label4;
                                    }
                                    try {
                                        a2.close();
                                    } catch(java.io.IOException a12) {
                                        a4 = a12;
                                        break label3;
                                    }
                                    break label2;
                                } else {
                                    sLogger.d("Meta json file not found");
                                    s = null;
                                    break label2;
                                }
                            } else {
                                sLogger.d("Here maps data directory on the maps partition does not exists");
                                s = null;
                                break label2;
                            }
                        }
                        a = a2;
                        break label4;
                    }
                    sLogger.e("Error closing the FileInputStream", (Throwable)a4);
                    break label2;
                }
                s = null;
                if (a2 == null) {
                    break label2;
                }
                label1: {
                    try {
                        a2.close();
                    } catch(java.io.IOException a13) {
                        a3 = a13;
                        break label1;
                    }
                    s = null;
                    break label2;
                }
                sLogger.e("Error closing the FileInputStream", (Throwable)a3);
                s = null;
            }
            return s;
        }
        label0: {
            java.io.IOException a14 = null;
            if (a == null) {
                break label0;
            }
            try {
                a.close();
                break label0;
            } catch(java.io.IOException a15) {
                a14 = a15;
            }
            sLogger.e("Error closing the FileInputStream", (Throwable)a14);
        }
        throw a0;
    }
    
    public static boolean isNavdyDevice() {
        return hudDevice;
    }
    
    public static boolean isUserBuild() {
        return "user".equals(android.os.Build.TYPE);
    }
    
    public static boolean supportsCamera() {
        android.hardware.Camera a = null;
        Throwable a0 = null;
        Boolean a1 = hasCamera;
        label1: {
            label0: {
                android.hardware.Camera a2 = null;
                if (a1 != null) {
                    break label0;
                }
                int i = android.hardware.Camera.getNumberOfCameras();
                sLogger.v(new StringBuilder().append("Found ").append(i).append(" cameras").toString());
                label3: {
                    label2: try {
                        Exception a3 = null;
                        try {
                            a = null;
                            a2 = null;
                            a2 = android.hardware.Camera.open(0);
                            a = a2;
                            hasCamera = Boolean.valueOf(true);
                            break label2;
                        } catch(Exception a4) {
                            a3 = a4;
                        }
                        com.navdy.service.library.log.Logger a5 = sLogger;
                        a = a2;
                        a5.e("Failed to open camera", (Throwable)a3);
                        hasCamera = Boolean.valueOf(false);
                        break label3;
                    } catch(Throwable a6) {
                        a0 = a6;
                        break label1;
                    }
                    if (a2 == null) {
                        break label0;
                    }
                    a2.release();
                    break label0;
                }
                if (a2 != null) {
                    a2.release();
                }
            }
            return hasCamera.booleanValue();
        }
        if (a != null) {
            a.release();
        }
        throw a0;
    }
    
    public static void takeDeviceScreenShot(String s) {
        label0: {
            java.io.IOException a = null;
            try {
                try {
                    Runtime.getRuntime().exec(new StringBuilder().append("screencap -p ").append(s).toString()).waitFor();
                    break label0;
                } catch(java.io.IOException a0) {
                    a = a0;
                }
            } catch(InterruptedException a1) {
                sLogger.e("Error while taking screen shot", (Throwable)a1);
                break label0;
            }
            sLogger.e("Error while taking screen shot", (Throwable)a);
        }
    }
}
