package com.navdy.hud.app.util;

public class CustomDimension {
    private android.util.TypedValue mAttribute;
    private float mValue;
    
    public CustomDimension(float f) {
        this.mValue = f;
    }
    
    public CustomDimension(android.util.TypedValue a) {
        this.mAttribute = a;
    }
    
    public static com.navdy.hud.app.util.CustomDimension getDimension(android.view.View a, android.content.res.TypedArray a0, int i, float f) {
        com.navdy.hud.app.util.CustomDimension a1 = null;
        label2: if (a.isInEditMode()) {
            String s = a0.getString(i);
            if (s != null) {
                boolean b = s.endsWith("%");
                label0: {
                    label1: {
                        if (b) {
                            break label1;
                        }
                        if (!s.endsWith("%p")) {
                            break label0;
                        }
                    }
                    android.util.TypedValue a2 = new android.util.TypedValue();
                    a0.getValue(i, a2);
                    a1 = new com.navdy.hud.app.util.CustomDimension(a2);
                    break label2;
                }
                a1 = new com.navdy.hud.app.util.CustomDimension(a0.getDimension(i, f));
            } else {
                a1 = new com.navdy.hud.app.util.CustomDimension(f);
            }
        } else {
            android.util.TypedValue a3 = new android.util.TypedValue();
            a0.getValue(i, a3);
            a1 = (a3.type != 0) ? (a3.type != 5) ? new com.navdy.hud.app.util.CustomDimension(a3) : new com.navdy.hud.app.util.CustomDimension(a0.getDimension(i, f)) : new com.navdy.hud.app.util.CustomDimension(f);
        }
        return a1;
    }
    
    public static boolean hasDimension(android.view.View a, android.content.res.TypedArray a0, int i) {
        boolean b = false;
        if (a.isInEditMode()) {
            b = a0.getString(i) != null;
        } else {
            android.util.TypedValue a1 = new android.util.TypedValue();
            a0.getValue(i, a1);
            b = a1.type != 0;
        }
        return b;
    }
    
    public float getSize(android.view.View a, float f, float f0) {
        float f1 = 0.0f;
        if (this.mAttribute == null) {
            f1 = this.mValue;
        } else {
            switch(this.mAttribute.type) {
                case 6: {
                    f1 = this.mAttribute.getFraction(f, f0);
                    break;
                }
                case 4: {
                    f1 = this.mAttribute.getFloat() * f;
                    break;
                }
                default: {
                    throw new IllegalArgumentException(new StringBuilder().append("Attribute must have type fraction or float - it is ").append(this.mAttribute.type).toString());
                }
            }
        }
        return f1;
    }
}
