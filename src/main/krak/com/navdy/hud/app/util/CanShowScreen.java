package com.navdy.hud.app.util;

abstract public interface CanShowScreen {
    abstract public void showScreen(mortar.Blueprint arg, flow.Flow$Direction arg0, int arg1, int arg2);
}
