package com.navdy.ancs;

final class AppleNotification$1 implements android.os.Parcelable$Creator {
    AppleNotification$1() {
    }
    
    public com.navdy.ancs.AppleNotification createFromParcel(android.os.Parcel a) {
        return new com.navdy.ancs.AppleNotification(a);
    }
    
    public Object createFromParcel(android.os.Parcel a) {
        return this.createFromParcel(a);
    }
    
    public com.navdy.ancs.AppleNotification[] newArray(int i) {
        return new com.navdy.ancs.AppleNotification[i];
    }
    
    public Object[] newArray(int i) {
        return this.newArray(i);
    }
}
