package com.navdy.ancs;

class IAncsServiceListener$Stub$Proxy implements com.navdy.ancs.IAncsServiceListener {
    private android.os.IBinder mRemote;
    
    IAncsServiceListener$Stub$Proxy(android.os.IBinder a) {
        this.mRemote = a;
    }
    
    public android.os.IBinder asBinder() {
        return this.mRemote;
    }
    
    public String getInterfaceDescriptor() {
        return "com.navdy.ancs.IAncsServiceListener";
    }
    
    public void onConnectionStateChange(int i) {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.ancs.IAncsServiceListener");
            a.writeInt(i);
            this.mRemote.transact(1, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void onNotification(com.navdy.ancs.AppleNotification a) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.ancs.IAncsServiceListener");
            if (a == null) {
                a0.writeInt(0);
            } else {
                a0.writeInt(1);
                a.writeToParcel(a0, 0);
            }
            this.mRemote.transact(2, a0, a1, 0);
            a1.readException();
        } catch(Throwable a2) {
            a1.recycle();
            a0.recycle();
            throw a2;
        }
        a1.recycle();
        a0.recycle();
    }
}
