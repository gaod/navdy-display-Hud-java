package com.navdy.obd;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public final class ScanSchedule implements Parcelable
{

    public static final Parcelable.Creator<ScanSchedule> CREATOR;
    Map<Integer, ScanSchedule.Scan> schedule;

    static {

        CREATOR = new Parcelable.Creator<ScanSchedule>() {
            public ScanSchedule createFromParcel(final Parcel parcel) {
                return new ScanSchedule(parcel);
            }
            
            public ScanSchedule[] newArray(final int n) {
                return new ScanSchedule[n];
            }
        };
    }
    
    public ScanSchedule() {
        this.schedule = new HashMap<Integer, Scan>();
    }
    
    public ScanSchedule(Parcel parcel) {
        this.readFromParcel(parcel);
    }
    
    public ScanSchedule(final ScanSchedule scanSchedule) {
        this.schedule = new HashMap<Integer, Scan>(scanSchedule.schedule);
    }
    
    public void addPid(final int n, final int scanInterval) {
        final Scan scan = this.schedule.get(n);
        if (scan != null) {
            if (scanInterval < scan.scanInterval) {
                scan.scanInterval = scanInterval;
            }
        }
        else {
            this.schedule.put(n, new Scan(n, scanInterval));
        }
    }
    
    public void addPids(final List<Pid> list, final int n) {
        final Iterator<Pid> iterator = list.iterator();
        while (iterator.hasNext()) {
            this.addPid(iterator.next().getId(), n);
        }
    }
    
    public int describeContents() {
        return 0;
    }
    
    public List<Scan> getScanList() {
        return new ArrayList<Scan>(this.schedule.values());
    }
    
    public boolean isEmpty() {
        return this.schedule.isEmpty();
    }
    
    public void merge(final ScanSchedule scanSchedule) {
        final Iterator<Map.Entry<Integer, Scan>> iterator = scanSchedule.schedule.entrySet().iterator();
        while (iterator.hasNext()) {
            final Scan scan = iterator.next().getValue();
            this.addPid(scan.pid, scan.scanInterval);
        }
    }
    
    public void readFromParcel(final Parcel parcel) {
        this.schedule = new HashMap<Integer, Scan>();
        for (int int1 = parcel.readInt(), i = 0; i < int1; ++i) {
            this.addPid(parcel.readInt(), parcel.readInt());
        }
    }
    
    public Scan remove(final int n) {
        return this.schedule.remove(n);
    }
    
    public int size() {
        return this.schedule.size();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ScanSchedule{ schedule=");
        int n = 1;
        if (this.schedule != null) {
            final Iterator<Map.Entry<Integer, Scan>> iterator = this.schedule.entrySet().iterator();
            while (iterator.hasNext()) {
                final Scan scan = iterator.next().getValue();
                if (n == 0) {
                    sb.append(",");
                }
                else {
                    n = 0;
                }
                sb.append("[").append(scan.pid).append(", ").append(scan.scanInterval).append("ms]");
            }
        }
        else {
            sb.append("null");
        }
        sb.append(" }");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeInt(this.schedule.size());
        final Iterator<Map.Entry<Integer, Scan>> iterator = this.schedule.entrySet().iterator();
        while (iterator.hasNext()) {
            final Scan scan = iterator.next().getValue();
            parcel.writeInt(scan.pid);
            parcel.writeInt(scan.scanInterval);
        }
    }
    
    public static class Scan
    {
        public int pid;
        public int scanInterval;
        
        public Scan(final int pid, final int scanInterval) {
            this.pid = pid;
            this.scanInterval = scanInterval;
        }
    }
}
