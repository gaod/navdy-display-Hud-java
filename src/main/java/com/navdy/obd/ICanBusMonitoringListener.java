package com.navdy.obd;

import android.os.Parcel;
import android.os.IBinder;
import android.os.Binder;
import android.os.RemoteException;
import android.os.IInterface;

public interface ICanBusMonitoringListener extends IInterface
{
    int getGpsSpeed() throws RemoteException;
    
    double getLatitude() throws RemoteException;
    
    double getLongitude() throws RemoteException;
    
    String getMake() throws RemoteException;
    
    String getModel() throws RemoteException;
    
    String getYear() throws RemoteException;
    
    boolean isMonitoringLimitReached() throws RemoteException;
    
    void onCanBusMonitorSuccess(final int p0) throws RemoteException;
    
    void onCanBusMonitoringError(final String p0) throws RemoteException;
    
    void onNewDataAvailable(final String p0) throws RemoteException;
    
    public abstract static class Stub extends Binder implements ICanBusMonitoringListener
    {
        private static final String DESCRIPTOR = "com.navdy.obd.ICanBusMonitoringListener";
        static final int TRANSACTION_getGpsSpeed = 4;
        static final int TRANSACTION_getLatitude = 5;
        static final int TRANSACTION_getLongitude = 6;
        static final int TRANSACTION_getMake = 8;
        static final int TRANSACTION_getModel = 9;
        static final int TRANSACTION_getYear = 10;
        static final int TRANSACTION_isMonitoringLimitReached = 7;
        static final int TRANSACTION_onCanBusMonitorSuccess = 3;
        static final int TRANSACTION_onCanBusMonitoringError = 2;
        static final int TRANSACTION_onNewDataAvailable = 1;
        
        public Stub() {
            this.attachInterface((IInterface)this, "com.navdy.obd.ICanBusMonitoringListener");
        }
        
        public static ICanBusMonitoringListener asInterface(final IBinder binder) {
            ICanBusMonitoringListener canBusMonitoringListener;
            if (binder == null) {
                canBusMonitoringListener = null;
            }
            else {
                final IInterface queryLocalInterface = binder.queryLocalInterface("com.navdy.obd.ICanBusMonitoringListener");
                if (queryLocalInterface != null && queryLocalInterface instanceof ICanBusMonitoringListener) {
                    canBusMonitoringListener = (ICanBusMonitoringListener)queryLocalInterface;
                }
                else {
                    canBusMonitoringListener = new Proxy(binder);
                }
            }
            return canBusMonitoringListener;
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(int gpsSpeed, final Parcel parcel, final Parcel parcel2, final int n) throws RemoteException {
            boolean onTransact = true;
            switch (gpsSpeed) {
                default:
                    onTransact = super.onTransact(gpsSpeed, parcel, parcel2, n);
                    break;
                case 1598968902:
                    parcel2.writeString("com.navdy.obd.ICanBusMonitoringListener");
                    break;
                case 1:
                    parcel.enforceInterface("com.navdy.obd.ICanBusMonitoringListener");
                    this.onNewDataAvailable(parcel.readString());
                    parcel2.writeNoException();
                    break;
                case 2:
                    parcel.enforceInterface("com.navdy.obd.ICanBusMonitoringListener");
                    this.onCanBusMonitoringError(parcel.readString());
                    parcel2.writeNoException();
                    break;
                case 3:
                    parcel.enforceInterface("com.navdy.obd.ICanBusMonitoringListener");
                    this.onCanBusMonitorSuccess(parcel.readInt());
                    parcel2.writeNoException();
                    break;
                case 4:
                    parcel.enforceInterface("com.navdy.obd.ICanBusMonitoringListener");
                    gpsSpeed = this.getGpsSpeed();
                    parcel2.writeNoException();
                    parcel2.writeInt(gpsSpeed);
                    break;
                case 5: {
                    parcel.enforceInterface("com.navdy.obd.ICanBusMonitoringListener");
                    final double latitude = this.getLatitude();
                    parcel2.writeNoException();
                    parcel2.writeDouble(latitude);
                    break;
                }
                case 6: {
                    parcel.enforceInterface("com.navdy.obd.ICanBusMonitoringListener");
                    final double longitude = this.getLongitude();
                    parcel2.writeNoException();
                    parcel2.writeDouble(longitude);
                    break;
                }
                case 7: {
                    parcel.enforceInterface("com.navdy.obd.ICanBusMonitoringListener");
                    final boolean monitoringLimitReached = this.isMonitoringLimitReached();
                    parcel2.writeNoException();
                    if (monitoringLimitReached) {
                        gpsSpeed = 1;
                    }
                    else {
                        gpsSpeed = 0;
                    }
                    parcel2.writeInt(gpsSpeed);
                    break;
                }
                case 8: {
                    parcel.enforceInterface("com.navdy.obd.ICanBusMonitoringListener");
                    final String make = this.getMake();
                    parcel2.writeNoException();
                    parcel2.writeString(make);
                    break;
                }
                case 9: {
                    parcel.enforceInterface("com.navdy.obd.ICanBusMonitoringListener");
                    final String model = this.getModel();
                    parcel2.writeNoException();
                    parcel2.writeString(model);
                    break;
                }
                case 10: {
                    parcel.enforceInterface("com.navdy.obd.ICanBusMonitoringListener");
                    final String year = this.getYear();
                    parcel2.writeNoException();
                    parcel2.writeString(year);
                    break;
                }
            }
            return onTransact;
        }
        
        private static class Proxy implements ICanBusMonitoringListener
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            @Override
            public int getGpsSpeed() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICanBusMonitoringListener");
                    this.mRemote.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            public String getInterfaceDescriptor() {
                return "com.navdy.obd.ICanBusMonitoringListener";
            }
            
            @Override
            public double getLatitude() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICanBusMonitoringListener");
                    this.mRemote.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readDouble();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public double getLongitude() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICanBusMonitoringListener");
                    this.mRemote.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readDouble();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public String getMake() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICanBusMonitoringListener");
                    this.mRemote.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public String getModel() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICanBusMonitoringListener");
                    this.mRemote.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public String getYear() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICanBusMonitoringListener");
                    this.mRemote.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean isMonitoringLimitReached() throws RemoteException {
                boolean b = false;
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICanBusMonitoringListener");
                    this.mRemote.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void onCanBusMonitorSuccess(final int n) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICanBusMonitoringListener");
                    obtain.writeInt(n);
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void onCanBusMonitoringError(final String s) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICanBusMonitoringListener");
                    obtain.writeString(s);
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void onNewDataAvailable(final String s) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.ICanBusMonitoringListener");
                    obtain.writeString(s);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
