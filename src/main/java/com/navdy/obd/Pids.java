package com.navdy.obd;

public class Pids
{
    public static final int FUEL_SYSTEM_STATUS = 3;
    public static final int ENGINE_COOLANT_TEMPERATURE = 5;
    public static final int MANIFOLD_AIR_PRESSURE = 11;
    public static final int ENGINE_RPM = 12;
    public static final int VEHICLE_SPEED = 13;
    public static final int INTAKE_AIR_TEMPERATURE = 15;
    public static final int MASS_AIRFLOW = 16;
    public static final int THROTTLE_POSITION = 17;
    public static final int DISTANCE_TRAVELLED = 49;
    public static final int FUEL_LEVEL = 47;

    public static final int AMBIENT_AIR_TEMRERATURE = 70;
    public static final int ENGINE_OIL_TEMPERATURE = 92;

    public static final int CUSTOM_PIDS = 64;
    public static final int CUSTOM_PID_START = 256;
    public static final int INSTANTANEOUS_FUEL_CONSUMPTION = 256;
    public static final int ENGINE_OIL_PRESSURE = 258;
    public static final int TOTAL_VEHICLE_DISTANCE = 260;
    public static final int ENGINE_TRIP_FUEL = 259;
    public static final int DISTANCE_TO_EMPTY = 257;
    public static final int MAX_PID = 320;
}
