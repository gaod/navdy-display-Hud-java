package com.navdy.service.library.events.connection;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;

public final class ConnectionRequest extends Message {
    public static final Action DEFAULT_ACTION = Action.CONNECTION_START_SEARCH;
    public static final String DEFAULT_REMOTEDEVICEID = "";
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final Action action;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String remoteDeviceId;

    public enum Action implements ProtoEnum {
        CONNECTION_START_SEARCH(1),
        CONNECTION_STOP_SEARCH(2),
        CONNECTION_SELECT(3),
        PHONE_LOCATION_START(4),
        PHONE_LOCATION_STOP(5);

        private final int value;

        private Action(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public static final class Builder extends com.squareup.wire.Message.Builder<ConnectionRequest> {
        public Action action;
        public String remoteDeviceId;

        public Builder() {
            super(null);
        }

        public Builder(ConnectionRequest message) {
            super(message);
            if (message != null) {
                this.action = message.action;
                this.remoteDeviceId = message.remoteDeviceId;
            }
        }

        public Builder action(Action action) {
            this.action = action;
            return this;
        }

        public Builder remoteDeviceId(String remoteDeviceId) {
            this.remoteDeviceId = remoteDeviceId;
            return this;
        }

        public ConnectionRequest build() {
            checkRequiredFields();
            return new ConnectionRequest(this);
        }
    }

    public ConnectionRequest(Action action, String remoteDeviceId) {
        this.action = action;
        this.remoteDeviceId = remoteDeviceId;
    }

    private ConnectionRequest(Builder builder) {
        this(builder.action, builder.remoteDeviceId);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof ConnectionRequest)) {
            return false;
        }
        ConnectionRequest o = (ConnectionRequest) other;
        if (equals( this.action,  o.action) && equals( this.remoteDeviceId,  o.remoteDeviceId)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        if (this.action != null) {
            result = this.action.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.remoteDeviceId != null) {
            i = this.remoteDeviceId.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
