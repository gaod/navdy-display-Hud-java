package com.navdy.service.library.util;

public class NetworkActivityTracker extends Thread {
    final private static int LOGGING_INTERVAL = 60000;
    final private static com.navdy.service.library.util.NetworkActivityTracker sInstance;
    final private static com.navdy.service.library.log.Logger sLogger;
    private java.util.concurrent.atomic.AtomicLong bytesReceived;
    private java.util.concurrent.atomic.AtomicLong bytesSent;
    private double highestReceivedBandwidth;
    private double highestSentBandwidth;
    private long starttime;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.util.NetworkActivityTracker.class);
        sInstance = new com.navdy.service.library.util.NetworkActivityTracker();
    }
    
    private NetworkActivityTracker() {
        this.bytesSent = new java.util.concurrent.atomic.AtomicLong(0L);
        this.bytesReceived = new java.util.concurrent.atomic.AtomicLong(0L);
        this.starttime = System.currentTimeMillis();
        this.highestSentBandwidth = 0.0;
        this.highestReceivedBandwidth = 0.0;
    }
    
    public static com.navdy.service.library.util.NetworkActivityTracker getInstance() {
        return sInstance;
    }
    
    public void addBytesReceived(int i) {
        this.bytesReceived.addAndGet((long)i);
    }
    
    public void addBytesSent(int i) {
        this.bytesSent.addAndGet((long)i);
    }
    
    public void run() {
        long j = this.bytesSent.get();
        long j0 = this.bytesReceived.get();
        long j1 = this.starttime;
        boolean b = false;
        while(true) {
            try {
                com.navdy.service.library.util.NetworkActivityTracker.sleep(60000L);
            } catch(InterruptedException ignoredException) {
                b = true;
            }
            long j2 = this.bytesSent.get();
            long j3 = this.bytesReceived.get();
            long j4 = System.currentTimeMillis();
            long j5 = j2 - j;
            long j6 = j3 - j0;
            long j7 = j4 - j1;
            long j8 = this.starttime;
            double d = (double)j5 * 1000.0 / (double)j7;
            double d0 = (double)j6 * 1000.0 / (double)j7;
            this.highestSentBandwidth = Math.max(this.highestSentBandwidth, d);
            this.highestReceivedBandwidth = Math.max(this.highestReceivedBandwidth, d0);
            int i = (j5 < 0L) ? -1 : (j5 == 0L) ? 0 : 1;
            label0: {
                label1: {
                    if (i > 0) {
                        break label1;
                    }
                    if (j6 <= 0L) {
                        break label0;
                    }
                }
                Object[] a = new Object[10];
                a[0] = Long.valueOf(j4 - j8);
                a[1] = Long.valueOf(j7);
                a[2] = Long.valueOf(j5);
                a[3] = Long.valueOf(j6);
                a[4] = Long.valueOf(j2);
                a[5] = Long.valueOf(j3);
                a[6] = Double.valueOf(d);
                a[7] = Double.valueOf(d0);
                a[8] = Double.valueOf(this.highestSentBandwidth);
                a[9] = Double.valueOf(this.highestReceivedBandwidth);
                String s = String.format("Total time: %dms interval: %dms sent: %d bytes received: %d bytes totalSent: %d bytes totalReceived: %d bytes sentBandwidth: %.2f bytes/sec receivedBandwidth: %.2f bytes/sec maxSentBandwidth: %.2f bytes/sec maxReceivedBandwidth: %.2f bytes/sec", a);
                sLogger.i(s);
            }
            if (b) {
                return;
            }
            j = j2;
            j0 = j3;
            j1 = j4;
        }
    }
}
