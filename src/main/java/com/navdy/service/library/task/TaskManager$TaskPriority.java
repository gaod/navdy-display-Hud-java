package com.navdy.service.library.task;


public enum TaskManager$TaskPriority {
    LOW(0),
    NORMAL(1),
    HIGH(2);

    private int value;
    TaskManager$TaskPriority(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class TaskManager$TaskPriority extends Enum {
//    final private static com.navdy.service.library.task.TaskManager$TaskPriority[] $VALUES;
//    final public static com.navdy.service.library.task.TaskManager$TaskPriority HIGH;
//    final public static com.navdy.service.library.task.TaskManager$TaskPriority LOW;
//    final public static com.navdy.service.library.task.TaskManager$TaskPriority NORMAL;
//    final private int val;
//    
//    static {
//        LOW = new com.navdy.service.library.task.TaskManager$TaskPriority("LOW", 0, 1);
//        NORMAL = new com.navdy.service.library.task.TaskManager$TaskPriority("NORMAL", 1, 2);
//        HIGH = new com.navdy.service.library.task.TaskManager$TaskPriority("HIGH", 2, 3);
//        com.navdy.service.library.task.TaskManager$TaskPriority[] a = new com.navdy.service.library.task.TaskManager$TaskPriority[3];
//        a[0] = LOW;
//        a[1] = NORMAL;
//        a[2] = HIGH;
//        $VALUES = a;
//    }
//    
//    private TaskManager$TaskPriority(String s, int i, int i0) {
//        super(s, i);
//        this.val = i0;
//    }
//    
//    public static com.navdy.service.library.task.TaskManager$TaskPriority valueOf(String s) {
//        return (com.navdy.service.library.task.TaskManager$TaskPriority)Enum.valueOf(com.navdy.service.library.task.TaskManager$TaskPriority.class, s);
//    }
//    
//    public static com.navdy.service.library.task.TaskManager$TaskPriority[] values() {
//        return $VALUES.clone();
//    }
//    
//    public int getValue() {
//        return this.val;
//    }
//}
//