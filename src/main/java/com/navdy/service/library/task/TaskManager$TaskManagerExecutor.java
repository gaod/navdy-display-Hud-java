package com.navdy.service.library.task;

public class TaskManager$TaskManagerExecutor extends java.util.concurrent.ThreadPoolExecutor {
    TaskManager$TaskManagerExecutor(int i, int i0, long j, java.util.concurrent.TimeUnit a, java.util.concurrent.BlockingQueue a0, java.util.concurrent.ThreadFactory a1) {
        super(i, i0, j, a, a0, a1);
    }
    
    protected java.util.concurrent.RunnableFuture newTaskFor(Runnable a, Object a0) {
        return (java.util.concurrent.RunnableFuture)new com.navdy.service.library.task.TaskManager$PriorityTask((com.navdy.service.library.task.TaskManager$PriorityRunnable)a, a0);
    }
}
