package com.navdy.service.library.log;

public class FilteringAppender implements com.navdy.service.library.log.LogAppender {
    private com.navdy.service.library.log.LogAppender baseAppender;
    private com.navdy.service.library.log.Filter filter;
    
    public FilteringAppender(com.navdy.service.library.log.LogAppender a, com.navdy.service.library.log.Filter a0) {
        this.baseAppender = a;
        this.filter = a0;
    }
    
    public void close() {
        this.baseAppender.close();
    }
    
    public void d(String s, String s0) {
        if (this.filter.matches(3, s, s0)) {
            this.baseAppender.d(s, s0);
        }
    }
    
    public void d(String s, String s0, Throwable a) {
        if (this.filter.matches(3, s, s0)) {
            this.baseAppender.d(s, s0, a);
        }
    }
    
    public void e(String s, String s0) {
        if (this.filter.matches(6, s, s0)) {
            this.baseAppender.e(s, s0);
        }
    }
    
    public void e(String s, String s0, Throwable a) {
        if (this.filter.matches(6, s, s0)) {
            this.baseAppender.e(s, s0, a);
        }
    }
    
    public void flush() {
        this.baseAppender.flush();
    }
    
    public void i(String s, String s0) {
        if (this.filter.matches(4, s, s0)) {
            this.baseAppender.i(s, s0);
        }
    }
    
    public void i(String s, String s0, Throwable a) {
        if (this.filter.matches(4, s, s0)) {
            this.baseAppender.i(s, s0, a);
        }
    }
    
    public void v(String s, String s0) {
        if (this.filter.matches(2, s, s0)) {
            this.baseAppender.v(s, s0);
        }
    }
    
    public void v(String s, String s0, Throwable a) {
        if (this.filter.matches(2, s, s0)) {
            this.baseAppender.v(s, s0, a);
        }
    }
    
    public void w(String s, String s0) {
        if (this.filter.matches(5, s, s0)) {
            this.baseAppender.w(s, s0);
        }
    }
    
    public void w(String s, String s0, Throwable a) {
        if (this.filter.matches(5, s, s0)) {
            this.baseAppender.w(s, s0, a);
        }
    }
}
