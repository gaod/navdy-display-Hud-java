package com.navdy.service.library.device.link;

public class IOThread extends Thread {
    final private static int SHUTDOWN_TIMEOUT = 1000;
    protected volatile boolean closing;
    final protected com.navdy.service.library.log.Logger logger;
    
    public IOThread() {
        this.logger = new com.navdy.service.library.log.Logger((this).getClass());
        this.closing = false;
    }
    
    public void cancel() {
        this.closing = true;
        this.interrupt();
        try {
            this.join(1000L);
        } catch(InterruptedException ignoredException) {
            this.logger.e("Interrupted");
        }
        if (this.isAlive()) {
            this.logger.w("Thread still alive after join");
        }
    }
    
    public boolean isClosing() {
        return this.closing;
    }
}
