package com.navdy.service.library.device;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.Nullable;

import com.navdy.service.library.device.connection.Connection;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.device.connection.ConnectionType;
import com.navdy.service.library.device.link.EventRequest;
import com.navdy.service.library.device.link.Link;
import com.navdy.service.library.device.link.LinkListener;
import com.navdy.service.library.device.link.LinkManager;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.Ext_NavdyEvent;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.connection.NetworkLinkReady;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.util.Listenable;
import com.squareup.wire.Message;
import com.squareup.wire.Wire;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.navdy.service.library.events.NavdyEventUtil.eventFromMessage;

public class RemoteDevice extends Listenable<RemoteDevice.Listener> implements Connection.Listener, LinkListener {
    public static final int MAX_PACKET_SIZE = 524288;
    private static final Object lock = new Object();
    public static final RemoteDevice.LastSeenDeviceInfo sLastSeenDeviceInfo = new RemoteDevice.LastSeenDeviceInfo();
    public static final Logger sLogger = new Logger(RemoteDevice.class);
    protected boolean convertToNavdyEvent;
    public long lastMessageSentTime;
    protected Connection mActiveConnection;
    protected ConnectionInfo mConnectionInfo;
    protected final Context mContext;
    protected NavdyDeviceId mDeviceId;
    protected DeviceInfo mDeviceInfo;
    protected final LinkedBlockingDeque<EventRequest> mEventQueue;
    protected final Handler mHandler;
    private Link mLink;
    private RemoteDevice.LinkStatus mLinkStatus;
    private volatile boolean mNetworkLinkReady;
    protected AtomicBoolean mNetworkReadyEventDispatched;
    private Wire mWire;

    protected interface EventDispatcher extends Listenable.EventDispatcher<RemoteDevice, RemoteDevice.Listener> {
    }

    public static class LastSeenDeviceInfo {
        /* access modifiers changed from: private */
        public DeviceInfo mDeviceInfo;

        public DeviceInfo getDeviceInfo() {
            return this.mDeviceInfo;
        }
    }

    public enum LinkStatus {
        DISCONNECTED,
        LINK_ESTABLISHED,
        CONNECTED
    }

    public interface Listener extends com.navdy.service.library.util.Listenable.Listener {
        void onDeviceConnectFailure(RemoteDevice remoteDevice, Connection.ConnectionFailureCause connectionFailureCause);

        void onDeviceConnected(RemoteDevice remoteDevice);

        void onDeviceConnecting(RemoteDevice remoteDevice);

        void onDeviceDisconnected(RemoteDevice remoteDevice, Connection.DisconnectCause disconnectCause);

        void onNavdyEventReceived(RemoteDevice remoteDevice, NavdyEvent navdyEvent);

        void onNavdyEventReceived(RemoteDevice remoteDevice, byte[] bArr);
    }

    public interface PostEventHandler {
        void onComplete(RemoteDevice.PostEventStatus postEventStatus);
    }

    public enum PostEventStatus {
        SUCCESS,
        DISCONNECTED,
        SEND_FAILED,
        UNKNOWN_FAILURE
    }

    public RemoteDevice(Context context, NavdyDeviceId navdyDeviceId, boolean z) {
        this.mLinkStatus = RemoteDevice.LinkStatus.DISCONNECTED;
        this.mNetworkLinkReady = false;
        this.mNetworkReadyEventDispatched = new AtomicBoolean(false);
        this.mContext = context;
        this.mDeviceId = navdyDeviceId;
        this.mHandler = new Handler(Looper.getMainLooper());
        this.mEventQueue = new LinkedBlockingDeque<>();
        this.convertToNavdyEvent = z;
        if (z) {
            this.mWire = new Wire(Ext_NavdyEvent.class);
        }
    }

    public RemoteDevice(Context context, ConnectionInfo connectionInfo, boolean z) {
        this(context, connectionInfo.getDeviceId(), z);
        this.mConnectionInfo = connectionInfo;
    }

    private void dispatchLocalEvent(Message message) {
        NavdyEvent eventFromMessage = eventFromMessage(message);
        if (this.convertToNavdyEvent) {
            dispatchNavdyEvent(eventFromMessage);
        } else {
            dispatchNavdyEvent(eventFromMessage.toByteArray());
        }
    }

    private void dispatchStateChangeEvents(RemoteDevice.LinkStatus linkStatus, RemoteDevice.LinkStatus linkStatus2) {
        String navdyDeviceId = getDeviceId().toString();
        switch (linkStatus) {
            case CONNECTED:
                if (linkStatus2 == RemoteDevice.LinkStatus.DISCONNECTED) {
                    dispatchLocalEvent(new ConnectionStateChange(navdyDeviceId, ConnectionStateChange.ConnectionState.CONNECTION_LINK_ESTABLISHED));
                }
                dispatchLocalEvent(new ConnectionStateChange(navdyDeviceId, ConnectionStateChange.ConnectionState.CONNECTION_CONNECTED));
                if (this.mNetworkLinkReady && this.mNetworkReadyEventDispatched.compareAndSet(false, true)) {
                    dispatchLocalEvent(new NetworkLinkReady());
                    sLogger.d("dispatchStateChangeEvents: dispatching the Network Link Ready message");
                    return;
                }
                return;
            case DISCONNECTED:
                if (linkStatus2 == RemoteDevice.LinkStatus.CONNECTED) {
                    dispatchLocalEvent(new ConnectionStateChange(navdyDeviceId, ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED));
                }
                dispatchLocalEvent(new ConnectionStateChange(navdyDeviceId, ConnectionStateChange.ConnectionState.CONNECTION_LINK_LOST));
                return;
            case LINK_ESTABLISHED:
                if (linkStatus2 == RemoteDevice.LinkStatus.CONNECTED) {
                    dispatchLocalEvent(new ConnectionStateChange(navdyDeviceId, ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED));
                    return;
                } else {
                    dispatchLocalEvent(new ConnectionStateChange(navdyDeviceId, ConnectionStateChange.ConnectionState.CONNECTION_LINK_ESTABLISHED));
                    return;
                }
            default:
        }
    }

    private void markDisconnected(Connection.DisconnectCause disconnectCause) {
        removeActiveConnection();
        dispatchDisconnectEvent(disconnectCause);
    }

    private void setLinkStatus(RemoteDevice.LinkStatus linkStatus) {
        if (this.mLinkStatus != linkStatus) {
            RemoteDevice.LinkStatus linkStatus2 = this.mLinkStatus;
            this.mLinkStatus = linkStatus;
            dispatchStateChangeEvents(this.mLinkStatus, linkStatus2);
        }
    }

    public boolean connect() {
        boolean z;
        if (getConnectionStatus() != Connection.Status.DISCONNECTED) {
            return false;
        }
        if (this.mConnectionInfo == null) {
            sLogger.e("can't connect without a connectionInfo");
            return false;
        } else if (setActiveConnection(Connection.instantiateFromConnectionInfo(this.mContext, this.mConnectionInfo))) {
            synchronized (lock) {
                z = this.mActiveConnection.connect() && getConnectionStatus() == Connection.Status.CONNECTING;
            }
            if (z) {
                dispatchConnectingEvent();
                sLogger.i("Starting connection");
                return true;
            }
            sLogger.e("Unable to connect");
            removeActiveConnection();
            return false;
        } else {
            sLogger.e("unable to find connection of type: " + this.mConnectionInfo.getType());
            return false;
        }
    }

    public boolean disconnect() {
        Connection connection;
        boolean z;
        synchronized (this) {
            if (getConnectionStatus() == Connection.Status.DISCONNECTED) {
                sLogger.e("can't disconnect; already disconnected");
                z = true;
            } else {
                synchronized (lock) {
                    connection = this.mActiveConnection;
                    this.mActiveConnection = null;
                }
                z = connection != null && connection.disconnect();
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public void dispatchConnectEvent() {
        dispatchToListeners(new RemoteDevice.EventDispatcher() {
            public void dispatchEvent(RemoteDevice remoteDevice, RemoteDevice.Listener listener) {
                listener.onDeviceConnected(remoteDevice);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void dispatchConnectFailureEvent(final Connection.ConnectionFailureCause connectionFailureCause) {
        dispatchToListeners(new RemoteDevice.EventDispatcher() {
            public void dispatchEvent(RemoteDevice remoteDevice, RemoteDevice.Listener listener) {
                listener.onDeviceConnectFailure(remoteDevice, connectionFailureCause);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void dispatchConnectingEvent() {
        dispatchToListeners(new RemoteDevice.EventDispatcher() {
            public void dispatchEvent(RemoteDevice remoteDevice, RemoteDevice.Listener listener) {
                listener.onDeviceConnecting(remoteDevice);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void dispatchDisconnectEvent(final Connection.DisconnectCause disconnectCause) {
        dispatchToListeners(new RemoteDevice.EventDispatcher() {
            public void dispatchEvent(RemoteDevice remoteDevice, RemoteDevice.Listener listener) {
                listener.onDeviceDisconnected(remoteDevice, disconnectCause);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void dispatchNavdyEvent(final NavdyEvent navdyEvent) {
        dispatchToListeners(new RemoteDevice.EventDispatcher() {
            public void dispatchEvent(RemoteDevice remoteDevice, RemoteDevice.Listener listener) {
                listener.onNavdyEventReceived(remoteDevice, navdyEvent);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void dispatchNavdyEvent(final byte[] bArr) {
        dispatchToListeners(new RemoteDevice.EventDispatcher() {
            public void dispatchEvent(RemoteDevice remoteDevice, RemoteDevice.Listener listener) {
                listener.onNavdyEventReceived(remoteDevice, bArr);
            }
        });
    }

    @Nullable
    public ConnectionInfo getActiveConnectionInfo() {
        synchronized (lock) {
            if (this.mActiveConnection == null) {
                return null;
            }
            ConnectionInfo connectionInfo = this.mActiveConnection.getConnectionInfo();
            return connectionInfo;
        }
    }

    public ConnectionInfo getConnectionInfo() {
        return this.mConnectionInfo;
    }

    public Connection.Status getConnectionStatus() {
        Connection.Status status;
        synchronized (lock) {
            status = this.mActiveConnection == null ? Connection.Status.DISCONNECTED : this.mActiveConnection.getStatus();
        }
        return status;
    }

    public NavdyDeviceId getDeviceId() {
        return this.mDeviceId;
    }

    public DeviceInfo getDeviceInfo() {
        return this.mDeviceInfo;
    }

    public int getLinkBandwidthLevel() {
        if (this.mLink == null || this.mLinkStatus != RemoteDevice.LinkStatus.CONNECTED) {
            return -1;
        }
        return this.mLink.getBandWidthLevel();
    }

    public RemoteDevice.LinkStatus getLinkStatus() {
        return this.mLinkStatus;
    }

    public boolean isConnected() {
        return getConnectionStatus() == Connection.Status.CONNECTED;
    }

    public boolean isConnecting() {
        return getConnectionStatus() == Connection.Status.CONNECTING;
    }

    public void linkEstablished(ConnectionType connectionType) {
        synchronized (this) {
            switch (connectionType) {
                case BT_PROTOBUF:
                case TCP_PROTOBUF:
                case EA_PROTOBUF:
                    setLinkStatus(RemoteDevice.LinkStatus.CONNECTED);
                    if (this.mLink != null) {
                        this.mLink.setBandwidthLevel(1);
                        break;
                    }
                    break;
                case BT_IAP2_LINK:
                    setLinkStatus(RemoteDevice.LinkStatus.LINK_ESTABLISHED);
                    if (this.mLink != null) {
                        this.mLink.setBandwidthLevel(1);
                        break;
                    }
                    break;
                default:
                    sLogger.e("Unknown connection type: " + connectionType);
                    break;
            }
        }
    }

    public void linkLost(ConnectionType connectionType, Connection.DisconnectCause disconnectCause) {
        synchronized (this) {
            if (getConnectionStatus() == Connection.Status.DISCONNECTED) {
                disconnectCause = Connection.DisconnectCause.NORMAL;
            }
            this.mNetworkReadyEventDispatched.set(false);
            this.mNetworkLinkReady = false;
            switch (connectionType) {
                case BT_PROTOBUF:
                case TCP_PROTOBUF:
                    setLinkStatus(RemoteDevice.LinkStatus.DISCONNECTED);
                    markDisconnected(disconnectCause);
                    break;
                case EA_PROTOBUF:
                    if (getLinkStatus() == RemoteDevice.LinkStatus.CONNECTED) {
                        setLinkStatus(RemoteDevice.LinkStatus.LINK_ESTABLISHED);
                        break;
                    }
                    break;
                case BT_IAP2_LINK:
                    setLinkStatus(RemoteDevice.LinkStatus.DISCONNECTED);
                    markDisconnected(disconnectCause);
                    break;
                default:
                    sLogger.e("Unknown connection type: " + connectionType);
                    break;
            }
        }
    }

    public void onConnected(Connection connection) {
        synchronized (lock) {
            if (this.mActiveConnection == connection) {
                sLogger.i("Connected");
                dispatchConnectEvent();
            } else {
                sLogger.e("Received connect event for unknown connection");
            }
        }
    }

    public void onConnectionFailed(Connection connection, Connection.ConnectionFailureCause connectionFailureCause) {
        synchronized (lock) {
            if (this.mActiveConnection == connection) {
                removeActiveConnection();
            }
        }
        sLogger.e("Connection failed: " + connectionFailureCause);
        dispatchConnectFailureEvent(connectionFailureCause);
    }

    public void onDisconnected(Connection connection, Connection.DisconnectCause disconnectCause) {
        synchronized (lock) {
            if (this.mActiveConnection == connection) {
                removeActiveConnection();
            }
        }
        Object connectionInfo = connection.getConnectionInfo();
        StringBuilder append = new StringBuilder().append("Disconnected ");
        if (connectionInfo == null) {
            connectionInfo = DeviceInfo.DEFAULT_DEVICEMAKE;
        }
        sLogger.i(append.append(connectionInfo).append(" - ").append(disconnectCause).toString());
        dispatchDisconnectEvent(disconnectCause);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0011  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onNavdyEventReceived(byte[] bArr) {
        NavdyEvent navdyEvent;
        if (this.convertToNavdyEvent) {
            try {
                navdyEvent = this.mWire.parseFrom(bArr, NavdyEvent.class);
                if (navdyEvent != null) {
                    dispatchNavdyEvent(navdyEvent);
                    return;
                }
            } catch (IOException th) {
                sLogger.e("Ignoring invalid navdy event[" + Arrays.toString(bArr) + "]", th);
            }
        }
        dispatchNavdyEvent(bArr);
    }

    public void onNetworkLinkReady() {
        this.mNetworkLinkReady = true;
        if (getLinkStatus() != RemoteDevice.LinkStatus.CONNECTED) {
            sLogger.d("Network Link is ready, but state is not connected");
        } else if (this.mNetworkReadyEventDispatched.compareAndSet(false, true)) {
            dispatchLocalEvent(new NetworkLinkReady());
            sLogger.d("onNetworkLinkReady: dispatching the Network Link Ready message");
        }
    }

    public boolean postEvent(NavdyEvent navdyEvent) {
        return postEvent(navdyEvent, null);
    }

    public boolean postEvent(NavdyEvent navdyEvent, RemoteDevice.PostEventHandler postEventHandler) {
        return postEvent(navdyEvent.toByteArray(), postEventHandler);
    }

    public boolean postEvent(Message message) {
        return postEvent(eventFromMessage(message), null);
    }

    public boolean postEvent(byte[] bArr) {
        return postEvent(bArr, null);
    }

    public boolean postEvent(byte[] bArr, RemoteDevice.PostEventHandler postEventHandler) {
        boolean add = this.mEventQueue.add(new EventRequest(this.mHandler, bArr, postEventHandler));
        this.lastMessageSentTime = SystemClock.elapsedRealtime();
        if (sLogger.isLoggable(2)) {
            sLogger.v("NAVDY-PACKET Event queue size [" + this.mEventQueue.size() + "]");
        }
        return add;
    }

    public void removeActiveConnection() {
        synchronized (lock) {
            if (this.mActiveConnection != null) {
                this.mActiveConnection.removeListener(this);
                this.mActiveConnection = null;
            }
        }
    }

    public boolean setActiveConnection(Connection connection) {
        boolean z;
        synchronized (lock) {
            this.mActiveConnection = connection;
            if (this.mActiveConnection != null) {
                this.mActiveConnection.addListener(this);
                this.mConnectionInfo = this.mActiveConnection.getConnectionInfo();
            }
            if (isConnected()) {
                dispatchConnectEvent();
            }
            z = this.mActiveConnection != null;
        }
        return z;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.mDeviceInfo = deviceInfo;
        if (deviceInfo != null) {
            sLastSeenDeviceInfo.mDeviceInfo = deviceInfo;
        }
    }

    public void setLinkBandwidthLevel(int i) {
        if (this.mLink != null && this.mLinkStatus == RemoteDevice.LinkStatus.CONNECTED) {
            this.mLink.setBandwidthLevel(i);
        }
    }

    public boolean startLink() {
        if (this.mLink != null) {
            throw new IllegalStateException("Link already started");
        }
        sLogger.i("Starting link");
        this.mLink = LinkManager.build(getConnectionInfo());
        SocketAdapter socketAdapter = null;
        synchronized (lock) {
            if (!(this.mLink == null || this.mActiveConnection == null)) {
                socketAdapter = this.mActiveConnection.getSocket();
            }
        }
        if (socketAdapter != null) {
            return this.mLink.start(socketAdapter, this.mEventQueue, this);
        }
        return false;
    }

    public boolean stopLink() {
        sLogger.i("Stopping link");
        if (this.mLink != null) {
            sLogger.i("Closing link");
            this.mNetworkReadyEventDispatched.set(false);
            this.mNetworkLinkReady = false;
            this.mLink.close();
            this.mLink = null;
            this.mEventQueue.clear();
        }
        return true;
    }
}