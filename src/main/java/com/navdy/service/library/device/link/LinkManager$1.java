package com.navdy.service.library.device.link;

final class LinkManager$1 implements com.navdy.service.library.device.link.LinkManager$LinkFactory {
    LinkManager$1() {
    }
    
    public com.navdy.service.library.device.link.Link build(com.navdy.service.library.device.connection.ConnectionInfo a) {
        com.navdy.service.library.device.link.ProtobufLink a0 = null;
        com.navdy.service.library.device.connection.ConnectionType a1 = a.getType();
        switch(com.navdy.service.library.device.link.LinkManager$2.$SwitchMap$com$navdy$service$library$device$connection$ConnectionType[a1.ordinal()]) {
            case 1: case 2: {
                a0 = new com.navdy.service.library.device.link.ProtobufLink(a);
                break;
            }
            default: {
                a0 = null;
            }
        }
        return (com.navdy.service.library.device.link.Link)a0;
    }
}
