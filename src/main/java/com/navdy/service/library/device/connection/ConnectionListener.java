package com.navdy.service.library.device.connection;

import android.content.Context;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.Listenable;
import java.io.IOException;

public abstract class ConnectionListener extends Listenable {
    protected final Logger logger;
    AcceptThread mAcceptThread;
    protected final Context mContext;

    protected abstract class AcceptThread extends Thread {
        protected AcceptThread() {
        }

        public abstract void cancel();
    }

    protected interface EventDispatcher extends com.navdy.service.library.util.Listenable.EventDispatcher<ConnectionListener, Listener> {
    }

    public interface Listener extends com.navdy.service.library.util.Listenable.Listener {
        void onConnected(ConnectionListener connectionListener, Connection connection);

        void onConnectionFailed(ConnectionListener connectionListener);

        void onStartFailure(ConnectionListener connectionListener);

        void onStarted(ConnectionListener connectionListener);

        void onStopped(ConnectionListener connectionListener);
    }

    public ConnectionListener(Context context, String str) {
        this.mContext = context;
        this.logger = new Logger(str);
    }

    public void dispatchConnected(final Connection connection) {
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(ConnectionListener connectionListener, Listener listener) {
                listener.onConnected(connectionListener, connection);
            }
        });
    }

    public void dispatchConnectionFailed() {
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(ConnectionListener connectionListener, Listener listener) {
                listener.onConnectionFailed(connectionListener);
            }
        });
    }

    public void dispatchStartFailure() {
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(ConnectionListener connectionListener, Listener listener) {
                listener.onStartFailure(connectionListener);
            }
        });
    }

    public void dispatchStarted() {
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(ConnectionListener connectionListener, Listener listener) {
                listener.onStarted(connectionListener);
            }
        });
    }

    public void dispatchStopped() {
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(ConnectionListener connectionListener, Listener listener) {
                listener.onStopped(connectionListener);
            }
        });
    }

    public abstract AcceptThread getNewAcceptThread() throws IOException;

    public abstract ConnectionType getType();

    public boolean start() {
        boolean z = false;
        synchronized (this) {
            if (this.mAcceptThread != null) {
                if (this.mAcceptThread.isAlive()) {
                    this.logger.e("Already running");
                    return true;
                } else {
                    this.logger.e("clearing existing accept thread");
                    this.mAcceptThread.cancel();
                    this.mAcceptThread = null;
                }
            }
            try {
                this.mAcceptThread = getNewAcceptThread();
                if (this.mAcceptThread != null) {
                    this.mAcceptThread.start();
                }
                z = true;
            } catch (Throwable th) {
                this.logger.e("Unable to start accept thread: ", th);
                dispatchStartFailure();
            }
        }
        return z;
    }

    public boolean stop() {
        boolean z;
        synchronized (this) {
            if (this.mAcceptThread == null) {
                this.logger.e("Already stopped.");
                z = false;
            } else {
                this.mAcceptThread.cancel();
                this.mAcceptThread = null;
                z = true;
            }
        }
        return z;
    }
}