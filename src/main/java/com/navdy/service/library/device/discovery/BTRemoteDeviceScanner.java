package com.navdy.service.library.device.discovery;

public class BTRemoteDeviceScanner extends com.navdy.service.library.device.discovery.RemoteDeviceScanner {
    final private static com.navdy.service.library.log.Logger sLogger;
    private android.bluetooth.BluetoothAdapter btAdapter;
    private java.util.Set devicesSeen;
    private Runnable handleStartScan;
    private Runnable handleStopScan;
    final private android.content.BroadcastReceiver mReceiver;
    private boolean scanning;
    private int taskQueue;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.class);
    }
    
    public BTRemoteDeviceScanner(android.content.Context a, int i) {
        super(a);
        this.scanning = false;
        this.handleStartScan = (Runnable)new com.navdy.service.library.device.discovery.BTRemoteDeviceScanner$1(this);
        this.handleStopScan = (Runnable)new com.navdy.service.library.device.discovery.BTRemoteDeviceScanner$2(this);
        this.mReceiver = new com.navdy.service.library.device.discovery.BTRemoteDeviceScanner$3(this);
        this.taskQueue = i;
        this.btAdapter = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
    }
    
    static boolean access$000(com.navdy.service.library.device.discovery.BTRemoteDeviceScanner a) {
        return a.scanning;
    }
    
    static boolean access$002(com.navdy.service.library.device.discovery.BTRemoteDeviceScanner a, boolean b) {
        a.scanning = b;
        return b;
    }
    
    static android.content.BroadcastReceiver access$100(com.navdy.service.library.device.discovery.BTRemoteDeviceScanner a) {
        return a.mReceiver;
    }
    
    static java.util.Set access$200(com.navdy.service.library.device.discovery.BTRemoteDeviceScanner a) {
        return a.devicesSeen;
    }
    
    static java.util.Set access$202(com.navdy.service.library.device.discovery.BTRemoteDeviceScanner a, java.util.Set a0) {
        a.devicesSeen = a0;
        return a0;
    }
    
    static android.bluetooth.BluetoothAdapter access$300(com.navdy.service.library.device.discovery.BTRemoteDeviceScanner a) {
        return a.btAdapter;
    }
    
    static com.navdy.service.library.log.Logger access$400() {
        return sLogger;
    }
    
    public boolean startScan() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(this.handleStartScan, this.taskQueue);
        return true;
    }
    
    public boolean stopScan() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(this.handleStopScan, this.taskQueue);
        return true;
    }
}
