package com.navdy.service.library.device.connection.tunnel;

class TransferThread extends Thread {
    final static com.navdy.service.library.log.Logger sLogger;
    final com.navdy.service.library.util.NetworkActivityTracker activityTracker;
    private volatile boolean canceled;
    final java.io.InputStream inStream;
    final java.io.OutputStream outStream;
    final com.navdy.service.library.device.connection.tunnel.Tunnel parentThread;
    final boolean sending;
    final int buffsize;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.device.connection.tunnel.TransferThread.class);
    }
    
    public TransferThread(com.navdy.service.library.device.connection.tunnel.Tunnel a, java.io.InputStream a0, java.io.OutputStream a1, boolean b, int buffsize) {
        sLogger.v(new StringBuilder().append("new transfer thread (").append(this).append("): ").append(a0).append(" -> ").append(a1).toString());
        this.setName(com.navdy.service.library.device.connection.tunnel.TransferThread.class.getSimpleName());
        this.parentThread = a;
        this.inStream = a0;
        this.outStream = a1;
        this.sending = b;
        a.transferThreads.add(this);
        this.activityTracker = com.navdy.service.library.util.NetworkActivityTracker.getInstance();
        this.buffsize = buffsize;
    }

    public TransferThread(com.navdy.service.library.device.connection.tunnel.Tunnel a, java.io.InputStream a0, java.io.OutputStream a1, boolean b) {
        this(a, a0, a1, b, 16*1024);
    }
    
    public void cancel() {
        this.canceled = true;
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.inStream);
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.outStream);
        this.parentThread.transferThreads.remove(this);
    }
    
    public void run() {
        byte[] a = new byte[this.buffsize];
        try {
            while(true) {
                int i = this.inStream.read(a);
                if (i < 0) {
                    break;
                }
                if (sLogger.isLoggable(2)) {
                    sLogger.v(String.format("transfer (%s -> %s): %d bytes%s", this.inStream, this.outStream, i, java.util.Arrays.copyOf(a, i)));
                }
                this.outStream.write(a, 0, i);
                this.outStream.flush();
                if (this.sending) {
                    this.activityTracker.addBytesSent(i);
                } else {
                    this.activityTracker.addBytesReceived(i);
                }
            }
            sLogger.d("socket was closed");
        } catch(Throwable a2) {
            if (!this.canceled) {
                sLogger.e("Exception", a2);
            }
        } finally {
            this.cancel();
        }
    }
}
