package com.navdy.service.library.device.discovery;

class RemoteDeviceScanner$1 implements com.navdy.service.library.device.discovery.RemoteDeviceScanner$ScanEventDispatcher {
    final com.navdy.service.library.device.discovery.RemoteDeviceScanner this$0;
    
    RemoteDeviceScanner$1(com.navdy.service.library.device.discovery.RemoteDeviceScanner a) {
        super();
        this.this$0 = a;
    }
    
    public void dispatchEvent(com.navdy.service.library.device.discovery.RemoteDeviceScanner a, com.navdy.service.library.device.discovery.RemoteDeviceScanner$Listener a0) {
        a0.onScanStopped(a);
    }
    
    public void dispatchEvent(com.navdy.service.library.util.Listenable a, com.navdy.service.library.util.Listenable.Listener a0) {
        this.dispatchEvent((com.navdy.service.library.device.discovery.RemoteDeviceScanner)a, (com.navdy.service.library.device.discovery.RemoteDeviceScanner$Listener)a0);
    }
}
