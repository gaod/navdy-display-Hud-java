package com.navdy.service.library.device.link;

abstract public interface LinkListener {
    abstract public void linkEstablished(com.navdy.service.library.device.connection.ConnectionType arg);
    
    
    abstract public void linkLost(com.navdy.service.library.device.connection.ConnectionType arg, com.navdy.service.library.device.connection.Connection.DisconnectCause arg0);
    
    
    abstract public void onNavdyEventReceived(byte[] arg);
    
    
    abstract public void onNetworkLinkReady();
}
