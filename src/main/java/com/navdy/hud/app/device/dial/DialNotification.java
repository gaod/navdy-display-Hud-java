package com.navdy.hud.app.device.dial;

import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.toast.ToastManager$ToastParams;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.framework.toast.IToastCallback;
import com.navdy.hud.app.framework.voice.TTSUtils;
import android.os.Bundle;
import android.bluetooth.BluetoothDevice;
import android.text.TextUtils;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.log.Logger;
import android.content.res.Resources;

public class DialNotification
{
    private static final String[] BATTERY_TOASTS;
    private static final int DIAL_BATTERY_TIMEOUT = 2000;
    private static final int DIAL_BATTERY_TIMEOUT_EXTREMELY_LOW = 20000;
    public static final int DIAL_CONNECTED_TIMEOUT = 5000;
    public static final String DIAL_CONNECT_ID = "dial-connect";
    private static final int DIAL_DISCONNECTED_TIMEOUT = 1000;
    public static final String DIAL_DISCONNECT_ID = "dial-disconnect";
    public static final String DIAL_EXTREMELY_LOW_BATTERY_ID = "dial-exlow-battery";
    public static final String DIAL_FORGOTTEN_ID = "dial-forgotten";
    private static final int DIAL_FORGOTTEN_TIMEOUT = 2000;
    public static final String DIAL_LOW_BATTERY_ID = "dial-low-battery";
    private static final String[] DIAL_TOASTS;
    public static final String DIAL_VERY_LOW_BATTERY_ID = "dial-vlow-battery";
    private static final String EMPTY = "";
    private static final String battery_unknown;
    private static final String connected;
    private static final String disconnected;
    private static final String forgotten;
    private static final String forgotten_plural;
    private static Resources resources;
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(DialNotification.class);
        BATTERY_TOASTS = new String[] { "dial-low-battery", "dial-vlow-battery", "dial-exlow-battery" };
        DIAL_TOASTS = new String[] { "dial-connect", "dial-disconnect", "dial-forgotten", "dial-low-battery", "dial-vlow-battery", "dial-exlow-battery" };
        DialNotification.resources = HudApplication.getAppContext().getResources();
        disconnected = DialNotification.resources.getString(R.string.connection_status_disconnected);
        connected = DialNotification.resources.getString(R.string.dial_paired_text);
        forgotten = DialNotification.resources.getString(R.string.dial_forgotten);
        forgotten_plural = DialNotification.resources.getString(R.string.dial_forgotten_multiple);
        battery_unknown = DialNotification.resources.getString(R.string.question_mark);
    }
    
    public static void dismissAllBatteryToasts() {
        final ToastManager instance = ToastManager.getInstance();
        instance.dismissCurrentToast(DialNotification.BATTERY_TOASTS);
        instance.clearPendingToast(DialNotification.BATTERY_TOASTS);
    }
    
    public static String getDialAddressPart(String substring) {
        String substring2 = substring;
        if (!TextUtils.isEmpty((CharSequence)substring)) {
            final int index = substring.indexOf("(");
            substring2 = substring;
            if (index >= 0) {
                substring = substring.substring(index + 1);
                final int index2 = substring.indexOf(")");
                substring2 = substring;
                if (index2 >= 0) {
                    substring2 = substring.substring(0, index2);
                }
            }
        }
        return substring2;
    }
    
    public static String getDialBatteryLevel() {
        final int lastKnownBatteryLevel = DialManager.getInstance().getLastKnownBatteryLevel();
        String s;
        if (lastKnownBatteryLevel == -1) {
            s = DialNotification.battery_unknown;
        }
        else {
            s = String.valueOf(DialManager.getDisplayBatteryLevel(lastKnownBatteryLevel));
        }
        return s;
    }
    
    public static String getDialName() {
        final BluetoothDevice dialDevice = DialManager.getInstance().getDialDevice();
        String s = null;
        if (dialDevice != null && TextUtils.isEmpty((CharSequence)(s = dialDevice.getName()))) {
            final String address = dialDevice.getAddress();
            final int length = address.length();
            String substring = address;
            if (length > 4) {
                substring = address.substring(length - 4);
            }
            s = HudApplication.getAppContext().getString(R.string.dial_str, new Object[] { substring });
        }
        return s;
    }
    
    public static void showConnectedToast() {
        final Bundle bundle = new Bundle();
        bundle.putInt("13", 5000);
        bundle.putInt("8", R.drawable.icon_dial_2);
        bundle.putInt("11", R.drawable.icon_sm_success);
        bundle.putString("4", DialNotification.resources.getString(R.string.navdy_dial));
        bundle.putInt("5", R.style.Glances_1_bold);
        final String dialAddressPart = getDialAddressPart(getDialName());
        if (!TextUtils.isEmpty((CharSequence)dialAddressPart)) {
            bundle.putString("6", DialNotification.resources.getString(R.string.navdy_dial_name, new Object[] { dialAddressPart }));
        }
        bundle.putString("17", TTSUtils.TTS_DIAL_CONNECTED);
        showDialToast("dial-connect", bundle, true);
    }
    
    private static void showDialToast(final String s, final Bundle bundle, final boolean b) {
        final ToastManager instance = ToastManager.getInstance();
        final String currentToastId = instance.getCurrentToastId();
        instance.clearPendingToast(DialNotification.DIAL_TOASTS);
        for (final String s2 : DialNotification.DIAL_TOASTS) {
            if (!TextUtils.equals((CharSequence)s2, (CharSequence)s)) {
                instance.dismissCurrentToast(s2);
            }
        }
        if (!TextUtils.equals((CharSequence)s, (CharSequence)currentToastId)) {
            ToastManager.getInstance().addToast(new ToastManager$ToastParams(s, bundle, null, b, false));
        }
    }
    
    public static void showDisconnectedToast(final String s) {
        final BaseScreen currentScreen = RemoteDeviceManager.getInstance().getUiStateManager().getCurrentScreen();
        if (currentScreen != null && currentScreen.getScreen() == Screen.SCREEN_DIAL_PAIRING) {
            DialNotification.sLogger.v("not showing dial disconnected toast");
        }
        else {
            final Bundle bundle = new Bundle();
            bundle.putInt("13", 1000);
            bundle.putInt("8", R.drawable.icon_dial_forgotten);
            bundle.putInt("11", R.drawable.icon_sm_forgotten);
            bundle.putString("4", DialNotification.disconnected);
            bundle.putInt("5", R.style.Glances_1);
            if (!TextUtils.isEmpty((CharSequence)s)) {
                bundle.putString("6", s);
            }
            bundle.putString("17", TTSUtils.TTS_DIAL_DISCONNECTED);
            showDialToast("dial-disconnect", bundle, true);
        }
    }
    
    public static void showExtremelyLowBatteryToast() {
        final Bundle bundle = new Bundle();
        bundle.putInt("13", 20000);
        bundle.putInt("8", R.drawable.icon_toast_dial_battery_very_low);
        bundle.putString("4", DialNotification.resources.getString(R.string.dial_ex_low_battery, new Object[] { getDialBatteryLevel() }));
        bundle.putInt("5", R.style.Glances_1);
        bundle.putBoolean("12", true);
        bundle.putString("17", TTSUtils.TTS_DIAL_BATTERY_EXTREMELY_LOW);
        showDialToast("dial-exlow-battery", bundle, false);
    }
    
    public static void showForgottenToast(final boolean b, final String s) {
        final Bundle bundle = new Bundle();
        bundle.putInt("13", 2000);
        bundle.putInt("8", R.drawable.icon_dial_forgotten);
        bundle.putInt("11", R.drawable.icon_sm_forgotten);
        if (b) {
            bundle.putString("4", DialNotification.forgotten_plural);
        }
        else {
            bundle.putString("4", DialNotification.forgotten);
        }
        if (!b && !TextUtils.isEmpty((CharSequence)s)) {
            bundle.putString("6", s);
            bundle.putInt("7", R.style.title3_single);
        }
        bundle.putInt("5", R.style.Glances_1);
        bundle.putString("17", TTSUtils.TTS_DIAL_FORGOTTEN);
        showDialToast("dial-forgotten", bundle, true);
    }
    
    public static void showLowBatteryToast() {
        final Bundle bundle = new Bundle();
        bundle.putInt("13", 2000);
        bundle.putInt("8", R.drawable.icon_toast_dial_battery_low);
        bundle.putString("4", DialNotification.resources.getString(R.string.dial_low_battery, new Object[] { getDialBatteryLevel() }));
        bundle.putInt("5", R.style.Glances_1);
        bundle.putString("17", TTSUtils.TTS_DIAL_BATTERY_LOW);
        showDialToast("dial-low-battery", bundle, true);
    }
    
    public static void showVeryLowBatteryToast() {
        final Bundle bundle = new Bundle();
        bundle.putInt("13", 2000);
        bundle.putInt("8", R.drawable.icon_toast_dial_battery_low);
        bundle.putString("4", DialNotification.resources.getString(R.string.dial_low_battery, new Object[] { getDialBatteryLevel() }));
        bundle.putInt("5", R.style.Glances_1);
        bundle.putString("17", TTSUtils.TTS_DIAL_BATTERY_VERY_LOW);
        showDialToast("dial-vlow-battery", bundle, false);
    }
}
