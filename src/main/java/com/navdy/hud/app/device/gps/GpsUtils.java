package com.navdy.hud.app.device.gps;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Process;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.config.Setting;
import com.navdy.hud.app.config.BooleanSetting;
import com.navdy.hud.app.config.BooleanSetting.Scope;
import com.navdy.hud.app.config.StringSetting;
import com.navdy.service.library.log.Logger;

import java.util.Objects;

import static com.navdy.hud.app.device.gps.GpsManager.MOCK_PROVIDER;

public class GpsUtils {
    public enum GPS_SOURCE_OPTION {
        UBLOX,
        PHONE,
        AUTO;

        public static String[] names() {
            GPS_SOURCE_OPTION[] states = values();
            String[] names = new String[states.length];

            for (int i = 0; i < states.length; i++) {
                names[i] = states[i].name();
            }

            return names;
        }
    }

    public static BooleanSetting SHOW_RAW_GPS = new BooleanSetting("Show Raw GPS", Scope.NEVER, "map.raw_gps", "Add map indicators showing raw and map matched GPS");
    public static StringSetting GPS_SOURCE = new StringSetting("Select GPS Source", "map.gps.source", "Select between UBLOX, PHONE, AUTO", "AUTO");
    private static final Logger sLogger = new Logger(GpsUtils.class);

    public static class GpsSatelliteData {
        public Bundle data;

        public GpsSatelliteData(Bundle bundle) {
            this.data = bundle;
        }
    }

    public static class GpsSwitch {
        public boolean usingPhone;
        public boolean usingUblox;

        public GpsSwitch(boolean z, boolean z2) {
            this.usingPhone = z;
            this.usingUblox = z2;
        }

        public String toString() {
            return "GpsSwitch [UsingPhone :" + this.usingPhone + ", UsingUblox : " + this.usingUblox + "]";
        }
    }

    public enum HeadingDirection {
        N,
        NE,
        E,
        SE,
        S,
        SW,
        W,
        NW
    }

    public static HeadingDirection getHeadingDirection(double d) {
        HeadingDirection headingDirection = HeadingDirection.N;
        if (d < 0.0d || d > 360.0d) {
            return headingDirection;
        }
        return HeadingDirection.values()[(int) (((float) ((22.5d + d) % 360.0d)) / 45.0f)];
    }

    public static boolean isDebugRawGpsPosEnabled() {
        return SHOW_RAW_GPS.isEnabled();
    }

    public static GPS_SOURCE_OPTION GpsSource() {
        try {
            return GPS_SOURCE_OPTION.valueOf(GPS_SOURCE.value.toUpperCase());
        } catch (IllegalArgumentException ex) {
            sLogger.e("Incorrect value:" + GPS_SOURCE.value, ex);
            return GPS_SOURCE_OPTION.AUTO;
        }
    }

    public static void SetGpsSource(GPS_SOURCE_OPTION source) {
        GPS_SOURCE.value = source.toString();
        GPS_SOURCE.save();
    }

    public static void SetGpsSource(String source) {
        try {
            GPS_SOURCE_OPTION.valueOf(source.toUpperCase());
            GPS_SOURCE.value = source.toUpperCase();
            GPS_SOURCE.save();
        } catch (IllegalArgumentException ex) {
            sLogger.e("Incorrect value:" + source, ex);
        }
    }


    public static void sendEventBroadcast(String str, Bundle bundle) {
        try {
            Intent intent = new Intent(str);
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            HudApplication.getAppContext().sendBroadcastAsUser(intent, Process.myUserHandle());
        } catch (Throwable th) {
            sLogger.e(th);
        }
    }
}
