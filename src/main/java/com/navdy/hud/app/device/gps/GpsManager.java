package com.navdy.hud.app.device.gps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.GpsStatus.Listener;
import android.location.GpsStatus.NmeaListener;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.SystemClock;
import android.os.UserHandle;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.debug.RouteRecorder;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport;
import com.navdy.hud.app.service.HudConnectionService;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.events.DeviceInfo.Platform;
import com.navdy.service.library.events.audio.SpeechRequest;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.location.TransmitLocation;
import com.navdy.service.library.log.Logger;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class GpsManager {
    final public static String MOCK_PROVIDER = LocationManager.NETWORK_PROVIDER;
    
    private static final int ACCEPTABLE_THRESHOLD = 2;
    private static final String DISCARD_TIME = "DISCARD_TIME";
    private static final String GPS_SYSTEM_PROPERTY = "persist.sys.hud_gps";
    public static final long INACCURATE_GPS_REPORT_INTERVAL = TimeUnit.SECONDS.toMillis(30);
    private static final int INITIAL_INTERVAL = 15000;
    private static final int KEEP_PHONE_GPS_ON = -1;
    private static final int LOCATION_ACCURACY_THROW_AWAY_THRESHOLD = 30;
    private static final int LOCATION_TIME_THROW_AWAY_THRESHOLD = 2000;
    public static final double MAXIMUM_STOPPED_SPEED = 0.22353333333333333d;
    private static final double METERS_PER_MILE = 1609.44d;
    public static final long MINIMUM_DRIVE_TIME = 3000;
    public static final double MINIMUM_DRIVING_SPEED = 2.2353333333333336d;
    static final int MSG_NMEA = 1;
    static final int MSG_SATELLITE_STATUS = 2;
    private static final int SECONDS_PER_HOUR = 3600;
    private static final String SET_LOCATION_DISCARD_TIME_THRESHOLD = "SET_LOCATION_DISCARD_TIME_THRESHOLD";
    private static final String SET_UBLOX_ACCURACY = "SET_UBLOX_ACCURACY";
    private static final TransmitLocation START_SENDING_LOCATION = new TransmitLocation(Boolean.valueOf(true));
    private static final String START_UBLOX = "START_REPORTING_UBLOX_LOCATION";
    private static final TransmitLocation STOP_SENDING_LOCATION = new TransmitLocation(Boolean.valueOf(false));
    private static final String STOP_UBLOX = "STOP_REPORTING_UBLOX_LOCATION";
    private static final String TAG_GPS = "[Gps-i] ";
    private static final String TAG_GPS_LOC = "[Gps-loc] ";
    private static final String TAG_GPS_LOC_NAVDY = "[Gps-loc-navdy] ";
    private static final String TAG_GPS_NMEA = "[Gps-nmea] ";
    private static final String TAG_GPS_STATUS = "[Gps-stat] ";
    private static final String TAG_PHONE_LOC = "[Phone-loc] ";
    private static final String UBLOX_ACCURACY = "UBLOX_ACCURACY";
    private static final float UBLOX_MIN_ACCEPTABLE_THRESHOLD = 5.0f;
    /* access modifiers changed from: private */
    public static final long WARM_RESET_INTERVAL = TimeUnit.SECONDS.toMillis(160);
    /* access modifiers changed from: private */
    public static final Logger sLogger = new Logger(GpsManager.class);
    private static final GpsManager singleton = new GpsManager();
    /* access modifiers changed from: private */
    public LocationSource activeLocationSource;
    /* access modifiers changed from: private */
    public HudConnectionService connectionService;
    private boolean driving;
    private BroadcastReceiver eventReceiver = new BroadcastReceiver() {
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onReceive(Context context, Intent intent) {
            try {
                String action = intent.getAction();
                if (action != null) {
                    RouteRecorder instance = RouteRecorder.getInstance();
                    switch (action) {
                        case GpsConstants.GPS_EVENT_DEAD_RECKONING_STARTED:
                            if (instance.isRecording()) {
                                String stringExtra = intent.getStringExtra(GpsConstants.GPS_EXTRA_DR_TYPE);
                                if (stringExtra == null) {
                                    stringExtra = GpsConstants.EMPTY;
                                }
                                instance.injectMarker("GPS_DR_STARTED " + stringExtra);
                                return;
                            }
                            return;
                        case GpsConstants.GPS_EVENT_DEAD_RECKONING_STOPPED:
                            if (instance.isRecording()) {
                                instance.injectMarker(GpsConstants.GPS_EVENT_DEAD_RECKONING_STOPPED);
                                return;
                            }
                            return;
                        case GpsConstants.EXTRAPOLATION:
                            GpsManager.this.extrapolationOn = intent.getBooleanExtra(GpsConstants.EXTRAPOLATION_FLAG, false);
                            if (GpsManager.this.extrapolationOn) {
                                GpsManager.this.extrapolationStartTime = SystemClock.elapsedRealtime();
                            } else {
                                GpsManager.this.extrapolationStartTime = 0;
                            }
                            GpsManager.sLogger.v("extrapolation is:" + GpsManager.this.extrapolationOn + " time:" + GpsManager.this.extrapolationStartTime);
                            return;
                        default:
                    }
                }
            } catch (Throwable th) {
                GpsManager.sLogger.e(th);
            }
        }
    };
    /* access modifiers changed from: private */
    public volatile boolean extrapolationOn;
    /* access modifiers changed from: private */
    public volatile long extrapolationStartTime;
    /* access modifiers changed from: private */
    public boolean firstSwitchToUbloxFromPhone;
    private float gpsAccuracy;
    /* access modifiers changed from: private */
    public long gpsLastEventTime;
    /* access modifiers changed from: private */
    public long gpsManagerStartTime;
    /* access modifiers changed from: private */
    public Handler handler = new Handler(Looper.getMainLooper());
    private long inaccurateGpsCount;
    private boolean isDebugTTSEnabled;
    private boolean keepPhoneGpsOn;
    private long lastInaccurateGpsTime;
    /* access modifiers changed from: private */
    public Coordinate lastPhoneCoordinate;
    /* access modifiers changed from: private */
    public long lastPhoneCoordinateTime;
    /* access modifiers changed from: private */
    public Location lastUbloxCoordinate;
    /* access modifiers changed from: private */
    public long lastUbloxCoordinateTime;
    /* access modifiers changed from: private */
    public Runnable locationFixRunnable = new Runnable() {
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0115  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x0129  */
        /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            boolean retry = false;
            try {
                long elapsedRealtime = SystemClock.elapsedRealtime() - GpsManager.this.lastUbloxCoordinateTime;
                if (elapsedRealtime >= GpsManager.MINIMUM_DRIVE_TIME) {
                    GpsManager.sLogger.i("[Gps-loc] locationFixRunnable: lost location fix[" + elapsedRealtime + "]");
                    if (GpsManager.this.extrapolationOn) {
                        long elapsedRealtime2 = SystemClock.elapsedRealtime() - GpsManager.this.extrapolationStartTime;
                        GpsManager.sLogger.i("[Gps-loc] locationFixRunnable: extrapolation on time:" + elapsedRealtime2);
                        if (elapsedRealtime2 < 5000) {
                            GpsManager.sLogger.i("[Gps-loc] locationFixRunnable: reset");
                            GpsManager.this.handler.postDelayed(GpsManager.this.locationFixRunnable, 1000);
                            return;
                        }
                        GpsManager.this.extrapolationOn = false;
                        GpsManager.this.extrapolationStartTime = 0;
                        GpsManager.sLogger.i("[Gps-loc] locationFixRunnable: expired");
                    }
                    GpsManager.this.updateDrivingState(null);
                    GpsManager.sLogger.e("[Gps-loc] lost gps fix");
                    GpsManager.this.handler.removeCallbacks(GpsManager.this.locationFixRunnable);
                    GpsManager.this.handler.removeCallbacks(GpsManager.this.noPhoneLocationFixRunnable);
                    GpsManager.this.handler.postDelayed(GpsManager.this.noPhoneLocationFixRunnable, 5000);
                    GpsManager.this.startSendingLocation();
                    ConnectionServiceAnalyticsSupport.recordGpsLostLocation(String.valueOf((SystemClock.elapsedRealtime() - GpsManager.this.gpsLastEventTime) / 1000));
                    GpsManager.this.gpsLastEventTime = SystemClock.elapsedRealtime();
                } else {
                    retry = true;
                }
                if (retry) {
                    GpsManager.this.handler.postDelayed(GpsManager.this.locationFixRunnable, 1000);
                }
            } catch (Throwable th2) {
                GpsManager.sLogger.e(th2);
            }
        }
    };
    private LocationManager locationManager;
    private LocationListener navdyGpsLocationListener = new LocationListener() {
        private float accuracyMax = 0.0f;
        private float accuracyMin = 0.0f;
        private int accuracySampleCount = 0;
        private long accuracySampleStartTime = SystemClock.elapsedRealtime();
        private float accuracySum = 0.0f;

        private void collectAccuracyStats(float f) {
            if (f != 0.0f) {
                this.accuracySum += f;
                this.accuracySampleCount++;
                if (this.accuracyMin == 0.0f || f < this.accuracyMin) {
                    this.accuracyMin = f;
                }
                if (this.accuracyMax < f) {
                    this.accuracyMax = f;
                }
            }
            if (GpsManager.this.lastUbloxCoordinateTime - this.accuracySampleStartTime > 300000) {
                if (this.accuracySampleCount > 0) {
                    ConnectionServiceAnalyticsSupport.recordGpsAccuracy(Integer.toString(Math.round(this.accuracyMin)), Integer.toString(Math.round(this.accuracyMax)), Integer.toString(Math.round(this.accuracySum / ((float) this.accuracySampleCount))));
                    this.accuracySum = 0.0f;
                    this.accuracyMax = 0.0f;
                    this.accuracyMin = 0.0f;
                    this.accuracySampleCount = 0;
                }
                this.accuracySampleStartTime = GpsManager.this.lastUbloxCoordinateTime;
            }
        }

        public void onLocationChanged(Location location) {
            try {
                if (!GpsManager.this.warmResetCancelled) {
                    GpsManager.this.cancelUbloxResetRunnable();
                }
                if (!RouteRecorder.getInstance().isPlaying()) {
                    GpsManager.this.updateDrivingState(location);
                    GpsManager.this.lastUbloxCoordinate = location;
                    GpsManager.this.lastUbloxCoordinateTime = SystemClock.elapsedRealtime();
                    collectAccuracyStats(location.getAccuracy());
                    if (GpsManager.this.connectionService != null && GpsManager.this.connectionService.isConnected()) {
                        if (GpsManager.this.activeLocationSource != LocationSource.UBLOX) {
                            long elapsedRealtime = SystemClock.elapsedRealtime() - GpsManager.this.lastPhoneCoordinateTime;
                            if (elapsedRealtime > GpsManager.MINIMUM_DRIVE_TIME) {
                                GpsManager.sLogger.v("phone threshold exceeded= " + elapsedRealtime + ", start uBloxReporting");
                                GpsManager.this.startUbloxReporting();
                                return;
                            }
                        }
                        if (!GpsManager.this.firstSwitchToUbloxFromPhone || GpsManager.this.switchBetweenPhoneUbloxAllowed) {
                            GpsManager.this.checkGpsToPhoneAccuracy(location);
                        }
                    } else if (GpsManager.this.activeLocationSource != LocationSource.UBLOX) {
                        GpsManager.sLogger.v("not connected with phone, start uBloxReporting");
                        GpsManager.this.startUbloxReporting();
                    }
                }
            } catch (Throwable th) {
                GpsManager.sLogger.e(th);
            }
        }

        public void onProviderDisabled(String str) {
        }

        public void onProviderEnabled(String str) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }
    };
    private Callback nmeaCallback = new Callback() {
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean handleMessage(Message message) {
            try {
                switch (message.what) {
                    case 1:
                        GpsManager.this.nmeaParser.parseNmeaMessage((String) message.obj);
                        break;
                    case GpsNmeaParser.FIX_TYPE_DIFFERENTIAL /*2*/:
                        if (SystemClock.elapsedRealtime() - GpsManager.this.gpsManagerStartTime > 15000) {
                            Bundle bundle = (Bundle) message.obj;
                            Bundle bundle2 = new Bundle();
                            bundle2.putBundle(GpsConstants.GPS_EVENT_SATELLITE_DATA, bundle);
                            GpsManager.this.sendGpsEventBroadcast(GpsConstants.GPS_SATELLITE_STATUS, bundle2);
                            break;
                        }
                        break;
                }
            } catch (Throwable th) {
                GpsManager.sLogger.e(th);
            }
            return true;
        }
    };
    private Handler nmeaHandler;
    private HandlerThread nmeaHandlerThread;
    /* access modifiers changed from: private */
    public GpsNmeaParser nmeaParser;
    private Runnable noLocationUpdatesRunnable = new Runnable() {
        public void run() {
            GpsManager.this.updateDrivingState(null);
        }
    };
    /* access modifiers changed from: private */
    public Runnable noPhoneLocationFixRunnable = new Runnable() {
        public void run() {
            boolean z = true;
            try {
                if (GpsManager.this.activeLocationSource == LocationSource.UBLOX) {
                    GpsManager.sLogger.i("[Gps-loc] noPhoneLocationFixRunnable: has location fix now");
                    z = false;
                } else {
                    GpsManager.this.updateDrivingState(null);
                    GpsManager.this.startSendingLocation();
                }
                if (!z) {
                }
            } catch (Throwable th) {
                GpsManager.sLogger.e(th);
            } finally {
                GpsManager.this.handler.postDelayed(GpsManager.this.noPhoneLocationFixRunnable, 5000);
            }
        }
    };
    /* access modifiers changed from: private */
    public ArrayList<Intent> queue = new ArrayList<>();
    private Runnable queueCallback = new Runnable() {
        public void run() {
            try {
                Context appContext = HudApplication.getAppContext();
                UserHandle myUserHandle = Process.myUserHandle();
                int size = GpsManager.this.queue.size();
                if (size > 0) {
                    for (int i = 0; i < size; i++) {
                        Intent intent = (Intent) GpsManager.this.queue.get(i);
                        appContext.sendBroadcastAsUser(intent, myUserHandle);
                        GpsManager.sLogger.v("sent-queue gps event user:" + intent.getAction());
                    }
                    GpsManager.this.queue.clear();
                }
            } catch (Throwable th) {
                GpsManager.sLogger.e(th);
            }
        }
    };
    /* access modifiers changed from: private */
    public volatile boolean startUbloxCalled;
    /* access modifiers changed from: private */
    public boolean switchBetweenPhoneUbloxAllowed;
    private long transitionStartTime;
    private boolean transitioning;
    private LocationListener ubloxGpsLocationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            if (GpsManager.sLogger.isLoggable(2)) {
                try {
                    GpsManager.sLogger.d(GpsManager.TAG_GPS_LOC + (location.isFromMockProvider() ? "[Mock] " : GpsConstants.EMPTY) + location);
                } catch (Throwable th) {
                    GpsManager.sLogger.e(th);
                    return;
                }
            }
            if (GpsManager.this.startUbloxCalled) {
                if (!location.isFromMockProvider() && GpsManager.this.activeLocationSource != LocationSource.UBLOX) {
                    LocationSource access$300 = GpsManager.this.activeLocationSource;
                    GpsManager.this.activeLocationSource = LocationSource.UBLOX;
                    GpsManager.this.handler.removeCallbacks(GpsManager.this.noPhoneLocationFixRunnable);
                    GpsManager.this.handler.removeCallbacks(GpsManager.this.locationFixRunnable);
                    GpsManager.this.handler.postDelayed(GpsManager.this.locationFixRunnable, 1000);
                    GpsManager.this.stopSendingLocation();
                    int elapsedRealtime = ((int) (SystemClock.elapsedRealtime() - GpsManager.this.gpsLastEventTime)) / GpsConstants.LOCATION_FIX_CHECK_INTERVAL;
                    long elapsedRealtime2 = SystemClock.elapsedRealtime() - GpsManager.this.lastPhoneCoordinateTime;
                    float f = (GpsManager.this.lastPhoneCoordinate == null || elapsedRealtime2 >= GpsManager.MINIMUM_DRIVE_TIME) ? -1.0f : GpsManager.this.lastPhoneCoordinate.accuracy.floatValue();
                    int accuracy = (int) (GpsManager.this.lastUbloxCoordinate != null ? GpsManager.this.lastUbloxCoordinate.getAccuracy() : -1.0f);
                    ConnectionServiceAnalyticsSupport.recordGpsAcquireLocation(String.valueOf(elapsedRealtime), String.valueOf(accuracy));
                    GpsManager.this.gpsLastEventTime = SystemClock.elapsedRealtime();
                    GpsManager.this.markLocationFix(GpsConstants.DEBUG_TTS_UBLOX_SWITCH, "Phone =" + (f == -1.0f ? "n/a" : Float.valueOf(f)) + " ublox =" + accuracy, false, true);
                    GpsManager.sLogger.v("[Gps-loc] [LocationProvider] switched from [" + access$300 + "] to [" + LocationSource.UBLOX + "] " + "Phone =" + (f == -1.0f ? "n/a" : Float.valueOf(f)) + " ublox =" + accuracy + " time=" + elapsedRealtime2);
                }
            }
        }

        public void onProviderDisabled(String str) {
            GpsManager.sLogger.i(GpsManager.TAG_GPS_STATUS + str + "[disabled]");
        }

        public void onProviderEnabled(String str) {
            GpsManager.sLogger.i(GpsManager.TAG_GPS_STATUS + str + "[enabled]");
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
            if (PowerManager.isAwake()) {
                GpsManager.sLogger.i(GpsManager.TAG_GPS_STATUS + str + "," + i);
            }
        }
    };
    private NmeaListener ubloxGpsNmeaListener = new NmeaListener() {
        public void onNmeaReceived(long j, String str) {
            try {
                if (GpsManager.sLogger.isLoggable(2)) {
                    GpsManager.sLogger.v(GpsManager.TAG_GPS_NMEA + str);
                }
                GpsManager.this.processNmea(str);
            } catch (Throwable th) {
                GpsManager.sLogger.e(th);
            }
        }
    };
    private Listener ubloxGpsStatusListener = new Listener() {
        public void onGpsStatusChanged(int i) {
            switch (i) {
                case 1:
                    GpsManager.sLogger.i("[Gps-stat] gps searching...");
                    return;
                case GpsNmeaParser.FIX_TYPE_DIFFERENTIAL /*2*/:
                    GpsManager.sLogger.w("[Gps-stat] gps stopped searching");
                    return;
                case GpsNmeaParser.FIX_TYPE_PPS /*3*/:
                    GpsManager.sLogger.i("[Gps-stat] gps got first fix");
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean warmResetCancelled;
    private Runnable warmResetRunnable = new Runnable() {
        public void run() {
            try {
                GpsManager.sLogger.v("issuing warm reset to ublox, no location report in last " + GpsManager.WARM_RESET_INTERVAL);
                GpsManager.this.sendGpsEventBroadcast(GpsConstants.GPS_EVENT_WARM_RESET_UBLOX, null);
            } catch (Throwable th) {
                GpsManager.sLogger.e(th);
            }
        }
    };

    enum LocationSource {
        UBLOX,
        PHONE
    }

    private GpsManager() {
        LocationProvider locationProvider;
        Context appContext = HudApplication.getAppContext();
        try {
            int i = SystemProperties.getInt(GPS_SYSTEM_PROPERTY, 0);
            if (i == -1) {
                this.keepPhoneGpsOn = true;
                this.switchBetweenPhoneUbloxAllowed = true;
                sLogger.v("switch between phone and ublox allowed");
            }
            sLogger.v("keepPhoneGpsOn[" + this.keepPhoneGpsOn + "] val[" + i + "]");
            this.isDebugTTSEnabled = new File("/sdcard/debug_tts").exists();
            setUbloxAccuracyThreshold();
            setUbloxTimeDiscardThreshold();
        } catch (Throwable th) {
            sLogger.e(th);
        }
        this.locationManager = (LocationManager) appContext.getSystemService("location");
        if (this.locationManager.isProviderEnabled("gps")) {
            locationProvider = this.locationManager.getProvider("gps");
            if (locationProvider != null) {
                this.nmeaHandlerThread = new HandlerThread("navdynmea");
                this.nmeaHandlerThread.start();
                this.nmeaHandler = new Handler(this.nmeaHandlerThread.getLooper(), this.nmeaCallback);
                this.nmeaParser = new GpsNmeaParser(sLogger, this.nmeaHandler);
                this.locationManager.addGpsStatusListener(this.ubloxGpsStatusListener);
//                this.locationManager.addNmeaListener(this.ubloxGpsNmeaListener);
            }
        } else {
            locationProvider = null;
        }
        if (DeviceUtil.isNavdyDevice()) {
            sLogger.i("[Gps-i] setting up ublox gps");
            if (locationProvider != null) {
                this.locationManager.requestLocationUpdates("gps", 0, 0.0f, this.ubloxGpsLocationListener);
                try {
                    this.locationManager.requestLocationUpdates(GpsConstants.NAVDY_GPS_PROVIDER, 0, 0.0f, this.navdyGpsLocationListener);
                    sLogger.v("requestLocationUpdates successful for NAVDY_GPS_PROVIDER");
                } catch (Throwable th2) {
                    sLogger.e("requestLocationUpdates", th2);
                }
                sLogger.i("[Gps-i] ublox gps listeners installed");
            } else {
                sLogger.e("[Gps-i] gps provider not found");
            }
        } else {
            sLogger.i("[Gps-i] not a Hud device,not setting up ublox gps");
        }
        this.locationManager.addTestProvider(MOCK_PROVIDER, false, true, false, false, true, true, true, 0, 3);
        this.locationManager.setTestProviderEnabled(MOCK_PROVIDER, true);
        this.locationManager.setTestProviderStatus(MOCK_PROVIDER, 2, null, System.currentTimeMillis());
        sLogger.i("[Gps-i] added mock network provider");
        this.gpsManagerStartTime = SystemClock.elapsedRealtime();
        this.gpsLastEventTime = this.gpsManagerStartTime;
        ConnectionServiceAnalyticsSupport.recordGpsAttemptAcquireLocation();
        if (DeviceUtil.isNavdyDevice()) {
            setUbloxResetRunnable();
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(GpsConstants.GPS_EVENT_DEAD_RECKONING_STARTED);
        intentFilter.addAction(GpsConstants.GPS_EVENT_DEAD_RECKONING_STOPPED);
        intentFilter.addAction(GpsConstants.EXTRAPOLATION);
        appContext.registerReceiver(this.eventReceiver, intentFilter);
    }

    private static Location androidLocationFromCoordinate(Coordinate coordinate) {
        Location location = new Location(MOCK_PROVIDER);
        location.setLatitude(coordinate.latitude);
        location.setLongitude(coordinate.longitude);
        location.setAccuracy(coordinate.accuracy);
        location.setAltitude(coordinate.altitude);
        location.setBearing(coordinate.bearing);
        location.setSpeed(coordinate.speed);
        location.setTime(System.currentTimeMillis());
        location.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos());
        return location;
    }

    /* access modifiers changed from: private */
    public void cancelUbloxResetRunnable() {
        this.handler.removeCallbacks(this.warmResetRunnable);
        this.warmResetCancelled = true;
    }

    /* access modifiers changed from: private */
    public void checkGpsToPhoneAccuracy(Location location) {
        float accuracy = location.getAccuracy();
        long elapsedRealtime = SystemClock.elapsedRealtime();
        long j = this.lastPhoneCoordinateTime;
        if (sLogger.isLoggable(3)) {
            sLogger.v("[Gps-loc-navdy] checkGpsToPhoneAccuracy: ublox[" + location.getAccuracy() + "] phone[" + (this.lastPhoneCoordinate != null ? this.lastPhoneCoordinate.accuracy : "n/a") + "] lastPhoneTime [" + (this.lastPhoneCoordinate != null ? Long.valueOf(elapsedRealtime - j) : "n/a") + "]");
        }
        if (accuracy != 0.0f) {
            if (this.activeLocationSource == LocationSource.UBLOX) {
                if (this.lastPhoneCoordinate != null && SystemClock.elapsedRealtime() - this.lastPhoneCoordinateTime <= MINIMUM_DRIVE_TIME) {
                    if (this.lastPhoneCoordinate.accuracy + 2.0f < accuracy) {
                        if (sLogger.isLoggable(3)) {
                            sLogger.v("[Gps-loc-navdy]  ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "]");
                        }
                        if (!this.startUbloxCalled) {
                            return;
                        }
                        if (accuracy <= UBLOX_MIN_ACCEPTABLE_THRESHOLD) {
                            sLogger.v("ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "], but not above threshold");
                            stopSendingLocation();
                            return;
                        }
                        sLogger.v("ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "], stop uBloxReporting");
                        this.handler.removeCallbacks(this.locationFixRunnable);
                        this.handler.removeCallbacks(this.noPhoneLocationFixRunnable);
                        stopUbloxReporting();
                        sLogger.v("[Gps-loc] [LocationProvider] switched from [" + this.activeLocationSource + "] to [" + LocationSource.PHONE + "]");
                        this.activeLocationSource = LocationSource.PHONE;
                        markLocationFix(GpsConstants.DEBUG_TTS_PHONE_SWITCH, "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox =" + accuracy, true, false);
                    } else if (sLogger.isLoggable(3)) {
                        sLogger.v("ublox accuracy [" + accuracy + "] < phone accuracy[" + this.lastPhoneCoordinate.accuracy + "] no action");
                    }
                }
            } else if (accuracy + 2.0f < this.lastPhoneCoordinate.accuracy.floatValue()) {
                sLogger.v("ublox accuracy [" + accuracy + "] < phone accuracy[" + this.lastPhoneCoordinate.accuracy + "], start uBloxReporting, switch complete");
                this.firstSwitchToUbloxFromPhone = true;
                markLocationFix(GpsConstants.DEBUG_TTS_UBLOX_SWITCH, "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox =" + accuracy, false, false);
                startUbloxReporting();
            } else if (sLogger.isLoggable(3)) {
                sLogger.v("ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "] no action");
            }
        }
    }

    private void feedLocationToProvider(Location location, Coordinate coordinate) {
        try {
            if (sLogger.isLoggable(3)) {
                sLogger.d(TAG_PHONE_LOC + coordinate);
            }
            if (location == null) {
                location = androidLocationFromCoordinate(coordinate);
            }
            updateDrivingState(location);
            this.locationManager.setTestProviderLocation(location.getProvider(), location);
        } catch (Throwable th) {
            sLogger.e("feedLocation", th);
        }
    }

    public static GpsManager getInstance() {
        return singleton;
    }

    /* access modifiers changed from: private */
    public void markLocationFix(String str, String str2, boolean z, boolean z2) {
        Bundle bundle = new Bundle();
        bundle.putString(GpsConstants.TITLE, str);
        bundle.putString(GpsConstants.INFO, str2);
        bundle.putBoolean(GpsConstants.USING_PHONE_LOCATION, z);
        bundle.putBoolean(GpsConstants.USING_UBLOX_LOCATION, z2);
        sendGpsEventBroadcast(GpsConstants.GPS_EVENT_SWITCH, bundle);
    }

    /* access modifiers changed from: private */
    public void processNmea(String str) {
        Message obtainMessage = this.nmeaHandler.obtainMessage(1);
        obtainMessage.obj = str;
        this.nmeaHandler.sendMessage(obtainMessage);
    }

    /* access modifiers changed from: private */
    public void sendGpsEventBroadcast(String str, Bundle bundle) {
        try {
            Intent intent = new Intent(str);
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            if (SystemClock.elapsedRealtime() - this.gpsManagerStartTime <= 15000) {
                this.queue.add(intent);
                this.handler.removeCallbacks(this.queueCallback);
                this.handler.postDelayed(this.queueCallback, 15000);
                return;
            }
            Context appContext = HudApplication.getAppContext();
            UserHandle myUserHandle = Process.myUserHandle();
            if (this.queue.size() > 0) {
                this.queueCallback.run();
            }
            appContext.sendBroadcastAsUser(intent, myUserHandle);
        } catch (Throwable th) {
            sLogger.e(th);
        }
    }

    private void sendSpeechRequest(SpeechRequest speechRequest) {
        if (this.connectionService != null && this.connectionService.isConnected()) {
            try {
                sLogger.i("[Gps-loc] send speech request");
                this.connectionService.sendMessage(speechRequest);
            } catch (Throwable th) {
                sLogger.e(th);
            }
        }
    }

    private void setDriving(boolean z) {
        if (this.driving != z) {
            this.driving = z;
            sendGpsEventBroadcast(z ? GpsConstants.GPS_EVENT_DRIVING_STARTED : GpsConstants.GPS_EVENT_DRIVING_STOPPED, null);
        }
    }

    private void setUbloxAccuracyThreshold() {
        try {
            Context appContext = HudApplication.getAppContext();
            UserHandle myUserHandle = Process.myUserHandle();
            Intent intent = new Intent(SET_UBLOX_ACCURACY);
            intent.putExtra(UBLOX_ACCURACY, 30);
            appContext.sendBroadcastAsUser(intent, myUserHandle);
            sLogger.v("setUbloxAccuracyThreshold:30");
        } catch (Throwable th) {
            sLogger.e("setUbloxAccuracyThreshold", th);
        }
    }

    private void setUbloxResetRunnable() {
        sLogger.v("setUbloxResetRunnable");
        this.handler.postDelayed(this.warmResetRunnable, WARM_RESET_INTERVAL);
    }

    private void setUbloxTimeDiscardThreshold() {
        try {
            Context appContext = HudApplication.getAppContext();
            UserHandle myUserHandle = Process.myUserHandle();
            Intent intent = new Intent(SET_LOCATION_DISCARD_TIME_THRESHOLD);
            intent.putExtra(DISCARD_TIME, LOCATION_TIME_THROW_AWAY_THRESHOLD);
            appContext.sendBroadcastAsUser(intent, myUserHandle);
            sLogger.v("setUbloxTimeDiscardThreshold:2000");
        } catch (Throwable th) {
            sLogger.e("setUbloxTimeDiscardThreshold", th);
        }
    }

    /* access modifiers changed from: private */
    public void startSendingLocation() {
        if (this.connectionService != null && this.connectionService.isConnected()) {
            try {
                sLogger.i("[Gps-loc] phone start sending location");
                this.connectionService.sendMessage(START_SENDING_LOCATION);
            } catch (Throwable th) {
                sLogger.e(th);
            }
        }
    }

    /* access modifiers changed from: private */
    public void stopSendingLocation() {
//        if (this.keepPhoneGpsOn) {
//            sLogger.v("stopSendingLocation: keeping phone gps on");
//        } else if (this.isDebugTTSEnabled) {
//            sLogger.v("stopSendingLocation: not stopping, debug_tts enabled");
//        } else
        if (this.connectionService != null && this.connectionService.isConnected()) {
            try {
                sLogger.i("[Gps-loc] phone stop sending location");
                this.connectionService.sendMessage(STOP_SENDING_LOCATION);
            } catch (Throwable th) {
                sLogger.e(th);
            }
        }
    }

    /* access modifiers changed from: private */
    public void updateDrivingState(Location location) {
        if (this.driving) {
            if (location != null && ((double) location.getSpeed()) >= 0.22353333333333333d) {
                this.transitioning = false;
            } else if (!this.transitioning) {
                this.transitioning = true;
                this.transitionStartTime = SystemClock.elapsedRealtime();
            } else if (SystemClock.elapsedRealtime() - this.transitionStartTime > MINIMUM_DRIVE_TIME) {
                setDriving(false);
                this.transitioning = false;
            }
        } else if (location == null || ((double) location.getSpeed()) <= 2.2353333333333336d) {
            this.transitioning = false;
        } else if (!this.transitioning) {
            this.transitioning = true;
            this.transitionStartTime = SystemClock.elapsedRealtime();
        } else if (SystemClock.elapsedRealtime() - this.transitionStartTime > MINIMUM_DRIVE_TIME) {
            setDriving(true);
            this.transitioning = false;
        }
        this.handler.removeCallbacks(this.noLocationUpdatesRunnable);
        if (this.driving) {
            this.handler.postDelayed(this.noLocationUpdatesRunnable, MINIMUM_DRIVE_TIME);
        }
    }

    public void feedLocation(Coordinate coordinate) {
        boolean z = this.lastPhoneCoordinateTime > 0 || this.lastUbloxCoordinateTime > 0;
        long currentTimeMillis = System.currentTimeMillis();
        if (coordinate.accuracy.floatValue() < 30.0f || !z) {
            long longValue = currentTimeMillis - coordinate.timestamp.longValue();
            if (longValue >= 2000 && z) {
                sLogger.e("OLD location from phone(discard) time:" + longValue + ", timestamp:" + coordinate.timestamp + " now:" + currentTimeMillis + " lat:" + coordinate.latitude + " lng:" + coordinate.longitude);
            } else if (this.lastPhoneCoordinate != null && this.connectionService.getDevicePlatform() == Platform.PLATFORM_iOS && this.lastPhoneCoordinate.latitude == coordinate.latitude && this.lastPhoneCoordinate.longitude == coordinate.longitude && this.lastPhoneCoordinate.accuracy == coordinate.accuracy && this.lastPhoneCoordinate.altitude == coordinate.altitude && this.lastPhoneCoordinate.bearing == coordinate.bearing && this.lastPhoneCoordinate.speed == coordinate.speed) {
                sLogger.e("same location(discard) diff=" + (coordinate.timestamp.longValue() - this.lastPhoneCoordinate.timestamp.longValue()));
            } else {
                this.lastPhoneCoordinate = coordinate;
                this.lastPhoneCoordinateTime = SystemClock.elapsedRealtime();
                this.handler.removeCallbacks(this.noPhoneLocationFixRunnable);
                Location location = null;
                RouteRecorder instance = RouteRecorder.getInstance();
                if (instance.isRecording()) {
                    location = androidLocationFromCoordinate(coordinate);
                    instance.injectLocation(location, true);
                }
                if (this.activeLocationSource == null) {
                    sLogger.v("[Gps-loc] [LocationProvider] switched from [" + this.activeLocationSource + "] to [" + LocationSource.PHONE + "]");
                    stopUbloxReporting();
                    this.activeLocationSource = LocationSource.PHONE;
                    feedLocationToProvider(location, coordinate);
                    markLocationFix(GpsConstants.DEBUG_TTS_PHONE_SWITCH, "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox=n/a", true, false);
                } else if (this.activeLocationSource == LocationSource.PHONE) {
                    feedLocationToProvider(location, coordinate);
                } else if (SystemClock.elapsedRealtime() - this.lastUbloxCoordinateTime > MINIMUM_DRIVE_TIME) {
                    sLogger.v("[Gps-loc] [LocationProvider] switched from [" + this.activeLocationSource + "] to [" + LocationSource.PHONE + "]");
                    this.activeLocationSource = LocationSource.PHONE;
                    stopUbloxReporting();
                    feedLocationToProvider(location, coordinate);
                    markLocationFix(GpsConstants.DEBUG_TTS_PHONE_SWITCH, "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox =lost", true, false);
                }
            }
        } else {
            this.inaccurateGpsCount++;
            this.gpsAccuracy += coordinate.accuracy.floatValue();
            if (this.lastInaccurateGpsTime == 0 || currentTimeMillis - this.lastInaccurateGpsTime >= INACCURATE_GPS_REPORT_INTERVAL) {
                sLogger.e("BAD gps accuracy (discarding) avg:" + (this.gpsAccuracy / ((float) this.inaccurateGpsCount)) + ", count=" + this.inaccurateGpsCount + ", last coord: " + coordinate);
                this.inaccurateGpsCount = 0;
                this.gpsAccuracy = 0.0f;
                this.lastInaccurateGpsTime = currentTimeMillis;
            }
        }
    }

    public void setConnectionService(HudConnectionService hudConnectionService) {
        this.connectionService = hudConnectionService;
    }

    public void shutdown() {
        if (this.locationManager != null) {
            this.locationManager.removeUpdates(this.ubloxGpsLocationListener);
            this.locationManager.removeUpdates(this.navdyGpsLocationListener);
            this.locationManager.removeGpsStatusListener(this.ubloxGpsStatusListener);
            this.locationManager.removeNmeaListener(this.ubloxGpsNmeaListener);
        }
    }

    public void startUbloxReporting() {
        try {
            HudApplication.getAppContext().sendBroadcastAsUser(new Intent(START_UBLOX), Process.myUserHandle());
            this.startUbloxCalled = true;
            sLogger.v("started ublox reporting");
        } catch (Throwable th) {
            sLogger.e("startUbloxReporting", th);
        }
    }

    public void stopUbloxReporting() {
        try {
            this.startUbloxCalled = false;
            HudApplication.getAppContext().sendBroadcastAsUser(new Intent(STOP_UBLOX), Process.myUserHandle());
            sLogger.v("stopped ublox reporting");
        } catch (Throwable th) {
            sLogger.e("stopUbloxReporting", th);
        }
    }
}
