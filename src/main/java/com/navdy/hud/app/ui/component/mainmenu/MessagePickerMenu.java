package com.navdy.hud.app.ui.component.mainmenu;

import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import android.view.View;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import java.util.Iterator;
import java.util.ArrayList;
import kotlin.jvm.internal.TypeIntrinsics;
import kotlin.TypeCastException;
import java.util.UUID;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.glance.GlanceHandler;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import android.support.v4.content.ContextCompat;
import com.navdy.hud.app.HudApplication;
//import kotlin.jvm.internal.DefaultConstructorMarker;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.framework.contacts.Contact;
import java.util.List;
import android.content.res.Resources;
import com.navdy.service.library.log.Logger;
import android.content.Context;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 <2\u00020\u0001:\u0001<B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0001\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\b\u0010\f\u001a\u0004\u0018\u00010\t¢\u0006\u0002\u0010\rJ(\u0010\u0014\u001a\u0004\u0018\u00010\u00012\b\u0010\u0006\u001a\u0004\u0018\u00010\u00012\b\u0010\u0015\u001a\u0004\u0018\u00010\t2\b\u0010\u0016\u001a\u0004\u0018\u00010\tH\u0016J\b\u0010\u0017\u001a\u00020\u000fH\u0016J\u000e\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012H\u0016J\u0012\u0010\u0019\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u001a\u001a\u00020\u000fH\u0016J\n\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\b\u0010\u001d\u001a\u00020\u001eH\u0016J\b\u0010\u001f\u001a\u00020 H\u0016J\b\u0010!\u001a\u00020 H\u0016J\u0018\u0010\"\u001a\u00020 2\u0006\u0010#\u001a\u00020\u000f2\u0006\u0010\u001a\u001a\u00020\u000fH\u0016J.\u0010$\u001a\u00020%2\b\u0010&\u001a\u0004\u0018\u00010\u00132\b\u0010'\u001a\u0004\u0018\u00010(2\u0006\u0010\u001a\u001a\u00020\u000f2\b\u0010)\u001a\u0004\u0018\u00010*H\u0016J\b\u0010+\u001a\u00020%H\u0016J\b\u0010,\u001a\u00020%H\u0016J\u0012\u0010-\u001a\u00020%2\b\u0010.\u001a\u0004\u0018\u00010/H\u0016J\b\u00100\u001a\u00020%H\u0016J\u0012\u00101\u001a\u00020%2\b\u00102\u001a\u0004\u0018\u000103H\u0016J\u0010\u00104\u001a\u00020 2\u0006\u0010.\u001a\u00020/H\u0016J\u0010\u00105\u001a\u00020%2\u0006\u00106\u001a\u00020\tH\u0002J\u0010\u00107\u001a\u00020%2\u0006\u0010#\u001a\u00020\u000fH\u0016J\u0010\u00108\u001a\u00020%2\u0006\u00109\u001a\u00020\u000fH\u0016J\b\u0010:\u001a\u00020%H\u0016J\b\u0010;\u001a\u00020%H\u0016R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u000fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u0012X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006=" }, d2 = { "Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;", "vscrollComponent", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;", "presenter", "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;", "parent", "messages", "", "", "contact", "Lcom/navdy/hud/app/framework/contacts/Contact;", "notificationId", "(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/util/List;Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;)V", "backSelection", "", "backSelectionId", "cachedList", "", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "getChildMenu", "args", "path", "getInitialSelection", "getItems", "getModelfromPos", "pos", "getScrollIndex", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;", "getType", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;", "isBindCallsEnabled", "", "isFirstItemEmpty", "isItemClickable", "id", "onBindToView", "", "model", "view", "Landroid/view/View;", "state", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;", "onFastScrollEnd", "onFastScrollStart", "onItemSelected", "selection", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;", "onScrollIdle", "onUnload", "level", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;", "selectItem", "sendMessage", "message", "setBackSelectionId", "setBackSelectionPos", "n", "setSelectedIcon", "showToolTip", "Companion", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class MessagePickerMenu implements IMenu
{
    public static final Companion Companion;
    private static final int SELECTION_ANIMATION_DELAY = 1000;
    private static final VerticalList.Model back;
    private static final int baseMessageId = 100;
    private static final Context context;
    private static final int fluctuatorColor;
    private static final Logger logger;
    private static final int messageColor;
    private static final Resources resources;
    private int backSelection;
    private int backSelectionId;
    private List<VerticalList.Model> cachedList;
    private final Contact contact;
    private final List<String> messages;
    private final String notificationId;
    private final IMenu parent;
    protected final MainMenuScreen2.Presenter presenter;
    private final VerticalMenuComponent vscrollComponent;
    
    static {
        Companion = new Companion();
        logger = new Logger("MessagePickerMenu");
        context = HudApplication.getAppContext();
        resources = HudApplication.getAppContext().getResources();
        fluctuatorColor = ContextCompat.getColor(MessagePickerMenu.Companion.getContext(), R.color.mm_back);
        messageColor = ContextCompat.getColor(MessagePickerMenu.Companion.getContext(), R.color.share_location);
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, MessagePickerMenu.Companion.getFluctuatorColor(), MainMenu.bkColorUnselected, MessagePickerMenu.Companion.getFluctuatorColor(), MessagePickerMenu.Companion.getResources().getString(R.string.back), null);
    }
    
    public MessagePickerMenu(@NotNull final VerticalMenuComponent vscrollComponent, @NotNull final MainMenuScreen2.Presenter presenter, @NotNull final IMenu parent, @NotNull final List<String> messages, @NotNull final Contact contact, @Nullable final String notificationId) {
        Intrinsics.checkParameterIsNotNull(vscrollComponent, "vscrollComponent");
        Intrinsics.checkParameterIsNotNull(presenter, "presenter");
        Intrinsics.checkParameterIsNotNull(parent, "parent");
        Intrinsics.checkParameterIsNotNull(messages, "messages");
        Intrinsics.checkParameterIsNotNull(contact, "contact");
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
        this.messages = messages;
        this.contact = contact;
        this.notificationId = notificationId;
    }
    
    public static final /* synthetic */ VerticalList.Model access$getBack$cp() {
        return MessagePickerMenu.back;
    }
    
    public static final /* synthetic */ int access$getBaseMessageId$cp() {
        return MessagePickerMenu.baseMessageId;
    }
    
    public static final /* synthetic */ Context access$getContext$cp() {
        return MessagePickerMenu.context;
    }
    
    public static final /* synthetic */ int access$getFluctuatorColor$cp() {
        return MessagePickerMenu.fluctuatorColor;
    }
    
    @NotNull
    public static final /* synthetic */ Logger access$getLogger$cp() {
        return MessagePickerMenu.logger;
    }
    
    public static final /* synthetic */ int access$getMessageColor$cp() {
        return MessagePickerMenu.messageColor;
    }
    
    public static final /* synthetic */ Resources access$getResources$cp() {
        return MessagePickerMenu.resources;
    }
    
    public static final /* synthetic */ int access$getSELECTION_ANIMATION_DELAY$cp() {
        return MessagePickerMenu.SELECTION_ANIMATION_DELAY;
    }
    
    private final void sendMessage(final String s) {
        MessagePickerMenu.Companion.getLogger().v("sendMessage:" + this.contact.number);
        final GlanceHandler instance = GlanceHandler.getInstance();
        if (this.notificationId != null) {
            NotificationManager.getInstance().removeNotification(this.notificationId);
        }
        if (instance.sendMessage(this.contact.number, s, this.contact.name)) {
            instance.sendSmsSuccessNotification(UUID.randomUUID().toString(), this.contact.number, s, this.contact.name);
        }
        else {
            instance.sendSmsFailedNotification(this.contact.number, s, this.contact.name);
        }
    }
    
    @Nullable
    @Override
    public IMenu getChildMenu(@Nullable final IMenu menu, @Nullable final String s, @Nullable final String s2) {
        return null;
    }
    
    @Override
    public int getInitialSelection() {
        return 1;
    }
    
    @NotNull
    @Override
    public List<VerticalList.Model> getItems() {
        List<VerticalList.Model> mutableList;
        if (this.cachedList != null) {
            final List<VerticalList.Model> cachedList = this.cachedList;
            if (cachedList == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.MutableList<com.navdy.hud.app.ui.component.vlist.VerticalList.Model>");
            }
            mutableList = (List<VerticalList.Model>)TypeIntrinsics.asMutableList(cachedList);
        }
        else {
            final ArrayList<VerticalList.Model> list = new ArrayList<VerticalList.Model>();
            list.add(MessagePickerMenu.Companion.getBack());
            int n = 0;
            final Iterator<String> iterator = this.messages.iterator();
            while (iterator.hasNext()) {
                final VerticalList.Model buildModel = IconBkColorViewHolder.buildModel(MessagePickerMenu.Companion.getBaseMessageId() + n, R.drawable.icon_message, MessagePickerMenu.Companion.getMessageColor(), MainMenu.bkColorUnselected, MessagePickerMenu.Companion.getMessageColor(), iterator.next(), null);
                Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu… messageColor, msg, null)");
                list.add(buildModel);
                ++n;
            }
            this.cachedList = list;
            mutableList = list;
        }
        return mutableList;
    }
    
    @Nullable
    @Override
    public VerticalList.Model getModelfromPos(final int n) {
        final List<VerticalList.Model> cachedList = this.cachedList;
        VerticalList.Model model;
        if (cachedList != null && cachedList.size() > n) {
            model = cachedList.get(n);
        }
        else {
            model = null;
        }
        return model;
    }
    
    @Nullable
    @Override
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    @NotNull
    @Override
    public Menu getType() {
        return Menu.MESSAGE_PICKER;
    }
    
    @Override
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    @Override
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    @Override
    public boolean isItemClickable(final int n, final int n2) {
        return true;
    }
    
    @Override
    public void onBindToView(@Nullable final VerticalList.Model model, @Nullable final View view, final int n, @Nullable final VerticalList.ModelState modelState) {
    }
    
    @Override
    public void onFastScrollEnd() {
    }
    
    @Override
    public void onFastScrollStart() {
    }
    
    @Override
    public void onItemSelected(@Nullable final VerticalList.ItemSelectionState itemSelectionState) {
    }
    
    @Override
    public void onScrollIdle() {
    }
    
    @Override
    public void onUnload(@Nullable final MenuLevel menuLevel) {
    }
    
    @Override
    public boolean selectItem(@NotNull final VerticalList.ItemSelectionState itemSelectionState) {
        Intrinsics.checkParameterIsNotNull(itemSelectionState, "selection");
        MessagePickerMenu.Companion.getLogger().v("select id:" + itemSelectionState.id + " pos:" + itemSelectionState.pos);
        switch (itemSelectionState.id) {
            default:
                this.presenter.performSelectionAnimation((Runnable) new Runnable() {

                    public final void run() {
                        MessagePickerMenu.this.presenter.close(new Runnable() {
                            public final void run() {
                                MessagePickerMenu.this.sendMessage((String) MessagePickerMenu.this.messages.get(itemSelectionState.id - Companion.getBaseMessageId()));
                            }
                        });
                    }
                }, MessagePickerMenu.Companion.getSELECTION_ANIMATION_DELAY());
                break;
            case R.id.menu_back:
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                this.backSelectionId = 0;
                break;
        }
        return true;
    }
    
    @Override
    public void setBackSelectionId(final int backSelectionId) {
        this.backSelectionId = backSelectionId;
    }
    
    @Override
    public void setBackSelectionPos(final int backSelection) {
        this.backSelection = backSelection;
    }
    
    @Override
    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_message, MessagePickerMenu.Companion.getMessageColor(), null, 1.0f);
        this.vscrollComponent.selectedText.setText((CharSequence)MessagePickerMenu.Companion.getResources().getString(R.string.message));
    }
    
    @Override
    public void showToolTip() {
    }
    
    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082D¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u001c\u0010\u0007\u001a\n \t*\u0004\u0018\u00010\b0\bX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\u00020\u0004X\u0082D¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u0006R\u001c\u0010\u000e\u001a\n \t*\u0004\u0018\u00010\u000f0\u000fX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0012\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0006R\u0014\u0010\u0014\u001a\u00020\u0015X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0006R\u001c\u0010\u001a\u001a\n \t*\u0004\u0018\u00010\u001b0\u001bX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001d¨\u0006\u001e" }, d2 = { "Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;", "", "()V", "SELECTION_ANIMATION_DELAY", "", "getSELECTION_ANIMATION_DELAY", "()I", "back", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "kotlin.jvm.PlatformType", "getBack", "()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "baseMessageId", "getBaseMessageId", "context", "Landroid/content/Context;", "getContext", "()Landroid/content/Context;", "fluctuatorColor", "getFluctuatorColor", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "messageColor", "getMessageColor", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    public static final class Companion
    {
        private final VerticalList.Model getBack() {
            return MessagePickerMenu.access$getBack$cp();
        }
        
        private final int getBaseMessageId() {
            return MessagePickerMenu.access$getBaseMessageId$cp();
        }
        
        private final Context getContext() {
            return MessagePickerMenu.access$getContext$cp();
        }
        
        private final int getFluctuatorColor() {
            return MessagePickerMenu.access$getFluctuatorColor$cp();
        }
        
        private final Logger getLogger() {
            return MessagePickerMenu.access$getLogger$cp();
        }
        
        private final int getMessageColor() {
            return MessagePickerMenu.access$getMessageColor$cp();
        }
        
        private final Resources getResources() {
            return MessagePickerMenu.access$getResources$cp();
        }
        
        private final int getSELECTION_ANIMATION_DELAY() {
            return MessagePickerMenu.access$getSELECTION_ANIMATION_DELAY$cp();
        }
    }
}
