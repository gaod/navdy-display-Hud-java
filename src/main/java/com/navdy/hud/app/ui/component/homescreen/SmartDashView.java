package com.navdy.hud.app.ui.component.homescreen;

import com.navdy.hud.app.R;
import com.navdy.hud.app.maps.MapEvents$LocationFix;
import com.navdy.hud.app.maps.MapEvents$ManeuverDisplay;
import com.navdy.obd.Pids;
import com.navdy.service.library.events.preferences.MiddleGauge;
import com.navdy.service.library.events.preferences.ScrollableSide;
import android.os.Bundle;
import android.view.View;
import com.navdy.hud.app.view.DashboardWidgetPresenter;
import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import android.os.SystemClock;
import com.navdy.hud.app.manager.InputManager;
import android.content.res.Resources;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.device.gps.GpsUtils;
import android.location.Location;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.service.library.events.preferences.DashboardPreferences;
import com.squareup.otto.Subscribe;
import com.navdy.obd.PidSet;
import com.navdy.hud.app.maps.MapSettings;
import android.text.TextUtils;
import android.animation.Animator;
import android.animation.Animator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.view.ViewGroup;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator;
import android.view.ViewGroup;
import android.animation.AnimatorSet;
import com.navdy.hud.app.view.MainView;
import android.animation.AnimatorSet;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.view.DashboardWidgetView;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.framework.DriverProfileHelper;
import android.util.AttributeSet;
import android.content.Context;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import com.navdy.hud.app.view.TachometerGaugePresenter;
import com.navdy.hud.app.view.SpeedometerGaugePresenter2;
import com.navdy.hud.app.view.GaugeView;
import com.navdy.hud.app.ui.component.dashboard.GaugeViewPager;
import android.view.ViewGroup;
import com.navdy.hud.app.util.HeadingDataUtil;
import android.content.SharedPreferences;
import butterknife.InjectView;
import android.widget.TextView;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter;

import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.AMBIENT_TEMPERATURE_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ANALOG_CLOCK_WIDGET_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.COMPASS_WIDGET_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.DRIVE_SCORE_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.EMPTY_WIDGET_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ENGINE_OIL_TEMPERATURE_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.FUEL_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.FUEL_GAUGE_RANGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.INTAKE_PRESSURE_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.MPG_AVG_WIDGET_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SPEED_LIMIT_SIGN_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.TRAFFIC_INCIDENT_GAUGE_ID;

public class SmartDashView extends BaseSmartDashView implements IDashboardOptionsAdapter
{
    public static final int HEADING_EXPIRE_INTERVAL = 3000;
    public static final long MINIMUM_INTERVAL_BETWEEN_RECORDING_USER_PREFERENCE;
    public static final float NAVIGATION_MODE_SCALE_FACTOR = 0.8f;
    private static final boolean SHOW_ETA = false;
    private static final Logger sLogger;
    private int activeModeGaugeMargin;
    private String activeRightGaugeId;
    private String activeleftGaugeId;
    @InjectView(R.id.eta_time_amPm)
    TextView etaAmPm;
    @InjectView(R.id.eta_time)
    TextView etaText;
    private int fullModeGaugeTopMargin;
    private SharedPreferences globalPreferences;
    private HeadingDataUtil headingDataUtil;
    public boolean isShowingDriveScoreGauge;
    private String lastETA;
    private String lastETA_AmPm;
    private double lastHeading;
    private long lastHeadingSampleTime;
    private long lastUserPreferenceRecordedTime;
    @InjectView(R.id.eta_layout)
    ViewGroup mEtaLayout;
    GaugeViewPagerAdapter mLeftAdapter;
    @InjectView(R.id.left_gauge)
    GaugeViewPager mLeftGaugeViewPager;
    @InjectView(R.id.middle_gauge)
    GaugeView mMiddleGaugeView;
    GaugeViewPagerAdapter mRightAdapter;
    @InjectView(R.id.right_gauge)
    GaugeViewPager mRightGaugeViewPager;
    private SmartDashWidgetManager mSmartDashWidgetManager;
    private int middleGauge;
    private int middleGaugeShrinkEarlyTbtOffset;
    private int middleGaugeShrinkMargin;
    private int middleGaugeShrinkModeMargin;
    private boolean paused;
    private int scrollableSide;
    SpeedometerGaugePresenter2 speedometerGaugePresenter2;
    TachometerGaugePresenter tachometerPresenter;

    static {
        sLogger = new Logger(SmartDashView.class);
        MINIMUM_INTERVAL_BETWEEN_RECORDING_USER_PREFERENCE = TimeUnit.MINUTES.toMillis(15L);
    }

    public SmartDashView(final Context context) {
        super(context);
        this.lastUserPreferenceRecordedTime = 0L;
        this.lastHeading = 0.0;
        this.lastHeadingSampleTime = 0L;
        this.middleGauge = 1;
        this.scrollableSide = 1;
        this.headingDataUtil = new HeadingDataUtil();
    }

    public SmartDashView(final Context context, final AttributeSet set) {
        super(context, set);
        this.lastUserPreferenceRecordedTime = 0L;
        this.lastHeading = 0.0;
        this.lastHeadingSampleTime = 0L;
        this.middleGauge = 1;
        this.scrollableSide = 1;
        this.headingDataUtil = new HeadingDataUtil();
    }

    public SmartDashView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.lastUserPreferenceRecordedTime = 0L;
        this.lastHeading = 0.0;
        this.lastHeadingSampleTime = 0L;
        this.middleGauge = 1;
        this.scrollableSide = 1;
        this.headingDataUtil = new HeadingDataUtil();
    }

    private void savePreference(final String s, final Object o) {
        final boolean defaultProfile = DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        final SharedPreferences driverPreferences = this.homeScreenView.getDriverPreferences();
        if (!defaultProfile) {
            if (o instanceof String) {
                driverPreferences.edit().putString(s, (String)o).apply();
            }
            else if (o instanceof Integer) {
                driverPreferences.edit().putInt(s, (int)o).apply();
            }
        }
        if (o instanceof String) {
            this.globalPreferences.edit().putString(s, (String)o).apply();
        }
        else if (o instanceof Integer) {
            this.globalPreferences.edit().putInt(s, (int)o).apply();
        }
    }

    private void showEta(final boolean b) {
        if (this.mEtaLayout.getVisibility() == VISIBLE) {
            this.mEtaLayout.setVisibility(GONE);
        }
    }

    private void showMiddleGaugeAccordingToUserPreferences() {
        SharedPreferences sharedPreferences;
        if (DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile()) {
            sharedPreferences = this.globalPreferences;
        }
        else {
            sharedPreferences = this.homeScreenView.getDriverPreferences();
        }
        this.middleGauge = sharedPreferences.getInt("PREFERENCE_MIDDLE_GAUGE", 1);
        SmartDashView.sLogger.d("Middle Gauge : " + this.middleGauge);
        ObdManager.getInstance();
        switch (this.middleGauge) {
            case 0:
                this.showTachometer();
                break;
            case 1:
                this.showSpeedometerGauge();
                break;
        }
    }

    private void showSpeedometerGauge() {
        this.tachometerPresenter.onPause();
        this.tachometerPresenter.setWidgetVisibleToUser(false);
        this.tachometerPresenter.setView(null);
        if (!this.paused) {
            this.speedometerGaugePresenter2.onResume();
        }
        this.speedometerGaugePresenter2.setWidgetVisibleToUser(true);
        this.speedometerGaugePresenter2.setView(this.mMiddleGaugeView);
        this.speedometerGaugePresenter2.setSpeed(SpeedManager.getInstance().getCurrentSpeed());
    }

    private void showTachometer() {
        this.speedometerGaugePresenter2.onPause();
        this.speedometerGaugePresenter2.setWidgetVisibleToUser(false);
        this.speedometerGaugePresenter2.setView(null);
        if (!this.paused) {
            this.tachometerPresenter.onResume();
        }
        this.tachometerPresenter.setWidgetVisibleToUser(true);
        this.tachometerPresenter.setView(this.mMiddleGaugeView);
        final int engineRpm = ObdManager.getInstance().getEngineRpm();
        if (engineRpm != -1 && this.tachometerPresenter != null) {
            this.tachometerPresenter.setRPM(engineRpm);
        }
        final int currentSpeed = SpeedManager.getInstance().getCurrentSpeed();
        if (this.tachometerPresenter != null) {
            this.tachometerPresenter.setSpeed(currentSpeed);
        }
    }

    private void showViewsAccordingToUserPreferences() {
        SmartDashView.sLogger.d("showViewsAccordingToUserPreferences");
        final boolean defaultProfile = DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        SharedPreferences sharedPreferences;
        if (defaultProfile) {
            sharedPreferences = this.globalPreferences;
        }
        else {
            sharedPreferences = this.homeScreenView.getDriverPreferences();
        }
        this.scrollableSide = sharedPreferences.getInt("PREFERENCE_SCROLLABLE_SIDE", 1);
        SmartDashView.sLogger.v("scrollableSide:" + this.scrollableSide);
        this.showMiddleGaugeAccordingToUserPreferences();
        final String string = sharedPreferences.getString("PREFERENCE_LEFT_GAUGE", ANALOG_CLOCK_WIDGET_ID);
        final int indexForWidgetIdentifier = this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(string);
        SmartDashView.sLogger.d("Left Gauge ID : " + string + ", Index : " + indexForWidgetIdentifier);
        int indexForWidgetIdentifier2 = indexForWidgetIdentifier;
        if (indexForWidgetIdentifier < 0) {
            indexForWidgetIdentifier2 = this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(EMPTY_WIDGET_ID);
        }
        this.mLeftGaugeViewPager.setSelectedPosition(indexForWidgetIdentifier2);
        final String string2 = sharedPreferences.getString("PREFERENCE_RIGHT_GAUGE", COMPASS_WIDGET_ID);
        final int indexForWidgetIdentifier3 = this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(string2);
        SmartDashView.sLogger.d("Right Gauge ID : " + string2 + ", Index : " + indexForWidgetIdentifier3);
        int indexForWidgetIdentifier4;
        if ((indexForWidgetIdentifier4 = indexForWidgetIdentifier3) < 0) {
            indexForWidgetIdentifier4 = this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(EMPTY_WIDGET_ID);
        }
        this.mRightGaugeViewPager.setSelectedPosition(indexForWidgetIdentifier4);
        if (!defaultProfile) {
            this.globalPreferences.edit().putInt("PREFERENCE_MIDDLE_GAUGE", this.middleGauge).putInt("PREFERENCE_SCROLLABLE_SIDE", this.scrollableSide).putString("PREFERENCE_LEFT_GAUGE", string).putString("PREFERENCE_RIGHT_GAUGE", string2).apply();
        }
        this.mLeftAdapter.setExcludedPosition(indexForWidgetIdentifier4);
        this.mRightAdapter.setExcludedPosition(indexForWidgetIdentifier2);
        this.recordUsersWidgetPreference();
    }

    @Override
    public void ObdPidChangeEvent(final ObdManager.ObdPidChangeEvent obdPidChangeEvent) {
        super.ObdPidChangeEvent(obdPidChangeEvent);
        switch (obdPidChangeEvent.pid.getId()) {
            case 12:
                if (this.tachometerPresenter != null) {
                    this.tachometerPresenter.setRPM((int)obdPidChangeEvent.pid.getValue());
                    break;
                }
                break;
            case 47:
                this.mSmartDashWidgetManager.updateWidget(FUEL_GAUGE_ID, obdPidChangeEvent.pid.getValue());   
                break;
            case 256:
                this.mSmartDashWidgetManager.updateWidget(MPG_AVG_WIDGET_ID, obdPidChangeEvent.pid.getValue());
                break;
            case 5:
                this.mSmartDashWidgetManager.updateWidget(ENGINE_TEMPERATURE_GAUGE_ID, obdPidChangeEvent.pid.getValue());
                break;
            case Pids.ENGINE_OIL_TEMPERATURE:
                this.mSmartDashWidgetManager.updateWidget(ENGINE_OIL_TEMPERATURE_GAUGE_ID, obdPidChangeEvent.pid.getValue());
                break;
            case Pids.AMBIENT_AIR_TEMRERATURE:
                this.mSmartDashWidgetManager.updateWidget(AMBIENT_TEMPERATURE_GAUGE_ID, obdPidChangeEvent.pid.getValue());
                break;
            case Pids.MANIFOLD_AIR_PRESSURE:
                this.mSmartDashWidgetManager.updateWidget(INTAKE_PRESSURE_GAUGE_ID, obdPidChangeEvent.pid.getValue());
                break;
            case Pids.DISTANCE_TO_EMPTY:
                this.mSmartDashWidgetManager.updateWidget(FUEL_GAUGE_RANGE_ID, obdPidChangeEvent.pid.getValue());
                break;
                
        }
    }

    public void adjustDashHeight(final int n) {
        super.adjustDashHeight(n);
    }

    @Override
    protected void animateToFullMode() {
        super.animateToFullMode();
    }

    @Override
    protected void animateToFullModeInternal(final AnimatorSet.Builder animatorSet$Builder) {
        super.animateToFullModeInternal(animatorSet$Builder);
    }

    public String getActiveRightGaugeId() {
        return this.activeRightGaugeId;
    }

    public String getActiveleftGaugeId() {
        return this.activeleftGaugeId;
    }

    @Override
    public int getCurrentScrollableSideOption() {
        int n;
        if (this.scrollableSide == 0) {
            n = 1;
        }
        else {
            n = 0;
        }
        return n;
    }

    @Override
    public void getCustomAnimator(final MainView.CustomAnimationMode customAnimationMode, final AnimatorSet.Builder animatorSet$Builder) {
        SmartDashView.sLogger.v("getCustomAnimator" + customAnimationMode);
        super.getCustomAnimator(customAnimationMode, animatorSet$Builder);
        final AnimatorSet set = new AnimatorSet();
        switch (customAnimationMode) {
            case EXPAND: {
                final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup.MarginLayoutParams)this.mMiddleGaugeView.getLayoutParams();
                final int leftMargin = viewGroup$MarginLayoutParams.leftMargin;
                final ValueAnimator valueAnimator = new ValueAnimator();
                valueAnimator.setIntValues(leftMargin, 0);
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                        viewGroup$MarginLayoutParams.leftMargin = (int)valueAnimator.getAnimatedValue();
                        SmartDashView.this.mMiddleGaugeView.setLayoutParams(viewGroup$MarginLayoutParams);
                    }
                });
                valueAnimator.addListener(new DefaultAnimationListener() {
                    @Override
                    public void onAnimationEnd(final Animator animator) {
                        SmartDashView.this.mLeftGaugeViewPager.setVisibility(View.VISIBLE);
                        SmartDashView.this.mRightGaugeViewPager.setVisibility(View.VISIBLE);
                        if (SmartDashView.this.homeScreenView.isNavigationActive()) {
                            SmartDashView.this.showEta(true);
                        }
                    }
                });
                set.playTogether(valueAnimator);
                break;
            }
            case SHRINK_LEFT:
            case SHRINK_MODE: {
                this.mLeftGaugeViewPager.setVisibility(GONE);
                this.mRightGaugeViewPager.setVisibility(GONE);
                this.showEta(false);
                final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams2 = (ViewGroup.MarginLayoutParams)this.mMiddleGaugeView.getLayoutParams();
                final int leftMargin2 = viewGroup$MarginLayoutParams2.leftMargin;
                int n;
                if (customAnimationMode == MainView.CustomAnimationMode.SHRINK_LEFT) {
                    n = this.middleGaugeShrinkMargin;
                }
                else {
                    n = this.middleGaugeShrinkModeMargin;
                }
                final ValueAnimator valueAnimator2 = new ValueAnimator();
                valueAnimator2.setIntValues(leftMargin2, n);
                valueAnimator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                        viewGroup$MarginLayoutParams2.leftMargin = (int)valueAnimator.getAnimatedValue();
                        SmartDashView.this.mMiddleGaugeView.setLayoutParams(viewGroup$MarginLayoutParams2);
                    }
                });
                set.playTogether(valueAnimator2);
                break;
            }
        }
        animatorSet$Builder.with(set);
    }

    @Override
    public int getMiddleGaugeOption() {
        int n;
        if (this.middleGauge == 0) {
            n = 1;
        }
        else {
            n = 0;
        }
        return n;
    }

    @Override
    public void init(final HomeScreenView homeScreenView) {
        this.globalPreferences = homeScreenView.globalPreferences;
        this.mSmartDashWidgetManager = new SmartDashWidgetManager(this.globalPreferences, this.getContext());


        homeScreenView.getDriverPreferences();
        this.mSmartDashWidgetManager.setFilter(new SmartDashWidgetManager.IWidgetFilter() {
            @Override
            public boolean filter(final String s) {
                final boolean b = true;
                final boolean b2 = false;
                final SharedPreferences driverPreferences = homeScreenView.getDriverPreferences();
                final String string = driverPreferences.getString("PREFERENCE_LEFT_GAUGE", ANALOG_CLOCK_WIDGET_ID);
                final String string2 = driverPreferences.getString("PREFERENCE_RIGHT_GAUGE", COMPASS_WIDGET_ID);
                Label_0073: {
                    if (TextUtils.isEmpty(s)) {
                        break Label_0073;
                    }
                    if (!s.equals(string)) {
                        if (!s.equals(string2)) {
                            break Label_0073;
                        }
                    }
                    return false;
                }
                switch (s) {
                    default: {
                        return b2;
                    }
                    case TRAFFIC_INCIDENT_GAUGE_ID: {
                        return !MapSettings.isTrafficDashWidgetsEnabled();
                    }
                    case FUEL_GAUGE_ID: {
                        final PidSet supportedPids = ObdManager.getInstance().getSupportedPids();
                        if (supportedPids != null) {
                            if (supportedPids.contains(47)) {
                                return b2;
                            }
                        }
                        return true;
                    }
                    case INTAKE_PRESSURE_GAUGE_ID: {
                        final PidSet supportedPids = ObdManager.getInstance().getSupportedPids();
                        if (supportedPids != null) {
                            if (supportedPids.contains(Pids.MANIFOLD_AIR_PRESSURE)) {
                                return b2;
                            }
                        }
                        return true;
                    }
                    case ENGINE_TEMPERATURE_GAUGE_ID: {
                        final PidSet supportedPids2 = ObdManager.getInstance().getSupportedPids();
                        if (supportedPids2 != null) {
                            if (supportedPids2.contains(5)) {
                                return b2;
                            }
                        }
                        return true;
                    }
                    case ENGINE_OIL_TEMPERATURE_GAUGE_ID: {
                        final PidSet supportedPids2 = ObdManager.getInstance().getSupportedPids();
                        if (supportedPids2 != null) {
                            if (supportedPids2.contains(Pids.ENGINE_OIL_TEMPERATURE)) {
                                return b2;
                            }
                        }
                        return true;
                    }
                    case AMBIENT_TEMPERATURE_GAUGE_ID: {
                        final PidSet supportedPids2 = ObdManager.getInstance().getSupportedPids();
                        if (supportedPids2 != null) {
                            if (supportedPids2.contains(Pids.AMBIENT_AIR_TEMRERATURE)) {
                                return b2;
                            }
                        }
                        return true;
                    }
                    case MPG_AVG_WIDGET_ID: {
                        final PidSet supportedPids3 = ObdManager.getInstance().getSupportedPids();
                        if (supportedPids3 != null) {
                            if (supportedPids3.contains(256)) {
                                return b2;
                            }
                        }
                        return true;
                    }
                }
            }
        });
        this.mSmartDashWidgetManager.registerForChanges(this);
        this.tachometerPresenter = new TachometerGaugePresenter(this.getContext());
        this.speedometerGaugePresenter2 = new SpeedometerGaugePresenter2(this.getContext());
        this.bus.register(new Object() {
            @Subscribe
            public void ObdStateChangeEvent(final ObdManager.ObdConnectionStatusEvent obdConnectionStatusEvent) {
                final ObdManager instance = ObdManager.getInstance();
                if (SmartDashView.this.tachometerPresenter != null) {
                    int engineRpm;
                    if ((engineRpm = instance.getEngineRpm()) < 0) {
                        engineRpm = 0;
                    }
                    SmartDashView.this.tachometerPresenter.setRPM(engineRpm);
                }
                if (SmartDashView.this.mSmartDashWidgetManager != null) {
                    SmartDashView.this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
                }
            }

            @Subscribe
            public void onDashboardPreferences(final DashboardPreferences dashboardPreferences) {
                if (dashboardPreferences != null) {
                    if (dashboardPreferences.middleGauge != null) {
                        switch (dashboardPreferences.middleGauge) {
                            case SPEEDOMETER:
                                SmartDashView.this.savePreference("PREFERENCE_MIDDLE_GAUGE", 1);
                                break;
                            case TACHOMETER:
                                SmartDashView.this.savePreference("PREFERENCE_MIDDLE_GAUGE", 0);
                                break;
                        }
                    }
                    if (dashboardPreferences.scrollableSide != null) {
                        switch (dashboardPreferences.scrollableSide) {
                            case LEFT:
                                SmartDashView.this.savePreference("PREFERENCE_SCROLLABLE_SIDE", 0);
                                break;
                            case RIGHT:
                                SmartDashView.this.savePreference("PREFERENCE_SCROLLABLE_SIDE", 1);
                                break;
                        }
                    }
                    if (SmartDashWidgetManager.isValidGaugeId(dashboardPreferences.leftGaugeId)) {
                        SmartDashView.this.savePreference("PREFERENCE_LEFT_GAUGE", dashboardPreferences.leftGaugeId);
                    }
                    if (SmartDashWidgetManager.isValidGaugeId(dashboardPreferences.rightGaugeId)) {
                        SmartDashView.this.savePreference("PREFERENCE_RIGHT_GAUGE", dashboardPreferences.rightGaugeId);
                    }
                    SmartDashView.this.showViewsAccordingToUserPreferences();
                }
            }

            @Subscribe
            public void onDriverProfileChanged(final DriverProfileChanged driverProfileChanged) {
                homeScreenView.updateDriverPrefs();
                if (SmartDashView.this.mSmartDashWidgetManager != null) {
                    SmartDashView.this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
                }
            }

            @Subscribe
            public void onGpsLocationChanged(final Location location) {
                SmartDashView.this.headingDataUtil.setHeading(location.getBearing());
                final double heading = SmartDashView.this.headingDataUtil.getHeading();
                if (SmartDashView.sLogger.isLoggable(2)) {
                    SmartDashView.sLogger.v("SmartDash New: Heading :" + heading + ", " + GpsUtils.getHeadingDirection(heading) + ", Provider :" + location.getProvider());
                }
                SmartDashView.this.mSmartDashWidgetManager.updateWidget(COMPASS_WIDGET_ID, heading);
            }

            @Subscribe
            public void onLocationFixChangeEvent(final MapEvents$LocationFix locationFix) {
                if (!locationFix.locationAvailable) {
                    SmartDashView.this.headingDataUtil.reset();
                    SmartDashView.this.mSmartDashWidgetManager.updateWidget(COMPASS_WIDGET_ID, 0.0f);
                }
            }

            @Subscribe
            public void onSupportedPidEventsChange(final ObdManager.ObdSupportedPidsChangedEvent obdSupportedPidsChangedEvent) {
                SmartDashView.this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
            }

            @Subscribe
            public void onUserGaugePreferencesChanged(final SmartDashWidgetManager.UserPreferenceChanged userPreferenceChanged) {
                SmartDashView.this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
            }

            @Subscribe
            public void updateETA(final MapEvents$ManeuverDisplay maneuverDisplay) {
                if (homeScreenView.isNavigationActive()) {
                    if (maneuverDisplay.etaDate == null) {
                        SmartDashView.sLogger.w("etaDate is not set");
                    }
                    else {
                        if (!TextUtils.equals(maneuverDisplay.eta, SmartDashView.this.lastETA)) {
                            SmartDashView.this.lastETA = maneuverDisplay.eta;
                            SmartDashView.this.etaText.setText(SmartDashView.this.lastETA);
                        }
                        if (!TextUtils.equals(maneuverDisplay.etaAmPm, SmartDashView.this.lastETA_AmPm)) {
                            SmartDashView.this.lastETA_AmPm = maneuverDisplay.etaAmPm;
                            SmartDashView.this.etaAmPm.setText(SmartDashView.this.lastETA_AmPm);
                        }
                    }
                }
            }
        });
        final Resources resources = this.getResources();
        this.middleGaugeShrinkMargin = resources.getDimensionPixelSize(R.dimen.middle_gauge_shrink_margin);
        this.middleGaugeShrinkModeMargin = resources.getDimensionPixelSize(R.dimen.middle_gauge_shrink_mode_margin);
        this.middleGaugeShrinkEarlyTbtOffset = this.getResources().getDimensionPixelSize(R.dimen.middle_gauge_shrink_early_tbt_offset);
        super.init(homeScreenView);
    }

    @Override
    protected void initializeView() {
        super.initializeView();
        this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
        if (this.mLeftAdapter == null) {
            this.mLeftAdapter = new GaugeViewPagerAdapter(this.mSmartDashWidgetManager, true);
            this.mLeftGaugeViewPager.setAdapter(this.mLeftAdapter);
            this.mLeftGaugeViewPager.setChangeListener(new GaugeViewPager.ChangeListener() {
                @Override
                public void onGaugeChanged(final int excludedPosition) {
                    final String widgetIdentifierForIndex = SmartDashView.this.mSmartDashWidgetManager.getWidgetIdentifierForIndex(excludedPosition);
                    SmartDashView.sLogger.d("onGaugeChanged, (Left) , Widget ID : " + widgetIdentifierForIndex + " , Position : " + excludedPosition);
                    SmartDashView.this.savePreference("PREFERENCE_LEFT_GAUGE", widgetIdentifierForIndex);
                    SmartDashView.this.mRightAdapter.setExcludedPosition(excludedPosition);
                    SmartDashView.this.mRightGaugeViewPager.updateState();
                }
            });
        }
        if (this.mRightAdapter == null) {
            this.mRightAdapter = new GaugeViewPagerAdapter(this.mSmartDashWidgetManager, false);
            this.mRightGaugeViewPager.setAdapter(this.mRightAdapter);
            this.mRightGaugeViewPager.setChangeListener(new GaugeViewPager.ChangeListener() {
                @Override
                public void onGaugeChanged(final int excludedPosition) {
                    final String widgetIdentifierForIndex = SmartDashView.this.mSmartDashWidgetManager.getWidgetIdentifierForIndex(excludedPosition);
                    SmartDashView.sLogger.d("onGaugeChanged, (Right) , Widget ID : " + widgetIdentifierForIndex + " , Position : " + excludedPosition);
                    SmartDashView.this.savePreference("PREFERENCE_RIGHT_GAUGE", widgetIdentifierForIndex);
                    SmartDashView.this.mLeftAdapter.setExcludedPosition(excludedPosition);
                    SmartDashView.this.mLeftGaugeViewPager.updateState();
                }
            });
        }
        this.showViewsAccordingToUserPreferences();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.fullModeGaugeTopMargin = this.getResources().getDimensionPixelSize(R.dimen.gauge_frame_top_margin_full_mode);
        this.activeModeGaugeMargin = this.getResources().getDimensionPixelSize(R.dimen.gauge_frame_top_margin_active_mode);
    }

    @Override
    public boolean onKey(final InputManager.CustomKeyEvent customKeyEvent) {
        GaugeViewPager gaugeViewPager;
        if (this.scrollableSide == 1) {
            gaugeViewPager = this.mRightGaugeViewPager;
        }
        else {
            gaugeViewPager = this.mLeftGaugeViewPager;
        }
        switch (customKeyEvent) {
            case LEFT:
                gaugeViewPager.moveNext();
                break;
            case RIGHT:
                gaugeViewPager.movePrevious();
                break;
        }
        return false;
    }

    @Override
    public void onPause() {
        if (!this.paused) {
            SmartDashView.sLogger.v("::onPause");
            this.paused = true;
            if (this.mSmartDashWidgetManager != null) {
                SmartDashView.sLogger.v("widgets disabled");
                this.mSmartDashWidgetManager.onPause();
                if (this.tachometerPresenter != null) {
                    this.tachometerPresenter.onPause();
                }
                if (this.speedometerGaugePresenter2 != null) {
                    this.speedometerGaugePresenter2.onPause();
                }
            }
        }
    }

    @Subscribe
    public void onReload(final SmartDashWidgetManager.Reload reload) {
        if (reload == SmartDashWidgetManager.Reload.RELOADED) {
            this.showViewsAccordingToUserPreferences();
        }
    }

    @Override
    public void onResume() {
        if (this.paused) {
            SmartDashView.sLogger.v("::onResume");
            this.paused = false;
            if (this.mSmartDashWidgetManager != null) {
                SmartDashView.sLogger.v("widgets enabled");
                this.mSmartDashWidgetManager.onResume();
                if (this.tachometerPresenter != null && this.tachometerPresenter.isWidgetActive()) {
                    this.tachometerPresenter.onResume();
                }
                if (this.speedometerGaugePresenter2 != null && this.speedometerGaugePresenter2.isWidgetActive()) {
                    this.speedometerGaugePresenter2.onResume();
                }
            }
            this.recordUsersWidgetPreference();
        }
    }

    @Override
    public void onScrollableSideOptionSelected() {
        switch (this.scrollableSide) {
            case 0:
                this.scrollableSide = 1;
                this.savePreference("PREFERENCE_SCROLLABLE_SIDE", 1);
                break;
            case 1:
                this.scrollableSide = 0;
                this.savePreference("PREFERENCE_SCROLLABLE_SIDE", 0);
                break;
        }
    }

    @Override
    public void onSpeedoMeterSelected() {
        if (this.middleGauge != 1) {
            this.middleGauge = 1;
            this.savePreference("PREFERENCE_MIDDLE_GAUGE", 1);
            this.showMiddleGaugeAccordingToUserPreferences();
        }
    }

    @Override
    public void onTachoMeterSelected() {
        if (this.middleGauge != 0) {
            this.middleGauge = 0;
            this.savePreference("PREFERENCE_MIDDLE_GAUGE", 0);
            this.showMiddleGaugeAccordingToUserPreferences();
        }
    }

    public void recordUsersWidgetPreference() {
        SmartDashView.sLogger.d("Recording the users Widget Preference");
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        if (!DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile() && (this.lastUserPreferenceRecordedTime <= 0L || elapsedRealtime - this.lastUserPreferenceRecordedTime >= SmartDashView.MINIMUM_INTERVAL_BETWEEN_RECORDING_USER_PREFERENCE)) {
            final SharedPreferences driverPreferences = this.homeScreenView.getDriverPreferences();
            final String string = driverPreferences.getString("PREFERENCE_LEFT_GAUGE", ANALOG_CLOCK_WIDGET_ID);
            final String string2 = driverPreferences.getString("PREFERENCE_RIGHT_GAUGE", COMPASS_WIDGET_ID);
            AnalyticsSupport.recordDashPreference(string, true);
            AnalyticsSupport.recordDashPreference(string2, false);
            this.lastUserPreferenceRecordedTime = elapsedRealtime;
        }
    }

    @Override
    public void setView(final MainView.CustomAnimationMode view) {
        SmartDashView.sLogger.v("setview: " + view);
        super.setView(view);
        switch (view) {
            case EXPAND:
                this.mLeftGaugeViewPager.setVisibility(View.VISIBLE);
                this.mRightGaugeViewPager.setVisibility(View.VISIBLE);
                if (this.homeScreenView.isNavigationActive()) {
                    this.showEta(true);
                }
                ((ViewGroup.MarginLayoutParams)this.mMiddleGaugeView.getLayoutParams()).leftMargin = 0;
                break;
            case SHRINK_LEFT:
                this.mLeftGaugeViewPager.setVisibility(GONE);
                this.mRightGaugeViewPager.setVisibility(GONE);
                this.showEta(false);
                ((ViewGroup.MarginLayoutParams)this.mMiddleGaugeView.getLayoutParams()).leftMargin = this.getResources().getDimensionPixelSize(R.dimen.middle_gauge_shrink_margin);
                break;
        }
    }

    @Override
    public boolean shouldShowTopViews() {
        return false;
    }

    @Override
    public void updateLayoutForMode(final NavigationMode navigationMode, final boolean b) {
        final boolean navigationActive = this.homeScreenView.isNavigationActive();
        SmartDashView.sLogger.v("updateLayoutForMode:" + navigationMode + " forced=" + b + " nav:" + navigationActive);
        if (navigationActive) {
            SmartDashView.sLogger.v("updateLayoutForMode: active");
            ((ViewGroup.MarginLayoutParams)this.mLeftGaugeViewPager.getLayoutParams()).topMargin = this.activeModeGaugeMargin;
            ((ViewGroup.MarginLayoutParams)this.mRightGaugeViewPager.getLayoutParams()).topMargin = this.activeModeGaugeMargin;
            ((ViewGroup.MarginLayoutParams)this.mMiddleGaugeView.getLayoutParams()).topMargin = this.getResources().getDimensionPixelSize(R.dimen.tachometer_tbt_mode_margin_top);
            this.mMiddleGaugeView.setScaleX(0.8f);
            this.mMiddleGaugeView.setScaleY(0.8f);
            this.mEtaLayout.setVisibility(View.VISIBLE);
            this.invalidate();
            this.requestLayout();
        }
        else {
            SmartDashView.sLogger.v("updateLayoutForMode: not active");
            ((ViewGroup.MarginLayoutParams)this.mLeftGaugeViewPager.getLayoutParams()).topMargin = this.fullModeGaugeTopMargin;
            ((ViewGroup.MarginLayoutParams)this.mRightGaugeViewPager.getLayoutParams()).topMargin = this.fullModeGaugeTopMargin;
            ((ViewGroup.MarginLayoutParams)this.mMiddleGaugeView.getLayoutParams()).topMargin = this.getResources().getDimensionPixelSize(R.dimen.tachometer_full_mode_margin_top);
            this.mMiddleGaugeView.setScaleX(1.0f);
            this.mMiddleGaugeView.setScaleY(1.0f);
            this.mEtaLayout.setVisibility(GONE);
            this.invalidate();
            this.requestLayout();
        }
        this.showEta(navigationActive);
        this.invalidate();
        this.requestLayout();
    }

    @Override
    protected void updateSpeed(final int n) {
        super.updateSpeed(n);
        if (this.tachometerPresenter != null) {
            this.tachometerPresenter.setSpeed(n);
        }
        if (this.speedometerGaugePresenter2 != null) {
            this.speedometerGaugePresenter2.setSpeed(n);
        }
    }

    @Override
    protected void updateSpeedLimit(final int n) {
        super.updateSpeedLimit(n);
        if (this.tachometerPresenter != null) {
            this.tachometerPresenter.setSpeedLimit(n);
        }
        if (this.speedometerGaugePresenter2 != null) {
            this.speedometerGaugePresenter2.setSpeedLimit(n);
        }
        this.mSmartDashWidgetManager.updateWidget(SPEED_LIMIT_SIGN_GAUGE_ID, n);
    }

    class GaugeViewPagerAdapter implements GaugeViewPager.Adapter
    {
        private int mExcludedPosition;
        private boolean mLeft;
        private SmartDashWidgetManager mSmartDashWidgetManager;
        private SmartDashWidgetManager.SmartDashWidgetCache mWidgetCache;

        GaugeViewPagerAdapter(final SmartDashWidgetManager mSmartDashWidgetManager, final boolean mLeft) {
            this.mExcludedPosition = -1;
            this.mSmartDashWidgetManager = mSmartDashWidgetManager;
            this.mLeft = mLeft;
            final SmartDashWidgetManager mSmartDashWidgetManager2 = this.mSmartDashWidgetManager;
            int n;
            if (this.mLeft) {
                n = 0;
            }
            else {
                n = 1;
            }
            this.mWidgetCache = mSmartDashWidgetManager2.buildSmartDashWidgetCache(n);
            this.mSmartDashWidgetManager.registerForChanges(this);
        }

        @Override
        public int getCount() {
            return this.mWidgetCache.getWidgetsCount();
        }

        @Override
        public int getExcludedPosition() {
            return this.mExcludedPosition;
        }

        @Override
        public DashboardWidgetPresenter getPresenter(final int n) {
            return this.mWidgetCache.getWidgetPresenter(n);
        }

        @Override
        public View getView(int n, final View view, final ViewGroup viewGroup, final boolean b) {
            final boolean b2 = false;
            DashboardWidgetView dashboardWidgetView;
            if (view == null) {
                dashboardWidgetView = new DashboardWidgetView(viewGroup.getContext());
                dashboardWidgetView.setLayoutParams(new ViewGroup.LayoutParams(viewGroup.getResources().getDimensionPixelSize(R.dimen.gauge_frame_width), viewGroup.getResources().getDimensionPixelSize(R.dimen.gauge_frame_height)));
            }
            else {
                dashboardWidgetView = (DashboardWidgetView)view;
            }
            final DashboardWidgetPresenter dashboardWidgetPresenter = (DashboardWidgetPresenter)dashboardWidgetView.getTag();
            final DashboardWidgetPresenter widgetPresenter = this.mWidgetCache.getWidgetPresenter(n);
            if (dashboardWidgetPresenter != null && dashboardWidgetPresenter != widgetPresenter && dashboardWidgetPresenter.getWidgetView() == dashboardWidgetView) {
                dashboardWidgetPresenter.setView(null, null);
            }
            if (widgetPresenter != null) {
                final Bundle bundle = new Bundle();
                if (this.mLeft) {
                    n = 0;
                }
                else {
                    n = 2;
                }
                bundle.putInt("EXTRA_GRAVITY", n);
                bundle.putBoolean("EXTRA_IS_ACTIVE", b);
                if (!b) {
                    widgetPresenter.setWidgetVisibleToUser(false);
                }
                else {
                    if (this.mLeft) {
                        SmartDashView.this.activeleftGaugeId = widgetPresenter.getWidgetIdentifier();
                    }
                    else {
                        SmartDashView.this.activeRightGaugeId = widgetPresenter.getWidgetIdentifier();
                    }
                    final SmartDashView this$0 = SmartDashView.this;
                    boolean isShowingDriveScoreGauge;
                    Label_0231: {
                        if (!DRIVE_SCORE_GAUGE_ID.equals(SmartDashView.this.activeleftGaugeId)) {
                            isShowingDriveScoreGauge = b2;
                            if (!DRIVE_SCORE_GAUGE_ID.equals(SmartDashView.this.activeRightGaugeId)) {
                                break Label_0231;
                            }
                        }
                        isShowingDriveScoreGauge = true;
                    }
                    this$0.isShowingDriveScoreGauge = isShowingDriveScoreGauge;
                    widgetPresenter.setWidgetVisibleToUser(true);
                }
                widgetPresenter.setView(dashboardWidgetView, bundle);
            }
            dashboardWidgetView.setTag(widgetPresenter);
            return dashboardWidgetView;
        }

        @Subscribe
        public void onWidgetsReload(final SmartDashWidgetManager.Reload reload) {
            if (reload == SmartDashWidgetManager.Reload.RELOAD_CACHE) {
                final SmartDashWidgetManager mSmartDashWidgetManager = this.mSmartDashWidgetManager;
                int n;
                if (this.mLeft) {
                    n = 0;
                }
                else {
                    n = 1;
                }
                this.mWidgetCache = mSmartDashWidgetManager.buildSmartDashWidgetCache(n);
            }
        }

        public void setExcludedPosition(final int mExcludedPosition) {
            if (this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(EMPTY_WIDGET_ID) == mExcludedPosition) {
                this.mExcludedPosition = -1;
            }
            else {
                this.mExcludedPosition = mExcludedPosition;
            }
        }
    }
}
