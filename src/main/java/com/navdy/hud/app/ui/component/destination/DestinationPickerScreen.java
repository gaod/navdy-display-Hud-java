package com.navdy.hud.app.ui.component.destination;

import android.animation.Animator;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.routing.Route;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.destinations.Destination;
import com.navdy.hud.app.framework.destinations.Destination$Builder;
import com.navdy.hud.app.framework.destinations.Destination$DestinationType;
import com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType;
import com.navdy.hud.app.framework.destinations.DestinationsManager;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents$NavigationModeChange;
import com.navdy.hud.app.maps.MapsNotification;
import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.maps.here.HereRouteCache;
import com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.ui.component.ChoiceLayout.IListener;
import com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.hud.app.ui.component.homescreen.NavigationView;
import com.navdy.hud.app.ui.component.image.InitialsImageView.Style;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.TitleViewHolder;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.places.PlaceType;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import flow.Flow.Direction;
import flow.Layout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Singleton;

@Layout(R.layout.screen_destination_picker)
public class DestinationPickerScreen extends BaseScreen {
    private static final int CANCEL_POSITION = 1;
    private static List<String> CONFIRMATION_CHOICES = new ArrayList();
    private static final float MIN_DISTANCE_EXPANSION = 3000.0f;
    private static final float MIN_DISTANCE_THRESHOLD = 5000.0f;
    private static final int NAVIGATE_POSITION = 0;
    public static final String PICKER_DESTINATIONS = "PICKER_DESTINATIONS";
    public static final String PICKER_DESTINATION_ICON = "PICKER_DESTINATION_ICON";
    public static final String PICKER_HIDE_IF_NAV_STOPS = "PICKER_HIDE";
    public static final String PICKER_INITIAL_SELECTION = "PICKER_INITIAL_SELECTION";
    public static final String PICKER_LEFT_ICON = "PICKER_LEFT_ICON";
    public static final String PICKER_LEFT_ICON_BKCOLOR = "PICKER_LEFT_ICON_BKCOLOR";
    public static final String PICKER_LEFT_TITLE = "PICKER_LEFT_TITLE";
    public static final String PICKER_MAP_END_LAT = "PICKER_MAP_END_LAT";
    public static final String PICKER_MAP_END_LNG = "PICKER_MAP_END_LNG";
    public static final String PICKER_MAP_START_LAT = "PICKER_MAP_START_LAT";
    public static final String PICKER_MAP_START_LNG = "PICKER_MAP_START_LNG";
    public static final String PICKER_SHOW_DESTINATION_MAP = "PICKER_SHOW_DESTINATION_MAP";
    public static final String PICKER_SHOW_ROUTE_MAP = "PICKER_SHOW_ROUTE_MAP";
    public static final String PICKER_TITLE = "PICKER_TITLE";
    private static final int SELECTION_ANIMATION_DELAY = 1000;
    private static DestinationPickerScreen instance;
    private static final Map<PlaceType, PlaceTypeResourceHolder> placeResMapping = new HashMap();
    private static final Logger sLogger = new Logger(DestinationPickerScreen.class);

    public static class DestinationPickerState {
        public boolean doNotAddOriginalRoute;
    }

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {DestinationPickerView.class})
    public class Module {
    }

    public static class PlaceTypeResourceHolder {
        public final int colorRes;
        public final int destinationIcon;
        public final int iconRes;
        public final int titleRes;

        private PlaceTypeResourceHolder(int iconRes, int titleRes, int colorRes, int destinationIcon) {
            this.iconRes = iconRes;
            this.titleRes = titleRes;
            this.colorRes = colorRes;
            this.destinationIcon = destinationIcon;
        }
    }

    @Singleton
    public static class Presenter extends BasePresenter<DestinationPickerView> {
        @Inject
        Bus bus;
        private boolean closed;
        private List<Model> data;
        private List<GeoBoundingBox> destinationGeoBBoxList;
        private List<GeoCoordinate> destinationGeoList;
        private int destinationIcon;
        DestinationParcelable[] destinations;
        boolean doNotAddOriginalRoute;
        private GeoCoordinate end;
        private boolean handledSelection;
        private boolean hideScreenOnNavStop;
        private HomeScreenView homeScreenView;
        int initialSelection;
        int itemSelection;
        private int leftBkColor;
        private int leftIcon;
        private String leftTitle;
        private NavigationView navigationView;
        private IDestinationPicker pickerCallback;
        private boolean registered;
        private boolean showDestinationMap;
        private boolean showRouteMap;
        private GeoCoordinate start;
        private DisplayMode switchBackMode;
        private String title;

        public void onLoad(Bundle savedInstanceState) {
            DestinationPickerScreen.sLogger.v("onLoad");
            NotificationManager.getInstance().enableNotifications(false);
            NotificationManager.getInstance().enablePhoneNotifications(true);
            clearState();
            reset();
            this.bus.register(this);
            this.registered = true;
            UIStateManager uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
            this.navigationView = uiStateManager.getNavigationView();
            this.homeScreenView = uiStateManager.getHomescreenView();
            if (savedInstanceState != null) {
                this.leftTitle = savedInstanceState.getString(DestinationPickerScreen.PICKER_LEFT_TITLE);
                this.leftIcon = savedInstanceState.getInt(DestinationPickerScreen.PICKER_LEFT_ICON);
                this.leftBkColor = savedInstanceState.getInt(DestinationPickerScreen.PICKER_LEFT_ICON_BKCOLOR);
                this.initialSelection = savedInstanceState.getInt(DestinationPickerScreen.PICKER_INITIAL_SELECTION, 0);
                this.title = savedInstanceState.getString(DestinationPickerScreen.PICKER_TITLE);
                this.destinations = (DestinationParcelable[]) savedInstanceState.getParcelableArray(DestinationPickerScreen.PICKER_DESTINATIONS);
                this.hideScreenOnNavStop = savedInstanceState.getBoolean(DestinationPickerScreen.PICKER_HIDE_IF_NAV_STOPS);
                this.showRouteMap = savedInstanceState.getBoolean(DestinationPickerScreen.PICKER_SHOW_ROUTE_MAP);
                if (this.showRouteMap) {
                    DestinationPickerScreen.sLogger.v("show route map");
                    double lat = savedInstanceState.getDouble(DestinationPickerScreen.PICKER_MAP_START_LAT);
                    double lng = savedInstanceState.getDouble(DestinationPickerScreen.PICKER_MAP_START_LNG);
                    if (lat == 0.0d || lng == 0.0d) {
                        this.start = null;
                    } else {
                        this.start = new GeoCoordinate(lat, lng);
                    }
                    lat = savedInstanceState.getDouble(DestinationPickerScreen.PICKER_MAP_END_LAT);
                    lng = savedInstanceState.getDouble(DestinationPickerScreen.PICKER_MAP_END_LNG);
                    if (lat == 0.0d || lng == 0.0d) {
                        this.end = null;
                    } else {
                        this.end = new GeoCoordinate(lat, lng);
                    }
                } else {
                    this.showDestinationMap = savedInstanceState.getBoolean(DestinationPickerScreen.PICKER_SHOW_DESTINATION_MAP);
                    if (this.showDestinationMap) {
                        this.destinationIcon = savedInstanceState.getInt(DestinationPickerScreen.PICKER_DESTINATION_ICON, -1);
                        DestinationPickerScreen.sLogger.v("show dest map");
                        buildDestinationBBox();
                    }
                    this.start = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                    NavigationRouteRequest request = HereNavigationManager.getInstance().getCurrentNavigationRouteRequest();
                    if (request != null) {
                        this.end = new GeoCoordinate(request.destination.latitude.doubleValue(), request.destination.longitude.doubleValue());
                    } else {
                        this.end = null;
                    }
                }
                if (this.showRouteMap || this.showDestinationMap) {
                    this.switchBackMode = this.homeScreenView.getDisplayMode();
                    if (this.switchBackMode == DisplayMode.MAP) {
                        DestinationPickerScreen.sLogger.v("switchbackmode: null");
                    } else {
                        DestinationPickerScreen.sLogger.v(" onLoad switchbackmode: " + this.switchBackMode);
                        this.homeScreenView.setDisplayMode(DisplayMode.MAP);
                    }
                }
            }
            if (DestinationPickerScreen.instance != null) {
                this.pickerCallback = (IDestinationPicker) DestinationPickerScreen.instance.arguments2;
                DestinationPickerScreen.instance = null;
            } else {
                this.pickerCallback = null;
            }
            updateView();
            super.onLoad(savedInstanceState);
        }

        private void updateView() {
            DestinationPickerView view = getView();
            if (view != null) {
                Resources resources = view.getResources();
                if (this.showRouteMap || this.showDestinationMap) {
                    view.vmenuComponent.leftContainer.removeAllViews();
                    view.vmenuComponent.leftContainer.addView(LayoutInflater.from(view.getContext()).inflate(R.layout.screen_destination_picker_content, view, false));
                    view.vmenuComponent.setLeftContainerWidth(resources.getDimensionPixelSize(R.dimen.dest_picker_map_left_container_w));
                    view.vmenuComponent.setRightContainerWidth(resources.getDimensionPixelSize(R.dimen.dest_picker_map_right_container_w));
                    view.vmenuComponent.rightContainer.setBackgroundColor(-16777216);
                    view.vmenuComponent.closeContainer.setBackgroundColor(-16777216);
                    view.setBackgroundColor(-16777216);
                    view.rightBackground.setVisibility(View.VISIBLE);
                    if (this.showRouteMap) {
                        GeoBoundingBox boundingBox;
                        GeoBoundingBox boundingBox2;
                        String routeId = this.destinations[0].routeId;
                        Route route = null;
                        if (routeId != null) {
                            HereRouteCache$RouteInfo routeInfo = HereRouteCache.getInstance().getRoute(routeId);
                            if (routeInfo != null) {
                                route = routeInfo.route;
                                boundingBox = routeInfo.route.getBoundingBox();
                                if (boundingBox == null && this.start != null) {
                                    boundingBox2 = new GeoBoundingBox(this.start, DestinationPickerScreen.MIN_DISTANCE_THRESHOLD, DestinationPickerScreen.MIN_DISTANCE_THRESHOLD);
                                    this.navigationView.switchToRouteSearchMode(this.start, this.end, boundingBox2, route, false, null, -1);
                                }
                                boundingBox2 = boundingBox;
                                this.navigationView.switchToRouteSearchMode(this.start, this.end, boundingBox2, route, false, null, -1);
                            }
                        }
                        boundingBox = null;
                        try {
                            boundingBox2 = new GeoBoundingBox(this.start, DestinationPickerScreen.MIN_DISTANCE_THRESHOLD, DestinationPickerScreen.MIN_DISTANCE_THRESHOLD);
                            this.navigationView.switchToRouteSearchMode(this.start, this.end, boundingBox2, route, false, null, -1);
                        } catch (Throwable t) {
                            DestinationPickerScreen.sLogger.e(t);
                        }
                    } else {
                        this.navigationView.switchToRouteSearchMode(this.start, this.end, (GeoBoundingBox) this.destinationGeoBBoxList.get(0), HereNavigationManager.getInstance().getCurrentRoute(), false, this.destinationGeoList, this.destinationIcon);
                    }
                } else {
                    view.vmenuComponent.setLeftContainerWidth(resources.getDimensionPixelSize(R.dimen.dest_picker_left_container_w));
                    view.vmenuComponent.setRightContainerWidth(resources.getDimensionPixelSize(R.dimen.dest_picker_right_container_w));
                }
                if (this.leftTitle != null) {
                    view.vmenuComponent.selectedText.setText(this.leftTitle);
                }
                if (this.leftIcon != 0) {
                    if (this.leftBkColor != 0) {
                        view.vmenuComponent.setSelectedIconColorImage(this.leftIcon, this.leftBkColor, null, 1.0f);
                    } else {
                        view.vmenuComponent.setSelectedIconImage(this.leftIcon, null, Style.DEFAULT);
                    }
                }
                if (this.destinations == null || this.destinations.length == 0) {
                    DestinationPickerScreen.sLogger.w("no destinations");
                    this.bus.post(new Builder().screen(Screen.SCREEN_BACK).build());
                    return;
                }
                DestinationPickerScreen.sLogger.v("title:" + this.title);
                DestinationPickerScreen.sLogger.v("destinations:" + this.destinations.length);
                List<Model> list = new ArrayList();
                list.add(TitleViewHolder.buildModel(this.title));
                for (int i = 0; i < this.destinations.length; i++) {
                    Model destinationModel;
                    if (this.destinations[i].iconUnselected == 0) {
                        destinationModel = IconBkColorViewHolder.buildModel(this.destinations[i].id, this.destinations[i].icon, this.destinations[i].iconSelectedColor, this.destinations[i].iconDeselectedColor, this.destinations[i].iconSelectedColor, this.destinations[i].destinationTitle, this.destinations[i].destinationSubTitle, this.destinations[i].destinationSubTitle2);
                    } else {
                        destinationModel = IconsTwoViewHolder.buildModel(this.destinations[i].id, this.destinations[i].icon, this.destinations[i].iconUnselected, this.destinations[i].iconSelectedColor, this.destinations[i].iconDeselectedColor, this.destinations[i].destinationTitle, this.destinations[i].destinationSubTitle, this.destinations[i].destinationSubTitle2);
                    }
                    destinationModel.subTitleFormatted = this.destinations[i].destinationSubTitleFormatted;
                    destinationModel.subTitle2Formatted = this.destinations[i].destinationSubTitle2Formatted;
                    destinationModel.state = this.destinations[i];
                    list.add(destinationModel);
                }
                this.data = list;
                view.vmenuComponent.updateView(this.data, this.initialSelection, false);
            }
        }

        public void onUnload() {
            boolean addRoute;
            if (this.doNotAddOriginalRoute) {
                addRoute = false;
            } else {
                addRoute = true;
            }
            if (!this.showRouteMap) {
                addRoute = true;
            }
            DestinationPickerScreen.sLogger.v("onUnload addbackRoute:" + addRoute);
            DestinationPickerScreen.instance = null;
            clearState();
            this.navigationView.switchBackfromRouteSearchMode(addRoute);
            if (this.switchBackMode != null) {
                DestinationPickerScreen.sLogger.v("onUnload switchbackmode: " + this.switchBackMode);
                this.homeScreenView.setDisplayMode(this.switchBackMode);
            }
            NotificationManager.getInstance().enableNotifications(true);
            this.pickerCallback = null;
            if (this.registered) {
                this.registered = false;
                this.bus.unregister(this);
            }
            reset();
            super.onUnload();
        }

        void clearState() {
            this.initialSelection = 0;
            this.title = null;
            this.destinations = null;
            this.showRouteMap = false;
            this.showDestinationMap = false;
            this.destinationIcon = -1;
            this.switchBackMode = null;
            this.destinationGeoList = null;
            this.destinationGeoBBoxList = null;
            this.itemSelection = -1;
            this.start = null;
            this.end = null;
            this.data = null;
            this.pickerCallback = null;
            this.doNotAddOriginalRoute = false;
        }

        void reset() {
            this.closed = false;
            this.handledSelection = false;
            this.data = null;
            this.hideScreenOnNavStop = false;
        }

        void clearSelection() {
            this.closed = false;
            this.handledSelection = false;
        }

        boolean itemClicked(ItemSelectionState selection) {
            DestinationPickerScreen.sLogger.v("itemClicked:" + selection.id + HereManeuverDisplayBuilder.COMMA + selection.pos);
            if (this.handledSelection) {
                DestinationPickerScreen.sLogger.v("already handled [" + selection.id + "], " + selection.pos);
                return true;
            }
            this.handledSelection = true;
            DestinationPickerView view = (DestinationPickerView) getView();
            if (view == null) {
                return true;
            }
            if (this.showDestinationMap && !isNotADestination(selection.pos)) {
                HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
                if (hereNavigationManager.isNavigationModeOn() && !hereNavigationManager.hasArrived()) {
                    showConfirmation(view, selection);
                    return true;
                }
            }
            performAction(view, selection.id, selection.pos);
            return this.handledSelection;
        }

        boolean isShowingRouteMap() {
            return this.showRouteMap;
        }

        boolean isAlive() {
            return this.registered;
        }

        boolean isShowDestinationMap() {
            return this.showDestinationMap;
        }

        void startMapFluctuator() {
            if (this.registered && this.navigationView != null) {
                this.navigationView.cleanupFluctuator();
                this.navigationView.startFluctuator();
            }
        }

        void itemSelected(ItemSelectionState selection) {
            if (this.data != null) {
                DestinationPickerScreen.sLogger.v("itemSelected:" + selection.id + " , " + selection.pos);
                if (this.pickerCallback != null) {
                    if (this.pickerCallback.onItemSelected(selection.id, selection.pos - 1, new DestinationPickerState())) {
                        DestinationPickerScreen.sLogger.v("overridden onItemSelected : " + selection.id + HereManeuverDisplayBuilder.COMMA + selection.pos);
                        return;
                    }
                }
                if (isNotADestination(selection.pos)) {
                    this.navigationView.changeMarkerSelection(-1, this.itemSelection);
                    this.itemSelection = -1;
                    return;
                }
                itemSelected(selection.pos - 1);
            }
        }

        void itemSelected(int pos) {
            AnalyticsSupport.recordNearbySearchDestinationScroll(pos);
            if (this.showRouteMap) {
                String routeId = this.destinations[pos].routeId;
                HereRouteCache$RouteInfo routeInfo = HereRouteCache.getInstance().getRoute(routeId);
                if (routeInfo != null) {
                    this.navigationView.zoomToBoundBox(routeInfo.route.getBoundingBox(), routeInfo.route, true, true);
                    return;
                }
                DestinationPickerScreen.sLogger.v("itemSelected: route not found:" + routeId);
            } else if (this.showDestinationMap) {
                this.navigationView.changeMarkerSelection(pos, this.itemSelection);
                GeoBoundingBox boundingBox = (GeoBoundingBox) this.destinationGeoBBoxList.get(pos);
                if (boundingBox != null) {
                    this.navigationView.zoomToBoundBox(boundingBox, null, false, true);
                }
                this.itemSelection = pos;
            }
        }

        private boolean isNotADestination(final int n) {
            final DestinationParcelable destinationParcelable = (DestinationParcelable)this.data.get(n).state;
            return destinationParcelable != null && destinationParcelable.type != DestinationParcelable.DestinationType.DESTINATION;
        }


        void close() {
            close(null);
        }

        void close(final Runnable endAction) {
            if (this.closed) {
                DestinationPickerScreen.sLogger.v("already closed");
                return;
            }
            DestinationPickerView view;
            if (this.showRouteMap || this.showDestinationMap) {
                view = (DestinationPickerView) getView();
                if (view != null) {
                    view.setBackgroundColor(-16777216);
                }
                if (this.navigationView != null) {
                    this.navigationView.setMapMaskVisibility(0);
                }
            }
            NotificationManager.getInstance().enableNotifications(true);
            this.closed = true;
            DestinationPickerScreen.sLogger.v("close");
            view = (DestinationPickerView) getView();
            if (view != null) {
                view.vmenuComponent.animateOut(new DefaultAnimationListener() {
                    public void onAnimationEnd(Animator animation) {
                        DestinationPickerScreen.sLogger.v("post back");
                        Presenter.this.bus.post(new Builder().screen(Screen.SCREEN_BACK).build());
                        if (endAction != null) {
                            endAction.run();
                        }
                        if (Presenter.this.pickerCallback != null) {
                            Presenter.this.pickerCallback.onDestinationPickerClosed();
                        }
                    }
                });
            }
        }

        boolean isClosed() {
            return this.closed;
        }

        private void launchDestination(DestinationPickerView view, final DestinationParcelable dest) {
            DestinationPickerScreen.sLogger.v("launchDestination {" + dest.destinationTitle + "} display {" + dest.displayLatitude + HereManeuverDisplayBuilder.COMMA + dest.displayLongitude + "}" + " nav {" + dest.navLatitude + HereManeuverDisplayBuilder.COMMA + dest.navLongitude + "}");
            if (HereMapsManager.getInstance().isInitialized()) {
                close(new Runnable() {
                    public void run() {
                        Destination d = new Destination$Builder().destinationTitle(dest.destinationTitle).destinationSubtitle(dest.destinationSubTitle).fullAddress(dest.destinationAddress).displayPositionLatitude(dest.displayLatitude).displayPositionLongitude(dest.displayLongitude).navigationPositionLatitude(dest.navLatitude).navigationPositionLongitude(dest.navLongitude).destinationType(Destination$DestinationType.DEFAULT).identifier(dest.identifier).favoriteDestinationType(Destination$FavoriteDestinationType.FAVORITE_NONE).destinationIcon(dest.icon).destinationIconBkColor(dest.iconSelectedColor).destinationPlaceId(dest.destinationPlaceId).placeType(dest.placeType).distanceStr(dest.distanceStr).contacts(dest.contacts).phoneNumbers(dest.phoneNumbers).build();
                        DestinationPickerScreen.sLogger.v("call requestNavigation");
                        DestinationsManager.getInstance().requestNavigationWithNavLookup(d);
                    }
                });
                return;
            }
            DestinationPickerScreen.sLogger.w("Here maps engine not initialized, exit");
            close(new Runnable() {
                public void run() {
                    MapsNotification.showMapsEngineNotInitializedToast();
                }
            });
        }

        private void buildDestinationBBox() {
            ArrayList<GeoCoordinate> list = new ArrayList();
            GeoCoordinate current = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
            DestinationPickerScreen.sLogger.v("destinationbbox-ce:" + current);
            this.destinationGeoList = new ArrayList();
            this.destinationGeoBBoxList = new ArrayList();
            int i = 0;
            while (i < this.destinations.length) {
                if (this.destinations[i].displayLatitude == 0.0d && this.destinations[i].displayLongitude == 0.0d) {
                    this.destinationGeoList.add(null);
                    this.destinationGeoBBoxList.add(null);
                } else {
                    list.clear();
                    if (current != null) {
                        list.add(current);
                    }
                    GeoCoordinate geoCoordinate = new GeoCoordinate(this.destinations[i].displayLatitude, this.destinations[i].displayLongitude);
                    list.add(geoCoordinate);
                    GeoBoundingBox bbox = GeoBoundingBox.getBoundingBoxContainingGeoCoordinates(list);
                    if (current != null && ((float) ((long) current.distanceTo(geoCoordinate))) <= DestinationPickerScreen.MIN_DISTANCE_THRESHOLD) {
                        try {
                            bbox.expand(DestinationPickerScreen.MIN_DISTANCE_EXPANSION, DestinationPickerScreen.MIN_DISTANCE_EXPANSION);
                        } catch (Throwable t) {
                            DestinationPickerScreen.sLogger.e("expand bbox", t);
                        }
                    }
                    this.destinationGeoList.add(geoCoordinate);
                    this.destinationGeoBBoxList.add(bbox);
                }
                i++;
            }
        }

        @Subscribe
        public void onNavigationModeChanged(MapEvents$NavigationModeChange event) {
            if (this.hideScreenOnNavStop && ((DestinationPickerView) getView()) != null) {
                DestinationPickerScreen.sLogger.v("onNavigationModeChanged event[" + event.navigationMode + "]");
                if (event.navigationMode == NavigationMode.MAP) {
                    close();
                }
            }
        }

        private void showConfirmation(DestinationPickerView view, final ItemSelectionState selection) {
            DestinationConfirmationHelper.configure(view.confirmationLayout, (DestinationParcelable)(this.data.get(selection.pos)).state);

            view.confirmationLayout.setChoices(DestinationPickerScreen.CONFIRMATION_CHOICES, 0, new IListener() {
                public void executeItem(int pos, int id) {
                    DestinationPickerView view1 = (DestinationPickerView) Presenter.this.getView();
                    if (view1 == null) {
                        DestinationPickerScreen.sLogger.v("not alive");
                        return;
                    }
                    switch (pos) {
                        case 0:
                            DestinationPickerScreen.sLogger.v("called presenter select");
                            DestinationPickerView v = (DestinationPickerView) Presenter.this.getView();
                            if (v != null) {
                                Presenter.this.performAction(v, selection.id, selection.pos);
                                return;
                            }
                            return;
                        case 1:
                            view1.confirmationLayout.setVisibility(View.GONE);
                            Presenter.this.clearSelection();
                            view1.vmenuComponent.verticalList.unlock();
                            return;
                        default:
                            return;
                    }
                }

                public void itemSelected(int pos, int id) {
                }
            });
            view.confirmationLayout.setVisibility(View.VISIBLE);
        }

        private void performAction(DestinationPickerView view, final int id, final int pos) {
            DestinationPickerScreen.sLogger.v("performAction {" + id + HereManeuverDisplayBuilder.COMMA + pos + "}");
            NotificationManager.getInstance().enableNotifications(true);
            if (this.showRouteMap || this.showDestinationMap) {
                view.setBackgroundColor(-16777216);
                if (this.navigationView != null) {
                    this.navigationView.setMapMaskVisibility(0);
                }
            }
            view.vmenuComponent.performSelectionAnimation(new Runnable() {
                public void run() {
                    DestinationPickerView view = (DestinationPickerView) Presenter.this.getView();
                    if (view != null) {
                        if (Presenter.this.data == null || pos >= Presenter.this.data.size()) {
                            DestinationPickerScreen.sLogger.w("invalid list,no selection");
                            Presenter.this.handledSelection = false;
                            return;
                        }
                        boolean override = false;
                        if (Presenter.this.pickerCallback != null) {
                            DestinationPickerState state = new DestinationPickerState();
                            DestinationParcelable destinationParcelable = (DestinationParcelable)(Presenter.this.data.get(pos)).state;
                            int selectionId = (destinationParcelable == null || destinationParcelable.id == 0) ? id : destinationParcelable.id;
                            override = Presenter.this.pickerCallback.onItemClicked(selectionId, pos - 1, state);
                            if (Presenter.this.showRouteMap) {
                                DestinationPickerScreen.sLogger.v("map-route- doNotAddOriginalRoute=" + state.doNotAddOriginalRoute);
                                Presenter.this.doNotAddOriginalRoute = state.doNotAddOriginalRoute;
                            }
                        }
                        if (override || Presenter.this.isNotADestination(pos)) {
                            DestinationPickerScreen.sLogger.v("overridden");
                            Presenter.this.close();
                        } else {
                            Model model = (Model) Presenter.this.data.get(pos);
                            DestinationPickerScreen.sLogger.v("not overriden, launching pos:" + pos + " title=" + model.title);
                            Presenter.this.launchDestination(view, (DestinationParcelable) model.state);
                        }
                        Presenter.this.handledSelection = true;
                    }
                }
            }, 1000);
        }
    }

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        CONFIRMATION_CHOICES.add(resources.getString(R.string.destination_start_navigation_yes));
        CONFIRMATION_CHOICES.add(resources.getString(R.string.destination_start_navigation_cancel));
        placeResMapping.put(PlaceType.PLACE_TYPE_GAS, new PlaceTypeResourceHolder(R.drawable.icon_place_gas, R.string.gas, R.color.mm_search_gas, R.drawable.icon_pin_dot_destination_indigo));
        placeResMapping.put(PlaceType.PLACE_TYPE_PARKING, new PlaceTypeResourceHolder(R.drawable.icon_place_parking, R.string.parking, R.color.mm_search_parking, R.drawable.icon_pin_dot_destination_indigo));
        placeResMapping.put(PlaceType.PLACE_TYPE_RESTAURANT, new PlaceTypeResourceHolder(R.drawable.icon_place_restaurant, R.string.food, R.color.mm_search_food, R.drawable.icon_pin_dot_destination_orange));
        placeResMapping.put(PlaceType.PLACE_TYPE_STORE, new PlaceTypeResourceHolder(R.drawable.icon_place_store, R.string.grocery_store, R.color.mm_search_grocery_store, R.drawable.icon_pin_dot_destination_blue));
        placeResMapping.put(PlaceType.PLACE_TYPE_COFFEE, new PlaceTypeResourceHolder(R.drawable.icon_place_coffee, R.string.coffee, R.color.mm_search_coffee, R.drawable.icon_pin_dot_destination_orange));
        placeResMapping.put(PlaceType.PLACE_TYPE_ATM, new PlaceTypeResourceHolder(R.drawable.icon_place_a_t_m, R.string.atm, R.color.mm_search_atm, R.drawable.icon_pin_dot_destination_blue));
        placeResMapping.put(PlaceType.PLACE_TYPE_HOSPITAL, new PlaceTypeResourceHolder(R.drawable.icon_place_hospital, R.string.hospital, R.color.mm_search_hospital, R.drawable.icon_pin_dot_destination_red));
    }

    public DestinationPickerScreen() {
        instance = this;
    }

    public String getMortarScopeName() {
        return getClass().getName();
    }

    public Object getDaggerModule() {
        return new Module();
    }

    public Screen getScreen() {
        return Screen.SCREEN_DESTINATION_PICKER;
    }

    public int getAnimationIn(Direction direction) {
        return -1;
    }

    public int getAnimationOut(Direction direction) {
        return -1;
    }

    public static PlaceTypeResourceHolder getPlaceTypeHolder(PlaceType placeType) {
        return (PlaceTypeResourceHolder) placeResMapping.get(placeType);
    }
}
