package com.navdy.hud.app.ui.component.homescreen;

import com.navdy.hud.app.R;
import com.navdy.hud.app.maps.MapEvents$RerouteEvent;
import com.navdy.hud.app.maps.MapEvents$RouteEventType;
import com.navdy.hud.app.view.MainView;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.view.View;
import butterknife.ButterKnife;
import android.util.AttributeSet;
import android.content.Context;
import com.navdy.hud.app.ui.component.image.ColorImageView;
import butterknife.InjectView;
import com.navdy.hud.app.ui.component.FluctuatorAnimatorView;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.maps.MapEvents;
import com.squareup.otto.Bus;
import android.widget.FrameLayout;

public class RecalculatingView extends FrameLayout implements IHomeScreenLifecycle
{
    private Bus bus;
    private HomeScreenView homeScreenView;
    private MapEvents$RerouteEvent lastRerouteEvent;
    private Logger logger;
    private boolean paused;
    @InjectView(R.id.recalcAnimator)
    FluctuatorAnimatorView recalcAnimator;
    @InjectView(R.id.recalcImageView)
    ColorImageView recalcImageView;
    
    public RecalculatingView(final Context context) {
        this(context, null);
    }
    
    public RecalculatingView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public RecalculatingView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    public void init(final HomeScreenView homeScreenView) {
        this.homeScreenView = homeScreenView;
        this.bus.register(this);
    }
    
    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        this.recalcImageView.setColor(HomeScreenResourceValues.reCalcAnimColor);
        this.bus = RemoteDeviceManager.getInstance().getBus();
    }
    
    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.logger.v("::onPause:recalc");
        }
    }
    
    @Subscribe
    public void onRerouteEvent(final MapEvents$RerouteEvent lastRerouteEvent) {
        this.logger.v("rerouteEvent:" + lastRerouteEvent.routeEventType);
        if (!this.homeScreenView.isNavigationActive() || HereNavigationManager.getInstance().hasArrived()) {
            this.logger.w("rerouteEvent:navigation not active|hasArrived|routeCalcOn");
        }
        else {
            this.lastRerouteEvent = lastRerouteEvent;
            if (lastRerouteEvent.routeEventType == MapEvents$RouteEventType.STARTED) {
                this.homeScreenView.setRecalculating(true);
                if (!this.paused) {
                    this.showRecalculating();
                }
            }
            else {
                this.homeScreenView.setRecalculating(false);
                if (!this.paused || this.getVisibility() == VISIBLE) {
                    this.hideRecalculating();
                    this.homeScreenView.getTbtView().setVisibility(View.VISIBLE);
                }
            }
        }
    }
    
    public void onResume() {
        if (this.paused) {
            this.paused = false;
            this.logger.v("::onResume:recalc");
            final MapEvents$RerouteEvent lastRerouteEvent = this.lastRerouteEvent;
            if (lastRerouteEvent != null) {
                this.lastRerouteEvent = null;
                this.logger.v("::onResume:recalc set last reroute event");
                this.onRerouteEvent(lastRerouteEvent);
            }
        }
    }
    
    public void setView(final MainView.CustomAnimationMode customAnimationMode) {
        switch (customAnimationMode) {
            case EXPAND:
                this.setX((float)HomeScreenResourceValues.recalcX);
                break;
            case SHRINK_LEFT:
                this.setX((float)HomeScreenResourceValues.recalcShrinkLeftX);
                break;
        }
    }
    
    public void showRecalculating() {
        this.homeScreenView.getTbtView().setVisibility(GONE);
        this.setVisibility(View.VISIBLE);
        this.recalcAnimator.start();
    }

    public void hideRecalculating() {
        this.setVisibility(GONE);
        this.recalcAnimator.stop();
        this.lastRerouteEvent = null;
    }
}
