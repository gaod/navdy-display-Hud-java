package com.navdy.hud.app.ui.component.vlist.viewholder;

import com.navdy.hud.app.R;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.animation.AnimatorSet;
import android.view.ViewGroup;
import com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils;
import android.widget.ImageView;
import android.text.Html;
import android.text.TextUtils;
import android.graphics.Color;
import android.view.View;
import android.view.LayoutInflater;
import android.animation.Animator;
import android.os.Handler;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import android.widget.TextView;
import android.view.ViewGroup;
import com.navdy.hud.app.ui.component.HaloView;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import android.animation.AnimatorSet;
import com.navdy.service.library.log.Logger;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;

public abstract class IconBaseViewHolder extends VerticalViewHolder
{
    public static final int FLUCTUATOR_OPACITY_ALPHA = 153;
    public static final int HALO_DELAY_START_DURATION = 100;
    private static final int[] location;
    private static final Logger sLogger;
    protected AnimatorSet.Builder animatorSetBuilder;
    protected CrossFadeImageView crossFadeImageView;
    private Runnable fluctuatorRunnable;
    private DefaultAnimationListener fluctuatorStartListener;
    protected HaloView haloView;
    private boolean hasIconFluctuatorColor;
    private boolean hasSubTitle;
    private boolean hasSubTitle2;
    protected ViewGroup iconContainer;
    protected boolean iconScaleAnimationDisabled;
    protected ViewGroup imageContainer;
    protected TextView subTitle;
    protected TextView subTitle2;
    protected boolean textAnimationDisabled;
    protected TextView title;
    private float titleSelectedTopMargin;
    private float titleUnselectedScale;
    
    static {
        sLogger = VerticalViewHolder.sLogger;
        location = new int[2];
    }
    
    public IconBaseViewHolder(final ViewGroup viewGroup, final VerticalList list, final Handler handler) {
        super(viewGroup, list, handler);
        this.fluctuatorStartListener = new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                IconBaseViewHolder.this.itemAnimatorSet = null;
                IconBaseViewHolder.this.handler.removeCallbacks(IconBaseViewHolder.this.fluctuatorRunnable);
                IconBaseViewHolder.this.handler.postDelayed(IconBaseViewHolder.this.fluctuatorRunnable, 100L);
            }
        };
        this.fluctuatorRunnable = new Runnable() {
            @Override
            public void run() {
                IconBaseViewHolder.this.startFluctuator();
            }
        };
        this.imageContainer = (ViewGroup)viewGroup.findViewById(R.id.imageContainer);
        this.iconContainer = (ViewGroup)viewGroup.findViewById(R.id.iconContainer);
        (this.haloView = (HaloView)viewGroup.findViewById(R.id.halo)).setVisibility(GONE);
        this.crossFadeImageView = (CrossFadeImageView)viewGroup.findViewById(R.id.vlist_image);
        this.title = (TextView)viewGroup.findViewById(R.id.title);
        this.subTitle = (TextView)viewGroup.findViewById(R.id.subTitle);
        this.subTitle2 = (TextView)viewGroup.findViewById(R.id.subTitle2);
    }
    
    static ViewGroup getLayout(ViewGroup viewGroup, final int n, final int n2) {
        final LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        viewGroup = (ViewGroup)from.inflate(n, viewGroup, false);
        final ViewGroup viewGroup2 = (ViewGroup)viewGroup.findViewById(R.id.iconContainer);
        final CrossFadeImageView crossFadeImageView = (CrossFadeImageView)from.inflate(n2, viewGroup2, false);
        crossFadeImageView.inject(CrossFadeImageView.Mode.SMALL);
        crossFadeImageView.setId(R.id.vlist_image);
        viewGroup2.addView((View)crossFadeImageView);
        return viewGroup;
    }
    
    private int getSubTitle2Color() {
        int textColor;
        if (this.extras == null) {
            textColor = 0;
        }
        else {
            textColor = this.getTextColor("SUBTITLE_2_COLOR");
        }
        return textColor;
    }
    
    private int getSubTitleColor() {
        int textColor;
        if (this.extras == null) {
            textColor = 0;
        }
        else {
            textColor = this.getTextColor("SUBTITLE_COLOR");
        }
        return textColor;
    }
    
    private int getTextColor(String s) {
        final boolean b = false;
        int int1;
        if (this.extras == null) {
            int1 = (b ? 1 : 0);
        }
        else {
            s = this.extras.get(s);
            int1 = (b ? 1 : 0);
            if (s != null) {
                try {
                    int1 = Integer.parseInt(s);
                }
                catch (NumberFormatException ex) {
                    int1 = (b ? 1 : 0);
                }
            }
        }
        return int1;
    }
    
    private void setIconFluctuatorColor(int argb) {
        if (argb != 0) {
            argb = Color.argb(153, Color.red(argb), Color.green(argb), Color.blue(argb));
            this.hasIconFluctuatorColor = true;
            this.haloView.setStrokeColor(argb);
        }
        else {
            this.hasIconFluctuatorColor = false;
        }
    }
    
    private void setMultiLineStyles(final TextView textView, final boolean singleLine, final int n) {
        if (n != -1) {
            textView.setTextAppearance(textView.getContext(), n);
        }
        textView.setSingleLine(singleLine);
        if (singleLine) {
            textView.setEllipsize((TextUtils.TruncateAt)null);
        }
        else {
            textView.setMaxLines(2);
            textView.setEllipsize(TextUtils.TruncateAt.END);
        }
    }
    
    private void setSubTitle(final String text, final boolean b) {
        if (text == null) {
            this.subTitle.setText((CharSequence)"");
            this.hasSubTitle = false;
        }
        else {
            final int subTitleColor = this.getSubTitleColor();
            if (subTitleColor == 0) {
                this.subTitle.setTextColor(IconBaseViewHolder.subTitleColor);
            }
            else {
                this.subTitle.setTextColor(subTitleColor);
            }
            if (b) {
                this.subTitle.setText((CharSequence)Html.fromHtml(text));
            }
            else {
                this.subTitle.setText((CharSequence)text);
            }
            this.hasSubTitle = true;
        }
    }
    
    private void setSubTitle2(final String text, final boolean b) {
        if (text == null) {
            this.subTitle2.setText((CharSequence)"");
            this.subTitle2.setVisibility(GONE);
            this.hasSubTitle2 = false;
        }
        else {
            final int subTitle2Color = this.getSubTitle2Color();
            if (subTitle2Color == 0) {
                this.subTitle2.setTextColor(IconBaseViewHolder.subTitle2Color);
            }
            else {
                this.subTitle2.setTextColor(subTitle2Color);
            }
            if (b) {
                this.subTitle2.setText((CharSequence)Html.fromHtml(text));
            }
            else {
                this.subTitle2.setText((CharSequence)text);
            }
            this.subTitle2.setVisibility(View.VISIBLE);
            this.hasSubTitle2 = true;
        }
    }
    
    private void setTitle(final String text) {
        this.title.setText((CharSequence)text);
    }
    
    private void stopFluctuator() {
        if (this.hasIconFluctuatorColor) {
            this.handler.removeCallbacks(this.fluctuatorRunnable);
            this.haloView.setVisibility(GONE);
            this.haloView.stop();
        }
    }
    
    @Override
    public void bind(final VerticalList.Model model, final VerticalList.ModelState modelState) {
        if (modelState.updateTitle) {
            this.setTitle(model.title);
        }
        if (modelState.updateSubTitle) {
            if (model.subTitle != null) {
                this.setSubTitle(model.subTitle, model.subTitleFormatted);
            }
            else {
                this.setSubTitle(null, false);
            }
        }
        if (modelState.updateSubTitle2) {
            if (model.subTitle2 != null) {
                this.setSubTitle2(model.subTitle2, model.subTitle2Formatted);
            }
            else {
                this.setSubTitle2(null, false);
            }
        }
        this.textAnimationDisabled = model.noTextAnimation;
        this.iconScaleAnimationDisabled = model.noImageScaleAnimation;
        this.setIconFluctuatorColor(model.iconFluctuatorColor);
    }
    
    @Override
    public void clearAnimation() {
        this.stopFluctuator();
        this.stopAnimation();
        this.currentState = null;
        this.layout.setVisibility(View.VISIBLE);
        this.layout.setAlpha(1.0f);
    }
    
    @Override
    public void copyAndPosition(final ImageView imageView, final TextView textView, final TextView textView2, final TextView textView3, final boolean b) {
        final View big = this.crossFadeImageView.getBig();
        if (b) {
            VerticalAnimationUtils.copyImage((ImageView)big, imageView);
        }
        imageView.setX((float)IconBaseViewHolder.selectedImageX);
        imageView.setY((float)IconBaseViewHolder.selectedImageY);
        textView.setText(this.title.getText());
        textView.setX((float)IconBaseViewHolder.selectedTextX);
        this.title.getLocationOnScreen(IconBaseViewHolder.location);
        textView.setY((float)(IconBaseViewHolder.location[1] - IconBaseViewHolder.rootTopOffset));
        if (this.hasSubTitle) {
            textView2.setText(this.subTitle.getText());
            textView2.setX((float)IconBaseViewHolder.selectedTextX);
            this.subTitle.getLocationOnScreen(IconBaseViewHolder.location);
            textView2.setY((float)(IconBaseViewHolder.location[1] - IconBaseViewHolder.rootTopOffset));
        }
        else {
            textView2.setText((CharSequence)"");
        }
        if (this.hasSubTitle2) {
            textView3.setText(this.subTitle2.getText());
            textView3.setX((float)IconBaseViewHolder.selectedTextX);
            this.subTitle2.getLocationOnScreen(IconBaseViewHolder.location);
            textView3.setY((float)(IconBaseViewHolder.location[1] - IconBaseViewHolder.rootTopOffset));
        }
        else {
            textView3.setText((CharSequence)"");
        }
    }
    
    @Override
    public void preBind(final VerticalList.Model model, final VerticalList.ModelState modelState) {
        ((ViewGroup.MarginLayoutParams)this.title.getLayoutParams()).topMargin = (int)model.fontInfo.titleFontTopMargin;
        this.title.setTextSize(model.fontInfo.titleFontSize);
        this.setMultiLineStyles(this.title, model.fontInfo.titleSingleLine, -1);
        this.title.setLineSpacing(0.0f, 0.9f);
        this.titleSelectedTopMargin = (int)model.fontInfo.titleFontTopMargin;
        ((ViewGroup.MarginLayoutParams)this.subTitle.getLayoutParams()).topMargin = (int)model.fontInfo.subTitleFontTopMargin;
        final TextView subTitle = this.subTitle;
        final boolean b = !model.subTitle_2Lines;
        int n;
        if (model.subTitle_2Lines) {
            n = R.style.vlist_subtitle_2_line;
        }
        else {
            n = R.style.vlist_subtitle;
        }
        this.setMultiLineStyles(subTitle, b, n);
        this.subTitle.setTextSize(model.fontInfo.subTitleFontSize);
        ((ViewGroup.MarginLayoutParams)this.subTitle2.getLayoutParams()).topMargin = (int)model.fontInfo.subTitle2FontTopMargin;
        this.subTitle2.setTextSize(model.fontInfo.subTitle2FontSize);
        this.titleUnselectedScale = model.fontInfo.titleScale;
        this.crossFadeImageView.setSmallAlpha(-1.0f);
    }
    
    @Override
    public void select(final VerticalList.Model model, final int n, final int n2) {
        if (this.hasIconFluctuatorColor) {
            this.haloView.setVisibility(INVISIBLE);
            this.haloView.stop();
        }
        VerticalAnimationUtils.performClick((View)this.iconContainer, n2, new Runnable() {
            @Override
            public void run() {
                final VerticalList.ItemSelectionState itemSelectionState = IconBaseViewHolder.this.vlist.getItemSelectionState();
                itemSelectionState.set(model, model.id, n, -1, -1);
                IconBaseViewHolder.this.vlist.performSelectAction(itemSelectionState);
            }
        });
    }
    
    @Override
    public void setItemState(State state, AnimationType animation, int duration, boolean startFluctuator) {
        float titleScaleFactor = 0.0f;
        float titleTopMargin = 0.0f;
        float subTitleAlpha = 0.0f;
        float subTitleScaleFactor = 0.0f;
        float subTitle2Alpha = 0.0f;
        float subTitle2ScaleFactor = 0.0f;
        State itemState = state;
        PropertyValuesHolder p1;
        PropertyValuesHolder p2;
        PropertyValuesHolder p3;
        this.animatorSetBuilder = null;
        if (this.textAnimationDisabled) {
            if (animation == AnimationType.MOVE) {
                this.itemAnimatorSet = new AnimatorSet();
                this.animatorSetBuilder = this.itemAnimatorSet.play(ValueAnimator.ofFloat(new float[]{0.0f, 1.0f}));
                return;
            }
            itemState = State.SELECTED;
        }
        switch (itemState) {
            case SELECTED:
                titleScaleFactor = 1.0f;
                titleTopMargin = this.titleSelectedTopMargin;
                subTitleAlpha = 1.0f;
                subTitleScaleFactor = 1.0f;
                subTitle2Alpha = 1.0f;
                subTitle2ScaleFactor = 1.0f;
                break;
            case UNSELECTED:
                titleScaleFactor = this.titleUnselectedScale;
                titleTopMargin = 0.0f;
                subTitleAlpha = 0.0f;
                subTitleScaleFactor = this.titleUnselectedScale;
                subTitle2Alpha = 0.0f;
                subTitle2ScaleFactor = this.titleUnselectedScale;
                break;
        }
        if (!this.hasSubTitle) {
            titleTopMargin = 0.0f;
            subTitleAlpha = 0.0f;
            subTitle2Alpha = 0.0f;
        }
        this.title.setPivotX(0.0f);
        this.title.setPivotY(titleHeight / 2.0f);
        switch (animation) {
            case INIT:
            case NONE:
                this.title.setScaleX(titleScaleFactor);
                this.title.setScaleY(titleScaleFactor);
                ((ViewGroup.MarginLayoutParams) this.title.getLayoutParams()).topMargin = (int) titleTopMargin;
                this.title.requestLayout();
                break;
            case MOVE:
                this.itemAnimatorSet = new AnimatorSet();
                p1 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{titleScaleFactor});
                p2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{titleScaleFactor});
                this.animatorSetBuilder = this.itemAnimatorSet.play(ObjectAnimator.ofPropertyValuesHolder(this.title, new PropertyValuesHolder[]{p1, p2}));
                this.animatorSetBuilder.with(VerticalAnimationUtils.animateMargin(this.title, (int) titleTopMargin));
                break;
        }
        this.subTitle.setPivotX(0.0f);
        this.subTitle.setPivotY(subTitleHeight / 2.0f);
        AnimationType animation2 = animation;
        if (!this.hasSubTitle) {
            animation2 = AnimationType.NONE;
        }
        switch (animation2) {
            case INIT:
            case NONE:
                this.subTitle.setAlpha(subTitleAlpha);
                this.subTitle.setScaleX(subTitleScaleFactor);
                this.subTitle.setScaleY(subTitleScaleFactor);
                break;
            case MOVE:
                p1 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{subTitleScaleFactor});
                p2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{subTitleScaleFactor});
                p3 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{subTitleAlpha});
                this.animatorSetBuilder.with(ObjectAnimator.ofPropertyValuesHolder(this.subTitle, new PropertyValuesHolder[]{p1, p2, p3}));
                break;
        }
        this.subTitle2.setPivotX(0.0f);
        this.subTitle2.setPivotY(subTitleHeight / 2.0f);
        animation2 = animation;
        if (!this.hasSubTitle2) {
            animation2 = AnimationType.NONE;
        }
        switch (animation2) {
            case INIT:
            case NONE:
                this.subTitle2.setAlpha(subTitle2Alpha);
                this.subTitle2.setScaleX(subTitle2ScaleFactor);
                this.subTitle2.setScaleY(subTitle2ScaleFactor);
                break;
            case MOVE:
                p1 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{subTitle2ScaleFactor});
                p2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{subTitle2ScaleFactor});
                p3 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{subTitleAlpha});
                this.animatorSetBuilder.with(ObjectAnimator.ofPropertyValuesHolder(this.subTitle2, new PropertyValuesHolder[]{p1, p2, p3}));
                break;
        }
        if (itemState != State.SELECTED || !startFluctuator) {
            return;
        }
        if (this.itemAnimatorSet != null) {
            this.itemAnimatorSet.addListener(this.fluctuatorStartListener);
        } else {
            startFluctuator();
        }
    }

    @Override
    public void startFluctuator() {
        if (this.hasIconFluctuatorColor) {
            this.haloView.setVisibility(View.VISIBLE);
            this.haloView.start();
        }
    }
}
