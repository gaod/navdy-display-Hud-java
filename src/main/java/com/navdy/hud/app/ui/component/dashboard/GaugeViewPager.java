package com.navdy.hud.app.ui.component.dashboard;

import android.view.LayoutInflater;

import com.navdy.hud.app.R;
import com.navdy.hud.app.view.DashboardWidgetPresenter;
import android.view.View;
import android.view.ViewGroup;
import android.animation.Animator;
import android.animation.Animator;
import android.view.ViewGroup;
import android.view.ViewGroup;
import android.animation.ValueAnimator;
import java.util.LinkedList;
import android.util.AttributeSet;
import android.content.Context;
import android.animation.ValueAnimator;
import java.util.Queue;
import android.widget.LinearLayout;
import com.navdy.service.library.log.Logger;
import android.widget.FrameLayout;

public class GaugeViewPager extends FrameLayout
{
    public static final int FAST_SCROLL_ANIMATION_DURATION = 100;
    public static final int REGULAR_SCROLL_ANIMATION_DURATION = 200;
    private static final Logger sLogger;
    private Operation currentOperation;
    private volatile boolean isAnimationRunning;
    private Adapter mAdapter;
    private ChangeListener mChangeListener;
    private int mCurrentPosition;
    private int mNextPosition;
    private int mPreviousPosition;
    private LinearLayout mSlider;
    private Queue<Operation> operationQueue;
    final ValueAnimator positionAnimator;
    
    static {
        sLogger = new Logger(GaugeViewPager.class);
    }
    
    public GaugeViewPager(final Context context) {
        this(context, null);
    }
    
    public GaugeViewPager(final Context context, final AttributeSet set) {
        this(context, set, -1);
    }
    
    public GaugeViewPager(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mCurrentPosition = -1;
        this.mNextPosition = -1;
        this.mPreviousPosition = -1;
        this.operationQueue = new LinkedList<>();
        this.isAnimationRunning = false;
        (this.positionAnimator = new ValueAnimator()).addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                final int intValue = (int)valueAnimator.getAnimatedValue();
                final MarginLayoutParams layoutParams = (MarginLayoutParams)GaugeViewPager.this.mSlider.getLayoutParams();
                layoutParams.topMargin = intValue;
                GaugeViewPager.this.mSlider.setLayoutParams(layoutParams);
            }
        });
        this.positionAnimator.addListener(new Animator.AnimatorListener() {
            public void onAnimationCancel(final Animator animator) {
            }
            
            public void onAnimationEnd(final Animator animator) {
                GaugeViewPager.this.getHandler().post(new Runnable() {
                    @Override
                    public void run() {
                        final GaugeViewPager this$0 = GaugeViewPager.this;
                        int selectedPosition;
                        if (GaugeViewPager.this.currentOperation == Operation.NEXT) {
                            selectedPosition = GaugeViewPager.this.mNextPosition;
                        }
                        else {
                            selectedPosition = GaugeViewPager.this.mPreviousPosition;
                        }
                        this$0.setSelectedPosition(selectedPosition);
                        Operation operation;
                        if (GaugeViewPager.this.operationQueue.size() > 0) {
                            operation = GaugeViewPager.this.operationQueue.remove();
                        }
                        else {
                            operation = null;
                        }
                        if (operation != null) {
                            GaugeViewPager.this.performOperation(operation, GaugeViewPager.this.operationQueue.size() > 0);
                        }
                        else {
                            GaugeViewPager.this.isAnimationRunning = false;
                        }
                    }
                });
            }
            
            public void onAnimationRepeat(final Animator animator) {
            }
            
            public void onAnimationStart(final Animator animator) {
                GaugeViewPager.this.isAnimationRunning = true;
            }
        });
    }
    
    public GaugeViewPager(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.mCurrentPosition = -1;
        this.mNextPosition = -1;
        this.mPreviousPosition = -1;
        this.operationQueue = new LinkedList<>();
        this.isAnimationRunning = false;
        this.positionAnimator = new ValueAnimator();
    }
    
    private int getMaxTopMarginValue() {
        return this.getResources().getDimensionPixelSize(R.dimen.gauge_transition_margin) + this.getResources().getDimensionPixelSize(R.dimen.gauge_frame_height);
    }
    
    private void performOperation(final Operation currentOperation, final boolean b) {
        this.currentOperation = currentOperation;
        if (currentOperation == Operation.NEXT) {
            this.updateWidgetView(this.mNextPosition);
            this.positionAnimator.setIntValues(-this.getMaxTopMarginValue(), 0);
        }
        else if (currentOperation == Operation.PREVIOUS) {
            this.updateWidgetView(this.mPreviousPosition);
            this.positionAnimator.setIntValues(-this.getMaxTopMarginValue(), -this.getMaxTopMarginValue() * 2);
        }
        final ValueAnimator positionAnimator = this.positionAnimator;
        long duration;
        if (b) {
            duration = 100L;
        }
        else {
            duration = 200L;
        }
        positionAnimator.setDuration(duration);
        this.positionAnimator.start();
    }
    
    private void populateView(final int n, final int n2) {
        boolean b = true;
        final ViewGroup viewGroup = (ViewGroup)this.mSlider.getChildAt(n);
        final View child = viewGroup.getChildAt(0);
        final Adapter mAdapter = this.mAdapter;
        if (n != 1) {
            b = false;
        }
        final View view = mAdapter.getView(n2, child, viewGroup, b);
        if (view != child) {
            viewGroup.removeAllViews();
            viewGroup.addView(view);
        }
    }
    
    private void updateWidgetView(final int n) {
        final DashboardWidgetPresenter presenter = this.mAdapter.getPresenter(n);
        if (presenter != null) {
            GaugeViewPager.sLogger.v("widget:: update before animation");
            presenter.setWidgetVisibleToUser(true);
        }
    }
    
    public int getSelectedPosition() {
        return this.mCurrentPosition;
    }
    
    public void moveNext() {
        if (this.isAnimationRunning) {
            if (this.operationQueue.peek() != Operation.NEXT) {
                this.operationQueue.clear();
            }
            this.operationQueue.add(Operation.NEXT);
        }
        else {
            this.performOperation(Operation.NEXT, false);
        }
    }
    
    public void movePrevious() {
        if (this.isAnimationRunning) {
            if (this.operationQueue.peek() != Operation.PREVIOUS) {
                this.operationQueue.clear();
            }
            this.operationQueue.add(Operation.PREVIOUS);
        }
        else {
            this.performOperation(Operation.PREVIOUS, false);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.removeAllViews();
        (this.mSlider = new LinearLayout(this.getContext())).setOrientation(LinearLayout.VERTICAL);
        this.mSlider.setLayoutParams(new MarginLayoutParams(-1, -2));
        this.addView(this.mSlider);
        LayoutInflater.from(this.getContext()).inflate(R.layout.gauge_view_scrims, this, true);
        for (int i = 0; i < 3; ++i) {
            final FrameLayout frameLayout = new FrameLayout(this.getContext());
            if (i == 0 || i == 1) {
                frameLayout.setLayoutParams(new MarginLayoutParams(this.getResources().getDimensionPixelSize(R.dimen.gauge_frame_width), this.getResources().getDimensionPixelSize(R.dimen.gauge_frame_height) + this.getResources().getDimensionPixelSize(R.dimen.gauge_transition_margin)));
            }
            else {
                frameLayout.setLayoutParams(new MarginLayoutParams(this.getResources().getDimensionPixelSize(R.dimen.gauge_frame_width), this.getResources().getDimensionPixelSize(R.dimen.gauge_frame_height)));
            }
            this.mSlider.addView(frameLayout);
        }
        ((ViewGroup.MarginLayoutParams)this.mSlider.getLayoutParams()).topMargin = -this.getMaxTopMarginValue();
    }
    
    public void setAdapter(final Adapter mAdapter) {
        this.mAdapter = mAdapter;
        this.setSelectedPosition(0);
    }
    
    public void setChangeListener(final ChangeListener mChangeListener) {
        this.mChangeListener = mChangeListener;
    }
    
    public void setSelectedPosition(final int mCurrentPosition) {
        if (this.mAdapter == null) {
            throw new IllegalStateException("Set the Adapter before setting the position");
        }
        final int mCurrentPosition2 = this.mCurrentPosition;
        if (mCurrentPosition >= 0 && mCurrentPosition < this.mAdapter.getCount()) {
            final int n = (this.mAdapter.getCount() + mCurrentPosition - 1) % this.mAdapter.getCount();
            final int n2 = (mCurrentPosition + 1) % this.mAdapter.getCount();
            int mNextPosition;
            if ((mNextPosition = n) == this.mAdapter.getExcludedPosition()) {
                mNextPosition = (this.mAdapter.getCount() + n - 1) % this.mAdapter.getCount();
            }
            int mPreviousPosition;
            if ((mPreviousPosition = n2) == this.mAdapter.getExcludedPosition()) {
                mPreviousPosition = (n2 + 1) % this.mAdapter.getCount();
            }
            this.mNextPosition = mNextPosition;
            this.mPreviousPosition = mPreviousPosition;
            this.mCurrentPosition = mCurrentPosition;
            this.populateView(0, this.mNextPosition);
            this.populateView(1, this.mCurrentPosition);
            this.populateView(2, this.mPreviousPosition);
            ((ViewGroup.MarginLayoutParams)this.mSlider.getLayoutParams()).topMargin = -this.getMaxTopMarginValue();
            if (this.mChangeListener != null && this.mCurrentPosition != mCurrentPosition2) {
                this.mChangeListener.onGaugeChanged(this.mCurrentPosition);
            }
        }
    }
    
    public void updateState() {
        this.setSelectedPosition(this.getSelectedPosition());
    }
    
    public interface Adapter
    {
        int getCount();
        
        int getExcludedPosition();
        
        DashboardWidgetPresenter getPresenter(final int p0);
        
        View getView(final int p0, final View p1, final ViewGroup p2, final boolean p3);
    }
    
    public interface ChangeListener
    {
        void onGaugeChanged(final int p0);
    }

    public enum Operation {
        NEXT(0),
        PREVIOUS(1);

        private int value;
        Operation(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}
