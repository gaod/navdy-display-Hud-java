package com.navdy.hud.app.ui.component;

import java.util.ArrayList;
import android.os.Looper;
import android.util.AttributeSet;
import android.content.Context;
import java.util.List;
import android.os.Handler;
import android.widget.TextView;

import com.navdy.hud.app.R;

public class CarouselTextView extends android.support.v7.widget.AppCompatTextView
{
    private static final long CAROUSEL_ANIM_DURATION = 500L;
    private static final long CAROUSEL_VIEW_INTERVAL = 5000L;
    private Runnable carouselChangeRunnable;
    private int currentTextIndex;
    private final Handler handler;
    private List<String> textList;
    private final float translationY;
    
    public CarouselTextView(final Context context, final AttributeSet set) {
        super(context, set);
        this.carouselChangeRunnable = new Runnable() {
            @Override
            public void run() {
                if (CarouselTextView.this.textList != null) {
                    final int size = CarouselTextView.this.textList.size();
                    if (size > 0) {
                        CarouselTextView.this.currentTextIndex = (CarouselTextView.this.currentTextIndex + 1) % size;
                        CarouselTextView.this.rotateWithAnimation(CarouselTextView.this.textList.get(CarouselTextView.this.currentTextIndex));
                        CarouselTextView.this.handler.postDelayed(this, CAROUSEL_VIEW_INTERVAL);
                    }
                }
            }
        };
        this.translationY = this.getResources().getDimension(R.dimen.text_fade_anim_translationy);
        this.handler = new Handler(Looper.getMainLooper());
    }
    
    private void rotateWithAnimation(final String s) {
        this.animate().translationY(this.translationY).alpha(0.0f).setDuration(CAROUSEL_ANIM_DURATION).withEndAction(new Runnable() {
            @Override
            public void run() {
                CarouselTextView.this.setText(s);
                CarouselTextView.this.animate().translationY(0.0f).alpha(1.0f).setDuration(500L);
            }
        });
    }
    
    public void clear() {
        this.animate().cancel();
        this.handler.removeCallbacks(this.carouselChangeRunnable);
        this.textList = null;
        this.currentTextIndex = -1;
    }
    
    public void setCarousel(final int... array) {
        this.handler.removeCallbacks(this.carouselChangeRunnable);
        this.textList = new ArrayList<>();
        for (int length = array.length, i = 0; i < length; ++i) {
            this.textList.add(this.getResources().getString(array[i]));
        }
        this.currentTextIndex = -1;
        this.handler.post(this.carouselChangeRunnable);
    }
}
