package com.navdy.hud.app.ui.component.homescreen;

import android.animation.Animator;

import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.animation.TimeInterpolator;
import android.view.animation.AccelerateDecelerateInterpolator;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import butterknife.ButterKnife;
import android.animation.Animator;
import android.view.View;
import android.animation.AnimatorSet;
import com.navdy.hud.app.view.MainView;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.LinearLayout;
import butterknife.InjectView;
import android.widget.ImageView;
import android.animation.ObjectAnimator;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import android.widget.RelativeLayout;

public class NoLocationView extends RelativeLayout
{
    private Bus bus;
    private Logger logger;
    private ObjectAnimator noLocationAnimator;
    @InjectView(R.id.noLocationImage)
    ImageView noLocationImage;
    @InjectView(R.id.noLocationTextContainer)
    LinearLayout noLocationTextContainer;
    
    public NoLocationView(final Context context) {
        this(context, null);
    }
    
    public NoLocationView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public NoLocationView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    public void getCustomAnimator(final MainView.CustomAnimationMode customAnimationMode, final AnimatorSet.Builder animatorSet$Builder) {
        switch (customAnimationMode) {
            case SHRINK_LEFT:
                animatorSet$Builder.with(HomeScreenUtils.getXPositionAnimator(this.noLocationImage, HomeScreenResourceValues.noLocationImageShrinkLeftX));
                animatorSet$Builder.with(HomeScreenUtils.getXPositionAnimator(this.noLocationTextContainer, HomeScreenResourceValues.noLocationTextShrinkLeftX));
                break;
            case EXPAND:
                animatorSet$Builder.with(HomeScreenUtils.getXPositionAnimator(this.noLocationImage, HomeScreenResourceValues.noLocationImageX));
                animatorSet$Builder.with(HomeScreenUtils.getXPositionAnimator(this.noLocationTextContainer, HomeScreenResourceValues.noLocationTextX));
                break;
        }
    }
    
    public void hideLocationUI() {
        this.logger.v("hideLocationUI");
        this.setVisibility(GONE);
        if (this.noLocationAnimator != null) {
            this.logger.v("cancelled animation");
            this.noLocationAnimator.cancel();
            this.noLocationImage.setRotation(0.0f);
        }
    }
    
    public boolean isAcquiringLocation() {
        return this.getVisibility() == 0;
    }
    
    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        ButterKnife.inject(this);
        (this.bus = RemoteDeviceManager.getInstance().getBus()).register(this);
    }
    
    public void setView(final MainView.CustomAnimationMode customAnimationMode) {
        switch (customAnimationMode) {
            case EXPAND:
                this.noLocationImage.setX((float)HomeScreenResourceValues.noLocationImageX);
                this.noLocationTextContainer.setX((float)HomeScreenResourceValues.noLocationTextX);
                break;
            case SHRINK_LEFT:
                this.noLocationImage.setX((float)HomeScreenResourceValues.noLocationImageShrinkLeftX);
                this.noLocationTextContainer.setX((float)HomeScreenResourceValues.noLocationTextShrinkLeftX);
                break;
        }
    }
    
    public void showLocationUI() {
        this.logger.v("showLocationUI");
        this.setVisibility(View.VISIBLE);
        if (this.noLocationAnimator == null) {
            (this.noLocationAnimator = ObjectAnimator.ofFloat(this.noLocationImage, View.ROTATION, 360.0f)).setDuration(500L);
            this.noLocationAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
            this.noLocationAnimator.addListener(new DefaultAnimationListener() {
                @Override
                public void onAnimationEnd(final Animator animator) {
                    if (NoLocationView.this.getVisibility() == 0) {
                        NoLocationView.this.noLocationAnimator.setStartDelay(33L);
                        NoLocationView.this.noLocationAnimator.start();
                    }
                }
            });
        }
        this.noLocationAnimator.start();
    }
}
