package com.navdy.hud.app.ui.component.carousel;

import android.view.ViewTreeObserver;
import android.graphics.RectF;
import android.util.Property;
import android.animation.ObjectAnimator;
import android.animation.Animator;
import android.animation.Animator;

import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.animation.AnimatorSet;
import android.graphics.drawable.GradientDrawable;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.view.ViewGroup;
import android.util.AttributeSet;
import android.view.View;
import android.content.Context;
import android.widget.RelativeLayout;

class CircleIndicator extends RelativeLayout implements IProgressIndicator
{
    private int circleFocusSize;
    private int circleMargin;
    private int circleSize;
    private final Context context;
    private int currentItem;
    private int currentItemColor;
    private int defaultColor;
    private int id;
    private int itemCount;
    private View moveAnimatorView;
    private CarouselIndicator.Orientation orientation;
    
    public CircleIndicator(final Context context) {
        super(context);
        this.currentItem = -1;
        this.currentItemColor = -1;
        this.id = 100;
        this.context = context;
    }
    
    public CircleIndicator(final Context context, final AttributeSet set) {
        super(context, set);
        this.currentItem = -1;
        this.currentItemColor = -1;
        this.id = 100;
        this.context = context;
    }
    
    private void buildItems(final int n) {
        if (n != 0) {
            View child = null;
            final int childCount = this.getChildCount();
            if (childCount > 0) {
                child = this.getChildAt(childCount - 1);
                final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup.MarginLayoutParams)child.getLayoutParams();
                if (this.orientation == CarouselIndicator.Orientation.HORIZONTAL) {
                    viewGroup$MarginLayoutParams.rightMargin = 0;
                }
                else {
                    viewGroup$MarginLayoutParams.bottomMargin = 0;
                }
            }
            for (int i = childCount; i < n + childCount; ++i) {
                final View view = new View(this.context);
                view.setId(this.id++);
                view.setBackgroundResource(R.drawable.circle_page_indicator);
                final RelativeLayout.LayoutParams relativeLayout$LayoutParams = new RelativeLayout.LayoutParams(this.circleSize, this.circleSize);
                if (i == 0 && child == null) {
                    if (this.orientation == CarouselIndicator.Orientation.HORIZONTAL) {
                        relativeLayout$LayoutParams.addRule(9);
                        relativeLayout$LayoutParams.leftMargin = this.circleMargin;
                    }
                    else {
                        relativeLayout$LayoutParams.addRule(10);
                        relativeLayout$LayoutParams.topMargin = this.circleMargin;
                    }
                }
                if (i == this.itemCount - 1) {
                    if (this.orientation == CarouselIndicator.Orientation.HORIZONTAL) {
                        relativeLayout$LayoutParams.rightMargin = this.circleMargin;
                    }
                    else {
                        relativeLayout$LayoutParams.bottomMargin = this.circleMargin;
                    }
                }
                if (this.orientation == CarouselIndicator.Orientation.HORIZONTAL) {
                    relativeLayout$LayoutParams.addRule(15);
                    if (i > 0) {
                        relativeLayout$LayoutParams.leftMargin = this.circleMargin;
                    }
                    if (child != null) {
                        relativeLayout$LayoutParams.addRule(1, child.getId());
                    }
                }
                else {
                    relativeLayout$LayoutParams.addRule(14);
                    if (i > 0) {
                        relativeLayout$LayoutParams.topMargin = this.circleMargin;
                    }
                    if (child != null) {
                        relativeLayout$LayoutParams.addRule(3, child.getId());
                    }
                }
                view.setAlpha(0.5f);
                this.addView(view, (ViewGroup.LayoutParams)relativeLayout$LayoutParams);
                child = view;
            }
            if (this.moveAnimatorView == null) {
                (this.moveAnimatorView = new View(this.context)).setBackgroundResource(R.drawable.circle_page_indicator_focus);
                this.moveAnimatorView.setLayoutParams((ViewGroup.LayoutParams)new RelativeLayout.LayoutParams(this.circleFocusSize, this.circleFocusSize));
                this.moveAnimatorView.setVisibility(GONE);
                this.addView(this.moveAnimatorView);
            }
        }
    }
    
    private int getTargetViewPos(final View view) {
        int n;
        if (this.orientation == CarouselIndicator.Orientation.HORIZONTAL) {
            n = view.getLeft() + view.getWidth() / 2 - this.circleFocusSize / 2;
        }
        else {
            n = view.getTop() + view.getHeight() / 2 - this.circleFocusSize / 2;
        }
        return n;
    }
    
    private void setViewPos(final View view) {
        final float n = this.getTargetViewPos(view);
        int color;
        if ((color = this.currentItemColor) == -1) {
            color = this.defaultColor;
        }
        if (this.orientation == CarouselIndicator.Orientation.HORIZONTAL) {
            this.moveAnimatorView.setX(n);
        }
        else {
            this.moveAnimatorView.setY(n);
        }
        ((GradientDrawable)this.moveAnimatorView.getBackground()).setColor(color);
        this.moveAnimatorView.setVisibility(View.VISIBLE);
        this.moveAnimatorView.invalidate();
    }
    
    public int getCurrentItem() {
        return this.currentItem;
    }
    
    public AnimatorSet getItemMoveAnimator(final int n, final int color) {
        final AnimatorSet set = null;
        AnimatorSet set2;
        if (this.currentItem == -1) {
            set2 = set;
        }
        else {
            set2 = set;
            if (n >= 0) {
                set2 = set;
                if (n < this.itemCount) {
                    this.getChildAt(this.currentItem);
                    final View child = this.getChildAt(n);
                    final GradientDrawable gradientDrawable = (GradientDrawable)this.moveAnimatorView.getBackground();
                    if (color == -1) {
                        gradientDrawable.setColor(this.defaultColor);
                    }
                    else {
                        gradientDrawable.setColor(color);
                    }
                    final AnimatorSet set3 = new AnimatorSet();
                    set3.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
                        @Override
                        public void onAnimationEnd(final Animator animator) {
                            CircleIndicator.this.currentItem = n;
                        }
                    });
                    final View moveAnimatorView = this.moveAnimatorView;
                    Property property;
                    if (this.orientation == CarouselIndicator.Orientation.HORIZONTAL) {
                        property = View.X;
                    }
                    else {
                        property = View.Y;
                    }
                    set3.play((Animator)ObjectAnimator.ofFloat(moveAnimatorView, property, new float[] { this.getTargetViewPos(child) }));
                    set2 = set3;
                }
            }
        }
        return set2;
    }
    
    public RectF getItemPos(final int n) {
        RectF rectF;
        if (n >= 0 && n < this.itemCount) {
            final View child = this.getChildAt(n);
            final View view = (View)child.getParent();
            rectF = new RectF((float)(child.getLeft() + view.getLeft()), (float)(child.getTop() + view.getTop()), 0.0f, 0.0f);
        }
        else {
            rectF = null;
        }
        return rectF;
    }
    
    public void setCurrentItem(final int n) {
        this.setCurrentItem(n, -1);
    }
    
    public void setCurrentItem(final int currentItem, int currentItemColor) {
        if (this.currentItem != currentItem) {
            this.currentItemColor = currentItemColor;
            if (currentItem >= 0 && currentItem < this.itemCount) {
                final View child = this.getChildAt(currentItem);
                if (this.orientation == CarouselIndicator.Orientation.HORIZONTAL) {
                    currentItemColor = child.getLeft();
                }
                else {
                    currentItemColor = child.getTop();
                }
                if (currentItemColor == 0) {
                    child.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)new ViewTreeObserver.OnGlobalLayoutListener() {
                        public void onGlobalLayout() {
                            int n;
                            if (CircleIndicator.this.orientation == CarouselIndicator.Orientation.HORIZONTAL) {
                                n = child.getLeft();
                            }
                            else {
                                n = child.getTop();
                            }
                            if (n != 0) {
                                child.getViewTreeObserver().removeOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)this);
                                CircleIndicator.this.setViewPos(child);
                            }
                        }
                    });
                }
                else {
                    this.setViewPos(child);
                }
            }
            this.currentItem = currentItem;
        }
    }
    
    public void setItemCount(int i) {
        if (i < 0) {
            throw new IllegalArgumentException();
        }
        this.currentItem = -1;
        if (this.getChildCount() == 0) {
            this.buildItems(this.itemCount = i);
        }
        else if (this.itemCount != i) {
            final int n = this.itemCount - i;
            if (n > 0) {
                this.itemCount = i;
                this.removeView(this.moveAnimatorView);
                for (i = 0; i < n; ++i) {
                    this.removeView(this.getChildAt(this.getChildCount() - 1));
                }
                i = this.getChildCount();
                if (i > 0) {
                    final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup.MarginLayoutParams)this.getChildAt(i - 1).getLayoutParams();
                    if (this.orientation == CarouselIndicator.Orientation.HORIZONTAL) {
                        viewGroup$MarginLayoutParams.rightMargin = this.circleMargin;
                    }
                    else {
                        viewGroup$MarginLayoutParams.bottomMargin = this.circleMargin;
                    }
                }
                this.addView(this.moveAnimatorView);
            }
            else {
                this.itemCount = i;
                this.removeView(this.moveAnimatorView);
                this.buildItems(-n);
                this.addView(this.moveAnimatorView);
            }
        }
    }
    
    public void setOrientation(final CarouselIndicator.Orientation orientation) {
        this.orientation = orientation;
    }
    
    public void setProperties(final int circleSize, final int circleFocusSize, final int circleMargin, final int defaultColor) {
        this.circleSize = circleSize;
        this.circleFocusSize = circleFocusSize;
        this.circleMargin = circleMargin;
        this.defaultColor = defaultColor;
    }
}
