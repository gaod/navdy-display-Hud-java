package com.navdy.hud.app.ui.component.mainmenu;

import android.graphics.Shader;

import com.navdy.hud.app.R;
import com.navdy.hud.app.maps.MapEvents$LocationFix;
import com.navdy.hud.app.maps.MapEvents$ManeuverDisplay;
import com.navdy.hud.app.ui.component.homescreen.SmartDashView;
import android.os.Bundle;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.MapEvents;
import android.location.Location;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.analytics.TelemetryDataManager;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.obd.PidSet;
import com.navdy.hud.app.view.DashboardWidgetPresenter;
import com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder;
import kotlin.NoWhenBranchMatchedException;
import java.util.List;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.obd.Pids;
import com.squareup.otto.Subscribe;
import android.view.View;
import android.view.ViewGroup;
import android.content.res.Resources;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import kotlin.jvm.internal.Intrinsics;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import kotlin.TypeCastException;
import com.navdy.hud.app.HudApplication;
//import kotlin.jvm.internal.DefaultConstructorMarker;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import org.jetbrains.annotations.Nullable;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager;
import android.content.SharedPreferences;
import com.navdy.hud.app.util.HeadingDataUtil;
import android.widget.FrameLayout;
import com.navdy.hud.app.view.DashboardWidgetView;
import org.jetbrains.annotations.NotNull;
import com.squareup.otto.Bus;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import kotlin.Metadata;

import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.AMBIENT_TEMPERATURE_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.COMPASS_WIDGET_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.DRIVE_SCORE_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ENGINE_OIL_TEMPERATURE_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.FUEL_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.INTAKE_PRESSURE_GAUGE_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.MPG_AVG_WIDGET_ID;
import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SPEED_LIMIT_SIGN_GAUGE_ID;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000Ò\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 r2\u00020\u0001:\u0002rsB-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u0001¢\u0006\u0002\u0010\u000bJ\u0010\u00109\u001a\u00020:2\u0006\u0010;\u001a\u00020<H\u0007J\"\u0010=\u001a\u0004\u0018\u00010\u00012\u0006\u0010\n\u001a\u00020\u00012\u0006\u0010>\u001a\u00020?2\u0006\u0010@\u001a\u00020?H\u0016J\b\u0010A\u001a\u00020\rH\u0016J\u0010\u0010B\u001a\n\u0012\u0004\u0012\u00020D\u0018\u00010CH\u0016J\u0012\u0010E\u001a\u0004\u0018\u00010D2\u0006\u0010F\u001a\u00020\rH\u0016J\n\u0010G\u001a\u0004\u0018\u00010HH\u0016J\n\u0010I\u001a\u0004\u0018\u00010JH\u0016J\b\u0010K\u001a\u00020'H\u0016J\b\u0010L\u001a\u00020'H\u0016J\u0018\u0010M\u001a\u00020'2\u0006\u0010N\u001a\u00020\r2\u0006\u0010F\u001a\u00020\rH\u0016J(\u0010O\u001a\u00020:2\u0006\u0010P\u001a\u00020D2\u0006\u0010Q\u001a\u00020R2\u0006\u0010F\u001a\u00020\r2\u0006\u0010S\u001a\u00020TH\u0016J\u0010\u0010U\u001a\u00020:2\u0006\u0010V\u001a\u00020WH\u0007J\u0010\u0010X\u001a\u00020:2\u0006\u0010Y\u001a\u00020ZH\u0007J\b\u0010[\u001a\u00020:H\u0016J\b\u0010\\\u001a\u00020:H\u0016J\u0010\u0010]\u001a\u00020:2\u0006\u0010^\u001a\u00020_H\u0007J\u0010\u0010`\u001a\u00020:2\u0006\u0010a\u001a\u00020bH\u0016J\u0010\u0010c\u001a\u00020:2\u0006\u0010;\u001a\u00020dH\u0007J\u0010\u0010e\u001a\u00020:2\u0006\u0010;\u001a\u00020fH\u0007J\b\u0010g\u001a\u00020:H\u0016J\u0010\u0010h\u001a\u00020:2\u0006\u0010i\u001a\u00020jH\u0016J\u0010\u0010k\u001a\u00020'2\u0006\u0010a\u001a\u00020bH\u0016J\u0010\u0010l\u001a\u00020:2\u0006\u0010N\u001a\u00020\rH\u0016J\u0010\u0010m\u001a\u00020:2\u0006\u0010n\u001a\u00020\rH\u0016J\b\u0010o\u001a\u00020:H\u0016J\u000e\u0010p\u001a\u00020:2\u0006\u0010F\u001a\u00020\rJ\b\u0010q\u001a\u00020:H\u0016R\u000e\u0010\f\u001a\u00020\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R$\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u000f\u001a\u00020\u0010@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001a\u0010\u0016\u001a\u00020\u0017X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u0011\u0010\u001c\u001a\u00020\u001d¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u001a\u0010 \u001a\u00020!X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R\u000e\u0010\n\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010&\u001a\u00020'X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R \u0010,\u001a\b\u0018\u00010-R\u00020.X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b/\u00100\"\u0004\b1\u00102R\u0011\u00103\u001a\u00020.¢\u0006\b\n\u0000\u001a\u0004\b4\u00105R\u001a\u00106\u001a\u00020'X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b7\u0010)\"\u0004\b8\u0010+R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006t" }, d2 = { "Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;", "bus", "Lcom/squareup/otto/Bus;", "sharedPreferences", "Landroid/content/SharedPreferences;", "vscrollComponent", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;", "presenter", "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;", "parent", "(Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V", "backSelection", "", "backSelectionId", "value", "Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;", "gaugeType", "getGaugeType", "()Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;", "setGaugeType", "(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;)V", "gaugeView", "Lcom/navdy/hud/app/view/DashboardWidgetView;", "getGaugeView", "()Lcom/navdy/hud/app/view/DashboardWidgetView;", "setGaugeView", "(Lcom/navdy/hud/app/view/DashboardWidgetView;)V", "gaugeViewContainer", "Landroid/widget/FrameLayout;", "getGaugeViewContainer", "()Landroid/widget/FrameLayout;", "headingDataUtil", "Lcom/navdy/hud/app/util/HeadingDataUtil;", "getHeadingDataUtil", "()Lcom/navdy/hud/app/util/HeadingDataUtil;", "setHeadingDataUtil", "(Lcom/navdy/hud/app/util/HeadingDataUtil;)V", "registered", "", "getRegistered", "()Z", "setRegistered", "(Z)V", "smartDashWidgetCache", "Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;", "Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;", "getSmartDashWidgetCache", "()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;", "setSmartDashWidgetCache", "(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;)V", "smartDashWidgetManager", "getSmartDashWidgetManager", "()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;", "userPreferenceChanged", "getUserPreferenceChanged", "setUserPreferenceChanged", "ObdPidChangeEvent", "", "event", "Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;", "getChildMenu", "args", "", "path", "getInitialSelection", "getItems", "", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "getModelfromPos", "pos", "getScrollIndex", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;", "getType", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;", "isBindCallsEnabled", "isFirstItemEmpty", "isItemClickable", "id", "onBindToView", "model", "view", "Landroid/view/View;", "state", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;", "onDriveScoreUpdated", "driveScoreUpdated", "Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;", "onDriverProfileChanged", "profileChanged", "Lcom/navdy/hud/app/event/DriverProfileChanged;", "onFastScrollEnd", "onFastScrollStart", "onGpsLocationChanged", "location", "Landroid/location/Location;", "onItemSelected", "selection", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;", "onLocationFixChangeEvent", "Lcom/navdy/hud/app/maps/MapEvents$LocationFix;", "onMapEvent", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "onScrollIdle", "onUnload", "level", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;", "selectItem", "setBackSelectionId", "setBackSelectionPos", "n", "setSelectedIcon", "showGauge", "showToolTip", "Companion", "GaugeType", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class DashGaugeConfiguratorMenu implements IMenu
{
    public static final Companion Companion;
    private static final VerticalList.Model back;
    private static final int backColor;
    private static final int bkColorUnselected;
    private static final int centerGaugeBackgroundColor;
    private static final ArrayList<VerticalList.Model> centerGaugeOptionsList;
    private static final String centerGaugeTitle;
    private static final String offLabel;
    private static final String onLabel;
    private static final Logger sLogger;
    private static final ArrayList<VerticalList.Model> sideGaugesOptionsList;
    private static final String sideGaugesTitle;
    private static final VerticalList.Model speedoMeter;
    private static final VerticalList.Model tachoMeter;
    private static final UIStateManager uiStateManager;
    private static final String unavailableLabel;
    private int backSelection;
    private int backSelectionId;
    private final Bus bus;
    @NotNull
    private GaugeType gaugeType;
    @NotNull
    private DashboardWidgetView gaugeView;
    @NotNull
    private final FrameLayout gaugeViewContainer;
    @NotNull
    private HeadingDataUtil headingDataUtil;
    private final IMenu parent;
    private final MainMenuScreen2.Presenter presenter;
    private boolean registered;
    @Nullable
    private SmartDashWidgetManager.SmartDashWidgetCache smartDashWidgetCache;
    @NotNull
    private final SmartDashWidgetManager smartDashWidgetManager;
    private boolean userPreferenceChanged;
    private final VerticalMenuComponent vscrollComponent;

    static {
        Companion = new Companion();
        centerGaugeOptionsList = new ArrayList<>();
        sideGaugesOptionsList = new ArrayList<>();
        sLogger = new Logger(DashGaugeConfiguratorMenu.Companion.getClass());
        final Resources resources = HudApplication.getAppContext().getResources();
        backColor = resources.getColor(R.color.mm_back);
        bkColorUnselected = resources.getColor(R.color.icon_bk_color_unselected);
        final CharSequence text = resources.getText(R.string.carousel_menu_smartdash_select_center_gauge);
        centerGaugeTitle = (String)text;
        final CharSequence text2 = resources.getText(R.string.carousel_menu_smartdash_side_gauges);
        sideGaugesTitle = (String)text2;
        final CharSequence text3 = resources.getText(R.string.on);
        onLabel = (String)text3;
        final CharSequence text4 = resources.getText(R.string.off);
        offLabel = (String)text4;
        final CharSequence text5 = resources.getText(R.string.unavailable);
        unavailableLabel = (String)text5;
        final UIStateManager uiStateManager2 = RemoteDeviceManager.getInstance().getUiStateManager();
        Intrinsics.checkExpressionValueIsNotNull(uiStateManager2, "remoteDeviceManager.uiStateManager");
        uiStateManager = uiStateManager2;
        centerGaugeBackgroundColor = resources.getColor(R.color.mm_options_scroll_gauge);
        final int access$getBackColor$p = DashGaugeConfiguratorMenu.Companion.getBackColor();
        final VerticalList.Model buildModel = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, access$getBackColor$p, MainMenu.bkColorUnselected, access$getBackColor$p, resources.getString(R.string.back), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu…tuatorColor, title, null)");
        back = buildModel;
        final VerticalList.Model buildModel2 = IconBkColorViewHolder.buildModel(R.id.main_menu_options_tachometer, R.drawable.icon_options_dash_tachometer_2, -16777216, MainMenu.bkColorUnselected, resources.getColor(R.color.mm_options_tachometer), resources.getString(R.string.tachometer), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel2, "IconBkColorViewHolder.bu…tuatorColor, title, null)");
        tachoMeter = buildModel2;
        final VerticalList.Model buildModel3 = IconBkColorViewHolder.buildModel(R.id.main_menu_options_speedometer, R.drawable.icon_options_dash_speedometer_2, -16777216, MainMenu.bkColorUnselected, resources.getColor(R.color.mm_options_speedometer), resources.getString(R.string.speedometer), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel3, "IconBkColorViewHolder.bu…tuatorColor, title, null)");
        speedoMeter = buildModel3;
        DashGaugeConfiguratorMenu.Companion.getCenterGaugeOptionsList().add(DashGaugeConfiguratorMenu.Companion.getBack());
        DashGaugeConfiguratorMenu.Companion.getCenterGaugeOptionsList().add(DashGaugeConfiguratorMenu.Companion.getTachoMeter());
        DashGaugeConfiguratorMenu.Companion.getCenterGaugeOptionsList().add(DashGaugeConfiguratorMenu.Companion.getSpeedoMeter());
    }

    public DashGaugeConfiguratorMenu(@NotNull final Bus bus, @NotNull final SharedPreferences sharedPreferences, @NotNull final VerticalMenuComponent vscrollComponent, @NotNull final MainMenuScreen2.Presenter presenter, @NotNull final IMenu parent) {
        Intrinsics.checkParameterIsNotNull(bus, "bus");
        Intrinsics.checkParameterIsNotNull(sharedPreferences, "sharedPreferences");
        Intrinsics.checkParameterIsNotNull(vscrollComponent, "vscrollComponent");
        Intrinsics.checkParameterIsNotNull(presenter, "presenter");
        Intrinsics.checkParameterIsNotNull(parent, "parent");
        this.bus = bus;
        SharedPreferences sharedPreferences1 = sharedPreferences;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
        this.headingDataUtil = new HeadingDataUtil();
        final FrameLayout selectedCustomView = this.vscrollComponent.selectedCustomView;
        Intrinsics.checkExpressionValueIsNotNull(selectedCustomView, "vscrollComponent.selectedCustomView");
        this.gaugeViewContainer = selectedCustomView;
        (this.gaugeView = new DashboardWidgetView(this.gaugeViewContainer.getContext())).setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.gaugeViewContainer.removeAllViews();
        this.gaugeViewContainer.addView(this.gaugeView);

        this.smartDashWidgetManager = new com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager(sharedPreferences1, com.navdy.hud.app.HudApplication.getAppContext());
        this.smartDashWidgetManager.onResume();
        this.smartDashWidgetManager.setFilter(DashGaugeConfiguratorMenu$1.INSTANCE);
        this.smartDashWidgetManager.reLoadAvailableWidgets(true);
        this.smartDashWidgetManager.registerForChanges(new Object() {
            @Subscribe
            public final void onReload(@NotNull SmartDashWidgetManager.Reload reload) {
                Intrinsics.checkParameterIsNotNull(reload, "reload");
                switch (reload) {
                    case RELOADED:
                        DashGaugeConfiguratorMenu.this.presenter.updateCurrentMenu(DashGaugeConfiguratorMenu.this);
                        return;
                    case RELOAD_CACHE:
                        DashGaugeConfiguratorMenu.this.setSmartDashWidgetCache(DashGaugeConfiguratorMenu.this.getSmartDashWidgetManager().buildSmartDashWidgetCache(0));
                        DashGaugeConfiguratorMenu.this.presenter.updateCurrentMenu(DashGaugeConfiguratorMenu.this);
                        return;
                    default:
                }
            }
        });


        this.gaugeType = GaugeType.CENTER;
    }

    @NotNull
    public static /* synthetic */ VerticalList.Model access$getBack$cp() {
        return DashGaugeConfiguratorMenu.back;
    }

    public static  /* synthetic */ int access$getBackColor$cp() {
        return DashGaugeConfiguratorMenu.backColor;
    }

    public static  /* synthetic */ int access$getBkColorUnselected$cp() {
        return DashGaugeConfiguratorMenu.bkColorUnselected;
    }

    public static  /* synthetic */ int access$getCenterGaugeBackgroundColor$cp() {
        return DashGaugeConfiguratorMenu.centerGaugeBackgroundColor;
    }

    @NotNull
    public static  /* synthetic */ ArrayList access$getCenterGaugeOptionsList$cp() {
        return DashGaugeConfiguratorMenu.centerGaugeOptionsList;
    }

    @NotNull
    public static  /* synthetic */ String access$getCenterGaugeTitle$cp() {
        return DashGaugeConfiguratorMenu.centerGaugeTitle;
    }

    @NotNull
    public static /* synthetic */ String access$getOffLabel$cp() {
        return DashGaugeConfiguratorMenu.offLabel;
    }

    @NotNull
    public static /* synthetic */ String access$getOnLabel$cp() {
        return DashGaugeConfiguratorMenu.onLabel;
    }

    @NotNull
    public static /* synthetic */ MainMenuScreen2.Presenter access$getPresenter$p(final DashGaugeConfiguratorMenu dashGaugeConfiguratorMenu) {
        return dashGaugeConfiguratorMenu.presenter;
    }

    @NotNull
    public static  /* synthetic */ Logger access$getSLogger$cp() {
        return DashGaugeConfiguratorMenu.sLogger;
    }

    @NotNull
    public static /* synthetic */ ArrayList access$getSideGaugesOptionsList$cp() {
        return DashGaugeConfiguratorMenu.sideGaugesOptionsList;
    }

    @NotNull
    public static  /* synthetic */ String access$getSideGaugesTitle$cp() {
        return DashGaugeConfiguratorMenu.sideGaugesTitle;
    }

    @NotNull
    public static  /* synthetic */ VerticalList.Model access$getSpeedoMeter$cp() {
        return DashGaugeConfiguratorMenu.speedoMeter;
    }

    @NotNull
    public static /* synthetic */ VerticalList.Model access$getTachoMeter$cp() {
        return DashGaugeConfiguratorMenu.tachoMeter;
    }

    @NotNull
    public static  /* synthetic */ UIStateManager access$getUiStateManager$cp() {
        return DashGaugeConfiguratorMenu.uiStateManager;
    }

    @NotNull
    public static  /* synthetic */ String access$getUnavailableLabel$cp() {
        return DashGaugeConfiguratorMenu.unavailableLabel;
    }

    @Subscribe
    public final void ObdPidChangeEvent(@NotNull final ObdManager.ObdPidChangeEvent obdPidChangeEvent) {
        Intrinsics.checkParameterIsNotNull(obdPidChangeEvent, "event");
        switch (obdPidChangeEvent.pid.getId()) {
            case 47:
                this.smartDashWidgetManager.updateWidget(FUEL_GAUGE_ID, obdPidChangeEvent.pid.getValue());
                break;
            case 256:
                this.smartDashWidgetManager.updateWidget(MPG_AVG_WIDGET_ID, obdPidChangeEvent.pid.getValue());
                break;
            case 5:
                this.smartDashWidgetManager.updateWidget(ENGINE_TEMPERATURE_GAUGE_ID, obdPidChangeEvent.pid.getValue());
                break;
        }
    }

    @Nullable
    @Override
    public IMenu getChildMenu(@NotNull final IMenu menu, @NotNull final String s, @NotNull final String s2) {
        Intrinsics.checkParameterIsNotNull(menu, "parent");
        Intrinsics.checkParameterIsNotNull(s, "args");
        Intrinsics.checkParameterIsNotNull(s2, "path");
        return null;
    }

    @NotNull
    public final GaugeType getGaugeType() {
        return this.gaugeType;
    }

    @NotNull
    public final DashboardWidgetView getGaugeView() {
        return this.gaugeView;
    }

    @NotNull
    public final FrameLayout getGaugeViewContainer() {
        return this.gaugeViewContainer;
    }

    @NotNull
    public final HeadingDataUtil getHeadingDataUtil() {
        return this.headingDataUtil;
    }

    @Override
    public int getInitialSelection() {
        return 1;
    }

    @Nullable
    @Override
    public List<VerticalList.Model> getItems() {
        switch (this.gaugeType) {
            case CENTER:
                return Companion.getCenterGaugeOptionsList();
            case SIDE:
                this.smartDashWidgetCache = this.smartDashWidgetManager.buildSmartDashWidgetCache(0);
                Companion.getSideGaugesOptionsList().clear();
                Companion.getSideGaugesOptionsList().add(Companion.getBack());
                if (this.smartDashWidgetCache != null) {
                    SmartDashWidgetManager.SmartDashWidgetCache $receiver = this.smartDashWidgetCache;
                    int fluctuatorColor = HudApplication.getAppContext().getResources().getColor(R.color.mm_options_side_gauges_halo);

                    int widgetsCount = $receiver.getWidgetsCount() - 1;
                    for (int i = 0; i < widgetsCount; i++) {
                        String title = null;
                        boolean isGaugeEnabled = true;
                        String subtitle;
                        DashboardWidgetPresenter widgetPresenter = $receiver.getWidgetPresenter(i);
                        if (widgetPresenter != null) {
                            title = widgetPresenter.getWidgetName();
                        }
                        if (title == null) {
                            title = "";
                        }
                        widgetPresenter = $receiver.getWidgetPresenter(i);
                        String identifier = widgetPresenter != null ? widgetPresenter.getWidgetIdentifier() : null;
                        boolean isGaugeOn = this.smartDashWidgetManager.isGaugeOn(identifier);
                        PidSet pidSet = ObdManager.getInstance().getSupportedPids();
                        if (identifier != null) {
                            boolean z;
                            switch (identifier) {
                                case SmartDashWidgetManager.MPG_AVG_WIDGET_ID:
                                    if (identifier.equals(MPG_AVG_WIDGET_ID)) {
                                        z = pidSet != null && pidSet.contains(Pids.INSTANTANEOUS_FUEL_CONSUMPTION);
                                        isGaugeEnabled = z;
                                        break;
                                    }
                                    break;
                                case SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID:
                                    if (identifier.equals(ENGINE_TEMPERATURE_GAUGE_ID)) {
                                        z = pidSet != null && pidSet.contains(Pids.ENGINE_COOLANT_TEMPERATURE);
                                        isGaugeEnabled = z;
                                        break;
                                    }
                                    break;
                                case SmartDashWidgetManager.FUEL_GAUGE_ID:
                                    if (identifier.equals(FUEL_GAUGE_ID)) {
                                        z = pidSet != null && pidSet.contains(Pids.FUEL_LEVEL);
                                        isGaugeEnabled = z;
                                        break;
                                    }
                                    break;
                                case INTAKE_PRESSURE_GAUGE_ID:
                                    if (identifier.equals(INTAKE_PRESSURE_GAUGE_ID)) {
                                        z = pidSet != null && pidSet.contains(Pids.MANIFOLD_AIR_PRESSURE);
                                        isGaugeEnabled = z;
                                        break;
                                    }
                                    break;
                                case ENGINE_OIL_TEMPERATURE_GAUGE_ID:
                                    if (identifier.equals(ENGINE_OIL_TEMPERATURE_GAUGE_ID)) {
                                        z = pidSet != null && pidSet.contains(Pids.ENGINE_OIL_TEMPERATURE);
                                        isGaugeEnabled = z;
                                        break;
                                    }
                                case AMBIENT_TEMPERATURE_GAUGE_ID:
                                    if (identifier.equals(AMBIENT_TEMPERATURE_GAUGE_ID)) {
                                        z = pidSet != null && pidSet.contains(Pids.AMBIENT_AIR_TEMRERATURE);
                                        isGaugeEnabled = z;
                                        break;
                                    }
                                    break;
                                default:
                            }
                            if (isGaugeEnabled) {
                                subtitle = isGaugeOn ? Companion.getOnLabel() : Companion.getOffLabel();
                            } else {
                                subtitle = Companion.getUnavailableLabel();
                            }
                            Companion.getSideGaugesOptionsList().add(SwitchViewHolder.Companion.buildModel(i, fluctuatorColor, title, subtitle, isGaugeOn, isGaugeEnabled));
                        }
                    }
                }
                return Companion.getSideGaugesOptionsList();
            default:
                throw new NoWhenBranchMatchedException();
        }
    }

    @Nullable
    @Override
    public VerticalList.Model getModelfromPos(int pos) {
        switch (this.gaugeType) {
            case CENTER:
                return Companion.getCenterGaugeOptionsList().get(pos);
            case SIDE:
                return Companion.getSideGaugesOptionsList().get(pos);
            default:
                throw new NoWhenBranchMatchedException();
        }
    }

    public final boolean getRegistered() {
        return this.registered;
    }

    @Nullable
    @Override
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    @Nullable
    public final SmartDashWidgetManager.SmartDashWidgetCache getSmartDashWidgetCache() {
        return this.smartDashWidgetCache;
    }

    @NotNull
    public final SmartDashWidgetManager getSmartDashWidgetManager() {
        return this.smartDashWidgetManager;
    }

    @Nullable
    @Override
    public Menu getType() {
        return Menu.MAIN_OPTIONS;
    }

    public final boolean getUserPreferenceChanged() {
        return this.userPreferenceChanged;
    }

    @Override
    public boolean isBindCallsEnabled() {
        return false;
    }

    @Override
    public boolean isFirstItemEmpty() {
        return false;
    }

    @Override
    public boolean isItemClickable(final int n, final int n2) {
        return true;
    }

    @Override
    public void onBindToView(@NotNull final VerticalList.Model model, @NotNull final View view, final int n, @NotNull final VerticalList.ModelState modelState) {
        Intrinsics.checkParameterIsNotNull(model, "model");
        Intrinsics.checkParameterIsNotNull(view, "view");
        Intrinsics.checkParameterIsNotNull(modelState, "state");
    }

    @Subscribe
    public final void onDriveScoreUpdated(@NotNull final TelemetryDataManager.DriveScoreUpdated driveScoreUpdated) {
        Intrinsics.checkParameterIsNotNull(driveScoreUpdated, "driveScoreUpdated");
        this.smartDashWidgetManager.updateWidget(DRIVE_SCORE_GAUGE_ID, driveScoreUpdated);
    }

    @Subscribe
    public final void onDriverProfileChanged(@NotNull final DriverProfileChanged driverProfileChanged) {
        Intrinsics.checkParameterIsNotNull(driverProfileChanged, "profileChanged");
        DashGaugeConfiguratorMenu.Companion.getSLogger().d("onDriverProfileChanged, reloading the widgets");
        final SmartDashWidgetManager smartDashWidgetManager = this.smartDashWidgetManager;
        smartDashWidgetManager.reLoadAvailableWidgets(false);
    }

    @Override
    public void onFastScrollEnd() {
    }

    @Override
    public void onFastScrollStart() {
    }

    @Subscribe
    public final void onGpsLocationChanged(@NotNull final Location location) {
        Intrinsics.checkParameterIsNotNull(location, "location");
        this.headingDataUtil.setHeading(location.getBearing());
        this.smartDashWidgetManager.updateWidget(COMPASS_WIDGET_ID, this.headingDataUtil.getHeading());
    }

    @Override
    public void onItemSelected(@NotNull final VerticalList.ItemSelectionState itemSelectionState) {
        Intrinsics.checkParameterIsNotNull(itemSelectionState, "selection");
        if (Intrinsics.areEqual(this.gaugeType, GaugeType.SIDE)) {
            this.showGauge(itemSelectionState.pos);
        }
    }

    @Subscribe
    public final void onLocationFixChangeEvent(@NotNull final MapEvents$LocationFix locationFix) {
        Intrinsics.checkParameterIsNotNull(locationFix, "event");
        if (!locationFix.locationAvailable) {
            this.headingDataUtil.reset();
            this.smartDashWidgetManager.updateWidget(COMPASS_WIDGET_ID, 0);
        }
    }

    @Subscribe
    public final void onMapEvent(@NotNull final MapEvents$ManeuverDisplay maneuverDisplay) {
        Intrinsics.checkParameterIsNotNull(maneuverDisplay, "event");
        this.smartDashWidgetManager.updateWidget(SPEED_LIMIT_SIGN_GAUGE_ID, Math.round(SpeedManager.convert(maneuverDisplay.currentSpeedLimit, SpeedManager.SpeedUnit.METERS_PER_SECOND, SpeedManager.getInstance().getSpeedUnit())));
    }

    @Override
    public void onScrollIdle() {
    }

    @Override
    public void onUnload(@NotNull final MenuLevel menuLevel) {
        Intrinsics.checkParameterIsNotNull(menuLevel, "level");
        final DashboardWidgetView gaugeView = this.gaugeView;
        Object tag;
        tag = gaugeView.getTag();
        Object o = tag;
        if (!(tag instanceof DashboardWidgetPresenter)) {
            o = null;
        }
        final DashboardWidgetPresenter dashboardWidgetPresenter = (DashboardWidgetPresenter)o;
        if (dashboardWidgetPresenter != null) {
            dashboardWidgetPresenter.setView(null, null);
        }
        if (this.userPreferenceChanged) {
            DashGaugeConfiguratorMenu.Companion.getSLogger().d("User preference has changed " + true);
            this.bus.post(new SmartDashWidgetManager.UserPreferenceChanged());
        }
        else {
            DashGaugeConfiguratorMenu.Companion.getSLogger().d("User preference has not been changed " + false);
        }
        if (this.registered) {
            this.bus.unregister(this);
            this.registered = false;
        }
    }

    @Override
    public boolean selectItem(@NotNull VerticalList.ItemSelectionState selection) {
        Intrinsics.checkParameterIsNotNull(selection, "selection");
        Companion.getSLogger().v("select id:" + selection.id + " pos:" + selection.pos);
        switch (this.gaugeType) {
            case CENTER:
                SmartDashView smartDashView;
                switch (selection.id) {
                    case R.id.main_menu_options_speedometer:
                        this.presenter.close();
                        smartDashView = Companion.getUiStateManager().getSmartDashView();
                        if (smartDashView != null) {
                            smartDashView.onSpeedoMeterSelected();
                            break;
                        }
                        break;
                    case R.id.main_menu_options_tachometer:
                        this.presenter.close();
                        smartDashView = Companion.getUiStateManager().getSmartDashView();
                        if (smartDashView != null) {
                            smartDashView.onTachoMeterSelected();
                            break;
                        }
                        break;
                    case R.id.menu_back:
                        this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                        break;
                    default:
                        break;
                }
                break;
            case SIDE:
                switch (selection.id) {
                    case R.id.menu_back:
                        this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                        break;
                    default:
                        VerticalList.Model model = selection.model;
                        int id = selection.id;
                        SmartDashWidgetManager.SmartDashWidgetCache smartDashWidgetCache = this.smartDashWidgetCache;
                        DashboardWidgetPresenter widgetPresenter = smartDashWidgetCache != null ? smartDashWidgetCache.getWidgetPresenter(id) : null;
                        SmartDashWidgetManager smartDashWidgetManager;
                        String widgetIdentifier;
                        if (model.isOn) {
                            model.isOn = false;
                            model.subTitle = Companion.getOffLabel();
                            smartDashWidgetManager = this.smartDashWidgetManager;
                            widgetIdentifier = "";
                            if (widgetPresenter != null) {
                                widgetIdentifier = widgetPresenter.getWidgetIdentifier();
                            }
                            smartDashWidgetManager.setGaugeOn(widgetIdentifier, false);
                        } else {
                            model.isOn = true;
                            model.subTitle = Companion.getOnLabel();
                            smartDashWidgetManager = this.smartDashWidgetManager;
                            widgetIdentifier = "";
                            if (widgetPresenter != null) {
                                widgetIdentifier = widgetPresenter.getWidgetIdentifier();
                            }
                            smartDashWidgetManager.setGaugeOn(widgetIdentifier, true);
                        }
                        this.userPreferenceChanged = true;
                        this.presenter.refreshDataforPos(selection.pos);
                        break;
                }
        }
        return false;
    }


    @Override
    public void setBackSelectionId(final int backSelectionId) {
        this.backSelectionId = backSelectionId;
    }

    @Override
    public void setBackSelectionPos(final int backSelection) {
        this.backSelection = backSelection;
    }

    public final void setGaugeType(@NotNull final GaugeType gaugeType) {
        Intrinsics.checkParameterIsNotNull(gaugeType, "value");
        this.gaugeType = gaugeType;
        if (Intrinsics.areEqual(gaugeType, GaugeType.SIDE)) {
            if (!this.registered) {
                this.bus.register(this);
                this.registered = true;
            }
        }
        else if (this.registered) {
            this.bus.unregister(this);
            this.registered = false;
        }
    }

    public final void setGaugeView(@NotNull final DashboardWidgetView gaugeView) {
        Intrinsics.checkParameterIsNotNull(gaugeView, "set");
        this.gaugeView = gaugeView;
    }

    public final void setHeadingDataUtil(@NotNull final HeadingDataUtil headingDataUtil) {
        Intrinsics.checkParameterIsNotNull(headingDataUtil, "set");
        this.headingDataUtil = headingDataUtil;
    }

    public final void setRegistered(final boolean registered) {
        this.registered = registered;
    }

    @Override
    public void setSelectedIcon() {
        switch (this.gaugeType) {
            case CENTER:
                this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_center_gauge, Companion.getCenterGaugeBackgroundColor(), null, 1.0f);
                this.vscrollComponent.selectedText.setText(Companion.getCenterGaugeTitle());
                return;
            case SIDE:
                this.vscrollComponent.showSelectedCustomView();
                this.vscrollComponent.selectedText.setText(null);
                if (this.smartDashWidgetCache == null) {
                    getItems();
                }
                showGauge(1);
                return;
            default:
        }
    }

    private void setSmartDashWidgetCache(@Nullable final SmartDashWidgetManager.SmartDashWidgetCache smartDashWidgetCache) {
        this.smartDashWidgetCache = smartDashWidgetCache;
    }

    public final void setUserPreferenceChanged(final boolean userPreferenceChanged) {
        this.userPreferenceChanged = userPreferenceChanged;
    }

    private void showGauge(final int n) {
        if (n > 0) {
            this.vscrollComponent.showSelectedCustomView();
            this.vscrollComponent.selectedText.setText(null);
            Object tag;
            if (!((tag = this.gaugeView.getTag()) instanceof DashboardWidgetPresenter)) {
                tag = null;
            }
            final DashboardWidgetPresenter dashboardWidgetPresenter = (DashboardWidgetPresenter)tag;
            final SmartDashWidgetManager.SmartDashWidgetCache smartDashWidgetCache = this.smartDashWidgetCache;
            DashboardWidgetPresenter widgetPresenter;
            if (smartDashWidgetCache != null) {
                widgetPresenter = smartDashWidgetCache.getWidgetPresenter(n - 1);
            }
            else {
                widgetPresenter = null;
            }
            if (dashboardWidgetPresenter != null && dashboardWidgetPresenter.hashCode() != this.presenter.hashCode() && dashboardWidgetPresenter.getWidgetView() == this.gaugeView) {
                dashboardWidgetPresenter.setView(null, null);
            }
            if (widgetPresenter != null) {
                final Bundle bundle = new Bundle();
                bundle.putInt("EXTRA_GRAVITY", 0);
                bundle.putBoolean("EXTRA_IS_ACTIVE", true);
                widgetPresenter.setView(this.gaugeView, bundle);
                widgetPresenter.setWidgetVisibleToUser(true);
            }
            this.gaugeView.setTag(widgetPresenter);
        }
        else {
            this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_side_gauges, DashGaugeConfiguratorMenu.Companion.getCenterGaugeBackgroundColor(), null, 1.0f);
            this.vscrollComponent.selectedText.setText(DashGaugeConfiguratorMenu.Companion.getSideGaugesTitle());
        }
    }

    @Override
    public void showToolTip() {
    }

    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\bX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\nR\u0014\u0010\r\u001a\u00020\bX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\nR\u001a\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00040\u0010X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u0014X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0014\u0010\u0017\u001a\u00020\u0014X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0016R\u0014\u0010\u0019\u001a\u00020\u0014X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0016R\u0014\u0010\u001b\u001a\u00020\u001cX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u001a\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00040\u0010X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b \u0010\u0012R\u0014\u0010!\u001a\u00020\u0014X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u0016R\u0014\u0010#\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b$\u0010\u0006R\u0014\u0010%\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b&\u0010\u0006R\u0014\u0010'\u001a\u00020(X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b)\u0010*R\u0014\u0010+\u001a\u00020\u0014X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b,\u0010\u0016¨\u0006-" }, d2 = { "Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;", "", "()V", "back", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "getBack", "()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "backColor", "", "getBackColor", "()I", "bkColorUnselected", "getBkColorUnselected", "centerGaugeBackgroundColor", "getCenterGaugeBackgroundColor", "centerGaugeOptionsList", "Ljava/util/ArrayList;", "getCenterGaugeOptionsList", "()Ljava/util/ArrayList;", "centerGaugeTitle", "", "getCenterGaugeTitle", "()Ljava/lang/String;", "offLabel", "getOffLabel", "onLabel", "getOnLabel", "sLogger", "Lcom/navdy/service/library/log/Logger;", "getSLogger", "()Lcom/navdy/service/library/log/Logger;", "sideGaugesOptionsList", "getSideGaugesOptionsList", "sideGaugesTitle", "getSideGaugesTitle", "speedoMeter", "getSpeedoMeter", "tachoMeter", "getTachoMeter", "uiStateManager", "Lcom/navdy/hud/app/ui/framework/UIStateManager;", "getUiStateManager", "()Lcom/navdy/hud/app/ui/framework/UIStateManager;", "unavailableLabel", "getUnavailableLabel", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    public static final class Companion
    {
        private VerticalList.Model getBack() {
            return DashGaugeConfiguratorMenu.access$getBack$cp();
        }

        private  int getBackColor() {
            return DashGaugeConfiguratorMenu.access$getBackColor$cp();
        }

        private  int getBkColorUnselected() {
            return DashGaugeConfiguratorMenu.access$getBkColorUnselected$cp();
        }

        private  int getCenterGaugeBackgroundColor() {
            return DashGaugeConfiguratorMenu.access$getCenterGaugeBackgroundColor$cp();
        }

        private  ArrayList<VerticalList.Model> getCenterGaugeOptionsList() {
            return (ArrayList<VerticalList.Model>)DashGaugeConfiguratorMenu.access$getCenterGaugeOptionsList$cp();
        }

        private  String getCenterGaugeTitle() {
            return DashGaugeConfiguratorMenu.access$getCenterGaugeTitle$cp();
        }

        private  String getOffLabel() {
            return DashGaugeConfiguratorMenu.access$getOffLabel$cp();
        }

        private  String getOnLabel() {
            return DashGaugeConfiguratorMenu.access$getOnLabel$cp();
        }

        private  Logger getSLogger() {
            return DashGaugeConfiguratorMenu.access$getSLogger$cp();
        }

        private ArrayList<VerticalList.Model> getSideGaugesOptionsList() {
            return (ArrayList<VerticalList.Model>)DashGaugeConfiguratorMenu.access$getSideGaugesOptionsList$cp();
        }

        private  String getSideGaugesTitle() {
            return DashGaugeConfiguratorMenu.access$getSideGaugesTitle$cp();
        }

        private  VerticalList.Model getSpeedoMeter() {
            return DashGaugeConfiguratorMenu.access$getSpeedoMeter$cp();
        }

        private  VerticalList.Model getTachoMeter() {
            return DashGaugeConfiguratorMenu.access$getTachoMeter$cp();
        }

        private  UIStateManager getUiStateManager() {
            return DashGaugeConfiguratorMenu.access$getUiStateManager$cp();
        }

        private  String getUnavailableLabel() {
            return DashGaugeConfiguratorMenu.access$getUnavailableLabel$cp();
        }
    }

    //    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005" }, d2 = { "Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;", "", "(Ljava/lang/String;I)V", "CENTER", "SIDE", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
//    public enum GaugeType
//    {
//        CENTER,
//        SIDE;
//    }
    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005" }, d2 = { "Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;", "", "(Ljava/lang/String;I)V", "CENTER", "SIDE", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    public enum GaugeType {
        CENTER(0),
        SIDE(1);

        private int value;
        GaugeType(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }

    }
}
