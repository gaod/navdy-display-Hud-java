package com.navdy.hud.app.ui.component.vlist.viewholder;

import com.navdy.hud.app.R;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.View;
import android.animation.PropertyValuesHolder;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import android.graphics.Shader;
import com.navdy.hud.app.ui.component.vlist.VerticalModelCache;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import android.os.Handler;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import android.view.ViewGroup;
import com.navdy.service.library.log.Logger;

public class IconBkColorViewHolder extends IconBaseViewHolder
{
    private static final Logger sLogger;
    
    static {
        sLogger = VerticalViewHolder.sLogger;
    }
    
    private IconBkColorViewHolder(final ViewGroup viewGroup, final VerticalList list, final Handler handler) {
        super(viewGroup, list, handler);
    }
    
    public static VerticalList.Model buildModel(final int n, final int n2, final int n3, final int n4, final int n5, final String s, final String s2) {
        return buildModel(n, n2, n3, n4, n5, s, s2, null, IconColorImageView.IconShape.CIRCLE);
    }
    
    public static VerticalList.Model buildModel(final int n, final int n2, final int n3, final int n4, final int n5, final String s, final String s2, final String s3) {
        return buildModel(n, n2, n3, n4, n5, s, s2, s3, IconColorImageView.IconShape.CIRCLE);
    }
    
    public static VerticalList.Model buildModel(final int id, final int icon, final int iconSelectedColor, final int iconDeselectedColor, final int iconFluctuatorColor, final String title, final String subTitle, final String subTitle2, final IconColorImageView.IconShape iconShape) {
        VerticalList.Model fromCache;
        if ((fromCache = VerticalModelCache.getFromCache(VerticalList.ModelType.ICON_BKCOLOR)) == null) {
            fromCache = new VerticalList.Model();
        }
        fromCache.type = VerticalList.ModelType.ICON_BKCOLOR;
        fromCache.id = id;
        fromCache.icon = icon;
        fromCache.iconSelectedColor = iconSelectedColor;
        fromCache.iconDeselectedColor = iconDeselectedColor;
        fromCache.iconFluctuatorColor = iconFluctuatorColor;
        fromCache.title = title;
        fromCache.subTitle = subTitle;
        fromCache.subTitle2 = subTitle2;
        fromCache.iconShape = iconShape;
        return fromCache;
    }
    
    public static IconBkColorViewHolder buildViewHolder(final ViewGroup viewGroup, final VerticalList list, final Handler handler) {
        return new IconBkColorViewHolder(IconBaseViewHolder.getLayout(viewGroup, R.layout.vlist_item, R.layout.crossfade_image_bkcolor_lyt), list, handler);
    }
    
    private void setIcon(final int n, final int n2, final int n3, final boolean b, final boolean b2) {
        final IconColorImageView iconColorImageView = (IconColorImageView)this.crossFadeImageView.getBig();
        if (b) {
            iconColorImageView.setDraw(true);
            iconColorImageView.setIcon(n, n2, null, 0.83f);
        }
        else {
            iconColorImageView.setDraw(false);
        }
        final IconColorImageView iconColorImageView2 = (IconColorImageView)this.crossFadeImageView.getSmall();
        if (b2) {
            iconColorImageView2.setDraw(true);
            iconColorImageView2.setIcon(n, n3, null, 0.83f);
        }
        else {
            iconColorImageView2.setDraw(false);
        }
    }
    
    @Override
    public void bind(final VerticalList.Model model, final VerticalList.ModelState modelState) {
        super.bind(model, modelState);
        this.setIcon(model.icon, model.iconSelectedColor, model.iconDeselectedColor, modelState.updateImage, modelState.updateSmallImage);
    }
    
    @Override
    public VerticalList.ModelType getModelType() {
        return VerticalList.ModelType.ICON_BKCOLOR;
    }
    
    @Override
    public void preBind(final VerticalList.Model model, final VerticalList.ModelState modelState) {
        super.preBind(model, modelState);
        ((IconColorImageView)this.crossFadeImageView.getBig()).setIconShape(model.iconShape);
        ((IconColorImageView)this.crossFadeImageView.getSmall()).setIconShape(model.iconShape);
    }
    
    @Override
    public void setItemState(final State state, final AnimationType animationType, final int n, final boolean b) {
        super.setItemState(state, animationType, n, b);
        float n2 = 0.0f;
        final CrossFadeImageView.Mode mode = null;
        CrossFadeImageView.Mode mode2 = null;
        switch (state) {
            default:
                mode2 = mode;
                break;
            case SELECTED:
                n2 = 1.0f;
                mode2 = CrossFadeImageView.Mode.BIG;
                break;
            case UNSELECTED:
                n2 = 0.6f;
                mode2 = CrossFadeImageView.Mode.SMALL;
                break;
        }
        switch (animationType) {
            case NONE:
            case INIT:
                this.imageContainer.setScaleX(n2);
                this.imageContainer.setScaleY(n2);
                this.crossFadeImageView.setMode(mode2);
                break;
            case MOVE:
                this.animatorSetBuilder.with((Animator)ObjectAnimator.ofPropertyValuesHolder(this.imageContainer, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.SCALE_X, new float[] { n2 }), PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[] { n2 }) }));
                if (this.crossFadeImageView.getMode() != mode2) {
                    this.animatorSetBuilder.with((Animator)this.crossFadeImageView.getCrossFadeAnimator());
                    break;
                }
                break;
        }
    }
}
