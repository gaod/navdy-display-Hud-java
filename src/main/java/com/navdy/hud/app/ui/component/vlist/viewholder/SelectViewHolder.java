package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.VerticalModelCache;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

import static android.view.View.INVISIBLE;

public class SelectViewHolder extends SwitchViewHolder {

    public SelectViewHolder(@NotNull final ViewGroup viewGroup, @NotNull final VerticalList list, @NotNull final Handler handler) {
        super(viewGroup, list, handler);
    }

    @NotNull
    @Override
    public VerticalList.ModelType getModelType() {
        return VerticalList.ModelType.SELECT;
    }

    @NotNull
    public static VerticalList.Model buildModel(final int id, final int iconFluctuatorColor, @NotNull final String title, @NotNull final String subTitle, @NotNull final String subTitle2, final boolean isEnabled, String[] options) {
        Intrinsics.checkParameterIsNotNull(title, "title");
        Intrinsics.checkParameterIsNotNull(subTitle, "subTitle");
        VerticalList.Model fromCache;
        if ((fromCache = VerticalModelCache.getFromCache(VerticalList.ModelType.SELECT)) == null) {
            fromCache = new VerticalList.Model();
        }
        fromCache.type = VerticalList.ModelType.SELECT;
        fromCache.id = id;
        fromCache.iconFluctuatorColor = iconFluctuatorColor;
        fromCache.title = title;
        fromCache.subTitle = subTitle;
        fromCache.subTitle2 = subTitle2;
        fromCache.isOn = true;
        fromCache.isEnabled = isEnabled;
        fromCache.extras = new HashMap<>();
        int index = 0;
        for (String option : options) {
            fromCache.extras.put(String.valueOf(index), option);
            index += 1;
        }
        return fromCache;
    }

    @NotNull
    public static SelectViewHolder buildViewHolder(@NotNull final ViewGroup viewGroup, @NotNull final VerticalList list, @NotNull final Handler handler) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "parent");
        Intrinsics.checkParameterIsNotNull(list, "vlist");
        Intrinsics.checkParameterIsNotNull(handler, "handler");
        return new SelectViewHolder(SelectViewHolder.getLayout(viewGroup, R.layout.vlist_toggle_switch), list, handler);
    }

    @NotNull
    public static ViewGroup getLayout(@NotNull final ViewGroup viewGroup, final int n) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "parent");
        final View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(n, viewGroup, false);
        if (inflate == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup");
        }
        return (ViewGroup)inflate;
    }

    @Override
    public void select(@NotNull final VerticalList.Model model, final int n, final int n2) {
        Intrinsics.checkParameterIsNotNull(model, "model");
//        if (this.hasIconFluctuatorColor) {
//            this.haloView.setVisibility(INVISIBLE);
//            this.haloView.stop();
//        }
        if (!this.isEnabled) {
            this.vlist.unlock();
            return;
        }

//        final AnimatorSet set = new AnimatorSet();
//        final ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(this.switchContainer, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.SCALE_X, new float[] { 1.0f, 0.8f }), PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[] { 1.0f, 0.8f }) });
//        ofPropertyValuesHolder.setDuration((long)n2);
//        final ObjectAnimator ofPropertyValuesHolder2 = ObjectAnimator.ofPropertyValuesHolder(this.switchContainer, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.SCALE_X, new float[] { 0.8f, 1.0f }), PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[] { 0.8f, 1.0f }) });
//        ofPropertyValuesHolder2.setDuration((long)n2);
//        final AnimatorSet set2 = new AnimatorSet();
//        set2.playSequentially(new Animator[] { (Animator)ofPropertyValuesHolder, (Animator)ofPropertyValuesHolder2 });
//        final AnimatorSet set3 = new AnimatorSet();
//        final Drawable background = this.switchBackground.getBackground();
//        if (background == null) {
//            throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
//        }
//        final Drawable drawable = ((LayerDrawable)background).getDrawable(1);
//        if (drawable == null) {
//            throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.ClipDrawable");
//        }
//        final ClipDrawable clipDrawable = (ClipDrawable)drawable;
//        final ViewGroup.LayoutParams layoutParams = this.switchThumb.getLayoutParams();
//        if (layoutParams == null) {
//            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
//        }
//        final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup.MarginLayoutParams)layoutParams;
//        final ValueAnimator valueAnimator = null;
//        final ValueAnimator valueAnimator2 = null;
//        ValueAnimator valueAnimator3;
//        ValueAnimator valueAnimator4;
//        if (this.isOn) {
//            valueAnimator3 = ValueAnimator.ofFloat(new float[] { this.thumbMargin, 0.0f });
//            valueAnimator3.setDuration(100L);
//            valueAnimator4 = ValueAnimator.ofFloat(new float[] { 5000.0f, 1000.0f });
//            set2.setDuration(100L);
//        }
//        else {
//            valueAnimator3 = ValueAnimator.ofFloat(new float[] { 0.0f, this.thumbMargin });
//            valueAnimator3.setDuration(100L);
//            valueAnimator4 = ValueAnimator.ofFloat(new float[] { 3000.0f, 7000.0f });
//            set2.setDuration(100L);
//        }
//        if (valueAnimator4 != null) {
//            valueAnimator4.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new SwitchViewHolder$select$1(this, clipDrawable));
//        }
//        if (valueAnimator3 != null) {
//            valueAnimator3.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new SwitchViewHolder$select$2(this, viewGroup$MarginLayoutParams));
//        }
//        set3.playTogether(new Animator[] { (Animator)valueAnimator3, (Animator)valueAnimator4 });
//        set.play((Animator)set3).after((Animator)set2);
//        set.addListener((Animator.AnimatorListener)new SwitchViewHolder$select$3(this, model, n));
//        set.start();

        int idx = 0;
        for (Map.Entry<String, String> entry : model.extras.entrySet()) {
//        for (String option : model.extras.keySet()) {

            if (entry.getValue().equals(model.subTitle2)) {
                idx = Integer.parseInt(entry.getKey());
                break;
            }
        }
        idx += 1;
        if (idx >= model.extras.size()) {
            idx = 0;
        }
        model.subTitle2 = model.extras.get(String.valueOf(idx));
        this.setSubTitle2(model.subTitle2, false);

        VerticalList.ItemSelectionState selectionState = this.vlist.getItemSelectionState();
        selectionState.set(model, model.id, n, -1, -1);
        this.vlist.performSelectAction(selectionState);
        this.vlist.unlock();
    }

    public static String selection(@NotNull final VerticalList.Model model) {
        return model.subTitle2;
    }



}
