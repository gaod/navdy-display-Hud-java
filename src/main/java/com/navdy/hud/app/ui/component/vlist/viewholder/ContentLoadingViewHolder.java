package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.View;
import android.animation.PropertyValuesHolder;
import android.animation.AnimatorSet;
import android.widget.TextView;
import android.graphics.Shader;

import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import android.view.LayoutInflater;
import com.navdy.hud.app.ui.component.vlist.VerticalModelCache;
import android.animation.Animator;
import android.os.Handler;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import android.widget.ImageView;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.graphics.drawable.AnimationDrawable;
import android.view.ViewGroup;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import android.animation.AnimatorSet;
import com.navdy.service.library.log.Logger;

import static android.view.View.GONE;

public class ContentLoadingViewHolder extends VerticalViewHolder
{
    private static final Logger sLogger;
    private AnimatorSet.Builder animatorSetBuilder;
    private CrossFadeImageView crossFadeImageView;
    private ViewGroup iconContainer;
    private ViewGroup imageContainer;
    private AnimationDrawable loopAnimation;
    private DefaultAnimationListener loopEnd;
    private ImageView loopImageView;
    private DefaultAnimationListener loopStart;
    private ImageView unSelImageView;
    
    static {
        sLogger = new Logger(ContentLoadingViewHolder.class);
    }
    
    private ContentLoadingViewHolder(final ViewGroup viewGroup, final VerticalList list, final Handler handler) {
        super(viewGroup, list, handler);
        this.loopStart = new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                ContentLoadingViewHolder.this.unSelImageView.setVisibility(GONE);
                if (ContentLoadingViewHolder.this.loopAnimation != null && !ContentLoadingViewHolder.this.loopAnimation.isRunning()) {
                    ContentLoadingViewHolder.this.loopAnimation.start();
                }
            }
            
            @Override
            public void onAnimationStart(final Animator animator) {
                ContentLoadingViewHolder.this.loopImageView.setVisibility(View.VISIBLE);
            }
        };
        this.loopEnd = new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                ContentLoadingViewHolder.this.loopImageView.setVisibility(GONE);
            }
            
            @Override
            public void onAnimationStart(final Animator animator) {
                ContentLoadingViewHolder.this.unSelImageView.setVisibility(View.VISIBLE);
                if (ContentLoadingViewHolder.this.loopAnimation != null && ContentLoadingViewHolder.this.loopAnimation.isRunning()) {
                    ContentLoadingViewHolder.this.loopAnimation.stop();
                }
            }
        };
        this.imageContainer = (ViewGroup)viewGroup.findViewById(R.id.imageContainer);
        this.iconContainer = (ViewGroup)viewGroup.findViewById(R.id.iconContainer);
        this.crossFadeImageView = (CrossFadeImageView)viewGroup.findViewById(R.id.vlist_image);
        this.loopImageView = (ImageView)viewGroup.findViewById(R.id.img_loop);
        this.unSelImageView = (ImageView)viewGroup.findViewById(R.id.img);
        this.loopAnimation = (AnimationDrawable)this.loopImageView.getDrawable();
    }
    
    public static VerticalList.Model buildModel(final int icon, final int iconSelectedColor, final int iconDeselectedColor) {
        VerticalList.Model fromCache;
        if ((fromCache = VerticalModelCache.getFromCache(VerticalList.ModelType.LOADING_CONTENT)) == null) {
            fromCache = new VerticalList.Model();
        }
        fromCache.type = VerticalList.ModelType.LOADING_CONTENT;
        fromCache.id = R.id.vlist_content_loading;
        fromCache.icon = icon;
        fromCache.iconSelectedColor = iconSelectedColor;
        fromCache.iconDeselectedColor = iconDeselectedColor;
        return fromCache;
    }
    
    public static ContentLoadingViewHolder buildViewHolder(final ViewGroup viewGroup, final VerticalList list, final Handler handler) {
        final ViewGroup viewGroup2 = (ViewGroup)LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.vlist_content_loading, viewGroup, false);
        final CrossFadeImageView crossFadeImageView = (CrossFadeImageView)((ViewGroup)viewGroup2.findViewById(R.id.iconContainer)).findViewById(R.id.crossFadeImageView);
        crossFadeImageView.inject(CrossFadeImageView.Mode.SMALL);
        crossFadeImageView.setId(R.id.vlist_image);
        return new ContentLoadingViewHolder(viewGroup2, list, handler);
    }
    
    private void setIcon(final int n, final int n2, final int n3) {
        final IconColorImageView iconColorImageView = (IconColorImageView)this.crossFadeImageView.getBig();
        iconColorImageView.setDraw(true);
        iconColorImageView.setIcon(n, n2, null, 0.83f);
        final IconColorImageView iconColorImageView2 = (IconColorImageView)this.crossFadeImageView.getSmall();
        iconColorImageView2.setDraw(true);
        iconColorImageView2.setIcon(n, n3, null, 0.83f);
    }
    
    @Override
    public void bind(final VerticalList.Model model, final VerticalList.ModelState modelState) {
        this.setIcon(model.icon, model.iconSelectedColor, model.iconDeselectedColor);
    }
    
    @Override
    public void clearAnimation() {
        if (this.loopAnimation != null && this.loopAnimation.isRunning()) {
            this.loopAnimation.stop();
        }
        this.stopAnimation();
        this.currentState = null;
        this.layout.setVisibility(View.VISIBLE);
        this.layout.setAlpha(1.0f);
    }
    
    @Override
    public void copyAndPosition(final ImageView imageView, final TextView textView, final TextView textView2, final TextView textView3, final boolean b) {
    }
    
    @Override
    public VerticalList.ModelType getModelType() {
        return VerticalList.ModelType.LOADING_CONTENT;
    }
    
    @Override
    public void preBind(final VerticalList.Model model, final VerticalList.ModelState modelState) {
        this.crossFadeImageView.setSmallAlpha(-1.0f);
        ((IconColorImageView)this.crossFadeImageView.getBig()).setIconShape(model.iconShape);
        ((IconColorImageView)this.crossFadeImageView.getSmall()).setIconShape(model.iconShape);
    }
    
    @Override
    public void select(final VerticalList.Model model, final int n, final int n2) {
    }
    
    @Override
    public void setItemState(final State state, final AnimationType animationType, final int n, final boolean b) {
        float n2 = 0.0f;
        CrossFadeImageView.Mode mode = null;
        this.animatorSetBuilder = null;
        switch (state) {
            case SELECTED:
                n2 = 1.0f;
                mode = CrossFadeImageView.Mode.BIG;
                break;
            case UNSELECTED:
                n2 = 0.6f;
                mode = CrossFadeImageView.Mode.SMALL;
                break;
        }
        switch (animationType) {
            case NONE:
            case INIT:
                this.imageContainer.setScaleX(n2);
                this.imageContainer.setScaleY(n2);
                this.crossFadeImageView.setMode(mode);
                if (state == State.SELECTED) {
                    this.loopImageView.setVisibility(View.VISIBLE);
                    this.loopImageView.setAlpha(1.0f);
                    this.unSelImageView.setVisibility(GONE);
                    this.unSelImageView.setAlpha(0.0f);
                    if (this.loopAnimation != null && !this.loopAnimation.isRunning()) {
                        this.loopAnimation.start();
                        break;
                    }
                    break;
                }
                else {
                    this.loopImageView.setVisibility(GONE);
                    this.loopImageView.setAlpha(0.0f);
                    this.unSelImageView.setVisibility(View.VISIBLE);
                    this.unSelImageView.setAlpha(1.0f);
                    if (this.loopAnimation != null && this.loopAnimation.isRunning()) {
                        this.loopAnimation.stop();
                    }
                }
                break;
            case MOVE:
                this.itemAnimatorSet = new AnimatorSet();
                this.animatorSetBuilder = this.itemAnimatorSet.play((Animator)ObjectAnimator.ofPropertyValuesHolder(this.imageContainer, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.SCALE_X, new float[] { n2 }), PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[] { n2 }) }));
                if (this.crossFadeImageView.getMode() != mode) {
                    this.animatorSetBuilder.with((Animator)this.crossFadeImageView.getCrossFadeAnimator());
                }
                if (state == State.SELECTED) {
                    this.itemAnimatorSet.addListener((Animator.AnimatorListener)this.loopStart);
                    this.animatorSetBuilder.with((Animator)ObjectAnimator.ofFloat(this.loopImageView, View.ALPHA, new float[] { 1.0f }));
                    this.animatorSetBuilder.with((Animator)ObjectAnimator.ofFloat(this.unSelImageView, View.ALPHA, new float[] { 0.0f }));
                    break;
                }
                this.itemAnimatorSet.addListener((Animator.AnimatorListener)this.loopEnd);
                this.animatorSetBuilder.with((Animator)ObjectAnimator.ofFloat(this.unSelImageView, View.ALPHA, new float[] { 1.0f }));
                this.animatorSetBuilder.with((Animator)ObjectAnimator.ofFloat(this.loopImageView, View.ALPHA, new float[] { 0.0f }));
                break;
        }
    }
}
