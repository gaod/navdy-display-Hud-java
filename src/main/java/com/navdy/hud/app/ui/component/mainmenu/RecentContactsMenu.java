package com.navdy.hud.app.ui.component.mainmenu;

import android.graphics.Shader;

import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.contacts.Contact;
import com.navdy.hud.app.ui.component.vlist.VerticalModelCache;

import android.text.TextUtils;
import android.view.View;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import java.util.Iterator;
import com.navdy.hud.app.framework.recentcall.RecentCallManager;
import java.util.ArrayList;
import com.navdy.hud.app.framework.recentcall.RecentCall;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import java.util.List;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import android.content.res.Resources;
import com.navdy.hud.app.ui.component.vlist.VerticalList;

public class RecentContactsMenu implements IMenu
{
    private static final Resources resources = HudApplication.getAppContext().getResources();
    private static final VerticalList.Model back;
    private static final int backColor = resources.getColor(R.color.mm_back);
    private static final int contactColor = resources.getColor(R.color.mm_contacts);
    private static final String contactStr = resources.getString(R.string.carousel_menu_recent_contacts_title);
    private static final int noContactColor = resources.getColor(R.color.icon_user_bg_4);
    private static final Logger sLogger = new Logger(RecentContactsMenu.class);
    private int backSelection;
    private Bus bus;
    private List<VerticalList.Model> cachedList;
    private IMenu parent;
    private MainMenuScreen2.Presenter presenter;
    private List<VerticalList.Model> returnToCacheList;
    private VerticalMenuComponent vscrollComponent;
    
    static {
        String title = resources.getString(R.string.back);
        int fluctuatorColor = backColor;
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
    }
    
    public RecentContactsMenu(final Bus bus, final VerticalMenuComponent vscrollComponent, final MainMenuScreen2.Presenter presenter, final IMenu parent) {
        this.bus = bus;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
    }
    
    private VerticalList.Model buildModel(final RecentCall recentCall, final int n) {
        String numberStr = (TextUtils.isEmpty(recentCall.formattedNumber)) ? recentCall.number:recentCall.formattedNumber;
        return ((ContactsMenu)this.parent).buildModel(n, recentCall.name, numberStr, recentCall.numberTypeStr, recentCall.defaultImageIndex, recentCall.numberType, recentCall.initials);
    }
    
    @Override
    public IMenu getChildMenu(final IMenu menu, final String s, final String s2) {
        return null;
    }
    
    @Override
    public int getInitialSelection() {
        int n = 1;
        if (this.cachedList == null || this.cachedList.size() <= 1) {
            n = 0;
        }
        return n;
    }
    
    @Override
    public List<VerticalList.Model> getItems() {
        List<VerticalList.Model> cachedList;
        if (this.cachedList != null) {
            cachedList = this.cachedList;
        }
        else {
            cachedList = new ArrayList<>();
            this.returnToCacheList = new ArrayList<>();
            cachedList.add(RecentContactsMenu.back);
            int n = 0;
            final List<RecentCall> recentCalls = RecentCallManager.getInstance().getRecentCalls();
            if (recentCalls != null) {
                RecentContactsMenu.sLogger.v("recent contacts:" + recentCalls.size());
                for (final RecentCall state : recentCalls) {
                    final VerticalList.Model buildModel = this.buildModel(state, n);
                    buildModel.state = state;
                    cachedList.add(buildModel);
                    this.returnToCacheList.add(buildModel);
                    ++n;
                }
            }
            else {
                RecentContactsMenu.sLogger.v("no recent contacts");
            }
            this.cachedList = cachedList;
        }
        return cachedList;
    }
    
    @Override
    public VerticalList.Model getModelfromPos(final int n) {
        VerticalList.Model model;
        if (this.cachedList != null && this.cachedList.size() > n) {
            model = this.cachedList.get(n);
        }
        else {
            model = null;
        }
        return model;
    }
    
    @Override
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    @Override
    public Menu getType() {
        return Menu.RECENT_CONTACTS;
    }
    
    @Override
    public boolean isBindCallsEnabled() {
        return true;
    }
    
    @Override
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    @Override
    public boolean isItemClickable(final int n, final int n2) {
        return true;
    }
    
    @Override
    public void onBindToView(final VerticalList.Model model, final View view, final int n, final VerticalList.ModelState modelState) {
        this.parent.onBindToView(model, view, n, modelState);
    }
    
    @Override
    public void onFastScrollEnd() {
    }
    
    @Override
    public void onFastScrollStart() {
    }
    
    @Override
    public void onItemSelected(final VerticalList.ItemSelectionState itemSelectionState) {
    }
    
    @Override
    public void onScrollIdle() {
    }
    
    @Override
    public void onUnload(final MenuLevel menuLevel) {
        switch (menuLevel) {
            case CLOSE:
                if (this.returnToCacheList != null) {
                    RecentContactsMenu.sLogger.v("rcm:unload add to cache");
                    VerticalModelCache.addToCache(this.returnToCacheList);
                    this.returnToCacheList = null;
                    break;
                }
                break;
        }
    }
    
    @Override
    public boolean selectItem(final VerticalList.ItemSelectionState itemSelectionState) {
        RecentContactsMenu.sLogger.v("select id:" + itemSelectionState.id + " pos:" + itemSelectionState.pos);
        switch (itemSelectionState.id) {
            default: {
                final RecentCall recentCall = (RecentCall)this.cachedList.get(itemSelectionState.pos).state;
                final Contact contact = new Contact(recentCall.name, recentCall.number, recentCall.numberType, recentCall.defaultImageIndex, recentCall.numericNumber);
                final ArrayList<Contact> list = new ArrayList<>(1);
                list.add(contact);
                this.presenter.loadMenu(new ContactOptionsMenu(list, this.vscrollComponent, this.presenter, this, this.bus), MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
                break;
            }
            case R.id.menu_back:
                RecentContactsMenu.sLogger.v("back");
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                break;
        }
        return true;
    }
    
    @Override
    public void setBackSelectionId(final int n) {
    }
    
    @Override
    public void setBackSelectionPos(final int backSelection) {
        this.backSelection = backSelection;
    }
    
    @Override
    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_place_recent_2, RecentContactsMenu.contactColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText(RecentContactsMenu.contactStr);
    }
    
    @Override
    public void showToolTip() {
    }
}
