package com.navdy.hud.app.ui.component.tbt;

import com.navdy.hud.app.R;
import com.navdy.hud.app.maps.MapEvents;
import android.view.ViewGroup;
import android.view.ViewGroup;
import kotlin.TypeCastException;
import android.animation.AnimatorSet;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.util.AttributeSet;
import org.jetbrains.annotations.NotNull;
import android.content.Context;
import kotlin.jvm.internal.Intrinsics;
import com.navdy.hud.app.HudApplication;
//import kotlin.jvm.internal.DefaultConstructorMarker;
import com.navdy.hud.app.maps.MapEvents$ManeuverDisplay;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState;
import com.navdy.hud.app.view.MainView;
import java.util.HashMap;
import android.content.res.Resources;
import com.navdy.service.library.log.Logger;
import kotlin.Metadata;
import android.widget.ImageView;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u0006\u0010\u0010\u001a\u00020\u0011J\u001a\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\f2\n\u0010\u0014\u001a\u00060\u0015R\u00020\u0016J\u000e\u0010\u0017\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\fJ\u000e\u0010\u0018\u001a\u00020\u00112\u0006\u0010\u0019\u001a\u00020\u001aR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\tX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u001c" }, d2 = { "Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;", "Landroid/widget/ImageView;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "currentMode", "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;", "lastManeuverState", "Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;", "lastTurnIconId", "clear", "", "getCustomAnimator", "mode", "mainBuilder", "Landroid/animation/AnimatorSet.Builder;", "Landroid/animation/AnimatorSet;", "setMode", "updateDisplay", "event", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "Companion", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class TbtDirectionView extends ImageView
{
    public static final Companion Companion;
    private static final int fullWidth;
    private static final Logger logger;
    private static final int mediumWidth;
    private static final Resources resources;
    private HashMap _$_findViewCache;
    private MainView.CustomAnimationMode currentMode;
    private HereManeuverDisplayBuilder$ManeuverState lastManeuverState;
    private int lastTurnIconId;
    
    static {
        Companion = new Companion();
        logger = new Logger("TbtDirectionView");
        final Resources resources2 = HudApplication.getAppContext().getResources();
        Intrinsics.checkExpressionValueIsNotNull(resources2, "HudApplication.getAppContext().resources");
        resources = resources2;
        fullWidth = TbtDirectionView.Companion.getResources().getDimensionPixelSize(R.dimen.tbt_direction_full_w);
        mediumWidth = TbtDirectionView.Companion.getResources().getDimensionPixelSize(R.dimen.tbt_direction_medium_w);
    }
    
    public TbtDirectionView(@NotNull final Context context) {
        super(context);
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.lastTurnIconId = -1;
    }
    
    public TbtDirectionView(@NotNull final Context context, @NotNull final AttributeSet set) {
        super(context, set);
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(set, "attrs");
        this.lastTurnIconId = -1;
    }
    
    public TbtDirectionView(@NotNull final Context context, @NotNull final AttributeSet set, final int n) {
        super(context, set, n);
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(set, "attrs");
        this.lastTurnIconId = -1;
    }
    
    public static final /* synthetic */ int access$getFullWidth$cp() {
        return TbtDirectionView.fullWidth;
    }
    
    @NotNull
    public static final /* synthetic */ Logger access$getLogger$cp() {
        return TbtDirectionView.logger;
    }
    
    public static final /* synthetic */ int access$getMediumWidth$cp() {
        return TbtDirectionView.mediumWidth;
    }
    
    @NotNull
    public static final /* synthetic */ Resources access$getResources$cp() {
        return TbtDirectionView.resources;
    }
    
    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }
    
    public View _$_findCachedViewById(final int n) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View viewById;
        if ((viewById = (View)this._$_findViewCache.get(n)) == null) {
            viewById = this.findViewById(n);
            this._$_findViewCache.put(n, viewById);
        }
        return viewById;
    }
    
    public final void clear() {
        this.lastTurnIconId = -1;
        this.setImageDrawable((Drawable)null);
    }
    
    public final void getCustomAnimator(@NotNull final MainView.CustomAnimationMode customAnimationMode, @NotNull final AnimatorSet.Builder animatorSet$Builder) {
        Intrinsics.checkParameterIsNotNull(customAnimationMode, "mode");
        Intrinsics.checkParameterIsNotNull(animatorSet$Builder, "mainBuilder");
    }
    
    public final void setMode(@NotNull final MainView.CustomAnimationMode currentMode) {
        Intrinsics.checkParameterIsNotNull(currentMode, "mode");
        if (!Intrinsics.areEqual(this.currentMode, currentMode)) {
            final ViewGroup.LayoutParams layoutParams = this.getLayoutParams();
            if (layoutParams == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup.MarginLayoutParams)layoutParams;
            if (Intrinsics.areEqual(currentMode, MainView.CustomAnimationMode.EXPAND)) {
                viewGroup$MarginLayoutParams.width = TbtDirectionView.Companion.getFullWidth();
                viewGroup$MarginLayoutParams.height = TbtDirectionView.Companion.getFullWidth();
            }
            else {
                viewGroup$MarginLayoutParams.width = TbtDirectionView.Companion.getMediumWidth();
                viewGroup$MarginLayoutParams.height = TbtDirectionView.Companion.getMediumWidth();
            }
            this.requestLayout();
            this.currentMode = currentMode;
        }
    }
    
    public final void updateDisplay(@NotNull final MapEvents$ManeuverDisplay maneuverDisplay) {
        Intrinsics.checkParameterIsNotNull(maneuverDisplay, "event");
        if (maneuverDisplay.turnIconId != -1) {
            this.setImageResource(maneuverDisplay.turnIconId);
        }
        else {
            this.setImageDrawable((Drawable)null);
        }
        this.lastManeuverState = maneuverDisplay.maneuverState;
        this.lastTurnIconId = maneuverDisplay.turnIconId;
    }
    
    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006R\u0014\u0010\r\u001a\u00020\u000eX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0011" }, d2 = { "Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;", "", "()V", "fullWidth", "", "getFullWidth", "()I", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "mediumWidth", "getMediumWidth", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    public static final class Companion
    {
        private final int getFullWidth() {
            return TbtDirectionView.access$getFullWidth$cp();
        }
        
        private final Logger getLogger() {
            return TbtDirectionView.access$getLogger$cp();
        }
        
        private final int getMediumWidth() {
            return TbtDirectionView.access$getMediumWidth$cp();
        }
        
        private final Resources getResources() {
            return TbtDirectionView.access$getResources$cp();
        }
    }
}
