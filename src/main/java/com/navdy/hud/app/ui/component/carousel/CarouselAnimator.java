package com.navdy.hud.app.ui.component.carousel;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;
import com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import com.navdy.service.library.log.Logger;

public class CarouselAnimator implements AnimationStrategy {
    private static final Logger sLogger = new Logger(CarouselAnimator.class);

    public AnimatorSet createMiddleLeftViewAnimation(CarouselLayout carousel, Direction direction) {
        View targetPosView;
        float scaleFactor;
        AnimatorSet animatorSet = new AnimatorSet();
        if (direction == Direction.LEFT) {
            targetPosView = carousel.leftView;
        } else {
            targetPosView = carousel.rightView;
        }
        if (carousel.middleLeftView.getScaleX() == 1.0f) {
            scaleFactor = ((float) carousel.sideImageSize) / ((float) carousel.mainImageSize);
        } else {
            scaleFactor = 1.0f;
        }
        ObjectAnimator a1 = ObjectAnimator.ofFloat(carousel.middleLeftView, "scaleX", new float[]{scaleFactor});
        ObjectAnimator a2 = ObjectAnimator.ofFloat(carousel.middleLeftView, "scaleY", new float[]{scaleFactor});
        ObjectAnimator a3 = ObjectAnimator.ofFloat(carousel.middleLeftView, "x", new float[]{targetPosView.getX()});
        ObjectAnimator a4 = ObjectAnimator.ofFloat(carousel.middleLeftView, "y", new float[]{targetPosView.getY()});
        AnimatorSet fade = ((CrossFadeImageView) carousel.middleLeftView).getCrossFadeAnimator();
        carousel.middleLeftView.setPivotX(0.5f);
        carousel.middleLeftView.setPivotY(0.5f);
        animatorSet.playTogether(new Animator[]{a1, a2, a3, a4, fade});
        return animatorSet;
    }

    public AnimatorSet createMiddleRightViewAnimation(CarouselLayout carousel, Direction direction) {
        View targetPosView;
        AnimatorSet animatorSet = new AnimatorSet();
        float scaleFactor = ((float) carousel.sideImageSize) / ((float) carousel.middleRightView.getMeasuredHeight());
        if (direction == Direction.LEFT) {
            targetPosView = carousel.leftView;
        } else {
            targetPosView = carousel.rightView;
        }
        ObjectAnimator a1 = ObjectAnimator.ofFloat(carousel.middleRightView, "scaleX", new float[]{scaleFactor});
        ObjectAnimator a2 = ObjectAnimator.ofFloat(carousel.middleRightView, "scaleY", new float[]{scaleFactor});
        ObjectAnimator a3 = ObjectAnimator.ofFloat(carousel.middleRightView, "x", new float[]{targetPosView.getX() + ((float) ((carousel.mainViewDividerPadding + carousel.sideImageSize) / 2))});
        ObjectAnimator a4 = ObjectAnimator.ofFloat(carousel.middleRightView, "y", new float[]{carousel.middleRightView.getY()});
        ObjectAnimator a5 = ObjectAnimator.ofFloat(carousel.middleRightView, "alpha", new float[]{0.0f});
        animatorSet.playTogether(new Animator[]{a1, a2, a3, a4, a5});
        return animatorSet;
    }

    public AnimatorSet createSideViewToMiddleAnimation(CarouselLayout carousel, Direction direction) {
        View sideImageView;
        float scaleFactor;
        AnimatorSet animatorSet = new AnimatorSet();
        if (direction == Direction.RIGHT) {
            sideImageView = carousel.leftView;
        } else {
            sideImageView = carousel.rightView;
        }
        if (sideImageView.getScaleX() == 1.0f) {
            scaleFactor = ((float) carousel.mainImageSize) / ((float) carousel.sideImageSize);
        } else {
            scaleFactor = 1.0f;
        }
        ObjectAnimator a1 = ObjectAnimator.ofFloat(sideImageView, "scaleX", new float[]{scaleFactor});
        ObjectAnimator a2 = ObjectAnimator.ofFloat(sideImageView, "scaleY", new float[]{scaleFactor});
        ObjectAnimator a3 = ObjectAnimator.ofFloat(sideImageView, "x", new float[]{carousel.middleLeftView.getX()});
        ObjectAnimator a4 = ObjectAnimator.ofFloat(sideImageView, "y", new float[]{carousel.middleLeftView.getY()});
        AnimatorSet fade = ((CrossFadeImageView) sideImageView).getCrossFadeAnimator();
        sideImageView.setPivotX(0.5f);
        sideImageView.setPivotY(0.5f);
        animatorSet.playTogether(new Animator[]{a1, a2, a3, a4, fade});
        return animatorSet;
    }

    public Animator createViewOutAnimation(CarouselLayout carousel, Direction direction) {
        if (direction == Direction.RIGHT) {
            return ObjectAnimator.ofFloat(carousel.rightView, "x", new float[]{carousel.rightView.getX() + ((float) carousel.getMeasuredWidth())});
        }
        return ObjectAnimator.ofFloat(carousel.leftView, "x", new float[]{-(carousel.leftView.getX() + ((float) carousel.leftView.getMeasuredWidth()))});
    }

    public AnimatorSet createHiddenViewAnimation(CarouselLayout carousel, Direction direction) {
        View srcView;
        View targetPosView;
        AnimatorSet animatorSet = new AnimatorSet();
        if (direction == Direction.RIGHT) {
            srcView = carousel.newLeftView;
            targetPosView = carousel.leftView;
        } else {
            srcView = carousel.newRightView;
            targetPosView = carousel.rightView;
        }
        animatorSet.play(ObjectAnimator.ofFloat(srcView, "x", new float[]{targetPosView.getX()}));
        return animatorSet;
    }

    public AnimatorSet createNewMiddleRightViewAnimation(CarouselLayout carousel, Direction direction) {
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator a1 = ObjectAnimator.ofFloat(carousel.newMiddleRightView, "x", new float[]{(carousel.middleLeftView.getX() + ((float) carousel.mainViewDividerPadding)) + ((float) carousel.mainImageSize)});
        if (direction == Direction.RIGHT) {
            ObjectAnimator a2 = ObjectAnimator.ofFloat(carousel.newMiddleRightView, "alpha", new float[]{1.0f});
            animatorSet.playTogether(new Animator[]{a1, a2});
        } else {
            animatorSet.playTogether(new Animator[]{a1});
        }
        return animatorSet;
    }

    public AnimatorSet buildLayoutAnimation(AnimatorListener listener, CarouselLayout carousel, int currentPos, int newPos) {
        return null;
    }
}
