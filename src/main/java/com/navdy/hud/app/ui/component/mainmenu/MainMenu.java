package com.navdy.hud.app.ui.component.mainmenu;

import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.toast.ToastManager$ToastParams;
import com.navdy.hud.app.maps.MapEvents$ArrivalEvent;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen;
import android.graphics.Shader;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import com.navdy.hud.app.screen.WelcomeScreen;
import com.navdy.hud.app.ui.component.UISettings;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.voice.VoiceSearchNotification;
import android.view.View;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.util.RemoteCapabilitiesUtil;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.service.library.events.preferences.DriverProfilePreferences;
import com.navdy.service.library.events.preferences.NotificationPreferences;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils;
import com.navdy.hud.app.framework.toast.IToastCallback;
import com.navdy.hud.app.framework.toast.ToastManager;
import android.os.Bundle;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.phonecall.CallNotification;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HereMapUtil;
import android.text.TextUtils;
import com.navdy.service.library.events.audio.MusicPlaybackState;
import com.navdy.service.library.events.DeviceInfo;
import java.util.Set;
import android.support.annotation.Nullable;
import okio.ByteString;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.HudApplication;
import android.os.Looper;
import java.util.concurrent.TimeUnit;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import java.util.List;
import com.squareup.otto.Bus;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.service.library.log.Logger;
import android.content.res.Resources;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.MusicManager;
import android.os.Handler;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import java.util.HashMap;
import com.navdy.hud.app.framework.phonecall.CallManager;
import java.util.ArrayList;

public class MainMenu implements IMenu
{
    private static final int ACTIVITY_TRAY_UPDATE;
    private static final int ETA_UPDATE_INTERVAL;
    private static final String activeTripTitle;
    private static final ArrayList<Integer> activityTrayIcon;
    private static final ArrayList<Integer> activityTrayIconId;
    private static final ArrayList<Integer> activityTraySelectedColor;
    private static final ArrayList<Integer> activityTrayUnSelectedColor;
    public static final int bkColorUnselected;
    private static final CallManager callManager;
    static final HashMap<String, Menu> childMenus;
    private static final VerticalList.Model connectPhone;
    private static final VerticalList.Model contacts;
    private static final String dashTitle;
    public static final VerticalList.FontInfo defaultFontInfo;
    private static final int glancesColor;
    private static final Handler handler;
    private static final String mapTitle;
    private static final VerticalList.Model maps;
    private static final VerticalList.Model musicControl;
    private static MusicManager musicManager;
    private static final int nowPlayingColor;
    private static final VerticalList.Model places;
    private static final RemoteDeviceManager remoteDeviceManager;
    private static final Resources resources;
    private static final Logger sLogger;
    private static final VerticalList.Model settings;
    private static final VerticalList.Model smartDash;
    private static final UIStateManager uiStateManager;
    private static final VerticalList.Model voiceAssistanceGoogle;
    private static final VerticalList.Model voiceAssistanceSiri;
    private static final VerticalList.Model voiceSearch;
    private ActiveTripMenu activeTripMenu;
    private Runnable activityTrayRunnable;
    private boolean activityTrayRunning;
    private int activityTraySelection;
    private int activityTraySelectionId;
    private MusicTrackInfo addedTrackInfo;
    private Bus bus;
    private boolean busRegistered;
    private List<VerticalList.Model> cachedList;
    private ContactsMenu contactsMenu;
    private int curToolTipId;
    private int curToolTipPos;
    private String currentEta;
    private Runnable etaRunnable;
    private MainOptionsMenu mainOptionsMenu;
    private IMenu musicMenu;
    private MusicManager.MusicUpdateListener musicUpdateListener;
    private PlacesMenu placesMenu;
    private MainMenuScreen2.Presenter presenter;
    private boolean registeredMusicCallback;
    private int selectedIndex;
    private SettingsMenu settingsMenu;
    private String swVersion;
    private VerticalMenuComponent vscrollComponent;
    
    static {
        sLogger = new Logger(MainMenu.class);
        ETA_UPDATE_INTERVAL = (int)TimeUnit.SECONDS.toMillis(30L);
        ACTIVITY_TRAY_UPDATE = (int)TimeUnit.SECONDS.toMillis(1L);
        childMenus = new HashMap<>();
        handler = new Handler(Looper.getMainLooper());
        activityTrayIcon = new ArrayList<>();
        activityTrayIconId = new ArrayList<>();
        activityTraySelectedColor = new ArrayList<>();
        activityTrayUnSelectedColor = new ArrayList<>();
        MainMenu.childMenus.put(Menu.SETTINGS.name(), Menu.SETTINGS);
        MainMenu.childMenus.put(Menu.PLACES.name(), Menu.PLACES);
        MainMenu.childMenus.put(Menu.CONTACTS.name(), Menu.CONTACTS);
        MainMenu.childMenus.put(Menu.MUSIC.name(), Menu.MUSIC);
        remoteDeviceManager = RemoteDeviceManager.getInstance();
        resources = HudApplication.getAppContext().getResources();
        callManager = MainMenu.remoteDeviceManager.getCallManager();
        uiStateManager = MainMenu.remoteDeviceManager.getUiStateManager();
        bkColorUnselected = MainMenu.resources.getColor(R.color.icon_bk_color_unselected);
        activeTripTitle = MainMenu.resources.getString(R.string.mm_active_trip);
        defaultFontInfo = VerticalList.getFontInfo(VerticalList.FontSize.FONT_SIZE_26);
        nowPlayingColor = MainMenu.resources.getColor(R.color.music_now_playing);
        glancesColor = MainMenu.resources.getColor(R.color.mm_glances);
        final String string = MainMenu.resources.getString(R.string.mm_voice_search);
        final String string2 = MainMenu.resources.getString(R.string.mm_voice_search_description);
        final int color = MainMenu.resources.getColor(R.color.mm_voice_search);
        voiceSearch = IconBkColorViewHolder.buildModel(R.id.main_menu_voice_search, R.drawable.icon_mm_voice_search_2, color, MainMenu.bkColorUnselected, color, string, string2);
        final String string3 = MainMenu.resources.getString(R.string.carousel_menu_smartdash_title);
        final int color2 = MainMenu.resources.getColor(R.color.mm_dash);
        smartDash = IconBkColorViewHolder.buildModel(R.id.main_menu_smart_dash, R.drawable.icon_mm_dash_2, color2, MainMenu.bkColorUnselected, color2, string3, null);
        final String string4 = MainMenu.resources.getString(R.string.carousel_menu_map_title);
        final int color3 = MainMenu.resources.getColor(R.color.mm_map);
        maps = IconBkColorViewHolder.buildModel(R.id.main_menu_maps, R.drawable.icon_mm_map_2, color3, MainMenu.bkColorUnselected, color3, string4, null);
        final String string5 = MainMenu.resources.getString(R.string.carousel_menu_map_places);
        final int color4 = MainMenu.resources.getColor(R.color.mm_places);
        places = IconBkColorViewHolder.buildModel(R.id.main_menu_places, R.drawable.icon_mm_places_2, color4, MainMenu.bkColorUnselected, color4, string5, null);
        final String string6 = MainMenu.resources.getString(R.string.carousel_menu_music_control);
        final int color5 = MainMenu.resources.getColor(R.color.mm_music);
        musicControl = IconBkColorViewHolder.buildModel(R.id.main_menu_music, R.drawable.icon_mm_music_2, color5, MainMenu.bkColorUnselected, color5, string6, null);
        final String string7 = MainMenu.resources.getString(R.string.carousel_menu_voice_control_siri);
        final int color6 = MainMenu.resources.getColor(R.color.mm_voice_assist_siri);
        voiceAssistanceSiri = IconBkColorViewHolder.buildModel(R.id.main_menu_voice, R.drawable.icon_mm_siri_2, color6, MainMenu.bkColorUnselected, color6, string7, null);
        final String string8 = MainMenu.resources.getString(R.string.carousel_menu_voice_control_google);
        final int color7 = MainMenu.resources.getColor(R.color.mm_voice_assist_gnow);
        voiceAssistanceGoogle = IconBkColorViewHolder.buildModel(R.id.main_menu_voice, R.drawable.icon_mm_googlenow_2, color7, MainMenu.bkColorUnselected, color7, string8, null);
        final String string9 = MainMenu.resources.getString(R.string.carousel_menu_contacts);
        final int color8 = MainMenu.resources.getColor(R.color.mm_contacts);
        contacts = IconBkColorViewHolder.buildModel(R.id.main_contacts, R.drawable.icon_mm_contacts_2, color8, MainMenu.bkColorUnselected, color8, string9, null);
        final String string10 = MainMenu.resources.getString(R.string.carousel_menu_settings_title);
        final int color9 = MainMenu.resources.getColor(R.color.mm_settings);
        settings = IconBkColorViewHolder.buildModel(R.id.main_menu_settings, R.drawable.icon_mm_settings_2, color9, MainMenu.bkColorUnselected, color9, string10, null);
        final String string11 = MainMenu.resources.getString(R.string.carousel_settings_connect_phone_title);
        final int color10 = MainMenu.resources.getColor(R.color.mm_connnect_phone);
        connectPhone = IconBkColorViewHolder.buildModel(R.id.main_menu_settings_connect_phone, R.drawable.icon_settings_connect_phone_2, color10, MainMenu.bkColorUnselected, color10, string11, null);
        dashTitle = MainMenu.resources.getString(R.string.carousel_menu_smartdash_options);
        mapTitle = MainMenu.resources.getString(R.string.carousel_menu_map_options);
        MainMenu.musicManager = MainMenu.remoteDeviceManager.getMusicManager();
    }
    
    public MainMenu(final Bus bus, final VerticalMenuComponent vscrollComponent, final MainMenuScreen2.Presenter presenter) {
        this.activityTraySelection = -1;
        this.activityTraySelectionId = -1;
        this.etaRunnable = new Runnable() {
            @Override
            public void run() {
                MainMenu.this.fillEta();
                MainMenu.handler.postDelayed(this, (long)MainMenu.ETA_UPDATE_INTERVAL);
            }
        };
        this.curToolTipPos = -1;
        this.curToolTipId = -1;
        this.activityTrayRunnable = new Runnable() {
            @Override
            public void run() {
                if (MainMenu.this.activityTrayRunning) {
                    if (MainMenu.this.curToolTipPos < 0) {
                        MainMenu.this.activityTrayRunning = false;
                    }
                    else {
                        final String access$500 = MainMenu.this.getToolTipString(R.id.main_menu_call);
                        if (access$500 != null) {
                            MainMenu.this.presenter.showToolTip(MainMenu.this.curToolTipPos, access$500);
                            MainMenu.handler.postDelayed(this, (long)MainMenu.ACTIVITY_TRAY_UPDATE);
                        }
                    }
                }
            }
        };
        this.musicUpdateListener = new MusicManager.MusicUpdateListener() {
            public void onAlbumArtUpdate(@Nullable ByteString artwork, boolean animate) {
            }

            public void onTrackUpdated(final MusicTrackInfo trackInfo, Set<MusicManager.MediaControl> set, boolean willOpenNotification) {
                if (!MainMenu.this.isSameTrack(MainMenu.this.addedTrackInfo, trackInfo) && MainMenu.this.canShowTrackInfo(trackInfo)) {
                    MainMenu.handler.post(new Runnable() {
                        public void run() {
                            MainMenu.this.addedTrackInfo = trackInfo;
                            if (MainMenu.this.curToolTipId == R.id.main_menu_now_playing && MainMenu.this.curToolTipPos >= 0) {
                                MainMenu.sLogger.v("update music tool tip");
                                String str = MainMenu.this.getMusicTrackInfo();
                                if (str != null) {
                                    MainMenu.this.presenter.showToolTip(MainMenu.this.curToolTipPos, str);
                                }
                            }
                        }
                    });
                }
            }
        };
        this.bus = bus;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
    }
    
    private void addLongPressAction(final DeviceInfo deviceInfo, final List<VerticalList.Model> list, final boolean b, final boolean b2, final boolean b3) {
        if (b3) {
            if (this.isLongPressActionPlaceSearch()) {
                if (this.selectedIndex == -1) {
                    this.selectedIndex = list.size();
                }
                this.addVoiceAssistance(deviceInfo, list);
            }
            else if (b && b2) {
                if (this.selectedIndex == -1) {
                    this.selectedIndex = list.size();
                }
                list.add(MainMenu.voiceSearch);
            }
        }
        else if (this.isLongPressActionPlaceSearch()) {
            if (b && b2) {
                list.add(MainMenu.voiceSearch);
            }
        }
        else {
            this.addVoiceAssistance(deviceInfo, list);
        }
    }
    
    private void addVoiceAssistance(final DeviceInfo deviceInfo, final List<VerticalList.Model> list) {
        if (deviceInfo.platform == DeviceInfo.Platform.PLATFORM_iOS) {
            list.add(MainMenu.voiceAssistanceSiri);
        }
        else {
            list.add(MainMenu.voiceAssistanceGoogle);
        }
    }
    
    private boolean canShowTrackInfo(final MusicTrackInfo musicTrackInfo) {
        return musicTrackInfo != null && (MusicManager.tryingToPlay(musicTrackInfo.playbackState) || musicTrackInfo.playbackState == MusicPlaybackState.PLAYBACK_PAUSED) && !TextUtils.isEmpty(musicTrackInfo.name);
    }
    
    private void clearNowPlayingState() {
        this.addedTrackInfo = null;
    }
    
    private IMenu createChildMenu(final Menu menu, final String s) {
        MainMenu.sLogger.v("createChildMenu:" + s);
        String s3;
        final String s2 = s3 = null;
        String substring = s;
        if (s != null) {
            if (s.indexOf("/") == 0) {
                s3 = s.substring(1);
                final int index = s3.indexOf("/");
                if (index >= 0) {
                    substring = s.substring(index + 1);
                    s3 = s3.substring(0, index);
                }
                else {
                    substring = null;
                }
            }
            else {
                substring = null;
                s3 = s2;
            }
        }
        IMenu menu2 = null;
        switch (menu) {
            default:
                menu2 = null;
                break;
            case CONTACTS:
                if (this.contactsMenu == null) {
                    this.contactsMenu = new ContactsMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                if (TextUtils.isEmpty(s3) || (menu2 = this.contactsMenu.getChildMenu(this, s3, substring)) == null) {
                    this.contactsMenu.setBackSelectionId(R.id.main_contacts);
                    menu2 = this.contactsMenu;
                    break;
                }
                break;
            case PLACES:
                if (this.placesMenu == null) {
                    this.placesMenu = new PlacesMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                if (TextUtils.isEmpty(s3) || (menu2 = this.placesMenu.getChildMenu(this, s3, substring)) == null) {
                    this.placesMenu.setBackSelectionId(R.id.main_menu_places);
                    menu2 = this.placesMenu;
                    break;
                }
                break;
            case SETTINGS:
                if (this.settingsMenu == null) {
                    this.settingsMenu = new SettingsMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                if (TextUtils.isEmpty(s3) || (menu2 = this.settingsMenu.getChildMenu(this, s3, substring)) == null) {
                    this.settingsMenu.setBackSelectionId(R.id.main_menu_settings);
                    menu2 = this.settingsMenu;
                    break;
                }
                break;
            case MUSIC:
                if (this.musicMenu == null) {
                    this.musicMenu = new MusicMenu2(this.bus, this.vscrollComponent, this.presenter, this, null);
                }
                this.musicMenu.setBackSelectionId(R.id.main_menu_music);
                if (TextUtils.isEmpty(s3) || (menu2 = this.musicMenu.getChildMenu(this, s3, substring)) == null) {
                    menu2 = this.musicMenu;
                    break;
                }
                break;
        }
        return menu2;
    }
    
    private void fillEta() {
        try {
            final String currentEtaStringWithDestination = HereMapUtil.getCurrentEtaStringWithDestination();
            if (!TextUtils.isEmpty(currentEtaStringWithDestination)) {
                this.currentEta = currentEtaStringWithDestination;
            }
        }
        catch (Throwable t) {
            MainMenu.sLogger.e(t);
        }
    }
    
    private VerticalList.Model getActivityTrayModel(final boolean b) {
        MainMenu.activityTrayIcon.clear();
        MainMenu.activityTrayIconId.clear();
        MainMenu.activityTraySelectedColor.clear();
        MainMenu.activityTrayUnSelectedColor.clear();
        boolean b2 = false;
        if (b) {
            b2 = b2;
            if (MainMenu.callManager.isCallInProgress()) {
                MainMenu.activityTrayIcon.add(R.drawable.icon_mm_contacts_2);
                MainMenu.activityTrayIconId.add(R.id.main_menu_call);
                MainMenu.activityTraySelectedColor.add(MainMenu.resources.getColor(R.color.mm_connnect_phone));
                MainMenu.activityTrayUnSelectedColor.add(MainMenu.bkColorUnselected);
                b2 = true;
            }
        }
        MainMenu.handler.removeCallbacks(this.etaRunnable);
        boolean b4;
        final boolean b3 = b4 = false;
        if (HereMapsManager.getInstance().isInitialized()) {
            b4 = b3;
            if (HereNavigationManager.getInstance().isNavigationModeOn()) {
                MainMenu.sLogger.v("nav on adding trip menu");
                MainMenu.activityTrayIcon.add(R.drawable.icon_badge_active_trip);
                MainMenu.activityTrayIconId.add(R.id.main_menu_active_trip);
                MainMenu.activityTraySelectedColor.add(MainMenu.resources.getColor(R.color.mm_active_trip));
                MainMenu.activityTrayUnSelectedColor.add(MainMenu.bkColorUnselected);
                this.fillEta();
                MainMenu.handler.postDelayed(this.etaRunnable, (long)MainMenu.ETA_UPDATE_INTERVAL);
                b4 = true;
            }
        }
        if (NotificationManager.getInstance().getNotificationCount() > 0) {
            MainMenu.activityTrayIcon.add(R.drawable.icon_mm_glances_2);
            MainMenu.activityTrayIconId.add(R.id.main_menu_glances);
            MainMenu.activityTraySelectedColor.add(MainMenu.glancesColor);
            MainMenu.activityTrayUnSelectedColor.add(MainMenu.bkColorUnselected);
        }
        if (b && !b2) {
            final MusicTrackInfo currentTrack = MainMenu.musicManager.getCurrentTrack();
            if (this.canShowTrackInfo(currentTrack)) {
                MainMenu.activityTrayIcon.add(R.drawable.icon_mm_music_2);
                MainMenu.activityTrayIconId.add(R.id.main_menu_now_playing);
                MainMenu.activityTraySelectedColor.add(MainMenu.nowPlayingColor);
                MainMenu.activityTrayUnSelectedColor.add(MainMenu.bkColorUnselected);
                this.addedTrackInfo = currentTrack;
                MainMenu.sLogger.v("added now playing");
            }
            else {
                MainMenu.sLogger.v("not added now playing");
            }
        }
        boolean b5;
        if (!(b5 = b4)) {
            b5 = b4;
            if (OTAUpdateService.isUpdateAvailable()) {
                this.swVersion = MainMenu.resources.getString(R.string.mm_navdy_update, OTAUpdateService.getSWUpdateVersion());
                MainMenu.sLogger.v("add navdy s/w");
                MainMenu.activityTrayIcon.add(R.drawable.icon_software_update_2);
                MainMenu.activityTrayIconId.add(R.id.settings_menu_software_update);
                MainMenu.activityTraySelectedColor.add(MainMenu.resources.getColor(R.color.mm_settings_update_display));
                MainMenu.activityTrayUnSelectedColor.add(MainMenu.bkColorUnselected);
                b5 = true;
            }
        }
        if (!b5) {
            final DialManager instance = DialManager.getInstance();
            if (instance.isDialConnected() && instance.getDialFirmwareUpdater().getVersions().isUpdateAvailable()) {
                MainMenu.sLogger.v("add dial s/w");
                MainMenu.activityTrayIcon.add(R.drawable.icon_dial_update_2);
                MainMenu.activityTrayIconId.add(R.id.settings_menu_dial_update);
                MainMenu.activityTraySelectedColor.add(MainMenu.resources.getColor(R.color.mm_settings_update_dial));
                MainMenu.activityTrayUnSelectedColor.add(MainMenu.bkColorUnselected);
            }
        }
        final Screen defaultMainActiveScreen = MainMenu.uiStateManager.getDefaultMainActiveScreen();
        final boolean b6 = false;
        final boolean b7 = false;
        int n = 0;
        boolean b8 = b7;
        if (defaultMainActiveScreen != null) {
            n = 0;
            b8 = b7;
            if (defaultMainActiveScreen == Screen.SCREEN_HOME) {
                final HomeScreenView homescreenView = MainMenu.uiStateManager.getHomescreenView();
                n = 0;
                b8 = b7;
                if (homescreenView != null) {
                    switch (homescreenView.getDisplayMode()) {
                        default:
                            b8 = b7;
                            n = (b6 ? 1 : 0);
                            break;
                        case SMART_DASH:
                            n = 1;
                            b8 = b7;
                            break;
                        case MAP:
                            b8 = true;
                            n = 0;
                            break;
                    }
                }
            }
        }
        if (n != 0) {
            MainMenu.activityTrayIcon.add(R.drawable.icon_main_menu_dash_options);
            MainMenu.activityTrayIconId.add(R.id.main_menu_smart_dash_options);
            MainMenu.activityTraySelectedColor.add(MainMenu.resources.getColor(R.color.mm_dash_options));
            MainMenu.activityTrayUnSelectedColor.add(MainMenu.bkColorUnselected);
        }
        else if (b8) {
            MainMenu.activityTrayIcon.add(R.drawable.icon_main_menu_map_options);
            MainMenu.activityTrayIconId.add(R.id.main_menu_maps_options);
            MainMenu.activityTraySelectedColor.add(MainMenu.resources.getColor(R.color.mm_map_options));
            MainMenu.activityTrayUnSelectedColor.add(MainMenu.bkColorUnselected);
        }
        final int size = MainMenu.activityTrayIcon.size();
        final int[] array = new int[size];
        final int[] array2 = new int[size];
        final int[] array3 = new int[size];
        final int[] array4 = new int[size];
        int n2 = 0;
        for (int i = size - 1; i >= 0; --i) {
            array[n2] = MainMenu.activityTrayIcon.get(i);
            array2[n2] = MainMenu.activityTrayIconId.get(i);
            array3[n2] = MainMenu.activityTraySelectedColor.get(i);
            array4[n2] = MainMenu.activityTrayUnSelectedColor.get(i);
            ++n2;
        }
        int activityTraySelection;
        if ((activityTraySelection = this.activityTraySelection) == -1) {
            activityTraySelection = size - 1;
        }
        return IconOptionsViewHolder.buildModel(R.id.main_menu_activity_tray, array, array2, array3, array4, array3, activityTraySelection, true);
    }
    
    private String getDurationStr(long n) {
        String s;
        if (n < 3600L) {
            final int n2 = (int)n / 60;
            s = String.format("%02d:%02d", n2, (int)(n - n2 * 60));
        }
        else {
            final int n3 = (int)n / 3600;
            n = (int)n - n3 * 60 * 60;
            final int n4 = (int)n / 60;
            s = String.format("%02d:%02d:%02d", n3, n4, (int)(n - n4 * 60));
        }
        return s;
    }
    
    private String getMusicTrackInfo() {
        String s;
        if (this.addedTrackInfo == null) {
            s = null;
        }
        else {
            s = this.addedTrackInfo.name;
            final String author = this.addedTrackInfo.author;
            boolean b;
            b = !TextUtils.isEmpty(author);
            boolean b2;
            b2 = !TextUtils.isEmpty(s);
            if (b && b2) {
                s = author + " - " + s;
            }
            else if (!b2) {
                s = author;
            }
        }
        return s;
    }
    
    private String getToolTipString(int n) {
        final String s = null;
        String s2 = null;
        switch (n) {
            default:
                s2 = s;
                break;
            case R.id.main_menu_now_playing:
                s2 = this.getMusicTrackInfo();
                break;
            case R.id.settings_menu_dial_update:
                s2 = MainMenu.resources.getString(R.string.mm_dial_update, "1.0." + DialManager.getInstance().getDialFirmwareUpdater().getVersions().local.incrementalVersion);
                break;
            case R.id.settings_menu_software_update:
                s2 = this.swVersion;
                break;
            case R.id.main_menu_call: {
                final CallNotification callNotification = (CallNotification)NotificationManager.getInstance().getNotification("navdy#phone#call#notif");
                s2 = s;
                if (callNotification == null) {
                    break;
                }
                s2 = s;
                if (!MainMenu.callManager.isCallInProgress()) {
                    break;
                }
                final String caller = callNotification.getCaller();
                s2 = s;
                if (caller == null) {
                    break;
                }
                n = MainMenu.callManager.getCurrentCallDuration();
                s2 = s;
                if (n >= 0) {
                    s2 = MainMenu.resources.getString(R.string.mm_call_info, caller, this.getDurationStr(n));
                    break;
                }
                break;
            }
            case R.id.main_menu_glances:
                n = NotificationManager.getInstance().getNotificationCount();
                if (n == 1) {
                    s2 = MainMenu.resources.getString(R.string.mm_glances_single);
                    break;
                }
                s2 = MainMenu.resources.getString(R.string.mm_glances_subtitle, n);
                break;
            case R.id.main_menu_active_trip:
                if (this.currentEta == null) {
                    s2 = MainMenu.activeTripTitle;
                    break;
                }
                s2 = this.currentEta;
                break;
            case R.id.main_menu_smart_dash_options:
                s2 = MainMenu.dashTitle;
                break;
            case R.id.main_menu_maps_options:
                s2 = MainMenu.mapTitle;
                break;
        }
        return s2;
    }
    
    private void handleGlances() {
        MainMenu.sLogger.v("glances");
        AnalyticsSupport.recordMenuSelection("glances");
        final Resources resources = HudApplication.getAppContext().getResources();
        final NotificationPreferences notificationPreferences = DriverProfileHelper.getInstance().getCurrentProfile().getNotificationPreferences();
        if (notificationPreferences.enabled != null && notificationPreferences.enabled) {
            final NotificationManager instance = NotificationManager.getInstance();
            if (instance.makeNotificationCurrent(true)) {
                MainMenu.sLogger.d("glances available");
                AnalyticsSupport.setGlanceNavigationSource("dial");
                instance.showNotification();
            }
            else {
                MainMenu.sLogger.d("no glances found");
                final Bundle bundle = new Bundle();
                bundle.putInt("13", 1000);
                bundle.putInt("8", R.drawable.icon_qm_glances_grey);
                bundle.putString("4", resources.getString(R.string.glances_none));
                bundle.putInt("5", R.style.Glances_1);
                ToastManager.getInstance().addToast(new ToastManager$ToastParams("glance-none", bundle, null, true, false));
                this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_BACK));
            }
        }
        else {
            MainMenu.sLogger.v("glances are not enabled");
            final Bundle bundle2 = new Bundle();
            bundle2.putInt("15", (int)resources.getDimension(R.dimen.no_glances_choice_padding));
            bundle2.putBoolean("12", true);
            bundle2.putInt("13", 5000);
            bundle2.putInt("8", R.drawable.icon_glances_disabled_modal);
            bundle2.putInt("11", R.drawable.icon_disabled);
            bundle2.putString("4", resources.getString(R.string.glances_disabled));
            bundle2.putInt("5", R.style.Glances_Disabled_1);
            bundle2.putString("6", resources.getString(R.string.glances_disabled_msg));
            bundle2.putInt("7", R.style.Glances_Disabled_2);
            bundle2.putInt("16", (int)resources.getDimension(R.dimen.no_glances_width));
            bundle2.putString("17", TTSUtils.TTS_GLANCES_DISABLED);
            ToastManager.getInstance().addToast(new ToastManager$ToastParams("glance-disabled", bundle2, null, true, false));
            this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_BACK));
        }
    }
    
    private boolean isLongPressActionPlaceSearch() {
        return DriverProfileHelper.getInstance().getCurrentProfile().getLongPressAction() == DriverProfilePreferences.DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH;
    }
    
    private boolean isSameTrack(final MusicTrackInfo musicTrackInfo, final MusicTrackInfo musicTrackInfo2) {
        return musicTrackInfo != null && musicTrackInfo2 != null && TextUtils.equals(musicTrackInfo.name, musicTrackInfo2.name) && TextUtils.equals(musicTrackInfo.author, musicTrackInfo2.author);
    }
    
    private void startActivityTrayRunnable(final int curToolTipId, final int curToolTipPos) {
        this.curToolTipId = curToolTipId;
        this.curToolTipPos = curToolTipPos;
        this.activityTrayRunning = true;
        MainMenu.handler.postDelayed(this.activityTrayRunnable, (long)MainMenu.ACTIVITY_TRAY_UPDATE);
    }
    
    private void stopActivityTrayRunnable() {
        this.curToolTipId = -1;
        this.curToolTipPos = -1;
        this.activityTrayRunning = false;
        MainMenu.handler.removeCallbacks(this.activityTrayRunnable);
    }
    
    @Subscribe
    public void arrivalEvent(final MapEvents$ArrivalEvent arrivalEvent) {
        MainMenu.sLogger.v("arrival event");
        MainMenu.handler.removeCallbacks(this.etaRunnable);
        this.fillEta();
    }
    
    public void clearState() {
        this.settingsMenu = null;
        this.placesMenu = null;
        this.contactsMenu = null;
        this.mainOptionsMenu = null;
        this.activeTripMenu = null;
        this.musicMenu = null;
    }
    
    public int getActivityTraySelection() {
        return this.activityTraySelection;
    }
    
    @Override
    public IMenu getChildMenu(IMenu childMenu, final String s, final String s2) {
        MainMenu.sLogger.v("getChildMenu:" + s + "," + s2);
        final Menu menu = MainMenu.childMenus.get(s);
        if (menu == null) {
            MainMenu.sLogger.v("getChildMenu: not found");
            childMenu = null;
        }
        else {
            childMenu = this.createChildMenu(menu, s2);
        }
        return childMenu;
    }
    
    @Override
    public int getInitialSelection() {
        return this.selectedIndex;
    }
    
    @Override
    public List<VerticalList.Model> getItems() {
        this.clearNowPlayingState();
        this.stopActivityTrayRunnable();
        this.curToolTipId = -1;
        this.curToolTipPos = -1;
        final DeviceInfo remoteDeviceInfo = MainMenu.remoteDeviceManager.getRemoteDeviceInfo();
        final ArrayList<VerticalList.Model> cachedList = new ArrayList<>();
        boolean b2;
        final boolean b = b2 = !DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        if (b) {
            b2 = b;
            if (remoteDeviceInfo == null) {
                b2 = false;
            }
        }
        MainMenu.sLogger.v("isConnected:" + b2);
        this.selectedIndex = -1;
        final HereMapsManager instance = HereMapsManager.getInstance();
        final VerticalList.Model activityTrayModel = this.getActivityTrayModel(b2);
        if (instance.isInitialized() && instance.isNavigationModeOn()) {
            this.selectedIndex = cachedList.size();
            if (activityTrayModel.iconIds != null) {
                for (int i = 0; i < activityTrayModel.iconIds.length; ++i) {
                    if (activityTrayModel.iconIds[i] == R.id.main_menu_active_trip) {
                        activityTrayModel.currentIconSelection = i;
                        break;
                    }
                }
            }
        }
        cachedList.add(activityTrayModel);
        final boolean navigationPossible = HereMapsManager.getInstance().isNavigationPossible();
        final boolean supportsVoiceSearch = RemoteCapabilitiesUtil.supportsVoiceSearch();
        if (b2) {
            if (!navigationPossible) {
                MainMenu.sLogger.v("nav not possible");
            }
            this.addLongPressAction(remoteDeviceInfo, cachedList, supportsVoiceSearch, navigationPossible, true);
        }
        else {
            this.selectedIndex = cachedList.size();
            cachedList.add(MainMenu.connectPhone);
        }
        if (this.selectedIndex == -1) {
            this.selectedIndex = cachedList.size();
        }
        cachedList.add(MainMenu.maps);
        cachedList.add(MainMenu.smartDash);
        if (b2) {
            if (navigationPossible) {
                cachedList.add(MainMenu.places);
            }
            cachedList.add(MainMenu.contacts);
        }
        final VerticalList.Model buildModel = IconBkColorViewHolder.buildModel(R.id.main_menu_glances, R.drawable.icon_mm_glances_2, MainMenu.glancesColor, MainMenu.bkColorUnselected, MainMenu.glancesColor, MainMenu.resources.getString(R.string.mm_glances), null);
        final NotificationPreferences notificationPreferences = DriverProfileHelper.getInstance().getCurrentProfile().getNotificationPreferences();
        if (notificationPreferences.enabled == null || !notificationPreferences.enabled) {
            buildModel.title = MainMenu.resources.getString(R.string.mm_glances_disabled);
            buildModel.subTitle = null;
        }
        else {
            final int notificationCount = NotificationManager.getInstance().getNotificationCount();
            buildModel.title = MainMenu.resources.getString(R.string.mm_glances);
            buildModel.subTitle = MainMenu.resources.getString(R.string.mm_glances_subtitle, notificationCount);
        }
        cachedList.add(buildModel);
        if (b2) {
            cachedList.add(MainMenu.musicControl);
            this.addLongPressAction(remoteDeviceInfo, cachedList, supportsVoiceSearch, navigationPossible, false);
        }
        cachedList.add(MainMenu.settings);
        this.cachedList = cachedList;
        if (!this.registeredMusicCallback) {
            MainMenu.musicManager.addMusicUpdateListener(this.musicUpdateListener);
            this.registeredMusicCallback = true;
            MainMenu.sLogger.v("register music");
        }
        if (!this.busRegistered) {
            this.bus.register(this);
            this.busRegistered = true;
            MainMenu.sLogger.v("registered bus");
        }
        return cachedList;
    }
    
    @Override
    public VerticalList.Model getModelfromPos(final int n) {
        VerticalList.Model model;
        if (this.cachedList != null && this.cachedList.size() > n) {
            model = this.cachedList.get(n);
        }
        else {
            model = null;
        }
        return model;
    }
    
    @Override
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    @Override
    public Menu getType() {
        return Menu.MAIN;
    }
    
    @Override
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    @Override
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    @Override
    public boolean isItemClickable(final int n, final int n2) {
        boolean b = false;
        switch (n) {
            default:
                b = true;
                break;
            case R.id.main_menu_no_fav_contacts:
            case R.id.main_menu_no_fav_places:
            case R.id.main_menu_no_recent_contacts:
            case R.id.main_menu_no_suggested_places:
                b = false;
                break;
        }
        return b;
    }
    
    @Override
    public void onBindToView(final VerticalList.Model model, final View view, final int n, final VerticalList.ModelState modelState) {
    }
    
    @Override
    public void onFastScrollEnd() {
    }
    
    @Override
    public void onFastScrollStart() {
    }
    
    @Override
    public void onItemSelected(final VerticalList.ItemSelectionState itemSelectionState) {
        switch (itemSelectionState.id) {
            default:
                this.stopActivityTrayRunnable();
                this.presenter.hideToolTip();
                break;
            case R.id.main_menu_activity_tray: {
                final String toolTipString = this.getToolTipString(itemSelectionState.subId);
                if (toolTipString != null) {
                    this.stopActivityTrayRunnable();
                    switch (itemSelectionState.subId) {
                        default:
                            this.curToolTipId = -1;
                            this.curToolTipPos = -1;
                            break;
                        case R.id.main_menu_call:
                            this.startActivityTrayRunnable(itemSelectionState.subId, itemSelectionState.subPosition);
                            break;
                        case R.id.main_menu_now_playing:
                            this.curToolTipId = itemSelectionState.subId;
                            this.curToolTipPos = itemSelectionState.subPosition;
                            break;
                    }
                    this.presenter.showToolTip(itemSelectionState.subPosition, toolTipString);
                    break;
                }
                this.stopActivityTrayRunnable();
                this.presenter.hideToolTip();
                break;
            }
        }
    }
    
    @Override
    public void onScrollIdle() {
    }
    
    @Override
    public void onUnload(final MenuLevel menuLevel) {
        MainMenu.handler.removeCallbacks(this.etaRunnable);
        if (this.registeredMusicCallback) {
            this.registeredMusicCallback = false;
            MainMenu.musicManager.removeMusicUpdateListener(this.musicUpdateListener);
            MainMenu.sLogger.v("unregister");
        }
        if (this.busRegistered) {
            this.bus.unregister(this);
            this.busRegistered = false;
            MainMenu.sLogger.v("bus unregistered");
        }
        this.clearNowPlayingState();
        if (menuLevel == MenuLevel.CLOSE) {
            if (this.placesMenu != null) {
                this.placesMenu.onUnload(MenuLevel.CLOSE);
            }
            if (this.contactsMenu != null) {
                this.contactsMenu.onUnload(MenuLevel.CLOSE);
            }
            if (this.musicMenu != null) {
                this.musicMenu.onUnload(MenuLevel.CLOSE);
            }
        }
    }
    
    @Override
    public boolean selectItem(final VerticalList.ItemSelectionState itemSelectionState) {
        MainMenu.sLogger.v("select id:" + itemSelectionState.id + " pos:" + itemSelectionState.pos);
        Label_0144: {
            switch (itemSelectionState.id) {
                case R.id.main_menu_smart_dash:
                    MainMenu.sLogger.v("dash");
                    AnalyticsSupport.recordMenuSelection("dash");
                    this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_DASHBOARD));
                    break;
                case R.id.main_menu_maps:
                    MainMenu.sLogger.v("map");
                    AnalyticsSupport.recordMenuSelection("hybrid_map");
                    this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_HYBRID_MAP));
                    break;
                case R.id.main_menu_glances:
                    this.handleGlances();
                    break;
                case R.id.main_menu_voice_search:
                    MainMenu.sLogger.v("voice_search");
                    AnalyticsSupport.recordMenuSelection("voice_search");
                    this.presenter.performSelectionAnimation(new Runnable() {
                        @Override
                        public void run() {
                            final NotificationManager instance = NotificationManager.getInstance();
                            VoiceSearchNotification voiceSearchNotification;
                            if ((voiceSearchNotification = (VoiceSearchNotification)instance.getNotification("navdy#voicesearch#notif")) == null) {
                                voiceSearchNotification = new VoiceSearchNotification();
                            }
                            instance.addNotification(voiceSearchNotification);
                        }
                    });
                    break;
                case R.id.main_menu_music: {
                    final MusicManager musicManager = MainMenu.remoteDeviceManager.getMusicManager();
                    if (UISettings.isMusicBrowsingEnabled() && musicManager.hasMusicCapabilities()) {
                        MainMenu.sLogger.v("music browse");
                        AnalyticsSupport.recordMenuSelection("music");
                        this.createChildMenu(Menu.MUSIC, null);
                        this.presenter.loadMenu(this.musicMenu, MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
                        break;
                    }
                    MainMenu.sLogger.v("music");
                    AnalyticsSupport.recordMenuSelection("music");
                    this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_MUSIC));
                    break;
                }
                case R.id.main_menu_voice:
                    MainMenu.sLogger.v("voice-assist");
                    AnalyticsSupport.recordMenuSelection("voice_assist");
                    this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_VOICE_CONTROL));
                    break;
                case R.id.main_menu_settings:
                    MainMenu.sLogger.v("settings");
                    AnalyticsSupport.recordMenuSelection("settings");
                    this.createChildMenu(Menu.SETTINGS, null);
                    this.presenter.loadMenu(this.settingsMenu, MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
                    break;
                case R.id.main_menu_settings_connect_phone: {
                    MainMenu.sLogger.v("connect phone");
                    AnalyticsSupport.recordMenuSelection("connect_phone");
                    final Bundle bundle = new Bundle();
                    bundle.putString(WelcomeScreen.ARG_ACTION, WelcomeScreen.ACTION_SWITCH_PHONE);
                    this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_WELCOME, bundle, false));
                    break;
                }
                case R.id.main_menu_places:
                    MainMenu.sLogger.v("places");
                    AnalyticsSupport.recordMenuSelection("places");
                    this.createChildMenu(Menu.PLACES, null);
                    this.presenter.loadMenu(this.placesMenu, MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
                    break;
                case R.id.main_contacts:
                    MainMenu.sLogger.v("contacts");
                    AnalyticsSupport.recordMenuSelection("contacts");
                    this.createChildMenu(Menu.CONTACTS, null);
                    this.presenter.loadMenu(this.contactsMenu, MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
                    break;
                case R.id.main_menu_activity_tray:
                    switch (itemSelectionState.subId) {
                        default:
                            break Label_0144;
                        case R.id.main_menu_active_trip:
                            MainMenu.sLogger.v("trip options");
                            AnalyticsSupport.recordMenuSelection("trip-options");
                            if (this.activeTripMenu == null) {
                                this.activeTripMenu = new ActiveTripMenu(this.bus, this.vscrollComponent, this.presenter, this);
                            }
                            this.activityTraySelection = itemSelectionState.subPosition;
                            this.activityTraySelectionId = itemSelectionState.subId;
                            this.presenter.loadMenu(this.activeTripMenu, MenuLevel.SUB_LEVEL, itemSelectionState.pos, 1, true);
                            break Label_0144;
                        case R.id.main_menu_now_playing:
                            MainMenu.sLogger.v("now_playing");
                            AnalyticsSupport.recordMenuSelection("now_playing");
                            this.presenter.performSelectionAnimation(new Runnable() {
                                @Override
                                public void run() {
                                    MainMenu.this.bus.post(Screen.SCREEN_BACK);
                                    MainMenu.musicManager.showMusicNotification();
                                }
                            });
                            break Label_0144;
                        case R.id.settings_menu_dial_update: {
                            MainMenu.sLogger.v("dial update");
                            AnalyticsSupport.recordMenuSelection("dial_update_mm");
                            final Bundle bundle2 = new Bundle();
                            bundle2.putBoolean("UPDATE_REMINDER", false);
                            this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_DIAL_UPDATE_CONFIRMATION, bundle2, false));
                            break Label_0144;
                        }
                        case R.id.settings_menu_software_update: {
                            MainMenu.sLogger.v("software update");
                            AnalyticsSupport.recordMenuSelection("software_update_mm");
                            final Bundle bundle3 = new Bundle();
                            bundle3.putBoolean("UPDATE_REMINDER", false);
                            this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_OTA_CONFIRMATION, bundle3, false));
                            break Label_0144;
                        }
                        case R.id.main_menu_call: {
                            MainMenu.sLogger.v("phone call");
                            AnalyticsSupport.recordMenuSelection("phone_call_mm");
                            final NotificationManager instance = NotificationManager.getInstance();
                            if (instance.getNotification("navdy#phone#call#notif") != null && instance.makeNotificationCurrent(true)) {
                                MainMenu.sLogger.d("phone call");
                                instance.showNotification();
                                break Label_0144;
                            }
                            this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_BACK));
                            break Label_0144;
                        }
                        case R.id.main_menu_glances:
                            this.handleGlances();
                            break Label_0144;
                        case R.id.main_menu_smart_dash_options:
                            MainMenu.sLogger.v("dash-options");
                            AnalyticsSupport.recordMenuSelection("dash-options");
                            if (this.mainOptionsMenu == null) {
                                this.mainOptionsMenu = new MainOptionsMenu(this.bus, this.vscrollComponent, this.presenter, this);
                            }
                            this.mainOptionsMenu.setMode(MainOptionsMenu.Mode.DASH);
                            this.activityTraySelection = itemSelectionState.subPosition;
                            this.activityTraySelectionId = itemSelectionState.subId;
                            this.presenter.loadMenu(this.mainOptionsMenu, MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
                            break Label_0144;
                        case R.id.main_menu_maps_options:
                            MainMenu.sLogger.v("map options");
                            AnalyticsSupport.recordMenuSelection("hybrid_map-options");
                            if (this.mainOptionsMenu == null) {
                                this.mainOptionsMenu = new MainOptionsMenu(this.bus, this.vscrollComponent, this.presenter, this);
                            }
                            this.mainOptionsMenu.setMode(MainOptionsMenu.Mode.MAP);
                            this.activityTraySelection = itemSelectionState.subPosition;
                            this.activityTraySelectionId = itemSelectionState.subId;
                            this.presenter.loadMenu(this.mainOptionsMenu, MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
                    }
//                    break;
            }
        }
        return true;
    }
    
    @Override
    public void setBackSelectionId(final int n) {
    }
    
    @Override
    public void setBackSelectionPos(final int n) {
    }
    
    @Override
    public void setSelectedIcon() {
        if (this.activityTraySelection != -1 && this.presenter.getCurrentSelection() == 0) {
            final String toolTipString = this.getToolTipString(this.activityTraySelectionId);
            if (toolTipString != null) {
                this.presenter.showToolTip(this.activityTraySelection, toolTipString);
            }
        }
        this.activityTraySelection = -1;
        this.activityTraySelectionId = -1;
        final Resources resources = HudApplication.getAppContext().getResources();
        final int dimensionPixelSize = resources.getDimensionPixelSize(R.dimen.vmenu_selected_image);
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_mm_navdy, -1, new LinearGradient(0.0f, 0.0f, (float)dimensionPixelSize, (float)dimensionPixelSize, new int[] { resources.getColor(R.color.mm_icon_gradient_start), resources.getColor(R.color.mm_icon_gradient_end) }, null, Shader.TileMode.MIRROR), 1.0f);
        this.vscrollComponent.selectedText.setText(resources.getString(R.string.menu_str));
    }
    
    @Override
    public void showToolTip() {
        final int currentSubSelectionId = this.presenter.getCurrentSubSelectionId();
        final int currentSubSelectionPos = this.presenter.getCurrentSubSelectionPos();
        final String toolTipString = this.getToolTipString(currentSubSelectionId);
        if (toolTipString != null) {
            this.stopActivityTrayRunnable();
            if (currentSubSelectionId == R.id.main_menu_call) {
                this.startActivityTrayRunnable(currentSubSelectionId, currentSubSelectionPos);
            }
            this.presenter.showToolTip(currentSubSelectionPos, toolTipString);
        }
        else {
            this.stopActivityTrayRunnable();
            this.presenter.hideToolTip();
        }
    }
}
