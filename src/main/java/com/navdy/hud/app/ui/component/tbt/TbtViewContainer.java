package com.navdy.hud.app.ui.component.tbt;

import com.navdy.hud.app.R;
import com.navdy.hud.app.maps.MapEvents$ManeuverDisplay;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.squareup.otto.Subscribe;

import android.animation.Animator;
import android.view.ViewPropertyAnimator;
import android.text.TextUtils;
import kotlin.TypeCastException;
import android.animation.AnimatorSet.Builder;
import android.view.View;
import org.jetbrains.annotations.Nullable;
import android.util.AttributeSet;
import android.content.Context;
import kotlin.jvm.internal.Intrinsics;
import com.navdy.hud.app.HudApplication;
//import kotlin.jvm.internal.DefaultConstructorMarker;
import com.navdy.hud.app.view.MainView;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import java.util.HashMap;
import com.navdy.hud.app.ui.framework.UIStateManager;
import android.content.res.Resources;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import com.navdy.service.library.log.Logger;
import org.jetbrains.annotations.NotNull;
import com.squareup.otto.Bus;
import kotlin.Metadata;
import com.navdy.hud.app.ui.component.homescreen.IHomeScreenLifecycle;
import android.widget.FrameLayout;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 .2\u00020\u00012\u00020\u0002:\u0001.B\u000f\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005B\u0017\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bB\u001f\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\n¢\u0006\u0002\u0010\u000bJ\u0006\u0010\u001f\u001a\u00020 J\u001a\u0010!\u001a\u00020 2\u0006\u0010\"\u001a\u00020\u00192\n\u0010#\u001a\u00060$R\u00020%J\b\u0010&\u001a\u0004\u0018\u00010\u0012J\u000e\u0010'\u001a\u00020 2\u0006\u0010\u000e\u001a\u00020\u000fJ\b\u0010(\u001a\u00020 H\u0014J\b\u0010)\u001a\u00020 H\u0016J\b\u0010*\u001a\u00020 H\u0016J\u000e\u0010+\u001a\u00020 2\u0006\u0010\"\u001a\u00020\u0019J\u0010\u0010,\u001a\u00020 2\u0006\u0010-\u001a\u00020\u0012H\u0007R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0012X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u0015X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u001c\u001a\u0004\u0018\u00010\u0015X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006/" }, d2 = { "Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;", "Landroid/widget/FrameLayout;", "Lcom/navdy/hud/app/ui/component/homescreen/IHomeScreenLifecycle;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "activeView", "Lcom/navdy/hud/app/ui/component/tbt/TbtView;", "homeScreenView", "Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;", "inactiveView", "lastEvent", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "lastManeuverDisplay", "lastManeuverId", "", "lastManeuverState", "Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;", "lastMode", "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;", "lastRoadText", "lastTurnIconId", "lastTurnText", "paused", "", "clearState", "", "getCustomAnimator", "mode", "mainBuilder", "Landroid/animation/AnimatorSet.Builder;", "Landroid/animation/AnimatorSet;", "getLastManeuverDisplay", "init", "onFinishInflate", "onPause", "onResume", "setView", "updateDisplay", "event", "Companion", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class TbtViewContainer extends FrameLayout implements IHomeScreenLifecycle
{
    public static final Companion Companion;
    private static final long MANEUVER_PROGRESS_ANIMATION_DURATION = 250L;
    private static final long MANEUVER_SWAP_DELAY = 300L;
    private static final long MANEUVER_SWAP_DURATION = 500L;
    @NotNull
    private static final Bus bus;
    private static final float itemHeight;
    private static final Logger logger;
    private static final AccelerateDecelerateInterpolator maneuverSwapInterpolator;
    @NotNull
    private static final RemoteDeviceManager remoteDeviceManager;
    @NotNull
    private static final Resources resources;
    @NotNull
    private static final UIStateManager uiStateManager;
    private HashMap _$_findViewCache;
    private TbtView activeView;
    private HomeScreenView homeScreenView;
    protected TbtView inactiveView;
    private MapEvents$ManeuverDisplay lastEvent;
    private MapEvents$ManeuverDisplay lastManeuverDisplay;
    private String lastManeuverId;
    private HereManeuverDisplayBuilder$ManeuverState lastManeuverState;
    private MainView.CustomAnimationMode lastMode;
    private String lastRoadText;
    private int lastTurnIconId;
    private String lastTurnText;
    private boolean paused;
    
    static {
        Companion = new Companion();
        logger = new Logger("TbtViewContainer");
        final Resources resources2 = HudApplication.getAppContext().getResources();
        Intrinsics.checkExpressionValueIsNotNull(resources2, "HudApplication.getAppContext().resources");
        resources = resources2;
        itemHeight = TbtViewContainer.Companion.getResources().getDimension(R.dimen.tbt_view_ht);
        maneuverSwapInterpolator = new AccelerateDecelerateInterpolator();
        final RemoteDeviceManager instance = RemoteDeviceManager.getInstance();
        Intrinsics.checkExpressionValueIsNotNull(instance, "RemoteDeviceManager.getInstance()");
        remoteDeviceManager = instance;
        final Bus bus2 = TbtViewContainer.Companion.getRemoteDeviceManager().getBus();
        Intrinsics.checkExpressionValueIsNotNull(bus2, "remoteDeviceManager.bus");
        bus = bus2;
        final UIStateManager uiStateManager2 = TbtViewContainer.Companion.getRemoteDeviceManager().getUiStateManager();
        Intrinsics.checkExpressionValueIsNotNull(uiStateManager2, "remoteDeviceManager.uiStateManager");
        uiStateManager = uiStateManager2;
    }
    
    public TbtViewContainer(@NotNull final Context context) {
        super(context);
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.lastTurnIconId = -1;
    }
    
    public TbtViewContainer(@NotNull final Context context, @NotNull final AttributeSet set) {
        super(context, set);
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(set, "attrs");
        this.lastTurnIconId = -1;
    }
    
    public TbtViewContainer(@NotNull final Context context, @NotNull final AttributeSet set, final int n) {
        super(context, set, n);
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(set, "attrs");
        this.lastTurnIconId = -1;
    }
    
    @NotNull
    public static /* synthetic */ Bus access$getBus$cp() {
        return TbtViewContainer.bus;
    }
    
    public static /* synthetic */ float access$getItemHeight$cp() {
        return TbtViewContainer.itemHeight;
    }
    
    @NotNull
    public static /* synthetic */ Logger access$getLogger$cp() {
        return TbtViewContainer.logger;
    }
    
    public static /* synthetic */ long access$getMANEUVER_PROGRESS_ANIMATION_DURATION$cp() {
        return TbtViewContainer.MANEUVER_PROGRESS_ANIMATION_DURATION;
    }
    
    public static /* synthetic */ long access$getMANEUVER_SWAP_DELAY$cp() {
        return TbtViewContainer.MANEUVER_SWAP_DELAY;
    }
    
    public static /* synthetic */ long access$getMANEUVER_SWAP_DURATION$cp() {
        return TbtViewContainer.MANEUVER_SWAP_DURATION;
    }
    
    @NotNull
    public static  /* synthetic */ AccelerateDecelerateInterpolator access$getManeuverSwapInterpolator$cp() {
        return TbtViewContainer.maneuverSwapInterpolator;
    }
    
    @NotNull
    public static /* synthetic */ RemoteDeviceManager access$getRemoteDeviceManager$cp() {
        return TbtViewContainer.remoteDeviceManager;
    }
    
    @NotNull
    public static /* synthetic */ Resources access$getResources$cp() {
        return TbtViewContainer.resources;
    }
    
    @NotNull
    public static /* synthetic */ UIStateManager access$getUiStateManager$cp() {
        return TbtViewContainer.uiStateManager;
    }
    
    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }
    
    public View _$_findCachedViewById(final int n) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View viewById;
        if ((viewById = (View)this._$_findViewCache.get(n)) == null) {
            viewById = this.findViewById(n);
            this._$_findViewCache.put(n, viewById);
        }
        return viewById;
    }
    
    public final void clearState() {
        this.lastTurnIconId = -1;
        this.lastTurnText = null;
        this.lastRoadText = null;
        final TbtView activeView = this.activeView;
        if (activeView != null) {
            activeView.clear();
        }
        final TbtView inactiveView = this.inactiveView;
        if (inactiveView != null) {
            inactiveView.clear();
        }
    }
    
    public final void getCustomAnimator(@NotNull final MainView.CustomAnimationMode lastMode, @NotNull final Builder Builder) {
        Intrinsics.checkParameterIsNotNull(lastMode, "mode");
        Intrinsics.checkParameterIsNotNull(Builder, "mainBuilder");
        this.lastMode = lastMode;
        final TbtView activeView = this.activeView;
        if (activeView != null) {
            activeView.getCustomAnimator(lastMode, Builder);
        }
        final TbtView inactiveView = this.inactiveView;
        if (inactiveView != null) {
            inactiveView.getCustomAnimator(lastMode, Builder);
        }
    }
    
    @Nullable
    public final MapEvents$ManeuverDisplay getLastManeuverDisplay() {
        return this.lastManeuverDisplay;
    }
    
    public final void init(@NotNull final HomeScreenView homeScreenView) {
        Intrinsics.checkParameterIsNotNull(homeScreenView, "homeScreenView");
        this.homeScreenView = homeScreenView;
        TbtViewContainer.Companion.getBus().register(this);
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        final View viewById = this.findViewById(R.id.view1);
        if (viewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtView");
        }
        this.activeView = (TbtView)viewById;
        final View viewById2 = this.findViewById(R.id.view2);
        if (viewById2 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtView");
        }
        this.inactiveView = (TbtView)viewById2;
        final TbtView activeView = this.activeView;
        if (activeView != null) {
            activeView.setTranslationY(-TbtViewContainer.Companion.getItemHeight());
        }
    }
    
    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            TbtViewContainer.Companion.getLogger().v("::onPause:tbt");
        }
    }
    
    public void onResume() {
        if (this.paused) {
            this.paused = false;
            TbtViewContainer.Companion.getLogger().v("::onResume:tbt");
            final MapEvents$ManeuverDisplay lastEvent = this.lastEvent;
            if (lastEvent != null) {
                this.lastEvent = null;
                TbtViewContainer.Companion.getLogger().v("::onResume:tbt updated last event");
                this.updateDisplay(lastEvent);
            }
        }
    }
    
    public final void setView(@NotNull final MainView.CustomAnimationMode view) {
        Intrinsics.checkParameterIsNotNull(view, "mode");
        this.lastMode = view;
        final TbtView activeView = this.activeView;
        if (activeView != null) {
            activeView.setView(view);
        }
        final TbtView inactiveView = this.inactiveView;
        if (inactiveView != null) {
            inactiveView.setView(view);
        }
    }
/*
    @Subscribe
    public final void updateDisplay(@NotNull final MapEvents.ManeuverDisplay maneuverDisplay) {
        Intrinsics.checkParameterIsNotNull(maneuverDisplay, "event");
        this.lastManeuverDisplay = maneuverDisplay;
        final HomeScreenView homeScreenView = this.homeScreenView;
        if (homeScreenView != null && homeScreenView.isNavigationActive()) {
            if (maneuverDisplay.empty) {
                this.clearState();
            }
            else if (maneuverDisplay.pendingTurn == null) {
                TbtViewContainer.Companion.getLogger().v("null pending turn");
                this.lastEvent = null;
                this.lastManeuverId = null;
            }
            else {
                this.lastEvent = maneuverDisplay;
                if (this.paused) {
                    this.lastManeuverId = maneuverDisplay.maneuverId;
                }
                else {
                    if (!TextUtils.equals((CharSequence)this.lastManeuverId, (CharSequence)maneuverDisplay.maneuverId)) {
                        final TbtView activeView = this.activeView;
                        this.activeView = this.inactiveView;
                        this.inactiveView = activeView;
                        final TbtView activeView2 = this.activeView;
                        if (activeView2 != null) {
                            activeView2.setTranslationY(-TbtViewContainer.Companion.getItemHeight());
                        }
                        final TbtView activeView3 = this.activeView;
                        if (activeView3 != null) {
                            activeView3.updateDisplay(maneuverDisplay);
                        }
                        final TbtView activeView4 = this.activeView;
                        if (activeView4 != null) {
                            final ViewPropertyAnimator animate = activeView4.animate();
                            if (animate != null) {
                                final ViewPropertyAnimator translationY = animate.translationY(0.0f);
                                if (translationY != null) {
                                    final ViewPropertyAnimator setDuration = translationY.setDuration(TbtViewContainer.Companion.getMANEUVER_SWAP_DURATION());
                                    if (setDuration != null) {
                                        final ViewPropertyAnimator setStartDelay = setDuration.setStartDelay(TbtViewContainer.Companion.getMANEUVER_SWAP_DELAY());
                                        if (setStartDelay != null) {
                                            setStartDelay.start();
                                        }
                                    }
                                }
                            }
                        }
                        final TbtView inactiveView = this.inactiveView;
                        if (inactiveView != null) {
                            final ViewPropertyAnimator animate2 = inactiveView.animate();
                            if (animate2 != null) {
                                final ViewPropertyAnimator translationY2 = animate2.translationY(TbtViewContainer.Companion.getItemHeight());
                                if (translationY2 != null) {
                                    final ViewPropertyAnimator setDuration2 = translationY2.setDuration(TbtViewContainer.Companion.getMANEUVER_SWAP_DURATION());
                                    if (setDuration2 != null) {
                                        final ViewPropertyAnimator setStartDelay2 = setDuration2.setStartDelay(TbtViewContainer.Companion.getMANEUVER_SWAP_DELAY());
                                        if (setStartDelay2 != null) {
                                            final ViewPropertyAnimator setListener = setStartDelay2.setListener((Animator.AnimatorListener)new updateDisplay.TbtViewContainer$updateDisplay$1(this));
                                            if (setListener != null) {
                                                setListener.start();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else {
                        final TbtView activeView5 = this.activeView;
                        if (activeView5 != null) {
                            activeView5.updateDisplay(maneuverDisplay);
                        }
                    }
                    this.lastManeuverId = maneuverDisplay.maneuverId;
                    this.lastManeuverState = maneuverDisplay.maneuverState;
                }
            }
        }
    }
*/
    @Subscribe
    public final void updateDisplay(@NotNull MapEvents$ManeuverDisplay event) {
        Intrinsics.checkParameterIsNotNull(event, "event");
        this.lastManeuverDisplay = event;
        HomeScreenView homeScreenView = this.homeScreenView;
        if (!(homeScreenView != null && homeScreenView.isNavigationActive())) {
            return;
        }
        if (event.empty) {
            clearState();
        } else if (event.pendingTurn == null) {
            Companion.getLogger().v("null pending turn");
            this.lastEvent = null;
            this.lastManeuverId = null;
        } else {
            this.lastEvent = event;
            if (this.paused) {
                this.lastManeuverId = event.maneuverId;
                return;
            }
            TbtView tbtView;
            if (TextUtils.equals(this.lastManeuverId, event.maneuverId)) {
                tbtView = this.activeView;
                if (tbtView != null) {
                    tbtView.updateDisplay(event);
                }
            } else {
                ViewPropertyAnimator animate;
                TbtView temp = this.activeView;
                this.activeView = this.inactiveView;
                this.inactiveView = temp;
                tbtView = this.activeView;
                if (tbtView != null) {
                    tbtView.setTranslationY(-Companion.getItemHeight());
                }
                tbtView = this.activeView;
                if (tbtView != null) {
                    tbtView.updateDisplay(event);
                }
                tbtView = this.activeView;
                if (tbtView != null) {
                    animate = tbtView.animate();
                    if (animate != null) {
                        animate = animate.translationY(0.0f);
                        if (animate != null) {
                            animate = animate.setDuration(Companion.getMANEUVER_SWAP_DURATION());
                            if (animate != null) {
                                animate = animate.setStartDelay(Companion.getMANEUVER_SWAP_DELAY());
                                if (animate != null) {
                                    animate.start();
                                }
                            }
                        }
                    }
                }
                tbtView = this.inactiveView;
                if (tbtView != null) {
                    animate = tbtView.animate();
                    if (animate != null) {
                        animate = animate.translationY(Companion.getItemHeight());
                        if (animate != null) {
                            animate = animate.setDuration(Companion.getMANEUVER_SWAP_DURATION());
                            if (animate != null) {
                                ViewPropertyAnimator startDelay = animate.setStartDelay(Companion.getMANEUVER_SWAP_DELAY());
                                if (startDelay != null) {
                                    final TbtViewContainer $outer = this;
                                    animate = startDelay.setListener(new DefaultAnimationListener() {
                                        final /* synthetic */ TbtViewContainer this$0 = $outer;

                                        public void onAnimationEnd(@NotNull Animator animation) {
                                            Intrinsics.checkParameterIsNotNull(animation, "animation");
                                            TbtView access$getInactiveView$p = this.this$0.inactiveView;
                                            if (access$getInactiveView$p != null) {
                                                access$getInactiveView$p.setTranslationY(-Companion.getItemHeight());
                                            }
                                        }
                                    });
                                    if (animate != null) {
                                        animate.start();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            this.lastManeuverId = event.maneuverId;
            this.lastManeuverState = event.maneuverState;
        }
    }

    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006R\u0014\u0010\t\u001a\u00020\u0004X\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u0006R\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u000f\u001a\u00020\u0010¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u0014X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0014\u0010\u0017\u001a\u00020\u0018X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\u001b\u001a\u00020\u001c¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\u001f\u001a\u00020 ¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\"R\u0011\u0010#\u001a\u00020$¢\u0006\b\n\u0000\u001a\u0004\b%\u0010&¨\u0006'" }, d2 = { "Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;", "", "()V", "MANEUVER_PROGRESS_ANIMATION_DURATION", "", "getMANEUVER_PROGRESS_ANIMATION_DURATION", "()J", "MANEUVER_SWAP_DELAY", "getMANEUVER_SWAP_DELAY", "MANEUVER_SWAP_DURATION", "getMANEUVER_SWAP_DURATION", "bus", "Lcom/squareup/otto/Bus;", "getBus", "()Lcom/squareup/otto/Bus;", "itemHeight", "", "getItemHeight", "()F", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "maneuverSwapInterpolator", "Landroid/view/animation/AccelerateDecelerateInterpolator;", "getManeuverSwapInterpolator", "()Landroid/view/animation/AccelerateDecelerateInterpolator;", "remoteDeviceManager", "Lcom/navdy/hud/app/manager/RemoteDeviceManager;", "getRemoteDeviceManager", "()Lcom/navdy/hud/app/manager/RemoteDeviceManager;", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "uiStateManager", "Lcom/navdy/hud/app/ui/framework/UIStateManager;", "getUiStateManager", "()Lcom/navdy/hud/app/ui/framework/UIStateManager;", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    public static final class Companion
    {
        private Logger getLogger() {
            return TbtViewContainer.access$getLogger$cp();
        }
        
        private AccelerateDecelerateInterpolator getManeuverSwapInterpolator() {
            return TbtViewContainer.access$getManeuverSwapInterpolator$cp();
        }
        
        @NotNull
        public final Bus getBus() {
            return TbtViewContainer.access$getBus$cp();
        }
        
        public final float getItemHeight() {
            return TbtViewContainer.access$getItemHeight$cp();
        }
        
        public final long getMANEUVER_PROGRESS_ANIMATION_DURATION() {
            return TbtViewContainer.access$getMANEUVER_PROGRESS_ANIMATION_DURATION$cp();
        }
        
        final long getMANEUVER_SWAP_DELAY() {
            return TbtViewContainer.access$getMANEUVER_SWAP_DELAY$cp();
        }
        
        final long getMANEUVER_SWAP_DURATION() {
            return TbtViewContainer.access$getMANEUVER_SWAP_DURATION$cp();
        }
        
        @NotNull
        public final RemoteDeviceManager getRemoteDeviceManager() {
            return TbtViewContainer.access$getRemoteDeviceManager$cp();
        }
        
        @NotNull
        public final Resources getResources() {
            return TbtViewContainer.access$getResources$cp();
        }
        
        @NotNull
        public final UIStateManager getUiStateManager() {
            return TbtViewContainer.access$getUiStateManager$cp();
        }
    }
}
