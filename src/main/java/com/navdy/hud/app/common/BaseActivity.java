package com.navdy.hud.app.common;

import mortar.MortarScope;
import android.os.Bundle;
import android.content.res.Configuration;
import mortar.Mortar;
import mortar.Blueprint;
import com.navdy.hud.app.profile.HudLocale;
import android.content.Context;
import com.navdy.service.library.log.Logger;
import mortar.MortarActivityScope;
import android.app.Activity;

public abstract class BaseActivity extends Activity
{
    private MortarActivityScope activityScope;
    protected Logger logger;
    
    public BaseActivity() {
        this.logger = new Logger(this.getClass());
    }
    
    protected void attachBaseContext(final Context context) {
        super.attachBaseContext(HudLocale.onAttach(context));
    }
    
    protected Blueprint getBlueprint() {
        return null;
    }
    
    public Object getSystemService(final String s) {
        Object o;
        if (Mortar.isScopeSystemService(s)) {
            if (this.activityScope == null) {
                throw new IllegalArgumentException("BaseActivity must override getBlueprint to use Mortar internally");
            }
            o = this.activityScope;
        }
        else {
            o = super.getSystemService(s);
        }
        return o;
    }
    
    public boolean isActivityDestroyed() {
        return this.isFinishing() || this.isDestroyed();
    }
    
    public void onConfigurationChanged(final Configuration configuration) {
        this.logger.v("::onConfigurationChanged");
        super.onConfigurationChanged(configuration);
        HudLocale.onAttach((Context)this);
    }
    
    protected void onCreate(final Bundle bundle) {
        this.logger.v("::OnCreate");
        super.onCreate(bundle);
        final Blueprint blueprint = this.getBlueprint();
        if (blueprint != null) {
            this.activityScope = Mortar.requireActivityScope(Mortar.getScope((Context)this.getApplication()), blueprint);
            Mortar.inject((Context)this, this);
            this.activityScope.onCreate(bundle);
        }
    }
    
    protected void onDestroy() {
        this.logger.v("::onDestroy");
        super.onDestroy();
        if (this.isFinishing() && this.activityScope != null) {
            Mortar.getScope((Context)this.getApplication()).destroyChild(this.activityScope);
            this.activityScope = null;
        }
    }
    
    protected void onPause() {
        this.logger.v("::onPause");
        super.onPause();
    }
    
    public void onResume() {
        this.logger.v("::onResume");
        super.onResume();
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        this.logger.v("::onSaveInstanceState");
        super.onSaveInstanceState(bundle);
        if (this.activityScope != null) {
            this.activityScope.onSaveInstanceState(bundle);
        }
    }
}
