package com.navdy.hud.app.view;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.device.dial.DialFirmwareUpdater.Versions;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.screen.DialUpdateConfirmationScreen.Presenter;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout.Mode;
import com.navdy.hud.app.util.ViewUtil;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import mortar.Mortar;

public class DialUpdateConfirmationView extends RelativeLayout implements IListener, IInputHandler {
    private static final int UPDATE_MESSAGE_MAX_WIDTH = 380;
    private static final Logger sLogger = new Logger(DialUpdateConfirmationView.class);
    private boolean isReminder;
    @InjectView(R.id.choiceLayout)
    ChoiceLayout mChoiceLayout;
    @InjectView(R.id.image )
    ImageView mIcon;
    @InjectView(R.id.title3)
    TextView mInfoText;
    @InjectView(R.id.leftSwipe)
    ImageView mLefttSwipe;
    @InjectView(R.id.title2)
    TextView mMainTitleText;
    @Inject
    Presenter mPresenter;
    @InjectView(R.id.rightSwipe)
    ImageView mRightSwipe;
    @InjectView(R.id.mainTitle)
    TextView mScreenTitleText;
    @InjectView(R.id.title4)
    TextView mSummaryText;
    @InjectView(R.id.mainSection)
    RelativeLayout mainSection;
    private String updateTargetVersion;

    public DialUpdateConfirmationView(Context context) {
        this(context, null, 0);
    }

    public DialUpdateConfirmationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DialUpdateConfirmationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.updateTargetVersion = "1.0.60";
        this.isReminder = false;
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        String updateFromVersion = "1.0.1";
        ButterKnife.inject((View) this);
        if (this.mPresenter != null) {
            this.mPresenter.takeView(this);
            this.isReminder = this.mPresenter.isReminder();
            ToastManager.getInstance().disableToasts(true);
            NotificationManager.getInstance().enableNotifications(false);
            Versions versions = DialManager.getInstance().getDialFirmwareUpdater().getVersions();
            this.updateTargetVersion = "1.0." + versions.local.incrementalVersion;
            updateFromVersion = "1.0." + versions.dial.incrementalVersion;
        }
        this.mScreenTitleText.setVisibility(GONE);
        findViewById(R.id.title1).setVisibility(GONE);
        this.mMainTitleText.setText(R.string.dial_update_ready_to_install);
        ViewUtil.adjustPadding(this.mainSection, 0, 0, 0, 10);
        ViewUtil.autosize(this.mMainTitleText, 2, (int) UPDATE_MESSAGE_MAX_WIDTH, (int) R.array.title_sizes);
        this.mMainTitleText.setVisibility(View.VISIBLE);
        this.mIcon.setImageResource(R.drawable.icon_dial_update);
        this.mInfoText.setText(R.string.dial_update_installation_will_take);
        this.mInfoText.setVisibility(View.VISIBLE);
        ((MaxWidthLinearLayout) findViewById(R.id.infoContainer)).setMaxWidth(UPDATE_MESSAGE_MAX_WIDTH);
        Resources res = getContext().getResources();
        this.mSummaryText.setText(String.format(res.getString(R.string.dial_update_will_upgrade_to_from), new Object[]{this.updateTargetVersion, updateFromVersion}));
        this.mSummaryText.setVisibility(View.VISIBLE);
        List<Choice> list = new ArrayList();
        if (this.isReminder) {
            list.add(new Choice(res.getString(R.string.install_now), 0));
            list.add(new Choice(res.getString(R.string.install_later), 1));
        } else {
            list.add(new Choice(res.getString(R.string.install), 0));
            list.add(new Choice(res.getString(R.string.cancel), 1));
        }
        this.mChoiceLayout.setChoices(Mode.LABEL, list, 0, this);
        this.mRightSwipe.setVisibility(View.VISIBLE);
        this.mLefttSwipe.setVisibility(View.VISIBLE);
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        BaseScreen screen = RemoteDeviceManager.getInstance().getUiStateManager().getCurrentScreen();
        boolean enableNotif = true;
        if (screen != null && screen.getScreen() == Screen.SCREEN_DIAL_UPDATE_PROGRESS) {
            enableNotif = false;
        }
        sLogger.v("enableNotif:" + enableNotif + " screen[" + (screen == null ? "none" : screen.getScreen()) + "]");
        if (enableNotif) {
            ToastManager.getInstance().disableToasts(false);
            NotificationManager.getInstance().enableNotifications(true);
        }
        if (this.mPresenter != null) {
            this.mPresenter.dropView(this);
        }
    }

    public void executeItem(int pos, int id) {
        switch (pos) {
            case 0:
                this.mPresenter.install();
                if (this.isReminder) {
                    AnalyticsSupport.recordUpdatePrompt(true, true, this.updateTargetVersion);
                    return;
                }
                return;
            case 1:
                this.mPresenter.finish();
                if (this.isReminder) {
                    AnalyticsSupport.recordUpdatePrompt(false, true, this.updateTargetVersion);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void itemSelected(int pos, int id) {
    }

    public boolean onGesture(GestureEvent event) {
        if (event.gesture != null) {
            switch (event.gesture) {
                case GESTURE_SWIPE_LEFT:
                    executeItem(0, 0);
                    return true;
                case GESTURE_SWIPE_RIGHT:
                    executeItem(1, 0);
                    return true;
            }
        }
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        if (this.mChoiceLayout == null || this.mChoiceLayout.getVisibility() != 0) {
            return false;
        }
        switch (event) {
            case LEFT:
                this.mChoiceLayout.moveSelectionLeft();
                return true;
            case RIGHT:
                this.mChoiceLayout.moveSelectionRight();
                return true;
            case SELECT:
                this.mChoiceLayout.executeSelectedItem(true);
                return true;
            default:
                return true;
        }
    }

    public IInputHandler nextHandler() {
        return null;
    }
}
