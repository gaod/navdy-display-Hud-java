package com.navdy.hud.app.view;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.view.animation.LinearInterpolator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import java.util.LinkedList;
import java.util.Queue;

public class SerialValueAnimator {
    private DefaultAnimationListener animationListener = new DefaultAnimationListener() {
        public void onAnimationEnd(Animator animation) {
            if (animation instanceof ValueAnimator) {
                SerialValueAnimator.this.mAdapter.animationComplete(((Float) ((ValueAnimator) animation).getAnimatedValue()).floatValue());
            }
            if (SerialValueAnimator.this.mValueQueue.size() == 0) {
                SerialValueAnimator.this.mAnimationRunning = false;
                SerialValueAnimator.this.mAnimator = null;
                return;
            }
            SerialValueAnimator.this.animate(((Float) SerialValueAnimator.this.mValueQueue.remove()).floatValue());
        }
    };
    private SerialValueAnimatorAdapter mAdapter;
    private boolean mAnimationRunning;
    private ValueAnimator mAnimator;
    private int mDuration;
    private Queue<Float> mValueQueue = new LinkedList();

    interface SerialValueAnimatorAdapter {
        void animationComplete(float f);

        float getValue();

        void setValue(float f);
    }

    public SerialValueAnimator(SerialValueAnimatorAdapter adapter, int animationDuration) {
        this.mAdapter = adapter;
        this.mDuration = animationDuration;
        if (adapter == null) {
            throw new IllegalArgumentException("SerialValueAnimator cannot run without the adapter");
        }
    }

    public void setValue(float val) {
        if (this.mAdapter.getValue() == val) {
            this.mAdapter.setValue(val);
            this.mAdapter.animationComplete(val);
        } else if (this.mAnimationRunning) {
            this.mValueQueue.add(Float.valueOf(val));
        } else {
            this.mAnimationRunning = true;
            animate(val);
        }
    }

    private void animate(float value) {
        this.mAnimator = ValueAnimator.ofFloat(new float[]{this.mAdapter.getValue(), value});
        this.mAnimator.setDuration((long) this.mDuration);
        this.mAnimator.setInterpolator(new LinearInterpolator());
        this.mAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                SerialValueAnimator.this.mAdapter.setValue(((Float) animation.getAnimatedValue()).floatValue());
            }
        });
        this.mAnimator.addListener(this.animationListener);
        this.mAnimator.start();
    }

    public void release() {
        this.mValueQueue.clear();
        if (this.mAnimator != null) {
            this.mAnimator.removeAllListeners();
            this.mAnimator.removeAllUpdateListeners();
            this.mAnimator.cancel();
        }
        this.mAnimator = null;
        this.mAnimationRunning = false;
    }

    public void setDuration(int duration) {
        this.mDuration = duration;
    }
}
