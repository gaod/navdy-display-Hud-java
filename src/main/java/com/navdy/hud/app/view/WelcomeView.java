package com.navdy.hud.app.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.makeramen.RoundedTransformationBuilder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.screen.WelcomeScreen.Presenter;
import com.navdy.hud.app.screen.WelcomeScreen.DeviceMetadata;
import com.navdy.hud.app.screen.WelcomeScreen;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout.Mode;
import com.navdy.hud.app.ui.component.FluctuatorAnimatorView;
import com.navdy.hud.app.ui.component.carousel.Carousel.InitParams;
import com.navdy.hud.app.ui.component.carousel.Carousel.Listener;
import com.navdy.hud.app.ui.component.carousel.Carousel.Model;
import com.navdy.hud.app.ui.component.carousel.Carousel.ViewProcessor;
import com.navdy.hud.app.ui.component.carousel.CarouselLayout;
import com.navdy.hud.app.ui.component.carousel.ShrinkAnimator;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView.Style;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Transformation;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import mortar.Mortar;

public class WelcomeView extends FrameLayout implements IInputHandler {
    private static final int DOWNLOAD_APP_TRANSITION_INTERVAL = ((int) TimeUnit.MILLISECONDS.toMillis(200));
    private static final int DOWNLOAD_APP_STAY_INTERVAL = (((int) TimeUnit.SECONDS.toMillis(5)) - DOWNLOAD_APP_TRANSITION_INTERVAL);
    public static final int STATE_DOWNLOAD_APP = 1;
    public static final int STATE_PICKER = 2;
    public static final int STATE_UNKNOWN = -1;
    public static final int STATE_WELCOME = 3;
    private static int[] appStoreIcons;
    private static int downloadColor1;
    private static int downloadColor2;
    private static String[] downloadText1;
    private static String[] downloadText2;
    private static int[] playStoreIcons;
    private static final Logger sLogger = new Logger(WelcomeView.class);
    private Runnable animationRunnable;
    @InjectView(R.id.animatorView)
    public FluctuatorAnimatorView animatorView;
    @InjectView(R.id.app_store_img_1)
    public ImageView appStoreImage1;
    @InjectView(R.id.app_store_img_2)
    public ImageView appStoreImage2;
    @InjectView(R.id.carousel)
    public CarouselLayout carousel;
    @InjectView(R.id.choiceLayout)
    public ChoiceLayout choiceLayout;
    private int currentDownloadIndex;
    private int currentDownloadView;
    @InjectView(R.id.download_app)
    public RelativeLayout downloadAppContainer;
    @InjectView(R.id.download_text_1)
    public TextView downloadAppTextView1;
    @InjectView(R.id.download_text_2)
    public TextView downloadAppTextView2;
    private Handler handler;
    private int initialState;
    @InjectView(R.id.leftDot)
    public ImageView leftDot;
    public List<Model> list;
    @InjectView(R.id.message)
    public TextView messageView;
    @InjectView(R.id.play_store_img_1)
    public ImageView playStoreImage1;
    @InjectView(R.id.play_store_img_2)
    public ImageView playStoreImage2;
    @Inject
    public WelcomeScreen.Presenter presenter;
    @InjectView(R.id.rightDot)
    public ImageView rightDot;
    @InjectView(R.id.rootContainer)
    public View rootContainer;
    Transformation roundTransformation;
    private boolean searching;
    private int state;
    @InjectView(R.id.subTitle)
    public TextView subTitleView;
    @InjectView(R.id.title)
    public TextView titleView;

    public WelcomeView(Context context) {
        this(context, null);
    }

    public WelcomeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WelcomeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.state = -1;
        this.currentDownloadIndex = -1;
        this.currentDownloadView = -1;
        this.list = new ArrayList();
        this.roundTransformation = new RoundedTransformationBuilder().oval(true).build();
        this.handler = new Handler(Looper.getMainLooper());
        this.animationRunnable = new Runnable() {
            public void run() {
                if (WelcomeView.this.currentDownloadIndex != -1) {
                    TextView current = null;
                    TextView next = null;
                    ImageView appStoreCurrent = null;
                    ImageView appStoreNext = null;
                    ImageView playStoreCurrent = null;
                    ImageView playStoreNext = null;
                    switch (WelcomeView.this.currentDownloadView) {
                        case 0:
                            current = WelcomeView.this.downloadAppTextView1;
                            next = WelcomeView.this.downloadAppTextView2;
                            appStoreCurrent = WelcomeView.this.appStoreImage1;
                            appStoreNext = WelcomeView.this.appStoreImage2;
                            playStoreCurrent = WelcomeView.this.playStoreImage1;
                            playStoreNext = WelcomeView.this.playStoreImage2;
                            WelcomeView.this.currentDownloadView = 1;
                            break;
                        case 1:
                            current = WelcomeView.this.downloadAppTextView2;
                            next = WelcomeView.this.downloadAppTextView1;
                            appStoreCurrent = WelcomeView.this.appStoreImage2;
                            appStoreNext = WelcomeView.this.appStoreImage1;
                            playStoreCurrent = WelcomeView.this.playStoreImage2;
                            playStoreNext = WelcomeView.this.playStoreImage1;
                            WelcomeView.this.currentDownloadView = 0;
                            break;
                    }
                    final TextView txtView = current;
                    final ImageView appView = appStoreCurrent;
                    final ImageView playView = playStoreCurrent;
                    current.animate().alpha(0.0f).setDuration((long) WelcomeView.DOWNLOAD_APP_TRANSITION_INTERVAL);
                    next.animate().alpha(1.0f).setDuration((long) WelcomeView.DOWNLOAD_APP_TRANSITION_INTERVAL).withEndAction(new Runnable() {
                        public void run() {
                            WelcomeView.this.currentDownloadIndex = WelcomeView.this.currentDownloadIndex + 1;
                            if (WelcomeView.this.currentDownloadIndex == WelcomeView.appStoreIcons.length) {
                                WelcomeView.this.currentDownloadIndex = 0;
                            }
                            WelcomeView.this.handler.removeCallbacks(WelcomeView.this.animationRunnable);
                            WelcomeView.this.handler.postDelayed(WelcomeView.this.animationRunnable, (long) WelcomeView.DOWNLOAD_APP_STAY_INTERVAL);
                            WelcomeView.this.setDownloadText(txtView, WelcomeView.this.currentDownloadIndex);
                            WelcomeView.this.setDownloadImage(appView, WelcomeView.appStoreIcons[WelcomeView.this.currentDownloadIndex]);
                            WelcomeView.this.setDownloadImage(playView, WelcomeView.playStoreIcons[WelcomeView.this.currentDownloadIndex]);
                        }
                    });
                    appStoreCurrent.animate().alpha(0.0f).setDuration((long) WelcomeView.DOWNLOAD_APP_TRANSITION_INTERVAL);
                    appStoreNext.animate().alpha(1.0f).setDuration((long) WelcomeView.DOWNLOAD_APP_TRANSITION_INTERVAL);
                    playStoreCurrent.animate().alpha(0.0f).setDuration((long) WelcomeView.DOWNLOAD_APP_TRANSITION_INTERVAL);
                    playStoreNext.animate().alpha(1.0f).setDuration((long) WelcomeView.DOWNLOAD_APP_TRANSITION_INTERVAL);
                }
            }
        };
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
        initFromAttributes(context, attrs);
    }

    private void initFromAttributes(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.WelcomeView, 0, 0);
        try {
            this.initialState = a.getInt(R.styleable.WelcomeView_welcomeState, 1);
        } finally {
            a.recycle();
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        setState(this.initialState);
        initializeCarousel();
        List<Choice> choices = new ArrayList(1);
        choices.add(new Choice(getResources().getString(R.string.welcome_cancel), 0));
        this.choiceLayout.setChoices(Mode.LABEL, choices, 0, new IListener() {
            public void executeItem(int pos, int id) {
                WelcomeView.this.presenter.cancel();
            }

            public void itemSelected(int pos, int id) {
            }
        });
    }

    public void setState(int state) {
        if (this.state != state) {
            this.state = state;
            this.rootContainer.setVisibility(GONE);
            stopDownloadAppAnimation();
            this.leftDot.setVisibility(GONE);
            this.rightDot.setVisibility(GONE);
            switch (state) {
                case 1:
                    startDownloadAppAnimation();
                    return;
                case 2:
                case 3:
                    this.rootContainer.setVisibility(View.VISIBLE);
                    return;
                default:
                    return;
            }
        }
    }

    private void initializeCarousel() {
        InitParams initParams = new InitParams();
        initParams.model = this.list;
        initParams.rootContainer = this.rootContainer;
        initParams.viewProcessor = new ViewProcessor() {
            public void processSmallImageView(Model model, View view, int pos, int width, int height) {
                CrossFadeImageView crossFadeImageView = (CrossFadeImageView) view;
                updateInitials(false, model, (InitialsImageView) crossFadeImageView.getSmall());
                updateInitials(true, model, (InitialsImageView) crossFadeImageView.getBig());
            }

            public void processLargeImageView(Model model, View view, int pos, int width, int height) {
                CrossFadeImageView crossFadeImageView = (CrossFadeImageView) view;
                updateInitials(false, model, (InitialsImageView) crossFadeImageView.getSmall());
                updateInitials(true, model, (InitialsImageView) crossFadeImageView.getBig());
            }

            private void updateInitials(boolean largeImage, Model model, ImageView view) {
                final InitialsImageView initialsImageView = (InitialsImageView) view;
                if (model.id == R.id.welcome_menu_add_driver || model.id == R.id.welcome_menu_searching) {
                    initialsImageView.setInitials(null, Style.LARGE);
                    return;
                }
                final DeviceMetadata device = (DeviceMetadata)model.extras;
                float f = (!(model.id >= 100) || largeImage) ? 1.0f : 0.5f;
                view.setAlpha(f);
                final NavdyDeviceId deviceId = device.deviceId;
                view.setTag(deviceId);
                Map<Integer, String> infoMap = model.infoMap;
                final String initials = ContactUtil.getInitials(infoMap != null ? (String) infoMap.get(R.id.subTitle) : "");
                initialsImageView.setInitials(null, Style.DEFAULT);
                final File imagePath = device.driverProfile.getDriverImageFile();
                Bitmap bitmap = PicassoUtil.getBitmapfromCache(imagePath);
                if (bitmap != null) {
                    initialsImageView.setImageBitmap(bitmap);
                    return;
                }
                final ImageView imageView = view;
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        if (imageView.getTag() == deviceId) {
                            final boolean imageExists = imagePath.exists();
                            WelcomeView.this.handler.post(new Runnable() {
                                public void run() {
                                    if (imageView.getTag() == deviceId) {
                                        if (imageExists) {
                                            WelcomeView.this.setImage(imagePath, initialsImageView, null);
                                        } else {
                                            initialsImageView.setInitials(initials, Style.LARGE);
                                        }
                                    }
                                }
                            });
                        }
                    }
                }, 1);
            }

            public void processInfoView(Model model, View view, int pos) {
            }
        };
        initParams.infoLayoutResourceId = R.layout.carousel_view_menu;
        initParams.imageLytResourceId = R.layout.crossfade_image_lyt;
        initParams.carouselIndicator = null;
        initParams.animator = new ShrinkAnimator();
        this.carousel.init(initParams);
        this.carousel.setListener(new Listener() {
            public void onCurrentItemChanging(int pos, int newPos, int id) {
                int i = 0;
                ImageView imageView;
                if (newPos > pos) {
                    imageView = WelcomeView.this.rightDot;
                    if (newPos + 2 >= WelcomeView.this.carousel.getCount()) {
                        i = 8;
                    }
                    imageView.setVisibility(i);
                } else {
                    imageView = WelcomeView.this.leftDot;
                    if (newPos <= 1) {
                        i = 8;
                    }
                    imageView.setVisibility(i);
                }
                WelcomeView.this.updateInfo(WelcomeView.this.carousel.getModel(newPos));
            }

            public void onCurrentItemChanged(int pos, int id) {
                int i;
                int i2 = 0;
                ImageView imageView = WelcomeView.this.leftDot;
                if (pos > 1) {
                    i = 0;
                } else {
                    i = 8;
                }
                imageView.setVisibility(i);
                ImageView imageView2 = WelcomeView.this.rightDot;
                if (pos + 2 >= WelcomeView.this.carousel.getCount()) {
                    i2 = 8;
                }
                imageView2.setVisibility(i2);
                Model model = WelcomeView.this.carousel.getModel(pos);
                WelcomeView.this.updateInfo(model);
                if (WelcomeView.this.presenter != null) {
                    WelcomeView.this.presenter.onCurrentItemChanged(pos, model.id);
                }
            }

            public void onExecuteItem(int id, int pos) {
                if (WelcomeView.this.presenter != null) {
                    WelcomeView.this.presenter.executeItem(id, pos);
                }
            }

            public void onExit() {
            }
        });
    }

    private void updateInfo(Model model) {
        if (model != null && model.infoMap != null) {
            String title = (String) model.infoMap.get(Integer.valueOf(R.id.title));
            String subTitle = (String) model.infoMap.get(Integer.valueOf(R.id.subTitle));
            String message = (String) model.infoMap.get(Integer.valueOf(R.id.message));
            TextView textView = this.titleView;
            if (title == null) {
                title = "";
            }
            textView.setText(title);
            textView = this.subTitleView;
            if (subTitle == null) {
                subTitle = "";
            }
            textView.setText(subTitle);
            textView = this.messageView;
            if (message == null) {
                message = "";
            }
            textView.setText(message);
        }
    }

    private void setImage(File path, ImageView imageView, Callback callback) {
        PicassoUtil.getInstance().load(path).noPlaceholder().fit().transform(this.roundTransformation).noFade().into(imageView, callback);
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
        if (this.searching) {
            this.animatorView.start();
        }
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.searching = false;
        this.animatorView.stop();
        stopDownloadAppAnimation();
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
    }

    public void setSearching(boolean searching) {
        if (searching != this.searching) {
            this.searching = searching;
            if (searching) {
                this.animatorView.setVisibility(View.VISIBLE);
                this.animatorView.start();
                return;
            }
            this.animatorView.stop();
            this.animatorView.setVisibility(GONE);
        }
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        if (this.state != 1) {
            return this.carousel.onKey(event);
        }
        switch (event) {
            case LEFT:
                this.choiceLayout.moveSelectionLeft();
                return true;
            case RIGHT:
                this.choiceLayout.moveSelectionRight();
                return true;
            case SELECT:
                this.choiceLayout.executeSelectedItem(true);
                return true;
            default:
                return false;
        }
    }

    public IInputHandler nextHandler() {
        return InputManager.nextContainingHandler(this);
    }

    private void startDownloadAppAnimation() {
        sLogger.v("startDownloadAppAnimation");
        Resources resources = getContext().getResources();
        if (downloadText1 == null) {
            int i;
            downloadText1 = resources.getStringArray(R.array.download_app_1);
            downloadText2 = resources.getStringArray(R.array.download_app_2);
            downloadColor1 = resources.getColor(R.color.download_app_1);
            downloadColor2 = resources.getColor(R.color.download_app_2);
            TypedArray typedArray = getResources().obtainTypedArray(R.array.download_app_icon_app_store);
            appStoreIcons = new int[downloadText1.length];
            for (i = 0; i < appStoreIcons.length; i++) {
                appStoreIcons[i] = typedArray.getResourceId(i, 0);
            }
            typedArray.recycle();
            typedArray = getResources().obtainTypedArray(R.array.download_app_icon_play_store);
            playStoreIcons = new int[downloadText1.length];
            for (i = 0; i < playStoreIcons.length; i++) {
                playStoreIcons[i] = typedArray.getResourceId(i, 0);
            }
            typedArray.recycle();
        }
        this.currentDownloadIndex = 0;
        this.currentDownloadView = 0;
        int index1 = this.currentDownloadIndex;
        int index2 = this.currentDownloadIndex + 1;
        setDownloadText(this.downloadAppTextView1, index1);
        setDownloadText(this.downloadAppTextView2, index2);
        setDownloadImage(this.appStoreImage1, appStoreIcons[index1]);
        setDownloadImage(this.appStoreImage2, appStoreIcons[index2]);
        setDownloadImage(this.playStoreImage1, playStoreIcons[index1]);
        setDownloadImage(this.playStoreImage2, playStoreIcons[index2]);
        this.downloadAppTextView1.setAlpha(1.0f);
        this.downloadAppTextView2.setAlpha(0.0f);
        this.appStoreImage1.setAlpha(1.0f);
        this.appStoreImage2.setAlpha(0.0f);
        this.playStoreImage1.setAlpha(1.0f);
        this.playStoreImage2.setAlpha(0.0f);
        this.downloadAppContainer.setVisibility(View.VISIBLE);
        this.currentDownloadIndex++;
        this.handler.removeCallbacks(this.animationRunnable);
        this.handler.postDelayed(this.animationRunnable, (long) DOWNLOAD_APP_STAY_INTERVAL);
    }

    private void setDownloadText(TextView textView, int index) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(downloadText1[index]);
        builder.setSpan(new ForegroundColorSpan(downloadColor1), 0, builder.length(), 33);
        builder.append(" ");
        int start = builder.length();
        builder.append(downloadText2[index]);
        builder.setSpan(new ForegroundColorSpan(downloadColor2), start, builder.length(), 33);
        builder.setSpan(new StyleSpan(1), start, builder.length(), 33);
        textView.setText(builder);
    }

    private void setDownloadImage(ImageView image, int resId) {
        image.setImageResource(resId);
    }

    private void stopDownloadAppAnimation() {
        sLogger.v("stopDownloadAppAnimation");
        this.handler.removeCallbacks(this.animationRunnable);
        this.downloadAppTextView1.animate().cancel();
        this.downloadAppTextView2.animate().cancel();
        this.appStoreImage1.animate().cancel();
        this.appStoreImage2.animate().cancel();
        this.playStoreImage1.animate().cancel();
        this.playStoreImage2.animate().cancel();
        this.downloadAppContainer.setVisibility(GONE);
        this.currentDownloadIndex = -1;
    }
}
