package com.navdy.hud.app.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.music.AlbumArtImageView;
import com.navdy.hud.app.framework.music.OutlineTextView;
import com.navdy.hud.app.manager.MusicManager;
import com.navdy.hud.app.manager.MusicManager.MediaControl;
import com.navdy.hud.app.manager.MusicManager.MusicUpdateListener;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.ScalingUtilities;
import com.navdy.service.library.util.ScalingUtilities$ScalingLogic;

import java.util.Set;
import javax.inject.Inject;
import mortar.Mortar;
import okio.ByteString;

public class MusicWidgetPresenter extends DashboardWidgetPresenter implements MusicUpdateListener {
    private static final float ARTWORK_ALPHA_NOT_PLAYING = 0.5f;
    private static final float ARTWORK_ALPHA_PLAYING = 1.0f;
    private static Logger logger = new Logger(MusicWidgetPresenter.class);
    private Bitmap albumArt;
    private boolean animateNextArtworkTransition;
    private OutlineTextView artistText;
    private Handler handler = new Handler();
    private AlbumArtImageView image;
    private CharSequence lastAlbumArtHash;
    private String musicGaugeName;
    @Inject
    MusicManager musicManager;
    private OutlineTextView stateText;
    private OutlineTextView titleText;
    private MusicTrackInfo trackInfo = null;

    public MusicWidgetPresenter(Context context) {
        this.musicGaugeName = context.getResources().getString(R.string.widget_music);
        Mortar.inject(HudApplication.getAppContext(), this);
    }

    public void setView(DashboardWidgetView dashboardWidgetView, Bundle arguments) {
        logger.d("setView " + dashboardWidgetView);
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView((int) R.layout.music_widget);
            this.titleText = (OutlineTextView) dashboardWidgetView.findViewById(R.id.txt_name);
            this.artistText = (OutlineTextView) dashboardWidgetView.findViewById(R.id.txt_author);
            this.stateText = (OutlineTextView) dashboardWidgetView.findViewById(R.id.txt_player_state);
            this.image = (AlbumArtImageView) dashboardWidgetView.findViewById(R.id.img_album_art);
        }
        super.setView(dashboardWidgetView);
    }

    public Drawable getDrawable() {
        return null;
    }

    protected void updateGauge() {
        this.image.setArtworkBitmap(this.albumArt, this.animateNextArtworkTransition);
        if (this.trackInfo == null || this.trackInfo == MusicManager.EMPTY_TRACK) {
            this.titleText.setText("");
            this.artistText.setText("");
            this.stateText.setVisibility(View.INVISIBLE);
            return;
        }
        if (MusicManager.tryingToPlay(this.trackInfo.playbackState)) {
            this.image.setAlpha(1.0f);
            this.stateText.setVisibility(View.INVISIBLE);
        } else {
            this.image.setAlpha(0.5f);
            this.stateText.setVisibility(View.VISIBLE);
        }
        updateMetadata();
    }

    private void updateMetadata() {
        CharSequence currentTitle = this.titleText.getText();
        if (this.trackInfo.name == null || !this.trackInfo.name.equals(currentTitle)) {
            this.titleText.setText(this.trackInfo.name);
        }
        CharSequence currentArtist = this.artistText.getText();
        if (this.trackInfo.author == null || !this.trackInfo.author.equals(currentArtist)) {
            this.artistText.setText(this.trackInfo.author);
        }
    }

    public String getWidgetIdentifier() {
        logger.d("getWidgetIdentifier");
        return SmartDashWidgetManager.MUSIC_WIDGET_ID;
    }

    public String getWidgetName() {
        return this.musicGaugeName;
    }

    public void onTrackUpdated(MusicTrackInfo trackInfo, Set<MediaControl> set, boolean willOpenNotification) {
        this.trackInfo = trackInfo;
        if (willOpenNotification) {
            logger.d("onTrackUpdated, delayed!");
            this.handler.postDelayed(new Runnable() {
                public void run() {
                    MusicWidgetPresenter.this.reDraw();
                }
            }, 250);
            return;
        }
        logger.d("onTrackUpdated (immediate)");
        reDraw();
    }

    @Override
    public void onAlbumArtUpdate(@Nullable final ByteString byteString, final boolean animateNextArtworkTransition) {
        MusicWidgetPresenter.logger.d("onAlbumArtUpdate " + byteString + ", " + animateNextArtworkTransition);
        this.animateNextArtworkTransition = animateNextArtworkTransition;
        if (byteString == null) {
            MusicWidgetPresenter.logger.v("ByteString image to set in notification is null");
            this.resetArtwork();
            this.reDraw();
        }
        else {
            final String hex = byteString.md5().hex();
            if (TextUtils.equals(this.lastAlbumArtHash, (CharSequence)hex) && this.albumArt != null) {
                MusicWidgetPresenter.logger.d("Already have this artwork, ignoring");
            }
            else {
                this.lastAlbumArtHash = hex;
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        MusicWidgetPresenter.logger.d("PhotoUpdate runnable " + byteString);
                        final byte[] byteArray = byteString.toByteArray();
                        if (byteArray == null || byteArray.length <= 0) {
                            MusicWidgetPresenter.logger.i("Received photo has null or empty byte array");
                            MusicWidgetPresenter.this.resetArtwork();
                            MusicWidgetPresenter.this.redrawOnMainThread();
                        }
                        else {
                            final int dimensionPixelSize = HudApplication.getAppContext().getResources().getDimensionPixelSize(R.dimen.music_gauge_album_art_size);
                            Bitmap bitmap = null;
                            Bitmap decodeByteArray = null;
                            try {
                                final Bitmap bitmap2 = bitmap = (decodeByteArray = ScalingUtilities.decodeByteArray(byteArray, dimensionPixelSize, dimensionPixelSize, ScalingUtilities$ScalingLogic.FIT));
                                MusicWidgetPresenter.this.albumArt = ScalingUtilities.createScaledBitmap(bitmap2, dimensionPixelSize, dimensionPixelSize, ScalingUtilities$ScalingLogic.FIT);
                                decodeByteArray = bitmap2;
                                bitmap = bitmap2;
                                MusicWidgetPresenter.this.redrawOnMainThread();
                            }
                            catch (Exception ex) {
                                bitmap = decodeByteArray;
                                MusicWidgetPresenter.logger.e("Error updating the art work received ", ex);
                                bitmap = decodeByteArray;
                                MusicWidgetPresenter.this.resetArtwork();
                                bitmap = decodeByteArray;
                                MusicWidgetPresenter.this.redrawOnMainThread();
                            }
                            finally {
                                if (bitmap != null && bitmap != MusicWidgetPresenter.this.albumArt) {
                                    bitmap.recycle();
                                }
                            }
                        }
                    }
                }, 1);
            }
        }
    }

    private void resetArtwork() {
        this.albumArt = null;
        this.lastAlbumArtHash = null;
    }

    private void redrawOnMainThread() {
        this.handler.post(new Runnable() {
            public void run() {
                MusicWidgetPresenter.this.reDraw();
            }
        });
    }

    public void setWidgetVisibleToUser(boolean b) {
        logger.d("setWidgetVisibleToUser " + b);
        if (b) {
            logger.v("setWidgetVisibleToUser: add music update listener");
            this.musicManager.addMusicUpdateListener(this);
            this.trackInfo = this.musicManager.getCurrentTrack();
        } else {
            logger.v("setWidgetVisibleToUser: remove music update listener");
            this.musicManager.removeMusicUpdateListener(this);
        }
        super.setWidgetVisibleToUser(b);
    }
}
