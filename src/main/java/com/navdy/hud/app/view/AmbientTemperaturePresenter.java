package com.navdy.hud.app.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.navdy.hud.app.ExtensionsKt;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnitChanged;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.hud.app.view.drawable.EngineTemperatureDrawable;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.inject.Inject;

import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import mortar.Mortar;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0006\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 R2\u00020\u0001:\u0001RB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\n\u0010C\u001a\u0004\u0018\u00010DH\u0016J\b\u0010E\u001a\u00020\u0018H\u0016J\b\u0010F\u001a\u00020\u0018H\u0016J\b\u0010G\u001a\u000206H\u0014J\u0010\u0010H\u001a\u00020I2\u0006\u0010J\u001a\u00020KH\u0007J\u001c\u0010L\u001a\u00020I2\b\u0010M\u001a\u0004\u0018\u00010N2\b\u0010O\u001a\u0004\u0018\u00010PH\u0016J\b\u0010Q\u001a\u00020IH\u0014R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\b\"\u0004\b\r\u0010\nR\u001a\u0010\u000e\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\b\"\u0004\b\u0010\u0010\nR\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001a\u0010\u0017\u001a\u00020\u0018X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u001e\u0010\u001f\u001a\u00020 8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R$\u0010'\u001a\u00020&2\u0006\u0010%\u001a\u00020&@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u001c\u0010,\u001a\u0004\u0018\u00010-X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R\u001a\u00102\u001a\u00020\u0018X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b3\u0010\u001a\"\u0004\b4\u0010\u001cR\u000e\u00105\u001a\u000206X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u00107\u001a\u000206X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u00108\u001a\u000209X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=R\u001a\u0010>\u001a\u000209X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b?\u0010;\"\u0004\b@\u0010=R\u0011\u0010A\u001a\u00020\u0018\u00a2\u0006\b\n\u0000\u001a\u0004\bB\u0010\u001a\u00a8\u0006S"}, d2 = {"Lcom/navdy/hud/app/view/EngineTemperaturePresenter;", "Lcom/navdy/hud/app/view/DashboardWidgetPresenter;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "STATE_COLD", "", "getSTATE_COLD", "()I", "setSTATE_COLD", "(I)V", "STATE_HOT", "getSTATE_HOT", "setSTATE_HOT", "STATE_NORMAL", "getSTATE_NORMAL", "setSTATE_NORMAL", "bus", "Lcom/squareup/otto/Bus;", "getBus", "()Lcom/squareup/otto/Bus;", "setBus", "(Lcom/squareup/otto/Bus;)V", "celsiusLabel", "", "getkPaLabel", "()Ljava/lang/String;", "setkPaLabel", "(Ljava/lang/String;)V", "getContext", "()Landroid/content/Context;", "driverProfileManager", "Lcom/navdy/hud/app/profile/DriverProfileManager;", "getDriverProfileManager", "()Lcom/navdy/hud/app/profile/DriverProfileManager;", "setDriverProfileManager", "(Lcom/navdy/hud/app/profile/DriverProfileManager;)V", "value", "", "engineTemperature", "getIntakePressure", "()D", "setIntakePressure", "(D)V", "engineTemperatureDrawable", "Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;", "getIntakePressureDrawable", "()Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;", "setIntakePressureDrawable", "(Lcom/navdy/hud/app/view/drawable/EngineTemperatureDrawable;)V", "fahrenheitLabel", "getPsiLabel", "setPsiLabel", "mLeftOriented", "", "registered", "tempText", "Landroid/widget/TextView;", "getTempText", "()Landroid/widget/TextView;", "setTempText", "(Landroid/widget/TextView;)V", "tempUnitText", "getTempUnitText", "setTempUnitText", "widgetNameString", "getWidgetNameString", "getDrawable", "Landroid/graphics/drawable/Drawable;", "getWidgetIdentifier", "getWidgetName", "isRegisteringToBusRequired", "onSpeedUnitChanged", "", "speedUnitChangedEvent", "Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;", "setView", "dashboardWidgetView", "Lcom/navdy/hud/app/view/DashboardWidgetView;", "arguments", "Landroid/os/Bundle;", "updateGauge", "Companion", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: EngineTemperaturePresenter.kt */
public final class AmbientTemperaturePresenter extends DashboardWidgetPresenter {
    public static final Companion Companion = new Companion();


    private static final String TEMPERATURE_GAUGE_COLD_KPA_OVERRIDE_PROPERTY = "persist.sys.at.cold";
    private static final String TEMPERATURE_GAUGE_HOT_KPA_OVERRIDE_PROPERTY = "persist.sys.at.hot";
    private static final String TEMPERATURE_GAUGE_LOWER_BOUND_KPA_OVERRIDE_PROPERTY = "persist.sys.at.lower";
    private static final String TEMPERATURE_GAUGE_MID_POINT_KPA_OVERRIDE_PROPERTY = "persist.sys.at.mid";
    private static final String TEMPERATURE_GAUGE_UPPER_BOUND_KPA_OVERRIDE_PROPERTY = "persist.sys.at.upper";


    private static final float DEFAULT_TEMPERATURE_GAUGE_GAUGE_COLD_KPA = -3f;
    private static final float DEFAULT_TEMPERATURE_GAUGE_HOT_KPA = 30.0f;
    private static final float DEFAULT_TEMPERATURE_GAUGE_LOWER_BOUND_KPA = -40.0f;
    private static final float DEFAULT_TEMPERATURE_GAUGE_MID_POINT_KPA = 0f;
    private static final float DEFAULT_TEMPERATURE_GAUGE_UPPER_BOUND_KPA = 50.0f;

    private static final double TEMPERATURE_GAUGE_COLD_THRESHOLD =  ((double) SystemProperties.getFloat(TEMPERATURE_GAUGE_COLD_KPA_OVERRIDE_PROPERTY, DEFAULT_TEMPERATURE_GAUGE_GAUGE_COLD_KPA));
    private static final double TEMPERATURE_GAUGE_HOT_THRESHOLD =  ((double) SystemProperties.getFloat(TEMPERATURE_GAUGE_HOT_KPA_OVERRIDE_PROPERTY, DEFAULT_TEMPERATURE_GAUGE_HOT_KPA));
    private static final double TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS =  ((double) SystemProperties.getFloat(TEMPERATURE_GAUGE_LOWER_BOUND_KPA_OVERRIDE_PROPERTY, DEFAULT_TEMPERATURE_GAUGE_LOWER_BOUND_KPA));
    private static final double TEMPERATURE_GAUGE_MID_POINT_CELSIUS =  ((double) SystemProperties.getFloat(TEMPERATURE_GAUGE_MID_POINT_KPA_OVERRIDE_PROPERTY, DEFAULT_TEMPERATURE_GAUGE_MID_POINT_KPA));
    private static final double TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS =  ((double) SystemProperties.getFloat(TEMPERATURE_GAUGE_UPPER_BOUND_KPA_OVERRIDE_PROPERTY, DEFAULT_TEMPERATURE_GAUGE_UPPER_BOUND_KPA));


    private int STATE_COLD = 1;
    private int STATE_HOT = 2;
    private int STATE_NORMAL;
    @Inject
    @NotNull
    public Bus bus;
    @NotNull
    private String celsiusLabel = "";
    @NotNull
    private final Context context;
    @Inject
    @NotNull
    public DriverProfileManager driverProfileManager;
    private double engineTemperature;
    @Nullable
    private EngineTemperatureDrawable engineTemperatureDrawable;
    @NotNull
    private String fahrenheitLabel = "";
    private boolean mLeftOriented = true;
    private boolean registered;
    @NotNull
    public TextView tempText;
    @NotNull
    public TextView tempUnitText;
    @NotNull
    private final String widgetNameString;

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u000b\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006R\u0014\u0010\t\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u0006R\u0014\u0010\u000b\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006R\u0014\u0010\r\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u0006\u00a8\u0006\u000f"}, d2 = {"Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;", "", "()V", "TEMPERATURE_GAUGE_COLD_THRESHOLD", "", "getTEMPERATURE_GAUGE_COLD_THRESHOLD", "()D", "TEMPERATURE_GAUGE_HOT_THRESHOLD", "getTEMPERATURE_GAUGE_HOT_THRESHOLD", "TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS", "getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS", "TEMPERATURE_GAUGE_MID_POINT_CELSIUS", "getTEMPERATURE_GAUGE_MID_POINT_CELSIUS", "TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS", "getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: EngineTemperaturePresenter.kt */
    public static final class Companion {
        private Companion() {
        }

//        public /* synthetic */ Companion(DefaultConstructorMarker $constructor_marker) {
//            this();
//        }

        public final double getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS() {
            return AmbientTemperaturePresenter.TEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS;
        }

        public final double getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS() {
            return AmbientTemperaturePresenter.TEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS;
        }

        public final double getTEMPERATURE_GAUGE_MID_POINT_CELSIUS() {
            return AmbientTemperaturePresenter.TEMPERATURE_GAUGE_MID_POINT_CELSIUS;
        }

        public final double getTEMPERATURE_GAUGE_COLD_THRESHOLD() {
            return AmbientTemperaturePresenter.TEMPERATURE_GAUGE_COLD_THRESHOLD;
        }

        public final double getTEMPERATURE_GAUGE_HOT_THRESHOLD() {
            return AmbientTemperaturePresenter.TEMPERATURE_GAUGE_HOT_THRESHOLD;
        }
    }

    public AmbientTemperaturePresenter(@NotNull Context context) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.context = context;
        this.engineTemperatureDrawable = new EngineTemperatureDrawable(this.context, R.array.smart_dash_engine_temperature_state_colors);
        if (this.engineTemperatureDrawable != null) {
            this.engineTemperatureDrawable.setMinValue((float) Companion.getTEMPERATURE_GAUGE_LOWER_BOUND_CELSIUS());
            this.engineTemperatureDrawable.setMaxGaugeValue((float) Companion.getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS());
        }
        Resources resources = this.context.getResources();
        String string = resources.getString(R.string.temperature_unit_fahrenheit);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.st\u2026perature_unit_fahrenheit)");
        this.fahrenheitLabel = string;
        string = resources.getString(R.string.temperature_unit_celsius);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.st\u2026temperature_unit_celsius)");
        this.celsiusLabel = string;
        Mortar.inject(HudApplication.getAppContext(), this);
        string = resources.getString(R.string.widget_ambient_air_temperature);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.st\u2026idget_engine_temperature)");
        this.widgetNameString = string;
    }

    @NotNull
    public final Context getContext() {
        return this.context;
    }

    @Nullable
    public final EngineTemperatureDrawable getEngineTemperatureDrawable() {
        return this.engineTemperatureDrawable;
    }

    public final void setEngineTemperatureDrawable(@Nullable EngineTemperatureDrawable set) {
        this.engineTemperatureDrawable = set;
    }

    public final int getSTATE_NORMAL() {
        return this.STATE_NORMAL;
    }

    public final void setSTATE_NORMAL(int set) {
        this.STATE_NORMAL = set;
    }

    public final int getSTATE_COLD() {
        return this.STATE_COLD;
    }

    public final void setSTATE_COLD(int set) {
        this.STATE_COLD = set;
    }

    public final int getSTATE_HOT() {
        return this.STATE_HOT;
    }

    public final void setSTATE_HOT(int set) {
        this.STATE_HOT = set;
    }

    @NotNull
    public final TextView getTempText() {
        TextView textView = this.tempText;
        if (textView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("tempText");
        }
        return textView;
    }

    public final void setTempText(@NotNull TextView set) {
        Intrinsics.checkParameterIsNotNull(set, "set");
        this.tempText = set;
    }

    @NotNull
    public final TextView getTempUnitText() {
        TextView textView = this.tempUnitText;
        if (textView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("tempUnitText");
        }
        return textView;
    }

    public final void setTempUnitText(@NotNull TextView set) {
        Intrinsics.checkParameterIsNotNull(set, "set");
        this.tempUnitText = set;
    }

    public final double getEngineTemperature() {
        return this.engineTemperature;
    }

    public final void setEngineTemperature(double value) {
        this.engineTemperature = value;
        reDraw();
    }

    @NotNull
    public final DriverProfileManager getDriverProfileManager() {
        DriverProfileManager driverProfileManager = this.driverProfileManager;
        if (driverProfileManager == null) {
            Intrinsics.throwUninitializedPropertyAccessException("driverProfileManager");
        }
        return driverProfileManager;
    }

    public final void setDriverProfileManager(@NotNull DriverProfileManager set) {
        Intrinsics.checkParameterIsNotNull(set, "set");
        this.driverProfileManager = set;
    }

    @NotNull
    public final Bus getBus() {
        Bus bus = this.bus;
        if (bus == null) {
            Intrinsics.throwUninitializedPropertyAccessException("bus");
        }
        return bus;
    }

    public final void setBus(@NotNull Bus set) {
        Intrinsics.checkParameterIsNotNull(set, "set");
        this.bus = set;
    }

    @NotNull
    public final String getFahrenheitLabel() {
        return this.fahrenheitLabel;
    }

    public final void setFahrenheitLabel(@NotNull String set) {
        Intrinsics.checkParameterIsNotNull(set, "set");
        this.fahrenheitLabel = set;
    }

    @NotNull
    public final String getCelsiusLabel() {
        return this.celsiusLabel;
    }

    public final void setCelsiusLabel(@NotNull String set) {
        Intrinsics.checkParameterIsNotNull(set, "set");
        this.celsiusLabel = set;
    }

    @NotNull
    public final String getWidgetNameString() {
        return this.widgetNameString;
    }

    @Nullable
    public Drawable getDrawable() {
        return this.engineTemperatureDrawable;
    }

    public void setView(@Nullable DashboardWidgetView dashboardWidgetView, @Nullable Bundle arguments) {
        int layoutResourceId = R.layout.engine_oil_temp_gauge_left;
        if (arguments != null) {
            switch (arguments.getInt(DashboardWidgetPresenter.EXTRA_GRAVITY)) {
                case 0:
                    layoutResourceId = R.layout.engine_oil_temp_gauge_left;
                    this.mLeftOriented = true;
                    break;
                case 2:
                    layoutResourceId = R.layout.engine_oil_temp_gauge_right;
                    this.mLeftOriented = false;
                    break;
            }
        }
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView(layoutResourceId);
            View findViewById = dashboardWidgetView.findViewById(R.id.txt_value);
            if (findViewById == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
            }
            this.tempText = (TextView) findViewById;
            findViewById = dashboardWidgetView.findViewById(R.id.txt_unit);
            if (findViewById == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
            }
            this.tempUnitText = (TextView) findViewById;
        }
        super.setView(dashboardWidgetView, arguments);
        reDraw();
    }

    protected boolean isRegisteringToBusRequired() {
        return true;
    }

    protected void updateGauge() {
        if (this.mWidgetView != null) {
            UnitSystem unitSystem = UnitSystem.UNIT_SYSTEM_METRIC;
            CharSequence valueOf;
            DriverProfileManager driverProfileManager = this.driverProfileManager;
            if (driverProfileManager == null) {
                Intrinsics.throwUninitializedPropertyAccessException("driverProfileManager");
            } else {
                DriverProfile currentProfile = driverProfileManager.getCurrentProfile();
                if (currentProfile != null) {
                    unitSystem = currentProfile.getUnitSystem();
                }
            }
            EngineTemperatureDrawable engineTemperatureDrawable = this.engineTemperatureDrawable;
            if (engineTemperatureDrawable != null) {
                engineTemperatureDrawable.setGaugeValue((float) ExtensionsKt.clamp(this.engineTemperature, Companion.getTEMPERATURE_GAUGE_COLD_THRESHOLD(), Companion.getTEMPERATURE_GAUGE_UPPER_BOUND_CELSIUS()));
                engineTemperatureDrawable.setLeftOriented(this.mLeftOriented);
            }
            int state = this.STATE_NORMAL;
            if (this.engineTemperature <= Companion.getTEMPERATURE_GAUGE_COLD_THRESHOLD()) {
                state = this.STATE_COLD;
            } else if (this.engineTemperature > Companion.getTEMPERATURE_GAUGE_HOT_THRESHOLD()) {
                state = this.STATE_HOT;
            }
            if (engineTemperatureDrawable != null) {
                engineTemperatureDrawable.setState(state);
            }
            TextView textView = this.tempText;
            if (textView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("tempText");
            } else {
                switch (unitSystem) {
                    case UNIT_SYSTEM_METRIC:
                        valueOf = String.valueOf((int) this.engineTemperature);
                        break;
                    case UNIT_SYSTEM_IMPERIAL:
                        valueOf = String.valueOf((int) ExtensionsKt.celsiusToFahrenheit(this.engineTemperature));
                        break;
                    default:
                        throw new NoWhenBranchMatchedException();
                }
                textView.setText(valueOf);
            }
            textView = this.tempUnitText;
            if (textView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("tempUnitText");
            } else {
                switch (unitSystem) {
                    case UNIT_SYSTEM_METRIC:
                        valueOf = this.celsiusLabel;
                        break;
                    case UNIT_SYSTEM_IMPERIAL:
                        valueOf = this.fahrenheitLabel;
                        break;
                    default:
                        throw new NoWhenBranchMatchedException();
                }
                textView.setText(valueOf);
            }
        }
    }

    @Subscribe
    public final void onSpeedUnitChanged(@NotNull SpeedUnitChanged speedUnitChangedEvent) {
        Intrinsics.checkParameterIsNotNull(speedUnitChangedEvent, "speedUnitChangedEvent");
        reDraw();
    }

    @NotNull
    public String getWidgetIdentifier() {
        return SmartDashWidgetManager.AMBIENT_TEMPERATURE_GAUGE_ID;
    }

    @NotNull
    public String getWidgetName() {
        return this.widgetNameString;
    }
}
