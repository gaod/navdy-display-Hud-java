package com.navdy.hud.app.view;

import android.content.Context;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.screen.TemperatureWarningScreen.Presenter;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout.Mode;
import com.navdy.service.library.events.input.Gesture;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import mortar.Mortar;

public class TemperatureWarningView extends RelativeLayout implements IListener, IInputHandler {
    private static final int WARNING_MESSAGE_MAX_WIDTH = 320;
    private static long lastDismissedTime = -1;
    private static final Logger sLogger = new Logger(TemperatureWarningView.class);
    @InjectView(R.id.choiceLayout)
    ChoiceLayout mChoiceLayout;
    @InjectView(R.id.image)
    ImageView mIcon;
    @InjectView(R.id.title2)
    TextView mMainTitleText;
    @Inject
    Presenter mPresenter;
    @InjectView(R.id.rightSwipe)
    ImageView mRightSwipe;
    @InjectView(R.id.mainTitle)
    TextView mScreenTitleText;
    @InjectView(R.id.title1)
    TextView mTitle1;
    @InjectView(R.id.title3)
    TextView mWarningMessage;

    public TemperatureWarningView(Context context) {
        super(context);
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
    }

    public TemperatureWarningView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        ToastManager.getInstance().disableToasts(true);
        this.mScreenTitleText.setText(R.string.temperature_title);
        this.mMainTitleText.setText(R.string.navdy_too_hot);
        this.mIcon.setImageResource(R.drawable.icon_warning_temperature);
        ((MaxWidthLinearLayout) findViewById(R.id.infoContainer)).setMaxWidth(320);
        this.mTitle1.setVisibility(GONE);
        this.mWarningMessage.setVisibility(View.VISIBLE);
        this.mWarningMessage.setText(R.string.avoid_overheating);
        this.mWarningMessage.setSingleLine(false);
        List<Choice> list = new ArrayList();
        list.add(new Choice(getContext().getString(R.string.dismiss), 0));
        this.mChoiceLayout.setChoices(Mode.LABEL, list, 0, this);
        this.mRightSwipe.setVisibility(View.VISIBLE);
        lastDismissedTime = 0;
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ToastManager.getInstance().disableToasts(false);
        lastDismissedTime = SystemClock.elapsedRealtime();
    }

    public void executeItem(int pos, int id) {
        this.mPresenter.finish();
    }

    public void itemSelected(int pos, int id) {
    }

    public boolean onGesture(GestureEvent event) {
        if (this.mRightSwipe == null || this.mRightSwipe.getVisibility() != 0 || event.gesture != Gesture.GESTURE_SWIPE_RIGHT) {
            return false;
        }
        this.mPresenter.finish();
        return true;
    }

    public boolean onKey(CustomKeyEvent event) {
        if (this.mChoiceLayout == null || this.mChoiceLayout.getVisibility() != 0 || event != CustomKeyEvent.SELECT) {
            return false;
        }
        this.mChoiceLayout.executeSelectedItem(true);
        return true;
    }

    public IInputHandler nextHandler() {
        return null;
    }

    public static long getLastDismissedTime() {
        return lastDismissedTime;
    }
}
