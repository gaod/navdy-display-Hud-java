package com.navdy.hud.app.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.glympse.android.hal.NotificationListener;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated;
import com.navdy.hud.app.analytics.TelemetrySession.InterestingEvent;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager;
import com.navdy.hud.app.view.drawable.DriveScoreGaugeDrawable;
import com.squareup.otto.Subscribe;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u001d\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\n\u0010P\u001a\u0004\u0018\u00010QH\u0016J\b\u0010R\u001a\u00020\u001cH\u0016J\b\u0010S\u001a\u00020\u001cH\u0016J\b\u0010T\u001a\u00020\u0007H\u0014J\u0010\u0010U\u001a\u00020V2\u0006\u0010'\u001a\u00020&H\u0007J\b\u0010W\u001a\u00020VH\u0016J\u001c\u0010X\u001a\u00020V2\b\u0010Y\u001a\u0004\u0018\u00010Z2\b\u0010[\u001a\u0004\u0018\u00010\\H\u0016J\u0010\u0010]\u001a\u00020V2\u0006\u0010^\u001a\u00020\u0007H\u0016J\u0006\u0010_\u001a\u00020VJ\u0006\u0010`\u001a\u00020VJ\b\u0010a\u001a\u00020VH\u0014R\u0014\u0010\t\u001a\u00020\nX\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u001a\u0010\r\u001a\u00020\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u00020\u0016X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u0011\u0010\u001b\u001a\u00020\u001c\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u001a\u0010\u001f\u001a\u00020 X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R$\u0010'\u001a\u00020&2\u0006\u0010%\u001a\u00020&@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u0011\u0010,\u001a\u00020-\u00a2\u0006\b\n\u0000\u001a\u0004\b.\u0010/R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b0\u00101R\u001e\u00102\u001a\u00020\u00072\u0006\u0010%\u001a\u00020\u0007@BX\u0082\u000e\u00a2\u0006\b\n\u0000\"\u0004\b3\u00104R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b5\u00106R\u001a\u00107\u001a\u00020 X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b8\u0010\"\"\u0004\b9\u0010$R\u0011\u0010:\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b;\u00101R\u0011\u0010<\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b=\u00101R\u0011\u0010>\u001a\u00020\u001c\u00a2\u0006\b\n\u0000\u001a\u0004\b?\u0010\u001eR\u0011\u0010@\u001a\u00020\u001c\u00a2\u0006\b\n\u0000\u001a\u0004\bA\u0010\u001eR\u0011\u0010B\u001a\u00020\u001c\u00a2\u0006\b\n\u0000\u001a\u0004\bC\u0010\u001eR\u0011\u0010D\u001a\u00020\u001c\u00a2\u0006\b\n\u0000\u001a\u0004\bE\u0010\u001eR\u0011\u0010F\u001a\u00020\u001c\u00a2\u0006\b\n\u0000\u001a\u0004\bG\u0010\u001eR\u0011\u0010H\u001a\u00020\u001c\u00a2\u0006\b\n\u0000\u001a\u0004\bI\u0010\u001eR\u001a\u0010J\u001a\u00020KX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bL\u0010M\"\u0004\bN\u0010O\u00a8\u0006b"}, d2 = {"Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;", "Lcom/navdy/hud/app/view/DashboardWidgetPresenter;", "context", "Landroid/content/Context;", "layoutId", "", "showScore", "", "(Landroid/content/Context;IZ)V", "CLEAR_WARNING_TIME_MS", "", "getCLEAR_WARNING_TIME_MS", "()J", "clearWarning", "Ljava/lang/Runnable;", "getClearWarning", "()Ljava/lang/Runnable;", "setClearWarning", "(Ljava/lang/Runnable;)V", "getContext", "()Landroid/content/Context;", "driveScoreGaugeDrawable", "Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;", "getDriveScoreGaugeDrawable", "()Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;", "setDriveScoreGaugeDrawable", "(Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;)V", "driveScoreGaugeName", "", "getDriveScoreGaugeName", "()Ljava/lang/String;", "driveScoreText", "Landroid/widget/TextView;", "getDriveScoreText", "()Landroid/widget/TextView;", "setDriveScoreText", "(Landroid/widget/TextView;)V", "value", "Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;", "driveScoreUpdated", "getDriveScoreUpdated", "()Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;", "setDriveScoreUpdated", "(Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;)V", "handler", "Landroid/os/Handler;", "getHandler", "()Landroid/os/Handler;", "getLayoutId", "()I", "newEvent", "setNewEvent", "(Z)V", "getShowScore", "()Z", "summaryText", "getSummaryText", "setSummaryText", "summaryTextColorNormal", "getSummaryTextColorNormal", "summaryTextColorWarning", "getSummaryTextColorWarning", "summaryTextDriveScore", "getSummaryTextDriveScore", "summaryTextExcessiveSpeeding", "getSummaryTextExcessiveSpeeding", "summaryTextHardAcceleration", "getSummaryTextHardAcceleration", "summaryTextHardBraking", "getSummaryTextHardBraking", "summaryTextHighG", "getSummaryTextHighG", "summaryTextSpeeding", "getSummaryTextSpeeding", "warningImage", "Landroid/widget/ImageView;", "getWarningImage", "()Landroid/widget/ImageView;", "setWarningImage", "(Landroid/widget/ImageView;)V", "getDrawable", "Landroid/graphics/drawable/Drawable;", "getWidgetIdentifier", "getWidgetName", "isRegisteringToBusRequired", "onDriveScoreUpdated", "", "onResume", "setView", "dashboardWidgetView", "Lcom/navdy/hud/app/view/DashboardWidgetView;", "arguments", "Landroid/os/Bundle;", "setWidgetVisibleToUser", "b", "showHighG", "showSpeeding", "updateGauge", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: DriveScoreGaugePresenter.kt */
public final class DriveScoreGaugePresenter extends DashboardWidgetPresenter {
    private final long CLEAR_WARNING_TIME_MS = 2000;
    @NotNull
    private Runnable clearWarning;
    @NotNull
    private final Context context;
    @NotNull
    private DriveScoreGaugeDrawable driveScoreGaugeDrawable;
    @NotNull
    private final String driveScoreGaugeName;
    @NotNull
    public TextView driveScoreText;
    @NotNull
    private DriveScoreUpdated driveScoreUpdated;
    @NotNull
    private final Handler handler;
    private final int layoutId;
    private boolean newEvent;
    private final boolean showScore;
    @NotNull
    public TextView summaryText;
    private final int summaryTextColorNormal;
    private final int summaryTextColorWarning;
    @NotNull
    private final String summaryTextDriveScore;
    @NotNull
    private final String summaryTextExcessiveSpeeding;
    @NotNull
    private final String summaryTextHardAcceleration;
    @NotNull
    private final String summaryTextHardBraking;
    @NotNull
    private final String summaryTextHighG;
    @NotNull
    private final String summaryTextSpeeding;
    @NotNull
    public ImageView warningImage;

    public DriveScoreGaugePresenter(@NotNull Context context, int layoutId, boolean showScore) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.context = context;
        this.layoutId = layoutId;
        this.showScore = showScore;
        DriveScoreGaugeDrawable driveScoreGaugeDrawable = this.driveScoreGaugeDrawable;
        if (driveScoreGaugeDrawable != null) {
            driveScoreGaugeDrawable.setMinValue(0.0f);
        }
        driveScoreGaugeDrawable = this.driveScoreGaugeDrawable;
        if (driveScoreGaugeDrawable != null) {
            driveScoreGaugeDrawable.setMaxGaugeValue(100.0f);
        }
        this.handler = new Handler(Looper.getMainLooper());
        Resources resources = this.context.getResources();
        String string = resources.getString(R.string.drive_score);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.string.drive_score)");
        this.summaryTextDriveScore = string;
        string = resources.getString(R.string.hard_accel);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.string.hard_accel)");
        this.summaryTextHardAcceleration = string;
        string = resources.getString(R.string.hard_braking);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.string.hard_braking)");
        this.summaryTextHardBraking = string;
        string = resources.getString(R.string.speeding);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.string.speeding)");
        this.summaryTextSpeeding = string;
        string = resources.getString(R.string.widget_drive_score);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.string.widget_drive_score)");
        this.driveScoreGaugeName = string;
        string = resources.getString(R.string.excessive_speeding);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.string.excessive_speeding)");
        this.summaryTextExcessiveSpeeding = string;
        string = resources.getString(R.string.high_g);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.string.high_g)");
        this.summaryTextHighG = string;
        this.summaryTextColorNormal = resources.getColor(R.color.hud_white);
        this.summaryTextColorWarning = resources.getColor(R.color.driving_score_warning_color);
//        this.clearWarning = new DriveScoreGaugePresenter$clearWarning$1(this);
        this.clearWarning = new Runnable() {
            @Override
            public void run() {
                DriveScoreGaugePresenter.this.setNewEvent(false);
                DriveScoreGaugePresenter.this.reDraw();
            }
        };
        this.driveScoreUpdated = new DriveScoreUpdated(InterestingEvent.NONE, false, false, false, 100);
        this.driveScoreGaugeDrawable = new DriveScoreGaugeDrawable(this.context, R.array.drive_state_colors);
    }

    @NotNull
    public final Context getContext() {
        return this.context;
    }

    public final int getLayoutId() {
        return this.layoutId;
    }

    public final boolean getShowScore() {
        return this.showScore;
    }

    public final long getCLEAR_WARNING_TIME_MS() {
        return this.CLEAR_WARNING_TIME_MS;
    }

    @NotNull
    public final DriveScoreGaugeDrawable getDriveScoreGaugeDrawable() {
        return this.driveScoreGaugeDrawable;
    }

    public final void setDriveScoreGaugeDrawable(@NotNull DriveScoreGaugeDrawable set) {
        Intrinsics.checkParameterIsNotNull(set, "set");
        this.driveScoreGaugeDrawable = set;
    }

    @NotNull
    public final TextView getDriveScoreText() {
        TextView textView = this.driveScoreText;
        if (textView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
        }
        return textView;
    }

    public final void setDriveScoreText(@NotNull TextView set) {
        Intrinsics.checkParameterIsNotNull(set, "set");
        this.driveScoreText = set;
    }

    @NotNull
    public final TextView getSummaryText() {
        TextView textView = this.summaryText;
        if (textView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("summaryText");
        }
        return textView;
    }

    public final void setSummaryText(@NotNull TextView set) {
        Intrinsics.checkParameterIsNotNull(set, "set");
        this.summaryText = set;
    }

    @NotNull
    public final ImageView getWarningImage() {
        ImageView imageView = this.warningImage;
        if (imageView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("warningImage");
        }
        return imageView;
    }

    public final void setWarningImage(@NotNull ImageView set) {
        Intrinsics.checkParameterIsNotNull(set, "set");
        this.warningImage = set;
    }

    @NotNull
    public final Handler getHandler() {
        return this.handler;
    }

    @NotNull
    public final String getSummaryTextDriveScore() {
        return this.summaryTextDriveScore;
    }

    @NotNull
    public final String getSummaryTextHardAcceleration() {
        return this.summaryTextHardAcceleration;
    }

    @NotNull
    public final String getSummaryTextHardBraking() {
        return this.summaryTextHardBraking;
    }

    @NotNull
    public final String getSummaryTextHighG() {
        return this.summaryTextHighG;
    }

    @NotNull
    public final String getDriveScoreGaugeName() {
        return this.driveScoreGaugeName;
    }

    @NotNull
    public final String getSummaryTextSpeeding() {
        return this.summaryTextSpeeding;
    }

    @NotNull
    public final String getSummaryTextExcessiveSpeeding() {
        return this.summaryTextExcessiveSpeeding;
    }

    public final int getSummaryTextColorNormal() {
        return this.summaryTextColorNormal;
    }

    public final int getSummaryTextColorWarning() {
        return this.summaryTextColorWarning;
    }

    @NotNull
    public final Runnable getClearWarning() {
        return this.clearWarning;
    }

    public final void setClearWarning(@NotNull Runnable set) {
        Intrinsics.checkParameterIsNotNull(set, "set");
        this.clearWarning = set;
    }

    @NotNull
    public final DriveScoreUpdated getDriveScoreUpdated() {
        return this.driveScoreUpdated;
    }

    public final void setDriveScoreUpdated(@NotNull DriveScoreUpdated value) {
        Intrinsics.checkParameterIsNotNull(value, NotificationListener.INTENT_EXTRA_VALUE);
        this.driveScoreUpdated = value;
        if (Intrinsics.areEqual(value.getInterestingEvent(), InterestingEvent.NONE) ^ true) {
            setNewEvent(true);
        }
        reDraw();
    }

    private final void setNewEvent(boolean value) {
        this.newEvent = value;
    }

    @Subscribe
    public final void onDriveScoreUpdated(@NotNull DriveScoreUpdated driveScoreUpdated) {
        Intrinsics.checkParameterIsNotNull(driveScoreUpdated, "driveScoreUpdated");
        setDriveScoreUpdated(driveScoreUpdated);
    }

    protected boolean isRegisteringToBusRequired() {
        return true;
    }

    public void setView(@Nullable DashboardWidgetView dashboardWidgetView, @Nullable Bundle arguments) {
        int layoutResourceId = this.layoutId;
        if (dashboardWidgetView != null) {
            View findViewById;
            setDriveScoreUpdated(RemoteDeviceManager.getInstance().getTelemetryDataManager().getDriveScoreUpdatedEvent());
            setNewEvent(false);
            dashboardWidgetView.setContentView(layoutResourceId);
            if (this.showScore) {
                findViewById = dashboardWidgetView.findViewById(R.id.txt_value);
                if (findViewById == null) {
                    throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
                }
                this.driveScoreText = (TextView) findViewById;
            }
            findViewById = dashboardWidgetView.findViewById(R.id.txt_summary);
            if (findViewById == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
            }
            this.summaryText = (TextView) findViewById;
            findViewById = dashboardWidgetView.findViewById(R.id.img_warning);
            if (findViewById == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.widget.ImageView");
            }
            this.warningImage = (ImageView) findViewById;
        }
        super.setView(dashboardWidgetView, arguments);
    }

    @Nullable
    public Drawable getDrawable() {
        if (this.showScore) {
            return this.driveScoreGaugeDrawable;
        }
        return null;
    }

    protected void updateGauge() {
        this.logger.d("Update Drive score " + this.driveScoreUpdated);
        if (this.mWidgetView != null) {
            if (this.showScore) {
                this.driveScoreGaugeDrawable.setGaugeValue((float) this.driveScoreUpdated.getDriveScore());
            }
            TextView textView;
            ImageView imageView;
            TextView textView2;
            if (this.newEvent) {
                if (this.showScore) {
                    this.driveScoreGaugeDrawable.setState(1);
                    textView = this.driveScoreText;
                    if (textView == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
                    }
                    textView.setVisibility(View.INVISIBLE);
                }
                imageView = this.warningImage;
                if (imageView == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("warningImage");
                }
                imageView.setVisibility(View.VISIBLE);
                textView = this.summaryText;
                if (textView == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                }
                textView.setTextColor(this.summaryTextColorWarning);
                if (Intrinsics.areEqual(this.driveScoreUpdated.getInterestingEvent(), InterestingEvent.HARD_ACCELERATION)) {
                    imageView = this.warningImage;
                    if (imageView == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("warningImage");
                    }
                    imageView.setImageResource(R.drawable.accel);
                    textView2 = this.summaryText;
                    if (textView2 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                    }
                    textView2.setText(this.summaryTextHardAcceleration);
                } else if (Intrinsics.areEqual(this.driveScoreUpdated.getInterestingEvent(), InterestingEvent.HARD_BRAKING)) {
                    imageView = this.warningImage;
                    if (imageView == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("warningImage");
                    }
                    imageView.setImageResource(R.drawable.brake);
                    textView2 = this.summaryText;
                    if (textView2 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                    }
                    textView2.setText(this.summaryTextHardBraking);
                } else if (this.driveScoreUpdated.isDoingHighGManeuver()) {
                    showHighG();
                } else if (this.driveScoreUpdated.isSpeeding()) {
                    showSpeeding();
                }
                this.handler.removeCallbacks(this.clearWarning);
                this.handler.postDelayed(this.clearWarning, this.CLEAR_WARNING_TIME_MS);
            } else if (this.driveScoreUpdated.isDoingHighGManeuver()) {
                showHighG();
            } else if (this.driveScoreUpdated.isSpeeding()) {
                showSpeeding();
            } else {
                this.handler.removeCallbacks(this.clearWarning);
                if (this.showScore) {
                    this.driveScoreGaugeDrawable.setState(0);
                    textView = this.driveScoreText;
                    if (textView == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
                    }
                    textView.setVisibility(View.VISIBLE);
                    textView2 = this.driveScoreText;
                    if (textView2 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
                    }
                    textView2.setText(String.valueOf(this.driveScoreUpdated.getDriveScore()));
                    textView = this.summaryText;
                    if (textView == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                    }
                    textView.setTextColor(this.summaryTextColorNormal);
                    textView2 = this.summaryText;
                    if (textView2 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                    }
                    textView2.setText(this.summaryTextDriveScore);
                } else {
                    textView = this.summaryText;
                    if (textView == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("summaryText");
                    }
                    textView.setText(null);
                }
                imageView = this.warningImage;
                if (imageView == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("warningImage");
                }
                imageView.setVisibility(View.INVISIBLE);
            }
        }
    }

    public final void showHighG() {
        TextView textView;
        if (this.showScore) {
            this.driveScoreGaugeDrawable.setState(1);
            textView = this.driveScoreText;
            if (textView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
            }
            textView.setVisibility(View.INVISIBLE);
        }
        ImageView imageView = this.warningImage;
        if (imageView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("warningImage");
        }
        imageView.setVisibility(View.VISIBLE);
        textView = this.summaryText;
        if (textView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("summaryText");
        }
        textView.setTextColor(this.summaryTextColorWarning);
        imageView = this.warningImage;
        if (imageView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("warningImage");
        }
        imageView.setImageResource(R.drawable.high_g);
        TextView textView2 = this.summaryText;
        if (textView2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("summaryText");
        }
        textView2.setText(this.summaryTextHighG);
    }

    public final void showSpeeding() {
        TextView textView;
        if (this.showScore) {
            this.driveScoreGaugeDrawable.setState(1);
            textView = this.driveScoreText;
            if (textView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("driveScoreText");
            }
            textView.setVisibility(View.INVISIBLE);
        }
        ImageView imageView = this.warningImage;
        if (imageView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("warningImage");
        }
        imageView.setVisibility(View.VISIBLE);
        textView = this.summaryText;
        if (textView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("summaryText");
        }
        textView.setTextColor(this.summaryTextColorWarning);
        if (this.driveScoreUpdated.isExcessiveSpeeding()) {
            TextView textView2;
            imageView = this.warningImage;
            if (imageView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("warningImage");
            }
            imageView.setImageResource(R.drawable.excessive_speed);
            textView2 = this.summaryText;
            if (textView2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("summaryText");
            }
            textView2.setText(this.summaryTextExcessiveSpeeding);
            return;
        }
        imageView = this.warningImage;
        if (imageView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("warningImage");
        }
        imageView.setImageResource(R.drawable.speed);
        textView = this.summaryText;
        if (textView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("summaryText");
        }
        textView.setText(this.summaryTextSpeeding);
    }

    public void onResume() {
        setNewEvent(false);
        super.onResume();
    }

    public void setWidgetVisibleToUser(boolean b) {
        setNewEvent(false);
        super.setWidgetVisibleToUser(b);
    }

    @NotNull
    public String getWidgetIdentifier() {
        return SmartDashWidgetManager.DRIVE_SCORE_GAUGE_ID;
    }

    @NotNull
    public String getWidgetName() {
        return this.driveScoreGaugeName;
    }

//    static final class DriveScoreGaugePresenter$clearWarning$1 implements Runnable {
//        @Override
//        public final void run() {
//            this.this$0.setNewEvent(false);
//            this.this$0.reDraw();
//        }
//    }
}
