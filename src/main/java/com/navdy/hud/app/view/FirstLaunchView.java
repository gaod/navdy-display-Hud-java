package com.navdy.hud.app.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import butterknife.ButterKnife;
import butterknife.InjectView;

import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.screen.FirstLaunchScreen.Presenter;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import javax.inject.Inject;
import mortar.Mortar;

public class FirstLaunchView extends RelativeLayout implements IInputHandler {
    private static final Logger sLogger = new Logger(FirstLaunchView.class);
    @InjectView(R.id.firstLaunchLogo)
    public ImageView firstLaunchLogo;
    @Inject
    Presenter presenter;

    public FirstLaunchView(Context context) {
        this(context, null);
    }

    public FirstLaunchView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FirstLaunchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
    }

    protected void onAttachedToWindow() {
        sLogger.v("onAttachToWindow");
        super.onAttachedToWindow();
        ToastManager.getInstance().disableToasts(true);
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }

    protected void onDetachedFromWindow() {
        sLogger.v("onDetachToWindow");
        ToastManager.getInstance().disableToasts(false);
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
    }

    public boolean onGesture(GestureEvent event) {
        return true;
    }

    public boolean onKey(CustomKeyEvent event) {
        return true;
    }

    public IInputHandler nextHandler() {
        return null;
    }
}
