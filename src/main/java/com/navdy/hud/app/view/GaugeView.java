package com.navdy.hud.app.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;
import butterknife.InjectView;
import butterknife.Optional;
import com.navdy.hud.app.R;

public class GaugeView extends DashboardWidgetView {
    @InjectView(R.id.txt_unit)
    @Optional
    TextView mTvUnit;
    @InjectView(R.id.txt_value)
    TextView mTvValue;

    public GaugeView(Context context) {
        this(context, null);
    }

    public GaugeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GaugeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray customAttributes = context.obtainStyledAttributes(attrs, R.styleable.GaugeView, defStyleAttr, 0);
        if (customAttributes != null) {
            setValueTextSize(customAttributes.getDimensionPixelSize(R.styleable.GaugeView_value_text_size, 0));
            customAttributes.recycle();
        }
    }

    public void setValueTextSize(int pixelSize) {
        this.mTvValue.setTextSize(0, (float) pixelSize);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void setValueText(CharSequence text) {
        if (this.mTvValue != null) {
            this.mTvValue.setText(text);
        }
    }

    public void setUnitText(CharSequence text) {
        if (this.mTvUnit != null) {
            this.mTvUnit.setText(text);
        }
    }

    public TextView getUnitTextView() {
        return this.mTvUnit;
    }

    public TextView getValueTextView() {
        return this.mTvValue;
    }

    public void clear() {
        super.clear();
        this.mTvUnit.setText(null);
        this.mTvValue.setText(null);
    }

    public TextView getValueView() {
        return this.mTvValue;
    }

    public TextView getUnitView() {
        return this.mTvUnit;
    }
}
