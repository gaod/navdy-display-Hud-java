package com.navdy.hud.app.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager;

public class CompassPresenter extends DashboardWidgetPresenter implements SerialValueAnimator.SerialValueAnimatorAdapter {
    private SerialValueAnimator animator = new SerialValueAnimator(this, 50);
    private String compassGaugeName;
    CompassDrawable mCompassDrawable;
    float mHeadingAngle;

    public CompassPresenter(Context context) {
        this.mCompassDrawable = new CompassDrawable(context);
        this.mCompassDrawable.setMaxGaugeValue(360.0f);
        this.mCompassDrawable.setMinValue(0.0f);
        this.compassGaugeName = context.getResources().getString(R.string.widget_compass);
    }

    public void setView(DashboardWidgetView dashboardWidgetView) {
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView((int) R.layout.smart_dash_widget_circular_content_layout);
        }
        super.setView(dashboardWidgetView);
    }

    public Drawable getDrawable() {
        return this.mCompassDrawable;
    }

    public void setHeadingAngle(double angle) {
        if (!this.isDashActive || !this.isWidgetVisibleToUser || this.mWidgetView == null) {
            this.mHeadingAngle = (float) angle;
        } else if (angle == 0.0d) {
            this.mHeadingAngle = 0.0f;
            reDraw();
        } else {
            this.animator.setValue((float) (angle % 360.0d));
        }
    }

    protected void updateGauge() {
        this.mCompassDrawable.setGaugeValue(this.mHeadingAngle);
    }

    public String getWidgetIdentifier() {
        return SmartDashWidgetManager.COMPASS_WIDGET_ID;
    }

    public String getWidgetName() {
        return this.compassGaugeName;
    }

    public float getValue() {
        return this.mHeadingAngle;
    }

    public void setValue(float newValue) {
        this.mHeadingAngle = newValue;
        reDraw();
    }

    public void animationComplete(float newValue) {
    }
}
