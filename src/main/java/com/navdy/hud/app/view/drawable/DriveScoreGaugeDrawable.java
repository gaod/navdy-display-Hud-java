package com.navdy.hud.app.view.drawable;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import com.navdy.hud.app.R;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.ClosedRange;
import kotlin.ranges.IntRange;
import kotlin.ranges.RangesKt;

import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u000e\u0010\u0007\u001a\u00020\bX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\bX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"}, d2 = {"Lcom/navdy/hud/app/view/drawable/DriveScoreGaugeDrawable;", "Lcom/navdy/hud/app/view/drawable/GaugeDrawable;", "context", "Landroid/content/Context;", "stateColorTable", "", "(Landroid/content/Context;I)V", "START_ANGLE", "", "SWEEP_ANGLE", "backgroundColor", "driveScoreGauge_Width", "warningColor", "draw", "", "canvas", "Landroid/graphics/Canvas;", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: DriveScoreGaugeDrawable.kt */
public final class DriveScoreGaugeDrawable extends GaugeDrawable {
    private final float START_ANGLE;
    private final float SWEEP_ANGLE;
    private final int backgroundColor;
    private final Context context;
    private final int driveScoreGauge_Width;
    private final int warningColor;

    public DriveScoreGaugeDrawable(@NotNull Context context, int stateColorTable) {
        super(context, 0, stateColorTable);
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.context = context;
        this.START_ANGLE = 157.5f;
        this.SWEEP_ANGLE = 225.0f;
        this.driveScoreGauge_Width = this.context.getResources().getDimensionPixelSize(R.dimen.drive_score_gauge_width);
        this.mDefaultColor = this.mColorTable[0];
        this.warningColor = this.mColorTable[1];
        this.backgroundColor = this.mColorTable[2];
    }

    public void draw(@NotNull Canvas canvas) {
        Intrinsics.checkParameterIsNotNull(canvas, "canvas");
        super.draw(canvas);
        Rect bounds = getBounds();
        RectF driveScoreGaugeBounds = new RectF((float) bounds.left, (float) bounds.top, (float) bounds.right, (float) bounds.bottom);
        driveScoreGaugeBounds.inset(((float) this.driveScoreGauge_Width) / ((float) 2), ((float) this.driveScoreGauge_Width) / ((float) 2));
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setStrokeWidth((float) this.driveScoreGauge_Width);
        this.mPaint.setColor(this.backgroundColor);
        canvas.drawArc(driveScoreGaugeBounds, this.START_ANGLE, this.SWEEP_ANGLE, false, this.mPaint);
        int valueSweepAngle = (int) ((this.mValue / this.mMaxValue) * this.SWEEP_ANGLE);
        float f = this.mValue;
        if (RangesKt.intRangeContains(new IntRange(1, 99), f)) {
            this.mPaint.setColor(this.mDefaultColor);
            canvas.drawArc(driveScoreGaugeBounds, this.START_ANGLE, ((float) valueSweepAngle) - 5.0f, false, this.mPaint);
            this.mPaint.setColor(-16777216);
            canvas.drawArc(driveScoreGaugeBounds, (this.START_ANGLE + ((float) valueSweepAngle)) - 5.0f, 5.0f, false, this.mPaint);
        } else if (f == 100.0f) {
            this.mPaint.setColor(this.mDefaultColor);
            canvas.drawArc(driveScoreGaugeBounds, this.START_ANGLE, (float) valueSweepAngle, false, this.mPaint);
        }
    }
}
