package com.navdy.hud.app;

import com.navdy.service.library.events.MessageStore;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import com.navdy.service.library.events.audio.MusicCollectionRequest;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000\u001a\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0003\u001a\u00020\u0004*\u00020\u0004\u001a\u001a\u0010\u0005\u001a\u00020\u0004*\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0004\u001a\u001a\u0010\u0005\u001a\u00020\b*\u00020\b2\u0006\u0010\u0006\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\b¨\u0006\t" }, d2 = { "cacheKey", "", "Lcom/navdy/service/library/events/audio/MusicCollectionRequest;", "celsiusToFahrenheit", "", "clamp", "min", "max", "", "app_hudRelease" }, k = 2, mv = { 1, 1, 6 })
public final class ExtensionsKt
{
    @NotNull
    public static final String cacheKey(@NotNull MusicCollectionRequest musicCollectionRequest) {
        Intrinsics.checkParameterIsNotNull(musicCollectionRequest, "$receiver");
        musicCollectionRequest = MessageStore.<MusicCollectionRequest>removeNulls(musicCollectionRequest);
        return musicCollectionRequest.collectionSource + "#" + musicCollectionRequest.collectionType + "#" + musicCollectionRequest.collectionId + "#" + musicCollectionRequest.offset;
    }

    public static final double celsiusToFahrenheit(final double n) {
        return 9 * n / 5 + 32;
    }

    public static final double kPaToPsi(final double n) {
        return n * 0.145038;
    }


    public static final double clamp(final double n, double n2, final double n3) {
        if (n >= n2) {
            if (n > n3) {
                n2 = n3;
            }
            else {
                n2 = n;
            }
        }
        return n2;
    }

    public static final float clamp(final float n, float n2, final float n3) {
        if (n >= n2) {
            if (n > n3) {
                n2 = n3;
            }
            else {
                n2 = n;
            }
        }
        return n2;
    }
}
