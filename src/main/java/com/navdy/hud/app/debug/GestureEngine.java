package com.navdy.hud.app.debug;

import com.navdy.hud.app.R;
import com.navdy.service.library.task.TaskManager;
import butterknife.OnCheckedChanged;
import android.widget.CheckBox;
import android.view.View;
import butterknife.ButterKnife;
import mortar.Mortar;
import android.util.AttributeSet;
import android.content.Context;
import javax.inject.Inject;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import android.widget.LinearLayout;

public class GestureEngine extends LinearLayout
{
    @Inject
    GestureServiceConnector gestureServiceConnector;
    
    public GestureEngine(final Context context) {
        this(context, null);
    }
    
    public GestureEngine(final Context context, final AttributeSet set) {
        super(context, set);
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        ((CheckBox)this.findViewById(R.id.enable_gesture)).setChecked(this.gestureServiceConnector.isRunning());
    }
    
    @OnCheckedChanged({ R.id.enable_discrete_mode })
    public void onToggleDiscreteMode(final boolean discreteMode) {
        this.gestureServiceConnector.setDiscreteMode(discreteMode);
    }
    
    @OnCheckedChanged({ R.id.enable_gesture })
    void onToggleGesture(final boolean b) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                if (b) {
                    GestureEngine.this.gestureServiceConnector.start();
                }
                else {
                    GestureEngine.this.gestureServiceConnector.stop();
                }
            }
        }, 1);
    }
    
    @OnCheckedChanged({ R.id.enable_preview })
    public void onTogglePreview(final boolean b) {
        this.gestureServiceConnector.enablePreview(b);
    }
}
