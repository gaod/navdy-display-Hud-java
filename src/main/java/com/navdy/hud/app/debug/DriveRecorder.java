package com.navdy.hud.app.debug;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.text.TextUtils;
import android.view.KeyEvent;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated;
import com.navdy.hud.app.analytics.TelemetrySession;
import com.navdy.hud.app.debug.IRouteRecorder.Stub;
import com.navdy.hud.app.device.gps.CalibratedGForceData;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.MapEvents$MapEngineInitialize;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.obd.IPidListener;
import com.navdy.obd.Pid;
import com.navdy.obd.PidSet;
import com.navdy.service.library.events.debug.StartDrivePlaybackEvent;
import com.navdy.service.library.events.debug.StartDriveRecordingEvent;
import com.navdy.service.library.events.debug.StopDrivePlaybackEvent;
import com.navdy.service.library.events.debug.StopDriveRecordingEvent;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;

public class DriveRecorder implements ServiceConnection, SerialExecutor {
    private static final String AUTO_DRIVERECORD_PROP = "persist.sys.driverecord.auto";
    public static final String AUTO_RECORD_LABEL = "auto";
    public static final int BUFFER_SIZE = 500;
    public static final int BUFFER_SIZE_OBD_LOG = 250;
    public static final String DEMO_LOG_FILE_NAME = "demo_log.log";
    public static final String DRIVE_LOGS_FOLDER = "drive_logs";
    public static final int MAXIMUM_FILES_IN_THE_FOLDER = 20;
    public static final Logger sLogger = new Logger(DriveRecorder.class);
    Bus bus;
    private ConnectionHandler connectionHandler;
    private final Context context = HudApplication.getAppContext();
    private volatile int currentObdDataInjectionIndex;
    boolean demoObdLogFileExists = false;
    boolean demoRouteLogFileExists = false;
    AsyncBufferedFileWriter driveScoreDataBufferedFileWriter;
    private StringBuilder gforceDataBuilder = new StringBuilder(40);
    private final Handler handler = new Handler(Looper.getMainLooper());
    private final Runnable injectFakeObdData = new Runnable() {
        public void run() {
            if (DriveRecorder.this.recordedObdData.size() > DriveRecorder.this.currentObdDataInjectionIndex + 1) {
                Entry<Long, List<Pid>> data = (Entry) DriveRecorder.this.recordedObdData.get(DriveRecorder.this.currentObdDataInjectionIndex);
                long timeStamp = ((Long) data.getKey()).longValue();
                ObdManager.getInstance().injectObdData((List) data.getValue(), (List) data.getValue());
                if (DriveRecorder.this.recordedObdData.size() > DriveRecorder.this.currentObdDataInjectionIndex + 1) {
                    DriveRecorder.this.obdDataPlaybackHandler.postDelayed(DriveRecorder.this.injectFakeObdData, ((Long) ((Entry) DriveRecorder.this.recordedObdData.get(DriveRecorder.access$204(DriveRecorder.this))).getKey()).longValue() - timeStamp);
                }
            } else if (DriveRecorder.this.isLooping) {
                DriveRecorder.this.currentObdDataInjectionIndex = 0;
                DriveRecorder.this.obdDataPlaybackHandler.post(DriveRecorder.this.injectFakeObdData);
            } else {
                DriveRecorder.sLogger.d("Done playing the recorded data. Stopping the playback");
                DriveRecorder.this.stopPlayback();
            }
        }
    };
    private boolean isEngineInitialized;
    private volatile boolean isLooping = false;
    private volatile boolean isRecording;
    private volatile boolean isSupportedPidsWritten;
    AsyncBufferedFileWriter obdAsyncBufferedFileWriter;
    private Handler obdDataPlaybackHandler;
    private HandlerThread obdPlaybackThread;
    private final IPidListener pidListener = new IPidListener() {
        public void pidsRead(List<Pid> list, List<Pid> pidsChanged) throws RemoteException {
            if (pidsChanged != null && DriveRecorder.this.isRecording) {
                if (!DriveRecorder.this.isSupportedPidsWritten) {
                    ObdManager.getInstance().setRecordingPidListener(null);
                    try {
                        DriveRecorder.this.bRecordSupportedPids();
                    } catch (Throwable th) {
                        DriveRecorder.sLogger.d("Error while recording the supported PIDs");
                    }
                    ObdManager.getInstance().setRecordingPidListener(DriveRecorder.this.pidListener);
                }
                DriveRecorder.this.obdAsyncBufferedFileWriter.write(System.currentTimeMillis() + HereManeuverDisplayBuilder.COMMA);
                for (Pid pid : pidsChanged) {
                    DriveRecorder.this.obdAsyncBufferedFileWriter.write(pid.getId() + GlanceConstants.COLON_SEPARATOR + pid.getValue());
                    if (pid != pidsChanged.get(pidsChanged.size() - 1)) {
                        DriveRecorder.this.obdAsyncBufferedFileWriter.write(HereManeuverDisplayBuilder.COMMA);
                    } else {
                        DriveRecorder.this.obdAsyncBufferedFileWriter.write(GlanceConstants.NEWLINE);
                    }
                }
            }
        }

        public void pidsChanged(List<Pid> list) throws RemoteException {
        }

        public void onConnectionStateChange(int newState) throws RemoteException {
        }

        public IBinder asBinder() {
            return null;
        }
    };
    private volatile State playbackState = State.STOPPED;
    private volatile long preparedFileLastModifiedTime;
    private volatile String preparedFileName;
    volatile boolean realObdConnected = false;
    private List<Entry<Long, List<Pid>>> recordedObdData;
    private IRouteRecorder routeRecorder;

    public enum Action {
        PRELOAD(0),
        PLAY(1),
        PAUSE(2),
        RESUME(3),
        RESTART(4),
        STOP(5);

        private int value;
        Action(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    public enum DemoPreference {
        NA(0),
        DISABLED(1),
        ENABLED(2);

        private int value;
        DemoPreference(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    public static class FilesModifiedTimeComparator implements Comparator<File> {
        public int compare(File file, File t1) {
            return Long.compare(file.lastModified(), t1.lastModified());
        }
    }

    public enum State {
        PLAYING(0),
        PAUSED(1),
        STOPPED(2);

        private int value;
        State(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    static /* synthetic */ int access$204(DriveRecorder x0) {
        int i = x0.currentObdDataInjectionIndex + 1;
        x0.currentObdDataInjectionIndex = i;
        return i;
    }

    public void onServiceConnected(ComponentName name, IBinder service) {
        this.routeRecorder = Stub.asInterface(service);
    }

    public void onServiceDisconnected(ComponentName name) {
        this.routeRecorder = null;
    }

    public void execute(Runnable runnable) {
        TaskManager.getInstance().execute(runnable, 9);
    }

    public void stopRecording() {
        if (this.isRecording) {
            sLogger.d("Stopping the Obd recording");
            this.handler.post(new Runnable() {
                public void run() {
                    DriveRecorder.this.isRecording = false;
                    DriveRecorder.this.isSupportedPidsWritten = false;
                    ObdManager.getInstance().setRecordingPidListener(null);
                    DriveRecorder.this.obdAsyncBufferedFileWriter.flushAndClose();
                    DriveRecorder.this.driveScoreDataBufferedFileWriter.flushAndClose();
                }
            });
            return;
        }
        sLogger.v("already stopped, no-op");
    }

    public boolean bPrepare(String s) {
        boolean b = false;
        java.io.File a = new java.io.File(com.navdy.hud.app.debug.DriveRecorder.getDriveLogsDir(s), new StringBuilder().append(s).append(".obd").toString());
        boolean b0 = a.exists();
        label0: {
            java.io.BufferedReader a0 = null;
            Throwable a1 = null;
            label1: {
                java.io.BufferedReader a2 = null;
                label5: {
                    if (b0) {
                        com.navdy.obd.PidSet a3 = null;
                        long j = a.lastModified();
                        boolean b1 = android.text.TextUtils.equals((CharSequence)this.preparedFileName, (CharSequence)s);
                        label6: {
                            if (!b1) {
                                break label6;
                            }
                            if (this.preparedFileLastModifiedTime != j) {
                                break label6;
                            }
                            sLogger.d("File already prepared");
                            b = true;
                            break label0;
                        }
                        this.preparedFileName = s;
                        this.preparedFileLastModifiedTime = j;
                        label4: {
                            String[] a4 = null;
                            int i = 0;
                            int i0 = 0;
                            label3: try {
                                a0 = null;
                                a0 = null;
                                a0 = null;
                                a0 = null;
                                java.io.FileInputStream a5 = new java.io.FileInputStream(a);
                                a0 = null;
                                java.io.InputStreamReader a6 = new java.io.InputStreamReader((java.io.InputStream)a5, "utf-8");
                                a0 = null;
                                a2 = new java.io.BufferedReader((java.io.Reader)a6);
                                com.navdy.service.library.log.Logger a7 = sLogger;
                                try {
                                    a7.v("startPlayback of Obd data");
                                    this.recordedObdData = (java.util.List)new java.util.ArrayList();
                                    a0 = new java.io.BufferedReader((java.io.Reader)new java.io.InputStreamReader((java.io.InputStream)new java.io.FileInputStream(a), "utf-8"));
                                } catch(Throwable a8) {
                                    a1 = a8;
                                    break label5;
                                }
                                String s0 = a0.readLine();
                                a3 = new com.navdy.obd.PidSet();
                                if (s0 == null) {
                                    break label4;
                                }
                                sLogger.d(new StringBuilder().append("Supported pids recorded ").append(s0).toString());
                                boolean b2 = s0.trim().equals("-1");
                                label2: {
                                    if (b2) {
                                        break label2;
                                    }
                                    a4 = s0.split(",");
                                    i = a4.length;
                                    i0 = 0;
                                    break label3;
                                }
                                sLogger.d("There are no supported pids when trying to playback the data");
                                break label4;
                            } catch(Throwable a9) {
                                a1 = a9;
                                break label1;
                            }
                            while(i0 < i) {
                                String s1 = a4[i0];
                                try {
                                    try {
                                        a3.add(Integer.parseInt(s1));
                                    } catch(Exception ignoredException) {
                                        sLogger.e("Exception while parsing the supported Pids");
                                    }
                                    i0 = i0 + 1;
                                } catch(Throwable a10) {
                                    a1 = a10;
                                    break label1;
                                }
                            }
                        }
                        try {
                            com.navdy.hud.app.obd.ObdManager.getInstance().setSupportedPidSet(a3);
                            while(true) {
                                NumberFormatException a11 = null;
                                String s2 = a0.readLine();
                                if (s2 == null) {
                                    break;
                                }
                                String[] a12 = s2.split(",");
                                if (a12 == null) {
                                    continue;
                                }
                                if (a12.length <= 1) {
                                    continue;
                                }
                                try {
                                    long j0 = Long.parseLong(a12[0]);
                                    java.util.ArrayList a13 = new java.util.ArrayList();
                                    int i1 = 1;
                                    while(i1 < a12.length) {
                                        String[] a14 = a12[i1].split(":");
                                        int i2 = Integer.parseInt(a14[0]);
                                        double d = Double.parseDouble(a14[1]);
                                        com.navdy.obd.Pid a15 = new com.navdy.obd.Pid(i2);
                                        a15.setValue(d);
                                        ((java.util.List)a13).add(a15);
                                        this.recordedObdData.add((Entry<Long, List<Pid>>) new SimpleEntry(Long.valueOf(j0), a13));
                                        i1 = i1 + 1;
                                    }
                                    continue;
                                } catch(NumberFormatException a16) {
                                    a11 = a16;
                                }
                                sLogger.e("Error parsing the Obd data from the file ", (Throwable)a11);
                            }
                        } catch(Throwable a17) {
                            a1 = a17;
                            break label1;
                        }
                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                        b = true;
                        break label0;
                    } else {
                        b = false;
                        break label0;
                    }
                }
                a0 = a2;
            }
            try {
                this.stopPlayback();
                sLogger.e(a1);
            } catch(Throwable a18) {
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                throw a18;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
            b = false;
        }
        return b;
    }

    public void prepare(final String fileName) {
        if (isPlaying() || isPaused()) {
            sLogger.e("Cannot prepare for playback as the playback is in progress");
        } else {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    if (DriveRecorder.this.bPrepare(fileName)) {
                        DriveRecorder.sLogger.d("Prepare succeeded");
                    } else {
                        DriveRecorder.sLogger.e("Prepare failed");
                    }
                }
            }, 9);
        }
    }

    public void startPlayback(final String fileName, boolean loop) {
        if (isPlaying() || this.isRecording) {
            sLogger.v("already busy, no-op");
            return;
        }
        sLogger.d("Starting the playback");
        this.playbackState = State.PLAYING;
        this.isLooping = loop;
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (DriveRecorder.this.bPrepare(fileName)) {
                    DriveRecorder.sLogger.d("Prepare succeeded");
                    try {
                        DriveRecorder.this.currentObdDataInjectionIndex = 0;
                        DriveRecorder.this.obdPlaybackThread = new HandlerThread("ObdPlayback");
                        DriveRecorder.this.obdPlaybackThread.start();
                        DriveRecorder.this.obdDataPlaybackHandler = new Handler(DriveRecorder.this.obdPlaybackThread.getLooper());
                        DriveRecorder.this.obdDataPlaybackHandler.post(DriveRecorder.this.injectFakeObdData);
                        return;
                    } catch (Throwable t) {
                        DriveRecorder.this.stopPlayback();
                        DriveRecorder.sLogger.e(t);
                        return;
                    }
                }
                DriveRecorder.sLogger.e("Failed to prepare for playback , file : " + fileName);
                DriveRecorder.this.stopPlayback();
            }
        }, 9);
    }

    public void stopPlayback() {
        if (isPlaying()) {
            sLogger.d("Stopping the playback");
            this.currentObdDataInjectionIndex = 0;
            if (this.obdDataPlaybackHandler != null) {
                this.obdDataPlaybackHandler.removeCallbacks(this.injectFakeObdData);
            }
            if (this.obdPlaybackThread != null && this.obdPlaybackThread.isAlive()) {
                sLogger.v("obd handler thread quit:" + this.obdPlaybackThread.quit());
            }
            this.obdPlaybackThread = null;
            this.isLooping = false;
            this.playbackState = State.STOPPED;
            return;
        }
        sLogger.v("already stopped, no-op");
    }

    public void release() {
        stopPlayback();
        if (this.recordedObdData != null) {
            this.recordedObdData = null;
        }
        this.preparedFileName = null;
        this.preparedFileLastModifiedTime = -1;
    }

    public void pausePlayback() {
        if (isPlaying()) {
            sLogger.d("Pausing the playback");
            this.obdDataPlaybackHandler.removeCallbacks(this.injectFakeObdData);
            this.playbackState = State.PAUSED;
            return;
        }
        sLogger.d("Playback is not happening so not pausing");
    }

    public void resumePlayback() {
        if (isPaused()) {
            this.playbackState = State.PLAYING;
            this.obdDataPlaybackHandler.post(this.injectFakeObdData);
        }
    }

    public boolean restartPlayback() {
        if (!isPlaying() && !isPaused()) {
            return false;
        }
        this.playbackState = State.PLAYING;
        this.currentObdDataInjectionIndex = 0;
        this.obdDataPlaybackHandler.removeCallbacks(this.injectFakeObdData);
        this.obdDataPlaybackHandler.post(this.injectFakeObdData);
        return true;
    }

    public void flushRecordings() {
        this.handler.post(new Runnable() {
            public void run() {
                if (DriveRecorder.this.isRecording) {
                    DriveRecorder.this.obdAsyncBufferedFileWriter.flush();
                    DriveRecorder.this.driveScoreDataBufferedFileWriter.flush();
                }
            }
        });
    }

    public DriveRecorder(Bus bus, ConnectionHandler connectionHandler) {
        this.connectionHandler = connectionHandler;
        this.isRecording = false;
        this.playbackState = State.STOPPED;
        this.isSupportedPidsWritten = false;
        this.bus = bus;
        this.bus.register(this);
        ObdManager.getInstance().setDriveRecorder(this);
        this.context.bindService(new Intent(this.context, RouteRecorderService.class), this, 1);
    }

    public void startRecording(String label) {
        if (this.isRecording || isPlaying()) {
            sLogger.v("Obd data recorder is already busy");
            return;
        }
        sLogger.d("Starting the recording");
        this.isRecording = true;
        this.isSupportedPidsWritten = false;
        TaskManager.getInstance().execute(createDriveRecord(label), 9);
    }

    private Runnable createDriveRecord(final String fileName) {
        return new Runnable() {
            public void run() {
                File driveLogsDir = DriveRecorder.getDriveLogsDir(fileName);
                if (!driveLogsDir.exists()) {
                    driveLogsDir.mkdirs();
                }
                File driveLogFile = new File(driveLogsDir, fileName);
                File obdDataLogFile = new File(driveLogFile.getAbsolutePath() + ".obd");
                File gforceLogFile = new File(driveLogFile.getAbsolutePath() + ".drive");
                try {
                    if (obdDataLogFile.createNewFile()) {
                        DriveRecorder.this.obdAsyncBufferedFileWriter = new AsyncBufferedFileWriter(obdDataLogFile.getAbsolutePath(), DriveRecorder.this, 250);
                        ObdManager.getInstance().setRecordingPidListener(null);
                        DriveRecorder.this.bRecordSupportedPids();
                        ObdManager.getInstance().setRecordingPidListener(DriveRecorder.this.pidListener);
                    }
                    if (gforceLogFile.createNewFile()) {
                        DriveRecorder.this.driveScoreDataBufferedFileWriter = new AsyncBufferedFileWriter(gforceLogFile.getAbsolutePath(), DriveRecorder.this, 500);
                    }
                } catch (IOException e) {
                    DriveRecorder.sLogger.e(e);
                    DriveRecorder.this.isRecording = false;
                    DriveRecorder.this.isSupportedPidsWritten = false;
                }
            }
        };
    }

    private void bRecordSupportedPids() throws IOException {
        ObdManager obdManager = ObdManager.getInstance();
        PidSet supportedPidSet = obdManager.getSupportedPids();
        if (supportedPidSet != null) {
            List<Pid> supportedPidsList = supportedPidSet.asList();
            if (supportedPidsList == null || supportedPidsList.size() <= 0) {
                this.obdAsyncBufferedFileWriter.write("-1\n");
            } else {
                for (Pid pid : supportedPidsList) {
                    if (pid != supportedPidsList.get(supportedPidsList.size() - 1)) {
                        this.obdAsyncBufferedFileWriter.write(pid.getId() + HereManeuverDisplayBuilder.COMMA);
                    } else {
                        this.obdAsyncBufferedFileWriter.write(pid.getId() + GlanceConstants.NEWLINE);
                    }
                }
            }
            this.obdAsyncBufferedFileWriter.write(System.currentTimeMillis() + HereManeuverDisplayBuilder.COMMA);
            this.obdAsyncBufferedFileWriter.write("47:" + obdManager.getFuelLevel() + HereManeuverDisplayBuilder.COMMA);
            this.obdAsyncBufferedFileWriter.write("13:" + SpeedManager.getInstance().getObdSpeed() + HereManeuverDisplayBuilder.COMMA);
            this.obdAsyncBufferedFileWriter.write("12:" + obdManager.getEngineRpm() + HereManeuverDisplayBuilder.COMMA);
            this.obdAsyncBufferedFileWriter.write("256:" + obdManager.getInstantFuelConsumption() + GlanceConstants.NEWLINE, true);
            this.isSupportedPidsWritten = true;
            sLogger.d("Obd data record created successfully and starting to listening to the PID changes");
        }
    }

    @Subscribe
    public void onKeyEvent(KeyEvent event) {
        if (event != null && event.getKeyCode() == 36 && event.getAction() == 1) {
            performDemoPlaybackAction(Action.STOP, false);
        } else if (event != null && event.getKeyCode() == 53 && event.getAction() == 1) {
            performDemoPlaybackAction(Action.RESTART, false);
        } else if (event != null && event.getKeyCode() == 44 && event.getAction() == 1) {
            performDemoPlaybackAction(Action.PRELOAD, false);
        }
    }

    @Subscribe
    public void onStartRecordingEvent(StartDriveRecordingEvent startRecordingEvent) {
        sLogger.d("Start recording event received");
        startRecording(startRecordingEvent.label);
    }

    @Subscribe
    public void onStopRecordingEvent(StopDriveRecordingEvent stopRecordingEvent) {
        sLogger.d("Stop recording event received");
        stopRecording();
    }

    @Subscribe
    public void onStartPlayback(StartDrivePlaybackEvent startPlayback) {
        sLogger.d("Start playback event received");
        startPlayback(startPlayback.label, false);
    }

    @Subscribe
    public void onStopPlayback(StopDrivePlaybackEvent stopDrivePlaybackEvent) {
        sLogger.d("Stop playback event received");
        stopPlayback();
    }

    @Subscribe
    public void onCalibratedGForceData(CalibratedGForceData calibratedGForceData) {
        if (this.isRecording && this.driveScoreDataBufferedFileWriter != null) {
            this.gforceDataBuilder.setLength(0);
            this.driveScoreDataBufferedFileWriter.write(this.gforceDataBuilder.append(System.currentTimeMillis()).append(HereManeuverDisplayBuilder.COMMA).append("G,").append(calibratedGForceData.getXAccel()).append(HereManeuverDisplayBuilder.COMMA).append(calibratedGForceData.getYAccel()).append(HereManeuverDisplayBuilder.COMMA).append(calibratedGForceData.getZAccel()).append(GlanceConstants.NEWLINE).toString());
        }
    }

    @Subscribe
    public void onDriveScoreUpdatedEvent(DriveScoreUpdated driveScoreUpdated) {
        if (this.isRecording && this.driveScoreDataBufferedFileWriter != null) {
            this.gforceDataBuilder.setLength(0);
            this.driveScoreDataBufferedFileWriter.write(this.gforceDataBuilder.append(System.currentTimeMillis()).append(HereManeuverDisplayBuilder.COMMA).append("D,").append("DS:").append(driveScoreUpdated.getDriveScore()).append(HereManeuverDisplayBuilder.COMMA).append("DE:").append(driveScoreUpdated.getInterestingEvent()).append(HereManeuverDisplayBuilder.COMMA).append("SD:").append(TelemetrySession.INSTANCE.getSessionDuration()).append(HereManeuverDisplayBuilder.COMMA).append("RD:").append(TelemetrySession.INSTANCE.getSessionRollingDuration()).append(HereManeuverDisplayBuilder.COMMA).append("SPD:").append(TelemetrySession.INSTANCE.getSpeedingDuration()).append(HereManeuverDisplayBuilder.COMMA).append("ESD:").append(TelemetrySession.INSTANCE.getExcessiveSpeedingDuration()).append(HereManeuverDisplayBuilder.COMMA).append("HA:").append(TelemetrySession.INSTANCE.getSessionHardAccelerationCount()).append(HereManeuverDisplayBuilder.COMMA).append("HB:").append(TelemetrySession.INSTANCE.getSessionHardBrakingCount()).append(HereManeuverDisplayBuilder.COMMA).append("HG:").append(TelemetrySession.INSTANCE.getSessionHighGCount()).append(HereManeuverDisplayBuilder.COMMA).append("LT:").append(getLatitude()).append(HereManeuverDisplayBuilder.COMMA).append("LN:").append(getLongitude()).append(GlanceConstants.NEWLINE).toString());
        }
    }

    private static double getLatitude() {
        if (HereMapsManager.getInstance().isInitialized()) {
            GeoCoordinate c = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
            if (c != null) {
                return c.getLatitude();
            }
        }
        return 0.0d;
    }

    private static double getLongitude() {
        if (HereMapsManager.getInstance().isInitialized()) {
            GeoCoordinate c = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
            if (c != null) {
                return c.getLongitude();
            }
        }
        return 0.0d;
    }

    @Subscribe
    public void onMapEngineInitialized(MapEvents$MapEngineInitialize mapEngineInitializedEvent) {
        sLogger.d("onMapEnigneInitialzed : message received. Initialized :" + mapEngineInitializedEvent.initialized);
        this.isEngineInitialized = mapEngineInitializedEvent.initialized;
        checkAndStartAutoRecording();
    }

    public void performDemoPlaybackAction(final Action action, final boolean loop) {
        sLogger.d(":performDemoPlaybackAction");
        if (this.isEngineInitialized) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    if (DriveRecorder.this.isDemoAvailable()) {
                        switch (action) {
                            case PLAY:
                                DriveRecorder.this.playDemo(loop);
                                return;
                            case PAUSE:
                                DriveRecorder.this.pauseDemo();
                                return;
                            case RESUME:
                                DriveRecorder.this.resumeDemo();
                                return;
                            case RESTART:
                                DriveRecorder.this.restartDemo(loop);
                                return;
                            case STOP:
                                DriveRecorder.this.stopDemo();
                                return;
                            case PRELOAD:
                                DriveRecorder.this.prepareDemo();
                                return;
                            default:
                                return;
                        }
                    }
                }
            }, 1);
        } else {
            sLogger.e("Engine is not initialized so can not start the playback");
        }
    }

    public void load() {
        sLogger.d("Loading the Obd Data recorder");
        this.demoRouteLogFileExists = new File(PathManager.getInstance().getMapsPartitionPath() + File.separator + DEMO_LOG_FILE_NAME).exists();
        sLogger.d("Demo file exists : " + this.demoRouteLogFileExists);
        this.demoObdLogFileExists = new File(PathManager.getInstance().getMapsPartitionPath() + File.separator + DEMO_LOG_FILE_NAME + ".obd").exists();
        sLogger.d("Demo obd file exists : " + this.demoObdLogFileExists);
    }

    public boolean isDemoAvailable() {
        if (this.demoRouteLogFileExists && !this.realObdConnected) {
            return true;
        }
        sLogger.d(!this.demoRouteLogFileExists ? "Demo log file does not exist in the map partition. So Demo preference : NA" : "Obd has been connected so skipping the demo");
        return false;
    }

    private void checkAndStartAutoRecording() {
        if (isAutoRecordingEnabled()) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    DriveRecorder.sLogger.d("Auto recording is enabled so starting the auto record");
                    File[] files = new File(PathManager.getInstance().getMapsPartitionPath() + File.separator + "drive_logs").listFiles();
                    if (files != null && files.length > 20) {
                        Arrays.sort(files, new FilesModifiedTimeComparator());
                        for (int i = 0; i < files.length - 20; i++) {
                            File lastFile = files[i];
                            DriveRecorder.sLogger.d("Deleting the drive record " + lastFile.getAbsolutePath() + " as its old ");
                            IOUtils.deleteFile(HudApplication.getAppContext(), lastFile.getAbsolutePath());
                        }
                    }
                    DriveRecorder.this.connectionHandler.sendLocalMessage(new StartDriveRecordingEvent("auto"));
                }
            }, 9);
        }
    }

    public void setRealObdConnected(boolean realObdConnected) {
        this.realObdConnected = realObdConnected;
        sLogger.d("setRealObdConnected : state " + realObdConnected);
        if (this.realObdConnected) {
            performDemoPlaybackAction(Action.STOP, true);
        }
    }

    public static File getDriveLogsDir(String fileName) {
        if (DEMO_LOG_FILE_NAME.equals(fileName)) {
            return new File(PathManager.getInstance().getMapsPartitionPath());
        }
        return new File(PathManager.getInstance().getMapsPartitionPath() + File.separator + "drive_logs");
    }

    public static boolean isAutoRecordingEnabled() {
        return SystemProperties.getBoolean(AUTO_DRIVERECORD_PROP, false) || !DeviceUtil.isUserBuild();
    }

    private boolean isPlaying() {
        return this.playbackState == State.PLAYING;
    }

    private boolean isPaused() {
        return this.playbackState == State.PAUSED;
    }

    private boolean isStopped() {
        return this.playbackState == State.STOPPED;
    }

    public boolean isDemoPlaying() {
        try {
            return isPlaying() || this.routeRecorder.isPlaying();
        } catch (Throwable e) {
            sLogger.e(e);
            return false;
        }
    }

    public boolean isDemoPaused() {
        try {
            return isPaused() || this.routeRecorder.isPaused();
        } catch (Throwable e) {
            sLogger.e(e);
            return false;
        }
    }

    public boolean isDemoStopped() {
        try {
            return isStopped() && this.routeRecorder.isStopped();
        } catch (Throwable e) {
            sLogger.e(e);
            return false;
        }
    }

    private void prepareDemo() {
        prepare(DEMO_LOG_FILE_NAME);
        try {
            this.routeRecorder.prepare(DEMO_LOG_FILE_NAME, false);
        } catch (Throwable e) {
            sLogger.e("Error preparing the route recorder for demo", e);
        }
    }

    private void playDemo(boolean loop) {
        stopRecordingBeforeDemo();
        if (!isPlaying()) {
            if (isPaused()) {
                resumePlayback();
            } else {
                startPlayback(DEMO_LOG_FILE_NAME, loop);
            }
        }
        try {
            if (this.routeRecorder.isPaused()) {
                this.routeRecorder.resumePlayback();
            } else if (!this.routeRecorder.isPlaying()) {
                this.routeRecorder.startPlayback(DEMO_LOG_FILE_NAME, false, loop);
            }
        } catch (Throwable e) {
            sLogger.e("Error playing the route playback", e);
        }
    }

    private void stopRecordingBeforeDemo() {
        if (this.isRecording) {
            stopRecording();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                sLogger.e("Interrupted ", e);
            }
        }
        try {
            if (this.routeRecorder.isRecording()) {
                this.routeRecorder.stopRecording();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e2) {
                    sLogger.e("Interrupted while stopping the recording");
                }
            }
        } catch (Exception e3) {
            sLogger.e("Exception while stopping the recording", e3);
        }
    }

    private void pauseDemo() {
        stopRecordingBeforeDemo();
        if (isPlaying()) {
            pausePlayback();
        }
        try {
            if (this.routeRecorder.isPlaying()) {
                this.routeRecorder.pausePlayback();
            }
        } catch (Exception e) {
            sLogger.e("Error pausing the route playback");
        }
    }

    private void stopDemo() {
        if (isPlaying()) {
            stopPlayback();
        }
        try {
            if (this.routeRecorder.isPlaying() || this.routeRecorder.isPaused()) {
                this.routeRecorder.stopPlayback();
            }
        } catch (Exception e) {
            sLogger.e("Error stopping the route playback", e);
        }
    }

    private void resumeDemo() {
        stopRecordingBeforeDemo();
        if (isPaused()) {
            resumePlayback();
        }
        try {
            if (this.routeRecorder.isPaused()) {
                this.routeRecorder.resumePlayback();
            }
        } catch (Exception e) {
            sLogger.e("Error resuming the route playback", e);
        }
    }

    private void restartDemo(boolean loop) {
        stopRecordingBeforeDemo();
        if (!restartPlayback()) {
            startPlayback(DEMO_LOG_FILE_NAME, loop);
        }
        try {
            if (!this.routeRecorder.restartPlayback()) {
                this.routeRecorder.startPlayback(DEMO_LOG_FILE_NAME, false, loop);
            }
        } catch (Exception e) {
            sLogger.e("Error restarting the route playback", e);
        }
    }
}