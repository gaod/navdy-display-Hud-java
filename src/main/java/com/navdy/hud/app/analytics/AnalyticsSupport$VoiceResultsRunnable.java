package com.navdy.hud.app.analytics;

class AnalyticsSupport$VoiceResultsRunnable implements Runnable {
    private com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction additionalResultsAction;
    private boolean done;
    private boolean explicitRetry;
    private Integer listItemSelected;
    private String microphoneUsed;
    private String prefix;
    private com.navdy.service.library.events.navigation.NavigationRouteRequest request;
    private int retryCount;
    private Integer searchConfidence;
    private java.util.List searchResults;
    private String searchWords;
    
    AnalyticsSupport$VoiceResultsRunnable(com.navdy.service.library.events.navigation.NavigationRouteRequest a) {
        this.listItemSelected = null;
        this.prefix = null;
        this.done = false;
        this.request = a;
        this.searchWords = com.navdy.hud.app.analytics.AnalyticsSupport.access$3500();
        this.searchConfidence = com.navdy.hud.app.analytics.AnalyticsSupport.access$3600();
        this.retryCount = com.navdy.hud.app.analytics.AnalyticsSupport.access$3700() - 1;
        this.microphoneUsed = com.navdy.hud.app.analytics.AnalyticsSupport.access$3800() ? "bluetooth" : "phone";
        this.searchResults = com.navdy.hud.app.analytics.AnalyticsSupport.access$3900();
        this.additionalResultsAction = com.navdy.hud.app.analytics.AnalyticsSupport.access$4000();
        this.explicitRetry = com.navdy.hud.app.analytics.AnalyticsSupport.access$4100();
        this.listItemSelected = com.navdy.hud.app.analytics.AnalyticsSupport.access$4200();
        this.prefix = com.navdy.hud.app.analytics.AnalyticsSupport.access$4300();
    }
    
    public void cancel(com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason a) {
        synchronized(this) {
            if (!this.done) {
                com.navdy.hud.app.analytics.AnalyticsSupport.access$200().removeCallbacks(this);
                com.navdy.hud.app.analytics.AnalyticsSupport.access$4502(null);
                this.done = true;
                com.navdy.hud.app.analytics.AnalyticsSupport.access$0001().i("cancelling voice search reason: " + a + " data:" + this);
                com.navdy.hud.app.analytics.AnalyticsSupport.access$4400(a, this.request, this.searchWords, this.searchConfidence, this.retryCount, this.microphoneUsed, this.searchResults, this.additionalResultsAction, this.explicitRetry, this.listItemSelected, this.prefix);
            }
        }
        /*monexit(this)*/
    }
    
    public void run() {
        synchronized(this) {
            if (!this.done) {
                com.navdy.hud.app.analytics.AnalyticsSupport.access$0001().i("sending voice search event: " + this);
                com.navdy.hud.app.analytics.AnalyticsSupport.access$4400(null, this.request, this.searchWords, this.searchConfidence, this.retryCount, this.microphoneUsed, this.searchResults, this.additionalResultsAction, this.explicitRetry, this.listItemSelected, this.prefix);
                com.navdy.hud.app.analytics.AnalyticsSupport.access$4502(null);
                this.done = true;
            }
        }
        /*monexit(this)*/
    }
    
    public String toString() {
        return "VoiceResultsRunnable{done=" + this.done + ", request=" + this.request + ", searchWords='" + this.searchWords + (char) 39 + ", searchConfidence=" + this.searchConfidence + ", retryCount=" + this.retryCount + ", microphoneUsed='" + this.microphoneUsed + (char) 39 + ", searchResults=" + this.searchResults + ", additionalResultsAction=" + this.additionalResultsAction + ", explicitRetry=" + this.explicitRetry + ", listItemSelected=" + this.listItemSelected + ", prefix='" + this.prefix + (char) 39 + (char) 125;
    }
}
