package com.navdy.hud.app.service;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.SystemClock;
import android.text.TextUtils;
import com.navdy.hud.app.debug.RouteRecorder;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.device.gps.GpsManager;
import com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.IAPListenerReceiver;
import com.navdy.hud.app.service.DeviceSearch.EventSink;
import com.navdy.hud.app.util.BluetoothUtil;
import com.navdy.hud.app.util.CrashReportService;
import com.navdy.hud.app.util.CrashReportService$CrashType;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.hud.device.connection.EASessionSocketAdapter;
import com.navdy.hud.device.connection.iAP2Link;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.device.connection.AcceptorListener;
import com.navdy.service.library.device.connection.Connection.DisconnectCause;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.device.connection.ConnectionListener;
import com.navdy.service.library.device.connection.ConnectionService;
import com.navdy.service.library.device.connection.ConnectionService.State;
import com.navdy.service.library.device.connection.ConnectionType;
import com.navdy.service.library.device.connection.ProxyService;
import com.navdy.service.library.device.connection.tunnel.Tunnel;
import com.navdy.service.library.device.discovery.BTDeviceBroadcaster;
import com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster;
import com.navdy.service.library.device.discovery.TCPRemoteDeviceBroadcaster;
import com.navdy.service.library.device.link.Link;
import com.navdy.service.library.device.link.LinkManager;
import com.navdy.service.library.device.link.LinkManager$LinkFactory;
import com.navdy.service.library.events.DeviceInfo.Platform;
import com.navdy.service.library.events.Ext_NavdyEvent;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.NavdyEvent.MessageType;
import com.navdy.service.library.events.NavdyEventUtil;
import com.navdy.service.library.events.audio.AudioStatus;
import com.navdy.service.library.events.audio.AudioStatus.ProfileType;
import com.navdy.service.library.events.connection.ConnectionRequest;
import com.navdy.service.library.events.connection.ConnectionRequest.Action;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState;
import com.navdy.service.library.events.connection.ConnectionStatus;
import com.navdy.service.library.events.connection.ConnectionStatus.Status;
import com.navdy.service.library.events.debug.StartDrivePlaybackEvent;
import com.navdy.service.library.events.debug.StartDriveRecordingEvent;
import com.navdy.service.library.events.file.FileTransferData;
import com.navdy.service.library.events.file.FileTransferRequest;
import com.navdy.service.library.events.file.FileTransferStatus;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.network.BTSocketAcceptor;
import com.navdy.service.library.network.BTSocketFactory;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.network.SocketFactory;
import com.navdy.service.library.network.TCPSocketAcceptor;
import com.navdy.service.library.network.TCPSocketFactory;
import com.navdy.service.library.util.NetworkActivityTracker;
import com.squareup.wire.Message;
import com.squareup.wire.Wire;
import java.io.IOException;
import java.util.UUID;

import static com.navdy.service.library.device.connection.ConnectionService.State.*;

public class HudConnectionService extends ConnectionService {
    public static final String ACTION_DEVICE_FORCE_RECONNECT = "com.navdy.hud.app.force_reconnect";
    private static final String APP_TERMINATION_RECONNECT_DELAY_MS = "persist.sys.app_reconnect_ms";
    private static final String AUTO_DRIVERECORD_PROP = "persist.sys.driverecord.auto";
    public static final String EXTRA_HUD_FORCE_RECONNECT_REASON = "force_reconnect_reason";
    private static final int PROXY_ENTRY_LOCAL_PORT = 3000;
    public static final String REASON_CONNECTION_DISCONNECTED = "CONNECTION_DISCONNECTED";
    static Boolean sendingPhoneLocation = false;
    private BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
        private BluetoothDevice cancelledDevice;
        private long cancelledTime;

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice bluetoothDevice = intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            if (action.equals("android.bluetooth.device.action.BOND_STATE_CHANGED")) {
                int intExtra = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", Integer.MIN_VALUE);
                HudConnectionService.this.logger.d("Bond state change - device:" + bluetoothDevice + " state:" + intExtra);
                if (intExtra != 11) {
                    return;
                }
                if ((HudConnectionService.this.search != null && HudConnectionService.this.search.forgetDevice(bluetoothDevice)) || (HudConnectionService.this.mRemoteDevice != null && HudConnectionService.this.mRemoteDevice.getDeviceId().getBluetoothAddress().equals(bluetoothDevice.getAddress()))) {
                    HudConnectionService.this.logger.i("Unexpected bonding with known device - removing bond");
                    HudConnectionService.this.forgetPairedDevice(bluetoothDevice);
                    BluetoothUtil.cancelBondProcess(bluetoothDevice);
                    BluetoothUtil.removeBond(bluetoothDevice);
                    this.cancelledDevice = bluetoothDevice;
                    this.cancelledTime = SystemClock.elapsedRealtime();
                    HudConnectionService.this.serviceHandler.removeCallbacks(HudConnectionService.this.reconnectRunnable);
                    HudConnectionService.this.setActiveDevice(null);
                }
            } else if (action.equals("android.bluetooth.device.action.PAIRING_REQUEST")) {
                if (bluetoothDevice.equals(this.cancelledDevice) && SystemClock.elapsedRealtime() - this.cancelledTime < 250) {
                    HudConnectionService.this.logger.d("Aborting pairing request for cancelled bonding");
                    abortBroadcast();
                }
                this.cancelledDevice = null;
            } else if (action.equals("android.bluetooth.device.action.ACL_DISCONNECTED") && HudConnectionService.this.search != null) {
                HudConnectionService.this.search.handleDisconnect(bluetoothDevice);
            }
        }
    };
    private BroadcastReceiver debugReceiver;
    private ReconnectRunnable deviceReconnectRunnable = new ReconnectRunnable();
    private FileTransferHandler fileTransferHandler;
    private final GpsManager gpsManager = GpsManager.getInstance();
    private LinkManager$LinkFactory iAPLinkManager$LinkFactory = new LinkManager$LinkFactory() {
        public Link build(ConnectionInfo connectionInfo) {
            return new iAP2Link(new IAPListenerReceiver(), HudConnectionService.this.getApplicationContext());
        }
    };
    private boolean isSimulatingGpsCoordinates;
    private boolean needAutoSearch = true;
    private final RouteRecorder routeRecorder = RouteRecorder.getInstance();
    /* access modifiers changed from: private */
    public DeviceSearch search;

    public class ReconnectRunnable implements Runnable {
        private String reason;

        public ReconnectRunnable() {
        }

        public void run() {
            HudConnectionService.this.reconnect(this.reason);
        }

        public void setReason(String str) {
            this.reason = str;
        }
    }

    private boolean parseStartDrivePlaybackEvent(byte[] bArr) {
        boolean z = false;
        try {
            StartDrivePlaybackEvent messageFromEvent =(StartDrivePlaybackEvent) NavdyEventUtil.messageFromEvent(this.mWire.parseFrom(bArr, NavdyEvent.class));
            String str = messageFromEvent.label;
            if (messageFromEvent.playSecondaryLocation != null) {
                z = messageFromEvent.playSecondaryLocation;
            }
            this.routeRecorder.startPlayback(str, z, false);
            this.logger.v("onStartDrivePlayback, name=" + messageFromEvent.label);
        } catch (Throwable th) {
            this.logger.e(th);
        }
        return true;
    }

    private boolean parseStartDriveRecordingEvent(byte[] bArr) {
        try {
            StartDriveRecordingEvent messageFromEvent = (StartDriveRecordingEvent) NavdyEventUtil.messageFromEvent(this.mWire.parseFrom(bArr, NavdyEvent.class));
            if (messageFromEvent != null) {
                String startRecording = this.routeRecorder.startRecording(messageFromEvent.label, false);
                if (!TextUtils.isEmpty(startRecording)) {
                    forwardEventLocally(new StartDriveRecordingEvent(startRecording));
                    this.logger.v("onStartDriveRecording, name=" + messageFromEvent.label);
                } else {
                    this.logger.e("startDriveRecording event fileName is empty");
                }
            } else {
                this.logger.e("startDriveRecording event is null");
            }
        } catch (Throwable th) {
            this.logger.e(th);
        }
        return true;
    }

    private void selectDevice(NavdyDeviceId navdyDeviceId) {
        if (this.search != null) {
            this.logger.i("Connecting to " + navdyDeviceId);
            RemoteDevice select = this.search.select(navdyDeviceId);
            if (select != null) {
                this.logger.i("Found corresponding remote device:" + select);
                setActiveDevice(select);
                return;
            }
            return;
        }
        ConnectionInfo findDevice = this.deviceRegistry.findDevice(navdyDeviceId);
        if (findDevice != null) {
            this.logger.i("Found corresponding connection info:" + findDevice);
            connect(findDevice);
        }
    }

    public void broadcastReconnectingIntent(String str) {
        Intent intent = new Intent(ACTION_DEVICE_FORCE_RECONNECT);
        intent.putExtra(EXTRA_HUD_FORCE_RECONNECT_REASON, str);
        sendBroadcast(intent);
    }

    /* access modifiers changed from: protected */
    public Tunnel createProxyService() {
        return new Tunnel(new TCPSocketAcceptor(3000, true), new SocketFactory() {
            public SocketAdapter build() throws IOException {
                if (iAP2Link.proxyEASession != null) {
                    return new EASessionSocketAdapter(iAP2Link.proxyEASession);
                }
                if (HudConnectionService.this.mRemoteDevice != null) {
                    return new BTSocketFactory(HudConnectionService.this.mRemoteDevice.getDeviceId().getBluetoothAddress(), ConnectionService.NAVDY_PROXY_TUNNEL_UUID).build();
                }
                throw new IOException("can't create proxy tunnel because HUD is not connected to remote device");
            }
        });
    }

    @Override
    public Tunnel createObdService() {
        UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");  // common Serial profile uuid
        final HudConnectionService a = this;
        return new Tunnel(
                new BTSocketAcceptor("NavdyObd", uuid, false),
                new TCPSocketFactory("localhost", 6500));
    }

    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.navdy.service.library.device.connection.ConnectionService, com.navdy.hud.app.service.HudConnectionService] */
    /* access modifiers changed from: protected */
    public void enterState(ConnectionService.State state) {
        switch (state) {
            case START:
                break;
            case IDLE:
                if (this.search != null) {
                    setState(SEARCHING);
                }
                break;
            case SEARCHING:
                if (isPromiscuous()) {
                    startBroadcasters();
                }
                startListeners();
                if (this.search == null) {
                    this.search = new DeviceSearch(this, new DeviceSearch.EventSink() {
                        public void onEvent(Message message) {
                            if (message instanceof com.navdy.service.library.events.connection.ConnectionStatus) {
                                ConnectionStatus a0 = (ConnectionStatus) message;
                                if (a0.status == Status.CONNECTION_SEARCH_FINISHED) {
                                    if (HudConnectionService.this.state == SEARCHING) {
                                        if (HudConnectionService.this.search != null) {
                                            HudConnectionService.this.search.close();
                                            HudConnectionService.this.search = null;
                                        }
                                        HudConnectionService.this.setState(IDLE);
                                        message = a0;
                                    }
                                }
                            }
                            HudConnectionService.this.forwardEventLocally(message);
//
//                                if ((message instanceof ConnectionStatus) && ((ConnectionStatus) message).status == Status.CONNECTION_SEARCH_FINISHED && HudConnectionService.this.state == SEARCHING) {
//                                if (HudConnectionService.this.search != null) {
//                                    HudConnectionService.this.search.close();
//                                    HudConnectionService.this.search = null;
//                                }
//                                HudConnectionService.this.setState(IDLE);
//                            }
//                            HudConnectionService.this.forwardEventLocally(message);
                        }
                    }, this.inProcess);
                    this.search.next();
                }
                if (this.mRemoteDevice != null && (this.mRemoteDevice.isConnected() || this.mRemoteDevice.isConnecting())) {
                    setState(DISCONNECTING);
                }
                break;
            case CONNECTED:
                this.needAutoSearch = false;
                if (this.search != null) {
                    this.search.close();
                    this.search = null;
                }
                if (this.mRemoteDevice != null && this.deviceRegistry.findDevice(this.mRemoteDevice.getDeviceId()) == null) {
                    ConnectionServiceAnalyticsSupport.recordNewDevice();
                }
                break;
            case CONNECTING:
            case RECONNECTING:
                this.needAutoSearch = false;
                break;
            case DISCONNECTING:
                break;
            case DISCONNECTED:
                break;
            case PAIRING:
                break;
            case LISTENING:
                break;
            case DESTROYED:
                break;
        }
        HudConnectionService.super.enterState(state);
    }

    /* access modifiers changed from: protected */
    public void exitState(ConnectionService.State state) {
        HudConnectionService.super.exitState(state);
        if (state == START) {
            IntentFilter intentFilter = new IntentFilter("android.bluetooth.device.action.PAIRING_REQUEST");
            intentFilter.setPriority(1);
            intentFilter.addAction("android.bluetooth.device.action.ACL_DISCONNECTED");
            intentFilter.addAction("android.bluetooth.device.action.BOND_STATE_CHANGED");
            registerReceiver(this.bluetoothReceiver, intentFilter);
        }
    }

    /* access modifiers changed from: protected */
    public void forgetPairedDevice(BluetoothDevice bluetoothDevice) {
        if (this.search != null) {
            this.search.forgetDevice(bluetoothDevice);
        }
        HudConnectionService.super.forgetPairedDevice(bluetoothDevice);
    }

    /* access modifiers changed from: protected */
    public ConnectionListener[] getConnectionListeners(Context context) {
        if (DeviceUtil.isNavdyDevice()) {
            return new ConnectionListener[]{
                    new AcceptorListener(context, new BTSocketAcceptor("Navdy", NAVDY_PROTO_SERVICE_UUID), ConnectionType.BT_PROTOBUF),
                    new AcceptorListener(context, new BTSocketAcceptor("Navdy iAP", ACCESSORY_IAP2), ConnectionType.BT_IAP2_LINK)
            };
        }
        return new ConnectionListener[]{
                new AcceptorListener(context, new TCPSocketAcceptor(21301), ConnectionType.TCP_PROTOBUF),
                new AcceptorListener(context, new BTSocketAcceptor("Navdy", NAVDY_PROTO_SERVICE_UUID), ConnectionType.BT_PROTOBUF)
        };
    }

    public Platform getDevicePlatform() {
        try {
            if (this.mRemoteDevice == null || !this.mRemoteDevice.isConnected()) {
                return null;
            }
            return this.mRemoteDevice.getDeviceInfo().platform;
        } catch (Throwable th) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public RemoteDeviceBroadcaster[] getRemoteDeviceBroadcasters() {
        if (DeviceUtil.isNavdyDevice()) {
            return new RemoteDeviceBroadcaster[]{new BTDeviceBroadcaster()};
        }
        return new RemoteDeviceBroadcaster[]{new BTDeviceBroadcaster(), new TCPRemoteDeviceBroadcaster(getApplicationContext())};
    }

    /* access modifiers changed from: protected */
    public void handleDeviceDisconnect(RemoteDevice remoteDevice, DisconnectCause disconnectCause) {
        HudConnectionService.super.handleDeviceDisconnect(remoteDevice, disconnectCause);
        this.logger.d("HUDConnectionService: handleDeviceDisconnect " + remoteDevice + ", Cause :" + disconnectCause);
        if (this.mRemoteDevice == remoteDevice && disconnectCause == DisconnectCause.ABORTED && this.serverMode) {
            this.logger.d("HUDConnectionService: Reporting the non fatal crash, Bluetooth disconnected");
            CrashReportService.dumpCrashReportAsync(CrashReportService$CrashType.BLUETOOTH_DISCONNECTED);
        }
    }

    /* access modifiers changed from: protected */
    public void heartBeat() {
        if (this.state == SEARCHING) {
            try {
                this.search.next();
            } catch (NullPointerException ignored) {}
        }
        HudConnectionService.super.heartBeat();
    }

    public boolean isPromiscuous() {
        return PowerManager.isAwake();
    }

    public boolean needAutoSearch() {
        return this.needAutoSearch && PowerManager.isAwake();
    }

    public void onCreate() {
        HudConnectionService.super.onCreate();
        LinkManager.registerFactory(ConnectionType.BT_IAP2_LINK, this.iAPLinkManager$LinkFactory);
        LinkManager.registerFactory(ConnectionType.EA_PROTOBUF, this.iAPLinkManager$LinkFactory);
        this.gpsManager.setConnectionService(this);
        this.routeRecorder.setConnectionService(this);
        this.serverMode = true;
        this.inProcess = false;
        NetworkActivityTracker.getInstance().start();
        this.fileTransferHandler = new FileTransferHandler(getApplicationContext());
        if (!DeviceUtil.isUserBuild()) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("LINK_BANDWIDTH_LEVEL_CHANGED");
            intentFilter.addCategory("NAVDY_LINK");
            this.debugReceiver = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    HudConnectionService.this.setBandwidthLevel(intent.getIntExtra("EXTRA_BANDWIDTH_MODE", 1));
                }
            };
            registerReceiver(this.debugReceiver, intentFilter);
        }
    }

    public void onDestroy() {
        HudConnectionService.super.onDestroy();
        this.routeRecorder.stopRecording(false);
        this.gpsManager.shutdown();
        unregisterReceiver(this.bluetoothReceiver);
        if (this.debugReceiver != null) {
            unregisterReceiver(this.debugReceiver);
        }
    }

    public void onDeviceConnected(RemoteDevice remoteDevice) {
        HudConnectionService.super.onDeviceConnected(remoteDevice);
        this.fileTransferHandler.onDeviceConnected(remoteDevice);
        startProxyService();
    }

    public void onDeviceDisconnected(RemoteDevice remoteDevice, DisconnectCause disconnectCause) {
        HudConnectionService.super.onDeviceDisconnected(remoteDevice, disconnectCause);
        this.fileTransferHandler.onDeviceDisconnected();
    }

    /* access modifiers changed from: protected */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean processEvent(byte[] bArr, MessageType messageType) {
        switch (messageType) {
            case Coordinate:
                try {
                    if (this.isSimulatingGpsCoordinates) {
                        return true;
                    }
                    this.gpsManager.feedLocation((Coordinate) NavdyEventUtil.messageFromEvent(this.mWire.parseFrom(bArr, NavdyEvent.class)));
                    return true;
                } catch (Throwable th) {
                    this.logger.e(th);
                    return true;
                }
            case StartDriveRecordingEvent:
                return parseStartDriveRecordingEvent(bArr);
            case StopDriveRecordingEvent:
                this.routeRecorder.stopRecording(false);
                this.logger.v("onStopDriveRecording");
                return true;
            case DriveRecordingsRequest:
                this.routeRecorder.requestRecordings();
                this.logger.v("onDriveRecordingsRequest");
                return true;
            case StartDrivePlaybackEvent:
                parseStartDrivePlaybackEvent(bArr);
                return false;
            case StopDrivePlaybackEvent:
                this.routeRecorder.stopPlayback();
                this.logger.v("onStopDrivePlayback");
                return false;
            case FileTransferRequest:
                try {
                    this.fileTransferHandler.onFileTransferRequest((FileTransferRequest)NavdyEventUtil.messageFromEvent(this.mWire.parseFrom(bArr, NavdyEvent.class)));
                    return true;
                } catch (Throwable th2) {
                    this.logger.e(th2);
                    return true;
                }
            case FileTransferStatus:
                try {
                    this.fileTransferHandler.onFileTransferStatus((FileTransferStatus)NavdyEventUtil.messageFromEvent(this.mWire.parseFrom(bArr, NavdyEvent.class)));
                    return true;
                } catch (Throwable th3) {
                    this.logger.e(th3);
                    return true;
                }
            case FileTransferData:
                try {
                    this.fileTransferHandler.onFileTransferData((FileTransferData)NavdyEventUtil.messageFromEvent(this.mWire.parseFrom(bArr, NavdyEvent.class)));
                    return true;
                } catch (Throwable th4) {
                    this.logger.e(th4);
                    return true;
                }
            case ConnectionStateChange:
                try {
                    switch (((ConnectionStateChange)NavdyEventUtil.messageFromEvent(this.mWire.parseFrom(bArr, NavdyEvent.class))).state) {
                        case CONNECTION_DISCONNECTED:
                            int i = SystemProperties.getInt(APP_TERMINATION_RECONNECT_DELAY_MS, 1000);
                            this.deviceReconnectRunnable.setReason(REASON_CONNECTION_DISCONNECTED);
                            this.serviceHandler.postDelayed(this.deviceReconnectRunnable, (long) i);
                            break;
                        case CONNECTION_CONNECTED:
                            if (sendingPhoneLocation) {
                                GpsManager.getInstance().startSendingLocation();
                            } else {
                                GpsManager.getInstance().stopSendingLocation();
                            }
                            // No break wanted here
                        case CONNECTION_LINK_LOST:
                            this.serviceHandler.removeCallbacks(this.deviceReconnectRunnable);
                            break;
                    }
                } catch (Throwable th5) {
                    this.logger.e(th5);
                }
                return false;
            case AudioStatus:
                try {
                    AudioStatus messageFromEvent = (AudioStatus)NavdyEventUtil.messageFromEvent(this.mWire.parseFrom(bArr, NavdyEvent.class));
                    if (messageFromEvent != null && messageFromEvent.profileType == ProfileType.AUDIO_PROFILE_HFP) {
                        this.logger.d("AudioStatus for HFP received");
                        if (!(Boolean) Wire.get(messageFromEvent.isConnected, Boolean.FALSE)) {
                            this.logger.d("AudioStatus: HFP disconnected");
                            setBandwidthLevel(1);
                            break;
                        } else {
                            this.logger.d("AudioStatus: HFP connected");
                            if (!(Boolean) Wire.get(messageFromEvent.hasSCOConnection, Boolean.FALSE)) {
                                this.logger.d("AudioStatus: does not have SCO connection");
                                setBandwidthLevel(1);
                                break;
                            } else {
                                this.logger.d("AudioStatus: has SCO connection");
                                setBandwidthLevel(0);
                                break;
                            }
                        }
                    }
                } catch (Throwable th6) {
                    this.logger.e(th6);
                }
                break;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean processLocalEvent(byte[] bArr, MessageType messageType) {
        ConnectionRequest connectionRequest = null;
        if (messageType == MessageType.ConnectionRequest) {
            try {
                connectionRequest = (ConnectionRequest) this.mWire.parseFrom(bArr, NavdyEvent.class).getExtension(Ext_NavdyEvent.connectionRequest);
            } catch (Throwable th) {
                this.logger.e(th);
            }
            if (connectionRequest == null) {
                return true;
            }
            switch (connectionRequest.action) {
                case CONNECTION_SELECT:
                    if (connectionRequest.remoteDeviceId != null) {
                        selectDevice(new NavdyDeviceId(connectionRequest.remoteDeviceId));
                        return true;
                    }
                    setActiveDevice(null);
                    return true;
                case CONNECTION_START_SEARCH:
                    setState(SEARCHING);
                    return true;
                case CONNECTION_STOP_SEARCH:
                    this.serviceHandler.post(new Runnable() {
                        public void run() {
                            HudConnectionService.this.logger.i("Ending search");
                            if (HudConnectionService.this.search != null) {
                                HudConnectionService.this.search.close();
                                HudConnectionService.this.search = null;
                            }
                            HudConnectionService.this.setState(IDLE);
                        }
                    });
                    return true;
                case PHONE_LOCATION_START:
                    GpsManager.getInstance().startSendingLocation();
                    sendingPhoneLocation = true;
                    return true;
                case PHONE_LOCATION_STOP:
                    GpsManager.getInstance().stopSendingLocation();
                    sendingPhoneLocation = false;
                    return true;
                default:
                    return true;
            }
//            this.logger.e(th);
//            return false;
        } else if (messageType == MessageType.StartDrivePlaybackEvent) {
            parseStartDrivePlaybackEvent(bArr);
            forwardEventLocally(bArr);
            return true;
        } else if (messageType == MessageType.StopDrivePlaybackEvent) {
            this.routeRecorder.stopPlayback();
            forwardEventLocally(bArr);
            this.logger.v("onStopDrivePlayback");
            return true;
        } else if (messageType == MessageType.StartDriveRecordingEvent) {
            return parseStartDriveRecordingEvent(bArr);
        } else {
            if (messageType == MessageType.StopDriveRecordingEvent) {
                this.routeRecorder.stopRecording(false);
                forwardEventLocally(bArr);
            }
            return false;
        }
    }

    public void reconnect(String str) {
        synchronized (this) {
            broadcastReconnectingIntent(str);
            HudConnectionService.super.reconnect(str);
        }
    }

    public boolean reconnectAfterDeadConnection() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void sendEventsOnLocalConnect() {
        this.logger.v("sendEventsOnLocalConnect");
        RouteRecorder instance = RouteRecorder.getInstance();
        if (instance.isRecording()) {
            String label = instance.getLabel();
            if (label != null) {
                forwardEventLocally(new StartDriveRecordingEvent(label));
                this.logger.v("sendEventsOnLocalConnect: send recording event:" + label);
                return;
            }
            this.logger.v("sendEventsOnLocalConnect: invalid label");
        }
    }

    public void setSimulatingGpsCoordinates(boolean z) {
        this.isSimulatingGpsCoordinates = z;
    }
}