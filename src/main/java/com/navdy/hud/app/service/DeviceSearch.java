package com.navdy.hud.app.service;

class DeviceSearch implements com.navdy.service.library.device.RemoteDevice.Listener {
    private static final int SEARCH_TIMEOUT = 15000;
    private java.util.Deque<com.navdy.service.library.device.RemoteDevice> connectedDevices = new java.util.ArrayDeque<>();
    private java.util.Deque<com.navdy.service.library.device.RemoteDevice> connectingDevices = new java.util.ArrayDeque<>();
    /* access modifiers changed from: private */
    public com.navdy.hud.app.service.DeviceSearch.EventSink eventSink;
    private final android.os.Handler handler = new android.os.Handler();
    protected final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(getClass());
    private java.util.Deque<com.navdy.service.library.device.RemoteDevice> remoteDevices = new java.util.ArrayDeque<>();
    private long startTime = android.os.SystemClock.elapsedRealtime();
    private boolean started = false;

    public interface EventSink {
        void onEvent(com.squareup.wire.Message message);
    }

    public DeviceSearch(android.content.Context context, com.navdy.hud.app.service.DeviceSearch.EventSink eventSink2, boolean z) {
        int i = 0;
        this.eventSink = eventSink2;
        java.util.List pairedConnections = com.navdy.service.library.device.RemoteDeviceRegistry.getInstance(context).getPairedConnections();
        while (true) {
            int i2 = i;
            if (i2 < pairedConnections.size()) {
                com.navdy.service.library.device.RemoteDevice remoteDevice = new com.navdy.service.library.device.RemoteDevice(context, (com.navdy.service.library.device.connection.ConnectionInfo) pairedConnections.get(i2), z);
                remoteDevice.addListener(this);
                this.remoteDevices.add(remoteDevice);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private void closeAll(java.util.Deque<com.navdy.service.library.device.RemoteDevice> deque) {
        for (com.navdy.service.library.device.RemoteDevice remoteDevice : deque) {
            remoteDevice.removeListener(this);
            queueDisconnect(remoteDevice);
        }
    }

    private com.navdy.service.library.device.RemoteDevice findDevice(android.bluetooth.BluetoothDevice bluetoothDevice, java.util.Deque<com.navdy.service.library.device.RemoteDevice> deque) {
        return findDevice(bluetoothDevice.getAddress(), deque);
    }

    private com.navdy.service.library.device.RemoteDevice findDevice(java.lang.String str, java.util.Deque<com.navdy.service.library.device.RemoteDevice> deque) {
        for (com.navdy.service.library.device.RemoteDevice remoteDevice : deque) {
            if (remoteDevice.getDeviceId().getBluetoothAddress().equals(str)) {
                return remoteDevice;
            }
        }
        return null;
    }

    private void queueDisconnect(final com.navdy.service.library.device.RemoteDevice remoteDevice) {
        if (remoteDevice != null) {
            this.handler.post(new java.lang.Runnable() {
                public void run() {
                    remoteDevice.disconnect();
                }
            });
        }
    }

    private void sendEvent(final com.squareup.wire.Message message) {
        if (this.eventSink != null) {
            this.handler.post(new java.lang.Runnable() {
                public void run() {
                    com.navdy.hud.app.service.DeviceSearch.this.eventSink.onEvent(message);
                }
            });
        }
    }

    public void close() {
        synchronized (this) {
            stop();
            closeAll(this.connectingDevices);
            closeAll(this.connectedDevices);
        }
    }

    public boolean forgetDevice(android.bluetooth.BluetoothDevice bluetoothDevice) {
        boolean z = true;
        synchronized (this) {
            this.logger.i("DeviceSearch forgetting:" + bluetoothDevice);
            com.navdy.service.library.device.RemoteDevice findDevice = findDevice(bluetoothDevice, this.connectingDevices);
            if (findDevice != null) {
                this.logger.i("DeviceSearch removing device from connecting devices");
                this.connectingDevices.remove(findDevice);
                queueDisconnect(findDevice);
            } else {
                com.navdy.service.library.device.RemoteDevice findDevice2 = findDevice(bluetoothDevice, this.remoteDevices);
                if (findDevice2 != null) {
                    this.logger.i("DeviceSearch removing device from remote devices");
                    this.remoteDevices.remove(findDevice2);
                } else {
                    z = false;
                }
            }
        }
        return z;
    }

    public void handleDisconnect(android.bluetooth.BluetoothDevice bluetoothDevice) {
        synchronized (this) {
            queueDisconnect(findDevice(bluetoothDevice, this.connectedDevices));
        }
    }

    public boolean next() {
        boolean z = false;
        synchronized (this) {
            if (android.os.SystemClock.elapsedRealtime() - this.startTime > 15000) {
                stop();
            } else if (this.remoteDevices.size() > 0) {
                start();
                com.navdy.service.library.device.RemoteDevice remoteDevice = this.remoteDevices.removeFirst();
                if (remoteDevice.connect()) {
                    this.connectingDevices.add(remoteDevice);
                } else {
                    this.remoteDevices.addLast(remoteDevice);
                }
                z = true;
            } else if (this.connectingDevices.size() > 0) {
                z = true;
            } else {
                stop();
            }
        }
        return z;
    }

    public void onDeviceConnectFailure(com.navdy.service.library.device.RemoteDevice remoteDevice, com.navdy.service.library.device.connection.Connection.ConnectionFailureCause connectionFailureCause) {
        synchronized (this) {
            this.logger.i("DeviceSearch connect failure:" + remoteDevice.getDeviceId());
            if (this.connectingDevices.remove(remoteDevice)) {
                this.remoteDevices.addLast(remoteDevice);
            }
        }
    }

    public void onDeviceConnected(com.navdy.service.library.device.RemoteDevice remoteDevice) {
        synchronized (this) {
            this.logger.i("DeviceSearch connected:" + remoteDevice.getDeviceId());
            this.connectingDevices.remove(remoteDevice);
            this.connectedDevices.add(remoteDevice);
            sendEvent(new com.navdy.service.library.events.connection.ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus.Status.CONNECTION_FOUND, remoteDevice.getDeviceId().toString()));
        }
    }

    public void onDeviceConnecting(com.navdy.service.library.device.RemoteDevice remoteDevice) {
    }

    public void onDeviceDisconnected(com.navdy.service.library.device.RemoteDevice remoteDevice, com.navdy.service.library.device.connection.Connection.DisconnectCause disconnectCause) {
        synchronized (this) {
            this.logger.i("DeviceSearch disconnected:" + remoteDevice.getDeviceId());
            this.connectedDevices.remove(remoteDevice);
            this.remoteDevices.addLast(remoteDevice);
            sendEvent(new com.navdy.service.library.events.connection.ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus.Status.CONNECTION_LOST, remoteDevice.getDeviceId().toString()));
        }
    }

    public void onNavdyEventReceived(com.navdy.service.library.device.RemoteDevice remoteDevice, com.navdy.service.library.events.NavdyEvent navdyEvent) {
    }

    public void onNavdyEventReceived(com.navdy.service.library.device.RemoteDevice remoteDevice, byte[] bArr) {
    }

    public com.navdy.service.library.device.RemoteDevice select(com.navdy.service.library.device.NavdyDeviceId navdyDeviceId) {
        com.navdy.service.library.device.RemoteDevice findDevice;
        synchronized (this) {
            java.lang.String bluetoothAddress = navdyDeviceId.getBluetoothAddress();
            findDevice = findDevice(bluetoothAddress, this.remoteDevices);
            com.navdy.service.library.device.RemoteDevice findDevice2 = findDevice(bluetoothAddress, this.connectingDevices);
            com.navdy.service.library.device.RemoteDevice findDevice3 = findDevice(bluetoothAddress, this.connectedDevices);
            if (findDevice != null) {
                this.remoteDevices.remove(findDevice);
                findDevice.removeListener(this);
            } else if (findDevice2 != null) {
                this.connectingDevices.remove(findDevice2);
                findDevice2.removeListener(this);
                findDevice = findDevice2;
            } else if (findDevice3 != null) {
                this.connectedDevices.remove(findDevice3);
                findDevice3.removeListener(this);
                findDevice = findDevice3;
            } else {
                findDevice = null;
            }
        }
        return findDevice;
    }

    public void start() {
        if (!this.started) {
            this.started = true;
            sendEvent(new com.navdy.service.library.events.connection.ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus.Status.CONNECTION_SEARCH_STARTED, null));
        }
    }

    public void stop() {
        if (this.started) {
            this.started = false;
            sendEvent(new com.navdy.service.library.events.connection.ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus.Status.CONNECTION_SEARCH_FINISHED, null));
        }
    }
}