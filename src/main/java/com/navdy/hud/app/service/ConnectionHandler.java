package com.navdy.hud.app.service;

import android.app.AlarmManager;
import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import com.navdy.hud.app.BuildConfig;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.event.DeviceInfoAvailable;
import com.navdy.hud.app.event.Disconnect;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.LocalSpeechRequest;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.event.Shutdown.State;
import com.navdy.hud.app.event.Wakeup;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.phonecall.CallUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.screen.WelcomeScreen;
import com.navdy.hud.app.ui.component.UISettings;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.OTAUpdateService.IncrementalOTAFailureDetected;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.service.library.Version;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.Capabilities;
import com.navdy.service.library.events.Capabilities.Builder;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.DeviceInfo.Platform;
import com.navdy.service.library.events.LegacyCapability;
import com.navdy.service.library.events.NavdyEventUtil;
import com.navdy.service.library.events.audio.NowPlayingUpdateRequest;
import com.navdy.service.library.events.callcontrol.CallStateUpdateRequest;
import com.navdy.service.library.events.connection.ConnectionRequest;
import com.navdy.service.library.events.connection.ConnectionRequest.Action;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.connection.LinkPropertiesChanged;
import com.navdy.service.library.events.connection.NetworkLinkReady;
import com.navdy.service.library.events.debug.StartDriveRecordingEvent;
import com.navdy.service.library.events.preferences.LocalPreferences;
import com.navdy.service.library.events.preferences.NotificationPreferences;
import com.navdy.service.library.events.settings.DateTimeConfiguration;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.wire.Message;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

public class ConnectionHandler {
    static Capabilities CAPABILITIES = new Builder().compactUi(Boolean.TRUE).localMusicBrowser(Boolean.TRUE).voiceSearch(Boolean.TRUE).voiceSearchNewIOSPauseBehaviors(Boolean.TRUE).musicArtworkCache(Boolean.TRUE).searchResultList(Boolean.TRUE).cannedResponseToSms(UISettings.supportsIosSms()).customDialLongPress(Boolean.TRUE).build();
    private static final DeviceSyncEvent DEVICE_FULL_SYNC_EVENT = new DeviceSyncEvent(1);
    private static final DeviceSyncEvent DEVICE_MINIMAL_SYNC_EVENT = new DeviceSyncEvent(0);
    public static final int IOS_APP_LAUNCH_TIMEOUT = 5000;
    private static List<LegacyCapability> LEGACY_CAPABILITIES = new ArrayList<>();
    private static final int LINK_LOST_TIME_OUT_AFTER_DISCONNECTED = 1000;
    private static final int REDOWNLOAD_THRESHOLD = 30000;
    private static final Logger sLogger = new Logger(ConnectionHandler.class);
    private Bus bus;
    protected Context context;
    protected boolean disconnecting;
    private volatile boolean forceFullUpdate = false;
    private volatile boolean isNetworkLinkReady = false;
    private String lastConnectedDeviceId;
    protected DeviceInfo lastConnectedDeviceInfo;
    private long lastDisconnectTime;
    private boolean mAppClosed = true;
    private boolean mAppLaunchAttempted = false;
    private Runnable mAppLaunchAttemptedRunnable;
    protected CustomNotificationServiceHandler mCustomNotificationServiceHandler;
    protected Runnable mDisconnectRunnable;
    protected DriverProfileManager mDriverProfileManager;
    protected Handler mHandler;
    private PowerManager mPowerManager;
    protected Runnable mReconnectTimedOutRunnable;
    protected RemoteDeviceProxy mRemoteDevice;
    protected ConnectionServiceProxy proxy;
    private TimeHelper timeHelper;
    private volatile boolean triggerDownload = true;
    private UIStateManager uiStateManager;

    public static class ApplicationLaunchAttempted {
    }

    public static class DeviceSyncEvent {
        public static final int FULL = 1;
        public static final int MINIMAL = 0;
        public final int amount;

        public DeviceSyncEvent(int amount) {
            this.amount = amount;
        }
    }

    static {
        LEGACY_CAPABILITIES.add(LegacyCapability.CAPABILITY_VOICE_SEARCH);
        LEGACY_CAPABILITIES.add(LegacyCapability.CAPABILITY_COMPACT_UI);
        LEGACY_CAPABILITIES.add(LegacyCapability.CAPABILITY_LOCAL_MUSIC_BROWSER);
    }

    public ConnectionHandler(Context context, ConnectionServiceProxy proxy, PowerManager powerManager, DriverProfileManager driverProfileManager, UIStateManager uiStateManager, TimeHelper timeHelper) {
        this.context = context;
        this.proxy = proxy;
        this.mPowerManager = powerManager;
        this.bus = this.proxy.getBus();
        this.bus.register(this);
        this.mCustomNotificationServiceHandler = new CustomNotificationServiceHandler(this.bus);
        this.mCustomNotificationServiceHandler.start();
        this.mDriverProfileManager = driverProfileManager;
        this.uiStateManager = uiStateManager;
        this.timeHelper = timeHelper;
        sLogger.i("Creating connectionHandler:" + this);
        this.mHandler = new Handler(Looper.getMainLooper());
        this.mDisconnectRunnable = new Runnable() {
            public void run() {
                ConnectionHandler.this.handleDisconnectWithoutLinkLoss();
            }
        };
        this.mReconnectTimedOutRunnable = new Runnable() {
            public void run() {
                ConnectionHandler.this.sendConnectionNotification();
            }
        };
        this.mAppLaunchAttemptedRunnable = new Runnable() {
            public void run() {
                ConnectionHandler.sLogger.d("No response after the timeout after App launch attempt");
                ConnectionHandler.this.sendConnectionNotification();
            }
        };
    }

    public boolean serviceConnected() {
        return this.proxy.connected();
    }

    public void connect() {
        this.proxy.connect();
    }

    public void shutdown() {
        disconnect();
        this.proxy.disconnect();
    }

    public RemoteDevice getRemoteDevice() {
        return this.mRemoteDevice;
    }

    public NavdyDeviceId getConnectedDevice() {
        if (this.mRemoteDevice != null) {
            return this.mRemoteDevice.getDeviceId();
        }
        return null;
    }

    public void connectToDevice(NavdyDeviceId deviceId) {
        sendLocalMessage(new ConnectionRequest(Action.CONNECTION_SELECT, deviceId != null ? deviceId.toString() : null));
    }

    public void searchForDevices() {
        sendLocalMessage(new ConnectionRequest(Action.CONNECTION_START_SEARCH, null));
    }

    public void stopSearch() {
        sendLocalMessage(new ConnectionRequest(Action.CONNECTION_STOP_SEARCH, null));
    }

    public void startPhoneSendingLocation() {
        sendLocalMessage(new ConnectionRequest(Action.PHONE_LOCATION_START, null));
    }

    public void stopPhoneSendingLocation() {
        sendLocalMessage(new ConnectionRequest(Action.PHONE_LOCATION_STOP, null));
    }

    public void sendLocalMessage(Message message) {
        try {
            this.proxy.postRemoteEvent(NavdyDeviceId.getThisDevice(this.context), NavdyEventUtil.eventFromMessage(message));
        } catch (Exception e) {
            sLogger.e("Failed to send local event", e);
        }
    }

    public void disconnect() {
        if (this.mRemoteDevice != null) {
            this.lastConnectedDeviceInfo = null;
            this.disconnecting = true;
            connectToDevice(null);
        }
    }

    @Subscribe
    public void onDisconnect(Disconnect event) {
        disconnect();
    }

    @Subscribe
    public void onShutdown(Shutdown event) {
        if (event.state == State.SHUTTING_DOWN) {
            shutdown();
        }
    }

    @Subscribe
    public void onIncrementalOTAFailureDetected(IncrementalOTAFailureDetected event) {
        sLogger.d("Incremental OTA failure detected, indicate the remote device to force full update");
        this.forceFullUpdate = true;
    }

    @Subscribe
    public void onLinkPropertiesChanged(LinkPropertiesChanged linkPropertiesChanged) {
        if (linkPropertiesChanged != null && linkPropertiesChanged.bandwidthLevel != null && this.mRemoteDevice != null) {
            this.mRemoteDevice.setLinkBandwidthLevel(linkPropertiesChanged.bandwidthLevel);
        }
    }

    @Subscribe
    public void onConnectionStateChange(ConnectionStateChange event) {
        sLogger.d("ConnectionStateChange - " + event.state);
        String remoteDeviceId = event.remoteDeviceId;
        NavdyDeviceId deviceId = TextUtils.isEmpty(remoteDeviceId) ? NavdyDeviceId.UNKNOWN_ID : new NavdyDeviceId(remoteDeviceId);
        this.mAppLaunchAttempted = false;
        this.mHandler.removeCallbacks(this.mAppLaunchAttemptedRunnable);
        switch (event.state) {
            case CONNECTION_LINK_ESTABLISHED:
                this.mAppClosed = true;
                onLinkEstablished(deviceId);
                return;
            case CONNECTION_CONNECTED:
                onConnect(deviceId);
                this.mAppClosed = false;
                return;
            case CONNECTION_DISCONNECTED:
                this.lastDisconnectTime = SystemClock.elapsedRealtime();
                this.mAppClosed = true;
                onDisconnect(deviceId);
                return;
            case CONNECTION_LINK_LOST:
                this.lastDisconnectTime = SystemClock.elapsedRealtime();
                this.mAppClosed = true;
                onLinkLost(deviceId);
                return;
            default:
        }
    }

    @Subscribe
    public void onApplicationLaunchAttempted(ApplicationLaunchAttempted applicationLaunchAttempted) {
        sLogger.d("Attempting to launch the App, Will wait and see");
        if (this.mAppClosed) {
            this.mAppLaunchAttempted = true;
            this.mHandler.removeCallbacks(this.mAppLaunchAttemptedRunnable);
            this.mHandler.postDelayed(this.mAppLaunchAttemptedRunnable, 5000);
        }
    }

    private void onLinkEstablished(NavdyDeviceId connectingDevice) {
        this.mPowerManager.wakeUp(AnalyticsSupport$WakeupReason.PHONE);
        if (this.mRemoteDevice != null) {
            this.mRemoteDevice = new RemoteDeviceProxy(this.proxy, this.context, connectingDevice);
            this.mHandler.removeCallbacks(this.mReconnectTimedOutRunnable);
        } else {
            this.mRemoteDevice = new RemoteDeviceProxy(this.proxy, this.context, connectingDevice);
            this.mHandler.removeCallbacks(this.mReconnectTimedOutRunnable);
        }
    }

    private void onConnect(NavdyDeviceId connectingDevice) {
        if (this.mRemoteDevice == null) {
            this.mRemoteDevice = new RemoteDeviceProxy(this.proxy, this.context, connectingDevice);
        }
        this.mDriverProfileManager.loadProfileForId(connectingDevice);
        NavdyDeviceId displayDeviceId = NavdyDeviceId.getThisDevice(this.context);
        DeviceInfo deviceInfo = new DeviceInfo.Builder().deviceId(displayDeviceId.toString()).clientVersion(BuildConfig.VERSION_NAME).protocolVersion(Version.PROTOCOL_VERSION.toString()).deviceName(displayDeviceId.getDeviceName()).systemVersion(getSystemVersion()).model(Build.MODEL).deviceUuid(Build.SERIAL).systemApiLevel(Integer.valueOf(VERSION.SDK_INT)).kernelVersion(System.getProperty("os.version")).platform(Platform.PLATFORM_Android).buildType(Build.TYPE).deviceMake("Navdy").forceFullUpdate(Boolean.valueOf(this.forceFullUpdate)).legacyCapabilities(LEGACY_CAPABILITIES).capabilities(CAPABILITIES).build();
        sLogger.d("onConnect - sending device info: " + deviceInfo);
        this.mRemoteDevice.postEvent((Message) deviceInfo);
    }

    @Subscribe
    public void onNetworkLinkReady(NetworkLinkReady networkLinkReady) {
        sLogger.v("DummyNetNetworkFactory:onConnect");
        this.isNetworkLinkReady = true;
        NetworkStateManager.getInstance().networkAvailable();
//        GestureVideosSyncService.scheduleWithDelay(30000);
//        ObdCANBusDataUploadService.scheduleWithDelay(45000);
    }

    private String getSystemVersion() {
        if (String.valueOf(BuildConfig.VERSION_CODE).equals(VERSION.INCREMENTAL)) {
            return VERSION.INCREMENTAL;
        }
        return BuildConfig.VERSION_NAME;
    }

    private synchronized void onDisconnect(NavdyDeviceId navdyDeviceId) {
        sLogger.v("onDisconnect:" + navdyDeviceId);
        this.isNetworkLinkReady = false;
        NetworkStateManager.getInstance().networkNotAvailable();
        this.mDriverProfileManager.setCurrentProfile(null);
        this.mHandler.postDelayed(this.mDisconnectRunnable, 1000);
    }

    private synchronized void onLinkLost(NavdyDeviceId deviceId) {
        boolean wasConnected = true;
        synchronized (this) {
            sLogger.v("onLinkLost:" + deviceId);
            this.isNetworkLinkReady = false;
            this.mHandler.removeCallbacks(this.mDisconnectRunnable);
            this.mHandler.removeCallbacks(this.mReconnectTimedOutRunnable);
            if (this.mRemoteDevice == null) {
                wasConnected = false;
            }
            this.mRemoteDevice = null;
            PicassoUtil.clearCache();
            if (wasConnected) {
                NotificationManager.getInstance().handleDisconnect(true);
                if (!this.disconnecting) {
                    this.mHandler.postDelayed(this.mReconnectTimedOutRunnable, 15000);
                }
            }
            this.disconnecting = false;
        }
    }

    private void handleDisconnectWithoutLinkLoss() {
        Platform remoteDevicePlatform = RemoteDeviceManager.getInstance().getRemoteDevicePlatform();
        if (remoteDevicePlatform != null && remoteDevicePlatform.equals(Platform.PLATFORM_iOS)) {
            sendConnectionNotification();
        }
    }

    @Subscribe
    public void onDeviceInfo(DeviceInfo deviceInfo) {
        boolean newDevice = false;
        sLogger.i("Received deviceInfo:" + deviceInfo);
        if (this.mRemoteDevice != null) {
            this.triggerDownload = true;
            if (this.lastConnectedDeviceId != null) {
                long time = SystemClock.elapsedRealtime() - this.lastDisconnectTime;
                if (time < 30000) {
                    sLogger.v("threshold met=" + time);
                    if (TextUtils.equals(this.lastConnectedDeviceId, deviceInfo.deviceId)) {
                        this.triggerDownload = false;
                        sLogger.v("same device connected:" + deviceInfo.deviceId);
                    } else {
                        sLogger.v("different device connected last[" + this.lastConnectedDeviceId + "] current[" + deviceInfo.deviceId + "]");
                        this.lastConnectedDeviceId = deviceInfo.deviceId;
                    }
                } else {
                    sLogger.v("threshold not met=" + time);
                    this.lastConnectedDeviceId = deviceInfo.deviceId;
                }
            } else {
                sLogger.v("no last device");
                this.lastConnectedDeviceId = deviceInfo.deviceId;
            }
            String currentDeviceId = this.mRemoteDevice.getDeviceId().toString();
            deviceInfo = new DeviceInfo.Builder(deviceInfo).deviceId(currentDeviceId).build();
            if (!currentDeviceId.equals(this.lastConnectedDeviceInfo != null ? this.lastConnectedDeviceInfo.deviceId : null)) {
                newDevice = true;
            }
            this.lastConnectedDeviceInfo = deviceInfo;
            this.mRemoteDevice.setDeviceInfo(deviceInfo);
            this.bus.post(new DeviceInfoAvailable(deviceInfo));
            if (this.triggerDownload) {
                sLogger.v("trigger device sync");
                this.bus.post(DEVICE_FULL_SYNC_EVENT);
            } else if (deviceInfo.platform == Platform.PLATFORM_iOS) {
                sLogger.v("triggering minimal sync");
                this.bus.post(DEVICE_MINIMAL_SYNC_EVENT);
            } else {
                sLogger.v("don't trigger device sync");
            }
            if (newDevice) {
                sendConnectionNotification();
                return;
            }
            NotificationManager.getInstance().handleConnect();
            NotificationManager.getInstance().hideConnectionToast(true);
            long elapsedTime = SystemClock.elapsedRealtime() - this.lastDisconnectTime;
            sLogger.i("Tracking a reconnect that took " + elapsedTime + "ms");
            AnalyticsSupport.recordPhoneDisconnect(true, String.valueOf(elapsedTime));
        }
    }

    @Subscribe
    public void onRemoteEvent(RemoteEvent event) {
        if (this.mRemoteDevice != null) {
            this.mRemoteDevice.postEvent(event.getMessage());
        }
    }

    @Subscribe
    public void onLocalSpeechRequest(LocalSpeechRequest event) {
        if (!CallUtils.isPhoneCallInProgress()) {
            if (sLogger.isLoggable(2)) {
                sLogger.v("[tts-outbound] [" + event.speechRequest.category.name() + "] [" + event.speechRequest.words + "]");
            }
            onRemoteEvent(new RemoteEvent(event.speechRequest));
        }
    }

    @Subscribe
    public void onDateTimeConfiguration(DateTimeConfiguration configurationEvent) {
        sLogger.i("Received timestamp from the client: " + configurationEvent.timestamp + " " + configurationEvent.timezone);
        try {
            if (DeviceUtil.isNavdyDevice()) {
                AlarmManager am = (AlarmManager) this.context.getSystemService(Context.ALARM_SERVICE);
                sLogger.v("setting date timezone[" + configurationEvent.timezone + "] time[" + configurationEvent.timestamp + "]");
                if (configurationEvent.timezone == null) {
                    sLogger.w("timezone not provided");
                } else if (TimeZone.getDefault().getID().equalsIgnoreCase(configurationEvent.timezone)) {
                    sLogger.v("timezone already set to " + configurationEvent.timezone);
                } else {
                    TimeZone timeZone = TimeZone.getTimeZone(configurationEvent.timezone);
                    if (timeZone.getID().equalsIgnoreCase(configurationEvent.timezone)) {
                        sLogger.v("timezone found on HUD:" + timeZone.getID());
                        am.setTimeZone(configurationEvent.timezone);
                    } else {
                        sLogger.e("timezone not found on HUD:" + configurationEvent.timezone);
                    }
                }
                sLogger.v("setting time [" + configurationEvent.timestamp + "]");
                am.setTime(configurationEvent.timestamp);
                sLogger.v("set time [" + configurationEvent.timestamp + "]");
                this.bus.post(TimeHelper.DATE_TIME_AVAILABLE_EVENT);
                sLogger.v("post date complete");
            }
            DriverProfileManager driverProfileManager = DriverProfileHelper.getInstance().getDriverProfileManager();
            driverProfileManager.getSessionPreferences().setClockConfiguration(configurationEvent);
            driverProfileManager.updateLocalPreferences(new LocalPreferences.Builder(driverProfileManager.getCurrentProfile().getLocalPreferences()).clockFormat(configurationEvent.format).build());
        } catch (Throwable t) {
            sLogger.e("Cannot update time on device", t);
        }
    }

    @Subscribe
    public void onDriverProfileUpdated(DriverProfileChanged profileChanged) {
        DriverProfile profile = this.mDriverProfileManager.getCurrentProfile();
        requestIAPUpdateBasedOnPreference(profile != null ? profile.getNotificationPreferences() : null);
    }

    @Subscribe
    public void onNotificationPreference(NotificationPreferences preferences) {
        requestIAPUpdateBasedOnPreference(preferences);
    }

    @Subscribe
    public void onWakeup(Wakeup event) {
        if (DialManager.getInstance().getBondedDialCount() == 0) {
            sLogger.v("go to dial pairing screen");
            this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_DIAL_PAIRING).build());
        } else if (this.mRemoteDevice == null) {
            sLogger.v("go to welcome screen");
            Bundle args = new Bundle();
            args.putString(WelcomeScreen.ARG_ACTION, WelcomeScreen.ACTION_RECONNECT);
            this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_WELCOME, args, false));
        } else {
            this.bus.post(new ShowScreen.Builder().screen(this.uiStateManager.getDefaultMainActiveScreen()).build());
        }
    }

    private void requestIAPUpdateBasedOnPreference(NotificationPreferences preferences) {
        if (preferences == null) {
            return;
        }
        if (Boolean.TRUE.equals(preferences.enabled)) {
            this.bus.post(new RemoteEvent(new CallStateUpdateRequest(Boolean.TRUE)));
            this.bus.post(new RemoteEvent(new NowPlayingUpdateRequest(Boolean.TRUE)));
            return;
        }
        this.bus.post(new RemoteEvent(new CallStateUpdateRequest(Boolean.FALSE)));
        this.bus.post(new RemoteEvent(new NowPlayingUpdateRequest(Boolean.FALSE)));
    }

    public void sendConnectionNotification() {
        NotificationManager notificationManager = NotificationManager.getInstance();
        if (!RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
            notificationManager.handleDisconnect(true);
            notificationManager.showHideToast(false);
        } else if (isAppClosed()) {
            notificationManager.handleDisconnect(false);
            notificationManager.showHideToast(false);
        } else {
            notificationManager.handleConnect();
            notificationManager.showHideToast(true);
        }
    }

    public DeviceInfo getLastConnectedDeviceInfo() {
        return this.lastConnectedDeviceInfo;
    }

    public boolean isAppClosed() {
        return this.mAppClosed;
    }

    public boolean isNetworkLinkReady() {
        return this.isNetworkLinkReady;
    }

    public StartDriveRecordingEvent getDriverRecordingEvent() {
        if (this.proxy != null) {
            return this.proxy.getDriverRecordingEvent();
        }
        return null;
    }

    public boolean isAppLaunchAttempted() {
        return this.mAppLaunchAttempted;
    }
}
