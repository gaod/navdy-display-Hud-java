package com.navdy.hud.app.obd;

import android.content.SharedPreferences;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.os.SystemClock;
import android.text.TextUtils;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason;
import com.navdy.hud.app.analytics.TelemetryDataManager;
import com.navdy.hud.app.debug.DriveRecorder;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.DriverProfileUpdated;
import com.navdy.hud.app.event.DrivingStateChange;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.event.Shutdown.Reason;
import com.navdy.hud.app.event.Shutdown.State;
import com.navdy.hud.app.event.Wakeup;
import com.navdy.hud.app.framework.trips.TripManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.obd.ECU;
import com.navdy.obd.ICanBusMonitoringListener;
import com.navdy.obd.ICanBusMonitoringListener.Stub;
import com.navdy.obd.ICarService;
import com.navdy.obd.IPidListener;
import com.navdy.obd.Pid;
import com.navdy.obd.PidSet;
import com.navdy.obd.Pids;
import com.navdy.obd.ScanSchedule;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.obd.ObdStatusRequest;
import com.navdy.service.library.events.obd.ObdStatusResponse;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import mortar.Mortar;

public final class ObdManager {
    public static final float BATTERY_CHARGING_VOLTAGE = 13.1f;
    public static final double BATTERY_NO_OBD = -1.0d;
    private static final String DEFAULT_SET = "default_set";
    public static final String LAST_ODOMETER_READING_KM = "last_odometer_reading";
    public static final String FIRST_ODOMETER_READING_KM = "first_odometer_reading";
    private static final String BLACKLIST_MODIFICATION_TIME = "blacklist_modification_time";
    private static final String LOW_VOLTAGE_OVERRIDE_PROPERTY = "persist.sys.voltage.low";
    private static final String CRITICAL_VOLTAGE_OVERRIDE_PROPERTY = "persist.sys.voltage.crit";
    private static final String LOW_VOLTAGE_DURATION_OVERRIDE_PROPERTY = "persist.sys.voltage.duration";
    private static final float DEFAULT_CRITICAL_BATTERY_VOLTAGE = 12.0f;
    private static final long DEFAULT_LOW_VOLTAGE_SHUTOFF_DURATION = TimeUnit.MINUTES.toMillis(15);
    private static final int DISTANCE_UPDATE_INTERVAL = 60000;
    private static final int ENGINE_TEMP_UPDATE_INTERVAL = 2000;
    public static final String FIRMWARE_VERSION = "4.2.1";
    private static final int FLUSH_INTERVAL_MS = 30000;
    private static final int FUEL_UPDATE_INTERVAL = 15000;
    private static final int GENERIC_UPDATE_INTERVAL = 1000;
    public static final int LOW_FUEL_LEVEL = 10;
    private static final long LOW_VOLTAGE_SHUTOFF_DURATION = SystemProperties.getLong(LOW_VOLTAGE_DURATION_OVERRIDE_PROPERTY, DEFAULT_LOW_VOLTAGE_SHUTOFF_DURATION);
    public static final double CRITICAL_BATTERY_VOLTAGE = ((double) SystemProperties.getFloat(CRITICAL_VOLTAGE_OVERRIDE_PROPERTY, DEFAULT_CRITICAL_BATTERY_VOLTAGE));
    public static final double LOW_BATTERY_VOLTAGE = ((double) SystemProperties.getFloat(LOW_VOLTAGE_OVERRIDE_PROPERTY, 12.2f));
    private static final int LOW_BATTERY_COUNT_THRESHOLD = ((int) Math.ceil(((double) LOW_VOLTAGE_SHUTOFF_DURATION) / 2000.0d));
    public static final long MINIMUM_TIME_BETWEEN_MILEAGE_REPORTING = TimeUnit.MINUTES.toMillis(10);
    private static final int MPG_UPDATE_INTERVAL = 1000;
    public static final int NOT_AVAILABLE = -1;
    private static final ObdConnectionStatusEvent OBD_CONNECTED = new ObdConnectionStatusEvent(true);
    private static final ObdConnectionStatusEvent OBD_NOT_CONNECTED = new ObdConnectionStatusEvent(false);
    private static final int RPM_UPDATE_INTERVAL = 250;
    public static final int SAFE_TO_SLEEP_BATTERY_OBSERVED_COUNT_THRESHOLD = 5;
    private static final String SCAN_SETTING = "scan_setting";
    private static final int SLOW_RPM_UPDATE_INTERVAL = 2500;
    private static final int SPEED_UPDATE_INTERVAL = 100;
    private static final int STATE_CONNECTED = 2;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_IDLE = 4;
    private static final int STATE_SLEEPING = 5;
    public static final String TELEMETRY_TAG = "Telemetry";
    public static final String TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS = "total_distance_travelled_with_navdy";
    public static final int VALID_FUEL_DATA_WAIT_TIME = 45000;
    public static final String VEHICLE_VIN = "vehicle_vin";
    public static final int VOLTAGE_REPORT_INTERVAL = 2000;
    private static final Logger sLogger = new Logger(ObdManager.class);
    private static final Logger sTelemetryLogger = new Logger(TELEMETRY_TAG);
    private static final ObdManager singleton = new ObdManager();
    private double batteryVoltage = -1.0d;
    @Inject
    Bus bus;
    private ICanBusMonitoringListener canBusMonitoringListener = new Stub() {
        public void onNewDataAvailable(String fileName) throws RemoteException {
            ObdManager.sLogger.d("onNewDataAvailable : " + fileName);
            ObdManager.this.obdCanBusRecordingPolicy.onNewDataAvailable(fileName);
        }

        public void onCanBusMonitoringError(String errorMessage) throws RemoteException {
            ObdManager.sLogger.d("onCanBusMonitoringError, Error : " + errorMessage);
            ObdManager.this.obdCanBusRecordingPolicy.onCanBusMonitoringFailed();
        }

        public void onCanBusMonitorSuccess(int averageAmountOfData) throws RemoteException {
            ObdManager.sLogger.d("onCanBusMonitorSuccess , Average amount of data : " + averageAmountOfData);
            ObdManager.this.obdCanBusRecordingPolicy.onCanBusMonitorSuccess(averageAmountOfData);
        }

        public int getGpsSpeed() throws RemoteException {
            return Math.round(ObdManager.this.speedManager.getGpsSpeedInMetersPerSecond());
        }

        public double getLatitude() throws RemoteException {
            if (HereMapsManager.getInstance().isInitialized()) {
                GeoCoordinate c = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                if (c != null) {
                    return c.getLatitude();
                }
            }
            return -1.0d;
        }

        public double getLongitude() throws RemoteException {
            if (HereMapsManager.getInstance().isInitialized()) {
                GeoCoordinate c = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                if (c != null) {
                    return c.getLongitude();
                }
            }
            return -1.0d;
        }

        public boolean isMonitoringLimitReached() throws RemoteException {
            return ObdManager.this.obdCanBusRecordingPolicy.isCanBusMonitoringLimitReached();
        }

        public String getMake() throws RemoteException {
            if (ObdManager.this.driverProfileManager != null) {
                DriverProfile profile = ObdManager.this.driverProfileManager.getCurrentProfile();
                if (profile != null) {
                    return profile.getCarMake();
                }
            }
            return "";
        }

        public String getModel() throws RemoteException {
            if (ObdManager.this.driverProfileManager != null) {
                DriverProfile profile = ObdManager.this.driverProfileManager.getCurrentProfile();
                if (profile != null) {
                    return profile.getCarModel();
                }
            }
            return "";
        }

        public String getYear() throws RemoteException {
            if (ObdManager.this.driverProfileManager != null) {
                DriverProfile profile = ObdManager.this.driverProfileManager.getCurrentProfile();
                if (profile != null) {
                    return profile.getCarYear();
                }
            }
            return "";
        }
    };
    private CarServiceConnector carServiceConnector;
    private Runnable checkVoltage = new Runnable() {
        public void run() {
            double prevVoltage = ObdManager.this.batteryVoltage;
            ObdManager.this.batteryVoltage = ObdManager.this.getBatteryVoltage();
            if (ObdManager.this.batteryVoltage != -1.0d && prevVoltage == -1.0d) {
                ObdManager.this.minBatteryVoltage = ObdManager.this.batteryVoltage;
                AnalyticsSupport.recordStartingBatteryVoltage(ObdManager.this.batteryVoltage);
            }
            if (ObdManager.this.batteryVoltage == 0.0d || ObdManager.this.batteryVoltage == -1.0d) {
                if (ObdManager.this.lowBatteryCount != 0) {
                    ObdManager.sLogger.d("Battery level data is not available");
                }
                ObdManager.this.lowBatteryCount = 0;
                ObdManager.this.safeToSleepBatteryLevelObservedCount = 0;
                return;
            }
            if (ObdManager.this.batteryVoltage < ObdManager.this.minBatteryVoltage) {
                ObdManager.this.minBatteryVoltage = ObdManager.this.batteryVoltage;
            }
            if (ObdManager.this.batteryVoltage > ObdManager.this.maxBatteryVoltage) {
                ObdManager.this.maxBatteryVoltage = ObdManager.this.batteryVoltage;
            }
            double lowBatteryThreshold = ObdManager.this.powerManager.inQuietMode() ? ObdManager.LOW_BATTERY_VOLTAGE : ObdManager.CRITICAL_BATTERY_VOLTAGE;
            if (ObdManager.this.batteryVoltage >= 13.100000381469727d && ObdManager.this.powerManager.inQuietMode()) {
                ObdManager.sLogger.d("Battery seems to be charging, waking up");
                ObdManager.this.powerManager.wakeUp(AnalyticsSupport$WakeupReason.VOLTAGE_SPIKE);
                ObdManager.this.safeToSleepBatteryLevelObservedCount = 0;
            } else if (ObdManager.this.batteryVoltage < lowBatteryThreshold) {
                ObdManager.this.safeToSleepBatteryLevelObservedCount = 0;
                ObdManager.sLogger.d("Battery voltage (LOW) : " + ObdManager.this.batteryVoltage);
                ObdManager.this.lowBatteryCount = ObdManager.this.lowBatteryCount + 1;
                if (ObdManager.this.lowBatteryCount > ObdManager.LOW_BATTERY_COUNT_THRESHOLD) {
                    ObdManager.sLogger.d("Battery Voltage is too low, shutting down");
                    try {
                        Reason reason = ObdManager.this.powerManager.inQuietMode() ? Reason.LOW_VOLTAGE : Reason.CRITICAL_VOLTAGE;
                        ObdManager.this.sleep(true);
                        ObdManager.this.bus.post(new Shutdown(reason));
                    } catch (Exception e) {
                        ObdManager.sLogger.e("error invoking ShutdownMonitor.androidShutdown(): " + e);
                    }
                }
            } else {
                ObdManager.this.lowBatteryCount = 0;
                if (ObdManager.this.powerManager.inQuietMode()) {
                    ObdManager.sLogger.d("In Quiet mode , Battery voltage : " + ObdManager.this.batteryVoltage);
                    ObdManager.this.safeToSleepBatteryLevelObservedCount = ObdManager.this.safeToSleepBatteryLevelObservedCount + 1;
                    if (ObdManager.this.safeToSleepBatteryLevelObservedCount >= 5) {
                        ObdManager.sLogger.d("Safe to put obd chip to sleep, invoking sleep");
                        ObdManager.this.safeToSleepBatteryLevelObservedCount = 0;
                        ObdManager.this.handler.removeCallbacks(ObdManager.this.periodicCheckRunnable);
                        ObdManager.this.sleep(false);
                    }
                }
            }
        }
    };
    private ConnectionType connectionType = ConnectionType.POWER_ONLY;
    private DriveRecorder driveRecorder;
    @Inject
    DriverProfileManager driverProfileManager;
    private List<ECU> ecus;
    private boolean firstScan;
    PidCheck fuelPidCheck;
    private ScanSchedule fullScan;
    private Handler handler;
    private boolean isCheckEngineLightOn = false;
    private int lowBatteryCount;
    private double maxBatteryVoltage = -1.0d;
    private long mileageEventLastReportTime = 0;
    private double minBatteryVoltage = -1.0d;
    private ScanSchedule minimalScan;
    private ObdCanBusRecordingPolicy obdCanBusRecordingPolicy;
    private String obdChipFirmwareVersion;
    private volatile boolean obdConnected;
    private ObdDeviceConfigurationManager obdDeviceConfigurationManager;
    private HashMap<Integer, Double> obdPidsCache = new HashMap();
    private boolean odoMeterReadingValidated = false;
    private Runnable periodicCheckRunnable = new Runnable() {
        public void run() {
            TaskManager.getInstance().execute(ObdManager.this.checkVoltage, 1);
            ObdManager.this.handler.postDelayed(this, 2000);
        }
    };
    private IPidListener pidListener = new IPidListener.Stub() {
        public synchronized void pidsRead(List<Pid> pidsRead, List<Pid> pidsChanged) throws RemoteException {
            if (ObdManager.this.firstScan && pidsChanged != null) {
                ObdManager.this.firstScan = false;
                try {
                    ICarService api = ObdManager.this.carServiceConnector.getCarApi();
                    ObdManager.this.vin = api.getVIN();
                    ObdManager.this.onVinRead(ObdManager.this.vin);
                    ObdManager.this.ecus = api.getEcus();
                    ObdManager.this.protocol = api.getProtocol();
                    List supportedPids = ObdManager.this.carServiceConnector.getCarApi().getSupportedPids();
                    ObdManager.sLogger.d("First scan, got the supported PIDS");
                    if (supportedPids != null) {
                        ObdManager.sLogger.d("Supported PIDS size :" + supportedPids.size());
                    } else {
                        ObdManager.sLogger.d("First scan, got the supported PIDS , list null");
                    }
                    if (supportedPids != null) {
                        PidSet pidSet = new PidSet(supportedPids);
                        if (pidSet.equals(ObdManager.this.supportedPidSet)) {
                            ObdManager.sLogger.v("pid-changed event not sent");
                        } else {
                            ObdManager.sTelemetryLogger.v("SUPPORTED VIN: " + (ObdManager.this.vin != null ? ObdManager.this.vin : "") + ", " + "FUEL: " + pidSet.contains(47) + ", " + "SPEED: " + pidSet.contains(13) + ", " + "RPM: " + pidSet.contains(12) + ", " + "DIST: " + pidSet.contains(49) + ", " + "MAF: " + pidSet.contains(16) + ", " + "IFC(LPKKM): " + supportedPids.contains(Integer.valueOf(256)));
                            ObdManager.this.supportedPidSet = pidSet;
                            ObdManager.this.fuelPidCheck.reset();
                            ObdManager.this.bus.post(new ObdSupportedPidsChangedEvent());
                            ObdManager.sLogger.v("pid-changed event sent");
                        }
                    }
                } catch (Throwable t) {
                    ObdManager.sLogger.e(t);
                }
            }
            if (pidsChanged == null) {
                ObdManager.this.bus.post(new ObdPidReadEvent(pidsRead));
            } else {
                for (Pid pid : pidsChanged) {
                    switch (pid.getId()) {
                        case 13:
                            ObdManager.this.speedManager.setObdSpeed(pid.getIntValue(), pid.getTimeStamp());
                            break;
                        case 47:
                            ObdManager.this.fuelPidCheck.checkPid((double) ((int) pid.getIntValue()));
                            if (!ObdManager.this.fuelPidCheck.hasIncorrectData()) {
                                ObdManager.this.obdPidsCache.put(pid.getId(), pid.getValue());
                                break;
                            } else {
                                ObdManager.this.obdPidsCache.remove(Pids.FUEL_LEVEL);
                                break;
                            }
                        case 49:
                            ObdManager.this.obdPidsCache.put(pid.getId(), pid.getValue());
                            ObdManager.this.updateCumulativeVehicleDistanceTravelled((long) pid.getValue());
                            break;
                        default:
                            ObdManager.this.obdPidsCache.put(pid.getId(), pid.getValue());
                            break;
                    }
                    ObdManager.this.bus.post(new ObdPidChangeEvent(pid));
                }
                if (!(ObdManager.this.recordingPidListener == null || pidsChanged == null)) {
                    ObdManager.this.recordingPidListener.pidsRead(pidsRead, pidsChanged);
                }
                ObdManager.this.bus.post(new ObdPidReadEvent(pidsRead));
            }
        }

        public void pidsChanged(List<Pid> list) throws RemoteException {
        }

        public void onConnectionStateChange(int newState) throws RemoteException {
            ObdManager.this.updateConnectionState(newState);
        }
    };
    @Inject
    PowerManager powerManager;
    private String protocol;
    private IPidListener recordingPidListener;
    private int safeToSleepBatteryLevelObservedCount = 0;
    private ScanSchedule schedule;
    @Inject
    SharedPreferences sharedPreferences;
    private SpeedManager speedManager = SpeedManager.getInstance();
    private PidSet supportedPidSet = new PidSet();
    @Inject
    public TelemetryDataManager telemetryDataManager;
    private long totalDistanceInCurrentSessionPersisted = 0;
    @Inject
    TripManager tripManager;
    private List<String> troubleCodes;
    private String vin;

    class PidCheck {
        private Handler handler;
        volatile boolean hasIncorrectData = false;
        private Runnable invalidPidRunnable;
        int pid;
        volatile boolean waitingForValidData = false;

        public PidCheck(final int pid, Handler handler) {
            this.pid = pid;
            this.handler = handler;
            this.invalidPidRunnable = new Runnable() {
                public void run() {
                    if (!PidCheck.this.hasIncorrectData) {
                        PidCheck.this.waitingForValidData = false;
                        PidCheck.this.hasIncorrectData = true;
                        PidCheck.this.invalidatePid(pid);
                    }
                }
            };
        }

        public boolean isPidValueValid(double value) {
            return true;
        }

        public void invalidatePid(int pid) {
        }

        public boolean hasIncorrectData() {
            return this.hasIncorrectData;
        }

        public boolean isWaitingForValidData() {
            return this.waitingForValidData;
        }

        public long getWaitForValidDataTimeout() {
            return 0;
        }

        public void reset() {
            this.hasIncorrectData = false;
            this.handler.removeCallbacks(this.invalidPidRunnable);
            this.waitingForValidData = false;
        }

        public void checkPid(double value) {
            if (isPidValueValid(value)) {
                reset();
            } else if (!this.hasIncorrectData) {
                if (this.waitingForValidData) {
                    ObdManager.sLogger.d("Already waiting for valid PID data, PID : " + this.pid + " ,  Value : " + value);
                    return;
                }
                this.handler.removeCallbacks(this.invalidPidRunnable);
                this.handler.postDelayed(this.invalidPidRunnable, getWaitForValidDataTimeout());
                this.waitingForValidData = true;
            }
        }
    }

    public enum ConnectionType {
        POWER_ONLY(0),
        OBD(1);

        private int value;
        ConnectionType(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    public enum Firmware {
        OLD_320(R.raw.update3_2_0),
        NEW_421(R.raw.update4_2_1);
        
        public int resource;

        private Firmware(int resource) {
            this.resource = resource;
        }
    }

    public static class ObdConnectionStatusEvent {
        public boolean connected;

        ObdConnectionStatusEvent(boolean connected) {
            this.connected = connected;
        }
    }

    public static class ObdPidChangeEvent {
        public Pid pid;

        ObdPidChangeEvent(Pid pid) {
            this.pid = pid;
        }
    }

    public static class ObdPidReadEvent {
        public PidSet readPids;

        public ObdPidReadEvent(List<Pid> readPids) {
            this.readPids = new PidSet((List) readPids);
        }
    }

    public static class ObdSupportedPidsChangedEvent {
    }

    public static ObdManager getInstance() {
        return singleton;
    }

    public ConnectionType getConnectionType() {
        return this.connectionType;
    }

    private ObdManager() {
        Mortar.inject(HudApplication.getAppContext(), this);
        this.obdCanBusRecordingPolicy = new ObdCanBusRecordingPolicy(HudApplication.getAppContext(), this.sharedPreferences, this, this.tripManager);
        this.carServiceConnector = new CarServiceConnector(HudApplication.getAppContext());
        this.obdDeviceConfigurationManager = new ObdDeviceConfigurationManager(this.carServiceConnector);
        this.minimalScan = new ScanSchedule();
        this.minimalScan.addPid(Pids.VEHICLE_SPEED, 100);
        this.minimalScan.addPid(Pids.FUEL_LEVEL, 15000);
        this.minimalScan.addPid(Pids.DISTANCE_TRAVELLED, 60000);
        this.minimalScan.addPid(Pids.ENGINE_RPM, SLOW_RPM_UPDATE_INTERVAL);
        this.fullScan = new ScanSchedule(this.minimalScan);
        this.fullScan.addPid(Pids.MANIFOLD_AIR_PRESSURE, GENERIC_UPDATE_INTERVAL);
        this.fullScan.addPid(Pids.ENGINE_RPM, RPM_UPDATE_INTERVAL);
        this.fullScan.addPid(Pids.MASS_AIRFLOW, GENERIC_UPDATE_INTERVAL);
        this.fullScan.addPid(Pids.ENGINE_OIL_TEMPERATURE, GENERIC_UPDATE_INTERVAL);
        this.fullScan.addPid(Pids.AMBIENT_AIR_TEMRERATURE, GENERIC_UPDATE_INTERVAL);
        this.fullScan.addPid(Pids.INSTANTANEOUS_FUEL_CONSUMPTION, MPG_UPDATE_INTERVAL);
        this.fullScan.addPid(Pids.ENGINE_COOLANT_TEMPERATURE, GENERIC_UPDATE_INTERVAL);
        this.fullScan.addPid(Pids.ENGINE_OIL_PRESSURE, GENERIC_UPDATE_INTERVAL);
        this.fullScan.addPid(Pids.ENGINE_TRIP_FUEL, GENERIC_UPDATE_INTERVAL);
        this.fullScan.addPid(Pids.TOTAL_VEHICLE_DISTANCE, GENERIC_UPDATE_INTERVAL);
        this.bus.register(this);
        this.bus.register(this.obdDeviceConfigurationManager);
        this.bus.register(this.obdCanBusRecordingPolicy);
        sTelemetryLogger.v("SESSION started");
        this.handler = new Handler(Looper.getMainLooper());
        this.fuelPidCheck = new PidCheck(47, this.handler) {
            public boolean isPidValueValid(double value) {
                return value > 0.0d;
            }

            public void invalidatePid(int pid) {
                ObdManager.this.obdPidsCache.remove(Integer.valueOf(47));
                if (ObdManager.this.supportedPidSet != null) {
                    ObdManager.this.supportedPidSet.remove(47);
                    ObdManager.this.bus.post(new ObdSupportedPidsChangedEvent());
                }
                AnalyticsSupport.recordIncorrectFuelData();
            }

            public long getWaitForValidDataTimeout() {
                return 45000;
            }
        };
        this.handler.post(new Runnable() {
            public void run() {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        Logger.flush();
                    }
                }, 6);
                ObdManager.this.handler.postDelayed(this, 30000);
            }
        });
    }

    public String getObdChipFirmwareVersion() {
        return this.obdChipFirmwareVersion;
    }

    void serviceConnected() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                ObdManager.this.updateListener();
            }
        }, 6);
    }

    public void updateListener() {
        ICarService carService = this.carServiceConnector.getCarApi();
        if (carService != null) {
            try {
                carService.setCANBusMonitoringListener(this.canBusMonitoringListener);
            } catch (RemoteException e) {
                sLogger.e("RemoteException ", e);
            }
            try {
                this.firstScan = true;
                int connectionState = carService.getConnectionState();
                switch (connectionState) {
                    case 2:
                    case 4:
                        this.obdChipFirmwareVersion = carService.getObdChipFirmwareVersion();
                        sLogger.d("Obd chip firmware version " + this.obdChipFirmwareVersion + ", Connection State :" + connectionState);
                        break;
                    case 5:
                        sLogger.d("ObdService is in Sleeping state, waking it up");
                        carService.wakeup();
                        break;
                }
                updateConnectionState(carService.getConnectionState());
                monitorBatteryVoltage();
                sLogger.i("adding listener for " + this.schedule);
                if (this.schedule != null) {
                    carService.updateScan(this.schedule, this.pidListener);
                }
                if (this.obdCanBusRecordingPolicy.isCanBusMonitoringNeeded()) {
                    sLogger.d("Start CAN bus monitoring");
                    carService.startCanBusMonitoring();
                    return;
                }
                sLogger.d("stop CAN bus monitoring");
                carService.stopCanBusMonitoring();
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public ICarService getCarService() {
        if (this.carServiceConnector != null) {
            return this.carServiceConnector.getCarApi();
        }
        return null;
    }

    @Subscribe
    public void onWakeUp(Wakeup wakeup) {
        sLogger.d("onWakeUp");
        updateListener();
    }

    public void monitorBatteryVoltage() {
        sLogger.i("monitorBatteryVoltage()");
        this.handler.removeCallbacks(this.periodicCheckRunnable);
        this.handler.postDelayed(this.periodicCheckRunnable, 4000);
    }

    private void setConnected(boolean connected) {
        if (this.obdConnected != connected) {
            if (this.driveRecorder != null) {
                this.driveRecorder.setRealObdConnected(connected);
            }
            this.fuelPidCheck.reset();
            this.obdDeviceConfigurationManager.setConnectionState(connected);
            this.obdConnected = connected;
            if (connected) {
                try {
                    ICarService api = this.carServiceConnector.getCarApi();
                    this.vin = api.getVIN();
                    onVinRead(this.vin);
                    this.ecus = api.getEcus();
                    this.protocol = api.getProtocol();
                    this.isCheckEngineLightOn = api.isCheckEngineLightOn();
                    this.troubleCodes = api.getTroubleCodes();
                    this.obdChipFirmwareVersion = api.getObdChipFirmwareVersion();
                    sLogger.d("Connected, Obd chip firmware version " + this.obdChipFirmwareVersion);
                } catch (Throwable t) {
                    sLogger.d("Failed to read vin/ecu/protocol after connecting", t);
                }
            } else {
                this.firstScan = true;
                this.vin = null;
                this.ecus = null;
                this.protocol = null;
                this.speedManager.setObdSpeed(-1, 0);
                if (this.supportedPidSet != null) {
                    this.supportedPidSet.clear();
                }
                this.obdPidsCache.clear();
            }
            this.bus.post(connected ? OBD_CONNECTED : OBD_NOT_CONNECTED);
            this.obdCanBusRecordingPolicy.onObdConnectionStateChanged(connected);
        }
    }

    void serviceDisconnected() {
        setConnected(false);
    }

    public boolean isConnected() {
        return this.obdConnected;
    }

    public boolean isSleeping() {
        ICarService carService = this.carServiceConnector.getCarApi();
        if (carService == null) {
            return false;
        }
        try {
            if (carService.getConnectionState() == 5) {
                return true;
            }
            return false;
        } catch (RemoteException e) {
            return false;
        }
    }

    private synchronized void onVinRead(String vin) {
        SharedPreferences sharedPreferences = RemoteDeviceManager.getInstance().getSharedPreferences();
        String savedVin = sharedPreferences.getString(VEHICLE_VIN, null);
        if (TextUtils.equals(vin, savedVin)) {
            sLogger.d("Vin has not changed , Vin " + vin);
        } else {
            sLogger.d("Vin changed , Old :" + savedVin + ", New Vin :" + vin);
            if (TextUtils.isEmpty(vin)) {
                sharedPreferences.edit().remove(VEHICLE_VIN).apply();
            } else {
                sharedPreferences.edit().putString(VEHICLE_VIN, vin).apply();
            }
            resetCumulativeMileageData();
        }
    }

    private void resetCumulativeMileageData() {
        SharedPreferences preferences = RemoteDeviceManager.getInstance().getSharedPreferences();
        sLogger.d("Resetting the cumulative mileage statistics");
        preferences.edit().putLong(FIRST_ODOMETER_READING_KM, 0).putLong(LAST_ODOMETER_READING_KM, 0).putLong(TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS, 0).apply();
        this.totalDistanceInCurrentSessionPersisted = 0;
        this.odoMeterReadingValidated = false;
    }

    private void reportMileageEvent() {
        long currentTime = SystemClock.elapsedRealtime();
        if (this.mileageEventLastReportTime == 0 || currentTime - this.mileageEventLastReportTime > MINIMUM_TIME_BETWEEN_MILEAGE_REPORTING) {
            SharedPreferences preferences = RemoteDeviceManager.getInstance().getSharedPreferences();
            long firstOdoMeterReadingKms = preferences.getLong(FIRST_ODOMETER_READING_KM, 0);
            long lastOdoMeterReadingKms = preferences.getLong(LAST_ODOMETER_READING_KM, 0);
            double totalDistanceVehicleTravelledInKms = (double) (lastOdoMeterReadingKms - firstOdoMeterReadingKms);
            sLogger.d("Initial reading (KM) : " + firstOdoMeterReadingKms + ", Current reading (KM) : " + lastOdoMeterReadingKms + " , Total (KM) : " + totalDistanceVehicleTravelledInKms);
            double totalDistanceTravelledWithNavdyInMeters = (double) preferences.getLong(TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS, 0);
            sLogger.d("Distance travelled with Navdy(Meters) : " + totalDistanceTravelledWithNavdyInMeters);
            double totalDistanceTravelledWithNavdyKms = totalDistanceTravelledWithNavdyInMeters / 1000.0d;
            if (totalDistanceVehicleTravelledInKms > 0.0d && totalDistanceTravelledWithNavdyKms > 0.0d) {
                double usageRate = totalDistanceVehicleTravelledInKms / totalDistanceVehicleTravelledInKms;
                sLogger.d("Reporting the Navdy Mileage Vehicle (KMs) : " + totalDistanceVehicleTravelledInKms + " , Navdy (KMs) : " + totalDistanceVehicleTravelledInKms + " , Navdy Usage rate " + usageRate);
                AnalyticsSupport.recordNavdyMileageEvent(totalDistanceVehicleTravelledInKms, totalDistanceTravelledWithNavdyKms, usageRate);
                this.mileageEventLastReportTime = currentTime;
            }
        }
    }

    private synchronized void updateCumulativeVehicleDistanceTravelled(long odoMeterReadingInKms) {
        if (odoMeterReadingInKms > 0) {
            sLogger.d("New odometer reading (KMs) : " + odoMeterReadingInKms);
            SharedPreferences preferences = RemoteDeviceManager.getInstance().getSharedPreferences();
            if (!this.odoMeterReadingValidated) {
                sLogger.d("Validating the Cumulative mileage data");
                double savedOdoMeterReadingKms = (double) preferences.getLong(LAST_ODOMETER_READING_KM, 0);
                if (savedOdoMeterReadingKms == 0.0d) {
                    sLogger.d("No saved odo meter reading, starting fresh");
                    resetCumulativeMileageData();
                    preferences.edit().putLong(FIRST_ODOMETER_READING_KM, odoMeterReadingInKms).apply();
                } else if (((double) odoMeterReadingInKms) < savedOdoMeterReadingKms) {
                    sLogger.d("Odometer reading mismatch with saved reading, resetting, Saved :" + savedOdoMeterReadingKms + ", New : " + odoMeterReadingInKms);
                    resetCumulativeMileageData();
                    preferences.edit().putLong(FIRST_ODOMETER_READING_KM, odoMeterReadingInKms).apply();
                }
                this.odoMeterReadingValidated = true;
            }
            preferences.edit().putLong(LAST_ODOMETER_READING_KM, odoMeterReadingInKms).apply();
            TripManager tripManager = RemoteDeviceManager.getInstance().getTripManager();
            long savedDistanceTravelledWithNavdyMeters = preferences.getLong(TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS, 0);
            long totalDistanceTravelledMeters = (long) tripManager.getTotalDistanceTravelled();
            sLogger.d("Saving total distance travelled in current session, before :" + savedDistanceTravelledWithNavdyMeters + ", Current Trip mileage : " + tripManager.getTotalDistanceTravelled());
            long newDistanceAfterLastSaveMeters = totalDistanceTravelledMeters - this.totalDistanceInCurrentSessionPersisted;
            if (newDistanceAfterLastSaveMeters > 0) {
                preferences.edit().putLong(TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS, savedDistanceTravelledWithNavdyMeters + newDistanceAfterLastSaveMeters).apply();
                this.totalDistanceInCurrentSessionPersisted = totalDistanceTravelledMeters;
                reportMileageEvent();
            }
        }
    }

    private void updateConnectionState(int newState) {
        if (newState == 2) {
            setConnected(true);
        } else {
            setConnected(false);
        }
        if (this.connectionType == ConnectionType.POWER_ONLY && newState > 1) {
            this.connectionType = ConnectionType.OBD;
        }
    }

    private void setScanSchedule(ScanSchedule schedule) {
        if (schedule != this.schedule) {
            this.schedule = schedule;
            updateListener();
        }
    }

    @Subscribe
    public void onDrivingStateChange(DrivingStateChange event) {
        if (event.driving && !isConnected()) {
            ICarService api = this.carServiceConnector.getCarApi();
            if (api != null) {
                try {
                    sLogger.i("Driving started - triggering obd scan");
                    api.rescan();
                } catch (RemoteException e) {
                    sLogger.e("Failed to trigger OBD rescan");
                }
            }
        }
    }

    public void enableInstantaneousMode(boolean enable) {
        this.obdCanBusRecordingPolicy.onInstantaneousModeChanged(enable);
        if (enable) {
            setScanSchedule(this.fullScan);
        } else if (DriveRecorder.isAutoRecordingEnabled()) {
            setScanSchedule(this.fullScan);
        } else {
            setScanSchedule(this.minimalScan);
        }
    }

    public void enableObdScanning(boolean enable) {
        ICarService carService = this.carServiceConnector.getCarApi();
        if (carService != null) {
            try {
                sLogger.i("setting obd scanning enabled:" + enable);
                carService.setObdPidsScanningEnabled(enable);
            } catch (RemoteException e) {
                sLogger.e("Error while " + (enable ? "enabling " : "disabling ") + "Obd PIDs scanning ", e);
            }
        }
    }

    public void sleep(boolean deep) {
        ICarService carService = this.carServiceConnector.getCarApi();
        if (carService != null) {
            try {
                carService.sleep(deep);
            } catch (RemoteException e) {
                sLogger.e("Error during call to sleep Remote Exception " + e.getCause());
            }
        }
    }

    public void wakeup() {
        ICarService carService = this.carServiceConnector.getCarApi();
        if (carService != null) {
            try {
                carService.wakeup();
            } catch (RemoteException e) {
                sLogger.e("Error during call to sleep Remote Exception " + e.getCause());
            }
        }
    }

    @Subscribe
    public void onShutdown(Shutdown shutdown) {
        if (shutdown.state == State.SHUTTING_DOWN) {
            this.carServiceConnector.shutdown();
        }
    }

    @Subscribe
    public void onObdStatusRequest(ObdStatusRequest request) {
        int speed = this.speedManager.getObdSpeed();
        this.bus.post(new RemoteEvent(new ObdStatusResponse(RequestStatus.REQUEST_SUCCESS, Boolean.valueOf(isConnected()), this.vin, speed != -1 ? Integer.valueOf(speed) : null)));
    }

    public int getFuelLevel() {
        if (this.fuelPidCheck.hasIncorrectData || this.fuelPidCheck.isWaitingForValidData()) {
            return -1;
        }
        return (int) getPidValue(47);
    }

    public int getEngineRpm() {
        return (int) getPidValue(12);
    }

    public int getDistanceToEmpty() {
        return (int) getPidValue(Pids.DISTANCE_TO_EMPTY);
    }
    
    public int getIntakePressure() {
        return (int) getPidValue(Pids.MANIFOLD_AIR_PRESSURE);
    }

    public double getInstantFuelConsumption() {
        return (double) ((int) getPidValue(256));
    }

    public int getDistanceTravelled() {
        return (int) getPidValue(49);
    }

    public long getDistanceTravelledWithNavdy() {
        return RemoteDeviceManager.getInstance().getSharedPreferences().getLong(TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS, -1);
    }

    public String getVin() {
        return this.vin;
    }

    public List<ECU> getEcus() {
        return this.ecus;
    }

    public String getProtocol() {
        return this.protocol;
    }

    public PidSet getSupportedPids() {
        return this.supportedPidSet;
    }

    public double getPidValue(int pid) {
        if (this.obdPidsCache.containsKey(Integer.valueOf(pid))) {
            return ((Double) this.obdPidsCache.get(Integer.valueOf(pid))).doubleValue();
        }
        return -1.0d;
    }

    public double getBatteryVoltage() {
        ICarService carService = this.carServiceConnector.getCarApi();
        if (carService != null) {
            try {
                this.batteryVoltage = carService.getBatteryVoltage();
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
        return this.batteryVoltage;
    }

    public double getMinBatteryVoltage() {
        return this.minBatteryVoltage;
    }

    public double getMaxBatteryVoltage() {
        return this.maxBatteryVoltage;
    }

    public void setRecordingPidListener(IPidListener recordingPidListener) {
        this.recordingPidListener = recordingPidListener;
    }

    public void injectObdData(List<Pid> pidsread, List<Pid> pidschanged) {
        try {
            this.pidListener.pidsRead(pidsread, pidschanged);
        } catch (RemoteException e) {
            sLogger.e("Remote exception when injecting fake obd data");
        }
    }

    public void setSupportedPidSet(PidSet supportedPids) {
        if (this.firstScan) {
            this.firstScan = true;
            this.supportedPidSet = supportedPids;
        } else if (this.supportedPidSet != null) {
            this.supportedPidSet.merge(supportedPids);
        } else {
            this.supportedPidSet = supportedPids;
        }
        this.bus.post(new ObdSupportedPidsChangedEvent());
    }

    public ObdDeviceConfigurationManager getObdDeviceConfigurationManager() {
        return this.obdDeviceConfigurationManager;
    }

    @Subscribe
    public void onDriverProfileChanged(DriverProfileChanged changed) {
        updateObdScanSetting();
    }

    @Subscribe
    public void onDriverProfileUpdated(DriverProfileUpdated updated) {
        updateObdScanSetting();
    }

    private void updateObdScanSetting() {
        if (this.driverProfileManager != null) {
            DriverProfile profile = this.driverProfileManager.getCurrentProfile();
            if (profile != null) {
                if (!profile.isAutoOnEnabled()) {
                    this.obdDeviceConfigurationManager.setAutoOnEnabled(false);
                } else if (!(TextUtils.isEmpty(profile.getCarMake()) && TextUtils.isEmpty(profile.getCarModel()) && TextUtils.isEmpty(profile.getCarYear()))) {
                    this.obdCanBusRecordingPolicy.onCarDetailsAvailable(profile.getCarMake(), profile.getCarModel(), profile.getCarYear());
                    this.obdDeviceConfigurationManager.setCarDetails(profile.getCarMake(), profile.getCarModel(), profile.getCarYear());
                }
                ObdScanSetting scanSetting = profile.getObdScanSetting();
                sLogger.d("ObdScanSetting received : " + scanSetting);
                if (scanSetting != null) {
                    ObdScanSetting savedSetting;
                    boolean writeSetting = false;
                    SharedPreferences preferences = RemoteDeviceManager.getInstance().getSharedPreferences();
                    if (preferences.getBoolean(DEFAULT_SET, false)) {
                        writeSetting = true;
                        savedSetting = isObdScanningEnabled() ? ObdScanSetting.SCAN_DEFAULT_ON : ObdScanSetting.SCAN_DEFAULT_OFF;
                        preferences.edit().remove(DEFAULT_SET).apply();
                    } else {
                        savedSetting = getScanSetting(preferences);
                    }
                    long phoneModificationTime = profile.getObdBlacklistModificationTime();
                    long localModificationTime = preferences.getLong(BLACKLIST_MODIFICATION_TIME, 0);
                    boolean phoneNewer = phoneModificationTime > localModificationTime;
                    sLogger.d("saved setting:" + savedSetting + " blacklist updated: (" + phoneModificationTime + HereManeuverDisplayBuilder.SLASH + localModificationTime + ") (phone/local)");
                    sLogger.d("Is Obd enabled :" + isObdScanningEnabled());
                    boolean isDefault = savedSetting == ObdScanSetting.SCAN_DEFAULT_OFF || savedSetting == ObdScanSetting.SCAN_DEFAULT_ON;
                    switch (scanSetting) {
                        case SCAN_DEFAULT_ON:
                            if (phoneNewer && isDefault) {
                                sLogger.d("Setting default to on");
                                enableObdScanning(true);
                                writeSetting = true;
                                break;
                            }
                        case SCAN_ON:
                            enableObdScanning(true);
                            writeSetting = true;
                            break;
                        case SCAN_OFF:
                            enableObdScanning(false);
                            writeSetting = true;
                            break;
                        case SCAN_DEFAULT_OFF:
                            if (phoneNewer && isDefault) {
                                enableObdScanning(false);
                                writeSetting = true;
                                break;
                            }
                    }
                    if (writeSetting) {
                        sLogger.i("Writing scan setting:" + scanSetting + " modTime:" + phoneModificationTime);
                        preferences.edit().putString(SCAN_SETTING, scanSetting.name()).putLong(BLACKLIST_MODIFICATION_TIME, phoneModificationTime).apply();
                    }
                }
            }
        }
    }

    private ObdScanSetting getScanSetting(SharedPreferences preferences) {
        ObdScanSetting defaultSetting = ObdScanSetting.SCAN_DEFAULT_ON;
        try {
            defaultSetting = ObdScanSetting.valueOf(preferences.getString(SCAN_SETTING, defaultSetting.name()));
        } catch (Exception e) {
            sLogger.e("Failed to parse saved scan setting ", e);
        }
        return defaultSetting;
    }

    public boolean isObdScanningEnabled() {
        ICarService carService = this.carServiceConnector.getCarApi();
        if (carService != null) {
            try {
                return carService.isObdPidsScanningEnabled();
            } catch (RemoteException e) {
                sLogger.e("RemoteException ", e);
            }
        }
        return false;
    }

    public boolean isSpeedPidAvailable() {
        if (this.supportedPidSet != null) {
            return this.supportedPidSet.contains(13);
        }
        return false;
    }

    public void setDriveRecorder(DriveRecorder driveRecorder) {
        this.driveRecorder = driveRecorder;
    }

    public DriveRecorder getDriveRecorder() {
        return this.driveRecorder;
    }

    public void setMode(int mode, boolean persistent) {
        switch (mode) {
            case 0:
            case 1:
                ICarService carService = this.carServiceConnector.getCarApi();
                if (carService != null) {
                    try {
                        carService.setMode(mode, persistent);
                        return;
                    } catch (RemoteException e) {
                        sLogger.e("RemoteException :" + e);
                        return;
                    }
                }
                return;
            default:
                return;
        }
    }

    public void updateFirmware(Firmware firmware) {
        sLogger.d("Updating the firmware of the OBD chip , Version : " + (firmware == Firmware.OLD_320 ? "3.2.0" : FIRMWARE_VERSION));
        ICarService carService = this.carServiceConnector.getCarApi();
        InputStream is = HudApplication.getAppContext().getResources().openRawResource(firmware.resource);
        String absolutePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "update.bin";
        File file = new File(absolutePath);
        if (file.exists()) {
            IOUtils.deleteFile(HudApplication.getAppContext(), file.getAbsolutePath());
        }
        try {
            IOUtils.copyFile(file.getAbsolutePath(), is);
            try {
                carService.updateFirmware(FIRMWARE_VERSION, absolutePath);
            } catch (RemoteException e) {
                sLogger.e("Remote Exception while updating firmware ", e);
            }
        } catch (Throwable e2) {
            sLogger.e("Error writing to the file ", e2);
        }
    }

    public ObdCanBusRecordingPolicy getObdCanBusRecordingPolicy() {
        return this.obdCanBusRecordingPolicy;
    }

    public boolean isCheckEngineLightOn() {
        return this.isCheckEngineLightOn;
    }

    public List<String> getTroubleCodes() {
        return this.troubleCodes;
    }
}
