package com.navdy.hud.app.gesture;

import com.navdy.service.library.events.input.GestureEvent;

public class GestureDetector {
    private static final float FOV_RANGE = 240.0f;
    private static final float FULL_RANGE = 640.0f;
    public float defaultOffset = 0.5f;
    public float defaultSensitivity = 1.0f;
    protected GestureEvent lastEvent;
    private GestureListener listener;
    protected float offset = -1.0f;
    protected int origin;
    protected int pinchDuration = 0;
    protected GestureEvent startEvent;
    boolean useRelativeOrigin = false;
    protected boolean useRelativePosition = true;

    public interface GestureListener {
        void onClick();

        void onTrackHand(float f);
    }

    public GestureDetector(GestureListener listener) {
        this.listener = listener;
    }

    private float calculatePosition(GestureEvent event) {
        if (event == null) {
            return -1.0f;
        }
        int x = event.x.intValue();
        if (this.useRelativePosition) {
            return clamp(this.offset + (((float) (x - this.origin)) / (this.defaultSensitivity * 120.0f)), 0.0f, 1.0f);
        }
        return clamp((((float) x) - 200.0f) / FOV_RANGE, 0.0f, 1.0f);
    }

    private boolean hasPosition(GestureEvent event) {
        return (event.x == null || event.y == null) ? false : true;
    }

    private void trackHand(GestureEvent event) {
        if (hasPosition(event)) {
            if (this.startEvent == null) {
                this.startEvent = event;
                int xPos = event.x.intValue();
                if (this.offset < 0.0f) {
                    this.offset = this.defaultOffset;
                }
                this.origin = xPos;
            }
            this.listener.onTrackHand(calculatePosition(event));
        }
    }

    private float clamp(float value, float min, float max) {
        return Math.min(Math.max(min, value), max);
    }

    public boolean onGesture(GestureEvent event) {
        switch (event.gesture) {
            case GESTURE_FINGER_DOWN:
            case GESTURE_HAND_DOWN:
                if (this.useRelativeOrigin && hasPosition(this.lastEvent)) {
                    this.offset = calculatePosition(this.lastEvent);
                } else {
                    this.offset = -1.0f;
                }
                this.startEvent = null;
                break;
            case GESTURE_FINGER_TO_PINCH:
            case GESTURE_HAND_TO_FIST:
                this.pinchDuration = 0;
                break;
            case GESTURE_PINCH:
            case GESTURE_FIST:
                this.pinchDuration++;
                if (this.pinchDuration == 1) {
                    this.listener.onClick();
                    break;
                }
                break;
            case GESTURE_FINGER:
            case GESTURE_PALM:
                trackHand(event);
                break;
        }
        this.lastEvent = event;
        return true;
    }
}
