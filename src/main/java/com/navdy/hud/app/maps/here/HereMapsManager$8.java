package com.navdy.hud.app.maps.here;

class HereMapsManager$8 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapsManager this$0;
    
    HereMapsManager$8(com.navdy.hud.app.maps.here.HereMapsManager a) {

        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.here.android.mpa.mapping.MapTrafficLayer a = this.this$0.map.getMapTrafficLayer();
        a.setDisplayFilter(com.here.android.mpa.mapping.TrafficEvent.Severity.NORMAL);
        a.setEnabled(com.here.android.mpa.mapping.MapTrafficLayer.RenderLayer.FLOW, true);
        a.setEnabled(com.here.android.mpa.mapping.MapTrafficLayer.RenderLayer.INCIDENT, true);
        a.setEnabled(com.here.android.mpa.mapping.MapTrafficLayer.RenderLayer.ONROUTE, true);
        this.this$0.map.setTrafficInfoVisible(true);
    }
}
