package com.navdy.hud.app.maps.notification;

class RouteCalculationNotification$3 implements com.navdy.hud.app.ui.component.ChoiceLayout2.IListener {
    final com.navdy.hud.app.maps.notification.RouteCalculationNotification this$0;
    
    RouteCalculationNotification$3(com.navdy.hud.app.maps.notification.RouteCalculationNotification a) {
        super();
        this.this$0 = a;
    }
    
    public void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection a) {
        if (com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$000(this.this$0) != null) {
            switch(a.id) {
                case 4: {
                    com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$200().v("retry");
                    com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent a0 = com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$900(this.this$0).getCurrentRouteCalcEvent();
                    label0: {
                        label1: {
                            if (a0 == null) {
                                break label1;
                            }
                            if (a0.request != null) {
                                break label0;
                            }
                        }
                        com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$200().v("retry: request not found");
                        com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$400(this.this$0);
                        break;
                    }
                    com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$700(this.this$0).post(a0.request);
                    com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$700(this.this$0).post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a0.request));
                    break;
                }
                case 3: {
                    com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$200().v("dismiss route");
                    com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$502(this.this$0, true);
                    com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$400(this.this$0);
                    break;
                }
                case 2: {
                    com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$200().v("show route picker: choice");
                    com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$502(this.this$0, true);
                    com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$800(this.this$0);
                    break;
                }
                case 1: {
                    com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$200().v("cancel");
                    com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$502(this.this$0, true);
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordRouteSelectionCancelled();
                    if (com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$100(this.this$0) == null) {
                        com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$200().v("cancel route");
                        if (com.navdy.hud.app.maps.here.HereRouteManager.cancelActiveRouteRequest()) {
                            com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$200().v("route calc was cancelled");
                        } else {
                            com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$200().v("route calc was not cancelled, may be it is not running");
                            if (android.text.TextUtils.isEmpty((CharSequence)com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$600(this.this$0))) {
                                com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$200().v("route calc was not cancelled,no route id");
                            } else {
                                com.navdy.service.library.events.navigation.NavigationRouteResponse a1 = new com.navdy.service.library.events.navigation.NavigationRouteResponse.Builder().requestId(com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$600(this.this$0)).status(com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED).destination(new com.navdy.service.library.events.location.Coordinate.Builder().latitude(Double.valueOf(0.0)).longitude(Double.valueOf(0.0)).build()).build();
                                com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$700(this.this$0).post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a1));
                                com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$200().v(new StringBuilder().append("route calc event sent:").append(com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$600(this.this$0)).toString());
                            }
                        }
                    }
                    com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$400(this.this$0);
                    break;
                }
            }
        }
    }
    
    public void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection a) {
    }
}
