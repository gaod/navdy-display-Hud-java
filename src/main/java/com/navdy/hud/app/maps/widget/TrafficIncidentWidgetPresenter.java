package com.navdy.hud.app.maps.widget;
import android.content.Context;

import com.navdy.hud.app.R;
import com.squareup.otto.Subscribe;

import static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.TRAFFIC_INCIDENT_GAUGE_ID;

public class TrafficIncidentWidgetPresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    private static android.os.Handler handler;
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.squareup.otto.Bus bus;
    private boolean registered;
    private android.widget.TextView textView;
    private String widgetName;

    static {
        sLogger = new com.navdy.service.library.log.Logger("TrafficIncidentWidget");
        handler = new android.os.Handler(android.os.Looper.getMainLooper());
    }

    public TrafficIncidentWidgetPresenter(Context context) {
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        this.widgetName = context.getResources().getString(R.string.widget_traffic_incident);
    }

    static android.widget.TextView access$000(com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter a) {
        return a.textView;
    }

    static android.os.Handler access$100() {
        return handler;
    }

    private void setText() {
        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.incident_1);
        boolean b = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized();
        label1: {
            label0: {
                if (!b) {
                    break label0;
                }
                if (!com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isTrafficUpdaterRunning()) {
                    break label0;
                }
                this.textView.setText(R.string.widget_traffic_normal);
                break label1;
            }
            this.textView.setText(R.string.widget_traffic_inactive);
        }
    }

    private void unregister() {
        if (this.registered) {
            sLogger.v("unregister");
            this.bus.unregister(this);
            this.registered = false;
        }
    }

    public android.graphics.drawable.Drawable getDrawable() {
        return null;
    }

    public String getWidgetIdentifier() {
        return TRAFFIC_INCIDENT_GAUGE_ID;
    }

    public String getWidgetName() {
        return this.widgetName;
    }

    @Subscribe
    public void onDisplayTrafficIncident(com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident a) {
        try {
            if (this.textView != null) {
                sLogger.v("DisplayTrafficIncident type:" + a.type + ", category:" + a.category + ", title:" + a.title + ", description:" + a.description + ", affectedStreet:" + a.affectedStreet + ", distanceToIncident:" + a.distanceToIncident + ", reported:" + a.reported + ", updated:" + a.updated + ", icon:" + a.icon);
                switch(com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter$2.$SwitchMap$com$navdy$hud$app$maps$MapEvents$DisplayTrafficIncident$Type[a.type.ordinal()]) {

                    case 6: {
                        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.incident_1);
                        this.textView.setText(R.string.widget_traffic_blocking);
                        break;
                    }
                    case 5: {

                        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.incident_1);
                        this.textView.setText(R.string.widget_traffic_very_high);
                        break;
                    }
                    case 4: {
                        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.incident_1);
                        this.textView.setText(R.string.widget_traffic_high);
                        break;
                    }
                    case 3: {
                        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.incident_1);
                        this.textView.setText(R.string.widget_traffic_failed);
                        break;
                    }
                    case 2: {
                        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.incident_1);
                        this.textView.setText(R.string.widget_traffic_inactive);
                        break;
                    }
                    case 1: {
                        this.textView.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.incident_1);
                        this.textView.setText(R.string.widget_traffic_normal);
                        break;
                    }
                    default: {
                        com.navdy.service.library.task.TaskManager.getInstance().execute(new TrafficIncidentWidgetPresenter$1(this, a), 1);
                    }
                }
            }
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
    }

    public void setView(com.navdy.hud.app.view.DashboardWidgetView a, android.os.Bundle a0) {
        if (a == null) {
            this.unregister();
            this.textView = null;
            super.setView(null, a0);
        } else {
            boolean b = a0 != null && a0.getBoolean("EXTRA_IS_ACTIVE", false);
            a.setContentView(R.layout.maps_traffic_incident_widget);
            this.textView = (android.widget.TextView)a.findViewById(R.id.incidentInfo);
            this.setText();
            super.setView(a, a0);
            if (b) {
                if (this.registered) {
                    sLogger.v("already register");
                } else {
                    this.bus.register(this);
                    this.registered = true;
                    sLogger.v("register");
                }
            } else {
                sLogger.v("not active");
                this.unregister();
            }
        }
    }

    protected void updateGauge() {
    }
}
