package com.navdy.hud.app.maps.here;

class HereMapsManager$2$1$1 implements Runnable {
    private final com.navdy.hud.app.maps.here.HereMapsManager$2$1 this$2;
    private final com.here.android.mpa.odml.MapPackage val$mapPackage;
    
    HereMapsManager$2$1$1(com.navdy.hud.app.maps.here.HereMapsManager$2$1 a, com.here.android.mpa.odml.MapPackage a0) {
        super();
        this.this$2 = a;
        this.val$mapPackage = a0;
    }
    
    public void run() {
        this.this$2.this$1.this$0.printMapPackages(this.val$mapPackage, java.util.EnumSet.of(com.here.android.mpa.odml.MapPackage.InstallationState.INSTALLED, com.here.android.mpa.odml.MapPackage.InstallationState.PARTIALLY_INSTALLED));
        HereMapsManager.sLogger.v("initMapLoader: map package count:" + this.this$2.this$1.this$0.mapPackageCount);
        this.this$2.this$1.this$0.mapDataVerified = true;
    }
}
