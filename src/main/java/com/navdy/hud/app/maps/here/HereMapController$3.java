package com.navdy.hud.app.maps.here;

class HereMapController$3 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapController this$0;
    private final com.here.android.mpa.mapping.Map.Animation val$animation;
    private final com.here.android.mpa.common.GeoCoordinate val$center;
    private final float val$orientation;
    private final float val$tilt;
    private final double val$zoom;
    
    HereMapController$3(com.navdy.hud.app.maps.here.HereMapController a, com.here.android.mpa.common.GeoCoordinate a0, com.here.android.mpa.mapping.Map.Animation a1, double d, float f, float f0) {
        super();
        this.this$0 = a;
        this.val$center = a0;
        this.val$animation = a1;
        this.val$zoom = d;
        this.val$orientation = f;
        this.val$tilt = f0;
    }
    
    public void run() {
        this.this$0.map.setCenter(this.val$center, this.val$animation, this.val$zoom, this.val$orientation, this.val$tilt);
    }
}
