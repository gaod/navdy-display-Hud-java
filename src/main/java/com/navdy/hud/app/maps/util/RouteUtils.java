package com.navdy.hud.app.maps.util;
import com.navdy.hud.app.R;

public class RouteUtils {
    final private static double SCALE_FACTOR = 1048576.0;
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.util.RouteUtils.class);
    }
    
    public RouteUtils() {
    }
    
    public static String formatEtaMinutes(android.content.res.Resources a, int i) {
        String s = null;
        if (i >= 60) {
            if (i >= 1440) {
                int i0 = i / 1440;
                int i1 = (i - i0 * 1440) / 60;
                if (i1 <= 0) {
                    Object[] a0 = new Object[1];
                    a0[0] = Integer.valueOf(i0);
                    s = a.getString(R.string.eta_time_day_no_hour, a0);
                } else {
                    Object[] a1 = new Object[2];
                    a1[0] = Integer.valueOf(i0);
                    a1[1] = Integer.valueOf(i1);
                    s = a.getString(R.string.eta_time_day, a1);
                }
            } else {
                int i2 = i / 60;
                int i3 = i - i2 * 60;
                if (i3 <= 0) {
                    Object[] a2 = new Object[1];
                    a2[0] = Integer.valueOf(i2);
                    s = a.getString(R.string.eta_time_hour_no_min, a2);
                } else {
                    Object[] a3 = new Object[2];
                    a3[0] = Integer.valueOf(i2);
                    a3[1] = Integer.valueOf(i3);
                    s = a.getString(R.string.eta_time_hour, a3);
                }
            }
        } else {
            Object[] a4 = new Object[1];
            a4[0] = Integer.valueOf(i);
            s = a.getString(R.string.eta_time_min, a4);
        }
        return s;
    }
    
    static com.navdy.hud.app.maps.util.RouteUtils$Bounds getLatLngBounds(java.util.List a) {
        double d = ((com.here.android.mpa.common.GeoCoordinate)a.get(0)).getLatitude();
        double d0 = ((com.here.android.mpa.common.GeoCoordinate)a.get(0)).getLongitude();
        java.util.Iterator a0 = a.iterator();
        double d1 = d;
        double d2 = d0;
        Object a1 = a0;
        while(((java.util.Iterator)a1).hasNext()) {
            com.here.android.mpa.common.GeoCoordinate a2 = (com.here.android.mpa.common.GeoCoordinate)((java.util.Iterator)a1).next();
            double d3 = a2.getLatitude();
            double d4 = a2.getLongitude();
            d1 = Math.min(d3, d1);
            d = Math.max(d3, d);
            d2 = Math.min(d4, d2);
            d0 = Math.max(d4, d0);
        }
        return new com.navdy.hud.app.maps.util.RouteUtils$Bounds(d0 - d2, d - d1);
    }
    
    public static java.util.List simplify(java.util.List a) {
        com.navdy.hud.app.maps.util.RouteUtils$Bounds a0 = com.navdy.hud.app.maps.util.RouteUtils.getLatLngBounds(a);
        return com.navdy.hud.app.maps.util.RouteUtils.simplify(a, 2000.0 * Math.max(a0.width, a0.height));
    }
    
    public static java.util.List simplify(java.util.List a, double d) {
        return com.navdy.hud.app.maps.util.RouteUtils.simplifyRadialDistance(a, d * d);
    }
    
    static java.util.List simplifyRadialDistance(java.util.List a, double d) {
        java.util.ArrayList a0 = null;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.size() != 0) {
                        break label0;
                    }
                }
                a0 = null;
                break label2;
            }
            float f = (float)((com.here.android.mpa.common.GeoCoordinate)a.get(0)).getLatitude();
            float f0 = (float)((com.here.android.mpa.common.GeoCoordinate)a.get(0)).getLongitude();
            a0 = new java.util.ArrayList();
            a0.add(Float.valueOf(f));
            a0.add(Float.valueOf(f0));
            int i = a.size();
            com.here.android.mpa.common.GeoCoordinate a1 = (com.here.android.mpa.common.GeoCoordinate)a.get(0);
            float f1 = (float)a1.getLatitude();
            float f2 = (float)a1.getLongitude();
            a0.add(Float.valueOf(f1));
            a0.add(Float.valueOf(f2));
            Object a2 = a;
            int i0 = 1;
            while(i0 < i - 1) {
                com.here.android.mpa.common.GeoCoordinate a3 = (com.here.android.mpa.common.GeoCoordinate)((java.util.List)a2).get(i0);
                float f3 = (float)a3.getLatitude();
                float f4 = (float)a3.getLongitude();
                double d0 = (double)(f3 - f) * 1048576.0;
                double d1 = (double)(f4 - f0) * 1048576.0;
                if (d0 * d0 + d1 * d1 > d) {
                    a0.add(Float.valueOf(f3));
                    a0.add(Float.valueOf(f4));
                    f = f3;
                    f0 = f4;
                }
                i0 = i0 + 1;
            }
            com.here.android.mpa.common.GeoCoordinate a4 = (com.here.android.mpa.common.GeoCoordinate)((java.util.List)a2).get(i - 1);
            float f5 = (float)a4.getLatitude();
            float f6 = (float)a4.getLongitude();
            a0.add(Float.valueOf(f5));
            a0.add(Float.valueOf(f6));
        }
        return (java.util.List)a0;
    }
}
