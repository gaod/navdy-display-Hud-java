package com.navdy.hud.app.maps.notification;
import android.view.View;

import com.navdy.hud.app.R;
import com.squareup.otto.Subscribe;

public class RouteCalculationNotification implements com.navdy.hud.app.framework.notifications.INotification {
    final public static com.navdy.hud.app.event.RemoteEvent CANCEL_CALC_TTS;
    final public static com.navdy.hud.app.event.RemoteEvent CANCEL_TBT_TTS;
    final private static float ICON_SCALE_FACTOR = 1.38f;
    final private static int NAV_LOOK_UP_TIMEOUT;
    final public static String ROUTE_CALC_TTS_ID;
    final public static String ROUTE_PICKER = "ROUTE_PICKER";
    final public static String ROUTE_TBT_TTS_ID;
    final private static int TAG_CANCEL = 1;
    final private static int TAG_DISMISS = 3;
    final private static int TAG_RETRY = 4;
    final private static int TAG_ROUTES = 2;
    final private static int TIMEOUT;
    private static java.util.List cancelRouteCalc;
    final public static String fastestRoute;
    final public static int notifBkColor;
    final private static android.content.res.Resources resources;
    private static java.util.List routeError;
    final private static String routeFailed;
    final private static com.navdy.service.library.log.Logger sLogger;
    final public static String shortestRoute;
    final private static int showRouteBkColor;
    private static java.util.List showRoutes;
    final private static int startTripImageContainerSize;
    final private static int startTripImageSize;
    final private static int startTripLeftMargin;
    final private static int startTripTextLeftMargin;
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.ui.component.ChoiceLayout2 choiceLayoutView;
    private java.util.List choices;
    private android.view.ViewGroup containerView;
    private com.navdy.hud.app.framework.notifications.INotificationController controller;
    private String destinationAddress;
    private String destinationDistance;
    private String destinationLabel;
    private boolean dismissedbyUser;
    private com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent event;
    private android.os.Handler handler;
    private int icon;
    private int iconBkColor;
    private com.navdy.hud.app.ui.component.image.IconColorImageView iconColorImageView;
    private com.navdy.hud.app.ui.component.image.InitialsImageView iconInitialsImageView;
    private int iconSide;
    private android.widget.ImageView iconSpinnerView;
    private String initials;
    private com.navdy.hud.app.ui.component.ChoiceLayout2.IListener listener;
    private android.animation.ObjectAnimator loadingAnimator;
    private com.navdy.service.library.events.destination.Destination lookupDestination;
    private com.navdy.service.library.events.places.DestinationSelectedRequest lookupRequest;
    private int notifColor;
    private boolean registered;
    private com.navdy.hud.app.maps.notification.RouteCalculationEventHandler routeCalculationEventHandler;
    private String routeId;
    private android.widget.TextView routeTtaView;
    private long startTime;
    private String subTitle;
    private android.widget.TextView subTitleView;
    private Runnable timeoutRunnable;
    private String title;
    private android.widget.TextView titleView;
    private android.text.SpannableStringBuilder tripDuration;
    private String tts;
    private boolean ttsPlayed;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    private Runnable waitForNavLookupRunnable;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.notification.RouteCalculationNotification.class);
        TIMEOUT = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(30L);
        NAV_LOOK_UP_TIMEOUT = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(10L);
        ROUTE_CALC_TTS_ID = new StringBuilder().append("route_calc_").append(java.util.UUID.randomUUID().toString()).toString();
        ROUTE_TBT_TTS_ID = new StringBuilder().append("route_tbt_").append(java.util.UUID.randomUUID().toString()).toString();
        CANCEL_CALC_TTS = new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.audio.CancelSpeechRequest(ROUTE_CALC_TTS_ID));
        CANCEL_TBT_TTS = new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.audio.CancelSpeechRequest(ROUTE_TBT_TTS_ID));
        resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        int i = resources.getColor(R.color.glance_dismiss);
        int i0 = resources.getColor(R.color.glance_ok_blue);
        int i1 = resources.getColor(R.color.glance_ok_blue);
        int i2 = resources.getColor(R.color.glance_ok_go);
        String s = resources.getString(R.string.more_routes);
        cancelRouteCalc = (java.util.List)new java.util.ArrayList(1);
        cancelRouteCalc.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(1, R.drawable.icon_glances_dismiss, i, R.drawable.icon_glances_dismiss, -16777216, resources.getString(R.string.cancel), i));
        showRoutes = (java.util.List)new java.util.ArrayList(2);
        showRoutes.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(2, R.drawable.icon_glances_read, i1, R.drawable.icon_glances_read, -16777216, s, i1));
        showRoutes.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(3, R.drawable.icon_glances_ok_strong, i2, R.drawable.icon_glances_ok_strong, -16777216, resources.getString(R.string.route_go), i2));
        routeError = (java.util.List)new java.util.ArrayList(2);
        routeError.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(4, R.drawable.icon_glances_retry, i0, R.drawable.icon_glances_retry, -16777216, resources.getString(R.string.retry), i0));
        routeError.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(3, R.drawable.icon_glances_dismiss, i, R.drawable.icon_glances_dismiss, -16777216, resources.getString(R.string.dismiss), i));
        routeFailed = resources.getString(R.string.route_failed);
        fastestRoute = resources.getString(R.string.fastest_route);
        shortestRoute = resources.getString(R.string.shortest_route);
        showRouteBkColor = resources.getColor(R.color.route_unsel);
        notifBkColor = resources.getColor(R.color.route_sel);
        startTripLeftMargin = resources.getDimensionPixelSize(R.dimen.start_trip_left_margin);
        startTripImageContainerSize = resources.getDimensionPixelSize(R.dimen.start_trip_image_container_size);
        startTripImageSize = resources.getDimensionPixelSize(R.dimen.start_trip_image_size);
        startTripTextLeftMargin = resources.getDimensionPixelSize(R.dimen.start_trip_text_left_margin);
    }
    
    public RouteCalculationNotification(com.navdy.hud.app.maps.notification.RouteCalculationEventHandler a, com.squareup.otto.Bus a0) {
        this.iconBkColor = 0;
        this.notifColor = 0;
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.waitForNavLookupRunnable = (Runnable)new com.navdy.hud.app.maps.notification.RouteCalculationNotification$1(this);
        this.timeoutRunnable = (Runnable)new com.navdy.hud.app.maps.notification.RouteCalculationNotification$2(this);
        this.listener = (com.navdy.hud.app.ui.component.ChoiceLayout2.IListener)new com.navdy.hud.app.maps.notification.RouteCalculationNotification$3(this);
        this.uiStateManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
        this.routeCalculationEventHandler = a;
        this.bus = a0;
    }
    
    static com.navdy.hud.app.framework.notifications.INotificationController access$000(com.navdy.hud.app.maps.notification.RouteCalculationNotification a) {
        return a.controller;
    }
    
    static com.navdy.service.library.events.places.DestinationSelectedRequest access$100(com.navdy.hud.app.maps.notification.RouteCalculationNotification a) {
        return a.lookupRequest;
    }
    
    static android.animation.ObjectAnimator access$1000(com.navdy.hud.app.maps.notification.RouteCalculationNotification a) {
        return a.loadingAnimator;
    }
    
    static com.navdy.service.library.events.places.DestinationSelectedRequest access$102(com.navdy.hud.app.maps.notification.RouteCalculationNotification a, com.navdy.service.library.events.places.DestinationSelectedRequest a0) {
        a.lookupRequest = a0;
        return a0;
    }
    
    static com.navdy.service.library.log.Logger access$200() {
        return sLogger;
    }
    
    static void access$300(com.navdy.hud.app.maps.notification.RouteCalculationNotification a) {
        a.launchOriginalDestination();
    }
    
    static void access$400(com.navdy.hud.app.maps.notification.RouteCalculationNotification a) {
        a.dismissNotification();
    }
    
    static boolean access$502(com.navdy.hud.app.maps.notification.RouteCalculationNotification a, boolean b) {
        a.dismissedbyUser = b;
        return b;
    }
    
    static String access$600(com.navdy.hud.app.maps.notification.RouteCalculationNotification a) {
        return a.routeId;
    }
    
    static com.squareup.otto.Bus access$700(com.navdy.hud.app.maps.notification.RouteCalculationNotification a) {
        return a.bus;
    }
    
    static void access$800(com.navdy.hud.app.maps.notification.RouteCalculationNotification a) {
        a.showRoutePicker();
    }
    
    static com.navdy.hud.app.maps.notification.RouteCalculationEventHandler access$900(com.navdy.hud.app.maps.notification.RouteCalculationNotification a) {
        return a.routeCalculationEventHandler;
    }
    
    private void dismissNotification() {
        this.routeCalculationEventHandler.clearCurrentRouteCalcEvent();
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification("navdy#route#calc#notif");
    }
    
    private void launchOriginalDestination() {
        if (this.lookupDestination == null) {
            sLogger.v("original destination not launched");
        } else {
            com.navdy.hud.app.framework.destinations.Destination a = com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().transformToInternalDestination(this.lookupDestination);
            com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().requestNavigation(a);
            this.lookupDestination = null;
            sLogger.v(new StringBuilder().append("original destination launched:").append(a).toString());
        }
    }
    
    private void playTts(String s) {
        sLogger.v(new StringBuilder().append("tts[").append(s).append("]").toString());
        if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getNavigationSessionPreference().spokenTurnByTurn) {
            this.bus.post(CANCEL_CALC_TTS);
            com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(s, com.navdy.service.library.events.audio.SpeechRequest.Category.SPEECH_TURN_BY_TURN, ROUTE_CALC_TTS_ID);
        }
    }
    
    private void setIcon() {
        if (this.iconBkColor == 0) {
            this.iconInitialsImageView.setImage(this.icon, this.initials, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.LARGE);
            this.iconInitialsImageView.setVisibility(View.VISIBLE);
            this.iconColorImageView.setVisibility(View.GONE);
        } else {
            this.iconColorImageView.setIcon(this.icon, this.iconBkColor, (android.graphics.Shader)null, 1.38f);
            this.iconColorImageView.setVisibility(View.VISIBLE);
            this.iconInitialsImageView.setVisibility(View.GONE);
        }
    }
    
    private void setUI() {
        boolean b = false;
        this.startTime = android.os.SystemClock.elapsedRealtime();
        this.titleView.setText((CharSequence)this.title);
        this.subTitleView.setText((CharSequence)this.subTitle);
        this.iconSpinnerView.setImageResource(this.iconSide);
        if (this.choices != cancelRouteCalc) {
            if (this.choices != showRoutes) {
                if (this.choices != routeError) {
                    b = false;
                } else {
                    this.routeTtaView.setVisibility(View.GONE);
                    this.setIcon();
                    this.stopLoadingAnimation();
                    b = false;
                }
            } else {
                this.setIcon();
                this.stopLoadingAnimation();
                this.routeTtaView.setText((CharSequence)this.tripDuration);
                this.routeTtaView.setVisibility(View.VISIBLE);
                b = true;
            }
        } else {
            this.routeTtaView.setVisibility(View.GONE);
            this.setIcon();
            this.startLoadingAnimation();
            if (this.tts != null && !this.ttsPlayed) {
                this.playTts(this.tts);
                this.tts = null;
                this.ttsPlayed = true;
            }
            if (this.lookupDestination == null) {
                b = false;
            } else {
                this.lookupRequest = new com.navdy.service.library.events.places.DestinationSelectedRequest.Builder().request_id(java.util.UUID.randomUUID().toString()).destination(this.lookupDestination).build();
                this.handler.removeCallbacks(this.waitForNavLookupRunnable);
                this.handler.postDelayed(this.waitForNavLookupRunnable, (long)NAV_LOOK_UP_TIMEOUT);
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)this.lookupRequest));
                sLogger.v(new StringBuilder().append("launched nav lookup request:").append(this.lookupRequest.request_id).append(" dest:").append(this.lookupDestination).append(" placeid:").append(this.lookupDestination.place_id).toString());
                b = false;
            }
        }
        this.choiceLayoutView.setChoices(this.choices, b ? 1 : 0, this.listener);
        this.controller.startTimeout(this.getTimeout());
    }
    
    private void showRoutePicker() {
        com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent a = this.event;
        label2: {
            double d = 0.0;
            double d0 = 0.0;
            double d1 = 0.0;
            double d2 = 0.0;
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (this.event.response == null) {
                        break label1;
                    }
                    if (this.event.request != null) {
                        break label0;
                    }
                }
                sLogger.v("showRoutePicker invalid state");
                this.dismissNotification();
                break label2;
            }
            android.os.Bundle a0 = new android.os.Bundle();
            a0.putString("PICKER_LEFT_TITLE", resources.getString(R.string.routes));
            a0.putInt("PICKER_LEFT_ICON", R.drawable.icon_route);
            a0.putInt("PICKER_LEFT_ICON_BKCOLOR", 0);
            a0.putString("PICKER_TITLE", resources.getString(R.string.pick_route));
            a0.putBoolean("PICKER_HIDE", true);
            a0.putBoolean("ROUTE_PICKER", true);
            a0.putBoolean("PICKER_SHOW_ROUTE_MAP", true);
            com.here.android.mpa.common.GeoCoordinate a1 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
            a0.putDouble("PICKER_MAP_START_LAT", a1.getLatitude());
            a0.putDouble("PICKER_MAP_START_LNG", a1.getLongitude());
            a0.putDouble("PICKER_MAP_END_LAT", this.event.request.destination.latitude.doubleValue());
            a0.putDouble("PICKER_MAP_END_LNG", this.event.request.destination.longitude.doubleValue());
            int i = this.event.response.results.size();
            com.navdy.hud.app.ui.component.destination.DestinationParcelable[] a2 = new com.navdy.hud.app.ui.component.destination.DestinationParcelable[i];
            if (this.event.request.destination == null) {
                d = 0.0;
                d0 = 0.0;
            } else {
                d = this.event.request.destination.latitude.doubleValue();
                d0 = this.event.request.destination.longitude.doubleValue();
            }
            if (this.event.request.destinationDisplay == null) {
                d1 = 0.0;
                d2 = 0.0;
            } else {
                d1 = this.event.request.destinationDisplay.latitude.doubleValue();
                d2 = this.event.request.destinationDisplay.longitude.doubleValue();
            }
            StringBuilder a3 = new StringBuilder();
            com.navdy.hud.app.maps.here.HereRouteCache a4 = com.navdy.hud.app.maps.here.HereRouteCache.getInstance();
            com.navdy.hud.app.common.TimeHelper a5 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper();
            com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a6 = new com.navdy.hud.app.maps.MapEvents$ManeuverDisplay();
            com.navdy.service.library.events.destination.Destination a7 = this.event.request.requestDestination;
            int i0 = 0;
            while(i0 < i) {
                String s = null;
                String s0 = null;
                com.navdy.service.library.events.navigation.NavigationRouteResult a8 = (com.navdy.service.library.events.navigation.NavigationRouteResult)this.event.response.results.get(i0);
                com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.setNavigationDistance((long)a8.length.intValue(), a6, false, true);
                com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a9 = a4.getRoute(a8.routeId);
                if (a9 == null) {
                    s = "";
                    s0 = "";
                } else {
                    java.util.Date a10 = com.navdy.hud.app.maps.here.HereMapUtil.getRouteTtaDate(a9.route);
                    if (a10 == null) {
                        s = "";
                        s0 = "";
                    } else {
                        String s1 = a5.formatTime12Hour(a10, a3, false);
                        s0 = new StringBuilder().append(s1).append(" ").append(a3.toString()).toString();
                        long j = a10.getTime() - System.currentTimeMillis();
                        long j0 = (j >= 0L) ? java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(j) : 0L;
                        s = com.navdy.hud.app.maps.util.RouteUtils.formatEtaMinutes(resources, (int)j0);
                    }
                }
                android.content.res.Resources a11 = resources;
                Object[] a12 = new Object[2];
                a12[0] = s;
                a12[1] = a8.via;
                String s2 = a11.getString(R.string.route_title, a12);
                String s3 = new StringBuilder().append(a6.distanceToPendingRoadText).append(" ").append(s0).toString();
                String s4 = a8.address;
                int i1 = notifBkColor;
                com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType a13 = com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType.DESTINATION;
                com.navdy.service.library.events.places.PlaceType a14 = (a7 == null) ? null : a7.place_type;
                a2[i0] = new com.navdy.hud.app.ui.component.destination.DestinationParcelable(0, s2, s3, false, (String)null, false, s4, d, d0, d1, d2, R.drawable.icon_route, R.drawable.icon_route_sm, i1, 0, a13, a14);
                a2[i0].setRouteId(((com.navdy.service.library.events.navigation.NavigationRouteResult)this.event.response.results.get(i0)).routeId);
                if (a7 != null) {
                    a2[i0].setIdentifier(a7.identifier);
                    a2[i0].setPlaceId(a7.place_id);
                }
                a3.setLength(0);
                i0 = i0 + 1;
            }
            a0.putParcelableArray("PICKER_DESTINATIONS", (android.os.Parcelable[])a2);
            com.navdy.hud.app.maps.notification.RouteCalcPickerHandler a15 = new com.navdy.hud.app.maps.notification.RouteCalcPickerHandler(this.event);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification("navdy#route#calc#notif", true, com.navdy.service.library.events.ui.Screen.SCREEN_DESTINATION_PICKER, a0, a15);
            this.event = null;
        }
    }
    
    private void startLoadingAnimation() {
        if (this.loadingAnimator == null) {
            android.widget.ImageView a = this.iconSpinnerView;
            android.util.Property a0 = android.view.View.ROTATION;
            float[] a1 = new float[1];
            a1[0] = 360f;
            this.loadingAnimator = android.animation.ObjectAnimator.ofFloat(a, a0, a1);
            this.loadingAnimator.setDuration(500L);
            this.loadingAnimator.setInterpolator((android.animation.TimeInterpolator)new android.view.animation.AccelerateDecelerateInterpolator());
            this.loadingAnimator.addListener((android.animation.Animator.AnimatorListener)new com.navdy.hud.app.maps.notification.RouteCalculationNotification$4(this));
        }
        if (!this.loadingAnimator.isRunning()) {
            sLogger.v("started loading animation");
            this.loadingAnimator.start();
        }
    }
    
    private void stopLoadingAnimation() {
        if (this.loadingAnimator != null) {
            sLogger.v("cancelled loading animation");
            this.loadingAnimator.removeAllListeners();
            this.loadingAnimator.cancel();
            this.iconSpinnerView.setRotation(0.0f);
            this.loadingAnimator = null;
        }
    }
    
    public boolean canAddToStackIfCurrentExists() {
        return true;
    }
    
    public boolean expandNotification() {
        return false;
    }
    
    public int getColor() {
        return notifBkColor;
    }
    
    public android.view.View getExpandedView(android.content.Context a, Object a0) {
        return null;
    }
    
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    public String getId() {
        return "navdy#route#calc#notif";
    }
    
    public int getTimeout() {
        return (this.choices != cancelRouteCalc) ? (this.choices != routeError) ? (this.choices != showRoutes) ? 0 : TIMEOUT : TIMEOUT : 0;
    }
    
    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.ROUTE_CALC;
    }
    
    public android.view.View getView(android.content.Context a) {
        if (this.containerView == null) {
            this.containerView = (android.view.ViewGroup)android.view.LayoutInflater.from(a).inflate(R.layout.notification_route_calc, (android.view.ViewGroup)null);
            this.titleView = (android.widget.TextView)this.containerView.findViewById(R.id.title);
            this.subTitleView = (android.widget.TextView)this.containerView.findViewById(R.id.subTitle);
            this.iconColorImageView = (com.navdy.hud.app.ui.component.image.IconColorImageView)this.containerView.findViewById(R.id.iconColorView);
            this.iconInitialsImageView = (com.navdy.hud.app.ui.component.image.InitialsImageView)this.containerView.findViewById(R.id.iconInitialsView);
            this.iconSpinnerView = (android.widget.ImageView)this.containerView.findViewById(R.id.iconSpinner);
            this.choiceLayoutView = (com.navdy.hud.app.ui.component.ChoiceLayout2)this.containerView.findViewById(R.id.choiceLayout);
            this.routeTtaView = (android.widget.TextView)this.containerView.findViewById(R.id.routeTtaView);
        }
        return this.containerView;
    }
    
    public android.animation.AnimatorSet getViewSwitchAnimation(boolean b) {
        return null;
    }
    
    public void hideStartTrip() {
        com.navdy.hud.app.ui.activity.Main a = this.uiStateManager.getRootScreen();
        if (a != null) {
            a.ejectMainLowerView();
        }
    }
    
    public boolean isAlive() {
        String s = null;
        boolean b = false;
        if (this.choices != cancelRouteCalc) {
            if (this.choices != routeError) {
                java.util.List a = this.choices;
                java.util.List a0 = showRoutes;
                s = null;
                if (a != a0) {
                    b = false;
                } else {
                    if (this.dismissedbyUser) {
                        b = false;
                    } else {
                        long j = android.os.SystemClock.elapsedRealtime() - this.startTime;
                        boolean b0 = j >= (long)TIMEOUT;
                        com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent a1 = this.routeCalculationEventHandler.getCurrentRouteCalcEvent();
                        label2: {
                            label0: {
                                label1: {
                                    if (a1 == null) {
                                        break label1;
                                    }
                                    if (!b0) {
                                        break label0;
                                    }
                                }
                                b = false;
                                break label2;
                            }
                            b = true;
                        }
                        sLogger.v(new StringBuilder().append("timedOut=").append(j).append(" = ").append(b0).toString());
                    }
                    s = "showRoutes";
                }
            } else {
                b = false;
                s = "routeError";
            }
        } else {
            b = !this.dismissedbyUser && this.routeCalculationEventHandler.getCurrentRouteCalcEvent() != null;
            s = "cancelRouteCalc";
        }
        sLogger.v(new StringBuilder().append("alive=").append(b).append(" type=").append(s).toString());
        return b;
    }
    
    public boolean isPurgeable() {
        return !this.isAlive();
    }
    
    public boolean isStarting() {
        return this.choices == cancelRouteCalc;
    }
    
    public com.navdy.hud.app.manager.InputManager.IInputHandler nextHandler() {
        return null;
    }
    
    public void onClick() {
    }
    
    @Subscribe
    public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        if (this.controller != null && com.navdy.hud.app.maps.notification.RouteCalculationNotification$5.$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[a.state.ordinal()] != 0 && this.lookupRequest != null) {
            sLogger.v("nav lookup device disconnected");
            this.lookupRequest = null;
            this.launchOriginalDestination();
        }
    }
    
    @Subscribe
    public void onDestinationLookup(com.navdy.service.library.events.places.DestinationSelectedResponse a) {
        if (this.controller != null) {
            if (this.lookupRequest != null) {
                if (android.text.TextUtils.equals((CharSequence)a.request_id, (CharSequence)this.lookupRequest.request_id)) {
                    com.navdy.service.library.events.RequestStatus a0 = a.request_status;
                    label1: {
                        label0: {
                            if (a0 == null) {
                                break label0;
                            }
                            if (a.request_status != com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
                                break label0;
                            }
                            sLogger.v(new StringBuilder().append("nav lookup suceeded:").append(a.destination).toString());
                            com.navdy.hud.app.framework.destinations.Destination a1 = com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().transformToInternalDestination(a.destination);
                            com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().requestNavigation(a1);
                            break label1;
                        }
                        sLogger.v(new StringBuilder().append("nav lookup failed:").append(a.request_status).toString());
                        this.launchOriginalDestination();
                    }
                    this.lookupRequest = null;
                } else {
                    sLogger.v(new StringBuilder().append("receive destination lookup, mismatch id recv[").append(a.request_id).append("] lookup[").append(this.lookupRequest.request_id).append("]").toString());
                }
            } else {
                sLogger.v(new StringBuilder().append("receive destination lookup:").append(a.request_id).append(" , not in lookup mode, ignore").toString());
            }
        }
    }
    
    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode a) {
    }
    
    public void onExpandedNotificationSwitched() {
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        boolean b = false;
        if (this.controller != null) {
            if (this.choices != showRoutes) {
                b = false;
            } else if (com.navdy.hud.app.maps.notification.RouteCalculationNotification$5.$SwitchMap$com$navdy$service$library$events$input$Gesture[a.gesture.ordinal()] != 0) {
                sLogger.v("show route picker:gesture");
                this.dismissedbyUser = true;
                this.showRoutePicker();
                b = true;
            } else {
                b = false;
            }
        } else {
            b = false;
        }
        return b;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent a) {
        boolean b = false;
        if (this.controller != null) {
            switch(com.navdy.hud.app.maps.notification.RouteCalculationNotification$5.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                case 3: {
                    this.choiceLayoutView.executeSelectedItem();
                    b = true;
                    break;
                }
                case 2: {
                    this.controller.resetTimeout();
                    this.choiceLayoutView.moveSelectionRight();
                    b = true;
                    break;
                }
                case 1: {
                    this.controller.resetTimeout();
                    this.choiceLayoutView.moveSelectionLeft();
                    b = true;
                    break;
                }
                default: {
                    b = false;
                }
            }
        } else {
            b = false;
        }
        return b;
    }
    
    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode a) {
    }
    
    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController a) {
        sLogger.v("onStart");
        this.controller = a;
        this.handler.removeCallbacks(this.timeoutRunnable);
        if (!this.registered) {
            this.bus.register(this);
            this.registered = true;
        }
        this.setUI();
    }
    
    public void onStop() {
        sLogger.v("onStop");
        this.hideStartTrip();
        this.stopLoadingAnimation();
        this.handler.removeCallbacks(this.waitForNavLookupRunnable);
        this.controller = null;
        this.ttsPlayed = false;
        if (this.choices == cancelRouteCalc) {
            sLogger.v("cancel tts");
            this.bus.post(CANCEL_CALC_TTS);
        }
        java.util.List a = this.choices;
        java.util.List a0 = showRoutes;
        label0: {
            label1: {
                if (a == a0) {
                    break label1;
                }
                if (this.choices != routeError) {
                    break label0;
                }
            }
            if (this.isAlive()) {
                this.handler.removeCallbacks(this.timeoutRunnable);
                this.handler.postDelayed(this.timeoutRunnable, (long)TIMEOUT);
                sLogger.v(new StringBuilder().append("expire notif in ").append(TIMEOUT).toString());
            }
        }
        if (this.registered) {
            this.registered = false;
            this.bus.unregister(this);
        }
    }
    
    public void onTrackHand(float f) {
    }
    
    public void onUpdate() {
    }
    
    public void showError() {
        this.title = routeFailed;
        this.choices = routeError;
        this.iconSide = R.drawable.icon_badge_alert;
        this.event = null;
        this.tts = null;
        if (this.controller != null) {
            this.setUI();
        }
    }
    
    public void showRouteSearch(String s, String s0, int i, int i0, int i1, String s1, String s2, com.navdy.service.library.events.destination.Destination a, String s3, String s4, String s5, String s6) {
        boolean b = false;
        sLogger.v(new StringBuilder().append("showRouteSearch: navlookup:").append(a != null).toString());
        if (this.lookupDestination == null) {
            b = false;
        } else if (android.text.TextUtils.equals((CharSequence)this.title, (CharSequence)s)) {
            sLogger.v("showRouteSearch: nav lookup transition");
            b = true;
        } else {
            b = false;
        }
        this.title = s;
        this.subTitle = s0;
        this.routeId = s3;
        if (!b) {
            this.icon = i;
            this.iconBkColor = i0;
            this.notifColor = i1;
            this.initials = s1;
        }
        this.choices = cancelRouteCalc;
        this.iconSide = R.drawable.loader_circle;
        this.event = null;
        this.tts = s2;
        this.lookupDestination = a;
        this.destinationLabel = s4;
        this.destinationAddress = s5;
        this.destinationDistance = s6;
        if (this.controller != null) {
            this.setUI();
        }
    }
    
    public void showRoutes(com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent a) {
        switch(com.navdy.hud.app.maps.notification.RouteCalculationNotification$5.$SwitchMap$com$here$android$mpa$routing$RouteOptions$Type[a.routeOptions.getRouteType().ordinal()]) {
            case 2: {
                this.title = shortestRoute;
                break;
            }
            case 1: {
                this.title = fastestRoute;
                break;
            }
        }
        android.content.res.Resources a0 = resources;
        Object[] a1 = new Object[1];
        a1[0] = ((com.navdy.service.library.events.navigation.NavigationRouteResult)a.response.results.get(0)).via;
        this.subTitle = a0.getString(R.string.via_desc, a1);
        this.choices = showRoutes;
        this.icon = 0;
        this.iconBkColor = showRouteBkColor;
        this.iconSide = R.drawable.icon_route;
        this.event = a;
        this.tripDuration = null;
        this.tts = null;
        com.navdy.service.library.events.navigation.NavigationRouteResult a2 = (com.navdy.service.library.events.navigation.NavigationRouteResult)a.response.results.get(0);
        com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a3 = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(a2.routeId);
        if (a3 == null) {
            this.tripDuration = new android.text.SpannableStringBuilder();
        } else {
            this.tripDuration = com.navdy.hud.app.maps.here.HereMapUtil.getEtaString(a3.route, a2.routeId, false);
        }
        if (this.controller != null) {
            this.setUI();
        }
    }
    
    public void showStartTrip() {
        if (this.controller != null) {
            com.navdy.hud.app.ui.activity.Main a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getRootScreen();
            if (a != null) {
                android.content.Context a0 = this.controller.getUIContext();
                android.view.ViewGroup a1 = (android.view.ViewGroup)android.view.LayoutInflater.from(a0).inflate(R.layout.vlist_item, (android.view.ViewGroup)null);
                com.navdy.hud.app.ui.component.HaloView a2 = (com.navdy.hud.app.ui.component.HaloView)a1.findViewById(R.id.halo);
                a2.setVisibility(View.GONE);
                android.view.ViewGroup.MarginLayoutParams a3 = (android.view.ViewGroup.MarginLayoutParams)((android.view.ViewGroup)a1.findViewById(R.id.imageContainer)).getLayoutParams();
                a3.width = startTripImageContainerSize;
                a3.height = startTripImageContainerSize;
                a3.leftMargin = startTripLeftMargin;
                android.view.ViewGroup a4 = (android.view.ViewGroup)a1.findViewById(R.id.iconContainer);
                android.view.ViewGroup.MarginLayoutParams a5 = (android.view.ViewGroup.MarginLayoutParams)a4.getLayoutParams();
                a5.width = startTripImageSize;
                a5.height = startTripImageSize;
                ((android.view.ViewGroup.MarginLayoutParams)((android.view.ViewGroup)a1.findViewById(R.id.textContainer)).getLayoutParams()).leftMargin = startTripTextLeftMargin;
                android.widget.TextView a6 = (android.widget.TextView)a1.findViewById(R.id.title);
                android.widget.TextView a7 = (android.widget.TextView)a1.findViewById(R.id.subTitle);
                android.widget.TextView a8 = (android.widget.TextView)a1.findViewById(R.id.subTitle2);
                com.navdy.hud.app.ui.component.vlist.VerticalList.Model a9 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(0, 0, 0, 0, 0, this.destinationLabel, this.destinationAddress, this.destinationDistance, (com.navdy.hud.app.ui.component.image.IconColorImageView.IconShape)null);
                com.navdy.hud.app.ui.component.vlist.VerticalList.setFontSize(a9, false);
                ((android.view.ViewGroup.MarginLayoutParams)a6.getLayoutParams()).topMargin = (int)a9.fontInfo.titleFontTopMargin;
                ((android.view.ViewGroup.MarginLayoutParams)a7.getLayoutParams()).topMargin = (int)a9.fontInfo.subTitleFontTopMargin;
                ((android.view.ViewGroup.MarginLayoutParams)a8.getLayoutParams()).topMargin = (int)a9.fontInfo.subTitle2FontTopMargin;
                a6.setTextSize(a9.fontInfo.titleFontSize);
                a6.setSingleLine(true);
                a6.setText((CharSequence)this.destinationLabel);
                if (android.text.TextUtils.isEmpty((CharSequence)this.destinationAddress)) {
                    a7.setVisibility(View.GONE);
                } else {
                    a7.setTextSize(a9.fontInfo.subTitleFontSize);
                    a7.setText((CharSequence)this.destinationAddress);
                }
                if (android.text.TextUtils.isEmpty((CharSequence)this.destinationDistance)) {
                    a8.setVisibility(View.GONE);
                } else {
                    a8.setTextSize(a9.fontInfo.subTitle2FontSize);
                    a8.setText((CharSequence)this.destinationDistance);
                    a8.setVisibility(View.VISIBLE);
                }
                if (this.iconBkColor == 0) {
                    com.navdy.hud.app.ui.component.image.InitialsImageView a10 = new com.navdy.hud.app.ui.component.image.InitialsImageView(a0);
                    a4.addView((android.view.View)a10);
                    a2.setStrokeColor(android.graphics.Color.argb(153, android.graphics.Color.red(this.notifColor), android.graphics.Color.green(this.notifColor), android.graphics.Color.blue(this.notifColor)));
                    a10.setImage(this.icon, this.initials, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.MEDIUM);
                } else {
                    com.navdy.hud.app.ui.component.image.IconColorImageView a11 = new com.navdy.hud.app.ui.component.image.IconColorImageView(a0);
                    a4.addView((android.view.View)a11);
                    a2.setStrokeColor(android.graphics.Color.argb(153, android.graphics.Color.red(this.iconBkColor), android.graphics.Color.green(this.iconBkColor), android.graphics.Color.blue(this.iconBkColor)));
                    a11.setIcon(this.icon, this.iconBkColor, (android.graphics.Shader)null, 0.83f);
                }
                a.injectMainLowerView((android.view.View)a1);
            }
        } else {
            sLogger.v("showStartTrip:null");
        }
    }
    
    public boolean supportScroll() {
        return false;
    }
}
