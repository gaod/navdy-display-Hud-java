package com.navdy.hud.app.maps.notification;
import com.navdy.hud.app.R;

public class TrafficEventNotification extends com.navdy.hud.app.maps.notification.BaseTrafficNotification {
    final private static int TAG_DISMISS = 1;
    private static String ahead;
    private static String dismiss;
    private static java.util.List dismissChoices;
    private static String enableInternet;
    private static String noUpdatedTraffic;
    private static String slowTraffic;
    private static String stoppedTraffic;
    private static String trafficIncident;
    private static String unknownIncident;
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.ui.component.ChoiceLayout2.IListener choiceListener;
    private android.view.ViewGroup container;
    private android.widget.ImageView image;
    private int notifColor;
    private android.widget.TextView subTitle;
    private android.widget.TextView title;
    private com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent trafficEvent;
    
    static {
        dismissChoices = (java.util.List)new java.util.ArrayList(1);
    }
    
    public TrafficEventNotification(com.squareup.otto.Bus a) {
        this.choiceListener = (com.navdy.hud.app.ui.component.ChoiceLayout2.IListener)new com.navdy.hud.app.maps.notification.TrafficEventNotification$1(this);
        if (stoppedTraffic == null) {
            android.content.res.Resources a0 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
            stoppedTraffic = a0.getString(R.string.traffic_notification_text_stoppedtraffic);
            slowTraffic = a0.getString(R.string.traffic_notification_text_slowtraffic);
            trafficIncident = a0.getString(R.string.traffic_notification_text_incident);
            noUpdatedTraffic = a0.getString(R.string.traffic_no_updated);
            enableInternet = a0.getString(R.string.enable_internet);
            ahead = a0.getString(R.string.traffic_notification_ahead);
            dismiss = a0.getString(R.string.traffic_notification_dismiss);
            unknownIncident = a0.getString(R.string.traffic_notification_text_default);
            int i = a0.getColor(R.color.glance_dismiss);
            dismissChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(1, R.drawable.icon_glances_dismiss, i, R.drawable.icon_glances_dismiss, -16777216, dismiss, i));
            this.notifColor = a0.getColor(R.color.traffic_bad);
        }
        this.bus = a;
    }
    
    static com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent access$002(com.navdy.hud.app.maps.notification.TrafficEventNotification a, com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent a0) {
        a.trafficEvent = a0;
        return a0;
    }
    
    private int getLiveTrafficEventImage(com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent a) {
        int i = 0;
        com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type a0 = a.type;
        com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type a1 = com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type.CONGESTION;
        label1: {
            label0: {
                if (a0 != a1) {
                    break label0;
                }
                if (a.severity.value <= com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Severity.VERY_HIGH.value) {
                    break label0;
                }
                i = R.drawable.icon_mm_stopped_traffic;
                break label1;
            }
            i = (a.type != com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type.INCIDENT) ? R.drawable.icon_mm_slow_traffic : R.drawable.icon_mm_incident;
        }
        return i;
    }
    
    private String getLiveTrafficEventText(com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent a) {
        return (a.type != com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type.CONGESTION) ? (a.type != com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type.INCIDENT) ? unknownIncident : trafficIncident : (a.severity.value <= com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Severity.HIGH.value) ? slowTraffic : stoppedTraffic;
    }
    
    private void updateState() {
        if (this.trafficEvent != null) {
            this.title.setText((CharSequence)this.getLiveTrafficEventText(this.trafficEvent));
            this.subTitle.setText((CharSequence)ahead);
            this.image.setImageResource(this.getLiveTrafficEventImage(this.trafficEvent));
            this.choiceLayout.setChoices(dismissChoices, 0, this.choiceListener, 0.5f);
        }
    }
    
    public boolean canAddToStackIfCurrentExists() {
        return true;
    }
    
    public boolean expandNotification() {
        return false;
    }
    
    public int getColor() {
        return this.notifColor;
    }
    
    public android.view.View getExpandedView(android.content.Context a, Object a0) {
        return null;
    }
    
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    public String getId() {
        return "navdy#traffic#event#notif";
    }
    
    public int getTimeout() {
        return 0;
    }
    
    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.TRAFFIC_EVENT;
    }
    
    public android.view.View getView(android.content.Context a) {
        if (this.container == null) {
            this.container = (android.view.ViewGroup)android.view.LayoutInflater.from(a).inflate(R.layout.notification_traffic_event, (android.view.ViewGroup)null);
            this.title = (android.widget.TextView)this.container.findViewById(R.id.title);
            this.subTitle = (android.widget.TextView)this.container.findViewById(R.id.subTitle);
            this.image = (android.widget.ImageView)this.container.findViewById(R.id.image);
            this.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2)this.container.findViewById(R.id.choiceLayout);
        }
        return this.container;
    }
    
    public android.animation.AnimatorSet getViewSwitchAnimation(boolean b) {
        return null;
    }
    
    public boolean isAlive() {
        return true;
    }
    
    public boolean isPurgeable() {
        return false;
    }
    
    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode a) {
    }
    
    public void onExpandedNotificationSwitched() {
    }
    
    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode a) {
    }
    
    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController a) {
        super.onStart(a);
        this.updateState();
    }
    
    public void onStop() {
        super.onStop();
    }
    
    public void onUpdate() {
        this.updateState();
    }
    
    public void setTrafficEvent(com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent a) {
        this.trafficEvent = a;
    }
}
