package com.navdy.hud.app.maps;

public enum MapEvents$DestinationDirection {
    LEFT(0),
    RIGHT(1),
    UNKNOWN(2);

    private int value;
    MapEvents$DestinationDirection(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class MapEvents$DestinationDirection extends Enum {
//    final private static com.navdy.hud.app.maps.MapEvents$DestinationDirection[] $VALUES;
//    final public static com.navdy.hud.app.maps.MapEvents$DestinationDirection LEFT;
//    final public static com.navdy.hud.app.maps.MapEvents$DestinationDirection RIGHT;
//    final public static com.navdy.hud.app.maps.MapEvents$DestinationDirection UNKNOWN;
//
//    static {
//        LEFT = new com.navdy.hud.app.maps.MapEvents$DestinationDirection("LEFT", 0);
//        RIGHT = new com.navdy.hud.app.maps.MapEvents$DestinationDirection("RIGHT", 1);
//        UNKNOWN = new com.navdy.hud.app.maps.MapEvents$DestinationDirection("UNKNOWN", 2);
//        com.navdy.hud.app.maps.MapEvents$DestinationDirection[] a = new com.navdy.hud.app.maps.MapEvents$DestinationDirection[3];
//        a[0] = LEFT;
//        a[1] = RIGHT;
//        a[2] = UNKNOWN;
//        $VALUES = a;
//    }
//
//    private MapEvents$DestinationDirection(String s, int i) {
//        super(s, i);
//    }
//
//    public static com.navdy.hud.app.maps.MapEvents$DestinationDirection valueOf(String s) {
//        return (com.navdy.hud.app.maps.MapEvents$DestinationDirection)Enum.valueOf(com.navdy.hud.app.maps.MapEvents$DestinationDirection.class, s);
//    }
//
//    public static com.navdy.hud.app.maps.MapEvents$DestinationDirection[] values() {
//        return $VALUES.clone();
//    }
//}
