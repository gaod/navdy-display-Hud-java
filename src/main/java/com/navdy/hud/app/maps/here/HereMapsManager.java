package com.navdy.hud.app.maps.here;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;

import com.here.android.mpa.common.DiskCacheUtility;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.LocationDataSource;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.common.Version;
import com.here.android.mpa.mapping.Map;
import java.io.File;
import javax.inject.Inject;

import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager$ToastParams;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.maps.MapEvents$MapEngineInitialize;
import com.navdy.hud.app.maps.MapSettings;
import com.navdy.hud.app.maps.here.CustomScheme.HereMapsScheme;
import com.navdy.hud.app.maps.notification.RouteCalculationEventHandler;
import com.navdy.hud.app.maps.notification.TrafficNotificationManager;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Subscribe;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Random;

import com.here.android.mpa.common.ApplicationContext;

import org.apache.commons.codec.binary.StringUtils;

@SuppressWarnings("WeakerAccess")
public class HereMapsManager {
    final public static double DEFAULT_LATITUDE = 37.802086;
    final public static double DEFAULT_LONGITUDE = -122.419015;
    final public static float DEFAULT_TILT = 60f;
    final public static float DEFAULT_ZOOM_LEVEL = 16.5f;
    final private static String DISABLE_MAPS_PROPERTY = "persist.sys.map.disable";
    final private static String MAPS_SCHEME_TRACKING_PROPERTY = "persist.sys.map.scheme.tracking";
    final private static String MAPS_SCHEME_ENROUTE_PROPERTY = "persist.sys.map.scheme.enroute";
    final private static String MAPS_UPDATE_PROPERTY = "persist.sys.map.update";
    final private static String TRAFFIC_REROUTE_PROPERTY = "persist.sys.hud_traffic_reroute";
    final private static String EXTRUDED_BUILDINGS_PROPERTY = "persist.sys.extruded_buildings";
    final private static String LANDMARKS_VISIBLE_PROPERTY = "persist.sys.landmarks_visible";

    final private static String FIXED_HERE_APPID = "persist.sys.here.appid";
    final private static String FIXED_HERE_APPCODE = "persist.sys.here.appcode";
    final private static String FIXED_HERE_LICENCE = "persist.sys.here.licence";

    final private static boolean ENABLE_MAPS;
    final private static boolean MAPS_UPDATE;
    final private static String ENROUTE_MAP_SCHEME = "hybrid.night";
    final private static int GPS_SPEED_LAST_LOCATION_THRESHOLD = 2000;
    final static int MIN_SAME_POSITION_UPDATE_THRESHOLD = 500;
    final private static String MW_CONFIG_EXCEPTION_MSG = "Invalid configuration file. Check MWConfig!";
    final private static String NAVDY_HERE_MAP_SERVICE_NAME = "com.navdy.HereMapService";
    final public static float ROUTE_CALC_START_ZOOM_LEVEL = 15.5f;
    private static String mapsSchemeEnroute;
    private static String mapsSchemeTracking;
    final private static String DEFAULT_MAP_SCHEME = Map.Scheme.CARNAV_TRAFFIC_NIGHT;
    final public static String CUSTOM_TRACKING_MAP_SCHEME = "navdy.trackingScheme";
    final public static String CUSTOM_ENROUTE_MAP_SCHEME = "navdy.enrouteScheme";
    private static boolean recalcCurrentRouteForTraffic;
    final protected static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.hud.app.maps.here.HereMapsManager singleton;
    final private android.os.HandlerThread bkLocationReceiverHandlerThread;
    @Inject
    com.squareup.otto.Bus bus;
    private WeakReference<Context> context;
    private com.here.android.mpa.common.OnEngineInitListener engineInitListener;
    protected volatile boolean extrapolationOn;
    protected android.os.Handler handler;
    protected com.navdy.hud.app.maps.here.HereLocationFixManager hereLocationFixManager;
    protected com.navdy.hud.app.maps.here.HereMapAnimator hereMapAnimator;
    private final Object initLock;
    private boolean initialMapRendering;
    protected com.here.android.mpa.common.GeoPosition lastGeoPosition;
    protected long lastGeoPositionTime;
    private android.content.SharedPreferences.OnSharedPreferenceChangeListener listener;
    private volatile boolean lowBandwidthDetected;
    @Inject
    com.navdy.hud.app.profile.DriverProfileManager mDriverProfileManager;
    protected com.here.android.mpa.mapping.Map map;
    protected com.navdy.hud.app.maps.here.HereMapController mapController;
    protected boolean mapDataVerified;
    private com.here.android.mpa.common.MapEngine mapEngine;
    private long mapEngineStartTime;
    private com.here.android.mpa.common.OnEngineInitListener.Error mapError;
    private boolean mapInitLoaderComplete;
    protected boolean mapUpdateInProgress;
    private boolean mapInitialized;
    private boolean mapInitializing;
    protected com.here.android.mpa.odml.MapLoader mapLoader;
    protected int mapPackageCount;
    private com.here.android.mpa.mapping.MapView mapView;
    private com.navdy.hud.app.ui.component.homescreen.NavigationView navigationView;
    protected com.navdy.hud.app.maps.here.HereOfflineMapsVersion offlineMapsVersion;
    private com.here.android.mpa.common.PositioningManager.OnPositionChangedListener positionChangedListener;
    protected com.here.android.mpa.common.PositioningManager positioningManager;
    private boolean positioningManagerInstalled;
    @Inject
    com.navdy.hud.app.device.PowerManager powerManager;
    private com.navdy.hud.app.maps.notification.RouteCalculationEventHandler routeCalcEventHandler;
    private com.here.android.mpa.common.GeoCoordinate routeStartPoint;
    @Inject
    android.content.SharedPreferences sharedPreferences;
    protected com.navdy.hud.app.manager.SpeedManager speedManager;
    private volatile boolean turnEngineOn;
    private boolean voiceSkinsLoaded;
    protected boolean mapsUpdateAvailable = false;
    public LostLocationDataSource dataSource;

    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereMapsManager.class);
        ENABLE_MAPS = !com.navdy.hud.app.util.os.SystemProperties.getBoolean(DISABLE_MAPS_PROPERTY, false);
        mapsSchemeTracking = com.navdy.hud.app.util.os.SystemProperties.get(MAPS_SCHEME_ENROUTE_PROPERTY, DEFAULT_MAP_SCHEME).replace('_', '.').toLowerCase();
        mapsSchemeEnroute = com.navdy.hud.app.util.os.SystemProperties.get(MAPS_SCHEME_TRACKING_PROPERTY, DEFAULT_MAP_SCHEME).replace('_', '.').toLowerCase();
        MAPS_UPDATE = com.navdy.hud.app.util.os.SystemProperties.getBoolean(MAPS_UPDATE_PROPERTY, false);
        recalcCurrentRouteForTraffic = false;
        singleton = new com.navdy.hud.app.maps.here.HereMapsManager();
    }

    private HereMapsManager() {
        this.initLock = new Object();
        this.initialMapRendering = true;
        this.lowBandwidthDetected = false;
        this.turnEngineOn = false;
        this.mapInitializing = true;
        this.mapUpdateInProgress = false;
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());

        this.positionChangedListener = new HereMapsManager$3(this);
        this.listener = new HereMapsManager$4(this);
        this.context = new WeakReference<>(com.navdy.hud.app.HudApplication.getAppContext());
        mortar.Mortar.inject(this.context.get(), this);
        this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
        this.bkLocationReceiverHandlerThread = new android.os.HandlerThread("here_bk_location");
        this.bkLocationReceiverHandlerThread.start();

        this.engineInitListener = new OnEngineInitListener() {
            public void onEngineInitializationCompleted(Error initError) {
                final long start = SystemClock.elapsedRealtime();
                sLogger.v("MAP-ENGINE-INIT took [" + (start - mapEngineStartTime) + "]");
                sLogger.v("MAP-ENGINE-INIT HERE-SDK version:" + Version.getSdkVersion());
                Error a2 = Error.NONE;
                if (initError != Error.NONE) {
                    String s = initError.toString();
                    AnalyticsSupport.recordKeyFailure("HereMaps", s);
                    sLogger.e("MAP-ENGINE-INIT engine NOT initialized:" + initError);
                    mapError = initError;
                    synchronized (initLock) {
                        mapInitializing = false;
                        HereMapsManager.this.bus.post(new MapEvents$MapEngineInitialize(mapInitialized));
                    }
        //            if (initError == Error.USAGE_EXPIRED) {
        //                return;
        //            }
        //            if (initError != Error.MISSING_APP_CREDENTIAL) {
        //                return;
        //            }
                    Main.handleLibraryInitializationError("Here Maps initialization failure: " + s);
        
                } else {

                    TaskManager.getInstance().execute(new Runnable() {
                        public void run() {
                            mapEngine.onResume();
                            sLogger.v(":initializing maps config");
//                            HereMapsConfigurator.getInstance().updateMapsConfig();
                            sLogger.v("creating geopos");
                            //        com.here.android.mpa.common.GeoPosition a = new com.here.android.mpa.common.GeoPosition(new com.here.android.mpa.common.GeoCoordinate(DEFAULT_LATITUDE, DEFAULT_LONGITUDE));
                            sLogger.v("created geopos");
        //                    if (!DeviceUtil.isUserBuild()) {
                                long j = SystemClock.elapsedRealtime();
                                HereLaneInfoBuilder.init();
                            sLogger.v("time to init HereLaneInfoBuilder [" + (SystemClock.elapsedRealtime() - j) + "]");
        //                    }
                            sLogger.v("engine initialized refcount:" + mapEngine.getResourceReferenceCount());
                            try {
                                map = new Map();
                            } catch (final Throwable a0) {

//                                if (a0.getMessage().contains("Invalid configuration file. Check MWConfig!")) {
//                                    sLogger.e("MWConfig is corrupted! Cleaning up and restarting HUD app.");
//                                    HereMapsConfigurator.getInstance().removeMWConfigFolder();
//                                }
//                                handler.post(new Runnable() {
//                                    public void run() {
//                                        throw new HereMapsManager$MWConfigCorruptException(a0);
//                                    }
//                                });
                            }
                            map.setProjectionMode(Map.Projection.GLOBE);
                            map.setTrafficInfoVisible(true);
                            mapController = new HereMapController(map, HereMapController$State.AR_MODE);
                            HereMapsManager.this.setMapAttributes(new GeoCoordinate(DEFAULT_LATITUDE, DEFAULT_LONGITUDE));
                            sLogger.v("setting default map attributes");
                            setMapTraffic();
                            setMapPoiLayer(map);
                            positioningManager = PositioningManager.getInstance();
                            dataSource = new LostLocationDataSource(HereMapsManager.this.context.get());
                            positioningManager.setDataSource(dataSource);
                            sLogger.v("MAP-ENGINE-INIT-2 took [" + (SystemClock.elapsedRealtime() - start) + "]");
                            final long start2 = SystemClock.elapsedRealtime();
                            handler.post(new Runnable() {
                                public void run() {
                                    positioningManager.start(PositioningManager.LocationMethod.GPS_NETWORK);
                                    sLogger.v("position manager started");
                                    sLogger.v("MAP-ENGINE-INIT-3 took [" + (SystemClock.elapsedRealtime() - start2) + "]");
                                    final long start = SystemClock.elapsedRealtime();
                                    final Runnable a = this;
                                    TaskManager.getInstance().execute(new Runnable() {

                                        public void run() {
                                            HereNavigationManager a = HereNavigationManager.getInstance();
                                            registerConnectivityReceiver();
                                            sLogger.v("map created");
                                            TrafficNotificationManager.getInstance();
                                            HereMapAnimator$AnimationMode a0 = MapSettings.isCustomAnimationEnabled() ? HereMapAnimator$AnimationMode.ANIMATION : MapSettings.isFullCustomAnimatonEnabled() ? HereMapAnimator$AnimationMode.ZOOM_TILT_ANIMATION : HereMapAnimator$AnimationMode.NONE;
                                            sLogger.v("HereMapAnimator mode is [" + a0 + "]");
                                            hereMapAnimator = new HereMapAnimator(a0, mapController, a.getNavController());
                                            if (!initialMapRendering) {
                                                sLogger.v("HereMapAnimator MapRendering initial disabled");
                                                hereMapAnimator.stopMapRendering();
                                            }
                                            HereMapCameraManager.getInstance().initialize(mapController, HereMapsManager.this.bus, hereMapAnimator);
                                            hereLocationFixManager = new HereLocationFixManager(HereMapsManager.this.bus);
                                            sLogger.v("MAP-ENGINE-INIT-4 took [" + (SystemClock.elapsedRealtime() - start) + "]");
                                            routeCalcEventHandler = new RouteCalculationEventHandler(HereMapsManager.this.bus);
                                            initMapLoader();
                                            offlineMapsVersion = new HereOfflineMapsVersion();
                                            synchronized(initLock) {
                                                mapInitializing = false;
                                                mapInitialized = true;
                                                HereMapsManager.this.bus.post(new MapEvents$MapEngineInitialize(mapInitialized));
                                                sLogger.v("MAP-ENGINE-INIT event sent");
                                                /*monexit(a1)*/
                                            }
                                            sLogger.v("setEngineOnlineState initial");
                                            setEngineOnlineState();
                                            if (!HereMapsManager.this.powerManager.inQuietMode()) {
                                                startTrafficUpdater();
                                            }
                                        }
                                    }, 1);
                                }
                            });
                        }
                    }, 1);
                }
            }
        };
        if (ENABLE_MAPS) {
            this.checkforNetworkProvider();
        }
        this.bus.register(this);
    }

    private void checkforNetworkProvider() {
        this.handler.post(new HereMapsManager$5(this));
    }
    
    private float getDefaultTiltLevel() {
        return DEFAULT_TILT;
    }
    
    private double getDefaultZoomLevel() {
        return DEFAULT_ZOOM_LEVEL;
    }
    
    public static com.navdy.hud.app.maps.here.HereMapsManager getInstance() {
        return singleton;
    }
    
    private void initMapLoader() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new HereMapsManager$2(this), 2);
    }

    protected void initialize() {
        sLogger.v(":initializing voice skins");
        com.navdy.hud.app.maps.here.VoiceSkinsConfigurator.updateVoiceSkins();
        sLogger.v(":initializing event handlers");
        com.navdy.hud.app.maps.MapsEventHandler.getInstance();
        if (com.navdy.hud.app.maps.MapSettings.isDebugHereLocation()) {
            com.navdy.hud.app.debug.DebugReceiver.setHereDebugLocation(true);
        }

        com.here.android.mpa.mapping.Map.setMaximumFps(com.navdy.hud.app.maps.MapSettings.getMapFps());
        com.here.android.mpa.mapping.Map.enableMaximumFpsLimit(true);
        sLogger.v("map-fps [" + Map.getMaximumFps() + "] enabled[" + Map.isMaximumFpsLimited() + "]");

        //        try {
//            java.lang.reflect.Field a = com.nokia.maps.MapSettings.class.getDeclaredField("g");
//            a.setAccessible(true);
//            sLogger.e(new StringBuilder().append("enable worker thread before is ").append(a.get(null)).toString());
//            a.set(null, com.nokia.maps.MapSettings.b.a);
//            sLogger.e(new StringBuilder().append("enable worker thread after is ").append(a.get(null)).toString());
//        } catch(Throwable a0) {
//            sLogger.e("enable worker thread", a0);
//        }
        this.sharedPreferences.registerOnSharedPreferenceChangeListener(this.listener);
        recalcCurrentRouteForTraffic = com.navdy.hud.app.util.os.SystemProperties.getBoolean(TRAFFIC_REROUTE_PROPERTY, false);
        sLogger.v("persist.sys.hud_traffic_reroute=" + recalcCurrentRouteForTraffic);

        String s = com.navdy.hud.app.storage.PathManager.getInstance().getMapsPartitionPath();
        boolean b = s.isEmpty();
        label0: {
            if (b) {
                break label0;
            }
            String cache_path = s + java.io.File.separator + ".here-maps";
            if (!(new File(cache_path)).exists()) {
                boolean ret = (new File(cache_path)).mkdirs();
                if (!ret) {
                    sLogger.v("Permissions issue, not setting maps path");
                    break label0;
                }
            }
            else if(!(new File(cache_path + java.io.File.separator + "diskcache-v5")).isDirectory()) {
                if(new File(cache_path + java.io.File.separator + "diskcache-v4").isDirectory()) {
                    sLogger.v("Migrating old cached maps database");
                    DiskCacheUtility.migrate(cache_path, cache_path);
                }
            }

            if (!com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(cache_path, NAVDY_HERE_MAP_SERVICE_NAME)) {
                sLogger.v("setIsolatedDiskCacheRootPath() failed");
            }
        }
        this.mapEngine = com.here.android.mpa.common.MapEngine.getInstance();
        sLogger.v("calling maps engine init");

        Context context = this.context.get();
        ApplicationContext appContext = new ApplicationContext(context);

        String here_maps_appid = com.navdy.hud.app.util.os.SystemProperties.get(FIXED_HERE_APPID, null);
        String here_maps_appcode = com.navdy.hud.app.util.os.SystemProperties.get(FIXED_HERE_APPCODE, null);
        String here_maps_licence = com.navdy.hud.app.util.os.SystemProperties.get(FIXED_HERE_LICENCE, null);

        if ( !TextUtils.isEmpty(here_maps_appid) && !TextUtils.isEmpty(here_maps_appcode) && !TextUtils.isEmpty(here_maps_licence)) {
            sLogger.v("Using fixed Here key from system properties");
            appContext.setAppIdCode(here_maps_appid, here_maps_appcode);
            appContext.setLicenseKey(here_maps_licence);
        } else {

            int sel = new Random().nextInt(3);
            switch(sel) {
                case 2:
                    sLogger.v("Using Here key 2");
                    appContext.setAppIdCode(
                            context.getResources().getString(R.string.here_maps_appid_2),
                            context.getResources().getString(R.string.here_maps_apptoken_2)
                    );
                    appContext.setLicenseKey(
                            context.getResources().getString(R.string.here_maps_license_key_2)
                    );
                    break;

                case 1:
                    sLogger.v("Using Here key 1");

                    appContext.setAppIdCode(
                            context.getResources().getString(R.string.here_maps_appid_1),
                            context.getResources().getString(R.string.here_maps_apptoken_1)
                    );
                    appContext.setLicenseKey(
                            context.getResources().getString(R.string.here_maps_license_key_1)
                    );
                    break;

                case 0:
                default:
                    sLogger.v("Using Here key 0");

                    appContext.setAppIdCode(
                            context.getResources().getString(R.string.here_maps_appid_0),
                            context.getResources().getString(R.string.here_maps_apptoken_0)
                    );
                    appContext.setLicenseKey(
                            context.getResources().getString(R.string.here_maps_license_key_0)
                    );
                    break;
            }
        }

        this.mapEngine.init(appContext, this.engineInitListener);
    }

    protected void invokeMapLoader() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                HereMapsManager.this._invokeMapLoader();
            }
        }, 5);
    }

    private void _invokeMapLoader() {
        if (this.mapLoader != null) {
            if (com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext())) {
                if (this.mapInitLoaderComplete) {
                    sLogger.v("invokeMapLoader: already complete");
                } else {
                    this.mapInitLoaderComplete = true;
                    boolean b = this.mapLoader.getMapPackages();
                    sLogger.v("initMapLoader called getMapPackages:" + b);
                }
            } else {
                sLogger.v("invokeMapLoader: not connected to n/w");
            }
        } else {
            sLogger.v("invokeMapLoader: no maploader");
        }
    }
    
    private boolean isTrafficOverlayEnabled(com.navdy.hud.app.maps.NavigationMode a) {
        com.here.android.mpa.mapping.MapTrafficLayer a0 = this.map.getMapTrafficLayer();
        return (a != com.navdy.hud.app.maps.NavigationMode.MAP) ? a == com.navdy.hud.app.maps.NavigationMode.MAP_ON_ROUTE && a0.isEnabled(com.here.android.mpa.mapping.MapTrafficLayer.RenderLayer.ONROUTE) : a0.isEnabled(com.here.android.mpa.mapping.MapTrafficLayer.RenderLayer.FLOW) && a0.isEnabled(com.here.android.mpa.mapping.MapTrafficLayer.RenderLayer.INCIDENT);
    }
    
    private boolean isTrafficOverlayVisible() {
        return this.isTrafficOverlayEnabled(com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentNavigationMode());
    }

    public static boolean isMapsUpdateAvailable() {
        return MAPS_UPDATE && singleton.mapsUpdateAvailable;
    }
    
    protected void printMapPackages(com.here.android.mpa.odml.MapPackage mapPackage, java.util.EnumSet a0) {
        if (mapPackage != null) {
            if (a0.contains(mapPackage.getInstallationState())) {
                this.mapPackageCount = this.mapPackageCount + 1;
                this.offlineMapsVersion.addPackage(mapPackage.getTitle());
            }
            java.util.List a1 = mapPackage.getChildren();
            if (a1 != null) {
                Object a2 = a1.iterator();
                while(((java.util.Iterator)a2).hasNext()) {
                    this.printMapPackages((com.here.android.mpa.odml.MapPackage)((java.util.Iterator)a2).next(), a0);
                }
            }
        }
    }
    
    private void registerConnectivityReceiver() {
        android.content.IntentFilter a = new android.content.IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        com.navdy.hud.app.maps.here.HereMapsManager$11 a0 = new com.navdy.hud.app.maps.here.HereMapsManager$11(this);
        this.context.get().registerReceiver(a0, a);
    }

    protected void sendExtrapolationEvent() {
        android.os.Bundle a = new android.os.Bundle();
        a.putBoolean("EXTRAPOLATION_FLAG", this.extrapolationOn);
        com.navdy.hud.app.device.gps.GpsUtils.sendEventBroadcast("EXTRAPOLATION", a);
    }
    
    protected void setEngineOnlineState() {
        boolean b = com.navdy.hud.app.framework.network.NetworkStateManager.isConnectedToNetwork(this.context.get());
        this.lowBandwidthDetected = false;
        sLogger.i("setEngineOnlineState:setting maps engine online state:" + b);
        if (b) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new HereMapsManager$6(this), 1);
        } else {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new HereMapsManager$7(this), 1);
        }
    }

    private void setMapAttributes(com.here.android.mpa.common.GeoCoordinate a) {
        this.map.setExtrudedBuildingsVisible(com.navdy.hud.app.util.os.SystemProperties.getBoolean(EXTRUDED_BUILDINGS_PROPERTY, false));
        this.map.setLandmarksVisible(com.navdy.hud.app.util.os.SystemProperties.getBoolean(LANDMARKS_VISIBLE_PROPERTY, false));
        try {
            HereMapsScheme customScheme = new HereMapsScheme(map);
            if (customScheme.TrackingSchemeName != null) {
                mapsSchemeEnroute = customScheme.TrackingSchemeName;
                mapsSchemeTracking = customScheme.EnrouteSchemeName;
                this.map.setMapScheme(mapsSchemeTracking);
            } else {
                this.map.setMapScheme(this.getTrackingMapScheme());
            }
        } catch(Exception e) {
            sLogger.e("Failed to customise scheme ", e);
            this.map.setMapScheme(this.getTrackingMapScheme());
        }
        sLogger.v("map scheme set to " + this.map.getMapScheme());
        if (a == null) {
            sLogger.v("geoCoordinate not available");
        } else {
            this.map.setCenter(a, com.here.android.mpa.mapping.Map.Animation.NONE, this.getDefaultZoomLevel(), -1f, this.getDefaultTiltLevel());
            sLogger.v("setcenterDefault:" + a);
        }
    }
    
    private void setMapOnRouteTraffic() {
        this.mapController.execute(new HereMapsManager$9(this));
    }
    
    private void setMapPoiLayer(com.here.android.mpa.mapping.Map a) {
        a.setCartoMarkersVisible(false);
        a.getMapTransitLayer().setMode(com.here.android.mpa.mapping.MapTransitLayer.Mode.NOTHING);
        com.here.android.mpa.mapping.Map.LayerCategory a0 = com.here.android.mpa.mapping.Map.LayerCategory.ICON_PUBLIC_TRANSIT_STATION;
        com.here.android.mpa.mapping.Map.LayerCategory[] a1 = new com.here.android.mpa.mapping.Map.LayerCategory[15];
        a1[0] = com.here.android.mpa.mapping.Map.LayerCategory.PUBLIC_TRANSIT_LINE;
        a1[1] = com.here.android.mpa.mapping.Map.LayerCategory.LABEL_PUBLIC_TRANSIT_STATION;
        a1[2] = com.here.android.mpa.mapping.Map.LayerCategory.LABEL_PUBLIC_TRANSIT_LINE;
        a1[3] = com.here.android.mpa.mapping.Map.LayerCategory.BEACH;
        a1[4] = com.here.android.mpa.mapping.Map.LayerCategory.WOODLAND;
        a1[5] = com.here.android.mpa.mapping.Map.LayerCategory.DESERT;
        a1[6] = com.here.android.mpa.mapping.Map.LayerCategory.GLACIER;
        a1[7] = com.here.android.mpa.mapping.Map.LayerCategory.AMUSEMENT_PARK;
        a1[8] = com.here.android.mpa.mapping.Map.LayerCategory.ANIMAL_PARK;
        a1[9] = com.here.android.mpa.mapping.Map.LayerCategory.BUILTUP;
        a1[10] = com.here.android.mpa.mapping.Map.LayerCategory.CEMETERY;
        a1[11] = com.here.android.mpa.mapping.Map.LayerCategory.BUILDING;
        a1[12] = com.here.android.mpa.mapping.Map.LayerCategory.NEIGHBORHOOD_AREA;
        a1[13] = com.here.android.mpa.mapping.Map.LayerCategory.NATIONAL_PARK;
        a1[14] = com.here.android.mpa.mapping.Map.LayerCategory.NATIVE_RESERVATION;
        a.setVisibleLayers(java.util.EnumSet.of(a0, a1), false);
        java.util.EnumSet a2 = a.getVisibleLayers();
        sLogger.v("==== visible layers == [" + a2.size() + "]");
        StringBuilder a3 = new StringBuilder();
        Object a4 = a2.iterator();
        boolean b = true;
        while(((java.util.Iterator)a4).hasNext()) {
            com.here.android.mpa.mapping.Map.LayerCategory a5 = (com.here.android.mpa.mapping.Map.LayerCategory)((java.util.Iterator)a4).next();
            if (!b) {
                a3.append(", ");
            }
            a3.append(a5.name());
            b = false;
        }
        sLogger.v(a3.toString());
    }
    
    private void setMapTraffic() {
        this.mapController.execute(new HereMapsManager$8(this));
    }
    
    private void startTrafficUpdater() {
        try {
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().startTrafficUpdater();
        } catch(Throwable a) {
            sLogger.e(a);
        }
    }

    public void checkForMapDataUpdate() {
        try {
            if (this.isInitialized()) {
                if (this.mapLoader != null) {
                    if (this.mapInitLoaderComplete) {
//                        if (! this.mapLoader.cancelCurrentOperation()) {
//                            sLogger.v("checkForMapDataUpdate MapLoader: cancellation failed");
//                        }
                        this.mapLoader.checkForMapDataUpdate();
//                        boolean b = this.mapLoader.performMapDataUpdate ();
                        sLogger.v("checkForMapDataUpdate MapLoader: called checkForMapDataUpdate");
                    } else {
                        sLogger.v("checkForMapDataUpdate MapLoader: loader not initialized");
                    }
                } else {
                    sLogger.v("checkForMapDataUpdate MapLoader: not available");
                }
            } else {
                sLogger.v("checkForMapDataUpdate MapLoader: engine not initialized");
            }
        } catch(Throwable a) {
            sLogger.e(a);
        }
    }
    
    public boolean isMapUpdateInProgress() {
        return mapUpdateInProgress;
    }

    public void performMapDataUpdate() {
        try {
            if (this.isInitialized()) {
                if (this.mapLoader != null) {
                    if (this.mapInitLoaderComplete) {
                        this.mapUpdateInProgress = this.mapLoader.performMapDataUpdate ();
                        sLogger.v("performMapDataUpdate MapLoader: called performMapDataUpdate:" + this.mapUpdateInProgress);
                        if (!this.mapUpdateInProgress) {
                            showMapsUpdateToast("Maps Update failed: engine busy, try again");
//                            this.mapLoader.cancelCurrentOperation ();
                        }
                    } else {
                        sLogger.v("performMapDataUpdate MapLoader: loader not initialized");
                    }
                } else {
                    sLogger.v("performMapDataUpdate MapLoader: not available");
                }
            } else {
                sLogger.v("performMapDataUpdate MapLoader: engine not initialized");
            }
        } catch(Throwable a) {
            sLogger.e(a);
        }
    }

    public void showMapsUpdateToast(String detail) {
        Bundle bundle = new Bundle();
//        String title = "";
        int icon;
//        String tts = null;
        ToastManager toastManager = ToastManager.getInstance();
        icon = R.drawable.icon_software_update;
        bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 5000);
        String id = "maps-update-notif";
        bundle.putBoolean(ToastPresenter.EXTRA_DEFAULT_CHOICE, true);
//        tts = TTSUtils.TTS_PHONE_BATTERY_VERY_LOW;
        toastManager.dismissCurrentToast(id);
        toastManager.clearPendingToast(id);

        bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, icon);
        bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, detail);
        bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
//        bundle.putString(ToastPresenter.EXTRA_TTS, tts);
        if (!TextUtils.equals(id, toastManager.getCurrentToastId())) {
            ToastManager.getInstance().addToast(new ToastManager$ToastParams(id, bundle, null, false, false));
        }
    }

    void clearMapTraffic() {
        this.mapController.execute(new HereMapsManager$10(this));
    }
    
    public void clearTrafficOverlay() {
        this.clearMapTraffic();
    }
    
    public android.os.Looper getBkLocationReceiverLooper() {
        return this.bkLocationReceiverHandlerThread.getLooper();
    }
    
    public com.here.android.mpa.common.OnEngineInitListener.Error getError() {
        return this.mapError;
    }
    
    public com.here.android.mpa.common.GeoPosition getLastGeoPosition() {
        return this.lastGeoPosition;
    }
    
    public com.navdy.hud.app.maps.here.HereLocationFixManager getLocationFixManager() {
        return this.hereLocationFixManager;
    }
    
    public com.navdy.hud.app.maps.here.HereMapAnimator getMapAnimator() {
        return this.hereMapAnimator;
    }
    
    public com.navdy.hud.app.maps.here.HereMapController getMapController() {
        return this.mapController;
    }
    
    public int getMapPackageCount() {
        return this.mapPackageCount;
    }
    
    public com.here.android.mpa.mapping.MapView getMapView() {
        return this.mapView;
    }
    
    public String getOfflineMapsData() {
        String s;
        try {
            java.io.File a = new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getHereMapsDataDirectory());
            if (a.exists()) {
                java.io.File a0 = new java.io.File(a, "meta.json");
                boolean b = a0.exists();
                s = null;
                if (b) {
                    s = com.navdy.service.library.util.IOUtils.convertFileToString(a0.getAbsolutePath());
                }
            } else {
                s = null;
            }
        } catch(Throwable a1) {
            sLogger.e(a1);
            s = null;
        }
        return s;
    }
    
    public com.navdy.hud.app.maps.here.HereOfflineMapsVersion getOfflineMapsVersion() {
        return this.offlineMapsVersion;
    }
    
    public com.here.android.mpa.common.PositioningManager getPositioningManager() {
        return this.positioningManager;
    }
    
    public com.navdy.hud.app.maps.notification.RouteCalculationEventHandler getRouteCalcEventHandler() {
        return this.routeCalcEventHandler;
    }
    
    public com.here.android.mpa.routing.RouteOptions getRouteOptions() {
        com.here.android.mpa.routing.RouteOptions a = new com.here.android.mpa.routing.RouteOptions();
        a.setTransportMode(com.here.android.mpa.routing.RouteOptions.TransportMode.CAR);
        com.navdy.service.library.events.preferences.NavigationPreferences a0 = this.mDriverProfileManager.getCurrentProfile().getNavigationPreferences();
        switch(com.navdy.hud.app.maps.here.HereMapsManager$14.$SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RoutingType[a0.routingType.ordinal()]) {
            case 2: {
                a.setRouteType(com.here.android.mpa.routing.RouteOptions.Type.SHORTEST);
                break;
            }
            case 1: {
                a.setRouteType(com.here.android.mpa.routing.RouteOptions.Type.FASTEST);
                break;
            }
        }
        a.setHighwaysAllowed(Boolean.TRUE.equals(a0.allowHighways));
        a.setTollRoadsAllowed(Boolean.TRUE.equals(a0.allowTollRoads));
        a.setFerriesAllowed(false);
        a.setTunnelsAllowed(Boolean.TRUE.equals(a0.allowTunnels));
        a.setDirtRoadsAllowed(Boolean.TRUE.equals(a0.allowUnpavedRoads));
        a.setCarShuttleTrainsAllowed(Boolean.TRUE.equals(a0.allowAutoTrains));
        a.setCarpoolAllowed(false);
        return a;
    }
    
    public com.here.android.mpa.common.GeoCoordinate getRouteStartPoint() {
        return this.routeStartPoint;
    }
    
    String getTrackingMapScheme() {
        return mapsSchemeEnroute;
    }

    String getEnrouteMapScheme() {
        return mapsSchemeTracking;
    }


    public String getVersion() {
        return (this.isInitialized()) ? com.here.android.mpa.common.Version.getSdkVersion() : null;
    }
    
    public void handleBandwidthPreferenceChange() {
        if (this.isInitialized()) {
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().setBandwidthPreferences();
        }
    }
    
    public void hideTraffic() {
        if (this.isInitialized()) {
            if (com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().isTrafficEnabled()) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new HereMapsManager$12(this), 3);
            } else {
                sLogger.v("hidetraffic: traffic is not enabled");
            }
        }
    }
    
    public void initNavigation() {
        if (this.isInitialized()) {
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getNavController().initialize();
        }
    }
    
    public void installPositionListener() {
        synchronized(this) {
            if (!this.positioningManagerInstalled) {
                this.positioningManager.addListener(new java.lang.ref.WeakReference(this.positionChangedListener));
                sLogger.v("position manager listener installed");
                this.positioningManagerInstalled = true;
            }
        }
        /*monexit(this)*/
    }
    
    public boolean isEngineOnline() {
        boolean b = false;
        boolean b0 = this.isInitialized();
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.mapEngine == null) {
                        break label1;
                    }
                    if (com.here.android.mpa.common.MapEngine.isOnlineEnabled()) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public boolean isInitialized() {
        boolean b;
        synchronized(this.initLock) {
            b = this.mapInitialized;
            /*monexit(a)*/
        }
        return b;
    }
    
    public boolean isInitializing() {
        boolean b = false;
        synchronized(this.initLock) {
            b = this.mapInitializing;
            /*monexit(a)*/
        }
        return b;
    }
    
    public boolean isMapDataVerified() {
        return this.mapDataVerified;
    }
    
    public boolean isNavigationModeOn() {
        return this.isInitialized() && com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn();
    }
    
    public boolean isNavigationPossible() {
        return this.isInitialized() && this.hereLocationFixManager.getLastGeoCoordinate() != null;
    }
    
    public boolean isRecalcCurrentRouteForTrafficEnabled() {
        return recalcCurrentRouteForTraffic;
    }
    
    public boolean isRenderingAllowed() {
        return !this.powerManager.inQuietMode();
    }
    
    public boolean isVoiceSkinsLoaded() {
        return this.voiceSkinsLoaded;
    }
    
    @Subscribe
    public void onLinkPropertiesChanged(com.navdy.service.library.events.connection.LinkPropertiesChanged a) {
        if (a != null && a.bandwidthLevel != null) {
            sLogger.d("Link Properties changed");
            if (a.bandwidthLevel > 0) {
                sLogger.d("Switching back to normal bandwidth mode");
                this.lowBandwidthDetected = false;
                this.updateEngineOnlineState();
            } else {
                sLogger.d("Switching to low bandwidth mode");
                this.lowBandwidthDetected = true;
                this.updateEngineOnlineState();
            }
        }
    }
    
    @Subscribe
    public void onWakeup(com.navdy.hud.app.event.Wakeup a) {
        if (this.isInitialized()) {
            this.startTrafficUpdater();
        }
    }
    
    public void postManeuverDisplay() {
        if (this.isInitialized()) {
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().postManeuverDisplay();
        }
    }
    
    public void requestMapsEngineOnlineStateChange(boolean b) {
        sLogger.d("Request to set online state " + b);
        this.turnEngineOn = b;
        this.updateEngineOnlineState();
    }
    
    public void setMapView(com.here.android.mpa.mapping.MapView a, com.navdy.hud.app.ui.component.homescreen.NavigationView a0) {
        this.mapView = a;
        this.navigationView = a0;
        com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().setMapView(a, a0);
        com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode a1 = this.hereMapAnimator.getAnimationMode();
        if (a1 == com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode.NONE) {
            sLogger.v("not installing map render listener:" + a1);
        } else {
            sLogger.v("installing map render listener:" + a1);
            this.mapView.addOnMapRenderListener(this.hereMapAnimator.getMapRenderListener());
        }
    }
    
    public void setRouteStartPoint(com.here.android.mpa.common.GeoCoordinate a) {
        this.routeStartPoint = a;
    }
    
    public void setTrafficOverlay(com.navdy.hud.app.maps.NavigationMode a) {
        if (a != com.navdy.hud.app.maps.NavigationMode.MAP) {
            if (a == com.navdy.hud.app.maps.NavigationMode.MAP_ON_ROUTE) {
                this.setMapOnRouteTraffic();
            }
        } else {
            this.setMapTraffic();
        }
    }
    
    public void setVoiceSkinsLoaded() {
        this.voiceSkinsLoaded = true;
    }
    
    public void showTraffic() {
        if (this.isInitialized()) {
            if (com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().isTrafficEnabled()) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new HereMapsManager$13(this), 3);
            } else {
                sLogger.v("showTraffic: traffic is not enabled");
            }
        }
    }
    
    public void startMapRendering() {
        if (this.hereMapAnimator == null) {
            this.initialMapRendering = true;
        } else {
            this.hereMapAnimator.startMapRendering();
        }
    }
    
    public void stopMapRendering() {
        if (this.hereMapAnimator == null) {
            this.initialMapRendering = false;
        } else {
            this.hereMapAnimator.stopMapRendering();
        }
    }
    
    public void turnOffline() {
        synchronized(this) {
            if (this.isInitialized()) {
                this.requestMapsEngineOnlineStateChange(false);
                com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().stopTrafficRerouteListener();
                sLogger.v("isOnline:" + com.here.android.mpa.common.MapEngine.isOnlineEnabled());
            } else {
                sLogger.i("turnOffline: engine not yet initialized");
            }
        }
        /*monexit(this)*/
    }
    
    public void turnOnline() {
        synchronized(this) {
            if (this.isInitialized()) {
                sLogger.i("turnOnline: enabling traffic info");
                this.requestMapsEngineOnlineStateChange(true);
                if (com.navdy.hud.app.framework.glance.GlanceHelper.isTrafficNotificationEnabled()) {
                    sLogger.i("turnOnline: enabling traffic notifications");
                    com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().startTrafficRerouteListener();
                } else {
                    sLogger.i("turnOnline: not enabling traffic notifications, glance off");
                }
                sLogger.v("turnOnline invokeMapLoader");
                this.invokeMapLoader();
                sLogger.v("isOnline:" + com.here.android.mpa.common.MapEngine.isOnlineEnabled());
            } else {
                sLogger.i("turnOnline: engine not yet initialized");
            }
        }
        /*monexit(this)*/
    }
    
    public void updateEngineOnlineState() {
        synchronized(this) {
            com.navdy.service.library.log.Logger a = sLogger;
            a.d("Updating the engine online state LowBandwidthDetected :" + this.lowBandwidthDetected + ", TurnEngineOn :" + this.turnEngineOn);
            if (this.isInitialized()) {
                boolean b = false;
                boolean b0 = false;
                com.navdy.service.library.log.Logger a0 = sLogger;
                StringBuilder a1 = new StringBuilder().append("Turning engine on :");
                boolean b1 = this.lowBandwidthDetected;
                label5: {
                    label3: {
                        label4: {
                            if (b1) {
                                break label4;
                            }
                            if (this.turnEngineOn) {
                                break label3;
                            }
                        }
                        b = false;
                        break label5;
                    }
                    b = true;
                }
                a0.d(a1.append(b).toString());
                boolean b2 = this.lowBandwidthDetected;
                label2: {
                    label0: {
                        label1: {
                            if (b2) {
                                break label1;
                            }
                            if (this.turnEngineOn) {
                                break label0;
                            }
                        }
                        b0 = false;
                        break label2;
                    }
                    b0 = true;
                }
                com.here.android.mpa.common.MapEngine.setOnline(b0);
            }
        }
        /*monexit(this)*/
    }
}
