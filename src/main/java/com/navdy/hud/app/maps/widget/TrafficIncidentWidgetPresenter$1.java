package com.navdy.hud.app.maps.widget;

class TrafficIncidentWidgetPresenter$1 implements Runnable {
    final com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter this$0;
    private final com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident val$event;
    
    TrafficIncidentWidgetPresenter$1(com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter a, com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident a0) {

        super();
        this.this$0 = a;
        this.val$event = a0;
    }
    
    public void run() {
        android.text.SpannableStringBuilder a = new android.text.SpannableStringBuilder();
        com.here.android.mpa.common.Image a0 = this.val$event.icon;
        android.graphics.Bitmap a1 = null;
        if (a0 != null) {
            a1 = this.val$event.icon.getBitmap(40, 40);
        }
        a.append(this.val$event.type.name());
        a.setSpan(new android.text.style.StyleSpan(1), 0, a.length(), 33);
        a.append(" ");
        int i = a.length();
        if (a1 != null) {
            android.text.style.ImageSpan a2 = new android.text.style.ImageSpan(com.navdy.hud.app.HudApplication.getAppContext(), a1);
            a.append("IMG");
            a.setSpan(a2, i, a.length(), 33);
            a.append(" ");
        }
        if (!android.text.TextUtils.isEmpty(this.val$event.title)) {
            a.append("(");
            a.append(this.val$event.title);
            a.append(") ,");
        }
        if (!android.text.TextUtils.isEmpty(this.val$event.description)) {
            a.append(this.val$event.description);
            a.append(" , ");
        }
        if (!android.text.TextUtils.isEmpty(this.val$event.affectedStreet)) {
            a.append("street=");
            a.append(this.val$event.affectedStreet);
            a.append(" , ");
        }
        if (this.val$event.distanceToIncident != -1L) {
            a.append("dist=");
            a.append(String.valueOf(this.val$event.distanceToIncident));
            a.append(" , ");
        }
        if (this.val$event.updated != null) {
            long j = new java.util.Date().getTime();
            long j0 = this.val$event.updated.getTime();
            a.append(String.valueOf(java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(j - j0))).append(" minutes ago");
        }
        com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter.access$100().post(new TrafficIncidentWidgetPresenter$1$1(this, a));
    }
}
