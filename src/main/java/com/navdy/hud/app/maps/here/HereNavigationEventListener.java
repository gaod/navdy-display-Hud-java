package com.navdy.hud.app.maps.here;

class HereNavigationEventListener extends com.here.android.mpa.guidance.NavigationManager.NavigationManagerEventListener {
    final private com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    final private com.navdy.service.library.log.Logger logger;
    final private String tag;
    
    HereNavigationEventListener(com.navdy.service.library.log.Logger a, String s, com.navdy.hud.app.maps.here.HereNavigationManager a0) {
        this.logger = a;
        this.tag = s;
        this.hereNavigationManager = a0;
    }
    
    static com.navdy.hud.app.maps.here.HereNavigationManager access$000(com.navdy.hud.app.maps.here.HereNavigationEventListener a) {
        return a.hereNavigationManager;
    }
    
    static String access$100(com.navdy.hud.app.maps.here.HereNavigationEventListener a) {
        return a.tag;
    }
    
    static com.navdy.service.library.log.Logger access$200(com.navdy.hud.app.maps.here.HereNavigationEventListener a) {
        return a.logger;
    }
    
    public void onEnded(com.here.android.mpa.guidance.NavigationManager.NavigationMode a) {
        this.logger.i(new StringBuilder().append(this.tag).append(" onEnded:").append(a).toString());
        if (this.hereNavigationManager.isLastManeuver()) {
            this.logger.i(new StringBuilder().append(this.tag).append(" onEnded: last maneuver on, marking arrived").toString());
            this.hereNavigationManager.arrived();
        }
    }
    
    public void onNavigationModeChanged() {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereNavigationEventListener$1(this), 2);
    }
    
    public void onRouteUpdated(com.here.android.mpa.routing.Route a) {
        this.logger.i(new StringBuilder().append(this.tag).append(" onRouteUpdated:").append((a).toString()).toString());
    }
}
