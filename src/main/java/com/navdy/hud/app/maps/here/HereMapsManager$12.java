package com.navdy.hud.app.maps.here;

import static com.navdy.hud.app.maps.here.HereMapsManager.sLogger;

class HereMapsManager$12 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapsManager this$0;
    
    HereMapsManager$12(com.navdy.hud.app.maps.here.HereMapsManager a) {
       super();
        this.this$0 = a;
    }
    
    public void run() {
        this.this$0.mapController.setTrafficInfoVisible(false);
        sLogger.v("hidetraffic: map off");
        com.here.android.mpa.mapping.MapRoute a = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentMapRoute();
        if (a != null) {
            a.setTrafficEnabled(false);
           sLogger.v("hidetraffic: route off");
        }
      sLogger.v("hidetraffic: traffic is disabled");
    }
}
