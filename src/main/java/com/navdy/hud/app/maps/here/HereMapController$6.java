package com.navdy.hud.app.maps.here;

class HereMapController$6 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapController this$0;
    private final java.util.List val$mapObjects;
    
    HereMapController$6(com.navdy.hud.app.maps.here.HereMapController a, java.util.List a0) {

        super();
        this.this$0 = a;
        this.val$mapObjects = a0;
    }
    
    public void run() {
        Object a = this.val$mapObjects.iterator();
        while(((java.util.Iterator)a).hasNext()) {
            com.here.android.mpa.mapping.MapObject a0 = (com.here.android.mpa.mapping.MapObject)((java.util.Iterator)a).next();
            com.navdy.hud.app.maps.here.HereMapController.access$200(this.this$0, a0);
        }
    }
}
