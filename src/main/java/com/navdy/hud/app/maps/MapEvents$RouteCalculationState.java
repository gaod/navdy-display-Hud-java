package com.navdy.hud.app.maps;

public enum MapEvents$RouteCalculationState {
    NONE(0),
    STARTED(1),
    STOPPED(2),
    IN_PROGRESS(3),
    ABORT_NAVIGATION(4),
    FINDING_NAV_COORDINATES(5);

    private int value;
    MapEvents$RouteCalculationState(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }

}

//final public class MapEvents$RouteCalculationState extends Enum {
//    final private static com.navdy.hud.app.maps.MapEvents$RouteCalculationState[] $VALUES;
//    final public static com.navdy.hud.app.maps.MapEvents$RouteCalculationState ABORT_NAVIGATION;
//    final public static com.navdy.hud.app.maps.MapEvents$RouteCalculationState FINDING_NAV_COORDINATES;
//    final public static com.navdy.hud.app.maps.MapEvents$RouteCalculationState IN_PROGRESS;
//    final public static com.navdy.hud.app.maps.MapEvents$RouteCalculationState NONE;
//    final public static com.navdy.hud.app.maps.MapEvents$RouteCalculationState STARTED;
//    final public static com.navdy.hud.app.maps.MapEvents$RouteCalculationState STOPPED;
//
//    static {
//        NONE = new com.navdy.hud.app.maps.MapEvents$RouteCalculationState("NONE", 0);
//        STARTED = new com.navdy.hud.app.maps.MapEvents$RouteCalculationState("STARTED", 1);
//        STOPPED = new com.navdy.hud.app.maps.MapEvents$RouteCalculationState("STOPPED", 2);
//        IN_PROGRESS = new com.navdy.hud.app.maps.MapEvents$RouteCalculationState("IN_PROGRESS", 3);
//        ABORT_NAVIGATION = new com.navdy.hud.app.maps.MapEvents$RouteCalculationState("ABORT_NAVIGATION", 4);
//        FINDING_NAV_COORDINATES = new com.navdy.hud.app.maps.MapEvents$RouteCalculationState("FINDING_NAV_COORDINATES", 5);
//        com.navdy.hud.app.maps.MapEvents$RouteCalculationState[] a = new com.navdy.hud.app.maps.MapEvents$RouteCalculationState[6];
//        a[0] = NONE;
//        a[1] = STARTED;
//        a[2] = STOPPED;
//        a[3] = IN_PROGRESS;
//        a[4] = ABORT_NAVIGATION;
//        a[5] = FINDING_NAV_COORDINATES;
//        $VALUES = a;
//    }
//
//    private MapEvents$RouteCalculationState(String s, int i) {
//        super(s, i);
//    }
//
//    public static com.navdy.hud.app.maps.MapEvents$RouteCalculationState valueOf(String s) {
//        return (com.navdy.hud.app.maps.MapEvents$RouteCalculationState)Enum.valueOf(com.navdy.hud.app.maps.MapEvents$RouteCalculationState.class, s);
//    }
//
//    public static com.navdy.hud.app.maps.MapEvents$RouteCalculationState[] values() {
//        return $VALUES.clone();
//    }
//}
