package com.navdy.hud.app.maps.here;

class HereNavigationManager$6 implements Runnable {
    final com.navdy.hud.app.maps.here.HereNavigationManager this$0;
    final com.navdy.service.library.device.NavdyDeviceId val$deviceId;
    
    HereNavigationManager$6(com.navdy.hud.app.maps.here.HereNavigationManager a, com.navdy.service.library.device.NavdyDeviceId a0) {

        super();
        this.this$0 = a;
        this.val$deviceId = a0;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.i(new StringBuilder().append("stop navigation, device id has changed from[").append(com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).deviceId).append("] to [").append(this.val$deviceId).append("]").toString());
        this.this$0.setNavigationMode(com.navdy.hud.app.maps.NavigationMode.MAP, (com.navdy.hud.app.maps.here.HereNavigationInfo)null, (StringBuilder)null);
    }
}
