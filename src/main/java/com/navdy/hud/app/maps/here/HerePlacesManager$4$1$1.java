package com.navdy.hud.app.maps.here;
import com.navdy.hud.app.R;

class HerePlacesManager$4$1$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HerePlacesManager$4$1 this$1;
    final com.here.android.mpa.search.ErrorCode val$errorCode;
    final com.here.android.mpa.search.DiscoveryResultPage val$result;
    
    HerePlacesManager$4$1$1(com.navdy.hud.app.maps.here.HerePlacesManager$4$1 a, com.here.android.mpa.search.ErrorCode a0, com.here.android.mpa.search.DiscoveryResultPage a1) {

        super();
        this.this$1 = a;
        this.val$errorCode = a0;
        this.val$result = a1;
    }
    
    public void run() {
        label0: {
            Throwable a = null;
            label1: {
                label6: {
                    label4: try {
                        com.here.android.mpa.search.ErrorCode a0 = this.val$errorCode;
                        com.here.android.mpa.search.ErrorCode a1 = com.here.android.mpa.search.ErrorCode.NONE;
                        label5: {
                            if (a0 == a1) {
                                break label5;
                            }
                            String s = this.this$1.val$searchQuery;
                            com.navdy.service.library.events.RequestStatus a2 = com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR;
                            android.content.Context a3 = com.navdy.hud.app.maps.here.HerePlacesManager.access$200();
                            Object[] a4 = new Object[1];
                            a4[0] = this.val$errorCode.toString();
                            com.navdy.hud.app.maps.here.HerePlacesManager.access$300(s, a2, a3.getString(R.string.search_error, a4));
                            break label6;
                        }
                        if (this.val$result == null) {
                            com.navdy.hud.app.maps.here.HerePlacesManager.access$000().d("[search] no results");
                            com.navdy.hud.app.maps.here.HerePlacesManager.access$500(this.this$1.val$geoPosition, this.this$1.val$searchQuery, (java.util.List)com.navdy.hud.app.maps.here.HerePlacesManager.access$400(), this.this$1.val$maxResults, this.this$1.val$searchArea);
                        } else {
                            java.util.List a5 = this.val$result.getPlaceLinks();
                            label2: {
                                label3: {
                                    if (a5 == null) {
                                        break label3;
                                    }
                                    if (a5.size() != 0) {
                                        break label2;
                                    }
                                }
                                com.navdy.hud.app.maps.here.HerePlacesManager.access$000().d("[search] no results");
                                com.navdy.hud.app.maps.here.HerePlacesManager.access$500(this.this$1.val$geoPosition, this.this$1.val$searchQuery, (java.util.List)com.navdy.hud.app.maps.here.HerePlacesManager.access$400(), this.this$1.val$maxResults, this.this$1.val$searchArea);
                                break label4;
                            }
                            com.navdy.hud.app.maps.here.HerePlacesManager.access$000().d(new StringBuilder().append("[search] returned results:").append(a5.size()).toString());
                            com.navdy.hud.app.maps.here.HerePlacesManager.access$500(this.this$1.val$geoPosition, this.this$1.val$searchQuery, a5, this.this$1.val$maxResults, this.this$1.val$searchArea);
                        }
                    } catch(Throwable a6) {
                        a = a6;
                        break label1;
                    }
                    if (!com.navdy.hud.app.maps.here.HerePlacesManager.access$000().isLoggable(2)) {
                        break label0;
                    }
                    long j = android.os.SystemClock.elapsedRealtime();
                    com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("[search] handleSearchRequest- time-2 =").append(j - this.this$1.val$l1).toString());
                    break label0;
                }
                if (!com.navdy.hud.app.maps.here.HerePlacesManager.access$000().isLoggable(2)) {
                    break label0;
                }
                long j0 = android.os.SystemClock.elapsedRealtime();
                com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("[search] handleSearchRequest- time-2 =").append(j0 - this.this$1.val$l1).toString());
                break label0;
            }
            try {
                com.navdy.hud.app.maps.here.HerePlacesManager.access$000().e(a);
                com.navdy.hud.app.maps.here.HerePlacesManager.access$300(this.this$1.val$searchQuery, com.navdy.service.library.events.RequestStatus.REQUEST_UNKNOWN_ERROR, com.navdy.hud.app.maps.here.HerePlacesManager.access$200().getString(R.string.search_error));
            } catch(Throwable a7) {
                if (com.navdy.hud.app.maps.here.HerePlacesManager.access$000().isLoggable(2)) {
                    long j1 = android.os.SystemClock.elapsedRealtime();
                    com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("[search] handleSearchRequest- time-2 =").append(j1 - this.this$1.val$l1).toString());
                }
                throw a7;
            }
            if (com.navdy.hud.app.maps.here.HerePlacesManager.access$000().isLoggable(2)) {
                long j2 = android.os.SystemClock.elapsedRealtime();
                com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("[search] handleSearchRequest- time-2 =").append(j2 - this.this$1.val$l1).toString());
            }
        }
    }
}
