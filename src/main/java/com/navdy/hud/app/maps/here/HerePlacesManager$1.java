package com.navdy.hud.app.maps.here;

final class HerePlacesManager$1 implements com.here.android.mpa.search.ResultListener {
    final com.navdy.hud.app.maps.here.HerePlacesManager$PlacesSearchCallback val$callBack;
    
    HerePlacesManager$1(com.navdy.hud.app.maps.here.HerePlacesManager$PlacesSearchCallback a) {

        super();
        this.val$callBack = a;
    }
    
    public void onCompleted(com.here.android.mpa.search.DiscoveryResultPage a, com.here.android.mpa.search.ErrorCode a0) {
        try {
            this.val$callBack.result(a0, a);
        } catch(Throwable a1) {
            com.navdy.hud.app.maps.here.HerePlacesManager.access$000().e(a1);
        }
    }
    
    public void onCompleted(Object a, com.here.android.mpa.search.ErrorCode a0) {
        this.onCompleted((com.here.android.mpa.search.DiscoveryResultPage)a, a0);
    }
}
