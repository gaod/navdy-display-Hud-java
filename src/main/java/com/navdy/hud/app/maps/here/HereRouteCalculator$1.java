package com.navdy.hud.app.maps.here;

class HereRouteCalculator$1 implements com.here.android.mpa.routing.CoreRouter.Listener {
    private int lastPercentage;
    private long lastProgress;
    final com.navdy.hud.app.maps.here.HereRouteCalculator this$0;
    final boolean val$allowCancellation;
    final boolean val$cache;
    final boolean val$calculatePolyline;
    final boolean val$factoringInTraffic;
    final com.navdy.hud.app.maps.here.HereRouteCalculator$RouteCalculatorListener val$listener;
    final com.navdy.service.library.events.navigation.NavigationRouteRequest val$request;
    final com.here.android.mpa.common.GeoCoordinate val$startPoint;
    final long val$startTime;
    
    HereRouteCalculator$1(com.navdy.hud.app.maps.here.HereRouteCalculator a, com.navdy.hud.app.maps.here.HereRouteCalculator$RouteCalculatorListener a0, boolean b, com.navdy.service.library.events.navigation.NavigationRouteRequest a1, boolean b0, boolean b1, boolean b2, com.here.android.mpa.common.GeoCoordinate a2, long j) {
        super();
        this.lastPercentage = -1;
        this.lastProgress = 0L;
        this.this$0 = a;
        this.val$listener = a0;
        this.val$allowCancellation = b;
        this.val$request = a1;
        this.val$calculatePolyline = b0;
        this.val$cache = b1;
        this.val$factoringInTraffic = b2;
        this.val$startPoint = a2;
        this.val$startTime = j;
    }
    
//    public void onCalculateRouteFinished(Object a, Enum a0) {
//        this.onCalculateRouteFinished((java.util.List)a, (com.here.android.mpa.routing.RoutingError)a0);
//    }
    
    public void onCalculateRouteFinished(java.util.List a, com.here.android.mpa.routing.RoutingError a0) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereRouteCalculator$1$1(this, a0, a), 2);
    }
    
    public void onProgress(int i) {
        long j = android.os.SystemClock.elapsedRealtime();
        label0: if (i != this.lastPercentage) {
            long j0 = this.lastProgress + 200L;
            int i0 = (j < j0) ? -1 : (j == j0) ? 0 : 1;
            label1: {
                if (i0 > 0) {
                    break label1;
                }
                if (i != 100) {
                    break label0;
                }
            }
            this.lastPercentage = i;
            this.lastProgress = j;
            long j1 = this.val$startTime;
            com.navdy.hud.app.maps.here.HereRouteCalculator.access$200(this.this$0).i(new StringBuilder().append(com.navdy.hud.app.maps.here.HereRouteCalculator.access$100(this.this$0)).append("route calculation: [").append(i).append("%] done timeElapsed:").append(j - j1).toString());
            this.val$listener.progress(i);
        }
    }
}
