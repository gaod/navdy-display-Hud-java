package com.navdy.hud.app.maps.here;
import com.squareup.otto.Subscribe;

public class NavdyTrafficRerouteManager {
    final private static int KILL_ROUTE_CALC = 45000;
    final private static int STALE_PERIOD = 300000;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private com.squareup.otto.Bus bus;
    private Runnable fasterRouteCheck;
    private Runnable fasterRouteTrigger;
    private android.os.Handler handler;
    final private com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    final private com.navdy.hud.app.maps.here.HereTrafficRerouteListener hereTrafficRerouteListener;
    private Runnable killRouteCalc;
    private com.navdy.hud.app.maps.here.HereRouteCalculator routeCalculator;
    private volatile boolean running;
    private Runnable staleRoute;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger("NavdyTrafficRerout");
    }
    
    NavdyTrafficRerouteManager(com.navdy.hud.app.maps.here.HereNavigationManager a, com.navdy.hud.app.maps.here.HereTrafficRerouteListener a0, com.squareup.otto.Bus a1) {
        this.routeCalculator = new com.navdy.hud.app.maps.here.HereRouteCalculator(sLogger, false);
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.staleRoute = new NavdyTrafficRerouteManager$1(this);
        this.killRouteCalc = new NavdyTrafficRerouteManager$2(this);
        this.fasterRouteTrigger = new NavdyTrafficRerouteManager$3(this);
        this.fasterRouteCheck = new NavdyTrafficRerouteManager$4(this);
        sLogger.v("NavdyTrafficRerouteManager:ctor");
        this.hereNavigationManager = a;
        this.hereTrafficRerouteListener = a0;
        this.bus = a1;
    }
    
    static com.navdy.hud.app.maps.here.HereTrafficRerouteListener access$000(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager a) {
        return a.hereTrafficRerouteListener;
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static com.navdy.hud.app.maps.here.HereRouteCalculator access$200(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager a) {
        return a.routeCalculator;
    }
    
    static Runnable access$300(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager a) {
        return a.fasterRouteCheck;
    }
    
    static Runnable access$400(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager a) {
        return a.killRouteCalc;
    }
    
    static android.os.Handler access$500(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager a) {
        return a.handler;
    }
    
    static void access$600(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager a) {
        a.cleanupRouteIfStale();
    }
    
    static com.navdy.hud.app.maps.here.HereNavigationManager access$700(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager a) {
        return a.hereNavigationManager;
    }
    
    static boolean access$800(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager a) {
        return a.running;
    }
    
    static Runnable access$900(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager a) {
        return a.fasterRouteTrigger;
    }
    
    private void cleanupRouteIfStale() {
        if (this.hereTrafficRerouteListener.getCurrentProposedRoute() == null) {
            sLogger.v("no proposed route");
        } else {
            long j = android.os.SystemClock.elapsedRealtime() - this.hereTrafficRerouteListener.getCurrentProposedRouteTime();
            if (j <= 300000L) {
                sLogger.v("route not stale:" + j);
            } else {
                sLogger.v("clear stale route:" + j);
                this.hereTrafficRerouteListener.dismissReroute();
            }
        }
    }
    
    @Subscribe
    public void onBandwidthSettingChanged(com.navdy.hud.app.framework.network.NetworkBandwidthController$UserBandwidthSettingChanged a) {
        this.handler.removeCallbacks(this.fasterRouteTrigger);
        if (this.running) {
            long j = (long)(this.hereNavigationManager.getRerouteInterval() / 2);
            sLogger.v("faster route time changed to " + j);
            this.handler.postDelayed(this.fasterRouteTrigger, j);
        }
    }
    
    void reCalculateCurrentRoute(com.here.android.mpa.routing.Route a, com.here.android.mpa.routing.Route a0, boolean b, String s, long j, String s0, long j0, long j1, String s1, com.here.android.mpa.routing.RouteTta a1) {
        try {
            sLogger.v("reCalculateCurrentRoute:");
            this.handler.removeCallbacks(this.killRouteCalc);
            if (com.navdy.hud.app.framework.network.NetworkStateManager.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext())) {
                if (this.hereNavigationManager.isNavigationModeOn()) {
                    if (this.hereNavigationManager.isRerouting()) {
                        sLogger.v("reCalculateCurrentRoute: rerouting");
                        this.hereTrafficRerouteListener.dismissReroute();
                        this.hereTrafficRerouteListener.fasterRouteNotFound(b, j, s, s1);
                    } else if (this.hereNavigationManager.hasArrived()) {
                        sLogger.v("reCalculateCurrentRoute: arrival mode on");
                        this.hereTrafficRerouteListener.dismissReroute();
                        this.hereTrafficRerouteListener.fasterRouteNotFound(b, j, s, s1);
                    } else if (this.hereNavigationManager.isOnGasRoute()) {
                        sLogger.v("reCalculateCurrentRoute: on gas route");
                        this.hereTrafficRerouteListener.dismissReroute();
                        this.hereTrafficRerouteListener.fasterRouteNotFound(b, j, s, s1);
                    } else {
                        com.navdy.hud.app.maps.here.HereMapsManager a2 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
                        com.here.android.mpa.common.GeoCoordinate a3 = a2.getLocationFixManager().getLastGeoCoordinate();
                        if (a3 != null) {
                            com.here.android.mpa.common.GeoCoordinate a4 = a.getDestination();
                            if (a4 != null) {
                                com.here.android.mpa.routing.RouteOptions a5 = a2.getRouteOptions();
                                a5.setRouteType(com.here.android.mpa.routing.RouteOptions.Type.FASTEST);
                                com.here.android.mpa.routing.Maneuver a6 = this.hereNavigationManager.getNavController().getNextManeuver();
                                if (a6 != null) {
                                    Object a7;
                                    int i;
                                    int i0;
                                    java.util.List a8 = a.getManeuvers();
                                    if (a8 == null) {
                                        a7 = null;
                                        i = 0;
                                        i0 = -1;
                                    } else {
                                        i = a8.size();
                                        a7 = a8;
                                        i0 = 0;
                                        while(true) {
                                            if (i0 >= i) {
                                                i0 = -1;
                                                break;
                                            } else {
                                                if (!com.navdy.hud.app.maps.here.HereMapUtil.areManeuverEqual((com.here.android.mpa.routing.Maneuver)((java.util.List)a7).get(i0), a6)) {
                                                    i0 = i0 + 1;
                                                    continue;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                    if (i0 != -1) {
                                        java.util.ArrayList a9 = new java.util.ArrayList<>();
                                        while(i0 < i) {
                                            com.here.android.mpa.common.GeoCoordinate a10 = ((com.here.android.mpa.routing.Maneuver)((java.util.List)a7).get(i0)).getCoordinate();
                                            if (a10 != null) {
                                                sLogger.i("reCalculateCurrentRoute: added waypoint:" + a10);
                                                a9.add(a10);
                                            }
                                            i0 = i0 + 1;
                                        }
                                        sLogger.i("reCalculateCurrentRoute: waypoints:" + a9.size());
                                        this.routeCalculator.cancel();
                                        this.handler.postDelayed(this.killRouteCalc, 45000L);
                                        this.routeCalculator.calculateRoute(null, a3, a9, a4, true, new NavdyTrafficRerouteManager$5(this, b, j, s, s1, a1, j0, a0, s0, j1), 1, a5, true, false, false);
                                    } else {
                                        sLogger.i("reCalculateCurrentRoute: cannot find current maneuver in route");
                                        this.cleanupRouteIfStale();
                                        this.hereTrafficRerouteListener.fasterRouteNotFound(b, j, s, s1);
                                    }
                                } else {
                                    this.cleanupRouteIfStale();
                                    this.hereTrafficRerouteListener.fasterRouteNotFound(b, j, s, s1);
                                    sLogger.i("reCalculateCurrentRoute: no current maneuver");
                                }
                            } else {
                                this.cleanupRouteIfStale();
                                this.hereTrafficRerouteListener.fasterRouteNotFound(b, j, s, s1);
                                sLogger.e("reCalculateCurrentRoute: no end point");
                            }
                        } else {
                            this.cleanupRouteIfStale();
                            this.hereTrafficRerouteListener.fasterRouteNotFound(b, j, s, s1);
                            sLogger.e("reCalculateCurrentRoute: no start point");
                        }
                    }
                } else {
                    this.hereTrafficRerouteListener.dismissReroute();
                    this.hereTrafficRerouteListener.fasterRouteNotFound(b, j, s, s1);
                    sLogger.v("reCalculateCurrentRoute: not navigating");
                }
            } else {
                this.cleanupRouteIfStale();
                this.hereTrafficRerouteListener.fasterRouteNotFound(b, j, s, s1);
                sLogger.v("reCalculateCurrentRoute:skip no n/w");
            }
        } catch(Throwable a11) {
            sLogger.e(a11);
            this.cleanupRouteIfStale();
            this.hereTrafficRerouteListener.fasterRouteNotFound(b, j, s, s1);
            com.navdy.hud.app.util.CrashReporter.getInstance().reportNonFatalException(a11);
        }
    }
    
    public void reset() {
        try {
            sLogger.v("reset::");
            this.routeCalculator.cancel();
            this.hereTrafficRerouteListener.dismissReroute();
            this.handler.removeCallbacks(this.fasterRouteTrigger);
            if (this.running) {
                sLogger.v("reset::timer reset");
                this.handler.postDelayed(this.fasterRouteTrigger, (long)com.navdy.hud.app.maps.here.HereTrafficRerouteListener.ROUTE_CHECK_INITIAL_INTERVAL);
            }
        } catch(Throwable a) {
            sLogger.e(a);
        }
    }
    
    public void start() {
        if (!this.running) {
            this.running = true;
            this.handler.removeCallbacks(this.staleRoute);
            this.handler.removeCallbacks(this.fasterRouteTrigger);
            this.handler.postDelayed(this.fasterRouteTrigger, (long)com.navdy.hud.app.maps.here.HereTrafficRerouteListener.ROUTE_CHECK_INITIAL_INTERVAL);
            this.bus.register(this);
            sLogger.v("running");
        }
    }
    
    public void stop() {
        if (this.running) {
            this.running = false;
            this.handler.removeCallbacks(this.fasterRouteTrigger);
            this.handler.postDelayed(this.staleRoute, 300000L);
            this.bus.unregister(this);
            sLogger.v("stopped");
        }
    }
}
