package com.navdy.hud.app.maps.here;

import android.content.Context;

import com.navdy.hud.app.HudApplication;

import java.util.Objects;

import static com.navdy.hud.app.device.gps.GpsManager.MOCK_PROVIDER;
import static com.navdy.hud.app.maps.here.HereMapsManager.sLogger;


class HereMapsManager$5 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapsManager this$0;
    
    HereMapsManager$5(com.navdy.hud.app.maps.here.HereMapsManager a) {

        super();
        this.this$0 = a;
    }
    
    public void run() {
        label0: {
            Throwable a;
            label1: {
                boolean b;
                try {
                    if (((android.location.LocationManager) Objects.requireNonNull(HudApplication.getAppContext().getSystemService(Context.LOCATION_SERVICE))).getProvider(MOCK_PROVIDER) != null) {
                        sLogger.v("n/w provider found, initialize here");
                        com.navdy.service.library.task.TaskManager.getInstance().execute(new HereMapsManager$5$1(this), 2);
                        b = false;
                    } else {
                       sLogger.v("n/w provider not found yet, check again");
                        b = true;
                    }
                } catch(Throwable a0) {
                    a = a0;
                    break label1;
                }
                if (!b) {
                    break label0;
                }
                this.this$0.handler.postDelayed(this, 1000L);
                break label0;
            }
            HereMapsManager.sLogger.e(a);
            this.this$0.handler.postDelayed(this, 1000L);
        }
    }
}
