package com.navdy.hud.app.maps.here;

class HereTrafficETATracker$4 implements Runnable {
    final com.navdy.hud.app.maps.here.HereTrafficETATracker this$0;
    
    HereTrafficETATracker$4(com.navdy.hud.app.maps.here.HereTrafficETATracker a) {

        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereNavController$State a = com.navdy.hud.app.maps.here.HereTrafficETATracker.access$300(this.this$0).getState();
        if (a != com.navdy.hud.app.maps.here.HereNavController$State.NAVIGATING) {
            com.navdy.hud.app.maps.here.HereTrafficETATracker.access$500(this.this$0).i(new StringBuilder().append("no baseEta, not navigation:").append(a).toString());
            com.navdy.hud.app.maps.here.HereTrafficETATracker.access$402(this.this$0, -1L);
            com.navdy.hud.app.maps.here.HereTrafficETATracker.access$800(this.this$0).removeCallbacks(com.navdy.hud.app.maps.here.HereTrafficETATracker.access$700(this.this$0));
            com.navdy.hud.app.maps.here.HereTrafficETATracker.access$800(this.this$0).removeCallbacks(com.navdy.hud.app.maps.here.HereTrafficETATracker.access$900(this.this$0));
            com.navdy.hud.app.maps.here.HereTrafficETATracker.access$200(this.this$0);
        } else {
            java.util.Date a0 = com.navdy.hud.app.maps.here.HereTrafficETATracker.access$300(this.this$0).getEta(true, com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL);
            if (com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(a0)) {
                com.navdy.hud.app.maps.here.HereTrafficETATracker.access$402(this.this$0, a0.getTime());
                com.navdy.hud.app.maps.here.HereTrafficETATracker.access$500(this.this$0).v(new StringBuilder().append("got baseEta from eta:").append(com.navdy.hud.app.maps.here.HereTrafficETATracker.access$400(this.this$0)).toString());
            } else {
                java.util.Date a1 = com.navdy.hud.app.maps.here.HereTrafficETATracker.access$300(this.this$0).getTtaDate(true, com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL);
                if (com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(a1)) {
                    com.navdy.hud.app.maps.here.HereTrafficETATracker.access$402(this.this$0, a1.getTime());
                    com.navdy.hud.app.maps.here.HereTrafficETATracker.access$500(this.this$0).v(new StringBuilder().append("got baseEta from eta--tta:").append(com.navdy.hud.app.maps.here.HereTrafficETATracker.access$400(this.this$0)).toString());
                } else {
                    com.navdy.hud.app.maps.here.HereTrafficETATracker.access$402(this.this$0, -1L);
                    com.navdy.hud.app.maps.here.HereTrafficETATracker.access$200(this.this$0);
                    com.navdy.hud.app.maps.here.HereTrafficETATracker.access$500(this.this$0).i("no baseEta");
                }
            }
            com.navdy.hud.app.maps.here.HereTrafficETATracker.access$600(this.this$0);
        }
    }
}
