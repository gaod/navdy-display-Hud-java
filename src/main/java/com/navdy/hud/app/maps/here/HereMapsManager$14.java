package com.navdy.hud.app.maps.here;

class HereMapsManager$14 {
    final static int[] $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RoutingType;
    
    static {
        $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RoutingType = new int[com.navdy.service.library.events.preferences.NavigationPreferences.RoutingType.values().length];
        com.navdy.service.library.events.preferences.NavigationPreferences.RoutingType a0 = com.navdy.service.library.events.preferences.NavigationPreferences.RoutingType.ROUTING_FASTEST;
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RoutingType[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RoutingType[com.navdy.service.library.events.preferences.NavigationPreferences.RoutingType.ROUTING_SHORTEST.ordinal()] = 2;
        } catch(NoSuchFieldError ignored) {
        }
    }
}
