package com.navdy.hud.app.maps.here;

class HereMapController$2 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapController this$0;
    private final double val$zoomLevel;
    
    HereMapController$2(com.navdy.hud.app.maps.here.HereMapController a, double d) {
        super();
        this.this$0 = a;
        this.val$zoomLevel = d;
    }
    
    public void run() {
        try {
            this.this$0.map.setZoomLevel(this.val$zoomLevel);
        } catch(Throwable a) {
            com.navdy.hud.app.maps.here.HereMapController.access$100().e(a);
        }
    }
}
