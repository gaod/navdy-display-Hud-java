package com.navdy.hud.app.maps.here;
import com.navdy.hud.app.R;

final class HerePlacesManager$4 implements Runnable {
    final com.navdy.service.library.events.places.PlacesSearchRequest val$request;
    
    HerePlacesManager$4(com.navdy.service.library.events.places.PlacesSearchRequest a) {

        super();
        this.val$request = a;
    }
    
    public void run() {
        long j = android.os.SystemClock.elapsedRealtime();
        String s = this.val$request.searchQuery;
        int i = (this.val$request.searchArea == null) ? -1 : this.val$request.searchArea.intValue();
        int i0 = (this.val$request.maxResults == null) ? 15 : this.val$request.maxResults.intValue();
        label11: {
            label12: {
                if (i0 <= 0) {
                    break label12;
                }
                if (i0 <= 15) {
                    break label11;
                }
            }
            i0 = 15;
        }
        label0: {
            Throwable a = null;
            label1: {
                label9: {
                    label7: {
                        label5: {
                            label10: {
                                label3: try {
                                    com.navdy.hud.app.maps.here.HerePlacesManager.printSearchRequest(this.val$request);
                                    boolean b = com.navdy.hud.app.maps.here.HerePlacesManager.access$100().isInitialized();
                                    label8: {
                                        if (b) {
                                            break label8;
                                        }
                                        com.navdy.hud.app.maps.here.HerePlacesManager.access$000().i("[search] engine not ready");
                                        com.navdy.hud.app.maps.here.HerePlacesManager.access$300(s, com.navdy.service.library.events.RequestStatus.REQUEST_NOT_READY, com.navdy.hud.app.maps.here.HerePlacesManager.access$200().getString(R.string.map_engine_not_ready));
                                        break label9;
                                    }
                                    boolean b0 = android.text.TextUtils.isEmpty((CharSequence)s);
                                    label6: {
                                        if (!b0) {
                                            break label6;
                                        }
                                        com.navdy.hud.app.maps.here.HerePlacesManager.access$000().i("[search] invalid search query");
                                        com.navdy.hud.app.maps.here.HerePlacesManager.access$300(s, com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_REQUEST, com.navdy.hud.app.maps.here.HerePlacesManager.access$200().getString(R.string.empty_search_query));
                                        break label7;
                                    }
                                    com.navdy.hud.app.maps.here.HereLocationFixManager a0 = com.navdy.hud.app.maps.here.HerePlacesManager.access$100().getLocationFixManager();
                                    label4: {
                                        if (a0 != null) {
                                            break label4;
                                        }
                                        com.navdy.hud.app.maps.here.HerePlacesManager.access$000().i("[search] engine not ready, location fix manager n/a");
                                        com.navdy.hud.app.maps.here.HerePlacesManager.access$300(s, com.navdy.service.library.events.RequestStatus.REQUEST_NOT_READY, com.navdy.hud.app.maps.here.HerePlacesManager.access$200().getString(R.string.map_engine_not_ready));
                                        break label5;
                                    }
                                    com.here.android.mpa.common.GeoCoordinate a1 = a0.getLastGeoCoordinate();
                                    label2: {
                                        if (a1 == null) {
                                            break label2;
                                        }
                                        com.navdy.hud.app.maps.here.HerePlacesManager.searchPlaces(a1, s, i, i0, (com.navdy.hud.app.maps.here.HerePlacesManager$PlacesSearchCallback)new com.navdy.hud.app.maps.here.HerePlacesManager$4$1(this, s, a1, i0, i, j));
                                        break label3;
                                    }
                                    com.navdy.hud.app.maps.here.HerePlacesManager.access$000().i("start location n/a");
                                    com.navdy.hud.app.maps.here.HerePlacesManager.access$300(s, com.navdy.service.library.events.RequestStatus.REQUEST_NO_LOCATION_SERVICE, com.navdy.hud.app.maps.here.HerePlacesManager.access$200().getString(R.string.no_current_position));
                                    break label10;
                                } catch(Throwable a2) {
                                    a = a2;
                                    break label1;
                                }
                                if (!com.navdy.hud.app.maps.here.HerePlacesManager.access$000().isLoggable(2)) {
                                    break label0;
                                }
                                long j0 = android.os.SystemClock.elapsedRealtime();
                                com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("[search] handleSearchRequest- time-1 =").append(j0 - j).toString());
                                break label0;
                            }
                            if (!com.navdy.hud.app.maps.here.HerePlacesManager.access$000().isLoggable(2)) {
                                break label0;
                            }
                            long j1 = android.os.SystemClock.elapsedRealtime();
                            com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("[search] handleSearchRequest- time-1 =").append(j1 - j).toString());
                            break label0;
                        }
                        if (!com.navdy.hud.app.maps.here.HerePlacesManager.access$000().isLoggable(2)) {
                            break label0;
                        }
                        long j2 = android.os.SystemClock.elapsedRealtime();
                        com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("[search] handleSearchRequest- time-1 =").append(j2 - j).toString());
                        break label0;
                    }
                    if (!com.navdy.hud.app.maps.here.HerePlacesManager.access$000().isLoggable(2)) {
                        break label0;
                    }
                    long j3 = android.os.SystemClock.elapsedRealtime();
                    com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("[search] handleSearchRequest- time-1 =").append(j3 - j).toString());
                    break label0;
                }
                if (!com.navdy.hud.app.maps.here.HerePlacesManager.access$000().isLoggable(2)) {
                    break label0;
                }
                long j4 = android.os.SystemClock.elapsedRealtime();
                com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("[search] handleSearchRequest- time-1 =").append(j4 - j).toString());
                break label0;
            }
            try {
                com.navdy.hud.app.maps.here.HerePlacesManager.access$000().e(a);
                com.navdy.hud.app.maps.here.HerePlacesManager.access$300(s, com.navdy.service.library.events.RequestStatus.REQUEST_UNKNOWN_ERROR, com.navdy.hud.app.maps.here.HerePlacesManager.access$200().getString(R.string.search_error));
            } catch(Throwable a3) {
                if (com.navdy.hud.app.maps.here.HerePlacesManager.access$000().isLoggable(2)) {
                    long j5 = android.os.SystemClock.elapsedRealtime();
                    com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("[search] handleSearchRequest- time-1 =").append(j5 - j).toString());
                }
                throw a3;
            }
            if (com.navdy.hud.app.maps.here.HerePlacesManager.access$000().isLoggable(2)) {
                long j6 = android.os.SystemClock.elapsedRealtime();
                com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("[search] handleSearchRequest- time-1 =").append(j6 - j).toString());
            }
        }
    }
}
