package com.navdy.hud.app.maps.notification;
import com.navdy.hud.app.R;
import com.squareup.otto.Subscribe;

public class RouteCalculationEventHandler {
    final private static int MIN_ROUTE_CALC_DISPLAY_TIME;
    final private static int ROUTE_CALC_DELAY_TIME;
    final private static com.navdy.service.library.log.Logger logger;
    private com.navdy.hud.app.ui.framework.INotificationAnimationListener animationListener;
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent currentRouteCalcEvent;
    private android.os.Handler handler;
    private long routeCalcStartTime;
    private com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent showRoutesCalcEvent;
    private Runnable startRouteRunnable;
    final private String startTrip;
    
    static {
        logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.notification.RouteCalculationEventHandler.class);
        MIN_ROUTE_CALC_DISPLAY_TIME = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(2L);
        ROUTE_CALC_DELAY_TIME = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(1L);
    }
    
    public RouteCalculationEventHandler(com.squareup.otto.Bus a) {
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.animationListener = (com.navdy.hud.app.ui.framework.INotificationAnimationListener)new com.navdy.hud.app.maps.notification.RouteCalculationEventHandler$1(this);
        this.startRouteRunnable = (Runnable)new com.navdy.hud.app.maps.notification.RouteCalculationEventHandler$2(this);
        logger.v("ctor");
        android.content.res.Resources a0 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        this.bus = a;
        this.startTrip = a0.getString(R.string.start_trip);
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().addNotificationAnimationListener(this.animationListener);
        a.register(this);
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return logger;
    }
    
    static void access$100(com.navdy.hud.app.maps.notification.RouteCalculationEventHandler a) {
        a.startRoute();
    }
    
    private void dismissNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification("navdy#route#calc#notif");
    }
    
    private void navigateToRoute(com.navdy.service.library.events.navigation.NavigationRouteResult a) {
        com.navdy.service.library.events.navigation.NavigationSessionRequest a0 = new com.navdy.service.library.events.navigation.NavigationSessionRequest.Builder().newState(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED).label(this.currentRouteCalcEvent.response.label).routeId(a.routeId).simulationSpeed(Integer.valueOf(0)).originDisplay(Boolean.valueOf(true)).build();
        this.bus.post(a0);
        this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a0));
    }
    
    private void showMoreRoutes() {
        logger.v("showMoreRoutes");
        if (this.showRoutesCalcEvent != null) {
            com.navdy.hud.app.framework.notifications.NotificationManager a = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
            com.navdy.hud.app.maps.notification.RouteCalculationNotification a0 = (com.navdy.hud.app.maps.notification.RouteCalculationNotification)a.getNotification("navdy#route#calc#notif");
            if (a0 == null) {
                a0 = new com.navdy.hud.app.maps.notification.RouteCalculationNotification(this, this.bus);
            }
            a0.showRoutes(this.showRoutesCalcEvent);
            this.showRoutesCalcEvent = null;
            logger.v("showMoreRoutes: posted notif");
            a.addNotification((com.navdy.hud.app.framework.notifications.INotification)a0);
        }
    }
    
    private void startRoute() {
        com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent a = this.currentRouteCalcEvent;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (this.currentRouteCalcEvent.request == null) {
                        break label1;
                    }
                    if (this.currentRouteCalcEvent.response == null) {
                        break label1;
                    }
                    if (this.currentRouteCalcEvent.response.results != null) {
                        break label0;
                    }
                }
                logger.v("invalid state");
                this.dismissNotification();
                break label2;
            }
            logger.v("put user on route");
            this.navigateToRoute((com.navdy.service.library.events.navigation.NavigationRouteResult)this.currentRouteCalcEvent.response.results.get(0));
            int i = this.currentRouteCalcEvent.response.results.size();
            if (i <= 1) {
                logger.v(new StringBuilder().append("more route NOT available:").append(i).toString());
                this.currentRouteCalcEvent = null;
                this.dismissNotification();
            } else {
                logger.v(new StringBuilder().append("more route available:").append(i).toString());
                this.showRoutesCalcEvent = this.currentRouteCalcEvent;
                this.showMoreRoutes();
            }
        }
    }
    
    public void clearCurrentRouteCalcEvent() {
        this.currentRouteCalcEvent = null;
        this.handler.removeCallbacks(this.startRouteRunnable);
    }
    
    public com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent getCurrentRouteCalcEvent() {
        return this.currentRouteCalcEvent;
    }
    
    public boolean hasMoreRoutes() {
        return this.showRoutesCalcEvent != null;
    }
    
    @Subscribe
    public void onFirstManeuver(com.navdy.hud.app.maps.MapEvents$ManeuverEvent a) {
        logger.v("onFirstManeuver");
    }
    
    @Subscribe
    public void onNavigationModeChanged(com.navdy.hud.app.maps.MapEvents$NavigationModeChange a) {
        boolean b = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isSwitchingToNewRoute();
        logger.v(new StringBuilder().append("onNavigationModeChanged event[").append(a.navigationMode).append("] switching=").append(b).toString());
        if (a.navigationMode == com.navdy.hud.app.maps.NavigationMode.MAP && !b) {
            this.showRoutesCalcEvent = null;
            this.dismissNotification();
        }
    }
    
    @Subscribe
    public void onRouteCalculation(com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent a) {
        logger.v(new StringBuilder().append("onRouteCalculation event[").append(a.state).append("]").toString());
        com.navdy.hud.app.framework.notifications.NotificationManager a0 = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        switch(com.navdy.hud.app.maps.notification.RouteCalculationEventHandler$3.$SwitchMap$com$navdy$hud$app$maps$MapEvents$RouteCalculationState[a.state.ordinal()]) {
            case 5: {
                logger.v(new StringBuilder().append("abort-nav routeCalc event[ABORT_NAVIGATION] ").append(a.pendingNavigationRequestId).toString());
                com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent a1 = this.currentRouteCalcEvent;
                label4: {
                    label5: {
                        if (a1 == null) {
                            break label5;
                        }
                        if (this.currentRouteCalcEvent.request != null) {
                            break label4;
                        }
                    }
                    logger.v("abort-nav:no current route calc event");
                    break;
                }
                if (android.text.TextUtils.equals((CharSequence)this.currentRouteCalcEvent.request.requestId, (CharSequence)a.pendingNavigationRequestId)) {
                    logger.v("abort-nav: found current route id, cancel");
                    this.currentRouteCalcEvent = null;
                    if (a.abortOriginDisplay) {
                        this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.navigation.NavigationRouteCancelRequest(a.pendingNavigationRequestId)));
                        this.dismissNotification();
                        break;
                    } else {
                        logger.v(new StringBuilder().append("abort-nav: sent navrouteresponse cancel [").append(a.pendingNavigationRequestId).append("]").toString());
                        com.navdy.service.library.events.navigation.NavigationRouteResponse a2 = new com.navdy.service.library.events.navigation.NavigationRouteResponse.Builder().requestId(a.pendingNavigationRequestId).status(com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED).destination(new com.navdy.service.library.events.location.Coordinate.Builder().latitude(Double.valueOf(0.0)).longitude(Double.valueOf(0.0)).build()).build();
                        this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a2));
                        com.navdy.hud.app.maps.here.HereRouteManager.clearActiveRouteCalc(a.pendingNavigationRequestId);
                        this.dismissNotification();
                        break;
                    }
                } else {
                    logger.v(new StringBuilder().append("abort-nav:does not match current request:").append(this.currentRouteCalcEvent.request.requestId).toString());
                    break;
                }
            }
            case 3: {
                if (a.stopped) {
                    break;
                }
                if (this.currentRouteCalcEvent == null) {
                    break;
                }
                if (this.currentRouteCalcEvent.response == null) {
                    break;
                }
                a.stopped = true;
                if (this.currentRouteCalcEvent.response.status != com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
                    logger.e(new StringBuilder().append("routing calculation not successful:").append(this.currentRouteCalcEvent.response.status).toString());
                    if (this.currentRouteCalcEvent.response.status != com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED) {
                        logger.v("show error");
                        com.navdy.hud.app.maps.notification.RouteCalculationNotification a3 = (com.navdy.hud.app.maps.notification.RouteCalculationNotification)a0.getNotification("navdy#route#calc#notif");
                        if (a3 == null) {
                            a3 = new com.navdy.hud.app.maps.notification.RouteCalculationNotification(this, this.bus);
                        }
                        a3.showError();
                        a0.addNotification((com.navdy.hud.app.framework.notifications.INotification)a3);
                        break;
                    } else {
                        logger.v("request cancelled");
                        this.currentRouteCalcEvent = null;
                        this.dismissNotification();
                        break;
                    }
                } else {
                    com.navdy.hud.app.maps.notification.RouteCalculationNotification a4 = (com.navdy.hud.app.maps.notification.RouteCalculationNotification)com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getNotification("navdy#route#calc#notif");
                    if (a4 != null) {
                        logger.v("hideStartTrip");
                        a4.hideStartTrip();
                    }
                    long j = android.os.SystemClock.elapsedRealtime() - this.routeCalcStartTime;
                    if (j >= (long)MIN_ROUTE_CALC_DISPLAY_TIME) {
                        logger.v(new StringBuilder().append("route calc took[").append(j).append("]").toString());
                        this.startRoute();
                        break;
                    } else {
                        logger.v(new StringBuilder().append("route calc too fast[").append(j).append("] wait").toString());
                        this.handler.postDelayed(this.startRouteRunnable, (long)ROUTE_CALC_DELAY_TIME);
                        break;
                    }
                }
            }
            case 1: case 2: {
                String s = null;
                String s0 = null;
                String s1 = null;
                com.navdy.hud.app.framework.destinations.Destination a5 = null;
                com.navdy.hud.app.screen.BaseScreen a6 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getCurrentScreen();
                if (a6 != null && a6.getScreen() == com.navdy.service.library.events.ui.Screen.SCREEN_DESTINATION_PICKER) {
                    logger.v("onRouteCalculation: destination picker on");
                    a0.enableNotifications(true);
                }
                this.handler.removeCallbacks(this.startRouteRunnable);
                this.showRoutesCalcEvent = null;
                this.currentRouteCalcEvent = a;
                this.routeCalcStartTime = android.os.SystemClock.elapsedRealtime();
                com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().clearSuggestedDestination();
                com.navdy.hud.app.ui.component.homescreen.HomeScreenView a7 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView();
                if (a7 != null) {
                    a7.setShowCollapsedNotification(false);
                }
                com.navdy.hud.app.maps.notification.RouteCalculationNotification a8 = (com.navdy.hud.app.maps.notification.RouteCalculationNotification)a0.getNotification("navdy#route#calc#notif");
                if (a8 == null) {
                    a8 = new com.navdy.hud.app.maps.notification.RouteCalculationNotification(this, this.bus);
                }
                if (a.state != com.navdy.hud.app.maps.MapEvents$RouteCalculationState.STARTED) {
                    com.navdy.hud.app.framework.destinations.Destination a9 = a.lookupDestination;
                    label2: {
                        label3: {
                            if (a9 == null) {
                                break label3;
                            }
                            if (android.text.TextUtils.isEmpty((CharSequence)a.lookupDestination.destinationTitle)) {
                                break label3;
                            }
                            s = a.lookupDestination.destinationTitle;
                            s0 = a.lookupDestination.fullAddress;
                            s1 = s;
                            break label2;
                        }
                        com.navdy.hud.app.framework.destinations.Destination a10 = a.lookupDestination;
                        label0: {
                            label1: {
                                if (a10 == null) {
                                    break label1;
                                }
                                if (!android.text.TextUtils.isEmpty((CharSequence)a.lookupDestination.fullAddress)) {
                                    break label0;
                                }
                            }
                            s1 = "";
                            s0 = null;
                            s = "";
                            break label2;
                        }
                        s = a.lookupDestination.fullAddress;
                        s1 = s;
                        s0 = null;
                    }
                    a5 = a.lookupDestination;
                } else {
                    if (android.text.TextUtils.isEmpty((CharSequence)a.request.label)) {
                        if (android.text.TextUtils.isEmpty((CharSequence)a.request.streetAddress)) {
                            s1 = "";
                            s0 = null;
                            s = "";
                        } else {
                            s = a.request.streetAddress;
                            s1 = s;
                            s0 = null;
                        }
                    } else {
                        s = a.request.label;
                        s0 = a.request.streetAddress;
                        s1 = s;
                    }
                    a5 = com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().transformToInternalDestination(a.request.requestDestination);
                }
                com.navdy.hud.app.ui.component.vlist.VerticalList.Model a11 = com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.getPlaceModel(a5, 0, (String)null, (String)null);
                if (a11.type != com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType.TWO_ICONS) {
                    if (a5 != null) {
                        if (a5 != null && a5.destinationType != null && a5.destinationType == com.navdy.hud.app.framework.destinations.Destination$DestinationType.DEFAULT && a5.favoriteDestinationType != null && a5.favoriteDestinationType == com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.FAVORITE_NONE) {
                            a11.icon = R.drawable.icon_mm_places_2;
                            a11.iconSelectedColor = com.navdy.hud.app.maps.notification.RouteCalculationNotification.notifBkColor;
                        }
                    } else {
                        a11.iconSelectedColor = com.navdy.hud.app.maps.notification.RouteCalculationNotification.notifBkColor;
                    }
                } else {
                    a11.iconSelectedColor = 0;
                }
                int i = a11.icon;
                int i0 = a11.iconSelectedColor;
                String s2 = null;
                String s3 = null;
                if (a5 != null) {
                    s3 = com.navdy.hud.app.framework.destinations.DestinationsManager.getInitials(a5.destinationTitle, a5.favoriteDestinationType);
                    if (a5.destinationIcon != 0 && a5.destinationIconBkColor != 0) {
                        i = a5.destinationIcon;
                        i0 = a5.destinationIconBkColor;
                    }
                    s2 = a5.distanceStr;
                }
                android.content.res.Resources a12 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
                int i1 = com.navdy.hud.app.maps.notification.RouteCalculationEventHandler$3.$SwitchMap$com$here$android$mpa$routing$RouteOptions$Type[a.routeOptions.getRouteType().ordinal()];
                String s4 = null;
                switch(i1) {
                    case 2: {
                        Object[] a13 = new Object[2];
                        a13[0] = com.navdy.hud.app.maps.notification.RouteCalculationNotification.fastestRoute;
                        a13[1] = s1;
                        s4 = a12.getString(R.string.finding_route, a13);
                        break;
                    }
                    case 1: {
                        Object[] a14 = new Object[2];
                        a14[0] = com.navdy.hud.app.maps.notification.RouteCalculationNotification.shortestRoute;
                        a14[1] = s1;
                        s4 = a12.getString(R.string.finding_route, a14);
                        break;
                    }
                }
                com.navdy.hud.app.framework.destinations.Destination a15 = a.lookupDestination;
                com.navdy.service.library.events.destination.Destination a16 = null;
                if (a15 != null) {
                    a16 = com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().transformToProtoDestination(a.lookupDestination);
                }
                com.navdy.service.library.events.navigation.NavigationRouteRequest a17 = a.request;
                String s5 = null;
                if (a17 != null) {
                    s5 = a.request.requestId;
                }
                int i2 = (i0 != 0) ? 0 : (a11.iconFluctuatorColor == -1) ? 0 : a11.iconFluctuatorColor;
                a8.showRouteSearch(this.startTrip, s1, i, i0, i2, s3, s4, a16, s5, s, s0, s2);
                a0.addNotification((com.navdy.hud.app.framework.notifications.INotification)a8);
                break;
            }
        }
    }
}
