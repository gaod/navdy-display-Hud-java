package com.navdy.hud.app.maps.here;

public class HereRouteCache extends android.util.LruCache {
    final public static int MAX_CACHE_ROUTE_COUNT = 20;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.hud.app.maps.here.HereRouteCache sSingleton;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereRouteCache.class);
        sSingleton = new com.navdy.hud.app.maps.here.HereRouteCache();
    }
    
    private HereRouteCache() {
        super(20);
    }
    
    public static com.navdy.hud.app.maps.here.HereRouteCache getInstance() {
        return sSingleton;
    }
    
    public void addRoute(String s, com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a) {
        super.put(s, a);
    }
    
    protected void entryRemoved(boolean b, String s, com.here.android.mpa.routing.Route a, com.here.android.mpa.routing.Route a0) {
        if (b && a != null) {
            sLogger.v(new StringBuilder().append("route removed from cache:").append(s).toString());
        }
    }
    
    public com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo getRoute(String s) {
        com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a = (s == null) ? null : (com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo)super.get(s);
        return a;
    }
    
    public com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo removeRoute(String s) {
        return (com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo)super.remove(s);
    }
    
    protected int sizeOf(String s, com.here.android.mpa.routing.Route a) {
        return 1;
    }
}
