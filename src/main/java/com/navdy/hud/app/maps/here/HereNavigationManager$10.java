package com.navdy.hud.app.maps.here;

class HereNavigationManager$10 implements Runnable {
    final com.navdy.hud.app.maps.here.HereNavigationManager this$0;
    
    HereNavigationManager$10(com.navdy.hud.app.maps.here.HereNavigationManager a) {

        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("limitbandwidth: turn off HERE traffic avoidance mode");
        com.navdy.hud.app.maps.here.HereNavigationManager.access$700(this.this$0).setTrafficAvoidanceMode(com.here.android.mpa.guidance.NavigationManager.TrafficAvoidanceMode.DISABLE);
    }
}
