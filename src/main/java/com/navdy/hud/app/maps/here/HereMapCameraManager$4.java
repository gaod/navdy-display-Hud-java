package com.navdy.hud.app.maps.here;

class HereMapCameraManager$4 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapCameraManager this$0;
    final com.navdy.hud.app.maps.MapEvents$DialMapZoom val$event;
    
    HereMapCameraManager$4(com.navdy.hud.app.maps.here.HereMapCameraManager a, com.navdy.hud.app.maps.MapEvents$DialMapZoom a0) {
        super();
        this.this$0 = a;
        this.val$event = a0;
    }
    
    public void run() {
        try {
            switch(com.navdy.hud.app.maps.here.HereMapCameraManager$9.$SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State[com.navdy.hud.app.maps.here.HereMapCameraManager.access$300(this.this$0).getState().ordinal()]) {
                case 1: case 2: {
                    com.here.android.mpa.common.GeoCoordinate a = (com.navdy.hud.app.maps.here.HereMapCameraManager.access$1300(this.this$0) != null) ? com.navdy.hud.app.maps.here.HereMapCameraManager.access$1300(this.this$0).getCoordinate() : com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                    if (a != null) {
                        double d = 0.0;
                        boolean b = false;
                        boolean b0 = com.navdy.hud.app.maps.here.HereMapCameraManager.access$1400(this.this$0);
                        label8: {
                            boolean b1 = false;
                            label9: {
                                if (b0) {
                                    break label9;
                                }
                                if (com.navdy.hud.app.maps.here.HereMapCameraManager.access$1500(this.this$0)) {
                                    break label9;
                                }
                                if (com.navdy.hud.app.maps.here.HereMapCameraManager.access$1600(this.this$0)) {
                                    break label9;
                                }
                                if (!com.navdy.hud.app.maps.here.HereMapCameraManager.access$600(this.this$0)) {
                                    break label8;
                                }
                                if (this.val$event.type != com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type.OUT) {
                                    break label8;
                                }
                            }
                            if (!com.navdy.hud.app.maps.here.HereMapCameraManager.access$400().isLoggable(2)) {
                                break;
                            }
                            com.navdy.service.library.log.Logger a0 = com.navdy.hud.app.maps.here.HereMapCameraManager.access$400();
                            StringBuilder a1 = new StringBuilder().append("onDialZoom no-op, animationOn: ").append(com.navdy.hud.app.maps.here.HereMapCameraManager.access$1400(this.this$0)).append(" goBackfromOverviewOn:").append(com.navdy.hud.app.maps.here.HereMapCameraManager.access$1500(this.this$0)).append(" goToOverviewOn:").append(com.navdy.hud.app.maps.here.HereMapCameraManager.access$1600(this.this$0)).append(" overviewMap on-out:");
                            boolean b2 = com.navdy.hud.app.maps.here.HereMapCameraManager.access$600(this.this$0);
                            label7: {
                                label5: {
                                    label6: {
                                        if (!b2) {
                                            break label6;
                                        }
                                        if (this.val$event.type == com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type.OUT) {
                                            break label5;
                                        }
                                    }
                                    b1 = false;
                                    break label7;
                                }
                                b1 = true;
                            }
                            a0.v(a1.append(b1).toString());
                            break;
                        }
                        double d0 = com.navdy.hud.app.maps.here.HereMapCameraManager.access$300(this.this$0).getZoomLevel();
                        com.navdy.hud.app.maps.here.HereMapCameraManager.access$202(this.this$0, true);
                        com.navdy.hud.app.maps.here.HereMapCameraManager.access$100(this.this$0).removeCallbacks(com.navdy.hud.app.maps.here.HereMapCameraManager.access$1700(this.this$0));
                        int i = com.navdy.hud.app.maps.here.HereMapCameraManager$9.$SwitchMap$com$navdy$hud$app$maps$MapEvents$DialMapZoom$Type[this.val$event.type.ordinal()];
                        label4: {
                            boolean b3 = false;
                            switch(i) {
                                case 2: {
                                    d = -0.25;
                                    break label4;
                                }
                                case 1: {
                                    b3 = com.navdy.hud.app.maps.here.HereMapCameraManager.access$600(this.this$0);
                                    break;
                                }
                                default: {
                                    d = 0.0;
                                    break label4;
                                }
                            }
                            label3: {
                                if (!b3) {
                                    break label3;
                                }
                                com.navdy.hud.app.maps.here.HereMapCameraManager.access$1602(this.this$0, false);
                                if (com.navdy.hud.app.maps.here.HereMapCameraManager.access$1500(this.this$0)) {
                                    break;
                                }
                                com.navdy.hud.app.maps.here.HereMapCameraManager.access$400().v("goBackfromOverview:on");
                                com.navdy.hud.app.maps.here.HereMapCameraManager.access$1502(this.this$0, true);
                                com.navdy.hud.app.maps.here.HereMapCameraManager.access$100(this.this$0).post((Runnable)new com.navdy.hud.app.maps.here.HereMapCameraManager$4$1(this));
                                break;
                            }
                            if (com.navdy.hud.app.maps.here.HereMapCameraManager.access$1500(this.this$0)) {
                                break;
                            }
                            d = 0.25;
                        }
                        double d1 = d0 + d;
                        if (d0 < 16.5 && d1 >= 16.5) {
                            d1 = 16.5;
                        }
                        int i0 = (d1 < 16.5) ? -1 : (d1 == 16.5) ? 0 : 1;
                        label2: {
                            label0: {
                                label1: {
                                    if (i0 > 0) {
                                        break label1;
                                    }
                                    if (d1 >= 12.0) {
                                        break label0;
                                    }
                                }
                                b = false;
                                break label2;
                            }
                            b = true;
                        }
                        boolean b4 = d0 + d < 12.0;
                        if (b) {
                            long j = android.os.SystemClock.elapsedRealtime();
                            if (com.navdy.hud.app.maps.here.HereMapCameraManager.access$1900(this.this$0)) {
                                com.navdy.hud.app.maps.here.HereMapCameraManager.access$2000(this.this$0).setZoom(d1);
                            } else {
                                com.navdy.hud.app.maps.here.HereMapCameraManager.access$300(this.this$0).setCenter(a, com.here.android.mpa.mapping.Map.Animation.NONE, d1, -1f, -1f);
                            }
                            com.navdy.hud.app.maps.here.HereMapCameraManager.access$400().v(new StringBuilder().append("zoomLevel:").append(d1).append(" time:").append(android.os.SystemClock.elapsedRealtime() - j).toString());
                        } else if (b4) {
                            com.navdy.hud.app.maps.here.HereMapCameraManager.access$1502(this.this$0, false);
                            com.navdy.hud.app.maps.here.HereMapCameraManager.access$1602(this.this$0, true);
                            com.navdy.hud.app.maps.here.HereMapCameraManager.access$400().v("goToOverviewOn:on");
                            com.navdy.hud.app.maps.here.HereMapCameraManager.access$100(this.this$0).post((Runnable)new com.navdy.hud.app.maps.here.HereMapCameraManager$4$2(this, a));
                            com.navdy.hud.app.maps.here.HereMapCameraManager.access$400().v("zoomLevel: overview");
                        }
                        com.navdy.hud.app.maps.here.HereMapCameraManager.access$100(this.this$0).postDelayed(com.navdy.hud.app.maps.here.HereMapCameraManager.access$1700(this.this$0), 10000L);
                        break;
                    } else {
                        com.navdy.hud.app.maps.here.HereMapCameraManager.access$400().i("no location");
                        break;
                    }
                }
            }
        } catch(Throwable a2) {
            com.navdy.hud.app.maps.here.HereMapCameraManager.access$400().e("onDialZoom", a2);
        }
    }
}
