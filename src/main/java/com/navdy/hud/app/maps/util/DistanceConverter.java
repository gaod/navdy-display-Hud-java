package com.navdy.hud.app.maps.util;

public class DistanceConverter {
    public DistanceConverter() {
    }
    
    public static void convertToDistance(com.navdy.hud.app.manager.SpeedManager.SpeedUnit a, float f, com.navdy.hud.app.maps.util.DistanceConverter$Distance a0) {
        a0.clear();
        if (com.navdy.hud.app.maps.util.DistanceConverter$1.$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[a.ordinal()] != 0) {
            if (f >= 400f) {
                a0.value = f / 1000f;
                a0.value = (float)com.navdy.hud.app.maps.here.HereMapUtil.roundToN((double)a0.value, 10);
                a0.unit = com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_KMS;
            } else {
                a0.value = f;
                a0.unit = com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_METERS;
            }
        } else if (f >= 160.934f) {
            a0.value = f / 1609.34f;
            a0.value = (float)com.navdy.hud.app.maps.here.HereMapUtil.roundToN((double)a0.value, 10);
            a0.unit = com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_MILES;
        } else {
            a0.value = (float)(int)(3.28084f * f);
            a0.unit = com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_FEET;
        }
    }
    
    public static float convertToMeters(float f, com.navdy.service.library.events.navigation.DistanceUnit a) {
        switch(com.navdy.hud.app.maps.util.DistanceConverter$1.$SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[a.ordinal()]) {
            case 4: {
                f = f * 1609.34f;
                break;
            }
            case 3: {
                f = f * 1000f;
                break;
            }
            case 2: {
                f = f / 3.28084f;
                break;
            }
            default: {
                break;
            }
            case 1: {
            }
        }
        return f;
    }
}
