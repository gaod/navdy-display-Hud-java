package com.navdy.hud.app.maps.here;

class HereMapController$9 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapController this$0;
    private final android.graphics.PointF val$pointF;
    
    HereMapController$9(com.navdy.hud.app.maps.here.HereMapController a, android.graphics.PointF a0) {
        super();
        this.this$0 = a;
        this.val$pointF = a0;
    }
    
    public void run() {
        this.this$0.map.setTransformCenter(this.val$pointF);
    }
}
