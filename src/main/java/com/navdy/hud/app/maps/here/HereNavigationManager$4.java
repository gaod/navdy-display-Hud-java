package com.navdy.hud.app.maps.here;

import com.here.android.mpa.routing.Route;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.events.navigation.NavigationSessionState;

class HereNavigationManager$4 implements Runnable {
    final com.navdy.hud.app.maps.here.HereNavigationManager this$0;
    final boolean val$sendRouteResult;
    final NavigationSessionState val$sessionState;
    
    HereNavigationManager$4(HereNavigationManager a, boolean b, NavigationSessionState a0) {

        super();
        this.this$0 = a;
        this.val$sendRouteResult = b;
        this.val$sessionState = a0;
    }
    
    public void run() {
        NavigationRouteRequest a = HereNavigationManager.access$100(this.this$0).navigationRouteRequest;
        String s = null;
        String s0 = null;
        NavigationRouteResult a0 = null;
        if (a != null) {
            s = a.label;
            s0 = HereNavigationManager.access$100(this.this$0).routeId;
            a0 = null;
            if (this.val$sendRouteResult) {
                HereRouteCache$RouteInfo a1 = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(s0);
                a0 = null;
                if (a1 != null) {
                    NavigationRouteResult a2 = a1.routeResult;
                    int i = a2.duration;
                    int i0 = a2.duration_traffic;
                    java.util.Date a3 = HereNavigationManager.access$700(this.this$0).getEta(true, Route.TrafficPenaltyMode.DISABLED);
                    java.util.Date a4 = HereNavigationManager.access$700(this.this$0).getEta(true, Route.TrafficPenaltyMode.OPTIMAL);
                    java.util.Date a7 = HereNavigationManager.access$700(this.this$0).getEta(true, Route.TrafficPenaltyMode.AVOID_LONG_TERM_CLOSURES);

                    long j = System.currentTimeMillis();
                    if (HereMapUtil.isValidEtaDate(a3)) {
                        i = (int)(a3.getTime() - j) / 1000;
                    }
                    if (HereMapUtil.isValidEtaDate(a4)) {
                        i0 = (int)(a4.getTime() - j) / 1000;
                    }

                    if (HereMapUtil.isValidEtaDate(a4)) {
                        i0 = (int)(a7.getTime() - j) / 1000;
                    }

                    int i1 = (int)HereNavigationManager.access$700(this.this$0).getDestinationDistance();
                    a0 = new NavigationRouteResult.Builder(a2).duration(i).duration_traffic(i0).length(i1).build();
                    HereNavigationManager.sLogger.v("NavigationSessionStatusEvent new data duration[" + i + "] traffic[" + i0 + "] length[" + i1 + "]");
                }
            }
        }
        if (s0 == null && this.val$sessionState == NavigationSessionState.NAV_SESSION_STOPPED) {
            NavigationRouteRequest a5 = HereNavigationManager.access$100(this.this$0).getLastNavigationRequest();
            if (a5 != null) {
                s = a5.label;
                s0 = HereNavigationManager.access$100(this.this$0).getLastRouteId();
            }
        }
        com.navdy.service.library.events.navigation.NavigationSessionStatusEvent a6 = new com.navdy.service.library.events.navigation.NavigationSessionStatusEvent(this.val$sessionState, s, s0, a0, HereNavigationManager.access$100(this.this$0).destinationIdentifier);
        HereNavigationManager.access$800().sendEventToClient(a6);
        HereNavigationManager.sLogger.v("posted NavigationSessionStatusEvent state[" + a6.sessionState + "] label[" + a6.label + "] routeId[" + a6.routeId + "]" + "] result[" + ((a0 != null) ? a0.via : "null") + "]");
    }
}
