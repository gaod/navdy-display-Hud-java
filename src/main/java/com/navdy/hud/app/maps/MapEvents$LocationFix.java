package com.navdy.hud.app.maps;

public class MapEvents$LocationFix {
    public boolean locationAvailable;
    public boolean usingLocalGpsLocation;
    public boolean usingPhoneLocation;
    
    public MapEvents$LocationFix(boolean locationAvailable, boolean usingPhoneLocation, boolean usingLocalGpsLocation) {
        this.locationAvailable = locationAvailable;
        this.usingPhoneLocation = usingPhoneLocation;
        this.usingLocalGpsLocation = usingLocalGpsLocation;
    }
}
