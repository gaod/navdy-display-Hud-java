package com.navdy.hud.app.maps.here;

import com.here.android.mpa.routing.Route;

import org.jetbrains.annotations.NonNls;

class NavdyTrafficRerouteManager$4$1$1 implements Runnable {
    final com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager$4$1 this$2;
    final java.util.ArrayList val$outgoingResults;
    
    NavdyTrafficRerouteManager$4$1$1(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager$4$1 a, java.util.ArrayList a0) {

        super();
        this.this$2 = a;
        this.val$outgoingResults = a0;
    }
    
    public void run() {
        label4: try {
            if (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$800(this.this$2.this$1.this$0)) {
                label8: {
                    label9: {
                        if (this.val$outgoingResults == null) {
                            break label9;
                        }
                        if (this.val$outgoingResults.size() != 0) {
                            break label8;
                        }
                    }
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("success, but no result");
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$600(this.this$2.this$1.this$0);
                    break label4;
                }
                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("success");
                com.navdy.service.library.events.navigation.NavigationRouteResult a0 = (com.navdy.service.library.events.navigation.NavigationRouteResult)this.val$outgoingResults.get(0);
                com.navdy.hud.app.maps.here.HereRouteCache a1 = com.navdy.hud.app.maps.here.HereRouteCache.getInstance();
                com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a2 = a1.getRoute(a0.routeId);
                if (a2 != null) {
                    a1.removeRoute(a0.routeId);
                    if (a0.duration_traffic != 0) {
                        com.here.android.mpa.routing.Route a3 = com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$2.this$1.this$0).getCurrentRoute();
                        if (a3 != null) {
                            Object a4 = null;
                            Object a5 = null;
                            int i = 0;
                            int i0 = 0;

                            com.here.android.mpa.routing.RouteTta a6 = a2.route.getTta(Route.TrafficPenaltyMode.OPTIMAL, 268435455);
                            com.here.android.mpa.routing.RouteTta a7 = a2.route.getTta(Route.TrafficPenaltyMode.DISABLED, 268435455);
                            com.here.android.mpa.routing.RouteTta a16 = a2.route.getTta(Route.TrafficPenaltyMode.AVOID_LONG_TERM_CLOSURES, 268435455);

                            if (a6 != null && a7 != null && a16 != null) {
//                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("route duration traffic[" + a6.getDuration() + "] no-traffic[" + a7.getDuration() + "]");
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("route duration traffic[" + a6.getDuration() + "] no-traffic[" + a7.getDuration() + "] long-term-clousre [" + a16.getDuration() + "]");

                            }
                            java.util.List a8 = a2.route.getManeuvers();
                            java.util.List a9 = a3.getManeuvers();
                            com.here.android.mpa.routing.Maneuver a10 = com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$2.this$1.this$0).getNavController().getNextManeuver();
                            label7: {
                                label6: {
                                    if (a10 != null) {
                                        break label6;
                                    }
                                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().i("no current maneuver");
                                    a4 = a8;
                                    a5 = a9;
                                    i = 0;
                                    i0 = 0;
                                    break label7;
                                }
                                int i1 = a9.size();
                                a4 = a8;
                                a5 = a9;
                                i = 0;
                                while(true) {
                                    if (i >= i1) {
                                        i = -1;
                                        break;
                                    } else {
                                        if (!com.navdy.hud.app.maps.here.HereMapUtil.areManeuverEqual((com.here.android.mpa.routing.Maneuver)((java.util.List)a5).get(i), a10)) {
                                            i = i + 1;
                                            continue;
                                        }
                                        break;
                                    }
                                }
                                label5: {
                                    if (i != -1) {
                                        break label5;
                                    }
                                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().i("cannot find current maneuver in route");
                                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$600(this.this$2.this$1.this$0);
                                    break label4;
                                }
                                int i2 = ((java.util.List)a4).size();
                                i0 = 0;
                                while(true) {
                                    if (i0 >= i2) {
                                        i0 = 0;
                                        break;
                                    } else {
                                        if (!com.navdy.hud.app.maps.here.HereMapUtil.areManeuverEqual((com.here.android.mpa.routing.Maneuver)((java.util.List)a4).get(i0), a10)) {
                                            i0 = i0 + 1;
                                            continue;
                                        }
                                        break;
                                    }
                                }
                            }
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().i("firstOff = " + i + " secondOff = " + i0);
                            boolean b = com.navdy.hud.app.maps.here.HereMapUtil.areManeuversEqual((java.util.List)a5, i, (java.util.List)a4, i0, (java.util.List)new java.util.ArrayList(2));
                            String s = com.navdy.hud.app.maps.here.HereRouteViaGenerator.getViaString(a2.route);
                            com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a11 = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$2.this$1.this$0).getCurrentRouteId());
                            String s0 = null;
                            if (a11 != null) {
                                s0 = a2.routeResult.via;
                            }
                            if (b) {
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("fast route same as current route faster-via[" + s + "] current-via[" + s0 + "]");
                                if (com.navdy.hud.app.framework.voice.TTSUtils.isDebugTTSEnabled()) {
                                    com.navdy.hud.app.framework.voice.TTSUtils.debugShowFasterRouteToast("Faster route not found", " current route via[" + s0 + "] is still fastest");
                                }
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$600(this.this$2.this$1.this$0);
                            } else {
                                java.util.Date a12 = null;
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().i("fast route is different than current route");
                                String s1 = com.navdy.hud.app.maps.here.HereMapUtil.getAllRoadNames((java.util.List)a5, 0);
                                String s2 = com.navdy.hud.app.maps.here.HereMapUtil.getAllRoadNames((java.util.List)a4, 0);
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("[RoadNames-current] [offset=" + i + "] maneuverCount=" + ((java.util.List) a5).size());
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v(s1);
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("[RoadNames-faster] [offset=" + i0 + "] maneuverCount=" + ((java.util.List) a4).size());
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v(s2);
                                com.here.android.mpa.routing.Route a13 = com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$000(this.this$2.this$1.this$0).getCurrentProposedRoute();
                                label1: {
                                    long j = 0L;
                                    label0: {
                                        label3: {
                                            if (a13 == null) {
                                                break label3;
                                            }
                                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().i("check if current proposed route is same as new one");
                                            java.util.List a14 = a13.getManeuvers();
                                            boolean b0 = com.navdy.hud.app.maps.here.HereMapUtil.areManeuversEqual(a14, 0, (java.util.List)a4, 0, (java.util.List)null);
                                            label2: {
                                                if (b0) {
                                                    break label2;
                                                }
                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().i("current proposed route[" + s0 + "] is different from new one[" + s + "]");
                                                String s3 = com.navdy.hud.app.maps.here.HereMapUtil.getAllRoadNames(a14, 0);
                                                String s4 = com.navdy.hud.app.maps.here.HereMapUtil.getAllRoadNames((java.util.List)a4, 0);
                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("[RoadNames-proposed-current] maneuverCount=" + a14.size());
                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v(s3);
                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("[RoadNames-proposed-faster] maneuverCount=" + ((java.util.List) a4).size());
                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v(s4);
                                                break label3;
                                            }
                                            a12 = com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$2.this$1.this$0).getNavController().getEta(true, com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL);
                                            if (!com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(a12)) {
                                                break label1;
                                            }
                                            long j0 = (long)a6.getDuration();
                                            j = (a12.getTime() - System.currentTimeMillis()) / 1000L - j0;
                                            int i3 = com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$000(this.this$2.this$1.this$0).getRerouteMinDuration();
                                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().i("current proposed route[" + s0 + "] is same as new one[" + s + "] new-diff[" + j + "] old-diff[" + NavdyTrafficRerouteManager.access$000(this.this$2.this$1.this$0).getFasterBy() + "] minDur[" + i3 + "]");
                                            if (j < (long)i3) {
                                                break label0;
                                            }
                                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().i("route still above threshold, update");
                                        }
                                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().i("calling here traffic reroute listener");
                                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$000(this.this$2.this$1.this$0).handleFasterRoute(a2.route, false);
                                        break label4;
                                    }
                                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().i("route has dropped below threshold:" + j);
                                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$000(this.this$2.this$1.this$0).dismissReroute();
                                    break label4;
                                }
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("current eta is invalid:" + a12);
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$600(this.this$2.this$1.this$0);
                            }
                        } else {
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("no current route");
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$600(this.this$2.this$1.this$0);
                        }
                    } else {
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("did not get traffic duration");
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$600(this.this$2.this$1.this$0);
                    }
                } else {
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("did not get routeinfo");
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$600(this.this$2.this$1.this$0);
                }
            } else {
                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("success,route manager stopped");
            }
        } catch(Throwable a15) {
            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().e(a15);
        }
    }
}
