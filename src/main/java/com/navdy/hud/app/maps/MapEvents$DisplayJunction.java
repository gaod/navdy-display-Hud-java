package com.navdy.hud.app.maps;

import com.here.android.mpa.common.Image;

public class MapEvents$DisplayJunction {
    public Image junction;
    public Image signpost;
    
    public MapEvents$DisplayJunction(Image a, Image a0) {
        this.junction = a;
        this.signpost = a0;
    }
}
