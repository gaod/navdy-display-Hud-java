package com.navdy.hud.app.maps.here;

import android.Manifest.permission;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;

import com.here.android.mpa.common.LocationDataSource;
import com.here.android.mpa.common.PositioningManager.LocationMethod;
import com.mapzen.android.lost.api.LocationListener;
import com.mapzen.android.lost.api.LocationRequest;
import com.mapzen.android.lost.api.LocationServices;
import com.mapzen.android.lost.api.LostApiClient;
import com.navdy.hud.app.device.gps.GpsManager;
import com.navdy.hud.app.device.gps.GpsUtils;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.service.library.log.Logger;

import static com.here.android.mpa.common.PositioningManager.LocationMethod.GPS_NETWORK;

public class LostLocationDataSource extends LocationDataSource implements LostApiClient.ConnectionCallbacks, LocationListener {

    private LostApiClient lostApiClient;
    private Context context;
    private static LostLocationDataSource instance;
    private static final Logger sLogger = new Logger(LostLocationDataSource.class);

    public LostLocationDataSource(Context context) {
        this.context = context;
        lostApiClient = new LostApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .build();
        instance = this;
    }

//    @Override
    public static LocationDataSource getInstance(LocationDataSource.a var0) {
        return LostLocationDataSource.instance;
    }

    public boolean start() {
        return start(null);
    }

        @Override
    public boolean start(LocationMethod locationMethod) {
        if (ActivityCompat.checkSelfPermission(this.context, permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.context, permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        lostApiClient.connect();
        return true;
    }

    @Override
    public void stop() {
        lostApiClient.disconnect();
    }

    @Override
    public int getGpsStatus() {
        return 1;
    }

    @Override
    public int getNetworkStatus() {
        return 1;
    }

    @Override
    public int getIndoorStatus() {
        return 1;
    }

    @SuppressLint("MissingPermission")
    @Override
    public Location getLastKnownLocation() {
//        if (ActivityCompat.checkSelfPermission(this.context, permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            return null;
//        }
        if (lostApiClient.isConnected()) {
            return LocationServices.FusedLocationApi.getLastLocation(lostApiClient);
        }
        return null;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onConnected() {

        int priority = 0;
        switch(GpsUtils.GpsSource()) {
            case AUTO:
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY;
                // Enable network
                com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getConnectionHandler().startPhoneSendingLocation();
                sLogger.i("LostLocationDataSource started in Auto mode");
                break;

            case UBLOX:
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY;
                // Disable network
                com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getConnectionHandler().stopPhoneSendingLocation();
                sLogger.i("LostLocationDataSource started in GPS Only mode");
                break;

            case PHONE:
                priority = LocationRequest.PRIORITY_LOW_POWER;
                // Enable network
                com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getConnectionHandler().startPhoneSendingLocation();
                sLogger.i("LostLocationDataSource started in Phone Only mode");
                break;

            default:
                sLogger.e("LostLocationDataSource started in UNKNOWN MODE");

        }

        LocationRequest request = LocationRequest.create()
                .setInterval(100)
                .setSmallestDisplacement(0)
                .setPriority(priority);

        LocationServices.FusedLocationApi.requestLocationUpdates(lostApiClient, request, (LocationListener) this);
    }

    @Override
    public void onConnectionSuspended() {

    }

    @Override
    public void onLocationChanged(Location location) {
        super.onLocationUpdated(GPS_NETWORK, location);
        try {
            HereMapsManager.getInstance().hereLocationFixManager.setBlueLocationMarker(location);
        } catch (NullPointerException ignored) {

        }
    }

}
