package com.navdy.hud.app.maps.here;

import static com.navdy.hud.app.maps.here.HereMapsManager.sLogger;

class HereMapsManager$2 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapsManager this$0;
    
    HereMapsManager$2(com.navdy.hud.app.maps.here.HereMapsManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        long j = android.os.SystemClock.elapsedRealtime();
        this.this$0.mapLoader = com.here.android.mpa.odml.MapLoader.getInstance();
        long j0 = android.os.SystemClock.elapsedRealtime();
        sLogger.v("initMapLoader took [" + (j0 - j) + "]");
        this.this$0.mapLoader.addListener(new HereMapsManager$2$1(this));
        this.this$0.mapPackageCount = 0;
        this.this$0.invokeMapLoader();
    }
}
