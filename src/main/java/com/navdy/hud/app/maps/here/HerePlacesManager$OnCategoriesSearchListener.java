package com.navdy.hud.app.maps.here;

import com.here.android.mpa.search.Place;

abstract public interface HerePlacesManager$OnCategoriesSearchListener {
    abstract public void onCompleted(java.util.List<Place> arg);
    
    
    abstract public void onError(com.navdy.hud.app.maps.here.HerePlacesManager$Error arg);
}
