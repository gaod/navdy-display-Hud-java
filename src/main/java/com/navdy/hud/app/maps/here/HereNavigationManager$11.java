package com.navdy.hud.app.maps.here;

class HereNavigationManager$11 implements Runnable {
    final com.navdy.hud.app.maps.here.HereNavigationManager this$0;
    
    HereNavigationManager$11(com.navdy.hud.app.maps.here.HereNavigationManager a) {

        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("limitbandwidth: turn on HERE traffic avoidance mode");
        com.navdy.hud.app.maps.here.HereNavigationManager.access$700(this.this$0).setTrafficAvoidanceMode(com.here.android.mpa.guidance.NavigationManager.TrafficAvoidanceMode.MANUAL);
    }
}
