package com.navdy.hud.app.maps.here;

class HereMapCameraManager$5 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapCameraManager this$0;
    
    HereMapCameraManager$5(com.navdy.hud.app.maps.here.HereMapCameraManager a) {

        super();
        this.this$0 = a;
    }
    
    public void run() {
        if (com.navdy.hud.app.maps.here.HereMapCameraManager.access$1900(this.this$0)) {
            com.navdy.hud.app.maps.here.HereMapCameraManager.access$2000(this.this$0).setZoom(com.navdy.hud.app.maps.here.HereMapCameraManager.access$900(this.this$0));
            com.navdy.hud.app.maps.here.HereMapCameraManager.access$2000(this.this$0).setTilt(com.navdy.hud.app.maps.here.HereMapCameraManager.access$1000(this.this$0));
        } else {
            com.navdy.hud.app.maps.here.HereMapCameraManager.access$2202(this.this$0, 0);
            com.navdy.hud.app.maps.here.HereMapCameraManager.access$1402(this.this$0, true);
            double d = (com.navdy.hud.app.maps.here.HereMapCameraManager.access$900(this.this$0) - com.navdy.hud.app.maps.here.HereMapCameraManager.access$300(this.this$0).getZoomLevel()) / 5.0;
            float f = (com.navdy.hud.app.maps.here.HereMapCameraManager.access$1000(this.this$0) - com.navdy.hud.app.maps.here.HereMapCameraManager.access$300(this.this$0).getTilt()) / 5f;
            com.navdy.hud.app.maps.here.HereMapCameraManager.access$100(this.this$0).post(com.navdy.hud.app.maps.here.HereMapCameraManager.access$2300(this.this$0, d, f));
        }
    }
}
