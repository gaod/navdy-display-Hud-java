package com.navdy.hud.app.maps;

public enum MapEvents$LiveTrafficEvent$Type {
    CONGESTION(0),
    INCIDENT(1);

    private int value;
    MapEvents$LiveTrafficEvent$Type(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class MapEvents$LiveTrafficEvent$Type extends Enum {
//    final private static com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type[] $VALUES;
//    final public static com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type CONGESTION;
//    final public static com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type INCIDENT;
//
//    static {
//        CONGESTION = new com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type("CONGESTION", 0);
//        INCIDENT = new com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type("INCIDENT", 1);
//        com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type[] a = new com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type[2];
//        a[0] = CONGESTION;
//        a[1] = INCIDENT;
//        $VALUES = a;
//    }
//
//    private MapEvents$LiveTrafficEvent$Type(String s, int i) {
//        super(s, i);
//    }
//
//    public static com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type valueOf(String s) {
//        return (com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type)Enum.valueOf(com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type.class, s);
//    }
//
//    public static com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type[] values() {
//        return $VALUES.clone();
//    }
//}
