package com.navdy.hud.app.maps.here;

class HereNavigationManager$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereNavigationManager this$0;
    
    HereNavigationManager$1(com.navdy.hud.app.maps.here.HereNavigationManager a) {

        super();
        this.this$0 = a;
    }
    
    public void run() {
        if (com.navdy.hud.app.maps.here.HereNavigationManager.access$000(this.this$0) != com.navdy.hud.app.maps.NavigationMode.MAP) {
            long j = System.currentTimeMillis() - com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).lastManeuverPostTime;
            if (j >= 0L) {
                if (j >= 60000L) {
                    this.this$0.refreshNavigationInfo();
                    if (com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v(new StringBuilder().append("etaCalc updated maneuver:").append(j).toString());
                    }
                    com.navdy.hud.app.maps.here.HereNavigationManager.access$200(this.this$0).postDelayed((Runnable)this, 30000L);
                } else {
                    if (com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.isLoggable(2)) {
                        com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v(new StringBuilder().append("etaCalc threshold not met:").append(j).toString());
                    }
                    com.navdy.hud.app.maps.here.HereNavigationManager.access$200(this.this$0).postDelayed((Runnable)this, 30000L);
                }
            } else {
                com.navdy.hud.app.maps.here.HereNavigationManager.access$200(this.this$0).postDelayed((Runnable)this, 30000L);
            }
        }
    }
}
