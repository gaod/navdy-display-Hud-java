package com.navdy.hud.app;

import com.navdy.hud.app.analytics.AnalyticsSupport$AnalyticsIntentsReceiver;
import com.navdy.hud.app.ui.activity.MainActivity;
import com.navdy.hud.app.maps.GpsEventsReceiver;
import com.navdy.hud.app.service.StickyService;
import android.content.Intent;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import dagger.ObjectGraph;
import com.navdy.hud.app.common.ProdModule;
import java.net.URL;
import java.util.Locale;
import android.os.Build;
import android.util.Log;
import android.content.SharedPreferences;
import com.here.android.mpa.common.ViewRect;
import com.navdy.hud.app.device.ProjectorBrightness;
import com.navdy.hud.app.util.os.PropsFileUpdater;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.hud.app.framework.glympse.GlympseManager;
import com.navdy.hud.app.util.os.CpuProfiler;
import com.navdy.hud.app.framework.network.NetworkBandwidthController;
import com.navdy.hud.app.device.gps.GpsDeadReckoningManager;
import com.navdy.hud.app.audio.SoundUtils;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.service.ShutdownMonitor;
import com.navdy.hud.app.util.ReportIssueService;
import com.navdy.hud.app.framework.glance.GlanceHandler;
import com.navdy.hud.app.framework.message.MessageManager;
import com.navdy.hud.app.framework.destinations.DestinationsManager;
import com.navdy.hud.app.framework.contacts.FavoriteContactsManager;
import com.navdy.hud.app.framework.contacts.ContactImageHelper;
import com.navdy.hud.app.bluetooth.pbap.PBAPClientManager;
import com.navdy.hud.app.framework.recentcall.RecentCallManager;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.ui.component.homescreen.SmartDashViewResourceValues;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.debug.DriveRecorder;
import android.app.Application;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.storage.db.HudDatabase;
import android.os.SystemClock;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.hud.app.util.CrashReporter;
import com.navdy.hud.app.util.NavdyNativeLibWrapper;
import com.navdy.service.library.util.SystemUtils;
import android.os.Process;
import mortar.Mortar;
import com.navdy.hud.app.profile.HudLocale;
import com.navdy.service.library.log.LogcatAppender;
import com.navdy.service.library.log.LogAppender;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import com.navdy.hud.app.receiver.LogLevelReceiver;
import com.navdy.hud.app.device.gps.GpsManager;
import com.navdy.service.library.task.TaskManager;
import android.os.Looper;
import com.navdy.hud.app.manager.UpdateReminderManager;
import mortar.MortarScope;
import okhttp3.OkHttpClient;
import okhttp3.OkUrlFactory;
import com.navdy.hud.app.event.Shutdown;
import android.os.Handler;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import android.content.Context;
import android.support.multidex.MultiDexApplication;

public class HudApplication extends MultiDexApplication
{
    public static final String NOT_A_CRASH = "NAVDY_NOT_A_CRASH";
    private static final String TAG = "HudApplication";
    private static Context sAppContext;
    private static HudApplication sApplication;
    private static final Logger sLogger;
    @Inject
    public Bus bus;
    private Handler handler;
    private boolean initialized;
    private Shutdown.Reason lastSeenReason;
    private MortarScope rootScope;
    private UpdateReminderManager updateReminderManager;
    private static OkHttpClient okHttpClient;
    
    static {
        sLogger = new Logger(HudApplication.class);
        okHttpClient = new OkHttpClient();
    }
    
    public HudApplication() {
        this.handler = new Handler(Looper.getMainLooper());
        this.lastSeenReason = Shutdown.Reason.UNKNOWN;

        URL.setURLStreamHandlerFactory(new OkUrlFactory(okHttpClient));
    }
    
    public static Context getAppContext() {
        return HudApplication.sAppContext;
    }
    
    public static HudApplication getApplication() {
        return HudApplication.sApplication;
    }
    
    private void initConnectionService(final String s) {
        HudApplication.sLogger.v(s + ":initializing taskMgr");
        final TaskManager instance = TaskManager.getInstance();
        instance.addTaskQueue(1, 3);
        instance.addTaskQueue(5, 1);
        instance.addTaskQueue(9, 1);
        instance.init();
        HudApplication.sLogger.v(s + ":initializing gps manager");
        GpsManager.getInstance();
        this.registerReceiver(new LogLevelReceiver(), new IntentFilter("com.navdy.service.library.log.action.RELOAD"));
    }
    
    private static void initLogger(final boolean b) {
        Logger.init(new LogAppender[] { new LogcatAppender() });
    }
    
    private void initTaskManager(final String s) {
        HudApplication.sLogger.v(s + ":initializing taskMgr");
        final TaskManager instance = TaskManager.getInstance();
        try {
            instance.addTaskQueue(1, 3);
            instance.addTaskQueue(5, 1);
            instance.addTaskQueue(8, 1);
            instance.addTaskQueue(6, 1);
            instance.addTaskQueue(7, 1);
            instance.addTaskQueue(9, 1);
            instance.addTaskQueue(10, 1);
            instance.addTaskQueue(11, 1);
            instance.addTaskQueue(12, 1);
            instance.addTaskQueue(13, 1);
            instance.addTaskQueue(14, 1);
            instance.addTaskQueue(22, 1);
            instance.addTaskQueue(15, 1);
            instance.addTaskQueue(16, 1);
            instance.addTaskQueue(17, 1);
            instance.addTaskQueue(18, 1);
            instance.addTaskQueue(2, 5);
            instance.addTaskQueue(3, 1);
            instance.addTaskQueue(4, 1);
            instance.addTaskQueue(19, 1);
            instance.addTaskQueue(20, 1);
            instance.addTaskQueue(21, 1);
            instance.addTaskQueue(23, 1);
            instance.init();
        }
        catch (IllegalStateException ex) {
            if (!ex.getMessage().equals("already initialized")) {
                throw ex;
            }
        }
    }
    
    public static boolean isDeveloperBuild() {
        return false;
    }
    
    public static void setContext(final Context sAppContext) {
        HudApplication.sAppContext = sAppContext;
    }
    
    @Override
    protected void attachBaseContext(final Context context) {
        super.attachBaseContext(HudLocale.onAttach(context));
    }
    
    public Bus getBus() {
        return this.bus;
    }
    
    public MortarScope getRootScope() {
        return this.rootScope;
    }
    
    public Object getSystemService(final String s) {
        Object o;
        if (Mortar.isScopeSystemService(s)) {
            o = this.rootScope;
        }
        else {
            o = super.getSystemService(s);
        }
        return o;
    }

    public void initHudApp() {
        if (this.initialized) {
            return;
        }
        this.initialized = true;
        String string = SystemUtils.getProcessName(sAppContext, Process.myPid());
        NavdyNativeLibWrapper.loadlibrary();
        if (CrashReporter.isEnabled()) {
            sLogger.v(string + ":initializing crash reporter");
            CrashReporter.getInstance().installCrashHandler(sAppContext);
        } else {
            sLogger.v(string + ":crash reporter not installed");
        }

        if (DeviceUtil.isNavdyDevice()) {
            PropsFileUpdater.run();
            ProjectorBrightness.init();
        }
        sLogger.v(string + ":updating logging setup");
        HudApplication.initLogger(true);
        IntentFilter object = new IntentFilter();
        object.addAction("com.navdy.hud.app.service.OTA_DOWNLOAD");
        SharedPreferences object2 = RemoteDeviceManager.getInstance().getSharedPreferences();
        if (object2 == null) {
            sLogger.e("unable to get SharedPreferences");
        } else {
            this.registerReceiver(new OTAUpdateService.OTADownloadIntentsReceiver(object2), object);
        }
        PicassoUtil.initPicasso(sAppContext);
        RemoteDeviceManager.getInstance().getSharedPreferences().edit();
        try {
            sLogger.v("dbase:opening...");
            long l = SystemClock.elapsedRealtime();
            HudDatabase.getInstance().getWritableDatabase();
            StringBuilder sb = new StringBuilder();
            sLogger.v(sb.append("dbase: opened[").append(SystemClock.elapsedRealtime() - l).append("]").toString());
        }
        catch (Throwable throwable) {
            sLogger.e("dbase: error opening, deleting it", throwable);
            HudDatabase.deleteDatabaseFile();
            sLogger.e("dbase: deletinged");
            try {
                sLogger.v("dbase:re-opening");
                long l = SystemClock.elapsedRealtime();
                HudDatabase.getInstance().getWritableDatabase();
                StringBuilder str = new StringBuilder();
                sLogger.v(str.append("dbase: opened[").append(SystemClock.elapsedRealtime() - l).append("]").toString());
            }
            catch (Throwable throwable2) {
                sLogger.e("dbase: error opening", throwable);
            }
        }
        /* ANL: we're missing a 'localytics.xml' with a key needed for this */
        AnalyticsSupport.analyticsApplicationInit(this);

        TaskManager.getInstance().execute(new Runnable() {
            final /* synthetic */ DriveRecorder val$dataRecorder = HudApplication.this.rootScope.getObjectGraph().get(DriveRecorder.class);

            @Override
            public void run() {
                this.val$dataRecorder.load();
            }
        }, 1);
        sLogger.v(string + ":initializing here maps engine");
        HereMapsManager.getInstance();
        sLogger.v(string + ":called initialized");
        final String speedMph = HomeScreenResourceValues.speedMph;
        final ViewRect routeOverviewRect = HomeScreenConstants.routeOverviewRect;
        final int middleGaugeShrinkLeftX = SmartDashViewResourceValues.middleGaugeShrinkLeftX;
        final String tts_DIAL_BATTERY_EXTREMELY_LOW = TTSUtils.TTS_DIAL_BATTERY_EXTREMELY_LOW;
        final int colorFacebook = GlanceConstants.colorFacebook;
        GlanceHelper.initMessageAttributes();
        if (CrashReporter.isEnabled()) {
            this.handler.post(new Runnable() {
                @Override
                public void run() {
                    TaskManager.getInstance().execute(new Runnable() {
                        @Override
                        public void run() {
                            CrashReporter.getInstance().checkForKernelCrashes(PathManager.getInstance().getKernelCrashFiles());
                        }
                    }, 1);
                }
            });
            this.handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    TaskManager.getInstance().execute(new Runnable() {
                        @Override
                        public void run() {
                            final CrashReporter instance = CrashReporter.getInstance();
                            instance.checkForAnr();
                            instance.checkForTombstones();
                            instance.checkForOTAFailure();
                        }
                    }, 1);
                }
            }, 45000L);
        }
        DialManager.getInstance();
        RecentCallManager.getInstance();
        PBAPClientManager.getInstance();
        ContactImageHelper.getInstance();
        FavoriteContactsManager.getInstance();
        DestinationsManager.getInstance();
        MessageManager.getInstance();
        GlanceHandler.getInstance();
        ReportIssueService.initialize();
        ShutdownMonitor.getInstance();
        if (!DeviceUtil.isNavdyDevice()) {
            SoundUtils.init();
        }
        RemoteDeviceManager.getInstance().getFeatureUtil();
        if (DeviceUtil.isNavdyDevice()) {
            sLogger.v("start dead reckoning mgr");
            GpsDeadReckoningManager.getInstance();
        }
        this.updateReminderManager = new UpdateReminderManager(this);
        sLogger.i("init network b/w controller");
        NetworkBandwidthController.getInstance();
        CpuProfiler.getInstance().start();
        this.handler.post(new Runnable() {
            @Override
            public void run() {
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                GlympseManager.getInstance();
                HudApplication.sLogger.v("time taken by Glympse init: " + (SystemClock.elapsedRealtime() - elapsedRealtime) + " ms");
            }
        });
        sLogger.v(string + ":background init done");
    }


    public void onCreate() {
        Log.e("", "::onCreate locale:" + this.getResources().getConfiguration().locale);
        HudApplication.sApplication = this;
        HudApplication.sAppContext = HudApplication.sApplication;
        super.onCreate();
        final String processName = SystemUtils.getProcessName(HudApplication.sAppContext, Process.myPid());
        final String packageName = this.getPackageName();
        initLogger(false);
        HudApplication.sLogger.d(processName + " starting  on " + Build.BRAND + "," + Build.HARDWARE + "," + Build.MODEL);
        if (processName.equalsIgnoreCase(packageName + ":connectionService")) {
            HudApplication.sLogger.v("startup:" + processName + " :connection service");
            this.initConnectionService(processName);
        }
        else if (processName.equalsIgnoreCase(packageName)) {
            HudApplication.sLogger.v("startup:" + processName + " :hud app locale=" + Locale.getDefault().toString());
            HudApplication.sLogger.v(processName + ":initializing Mortar");
            final long elapsedRealtime = SystemClock.elapsedRealtime();
            this.rootScope = Mortar.createRootScope(false, ObjectGraph.create(new ProdModule(this)));
            this.rootScope.getObjectGraph().inject(this);
            HudApplication.sLogger.v(processName + ":mortar init took:" + (SystemClock.elapsedRealtime() - elapsedRealtime));
            this.initTaskManager(processName);
            HudApplication.sLogger.i("init network state manager");
            NetworkStateManager.getInstance();
            HudApplication.sLogger.i("*** starting hud sticky-service ***");
            final Intent intent = new Intent();
            intent.setClass(this, StickyService.class);
            this.startService(intent);
            HudApplication.sLogger.i("*** started hud sticky-service ***");
            final IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("GPS_Switch");
            intentFilter.addAction("GPS_WARM_RESET_UBLOX");
            intentFilter.addAction("GPS_SATELLITE_STATUS");
            intentFilter.addAction("GPS_COLLECT_LOGS");
            intentFilter.addAction("driving_started");
            intentFilter.addAction("driving_stopped");
            intentFilter.addAction("GPS_ENABLE_ESF_RAW");
            this.registerReceiver(new GpsEventsReceiver(), intentFilter);
            HudApplication.sLogger.v("registered GpsEventsReceiver");
            final IntentFilter intentFilter2 = new IntentFilter();
            intentFilter2.addAction("com.navdy.hud.app.analytics.AnalyticsEvent");
            this.registerReceiver(new AnalyticsSupport$AnalyticsIntentsReceiver(), intentFilter2);
            HudApplication.sLogger.v("registered AnalyticsIntentsReceiver");
            final Intent intent2 = new Intent(this, MainActivity.class);
            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.startActivity(intent2);
        }
        else {
            HudApplication.sLogger.v("startup:" + processName + " :no-op");
        }
        HudApplication.sLogger.v("startup:" + processName + " :initialization done");
    }
    
    public void setShutdownReason(final Shutdown.Reason lastSeenReason) {
        this.lastSeenReason = lastSeenReason;
    }
    
    public void shutdown() {
        HudApplication.sLogger.i("shutting down HUD");
        CrashReporter.getInstance().stopCrashReporting(true);
        final Intent intent = new Intent();
        intent.setClass(this, StickyService.class);
        this.stopService(intent);
        if (this.bus != null) {
            HudApplication.sLogger.i("shutting down HUD - reason: " + this.lastSeenReason);
            this.bus.post(new Shutdown(this.lastSeenReason, Shutdown.State.SHUTTING_DOWN));
        }
        try {
            HereMapsManager.getInstance().getLocationFixManager().shutdown();
        } catch (NullPointerException e) {
            // HudApplication.sLogger.i("LocationFixManager not initialised yet");
        }
    }
}
