package com.navdy.hud.app.util.os;

class CpuProfiler$2 implements Runnable {
    private final com.navdy.hud.app.util.os.CpuProfiler this$0;
    
    CpuProfiler$2(com.navdy.hud.app.util.os.CpuProfiler a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        label0: {
            Throwable a;
            label1: {
                label2: {
                    try {
                        if (!com.navdy.hud.app.util.os.CpuProfiler.access$100(this.this$0)) {
                            break label2;
                        }
                        com.navdy.service.library.util.SystemUtils$CpuInfo a0 = com.navdy.service.library.util.SystemUtils.getCpuUsage();
                        int i = a0.getCpuUser();
                        int i0 = a0.getCpuSystem();
                        int i1 = i + i0;
                        if (com.navdy.hud.app.util.os.CpuProfiler.access$200(this.this$0)) {
                            if (i1 >= 70) {
                                com.navdy.hud.app.util.os.CpuProfiler.access$600(this.this$0, "STILL HIGH", i1, i, i0, a0.getList());
                                if (com.navdy.hud.app.util.os.CpuProfiler.access$1000(this.this$0) == null) {
                                    com.navdy.hud.app.util.os.CpuProfiler.access$500(this.this$0, a0);
                                }
                            } else {
                                com.navdy.hud.app.util.os.CpuProfiler.access$202(this.this$0, false);
                                com.navdy.hud.app.util.os.CpuProfiler.access$400(this.this$0).post(com.navdy.hud.app.util.os.CpuProfiler.access$800());
                                com.navdy.hud.app.util.os.CpuProfiler.access$900(this.this$0, a0);
                                com.navdy.hud.app.util.os.CpuProfiler.access$600(this.this$0, "NOW NORMAL", i1, i, i0, a0.getList());
                            }
                        } else if (i1 <= 85) {
                            if (com.navdy.hud.app.util.os.CpuProfiler.access$700().isLoggable(2)) {
                                com.navdy.hud.app.util.os.CpuProfiler.access$600(this.this$0, "NORMAL", i1, i, i0, a0.getList());
                            }
                        } else {
                            com.navdy.hud.app.util.os.CpuProfiler.access$202(this.this$0, true);
                            com.navdy.hud.app.util.os.CpuProfiler.access$400(this.this$0).post(com.navdy.hud.app.util.os.CpuProfiler.access$300());
                            com.navdy.hud.app.util.os.CpuProfiler.access$500(this.this$0, a0);
                            com.navdy.hud.app.util.os.CpuProfiler.access$600(this.this$0, "HIGH", i1, i, i0, a0.getList());
                        }
                    } catch(Throwable a1) {
                        a = a1;
                        break label1;
                    }
                    if (!com.navdy.hud.app.util.os.CpuProfiler.access$100(this.this$0)) {
                        break label0;
                    }
                    com.navdy.hud.app.util.os.CpuProfiler.access$1300(this.this$0).postDelayed(com.navdy.hud.app.util.os.CpuProfiler.access$1100(this.this$0), (long)com.navdy.hud.app.util.os.CpuProfiler.access$1200());
                    break label0;
                }
                if (!com.navdy.hud.app.util.os.CpuProfiler.access$100(this.this$0)) {
                    break label0;
                }
                com.navdy.hud.app.util.os.CpuProfiler.access$1300(this.this$0).postDelayed(com.navdy.hud.app.util.os.CpuProfiler.access$1100(this.this$0), (long)com.navdy.hud.app.util.os.CpuProfiler.access$1200());
                break label0;
            }
            try {
                com.navdy.hud.app.util.os.CpuProfiler.access$700().e(a);
            } catch(Throwable a2) {
                if (com.navdy.hud.app.util.os.CpuProfiler.access$100(this.this$0)) {
                    com.navdy.hud.app.util.os.CpuProfiler.access$1300(this.this$0).postDelayed(com.navdy.hud.app.util.os.CpuProfiler.access$1100(this.this$0), (long)com.navdy.hud.app.util.os.CpuProfiler.access$1200());
                }
                throw a2;
            }
            if (com.navdy.hud.app.util.os.CpuProfiler.access$100(this.this$0)) {
                com.navdy.hud.app.util.os.CpuProfiler.access$1300(this.this$0).postDelayed(com.navdy.hud.app.util.os.CpuProfiler.access$1100(this.this$0), (long)com.navdy.hud.app.util.os.CpuProfiler.access$1200());
            }
        }
    }
}
