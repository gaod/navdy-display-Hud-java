package com.navdy.hud.app.util.picasso;

public class PicassoLruCache extends com.squareup.picasso.LruCache implements com.navdy.hud.app.util.picasso.PicassoItemCacheListener {
    private java.util.HashMap keyMap;
    
    PicassoLruCache(int i) {
        super(i);
        this.keyMap = new java.util.HashMap();
    }
    
    private String getKey(String s) {
        if (s != null) {
            int i = s.indexOf("\n");
            if (i != -1) {
                s = s.substring(0, i);
            }
        } else {
            s = null;
        }
        return s;
    }
    
    public android.graphics.Bitmap getBitmap(String s) {
        android.graphics.Bitmap a;
        synchronized(this) {
            if (s != null) {
                    String s0 = (String)this.keyMap.get(s);
                    a = null;
                    if (s0 != null) {
                        a = super.get(s0);
                    }
            } else {
                a = null;
            }
        }
        /*monexit(this)*/
        return a;
    }
    
    public void init() throws NoSuchFieldException, IllegalAccessException {
        com.navdy.hud.app.util.picasso.PicassoCacheMap a = new com.navdy.hud.app.util.picasso.PicassoCacheMap(0, 0.75f);
        a.setListener(this);
        java.lang.reflect.Field a0 = com.squareup.picasso.LruCache.class.getDeclaredField("map");
        a0.setAccessible(true);
        a0.set(this, a);
    }
    
    public void itemAdded(Object a) {
        this.itemAdded((String)a);
    }
    
    public void itemAdded(String s) {
        synchronized(this) {
            if (s != null) {
                this.keyMap.put(this.getKey(s), s);
            }
        }
        /*monexit(this)*/
    }
    
    public void itemRemoved(Object a) {
        this.itemRemoved((String)a);
    }
    
    public void itemRemoved(String s) {
        synchronized(this) {
            if (s != null) {
                this.keyMap.remove(this.getKey(s));
            }
        }
        /*monexit(this)*/
    }
    
    public void setBitmap(String s, android.graphics.Bitmap a) {
        label1: synchronized(this) {
            if (s == null) {
                break label1;
            }
            if (a == null) {
                break label1;
            }
            String s0 = this.getKey(s);
            this.keyMap.put(s0, s);
            super.set(s, a);
        }
        /*monexit(this)*/
    }
}
