package com.navdy.hud.app.util;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import flow.Flow.Direction;
import flow.Layouts;
import mortar.Blueprint;
import mortar.Mortar;
import mortar.MortarScope;

public class ScreenConductor<S extends Blueprint> implements CanShowScreen<S> {
    private final ViewGroup container;
    private final Context context;
    HomeScreenView homeScreenView;
    private final Logger logger = new Logger(getClass());
    UIStateManager uiStateManager;

    public ScreenConductor(Context context, ViewGroup container, UIStateManager uiStateManager) {
        this.context = context;
        this.container = container;
        this.uiStateManager = uiStateManager;
    }

    public void createScreens() {
        if (this.homeScreenView == null) {
            HomeScreen screen = new HomeScreen();
            this.homeScreenView = (HomeScreenView) Layouts.createView(Mortar.getScope(this.context).requireChild(screen).createContext(this.context), screen);
            this.homeScreenView.setVisibility(View.INVISIBLE);
            this.container.addView(this.homeScreenView, 0);
            this.uiStateManager.setHomescreenView(this.homeScreenView);
            this.logger.v("createScreens:homescreen created and added");
        }
    }

    public void showScreen(S screen, Direction direction, int animIn, int animOut) {
        MortarScope newChildScope = Mortar.getScope(this.context).requireChild(screen);
        View oldChild = getChildView();
        View newChild = null;
        if (oldChild != null && Mortar.getScope(oldChild.getContext()).getName().equals(screen.getMortarScopeName()) && oldChild.getVisibility() == View.VISIBLE) {
            BaseScreen baseScreen = (BaseScreen) screen;
            this.uiStateManager.postScreenAnimationEvent(true, baseScreen, baseScreen);
            this.uiStateManager.postScreenAnimationEvent(false, baseScreen, baseScreen);
            return;
        }
        boolean homescreen = screen instanceof HomeScreen;
        boolean homescreenCreated = false;
        if (homescreen && this.homeScreenView != null) {
            this.logger.v("reusing homescreen view");
            newChild = this.homeScreenView;
        }
        if (newChild == null) {
            newChild = Layouts.createView(newChildScope.createContext(this.context),  screen);
            if (homescreen) {
                this.homeScreenView = (HomeScreenView) newChild;
                homescreenCreated = true;
            }
        }
        BaseScreen oldScreen = this.uiStateManager.getCurrentScreen();
        BaseScreen newScreen = (BaseScreen) screen;
        setAnimation(direction, oldChild, newChild, oldScreen, newScreen, animIn, animOut);
        if (!(oldChild == null || oldChild == this.homeScreenView)) {
            this.container.removeView(oldChild);
        }
        if (homescreen) {
            if (this.homeScreenView != null) {
                this.homeScreenView.setVisibility(View.INVISIBLE);
            }
            if (homescreenCreated) {
                this.logger.v("home screen added");
                this.container.addView(newChild);
            }
            this.homeScreenView.onResume();
            this.homeScreenView.setVisibility(View.VISIBLE);
        } else {
            if (this.homeScreenView != null) {
                UIStateManager uIStateManager = this.uiStateManager;
                if (UIStateManager.isPauseHomescreen(((BaseScreen) screen).getScreen())) {
                    this.homeScreenView.onPause();
                    this.homeScreenView.setVisibility(View.INVISIBLE);
                }
            }
            this.container.addView(newChild);
        }
        GenericUtil.clearInputMethodManagerFocusLeak();
        this.uiStateManager.setMainActiveScreen((BaseScreen) screen);
        if (((BaseScreen) screen).getScreen() != Screen.SCREEN_BACK) {
            AnalyticsSupport.recordScreen((BaseScreen) screen);
        }
        if (animIn == -1 && animOut == -1) {
            this.uiStateManager.postScreenAnimationEvent(false, newScreen, oldScreen);
        }
    }

    protected void setAnimation(Direction direction, View oldChild, View newChild, final BaseScreen oldScreen, final BaseScreen newScreen, final int animIn, int animOut) {
        if (oldChild == null) {
            if (newScreen != null) {
                newScreen.onAnimationInStart();
                this.uiStateManager.postScreenAnimationEvent(true, newScreen, null);
                newScreen.onAnimationInEnd();
                this.uiStateManager.postScreenAnimationEvent(false, newScreen, null);
            }
        } else if (animIn == -1 && animOut == -1) {
            this.uiStateManager.postScreenAnimationEvent(true, newScreen, oldScreen);
        } else {
            Animation animation;
            if (animOut != -1) {
                animation = AnimationUtils.loadAnimation(this.context, animOut);
                if (oldScreen != null) {
                    animation.setAnimationListener(new AnimationListener() {
                        public void onAnimationStart(Animation animation) {
                            oldScreen.onAnimationOutStart();
                            if (animIn == -1) {
                                ScreenConductor.this.uiStateManager.postScreenAnimationEvent(true, newScreen, oldScreen);
                            }
                        }

                        public void onAnimationEnd(Animation animation) {
                            oldScreen.onAnimationOutEnd();
                            if (animIn == -1) {
                                ScreenConductor.this.uiStateManager.postScreenAnimationEvent(false, newScreen, oldScreen);
                            }
                        }

                        public void onAnimationRepeat(Animation animation) {
                        }
                    });
                }
                oldChild.setAnimation(animation);
            }
            if (animIn != -1) {
                animation = AnimationUtils.loadAnimation(this.context, animIn);
                if (newScreen != null) {
                    animation.setAnimationListener(new AnimationListener() {
                        public void onAnimationStart(Animation animation) {
                            newScreen.onAnimationInStart();
                            ScreenConductor.this.uiStateManager.postScreenAnimationEvent(true, newScreen, oldScreen);
                        }

                        public void onAnimationEnd(Animation animation) {
                            newScreen.onAnimationInEnd();
                            ScreenConductor.this.uiStateManager.postScreenAnimationEvent(false, newScreen, oldScreen);
                        }

                        public void onAnimationRepeat(Animation animation) {
                        }
                    });
                }
                newChild.setAnimation(animation);
            }
        }
    }

    public View getChildView() {
        int count = this.container.getChildCount();
        if (count == 0) {
            return null;
        }
        for (count--; count >= 0; count--) {
            View view = this.container.getChildAt(count);
            if (view.getVisibility() == View.VISIBLE) {
                return view;
            }
        }
        return null;
    }
}
