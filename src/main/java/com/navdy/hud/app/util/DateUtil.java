package com.navdy.hud.app.util;

import com.navdy.hud.app.R;
import java.util.Calendar;
import com.navdy.hud.app.HudApplication;
import android.text.format.DateUtils;
import java.util.Date;
import java.util.Locale;
import com.navdy.service.library.log.Logger;
import java.text.SimpleDateFormat;

public class DateUtil
{
    private static final int HOUR_HAND_ANGLE_PER_HOUR = 30;
    private static final int MINUTE_HAND_ANGLE_PER_MINUTE = 6;
    private static SimpleDateFormat dateLabelFormat;
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(DateUtil.class);
        DateUtil.dateLabelFormat = new SimpleDateFormat("d MMMM", Locale.US);
    }
    
    public static float getClockAngleForHour(final int n, final int n2) {
        return ((n % 12 - 3) * HOUR_HAND_ANGLE_PER_HOUR + 360) % 360 + n2 / 60.0f * 30.0f;
    }
    
    public static float getClockAngleForMinutes(final int n) {
        return ((n - 15) * MINUTE_HAND_ANGLE_PER_MINUTE + 360) % 360;
    }

    public static String getDateLabel(Date d) {
        try {
            if (DateUtils.isToday(d.getTime())) {
                return HudApplication.getAppContext().getResources().getString(R.string.today);
            }
            Date now = new Date();
            Calendar nowCal = Calendar.getInstance();
            nowCal.setTime(now);
            Calendar dCal = Calendar.getInstance();
            dCal.setTime(d);
            int year1 = nowCal.get(Calendar.YEAR);
            int year2 = dCal.get(Calendar.YEAR);
            int dayOfYear1 = nowCal.get(Calendar.DAY_OF_YEAR);
            int dayOfYear2 = dCal.get(Calendar.DAY_OF_YEAR);
            if (year1 == year2 && dayOfYear1 - dayOfYear2 == 1) {
                return HudApplication.getAppContext().getResources().getString(R.string.yesterday);
            }
            String format;
            synchronized (dateLabelFormat) {
                format = dateLabelFormat.format(d);
            }
            return format;
        } catch (Throwable t) {
            sLogger.e(t);
            return null;
        }
    }
    
    public static Date parseIrmcDateStr(final String s) {
        final Date date = null;
        Date time;
        if (s == null) {
            time = date;
        }
        else {
            time = date;
            try {
                if (s.length() == 15) {
                    final Calendar instance = Calendar.getInstance();
                    instance.setTimeInMillis(0L);
                    instance.set(Calendar.YEAR, Integer.parseInt(s.substring(0, 4)));
                    instance.set(Calendar.MONTH, Integer.parseInt(s.substring(4, 6)) - 1);
                    instance.set(Calendar.DATE, Integer.parseInt(s.substring(6, 8)));
                    instance.set(Calendar.HOUR_OF_DAY, Integer.parseInt(s.substring(9, 11)));
                    instance.set(Calendar.MINUTE, Integer.parseInt(s.substring(11, 13)));
                    instance.set(Calendar.SECOND, Integer.parseInt(s.substring(13, 15)));
                    time = instance.getTime();
                }
            }
            catch (Throwable t) {
                DateUtil.sLogger.e(t);
                time = date;
            }
        }
        return time;
    }
}
