package com.navdy.hud.app.util;

import com.navdy.service.library.log.Logger;

public class NavdyNativeLibWrapper
{
    private static boolean loaded;
    private static final Logger sLogger;

    static {
        sLogger = new Logger(NavdyNativeLibWrapper.class);
    }

//    public static native void crashSegv();
//
//    public static native void crashSegvOnNewThread();

    public static void loadlibrary() {
        synchronized (NavdyNativeLibWrapper.class) {
            if (NavdyNativeLibWrapper.loaded) {
                return;
            }
            NavdyNativeLibWrapper.loaded = true;
            try {
                NavdyNativeLibWrapper.sLogger.v("loading navdy lib");
                System.loadLibrary("Navdy");
                NavdyNativeLibWrapper.sLogger.v("loaded navdy lib");
            }
            catch (Throwable t) {
                NavdyNativeLibWrapper.sLogger.e(t);
            }
        }
    }

//    public static synchronized native void startCpuHog();
//
//    public static synchronized native void stopCpuHog();
}
