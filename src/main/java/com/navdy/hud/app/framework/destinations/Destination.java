package com.navdy.hud.app.framework.destinations;
import com.navdy.hud.app.R;

public class Destination {
    final public java.util.List contacts;
    final public int destinationIcon;
    final public int destinationIconBkColor;
    final public String destinationPlaceId;
    final public String destinationSubtitle;
    final public String destinationTitle;
    final public com.navdy.hud.app.framework.destinations.Destination$DestinationType destinationType;
    final public double displayPositionLatitude;
    final public double displayPositionLongitude;
    final public String distanceStr;
    final public com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType favoriteDestinationType;
    final public String fullAddress;
    final public String identifier;
    final public String initials;
    public boolean isInitialNumber;
    final public double navigationPositionLatitude;
    final public double navigationPositionLongitude;
    public boolean needShortTitleFont;
    final public java.util.List phoneNumbers;
    final public com.navdy.hud.app.framework.destinations.Destination$PlaceCategory placeCategory;
    final public com.navdy.service.library.events.places.PlaceType placeType;
    final public String recentTimeLabel;
    final public int recentTimeLabelColor;
    final public boolean recommendation;
    
    public Destination(double d, double d0, double d1, double d2, String s, String s0, String s1, String s2, com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType a, com.navdy.hud.app.framework.destinations.Destination$DestinationType a0, String s3, com.navdy.hud.app.framework.destinations.Destination$PlaceCategory a1, String s4, int i, boolean b, int i0, int i1, String s5, com.navdy.service.library.events.places.PlaceType a2, String s6, java.util.List a3, java.util.List a4) {
        this.navigationPositionLatitude = d;
        this.navigationPositionLongitude = d0;
        this.displayPositionLatitude = d1;
        this.displayPositionLongitude = d2;
        this.fullAddress = s;
        this.destinationTitle = s0;
        this.destinationSubtitle = s1;
        this.identifier = s2;
        this.favoriteDestinationType = a;
        this.destinationType = a0;
        this.initials = s3;
        this.placeCategory = a1;
        this.recentTimeLabel = s4;
        this.recentTimeLabelColor = i;
        this.recommendation = b;
        this.destinationIcon = i0;
        this.destinationIconBkColor = i1;
        this.destinationPlaceId = s5;
        this.placeType = a2;
        this.distanceStr = s6;
        this.phoneNumbers = a3;
        this.contacts = a4;
        try {
            Integer.parseInt(s3);
            this.isInitialNumber = true;
        } catch(Throwable ignoredException) {
            this.isInitialNumber = false;
        }
    }
    
    private Destination(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        this(com.navdy.hud.app.framework.destinations.Destination$Builder.access$100(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$200(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$300(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$400(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$500(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$600(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$700(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$800(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$900(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$1000(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$1100(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$1200(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$1300(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$1400(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$1500(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$1600(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$1700(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$1800(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$1900(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$2000(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$2100(a), com.navdy.hud.app.framework.destinations.Destination$Builder.access$2200(a));
    }
    
    Destination(com.navdy.hud.app.framework.destinations.Destination$Builder a, com.navdy.hud.app.framework.destinations.Destination$1 a0) {
        this(a);
    }
    
    public static com.navdy.hud.app.framework.destinations.Destination getGasDestination() {
        String s = null;
        String s0 = null;
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn()) {
            s = a.getString(R.string.suggested_dest_find_gas_on_route_title);
            s0 = a.getString(R.string.suggested_dest_find_gas_on_route_desc);
        } else {
            s = a.getString(R.string.suggested_dest_find_gas_title);
            s0 = a.getString(R.string.suggested_dest_find_gas_desc);
        }
        return new com.navdy.hud.app.framework.destinations.Destination$Builder().destinationTitle(s).destinationSubtitle(s0).favoriteDestinationType(com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.FAVORITE_NONE).destinationType(com.navdy.hud.app.framework.destinations.Destination$DestinationType.FIND_GAS).build();
    }
    
    public String toString() {
        return new StringBuilder().append("id[").append(this.identifier).append("] placeid[").append(this.destinationPlaceId).append("] nav_pos[").append(this.navigationPositionLatitude).append(",").append(this.navigationPositionLongitude).append("]").append(" display_pos[").append(this.displayPositionLatitude).append(",").append(this.displayPositionLongitude).append("] title[").append(this.destinationTitle).append("] subTitle[").append(this.destinationSubtitle).append("] address[").append(this.fullAddress).append("] place_type[").append(this.placeType).append("]").toString();
    }
}
