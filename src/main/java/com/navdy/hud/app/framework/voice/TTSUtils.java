package com.navdy.hud.app.framework.voice;

import com.navdy.hud.app.R;
import com.navdy.hud.app.event.LocalSpeechRequest;
import java.util.Locale;
import android.text.TextUtils;

import com.navdy.hud.app.framework.toast.ToastManager$ToastParams;
import com.navdy.service.library.events.audio.SpeechRequest;
import com.navdy.hud.app.device.gps.GpsUtils;
import com.navdy.hud.app.framework.toast.IToastCallback;
import com.navdy.hud.app.framework.toast.ToastManager;
import android.os.Bundle;
import com.navdy.hud.app.util.DeviceUtil;
import android.content.res.Resources;
import java.io.File;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.squareup.otto.Bus;

public class TTSUtils
{
    public static final String DEBUG_GPS_CALIBRATION_IMU_DONE = "Ublox IMU Calibration Done";
    public static final String DEBUG_GPS_CALIBRATION_SENSOR_DONE = "Ublox Sensor Calibration Done";
    public static final String DEBUG_GPS_CALIBRATION_SENSOR_NOT_NEEDED = "Ublox Sensor Calibration Not Needed";
    public static final String DEBUG_GPS_CALIBRATION_STARTED = "Ublox Calibration started";
    private static final String DEBUG_GPS_SWITCH_TOAST_ID = "debug-tts-gps-switch";
    public static final String DEBUG_TTS_FASTER_ROUTE_AVAILABLE = "Faster route is available";
    public static final String DEBUG_TTS_TRAFFIC_NOT_USED_IN_ROUTE_CALC = "Traffic Not used for Route Calculation";
    public static final String DEBUG_TTS_UBLOX_DR_ENDED = "Dead Reckoning has Stopped";
    public static final String DEBUG_TTS_UBLOX_DR_STARTED = "Dead Reckoning has Started";
    public static final String DEBUG_TTS_UBLOX_HAS_LOCATION_FIX = "Ublox Has Location Fix";
    public static final String DEBUG_TTS_USING_GPS_SPEED = "Using GPS RawSpeed";
    public static final String DEBUG_TTS_USING_OBD_SPEED = "Using OBD RawSpeed";
    public static final String TTS_AUTOMATIC_ZOOM;
    public static final String TTS_DIAL_BATTERY_EXTREMELY_LOW;
    public static final String TTS_DIAL_BATTERY_LOW;
    public static final String TTS_DIAL_BATTERY_VERY_LOW;
    public static final String TTS_DIAL_CONNECTED;
    public static final String TTS_DIAL_DISCONNECTED;
    public static final String TTS_DIAL_FORGOTTEN;
    public static final String TTS_FUEL_GAUGE_ADDED;
    public static final String TTS_FUEL_RANGE_GAUGE_ADDED;
    public static final String TTS_GLANCES_DISABLED;
    public static final String TTS_HEADING_GAUGE_ADDED;
    public static final String TTS_MANUAL_ZOOM;
    public static final String TTS_NAV_STOPPED;
    public static final String TTS_PHONE_BATTERY_LOW;
    public static final String TTS_PHONE_BATTERY_VERY_LOW;
    public static final String TTS_PHONE_OFFLINE;
    public static final String TTS_RPM_GAUGE_ADDED;
    public static final String TTS_TBT_DISABLED;
    public static final String TTS_TBT_ENABLED;
    public static final String TTS_TRAFFIC_DISABLED;
    public static final String TTS_TRAFFIC_ENABLED;
    private static final Bus bus;
    private static final boolean isDebugTTSEnabled;
    
    static {
        bus = RemoteDeviceManager.getInstance().getBus();
        final Resources resources = HudApplication.getAppContext().getResources();
        TTS_GLANCES_DISABLED = resources.getString(R.string.glances_disabled);
        TTS_DIAL_DISCONNECTED = resources.getString(R.string.tts_dial_disconnected);
        TTS_DIAL_CONNECTED = resources.getString(R.string.tts_dial_connected);
        TTS_DIAL_FORGOTTEN = resources.getString(R.string.tts_dial_forgotten);
        TTS_TRAFFIC_DISABLED = resources.getString(R.string.tts_traffic_disabled);
        TTS_TRAFFIC_ENABLED = resources.getString(R.string.tts_traffic_enabled);
        TTS_NAV_STOPPED = resources.getString(R.string.tts_nav_stopped);
        TTS_TBT_DISABLED = resources.getString(R.string.tts_tbt_disabled);
        TTS_TBT_ENABLED = resources.getString(R.string.tts_tbt_enabled);
        TTS_FUEL_RANGE_GAUGE_ADDED = resources.getString(R.string.tts_fuel_range_gauge_added);
        TTS_FUEL_GAUGE_ADDED = resources.getString(R.string.tts_fuel_gauge_added);
        TTS_RPM_GAUGE_ADDED = resources.getString(R.string.tts_rpm_gauge_added);
        TTS_HEADING_GAUGE_ADDED = resources.getString(R.string.tts_heading_gauge_added);
        TTS_DIAL_BATTERY_VERY_LOW = resources.getString(R.string.tts_dial_battery_very_low);
        TTS_DIAL_BATTERY_LOW = resources.getString(R.string.tts_dial_battery_low);
        TTS_DIAL_BATTERY_EXTREMELY_LOW = resources.getString(R.string.tts_dial_battery_extremely_low);
        TTS_PHONE_BATTERY_VERY_LOW = resources.getString(R.string.tts_phone_battery_very_low);
        TTS_PHONE_BATTERY_LOW = resources.getString(R.string.tts_phone_battery_low);
        TTS_PHONE_OFFLINE = resources.getString(R.string.phone_no_network);
        TTS_MANUAL_ZOOM = resources.getString(R.string.tts_manual_zoom);
        TTS_AUTOMATIC_ZOOM = resources.getString(R.string.tts_auto_zoon);
        isDebugTTSEnabled = new File("/sdcard/debug_tts").exists();
    }
    
    public static void debugShowDREnded() {
        if (TTSUtils.isDebugTTSEnabled && DeviceUtil.isNavdyDevice()) {
            final Bundle bundle = new Bundle();
            bundle.putInt("13", 3000);
            bundle.putInt("8", R.drawable.icon_gpserror);
            bundle.putString("4", "Dead Reckoning has Stopped");
            bundle.putInt("5", R.style.Glances_1);
            bundle.putString("17", "Dead Reckoning has Stopped");
            ToastManager.getInstance().addToast(new ToastManager$ToastParams("debug-tts-ublox-dr-end", bundle, null, true, false));
        }
    }
    
    public static void debugShowDRStarted(final String s) {
        if (TTSUtils.isDebugTTSEnabled && DeviceUtil.isNavdyDevice()) {
            final Bundle bundle = new Bundle();
            bundle.putInt("13", 3000);
            bundle.putInt("8", R.drawable.icon_gpserror);
            bundle.putString("2", s);
            bundle.putInt("3", R.style.Glances_1);
            bundle.putString("4", "Dead Reckoning has Started");
            bundle.putInt("5", R.style.Glances_1);
            bundle.putString("17", "Dead Reckoning has Started");
            ToastManager.getInstance().addToast(new ToastManager$ToastParams("debug-tts-ublox-dr-start", bundle, null, true, false));
        }
    }
    
    public static void debugShowFasterRouteToast(final String s, final String s2) {
        if (TTSUtils.isDebugTTSEnabled) {
            final Bundle bundle = new Bundle();
            bundle.putInt("13", 7000);
            bundle.putInt("8", R.drawable.icon_open_nav_disable_traffic);
            bundle.putString("4", s);
            bundle.putInt("5", R.style.Glances_1);
            bundle.putString("6", s2);
            bundle.putInt("7", R.style.Glances_2);
            bundle.putString("17", "Faster route is available");
            bundle.putBoolean("12", true);
            ToastManager.getInstance().addToast(new ToastManager$ToastParams("debug-tts-faster-route", bundle, null, true, false));
        }
    }
    
    public static void debugShowGotUbloxFix() {
        if (TTSUtils.isDebugTTSEnabled && DeviceUtil.isNavdyDevice()) {
            final Bundle bundle = new Bundle();
            bundle.putInt("13", 3000);
            bundle.putInt("8", R.drawable.icon_gpserror);
            bundle.putString("4", "Ublox Has Location Fix");
            bundle.putInt("5", R.style.Glances_1);
            bundle.putString("17", "Ublox Has Location Fix");
            ToastManager.getInstance().addToast(new ToastManager$ToastParams("debug-tts-ublox-fix", bundle, null, true, false));
        }
    }
    
    public static void debugShowGpsCalibrationStarted() {
        if (GpsUtils.isDebugRawGpsPosEnabled() && DeviceUtil.isNavdyDevice()) {
            final Bundle bundle = new Bundle();
            bundle.putInt("13", 3000);
            bundle.putInt("8", R.drawable.icon_gpserror);
            bundle.putString("2", "Ublox Calibration started");
            bundle.putInt("3", R.style.Glances_1);
            ToastManager.getInstance().addToast(new ToastManager$ToastParams("debug-gps-calibration_start", bundle, null, true, false));
        }
    }
    
    public static void debugShowGpsImuCalibrationDone() {
        if (GpsUtils.isDebugRawGpsPosEnabled() && DeviceUtil.isNavdyDevice()) {
            final Bundle bundle = new Bundle();
            bundle.putInt("13", 3000);
            bundle.putInt("8", R.drawable.icon_gpserror);
            bundle.putString("2", "Ublox IMU Calibration Done");
            bundle.putInt("3", R.style.Glances_1);
            ToastManager.getInstance().addToast(new ToastManager$ToastParams("debug-gps-calibration_imu", bundle, null, true, false));
        }
    }
    
    public static void debugShowGpsReset(final String s) {
        if (TTSUtils.isDebugTTSEnabled && DeviceUtil.isNavdyDevice()) {
            final Bundle bundle = new Bundle();
            bundle.putInt("13", 3000);
            bundle.putInt("8", R.drawable.icon_gpserror);
            bundle.putString("4", s);
            bundle.putInt("5", R.style.Glances_1);
            bundle.putInt("16", (int)HudApplication.getAppContext().getResources().getDimension(R.dimen.no_glances_width));
            bundle.putString("17", s);
            ToastManager.getInstance().addToast(new ToastManager$ToastParams("debug-tts-gps-reset", bundle, null, true, false));
        }
    }
    
    public static void debugShowGpsSensorCalibrationDone() {
        if (GpsUtils.isDebugRawGpsPosEnabled() && DeviceUtil.isNavdyDevice()) {
            final Bundle bundle = new Bundle();
            bundle.putInt("13", 3000);
            bundle.putInt("8", R.drawable.icon_gpserror);
            bundle.putString("2", "Ublox Sensor Calibration Done");
            bundle.putInt("3", R.style.Glances_1);
            ToastManager.getInstance().addToast(new ToastManager$ToastParams("debug-gps-calibration_sensor", bundle, null, true, false));
        }
    }
    
    public static void debugShowGpsSensorCalibrationNotNeeded() {
        if (GpsUtils.isDebugRawGpsPosEnabled() && DeviceUtil.isNavdyDevice()) {
            final Bundle bundle = new Bundle();
            bundle.putInt("13", 3000);
            bundle.putInt("8", R.drawable.icon_gpserror);
            bundle.putString("2", "Ublox Sensor Calibration Not Needed");
            bundle.putInt("3", R.style.Glances_1);
            ToastManager.getInstance().addToast(new ToastManager$ToastParams("debug-gps-calibration_not_needed", bundle, null, true, false));
        }
    }
    
    public static void debugShowGpsSwitch(final String s, final String s2) {
        if (TTSUtils.isDebugTTSEnabled && DeviceUtil.isNavdyDevice()) {
            final Bundle bundle = new Bundle();
            bundle.putInt("13", 3000);
            bundle.putInt("8", R.drawable.icon_gpserror);
            bundle.putString("4", s);
            bundle.putInt("5", R.style.Glances_1);
            bundle.putString("6", s2);
            bundle.putInt("7", R.style.Glances_2);
            bundle.putInt("16", (int)HudApplication.getAppContext().getResources().getDimension(R.dimen.no_glances_width));
            bundle.putString("17", s);
            final ToastManager instance = ToastManager.getInstance();
            instance.dismissCurrentToast("debug-tts-gps-switch");
            instance.addToast(new ToastManager$ToastParams("debug-tts-gps-switch", bundle, null, true, false));
        }
    }
    
    public static void debugShowNoTrafficToast() {
        if (TTSUtils.isDebugTTSEnabled) {
            final Bundle bundle = new Bundle();
            bundle.putInt("13", 3000);
            bundle.putInt("8", R.drawable.icon_open_nav_disable_traffic);
            bundle.putString("4", "Traffic Not used for Route Calculation");
            bundle.putInt("5", R.style.Glances_1);
            bundle.putString("17", "Traffic Not used for Route Calculation");
            ToastManager.getInstance().addToast(new ToastManager$ToastParams("debug-tts-no-traffic", bundle, null, true, false));
        }
    }
    
    public static void debugShowSpeedInput(final String s) {
        if (TTSUtils.isDebugTTSEnabled && DeviceUtil.isNavdyDevice()) {
            final Bundle bundle = new Bundle();
            bundle.putInt("13", 3000);
            bundle.putInt("8", R.drawable.icon_mm_dash);
            bundle.putString("4", s);
            bundle.putInt("5", R.style.Glances_1);
            bundle.putString("17", s);
            ToastManager.getInstance().addToast(new ToastManager$ToastParams("debug-tts-speed-input", bundle, null, true, false));
        }
    }
    
    public static boolean isDebugTTSEnabled() {
        return TTSUtils.isDebugTTSEnabled;
    }
    
    public static void sendSpeechRequest(final String s, final SpeechRequest.Category category, final String s2) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            TTSUtils.bus.post(new LocalSpeechRequest(new SpeechRequest.Builder().words(s).category(category).id(s2).sendStatus(false).language(Locale.getDefault().toLanguageTag()).build()));
        }
    }
    
    public static void sendSpeechRequest(final String s, final String s2) {
        sendSpeechRequest(s, SpeechRequest.Category.SPEECH_NOTIFICATION, s2);
    }
}
