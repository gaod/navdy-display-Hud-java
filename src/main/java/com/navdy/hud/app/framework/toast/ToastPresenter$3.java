package com.navdy.hud.app.framework.toast;

final class ToastPresenter$3 implements com.navdy.hud.app.ui.component.ChoiceLayout.IListener {
    ToastPresenter$3() {
    }
    
    public void executeItem(int i, int i0) {
        if (com.navdy.hud.app.framework.toast.ToastPresenter.access$200() != null) {
            com.navdy.hud.app.framework.toast.ToastPresenter.access$200().executeChoiceItem(i, i0);
        }
    }
    
    public void itemSelected(int i, int i0) {
        com.navdy.hud.app.framework.toast.ToastPresenter.resetTimeout();
    }
}
