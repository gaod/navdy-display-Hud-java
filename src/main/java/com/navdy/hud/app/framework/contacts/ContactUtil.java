package com.navdy.hud.app.framework.contacts;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Priority;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView.Style;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.service.library.events.contacts.PhoneNumber.Builder;
import com.navdy.service.library.events.contacts.PhoneNumberType;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.service.library.task.TaskManager;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Transformation;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class ContactUtil {
    private static final String EMPTY = "";
    private static String HOME_STR = null;
    private static HashSet<Character> MARKER_CHARS = new HashSet();
    private static String MOBILE_STR = null;
    public static final String SPACE = " ";
    private static final char SPACE_CHAR = ' ';
    private static String WORK_STR;
    private static StringBuilder builder = new StringBuilder();
    private static Handler handler = new Handler(Looper.getMainLooper());

    public interface IContactCallback {
        boolean isContextValid();
    }

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        WORK_STR = resources.getString(R.string.work);
        MOBILE_STR = resources.getString(R.string.mobile);
        HOME_STR = resources.getString(R.string.home);
        ContactUtil.MARKER_CHARS.add('\u200e');
        ContactUtil.MARKER_CHARS.add('\u202a');
        ContactUtil.MARKER_CHARS.add('\u202c');
        ContactUtil.MARKER_CHARS.add('\u200e');
    }

    public static String getFirstName(String name) {
        if (TextUtils.isEmpty(name)) {
            return "";
        }
        name = name.trim();
        int index = name.indexOf(" ");
        if (index > 0) {
            return name.substring(0, index);
        }
        return name;
    }

    public static synchronized String getInitials(String name) {
        String str;
        synchronized (ContactUtil.class) {
            if (TextUtils.isEmpty(name)) {
                str = "";
            } else {
                name = GenericUtil.removePunctuation(name).trim();
                int index = name.indexOf(" ");
                if (index > 0) {
                    String first = name.substring(0, index).trim().replaceAll("[\\u202A]", "");
                    String last = name.substring(index + 1).trim().replaceAll("[\\u202A]", "");
                    builder.setLength(0);
                    if (!TextUtils.isEmpty(first)) {
                        builder.append(first.charAt(0));
                    }
                    if (!TextUtils.isEmpty(last)) {
                        builder.append(last.charAt(0));
                    }
                    str = builder.toString().toUpperCase();
                } else if (name.length() > 0) {
                    str = String.valueOf(name.charAt(0)).toUpperCase();
                } else {
                    str = "";
                }
            }
        }
        return str;
    }

    public static String getPhoneType(NumberType numberType) {
        if (numberType == null) {
            return null;
        }
        switch (numberType) {
            case WORK_MOBILE:
            case WORK:
                return WORK_STR;
            case MOBILE:
                return MOBILE_STR;
            case HOME:
                return HOME_STR;
            default:
                return null;
        }
    }

    public static boolean isValidNumber(String number) {
        try {
            if (TextUtils.isEmpty(number)) {
                return false;
            }
            PhoneNumber pNumber = PhoneNumberUtil.getInstance().parse(number, DriverProfileHelper.getInstance().getCurrentLocale().getCountry());
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    public static boolean isDisplayNameValid(String displayName, String phoneNumber, long nPhoneNum) {
        if (TextUtils.isEmpty(displayName) && TextUtils.isEmpty(phoneNumber)) {
            return false;
        }
        if (!TextUtils.isEmpty(displayName) && TextUtils.isEmpty(phoneNumber)) {
            return true;
        }
        if (phoneNumber.equalsIgnoreCase(displayName)) {
            return false;
        }
        try {
            if (PhoneNumberUtil.getInstance().parse(displayName, DriverProfileHelper.getInstance().getCurrentLocale().getCountry()).getNationalNumber() == nPhoneNum) {
                return false;
            }
        } catch (Throwable th) {
        }
        return true;
    }

    public static long getPhoneNumber(String number) {
        try {
            return PhoneNumberUtil.getInstance().parse(number, DriverProfileHelper.getInstance().getCurrentLocale().getCountry()).getNationalNumber();
        } catch (Throwable th) {
            return -1;
        }
    }

    public static boolean isImageAvailable(String contactName, String number) {
        if ((TextUtils.isEmpty(contactName) && TextUtils.isEmpty(number)) || TextUtils.isEmpty(number)) {
            return false;
        }
        return true;
    }

    public static void setContactPhoto(String contactName, String number, boolean triggerPhotoDownload, InitialsImageView imageView, Transformation transformation, IContactCallback callback) {
        imageView.setTag(null);
        if (TextUtils.isEmpty(contactName) && TextUtils.isEmpty(number)) {
            imageView.setImage(R.drawable.icon_user_numberonly, null, Style.DEFAULT);
        } else if (TextUtils.isEmpty(number)) {
            imageView.setImage(R.drawable.icon_user_numberonly, null, Style.DEFAULT);
        } else {
            if (!isDisplayNameValid(contactName, number, getPhoneNumber(number))) {
                contactName = null;
            }
            PhoneImageDownloader phoneImageDownloader = PhoneImageDownloader.getInstance();
            File imagePath = phoneImageDownloader.getImagePath(number, PhotoType.PHOTO_CONTACT);
            Bitmap bitmap = PicassoUtil.getBitmapfromCache(imagePath);
            if (bitmap != null) {
                imageView.setInitials(null, Style.DEFAULT);
                imageView.setImageBitmap(bitmap);
                setContactPhoto(contactName, number, triggerPhotoDownload, imageView, transformation, callback, imagePath, false, phoneImageDownloader, bitmap);
                return;
            }
            final String cName = contactName;
            final IContactCallback iContactCallback = callback;
            final File file = imagePath;
            final String str = number;
            final boolean z = triggerPhotoDownload;
            final InitialsImageView initialsImageView = imageView;
            final Transformation transformation2 = transformation;
            final PhoneImageDownloader phoneImageDownloader2 = phoneImageDownloader;
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    if (iContactCallback.isContextValid()) {
                        final boolean imageExists = file.exists();
                        ContactUtil.handler.post(new Runnable() {
                            public void run() {
                                ContactUtil.setContactPhoto(cName, str, z, initialsImageView, transformation2, iContactCallback, file, imageExists, phoneImageDownloader2, null);
                            }
                        });
                    }
                }
            }, 1);
        }
    }

    private static void setContactPhoto(String contactName, String number, boolean triggerPhotoDownload, InitialsImageView imageView, Transformation transformation, IContactCallback callback, File imagePath, boolean imageExists, PhoneImageDownloader phoneImageDownloader, Bitmap bitmap) {
        if (bitmap == null) {
            if (!callback.isContextValid()) {
                return;
            }
            if (imageExists) {
                updateInitials(Style.DEFAULT, null, imageView);
                setImage(imagePath, imageView, transformation);
            } else if (contactName != null) {
                ContactImageHelper contactImageHelper = ContactImageHelper.getInstance();
                imageView.setImage(contactImageHelper.getResourceId(contactImageHelper.getContactImageIndex(number)), getInitials(contactName), Style.LARGE);
            } else {
                imageView.setImage(R.drawable.icon_user_numberonly, null, Style.DEFAULT);
            }
        }
        if (triggerPhotoDownload) {
            phoneImageDownloader.clearPhotoCheckEntry(number, PhotoType.PHOTO_CONTACT);
            phoneImageDownloader.submitDownload(number, Priority.HIGH, PhotoType.PHOTO_CONTACT, contactName);
        }
    }

    private static void updateInitials(Style style, String initials, InitialsImageView imageView) {
        imageView.setInitials(initials, style);
    }

    private static void setImage(final File path, final InitialsImageView imageView, Transformation transformation) {
        PicassoUtil.getInstance().load(path).fit().transform(transformation).into(imageView, new Callback() {
            public void onSuccess() {
                imageView.setTag(path.getAbsolutePath());
            }

            public void onError() {
            }
        });
    }

    public static String sanitizeString(String str) {
        if (str == null) {
            return null;
        }
        boolean foundMarker = false;
        int len = str.length();
        char[] chrs = str.toCharArray();
        for (int i = 0; i < chrs.length; i++) {
            if (MARKER_CHARS.contains(chrs[i])) {
                foundMarker = true;
                chrs[i] = ' ';
            }
        }
        if (foundMarker) {
            return new String(chrs, 0, len).trim();
        }
        return str.trim();
    }

    public static NumberType getNumberType(PhoneNumberType numberType) {
        if (numberType == null) {
            return null;
        }
        switch (numberType) {
            case PHONE_NUMBER_HOME:
                return NumberType.HOME;
            case PHONE_NUMBER_MOBILE:
                return NumberType.MOBILE;
            case PHONE_NUMBER_WORK:
                return NumberType.WORK;
            default:
                return NumberType.OTHER;
        }
    }

    public static List<Contact> fromPhoneNumbers(List<com.navdy.service.library.events.contacts.PhoneNumber> phoneNumbers) {
        if (phoneNumbers == null) {
            return null;
        }
        List<Contact> result = new ArrayList<>(phoneNumbers.size());
        for (com.navdy.service.library.events.contacts.PhoneNumber phoneNumber : phoneNumbers) {
            result.add(new Contact(phoneNumber));
        }
        return result;
    }

    public static List<Contact> fromContacts(List<com.navdy.service.library.events.contacts.Contact> contacts) {
        if (contacts == null) {
            return null;
        }
        List<Contact> result = new ArrayList<>(contacts.size());
        for (com.navdy.service.library.events.contacts.Contact contact : contacts) {
            result.add(new Contact(contact));
        }
        return result;
    }

    public static PhoneNumberType convertNumberType(NumberType numberType) {
        if (numberType == null) {
            return null;
        }
        switch (numberType) {
            case WORK_MOBILE:
            case WORK:
                return PhoneNumberType.PHONE_NUMBER_WORK;
            case MOBILE:
                return PhoneNumberType.PHONE_NUMBER_MOBILE;
            case HOME:
                return PhoneNumberType.PHONE_NUMBER_HOME;
            default:
                return PhoneNumberType.PHONE_NUMBER_OTHER;
        }
    }

    public static List<com.navdy.service.library.events.contacts.PhoneNumber> toPhoneNumbers(List<Contact> contacts) {
        if (contacts == null) {
            return null;
        }
        List<com.navdy.service.library.events.contacts.PhoneNumber> result = new ArrayList<>(contacts.size());
        for (Contact c : contacts) {
            String str;
            Builder numberType = new Builder().number(c.number).numberType(convertNumberType(c.numberType));
            if (c.numberType == NumberType.OTHER) {
                str = c.numberTypeStr;
            } else {
                str = null;
            }
            result.add(numberType.customType(str).build());
        }
        return result;
    }

    public static List<com.navdy.service.library.events.contacts.Contact> toContacts(List<Contact> contacts) {
        if (contacts == null) {
            return null;
        }
        List<com.navdy.service.library.events.contacts.Contact> result = new ArrayList<>(contacts.size());
        for (Contact c : contacts) {
            String str;
            com.navdy.service.library.events.contacts.Contact.Builder numberType = new com.navdy.service.library.events.contacts.Contact.Builder().name(c.name).number(c.number).numberType(convertNumberType(c.numberType));
            if (c.numberType == NumberType.OTHER) {
                str = c.numberTypeStr;
            } else {
                str = null;
            }
            result.add(numberType.label(str).build());
        }
        return result;
    }
}