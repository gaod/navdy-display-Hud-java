package com.navdy.hud.app.framework.toast;

public class ToastManager {
    final private static com.navdy.hud.app.framework.toast.ToastManager sInstance;
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.framework.toast.ToastManager$ToastInfo currentToast;
    private android.os.Handler handler;
    private boolean isToastDisplayDisabled;
    private boolean isToastScreenDisplayed;
    private Runnable nextToast;
    private java.util.concurrent.LinkedBlockingDeque queue;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.toast.ToastManager.class);
        sInstance = new com.navdy.hud.app.framework.toast.ToastManager();
    }
    
    private ToastManager() {
        this.queue = new java.util.concurrent.LinkedBlockingDeque();
        this.isToastDisplayDisabled = false;
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.nextToast = (Runnable)new com.navdy.hud.app.framework.toast.ToastManager$1(this);
        com.navdy.hud.app.manager.RemoteDeviceManager a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        this.bus = a.getBus();
        this.uiStateManager = a.getUiStateManager();
    }
    
    static java.util.concurrent.LinkedBlockingDeque access$000(com.navdy.hud.app.framework.toast.ToastManager a) {
        return a.queue;
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static boolean access$200(com.navdy.hud.app.framework.toast.ToastManager a) {
        return a.isToastScreenDisplayed;
    }
    
    static boolean access$300(com.navdy.hud.app.framework.toast.ToastManager a) {
        return a.isToastDisplayDisabled;
    }
    
    static com.navdy.hud.app.framework.toast.ToastManager$ToastInfo access$402(com.navdy.hud.app.framework.toast.ToastManager a, com.navdy.hud.app.framework.toast.ToastManager$ToastInfo a0) {
        a.currentToast = a0;
        return a0;
    }
    
    static void access$500(com.navdy.hud.app.framework.toast.ToastManager a, com.navdy.hud.app.framework.toast.ToastManager$ToastInfo a0) {
        a.showToast(a0);
    }
    
    static void access$600(com.navdy.hud.app.framework.toast.ToastManager a, com.navdy.hud.app.framework.toast.ToastManager$ToastParams a0) {
        a.addToastInternal(a0);
    }
    
    static void access$700(com.navdy.hud.app.framework.toast.ToastManager a, String s, boolean b) {
        a.dismissCurrentToastInternal(s, b);
    }
    
    static void access$800(com.navdy.hud.app.framework.toast.ToastManager a, String s) {
        a.clearPendingToastInternal(s);
    }
    
    private void addToastInternal(com.navdy.hud.app.framework.toast.ToastManager$ToastParams a) {
        java.util.concurrent.LinkedBlockingDeque a0 = null;
        Throwable a1 = null;
        String s = a.id;
        label0: {
            label3: {
                label6: {
                    label7: {
                        if (s == null) {
                            break label7;
                        }
                        if (a.data != null) {
                            break label6;
                        }
                    }
                    sLogger.w("invalid toast passed");
                    break label3;
                }
                if (this.uiStateManager.getRootScreen() != null) {
                    if (this.isLocked()) {
                        sLogger.v(new StringBuilder().append("locked, cannot add toast[").append(a.id).append("]").toString());
                    } else {
                        a.data.putString("id", a.id);
                        com.navdy.hud.app.framework.toast.ToastManager$ToastInfo a2 = new com.navdy.hud.app.framework.toast.ToastManager$ToastInfo(a);
                        synchronized(this.queue) {
                            if (a.makeCurrent) {
                                com.navdy.hud.app.framework.toast.ToastManager$ToastInfo a3 = this.currentToast;
                                label5: {
                                    label4: {
                                        if (a3 == null) {
                                            break label4;
                                        }
                                        if (this.currentToast.dismissed) {
                                            break label4;
                                        }
                                        this.queue.addFirst(this.currentToast);
                                        sLogger.v(new StringBuilder().append("replace current, item to front:").append(this.currentToast.toastParams.id).toString());
                                        this.queue.addFirst(a2);
                                        this.dismissCurrentToast();
                                        break label5;
                                    }
                                    sLogger.v("replace current not reqd");
                                    this.queue.addFirst(a2);
                                    if (!this.isToastScreenDisplayed) {
                                        sLogger.v("replace called next:run");
                                        this.nextToast.run();
                                    }
                                }
                                /*monexit(a0)*/;
                            } else {
                                int i = this.queue.size();
                                label1: {
                                    label2: {
                                        if (i > 0) {
                                            break label2;
                                        }
                                        if (this.isToastScreenDisplayed) {
                                            break label2;
                                        }
                                        if (this.currentToast == null) {
                                            break label1;
                                        }
                                    }
                                    sLogger.v(new StringBuilder().append("[").append(a.id).append("] wait for display to be cleared size[").append(this.queue.size()).append("] current[").append(this.currentToast).append("] displayed[").append(this.isToastScreenDisplayed).append("]").toString());
                                    this.queue.add(a2);
                                    /*monexit(a0)*/;
                                    break label3;
                                }
                                if (this.isToastDisplayDisabled) {
                                    sLogger.v(new StringBuilder().append("[").append(a.id).append("] toasts disabled").toString());
                                    this.queue.add(a2);
                                    /*monexit(a0)*/;
                                } else {
                                    this.currentToast = a2;
                                    sLogger.v("push toast to display");
                                    this.showToast(a2);
                                    /*monexit(a0)*/;
                                }
                            }
                        }
                    }
                } else {
                    sLogger.w(new StringBuilder().append("toast [").append(a.id).append("] not accepted, Main screen not on").toString());
                }
            }
            return;
        }
    }
    
    private void clearPendingToastInternal(String s) {
        synchronized(this.queue) {
            if (s != null) {
                Object a0 = this.queue.iterator();
                while(((java.util.Iterator)a0).hasNext()) {
                    String s0 = ((com.navdy.hud.app.framework.toast.ToastManager$ToastInfo)((java.util.Iterator)a0).next()).toastParams.id;
                    if (android.text.TextUtils.equals((CharSequence)s, (CharSequence)s0)) {
                        sLogger.v(new StringBuilder().append("removed:").append(s0).toString());
                        ((java.util.Iterator)a0).remove();
                    }
                }
            } else {
                sLogger.v(new StringBuilder().append("removed-all:").append(this.queue.size()).toString());
                this.queue.clear();
                this.currentToast = null;
                this.isToastScreenDisplayed = false;
            }
            /*monexit(a)*/;
        }
    }
    
    private void dismissCurrentToast(String s, boolean b) {
        if (com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            this.dismissCurrentToastInternal(s, b);
        } else {
            this.handler.post((Runnable)new com.navdy.hud.app.framework.toast.ToastManager$3(this, s, b));
        }
    }
    
    private void dismissCurrentToastInternal(String s, boolean b) {
        label3: if (this.currentToast != null) {
            label2: {
                if (s == null) {
                    break label2;
                }
                if (android.text.TextUtils.equals((CharSequence)s, (CharSequence)this.currentToast.toastParams.id)) {
                    break label2;
                }
                sLogger.v(new StringBuilder().append("current toast[").append(this.currentToast.toastParams.id).append("] does not match[").append(s).append("]").toString());
                break label3;
            }
            label1: {
                label0: {
                    if (!b) {
                        break label0;
                    }
                    if (!this.currentToast.toastParams.removeOnDisable) {
                        break label0;
                    }
                    this.currentToast = null;
                    sLogger.v(new StringBuilder().append("dismiss current toast[").append(s).append("] removed").toString());
                    break label1;
                }
                this.currentToast.dismissed = true;
                sLogger.v(new StringBuilder().append("dismiss current toast[").append(s).append("] saved").toString());
            }
            com.navdy.hud.app.framework.toast.ToastPresenter.dismiss();
        }
    }
    
    public static com.navdy.hud.app.framework.toast.ToastManager getInstance() {
        return sInstance;
    }
    
    private boolean isLocked() {
        return this.currentToast != null && this.currentToast.toastParams.lock;
    }
    
    private void showToast(com.navdy.hud.app.framework.toast.ToastManager$ToastInfo a) {
        sLogger.v(new StringBuilder().append("[show-toast] id[").append(a.toastParams.id).append("]").toString());
        com.navdy.hud.app.framework.toast.ToastPresenter.present(this.uiStateManager.getToastView(), a);
    }
    
    public void addToast(com.navdy.hud.app.framework.toast.ToastManager$ToastParams a) {
        if (a != null) {
            if (com.navdy.hud.app.util.GenericUtil.isMainThread()) {
                this.addToastInternal(a);
            } else {
                this.handler.post((Runnable)new com.navdy.hud.app.framework.toast.ToastManager$2(this, a));
            }
        } else {
            sLogger.w("invalid toast params");
        }
    }
    
    public void clearAllPendingToast() {
        if (com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            this.clearPendingToastInternal((String)null);
        } else {
            this.handler.post((Runnable)new com.navdy.hud.app.framework.toast.ToastManager$4(this));
        }
    }
    
    public void clearPendingToast(String s) {
        if (com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            this.clearPendingToastInternal(s);
        } else {
            this.handler.post((Runnable)new com.navdy.hud.app.framework.toast.ToastManager$5(this, s));
        }
    }
    
    public void clearPendingToast(String[] a) {
        int i = a.length;
        int i0 = 0;
        while(i0 < i) {
            this.clearPendingToast(a[i0]);
            i0 = i0 + 1;
        }
    }
    
    public void disableToasts(boolean b) {
        java.util.concurrent.LinkedBlockingDeque a = null;
        Throwable a0 = null;
        sLogger.v(new StringBuilder().append("disableToast [").append(b).append("]").toString());
        boolean b0 = this.isToastDisplayDisabled;
        label0: {
            if (b0 != b) {
                this.isToastDisplayDisabled = b;
                if (b) {
                    if (this.isLocked()) {
                        sLogger.v("disableToast locked, cannot disable toast");
                    } else {
                        this.dismissCurrentToast(true);
                    }
                } else {
                    synchronized(this.queue) {
                        if (this.currentToast == null) {
                            this.handler.post(this.nextToast);
                        } else {
                            this.showToast(this.currentToast);
                        }
                        /*monexit(a)*/;
                    }
                }
            } else {
                sLogger.v("disableToast same state as before");
            }
            return;
        }
    }
    
    public void dismissCurrentToast() {
        this.dismissCurrentToast(this.getCurrentToastId(), false);
    }
    
    public void dismissCurrentToast(String s) {
        this.dismissCurrentToast(s, false);
    }
    
    void dismissCurrentToast(boolean b) {
        this.dismissCurrentToast(this.getCurrentToastId(), b);
    }
    
    public void dismissCurrentToast(String[] a) {
        int i = a.length;
        int i0 = 0;
        while(i0 < i) {
            this.dismissCurrentToast(a[i0]);
            i0 = i0 + 1;
        }
    }
    
    public void dismissToast(String s) {
        this.dismissCurrentToast(s);
        this.clearPendingToast(s);
    }
    
    public com.navdy.hud.app.framework.toast.IToastCallback getCallback(String s) {
        com.navdy.hud.app.framework.toast.IToastCallback a = null;
        label2: {
            label0: {
                label1: {
                    if (s == null) {
                        break label1;
                    }
                    if (this.currentToast == null) {
                        break label1;
                    }
                    if (android.text.TextUtils.equals((CharSequence)s, (CharSequence)this.currentToast.toastParams.id)) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = this.currentToast.toastParams.cb;
        }
        return a;
    }
    
    public String getCurrentToastId() {
        return (this.currentToast != null) ? this.currentToast.toastParams.id : null;
    }
    
    public boolean isCurrentToast(String s) {
        boolean b = false;
        label2: {
            label0: {
                label1: {
                    if (s == null) {
                        break label1;
                    }
                    if (this.currentToast == null) {
                        break label1;
                    }
                    if (android.text.TextUtils.equals((CharSequence)this.currentToast.toastParams.id, (CharSequence)s)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public boolean isToastDisplayed() {
        return this.isToastScreenDisplayed;
    }
    
    public void setToastDisplayFlag(boolean b) {
        this.isToastScreenDisplayed = b;
        if (!this.isToastScreenDisplayed) {
            this.currentToast = null;
            this.handler.removeCallbacks(this.nextToast);
            this.handler.post(this.nextToast);
        }
    }
}
