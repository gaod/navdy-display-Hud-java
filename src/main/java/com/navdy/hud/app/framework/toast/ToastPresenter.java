package com.navdy.hud.app.framework.toast;
import android.view.View;

import com.navdy.hud.app.R;

final public class ToastPresenter {
    final public static int ANIMATION_TRANSLATION;
    final private static java.util.List DISMISS_CHOICE;
    final public static String EXTRA_CHOICE_LAYOUT_PADDING = "15";
    final public static String EXTRA_CHOICE_LIST = "20";
    final public static String EXTRA_DEFAULT_CHOICE = "12";
    final public static String EXTRA_INFO_CONTAINER_DEFAULT_MAX_WIDTH = "16_1";
    final public static String EXTRA_INFO_CONTAINER_LEFT_PADDING = "16_2";
    final public static String EXTRA_INFO_CONTAINER_MAX_WIDTH = "16";
    final public static String EXTRA_MAIN_IMAGE = "8";
    final public static String EXTRA_MAIN_IMAGE_CACHE_KEY = "9";
    final public static String EXTRA_MAIN_IMAGE_INITIALS = "10";
    final public static String EXTRA_MAIN_TITLE = "1";
    final public static String EXTRA_MAIN_TITLE_1 = "2";
    final public static String EXTRA_MAIN_TITLE_1_STYLE = "3";
    final public static String EXTRA_MAIN_TITLE_2 = "4";
    final public static String EXTRA_MAIN_TITLE_2_STYLE = "5";
    final public static String EXTRA_MAIN_TITLE_3 = "6";
    final public static String EXTRA_MAIN_TITLE_3_STYLE = "7";
    final public static String EXTRA_MAIN_TITLE_STYLE = "1_1";
    final public static String EXTRA_NO_START_DELAY = "21";
    final public static String EXTRA_SHOW_SCREEN_ID = "18";
    final public static String EXTRA_SIDE_IMAGE = "11";
    final public static String EXTRA_SUPPORTS_GESTURE = "19";
    final public static String EXTRA_TIMEOUT = "13";
    final public static String EXTRA_TOAST_ID = "id";
    final public static String EXTRA_TTS = "17";
    final public static int TOAST_ANIM_DURATION_IN = 100;
    final public static int TOAST_ANIM_DURATION_OUT = 100;
    final public static int TOAST_ANIM_DURATION_OUT_QUICK = 50;
    final public static int TOAST_START_DELAY = 250;
    final private static com.squareup.otto.Bus bus;
    private static com.navdy.hud.app.ui.component.ChoiceLayout.IListener choiceListener;
    private static com.navdy.hud.app.framework.toast.ToastManager$ToastInfo currentInfo;
    private static com.navdy.hud.app.ui.component.ChoiceLayout.IListener defaultChoiceListener;
    final public static int defaultMaxInfoWidth;
    private static com.navdy.hud.app.device.light.LED.Settings gestureLedSettings;
    final private static com.navdy.hud.app.gesture.GestureServiceConnector gestureServiceConnector;
    private static android.os.Handler handler;
    private static String id;
    private static int mainSectionBottomPadding;
    final private static com.navdy.service.library.log.Logger sLogger;
    private static volatile String screenName;
    private static boolean supportsGesture;
    private static int timeout;
    private static Runnable timeoutRunnable;
    private static com.navdy.hud.app.framework.toast.IToastCallback toastCallback;
    private static com.navdy.hud.app.view.ToastView toastView;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.toast.ToastPresenter.class);
        DISMISS_CHOICE = (java.util.List)new java.util.ArrayList();
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        DISMISS_CHOICE.add(a.getString(R.string.dismiss));
        ANIMATION_TRANSLATION = (int)a.getDimension(R.dimen.toast_anim_translation);
        defaultMaxInfoWidth = (int)a.getDimension(R.dimen.carousel_main_right_section_width);
        com.navdy.hud.app.manager.RemoteDeviceManager a0 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        bus = a0.getBus();
        gestureServiceConnector = a0.getGestureServiceConnector();
        handler = new android.os.Handler(android.os.Looper.getMainLooper());
        timeoutRunnable = (Runnable)new com.navdy.hud.app.framework.toast.ToastPresenter$1();
        mainSectionBottomPadding = -1;
        defaultChoiceListener = (com.navdy.hud.app.ui.component.ChoiceLayout.IListener)new com.navdy.hud.app.framework.toast.ToastPresenter$2();
        choiceListener = (com.navdy.hud.app.ui.component.ChoiceLayout.IListener)new com.navdy.hud.app.framework.toast.ToastPresenter$3();
    }
    
    private ToastPresenter() {
    }
    
    static com.navdy.hud.app.view.ToastView access$000() {
        return toastView;
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static com.navdy.hud.app.framework.toast.IToastCallback access$200() {
        return toastCallback;
    }
    
    static void access$300(boolean b) {
        com.navdy.hud.app.framework.toast.ToastPresenter.dismissInternal(b);
    }
    
    static void access$400() {
        com.navdy.hud.app.framework.toast.ToastPresenter.clearInternal();
    }
    
    private static int applyTextAndStyle(android.widget.TextView a, android.os.Bundle a0, String s, String s0, int i) {
        return com.navdy.hud.app.util.ViewUtil.applyTextAndStyle(a, (CharSequence)a0.getString(s), i, a0.getInt(s0, -1));
    }
    
    public static void clear() {
        if (com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            com.navdy.hud.app.framework.toast.ToastPresenter.clearInternal();
        } else {
            handler.post((Runnable)new com.navdy.hud.app.framework.toast.ToastPresenter$5());
        }
    }
    
    private static void clearInternal() {
        sLogger.v("clearInternal");
        handler.removeCallbacks(timeoutRunnable);
        if (toastCallback != null) {
            sLogger.v(new StringBuilder().append("called onStop():").append(id).toString());
            toastCallback.onStop();
            if (supportsGesture) {
                com.navdy.hud.app.device.light.HUDLightUtils.removeSettings(gestureLedSettings);
            }
        }
        bus.post(new com.navdy.hud.app.framework.toast.ToastManager$DismissedToast(id));
        currentInfo = null;
        supportsGesture = false;
        toastCallback = null;
        sLogger.v("toast-cb null");
        toastView = null;
        id = null;
        timeout = 0;
    }
    
    public static void clearScreenName() {
        screenName = null;
    }
    
    public static void dismiss() {
        com.navdy.hud.app.framework.toast.ToastPresenter.dismiss(false);
    }
    
    public static void dismiss(boolean b) {
        if (com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            com.navdy.hud.app.framework.toast.ToastPresenter.dismissInternal(b);
        } else {
            handler.post((Runnable)new com.navdy.hud.app.framework.toast.ToastPresenter$4(b));
        }
    }
    
    private static void dismissInternal(boolean b) {
        sLogger.v("dismissInternal");
        handler.removeCallbacks(timeoutRunnable);
        if (toastView != null) {
            toastView.animateOut(b);
        }
    }
    
    public static com.navdy.hud.app.framework.toast.IToastCallback getCurrentCallback() {
        return toastCallback;
    }
    
    public static String getCurrentId() {
        return id;
    }
    
    public static String getScreenName() {
        return screenName;
    }
    
    public static boolean hasTimeout() {
        return timeout > 0;
    }
    
    static void present(com.navdy.hud.app.view.ToastView a, com.navdy.hud.app.framework.toast.ToastManager$ToastInfo a0) {
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a0 != null) {
                        break label0;
                    }
                }
                sLogger.e("invalid toast info");
                break label2;
            }
            currentInfo = a0;
            toastView = a;
            if (mainSectionBottomPadding == -1) {
                mainSectionBottomPadding = toastView.getMainLayout().mainSection.getPaddingBottom();
            }
            com.navdy.hud.app.framework.toast.ToastPresenter.updateView(a, a0);
        }
    }
    
    public static void resetTimeout() {
        if (timeout > 0) {
            handler.removeCallbacks(timeoutRunnable);
            handler.postDelayed(timeoutRunnable, (long)timeout);
            sLogger.v(new StringBuilder().append("reset timeout = ").append(timeout).toString());
        }
    }
    
    private static void updateView(com.navdy.hud.app.view.ToastView a, com.navdy.hud.app.framework.toast.ToastManager$ToastInfo a0) {
        int i = 0;
        android.content.Context a1 = com.navdy.hud.app.HudApplication.getAppContext();
        android.os.Bundle a2 = a0.toastParams.data;
        com.navdy.hud.app.ui.component.ConfirmationLayout a3 = a.getConfirmation();
        a3.screenTitle.setTextAppearance(a1, R.style.mainTitle);
        a3.title1.setTextAppearance(a1, R.style.title1);
        a3.title2.setTextAppearance(a1, R.style.title2);
        a3.title3.setTextAppearance(a1, R.style.title3);
        int i0 = a2.getInt("15", 0);
        if (i0 > 0) {
            android.widget.RelativeLayout.LayoutParams a4 = (android.widget.RelativeLayout.LayoutParams)a3.choiceLayout.getLayoutParams();
            a4.topMargin = 0;
            a4.leftMargin = 0;
            a4.rightMargin = 0;
            a4.bottomMargin = i0;
        }
        id = a2.getString("id");
        toastCallback = com.navdy.hud.app.framework.toast.ToastManager.getInstance().getCallback(id);
        sLogger.v((new StringBuilder().append("toast-cb-update :").append(toastCallback).toString() != null) ? "not null" : "null");
        int i1 = a2.getInt("8", 0);
        String s = a2.getString("9");
        String s0 = a2.getString("10");
        timeout = a2.getInt("13", 0);
        boolean b = a2.getBoolean("12", false);
        int i2 = a2.getInt("11", 0);
        screenName = a2.getString("18", (String)null);
        supportsGesture = a2.getBoolean("19");
        if (supportsGesture && !gestureServiceConnector.isRunning()) {
            sLogger.v("gesture not available, turn off");
            supportsGesture = false;
        }
        java.util.ArrayList a5 = a2.getParcelableArrayList("20");
        com.navdy.hud.app.view.MaxWidthLinearLayout a6 = (com.navdy.hud.app.view.MaxWidthLinearLayout)a3.findViewById(R.id.infoContainer);
        if (a2.containsKey("16")) {
            i = a2.getInt("16");
            if (i <= 0) {
                i = 0;
            }
        } else {
            i = (a2.containsKey("16_1")) ? defaultMaxInfoWidth : 0;
        }
        a6.setMaxWidth(i);
        ((android.view.ViewGroup.MarginLayoutParams)a6.getLayoutParams()).width = -2;
        if (a2.containsKey("16_2")) {
            a6.setPadding(a2.getInt("16_2"), a6.getPaddingTop(), a6.getPaddingRight(), a6.getPaddingBottom());
        }
        a3.fluctuatorView.setVisibility(View.GONE);
        int i3 = com.navdy.hud.app.framework.toast.ToastPresenter.applyTextAndStyle(a3.screenTitle, a2, "1", "1_1", 1);
        int i4 = com.navdy.hud.app.framework.toast.ToastPresenter.applyTextAndStyle(a3.title1, a2, "2", "3", 1);
        int i5 = com.navdy.hud.app.framework.toast.ToastPresenter.applyTextAndStyle(a3.title2, a2, "4", "5", 1);
        int i6 = com.navdy.hud.app.framework.toast.ToastPresenter.applyTextAndStyle(a3.title3, a2, "6", "7", 3);
        android.view.ViewGroup dummy = a3.mainSection;
        if (0 + i3 + i4 + i5 + i6 <= 4) {
            com.navdy.hud.app.util.ViewUtil.setBottomPadding((android.view.View)a3.mainSection, mainSectionBottomPadding);
        } else {
            com.navdy.hud.app.util.ViewUtil.setBottomPadding((android.view.View)a3.mainSection, mainSectionBottomPadding + a1.getResources().getDimensionPixelOffset(R.dimen.main_section_vertical_padding));
        }
        if (i1 == 0) {
            if (s == null) {
                a3.screenImage.setImageResource(0);
                a3.screenImage.setVisibility(View.GONE);
            } else {
                android.graphics.Bitmap a7 = com.navdy.hud.app.util.picasso.PicassoUtil.getBitmapfromCache(new java.io.File(s));
                if (a7 != null) {
                    a3.screenImage.setInitials((String)null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
                    a3.screenImage.setImageBitmap(a7);
                    a3.screenImage.setVisibility(View.VISIBLE);
                }
            }
        } else if (s0 == null) {
            a3.screenImage.setImage(i1, (String)null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
            a3.screenImage.setVisibility(View.VISIBLE);
        } else {
            a3.screenImage.setImage(i1, s0, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.LARGE);
            a3.screenImage.setVisibility(View.VISIBLE);
        }
        if (i2 == 0) {
            a3.sideImage.setVisibility(View.GONE);
        } else {
            a3.sideImage.setImageResource(i2);
            a3.sideImage.setVisibility(View.VISIBLE);
        }
        if (b) {
            a3.setChoices(DISMISS_CHOICE, 0, defaultChoiceListener);
            a3.choiceLayout.setVisibility(View.VISIBLE);
            supportsGesture = false;
        } else if (a5 == null) {
            supportsGesture = false;
            a3.choiceLayout.setVisibility(View.GONE);
        } else {
            if (supportsGesture && a5.size() > 2) {
                throw new RuntimeException("max 2 choice allowed when gesture are on");
            }
            a3.setChoicesList((java.util.List)a5, 0, choiceListener);
            a3.choiceLayout.setVisibility(View.VISIBLE);
        }
        if (supportsGesture) {
            a3.leftSwipe.setVisibility(View.VISIBLE);
            a3.rightSwipe.setVisibility(View.VISIBLE);
            toastView.gestureOn = true;
        } else {
            a3.leftSwipe.setVisibility(View.GONE);
            a3.rightSwipe.setVisibility(View.GONE);
            toastView.gestureOn = b;
        }
        if (timeout > 0) {
            handler.postDelayed(timeoutRunnable, (long)(timeout + 100));
            sLogger.v(new StringBuilder().append("timeout = ").append(timeout + 100).toString());
        }
        if (toastCallback != null) {
            sLogger.v(new StringBuilder().append("called onStart():").append(id).toString());
            toastCallback.onStart(a);
            if (supportsGesture) {
                gestureLedSettings = com.navdy.hud.app.device.light.HUDLightUtils.showGestureDetectionEnabled(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), new StringBuilder().append("Toast").append(a2.getString("1")).toString());
            }
        }
        boolean b0 = a2.getBoolean("21", false);
        a.animateIn((String)null, screenName, id, b0);
    }
}
