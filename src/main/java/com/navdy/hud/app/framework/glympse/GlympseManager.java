package com.navdy.hud.app.framework.glympse;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import com.navdy.hud.app.R;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereNavController$State;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.glympse.android.api.GPlace;
import java.util.UUID;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import android.support.annotation.Nullable;
import com.glympse.android.api.GUser;
import com.navdy.hud.app.profile.DriverProfile;
import com.glympse.android.core.GDrawable;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.util.PhoneUtil;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import android.text.TextUtils;
import com.glympse.android.api.GEventListener;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapSettings;
import java.util.HashMap;
import com.here.android.mpa.common.GeoCoordinate;
import java.util.Iterator;
import java.util.List;
import com.glympse.android.api.GTrackBuilder;
import com.glympse.android.core.GArray;
import com.glympse.android.api.GTicket;
import com.glympse.android.api.GTrack;
import java.util.Date;
import com.glympse.android.core.CoreFactory;
import com.here.android.mpa.routing.Maneuver;
import com.glympse.android.api.GlympseFactory;
import android.os.SystemClock;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.here.android.mpa.routing.Route;
import com.navdy.hud.app.maps.here.HereNavController;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.service.library.task.TaskManager;
import android.content.res.Resources;
import android.os.Looper;
import java.util.concurrent.TimeUnit;
import com.navdy.service.library.util.CredentialUtil;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.framework.contacts.Contact;
import java.util.Map;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.glympse.android.api.GGlympse;
import com.navdy.service.library.log.Logger;
import android.os.Handler;

public final class GlympseManager
{
    private static final String GLYMPSE_API_KEY;
    private static final String GLYMPSE_BASE_URL = "api.glympse.com";
    public static final int GLYMPSE_DURATION;
    public static final String GLYMPSE_TYPE_SHARE_LOCATION = "Location";
    public static final String GLYMPSE_TYPE_SHARE_TRIP = "Trip";
    private static final String NAVDY_CONTACT_UUID = "navdy_contact_uuid";
    private static final long NAVDY_GLYMPSE_PARTNER_ID = 1L;
    private static final int UPDATE_ETA_INTERVAL;
    private static final Handler handler;
    private static final Logger logger;
    private static final String[] messages;
    private String currentGlympseUserId;
    private GGlympse glympse;
    private boolean glympseInitFailed;
    private boolean isTrackingETA;
    private final NotificationManager notificationManager;
    private final Map<String, Contact> ticketsAwaitingReadReceipt;
    private Runnable updateETA;
    
    static {
        logger = new Logger(GlympseManager.class);
        final Resources resources = HudApplication.getAppContext().getResources();
        messages = new String[] { resources.getString(R.string.send_without_message), resources.getString(R.string.on_my_way), resources.getString(R.string.i_am_driving), resources.getString(R.string.running_late), resources.getString(R.string.i_am_here) };
        GLYMPSE_API_KEY = CredentialUtil.getCredentials(HudApplication.getAppContext(), "GLYMPSE_API_KEY");
        GLYMPSE_DURATION = (int)TimeUnit.HOURS.toMillis(1L);
        UPDATE_ETA_INTERVAL = (int)TimeUnit.MINUTES.toMillis(1L);
        handler = new Handler(Looper.getMainLooper());
    }
    
    private GlympseManager() {
        this.updateETA = new Runnable() {
            public void run() {
                if (GlympseManager.getInstance().isSynced()) {
                    TaskManager.getInstance().execute(new Runnable() {
                        public void run() {
                            if (HereMapsManager.getInstance().isInitialized()) {
                                HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
                                HereNavController navController = hereNavigationManager.getNavController();
                                if (navController.getState() == HereNavController$State.NAVIGATING) {
                                    final Date etaDate = navController.getEta(true, Route.TrafficPenaltyMode.OPTIMAL);
                                    if (HereMapUtil.isValidEtaDate(etaDate)) {
                                        long l1 = SystemClock.elapsedRealtime();
                                        GTrackBuilder trackBuilder = GlympseFactory.createTrackBuilder();
                                        trackBuilder.setSource(0);
                                        trackBuilder.setDistance((int) navController.getDestinationDistance());
                                        Route route = hereNavigationManager.getCurrentRoute();
                                        StringBuilder builder = new StringBuilder();
                                        if (route != null) {
                                            List<Maneuver> maneuverList = route.getManeuvers();
                                            if (maneuverList != null) {
                                                for (Maneuver maneuver : maneuverList) {
                                                    GeoCoordinate coordinate = maneuver.getCoordinate();
                                                    if (coordinate != null) {
                                                        double lat = coordinate.getLatitude();
                                                        double lng = coordinate.getLongitude();
                                                        trackBuilder.addLocation(CoreFactory.createLocation(lat, lng));
                                                        builder.append("{" + lat + HereManeuverDisplayBuilder.COMMA + lng + "} ");
                                                    }
                                                }
                                            }
                                        }
                                        GTrack track = trackBuilder.getTrack();
                                        GlympseManager.logger.v("update ETA track = " + builder.toString());
                                        GlympseManager.logger.v("update ETA time to generate track:" + (SystemClock.elapsedRealtime() - l1));
                                        final GTrack gTrack = track;
                                        GlympseManager.handler.post(new Runnable() {
                                            public void run() {
                                                long now = System.currentTimeMillis();
                                                long durationInMillis = etaDate.getTime() - now;
                                                if (durationInMillis < 0) {
                                                    durationInMillis = 0;
                                                }
                                                long minutes = durationInMillis / 60000;
                                                GArray<GTicket> activeTickets = GlympseManager.this.glympse.getHistoryManager().getTickets();
                                                int ticketListLength = activeTickets.length();
                                                for (int i = 0; i < ticketListLength; i++) {
                                                    GTicket ticket = (GTicket) activeTickets.at(i);
                                                    if (!ticket.isActive()) {
                                                        GlympseManager.logger.v("update ETA ticket not active:" + ticket.getId());
                                                    } else if (ticket.getDestination() == null) {
                                                        GlympseManager.logger.v("update ETA ticket not for trip:" + ticket.getId());
                                                    } else {
                                                        GlympseManager.logger.v("update ETA for ticket with id[" + ticket.getId() + "] durationMillis[" + durationInMillis + "] ETA[" + etaDate + "] currentTime=" + now + " minutes[" + minutes + "] expireTime[" + new Date(ticket.getExpireTime()) + "]");
                                                        ticket.updateEta(durationInMillis);
                                                        ticket.updateRoute(gTrack);
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    }, 1);
                    GlympseManager.handler.postDelayed(this, (long) GlympseManager.UPDATE_ETA_INTERVAL);
                    return;
                }
                GlympseManager.logger.v("update ETA: glympse not synched");
            }
        };
        this.notificationManager = NotificationManager.getInstance();
        this.ticketsAwaitingReadReceipt = new HashMap<String, Contact>();
        try {
            GlympseManager.logger.v("creating glympse");
            this.glympse = GlympseFactory.createGlympse(HudApplication.getAppContext(), GLYMPSE_BASE_URL, GlympseManager.GLYMPSE_API_KEY);
            this.setUpEventListener();
            this.glympse.setEtaMode(1);
            this.glympse.start();
            GlympseManager.logger.v("calling glympse start duration=" + MapSettings.getGlympseDuration() / TimeUnit.MINUTES.toMillis(1L) + " minutes");
            RemoteDeviceManager.getInstance().getBus().register(this);
        }
        catch (Throwable t) {
            GlympseManager.logger.e(t);
            this.glympseInitFailed = true;
        }
    }
    
    private void addGlympseReadGlance(final Contact contact) {
        this.notificationManager.addNotification(new GlympseNotification(contact, GlympseNotification.Type.READ));
    }
    
    private void addGlympseSentGlance(final Contact contact) {
        this.notificationManager.addNotification(new GlympseNotification(contact, GlympseNotification.Type.SENT));
    }
    
    private void addTicketListener(final GTicket gTicket) {
        gTicket.addListener(new GEventListener() {
            @Override
            public void eventsOccurred(final GGlympse gGlympse, final int n, final int n2, final Object o) {
                final GTicket gTicket = (GTicket)o;
                if (GlympseManager.this.hasGlympseEvent(n2, 8192)) {
                    final String id = gTicket.getId();
                    final String string = gTicket.getProperty(1L, "navdy_contact_uuid").getString();
                    GlympseManager.logger.v("ticket added, ticket id is " + id + "; contact uuid is " + string);
                    if (!TextUtils.isEmpty((CharSequence)string) && GlympseManager.this.ticketsAwaitingReadReceipt.containsKey(string)) {
                        GlympseManager.this.addGlympseSentGlance((Contact)GlympseManager.this.ticketsAwaitingReadReceipt.get(string));
                    }
                }
                if (GlympseManager.this.hasGlympseEvent(n2, 16384)) {
                    final String id2 = gTicket.getId();
                    final String string2 = gTicket.getProperty(1L, "navdy_contact_uuid").getString();
                    GlympseManager.logger.v("ticket updated, ticket id is " + id2 + "; contact uuid is " + string2);
                    if (GlympseManager.this.ticketsAwaitingReadReceipt.containsKey(string2)) {
                        final Contact contact = GlympseManager.this.ticketsAwaitingReadReceipt.remove(string2);
                        GlympseManager.this.addGlympseReadGlance(contact);
                        gTicket.removeListener(this);
                        String s;
                        if ((s = string2) == null) {
                            s = "";
                        }
                        AnalyticsSupport.recordGlympseViewed(s);
                        GlympseManager.logger.v("Glympse viewed ticket id is " + id2 + " contact[" + contact.name + "] number[" + contact.number + "] e164[" + PhoneUtil.convertToE164Format(contact.number) + "]");
                    }
                }
            }
        });
    }
    
    private void deleteActiveTickets() {
        GlympseManager.logger.v("deleteActiveTickets");
        this.stopTrackingETA();
        final GArray<GTicket> tickets = this.glympse.getHistoryManager().getTickets();
        for (int length = tickets.length(), i = 0; i < length; ++i) {
            final GTicket gTicket = tickets.at(i);
            GlympseManager.logger.v("deleteActiveTickets deleting ticket with id " + gTicket.getId());
            gTicket.deleteTicket();
        }
    }
    
    public static GlympseManager getInstance() {
        return InternalSingleton.singleton;
    }
    
    private boolean hasGlympseEvent(final int n, final int n2) {
        return (n & n2) != 0x0;
    }
    
    private void setUpEventListener() {
        this.glympse.addListener(new GEventListener() {
            @Override
            public void eventsOccurred(final GGlympse gGlympse, final int n, final int n2, final Object o) {
                if (GlympseManager.this.hasGlympseEvent(n2, 128)) {
                    GlympseManager.logger.v("synced with server, setting user profile");
                    GlympseManager.this.setUserProfile();
                }
                if (GlympseManager.this.hasGlympseEvent(n2, 131072)) {
                    GlympseManager.this.addTicketListener((GTicket)o);
                }
            }
        });
    }
    
    private void setUserProfile() {
        final DriverProfile currentProfile = DriverProfileHelper.getInstance().getCurrentProfile();
        if (TextUtils.equals((CharSequence)this.currentGlympseUserId, (CharSequence)currentProfile.getProfileName())) {
            GlympseManager.logger.v("called setUserProfile with same profile as current, no-op");
        }
        else {
            final GUser self = this.glympse.getUserManager().getSelf();
            if (!currentProfile.isDefaultProfile()) {
                this.deleteActiveTickets();
                this.ticketsAwaitingReadReceipt.clear();
                GlympseManager.logger.v("setting new user profile for Glympse: " + currentProfile.getProfileName());
                if (!TextUtils.isEmpty((CharSequence)currentProfile.getFirstName())) {
                    self.setNickname(currentProfile.getFirstName());
                }
                if (currentProfile.getDriverImage() != null) {
                    GlympseManager.logger.v("setting photo");
                    self.setAvatar(CoreFactory.createDrawable(currentProfile.getDriverImage()));
                }
                else {
                    GlympseManager.logger.v("not setting photo");
                    self.setAvatar(null);
                }
                this.currentGlympseUserId = currentProfile.getProfileName();
            }
            else {
                GlympseManager.logger.v("driverProfile is default, not setting it for Glympse");
            }
        }
    }
    
    private void startTrackingETA() {
        if (!this.isTrackingETA) {
            this.isTrackingETA = true;
            GlympseManager.handler.post(this.updateETA);
        }
    }
    
    private void stopTrackingETA() {
        GlympseManager.handler.removeCallbacks(this.updateETA);
        this.isTrackingETA = false;
    }
    
    public Error addMessage(final Contact contact, String convertToE164Format, @Nullable final String s, final double n, final double n2, final StringBuilder sb) {
        Error error;
        if (this.glympseInitFailed) {
            GlympseManager.logger.e("glympse init failed, cannot add message, no-op");
            error = Error.INTERNAL_ERROR;
        }
        else if (!NetworkStateManager.isConnectedToNetwork(HudApplication.getAppContext())) {
            GlympseManager.logger.v("addMessage, no connection, no-op");
            error = Error.NO_INTERNET;
        }
        else {
            final GPlace gPlace = null;
            int n3;
            if (!TextUtils.isEmpty((CharSequence)s) && (n != 0.0 || n2 != 0.0)) {
                n3 = 1;
            }
            else {
                n3 = 0;
            }
            GPlace place;
            if (n3 != 0) {
                place = GlympseFactory.createPlace(n, n2, CoreFactory.createString(s));
                this.startTrackingETA();
            }
            else {
                this.stopTrackingETA();
                place = gPlace;
            }
            final GTicket ticket = GlympseFactory.createTicket(MapSettings.getGlympseDuration(), convertToE164Format, place);
            convertToE164Format = PhoneUtil.convertToE164Format(contact.number);
            ticket.addInvite(GlympseFactory.createInvite(3, contact.name, convertToE164Format));
            final String string = UUID.randomUUID().toString();
            sb.setLength(0);
            sb.append(string);
            this.ticketsAwaitingReadReceipt.put(string, contact);
            ticket.appendData(1L, "navdy_contact_uuid", CoreFactory.createPrimitive(string));
            this.glympse.sendTicket(ticket);
            GlympseManager.logger.v("addMessage, added ticket name[" + contact.name + "] e164Number[" + convertToE164Format + "] number[" + contact.number + "] uuid[" + string + "]");
            error = Error.NONE;
        }
        return error;
    }
    
    public void expireActiveTickets() {
        if (this.isSynced()) {
            GlympseManager.logger.v("expireActiveTickets");
            this.stopTrackingETA();
            final GArray<GTicket> tickets = this.glympse.getHistoryManager().getTickets();
            for (int length = tickets.length(), i = 0; i < length; ++i) {
                final GTicket gTicket = tickets.at(i);
                if (!gTicket.isActive()) {
                    GlympseManager.logger.v("expireActiveTickets ticket[" + gTicket.getId() + "] not active");
                    break;
                }
                GlympseManager.logger.v("expireActiveTickets ticket[ " + gTicket.getId() + "] expiryTime[" + new Date(gTicket.getExpireTime()) + "]");
                gTicket.expire();
            }
        }
    }
    
    public String[] getMessages() {
        return GlympseManager.messages;
    }
    
    public boolean isSynced() {
        return !this.glympseInitFailed && this.glympse.getHistoryManager().isSynced();
    }
    
    @Subscribe
    public void onDriverProfileChanged(final DriverProfileChanged driverProfileChanged) {
        if (this.isSynced()) {
            this.setUserProfile();
        }
    }

    public enum Error {
        NONE(0),
        INTERNAL_ERROR(1),
        NO_INTERNET(2);

        private int value;
        Error(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
    private static class InternalSingleton
    {
        private static final GlympseManager singleton;
        
        static {
            singleton = new GlympseManager();
        }
    }
}
