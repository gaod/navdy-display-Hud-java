package com.navdy.hud.app.framework.notifications;

class NotificationManager$4 implements com.navdy.hud.app.ui.framework.INotificationAnimationListener {
    final com.navdy.hud.app.framework.notifications.NotificationManager this$0;
    
    NotificationManager$4(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        super();
        this.this$0 = a;
    }
    
    public void onStart(String s, com.navdy.hud.app.framework.notifications.NotificationType a, com.navdy.hud.app.ui.framework.UIStateManager.Mode a0) {
        com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v(new StringBuilder().append("notif-anim-start [").append(s).append("] type[").append(a).append("] mode [").append(a0).append("]").toString());
        com.navdy.hud.app.framework.notifications.NotificationManager.access$502(this.this$0, true);
        if (a0 != com.navdy.hud.app.ui.framework.UIStateManager.Mode.EXPAND) {
            com.navdy.hud.app.framework.notifications.NotificationManager.access$2002(this.this$0, false);
            com.navdy.hud.app.device.light.HUDLightUtils.removeSettings(com.navdy.hud.app.framework.notifications.NotificationManager.access$2100(this.this$0));
        } else {
            com.navdy.hud.app.framework.notifications.NotificationManager.access$1802(this.this$0, false);
            com.navdy.hud.app.view.NotificationView a1 = com.navdy.hud.app.framework.notifications.NotificationManager.access$100(this.this$0);
            a1.border.stopTimeout(true, (Runnable)null);
            com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v(new StringBuilder().append("notif-anim-start showing notif [").append(com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.getId()).append("]").toString());
            android.view.View a2 = com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.getView(a1.getContext());
            com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).pushBackDuetoHighPriority = false;
            com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).startCalled = true;
            com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.onStart(com.navdy.hud.app.framework.notifications.NotificationManager.access$1900(this.this$0));
            com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v(new StringBuilder().append("current notification [").append(com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.getId()).append("] called start[").append(System.identityHashCode(com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification)).append("]").toString());
            a1.addCustomView(com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification, a2);
            this.this$0.setNotificationColor();
            com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode.EXPAND);
        }
    }
    
    public void onStop(String s, com.navdy.hud.app.framework.notifications.NotificationType a, com.navdy.hud.app.ui.framework.UIStateManager.Mode a0) {
        com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v(new StringBuilder().append("notif-anim-stop [").append(s).append("] type[").append(a).append("] mode [").append(a0).append("]").toString());
        label17: {
            label0: {
                label8: {
                    label1: {
                        label4: {
                            label2: {
//                                try {
                                    com.navdy.hud.app.framework.notifications.INotification a1 = this.this$0.pendingNotification;
                                    this.this$0.pendingNotification = null;
                                    com.navdy.hud.app.ui.framework.UIStateManager.Mode a2 = com.navdy.hud.app.ui.framework.UIStateManager.Mode.COLLAPSE;
                                    label16: {
                                        if (a0 == a2) {
                                            break label16;
                                        }
                                        com.navdy.hud.app.framework.notifications.NotificationManager.access$2002(this.this$0, true);
                                        com.navdy.hud.app.framework.notifications.NotificationManager$Info a3 = com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0);
                                        label15: {
                                            label13: {
                                                label14: {
                                                    if (a3 == null) {
                                                        break label14;
                                                    }
                                                    if (com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).removed) {
                                                        break label14;
                                                    }
                                                    if (com.navdy.hud.app.framework.notifications.NotificationManager.access$2800(this.this$0) == null) {
                                                        break label13;
                                                    }
                                                }
                                                com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("notif-anim-start got removed/changed while it was animating, hide it");
                                                com.navdy.hud.app.framework.notifications.NotificationManager.access$502(this.this$0, false);
                                                this.this$0.hideNotification();
                                                break label15;
                                            }
                                            this.this$0.setNotificationColor();
                                            com.navdy.hud.app.framework.notifications.NotificationManager.access$3500(this.this$0).showNextNotificationColor();
                                            com.navdy.hud.app.framework.notifications.NotificationManager.access$1900(this.this$0).startTimeout(com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.getTimeout());
                                        }
                                        com.navdy.hud.app.framework.notifications.NotificationManager.access$2102(this.this$0, com.navdy.hud.app.device.light.HUDLightUtils.showGestureDetectionEnabled(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), "Notification"));
                                        break label2;
                                    }
                                    label11: {
                                        label10: {
                                            label6: {
                                                boolean b = false;
                                                boolean b0 = false;
                                                try {
                                                    b = false;
                                                    com.navdy.hud.app.framework.notifications.NotificationManager.access$1802(this.this$0, true);
                                                    com.navdy.hud.app.view.NotificationView a4 = com.navdy.hud.app.framework.notifications.NotificationManager.access$100(this.this$0);
                                                    this.this$0.hideNotificationCoverView();
                                                    boolean b1 = com.navdy.hud.app.framework.notifications.NotificationManager.access$2200(this.this$0);
                                                    label12: {
                                                        if (!b1) {
                                                            break label12;
                                                        }
                                                        b = false;
                                                        com.navdy.hud.app.framework.notifications.NotificationManager.access$2202(this.this$0, false);
                                                        com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("*** user selected delete all glances");
                                                        a4.removeCustomView();
                                                        synchronized(com.navdy.hud.app.framework.notifications.NotificationManager.access$2300(this.this$0)) {
                                                            com.navdy.hud.app.framework.notifications.NotificationManager.access$2400(this.this$0);
                                                            /*monexit(a5)*/;
                                                            break label11;
                                                        }
                                                    }
                                                    b = false;
                                                    this.this$0.cleanupViews(a4);
                                                    if (com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0) == null) {
                                                        break label10;
                                                    }
                                                    b = false;
                                                    com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode.COLLAPSE);
                                                    String s0 = com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.getId();
                                                    com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v(new StringBuilder().append("notif-anim-start [").append(s0).append("]").toString());
                                                    com.navdy.hud.app.framework.notifications.NotificationManager.access$1900(this.this$0).stopTimeout(false);
                                                    if (!com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).removed) {
                                                        b = false;
                                                        if (!com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).resurrected) {
                                                            b = false;
                                                            if (!com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.isAlive()) {
                                                                com.navdy.service.library.log.Logger a10 = com.navdy.hud.app.framework.notifications.NotificationManager.sLogger;
                                                                b = false;
                                                                a10.v("notif-anim-stop not alive, marked removed");
                                                                com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).removed = true;
                                                            }
                                                        }
                                                    }
                                                    b = false;
                                                    boolean b2 = com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).removed;
                                                    label9: {
                                                        Object a11 = null;
                                                        Throwable a12 = null;
                                                        if (b2) {
                                                            com.navdy.service.library.log.Logger a13 = com.navdy.hud.app.framework.notifications.NotificationManager.sLogger;
                                                            b = false;
                                                            a13.v(new StringBuilder().append("notif-anim-stop removed [").append(s0).append("]").toString());
                                                            synchronized(com.navdy.hud.app.framework.notifications.NotificationManager.access$2300(this.this$0)) {
                                                                com.navdy.hud.app.framework.notifications.NotificationManager.access$2600(this.this$0, com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0), false);
                                                                /*monexit(a11)*/;
                                                                break label9;
                                                            }
                                                        } else {
                                                            com.navdy.service.library.log.Logger a15 = com.navdy.hud.app.framework.notifications.NotificationManager.sLogger;
                                                            b = false;
                                                            a15.v(new StringBuilder().append("notif-anim-stop pushed back [").append(s0).append("]").toString());
                                                            if (!com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).startCalled) {
                                                                break label9;
                                                            }
                                                            b = false;
                                                            com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).startCalled = false;
                                                            com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.onStop();
                                                            break label9;
                                                        }

                                                    }
                                                    b = false;
                                                    a4.removeCustomView();
                                                    boolean b3 = com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).resurrected;
                                                    label7: {
                                                        if (!b3) {
                                                            break label7;
                                                        }
                                                        com.navdy.service.library.log.Logger a18 = com.navdy.hud.app.framework.notifications.NotificationManager.sLogger;
                                                        b = false;
                                                        a18.v("notif-anim-stop notif resurrected");
                                                        com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).resurrected = false;
                                                        com.navdy.hud.app.framework.notifications.NotificationManager.access$502(this.this$0, false);
                                                        b = true;
                                                        com.navdy.hud.app.framework.notifications.NotificationManager.access$2700(this.this$0, true);
                                                        break label8;
                                                    }
                                                    b = false;
                                                    com.navdy.hud.app.framework.notifications.NotificationManager.access$402(this.this$0, (com.navdy.hud.app.framework.notifications.NotificationManager$Info)null);
                                                    com.navdy.hud.app.framework.notifications.NotificationManager$Info a19 = com.navdy.hud.app.framework.notifications.NotificationManager.access$2800(this.this$0);
                                                    label5: {
                                                        if (a19 != null) {
                                                            break label5;
                                                        }
                                                        b = false;
                                                        if (com.navdy.hud.app.framework.notifications.NotificationManager.access$2900(this.this$0) == null) {
                                                            break label5;
                                                        }
                                                        b = false;
                                                        if (!com.navdy.hud.app.framework.notifications.NotificationManager.access$3000(this.this$0, com.navdy.hud.app.framework.notifications.NotificationManager.access$2900(this.this$0))) {
                                                            break label5;
                                                        }
                                                        b = false;
                                                        this.this$0.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.hud.app.framework.notifications.NotificationManager.access$2900(this.this$0), com.navdy.hud.app.framework.notifications.NotificationManager.access$3100(this.this$0), com.navdy.hud.app.framework.notifications.NotificationManager.access$3200(this.this$0), false));
                                                        com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v(new StringBuilder().append("launched pending screen hp:").append(com.navdy.hud.app.framework.notifications.NotificationManager.access$2900(this.this$0)).toString());
                                                        com.navdy.hud.app.framework.notifications.NotificationManager.access$2902(this.this$0, (com.navdy.service.library.events.ui.Screen)null);
                                                        com.navdy.hud.app.framework.notifications.NotificationManager.access$3102(this.this$0, (android.os.Bundle)null);
                                                        com.navdy.hud.app.framework.notifications.NotificationManager.access$3202(this.this$0, null);
                                                        break label6;
                                                    }
                                                    b = false;
                                                    com.navdy.hud.app.framework.notifications.NotificationManager$Info a20 = com.navdy.hud.app.framework.notifications.NotificationManager.access$2800(this.this$0);
                                                    label3: {
                                                        if (a20 == null) {
                                                            break label3;
                                                        }
                                                        com.navdy.service.library.log.Logger a21 = com.navdy.hud.app.framework.notifications.NotificationManager.sLogger;
                                                        b = false;
                                                        a21.v(new StringBuilder().append("notif-anim-stop staged-notif [").append(com.navdy.hud.app.framework.notifications.NotificationManager.access$2800(this.this$0).notification.getId()).append("]").toString());
                                                        com.navdy.hud.app.framework.notifications.NotificationManager.access$402(this.this$0, com.navdy.hud.app.framework.notifications.NotificationManager.access$2800(this.this$0));
                                                        com.navdy.hud.app.framework.notifications.NotificationManager.access$2802(this.this$0, (com.navdy.hud.app.framework.notifications.NotificationManager$Info)null);
                                                        com.navdy.hud.app.framework.notifications.NotificationManager.access$502(this.this$0, false);
                                                        b = true;
                                                        this.this$0.showNotification();
                                                        break label4;
                                                    }
                                                    com.navdy.service.library.log.Logger a22 = com.navdy.hud.app.framework.notifications.NotificationManager.sLogger;
                                                    b = false;
                                                    a22.v("notif-anim-stop no staged-notif, check if there is pending one");
                                                    synchronized(com.navdy.hud.app.framework.notifications.NotificationManager.access$2300(this.this$0)) {
                                                        b0 = false;
                                                        if (com.navdy.hud.app.framework.notifications.NotificationManager.access$3300(this.this$0).size() <= 0) {
                                                            b0 = false;
                                                        } else {
                                                            b0 = false;
                                                            com.navdy.hud.app.framework.notifications.NotificationManager$Info a24 = (com.navdy.hud.app.framework.notifications.NotificationManager$Info)com.navdy.hud.app.framework.notifications.NotificationManager.access$3300(this.this$0).lastKey();
                                                            b0 = false;
                                                            if (a24.pushBackDuetoHighPriority) {
                                                                com.navdy.service.library.log.Logger a25 = com.navdy.hud.app.framework.notifications.NotificationManager.sLogger;
                                                                b0 = false;
                                                                a25.v(new StringBuilder().append("notif-anim-stop found pending [").append(a24.notification.getId()).append("]").toString());
                                                                com.navdy.hud.app.framework.notifications.NotificationManager.access$402(this.this$0, a24);
                                                                com.navdy.hud.app.framework.notifications.NotificationManager.access$502(this.this$0, false);
                                                                b0 = true;
                                                                this.this$0.showNotification();
                                                                a1 = null;
                                                                b0 = true;
                                                            } else {
                                                                b0 = false;
                                                            }
                                                        }
                                                        /*monexit(a23)*/;
                                                    }
                                                    b = b0;
                                                    if (com.navdy.hud.app.framework.notifications.NotificationManager.access$3400(this.this$0) == null) {
                                                        if (a1 == null) {
                                                            b = b0;
                                                            if (com.navdy.hud.app.framework.notifications.NotificationManager.access$2900(this.this$0) != null) {
                                                                b = b0;
                                                                this.this$0.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.hud.app.framework.notifications.NotificationManager.access$2900(this.this$0), com.navdy.hud.app.framework.notifications.NotificationManager.access$3100(this.this$0), com.navdy.hud.app.framework.notifications.NotificationManager.access$3200(this.this$0), false));
                                                                com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v(new StringBuilder().append("launched pending screen:").append(com.navdy.hud.app.framework.notifications.NotificationManager.access$2900(this.this$0)).toString());
                                                            }
                                                        } else {
                                                            com.navdy.service.library.log.Logger a30 = com.navdy.hud.app.framework.notifications.NotificationManager.sLogger;
                                                            b = b0;
                                                            a30.v(new StringBuilder().append("adding pending notification:").append(a1).append(" current = ").append(com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0)).toString());
                                                            com.navdy.hud.app.framework.notifications.NotificationManager.access$502(this.this$0, false);
                                                            this.this$0.addNotification(a1);
                                                        }
                                                    } else {
                                                        b = b0;
                                                        this.this$0.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_SHUTDOWN_CONFIRMATION, com.navdy.hud.app.framework.notifications.NotificationManager.access$3400(this.this$0), false));
                                                        com.navdy.hud.app.framework.notifications.NotificationManager.access$3402(this.this$0, (android.os.Bundle)null);
                                                    }
                                                    b = b0;
                                                    com.navdy.hud.app.framework.notifications.NotificationManager.access$2902(this.this$0, (com.navdy.service.library.events.ui.Screen)null);
                                                    com.navdy.hud.app.framework.notifications.NotificationManager.access$3102(this.this$0, (android.os.Bundle)null);
                                                    com.navdy.hud.app.framework.notifications.NotificationManager.access$3202(this.this$0, null);
                                                } catch(Throwable a31) {
                                                    if (!b) {
                                                        com.navdy.hud.app.ui.component.homescreen.HomeScreenView a32 = this.this$0.uiStateManager.getHomescreenView();
                                                        if (a32.hasModeView()) {
                                                            com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("mode-view: notif onStop show");
                                                            a32.animateInModeView();
                                                        }
                                                    }
                                                    throw a31;
                                                }
                                                if (b0) {
                                                    break label2;
                                                }
                                                com.navdy.hud.app.ui.component.homescreen.HomeScreenView a33 = this.this$0.uiStateManager.getHomescreenView();
                                                if (!a33.hasModeView()) {
                                                    break label2;
                                                }
                                                com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("mode-view: notif onStop show");
                                                a33.animateInModeView();
                                                break label2;
                                            }
                                            com.navdy.hud.app.ui.component.homescreen.HomeScreenView a34 = this.this$0.uiStateManager.getHomescreenView();
                                            if (!a34.hasModeView()) {
                                                break label1;
                                            }
                                            com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("mode-view: notif onStop show");
                                            a34.animateInModeView();
                                            break label1;
                                        }
                                        com.navdy.hud.app.ui.component.homescreen.HomeScreenView a35 = this.this$0.uiStateManager.getHomescreenView();
                                        if (!a35.hasModeView()) {
                                            break label0;
                                        }
                                        com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("mode-view: notif onStop show");
                                        a35.animateInModeView();
                                        break label0;
                                    }
                                    com.navdy.hud.app.ui.component.homescreen.HomeScreenView a36 = this.this$0.uiStateManager.getHomescreenView();
                                    if (a36.hasModeView()) {
                                        com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("mode-view: notif onStop show");
                                        a36.animateInModeView();
                                    }
//                                } catch(Throwable a37) {
//                                    com.navdy.hud.app.framework.notifications.NotificationManager.access$502(this.this$0, false);
//                                    throw a37;
//                                }
                                com.navdy.hud.app.framework.notifications.NotificationManager.access$502(this.this$0, false);
                                break label17;
                            }
                            com.navdy.hud.app.framework.notifications.NotificationManager.access$502(this.this$0, false);
                            break label17;
                        }
                        com.navdy.hud.app.framework.notifications.NotificationManager.access$502(this.this$0, false);
                        break label17;
                    }
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$502(this.this$0, false);
                    break label17;
                }
                com.navdy.hud.app.framework.notifications.NotificationManager.access$502(this.this$0, false);
                break label17;
            }
            com.navdy.hud.app.framework.notifications.NotificationManager.access$502(this.this$0, false);
        }
    }
}
