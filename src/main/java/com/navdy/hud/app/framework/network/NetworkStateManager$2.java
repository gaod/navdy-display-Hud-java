package com.navdy.hud.app.framework.network;

import android.content.Context;

class NetworkStateManager$2 implements Runnable {
    final com.navdy.hud.app.framework.network.NetworkStateManager this$0;
    
    NetworkStateManager$2(com.navdy.hud.app.framework.network.NetworkStateManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        label0: {
            Throwable a;
            label1: {
                label8: {
                    label2: {
                        label12: try {
                            com.navdy.hud.app.framework.network.NetworkStateManager.access$200().v("checking n/w state");
                            android.net.NetworkInfo[] a0 = ((android.net.ConnectivityManager)com.navdy.hud.app.HudApplication.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE)).getAllNetworkInfo();
                            label10: {
                                label11: {
                                    if (a0 == null) {
                                        break label11;
                                    }
                                    if (a0.length != 0) {
                                        break label10;
                                    }
                                }
                                com.navdy.hud.app.framework.network.NetworkStateManager.access$200().v("no n/w info");
                                break label12;
                            }
                            int i = 0;
                            label9: {
                                while(true) {
                                    if (i >= a0.length) {
                                        break label9;
                                    }
                                    if (a0[i].getType() == 8) {
                                        break;
                                    }
                                    i = i + 1;
                                }
                                com.navdy.hud.app.framework.network.NetworkStateManager.access$200().v("found dummy network we are up:" + a0[i].getTypeName() + " count down=" + NetworkStateManager.access$300(this.this$0));
                                com.navdy.hud.app.framework.network.NetworkStateManager.access$306(this.this$0);
                                if (com.navdy.hud.app.framework.network.NetworkStateManager.access$300(this.this$0) > 0) {
                                    break label8;
                                }
                                com.navdy.hud.app.framework.network.NetworkStateManager.access$200().v("found dummy network starting init");
                                com.navdy.hud.app.framework.network.NetworkStateManager.access$002(this.this$0);
                                boolean b = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected();
                                label5: {
                                    label6: {
                                        label7: {
                                            if (!b) {
                                                break label7;
                                            }
                                            if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isNetworkLinkReady()) {
                                                break label6;
                                            }
                                        }
                                        this.this$0.networkNotAvailable();
                                        break label5;
                                    }
                                    com.navdy.service.library.events.settings.NetworkStateChange a1 = com.navdy.hud.app.framework.network.NetworkStateManager.access$400(this.this$0);
                                    label3: {
                                        label4: {
                                            if (a1 == null) {
                                                break label4;
                                            }
                                            if (NetworkStateManager.access$400(this.this$0).networkAvailable) {
                                                break label3;
                                            }
                                        }
                                        this.this$0.networkNotAvailable();
                                        break label5;
                                    }
                                    this.this$0.networkAvailable();
                                }
                                this.this$0.bus.post(new com.navdy.hud.app.framework.network.NetworkStateManager$HudNetworkInitialized());
                            }
                            if (com.navdy.hud.app.framework.network.NetworkStateManager.access$000(this.this$0)) {
                                break label2;
                            }
                            com.navdy.hud.app.framework.network.NetworkStateManager.access$200().v("network state not initialized");
                            break label2;
                        } catch(Throwable a2) {
                            a = a2;
                            break label1;
                        }
                        if (com.navdy.hud.app.framework.network.NetworkStateManager.access$000(this.this$0)) {
                            break label0;
                        }
                        com.navdy.hud.app.framework.network.NetworkStateManager.access$600(this.this$0).postDelayed(com.navdy.hud.app.framework.network.NetworkStateManager.access$500(this.this$0), 1000L);
                        break label0;
                    }
                    if (com.navdy.hud.app.framework.network.NetworkStateManager.access$000(this.this$0)) {
                        break label0;
                    }
                    com.navdy.hud.app.framework.network.NetworkStateManager.access$600(this.this$0).postDelayed(com.navdy.hud.app.framework.network.NetworkStateManager.access$500(this.this$0), 1000L);
                    break label0;
                }
                if (com.navdy.hud.app.framework.network.NetworkStateManager.access$000(this.this$0)) {
                    break label0;
                }
                com.navdy.hud.app.framework.network.NetworkStateManager.access$600(this.this$0).postDelayed(com.navdy.hud.app.framework.network.NetworkStateManager.access$500(this.this$0), 1000L);
                break label0;
            }
            try {
                com.navdy.hud.app.framework.network.NetworkStateManager.access$200().e(a);
            } catch(Throwable a3) {
                if (!com.navdy.hud.app.framework.network.NetworkStateManager.access$000(this.this$0)) {
                    com.navdy.hud.app.framework.network.NetworkStateManager.access$600(this.this$0).postDelayed(com.navdy.hud.app.framework.network.NetworkStateManager.access$500(this.this$0), 1000L);
                }
                throw a3;
            }
            if (!com.navdy.hud.app.framework.network.NetworkStateManager.access$000(this.this$0)) {
                com.navdy.hud.app.framework.network.NetworkStateManager.access$600(this.this$0).postDelayed(com.navdy.hud.app.framework.network.NetworkStateManager.access$500(this.this$0), 1000L);
            }
        }
    }
}
