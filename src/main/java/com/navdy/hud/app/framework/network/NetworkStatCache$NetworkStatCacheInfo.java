package com.navdy.hud.app.framework.network;

public class NetworkStatCache$NetworkStatCacheInfo {
    public String destIP;
    public int rxBytes;
    public int txBytes;
    
    NetworkStatCache$NetworkStatCacheInfo() {
    }
    
    NetworkStatCache$NetworkStatCacheInfo(int i, int i0, String s) {
        this.txBytes = i;
        this.rxBytes = i0;
        this.destIP = s;
    }
}
