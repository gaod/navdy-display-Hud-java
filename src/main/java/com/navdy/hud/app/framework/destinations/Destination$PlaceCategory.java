package com.navdy.hud.app.framework.destinations;

public enum Destination$PlaceCategory {
    SUGGESTED(0),
    RECENT(1),
    SUGGESTED_RECENT(2);

    private int value;
    Destination$PlaceCategory(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class Destination$PlaceCategory extends Enum {
//    final private static com.navdy.hud.app.framework.destinations.Destination$PlaceCategory[] $VALUES;
//    final public static com.navdy.hud.app.framework.destinations.Destination$PlaceCategory RECENT;
//    final public static com.navdy.hud.app.framework.destinations.Destination$PlaceCategory SUGGESTED;
//    final public static com.navdy.hud.app.framework.destinations.Destination$PlaceCategory SUGGESTED_RECENT;
//
//    static {
//        SUGGESTED = new com.navdy.hud.app.framework.destinations.Destination$PlaceCategory("SUGGESTED", 0);
//        RECENT = new com.navdy.hud.app.framework.destinations.Destination$PlaceCategory("RECENT", 1);
//        SUGGESTED_RECENT = new com.navdy.hud.app.framework.destinations.Destination$PlaceCategory("SUGGESTED_RECENT", 2);
//        com.navdy.hud.app.framework.destinations.Destination$PlaceCategory[] a = new com.navdy.hud.app.framework.destinations.Destination$PlaceCategory[3];
//        a[0] = SUGGESTED;
//        a[1] = RECENT;
//        a[2] = SUGGESTED_RECENT;
//        $VALUES = a;
//    }
//
//    private Destination$PlaceCategory(String s, int i) {
//        super(s, i);
//    }
//
//    public static com.navdy.hud.app.framework.destinations.Destination$PlaceCategory valueOf(String s) {
//        return (com.navdy.hud.app.framework.destinations.Destination$PlaceCategory)Enum.valueOf(com.navdy.hud.app.framework.destinations.Destination$PlaceCategory.class, s);
//    }
//
//    public static com.navdy.hud.app.framework.destinations.Destination$PlaceCategory[] values() {
//        return $VALUES.clone();
//    }
//}
