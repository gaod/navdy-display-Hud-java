package com.navdy.hud.app.bluetooth.obex;

import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.InputStream;

public final class ClientSession extends ObexSession
{
    private static final String TAG = "ClientSession";
    private byte[] mConnectionId;
    private final InputStream mInput;
    private final boolean mLocalSrmSupported;
    private int mMaxTxPacketSize;
    private boolean mObexConnected;
    private boolean mOpen;
    private final OutputStream mOutput;
    private boolean mRequestActive;
    private final ObexTransport mTransport;
    
    public ClientSession(final ObexTransport mTransport) throws IOException {
        this.mConnectionId = null;
        this.mMaxTxPacketSize = 255;
        this.mInput = mTransport.openInputStream();
        this.mOutput = mTransport.openOutputStream();
        this.mOpen = true;
        this.mRequestActive = false;
        this.mLocalSrmSupported = mTransport.isSrmSupported();
        this.mTransport = mTransport;
    }
    
    public ClientSession(final ObexTransport mTransport, final boolean mLocalSrmSupported) throws IOException {
        this.mConnectionId = null;
        this.mMaxTxPacketSize = 255;
        this.mInput = mTransport.openInputStream();
        this.mOutput = mTransport.openOutputStream();
        this.mOpen = true;
        this.mRequestActive = false;
        this.mLocalSrmSupported = mLocalSrmSupported;
        this.mTransport = mTransport;
    }
    
    private void setRequestActive() throws IOException {
        synchronized (this) {
            if (this.mRequestActive) {
                throw new IOException("OBEX request is already being performed");
            }
        }
        this.mRequestActive = true;
    }
    // monitorexit(this)
    
    public void close() throws IOException {
        this.mOpen = false;
        this.mInput.close();
        this.mOutput.close();
    }
    
    public HeaderSet connect(final HeaderSet set) throws IOException {
        this.ensureOpen();
        if (this.mObexConnected) {
            throw new IOException("Already connected to server");
        }
        this.setRequestActive();
        int n = 4;
        byte[] header = null;
        if (set != null) {
            if (set.nonce != null) {
                this.mChallengeDigest = new byte[16];
                System.arraycopy(set.nonce, 0, this.mChallengeDigest, 0, 16);
            }
            header = ObexHelper.createHeader(set, false);
            n = 4 + header.length;
        }
        final byte[] array = new byte[n];
        final int maxRxPacketSize = ObexHelper.getMaxRxPacketSize(this.mTransport);
        array[0] = 16;
        array[1] = 0;
        array[2] = (byte)(maxRxPacketSize >> 8);
        array[3] = (byte)(maxRxPacketSize & 0xFF);
        if (header != null) {
            System.arraycopy(header, 0, array, 4, header.length);
        }
        if (array.length + 3 > 65534) {
            throw new IOException("Packet size exceeds max packet size for connect");
        }
        final HeaderSet set2 = new HeaderSet();
        this.sendRequest(128, array, set2, null, false);
        if (set2.responseCode == 160) {
            this.mObexConnected = true;
        }
        this.setRequestInactive();
        return set2;
    }
    
    public HeaderSet delete(final HeaderSet set) throws IOException {
        final Operation put = this.put(set);
        put.getResponseCode();
        final HeaderSet receivedHeader = put.getReceivedHeader();
        put.close();
        return receivedHeader;
    }
    
    public HeaderSet disconnect(final HeaderSet set) throws IOException {
        if (!this.mObexConnected) {
            throw new IOException("Not connected to the server");
        }
        this.setRequestActive();
        this.ensureOpen();
        final byte[] array = null;
        byte[] header;
        if (set != null) {
            if (set.nonce != null) {
                this.mChallengeDigest = new byte[16];
                System.arraycopy(set.nonce, 0, this.mChallengeDigest, 0, 16);
            }
            if (this.mConnectionId != null) {
                set.mConnectionID = new byte[4];
                System.arraycopy(this.mConnectionId, 0, set.mConnectionID, 0, 4);
            }
            if ((header = ObexHelper.createHeader(set, false)).length + 3 > this.mMaxTxPacketSize) {
                throw new IOException("Packet size exceeds max packet size");
            }
        }
        else {
            header = array;
            if (this.mConnectionId != null) {
                header = new byte[5];
                header[0] = -53;
                System.arraycopy(this.mConnectionId, 0, header, 1, 4);
            }
        }
        final HeaderSet set2 = new HeaderSet();
        this.sendRequest(129, header, set2, null, false);
        synchronized (this) {
            this.mObexConnected = false;
            this.setRequestInactive();
            return set2;
        }
    }
    
    public void ensureOpen() throws IOException {
        synchronized (this) {
            if (!this.mOpen) {
                throw new IOException("Connection closed");
            }
        }
    }
    // monitorexit(this)
    
    public Operation get(HeaderSet set) throws IOException {
        if (!this.mObexConnected) {
            throw new IOException("Not connected to the server");
        }
        this.setRequestActive();
        this.ensureOpen();
        if (set == null) {
            set = new HeaderSet();
        }
        else {
            final HeaderSet set2 = set = set;
            if (set2.nonce != null) {
                this.mChallengeDigest = new byte[16];
                System.arraycopy(set2.nonce, 0, this.mChallengeDigest, 0, 16);
                set = set2;
            }
        }
        if (this.mConnectionId != null) {
            set.mConnectionID = new byte[4];
            System.arraycopy(this.mConnectionId, 0, set.mConnectionID, 0, 4);
        }
        if (this.mLocalSrmSupported) {
            set.setHeader(151, (byte)1);
        }
        return new ClientOperation(this.mMaxTxPacketSize, this, set, true);
    }
    
    public long getConnectionID() {
        long convertToLong;
        if (this.mConnectionId == null) {
            convertToLong = -1L;
        }
        else {
            convertToLong = ObexHelper.convertToLong(this.mConnectionId);
        }
        return convertToLong;
    }
    
    public boolean isSrmSupported() {
        return this.mLocalSrmSupported;
    }
    
    public Operation put(final HeaderSet set) throws IOException {
        if (!this.mObexConnected) {
            throw new IOException("Not connected to the server");
        }
        this.setRequestActive();
        this.ensureOpen();
        HeaderSet set2;
        if (set == null) {
            set2 = new HeaderSet();
        }
        else {
            set2 = set;
            if (set.nonce != null) {
                this.mChallengeDigest = new byte[16];
                System.arraycopy(set.nonce, 0, this.mChallengeDigest, 0, 16);
                set2 = set;
            }
        }
        if (this.mConnectionId != null) {
            set2.mConnectionID = new byte[4];
            System.arraycopy(this.mConnectionId, 0, set2.mConnectionID, 0, 4);
        }
        if (this.mLocalSrmSupported) {
            set2.setHeader(151, (byte)1);
        }
        return new ClientOperation(this.mMaxTxPacketSize, this, set2, false);
    }

    public boolean sendRequest(int opCode, byte[] head, HeaderSet header, PrivateInputStream privateInput, boolean srmActive) throws IOException {
        if (head == null || head.length + 3 <= ObexHelper.MAX_PACKET_SIZE_INT) {
            boolean skipSend = false;
            boolean skipReceive = false;
            if (srmActive) {
                if (opCode == 2) {
                    skipReceive = true;
                } else if (opCode == 3) {
                    skipReceive = true;
                } else if (opCode == 131) {
                    skipSend = true;
                }
            }
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            out.write((byte) opCode);
            if (head == null) {
                out.write(0);
                out.write(3);
            } else {
                out.write((byte) ((head.length + 3) >> 8));
                out.write((byte) (head.length + 3));
                out.write(head);
            }
            if (!skipSend) {
                this.mOutput.write(out.toByteArray());
                this.mOutput.flush();
            }
            if (!skipReceive) {
                header.responseCode = this.mInput.read();
                int length = (this.mInput.read() << 8) | this.mInput.read();
                if (length > ObexHelper.getMaxRxPacketSize(this.mTransport)) {
                    throw new IOException("Packet received exceeds packet size limit");
                } else if (length > 3) {
                    byte[] data;
                    int bytesReceived;
                    if (opCode == 128) {
                        int version = this.mInput.read();
                        int flags = this.mInput.read();
                        this.mMaxTxPacketSize = (this.mInput.read() << 8) + this.mInput.read();
                        if (this.mMaxTxPacketSize > ObexHelper.MAX_CLIENT_PACKET_SIZE) {
                            this.mMaxTxPacketSize = ObexHelper.MAX_CLIENT_PACKET_SIZE;
                        }
                        if (this.mMaxTxPacketSize > ObexHelper.getMaxTxPacketSize(this.mTransport)) {
                            Log.w(TAG, "An OBEX packet size of " + this.mMaxTxPacketSize + "was" + " requested. Transport only allows: " + ObexHelper.getMaxTxPacketSize(this.mTransport) + " Lowering limit to this value.");
                            this.mMaxTxPacketSize = ObexHelper.getMaxTxPacketSize(this.mTransport);
                        }
                        if (length <= 7) {
                            return true;
                        }
                        data = new byte[(length - 7)];
                        bytesReceived = this.mInput.read(data);
                        while (bytesReceived != length - 7) {
                            bytesReceived += this.mInput.read(data, bytesReceived, data.length - bytesReceived);
                        }
                    } else {
                        data = new byte[(length - 3)];
                        bytesReceived = this.mInput.read(data);
                        while (bytesReceived != length - 3) {
                            bytesReceived += this.mInput.read(data, bytesReceived, data.length - bytesReceived);
                        }
                        if (opCode == 255) {
                            return true;
                        }
                    }
                    byte[] body = ObexHelper.updateHeaderSet(header, data);
                    if (!(privateInput == null || body == null)) {
                        privateInput.writeBytes(body, 1);
                    }
                    if (header.mConnectionID != null) {
                        this.mConnectionId = new byte[4];
                        System.arraycopy(header.mConnectionID, 0, this.mConnectionId, 0, 4);
                    }
                    if (header.mAuthResp != null) {
                        if (!handleAuthResp(header.mAuthResp)) {
                            setRequestInactive();
                            throw new IOException("Authentication Failed");
                        }
                    }
                    if (header.responseCode == ResponseCodes.OBEX_HTTP_UNAUTHORIZED && header.mAuthChall != null && handleAuthChall(header)) {
                        out.write(78);
                        out.write((byte) ((header.mAuthResp.length + 3) >> 8));
                        out.write((byte) (header.mAuthResp.length + 3));
                        out.write(header.mAuthResp);
                        header.mAuthChall = null;
                        header.mAuthResp = null;
                        byte[] sendHeaders = new byte[(out.size() - 3)];
                        System.arraycopy(out.toByteArray(), 3, sendHeaders, 0, sendHeaders.length);
                        return sendRequest(opCode, sendHeaders, header, privateInput, false);
                    }
                }
            }
            return true;
        }
        throw new IOException("header too large ");
    }

    public void setAuthenticator(final Authenticator mAuthenticator) throws IOException {
        if (mAuthenticator == null) {
            throw new IOException("Authenticator may not be null");
        }
        this.mAuthenticator = mAuthenticator;
    }
    
    public void setConnectionID(final long n) {
        if (n < 0L || n > 4294967295L) {
            throw new IllegalArgumentException("Connection ID is not in a valid range");
        }
        this.mConnectionId = ObexHelper.convertToByteArray(n);
    }
    
    public HeaderSet setPath(HeaderSet set, final boolean b, final boolean b2) throws IOException {
        if (!this.mObexConnected) {
            throw new IOException("Not connected to the server");
        }
        this.setRequestActive();
        this.ensureOpen();
        if (set == null) {
            set = new HeaderSet();
        }
        else {
            final HeaderSet set2 = set = set;
            if (set2.nonce != null) {
                this.mChallengeDigest = new byte[16];
                System.arraycopy(set2.nonce, 0, this.mChallengeDigest, 0, 16);
                set = set2;
            }
        }
        if (set.nonce != null) {
            this.mChallengeDigest = new byte[16];
            System.arraycopy(set.nonce, 0, this.mChallengeDigest, 0, 16);
        }
        if (this.mConnectionId != null) {
            set.mConnectionID = new byte[4];
            System.arraycopy(this.mConnectionId, 0, set.mConnectionID, 0, 4);
        }
        final byte[] header = ObexHelper.createHeader(set, false);
        final int n = 2 + header.length;
        if (n > this.mMaxTxPacketSize) {
            throw new IOException("Packet size exceeds max packet size");
        }
        int n2 = 0;
        if (b) {
            n2 = 0 + 1;
        }
        int n3 = n2;
        if (!b2) {
            n3 = (n2 | 0x2);
        }
        final byte[] array = new byte[n];
        array[0] = (byte)n3;
        array[1] = 0;
        if (set != null) {
            System.arraycopy(header, 0, array, 2, header.length);
        }
        final HeaderSet set3 = new HeaderSet();
        this.sendRequest(133, array, set3, null, false);
        this.setRequestInactive();
        return set3;
    }
    
    void setRequestInactive() {
        synchronized (this) {
            this.mRequestActive = false;
        }
    }
}
