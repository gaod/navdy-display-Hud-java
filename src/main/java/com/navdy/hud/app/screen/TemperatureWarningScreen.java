package com.navdy.hud.app.screen;

import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.view.TemperatureWarningView;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.squareup.otto.Bus;
import flow.Layout;
import javax.inject.Inject;
import javax.inject.Singleton;

@Layout(R.layout.temperature_warning_lyt)
public class TemperatureWarningScreen extends BaseScreen {

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {TemperatureWarningView.class})
    public class Module {
    }

    @Singleton
    public static class Presenter extends BasePresenter<TemperatureWarningView> {
        @Inject
        Bus mBus;

        public void finish() {
            this.mBus.post(new Builder().screen(Screen.SCREEN_BACK).build());
        }
    }

    public String getMortarScopeName() {
        return TemperatureWarningScreen.class.getName();
    }

    public Object getDaggerModule() {
        return new Module();
    }

    public Screen getScreen() {
        return Screen.SCREEN_TEMPERATURE_WARNING;
    }
}
