#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
pushd "$DIR" &> /dev/null

set -e # Any commands which fail will cause the shell script to exit

#apt install -y dos2unix sudo gawk android-tools-fsutils zip coreutils cpio

VNUM=$(git describe --tags | cut -d'-' -f1)
VERS=${VNUM}-$(git rev-parse --short HEAD)

echo -e "\n"\
"***********************\n"\
" Building ${VERS}     \n"\
"***********************\n"

# bash ./set_version.sh $VNUM

# echo '
# ****************
#  Build boot.img
# ****************'
# ./build_boot_img.sh
# sudo chown $(whoami) boot.img

#echo '
#***************
# Android Build
#***************'
#export ANDROID_BUILD_TOOLS_VERSION=24.2.1
#pushd $ANDROID_HOME
#cd ./tools/bin
#echo 'y' | android update sdk --no-ui --all --filter build-tools-24.2.1,android-24,extra-android-m2repository
#popd

echo '
***************
 Build Hud.apk
***************'
#mkdir -p $HOME/.local/share/apktool/framework
#./Hud/build.sh
#cp ./Hud/Hud.apk ./system/priv-app/Hud/
# pushd ./Hud
#sh ./gradlew assembleRelease
sh ./gradlew assemble
# popd

# echo '
# ***************
#  Build Obd.apk
# ***************'
# #./Obd/build.sh
# #cp ./Obd/Obd.apk ./system/priv-app/Obd/
# pushd ./Obd
# #sh ./gradlew assembleDebug
# sh ./gradlew assemble
# popd

# build_system_dist () {

# echo '
# ******************
#  Build system.img
# ******************'
# ./build_system_img.sh

# OUT=$DIR/dist/navdy-display-alelec-${VERS}$1
# rm -rf ${OUT} 2> /dev/null
# rm -rf ${OUT}.zip 2> /dev/null
# mkdir -p ${OUT}


# echo '
# ***************
#  Assemble Dist
# ***************'
# cp -l boot.img $OUT/
# mv system_new.img $OUT/system.img
# cp -l recovery.img $OUT/
# cp -l FLASH* $OUT/
# cp -l FACTORY_RESET* $OUT/
# cp -l WIN_INSTALL_USB_DRIVER.bat $OUT/
# cp -l android-info.txt $OUT/

# pushd $OUT &> /dev/null
# for IMG in system.img boot.img recovery.img; do
# echo "$IMG $(stat -c %s $IMG) $(sha256sum $IMG | cut -d' ' -f1)" >> info.txt
# done
# echo "version: ${VNUM}" >> info.txt
# popd &> /dev/null

# mkdir $OUT/tools
# cp -al tools/usb_driver_windows $OUT/tools/
# cp -al tools/nexus-tools-673c086 $OUT/tools/
# cp -al tools/linux $OUT/tools/
# cp -al tools/windows $OUT/tools/
# cp -al tools/macos $OUT/tools/

# echo $OUT
# ls -l $OUT
# pushd $OUT
# zip -r $OUT.zip *
# popd
# echo "
# Built ${OUT}
# "
# }

# echo '
# ****************************
#   RELEASE (ORIG_OBD) BUILD
# ****************************'
# perl -0777 -i -pe 's#ro\.build\.type=.*\n#ro.build.type=user\n#' system/build.prop
# perl -0777 -i -pe 's#:userdebug/release-keys#:user/release-keys#' system/build.prop

# cp ./Hud/build/outputs/apk/release/Hud-armeabi-v7a-release.apk ./system/priv-app/Hud/Hud.apk
# cp ./Hud/build/intermediates/transforms/mergeJniLibs/release/0/lib/armeabi-v7a/* ./system/priv-app/Hud/lib/arm/

# git checkout ./system/priv-app/Obd/Obd.apk

# build_system_dist -oobd

# echo '
# ******************
#   RELEASE BUILD
# ******************'
# perl -0777 -i -pe 's#ro\.build\.type=.*\n#ro.build.type=user\n#' system/build.prop
# perl -0777 -i -pe 's#:userdebug/release-keys#:user/release-keys#' system/build.prop

# cp ./Hud/build/outputs/apk/release/Hud-armeabi-v7a-release.apk ./system/priv-app/Hud/Hud.apk
# cp ./Hud/build/intermediates/transforms/mergeJniLibs/release/0/lib/armeabi-v7a/* ./system/priv-app/Hud/lib/arm/

# cp ./Obd//build/outputs/apk/release/Obd-armeabi-v7a-release.apk ./system/priv-app/Obd/Obd.apk

# build_system_dist

# echo '
# ******************
#     DEBUG BUILD
# ******************'
# perl -0777 -i -pe 's#ro\.build\.type=.*\n#ro.build.type=userdebug\n#' system/build.prop
# perl -0777 -i -pe 's#:user/release-keys#:userdebug/release-keys#' system/build.prop

# cp ./Hud/build/outputs/apk/debug/Hud-armeabi-v7a-debug.apk ./system/priv-app/Hud/Hud.apk
# cp ./Hud/build/intermediates/transforms/mergeJniLibs/debug/0/lib/armeabi-v7a/* ./system/priv-app/Hud/lib/arm/

# cp ./Obd//build/outputs/apk/debug/Obd-armeabi-v7a-debug.apk ./system/priv-app/Obd/Obd.apk

# build_system_dist -debug

echo '
DONE'
popd &> /dev/null
