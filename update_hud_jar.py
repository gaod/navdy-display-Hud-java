#!/usr/bin/env python3

import os
from pathlib import Path
import ruamel.std.zipfile as zipfile

hudjar = Path(__file__).parent / 'libs' / 'Hud.jar'
src = Path(__file__).parent / 'src' / 'main' / 'java'

with zipfile.ZipFile(hudjar, 'r') as jar:
    filelist = jar.namelist()

dellist = set()
for root, dirs, files in os.walk(src):
    for f in files:
        fpath = Path(root) / f
        rel = os.path.relpath(fpath, src)
        rel = rel.lstrip('.').replace('\\', '/')
        rel = rel.replace('.java', '.class')
        inner = rel.replace('.class', "$")
        for zf in filelist:
            if zf == rel or zf.startswith(inner):
                dellist.add(zf)

print(dellist)
if dellist:
    zipfile.delete_from_zip_file(hudjar, file_names=list(dellist))
